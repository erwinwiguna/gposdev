package com.gpos.tools;

import java.text.ParseException;

import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class ApiCall {
	public static HttpResponse<JsonNode> ApiResponse(String token, String body, long currentTime, String url, String method) throws ParseException 
	{
		JSONObject jObj = new JSONObject();
		HttpResponse<JsonNode> response = null;
		  try {
			  switch (method) {
			  case "post":				
				   response = Unirest.post(url) 
					   		.header("Authorization", "Bearer " + token)
							.header("Content-Type", "application/json")
							.header("Origin", "http://dexagroup.com")
							.header("Client-ID", "GPOS")
							.header("X-GOAPOTIK-Key", "41138489-1057-4e7e-ab93-9bc97b511cf6")
							.header("X-GOAPOTIK-Timestamp", Long.toString(currentTime))
							.body(body).asJson(); 
//							jObj = response.getBody()
//									.getObject()
//									.getJSONObject("data");	
							break;
			  case "put":				
				   response = Unirest.put(url)  
					   		.header("Authorization", "Bearer " + token)
							.header("Content-Type", "application/json")
							.header("Origin", "http://dexagroup.com")
							.header("Client-ID", "GPOS")
							.header("X-GOAPOTIK-Key", "41138489-1057-4e7e-ab93-9bc97b511cf6")
							.header("X-GOAPOTIK-Timestamp", Long.toString(currentTime))
							.body(body).asJson(); 
//							jObj = response.getBody()
//									.getObject()
//									.getJSONObject("data");	
							break;
			  case "delete":				
				   response = Unirest.delete(url) 
					   		.header("Authorization", "Bearer " + token)
							.header("Content-Type", "application/json")
							.header("Origin", "http://dexagroup.com")
							.header("Client-ID", "GPOS")
							.header("X-GOAPOTIK-Key", "41138489-1057-4e7e-ab93-9bc97b511cf6")
							.header("X-GOAPOTIK-Timestamp", Long.toString(currentTime))
							.body(body).asJson(); 
//							jObj = response.getBody()
//									.getObject()
//									.getJSONObject("data");	
							break;
			  case "get":				
				   response = Unirest.get(url) 
					   		.header("Authorization", "Bearer " + token)
							.header("Content-Type", "application/json")
							.header("Origin", "http://dexagroup.com")
							.header("Client-ID", "GPOS")
							.header("X-GOAPOTIK-Key", "41138489-1057-4e7e-ab93-9bc97b511cf6")
							.header("X-GOAPOTIK-Timestamp", Long.toString(currentTime)).asJson(); 
//							jObj = response.getBody()
//									.getObject()
//									.getJSONObject("data");
							break;
			  default:
				break;
			  }			  
			} catch (UnirestException e) {
			    e.printStackTrace();
			}
		return response;
	}
}
