package com.gpos.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.TimeZone;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class Token {
	public static String JsonBody(String body, long currentTime) throws ParseException 
	{	
		String token = "";	
		body = Base64.getEncoder().encodeToString(body.getBytes());		  
		  try {
			  HttpResponse<JsonNode> response = Unirest.post("https://apiguedev.dexagroup.com/v1/token")
						.header("Content-Type", "application/json")
						.header("Origin", "http://dexagroup.com")
						.header("Client-ID", "GPOS")
						.header("X-GOAPOTIK-Key", "41138489-1057-4e7e-ab93-9bc97b511cf6")
						.header("X-GOAPOTIK-Timestamp",Long.toString(currentTime))
						.header("X-GOAPOTIK-Data", body).asJson();
			  token = response.getBody()
								.getObject()
								.getJSONObject("data")
								.getString("token");	
			} catch (UnirestException e) {
			    e.printStackTrace();
			    token = "";
			}
		return token;
	}
	public static long GetCurrentTimeStamp() throws ParseException
	{
		//capturing today's date
        Date today = new Date();      
        //displaying this date
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        //convert string date time to long
        String timeStamp = df.format(today);
        Date date = df.parse(timeStamp);
        long unixtimeStamp = date.getTime()/1000; // remove 000	
		return unixtimeStamp;
	}
}
