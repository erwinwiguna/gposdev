package com.gpos.views.screens.seller;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.gpos.tools.ApiCall;
import com.gpos.tools.Token;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

public class OrderMenu extends SecureScreen {

	static Gson gson = new Gson();
	
	@Override
	public void doBuildTemplate(RunData data, Context context) {
		// TODO Auto-generated method stub
		try {
			int iCond = data.getParameters().getInt("condition");
    		String sKeywords = data.getParameters().getString("keywords");
    		int iLimit = data.getParameters().getInt("ViewLimit", 25);
    		int iPage = data.getParameters().getInt("pages");
    		int iLastPage = data.getParameters().getInt("lastpages");
    		int currentPages = 1;
    		
    		String url = "https://apiguedev.dexagroup.com/v1/orders";
			String merchantCode = "jkt_goapotik";
			String status = "all";
			
    		if(StringUtil.isNotEmpty(data.getParameters().getString("eventSubmit_doFind"))) {
    			String body = "";
    			if(iCond == 1) {
    				body = "{" + 
        					"  \"merchant_code\": \""+merchantCode+"\"," + 
        					"  \"status\": \""+status+"\"," + 
        					"  \"page\": "+currentPages+"," +
        					"  \"increment_id\": \""+sKeywords+"\"," +
        					"  \"limit\" : "+iLimit+" " + 
        					"}";
    			} else {
    				body = "{" + 
        					"  \"merchant_code\": \""+merchantCode+"\"," + 
        					"  \"status\": \""+status+"\"," + 
        					"  \"page\": "+currentPages+"," + 
        					"  \"prescription_id\": \""+sKeywords+"\"," + 
        					"  \"limit\" : "+iLimit+" " + 
        					"}";
    			}
//    			HttpResponse<JsonNode> test = Unirest.post("https://apigue.dexagroup.com/api/public/vi/partner/seller/order/list").header("Authorization","GoApotik:eyJhbGciOiJIUzI1NiJ9.MQ.uqtlW_34VuT6Amnbajp-mAmaAIZ7GznrTRzFtvtbigo").header("Content-Type","application/json").body(body).asJson();
    			long currentTime = Token.GetCurrentTimeStamp();
    			String token = Token.JsonBody(body, currentTime);
    			
    			if (token != "") {
    				HttpResponse<JsonNode> objectResponse = ApiCall.ApiResponse(token , body, currentTime, url, "post");
    				System.out.println(objectResponse.getBody());
    				context.put("objectResponse", objectResponse.getBody());
    				context.put("pages", currentPages);
    			}
    			
    		} else if(data.getParameters().getString("op").equals("next") || data.getParameters().getString("op").equals("prev")) {
    			if(data.getParameters().getString("op").equals("next")) {
    				if(iPage == iLastPage) {
    					currentPages = iPage;
    				} else {
    					currentPages = iPage + 1;
    				}
    			}else if (data.getParameters().getString("op").equals("prev")) {
    				if(iPage == 1) {
    					currentPages = 1;
    				} else {
    					currentPages = iPage - 1;
    				}
    			}
    			
    			String body = "";
    			if(iCond == 1) {
    				body = "{" + 
        					"  \"merchant_code\": \""+merchantCode+"\"," + 
        					"  \"status\": \""+status+"\"," + 
        					"  \"page\": "+currentPages+"," +
        					"  \"increment_id\": \""+sKeywords+"\"," +
        					"  \"limit\" : "+iLimit+" " + 
        					"}";
    			} else {
    				body = "{" + 
        					"  \"merchant_code\": \""+merchantCode+"\"," + 
        					"  \"status\": \""+status+"\"," + 
        					"  \"page\": "+currentPages+"," + 
        					"  \"prescription_id\": \""+sKeywords+"\"," + 
        					"  \"limit\" : "+iLimit+" " + 
        					"}";
    			}
    			
    			long currentTime = Token.GetCurrentTimeStamp();
    			String token = Token.JsonBody(body, currentTime);
    			
    			if (token != "") {
    				HttpResponse<JsonNode> objectResponse = ApiCall.ApiResponse(token , body, currentTime, url, "post");
    				context.put("objectResponse", objectResponse.getBody());
    				context.put("pages", currentPages);
    			}
    		}
		} catch (Exception e) {
			System.out.println(e);
			// TODO: handle exception
		}
	}
	
}
