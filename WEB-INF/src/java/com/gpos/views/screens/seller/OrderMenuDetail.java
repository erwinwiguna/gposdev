package com.gpos.views.screens.seller;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.gpos.tools.ApiCall;
import com.gpos.tools.Token;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

public class OrderMenuDetail extends SecureScreen {

	@Override
	public void doBuildTemplate(RunData data, Context context) {
		// TODO Auto-generated method stub
		try {
			String url = "https://apiguedev.dexagroup.com/v1/orders/detail";
			String urlTrx = "https://apiguedev.dexagroup.com/v1/transaction";
			String incrementId = data.getParameters().getString("id");
			String sComment = data.getParameters().getString("comment");
			
			String body = "{" + 
					"  \"increment_id\" : \""+incrementId+"\"" + 
					"}";
			String bodyTrx = "";
			if(StringUtil.isNotEmpty(data.getParameters().getString("eventSubmit_doPickup"))) {
    			String state = "ready_pickup";
    			bodyTrx = "{" + 
    					"  \"order_id\": \""+incrementId+"\"," +
    					"  \"state\": \""+state+"\"," +
    					"  \"comment\" : \""+sComment+"\" " + 
    					"}";
    			
    			long currentTime = Token.GetCurrentTimeStamp();
    			String token = Token.JsonBody(bodyTrx, currentTime);
    			
    			if (token != "") {
    				HttpResponse<JsonNode> objectResponse = ApiCall.ApiResponse(token , bodyTrx, currentTime, urlTrx, "put");
    				if(objectResponse.getBody().getObject().get("code").equals("SGP-01-002")) {
    					HttpResponse<JsonNode> response = this.getDataResponse(body, url);
    					context.put("objectResponse", response.getBody());
    					data.setMessage ("Order #"+incrementId+" is Ready To Pickup");
    				} else {
    					HttpResponse<JsonNode> response = this.getDataResponse(body, url);
    					context.put("objectResponse", response.getBody());
    					data.setMessage ("Order #"+incrementId+" Failed To Change Status");
    				}
    			}
    		} else if(StringUtil.isNotEmpty(data.getParameters().getString("eventSubmit_doReject"))) {
    			String state = "rejected";
    			bodyTrx = "{" + 
    					"  \"order_id\": \""+incrementId+"\"," +
    					"  \"state\": \""+state+"\"," +
    					"  \"comment\" : \""+sComment+"\" " + 
    					"}";
    			
    			long currentTime = Token.GetCurrentTimeStamp();
    			String token = Token.JsonBody(bodyTrx, currentTime);
    			
    			if (token != "") {
    				HttpResponse<JsonNode> objectResponse = ApiCall.ApiResponse(token , bodyTrx, currentTime, urlTrx, "put");
    				if(objectResponse.getBody().getObject().get("code").equals("SGP-01-002")) {
    					HttpResponse<JsonNode> response = this.getDataResponse(body, url);
    					context.put("objectResponse", response.getBody());
    					data.setMessage ("Order #"+incrementId+" Successfully Rejected");
    				} else {
    					HttpResponse<JsonNode> response = this.getDataResponse(body, url);
    					context.put("objectResponse", response.getBody());
    					data.setMessage ("Order #"+incrementId+" Failed To Change Status");
    				}
    			}
    		} else {
    			HttpResponse<JsonNode> response = this.getDataResponse(body, url);
    			System.out.println(response.getBody().getObject().get("code"));
    			context.put("objectResponse", response.getBody());
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public HttpResponse<JsonNode> getDataResponse(String body, String url) {
		HttpResponse<JsonNode> objectResponse = null;
		try {
			long currentTime = Token.GetCurrentTimeStamp();
			String token = Token.JsonBody(body, currentTime);
			if (token != "") {
				objectResponse = ApiCall.ApiResponse(token , body, currentTime, url, "post");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return objectResponse;
	}
}
