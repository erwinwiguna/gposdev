package com.ssti.enterprise.medical.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.medical.om.CustomerMargin;
import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.StringUtil;

public class CustomerMarginLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (CustomerMarginLoader.class );
	
	public static final String s_START_HEADING = "Customer Code";
	
	public CustomerMarginLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		String sMsg = "";
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCode = getString(_oRow, _iIdx + i); i++;
			String sName = getString(_oRow, _iIdx + i); i++;
			String sColor = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCode))
			{				
				Customer oCust = CustomerTool.getCustomerByCode(sCode);
				boolean bNew = true;
				boolean bValid = true;
				
				
				if (oCust != null)
				{
					CustomerMargin oData = null;// CustomerMarginTool.getByCustID(oCust.getCustomerId(), sColor);

					log.debug ("Column " + _iIdx + " Customer Code : " + sCode + " Color : " + sColor);
					if (oData == null) 
					{	
						log.debug ("Code Not Exist, create new Code");
						
						oData = new CustomerMargin();
						oData.setCustomerMarginId (IDGenerator.generateSysID());
						bNew = true;
					}
					//else 
					//{
						//bNew = false;
					//}
					oData.setCustomerId(oCust.getCustomerId());
					oData.setColor(sColor);
					oData.setMargin(getString(_oRow, _iIdx + i));
					oData.setRfeeApplied(Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;
					oData.setPackApplied(Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;
					
					oData.save();
				}
				else
				{
					bValid = false;
					sMsg = "Customer Not Found";
				}
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sCode,30));
						m_oNewData.append (StringUtil.left(sName,50));
						m_oNewData.append (StringUtil.left(sColor,20));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sCode,30));
						m_oUpdated.append (StringUtil.left(sName,50));
						m_oUpdated.append (StringUtil.left(sColor,20));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCode,30));
				    m_oRejected.append (StringUtil.left(sName,50));
				    m_oRejected.append  (StringUtil.left(sColor,20));
				    m_oRejected.append (StringUtil.left(sMsg,50));
				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}
