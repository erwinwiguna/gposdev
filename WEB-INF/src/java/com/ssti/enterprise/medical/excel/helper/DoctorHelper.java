package com.ssti.enterprise.medical.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.medical.om.Doctor;
import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class DoctorHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
    	String[] a_HEADER =
    	{
	    	"Doctor Code",	
	    	"Doctor Name",	
	    	"Specialization",	
	    	"PracticeAddress",	
	    	"HomeAddress",
	    	"Phone",	
	    	"Mobile Phone",
	    	"Internal",
	    	"Location",
	    	"UserName"
    	};

        List vData = DoctorTool.getAllDoctor();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Doctor Worksheet");
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        for (int i = 0; i < a_HEADER.length; i++)
        {
        	createHeaderCell(oWB, oRow, (short)iCol, a_HEADER[i]); iCol++;
        }
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Doctor oData = (Doctor) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol,  oData.getDoctorCode());  iCol++; 
                createCell(oWB, oRow2, (short)iCol,  oData.getDoctorName());  iCol++; 
                createCell(oWB, oRow2, (short)iCol,  oData.getSpecialization());  iCol++;                                
                createCell(oWB, oRow2, (short)iCol,  oData.getPracticeAddress()); iCol++;            
                createCell(oWB, oRow2, (short)iCol,  oData.getHomeAddress()); iCol++;
                createCell(oWB, oRow2, (short)iCol,  oData.getPhone1());	  iCol++;            
                createCell(oWB, oRow2, (short)iCol,  oData.getPhone2());	  iCol++;  
                createCell(oWB, oRow2, (short)iCol,  oData.getIsInternal());  iCol++;  
                createCell(oWB, oRow2, (short)iCol,  oData.getLocationCode());  iCol++;  
                createCell(oWB, oRow2, (short)iCol,  oData.getUserName());  iCol++;                  
                
            }        
        }
        return oWB;
    }
}
