package com.ssti.enterprise.medical.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.medical.om.PowderItem;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.StringUtil;

public class PowderItemLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (PowderItemLoader.class );
	
	public static final String s_START_HEADING = "Item Code";
	
	public PowderItemLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		String sMsg = "";
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCode = getString(_oRow, _iIdx + i); i++;
			String sName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCode))
			{				
				PowderItem oData = PowderTool.getByItemCode(sCode);
				Item oItem = ItemTool.getItemByCode(sCode);
				boolean bNew = true;
				boolean bValid = true;
				
				if (oItem != null)
				{
					log.debug ("Column " + _iIdx + " Item Code : " + sCode);
					if (oData == null) 
					{	
						log.debug ("Code Not Exist, create new Code");
						
						oData = new PowderItem();
						oData.setPowderItemId (IDGenerator.generateSysID());
						oData.setItemId(oItem.getItemId());
						oData.setItemCode(sCode); 	
						bNew = true;
					}
					else 
					{
						bNew = false;
					}
					oData.setItemId(oItem.getItemId());

					oData.setMultiplied(Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;
					oData.setDefaultRfee(getString(_oRow, _iIdx + i)); i++;
					oData.setRfeeMinAmount(getBigDecimal(_oRow, _iIdx + i)); i++;
					oData.setRfeeMaxAmount(getBigDecimal(_oRow, _iIdx + i)); i++;
					
					oData.setDefaultPackaging(getString(_oRow, _iIdx + i)); i++;				
					oData.setPackMinAmount(getBigDecimal(_oRow, _iIdx + i)); i++;
					oData.setPackMaxAmount(getBigDecimal(_oRow, _iIdx + i)); i++;
					
					oData.save();
				}
				else
				{
					bValid = false;
					sMsg = "Item Not Found";
				}
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sCode,30));
						m_oNewData.append (StringUtil.left(sName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sCode,30));
						m_oUpdated.append (StringUtil.left(sName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCode,30));
				    m_oRejected.append (StringUtil.left(sName,50));
				    m_oRejected.append (StringUtil.left(sMsg,50));

				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}
