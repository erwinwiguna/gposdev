package com.ssti.enterprise.medical.model;

import org.apache.torque.om.Persistent;

/**
 * Project, Department, 
 * @author Albert
 *
 */
public interface MedDetailOM extends Persistent
{
	public String getItemId();
	
	public String getRfee();	
	public String getPackaging();
	
	public void setRfee(String s);
	public void setPackaging(String s);
}
