package com.ssti.enterprise.medical.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerMargin
 */
public abstract class BaseCustomerMargin extends BaseObject
{
    /** The Peer class */
    private static final CustomerMarginPeer peer =
        new CustomerMarginPeer();

        
    /** The value for the customerMarginId field */
    private String customerMarginId;
                                                
    /** The value for the customerTypeId field */
    private String customerTypeId = "";
                                                
    /** The value for the customerId field */
    private String customerId = "";
                                                
    /** The value for the categoryId field */
    private String categoryId = "";
                                                
    /** The value for the itemCodes field */
    private String itemCodes = "";
                                                
    /** The value for the locationIds field */
    private String locationIds = "";
                                                
    /** The value for the color field */
    private String color = "";
      
    /** The value for the margin field */
    private String margin;
                                          
    /** The value for the priority field */
    private int priority = 1;
                                          
    /** The value for the transType field */
    private int transType = 0;
                                                                
    /** The value for the rfeeApplied field */
    private boolean rfeeApplied = false;
                                                                
    /** The value for the packApplied field */
    private boolean packApplied = false;
  
    
    /**
     * Get the CustomerMarginId
     *
     * @return String
     */
    public String getCustomerMarginId()
    {
        return customerMarginId;
    }

                        
    /**
     * Set the value of CustomerMarginId
     *
     * @param v new value
     */
    public void setCustomerMarginId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerMarginId, v))
              {
            this.customerMarginId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CategoryId
     *
     * @return String
     */
    public String getCategoryId()
    {
        return categoryId;
    }

                        
    /**
     * Set the value of CategoryId
     *
     * @param v new value
     */
    public void setCategoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.categoryId, v))
              {
            this.categoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCodes
     *
     * @return String
     */
    public String getItemCodes()
    {
        return itemCodes;
    }

                        
    /**
     * Set the value of ItemCodes
     *
     * @param v new value
     */
    public void setItemCodes(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCodes, v))
              {
            this.itemCodes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationIds
     *
     * @return String
     */
    public String getLocationIds()
    {
        return locationIds;
    }

                        
    /**
     * Set the value of LocationIds
     *
     * @param v new value
     */
    public void setLocationIds(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationIds, v))
              {
            this.locationIds = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Color
     *
     * @return String
     */
    public String getColor()
    {
        return color;
    }

                        
    /**
     * Set the value of Color
     *
     * @param v new value
     */
    public void setColor(String v) 
    {
    
                  if (!ObjectUtils.equals(this.color, v))
              {
            this.color = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Margin
     *
     * @return String
     */
    public String getMargin()
    {
        return margin;
    }

                        
    /**
     * Set the value of Margin
     *
     * @param v new value
     */
    public void setMargin(String v) 
    {
    
                  if (!ObjectUtils.equals(this.margin, v))
              {
            this.margin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Priority
     *
     * @return int
     */
    public int getPriority()
    {
        return priority;
    }

                        
    /**
     * Set the value of Priority
     *
     * @param v new value
     */
    public void setPriority(int v) 
    {
    
                  if (this.priority != v)
              {
            this.priority = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransType
     *
     * @return int
     */
    public int getTransType()
    {
        return transType;
    }

                        
    /**
     * Set the value of TransType
     *
     * @param v new value
     */
    public void setTransType(int v) 
    {
    
                  if (this.transType != v)
              {
            this.transType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeApplied
     *
     * @return boolean
     */
    public boolean getRfeeApplied()
    {
        return rfeeApplied;
    }

                        
    /**
     * Set the value of RfeeApplied
     *
     * @param v new value
     */
    public void setRfeeApplied(boolean v) 
    {
    
                  if (this.rfeeApplied != v)
              {
            this.rfeeApplied = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PackApplied
     *
     * @return boolean
     */
    public boolean getPackApplied()
    {
        return packApplied;
    }

                        
    /**
     * Set the value of PackApplied
     *
     * @param v new value
     */
    public void setPackApplied(boolean v) 
    {
    
                  if (this.packApplied != v)
              {
            this.packApplied = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerMarginId");
              fieldNames.add("CustomerTypeId");
              fieldNames.add("CustomerId");
              fieldNames.add("CategoryId");
              fieldNames.add("ItemCodes");
              fieldNames.add("LocationIds");
              fieldNames.add("Color");
              fieldNames.add("Margin");
              fieldNames.add("Priority");
              fieldNames.add("TransType");
              fieldNames.add("RfeeApplied");
              fieldNames.add("PackApplied");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerMarginId"))
        {
                return getCustomerMarginId();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CategoryId"))
        {
                return getCategoryId();
            }
          if (name.equals("ItemCodes"))
        {
                return getItemCodes();
            }
          if (name.equals("LocationIds"))
        {
                return getLocationIds();
            }
          if (name.equals("Color"))
        {
                return getColor();
            }
          if (name.equals("Margin"))
        {
                return getMargin();
            }
          if (name.equals("Priority"))
        {
                return Integer.valueOf(getPriority());
            }
          if (name.equals("TransType"))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals("RfeeApplied"))
        {
                return Boolean.valueOf(getRfeeApplied());
            }
          if (name.equals("PackApplied"))
        {
                return Boolean.valueOf(getPackApplied());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerMarginPeer.CUSTOMER_MARGIN_ID))
        {
                return getCustomerMarginId();
            }
          if (name.equals(CustomerMarginPeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          if (name.equals(CustomerMarginPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerMarginPeer.CATEGORY_ID))
        {
                return getCategoryId();
            }
          if (name.equals(CustomerMarginPeer.ITEM_CODES))
        {
                return getItemCodes();
            }
          if (name.equals(CustomerMarginPeer.LOCATION_IDS))
        {
                return getLocationIds();
            }
          if (name.equals(CustomerMarginPeer.COLOR))
        {
                return getColor();
            }
          if (name.equals(CustomerMarginPeer.MARGIN))
        {
                return getMargin();
            }
          if (name.equals(CustomerMarginPeer.PRIORITY))
        {
                return Integer.valueOf(getPriority());
            }
          if (name.equals(CustomerMarginPeer.TRANS_TYPE))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals(CustomerMarginPeer.RFEE_APPLIED))
        {
                return Boolean.valueOf(getRfeeApplied());
            }
          if (name.equals(CustomerMarginPeer.PACK_APPLIED))
        {
                return Boolean.valueOf(getPackApplied());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerMarginId();
            }
              if (pos == 1)
        {
                return getCustomerTypeId();
            }
              if (pos == 2)
        {
                return getCustomerId();
            }
              if (pos == 3)
        {
                return getCategoryId();
            }
              if (pos == 4)
        {
                return getItemCodes();
            }
              if (pos == 5)
        {
                return getLocationIds();
            }
              if (pos == 6)
        {
                return getColor();
            }
              if (pos == 7)
        {
                return getMargin();
            }
              if (pos == 8)
        {
                return Integer.valueOf(getPriority());
            }
              if (pos == 9)
        {
                return Integer.valueOf(getTransType());
            }
              if (pos == 10)
        {
                return Boolean.valueOf(getRfeeApplied());
            }
              if (pos == 11)
        {
                return Boolean.valueOf(getPackApplied());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerMarginPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerMarginPeer.doInsert((CustomerMargin) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerMarginPeer.doUpdate((CustomerMargin) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerMarginId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerMarginId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerMarginId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerMarginId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerMargin copy() throws TorqueException
    {
        return copyInto(new CustomerMargin());
    }
  
    protected CustomerMargin copyInto(CustomerMargin copyObj) throws TorqueException
    {
          copyObj.setCustomerMarginId(customerMarginId);
          copyObj.setCustomerTypeId(customerTypeId);
          copyObj.setCustomerId(customerId);
          copyObj.setCategoryId(categoryId);
          copyObj.setItemCodes(itemCodes);
          copyObj.setLocationIds(locationIds);
          copyObj.setColor(color);
          copyObj.setMargin(margin);
          copyObj.setPriority(priority);
          copyObj.setTransType(transType);
          copyObj.setRfeeApplied(rfeeApplied);
          copyObj.setPackApplied(packApplied);
  
                    copyObj.setCustomerMarginId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerMarginPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerMargin\n");
        str.append("--------------\n")
           .append("CustomerMarginId     : ")
           .append(getCustomerMarginId())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CategoryId           : ")
           .append(getCategoryId())
           .append("\n")
           .append("ItemCodes            : ")
           .append(getItemCodes())
           .append("\n")
           .append("LocationIds          : ")
           .append(getLocationIds())
           .append("\n")
           .append("Color                : ")
           .append(getColor())
           .append("\n")
           .append("Margin               : ")
           .append(getMargin())
           .append("\n")
           .append("Priority             : ")
           .append(getPriority())
           .append("\n")
           .append("TransType            : ")
           .append(getTransType())
           .append("\n")
           .append("RfeeApplied          : ")
           .append(getRfeeApplied())
           .append("\n")
           .append("PackApplied          : ")
           .append(getPackApplied())
           .append("\n")
        ;
        return(str.toString());
    }
}
