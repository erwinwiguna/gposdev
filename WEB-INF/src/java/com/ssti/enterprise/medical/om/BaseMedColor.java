package com.ssti.enterprise.medical.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to MedColor
 */
public abstract class BaseMedColor extends BaseObject
{
    /** The Peer class */
    private static final MedColorPeer peer =
        new MedColorPeer();

        
    /** The value for the color field */
    private String color;
                                                
    /** The value for the logo field */
    private String logo = "";
                                                                
    /** The value for the isPres field */
    private boolean isPres = false;
                                                                
    /** The value for the isWarn field */
    private boolean isWarn = false;
  
    
    /**
     * Get the Color
     *
     * @return String
     */
    public String getColor()
    {
        return color;
    }

                        
    /**
     * Set the value of Color
     *
     * @param v new value
     */
    public void setColor(String v) 
    {
    
                  if (!ObjectUtils.equals(this.color, v))
              {
            this.color = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Logo
     *
     * @return String
     */
    public String getLogo()
    {
        return logo;
    }

                        
    /**
     * Set the value of Logo
     *
     * @param v new value
     */
    public void setLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.logo, v))
              {
            this.logo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsPres
     *
     * @return boolean
     */
    public boolean getIsPres()
    {
        return isPres;
    }

                        
    /**
     * Set the value of IsPres
     *
     * @param v new value
     */
    public void setIsPres(boolean v) 
    {
    
                  if (this.isPres != v)
              {
            this.isPres = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsWarn
     *
     * @return boolean
     */
    public boolean getIsWarn()
    {
        return isWarn;
    }

                        
    /**
     * Set the value of IsWarn
     *
     * @param v new value
     */
    public void setIsWarn(boolean v) 
    {
    
                  if (this.isWarn != v)
              {
            this.isWarn = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("Color");
              fieldNames.add("Logo");
              fieldNames.add("IsPres");
              fieldNames.add("IsWarn");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("Color"))
        {
                return getColor();
            }
          if (name.equals("Logo"))
        {
                return getLogo();
            }
          if (name.equals("IsPres"))
        {
                return Boolean.valueOf(getIsPres());
            }
          if (name.equals("IsWarn"))
        {
                return Boolean.valueOf(getIsWarn());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(MedColorPeer.COLOR))
        {
                return getColor();
            }
          if (name.equals(MedColorPeer.LOGO))
        {
                return getLogo();
            }
          if (name.equals(MedColorPeer.IS_PRES))
        {
                return Boolean.valueOf(getIsPres());
            }
          if (name.equals(MedColorPeer.IS_WARN))
        {
                return Boolean.valueOf(getIsWarn());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getColor();
            }
              if (pos == 1)
        {
                return getLogo();
            }
              if (pos == 2)
        {
                return Boolean.valueOf(getIsPres());
            }
              if (pos == 3)
        {
                return Boolean.valueOf(getIsWarn());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(MedColorPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        MedColorPeer.doInsert((MedColor) this, con);
                        setNew(false);
                    }
                    else
                    {
                        MedColorPeer.doUpdate((MedColor) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key color ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setColor(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setColor(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getColor());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public MedColor copy() throws TorqueException
    {
        return copyInto(new MedColor());
    }
  
    protected MedColor copyInto(MedColor copyObj) throws TorqueException
    {
          copyObj.setColor(color);
          copyObj.setLogo(logo);
          copyObj.setIsPres(isPres);
          copyObj.setIsWarn(isWarn);
  
                    copyObj.setColor((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public MedColorPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("MedColor\n");
        str.append("--------\n")
           .append("Color                : ")
           .append(getColor())
           .append("\n")
           .append("Logo                 : ")
           .append(getLogo())
           .append("\n")
           .append("IsPres               : ")
           .append(getIsPres())
           .append("\n")
           .append("IsWarn               : ")
           .append(getIsWarn())
           .append("\n")
        ;
        return(str.toString());
    }
}
