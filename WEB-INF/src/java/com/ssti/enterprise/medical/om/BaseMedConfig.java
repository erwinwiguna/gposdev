package com.ssti.enterprise.medical.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to MedConfig
 */
public abstract class BaseMedConfig extends BaseObject
{
    /** The Peer class */
    private static final MedConfigPeer peer =
        new MedConfigPeer();

        
    /** The value for the medConfigId field */
    private String medConfigId;
                                                
    /** The value for the pharmacistName field */
    private String pharmacistName = "";
                                                
    /** The value for the licenseNo field */
    private String licenseNo = "";
                                                
    /** The value for the workLicenseNo field */
    private String workLicenseNo = "";
                                                
    /** The value for the presNoFormat field */
    private String presNoFormat = "";
      
    /** The value for the defaultRfee field */
    private String defaultRfee;
      
    /** The value for the defaultPackaging field */
    private String defaultPackaging;
                                                        
    /** The value for the rfeeEditable field */
    private boolean rfeeEditable = false;
                                                        
    /** The value for the rfeeMultiplied field */
    private boolean rfeeMultiplied = false;
                                                
          
    /** The value for the rfeeMinAmount field */
    private BigDecimal rfeeMinAmount= bd_ZERO;
                                                
          
    /** The value for the rfeeMaxAmount field */
    private BigDecimal rfeeMaxAmount= bd_ZERO;
                                                
          
    /** The value for the packMinAmount field */
    private BigDecimal packMinAmount= bd_ZERO;
                                                
          
    /** The value for the packMaxAmount field */
    private BigDecimal packMaxAmount= bd_ZERO;
      
    /** The value for the posDefaultRfee field */
    private String posDefaultRfee;
      
    /** The value for the posDefaultPack field */
    private String posDefaultPack;
                                                        
    /** The value for the posRfeeEditable field */
    private boolean posRfeeEditable = false;
                                                        
    /** The value for the posRfeeMultiplied field */
    private boolean posRfeeMultiplied = false;
                                                
          
    /** The value for the posRfeeMinAmount field */
    private BigDecimal posRfeeMinAmount= bd_ZERO;
                                                
          
    /** The value for the posRfeeMaxAmount field */
    private BigDecimal posRfeeMaxAmount= bd_ZERO;
                                                
          
    /** The value for the posPackMinAmount field */
    private BigDecimal posPackMinAmount= bd_ZERO;
                                                
          
    /** The value for the posPackMaxAmount field */
    private BigDecimal posPackMaxAmount= bd_ZERO;
      
    /** The value for the rfeeItemCode field */
    private String rfeeItemCode;
      
    /** The value for the packagingItemCode field */
    private String packagingItemCode;
      
    /** The value for the presRootKategori field */
    private String presRootKategori;
      
    /** The value for the presItemsGroup field */
    private String presItemsGroup;
      
    /** The value for the genItemsGroup field */
    private String genItemsGroup;
      
    /** The value for the presItemsMargin field */
    private String presItemsMargin;
      
    /** The value for the genItemsMargin field */
    private String genItemsMargin;
                                                        
    /** The value for the genUsePresMargin field */
    private boolean genUsePresMargin = false;
                                          
    /** The value for the posCommaScale field */
    private int posCommaScale = 0;
                                          
    /** The value for the roundingScale field */
    private int roundingScale = 0;
                                          
    /** The value for the roundingMode field */
    private int roundingMode = 4;
      
    /** The value for the roundingItemCode field */
    private String roundingItemCode;
      
    /** The value for the powderItemCode field */
    private String powderItemCode;
      
    /** The value for the warningItems field */
    private String warningItems;
      
    /** The value for the insuranceCtype field */
    private String insuranceCtype;
      
    /** The value for the insurancePtype field */
    private String insurancePtype;
                                                        
    /** The value for the alertPrescription field */
    private boolean alertPrescription = false;
      
    /** The value for the alertHosts field */
    private String alertHosts;
                                                
    /** The value for the regNoFormat field */
    private String regNoFormat = "";
                                                
    /** The value for the recNoFormat field */
    private String recNoFormat = "";
                                                
    /** The value for the labNoFormat field */
    private String labNoFormat = "";
                                                
    /** The value for the clConfigFile field */
    private String clConfigFile = "";
                                                
    /** The value for the clMenuFile field */
    private String clMenuFile = "";
  
    
    /**
     * Get the MedConfigId
     *
     * @return String
     */
    public String getMedConfigId()
    {
        return medConfigId;
    }

                        
    /**
     * Set the value of MedConfigId
     *
     * @param v new value
     */
    public void setMedConfigId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.medConfigId, v))
              {
            this.medConfigId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PharmacistName
     *
     * @return String
     */
    public String getPharmacistName()
    {
        return pharmacistName;
    }

                        
    /**
     * Set the value of PharmacistName
     *
     * @param v new value
     */
    public void setPharmacistName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pharmacistName, v))
              {
            this.pharmacistName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LicenseNo
     *
     * @return String
     */
    public String getLicenseNo()
    {
        return licenseNo;
    }

                        
    /**
     * Set the value of LicenseNo
     *
     * @param v new value
     */
    public void setLicenseNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.licenseNo, v))
              {
            this.licenseNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the WorkLicenseNo
     *
     * @return String
     */
    public String getWorkLicenseNo()
    {
        return workLicenseNo;
    }

                        
    /**
     * Set the value of WorkLicenseNo
     *
     * @param v new value
     */
    public void setWorkLicenseNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.workLicenseNo, v))
              {
            this.workLicenseNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresNoFormat
     *
     * @return String
     */
    public String getPresNoFormat()
    {
        return presNoFormat;
    }

                        
    /**
     * Set the value of PresNoFormat
     *
     * @param v new value
     */
    public void setPresNoFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presNoFormat, v))
              {
            this.presNoFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultRfee
     *
     * @return String
     */
    public String getDefaultRfee()
    {
        return defaultRfee;
    }

                        
    /**
     * Set the value of DefaultRfee
     *
     * @param v new value
     */
    public void setDefaultRfee(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultRfee, v))
              {
            this.defaultRfee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultPackaging
     *
     * @return String
     */
    public String getDefaultPackaging()
    {
        return defaultPackaging;
    }

                        
    /**
     * Set the value of DefaultPackaging
     *
     * @param v new value
     */
    public void setDefaultPackaging(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultPackaging, v))
              {
            this.defaultPackaging = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeEditable
     *
     * @return boolean
     */
    public boolean getRfeeEditable()
    {
        return rfeeEditable;
    }

                        
    /**
     * Set the value of RfeeEditable
     *
     * @param v new value
     */
    public void setRfeeEditable(boolean v) 
    {
    
                  if (this.rfeeEditable != v)
              {
            this.rfeeEditable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeMultiplied
     *
     * @return boolean
     */
    public boolean getRfeeMultiplied()
    {
        return rfeeMultiplied;
    }

                        
    /**
     * Set the value of RfeeMultiplied
     *
     * @param v new value
     */
    public void setRfeeMultiplied(boolean v) 
    {
    
                  if (this.rfeeMultiplied != v)
              {
            this.rfeeMultiplied = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeMinAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRfeeMinAmount()
    {
        return rfeeMinAmount;
    }

                        
    /**
     * Set the value of RfeeMinAmount
     *
     * @param v new value
     */
    public void setRfeeMinAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.rfeeMinAmount, v))
              {
            this.rfeeMinAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeMaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRfeeMaxAmount()
    {
        return rfeeMaxAmount;
    }

                        
    /**
     * Set the value of RfeeMaxAmount
     *
     * @param v new value
     */
    public void setRfeeMaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.rfeeMaxAmount, v))
              {
            this.rfeeMaxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PackMinAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPackMinAmount()
    {
        return packMinAmount;
    }

                        
    /**
     * Set the value of PackMinAmount
     *
     * @param v new value
     */
    public void setPackMinAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.packMinAmount, v))
              {
            this.packMinAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PackMaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPackMaxAmount()
    {
        return packMaxAmount;
    }

                        
    /**
     * Set the value of PackMaxAmount
     *
     * @param v new value
     */
    public void setPackMaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.packMaxAmount, v))
              {
            this.packMaxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultRfee
     *
     * @return String
     */
    public String getPosDefaultRfee()
    {
        return posDefaultRfee;
    }

                        
    /**
     * Set the value of PosDefaultRfee
     *
     * @param v new value
     */
    public void setPosDefaultRfee(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultRfee, v))
              {
            this.posDefaultRfee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultPack
     *
     * @return String
     */
    public String getPosDefaultPack()
    {
        return posDefaultPack;
    }

                        
    /**
     * Set the value of PosDefaultPack
     *
     * @param v new value
     */
    public void setPosDefaultPack(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultPack, v))
              {
            this.posDefaultPack = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRfeeEditable
     *
     * @return boolean
     */
    public boolean getPosRfeeEditable()
    {
        return posRfeeEditable;
    }

                        
    /**
     * Set the value of PosRfeeEditable
     *
     * @param v new value
     */
    public void setPosRfeeEditable(boolean v) 
    {
    
                  if (this.posRfeeEditable != v)
              {
            this.posRfeeEditable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRfeeMultiplied
     *
     * @return boolean
     */
    public boolean getPosRfeeMultiplied()
    {
        return posRfeeMultiplied;
    }

                        
    /**
     * Set the value of PosRfeeMultiplied
     *
     * @param v new value
     */
    public void setPosRfeeMultiplied(boolean v) 
    {
    
                  if (this.posRfeeMultiplied != v)
              {
            this.posRfeeMultiplied = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRfeeMinAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPosRfeeMinAmount()
    {
        return posRfeeMinAmount;
    }

                        
    /**
     * Set the value of PosRfeeMinAmount
     *
     * @param v new value
     */
    public void setPosRfeeMinAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.posRfeeMinAmount, v))
              {
            this.posRfeeMinAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRfeeMaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPosRfeeMaxAmount()
    {
        return posRfeeMaxAmount;
    }

                        
    /**
     * Set the value of PosRfeeMaxAmount
     *
     * @param v new value
     */
    public void setPosRfeeMaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.posRfeeMaxAmount, v))
              {
            this.posRfeeMaxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosPackMinAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPosPackMinAmount()
    {
        return posPackMinAmount;
    }

                        
    /**
     * Set the value of PosPackMinAmount
     *
     * @param v new value
     */
    public void setPosPackMinAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.posPackMinAmount, v))
              {
            this.posPackMinAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosPackMaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPosPackMaxAmount()
    {
        return posPackMaxAmount;
    }

                        
    /**
     * Set the value of PosPackMaxAmount
     *
     * @param v new value
     */
    public void setPosPackMaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.posPackMaxAmount, v))
              {
            this.posPackMaxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeItemCode
     *
     * @return String
     */
    public String getRfeeItemCode()
    {
        return rfeeItemCode;
    }

                        
    /**
     * Set the value of RfeeItemCode
     *
     * @param v new value
     */
    public void setRfeeItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.rfeeItemCode, v))
              {
            this.rfeeItemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PackagingItemCode
     *
     * @return String
     */
    public String getPackagingItemCode()
    {
        return packagingItemCode;
    }

                        
    /**
     * Set the value of PackagingItemCode
     *
     * @param v new value
     */
    public void setPackagingItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.packagingItemCode, v))
              {
            this.packagingItemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresRootKategori
     *
     * @return String
     */
    public String getPresRootKategori()
    {
        return presRootKategori;
    }

                        
    /**
     * Set the value of PresRootKategori
     *
     * @param v new value
     */
    public void setPresRootKategori(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presRootKategori, v))
              {
            this.presRootKategori = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresItemsGroup
     *
     * @return String
     */
    public String getPresItemsGroup()
    {
        return presItemsGroup;
    }

                        
    /**
     * Set the value of PresItemsGroup
     *
     * @param v new value
     */
    public void setPresItemsGroup(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presItemsGroup, v))
              {
            this.presItemsGroup = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GenItemsGroup
     *
     * @return String
     */
    public String getGenItemsGroup()
    {
        return genItemsGroup;
    }

                        
    /**
     * Set the value of GenItemsGroup
     *
     * @param v new value
     */
    public void setGenItemsGroup(String v) 
    {
    
                  if (!ObjectUtils.equals(this.genItemsGroup, v))
              {
            this.genItemsGroup = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresItemsMargin
     *
     * @return String
     */
    public String getPresItemsMargin()
    {
        return presItemsMargin;
    }

                        
    /**
     * Set the value of PresItemsMargin
     *
     * @param v new value
     */
    public void setPresItemsMargin(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presItemsMargin, v))
              {
            this.presItemsMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GenItemsMargin
     *
     * @return String
     */
    public String getGenItemsMargin()
    {
        return genItemsMargin;
    }

                        
    /**
     * Set the value of GenItemsMargin
     *
     * @param v new value
     */
    public void setGenItemsMargin(String v) 
    {
    
                  if (!ObjectUtils.equals(this.genItemsMargin, v))
              {
            this.genItemsMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GenUsePresMargin
     *
     * @return boolean
     */
    public boolean getGenUsePresMargin()
    {
        return genUsePresMargin;
    }

                        
    /**
     * Set the value of GenUsePresMargin
     *
     * @param v new value
     */
    public void setGenUsePresMargin(boolean v) 
    {
    
                  if (this.genUsePresMargin != v)
              {
            this.genUsePresMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosCommaScale
     *
     * @return int
     */
    public int getPosCommaScale()
    {
        return posCommaScale;
    }

                        
    /**
     * Set the value of PosCommaScale
     *
     * @param v new value
     */
    public void setPosCommaScale(int v) 
    {
    
                  if (this.posCommaScale != v)
              {
            this.posCommaScale = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoundingScale
     *
     * @return int
     */
    public int getRoundingScale()
    {
        return roundingScale;
    }

                        
    /**
     * Set the value of RoundingScale
     *
     * @param v new value
     */
    public void setRoundingScale(int v) 
    {
    
                  if (this.roundingScale != v)
              {
            this.roundingScale = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoundingMode
     *
     * @return int
     */
    public int getRoundingMode()
    {
        return roundingMode;
    }

                        
    /**
     * Set the value of RoundingMode
     *
     * @param v new value
     */
    public void setRoundingMode(int v) 
    {
    
                  if (this.roundingMode != v)
              {
            this.roundingMode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoundingItemCode
     *
     * @return String
     */
    public String getRoundingItemCode()
    {
        return roundingItemCode;
    }

                        
    /**
     * Set the value of RoundingItemCode
     *
     * @param v new value
     */
    public void setRoundingItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.roundingItemCode, v))
              {
            this.roundingItemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PowderItemCode
     *
     * @return String
     */
    public String getPowderItemCode()
    {
        return powderItemCode;
    }

                        
    /**
     * Set the value of PowderItemCode
     *
     * @param v new value
     */
    public void setPowderItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.powderItemCode, v))
              {
            this.powderItemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the WarningItems
     *
     * @return String
     */
    public String getWarningItems()
    {
        return warningItems;
    }

                        
    /**
     * Set the value of WarningItems
     *
     * @param v new value
     */
    public void setWarningItems(String v) 
    {
    
                  if (!ObjectUtils.equals(this.warningItems, v))
              {
            this.warningItems = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InsuranceCtype
     *
     * @return String
     */
    public String getInsuranceCtype()
    {
        return insuranceCtype;
    }

                        
    /**
     * Set the value of InsuranceCtype
     *
     * @param v new value
     */
    public void setInsuranceCtype(String v) 
    {
    
                  if (!ObjectUtils.equals(this.insuranceCtype, v))
              {
            this.insuranceCtype = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InsurancePtype
     *
     * @return String
     */
    public String getInsurancePtype()
    {
        return insurancePtype;
    }

                        
    /**
     * Set the value of InsurancePtype
     *
     * @param v new value
     */
    public void setInsurancePtype(String v) 
    {
    
                  if (!ObjectUtils.equals(this.insurancePtype, v))
              {
            this.insurancePtype = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AlertPrescription
     *
     * @return boolean
     */
    public boolean getAlertPrescription()
    {
        return alertPrescription;
    }

                        
    /**
     * Set the value of AlertPrescription
     *
     * @param v new value
     */
    public void setAlertPrescription(boolean v) 
    {
    
                  if (this.alertPrescription != v)
              {
            this.alertPrescription = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AlertHosts
     *
     * @return String
     */
    public String getAlertHosts()
    {
        return alertHosts;
    }

                        
    /**
     * Set the value of AlertHosts
     *
     * @param v new value
     */
    public void setAlertHosts(String v) 
    {
    
                  if (!ObjectUtils.equals(this.alertHosts, v))
              {
            this.alertHosts = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RegNoFormat
     *
     * @return String
     */
    public String getRegNoFormat()
    {
        return regNoFormat;
    }

                        
    /**
     * Set the value of RegNoFormat
     *
     * @param v new value
     */
    public void setRegNoFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.regNoFormat, v))
              {
            this.regNoFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RecNoFormat
     *
     * @return String
     */
    public String getRecNoFormat()
    {
        return recNoFormat;
    }

                        
    /**
     * Set the value of RecNoFormat
     *
     * @param v new value
     */
    public void setRecNoFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.recNoFormat, v))
              {
            this.recNoFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LabNoFormat
     *
     * @return String
     */
    public String getLabNoFormat()
    {
        return labNoFormat;
    }

                        
    /**
     * Set the value of LabNoFormat
     *
     * @param v new value
     */
    public void setLabNoFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.labNoFormat, v))
              {
            this.labNoFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClConfigFile
     *
     * @return String
     */
    public String getClConfigFile()
    {
        return clConfigFile;
    }

                        
    /**
     * Set the value of ClConfigFile
     *
     * @param v new value
     */
    public void setClConfigFile(String v) 
    {
    
                  if (!ObjectUtils.equals(this.clConfigFile, v))
              {
            this.clConfigFile = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClMenuFile
     *
     * @return String
     */
    public String getClMenuFile()
    {
        return clMenuFile;
    }

                        
    /**
     * Set the value of ClMenuFile
     *
     * @param v new value
     */
    public void setClMenuFile(String v) 
    {
    
                  if (!ObjectUtils.equals(this.clMenuFile, v))
              {
            this.clMenuFile = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("MedConfigId");
              fieldNames.add("PharmacistName");
              fieldNames.add("LicenseNo");
              fieldNames.add("WorkLicenseNo");
              fieldNames.add("PresNoFormat");
              fieldNames.add("DefaultRfee");
              fieldNames.add("DefaultPackaging");
              fieldNames.add("RfeeEditable");
              fieldNames.add("RfeeMultiplied");
              fieldNames.add("RfeeMinAmount");
              fieldNames.add("RfeeMaxAmount");
              fieldNames.add("PackMinAmount");
              fieldNames.add("PackMaxAmount");
              fieldNames.add("PosDefaultRfee");
              fieldNames.add("PosDefaultPack");
              fieldNames.add("PosRfeeEditable");
              fieldNames.add("PosRfeeMultiplied");
              fieldNames.add("PosRfeeMinAmount");
              fieldNames.add("PosRfeeMaxAmount");
              fieldNames.add("PosPackMinAmount");
              fieldNames.add("PosPackMaxAmount");
              fieldNames.add("RfeeItemCode");
              fieldNames.add("PackagingItemCode");
              fieldNames.add("PresRootKategori");
              fieldNames.add("PresItemsGroup");
              fieldNames.add("GenItemsGroup");
              fieldNames.add("PresItemsMargin");
              fieldNames.add("GenItemsMargin");
              fieldNames.add("GenUsePresMargin");
              fieldNames.add("PosCommaScale");
              fieldNames.add("RoundingScale");
              fieldNames.add("RoundingMode");
              fieldNames.add("RoundingItemCode");
              fieldNames.add("PowderItemCode");
              fieldNames.add("WarningItems");
              fieldNames.add("InsuranceCtype");
              fieldNames.add("InsurancePtype");
              fieldNames.add("AlertPrescription");
              fieldNames.add("AlertHosts");
              fieldNames.add("RegNoFormat");
              fieldNames.add("RecNoFormat");
              fieldNames.add("LabNoFormat");
              fieldNames.add("ClConfigFile");
              fieldNames.add("ClMenuFile");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("MedConfigId"))
        {
                return getMedConfigId();
            }
          if (name.equals("PharmacistName"))
        {
                return getPharmacistName();
            }
          if (name.equals("LicenseNo"))
        {
                return getLicenseNo();
            }
          if (name.equals("WorkLicenseNo"))
        {
                return getWorkLicenseNo();
            }
          if (name.equals("PresNoFormat"))
        {
                return getPresNoFormat();
            }
          if (name.equals("DefaultRfee"))
        {
                return getDefaultRfee();
            }
          if (name.equals("DefaultPackaging"))
        {
                return getDefaultPackaging();
            }
          if (name.equals("RfeeEditable"))
        {
                return Boolean.valueOf(getRfeeEditable());
            }
          if (name.equals("RfeeMultiplied"))
        {
                return Boolean.valueOf(getRfeeMultiplied());
            }
          if (name.equals("RfeeMinAmount"))
        {
                return getRfeeMinAmount();
            }
          if (name.equals("RfeeMaxAmount"))
        {
                return getRfeeMaxAmount();
            }
          if (name.equals("PackMinAmount"))
        {
                return getPackMinAmount();
            }
          if (name.equals("PackMaxAmount"))
        {
                return getPackMaxAmount();
            }
          if (name.equals("PosDefaultRfee"))
        {
                return getPosDefaultRfee();
            }
          if (name.equals("PosDefaultPack"))
        {
                return getPosDefaultPack();
            }
          if (name.equals("PosRfeeEditable"))
        {
                return Boolean.valueOf(getPosRfeeEditable());
            }
          if (name.equals("PosRfeeMultiplied"))
        {
                return Boolean.valueOf(getPosRfeeMultiplied());
            }
          if (name.equals("PosRfeeMinAmount"))
        {
                return getPosRfeeMinAmount();
            }
          if (name.equals("PosRfeeMaxAmount"))
        {
                return getPosRfeeMaxAmount();
            }
          if (name.equals("PosPackMinAmount"))
        {
                return getPosPackMinAmount();
            }
          if (name.equals("PosPackMaxAmount"))
        {
                return getPosPackMaxAmount();
            }
          if (name.equals("RfeeItemCode"))
        {
                return getRfeeItemCode();
            }
          if (name.equals("PackagingItemCode"))
        {
                return getPackagingItemCode();
            }
          if (name.equals("PresRootKategori"))
        {
                return getPresRootKategori();
            }
          if (name.equals("PresItemsGroup"))
        {
                return getPresItemsGroup();
            }
          if (name.equals("GenItemsGroup"))
        {
                return getGenItemsGroup();
            }
          if (name.equals("PresItemsMargin"))
        {
                return getPresItemsMargin();
            }
          if (name.equals("GenItemsMargin"))
        {
                return getGenItemsMargin();
            }
          if (name.equals("GenUsePresMargin"))
        {
                return Boolean.valueOf(getGenUsePresMargin());
            }
          if (name.equals("PosCommaScale"))
        {
                return Integer.valueOf(getPosCommaScale());
            }
          if (name.equals("RoundingScale"))
        {
                return Integer.valueOf(getRoundingScale());
            }
          if (name.equals("RoundingMode"))
        {
                return Integer.valueOf(getRoundingMode());
            }
          if (name.equals("RoundingItemCode"))
        {
                return getRoundingItemCode();
            }
          if (name.equals("PowderItemCode"))
        {
                return getPowderItemCode();
            }
          if (name.equals("WarningItems"))
        {
                return getWarningItems();
            }
          if (name.equals("InsuranceCtype"))
        {
                return getInsuranceCtype();
            }
          if (name.equals("InsurancePtype"))
        {
                return getInsurancePtype();
            }
          if (name.equals("AlertPrescription"))
        {
                return Boolean.valueOf(getAlertPrescription());
            }
          if (name.equals("AlertHosts"))
        {
                return getAlertHosts();
            }
          if (name.equals("RegNoFormat"))
        {
                return getRegNoFormat();
            }
          if (name.equals("RecNoFormat"))
        {
                return getRecNoFormat();
            }
          if (name.equals("LabNoFormat"))
        {
                return getLabNoFormat();
            }
          if (name.equals("ClConfigFile"))
        {
                return getClConfigFile();
            }
          if (name.equals("ClMenuFile"))
        {
                return getClMenuFile();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(MedConfigPeer.MED_CONFIG_ID))
        {
                return getMedConfigId();
            }
          if (name.equals(MedConfigPeer.PHARMACIST_NAME))
        {
                return getPharmacistName();
            }
          if (name.equals(MedConfigPeer.LICENSE_NO))
        {
                return getLicenseNo();
            }
          if (name.equals(MedConfigPeer.WORK_LICENSE_NO))
        {
                return getWorkLicenseNo();
            }
          if (name.equals(MedConfigPeer.PRES_NO_FORMAT))
        {
                return getPresNoFormat();
            }
          if (name.equals(MedConfigPeer.DEFAULT_RFEE))
        {
                return getDefaultRfee();
            }
          if (name.equals(MedConfigPeer.DEFAULT_PACKAGING))
        {
                return getDefaultPackaging();
            }
          if (name.equals(MedConfigPeer.RFEE_EDITABLE))
        {
                return Boolean.valueOf(getRfeeEditable());
            }
          if (name.equals(MedConfigPeer.RFEE_MULTIPLIED))
        {
                return Boolean.valueOf(getRfeeMultiplied());
            }
          if (name.equals(MedConfigPeer.RFEE_MIN_AMOUNT))
        {
                return getRfeeMinAmount();
            }
          if (name.equals(MedConfigPeer.RFEE_MAX_AMOUNT))
        {
                return getRfeeMaxAmount();
            }
          if (name.equals(MedConfigPeer.PACK_MIN_AMOUNT))
        {
                return getPackMinAmount();
            }
          if (name.equals(MedConfigPeer.PACK_MAX_AMOUNT))
        {
                return getPackMaxAmount();
            }
          if (name.equals(MedConfigPeer.POS_DEFAULT_RFEE))
        {
                return getPosDefaultRfee();
            }
          if (name.equals(MedConfigPeer.POS_DEFAULT_PACK))
        {
                return getPosDefaultPack();
            }
          if (name.equals(MedConfigPeer.POS_RFEE_EDITABLE))
        {
                return Boolean.valueOf(getPosRfeeEditable());
            }
          if (name.equals(MedConfigPeer.POS_RFEE_MULTIPLIED))
        {
                return Boolean.valueOf(getPosRfeeMultiplied());
            }
          if (name.equals(MedConfigPeer.POS_RFEE_MIN_AMOUNT))
        {
                return getPosRfeeMinAmount();
            }
          if (name.equals(MedConfigPeer.POS_RFEE_MAX_AMOUNT))
        {
                return getPosRfeeMaxAmount();
            }
          if (name.equals(MedConfigPeer.POS_PACK_MIN_AMOUNT))
        {
                return getPosPackMinAmount();
            }
          if (name.equals(MedConfigPeer.POS_PACK_MAX_AMOUNT))
        {
                return getPosPackMaxAmount();
            }
          if (name.equals(MedConfigPeer.RFEE_ITEM_CODE))
        {
                return getRfeeItemCode();
            }
          if (name.equals(MedConfigPeer.PACKAGING_ITEM_CODE))
        {
                return getPackagingItemCode();
            }
          if (name.equals(MedConfigPeer.PRES_ROOT_KATEGORI))
        {
                return getPresRootKategori();
            }
          if (name.equals(MedConfigPeer.PRES_ITEMS_GROUP))
        {
                return getPresItemsGroup();
            }
          if (name.equals(MedConfigPeer.GEN_ITEMS_GROUP))
        {
                return getGenItemsGroup();
            }
          if (name.equals(MedConfigPeer.PRES_ITEMS_MARGIN))
        {
                return getPresItemsMargin();
            }
          if (name.equals(MedConfigPeer.GEN_ITEMS_MARGIN))
        {
                return getGenItemsMargin();
            }
          if (name.equals(MedConfigPeer.GEN_USE_PRES_MARGIN))
        {
                return Boolean.valueOf(getGenUsePresMargin());
            }
          if (name.equals(MedConfigPeer.POS_COMMA_SCALE))
        {
                return Integer.valueOf(getPosCommaScale());
            }
          if (name.equals(MedConfigPeer.ROUNDING_SCALE))
        {
                return Integer.valueOf(getRoundingScale());
            }
          if (name.equals(MedConfigPeer.ROUNDING_MODE))
        {
                return Integer.valueOf(getRoundingMode());
            }
          if (name.equals(MedConfigPeer.ROUNDING_ITEM_CODE))
        {
                return getRoundingItemCode();
            }
          if (name.equals(MedConfigPeer.POWDER_ITEM_CODE))
        {
                return getPowderItemCode();
            }
          if (name.equals(MedConfigPeer.WARNING_ITEMS))
        {
                return getWarningItems();
            }
          if (name.equals(MedConfigPeer.INSURANCE_CTYPE))
        {
                return getInsuranceCtype();
            }
          if (name.equals(MedConfigPeer.INSURANCE_PTYPE))
        {
                return getInsurancePtype();
            }
          if (name.equals(MedConfigPeer.ALERT_PRESCRIPTION))
        {
                return Boolean.valueOf(getAlertPrescription());
            }
          if (name.equals(MedConfigPeer.ALERT_HOSTS))
        {
                return getAlertHosts();
            }
          if (name.equals(MedConfigPeer.REG_NO_FORMAT))
        {
                return getRegNoFormat();
            }
          if (name.equals(MedConfigPeer.REC_NO_FORMAT))
        {
                return getRecNoFormat();
            }
          if (name.equals(MedConfigPeer.LAB_NO_FORMAT))
        {
                return getLabNoFormat();
            }
          if (name.equals(MedConfigPeer.CL_CONFIG_FILE))
        {
                return getClConfigFile();
            }
          if (name.equals(MedConfigPeer.CL_MENU_FILE))
        {
                return getClMenuFile();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getMedConfigId();
            }
              if (pos == 1)
        {
                return getPharmacistName();
            }
              if (pos == 2)
        {
                return getLicenseNo();
            }
              if (pos == 3)
        {
                return getWorkLicenseNo();
            }
              if (pos == 4)
        {
                return getPresNoFormat();
            }
              if (pos == 5)
        {
                return getDefaultRfee();
            }
              if (pos == 6)
        {
                return getDefaultPackaging();
            }
              if (pos == 7)
        {
                return Boolean.valueOf(getRfeeEditable());
            }
              if (pos == 8)
        {
                return Boolean.valueOf(getRfeeMultiplied());
            }
              if (pos == 9)
        {
                return getRfeeMinAmount();
            }
              if (pos == 10)
        {
                return getRfeeMaxAmount();
            }
              if (pos == 11)
        {
                return getPackMinAmount();
            }
              if (pos == 12)
        {
                return getPackMaxAmount();
            }
              if (pos == 13)
        {
                return getPosDefaultRfee();
            }
              if (pos == 14)
        {
                return getPosDefaultPack();
            }
              if (pos == 15)
        {
                return Boolean.valueOf(getPosRfeeEditable());
            }
              if (pos == 16)
        {
                return Boolean.valueOf(getPosRfeeMultiplied());
            }
              if (pos == 17)
        {
                return getPosRfeeMinAmount();
            }
              if (pos == 18)
        {
                return getPosRfeeMaxAmount();
            }
              if (pos == 19)
        {
                return getPosPackMinAmount();
            }
              if (pos == 20)
        {
                return getPosPackMaxAmount();
            }
              if (pos == 21)
        {
                return getRfeeItemCode();
            }
              if (pos == 22)
        {
                return getPackagingItemCode();
            }
              if (pos == 23)
        {
                return getPresRootKategori();
            }
              if (pos == 24)
        {
                return getPresItemsGroup();
            }
              if (pos == 25)
        {
                return getGenItemsGroup();
            }
              if (pos == 26)
        {
                return getPresItemsMargin();
            }
              if (pos == 27)
        {
                return getGenItemsMargin();
            }
              if (pos == 28)
        {
                return Boolean.valueOf(getGenUsePresMargin());
            }
              if (pos == 29)
        {
                return Integer.valueOf(getPosCommaScale());
            }
              if (pos == 30)
        {
                return Integer.valueOf(getRoundingScale());
            }
              if (pos == 31)
        {
                return Integer.valueOf(getRoundingMode());
            }
              if (pos == 32)
        {
                return getRoundingItemCode();
            }
              if (pos == 33)
        {
                return getPowderItemCode();
            }
              if (pos == 34)
        {
                return getWarningItems();
            }
              if (pos == 35)
        {
                return getInsuranceCtype();
            }
              if (pos == 36)
        {
                return getInsurancePtype();
            }
              if (pos == 37)
        {
                return Boolean.valueOf(getAlertPrescription());
            }
              if (pos == 38)
        {
                return getAlertHosts();
            }
              if (pos == 39)
        {
                return getRegNoFormat();
            }
              if (pos == 40)
        {
                return getRecNoFormat();
            }
              if (pos == 41)
        {
                return getLabNoFormat();
            }
              if (pos == 42)
        {
                return getClConfigFile();
            }
              if (pos == 43)
        {
                return getClMenuFile();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(MedConfigPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        MedConfigPeer.doInsert((MedConfig) this, con);
                        setNew(false);
                    }
                    else
                    {
                        MedConfigPeer.doUpdate((MedConfig) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key medConfigId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setMedConfigId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setMedConfigId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getMedConfigId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public MedConfig copy() throws TorqueException
    {
        return copyInto(new MedConfig());
    }
  
    protected MedConfig copyInto(MedConfig copyObj) throws TorqueException
    {
          copyObj.setMedConfigId(medConfigId);
          copyObj.setPharmacistName(pharmacistName);
          copyObj.setLicenseNo(licenseNo);
          copyObj.setWorkLicenseNo(workLicenseNo);
          copyObj.setPresNoFormat(presNoFormat);
          copyObj.setDefaultRfee(defaultRfee);
          copyObj.setDefaultPackaging(defaultPackaging);
          copyObj.setRfeeEditable(rfeeEditable);
          copyObj.setRfeeMultiplied(rfeeMultiplied);
          copyObj.setRfeeMinAmount(rfeeMinAmount);
          copyObj.setRfeeMaxAmount(rfeeMaxAmount);
          copyObj.setPackMinAmount(packMinAmount);
          copyObj.setPackMaxAmount(packMaxAmount);
          copyObj.setPosDefaultRfee(posDefaultRfee);
          copyObj.setPosDefaultPack(posDefaultPack);
          copyObj.setPosRfeeEditable(posRfeeEditable);
          copyObj.setPosRfeeMultiplied(posRfeeMultiplied);
          copyObj.setPosRfeeMinAmount(posRfeeMinAmount);
          copyObj.setPosRfeeMaxAmount(posRfeeMaxAmount);
          copyObj.setPosPackMinAmount(posPackMinAmount);
          copyObj.setPosPackMaxAmount(posPackMaxAmount);
          copyObj.setRfeeItemCode(rfeeItemCode);
          copyObj.setPackagingItemCode(packagingItemCode);
          copyObj.setPresRootKategori(presRootKategori);
          copyObj.setPresItemsGroup(presItemsGroup);
          copyObj.setGenItemsGroup(genItemsGroup);
          copyObj.setPresItemsMargin(presItemsMargin);
          copyObj.setGenItemsMargin(genItemsMargin);
          copyObj.setGenUsePresMargin(genUsePresMargin);
          copyObj.setPosCommaScale(posCommaScale);
          copyObj.setRoundingScale(roundingScale);
          copyObj.setRoundingMode(roundingMode);
          copyObj.setRoundingItemCode(roundingItemCode);
          copyObj.setPowderItemCode(powderItemCode);
          copyObj.setWarningItems(warningItems);
          copyObj.setInsuranceCtype(insuranceCtype);
          copyObj.setInsurancePtype(insurancePtype);
          copyObj.setAlertPrescription(alertPrescription);
          copyObj.setAlertHosts(alertHosts);
          copyObj.setRegNoFormat(regNoFormat);
          copyObj.setRecNoFormat(recNoFormat);
          copyObj.setLabNoFormat(labNoFormat);
          copyObj.setClConfigFile(clConfigFile);
          copyObj.setClMenuFile(clMenuFile);
  
                    copyObj.setMedConfigId((String)null);
                                                                                                                                                                                                                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public MedConfigPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("MedConfig\n");
        str.append("---------\n")
           .append("MedConfigId          : ")
           .append(getMedConfigId())
           .append("\n")
           .append("PharmacistName       : ")
           .append(getPharmacistName())
           .append("\n")
           .append("LicenseNo            : ")
           .append(getLicenseNo())
           .append("\n")
           .append("WorkLicenseNo        : ")
           .append(getWorkLicenseNo())
           .append("\n")
           .append("PresNoFormat         : ")
           .append(getPresNoFormat())
           .append("\n")
           .append("DefaultRfee          : ")
           .append(getDefaultRfee())
           .append("\n")
           .append("DefaultPackaging     : ")
           .append(getDefaultPackaging())
           .append("\n")
           .append("RfeeEditable         : ")
           .append(getRfeeEditable())
           .append("\n")
           .append("RfeeMultiplied       : ")
           .append(getRfeeMultiplied())
           .append("\n")
           .append("RfeeMinAmount        : ")
           .append(getRfeeMinAmount())
           .append("\n")
           .append("RfeeMaxAmount        : ")
           .append(getRfeeMaxAmount())
           .append("\n")
           .append("PackMinAmount        : ")
           .append(getPackMinAmount())
           .append("\n")
           .append("PackMaxAmount        : ")
           .append(getPackMaxAmount())
           .append("\n")
           .append("PosDefaultRfee       : ")
           .append(getPosDefaultRfee())
           .append("\n")
           .append("PosDefaultPack       : ")
           .append(getPosDefaultPack())
           .append("\n")
           .append("PosRfeeEditable      : ")
           .append(getPosRfeeEditable())
           .append("\n")
           .append("PosRfeeMultiplied    : ")
           .append(getPosRfeeMultiplied())
           .append("\n")
           .append("PosRfeeMinAmount     : ")
           .append(getPosRfeeMinAmount())
           .append("\n")
           .append("PosRfeeMaxAmount     : ")
           .append(getPosRfeeMaxAmount())
           .append("\n")
           .append("PosPackMinAmount     : ")
           .append(getPosPackMinAmount())
           .append("\n")
           .append("PosPackMaxAmount     : ")
           .append(getPosPackMaxAmount())
           .append("\n")
           .append("RfeeItemCode         : ")
           .append(getRfeeItemCode())
           .append("\n")
           .append("PackagingItemCode    : ")
           .append(getPackagingItemCode())
           .append("\n")
           .append("PresRootKategori     : ")
           .append(getPresRootKategori())
           .append("\n")
           .append("PresItemsGroup       : ")
           .append(getPresItemsGroup())
           .append("\n")
           .append("GenItemsGroup        : ")
           .append(getGenItemsGroup())
           .append("\n")
           .append("PresItemsMargin      : ")
           .append(getPresItemsMargin())
           .append("\n")
           .append("GenItemsMargin       : ")
           .append(getGenItemsMargin())
           .append("\n")
           .append("GenUsePresMargin     : ")
           .append(getGenUsePresMargin())
           .append("\n")
           .append("PosCommaScale        : ")
           .append(getPosCommaScale())
           .append("\n")
           .append("RoundingScale        : ")
           .append(getRoundingScale())
           .append("\n")
           .append("RoundingMode         : ")
           .append(getRoundingMode())
           .append("\n")
           .append("RoundingItemCode     : ")
           .append(getRoundingItemCode())
           .append("\n")
           .append("PowderItemCode       : ")
           .append(getPowderItemCode())
           .append("\n")
           .append("WarningItems         : ")
           .append(getWarningItems())
           .append("\n")
           .append("InsuranceCtype       : ")
           .append(getInsuranceCtype())
           .append("\n")
           .append("InsurancePtype       : ")
           .append(getInsurancePtype())
           .append("\n")
           .append("AlertPrescription    : ")
           .append(getAlertPrescription())
           .append("\n")
           .append("AlertHosts           : ")
           .append(getAlertHosts())
           .append("\n")
           .append("RegNoFormat          : ")
           .append(getRegNoFormat())
           .append("\n")
           .append("RecNoFormat          : ")
           .append(getRecNoFormat())
           .append("\n")
           .append("LabNoFormat          : ")
           .append(getLabNoFormat())
           .append("\n")
           .append("ClConfigFile         : ")
           .append(getClConfigFile())
           .append("\n")
           .append("ClMenuFile           : ")
           .append(getClMenuFile())
           .append("\n")
        ;
        return(str.toString());
    }
}
