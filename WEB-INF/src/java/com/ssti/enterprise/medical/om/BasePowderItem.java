package com.ssti.enterprise.medical.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PowderItem
 */
public abstract class BasePowderItem extends BaseObject
{
    /** The Peer class */
    private static final PowderItemPeer peer =
        new PowderItemPeer();

        
    /** The value for the powderItemId field */
    private String powderItemId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the defaultRfee field */
    private String defaultRfee;
      
    /** The value for the defaultPackaging field */
    private String defaultPackaging;
                                                        
    /** The value for the multiplied field */
    private boolean multiplied = false;
                                                
          
    /** The value for the rfeeMinAmount field */
    private BigDecimal rfeeMinAmount= bd_ZERO;
                                                
          
    /** The value for the rfeeMaxAmount field */
    private BigDecimal rfeeMaxAmount= bd_ZERO;
                                                
          
    /** The value for the packMinAmount field */
    private BigDecimal packMinAmount= bd_ZERO;
                                                
          
    /** The value for the packMaxAmount field */
    private BigDecimal packMaxAmount= bd_ZERO;
  
    
    /**
     * Get the PowderItemId
     *
     * @return String
     */
    public String getPowderItemId()
    {
        return powderItemId;
    }

                        
    /**
     * Set the value of PowderItemId
     *
     * @param v new value
     */
    public void setPowderItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.powderItemId, v))
              {
            this.powderItemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultRfee
     *
     * @return String
     */
    public String getDefaultRfee()
    {
        return defaultRfee;
    }

                        
    /**
     * Set the value of DefaultRfee
     *
     * @param v new value
     */
    public void setDefaultRfee(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultRfee, v))
              {
            this.defaultRfee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultPackaging
     *
     * @return String
     */
    public String getDefaultPackaging()
    {
        return defaultPackaging;
    }

                        
    /**
     * Set the value of DefaultPackaging
     *
     * @param v new value
     */
    public void setDefaultPackaging(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultPackaging, v))
              {
            this.defaultPackaging = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Multiplied
     *
     * @return boolean
     */
    public boolean getMultiplied()
    {
        return multiplied;
    }

                        
    /**
     * Set the value of Multiplied
     *
     * @param v new value
     */
    public void setMultiplied(boolean v) 
    {
    
                  if (this.multiplied != v)
              {
            this.multiplied = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeMinAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRfeeMinAmount()
    {
        return rfeeMinAmount;
    }

                        
    /**
     * Set the value of RfeeMinAmount
     *
     * @param v new value
     */
    public void setRfeeMinAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.rfeeMinAmount, v))
              {
            this.rfeeMinAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RfeeMaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRfeeMaxAmount()
    {
        return rfeeMaxAmount;
    }

                        
    /**
     * Set the value of RfeeMaxAmount
     *
     * @param v new value
     */
    public void setRfeeMaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.rfeeMaxAmount, v))
              {
            this.rfeeMaxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PackMinAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPackMinAmount()
    {
        return packMinAmount;
    }

                        
    /**
     * Set the value of PackMinAmount
     *
     * @param v new value
     */
    public void setPackMinAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.packMinAmount, v))
              {
            this.packMinAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PackMaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPackMaxAmount()
    {
        return packMaxAmount;
    }

                        
    /**
     * Set the value of PackMaxAmount
     *
     * @param v new value
     */
    public void setPackMaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.packMaxAmount, v))
              {
            this.packMaxAmount = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PowderItemId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("DefaultRfee");
              fieldNames.add("DefaultPackaging");
              fieldNames.add("Multiplied");
              fieldNames.add("RfeeMinAmount");
              fieldNames.add("RfeeMaxAmount");
              fieldNames.add("PackMinAmount");
              fieldNames.add("PackMaxAmount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PowderItemId"))
        {
                return getPowderItemId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("DefaultRfee"))
        {
                return getDefaultRfee();
            }
          if (name.equals("DefaultPackaging"))
        {
                return getDefaultPackaging();
            }
          if (name.equals("Multiplied"))
        {
                return Boolean.valueOf(getMultiplied());
            }
          if (name.equals("RfeeMinAmount"))
        {
                return getRfeeMinAmount();
            }
          if (name.equals("RfeeMaxAmount"))
        {
                return getRfeeMaxAmount();
            }
          if (name.equals("PackMinAmount"))
        {
                return getPackMinAmount();
            }
          if (name.equals("PackMaxAmount"))
        {
                return getPackMaxAmount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PowderItemPeer.POWDER_ITEM_ID))
        {
                return getPowderItemId();
            }
          if (name.equals(PowderItemPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PowderItemPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PowderItemPeer.DEFAULT_RFEE))
        {
                return getDefaultRfee();
            }
          if (name.equals(PowderItemPeer.DEFAULT_PACKAGING))
        {
                return getDefaultPackaging();
            }
          if (name.equals(PowderItemPeer.MULTIPLIED))
        {
                return Boolean.valueOf(getMultiplied());
            }
          if (name.equals(PowderItemPeer.RFEE_MIN_AMOUNT))
        {
                return getRfeeMinAmount();
            }
          if (name.equals(PowderItemPeer.RFEE_MAX_AMOUNT))
        {
                return getRfeeMaxAmount();
            }
          if (name.equals(PowderItemPeer.PACK_MIN_AMOUNT))
        {
                return getPackMinAmount();
            }
          if (name.equals(PowderItemPeer.PACK_MAX_AMOUNT))
        {
                return getPackMaxAmount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPowderItemId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getItemCode();
            }
              if (pos == 3)
        {
                return getDefaultRfee();
            }
              if (pos == 4)
        {
                return getDefaultPackaging();
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getMultiplied());
            }
              if (pos == 6)
        {
                return getRfeeMinAmount();
            }
              if (pos == 7)
        {
                return getRfeeMaxAmount();
            }
              if (pos == 8)
        {
                return getPackMinAmount();
            }
              if (pos == 9)
        {
                return getPackMaxAmount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PowderItemPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PowderItemPeer.doInsert((PowderItem) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PowderItemPeer.doUpdate((PowderItem) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key powderItemId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPowderItemId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPowderItemId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPowderItemId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PowderItem copy() throws TorqueException
    {
        return copyInto(new PowderItem());
    }
  
    protected PowderItem copyInto(PowderItem copyObj) throws TorqueException
    {
          copyObj.setPowderItemId(powderItemId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setDefaultRfee(defaultRfee);
          copyObj.setDefaultPackaging(defaultPackaging);
          copyObj.setMultiplied(multiplied);
          copyObj.setRfeeMinAmount(rfeeMinAmount);
          copyObj.setRfeeMaxAmount(rfeeMaxAmount);
          copyObj.setPackMinAmount(packMinAmount);
          copyObj.setPackMaxAmount(packMaxAmount);
  
                    copyObj.setPowderItemId((String)null);
                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PowderItemPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PowderItem\n");
        str.append("----------\n")
           .append("PowderItemId         : ")
           .append(getPowderItemId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("DefaultRfee          : ")
           .append(getDefaultRfee())
           .append("\n")
           .append("DefaultPackaging     : ")
           .append(getDefaultPackaging())
           .append("\n")
           .append("Multiplied           : ")
           .append(getMultiplied())
           .append("\n")
           .append("RfeeMinAmount        : ")
           .append(getRfeeMinAmount())
           .append("\n")
           .append("RfeeMaxAmount        : ")
           .append(getRfeeMaxAmount())
           .append("\n")
           .append("PackMinAmount        : ")
           .append(getPackMinAmount())
           .append("\n")
           .append("PackMaxAmount        : ")
           .append(getPackMaxAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}
