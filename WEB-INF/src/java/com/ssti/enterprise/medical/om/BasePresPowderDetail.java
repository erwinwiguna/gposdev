package com.ssti.enterprise.medical.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PresPowderDetail
 */
public abstract class BasePresPowderDetail extends BaseObject
{
    /** The Peer class */
    private static final PresPowderDetailPeer peer =
        new PresPowderDetailPeer();

        
    /** The value for the presPowderDetailId field */
    private String presPowderDetailId;
      
    /** The value for the prescriptionDetailId field */
    private String prescriptionDetailId;
      
    /** The value for the prescriptionId field */
    private String prescriptionId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
      
    /** The value for the itemDesc field */
    private String itemDesc;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
                                                
          
    /** The value for the itemDosage field */
    private BigDecimal itemDosage= bd_ZERO;
                                                
          
    /** The value for the useDosage field */
    private BigDecimal useDosage= bd_ZERO;
                                                
          
    /** The value for the reqQty field */
    private BigDecimal reqQty= bd_ZERO;
      
    /** The value for the usedQty field */
    private BigDecimal usedQty;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the billedQty field */
    private BigDecimal billedQty;
      
    /** The value for the itemPrice field */
    private BigDecimal itemPrice;
      
    /** The value for the taxId field */
    private String taxId;
      
    /** The value for the taxAmount field */
    private BigDecimal taxAmount;
      
    /** The value for the subTotal field */
    private BigDecimal subTotal;
                                                
    /** The value for the batchNo field */
    private String batchNo = "";
  
    
    /**
     * Get the PresPowderDetailId
     *
     * @return String
     */
    public String getPresPowderDetailId()
    {
        return presPowderDetailId;
    }

                        
    /**
     * Set the value of PresPowderDetailId
     *
     * @param v new value
     */
    public void setPresPowderDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presPowderDetailId, v))
              {
            this.presPowderDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrescriptionDetailId
     *
     * @return String
     */
    public String getPrescriptionDetailId()
    {
        return prescriptionDetailId;
    }

                              
    /**
     * Set the value of PrescriptionDetailId
     *
     * @param v new value
     */
    public void setPrescriptionDetailId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.prescriptionDetailId, v))
              {
            this.prescriptionDetailId = v;
            setModified(true);
        }
    
                          
                if (aPrescriptionDetail != null && !ObjectUtils.equals(aPrescriptionDetail.getPrescriptionDetailId(), v))
                {
            aPrescriptionDetail = null;
        }
      
              }
  
    /**
     * Get the PrescriptionId
     *
     * @return String
     */
    public String getPrescriptionId()
    {
        return prescriptionId;
    }

                        
    /**
     * Set the value of PrescriptionId
     *
     * @param v new value
     */
    public void setPrescriptionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.prescriptionId, v))
              {
            this.prescriptionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemDesc
     *
     * @return String
     */
    public String getItemDesc()
    {
        return itemDesc;
    }

                        
    /**
     * Set the value of ItemDesc
     *
     * @param v new value
     */
    public void setItemDesc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemDesc, v))
              {
            this.itemDesc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemDosage
     *
     * @return BigDecimal
     */
    public BigDecimal getItemDosage()
    {
        return itemDosage;
    }

                        
    /**
     * Set the value of ItemDosage
     *
     * @param v new value
     */
    public void setItemDosage(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemDosage, v))
              {
            this.itemDosage = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseDosage
     *
     * @return BigDecimal
     */
    public BigDecimal getUseDosage()
    {
        return useDosage;
    }

                        
    /**
     * Set the value of UseDosage
     *
     * @param v new value
     */
    public void setUseDosage(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.useDosage, v))
              {
            this.useDosage = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReqQty
     *
     * @return BigDecimal
     */
    public BigDecimal getReqQty()
    {
        return reqQty;
    }

                        
    /**
     * Set the value of ReqQty
     *
     * @param v new value
     */
    public void setReqQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.reqQty, v))
              {
            this.reqQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UsedQty
     *
     * @return BigDecimal
     */
    public BigDecimal getUsedQty()
    {
        return usedQty;
    }

                        
    /**
     * Set the value of UsedQty
     *
     * @param v new value
     */
    public void setUsedQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.usedQty, v))
              {
            this.usedQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BilledQty
     *
     * @return BigDecimal
     */
    public BigDecimal getBilledQty()
    {
        return billedQty;
    }

                        
    /**
     * Set the value of BilledQty
     *
     * @param v new value
     */
    public void setBilledQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.billedQty, v))
              {
            this.billedQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

                        
    /**
     * Set the value of TaxAmount
     *
     * @param v new value
     */
    public void setTaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.taxAmount, v))
              {
            this.taxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotal
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotal()
    {
        return subTotal;
    }

                        
    /**
     * Set the value of SubTotal
     *
     * @param v new value
     */
    public void setSubTotal(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotal, v))
              {
            this.subTotal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BatchNo
     *
     * @return String
     */
    public String getBatchNo()
    {
        return batchNo;
    }

                        
    /**
     * Set the value of BatchNo
     *
     * @param v new value
     */
    public void setBatchNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchNo, v))
              {
            this.batchNo = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PrescriptionDetail aPrescriptionDetail;

    /**
     * Declares an association between this object and a PrescriptionDetail object
     *
     * @param v PrescriptionDetail
     * @throws TorqueException
     */
    public void setPrescriptionDetail(PrescriptionDetail v) throws TorqueException
    {
            if (v == null)
        {
                  setPrescriptionDetailId((String) null);
              }
        else
        {
            setPrescriptionDetailId(v.getPrescriptionDetailId());
        }
            aPrescriptionDetail = v;
    }

                                            
    /**
     * Get the associated PrescriptionDetail object
     *
     * @return the associated PrescriptionDetail object
     * @throws TorqueException
     */
    public PrescriptionDetail getPrescriptionDetail() throws TorqueException
    {
        if (aPrescriptionDetail == null && (!ObjectUtils.equals(this.prescriptionDetailId, null)))
        {
                          aPrescriptionDetail = PrescriptionDetailPeer.retrieveByPK(SimpleKey.keyFor(this.prescriptionDetailId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PrescriptionDetail obj = PrescriptionDetailPeer.retrieveByPK(this.prescriptionDetailId);
               obj.addPresPowderDetails(this);
            */
        }
        return aPrescriptionDetail;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPrescriptionDetailKey(ObjectKey key) throws TorqueException
    {
      
                        setPrescriptionDetailId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PresPowderDetailId");
              fieldNames.add("PrescriptionDetailId");
              fieldNames.add("PrescriptionId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("ItemDesc");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("ItemDosage");
              fieldNames.add("UseDosage");
              fieldNames.add("ReqQty");
              fieldNames.add("UsedQty");
              fieldNames.add("Qty");
              fieldNames.add("QtyBase");
              fieldNames.add("BilledQty");
              fieldNames.add("ItemPrice");
              fieldNames.add("TaxId");
              fieldNames.add("TaxAmount");
              fieldNames.add("SubTotal");
              fieldNames.add("BatchNo");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PresPowderDetailId"))
        {
                return getPresPowderDetailId();
            }
          if (name.equals("PrescriptionDetailId"))
        {
                return getPrescriptionDetailId();
            }
          if (name.equals("PrescriptionId"))
        {
                return getPrescriptionId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("ItemDesc"))
        {
                return getItemDesc();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("ItemDosage"))
        {
                return getItemDosage();
            }
          if (name.equals("UseDosage"))
        {
                return getUseDosage();
            }
          if (name.equals("ReqQty"))
        {
                return getReqQty();
            }
          if (name.equals("UsedQty"))
        {
                return getUsedQty();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("BilledQty"))
        {
                return getBilledQty();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("TaxAmount"))
        {
                return getTaxAmount();
            }
          if (name.equals("SubTotal"))
        {
                return getSubTotal();
            }
          if (name.equals("BatchNo"))
        {
                return getBatchNo();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PresPowderDetailPeer.PRES_POWDER_DETAIL_ID))
        {
                return getPresPowderDetailId();
            }
          if (name.equals(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID))
        {
                return getPrescriptionDetailId();
            }
          if (name.equals(PresPowderDetailPeer.PRESCRIPTION_ID))
        {
                return getPrescriptionId();
            }
          if (name.equals(PresPowderDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PresPowderDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PresPowderDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(PresPowderDetailPeer.ITEM_DESC))
        {
                return getItemDesc();
            }
          if (name.equals(PresPowderDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(PresPowderDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(PresPowderDetailPeer.ITEM_DOSAGE))
        {
                return getItemDosage();
            }
          if (name.equals(PresPowderDetailPeer.USE_DOSAGE))
        {
                return getUseDosage();
            }
          if (name.equals(PresPowderDetailPeer.REQ_QTY))
        {
                return getReqQty();
            }
          if (name.equals(PresPowderDetailPeer.USED_QTY))
        {
                return getUsedQty();
            }
          if (name.equals(PresPowderDetailPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(PresPowderDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(PresPowderDetailPeer.BILLED_QTY))
        {
                return getBilledQty();
            }
          if (name.equals(PresPowderDetailPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          if (name.equals(PresPowderDetailPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(PresPowderDetailPeer.TAX_AMOUNT))
        {
                return getTaxAmount();
            }
          if (name.equals(PresPowderDetailPeer.SUB_TOTAL))
        {
                return getSubTotal();
            }
          if (name.equals(PresPowderDetailPeer.BATCH_NO))
        {
                return getBatchNo();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPresPowderDetailId();
            }
              if (pos == 1)
        {
                return getPrescriptionDetailId();
            }
              if (pos == 2)
        {
                return getPrescriptionId();
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getItemName();
            }
              if (pos == 6)
        {
                return getItemDesc();
            }
              if (pos == 7)
        {
                return getUnitId();
            }
              if (pos == 8)
        {
                return getUnitCode();
            }
              if (pos == 9)
        {
                return getItemDosage();
            }
              if (pos == 10)
        {
                return getUseDosage();
            }
              if (pos == 11)
        {
                return getReqQty();
            }
              if (pos == 12)
        {
                return getUsedQty();
            }
              if (pos == 13)
        {
                return getQty();
            }
              if (pos == 14)
        {
                return getQtyBase();
            }
              if (pos == 15)
        {
                return getBilledQty();
            }
              if (pos == 16)
        {
                return getItemPrice();
            }
              if (pos == 17)
        {
                return getTaxId();
            }
              if (pos == 18)
        {
                return getTaxAmount();
            }
              if (pos == 19)
        {
                return getSubTotal();
            }
              if (pos == 20)
        {
                return getBatchNo();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PresPowderDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PresPowderDetailPeer.doInsert((PresPowderDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PresPowderDetailPeer.doUpdate((PresPowderDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key presPowderDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPresPowderDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPresPowderDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPresPowderDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PresPowderDetail copy() throws TorqueException
    {
        return copyInto(new PresPowderDetail());
    }
  
    protected PresPowderDetail copyInto(PresPowderDetail copyObj) throws TorqueException
    {
          copyObj.setPresPowderDetailId(presPowderDetailId);
          copyObj.setPrescriptionDetailId(prescriptionDetailId);
          copyObj.setPrescriptionId(prescriptionId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setItemDesc(itemDesc);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setItemDosage(itemDosage);
          copyObj.setUseDosage(useDosage);
          copyObj.setReqQty(reqQty);
          copyObj.setUsedQty(usedQty);
          copyObj.setQty(qty);
          copyObj.setQtyBase(qtyBase);
          copyObj.setBilledQty(billedQty);
          copyObj.setItemPrice(itemPrice);
          copyObj.setTaxId(taxId);
          copyObj.setTaxAmount(taxAmount);
          copyObj.setSubTotal(subTotal);
          copyObj.setBatchNo(batchNo);
  
                    copyObj.setPresPowderDetailId((String)null);
                                                                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PresPowderDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PresPowderDetail\n");
        str.append("----------------\n")
           .append("PresPowderDetailId   : ")
           .append(getPresPowderDetailId())
           .append("\n")
           .append("PrescriptionDetailId   : ")
           .append(getPrescriptionDetailId())
           .append("\n")
           .append("PrescriptionId       : ")
           .append(getPrescriptionId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("ItemDesc             : ")
           .append(getItemDesc())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("ItemDosage           : ")
           .append(getItemDosage())
           .append("\n")
           .append("UseDosage            : ")
           .append(getUseDosage())
           .append("\n")
           .append("ReqQty               : ")
           .append(getReqQty())
           .append("\n")
           .append("UsedQty              : ")
           .append(getUsedQty())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("BilledQty            : ")
           .append(getBilledQty())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("TaxAmount            : ")
           .append(getTaxAmount())
           .append("\n")
           .append("SubTotal             : ")
           .append(getSubTotal())
           .append("\n")
           .append("BatchNo              : ")
           .append(getBatchNo())
           .append("\n")
        ;
        return(str.toString());
    }
}
