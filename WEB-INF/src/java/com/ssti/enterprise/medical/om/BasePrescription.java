package com.ssti.enterprise.medical.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Prescription
 */
public abstract class BasePrescription extends BaseObject
{
    /** The Peer class */
    private static final PrescriptionPeer peer =
        new PrescriptionPeer();

        
    /** The value for the prescriptionId field */
    private String prescriptionId;
      
    /** The value for the prescriptionNo field */
    private String prescriptionNo;
      
    /** The value for the customerId field */
    private String customerId;
                                                
    /** The value for the patientName field */
    private String patientName = "";
                                                
    /** The value for the patientAge field */
    private String patientAge = "";
                                                
    /** The value for the patientPhone field */
    private String patientPhone = "";
                                          
    /** The value for the patientWeight field */
    private int patientWeight = 1;
                                                
    /** The value for the patientAddress field */
    private String patientAddress = "";
                                                
    /** The value for the presFile field */
    private String presFile = "";
      
    /** The value for the createBy field */
    private String createBy;
      
    /** The value for the doctorId field */
    private String doctorId;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the presDiscount field */
    private String presDiscount;
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
      
    /** The value for the totalRfee field */
    private BigDecimal totalRfee;
      
    /** The value for the totalPackaging field */
    private BigDecimal totalPackaging;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
                                                
    /** The value for the medicalRecordId field */
    private String medicalRecordId = "";
                                                
    /** The value for the registrationId field */
    private String registrationId = "";
  
    
    /**
     * Get the PrescriptionId
     *
     * @return String
     */
    public String getPrescriptionId()
    {
        return prescriptionId;
    }

                                              
    /**
     * Set the value of PrescriptionId
     *
     * @param v new value
     */
    public void setPrescriptionId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.prescriptionId, v))
              {
            this.prescriptionId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PrescriptionDetail
        if (collPrescriptionDetails != null)
        {
            for (int i = 0; i < collPrescriptionDetails.size(); i++)
            {
                ((PrescriptionDetail) collPrescriptionDetails.get(i))
                    .setPrescriptionId(v);
            }
        }
                                }
  
    /**
     * Get the PrescriptionNo
     *
     * @return String
     */
    public String getPrescriptionNo()
    {
        return prescriptionNo;
    }

                        
    /**
     * Set the value of PrescriptionNo
     *
     * @param v new value
     */
    public void setPrescriptionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.prescriptionNo, v))
              {
            this.prescriptionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientName
     *
     * @return String
     */
    public String getPatientName()
    {
        return patientName;
    }

                        
    /**
     * Set the value of PatientName
     *
     * @param v new value
     */
    public void setPatientName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientName, v))
              {
            this.patientName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientAge
     *
     * @return String
     */
    public String getPatientAge()
    {
        return patientAge;
    }

                        
    /**
     * Set the value of PatientAge
     *
     * @param v new value
     */
    public void setPatientAge(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientAge, v))
              {
            this.patientAge = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientPhone
     *
     * @return String
     */
    public String getPatientPhone()
    {
        return patientPhone;
    }

                        
    /**
     * Set the value of PatientPhone
     *
     * @param v new value
     */
    public void setPatientPhone(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientPhone, v))
              {
            this.patientPhone = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientWeight
     *
     * @return int
     */
    public int getPatientWeight()
    {
        return patientWeight;
    }

                        
    /**
     * Set the value of PatientWeight
     *
     * @param v new value
     */
    public void setPatientWeight(int v) 
    {
    
                  if (this.patientWeight != v)
              {
            this.patientWeight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientAddress
     *
     * @return String
     */
    public String getPatientAddress()
    {
        return patientAddress;
    }

                        
    /**
     * Set the value of PatientAddress
     *
     * @param v new value
     */
    public void setPatientAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientAddress, v))
              {
            this.patientAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresFile
     *
     * @return String
     */
    public String getPresFile()
    {
        return presFile;
    }

                        
    /**
     * Set the value of PresFile
     *
     * @param v new value
     */
    public void setPresFile(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presFile, v))
              {
            this.presFile = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DoctorId
     *
     * @return String
     */
    public String getDoctorId()
    {
        return doctorId;
    }

                        
    /**
     * Set the value of DoctorId
     *
     * @param v new value
     */
    public void setDoctorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.doctorId, v))
              {
            this.doctorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresDiscount
     *
     * @return String
     */
    public String getPresDiscount()
    {
        return presDiscount;
    }

                        
    /**
     * Set the value of PresDiscount
     *
     * @param v new value
     */
    public void setPresDiscount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presDiscount, v))
              {
            this.presDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalRfee
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalRfee()
    {
        return totalRfee;
    }

                        
    /**
     * Set the value of TotalRfee
     *
     * @param v new value
     */
    public void setTotalRfee(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalRfee, v))
              {
            this.totalRfee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalPackaging
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalPackaging()
    {
        return totalPackaging;
    }

                        
    /**
     * Set the value of TotalPackaging
     *
     * @param v new value
     */
    public void setTotalPackaging(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalPackaging, v))
              {
            this.totalPackaging = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MedicalRecordId
     *
     * @return String
     */
    public String getMedicalRecordId()
    {
        return medicalRecordId;
    }

                        
    /**
     * Set the value of MedicalRecordId
     *
     * @param v new value
     */
    public void setMedicalRecordId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.medicalRecordId, v))
              {
            this.medicalRecordId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RegistrationId
     *
     * @return String
     */
    public String getRegistrationId()
    {
        return registrationId;
    }

                        
    /**
     * Set the value of RegistrationId
     *
     * @param v new value
     */
    public void setRegistrationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.registrationId, v))
              {
            this.registrationId = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPrescriptionDetails
     */
    protected List collPrescriptionDetails;

    /**
     * Temporary storage of collPrescriptionDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPrescriptionDetails()
    {
        if (collPrescriptionDetails == null)
        {
            collPrescriptionDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PrescriptionDetail object to this object
     * through the PrescriptionDetail foreign key attribute
     *
     * @param l PrescriptionDetail
     * @throws TorqueException
     */
    public void addPrescriptionDetail(PrescriptionDetail l) throws TorqueException
    {
        getPrescriptionDetails().add(l);
        l.setPrescription((Prescription) this);
    }

    /**
     * The criteria used to select the current contents of collPrescriptionDetails
     */
    private Criteria lastPrescriptionDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPrescriptionDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPrescriptionDetails() throws TorqueException
    {
              if (collPrescriptionDetails == null)
        {
            collPrescriptionDetails = getPrescriptionDetails(new Criteria(10));
        }
        return collPrescriptionDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prescription has previously
     * been saved, it will retrieve related PrescriptionDetails from storage.
     * If this Prescription is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPrescriptionDetails(Criteria criteria) throws TorqueException
    {
              if (collPrescriptionDetails == null)
        {
            if (isNew())
            {
               collPrescriptionDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PrescriptionDetailPeer.PRESCRIPTION_ID, getPrescriptionId() );
                        collPrescriptionDetails = PrescriptionDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PrescriptionDetailPeer.PRESCRIPTION_ID, getPrescriptionId());
                            if (!lastPrescriptionDetailsCriteria.equals(criteria))
                {
                    collPrescriptionDetails = PrescriptionDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPrescriptionDetailsCriteria = criteria;

        return collPrescriptionDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPrescriptionDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPrescriptionDetails(Connection con) throws TorqueException
    {
              if (collPrescriptionDetails == null)
        {
            collPrescriptionDetails = getPrescriptionDetails(new Criteria(10), con);
        }
        return collPrescriptionDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prescription has previously
     * been saved, it will retrieve related PrescriptionDetails from storage.
     * If this Prescription is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPrescriptionDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPrescriptionDetails == null)
        {
            if (isNew())
            {
               collPrescriptionDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PrescriptionDetailPeer.PRESCRIPTION_ID, getPrescriptionId());
                         collPrescriptionDetails = PrescriptionDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PrescriptionDetailPeer.PRESCRIPTION_ID, getPrescriptionId());
                             if (!lastPrescriptionDetailsCriteria.equals(criteria))
                 {
                     collPrescriptionDetails = PrescriptionDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPrescriptionDetailsCriteria = criteria;

         return collPrescriptionDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prescription is new, it will return
     * an empty collection; or if this Prescription has previously
     * been saved, it will retrieve related PrescriptionDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prescription.
     */
    protected List getPrescriptionDetailsJoinPrescription(Criteria criteria)
        throws TorqueException
    {
                    if (collPrescriptionDetails == null)
        {
            if (isNew())
            {
               collPrescriptionDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PrescriptionDetailPeer.PRESCRIPTION_ID, getPrescriptionId());
                              collPrescriptionDetails = PrescriptionDetailPeer.doSelectJoinPrescription(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PrescriptionDetailPeer.PRESCRIPTION_ID, getPrescriptionId());
                                    if (!lastPrescriptionDetailsCriteria.equals(criteria))
            {
                collPrescriptionDetails = PrescriptionDetailPeer.doSelectJoinPrescription(criteria);
            }
        }
        lastPrescriptionDetailsCriteria = criteria;

        return collPrescriptionDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PrescriptionId");
              fieldNames.add("PrescriptionNo");
              fieldNames.add("CustomerId");
              fieldNames.add("PatientName");
              fieldNames.add("PatientAge");
              fieldNames.add("PatientPhone");
              fieldNames.add("PatientWeight");
              fieldNames.add("PatientAddress");
              fieldNames.add("PresFile");
              fieldNames.add("CreateBy");
              fieldNames.add("DoctorId");
              fieldNames.add("Remark");
              fieldNames.add("LocationId");
              fieldNames.add("Status");
              fieldNames.add("CreateDate");
              fieldNames.add("UpdateDate");
              fieldNames.add("PresDiscount");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalAmount");
              fieldNames.add("TotalRfee");
              fieldNames.add("TotalPackaging");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("MedicalRecordId");
              fieldNames.add("RegistrationId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PrescriptionId"))
        {
                return getPrescriptionId();
            }
          if (name.equals("PrescriptionNo"))
        {
                return getPrescriptionNo();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("PatientName"))
        {
                return getPatientName();
            }
          if (name.equals("PatientAge"))
        {
                return getPatientAge();
            }
          if (name.equals("PatientPhone"))
        {
                return getPatientPhone();
            }
          if (name.equals("PatientWeight"))
        {
                return Integer.valueOf(getPatientWeight());
            }
          if (name.equals("PatientAddress"))
        {
                return getPatientAddress();
            }
          if (name.equals("PresFile"))
        {
                return getPresFile();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("DoctorId"))
        {
                return getDoctorId();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("PresDiscount"))
        {
                return getPresDiscount();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("TotalRfee"))
        {
                return getTotalRfee();
            }
          if (name.equals("TotalPackaging"))
        {
                return getTotalPackaging();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("MedicalRecordId"))
        {
                return getMedicalRecordId();
            }
          if (name.equals("RegistrationId"))
        {
                return getRegistrationId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PrescriptionPeer.PRESCRIPTION_ID))
        {
                return getPrescriptionId();
            }
          if (name.equals(PrescriptionPeer.PRESCRIPTION_NO))
        {
                return getPrescriptionNo();
            }
          if (name.equals(PrescriptionPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(PrescriptionPeer.PATIENT_NAME))
        {
                return getPatientName();
            }
          if (name.equals(PrescriptionPeer.PATIENT_AGE))
        {
                return getPatientAge();
            }
          if (name.equals(PrescriptionPeer.PATIENT_PHONE))
        {
                return getPatientPhone();
            }
          if (name.equals(PrescriptionPeer.PATIENT_WEIGHT))
        {
                return Integer.valueOf(getPatientWeight());
            }
          if (name.equals(PrescriptionPeer.PATIENT_ADDRESS))
        {
                return getPatientAddress();
            }
          if (name.equals(PrescriptionPeer.PRES_FILE))
        {
                return getPresFile();
            }
          if (name.equals(PrescriptionPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(PrescriptionPeer.DOCTOR_ID))
        {
                return getDoctorId();
            }
          if (name.equals(PrescriptionPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PrescriptionPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PrescriptionPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(PrescriptionPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(PrescriptionPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(PrescriptionPeer.PRES_DISCOUNT))
        {
                return getPresDiscount();
            }
          if (name.equals(PrescriptionPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(PrescriptionPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(PrescriptionPeer.TOTAL_RFEE))
        {
                return getTotalRfee();
            }
          if (name.equals(PrescriptionPeer.TOTAL_PACKAGING))
        {
                return getTotalPackaging();
            }
          if (name.equals(PrescriptionPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(PrescriptionPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(PrescriptionPeer.MEDICAL_RECORD_ID))
        {
                return getMedicalRecordId();
            }
          if (name.equals(PrescriptionPeer.REGISTRATION_ID))
        {
                return getRegistrationId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPrescriptionId();
            }
              if (pos == 1)
        {
                return getPrescriptionNo();
            }
              if (pos == 2)
        {
                return getCustomerId();
            }
              if (pos == 3)
        {
                return getPatientName();
            }
              if (pos == 4)
        {
                return getPatientAge();
            }
              if (pos == 5)
        {
                return getPatientPhone();
            }
              if (pos == 6)
        {
                return Integer.valueOf(getPatientWeight());
            }
              if (pos == 7)
        {
                return getPatientAddress();
            }
              if (pos == 8)
        {
                return getPresFile();
            }
              if (pos == 9)
        {
                return getCreateBy();
            }
              if (pos == 10)
        {
                return getDoctorId();
            }
              if (pos == 11)
        {
                return getRemark();
            }
              if (pos == 12)
        {
                return getLocationId();
            }
              if (pos == 13)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 14)
        {
                return getCreateDate();
            }
              if (pos == 15)
        {
                return getUpdateDate();
            }
              if (pos == 16)
        {
                return getPresDiscount();
            }
              if (pos == 17)
        {
                return getTotalDiscount();
            }
              if (pos == 18)
        {
                return getTotalAmount();
            }
              if (pos == 19)
        {
                return getTotalRfee();
            }
              if (pos == 20)
        {
                return getTotalPackaging();
            }
              if (pos == 21)
        {
                return getCancelBy();
            }
              if (pos == 22)
        {
                return getCancelDate();
            }
              if (pos == 23)
        {
                return getMedicalRecordId();
            }
              if (pos == 24)
        {
                return getRegistrationId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PrescriptionPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PrescriptionPeer.doInsert((Prescription) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PrescriptionPeer.doUpdate((Prescription) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPrescriptionDetails != null)
            {
                for (int i = 0; i < collPrescriptionDetails.size(); i++)
                {
                    ((PrescriptionDetail) collPrescriptionDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key prescriptionId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPrescriptionId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPrescriptionId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPrescriptionId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Prescription copy() throws TorqueException
    {
        return copyInto(new Prescription());
    }
  
    protected Prescription copyInto(Prescription copyObj) throws TorqueException
    {
          copyObj.setPrescriptionId(prescriptionId);
          copyObj.setPrescriptionNo(prescriptionNo);
          copyObj.setCustomerId(customerId);
          copyObj.setPatientName(patientName);
          copyObj.setPatientAge(patientAge);
          copyObj.setPatientPhone(patientPhone);
          copyObj.setPatientWeight(patientWeight);
          copyObj.setPatientAddress(patientAddress);
          copyObj.setPresFile(presFile);
          copyObj.setCreateBy(createBy);
          copyObj.setDoctorId(doctorId);
          copyObj.setRemark(remark);
          copyObj.setLocationId(locationId);
          copyObj.setStatus(status);
          copyObj.setCreateDate(createDate);
          copyObj.setUpdateDate(updateDate);
          copyObj.setPresDiscount(presDiscount);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setTotalRfee(totalRfee);
          copyObj.setTotalPackaging(totalPackaging);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setMedicalRecordId(medicalRecordId);
          copyObj.setRegistrationId(registrationId);
  
                    copyObj.setPrescriptionId((String)null);
                                                                                                                                                            
                                      
                            
        List v = getPrescriptionDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PrescriptionDetail obj = (PrescriptionDetail) v.get(i);
            copyObj.addPrescriptionDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PrescriptionPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Prescription\n");
        str.append("------------\n")
           .append("PrescriptionId       : ")
           .append(getPrescriptionId())
           .append("\n")
           .append("PrescriptionNo       : ")
           .append(getPrescriptionNo())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("PatientName          : ")
           .append(getPatientName())
           .append("\n")
           .append("PatientAge           : ")
           .append(getPatientAge())
           .append("\n")
           .append("PatientPhone         : ")
           .append(getPatientPhone())
           .append("\n")
           .append("PatientWeight        : ")
           .append(getPatientWeight())
           .append("\n")
           .append("PatientAddress       : ")
           .append(getPatientAddress())
           .append("\n")
           .append("PresFile             : ")
           .append(getPresFile())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("DoctorId             : ")
           .append(getDoctorId())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("PresDiscount         : ")
           .append(getPresDiscount())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("TotalRfee            : ")
           .append(getTotalRfee())
           .append("\n")
           .append("TotalPackaging       : ")
           .append(getTotalPackaging())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("MedicalRecordId      : ")
           .append(getMedicalRecordId())
           .append("\n")
           .append("RegistrationId       : ")
           .append(getRegistrationId())
           .append("\n")
        ;
        return(str.toString());
    }
}
