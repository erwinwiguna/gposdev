package com.ssti.enterprise.medical.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.om.map.PrescriptionMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BasePrescriptionPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "prescription";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PrescriptionMapBuilder.CLASS_NAME);
    }

      /** the column name for the PRESCRIPTION_ID field */
    public static final String PRESCRIPTION_ID;
      /** the column name for the PRESCRIPTION_NO field */
    public static final String PRESCRIPTION_NO;
      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the PATIENT_NAME field */
    public static final String PATIENT_NAME;
      /** the column name for the PATIENT_AGE field */
    public static final String PATIENT_AGE;
      /** the column name for the PATIENT_PHONE field */
    public static final String PATIENT_PHONE;
      /** the column name for the PATIENT_WEIGHT field */
    public static final String PATIENT_WEIGHT;
      /** the column name for the PATIENT_ADDRESS field */
    public static final String PATIENT_ADDRESS;
      /** the column name for the PRES_FILE field */
    public static final String PRES_FILE;
      /** the column name for the CREATE_BY field */
    public static final String CREATE_BY;
      /** the column name for the DOCTOR_ID field */
    public static final String DOCTOR_ID;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the CREATE_DATE field */
    public static final String CREATE_DATE;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the PRES_DISCOUNT field */
    public static final String PRES_DISCOUNT;
      /** the column name for the TOTAL_DISCOUNT field */
    public static final String TOTAL_DISCOUNT;
      /** the column name for the TOTAL_AMOUNT field */
    public static final String TOTAL_AMOUNT;
      /** the column name for the TOTAL_RFEE field */
    public static final String TOTAL_RFEE;
      /** the column name for the TOTAL_PACKAGING field */
    public static final String TOTAL_PACKAGING;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
      /** the column name for the MEDICAL_RECORD_ID field */
    public static final String MEDICAL_RECORD_ID;
      /** the column name for the REGISTRATION_ID field */
    public static final String REGISTRATION_ID;
  
    static
    {
          PRESCRIPTION_ID = "prescription.PRESCRIPTION_ID";
          PRESCRIPTION_NO = "prescription.PRESCRIPTION_NO";
          CUSTOMER_ID = "prescription.CUSTOMER_ID";
          PATIENT_NAME = "prescription.PATIENT_NAME";
          PATIENT_AGE = "prescription.PATIENT_AGE";
          PATIENT_PHONE = "prescription.PATIENT_PHONE";
          PATIENT_WEIGHT = "prescription.PATIENT_WEIGHT";
          PATIENT_ADDRESS = "prescription.PATIENT_ADDRESS";
          PRES_FILE = "prescription.PRES_FILE";
          CREATE_BY = "prescription.CREATE_BY";
          DOCTOR_ID = "prescription.DOCTOR_ID";
          REMARK = "prescription.REMARK";
          LOCATION_ID = "prescription.LOCATION_ID";
          STATUS = "prescription.STATUS";
          CREATE_DATE = "prescription.CREATE_DATE";
          UPDATE_DATE = "prescription.UPDATE_DATE";
          PRES_DISCOUNT = "prescription.PRES_DISCOUNT";
          TOTAL_DISCOUNT = "prescription.TOTAL_DISCOUNT";
          TOTAL_AMOUNT = "prescription.TOTAL_AMOUNT";
          TOTAL_RFEE = "prescription.TOTAL_RFEE";
          TOTAL_PACKAGING = "prescription.TOTAL_PACKAGING";
          CANCEL_BY = "prescription.CANCEL_BY";
          CANCEL_DATE = "prescription.CANCEL_DATE";
          MEDICAL_RECORD_ID = "prescription.MEDICAL_RECORD_ID";
          REGISTRATION_ID = "prescription.REGISTRATION_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PrescriptionMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PrescriptionMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  25;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.medical.om.Prescription";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePrescriptionPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                        
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PRESCRIPTION_ID);
          criteria.addSelectColumn(PRESCRIPTION_NO);
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(PATIENT_NAME);
          criteria.addSelectColumn(PATIENT_AGE);
          criteria.addSelectColumn(PATIENT_PHONE);
          criteria.addSelectColumn(PATIENT_WEIGHT);
          criteria.addSelectColumn(PATIENT_ADDRESS);
          criteria.addSelectColumn(PRES_FILE);
          criteria.addSelectColumn(CREATE_BY);
          criteria.addSelectColumn(DOCTOR_ID);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(CREATE_DATE);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(PRES_DISCOUNT);
          criteria.addSelectColumn(TOTAL_DISCOUNT);
          criteria.addSelectColumn(TOTAL_AMOUNT);
          criteria.addSelectColumn(TOTAL_RFEE);
          criteria.addSelectColumn(TOTAL_PACKAGING);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
          criteria.addSelectColumn(MEDICAL_RECORD_ID);
          criteria.addSelectColumn(REGISTRATION_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Prescription row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Prescription obj = (Prescription) cls.newInstance();
            PrescriptionPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Prescription obj)
        throws TorqueException
    {
        try
        {
                obj.setPrescriptionId(row.getValue(offset + 0).asString());
                  obj.setPrescriptionNo(row.getValue(offset + 1).asString());
                  obj.setCustomerId(row.getValue(offset + 2).asString());
                  obj.setPatientName(row.getValue(offset + 3).asString());
                  obj.setPatientAge(row.getValue(offset + 4).asString());
                  obj.setPatientPhone(row.getValue(offset + 5).asString());
                  obj.setPatientWeight(row.getValue(offset + 6).asInt());
                  obj.setPatientAddress(row.getValue(offset + 7).asString());
                  obj.setPresFile(row.getValue(offset + 8).asString());
                  obj.setCreateBy(row.getValue(offset + 9).asString());
                  obj.setDoctorId(row.getValue(offset + 10).asString());
                  obj.setRemark(row.getValue(offset + 11).asString());
                  obj.setLocationId(row.getValue(offset + 12).asString());
                  obj.setStatus(row.getValue(offset + 13).asInt());
                  obj.setCreateDate(row.getValue(offset + 14).asUtilDate());
                  obj.setUpdateDate(row.getValue(offset + 15).asUtilDate());
                  obj.setPresDiscount(row.getValue(offset + 16).asString());
                  obj.setTotalDiscount(row.getValue(offset + 17).asBigDecimal());
                  obj.setTotalAmount(row.getValue(offset + 18).asBigDecimal());
                  obj.setTotalRfee(row.getValue(offset + 19).asBigDecimal());
                  obj.setTotalPackaging(row.getValue(offset + 20).asBigDecimal());
                  obj.setCancelBy(row.getValue(offset + 21).asString());
                  obj.setCancelDate(row.getValue(offset + 22).asUtilDate());
                  obj.setMedicalRecordId(row.getValue(offset + 23).asString());
                  obj.setRegistrationId(row.getValue(offset + 24).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePrescriptionPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                        
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PrescriptionPeer.row2Object(row, 1,
                PrescriptionPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePrescriptionPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PRESCRIPTION_ID, criteria.remove(PRESCRIPTION_ID));
                                                                                                                                                                                                                                                      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PrescriptionPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                        
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Prescription obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Prescription obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Prescription obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Prescription obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Prescription) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Prescription obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Prescription) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Prescription obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Prescription) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Prescription obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePrescriptionPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PRESCRIPTION_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Prescription obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PRESCRIPTION_ID, obj.getPrescriptionId());
              criteria.add(PRESCRIPTION_NO, obj.getPrescriptionNo());
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(PATIENT_NAME, obj.getPatientName());
              criteria.add(PATIENT_AGE, obj.getPatientAge());
              criteria.add(PATIENT_PHONE, obj.getPatientPhone());
              criteria.add(PATIENT_WEIGHT, obj.getPatientWeight());
              criteria.add(PATIENT_ADDRESS, obj.getPatientAddress());
              criteria.add(PRES_FILE, obj.getPresFile());
              criteria.add(CREATE_BY, obj.getCreateBy());
              criteria.add(DOCTOR_ID, obj.getDoctorId());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(CREATE_DATE, obj.getCreateDate());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(PRES_DISCOUNT, obj.getPresDiscount());
              criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
              criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
              criteria.add(TOTAL_RFEE, obj.getTotalRfee());
              criteria.add(TOTAL_PACKAGING, obj.getTotalPackaging());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
              criteria.add(MEDICAL_RECORD_ID, obj.getMedicalRecordId());
              criteria.add(REGISTRATION_ID, obj.getRegistrationId());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Prescription obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PRESCRIPTION_ID, obj.getPrescriptionId());
                          criteria.add(PRESCRIPTION_NO, obj.getPrescriptionNo());
                          criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(PATIENT_NAME, obj.getPatientName());
                          criteria.add(PATIENT_AGE, obj.getPatientAge());
                          criteria.add(PATIENT_PHONE, obj.getPatientPhone());
                          criteria.add(PATIENT_WEIGHT, obj.getPatientWeight());
                          criteria.add(PATIENT_ADDRESS, obj.getPatientAddress());
                          criteria.add(PRES_FILE, obj.getPresFile());
                          criteria.add(CREATE_BY, obj.getCreateBy());
                          criteria.add(DOCTOR_ID, obj.getDoctorId());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(CREATE_DATE, obj.getCreateDate());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(PRES_DISCOUNT, obj.getPresDiscount());
                          criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
                          criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
                          criteria.add(TOTAL_RFEE, obj.getTotalRfee());
                          criteria.add(TOTAL_PACKAGING, obj.getTotalPackaging());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
                          criteria.add(MEDICAL_RECORD_ID, obj.getMedicalRecordId());
                          criteria.add(REGISTRATION_ID, obj.getRegistrationId());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Prescription retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Prescription retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Prescription retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Prescription retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Prescription retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Prescription)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PRESCRIPTION_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
