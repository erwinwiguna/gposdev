package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerMarginMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.CustomerMarginMapBuilder";

    /**
     * Item
     * @deprecated use CustomerMarginPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_margin";
    }

  
    /**
     * customer_margin.CUSTOMER_MARGIN_ID
     * @return the column name for the CUSTOMER_MARGIN_ID field
     * @deprecated use CustomerMarginPeer.customer_margin.CUSTOMER_MARGIN_ID constant
     */
    public static String getCustomerMargin_CustomerMarginId()
    {
        return "customer_margin.CUSTOMER_MARGIN_ID";
    }
  
    /**
     * customer_margin.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use CustomerMarginPeer.customer_margin.CUSTOMER_TYPE_ID constant
     */
    public static String getCustomerMargin_CustomerTypeId()
    {
        return "customer_margin.CUSTOMER_TYPE_ID";
    }
  
    /**
     * customer_margin.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerMarginPeer.customer_margin.CUSTOMER_ID constant
     */
    public static String getCustomerMargin_CustomerId()
    {
        return "customer_margin.CUSTOMER_ID";
    }
  
    /**
     * customer_margin.CATEGORY_ID
     * @return the column name for the CATEGORY_ID field
     * @deprecated use CustomerMarginPeer.customer_margin.CATEGORY_ID constant
     */
    public static String getCustomerMargin_CategoryId()
    {
        return "customer_margin.CATEGORY_ID";
    }
  
    /**
     * customer_margin.ITEM_CODES
     * @return the column name for the ITEM_CODES field
     * @deprecated use CustomerMarginPeer.customer_margin.ITEM_CODES constant
     */
    public static String getCustomerMargin_ItemCodes()
    {
        return "customer_margin.ITEM_CODES";
    }
  
    /**
     * customer_margin.LOCATION_IDS
     * @return the column name for the LOCATION_IDS field
     * @deprecated use CustomerMarginPeer.customer_margin.LOCATION_IDS constant
     */
    public static String getCustomerMargin_LocationIds()
    {
        return "customer_margin.LOCATION_IDS";
    }
  
    /**
     * customer_margin.COLOR
     * @return the column name for the COLOR field
     * @deprecated use CustomerMarginPeer.customer_margin.COLOR constant
     */
    public static String getCustomerMargin_Color()
    {
        return "customer_margin.COLOR";
    }
  
    /**
     * customer_margin.MARGIN
     * @return the column name for the MARGIN field
     * @deprecated use CustomerMarginPeer.customer_margin.MARGIN constant
     */
    public static String getCustomerMargin_Margin()
    {
        return "customer_margin.MARGIN";
    }
  
    /**
     * customer_margin.PRIORITY
     * @return the column name for the PRIORITY field
     * @deprecated use CustomerMarginPeer.customer_margin.PRIORITY constant
     */
    public static String getCustomerMargin_Priority()
    {
        return "customer_margin.PRIORITY";
    }
  
    /**
     * customer_margin.TRANS_TYPE
     * @return the column name for the TRANS_TYPE field
     * @deprecated use CustomerMarginPeer.customer_margin.TRANS_TYPE constant
     */
    public static String getCustomerMargin_TransType()
    {
        return "customer_margin.TRANS_TYPE";
    }
  
    /**
     * customer_margin.RFEE_APPLIED
     * @return the column name for the RFEE_APPLIED field
     * @deprecated use CustomerMarginPeer.customer_margin.RFEE_APPLIED constant
     */
    public static String getCustomerMargin_RfeeApplied()
    {
        return "customer_margin.RFEE_APPLIED";
    }
  
    /**
     * customer_margin.PACK_APPLIED
     * @return the column name for the PACK_APPLIED field
     * @deprecated use CustomerMarginPeer.customer_margin.PACK_APPLIED constant
     */
    public static String getCustomerMargin_PackApplied()
    {
        return "customer_margin.PACK_APPLIED";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_margin");
        TableMap tMap = dbMap.getTable("customer_margin");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_margin.CUSTOMER_MARGIN_ID", "");
                          tMap.addColumn("customer_margin.CUSTOMER_TYPE_ID", "");
                          tMap.addColumn("customer_margin.CUSTOMER_ID", "");
                          tMap.addColumn("customer_margin.CATEGORY_ID", "");
                          tMap.addColumn("customer_margin.ITEM_CODES", "");
                          tMap.addColumn("customer_margin.LOCATION_IDS", "");
                          tMap.addColumn("customer_margin.COLOR", "");
                          tMap.addColumn("customer_margin.MARGIN", "");
                            tMap.addColumn("customer_margin.PRIORITY", Integer.valueOf(0));
                            tMap.addColumn("customer_margin.TRANS_TYPE", Integer.valueOf(0));
                          tMap.addColumn("customer_margin.RFEE_APPLIED", Boolean.TRUE);
                          tMap.addColumn("customer_margin.PACK_APPLIED", Boolean.TRUE);
          }
}
