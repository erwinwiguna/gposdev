package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DoctorMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.DoctorMapBuilder";

    /**
     * Item
     * @deprecated use DoctorPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "doctor";
    }

  
    /**
     * doctor.DOCTOR_ID
     * @return the column name for the DOCTOR_ID field
     * @deprecated use DoctorPeer.doctor.DOCTOR_ID constant
     */
    public static String getDoctor_DoctorId()
    {
        return "doctor.DOCTOR_ID";
    }
  
    /**
     * doctor.DOCTOR_CODE
     * @return the column name for the DOCTOR_CODE field
     * @deprecated use DoctorPeer.doctor.DOCTOR_CODE constant
     */
    public static String getDoctor_DoctorCode()
    {
        return "doctor.DOCTOR_CODE";
    }
  
    /**
     * doctor.DOCTOR_NAME
     * @return the column name for the DOCTOR_NAME field
     * @deprecated use DoctorPeer.doctor.DOCTOR_NAME constant
     */
    public static String getDoctor_DoctorName()
    {
        return "doctor.DOCTOR_NAME";
    }
  
    /**
     * doctor.SPECIALIZATION
     * @return the column name for the SPECIALIZATION field
     * @deprecated use DoctorPeer.doctor.SPECIALIZATION constant
     */
    public static String getDoctor_Specialization()
    {
        return "doctor.SPECIALIZATION";
    }
  
    /**
     * doctor.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use DoctorPeer.doctor.DESCRIPTION constant
     */
    public static String getDoctor_Description()
    {
        return "doctor.DESCRIPTION";
    }
  
    /**
     * doctor.HOME_ADDRESS
     * @return the column name for the HOME_ADDRESS field
     * @deprecated use DoctorPeer.doctor.HOME_ADDRESS constant
     */
    public static String getDoctor_HomeAddress()
    {
        return "doctor.HOME_ADDRESS";
    }
  
    /**
     * doctor.PRACTICE_ADDRESS
     * @return the column name for the PRACTICE_ADDRESS field
     * @deprecated use DoctorPeer.doctor.PRACTICE_ADDRESS constant
     */
    public static String getDoctor_PracticeAddress()
    {
        return "doctor.PRACTICE_ADDRESS";
    }
  
    /**
     * doctor.PHONE1
     * @return the column name for the PHONE1 field
     * @deprecated use DoctorPeer.doctor.PHONE1 constant
     */
    public static String getDoctor_Phone1()
    {
        return "doctor.PHONE1";
    }
  
    /**
     * doctor.PHONE2
     * @return the column name for the PHONE2 field
     * @deprecated use DoctorPeer.doctor.PHONE2 constant
     */
    public static String getDoctor_Phone2()
    {
        return "doctor.PHONE2";
    }
  
    /**
     * doctor.IS_INTERNAL
     * @return the column name for the IS_INTERNAL field
     * @deprecated use DoctorPeer.doctor.IS_INTERNAL constant
     */
    public static String getDoctor_IsInternal()
    {
        return "doctor.IS_INTERNAL";
    }
  
    /**
     * doctor.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use DoctorPeer.doctor.LOCATION_ID constant
     */
    public static String getDoctor_LocationId()
    {
        return "doctor.LOCATION_ID";
    }
  
    /**
     * doctor.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use DoctorPeer.doctor.USER_NAME constant
     */
    public static String getDoctor_UserName()
    {
        return "doctor.USER_NAME";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("doctor");
        TableMap tMap = dbMap.getTable("doctor");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("doctor.DOCTOR_ID", "");
                          tMap.addColumn("doctor.DOCTOR_CODE", "");
                          tMap.addColumn("doctor.DOCTOR_NAME", "");
                          tMap.addColumn("doctor.SPECIALIZATION", "");
                          tMap.addColumn("doctor.DESCRIPTION", "");
                          tMap.addColumn("doctor.HOME_ADDRESS", "");
                          tMap.addColumn("doctor.PRACTICE_ADDRESS", "");
                          tMap.addColumn("doctor.PHONE1", "");
                          tMap.addColumn("doctor.PHONE2", "");
                          tMap.addColumn("doctor.IS_INTERNAL", Boolean.TRUE);
                          tMap.addColumn("doctor.LOCATION_ID", "");
                          tMap.addColumn("doctor.USER_NAME", "");
          }
}
