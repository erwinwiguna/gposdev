package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class MedColorMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.MedColorMapBuilder";

    /**
     * Item
     * @deprecated use MedColorPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "med_color";
    }

  
    /**
     * med_color.COLOR
     * @return the column name for the COLOR field
     * @deprecated use MedColorPeer.med_color.COLOR constant
     */
    public static String getMedColor_Color()
    {
        return "med_color.COLOR";
    }
  
    /**
     * med_color.LOGO
     * @return the column name for the LOGO field
     * @deprecated use MedColorPeer.med_color.LOGO constant
     */
    public static String getMedColor_Logo()
    {
        return "med_color.LOGO";
    }
  
    /**
     * med_color.IS_PRES
     * @return the column name for the IS_PRES field
     * @deprecated use MedColorPeer.med_color.IS_PRES constant
     */
    public static String getMedColor_IsPres()
    {
        return "med_color.IS_PRES";
    }
  
    /**
     * med_color.IS_WARN
     * @return the column name for the IS_WARN field
     * @deprecated use MedColorPeer.med_color.IS_WARN constant
     */
    public static String getMedColor_IsWarn()
    {
        return "med_color.IS_WARN";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("med_color");
        TableMap tMap = dbMap.getTable("med_color");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("med_color.COLOR", "");
                          tMap.addColumn("med_color.LOGO", "");
                          tMap.addColumn("med_color.IS_PRES", Boolean.TRUE);
                          tMap.addColumn("med_color.IS_WARN", Boolean.TRUE);
          }
}
