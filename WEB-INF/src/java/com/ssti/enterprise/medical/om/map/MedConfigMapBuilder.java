package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class MedConfigMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.MedConfigMapBuilder";

    /**
     * Item
     * @deprecated use MedConfigPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "med_config";
    }

  
    /**
     * med_config.MED_CONFIG_ID
     * @return the column name for the MED_CONFIG_ID field
     * @deprecated use MedConfigPeer.med_config.MED_CONFIG_ID constant
     */
    public static String getMedConfig_MedConfigId()
    {
        return "med_config.MED_CONFIG_ID";
    }
  
    /**
     * med_config.PHARMACIST_NAME
     * @return the column name for the PHARMACIST_NAME field
     * @deprecated use MedConfigPeer.med_config.PHARMACIST_NAME constant
     */
    public static String getMedConfig_PharmacistName()
    {
        return "med_config.PHARMACIST_NAME";
    }
  
    /**
     * med_config.LICENSE_NO
     * @return the column name for the LICENSE_NO field
     * @deprecated use MedConfigPeer.med_config.LICENSE_NO constant
     */
    public static String getMedConfig_LicenseNo()
    {
        return "med_config.LICENSE_NO";
    }
  
    /**
     * med_config.WORK_LICENSE_NO
     * @return the column name for the WORK_LICENSE_NO field
     * @deprecated use MedConfigPeer.med_config.WORK_LICENSE_NO constant
     */
    public static String getMedConfig_WorkLicenseNo()
    {
        return "med_config.WORK_LICENSE_NO";
    }
  
    /**
     * med_config.PRES_NO_FORMAT
     * @return the column name for the PRES_NO_FORMAT field
     * @deprecated use MedConfigPeer.med_config.PRES_NO_FORMAT constant
     */
    public static String getMedConfig_PresNoFormat()
    {
        return "med_config.PRES_NO_FORMAT";
    }
  
    /**
     * med_config.DEFAULT_RFEE
     * @return the column name for the DEFAULT_RFEE field
     * @deprecated use MedConfigPeer.med_config.DEFAULT_RFEE constant
     */
    public static String getMedConfig_DefaultRfee()
    {
        return "med_config.DEFAULT_RFEE";
    }
  
    /**
     * med_config.DEFAULT_PACKAGING
     * @return the column name for the DEFAULT_PACKAGING field
     * @deprecated use MedConfigPeer.med_config.DEFAULT_PACKAGING constant
     */
    public static String getMedConfig_DefaultPackaging()
    {
        return "med_config.DEFAULT_PACKAGING";
    }
  
    /**
     * med_config.RFEE_EDITABLE
     * @return the column name for the RFEE_EDITABLE field
     * @deprecated use MedConfigPeer.med_config.RFEE_EDITABLE constant
     */
    public static String getMedConfig_RfeeEditable()
    {
        return "med_config.RFEE_EDITABLE";
    }
  
    /**
     * med_config.RFEE_MULTIPLIED
     * @return the column name for the RFEE_MULTIPLIED field
     * @deprecated use MedConfigPeer.med_config.RFEE_MULTIPLIED constant
     */
    public static String getMedConfig_RfeeMultiplied()
    {
        return "med_config.RFEE_MULTIPLIED";
    }
  
    /**
     * med_config.RFEE_MIN_AMOUNT
     * @return the column name for the RFEE_MIN_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.RFEE_MIN_AMOUNT constant
     */
    public static String getMedConfig_RfeeMinAmount()
    {
        return "med_config.RFEE_MIN_AMOUNT";
    }
  
    /**
     * med_config.RFEE_MAX_AMOUNT
     * @return the column name for the RFEE_MAX_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.RFEE_MAX_AMOUNT constant
     */
    public static String getMedConfig_RfeeMaxAmount()
    {
        return "med_config.RFEE_MAX_AMOUNT";
    }
  
    /**
     * med_config.PACK_MIN_AMOUNT
     * @return the column name for the PACK_MIN_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.PACK_MIN_AMOUNT constant
     */
    public static String getMedConfig_PackMinAmount()
    {
        return "med_config.PACK_MIN_AMOUNT";
    }
  
    /**
     * med_config.PACK_MAX_AMOUNT
     * @return the column name for the PACK_MAX_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.PACK_MAX_AMOUNT constant
     */
    public static String getMedConfig_PackMaxAmount()
    {
        return "med_config.PACK_MAX_AMOUNT";
    }
  
    /**
     * med_config.POS_DEFAULT_RFEE
     * @return the column name for the POS_DEFAULT_RFEE field
     * @deprecated use MedConfigPeer.med_config.POS_DEFAULT_RFEE constant
     */
    public static String getMedConfig_PosDefaultRfee()
    {
        return "med_config.POS_DEFAULT_RFEE";
    }
  
    /**
     * med_config.POS_DEFAULT_PACK
     * @return the column name for the POS_DEFAULT_PACK field
     * @deprecated use MedConfigPeer.med_config.POS_DEFAULT_PACK constant
     */
    public static String getMedConfig_PosDefaultPack()
    {
        return "med_config.POS_DEFAULT_PACK";
    }
  
    /**
     * med_config.POS_RFEE_EDITABLE
     * @return the column name for the POS_RFEE_EDITABLE field
     * @deprecated use MedConfigPeer.med_config.POS_RFEE_EDITABLE constant
     */
    public static String getMedConfig_PosRfeeEditable()
    {
        return "med_config.POS_RFEE_EDITABLE";
    }
  
    /**
     * med_config.POS_RFEE_MULTIPLIED
     * @return the column name for the POS_RFEE_MULTIPLIED field
     * @deprecated use MedConfigPeer.med_config.POS_RFEE_MULTIPLIED constant
     */
    public static String getMedConfig_PosRfeeMultiplied()
    {
        return "med_config.POS_RFEE_MULTIPLIED";
    }
  
    /**
     * med_config.POS_RFEE_MIN_AMOUNT
     * @return the column name for the POS_RFEE_MIN_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.POS_RFEE_MIN_AMOUNT constant
     */
    public static String getMedConfig_PosRfeeMinAmount()
    {
        return "med_config.POS_RFEE_MIN_AMOUNT";
    }
  
    /**
     * med_config.POS_RFEE_MAX_AMOUNT
     * @return the column name for the POS_RFEE_MAX_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.POS_RFEE_MAX_AMOUNT constant
     */
    public static String getMedConfig_PosRfeeMaxAmount()
    {
        return "med_config.POS_RFEE_MAX_AMOUNT";
    }
  
    /**
     * med_config.POS_PACK_MIN_AMOUNT
     * @return the column name for the POS_PACK_MIN_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.POS_PACK_MIN_AMOUNT constant
     */
    public static String getMedConfig_PosPackMinAmount()
    {
        return "med_config.POS_PACK_MIN_AMOUNT";
    }
  
    /**
     * med_config.POS_PACK_MAX_AMOUNT
     * @return the column name for the POS_PACK_MAX_AMOUNT field
     * @deprecated use MedConfigPeer.med_config.POS_PACK_MAX_AMOUNT constant
     */
    public static String getMedConfig_PosPackMaxAmount()
    {
        return "med_config.POS_PACK_MAX_AMOUNT";
    }
  
    /**
     * med_config.RFEE_ITEM_CODE
     * @return the column name for the RFEE_ITEM_CODE field
     * @deprecated use MedConfigPeer.med_config.RFEE_ITEM_CODE constant
     */
    public static String getMedConfig_RfeeItemCode()
    {
        return "med_config.RFEE_ITEM_CODE";
    }
  
    /**
     * med_config.PACKAGING_ITEM_CODE
     * @return the column name for the PACKAGING_ITEM_CODE field
     * @deprecated use MedConfigPeer.med_config.PACKAGING_ITEM_CODE constant
     */
    public static String getMedConfig_PackagingItemCode()
    {
        return "med_config.PACKAGING_ITEM_CODE";
    }
  
    /**
     * med_config.PRES_ROOT_KATEGORI
     * @return the column name for the PRES_ROOT_KATEGORI field
     * @deprecated use MedConfigPeer.med_config.PRES_ROOT_KATEGORI constant
     */
    public static String getMedConfig_PresRootKategori()
    {
        return "med_config.PRES_ROOT_KATEGORI";
    }
  
    /**
     * med_config.PRES_ITEMS_GROUP
     * @return the column name for the PRES_ITEMS_GROUP field
     * @deprecated use MedConfigPeer.med_config.PRES_ITEMS_GROUP constant
     */
    public static String getMedConfig_PresItemsGroup()
    {
        return "med_config.PRES_ITEMS_GROUP";
    }
  
    /**
     * med_config.GEN_ITEMS_GROUP
     * @return the column name for the GEN_ITEMS_GROUP field
     * @deprecated use MedConfigPeer.med_config.GEN_ITEMS_GROUP constant
     */
    public static String getMedConfig_GenItemsGroup()
    {
        return "med_config.GEN_ITEMS_GROUP";
    }
  
    /**
     * med_config.PRES_ITEMS_MARGIN
     * @return the column name for the PRES_ITEMS_MARGIN field
     * @deprecated use MedConfigPeer.med_config.PRES_ITEMS_MARGIN constant
     */
    public static String getMedConfig_PresItemsMargin()
    {
        return "med_config.PRES_ITEMS_MARGIN";
    }
  
    /**
     * med_config.GEN_ITEMS_MARGIN
     * @return the column name for the GEN_ITEMS_MARGIN field
     * @deprecated use MedConfigPeer.med_config.GEN_ITEMS_MARGIN constant
     */
    public static String getMedConfig_GenItemsMargin()
    {
        return "med_config.GEN_ITEMS_MARGIN";
    }
  
    /**
     * med_config.GEN_USE_PRES_MARGIN
     * @return the column name for the GEN_USE_PRES_MARGIN field
     * @deprecated use MedConfigPeer.med_config.GEN_USE_PRES_MARGIN constant
     */
    public static String getMedConfig_GenUsePresMargin()
    {
        return "med_config.GEN_USE_PRES_MARGIN";
    }
  
    /**
     * med_config.POS_COMMA_SCALE
     * @return the column name for the POS_COMMA_SCALE field
     * @deprecated use MedConfigPeer.med_config.POS_COMMA_SCALE constant
     */
    public static String getMedConfig_PosCommaScale()
    {
        return "med_config.POS_COMMA_SCALE";
    }
  
    /**
     * med_config.ROUNDING_SCALE
     * @return the column name for the ROUNDING_SCALE field
     * @deprecated use MedConfigPeer.med_config.ROUNDING_SCALE constant
     */
    public static String getMedConfig_RoundingScale()
    {
        return "med_config.ROUNDING_SCALE";
    }
  
    /**
     * med_config.ROUNDING_MODE
     * @return the column name for the ROUNDING_MODE field
     * @deprecated use MedConfigPeer.med_config.ROUNDING_MODE constant
     */
    public static String getMedConfig_RoundingMode()
    {
        return "med_config.ROUNDING_MODE";
    }
  
    /**
     * med_config.ROUNDING_ITEM_CODE
     * @return the column name for the ROUNDING_ITEM_CODE field
     * @deprecated use MedConfigPeer.med_config.ROUNDING_ITEM_CODE constant
     */
    public static String getMedConfig_RoundingItemCode()
    {
        return "med_config.ROUNDING_ITEM_CODE";
    }
  
    /**
     * med_config.POWDER_ITEM_CODE
     * @return the column name for the POWDER_ITEM_CODE field
     * @deprecated use MedConfigPeer.med_config.POWDER_ITEM_CODE constant
     */
    public static String getMedConfig_PowderItemCode()
    {
        return "med_config.POWDER_ITEM_CODE";
    }
  
    /**
     * med_config.WARNING_ITEMS
     * @return the column name for the WARNING_ITEMS field
     * @deprecated use MedConfigPeer.med_config.WARNING_ITEMS constant
     */
    public static String getMedConfig_WarningItems()
    {
        return "med_config.WARNING_ITEMS";
    }
  
    /**
     * med_config.INSURANCE_CTYPE
     * @return the column name for the INSURANCE_CTYPE field
     * @deprecated use MedConfigPeer.med_config.INSURANCE_CTYPE constant
     */
    public static String getMedConfig_InsuranceCtype()
    {
        return "med_config.INSURANCE_CTYPE";
    }
  
    /**
     * med_config.INSURANCE_PTYPE
     * @return the column name for the INSURANCE_PTYPE field
     * @deprecated use MedConfigPeer.med_config.INSURANCE_PTYPE constant
     */
    public static String getMedConfig_InsurancePtype()
    {
        return "med_config.INSURANCE_PTYPE";
    }
  
    /**
     * med_config.ALERT_PRESCRIPTION
     * @return the column name for the ALERT_PRESCRIPTION field
     * @deprecated use MedConfigPeer.med_config.ALERT_PRESCRIPTION constant
     */
    public static String getMedConfig_AlertPrescription()
    {
        return "med_config.ALERT_PRESCRIPTION";
    }
  
    /**
     * med_config.ALERT_HOSTS
     * @return the column name for the ALERT_HOSTS field
     * @deprecated use MedConfigPeer.med_config.ALERT_HOSTS constant
     */
    public static String getMedConfig_AlertHosts()
    {
        return "med_config.ALERT_HOSTS";
    }
  
    /**
     * med_config.REG_NO_FORMAT
     * @return the column name for the REG_NO_FORMAT field
     * @deprecated use MedConfigPeer.med_config.REG_NO_FORMAT constant
     */
    public static String getMedConfig_RegNoFormat()
    {
        return "med_config.REG_NO_FORMAT";
    }
  
    /**
     * med_config.REC_NO_FORMAT
     * @return the column name for the REC_NO_FORMAT field
     * @deprecated use MedConfigPeer.med_config.REC_NO_FORMAT constant
     */
    public static String getMedConfig_RecNoFormat()
    {
        return "med_config.REC_NO_FORMAT";
    }
  
    /**
     * med_config.LAB_NO_FORMAT
     * @return the column name for the LAB_NO_FORMAT field
     * @deprecated use MedConfigPeer.med_config.LAB_NO_FORMAT constant
     */
    public static String getMedConfig_LabNoFormat()
    {
        return "med_config.LAB_NO_FORMAT";
    }
  
    /**
     * med_config.CL_CONFIG_FILE
     * @return the column name for the CL_CONFIG_FILE field
     * @deprecated use MedConfigPeer.med_config.CL_CONFIG_FILE constant
     */
    public static String getMedConfig_ClConfigFile()
    {
        return "med_config.CL_CONFIG_FILE";
    }
  
    /**
     * med_config.CL_MENU_FILE
     * @return the column name for the CL_MENU_FILE field
     * @deprecated use MedConfigPeer.med_config.CL_MENU_FILE constant
     */
    public static String getMedConfig_ClMenuFile()
    {
        return "med_config.CL_MENU_FILE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("med_config");
        TableMap tMap = dbMap.getTable("med_config");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("med_config.MED_CONFIG_ID", "");
                          tMap.addColumn("med_config.PHARMACIST_NAME", "");
                          tMap.addColumn("med_config.LICENSE_NO", "");
                          tMap.addColumn("med_config.WORK_LICENSE_NO", "");
                          tMap.addColumn("med_config.PRES_NO_FORMAT", "");
                          tMap.addColumn("med_config.DEFAULT_RFEE", "");
                          tMap.addColumn("med_config.DEFAULT_PACKAGING", "");
                          tMap.addColumn("med_config.RFEE_EDITABLE", Boolean.TRUE);
                          tMap.addColumn("med_config.RFEE_MULTIPLIED", Boolean.TRUE);
                            tMap.addColumn("med_config.RFEE_MIN_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_config.RFEE_MAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_config.PACK_MIN_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_config.PACK_MAX_AMOUNT", bd_ZERO);
                          tMap.addColumn("med_config.POS_DEFAULT_RFEE", "");
                          tMap.addColumn("med_config.POS_DEFAULT_PACK", "");
                          tMap.addColumn("med_config.POS_RFEE_EDITABLE", Boolean.TRUE);
                          tMap.addColumn("med_config.POS_RFEE_MULTIPLIED", Boolean.TRUE);
                            tMap.addColumn("med_config.POS_RFEE_MIN_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_config.POS_RFEE_MAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_config.POS_PACK_MIN_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_config.POS_PACK_MAX_AMOUNT", bd_ZERO);
                          tMap.addColumn("med_config.RFEE_ITEM_CODE", "");
                          tMap.addColumn("med_config.PACKAGING_ITEM_CODE", "");
                          tMap.addColumn("med_config.PRES_ROOT_KATEGORI", "");
                          tMap.addColumn("med_config.PRES_ITEMS_GROUP", "");
                          tMap.addColumn("med_config.GEN_ITEMS_GROUP", "");
                          tMap.addColumn("med_config.PRES_ITEMS_MARGIN", "");
                          tMap.addColumn("med_config.GEN_ITEMS_MARGIN", "");
                          tMap.addColumn("med_config.GEN_USE_PRES_MARGIN", Boolean.TRUE);
                            tMap.addColumn("med_config.POS_COMMA_SCALE", Integer.valueOf(0));
                            tMap.addColumn("med_config.ROUNDING_SCALE", Integer.valueOf(0));
                            tMap.addColumn("med_config.ROUNDING_MODE", Integer.valueOf(0));
                          tMap.addColumn("med_config.ROUNDING_ITEM_CODE", "");
                          tMap.addColumn("med_config.POWDER_ITEM_CODE", "");
                          tMap.addColumn("med_config.WARNING_ITEMS", "");
                          tMap.addColumn("med_config.INSURANCE_CTYPE", "");
                          tMap.addColumn("med_config.INSURANCE_PTYPE", "");
                          tMap.addColumn("med_config.ALERT_PRESCRIPTION", Boolean.TRUE);
                          tMap.addColumn("med_config.ALERT_HOSTS", "");
                          tMap.addColumn("med_config.REG_NO_FORMAT", "");
                          tMap.addColumn("med_config.REC_NO_FORMAT", "");
                          tMap.addColumn("med_config.LAB_NO_FORMAT", "");
                          tMap.addColumn("med_config.CL_CONFIG_FILE", "");
                          tMap.addColumn("med_config.CL_MENU_FILE", "");
          }
}
