package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class MedOrderDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.MedOrderDetailMapBuilder";

    /**
     * Item
     * @deprecated use MedOrderDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "med_order_detail";
    }

  
    /**
     * med_order_detail.MED_ORDER_DETAIL_ID
     * @return the column name for the MED_ORDER_DETAIL_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.MED_ORDER_DETAIL_ID constant
     */
    public static String getMedOrderDetail_MedOrderDetailId()
    {
        return "med_order_detail.MED_ORDER_DETAIL_ID";
    }
  
    /**
     * med_order_detail.MED_ORDER_ID
     * @return the column name for the MED_ORDER_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.MED_ORDER_ID constant
     */
    public static String getMedOrderDetail_MedOrderId()
    {
        return "med_order_detail.MED_ORDER_ID";
    }
  
    /**
     * med_order_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use MedOrderDetailPeer.med_order_detail.INDEX_NO constant
     */
    public static String getMedOrderDetail_IndexNo()
    {
        return "med_order_detail.INDEX_NO";
    }
  
    /**
     * med_order_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.ITEM_ID constant
     */
    public static String getMedOrderDetail_ItemId()
    {
        return "med_order_detail.ITEM_ID";
    }
  
    /**
     * med_order_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.ITEM_CODE constant
     */
    public static String getMedOrderDetail_ItemCode()
    {
        return "med_order_detail.ITEM_CODE";
    }
  
    /**
     * med_order_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use MedOrderDetailPeer.med_order_detail.ITEM_NAME constant
     */
    public static String getMedOrderDetail_ItemName()
    {
        return "med_order_detail.ITEM_NAME";
    }
  
    /**
     * med_order_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use MedOrderDetailPeer.med_order_detail.DESCRIPTION constant
     */
    public static String getMedOrderDetail_Description()
    {
        return "med_order_detail.DESCRIPTION";
    }
  
    /**
     * med_order_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.UNIT_ID constant
     */
    public static String getMedOrderDetail_UnitId()
    {
        return "med_order_detail.UNIT_ID";
    }
  
    /**
     * med_order_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.UNIT_CODE constant
     */
    public static String getMedOrderDetail_UnitCode()
    {
        return "med_order_detail.UNIT_CODE";
    }
  
    /**
     * med_order_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use MedOrderDetailPeer.med_order_detail.QTY constant
     */
    public static String getMedOrderDetail_Qty()
    {
        return "med_order_detail.QTY";
    }
  
    /**
     * med_order_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.QTY_BASE constant
     */
    public static String getMedOrderDetail_QtyBase()
    {
        return "med_order_detail.QTY_BASE";
    }
  
    /**
     * med_order_detail.RETURNED_QTY
     * @return the column name for the RETURNED_QTY field
     * @deprecated use MedOrderDetailPeer.med_order_detail.RETURNED_QTY constant
     */
    public static String getMedOrderDetail_ReturnedQty()
    {
        return "med_order_detail.RETURNED_QTY";
    }
  
    /**
     * med_order_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.ITEM_PRICE constant
     */
    public static String getMedOrderDetail_ItemPrice()
    {
        return "med_order_detail.ITEM_PRICE";
    }
  
    /**
     * med_order_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.TAX_ID constant
     */
    public static String getMedOrderDetail_TaxId()
    {
        return "med_order_detail.TAX_ID";
    }
  
    /**
     * med_order_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use MedOrderDetailPeer.med_order_detail.TAX_AMOUNT constant
     */
    public static String getMedOrderDetail_TaxAmount()
    {
        return "med_order_detail.TAX_AMOUNT";
    }
  
    /**
     * med_order_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use MedOrderDetailPeer.med_order_detail.SUB_TOTAL_TAX constant
     */
    public static String getMedOrderDetail_SubTotalTax()
    {
        return "med_order_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * med_order_detail.DISCOUNT_ID
     * @return the column name for the DISCOUNT_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.DISCOUNT_ID constant
     */
    public static String getMedOrderDetail_DiscountId()
    {
        return "med_order_detail.DISCOUNT_ID";
    }
  
    /**
     * med_order_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use MedOrderDetailPeer.med_order_detail.DISCOUNT constant
     */
    public static String getMedOrderDetail_Discount()
    {
        return "med_order_detail.DISCOUNT";
    }
  
    /**
     * med_order_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use MedOrderDetailPeer.med_order_detail.SUB_TOTAL_DISC constant
     */
    public static String getMedOrderDetail_SubTotalDisc()
    {
        return "med_order_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * med_order_detail.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use MedOrderDetailPeer.med_order_detail.ITEM_COST constant
     */
    public static String getMedOrderDetail_ItemCost()
    {
        return "med_order_detail.ITEM_COST";
    }
  
    /**
     * med_order_detail.SUB_TOTAL_COST
     * @return the column name for the SUB_TOTAL_COST field
     * @deprecated use MedOrderDetailPeer.med_order_detail.SUB_TOTAL_COST constant
     */
    public static String getMedOrderDetail_SubTotalCost()
    {
        return "med_order_detail.SUB_TOTAL_COST";
    }
  
    /**
     * med_order_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use MedOrderDetailPeer.med_order_detail.SUB_TOTAL constant
     */
    public static String getMedOrderDetail_SubTotal()
    {
        return "med_order_detail.SUB_TOTAL";
    }
  
    /**
     * med_order_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.PROJECT_ID constant
     */
    public static String getMedOrderDetail_ProjectId()
    {
        return "med_order_detail.PROJECT_ID";
    }
  
    /**
     * med_order_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.DEPARTMENT_ID constant
     */
    public static String getMedOrderDetail_DepartmentId()
    {
        return "med_order_detail.DEPARTMENT_ID";
    }
  
    /**
     * med_order_detail.ITEM_TYPE
     * @return the column name for the ITEM_TYPE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.ITEM_TYPE constant
     */
    public static String getMedOrderDetail_ItemType()
    {
        return "med_order_detail.ITEM_TYPE";
    }
  
    /**
     * med_order_detail.TX_TYPE
     * @return the column name for the TX_TYPE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.TX_TYPE constant
     */
    public static String getMedOrderDetail_TxType()
    {
        return "med_order_detail.TX_TYPE";
    }
  
    /**
     * med_order_detail.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.PARENT_ID constant
     */
    public static String getMedOrderDetail_ParentId()
    {
        return "med_order_detail.PARENT_ID";
    }
  
    /**
     * med_order_detail.PRESCRIPTION_DETAIL_ID
     * @return the column name for the PRESCRIPTION_DETAIL_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.PRESCRIPTION_DETAIL_ID constant
     */
    public static String getMedOrderDetail_PrescriptionDetailId()
    {
        return "med_order_detail.PRESCRIPTION_DETAIL_ID";
    }
  
    /**
     * med_order_detail.PRESCRIPTION_ID
     * @return the column name for the PRESCRIPTION_ID field
     * @deprecated use MedOrderDetailPeer.med_order_detail.PRESCRIPTION_ID constant
     */
    public static String getMedOrderDetail_PrescriptionId()
    {
        return "med_order_detail.PRESCRIPTION_ID";
    }
  
    /**
     * med_order_detail.RFEE
     * @return the column name for the RFEE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.RFEE constant
     */
    public static String getMedOrderDetail_Rfee()
    {
        return "med_order_detail.RFEE";
    }
  
    /**
     * med_order_detail.PACKAGING
     * @return the column name for the PACKAGING field
     * @deprecated use MedOrderDetailPeer.med_order_detail.PACKAGING constant
     */
    public static String getMedOrderDetail_Packaging()
    {
        return "med_order_detail.PACKAGING";
    }
  
    /**
     * med_order_detail.TOTAL
     * @return the column name for the TOTAL field
     * @deprecated use MedOrderDetailPeer.med_order_detail.TOTAL constant
     */
    public static String getMedOrderDetail_Total()
    {
        return "med_order_detail.TOTAL";
    }
  
    /**
     * med_order_detail.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use MedOrderDetailPeer.med_order_detail.BATCH_NO constant
     */
    public static String getMedOrderDetail_BatchNo()
    {
        return "med_order_detail.BATCH_NO";
    }
  
    /**
     * med_order_detail.DOSAGE
     * @return the column name for the DOSAGE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.DOSAGE constant
     */
    public static String getMedOrderDetail_Dosage()
    {
        return "med_order_detail.DOSAGE";
    }
  
    /**
     * med_order_detail.BEFORE_AFTER
     * @return the column name for the BEFORE_AFTER field
     * @deprecated use MedOrderDetailPeer.med_order_detail.BEFORE_AFTER constant
     */
    public static String getMedOrderDetail_BeforeAfter()
    {
        return "med_order_detail.BEFORE_AFTER";
    }
  
    /**
     * med_order_detail.INSTRUCTION
     * @return the column name for the INSTRUCTION field
     * @deprecated use MedOrderDetailPeer.med_order_detail.INSTRUCTION constant
     */
    public static String getMedOrderDetail_Instruction()
    {
        return "med_order_detail.INSTRUCTION";
    }
  
    /**
     * med_order_detail.PRES_FILE
     * @return the column name for the PRES_FILE field
     * @deprecated use MedOrderDetailPeer.med_order_detail.PRES_FILE constant
     */
    public static String getMedOrderDetail_PresFile()
    {
        return "med_order_detail.PRES_FILE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("med_order_detail");
        TableMap tMap = dbMap.getTable("med_order_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("med_order_detail.MED_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "med_order_detail.MED_ORDER_ID", "" , "med_order" ,
                "med_order_id");
                            tMap.addColumn("med_order_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("med_order_detail.ITEM_ID", "");
                          tMap.addColumn("med_order_detail.ITEM_CODE", "");
                          tMap.addColumn("med_order_detail.ITEM_NAME", "");
                          tMap.addColumn("med_order_detail.DESCRIPTION", "");
                          tMap.addColumn("med_order_detail.UNIT_ID", "");
                          tMap.addColumn("med_order_detail.UNIT_CODE", "");
                            tMap.addColumn("med_order_detail.QTY", bd_ZERO);
                            tMap.addColumn("med_order_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("med_order_detail.RETURNED_QTY", bd_ZERO);
                            tMap.addColumn("med_order_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("med_order_detail.TAX_ID", "");
                            tMap.addColumn("med_order_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_order_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("med_order_detail.DISCOUNT_ID", "");
                          tMap.addColumn("med_order_detail.DISCOUNT", "");
                            tMap.addColumn("med_order_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("med_order_detail.ITEM_COST", bd_ZERO);
                            tMap.addColumn("med_order_detail.SUB_TOTAL_COST", bd_ZERO);
                            tMap.addColumn("med_order_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("med_order_detail.PROJECT_ID", "");
                          tMap.addColumn("med_order_detail.DEPARTMENT_ID", "");
                            tMap.addColumn("med_order_detail.ITEM_TYPE", Integer.valueOf(0));
                            tMap.addColumn("med_order_detail.TX_TYPE", Integer.valueOf(0));
                          tMap.addColumn("med_order_detail.PARENT_ID", "");
                          tMap.addColumn("med_order_detail.PRESCRIPTION_DETAIL_ID", "");
                          tMap.addColumn("med_order_detail.PRESCRIPTION_ID", "");
                          tMap.addColumn("med_order_detail.RFEE", "");
                          tMap.addColumn("med_order_detail.PACKAGING", "");
                            tMap.addColumn("med_order_detail.TOTAL", bd_ZERO);
                          tMap.addColumn("med_order_detail.BATCH_NO", "");
                          tMap.addColumn("med_order_detail.DOSAGE", "");
                          tMap.addColumn("med_order_detail.BEFORE_AFTER", "");
                          tMap.addColumn("med_order_detail.INSTRUCTION", "");
                          tMap.addColumn("med_order_detail.PRES_FILE", "");
          }
}
