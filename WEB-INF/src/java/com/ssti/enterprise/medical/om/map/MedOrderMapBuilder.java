package com.ssti.enterprise.medical.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class MedOrderMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.MedOrderMapBuilder";

    /**
     * Item
     * @deprecated use MedOrderPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "med_order";
    }

  
    /**
     * med_order.MED_ORDER_ID
     * @return the column name for the MED_ORDER_ID field
     * @deprecated use MedOrderPeer.med_order.MED_ORDER_ID constant
     */
    public static String getMedOrder_MedOrderId()
    {
        return "med_order.MED_ORDER_ID";
    }
  
    /**
     * med_order.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use MedOrderPeer.med_order.LOCATION_ID constant
     */
    public static String getMedOrder_LocationId()
    {
        return "med_order.LOCATION_ID";
    }
  
    /**
     * med_order.STATUS
     * @return the column name for the STATUS field
     * @deprecated use MedOrderPeer.med_order.STATUS constant
     */
    public static String getMedOrder_Status()
    {
        return "med_order.STATUS";
    }
  
    /**
     * med_order.ORDER_NO
     * @return the column name for the ORDER_NO field
     * @deprecated use MedOrderPeer.med_order.ORDER_NO constant
     */
    public static String getMedOrder_OrderNo()
    {
        return "med_order.ORDER_NO";
    }
  
    /**
     * med_order.INVOICE_NO
     * @return the column name for the INVOICE_NO field
     * @deprecated use MedOrderPeer.med_order.INVOICE_NO constant
     */
    public static String getMedOrder_InvoiceNo()
    {
        return "med_order.INVOICE_NO";
    }
  
    /**
     * med_order.DELIVERY_NO
     * @return the column name for the DELIVERY_NO field
     * @deprecated use MedOrderPeer.med_order.DELIVERY_NO constant
     */
    public static String getMedOrder_DeliveryNo()
    {
        return "med_order.DELIVERY_NO";
    }
  
    /**
     * med_order.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use MedOrderPeer.med_order.TRANSACTION_DATE constant
     */
    public static String getMedOrder_TransactionDate()
    {
        return "med_order.TRANSACTION_DATE";
    }
  
    /**
     * med_order.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use MedOrderPeer.med_order.DUE_DATE constant
     */
    public static String getMedOrder_DueDate()
    {
        return "med_order.DUE_DATE";
    }
  
    /**
     * med_order.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use MedOrderPeer.med_order.CUSTOMER_ID constant
     */
    public static String getMedOrder_CustomerId()
    {
        return "med_order.CUSTOMER_ID";
    }
  
    /**
     * med_order.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use MedOrderPeer.med_order.CUSTOMER_NAME constant
     */
    public static String getMedOrder_CustomerName()
    {
        return "med_order.CUSTOMER_NAME";
    }
  
    /**
     * med_order.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use MedOrderPeer.med_order.TOTAL_QTY constant
     */
    public static String getMedOrder_TotalQty()
    {
        return "med_order.TOTAL_QTY";
    }
  
    /**
     * med_order.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use MedOrderPeer.med_order.TOTAL_AMOUNT constant
     */
    public static String getMedOrder_TotalAmount()
    {
        return "med_order.TOTAL_AMOUNT";
    }
  
    /**
     * med_order.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use MedOrderPeer.med_order.TOTAL_DISCOUNT_PCT constant
     */
    public static String getMedOrder_TotalDiscountPct()
    {
        return "med_order.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * med_order.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use MedOrderPeer.med_order.TOTAL_DISCOUNT constant
     */
    public static String getMedOrder_TotalDiscount()
    {
        return "med_order.TOTAL_DISCOUNT";
    }
  
    /**
     * med_order.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use MedOrderPeer.med_order.TOTAL_TAX constant
     */
    public static String getMedOrder_TotalTax()
    {
        return "med_order.TOTAL_TAX";
    }
  
    /**
     * med_order.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use MedOrderPeer.med_order.COURIER_ID constant
     */
    public static String getMedOrder_CourierId()
    {
        return "med_order.COURIER_ID";
    }
  
    /**
     * med_order.SHIPPING_PRICE
     * @return the column name for the SHIPPING_PRICE field
     * @deprecated use MedOrderPeer.med_order.SHIPPING_PRICE constant
     */
    public static String getMedOrder_ShippingPrice()
    {
        return "med_order.SHIPPING_PRICE";
    }
  
    /**
     * med_order.TOTAL_EXPENSE
     * @return the column name for the TOTAL_EXPENSE field
     * @deprecated use MedOrderPeer.med_order.TOTAL_EXPENSE constant
     */
    public static String getMedOrder_TotalExpense()
    {
        return "med_order.TOTAL_EXPENSE";
    }
  
    /**
     * med_order.TOTAL_COST
     * @return the column name for the TOTAL_COST field
     * @deprecated use MedOrderPeer.med_order.TOTAL_COST constant
     */
    public static String getMedOrder_TotalCost()
    {
        return "med_order.TOTAL_COST";
    }
  
    /**
     * med_order.SALES_ID
     * @return the column name for the SALES_ID field
     * @deprecated use MedOrderPeer.med_order.SALES_ID constant
     */
    public static String getMedOrder_SalesId()
    {
        return "med_order.SALES_ID";
    }
  
    /**
     * med_order.CASHIER_NAME
     * @return the column name for the CASHIER_NAME field
     * @deprecated use MedOrderPeer.med_order.CASHIER_NAME constant
     */
    public static String getMedOrder_CashierName()
    {
        return "med_order.CASHIER_NAME";
    }
  
    /**
     * med_order.REMARK
     * @return the column name for the REMARK field
     * @deprecated use MedOrderPeer.med_order.REMARK constant
     */
    public static String getMedOrder_Remark()
    {
        return "med_order.REMARK";
    }
  
    /**
     * med_order.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use MedOrderPeer.med_order.PAYMENT_TYPE_ID constant
     */
    public static String getMedOrder_PaymentTypeId()
    {
        return "med_order.PAYMENT_TYPE_ID";
    }
  
    /**
     * med_order.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use MedOrderPeer.med_order.PAYMENT_TERM_ID constant
     */
    public static String getMedOrder_PaymentTermId()
    {
        return "med_order.PAYMENT_TERM_ID";
    }
  
    /**
     * med_order.PRINT_TIMES
     * @return the column name for the PRINT_TIMES field
     * @deprecated use MedOrderPeer.med_order.PRINT_TIMES constant
     */
    public static String getMedOrder_PrintTimes()
    {
        return "med_order.PRINT_TIMES";
    }
  
    /**
     * med_order.PAID_AMOUNT
     * @return the column name for the PAID_AMOUNT field
     * @deprecated use MedOrderPeer.med_order.PAID_AMOUNT constant
     */
    public static String getMedOrder_PaidAmount()
    {
        return "med_order.PAID_AMOUNT";
    }
  
    /**
     * med_order.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use MedOrderPeer.med_order.PAYMENT_AMOUNT constant
     */
    public static String getMedOrder_PaymentAmount()
    {
        return "med_order.PAYMENT_AMOUNT";
    }
  
    /**
     * med_order.CHANGE_AMOUNT
     * @return the column name for the CHANGE_AMOUNT field
     * @deprecated use MedOrderPeer.med_order.CHANGE_AMOUNT constant
     */
    public static String getMedOrder_ChangeAmount()
    {
        return "med_order.CHANGE_AMOUNT";
    }
  
    /**
     * med_order.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use MedOrderPeer.med_order.CURRENCY_ID constant
     */
    public static String getMedOrder_CurrencyId()
    {
        return "med_order.CURRENCY_ID";
    }
  
    /**
     * med_order.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use MedOrderPeer.med_order.CURRENCY_RATE constant
     */
    public static String getMedOrder_CurrencyRate()
    {
        return "med_order.CURRENCY_RATE";
    }
  
    /**
     * med_order.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use MedOrderPeer.med_order.SHIP_TO constant
     */
    public static String getMedOrder_ShipTo()
    {
        return "med_order.SHIP_TO";
    }
  
    /**
     * med_order.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use MedOrderPeer.med_order.FOB_ID constant
     */
    public static String getMedOrder_FobId()
    {
        return "med_order.FOB_ID";
    }
  
    /**
     * med_order.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use MedOrderPeer.med_order.IS_TAXABLE constant
     */
    public static String getMedOrder_IsTaxable()
    {
        return "med_order.IS_TAXABLE";
    }
  
    /**
     * med_order.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use MedOrderPeer.med_order.IS_INCLUSIVE_TAX constant
     */
    public static String getMedOrder_IsInclusiveTax()
    {
        return "med_order.IS_INCLUSIVE_TAX";
    }
  
    /**
     * med_order.PAYMENT_DATE
     * @return the column name for the PAYMENT_DATE field
     * @deprecated use MedOrderPeer.med_order.PAYMENT_DATE constant
     */
    public static String getMedOrder_PaymentDate()
    {
        return "med_order.PAYMENT_DATE";
    }
  
    /**
     * med_order.PAYMENT_STATUS
     * @return the column name for the PAYMENT_STATUS field
     * @deprecated use MedOrderPeer.med_order.PAYMENT_STATUS constant
     */
    public static String getMedOrder_PaymentStatus()
    {
        return "med_order.PAYMENT_STATUS";
    }
  
    /**
     * med_order.IS_INCLUSIVE_FREIGHT
     * @return the column name for the IS_INCLUSIVE_FREIGHT field
     * @deprecated use MedOrderPeer.med_order.IS_INCLUSIVE_FREIGHT constant
     */
    public static String getMedOrder_IsInclusiveFreight()
    {
        return "med_order.IS_INCLUSIVE_FREIGHT";
    }
  
    /**
     * med_order.FREIGHT_ACCOUNT_ID
     * @return the column name for the FREIGHT_ACCOUNT_ID field
     * @deprecated use MedOrderPeer.med_order.FREIGHT_ACCOUNT_ID constant
     */
    public static String getMedOrder_FreightAccountId()
    {
        return "med_order.FREIGHT_ACCOUNT_ID";
    }
  
    /**
     * med_order.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use MedOrderPeer.med_order.CANCEL_BY constant
     */
    public static String getMedOrder_CancelBy()
    {
        return "med_order.CANCEL_BY";
    }
  
    /**
     * med_order.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use MedOrderPeer.med_order.CANCEL_DATE constant
     */
    public static String getMedOrder_CancelDate()
    {
        return "med_order.CANCEL_DATE";
    }
  
    /**
     * med_order.SALES_TRANSACTION_ID
     * @return the column name for the SALES_TRANSACTION_ID field
     * @deprecated use MedOrderPeer.med_order.SALES_TRANSACTION_ID constant
     */
    public static String getMedOrder_SalesTransactionId()
    {
        return "med_order.SALES_TRANSACTION_ID";
    }
  
    /**
     * med_order.TOTAL_FEE
     * @return the column name for the TOTAL_FEE field
     * @deprecated use MedOrderPeer.med_order.TOTAL_FEE constant
     */
    public static String getMedOrder_TotalFee()
    {
        return "med_order.TOTAL_FEE";
    }
  
    /**
     * med_order.ROUNDING_AMOUNT
     * @return the column name for the ROUNDING_AMOUNT field
     * @deprecated use MedOrderPeer.med_order.ROUNDING_AMOUNT constant
     */
    public static String getMedOrder_RoundingAmount()
    {
        return "med_order.ROUNDING_AMOUNT";
    }
  
    /**
     * med_order.DOCTOR_NAME
     * @return the column name for the DOCTOR_NAME field
     * @deprecated use MedOrderPeer.med_order.DOCTOR_NAME constant
     */
    public static String getMedOrder_DoctorName()
    {
        return "med_order.DOCTOR_NAME";
    }
  
    /**
     * med_order.PATIENT_NAME
     * @return the column name for the PATIENT_NAME field
     * @deprecated use MedOrderPeer.med_order.PATIENT_NAME constant
     */
    public static String getMedOrder_PatientName()
    {
        return "med_order.PATIENT_NAME";
    }
  
    /**
     * med_order.PATIENT_AGE
     * @return the column name for the PATIENT_AGE field
     * @deprecated use MedOrderPeer.med_order.PATIENT_AGE constant
     */
    public static String getMedOrder_PatientAge()
    {
        return "med_order.PATIENT_AGE";
    }
  
    /**
     * med_order.PATIENT_PHONE
     * @return the column name for the PATIENT_PHONE field
     * @deprecated use MedOrderPeer.med_order.PATIENT_PHONE constant
     */
    public static String getMedOrder_PatientPhone()
    {
        return "med_order.PATIENT_PHONE";
    }
  
    /**
     * med_order.PATIENT_ADDRESS
     * @return the column name for the PATIENT_ADDRESS field
     * @deprecated use MedOrderPeer.med_order.PATIENT_ADDRESS constant
     */
    public static String getMedOrder_PatientAddress()
    {
        return "med_order.PATIENT_ADDRESS";
    }
  
    /**
     * med_order.REGISTRATION_ID
     * @return the column name for the REGISTRATION_ID field
     * @deprecated use MedOrderPeer.med_order.REGISTRATION_ID constant
     */
    public static String getMedOrder_RegistrationId()
    {
        return "med_order.REGISTRATION_ID";
    }
  
    /**
     * med_order.INSURANCE_ID
     * @return the column name for the INSURANCE_ID field
     * @deprecated use MedOrderPeer.med_order.INSURANCE_ID constant
     */
    public static String getMedOrder_InsuranceId()
    {
        return "med_order.INSURANCE_ID";
    }
  
    /**
     * med_order.POLICY_NO
     * @return the column name for the POLICY_NO field
     * @deprecated use MedOrderPeer.med_order.POLICY_NO constant
     */
    public static String getMedOrder_PolicyNo()
    {
        return "med_order.POLICY_NO";
    }
  
    /**
     * med_order.POLICY_NAME
     * @return the column name for the POLICY_NAME field
     * @deprecated use MedOrderPeer.med_order.POLICY_NAME constant
     */
    public static String getMedOrder_PolicyName()
    {
        return "med_order.POLICY_NAME";
    }
  
    /**
     * med_order.IS_INSURANCE
     * @return the column name for the IS_INSURANCE field
     * @deprecated use MedOrderPeer.med_order.IS_INSURANCE constant
     */
    public static String getMedOrder_IsInsurance()
    {
        return "med_order.IS_INSURANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("med_order");
        TableMap tMap = dbMap.getTable("med_order");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("med_order.MED_ORDER_ID", "");
                          tMap.addColumn("med_order.LOCATION_ID", "");
                            tMap.addColumn("med_order.STATUS", Integer.valueOf(0));
                          tMap.addColumn("med_order.ORDER_NO", "");
                          tMap.addColumn("med_order.INVOICE_NO", "");
                          tMap.addColumn("med_order.DELIVERY_NO", "");
                          tMap.addColumn("med_order.TRANSACTION_DATE", new Date());
                          tMap.addColumn("med_order.DUE_DATE", new Date());
                          tMap.addColumn("med_order.CUSTOMER_ID", "");
                          tMap.addColumn("med_order.CUSTOMER_NAME", "");
                            tMap.addColumn("med_order.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("med_order.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("med_order.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("med_order.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("med_order.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("med_order.COURIER_ID", "");
                            tMap.addColumn("med_order.SHIPPING_PRICE", bd_ZERO);
                            tMap.addColumn("med_order.TOTAL_EXPENSE", bd_ZERO);
                            tMap.addColumn("med_order.TOTAL_COST", bd_ZERO);
                          tMap.addColumn("med_order.SALES_ID", "");
                          tMap.addColumn("med_order.CASHIER_NAME", "");
                          tMap.addColumn("med_order.REMARK", "");
                          tMap.addColumn("med_order.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("med_order.PAYMENT_TERM_ID", "");
                            tMap.addColumn("med_order.PRINT_TIMES", Integer.valueOf(0));
                            tMap.addColumn("med_order.PAID_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_order.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("med_order.CHANGE_AMOUNT", bd_ZERO);
                          tMap.addColumn("med_order.CURRENCY_ID", "");
                            tMap.addColumn("med_order.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("med_order.SHIP_TO", "");
                          tMap.addColumn("med_order.FOB_ID", "");
                          tMap.addColumn("med_order.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("med_order.IS_INCLUSIVE_TAX", Boolean.TRUE);
                          tMap.addColumn("med_order.PAYMENT_DATE", new Date());
                            tMap.addColumn("med_order.PAYMENT_STATUS", Integer.valueOf(0));
                          tMap.addColumn("med_order.IS_INCLUSIVE_FREIGHT", Boolean.TRUE);
                          tMap.addColumn("med_order.FREIGHT_ACCOUNT_ID", "");
                          tMap.addColumn("med_order.CANCEL_BY", "");
                          tMap.addColumn("med_order.CANCEL_DATE", new Date());
                          tMap.addColumn("med_order.SALES_TRANSACTION_ID", "");
                            tMap.addColumn("med_order.TOTAL_FEE", bd_ZERO);
                            tMap.addColumn("med_order.ROUNDING_AMOUNT", bd_ZERO);
                          tMap.addColumn("med_order.DOCTOR_NAME", "");
                          tMap.addColumn("med_order.PATIENT_NAME", "");
                          tMap.addColumn("med_order.PATIENT_AGE", "");
                          tMap.addColumn("med_order.PATIENT_PHONE", "");
                          tMap.addColumn("med_order.PATIENT_ADDRESS", "");
                          tMap.addColumn("med_order.REGISTRATION_ID", "");
                          tMap.addColumn("med_order.INSURANCE_ID", "");
                          tMap.addColumn("med_order.POLICY_NO", "");
                          tMap.addColumn("med_order.POLICY_NAME", "");
                          tMap.addColumn("med_order.IS_INSURANCE", Boolean.TRUE);
          }
}
