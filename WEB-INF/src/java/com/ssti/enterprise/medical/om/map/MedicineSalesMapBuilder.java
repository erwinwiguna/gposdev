package com.ssti.enterprise.medical.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class MedicineSalesMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.MedicineSalesMapBuilder";

    /**
     * Item
     * @deprecated use MedicineSalesPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "medicine_sales";
    }

  
    /**
     * medicine_sales.MEDICINE_SALES_ID
     * @return the column name for the MEDICINE_SALES_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.MEDICINE_SALES_ID constant
     */
    public static String getMedicineSales_MedicineSalesId()
    {
        return "medicine_sales.MEDICINE_SALES_ID";
    }
  
    /**
     * medicine_sales.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.LOCATION_ID constant
     */
    public static String getMedicineSales_LocationId()
    {
        return "medicine_sales.LOCATION_ID";
    }
  
    /**
     * medicine_sales.STATUS
     * @return the column name for the STATUS field
     * @deprecated use MedicineSalesPeer.medicine_sales.STATUS constant
     */
    public static String getMedicineSales_Status()
    {
        return "medicine_sales.STATUS";
    }
  
    /**
     * medicine_sales.INVOICE_NO
     * @return the column name for the INVOICE_NO field
     * @deprecated use MedicineSalesPeer.medicine_sales.INVOICE_NO constant
     */
    public static String getMedicineSales_InvoiceNo()
    {
        return "medicine_sales.INVOICE_NO";
    }
  
    /**
     * medicine_sales.DELIVERY_NO
     * @return the column name for the DELIVERY_NO field
     * @deprecated use MedicineSalesPeer.medicine_sales.DELIVERY_NO constant
     */
    public static String getMedicineSales_DeliveryNo()
    {
        return "medicine_sales.DELIVERY_NO";
    }
  
    /**
     * medicine_sales.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use MedicineSalesPeer.medicine_sales.TRANSACTION_DATE constant
     */
    public static String getMedicineSales_TransactionDate()
    {
        return "medicine_sales.TRANSACTION_DATE";
    }
  
    /**
     * medicine_sales.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use MedicineSalesPeer.medicine_sales.DUE_DATE constant
     */
    public static String getMedicineSales_DueDate()
    {
        return "medicine_sales.DUE_DATE";
    }
  
    /**
     * medicine_sales.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.CUSTOMER_ID constant
     */
    public static String getMedicineSales_CustomerId()
    {
        return "medicine_sales.CUSTOMER_ID";
    }
  
    /**
     * medicine_sales.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use MedicineSalesPeer.medicine_sales.CUSTOMER_NAME constant
     */
    public static String getMedicineSales_CustomerName()
    {
        return "medicine_sales.CUSTOMER_NAME";
    }
  
    /**
     * medicine_sales.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_QTY constant
     */
    public static String getMedicineSales_TotalQty()
    {
        return "medicine_sales.TOTAL_QTY";
    }
  
    /**
     * medicine_sales.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_AMOUNT constant
     */
    public static String getMedicineSales_TotalAmount()
    {
        return "medicine_sales.TOTAL_AMOUNT";
    }
  
    /**
     * medicine_sales.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_DISCOUNT_PCT constant
     */
    public static String getMedicineSales_TotalDiscountPct()
    {
        return "medicine_sales.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * medicine_sales.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_DISCOUNT constant
     */
    public static String getMedicineSales_TotalDiscount()
    {
        return "medicine_sales.TOTAL_DISCOUNT";
    }
  
    /**
     * medicine_sales.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_TAX constant
     */
    public static String getMedicineSales_TotalTax()
    {
        return "medicine_sales.TOTAL_TAX";
    }
  
    /**
     * medicine_sales.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.COURIER_ID constant
     */
    public static String getMedicineSales_CourierId()
    {
        return "medicine_sales.COURIER_ID";
    }
  
    /**
     * medicine_sales.SHIPPING_PRICE
     * @return the column name for the SHIPPING_PRICE field
     * @deprecated use MedicineSalesPeer.medicine_sales.SHIPPING_PRICE constant
     */
    public static String getMedicineSales_ShippingPrice()
    {
        return "medicine_sales.SHIPPING_PRICE";
    }
  
    /**
     * medicine_sales.TOTAL_EXPENSE
     * @return the column name for the TOTAL_EXPENSE field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_EXPENSE constant
     */
    public static String getMedicineSales_TotalExpense()
    {
        return "medicine_sales.TOTAL_EXPENSE";
    }
  
    /**
     * medicine_sales.TOTAL_COST
     * @return the column name for the TOTAL_COST field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_COST constant
     */
    public static String getMedicineSales_TotalCost()
    {
        return "medicine_sales.TOTAL_COST";
    }
  
    /**
     * medicine_sales.SALES_ID
     * @return the column name for the SALES_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.SALES_ID constant
     */
    public static String getMedicineSales_SalesId()
    {
        return "medicine_sales.SALES_ID";
    }
  
    /**
     * medicine_sales.CASHIER_NAME
     * @return the column name for the CASHIER_NAME field
     * @deprecated use MedicineSalesPeer.medicine_sales.CASHIER_NAME constant
     */
    public static String getMedicineSales_CashierName()
    {
        return "medicine_sales.CASHIER_NAME";
    }
  
    /**
     * medicine_sales.REMARK
     * @return the column name for the REMARK field
     * @deprecated use MedicineSalesPeer.medicine_sales.REMARK constant
     */
    public static String getMedicineSales_Remark()
    {
        return "medicine_sales.REMARK";
    }
  
    /**
     * medicine_sales.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.PAYMENT_TYPE_ID constant
     */
    public static String getMedicineSales_PaymentTypeId()
    {
        return "medicine_sales.PAYMENT_TYPE_ID";
    }
  
    /**
     * medicine_sales.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.PAYMENT_TERM_ID constant
     */
    public static String getMedicineSales_PaymentTermId()
    {
        return "medicine_sales.PAYMENT_TERM_ID";
    }
  
    /**
     * medicine_sales.PRINT_TIMES
     * @return the column name for the PRINT_TIMES field
     * @deprecated use MedicineSalesPeer.medicine_sales.PRINT_TIMES constant
     */
    public static String getMedicineSales_PrintTimes()
    {
        return "medicine_sales.PRINT_TIMES";
    }
  
    /**
     * medicine_sales.PAID_AMOUNT
     * @return the column name for the PAID_AMOUNT field
     * @deprecated use MedicineSalesPeer.medicine_sales.PAID_AMOUNT constant
     */
    public static String getMedicineSales_PaidAmount()
    {
        return "medicine_sales.PAID_AMOUNT";
    }
  
    /**
     * medicine_sales.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use MedicineSalesPeer.medicine_sales.PAYMENT_AMOUNT constant
     */
    public static String getMedicineSales_PaymentAmount()
    {
        return "medicine_sales.PAYMENT_AMOUNT";
    }
  
    /**
     * medicine_sales.CHANGE_AMOUNT
     * @return the column name for the CHANGE_AMOUNT field
     * @deprecated use MedicineSalesPeer.medicine_sales.CHANGE_AMOUNT constant
     */
    public static String getMedicineSales_ChangeAmount()
    {
        return "medicine_sales.CHANGE_AMOUNT";
    }
  
    /**
     * medicine_sales.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.CURRENCY_ID constant
     */
    public static String getMedicineSales_CurrencyId()
    {
        return "medicine_sales.CURRENCY_ID";
    }
  
    /**
     * medicine_sales.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use MedicineSalesPeer.medicine_sales.CURRENCY_RATE constant
     */
    public static String getMedicineSales_CurrencyRate()
    {
        return "medicine_sales.CURRENCY_RATE";
    }
  
    /**
     * medicine_sales.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use MedicineSalesPeer.medicine_sales.SHIP_TO constant
     */
    public static String getMedicineSales_ShipTo()
    {
        return "medicine_sales.SHIP_TO";
    }
  
    /**
     * medicine_sales.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.FOB_ID constant
     */
    public static String getMedicineSales_FobId()
    {
        return "medicine_sales.FOB_ID";
    }
  
    /**
     * medicine_sales.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use MedicineSalesPeer.medicine_sales.IS_TAXABLE constant
     */
    public static String getMedicineSales_IsTaxable()
    {
        return "medicine_sales.IS_TAXABLE";
    }
  
    /**
     * medicine_sales.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use MedicineSalesPeer.medicine_sales.IS_INCLUSIVE_TAX constant
     */
    public static String getMedicineSales_IsInclusiveTax()
    {
        return "medicine_sales.IS_INCLUSIVE_TAX";
    }
  
    /**
     * medicine_sales.PAYMENT_DATE
     * @return the column name for the PAYMENT_DATE field
     * @deprecated use MedicineSalesPeer.medicine_sales.PAYMENT_DATE constant
     */
    public static String getMedicineSales_PaymentDate()
    {
        return "medicine_sales.PAYMENT_DATE";
    }
  
    /**
     * medicine_sales.PAYMENT_STATUS
     * @return the column name for the PAYMENT_STATUS field
     * @deprecated use MedicineSalesPeer.medicine_sales.PAYMENT_STATUS constant
     */
    public static String getMedicineSales_PaymentStatus()
    {
        return "medicine_sales.PAYMENT_STATUS";
    }
  
    /**
     * medicine_sales.IS_INCLUSIVE_FREIGHT
     * @return the column name for the IS_INCLUSIVE_FREIGHT field
     * @deprecated use MedicineSalesPeer.medicine_sales.IS_INCLUSIVE_FREIGHT constant
     */
    public static String getMedicineSales_IsInclusiveFreight()
    {
        return "medicine_sales.IS_INCLUSIVE_FREIGHT";
    }
  
    /**
     * medicine_sales.FREIGHT_ACCOUNT_ID
     * @return the column name for the FREIGHT_ACCOUNT_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.FREIGHT_ACCOUNT_ID constant
     */
    public static String getMedicineSales_FreightAccountId()
    {
        return "medicine_sales.FREIGHT_ACCOUNT_ID";
    }
  
    /**
     * medicine_sales.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use MedicineSalesPeer.medicine_sales.CANCEL_BY constant
     */
    public static String getMedicineSales_CancelBy()
    {
        return "medicine_sales.CANCEL_BY";
    }
  
    /**
     * medicine_sales.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use MedicineSalesPeer.medicine_sales.CANCEL_DATE constant
     */
    public static String getMedicineSales_CancelDate()
    {
        return "medicine_sales.CANCEL_DATE";
    }
  
    /**
     * medicine_sales.SALES_TRANSACTION_ID
     * @return the column name for the SALES_TRANSACTION_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.SALES_TRANSACTION_ID constant
     */
    public static String getMedicineSales_SalesTransactionId()
    {
        return "medicine_sales.SALES_TRANSACTION_ID";
    }
  
    /**
     * medicine_sales.TOTAL_FEE
     * @return the column name for the TOTAL_FEE field
     * @deprecated use MedicineSalesPeer.medicine_sales.TOTAL_FEE constant
     */
    public static String getMedicineSales_TotalFee()
    {
        return "medicine_sales.TOTAL_FEE";
    }
  
    /**
     * medicine_sales.ROUNDING_AMOUNT
     * @return the column name for the ROUNDING_AMOUNT field
     * @deprecated use MedicineSalesPeer.medicine_sales.ROUNDING_AMOUNT constant
     */
    public static String getMedicineSales_RoundingAmount()
    {
        return "medicine_sales.ROUNDING_AMOUNT";
    }
  
    /**
     * medicine_sales.DOCTOR_NAME
     * @return the column name for the DOCTOR_NAME field
     * @deprecated use MedicineSalesPeer.medicine_sales.DOCTOR_NAME constant
     */
    public static String getMedicineSales_DoctorName()
    {
        return "medicine_sales.DOCTOR_NAME";
    }
  
    /**
     * medicine_sales.PATIENT_NAME
     * @return the column name for the PATIENT_NAME field
     * @deprecated use MedicineSalesPeer.medicine_sales.PATIENT_NAME constant
     */
    public static String getMedicineSales_PatientName()
    {
        return "medicine_sales.PATIENT_NAME";
    }
  
    /**
     * medicine_sales.PATIENT_AGE
     * @return the column name for the PATIENT_AGE field
     * @deprecated use MedicineSalesPeer.medicine_sales.PATIENT_AGE constant
     */
    public static String getMedicineSales_PatientAge()
    {
        return "medicine_sales.PATIENT_AGE";
    }
  
    /**
     * medicine_sales.PATIENT_WEIGHT
     * @return the column name for the PATIENT_WEIGHT field
     * @deprecated use MedicineSalesPeer.medicine_sales.PATIENT_WEIGHT constant
     */
    public static String getMedicineSales_PatientWeight()
    {
        return "medicine_sales.PATIENT_WEIGHT";
    }
  
    /**
     * medicine_sales.PATIENT_PHONE
     * @return the column name for the PATIENT_PHONE field
     * @deprecated use MedicineSalesPeer.medicine_sales.PATIENT_PHONE constant
     */
    public static String getMedicineSales_PatientPhone()
    {
        return "medicine_sales.PATIENT_PHONE";
    }
  
    /**
     * medicine_sales.PATIENT_ADDRESS
     * @return the column name for the PATIENT_ADDRESS field
     * @deprecated use MedicineSalesPeer.medicine_sales.PATIENT_ADDRESS constant
     */
    public static String getMedicineSales_PatientAddress()
    {
        return "medicine_sales.PATIENT_ADDRESS";
    }
  
    /**
     * medicine_sales.REGISTRATION_ID
     * @return the column name for the REGISTRATION_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.REGISTRATION_ID constant
     */
    public static String getMedicineSales_RegistrationId()
    {
        return "medicine_sales.REGISTRATION_ID";
    }
  
    /**
     * medicine_sales.INSURANCE_ID
     * @return the column name for the INSURANCE_ID field
     * @deprecated use MedicineSalesPeer.medicine_sales.INSURANCE_ID constant
     */
    public static String getMedicineSales_InsuranceId()
    {
        return "medicine_sales.INSURANCE_ID";
    }
  
    /**
     * medicine_sales.POLICY_NO
     * @return the column name for the POLICY_NO field
     * @deprecated use MedicineSalesPeer.medicine_sales.POLICY_NO constant
     */
    public static String getMedicineSales_PolicyNo()
    {
        return "medicine_sales.POLICY_NO";
    }
  
    /**
     * medicine_sales.POLICY_NAME
     * @return the column name for the POLICY_NAME field
     * @deprecated use MedicineSalesPeer.medicine_sales.POLICY_NAME constant
     */
    public static String getMedicineSales_PolicyName()
    {
        return "medicine_sales.POLICY_NAME";
    }
  
    /**
     * medicine_sales.IS_INSURANCE
     * @return the column name for the IS_INSURANCE field
     * @deprecated use MedicineSalesPeer.medicine_sales.IS_INSURANCE constant
     */
    public static String getMedicineSales_IsInsurance()
    {
        return "medicine_sales.IS_INSURANCE";
    }
  
    /**
     * medicine_sales.PREP_QUEUE_NO
     * @return the column name for the PREP_QUEUE_NO field
     * @deprecated use MedicineSalesPeer.medicine_sales.PREP_QUEUE_NO constant
     */
    public static String getMedicineSales_PrepQueueNo()
    {
        return "medicine_sales.PREP_QUEUE_NO";
    }
  
    /**
     * medicine_sales.PREP_STATUS
     * @return the column name for the PREP_STATUS field
     * @deprecated use MedicineSalesPeer.medicine_sales.PREP_STATUS constant
     */
    public static String getMedicineSales_PrepStatus()
    {
        return "medicine_sales.PREP_STATUS";
    }
  
    /**
     * medicine_sales.PREP_START
     * @return the column name for the PREP_START field
     * @deprecated use MedicineSalesPeer.medicine_sales.PREP_START constant
     */
    public static String getMedicineSales_PrepStart()
    {
        return "medicine_sales.PREP_START";
    }
  
    /**
     * medicine_sales.PREP_END
     * @return the column name for the PREP_END field
     * @deprecated use MedicineSalesPeer.medicine_sales.PREP_END constant
     */
    public static String getMedicineSales_PrepEnd()
    {
        return "medicine_sales.PREP_END";
    }
  
    /**
     * medicine_sales.PREP_BY
     * @return the column name for the PREP_BY field
     * @deprecated use MedicineSalesPeer.medicine_sales.PREP_BY constant
     */
    public static String getMedicineSales_PrepBy()
    {
        return "medicine_sales.PREP_BY";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("medicine_sales");
        TableMap tMap = dbMap.getTable("medicine_sales");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("medicine_sales.MEDICINE_SALES_ID", "");
                          tMap.addColumn("medicine_sales.LOCATION_ID", "");
                            tMap.addColumn("medicine_sales.STATUS", Integer.valueOf(0));
                          tMap.addColumn("medicine_sales.INVOICE_NO", "");
                          tMap.addColumn("medicine_sales.DELIVERY_NO", "");
                          tMap.addColumn("medicine_sales.TRANSACTION_DATE", new Date());
                          tMap.addColumn("medicine_sales.DUE_DATE", new Date());
                          tMap.addColumn("medicine_sales.CUSTOMER_ID", "");
                          tMap.addColumn("medicine_sales.CUSTOMER_NAME", "");
                            tMap.addColumn("medicine_sales.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("medicine_sales.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("medicine_sales.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("medicine_sales.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("medicine_sales.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("medicine_sales.COURIER_ID", "");
                            tMap.addColumn("medicine_sales.SHIPPING_PRICE", bd_ZERO);
                            tMap.addColumn("medicine_sales.TOTAL_EXPENSE", bd_ZERO);
                            tMap.addColumn("medicine_sales.TOTAL_COST", bd_ZERO);
                          tMap.addColumn("medicine_sales.SALES_ID", "");
                          tMap.addColumn("medicine_sales.CASHIER_NAME", "");
                          tMap.addColumn("medicine_sales.REMARK", "");
                          tMap.addColumn("medicine_sales.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("medicine_sales.PAYMENT_TERM_ID", "");
                            tMap.addColumn("medicine_sales.PRINT_TIMES", Integer.valueOf(0));
                            tMap.addColumn("medicine_sales.PAID_AMOUNT", bd_ZERO);
                            tMap.addColumn("medicine_sales.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("medicine_sales.CHANGE_AMOUNT", bd_ZERO);
                          tMap.addColumn("medicine_sales.CURRENCY_ID", "");
                            tMap.addColumn("medicine_sales.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("medicine_sales.SHIP_TO", "");
                          tMap.addColumn("medicine_sales.FOB_ID", "");
                          tMap.addColumn("medicine_sales.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("medicine_sales.IS_INCLUSIVE_TAX", Boolean.TRUE);
                          tMap.addColumn("medicine_sales.PAYMENT_DATE", new Date());
                            tMap.addColumn("medicine_sales.PAYMENT_STATUS", Integer.valueOf(0));
                          tMap.addColumn("medicine_sales.IS_INCLUSIVE_FREIGHT", Boolean.TRUE);
                          tMap.addColumn("medicine_sales.FREIGHT_ACCOUNT_ID", "");
                          tMap.addColumn("medicine_sales.CANCEL_BY", "");
                          tMap.addColumn("medicine_sales.CANCEL_DATE", new Date());
                          tMap.addColumn("medicine_sales.SALES_TRANSACTION_ID", "");
                            tMap.addColumn("medicine_sales.TOTAL_FEE", bd_ZERO);
                            tMap.addColumn("medicine_sales.ROUNDING_AMOUNT", bd_ZERO);
                          tMap.addColumn("medicine_sales.DOCTOR_NAME", "");
                          tMap.addColumn("medicine_sales.PATIENT_NAME", "");
                          tMap.addColumn("medicine_sales.PATIENT_AGE", "");
                            tMap.addColumn("medicine_sales.PATIENT_WEIGHT", Integer.valueOf(0));
                          tMap.addColumn("medicine_sales.PATIENT_PHONE", "");
                          tMap.addColumn("medicine_sales.PATIENT_ADDRESS", "");
                          tMap.addColumn("medicine_sales.REGISTRATION_ID", "");
                          tMap.addColumn("medicine_sales.INSURANCE_ID", "");
                          tMap.addColumn("medicine_sales.POLICY_NO", "");
                          tMap.addColumn("medicine_sales.POLICY_NAME", "");
                          tMap.addColumn("medicine_sales.IS_INSURANCE", Boolean.TRUE);
                          tMap.addColumn("medicine_sales.PREP_QUEUE_NO", "");
                            tMap.addColumn("medicine_sales.PREP_STATUS", Integer.valueOf(0));
                          tMap.addColumn("medicine_sales.PREP_START", new Date());
                          tMap.addColumn("medicine_sales.PREP_END", new Date());
                          tMap.addColumn("medicine_sales.PREP_BY", "");
          }
}
