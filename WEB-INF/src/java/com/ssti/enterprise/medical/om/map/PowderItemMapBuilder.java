package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PowderItemMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.PowderItemMapBuilder";

    /**
     * Item
     * @deprecated use PowderItemPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "powder_item";
    }

  
    /**
     * powder_item.POWDER_ITEM_ID
     * @return the column name for the POWDER_ITEM_ID field
     * @deprecated use PowderItemPeer.powder_item.POWDER_ITEM_ID constant
     */
    public static String getPowderItem_PowderItemId()
    {
        return "powder_item.POWDER_ITEM_ID";
    }
  
    /**
     * powder_item.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PowderItemPeer.powder_item.ITEM_ID constant
     */
    public static String getPowderItem_ItemId()
    {
        return "powder_item.ITEM_ID";
    }
  
    /**
     * powder_item.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PowderItemPeer.powder_item.ITEM_CODE constant
     */
    public static String getPowderItem_ItemCode()
    {
        return "powder_item.ITEM_CODE";
    }
  
    /**
     * powder_item.DEFAULT_RFEE
     * @return the column name for the DEFAULT_RFEE field
     * @deprecated use PowderItemPeer.powder_item.DEFAULT_RFEE constant
     */
    public static String getPowderItem_DefaultRfee()
    {
        return "powder_item.DEFAULT_RFEE";
    }
  
    /**
     * powder_item.DEFAULT_PACKAGING
     * @return the column name for the DEFAULT_PACKAGING field
     * @deprecated use PowderItemPeer.powder_item.DEFAULT_PACKAGING constant
     */
    public static String getPowderItem_DefaultPackaging()
    {
        return "powder_item.DEFAULT_PACKAGING";
    }
  
    /**
     * powder_item.MULTIPLIED
     * @return the column name for the MULTIPLIED field
     * @deprecated use PowderItemPeer.powder_item.MULTIPLIED constant
     */
    public static String getPowderItem_Multiplied()
    {
        return "powder_item.MULTIPLIED";
    }
  
    /**
     * powder_item.RFEE_MIN_AMOUNT
     * @return the column name for the RFEE_MIN_AMOUNT field
     * @deprecated use PowderItemPeer.powder_item.RFEE_MIN_AMOUNT constant
     */
    public static String getPowderItem_RfeeMinAmount()
    {
        return "powder_item.RFEE_MIN_AMOUNT";
    }
  
    /**
     * powder_item.RFEE_MAX_AMOUNT
     * @return the column name for the RFEE_MAX_AMOUNT field
     * @deprecated use PowderItemPeer.powder_item.RFEE_MAX_AMOUNT constant
     */
    public static String getPowderItem_RfeeMaxAmount()
    {
        return "powder_item.RFEE_MAX_AMOUNT";
    }
  
    /**
     * powder_item.PACK_MIN_AMOUNT
     * @return the column name for the PACK_MIN_AMOUNT field
     * @deprecated use PowderItemPeer.powder_item.PACK_MIN_AMOUNT constant
     */
    public static String getPowderItem_PackMinAmount()
    {
        return "powder_item.PACK_MIN_AMOUNT";
    }
  
    /**
     * powder_item.PACK_MAX_AMOUNT
     * @return the column name for the PACK_MAX_AMOUNT field
     * @deprecated use PowderItemPeer.powder_item.PACK_MAX_AMOUNT constant
     */
    public static String getPowderItem_PackMaxAmount()
    {
        return "powder_item.PACK_MAX_AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("powder_item");
        TableMap tMap = dbMap.getTable("powder_item");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("powder_item.POWDER_ITEM_ID", "");
                          tMap.addColumn("powder_item.ITEM_ID", "");
                          tMap.addColumn("powder_item.ITEM_CODE", "");
                          tMap.addColumn("powder_item.DEFAULT_RFEE", "");
                          tMap.addColumn("powder_item.DEFAULT_PACKAGING", "");
                          tMap.addColumn("powder_item.MULTIPLIED", Boolean.TRUE);
                            tMap.addColumn("powder_item.RFEE_MIN_AMOUNT", bd_ZERO);
                            tMap.addColumn("powder_item.RFEE_MAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("powder_item.PACK_MIN_AMOUNT", bd_ZERO);
                            tMap.addColumn("powder_item.PACK_MAX_AMOUNT", bd_ZERO);
          }
}
