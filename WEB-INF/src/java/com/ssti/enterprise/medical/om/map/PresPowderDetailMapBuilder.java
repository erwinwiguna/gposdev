package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PresPowderDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.PresPowderDetailMapBuilder";

    /**
     * Item
     * @deprecated use PresPowderDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "pres_powder_detail";
    }

  
    /**
     * pres_powder_detail.PRES_POWDER_DETAIL_ID
     * @return the column name for the PRES_POWDER_DETAIL_ID field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.PRES_POWDER_DETAIL_ID constant
     */
    public static String getPresPowderDetail_PresPowderDetailId()
    {
        return "pres_powder_detail.PRES_POWDER_DETAIL_ID";
    }
  
    /**
     * pres_powder_detail.PRESCRIPTION_DETAIL_ID
     * @return the column name for the PRESCRIPTION_DETAIL_ID field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.PRESCRIPTION_DETAIL_ID constant
     */
    public static String getPresPowderDetail_PrescriptionDetailId()
    {
        return "pres_powder_detail.PRESCRIPTION_DETAIL_ID";
    }
  
    /**
     * pres_powder_detail.PRESCRIPTION_ID
     * @return the column name for the PRESCRIPTION_ID field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.PRESCRIPTION_ID constant
     */
    public static String getPresPowderDetail_PrescriptionId()
    {
        return "pres_powder_detail.PRESCRIPTION_ID";
    }
  
    /**
     * pres_powder_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.ITEM_ID constant
     */
    public static String getPresPowderDetail_ItemId()
    {
        return "pres_powder_detail.ITEM_ID";
    }
  
    /**
     * pres_powder_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.ITEM_CODE constant
     */
    public static String getPresPowderDetail_ItemCode()
    {
        return "pres_powder_detail.ITEM_CODE";
    }
  
    /**
     * pres_powder_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.ITEM_NAME constant
     */
    public static String getPresPowderDetail_ItemName()
    {
        return "pres_powder_detail.ITEM_NAME";
    }
  
    /**
     * pres_powder_detail.ITEM_DESC
     * @return the column name for the ITEM_DESC field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.ITEM_DESC constant
     */
    public static String getPresPowderDetail_ItemDesc()
    {
        return "pres_powder_detail.ITEM_DESC";
    }
  
    /**
     * pres_powder_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.UNIT_ID constant
     */
    public static String getPresPowderDetail_UnitId()
    {
        return "pres_powder_detail.UNIT_ID";
    }
  
    /**
     * pres_powder_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.UNIT_CODE constant
     */
    public static String getPresPowderDetail_UnitCode()
    {
        return "pres_powder_detail.UNIT_CODE";
    }
  
    /**
     * pres_powder_detail.ITEM_DOSAGE
     * @return the column name for the ITEM_DOSAGE field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.ITEM_DOSAGE constant
     */
    public static String getPresPowderDetail_ItemDosage()
    {
        return "pres_powder_detail.ITEM_DOSAGE";
    }
  
    /**
     * pres_powder_detail.USE_DOSAGE
     * @return the column name for the USE_DOSAGE field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.USE_DOSAGE constant
     */
    public static String getPresPowderDetail_UseDosage()
    {
        return "pres_powder_detail.USE_DOSAGE";
    }
  
    /**
     * pres_powder_detail.REQ_QTY
     * @return the column name for the REQ_QTY field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.REQ_QTY constant
     */
    public static String getPresPowderDetail_ReqQty()
    {
        return "pres_powder_detail.REQ_QTY";
    }
  
    /**
     * pres_powder_detail.USED_QTY
     * @return the column name for the USED_QTY field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.USED_QTY constant
     */
    public static String getPresPowderDetail_UsedQty()
    {
        return "pres_powder_detail.USED_QTY";
    }
  
    /**
     * pres_powder_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.QTY constant
     */
    public static String getPresPowderDetail_Qty()
    {
        return "pres_powder_detail.QTY";
    }
  
    /**
     * pres_powder_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.QTY_BASE constant
     */
    public static String getPresPowderDetail_QtyBase()
    {
        return "pres_powder_detail.QTY_BASE";
    }
  
    /**
     * pres_powder_detail.BILLED_QTY
     * @return the column name for the BILLED_QTY field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.BILLED_QTY constant
     */
    public static String getPresPowderDetail_BilledQty()
    {
        return "pres_powder_detail.BILLED_QTY";
    }
  
    /**
     * pres_powder_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.ITEM_PRICE constant
     */
    public static String getPresPowderDetail_ItemPrice()
    {
        return "pres_powder_detail.ITEM_PRICE";
    }
  
    /**
     * pres_powder_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.TAX_ID constant
     */
    public static String getPresPowderDetail_TaxId()
    {
        return "pres_powder_detail.TAX_ID";
    }
  
    /**
     * pres_powder_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.TAX_AMOUNT constant
     */
    public static String getPresPowderDetail_TaxAmount()
    {
        return "pres_powder_detail.TAX_AMOUNT";
    }
  
    /**
     * pres_powder_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.SUB_TOTAL constant
     */
    public static String getPresPowderDetail_SubTotal()
    {
        return "pres_powder_detail.SUB_TOTAL";
    }
  
    /**
     * pres_powder_detail.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use PresPowderDetailPeer.pres_powder_detail.BATCH_NO constant
     */
    public static String getPresPowderDetail_BatchNo()
    {
        return "pres_powder_detail.BATCH_NO";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("pres_powder_detail");
        TableMap tMap = dbMap.getTable("pres_powder_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("pres_powder_detail.PRES_POWDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "pres_powder_detail.PRESCRIPTION_DETAIL_ID", "" , "prescription_detail" ,
                "prescription_detail_id");
                          tMap.addColumn("pres_powder_detail.PRESCRIPTION_ID", "");
                          tMap.addColumn("pres_powder_detail.ITEM_ID", "");
                          tMap.addColumn("pres_powder_detail.ITEM_CODE", "");
                          tMap.addColumn("pres_powder_detail.ITEM_NAME", "");
                          tMap.addColumn("pres_powder_detail.ITEM_DESC", "");
                          tMap.addColumn("pres_powder_detail.UNIT_ID", "");
                          tMap.addColumn("pres_powder_detail.UNIT_CODE", "");
                            tMap.addColumn("pres_powder_detail.ITEM_DOSAGE", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.USE_DOSAGE", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.REQ_QTY", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.USED_QTY", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.QTY", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.BILLED_QTY", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("pres_powder_detail.TAX_ID", "");
                            tMap.addColumn("pres_powder_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("pres_powder_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("pres_powder_detail.BATCH_NO", "");
          }
}
