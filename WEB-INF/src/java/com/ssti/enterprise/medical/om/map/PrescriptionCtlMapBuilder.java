package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PrescriptionCtlMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.PrescriptionCtlMapBuilder";

    /**
     * Item
     * @deprecated use PrescriptionCtlPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "prescription_ctl";
    }

  
    /**
     * prescription_ctl.LAST_NUMBER
     * @return the column name for the LAST_NUMBER field
     * @deprecated use PrescriptionCtlPeer.prescription_ctl.LAST_NUMBER constant
     */
    public static String getPrescriptionCtl_LastNumber()
    {
        return "prescription_ctl.LAST_NUMBER";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("prescription_ctl");
        TableMap tMap = dbMap.getTable("prescription_ctl");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("prescription_ctl.LAST_NUMBER", "");
          }
}
