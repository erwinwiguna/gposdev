package com.ssti.enterprise.medical.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PrescriptionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.PrescriptionMapBuilder";

    /**
     * Item
     * @deprecated use PrescriptionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "prescription";
    }

  
    /**
     * prescription.PRESCRIPTION_ID
     * @return the column name for the PRESCRIPTION_ID field
     * @deprecated use PrescriptionPeer.prescription.PRESCRIPTION_ID constant
     */
    public static String getPrescription_PrescriptionId()
    {
        return "prescription.PRESCRIPTION_ID";
    }
  
    /**
     * prescription.PRESCRIPTION_NO
     * @return the column name for the PRESCRIPTION_NO field
     * @deprecated use PrescriptionPeer.prescription.PRESCRIPTION_NO constant
     */
    public static String getPrescription_PrescriptionNo()
    {
        return "prescription.PRESCRIPTION_NO";
    }
  
    /**
     * prescription.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use PrescriptionPeer.prescription.CUSTOMER_ID constant
     */
    public static String getPrescription_CustomerId()
    {
        return "prescription.CUSTOMER_ID";
    }
  
    /**
     * prescription.PATIENT_NAME
     * @return the column name for the PATIENT_NAME field
     * @deprecated use PrescriptionPeer.prescription.PATIENT_NAME constant
     */
    public static String getPrescription_PatientName()
    {
        return "prescription.PATIENT_NAME";
    }
  
    /**
     * prescription.PATIENT_AGE
     * @return the column name for the PATIENT_AGE field
     * @deprecated use PrescriptionPeer.prescription.PATIENT_AGE constant
     */
    public static String getPrescription_PatientAge()
    {
        return "prescription.PATIENT_AGE";
    }
  
    /**
     * prescription.PATIENT_PHONE
     * @return the column name for the PATIENT_PHONE field
     * @deprecated use PrescriptionPeer.prescription.PATIENT_PHONE constant
     */
    public static String getPrescription_PatientPhone()
    {
        return "prescription.PATIENT_PHONE";
    }
  
    /**
     * prescription.PATIENT_WEIGHT
     * @return the column name for the PATIENT_WEIGHT field
     * @deprecated use PrescriptionPeer.prescription.PATIENT_WEIGHT constant
     */
    public static String getPrescription_PatientWeight()
    {
        return "prescription.PATIENT_WEIGHT";
    }
  
    /**
     * prescription.PATIENT_ADDRESS
     * @return the column name for the PATIENT_ADDRESS field
     * @deprecated use PrescriptionPeer.prescription.PATIENT_ADDRESS constant
     */
    public static String getPrescription_PatientAddress()
    {
        return "prescription.PATIENT_ADDRESS";
    }
  
    /**
     * prescription.PRES_FILE
     * @return the column name for the PRES_FILE field
     * @deprecated use PrescriptionPeer.prescription.PRES_FILE constant
     */
    public static String getPrescription_PresFile()
    {
        return "prescription.PRES_FILE";
    }
  
    /**
     * prescription.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use PrescriptionPeer.prescription.CREATE_BY constant
     */
    public static String getPrescription_CreateBy()
    {
        return "prescription.CREATE_BY";
    }
  
    /**
     * prescription.DOCTOR_ID
     * @return the column name for the DOCTOR_ID field
     * @deprecated use PrescriptionPeer.prescription.DOCTOR_ID constant
     */
    public static String getPrescription_DoctorId()
    {
        return "prescription.DOCTOR_ID";
    }
  
    /**
     * prescription.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PrescriptionPeer.prescription.REMARK constant
     */
    public static String getPrescription_Remark()
    {
        return "prescription.REMARK";
    }
  
    /**
     * prescription.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PrescriptionPeer.prescription.LOCATION_ID constant
     */
    public static String getPrescription_LocationId()
    {
        return "prescription.LOCATION_ID";
    }
  
    /**
     * prescription.STATUS
     * @return the column name for the STATUS field
     * @deprecated use PrescriptionPeer.prescription.STATUS constant
     */
    public static String getPrescription_Status()
    {
        return "prescription.STATUS";
    }
  
    /**
     * prescription.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use PrescriptionPeer.prescription.CREATE_DATE constant
     */
    public static String getPrescription_CreateDate()
    {
        return "prescription.CREATE_DATE";
    }
  
    /**
     * prescription.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use PrescriptionPeer.prescription.UPDATE_DATE constant
     */
    public static String getPrescription_UpdateDate()
    {
        return "prescription.UPDATE_DATE";
    }
  
    /**
     * prescription.PRES_DISCOUNT
     * @return the column name for the PRES_DISCOUNT field
     * @deprecated use PrescriptionPeer.prescription.PRES_DISCOUNT constant
     */
    public static String getPrescription_PresDiscount()
    {
        return "prescription.PRES_DISCOUNT";
    }
  
    /**
     * prescription.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use PrescriptionPeer.prescription.TOTAL_DISCOUNT constant
     */
    public static String getPrescription_TotalDiscount()
    {
        return "prescription.TOTAL_DISCOUNT";
    }
  
    /**
     * prescription.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use PrescriptionPeer.prescription.TOTAL_AMOUNT constant
     */
    public static String getPrescription_TotalAmount()
    {
        return "prescription.TOTAL_AMOUNT";
    }
  
    /**
     * prescription.TOTAL_RFEE
     * @return the column name for the TOTAL_RFEE field
     * @deprecated use PrescriptionPeer.prescription.TOTAL_RFEE constant
     */
    public static String getPrescription_TotalRfee()
    {
        return "prescription.TOTAL_RFEE";
    }
  
    /**
     * prescription.TOTAL_PACKAGING
     * @return the column name for the TOTAL_PACKAGING field
     * @deprecated use PrescriptionPeer.prescription.TOTAL_PACKAGING constant
     */
    public static String getPrescription_TotalPackaging()
    {
        return "prescription.TOTAL_PACKAGING";
    }
  
    /**
     * prescription.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PrescriptionPeer.prescription.CANCEL_BY constant
     */
    public static String getPrescription_CancelBy()
    {
        return "prescription.CANCEL_BY";
    }
  
    /**
     * prescription.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PrescriptionPeer.prescription.CANCEL_DATE constant
     */
    public static String getPrescription_CancelDate()
    {
        return "prescription.CANCEL_DATE";
    }
  
    /**
     * prescription.MEDICAL_RECORD_ID
     * @return the column name for the MEDICAL_RECORD_ID field
     * @deprecated use PrescriptionPeer.prescription.MEDICAL_RECORD_ID constant
     */
    public static String getPrescription_MedicalRecordId()
    {
        return "prescription.MEDICAL_RECORD_ID";
    }
  
    /**
     * prescription.REGISTRATION_ID
     * @return the column name for the REGISTRATION_ID field
     * @deprecated use PrescriptionPeer.prescription.REGISTRATION_ID constant
     */
    public static String getPrescription_RegistrationId()
    {
        return "prescription.REGISTRATION_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("prescription");
        TableMap tMap = dbMap.getTable("prescription");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("prescription.PRESCRIPTION_ID", "");
                          tMap.addColumn("prescription.PRESCRIPTION_NO", "");
                          tMap.addColumn("prescription.CUSTOMER_ID", "");
                          tMap.addColumn("prescription.PATIENT_NAME", "");
                          tMap.addColumn("prescription.PATIENT_AGE", "");
                          tMap.addColumn("prescription.PATIENT_PHONE", "");
                            tMap.addColumn("prescription.PATIENT_WEIGHT", Integer.valueOf(0));
                          tMap.addColumn("prescription.PATIENT_ADDRESS", "");
                          tMap.addColumn("prescription.PRES_FILE", "");
                          tMap.addColumn("prescription.CREATE_BY", "");
                          tMap.addColumn("prescription.DOCTOR_ID", "");
                          tMap.addColumn("prescription.REMARK", "");
                          tMap.addColumn("prescription.LOCATION_ID", "");
                            tMap.addColumn("prescription.STATUS", Integer.valueOf(0));
                          tMap.addColumn("prescription.CREATE_DATE", new Date());
                          tMap.addColumn("prescription.UPDATE_DATE", new Date());
                          tMap.addColumn("prescription.PRES_DISCOUNT", "");
                            tMap.addColumn("prescription.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("prescription.TOTAL_AMOUNT", bd_ZERO);
                            tMap.addColumn("prescription.TOTAL_RFEE", bd_ZERO);
                            tMap.addColumn("prescription.TOTAL_PACKAGING", bd_ZERO);
                          tMap.addColumn("prescription.CANCEL_BY", "");
                          tMap.addColumn("prescription.CANCEL_DATE", new Date());
                          tMap.addColumn("prescription.MEDICAL_RECORD_ID", "");
                          tMap.addColumn("prescription.REGISTRATION_ID", "");
          }
}
