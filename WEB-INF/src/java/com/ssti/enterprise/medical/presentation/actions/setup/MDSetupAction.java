package com.ssti.enterprise.medical.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.excel.helper.DoctorLoader;
import com.ssti.enterprise.medical.manager.DoctorManager;
import com.ssti.enterprise.medical.om.Doctor;
import com.ssti.enterprise.medical.om.DoctorPeer;
import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.enterprise.pos.presentation.actions.setup.SetupSecureAction;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.SqlUtil;

public class MDSetupAction extends SetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Doctor oDoctor = new Doctor();
			setProperties (oDoctor, data);
			oDoctor.setDoctorId (IDGenerator.generateSysID());
	        oDoctor.save();
	        DoctorManager.getInstance().refreshCache(oDoctor);
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Doctor oDoctor = DoctorTool.getDoctorByID(data.getParameters().getString("ID"));
			setProperties (oDoctor, data);
	        oDoctor.save();
	        DoctorManager.getInstance().refreshCache(oDoctor);
           	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("prescription","doctor_id",sID))
    		{
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(DoctorPeer.DOCTOR_ID, sID);
	        	DoctorPeer.doDelete(oCrit);
	        	DoctorManager.getInstance().refreshCache(sID);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));	
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }

    private void setProperties (Doctor _oDoctor, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oDoctor);
	    	_oDoctor.setIsInternal(data.getParameters().getBoolean("IsInternal"));
        }
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
    
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			DoctorLoader oLoader = new DoctorLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}
