package com.ssti.enterprise.medical.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.excel.helper.PowderItemLoader;
import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.om.PowderItem;
import com.ssti.enterprise.medical.om.PowderItemPeer;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.presentation.actions.setup.SetupSecureAction;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.SqlUtil;

public class PowderItemAction extends SetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			String sCode = data.getParameters().getString("ItemCode");
			Item oItem = ItemTool.getItemByCode(sCode);
			
			if (oItem != null)
			{			
				PowderItem oPI = new PowderItem();
				oPI.setPowderItemId(IDGenerator.generateSysID());
				data.getParameters().setProperties(oPI);
				oPI.setMultiplied(data.getParameters().getBoolean("Multiplied"));
				oPI.setItemId(oItem.getItemId());
				oPI.save();
				MedConfigManager.getInstance().refreshCache();
	        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
			}
			else
			{
	        	data.setMessage("Item " + sCode + " " + LocaleTool.getString("not_found"));				
			}
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			String sCode = data.getParameters().getString("ItemCode");
			Item oItem = ItemTool.getItemByCode(sCode);
			
			if (oItem != null)
			{			
				String sID = data.getParameters().getString("id");
				PowderItem oPI = PowderTool.getPowderItemByID(sID);
				if (oPI != null)
				{
					data.getParameters().setProperties(oPI);
					oPI.setMultiplied(data.getParameters().getBoolean("Multiplied"));
					oPI.setItemId(oItem.getItemId());
					oPI.save();
					MedConfigManager.getInstance().refreshCache();
		        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
				}
			}
			else
			{
	        	data.setMessage("Item " + sCode + " " + LocaleTool.getString("not_found"));				
			}
		}
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("id");
    		String sItemID = data.getParameters().getString("ItemId");
    		if(SqlUtil.validateFKRef("pres_powder_detail","item_id",sItemID))
    		{
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(PowderItemPeer.POWDER_ITEM_ID, sID);
	        	PowderItemPeer.doDelete(oCrit);
	        	MedConfigManager.getInstance().refreshCache();
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));	
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }
 	
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			PowderItemLoader oLoader = new PowderItemLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			MedConfigManager.getInstance().refreshCache();
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}
