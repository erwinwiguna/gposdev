package com.ssti.enterprise.medical.presentation.actions.transaction;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.presentation.screens.transaction.PrescriptionTrans;
import com.ssti.enterprise.medical.tools.PrescriptionTool;
import com.ssti.enterprise.pos.presentation.actions.transaction.TransactionSecureAction;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class PrescriptionAction extends TransactionSecureAction
{   
    private HttpSession oSes;  

    private static final String s_VIEW_PERM    = "View Prescription";
	private static final String s_CREATE_PERM  = "Create Prescription";
	private static final String s_CANCEL_PERM  = "Edit Prescription";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {	
    	if(data.getParameters().getInt("save") == 1)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
    	if(data.getParameters().getInt("save") == 2)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
    		doSave (data,context);
    	}
    	else if (iSave == 2) 
    	{
    		doEdit (data,context);
    	}
    	else if (iSave == 3) 
    	{
    		doCancel (data,context);
    	}    	
    	else
    	{
    		doFind(data, context);
    	}
    }

	public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);			
			String sCustomerID = data.getParameters().getString("CustomerId","");						
			String sLocationID = data.getParameters().getString("LocationId","");			
			String sDoctorID = data.getParameters().getString("DoctorId","");			
			String sCreateBy = data.getParameters().getString("CreateBy","");			
			int iStatus = data.getParameters().getInt("Status");
			int iGroupBy = data.getParameters().getInt("GroupBy", -1);
			
			LargeSelect vData = 
				PrescriptionTool.findData (iCond, sKeywords, dStart, dEnd, sCustomerID, 
										   sLocationID, sDoctorID, sCreateBy, iStatus, iGroupBy, iLimit);
			data.getUser().setTemp("findPrescriptionResult", vData);
        }
        catch(Exception _oEx)
        {
	       	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
    
	public void doSave(RunData data, Context context)
        throws Exception
    {
		try
		{
	        Prescription oPRS = (Prescription) data.getSession().getAttribute(PrescriptionTrans.s_PRS);
	        List vPRD = (List) data.getSession().getAttribute(PrescriptionTrans.s_PRSD);	        
	        PrescriptionTool.updateDetail(data, vPRD);
	        PrescriptionTool.setHeader(data, oPRS, vPRD);
    		
	        PrescriptionTool.saveData (oPRS, vPRD, null);
        	data.setMessage(LocaleTool.getString("prs_save_success"));
        }
        catch(Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("prs_save_failed"), _oEx);
        }
    }
    
	public void doEdit(RunData data, Context context)
	    throws Exception
	{
		try
		{
			Prescription oPRS = (Prescription) data.getSession().getAttribute(PrescriptionTrans.s_PRS);
	    	List vIRD = (List) data.getSession().getAttribute(PrescriptionTrans.s_PRSD);
	    	
	    	PrescriptionTool.editTrans(oPRS, vIRD, data.getUser().getName(), null);	    	
			data.setMessage(LocaleTool.getString("prs_edit_success"));
	    }
	    catch(Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("prs_edit_failed"), _oEx);
	    }
	}	
	
	public void doCancel(RunData data, Context context)
	    throws Exception
	{
		try
		{
			Prescription oPRS = (Prescription) data.getSession().getAttribute(PrescriptionTrans.s_PRS);	    	
	    	PrescriptionTool.cancelData(oPRS, data.getUser().getName());
			data.setMessage(LocaleTool.getString("prs_cancel_success"));
	    }
	    catch(Exception _oEx)
	    {
	    	handleError (data, LocaleTool.getString("prs_edit_failed"), _oEx);
	    }
	}	
}