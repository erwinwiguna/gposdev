package com.ssti.enterprise.medical.presentation.screens.report.medical;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;

public class MedDailySalesReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
		{
    		super.doBuildTemplate(data, context);
	    	Date dStart = CustomParser.parseDate(data.getParameters().getString("StartDate"));
	    	Date dEnd = CustomParser.parseDate(data.getParameters().getString("EndDate")); 
	    	
	    	if(dStart == null || dEnd == null)
	    	{
	    	    dStart = new Date();
	    	    dEnd = new Date();
	        }
	        
    	    dStart  = DateUtil.getStartOfDayDate(dStart);
    	    dEnd    = DateUtil.getEndOfDayDate(dEnd);

    		context.put ("vDates", DateUtil.getDateInRange (dStart, dEnd));     
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Sales Summary Data Lookup Failed : " + _oEx.getMessage());
    	}    
    }   
}
