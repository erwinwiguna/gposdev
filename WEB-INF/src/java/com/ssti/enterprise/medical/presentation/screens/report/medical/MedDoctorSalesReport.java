package com.ssti.enterprise.medical.presentation.screens.report.medical;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.report.SalesReportTool;

public class MedDoctorSalesReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {
	    	    setParams(data);
	    	    context.put("salesreport",SalesReportTool.getInstance());
            }
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    }    
}
