package com.ssti.enterprise.medical.presentation.screens.report.medical;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class MedRecordReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		MedConfigTool.setContextTool(context);
    		LargeSelectHandler.handleLargeSelectParameter (data, context, "findMedRecordResult", "MedRecords");    		
    	}   
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
   }    
}
