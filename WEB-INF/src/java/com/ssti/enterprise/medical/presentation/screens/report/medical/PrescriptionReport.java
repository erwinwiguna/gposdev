package com.ssti.enterprise.medical.presentation.screens.report.medical;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class PrescriptionReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		LargeSelectHandler.handleLargeSelectParameter (data, context, "findPrescriptionResult", "Prescriptions");    		
    	}   
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
   }    
}
