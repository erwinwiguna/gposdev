package com.ssti.enterprise.medical.presentation.screens.report.medical;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalSalesTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;

public class RfeePackReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {
	    	    setParams(data);
	    	    String sCashier = data.getParameters().getString("Cashier","");	    	    
	    	    List vMST = MedicalSalesTool.findTrans(1,"",dStart,dEnd,"",sLocationID,i_PROCESSED,"",sCashier,"","");
	    	    context.put("vMST",vMST);
	    	    
	    	    List vRET = SalesReturnTool.findTrans(dStart, dEnd, sLocationID, sCashier, i_PROCESSED);
	    	    context.put("vRET",vRET);
    	    }
    		MedConfigTool.setContextTool(context);
    	}   
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
   }    
}
