package com.ssti.enterprise.medical.presentation.screens.report.medical;

import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

public class StockMovementByType extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	    	String sKeywords = data.getParameters().getString("Keywords");
	    	String sColor = data.getParameters().getString("Color");	    	
	    	int iSearchBy = data.getParameters().getInt("Condition");
	    	int iGroupBy = data.getParameters().getInt("GroupBy");
	    	int iType = data.getParameters().getInt("TransType");
	    	String sKategoriID= data.getParameters().getString("KategoriId");
	        
    		context.put ("InventoryTransactions", 
    			findData (dStart, dEnd, iSearchBy, iType, sKeywords, sColor, sKategoriID, sLocationID));
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Data LookUp Failed !");
    	}    
    }    
    
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _iGroupBy
	 * @param _sKeywords
	 * @param _sKategoriID
	 * @return List of InventoryTransaction
	 * @throws Exception
	 */
	public List findData (Date _dStart, 
						  Date _dEnd,
						  int _iCondition,
						  int _iInOut,
						  String _sKeywords,
						  String _sColor,
						  String _sKategoriID,
						  String _sLocationID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(InventoryTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
		oCrit.and(InventoryTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		
		if (StringUtil.isNotEmpty(_sKeywords))
		{
			
			if(_iCondition == 1)
			{
				oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_CODE, _sKeywords);				
			}
			if(_iCondition == 2)
			{
				oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_NAME,(Object) SqlUtil.like 
						 (ItemPeer.ITEM_NAME,_sKeywords,false,true), Criteria.CUSTOM);
			}
			else if(_iCondition == 3)
			{
				oCrit.add(InventoryTransactionPeer.TRANSACTION_NO, (Object)_sKeywords, Criteria.ILIKE);
			}
		}
		if (StringUtil.isNotEmpty(_sColor))
		{
			oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
			if (_sColor.contains(","))
			{
				String[] s = StringUtil.split(_sColor,",");
				for (int i = 0; i < s.length; i++)
				{
					if (i == 0) oCrit.add(ItemPeer.COLOR, s[i]);
					else oCrit.or(ItemPeer.COLOR, s[i]);
				}				
			}
			else
			{
				oCrit.add(ItemPeer.COLOR, _sColor);		
			}
		}
		if (_iInOut == 1) //IN
		{
			oCrit.add(InventoryTransactionPeer.QTY_CHANGES, 0, Criteria.GREATER_EQUAL);
		}
		else if (_iInOut == 2)
		{
			oCrit.add(InventoryTransactionPeer.QTY_CHANGES, 0, Criteria.LESS_THAN);			
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			
			oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			if(vKategori.size() > 0)
			{
				vKategori.add(_sKategoriID);
				oCrit.addIn(ItemPeer.KATEGORI_ID,vKategori);
			}
			else{
				oCrit.add(ItemPeer.KATEGORI_ID,_sKategoriID);
			}
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID);
		}
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
		return InventoryTransactionPeer.doSelect(oCrit);
	}
}