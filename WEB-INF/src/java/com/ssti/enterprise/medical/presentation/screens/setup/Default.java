package com.ssti.enterprise.medical.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.presentation.screens.transaction.MedSecureScreen;
import com.ssti.enterprise.medical.tools.MedicalAttributes;

public class Default extends MedSecureScreen implements MedicalAttributes
{
    public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);
    }
}