package com.ssti.enterprise.medical.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class MDSetupForm extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		try
		{
			if (sMode.equals("update") || sMode.equals("delete"))
			{
				sID = data.getParameters().getString("id");
				context.put("Doctor", DoctorTool.getDoctorByID(sID));
    		}
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}