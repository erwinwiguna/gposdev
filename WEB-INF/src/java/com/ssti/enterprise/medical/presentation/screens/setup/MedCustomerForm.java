package com.ssti.enterprise.medical.presentation.screens.setup;

import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerCfield;
import com.ssti.enterprise.pos.om.CustomerField;
import com.ssti.enterprise.pos.presentation.screens.setup.CustomerSecureScreen;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class MedCustomerForm extends CustomerSecureScreen
{
	Customer oCustomer = null;
	String sID = "";
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		try
		{			
			if (iOp == 1)//save
			{
				save(data);
			}
			else //view
			{
				sID = data.getParameters().getString("id");
				oCustomer = CustomerTool.getCustomerByID(sID);
			}
			context.put("Customer", oCustomer);
			
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx);
    	}
    }
    
    private void save(RunData data)
    {
		try
		{
			sID = data.getParameters().getString("id");
			oCustomer = CustomerTool.getCustomerByID(sID);
			if (oCustomer == null) 
			{
				oCustomer = new Customer();
				oCustomer.setCustomerId (IDGenerator.generateSysID());
				oCustomer.setAddDate(new Date());	
				oCustomer.setCreateBy(data.getUser().getName());
			}
			setProperties (oCustomer, data);
			CustomerTool.validateCustomer (oCustomer);		
			
			if(StringUtil.isEmpty(oCustomer.getDefaultTypeId())) oCustomer.setDefaultTypeId(PaymentTypeTool.getDefaultPaymentType().getId());
			if(StringUtil.isEmpty(oCustomer.getDefaultTermId())) oCustomer.setDefaultTermId(PaymentTermTool.getDefaultPaymentTerm().getId());
			if(StringUtil.isEmpty(oCustomer.getDefaultTaxId()))  oCustomer.setDefaultTaxId(TaxTool.getDefaultSalesTax().getId());
			
			oCustomer.setOpeningBalance(bd_ZERO);
			oCustomer.setLastUpdateLocationId(PreferenceTool.getLocationID());
			oCustomer.setLastUpdateBy(data.getUser().getName());
			oCustomer.setUpdateDate(new Date());
			
			oCustomer.save();
        	CustomerManager.getInstance().refreshCache(oCustomer);
        	
        	saveFields(data);
			
            data.setMessage (LocaleTool.getString(s_SAVE_SUCCESS));

		}
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	_oEx.printStackTrace();

        	if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_SAVE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_SAVE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }
    
	public void setProperties ( Customer _oCustomer, RunData data)
    {
    	try
    	{
	    	data.getParameters().setProperties(_oCustomer);
	    	_oCustomer.setAsDate(CustomParser.parseDate(data.getParameters().getString("AsDate")));
    	}
    	catch (Exception _oEx)
    	{
    		log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
	
	private void saveFields(RunData data)
		throws Exception
	{
		if(oCustomer !=  null)
		{
			List vCF = CustomerTool.getCFields(oCustomer.getCustomerTypeId());
			for(int i = 0; i < vCF.size(); i++)
			{
				CustomerCfield oCF = (CustomerCfield) vCF.get(i);				
				CustomerField oCFD = CustomerTool.getField(oCustomer.getCustomerId(), oCF.getId());
				if(oCFD == null)
				{
					oCFD = new CustomerField();				
					oCFD.setCustomerFieldId(IDGenerator.generateSysID());
				}				
				oCFD.setCustomerId(oCustomer.getId());
				oCFD.setCfieldId(oCF.getId());				
				oCFD.setFieldName(oCF.getFieldName());
				oCFD.setFieldValue(data.getParameters().getString(oCF.getId(),""));
				oCFD.setUpdateDate(new Date());
				oCFD.save();
			}
		}
	}
}
