package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

/**
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Item Code Lookup Screen Handler
 * <br>
 *
 * $@author  Author: Albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: MedItemCodeLookup.java,v $
 * Revision 1.1  2009/05/04 01:37:51  albert
 * *** empty log message ***
 *
 * Revision 1.23  2008/08/17 02:19:49  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class MedItemCodeLookupAjax extends MedItemCodeLookup
{   
	private static final Log log = LogFactory.getLog(MedItemCodeLookupAjax.class);

    public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);
    }
}
