package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-12-08
 * - change name from MedicineItemLookup
 * 
 * </pre><br>
 */
public class MedItemLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");
			String sDisplayItemID = data.getParameters().getString ("DisplayItemID", "");
			if (!sDisplayItemID.equals(""))
    		{
    			context.put ("ItemQtyDetails", InventoryLocationTool.getDataByItemID(sDisplayItemID));
    		}
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}    	
    }
}