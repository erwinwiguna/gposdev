package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-06-13
 * - change name from MedicineSalesView
 * 
 * </pre><br>
 */
public class MedOrderView extends TransactionSecureScreen
{
	protected static final String[] a_PERM = {"View Sales Order"};

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{    		
    		super.doBuildTemplate(data, context);
    		MedConfigTool.setContextTool(context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findMedOrderRes", "Orders");    		
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}