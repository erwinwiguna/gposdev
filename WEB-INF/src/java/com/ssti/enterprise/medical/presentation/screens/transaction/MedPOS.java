package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-06-14
 * - initial commit
 * 
 * </pre><br>
 */
public class MedPOS extends MedicalSalesTrans
{
	protected static final Log log = LogFactory.getLog(MedPOS.class);
	
    public void doBuildTemplate ( RunData data, Context context )
    {	    	
    	super.doBuildTemplate(data, context);    	    
    }
}
