package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.pos.presentation.screens.setup.CustomerView;

public class MedPatientView extends CustomerView
{    
	@Override
	public void doBuildTemplate(RunData data, Context context) {
		super.doBuildTemplate(data, context);
		System.out.println(data.getParameters());
		MedConfigTool.setContextTool(context);
	}
}
