package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.PowderItem;
import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.om.PrescriptionDetail;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

public class PowderTrans extends TransactionSecureScreen implements MedicalAttributes
{	
	private static final String[] a_PERM = {"View Prescription"};
	
	protected boolean isAuthorized(RunData data)
        throws Exception
    {
		return isAuthorized (data, a_PERM);
    }

    //start screen methods	
	private static final int i_OP_ADD_DETAIL  = 1;                                 
	private static final int i_OP_DEL_DETAIL  = 2;                                 
	private static final int i_OP_NEW_TRANS   = 3;                                 
	private static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	private static final int i_OP_SET_TRANS   = 6;
	private static final int i_OP_RECALCULATE = 7;
	
	private Prescription oPRS = null;
  	private PrescriptionDetail oPRSD = null;
    private List<PresPowderDetail> vPWDD = null;
    private HttpSession oSes;  
  
  	public void doBuildTemplate(RunData data, Context context)
  	{
  		super.doBuildTemplate(data, context);
  		
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			System.out.println("iOp: " + iOp);
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if ( iOp == i_OP_NEW_TRANS ) {
				createNewTrans ();
			}
			else if (iOp == i_OP_VIEW_TRANS ) {
				getData (data, context);
			}
			else if (iOp == i_OP_SET_TRANS ) {
				setPackaging (data, context);
			}
			else if (iOp == i_OP_RECALCULATE ) {
				recalculate (data);
			}			
			
		}
		catch (Exception _oEx) 
		{
		    _oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
		
		context.put(s_PWD, oSes.getAttribute (s_PWD));
    	context.put(s_PWDD, oSes.getAttribute (s_PWDD));
    	MedConfigTool.setContextTool(context);
    }


	protected void initSession ( RunData data )
	{	
		oSes = data.getSession();
		if ( oSes.getAttribute (s_PWD) == null ) 
		{
			oSes.setAttribute (s_PWD, new PrescriptionDetail());
			if ( oSes.getAttribute (s_PWDD) == null ) 
			{
				oSes.setAttribute (s_PWDD, new ArrayList<PresPowderDetail>());
			}
		}
		oPRS = (Prescription) oSes.getAttribute(s_PRS);
		oPRSD  = (PrescriptionDetail) oSes.getAttribute (s_PWD);
		vPWDD = (List) oSes.getAttribute (s_PWDD);
	}

    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
    	PowderTool.updateDetail(data, vPWDD);
    	PresPowderDetail oDetail = new PresPowderDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oDetail);	
			oDetail.setItemCode (oItem.getItemCode());
			oDetail.setItemPrice(oDetail.getItemPrice().setScale(0, BigDecimal.ROUND_UP));
			oDetail.setReqQty(data.getParameters().getBigDecimal("PwdReqQty", bd_ONE));
			if(MedConfigTool.isWarningItems(oItem.getColor())) //if NKT/PSI do NOT ROUND UP
			{
				oDetail.setBilledQty(oDetail.getUsedQty());
			}
			else
			{
				oDetail.setBilledQty(oDetail.getUsedQty().setScale(0, BigDecimal.ROUND_UP));			
			}
			double dBilled = oDetail.getBilledQty().doubleValue();
			double dSubTotal = dBilled * oDetail.getItemPrice().doubleValue();				
			oDetail.setSubTotal(new BigDecimal(dSubTotal).setScale(0, BigDecimal.ROUND_HALF_UP));
		}
		else
		{
			return;
		}				
		vPWDD.add (oDetail);
		PowderTool.setHeader(data, oPRSD, vPWDD);
		
		oSes.setAttribute (s_PWD, oPRSD); 
		oSes.setAttribute (s_PWDD, vPWDD);     
    }	
	    
    private void delDetailData ( RunData data )
    	throws Exception
    {
    	PowderTool.updateDetail(data, vPWDD);
    	int iDelNo = data.getParameters().getInt("No") - 1;
		if (vPWDD.size() > iDelNo) 
		{
			vPWDD.remove (iDelNo);
			oSes.setAttribute (s_PWDD, vPWDD);		
		}    	
		PowderTool.setHeader(data, oPRSD, vPWDD);
    }
    
    private void createNewTrans () 
    {
    	oPRSD = new PrescriptionDetail();
		vPWDD = new ArrayList();
		
    	oSes.setAttribute (s_PWD,  oPRSD);
		oSes.setAttribute (s_PWDD, vPWDD);
    }
        
 	private void getData ( RunData data, Context context ) 
 		throws Exception
    {
 		List vPRSD = (List) oSes.getAttribute(s_PRSD);

 		int iNo = data.getParameters().getInt("pwdno") - 1;
 		if (iNo >= 0 && iNo < vPRSD.size())
 		{
	 		oPRSD = (PrescriptionDetail) vPRSD.get(iNo);
	 		if (oPRSD != null)
	 		{
	 			vPWDD = oPRSD.getPowderDetail();
	 			String sID = oPRSD.getPrescriptionDetailId();
	 			if (StringUtil.isNotEmpty(sID))
	 			{
	 				vPWDD = PowderTool.getByPresDetID(sID, null);
	 			}
	 		}
 		}
 		oSes.setAttribute (s_PWD,  oPRSD);
 		oSes.setAttribute (s_PWDD, vPWDD);
    }
 	
	private void setPackaging(RunData data, Context context) 
		throws Exception
	{
		recalculate (data);
		
    	String sID = data.getParameters().getString("PowderItemId");    	
    	BigDecimal bdResQty = data.getParameters().getBigDecimal("ResultQty");
    	BigDecimal bdReqQty = data.getParameters().getBigDecimal("ReqQty",bd_ONE);
    	double dResQty = bdResQty.doubleValue();     	
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{    	    	
    			PowderItem oPI = PowderTool.getPowderItemByID(sID);
    			if (oPI != null)
    			{
    				PowderTool.updateDetail(data, vPWDD);    				
    				Item oItem = ItemTool.getItemByID(oPI.getItemId());
    				PresPowderDetail oDetail = new PresPowderDetail();
    				oDetail.setItemCode(oItem.getItemCode());
    				oDetail.setItemId(oItem.getItemId());
    				oDetail.setItemName(oItem.getItemName());
    				oDetail.setItemPrice(oItem.getItemPrice());
    				oDetail.setUnitId(oItem.getUnitId());
    				oDetail.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));
    				oDetail.setQty(bd_ONE);
    				oDetail.setQtyBase(bd_ONE);
    				oDetail.setUsedQty(bdResQty);    				
    				oDetail.setReqQty(bdReqQty);
    				
    				Tax oTax = TaxTool.getTaxByID(oItem.getTaxId());
    				if (oTax == null) oTax = TaxTool.getDefaultSalesTax();
    				if (oTax != null)
    				{
    					oDetail.setTaxId(oTax.getTaxId());
    					oDetail.setTaxAmount(oTax.getAmount());
    				}
    				
    				double dSub = oItem.getItemPrice().doubleValue() * dResQty;
    				oDetail.setSubTotal(new BigDecimal(dSub));
    				
    				vPWDD.add(oDetail);
    				oSes.setAttribute (s_PWDD, vPWDD);		

    				System.out.println("dResQty: " + dResQty);
					System.out.println("Rfee " + data.getParameters().getDouble("Rfee"));
					System.out.println("Pack " + data.getParameters().getDouble("Packaging"));
    				
    				double dRFee = 0;
    				double dPack = 0;
    				if(dResQty > 0)
    				{
    					dRFee = PowderTool.calculateRFee(oPI, dResQty);
    					dPack = PowderTool.calculatePack(oPI, dResQty);    					
    				}    				
    				oPRSD.setRfee(new BigDecimal(dRFee).toString());
    				oPRSD.setPackaging(new BigDecimal(dPack).toString());    				
    				
    				oPRSD.setUnitId(oItem.getUnitId());
    				oPRSD.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));
    				oPRSD.setTaxId(oItem.getTaxId());
    				oPRSD.setTaxAmount(TaxTool.getAmountByID(oItem.getTaxId()));
    				
    				PowderTool.setHeader(data, oPRSD, vPWDD);
    				
    				//validate Packaging Qty
    				String sLocID = PreferenceTool.getLocationID();
    				if (oPRS != null) sLocID = oPRS.getLocationId();
    				double dQoH = InventoryLocationTool.getInventoryOnHand(oItem.getItemId(), sLocID, null).doubleValue();
    				if (dResQty > dQoH)
    				{
    					String sCode = oItem.getItemCode();
    					data.setMessage("WARNING: " + sCode + " " + LocaleTool.getString("qty_oh_not_avail") + 
    									" (" + CustomFormatter.formatNumber(dQoH) + ")");
    				}
    			}
    		}
    		catch (Exception _oEx) 
    		{
    			_oEx.printStackTrace();
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    	else
    	{
    		createNewTrans();
    	}
	}
	
    protected void recalculate ( RunData data ) 
		throws Exception
	{    	
    	PowderTool.updateDetail(data, vPWDD);
		PowderTool.setHeader(data, oPRSD, vPWDD);
		
		oSes.setAttribute (s_PWD, oPRSD); 
		oSes.setAttribute (s_PWDD, vPWDD);     
	}	
}
