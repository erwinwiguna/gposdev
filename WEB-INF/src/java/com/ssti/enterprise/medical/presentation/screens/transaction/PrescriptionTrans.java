package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.om.PrescriptionDetail;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.medical.tools.PrescriptionTool;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-07-11
 * - change copy Trans, update prescription and powder item price with latest price
 * 
 * 2016-12-08
 * - change method newFromMR, add PharmacyLoc options in ClinicConfig
 *   if pharmacyLOC is empty use same loc with MR 
 * 
 * </pre><br>
 */
public class PrescriptionTrans extends TransactionSecureScreen implements MedicalAttributes
{	
	private static final String[] a_PERM = {"View Prescription"};
	
	protected boolean isAuthorized(RunData data)
        throws Exception
    {
		return isAuthorized (data, a_PERM);
    }

    //start screen methods	
	private static final int i_OP_ADD_DETAIL  = 1;                                 
	private static final int i_OP_DEL_DETAIL  = 2;                                 
	private static final int i_OP_NEW_TRANS   = 3;                                 
	private static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	private static final int i_OP_COPY_TRANS  = 6;
	private static final int i_OP_PWD_TRANS   = 7;
	private static final int i_OP_UPDATE_PWD  = 8;
	
	private static final int i_OP_NEW_FROM_MR = 9;
	  	
  	private Prescription oPRS = null;
    private List vPRSD = null;
    private HttpSession oSes;  
  
  	public void doBuildTemplate(RunData data, Context context)
  	{
  		super.doBuildTemplate(data, context);
  		
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if ( iOp == i_OP_NEW_TRANS ) {
				createNewTrans ();
			}
			else if (iOp == i_OP_VIEW_TRANS ) {
				getData (data, context);
			}
			else if (iOp == i_OP_COPY_TRANS && !b_REFRESH) {
				copyTrans (data);
			}
			else if (iOp == i_OP_PWD_TRANS && !b_REFRESH) {
				addPwdTrans (data);
			}
			else if (iOp == i_OP_UPDATE_PWD && !b_REFRESH) {
				updatePwdTrans (data);
			}
			else if (iOp == i_OP_NEW_FROM_MR && !b_REFRESH) {
				newFromMR (data);
			}
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx.getMessage(), _oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
		
		context.put(s_PRS, oSes.getAttribute (s_PRS));
    	context.put(s_PRSD, oSes.getAttribute (s_PRSD));
    	MedConfigTool.setContextTool(context);
    }
    
	protected void initSession ( RunData data )
	{	
		oSes = data.getSession();
		if ( oSes.getAttribute (s_PRS) == null ) 
		{
			oSes.setAttribute (s_PRS, new Prescription());
			if ( oSes.getAttribute (s_PRSD) == null ) 
			{
				oSes.setAttribute (s_PRSD, new ArrayList());
			}
		}
		oPRS  = (Prescription) oSes.getAttribute (s_PRS);
		vPRSD = (List) oSes.getAttribute (s_PRSD);
	}

    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
    	PrescriptionTool.updateDetail(data, vPRSD);
    	PrescriptionDetail oDetail = new PrescriptionDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oDetail);	
			oDetail.setItemCode (oItem.getItemCode());
			oDetail.setDescription(data.getParameters().getString("ItemName"));
			oDetail.setPrescriptionType(i_PRES_NORMAL);
			
			if (!isExist (oDetail))
			{
				vPRSD.add (oDetail);
			}
			PrescriptionTool.setHeader(data, oPRS, vPRSD);
			oSes.setAttribute (s_PRS, oPRS); 
			oSes.setAttribute (s_PRSD, vPRSD);     
		}
    }	
	    
    private boolean isExist(PrescriptionDetail _oTD) 
    	throws Exception 
    {
    	for (int i = 0; i < vPRSD.size(); i++) 
    	{
    		PrescriptionDetail oExistTD = (PrescriptionDetail) vPRSD.get (i);
    		if ( oExistTD.getItemId().equals(_oTD.getItemId()) && 
    		     oExistTD.getUnitId().equals(_oTD.getUnitId()) &&
    		     oExistTD.getTaxId().equals(_oTD.getTaxId())&&
    		     oExistTD.getItemPrice().doubleValue() == _oTD.getItemPrice().doubleValue())
    		{
				mergeData(oExistTD, _oTD);
    			return true;    			
    		}		
    	}
    	return false;
	}

    protected void mergeData (PrescriptionDetail _oOldTD, PrescriptionDetail _oNewTD) 
		throws Exception
	{		
		double dQty 		= _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();

		double dTotalSales  = dQty * _oNewTD.getItemPrice().doubleValue();
		double dTaxAmount   = _oNewTD.getTaxAmount().doubleValue()/100;		
		double dTotalTax 	= dTotalSales * dTaxAmount;
		double dSubTotal 	= dTotalSales; //+ dTotalTax;
		
		//return RFEE to percentage or default mode so it will be recalculated 
		//by set header properties
		_oOldTD.setRfee			( _oNewTD.getRfee());
		_oOldTD.setPackaging	( _oNewTD.getPackaging());   
		
		_oOldTD.setQty 			( new BigDecimal (dQty));    	
		_oOldTD.setQtyBase		( UnitTool.getBaseQty(_oOldTD.getItemId(), _oOldTD.getUnitId(), _oOldTD.getQty()));
		_oOldTD.setTaxAmount 	( new BigDecimal (dTaxAmount * 100)  );    			
		_oOldTD.setSubTotal     ( new BigDecimal (dSubTotal)    );
		_oNewTD = null;
	}
    
	private void delDetailData ( RunData data )
    	throws Exception
    {
    	PrescriptionTool.updateDetail(data, vPRSD);
    	int iDelNo = data.getParameters().getInt("No") - 1;
		if (vPRSD.size() > iDelNo) 
		{
			vPRSD.remove (iDelNo);
			oSes.setAttribute (s_PRSD, vPRSD);		
		}    	
		oPRS.setPresDiscount(""); //reset pres discount so it will be calculated automatically
		PrescriptionTool.setHeader(null, oPRS, vPRSD);
    }
    
    private void createNewTrans () 
    {
    	oPRS = new Prescription();
		vPRSD = new ArrayList();
		
    	oSes.setAttribute (s_PRS,  oPRS);
		oSes.setAttribute (s_PRSD, vPRSD);
    }
        
 	private void getData (RunData data, Context context) 
 		throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    			oSes.setAttribute (s_PRS, PrescriptionTool.getHeaderByID (sID, null));
				oSes.setAttribute (s_PRSD, PrescriptionTool.getDetailsByID (sID, null));
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }

	/**
	 * @param data
	 */
	private void copyTrans(RunData data) 
		throws Exception
	{
		try
		{
			String sTransID = data.getParameters().getString("TransId");
	    	PrescriptionTool.updateDetail(data, vPRSD);
			
			if (StringUtil.isNotEmpty (sTransID))
			{			
				Prescription oOld = PrescriptionTool.getHeaderByID(sTransID, null);
				List vTrans = PrescriptionTool.getDetailsByID(sTransID, null);
				vPRSD = new ArrayList(vTrans.size());
				for (int i = 0; i < vTrans.size(); i++)
				{
					PrescriptionDetail oDet = (PrescriptionDetail) vTrans.get(i);
					PrescriptionDetail oNewDet = new PrescriptionDetail();
					BeanUtil.setBean2BeanProperties(oDet, oNewDet);
					oNewDet.setNew(true);
					oNewDet.setModified(true);
					oNewDet.setPrescriptionDetailId("");
					if (oNewDet.getPrescriptionType() == i_PRES_POWDER)
					{
						List vPWD = PowderTool.getByPresDetID(oDet.getPrescriptionDetailId(), null);
						BigDecimal bdPDSub = bd_ZERO;
						for (int j = 0; j < vPWD.size(); j++)
						{
							PresPowderDetail oPWDD = (PresPowderDetail) vPWD.get(j);
							oPWDD.setNew(true);
							oPWDD.setModified(true);
							oPWDD.setPresPowderDetailId("");
							oPWDD.setItemPrice(MedConfigTool.getPrice(oPWDD.getItem(),oOld.getCustomerId(),oOld.getLocationId(),i_PRES_POWDER));
							oPWDD.setSubTotal(oPWDD.getBilledQty().multiply(oPWDD.getItemPrice()));
							bdPDSub = bdPDSub.add(oPWDD.getSubTotal());
						}
						if(oNewDet.getResultQty().doubleValue() > 0)
						{
							BigDecimal bdPrice = bdPDSub.divide(oNewDet.getResultQty(),0,BigDecimal.ROUND_HALF_UP);
							oNewDet.setItemPrice(bdPrice);
							oNewDet.setSubTotal(oNewDet.getQty().multiply(oNewDet.getItemPrice()));
							oNewDet.setTotal(oNewDet.getSubTotal());
						}
						oNewDet.setPowderDetail(vPWD);
					}
					else
					{
						oNewDet.setItemPrice(MedConfigTool.getPrice(oNewDet.getItem(),oOld.getCustomerId(),oOld.getLocationId(),i_PRES_NORMAL));
						oNewDet.setSubTotal(oNewDet.getQty().multiply(oNewDet.getItemPrice()));						
					}
					vPRSD.add(oNewDet);
				}
				
				if (oOld != null)
				{
					oPRS.setCustomerId(oOld.getCustomerId());
					oPRS.setPatientName(oOld.getPatientName());
					oPRS.setPatientAge(oOld.getPatientAge());
					oPRS.setPatientAddress(oOld.getPatientAddress());
					oPRS.setPatientWeight(oOld.getPatientWeight());
					oPRS.setDoctorId(oOld.getDoctorId());
					oPRS.setRemark(oOld.getRemark());
					oPRS.setPresDiscount(oOld.getPresDiscount());
				}
				
				oSes.setAttribute (s_PRS, oPRS);
				oSes.setAttribute (s_PRSD, vPRSD);
			}
			//do not pass data because some parametes value set to oPRS from oOld
			PrescriptionTool.setHeader(null, oPRS, vPRSD);
			oPRS.setPrescriptionId(""); //reset PRESID
		}
		catch (Exception _oEx)
		{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage (), _oEx);
		}
	}
	
	private void addPwdTrans(RunData data) 
		throws Exception 
	{
		PrescriptionTool.updateDetail(data, vPRSD);
		PrescriptionDetail oDetail = (PrescriptionDetail) oSes.getAttribute(s_PWD);
		if (oDetail != null)
		{    	
			log.debug(oDetail);
			
			vPRSD.add (oDetail);
			PrescriptionTool.setHeader(data, oPRS, vPRSD);
		}
		oSes.setAttribute (s_PRS, oPRS); 
		oSes.setAttribute (s_PRSD, vPRSD);   
	}
	
	private void updatePwdTrans(RunData data) 
		throws Exception 
	{
		int iNo = data.getParameters().getInt("pwdno") - 1;
		//PrescriptionTool.updateDetail(data, vPRSD);
		PrescriptionDetail oDetail = (PrescriptionDetail) oSes.getAttribute(s_PWD);
		if (oDetail != null)
		{    				
			System.out.println("update PWD no " + iNo + " with : " + oDetail);
			
			if (vPRSD.size() > iNo) vPRSD.set(iNo, oDetail);
			PrescriptionTool.setHeader(data, oPRS, vPRSD);
		}
		oSes.setAttribute (s_PRS, oPRS); 
		oSes.setAttribute (s_PRSD, vPRSD);   
	}
	
	private void newFromMR(RunData data) 
		throws Exception
	{		
	}
}
