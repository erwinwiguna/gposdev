package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class PrescriptionView extends PrescriptionTrans
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		MedConfigTool.setContextTool(context);
    		LargeSelectHandler.handleLargeSelectParameter (data, context, "findPrescriptionResult", "Prescriptions");    		
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}