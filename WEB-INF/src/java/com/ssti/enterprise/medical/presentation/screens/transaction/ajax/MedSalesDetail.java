package com.ssti.enterprise.medical.presentation.screens.transaction.ajax;

import com.ssti.enterprise.medical.presentation.screens.transaction.MedicalSalesTrans;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * MedSalesDetail AJAX Processor Screen Class 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MedSalesDetail.java,v 1.20 2009/05/04 02:02:36 albert Exp $ <br>
 *
 * <pre>

 * </pre><br>
 */
public class MedSalesDetail extends MedicalSalesTrans
{    

}
