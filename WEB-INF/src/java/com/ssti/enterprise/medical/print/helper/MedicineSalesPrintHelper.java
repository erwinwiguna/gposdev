package com.ssti.enterprise.medical.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalSalesTool;
import com.ssti.enterprise.medical.tools.PrescriptionTool;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.PrintingLogTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-07-01
 * - remove invoice message to comply to backdate compatibility
 * 
 * </pre><br>
 */
public class MedicineSalesPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
	private static Log log = LogFactory.getLog(MedicineSalesPrintHelper.class);

	private static final String s_TR = "mst";
	private static final String s_TD = "mstDet";
		   
	private MedicineSales oTR = null;
	private List vTD = null;
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = "";
    	String[] aResult =  (String[]) oParam.get(s_PARAM_ID);
    	if (aResult != null && aResult.length > 0)
    	{
    		sID = aResult[0];
    	}
    	
    	//getString(oParam, s_PARAM_ID);
		String sUserName = "";
		aResult =  (String[]) oParam.get(s_PARAM_USER);
    	if (aResult != null && aResult.length > 0)
    	{
    		sUserName = aResult[0];
    	}
		
		if (_oSession != null && !StringUtils.equalsIgnoreCase(sUserName, "nosession"))
		{
			oTR = (MedicineSales) _oSession.getAttribute(s_TR);
			vTD = (List) _oSession.getAttribute(s_TD);
		}
		//get transaction data by id		
		if(oTR == null) 
		{
			oTR = MedicalSalesTool.getHeaderByID (sID);
			vTD = MedicalSalesTool.getDetailsByID (sID);
		}
        logPrintingProcess (oTR,  sUserName);
        
        prepareContext();
		oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        MedConfigTool.setContextTool(oCtx);
        
        checkTemplate (oParam);
        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
        {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML)
        {
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF)
        {
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        return oWriter.toString();
    }
 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = "";
    	String[] aResult =  (String[]) oParam.get(s_PARAM_ID);
    	if (aResult != null && aResult.length > 0)
    	{
    		sID = aResult[0];
    	}
    	
    	//getString(oParam, s_PARAM_ID);
		String sUserName = "";
		aResult =  (String[]) oParam.get(s_PARAM_USER);
    	if (aResult != null && aResult.length > 0)
    	{
    		sUserName = aResult[0];
    	}
				
		if (StringUtil.isNotEmpty(sUserName))
		{
			logPrintingProcess (oTR,  sUserName);
		}
		
		if (_oSession != null)
		{
			oTR = (MedicineSales) _oSession.getAttribute(s_TR);
			vTD = (List) _oSession.getAttribute(s_TD);
		}
		//get transaction data by id		
		else if (_oSession == null || oTR == null) 
		{
			oTR = MedicalSalesTool.getHeaderByID (sID);
			vTD = MedicalSalesTool.getDetailsByID (sID);
		}
		
        oCtx.put("medicinesales", MedicalSalesTool.getInstance());
        oCtx.put("prescription", PrescriptionTool.getInstance());

		prepareContext();
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }    
    
    /**
     * 
     * @param _oTR
     * @param _sUserName
     * @throws Exception
     */
    private void logPrintingProcess (MedicineSales _oTR, String _sUserName)	
    	throws Exception
    {
		if (_oTR != null)
		{
			MedicalSalesTool.updatePrintTimes (_oTR);
			
			SalesTransaction oTR = TransactionTool.getHeaderByID(_oTR.getMedicineSalesId());
			TransactionTool.updatePrintTimes(oTR);
			PrintingLogTool.createPrintingLogFromSalesTransaction(oTR, _sUserName);
		}
    }
    
    /**
     * 
     * @throws Exception
     */
    private void prepareContext()
    	throws Exception
    {		        
    	s_HTML_TPL_NAME = "/print/MedInvoiceHTML.vm";        
        s_PDF_TPL_NAME = "/print/MedInvoicePDF.vm";
        s_TXT_TPL_NAME = "/print/MedInvoiceTXT.vm";

//      load in vm from butil        
//    	oCtx.put("invoicemessage", new InvoiceMessageTool());
//    	
//        if (oTR != null)
//        {
//        	oCtx.put("invoice_msg",  CustomerTool.getInvoiceMessageByID (oTR.getCustomerId()));
//        }
    }

}
