package com.ssti.enterprise.medical.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.PrescriptionTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

public class PrescriptionPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
	private static Log log = LogFactory.getLog(PrescriptionPrintHelper.class);
		   
	private Prescription oTR = null;
	private List vTD = null;

		
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		String sUserName = getString(oParam, s_PARAM_USER);
		
		oTR = PrescriptionTool.getHeaderByID (sID, null);
		vTD = PrescriptionTool.getDetailsByID (sID, null);
		
        prepareContext();
		oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        MedConfigTool.setContextTool(oCtx);
        
        checkTemplate (oParam);
        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
        {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF)
        {
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML)
        {
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        return oWriter.toString();
    }
 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);
		String sUserName = getString(oParam, s_PARAM_USER);

		oTR = PrescriptionTool.getHeaderByID (sID, null);
		vTD = PrescriptionTool.getDetailsByID (sID, null);
		
		prepareContext();
		MedConfigTool.setContextTool(oCtx);
		checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }
    
    /**
     * 
     * @throws Exception
     */
    private void prepareContext()
    	throws Exception
    {		        
        s_PDF_TPL_NAME = "/print/PrescriptionPDF.vm";
        s_TXT_TPL_NAME = "/print/PrescriptionTXT.vm";
        s_HTML_TPL_NAME = "/print/PrescriptionHTML.vm";
    }
}
