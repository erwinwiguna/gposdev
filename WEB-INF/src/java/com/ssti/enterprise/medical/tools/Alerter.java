package com.ssti.enterprise.medical.tools;

import java.util.List;

import com.ssti.framework.networking.SocketTool;

public class Alerter implements Runnable 
{
	static final int i_ALERTER_PORT = 8088;
	
	List v_HOSTS = null;
	String s_MSG = "";
		
	public Alerter(List hosts, String msg)
	{
		v_HOSTS = hosts;
		s_MSG = msg;
	}	
	
	public void run() 
	{
		SocketTool.writeToSocket(v_HOSTS,i_ALERTER_PORT,s_MSG);		
	}

}
