package com.ssti.enterprise.medical.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.om.CustomerMargin;
import com.ssti.enterprise.medical.om.CustomerMarginPeer;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PowderTool.java,v 1.1 2009/05/04 01:38:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: PowderTool.java,v $
 * Revision 1.1  2009/05/04 01:38:15  albert
 * *** empty log message ***
 *
 * 2018-05-28
 * - new CustomerMargin schema
 * - new method findData -> query DB, findMargin -> findAll then filter
 * - deprecate old method and create new method for applyRFee & applyPack
 * 
 * </pre><br>
 */
public class CustomerMarginTool extends BaseTool 
{	
	static final Log log = LogFactory.getLog(CustomerMarginTool.class);
	
	static CustomerMarginTool instance = null;
	
	public static synchronized CustomerMarginTool getInstance() 
	{
		if (instance == null) instance = new CustomerMarginTool();
		return instance;
	}		

	public static List getAllData() 
		throws Exception 
	{
		return MedConfigManager.getInstance().getMargins();
	}		

	public static CustomerMargin getByID(String _sID) 
		throws Exception 
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerMarginPeer.CUSTOMER_MARGIN_ID, _sID);
		List vData = CustomerMarginPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (CustomerMargin) vData.get(0);
		}
		return null;
	}		

	/**
	 * @deprecated
	 * @param _sCustID
	 * @param _sColor
	 * @return
	 * @throws Exception
	 */
	public static CustomerMargin getByCustID(String _sCustID, String _sColor) 
		throws Exception 
	{
		String sMargin = "";
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerMarginPeer.CUSTOMER_ID, _sCustID);		
		List vData = CustomerMarginPeer.doSelect(oCrit);
		for (int i = 0; i < vData.size(); i++)
		{
			CustomerMargin oCM = (CustomerMargin) vData.get(i);
			if ((StringUtil.isNotEmpty(oCM.getColor()) && StringUtil.contains(oCM.getColor(), _sColor)) || 
				 StringUtil.isEmpty(oCM.getColor()))
			{
				return oCM;
			}
		}
		return null;
	}
	
	/**
	 * Find margin data by supplied parameters 
	 * 
	 * @param _sCustCode
	 * @param _sCTypeID
	 * @param _sLocID
	 * @param _oItem
	 * @param _iTxType
	 * @return
	 * @throws Exception
	 */
	public static List findData(String _sCustCode, 
							    String _sCTypeID, 
							    String _sLocID, 
							    Item _oItem, 
							    int _iTxType) 
		throws Exception 
	{        
		Criteria oCrit = new Criteria ();
        
        Criteria.Criterion oLoc1 = oCrit.getNewCriterion(CustomerMarginPeer.LOCATION_IDS, "%"+_sLocID+"%", Criteria.ILIKE);
        Criteria.Criterion oLoc2 = oCrit.getNewCriterion(CustomerMarginPeer.LOCATION_IDS, "", Criteria.EQUAL);

        Criteria.Criterion oCus1 = oCrit.getNewCriterion(CustomerMarginPeer.CUSTOMER_ID, "%"+_sCustCode+"%", Criteria.ILIKE);
        Criteria.Criterion oCus2 = oCrit.getNewCriterion(CustomerMarginPeer.CUSTOMER_ID, "", Criteria.EQUAL);

        Criteria.Criterion oCT1 = oCrit.getNewCriterion(CustomerMarginPeer.CUSTOMER_TYPE_ID, _sCTypeID ,Criteria.EQUAL);
        Criteria.Criterion oCT2 = oCrit.getNewCriterion(CustomerMarginPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);

        Criteria.Criterion oTP1 = oCrit.getNewCriterion(CustomerMarginPeer.TRANS_TYPE, _iTxType ,Criteria.EQUAL);
        Criteria.Criterion oTP2 = oCrit.getNewCriterion(CustomerMarginPeer.TRANS_TYPE, -1 ,Criteria.EQUAL);

        oCrit.add(oLoc1.or(oLoc2))                
             .and(oCus1.or(oCus2))
             .and(oTP1.or(oTP2))
        	 .and(oCT1.or(oCT2));

        if(_oItem != null)
        {        	
	        String sKatID = _oItem.getKategoriId();
	        List vKat = KategoriTool.getParentIDList(sKatID, null);        

	        Criteria.Criterion oKat1 = oCrit.getNewCriterion(CustomerMarginPeer.CATEGORY_ID, vKat, Criteria.IN);
	        Criteria.Criterion oKat2 = oCrit.getNewCriterion(CustomerMarginPeer.CATEGORY_ID, "", Criteria.EQUAL);
	        
	        String sItemCode = _oItem.getItemCode();
	        Criteria.Criterion oItem1 = oCrit.getNewCriterion(CustomerMarginPeer.ITEM_CODES, sItemCode, Criteria.EQUAL);        //002 i.e we want disc for PLU 002
	        Criteria.Criterion oItem2 = oCrit.getNewCriterion(CustomerMarginPeer.ITEM_CODES, "%," + sItemCode, Criteria.ILIKE); //001,002
	        Criteria.Criterion oItem3 = oCrit.getNewCriterion(CustomerMarginPeer.ITEM_CODES, sItemCode + ",%", Criteria.ILIKE); //002,003
	        Criteria.Criterion oItem4 = oCrit.getNewCriterion(CustomerMarginPeer.ITEM_CODES, "%," + sItemCode + ",%", Criteria.ILIKE); //001,002,003            
	        Criteria.Criterion oItem5 = oCrit.getNewCriterion(CustomerMarginPeer.ITEM_CODES, "", Criteria.EQUAL);
	
	        String sColor = "%"+_oItem.getColor()+"%";
	        Criteria.Criterion oCL1 = oCrit.getNewCriterion(CustomerMarginPeer.COLOR, (Object)sColor ,Criteria.ILIKE);
	        Criteria.Criterion oCL2 = oCrit.getNewCriterion(CustomerMarginPeer.COLOR, "" ,Criteria.EQUAL);
	        
	        oCrit.and(oCL1.or(oCL2))
	        	 .and(oKat1.or(oKat2))
	        	 .and(oItem1.or(oItem2).or(oItem3).or(oItem4).or(oItem5));
        }
        oCrit.addDescendingOrderByColumn(CustomerMarginPeer.PRIORITY);
        log.debug("Find Margin Criteria:" + oCrit);
        return CustomerMarginPeer.doSelect(oCrit);
	}
	
	/**
	 * faster method than query, scan from existing cached all data
	 * 
	 * @param _sCustCode
	 * @param _sCTypeID
	 * @param _sLocID
	 * @param _oItem
	 * @param _iTxType
	 * @return
	 * @throws Exception
	 */
	public static CustomerMargin findMargin(String _sCustID,  
		    					  			String _sLocID, 
		    					  			Item _oItem, 
		    					  			int _iTxType) 
		throws Exception 
	{
		List v = getActiveMargin(_sCustID, _sLocID, _oItem, _iTxType);
		if(v.size() > 0)
		{
			return (CustomerMargin) v.get(0); //return first priority
		}
		return null;
	}
	
	/**
	 * 
	 * @param _sCustID
	 * @param _sLocID
	 * @param _oItem
	 * @param _iTxType
	 * @return
	 * @throws Exception
	 */
	public static String getMargin(String _sCustID, String _sLocID, Item _oItem, int _iTxType) 
		throws Exception 
	{
		
		String sMargin = "";
		if(_oItem != null)
		{			
			CustomerMargin oCM = findMargin(_sCustID, _sLocID, _oItem, _iTxType);
			if(oCM != null && StringUtil.isNotEmpty(oCM.getMargin())) sMargin = oCM.getMargin();
		}
		sMargin = StringUtil.replace(sMargin, "%","");
		return sMargin;
	}	
	
	/**
	 * @deprecated
	 * @param _sCustID
	 * @param _sColor
	 * @return
	 * @throws Exception
	 */
	public static boolean applyRfee(String _sCustID, String _sColor) 
		throws Exception 
	{
		boolean bApplied = true;
		CustomerMargin oCM = getByCustID(_sCustID, _sColor);
		//CustomerMargin oCM = findMargin(_sCustID, _sLocID, _oItem, _iTxType);
		if (oCM != null && StringUtil.isNotEmpty(oCM.getMargin()))
		{
			bApplied = oCM.getRfeeApplied();
		}
		return bApplied;
	}
	
	/**
	 * 
	 * @param _sCustID
	 * @param _sLocID
	 * @param _oItem
	 * @param _iTxType
	 * @return
	 * @throws Exception
	 */
	public static boolean applyRfee(String _sCustID, String _sLocID, Item _oItem, int _iTxType) 
		throws Exception 
	{
		boolean bApplied = true;
		CustomerMargin oCM = findMargin(_sCustID, _sLocID, _oItem, _iTxType);
		if (oCM != null && StringUtil.isNotEmpty(oCM.getMargin()))
		{
			bApplied = oCM.getRfeeApplied();
		}
		return bApplied;
	}

	/**
	 * @deprecated
	 * @param _sCustID
	 * @param _sColor
	 * @return
	 * @throws Exception
	 */
	public static boolean applyPack(String _sCustID, String _sColor) 
		throws Exception 
	{
		boolean bApplied = true;
		CustomerMargin oCM = getByCustID(_sCustID, _sColor);
		//CustomerMargin oCM = findMargin(_sCustID, _sLocID, _oItem, _iTxType);
		if (oCM != null && StringUtil.isNotEmpty(oCM.getMargin()))
		{
			bApplied = oCM.getPackApplied();
		}
		return bApplied;
	}
	
	/**
	 * 
	 * @param _sCustID
	 * @param _sLocID
	 * @param _oItem
	 * @param _iTxType
	 * @return
	 * @throws Exception
	 */
	public static boolean applyPack(String _sCustID, String _sLocID, Item _oItem, int _iTxType) 
		throws Exception 
	{
		boolean bApplied = true;
		CustomerMargin oCM = findMargin(_sCustID, _sLocID, _oItem, _iTxType);
		if (oCM != null && StringUtil.isNotEmpty(oCM.getMargin()))
		{
			bApplied = oCM.getPackApplied();
		}
		return bApplied;
	}
		
	public static String getTypeDesc(int _iType)
	{
		return MedicalSalesTool.getTxTypeDesc(_iType);
	}
	
	/**
	 * faster method than query, scan from existing cached all data
	 * 
	 * @param _sCustCode
	 * @param _sCTypeID
	 * @param _sLocID
	 * @param _oItem
	 * @param _iTxType
	 * @return
	 * @throws Exception
	 */
	public static List getActiveMargin(String _sCustID,  
		    					  	   String _sLocID, 
		    					  	   Item _oItem, 
		    					  	   int _iTxType) 
		throws Exception 
	{
		String sCustCode = "";
		String sCTypeID = "";
		if(StringUtil.isNotEmpty(_sCustID))
		{
			Customer oCust = CustomerTool.getCustomerByID(_sCustID);
			if(oCust != null)
			{
				sCustCode = oCust.getCustomerCode();
				sCTypeID = oCust.getCustomerTypeId();
			}
		}
		String sKats = "";
		String sCode = "";
		String sColor = "";
		if(_oItem != null) {
			List vKat = KategoriTool.getParentIDList(_oItem.getKategoriId(), null);  
			sKats = SqlUtil.convertToINMode(vKat);
			sCode = _oItem.getItemCode();
			sColor = _oItem.getColor();
		}
		
		List v = getAllData();
		List r = new ArrayList(v.size());
		Iterator iter = v.iterator();
		while(iter.hasNext())
		{
			CustomerMargin oCM = (CustomerMargin) iter.next();
			
			//log.debug("Margin check: " + StringUtil.equals(oCM.getCustTypeId(),"")  + " " + StringUtil.empty(sCTypeID) + " " + StringUtil.isEqual(oCM.getCustomerTypeId(), sCTypeID)  );
			
			//validate item only
			if( (StringUtil.empty(sCTypeID) || (StringUtil.equals(oCM.getCustTypeId(),"") || (!StringUtil.empty(sCTypeID) && StringUtil.isEqual(oCM.getCustomerTypeId(), sCTypeID)))) &&
				(StringUtil.empty(sCTypeID) || (StringUtil.equals(oCM.getCustomerId(),"") || (!StringUtil.empty(sCustCode) && StringUtil.containsIgnoreCase(oCM.getCustomerId(), sCustCode)))) &&
				(StringUtil.empty(_sLocID)  || (StringUtil.equals(oCM.getLocationIds(),"")|| (!StringUtil.empty(_sLocID) && StringUtil.containsIgnoreCase(oCM.getLocationIds(), _sLocID)))) &&								
				(StringUtil.empty(sKats)    || (StringUtil.equals(oCM.getCategoryId(),"") || (!StringUtil.empty(sKats) && StringUtil.containsIgnoreCase(sKats, oCM.getCategoryId())))) &&
				(StringUtil.empty(sCode)    || (StringUtil.equals(oCM.getItemCodes(),"")  || (!StringUtil.empty(sCode) && StringUtil.containsIgnoreCase(oCM.getItemCodes(), sCode)))) &&
				(StringUtil.empty(sColor)   || (StringUtil.equals(oCM.getColor(),"") 	    || (!StringUtil.empty(sColor) && StringUtil.containsIgnoreCase(oCM.getColor(), sColor)))) &&
				(_iTxType < -1 || (oCM.getTransType() == -1 || (_iTxType >= -1 && oCM.getTransType() == _iTxType))) )			
			{
				log.debug("Matching Margin: " + oCM);
				r.add(oCM);
			}
		}
		return r;
	}
	
	public static List getByItem(Item _oItem)
		throws Exception 
	{		
		String sKats = "";
		String sCode = "";
		String sColor = "";
		if(_oItem != null) {
			List vKat = KategoriTool.getParentIDList(_oItem.getKategoriId(), null);  
			sKats = SqlUtil.convertToINMode(vKat);
			sCode = _oItem.getItemCode();
			sColor = _oItem.getColor();
		}

		List v = getAllData();
		List r = new ArrayList(v.size());
		Iterator iter = v.iterator();
		while(iter.hasNext())
		{
			CustomerMargin oCM = (CustomerMargin) iter.next();


			//validate item only
			if( (StringUtil.equals(oCM.getCategoryId(),"") || (!StringUtil.empty(sKats) && StringUtil.containsIgnoreCase(sKats, oCM.getCategoryId()))) &&
				(StringUtil.equals(oCM.getItemCodes(),"")  || (!StringUtil.empty(sCode) && StringUtil.containsIgnoreCase(oCM.getItemCodes(), sCode))) &&
				(StringUtil.equals(oCM.getColor(),"") 	   || (!StringUtil.empty(sColor) && StringUtil.containsIgnoreCase(oCM.getColor(), sColor))) 
			  )			
			{
				log.debug("Matching Margin: " + oCM);
				r.add(oCM);
			}
		}
		return r;
	}
}
