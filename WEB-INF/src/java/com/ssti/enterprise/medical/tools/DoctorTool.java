package com.ssti.enterprise.medical.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.manager.DoctorManager;
import com.ssti.enterprise.medical.om.Doctor;
import com.ssti.enterprise.medical.om.DoctorPeer;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Business object controller for MedicineSales OM
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DoctorTool.java,v 1.1 2009/05/04 01:38:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: DoctorTool.java,v $
 * Revision 1.1  2009/05/04 01:38:15  albert
 * *** empty log message ***
 *
 * Revision 1.7  2008/06/29 07:02:06  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class DoctorTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( DoctorTool.class );

	static DoctorTool instance = null;
	
	public static synchronized DoctorTool getInstance() 
	{
		if (instance == null) instance = new DoctorTool();
		return instance;
	}		

	public static List getAllDoctor()
		throws Exception
	{
		return DoctorManager.getInstance().getAllDoctor();
	}
	
	public static List getInternalDoctor(String _sLocID)
		throws Exception
	{
		return DoctorManager.getInstance().getInternalDoctor(_sLocID);
	}

	public static List findDoctor(String _sCode, String _sName, 
								  String _sSiteID, String _sLocID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addAscendingOrderByColumn (DoctorPeer.DOCTOR_NAME);	
		if (StringUtil.isNotEmpty(_sCode))
		{
			oCrit.add(DoctorPeer.DOCTOR_CODE,(Object)_sCode,Criteria.ILIKE);
		}
		if (StringUtil.isNotEmpty(_sName))
		{
			oCrit.add(DoctorPeer.DOCTOR_NAME,
				(Object)("%" + _sName + "%"),Criteria.ILIKE);
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			List vLoc = LocationTool.getBySiteID(_sSiteID);		
			List vID = SqlUtil.toListOfID(vLoc);
			oCrit.addIn(DoctorPeer.LOCATION_ID, vID);
			oCrit.or(DoctorPeer.LOCATION_ID, "");
		}
		if(StringUtil.isNotEmpty(_sLocID))
		{
			oCrit.add(DoctorPeer.LOCATION_ID, _sLocID);
			oCrit.or(DoctorPeer.LOCATION_ID, "");
		}
		List vData = DoctorPeer.doSelect(oCrit);
		return vData;
	}

	
	public static List getDoctorBySiteID(String _sSiteID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addAscendingOrderByColumn (DoctorPeer.DOCTOR_NAME);	
					
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			List vLoc = LocationTool.getBySiteID(_sSiteID);		
			List vID = SqlUtil.toListOfID(vLoc);
			oCrit.addIn(DoctorPeer.LOCATION_ID, vID);
			oCrit.or(DoctorPeer.LOCATION_ID, "");
		}
		List vData = DoctorPeer.doSelect(oCrit);
		return vData;
	}

	public static List getDoctorByLocID(String _sLocID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addAscendingOrderByColumn (DoctorPeer.DOCTOR_NAME);	
		if (StringUtil.isNotEmpty(_sLocID))
		{
			oCrit.add(DoctorPeer.LOCATION_ID, _sLocID);
			oCrit.or(DoctorPeer.LOCATION_ID, "");
		}
		List vData = DoctorPeer.doSelect(oCrit);
		return vData;
	}
	
	public static Doctor getDoctorByID(String _sID)
		throws Exception
	{
		return DoctorManager.getInstance().getDoctorByID(_sID, null);
	}
	
	public static Doctor getDoctorByID(String _sID, Connection _oConn)
		throws Exception
	{
		return DoctorManager.getInstance().getDoctorByID(_sID, _oConn);
	}
		
	public static String getDoctorNameByID(String _sID)
		throws Exception
	{
		Doctor oDoctor = getDoctorByID(_sID);
		if (oDoctor != null)
		{
			return oDoctor.getDoctorName();
		}
		return "";
	}
	
	public static Doctor getDoctorByCode(String _sCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(DoctorPeer.DOCTOR_CODE, _sCode);
		List vData = DoctorPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (Doctor) vData.get(0);
		}
		return null;
	}

	public static Doctor getDoctorByUserName(String _sUserName)
		throws Exception
	{
		return DoctorManager.getInstance().getDoctorByUserName(_sUserName, null);
	}	

	
	public static String generateCode(String _sPrefix, int _iLength)
	{
		if (_iLength <= 0) _iLength = PreferenceTool.getOthrCodeLength();
		return generateCode(_sPrefix, DoctorPeer.DOCTOR_CODE, "doctorCode", DoctorPeer.class, _iLength);
	}

	public static List getNurses(String _sSiteID, String _sUserName)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if(StringUtil.isNotEmpty(_sUserName))
		{
			oCrit.add(EmployeePeer.USER_NAME, _sUserName);
		}
		oCrit.addJoin(EmployeePeer.LOCATION_ID, LocationPeer.LOCATION_ID);
		oCrit.add(LocationPeer.SITE_ID,_sSiteID);
				
		return EmployeePeer.doSelect(oCrit);
	}
}