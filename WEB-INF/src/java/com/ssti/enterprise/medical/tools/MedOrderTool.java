package com.ssti.enterprise.medical.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.medical.om.MedOrder;
import com.ssti.enterprise.medical.om.MedOrderDetail;
import com.ssti.enterprise.medical.om.MedOrderDetailPeer;
import com.ssti.enterprise.medical.om.MedOrderPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.sales.CreditLimitValidator;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Business object controller for MedOrder OM
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * 
 * 2018-06-13
 * - initialVersion
 * </pre><br>
 */
public class MedOrderTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( MedOrderTool.class );
	
	public static final int i_PRS_TYPE = 5;
	public static final int i_MO_DELIVERED = 4;
	public static final int i_MO_INVOICED = 5;
	
	public static final String s_ITEM = new StringBuilder(MedOrderPeer.MED_ORDER_ID).append(",")
			.append(MedOrderDetailPeer.MED_ORDER_ID).append(",")
			.append(MedOrderDetailPeer.ITEM_ID).toString();
	
	public static final String s_TAX = new StringBuilder(MedOrderPeer.MED_ORDER_ID).append(",")
			.append(MedOrderDetailPeer.MED_ORDER_ID).append(",")
			.append(MedOrderDetailPeer.TAX_ID).toString();
	
	//field to skip from set scale
	public static final String[] a_SKIP_FIELD = {"qty", "qtyBase"};
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), MedOrderPeer.INVOICE_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), MedOrderPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER), 	 MedOrderPeer.CUSTOMER_ID);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), MedOrderPeer.CASHIER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), MedOrderPeer.CASHIER_NAME);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), MedOrderPeer.LOCATION_ID);             
		
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), MedOrderPeer.PAYMENT_TYPE_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), MedOrderPeer.PAYMENT_TERM_ID);
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), MedOrderPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), MedOrderPeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), MedOrderPeer.CURRENCY_ID);
		m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), "");
		m_FIND_PEER.put (Integer.valueOf(i_SALESMAN   ), MedOrderPeer.SALES_ID);             
		
		addInv(m_FIND_PEER, s_ITEM);
		//modules specific
		m_FIND_PEER.put (Integer.valueOf(50), MedOrderPeer.PATIENT_NAME);
		m_FIND_PEER.put (Integer.valueOf(51), MedOrderPeer.PATIENT_PHONE);		
		m_FIND_PEER.put (Integer.valueOf(52), MedOrderPeer.DOCTOR_NAME);
		addInv(m_FIND_PEER, s_ITEM);

		m_FIND_TEXT.put (Integer.valueOf(50), LocaleTool.getString("patient_name"));
		m_FIND_TEXT.put (Integer.valueOf(51), LocaleTool.getString("phone"));
		m_FIND_TEXT.put (Integer.valueOf(52), LocaleTool.getString("doctor_name"));
	}   
	
	protected static Map m_GROUP_PEER = new HashMap();
	
	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), MedOrderPeer.TRANSACTION_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), MedOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), MedOrderPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), MedOrderPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), MedOrderPeer.CASHIER_NAME);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), "");               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), MedOrderPeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), MedOrderPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), MedOrderPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), MedOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), MedOrderPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), MedOrderPeer.SALES_ID); 
	}
	
	static MedOrderTool instance = null;
	
	public static synchronized MedOrderTool getInstance() 
	{
		if (instance == null) instance = new MedOrderTool();
		return instance;
	}	
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
	
	public static MedOrder getHeaderByID(String _sID) 
		throws Exception	
	{
		return getHeaderByID (_sID, null);
	}
	
	/**
	 * get header by ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static MedOrder getHeaderByID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedOrderPeer.MED_ORDER_ID, _sID);
		List vData = MedOrderPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (MedOrder) vData.get(0);
		}
		return null;	
	}
	
	public static List getDetailsByID(String _sID) 
		throws Exception
	{
		return getDetailsByID(_sID, null);
	}
	
	/**
	 * get details by id
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedOrderDetailPeer.MED_ORDER_ID, _sID);
		return MedOrderDetailPeer.doSelect(oCrit, _oConn);
	}
	
	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static MedOrderDetail getDetailByDetailID(String _sID) 
		throws Exception
	{
		return getDetailByDetailID(_sID, null) ;
	}	
	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static MedOrderDetail getDetailByDetailID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedOrderDetailPeer.MED_ORDER_DETAIL_ID, _sID);
		List vData = MedOrderDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) return (MedOrderDetail) vData.get(0);
		return null;
	}		
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteDetailsByID (String _sID, Connection _oConn) 
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedOrderDetailPeer.MED_ORDER_ID, _sID);
		MedOrderDetailPeer.doDelete (oCrit, _oConn);
	}
	
	/**
	 * update print times indicate how many times this invoice printed
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
	public static void updatePrintTimes (MedOrder _oTR) 
	throws Exception
	{
		_oTR.setPrintTimes (_oTR.getPrintTimes() + 1);
		_oTR.save();
	}
	
	///////////////////////////////////////////////////////////////////////////
	////transaction processing method
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * set header properties from screen entry
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param data
	 * @param _iIsImportDO
	 * @throws Exception
	 */
	public static void setHeaderProperties (MedOrder _oTR, List _vTD, RunData data) 
		throws Exception
	{
		try 
		{
			ValueParser formData = null;
			if (data != null) 
			{
				formData = data.getParameters();
				formData.setProperties  (_oTR);
				_oTR.setTransactionDate (CustomParser.parseDate(formData.getString("TransactionDate")));    		
				_oTR.setIsTaxable  		(formData.getBoolean("IsTaxable", false));
				_oTR.setIsInclusiveTax  (formData.getBoolean("IsInclusiveTax", false));
			}			
			
			_oTR.setTotalQty 	    (new BigDecimal(countQty(_vTD)));			
			_oTR.setTotalCost	    (countCost(_vTD));
			_oTR.setTotalAmount     (countSubTotal(_vTD));
			_oTR.setTotalDiscount   (countDiscount(_vTD));
			setDueDate (_oTR);
			
			//if this is invoice not contains prescription, check if POS value discount exists
//			if (!containsPres(_vTD)) 
//			{
//				
//			}
			
			//count total amount after total discount percentage
			String sTDiscPct = _oTR.getTotalDiscountPct();
			double dTDiscAmt = 0;
			double dTotalAmount = _oTR.getTotalAmount().doubleValue();
			
			_oTR.setTotalTax (countTax(sTDiscPct,_vTD,_oTR));
			
			if (StringUtil.isNotEmpty(sTDiscPct)) 
			{
				//get discount for all 
				//count total discount from total amount
				dTDiscAmt = Calculator.calculateDiscount(sTDiscPct,dTotalAmount);
				double dSubTotalDiscount = _oTR.getTotalDiscount().doubleValue();
				_oTR.setTotalDiscount (new BigDecimal(dTDiscAmt + dSubTotalDiscount));
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount - dTDiscAmt));							
			}
			
			//count Total Amount with tax
			if(!_oTR.getIsInclusiveTax())
			{
				dTotalAmount = _oTR.getTotalAmount().doubleValue() + _oTR.getTotalTax().doubleValue();
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
			}
			
			//if freight cost > 0 add freight cost to total amount
			if(_oTR.getTotalExpense().doubleValue() > 0)
			{
				dTotalAmount = _oTR.getTotalAmount().doubleValue() + _oTR.getTotalExpense().doubleValue();
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
			}
			
			BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oTR);
			
			log.debug("Total Amount : " + dTotalAmount);
			
			//calculate rounding
			double dRounded = Calculator.roundNumber(_oTR.getTotalAmount(), 
					MedConfigTool.getRoundingScale(), 
					MedConfigTool.getRoundingMode());
			
			log.debug("Rounded Amount : " + dRounded);
			
			double dDelta = dRounded - _oTR.getTotalAmount().doubleValue();
			_oTR.setTotalAmount(new BigDecimal(dRounded));
			_oTR.setRoundingAmount(new BigDecimal(dDelta));
			//init / setPayment
			//InvoicePaymentTool.setPayment(_oTR, data); 
			
			//if CC Charges exist then reset payment amount
			if(_oTR.getTotalExpense().doubleValue() > 0)
			{
				_oTR.setPaymentAmount(_oTR.getTotalAmount());
				_oTR.setChangeAmount(bd_ZERO);
			}
			
			//recalculate Payment Amount & Change Amount
			if (_oTR.getTotalAmount().doubleValue() == 0 || 
					(_oTR.getTotalAmount().doubleValue() == _oTR.getPaymentAmount().doubleValue()))
			{
				_oTR.setPaymentAmount(_oTR.getTotalAmount());
				_oTR.setChangeAmount(bd_ZERO);            		            	
			}
			/*
			 if((_oTR.getTotalAmount().doubleValue() != _oTR.getPaymentAmount().doubleValue()) && 
			 _oTR.getTotalAmount().doubleValue() > 0)
			 {
			 double dChange = _oTR.getPaymentAmount().doubleValue() - _oTR.getTotalAmount().doubleValue();
			 if (dChange >= 0)
			 {
			 _oTR.setChangeAmount(new BigDecimal(dChange));
			 }
			 else
			 {
			 _oTR.setPaymentAmount(_oTR.getTotalAmount());
			 _oTR.setChangeAmount(bd_ZERO);            		
			 }
			 }
			 else
			 {
			 _oTR.setChangeAmount(bd_ZERO);            		            	
			 }
			 */
			
			//log.debug(" #$@#%#$%$^ " + _oTR);
			
			BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _vTD, a_SKIP_FIELD);
			BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oTR);			           
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
	}
	
	/**
	 * update detail properties
	 * 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 
	 public static void updateDetail (List _vDet, RunData data)
	 throws Exception
	 {
	 ValueParser formData = data.getParameters();
	 try
	 {
	 for (int i = 0; i < _vDet.size(); i++)
	 {
	 MedOrderDetail oDetail = (MedOrderDetail) _vDet.get(i);
	 
	 //log.debug ("Before update : "  + oDetail );
	  //check null to prevent refresh from user that didn't submit param from data
	   if (formData.getString("Discount" + (i + 1)) != null)
	   {				
	   oDetail.setQty          ( formData.getBigDecimal("Qty"          + (i + 1)) );
	   oDetail.setQtyBase 		( UnitTool.getBaseQty(oDetail.getUnitId(), oDetail.getQty()));
	   oDetail.setItemPrice    ( formData.getBigDecimal("ItemPrice"    + (i + 1)) );
	   oDetail.setTaxId        ( formData.getString    ("TaxId"        + (i + 1)) );
	   oDetail.setTaxAmount    ( formData.getBigDecimal("TaxAmount"    + (i + 1)) );
	   oDetail.setSubTotalTax  ( formData.getBigDecimal("SubTotalTax"  + (i + 1)) );
	   oDetail.setDiscount     ( formData.getString    ("Discount"     + (i + 1)) );
	   oDetail.setSubTotalDisc ( formData.getBigDecimal("SubTotalDisc" + (i + 1)) );
	   oDetail.setSubTotal     ( formData.getBigDecimal("SubTotal"     + (i + 1)) );				
	   }
	   //log.debug ("After update : "  + oDetail);
	    }
	    }
	    catch (Exception _oEx)
	    {
	    _oEx.printStackTrace();
	    throw new NestableException ("Update Detail Failed : " + _oEx.getMessage (), _oEx); 
	    }
	    }	*/
	
	/**
	 * count total qty 
	 * @param _vDetails
	 * @return
	 */
	private static boolean containsPres (List _vDetails) 
		throws Exception
	{
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedOrderDetail oTD = (MedOrderDetail) _vDetails.get(i);
			if (StringUtil.isNotEmpty(oTD.getPrescriptionId()))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * count total qty 
	 * @param _vDetails
	 * @return
	 */
	private static double countQty (List _vDetails) 
	throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedOrderDetail oTD = (MedOrderDetail) _vDetails.get(i);
			if (oTD.getQtyBase() == null) //if from add detail
			{
				oTD.setQtyBase (UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
			}
			//the best we can do is count the base qty
			dQty += oTD.getQtyBase().doubleValue();
		}
		return dQty;
	}
	
	/**
	 * count qty per item
	 * 
	 * @param _vDetails
	 * @param _sItemID
	 * @return
	 */
	private static double countQty (List _vDetails, String _sItemID)
	{
		return countQtyPerItem (_vDetails, _sItemID, "qty");
	}
	
	private static BigDecimal countTax (String _sDiscountPct,List _vTD,MedOrder _oTR)
	{
		double dTotalAmount = _oTR.getTotalAmount().doubleValue();
		double dAmt = 0;
		for (int i = 0; i < _vTD.size(); i++)
		{
			MedOrderDetail _oTD = (MedOrderDetail) _vTD.get(i);
			double dSubTotal = _oTD.getSubTotal().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDiscountPct, dSubTotal);
			
			// if total amount
			if(!_sDiscountPct.contains(Calculator.s_PCT))
			{
				if (dTotalAmount > 0) //if dTotalAmount > 0, prevent divide by zero
				{
					dSubTotal = dSubTotal - (dDisc * (dSubTotal / dTotalAmount));
				}
			}
			else
			{
				dSubTotal = dSubTotal - dDisc;
			}
			
			
			double dSubTotalTax = 0;
			if(!_oTR.getIsInclusiveTax())
			{
				dSubTotalTax = dSubTotal * _oTD.getTaxAmount().doubleValue() / 100;
			}
			else
			{
				//calculate inclusive Tax
				dSubTotalTax = (dSubTotal * 100) / (_oTD.getTaxAmount().doubleValue() + 100);
				dSubTotalTax = dSubTotal - dSubTotalTax;  
			}
			
			_oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
			dAmt += _oTD.getSubTotalTax().doubleValue();
		}
		return new BigDecimal(dAmt);
	}
	
	private static BigDecimal countDiscount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedOrderDetail oDet = (MedOrderDetail) _vDetails.get(i);
			dAmt += oDet.getSubTotalDisc().doubleValue();
		}
		return new BigDecimal(dAmt);
	}
	
	private static BigDecimal countCost (List _vDetails) 
	throws Exception
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedOrderDetail oDet = (MedOrderDetail) _vDetails.get(i);
			double dCost = oDet.getItemCost().doubleValue();
			double dQtyBase = UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty().doubleValue());
			double dSubTotalCost = dCost * dQtyBase;
			oDet.setSubTotalCost(new BigDecimal(dSubTotalCost));
			
			dAmt += dSubTotalCost;
		}
		return new BigDecimal(dAmt);
	}
	
	private static BigDecimal countSubTotal (List _vDetails) 
		throws Exception 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedOrderDetail oDet = (MedOrderDetail) _vDetails.get(i);
			
			double dPrice = oDet.getItemPrice().doubleValue();
			String sDisc = oDet.getDiscount();
			double dTax	= oDet.getTaxAmount().doubleValue();
			
			double dQty = oDet.getQty().doubleValue();
			double dSubTotal = dQty * dPrice;
			double dSubTotalDisc = Calculator.calculateDiscount(sDisc, dSubTotal);
			dSubTotal = dSubTotal - dSubTotalDisc;
			
			if (oDet.getItemType() != i_PRS_TYPE)
			{
				MedConfigTool.calculateRfee(oDet, true, dQty, dSubTotal);
			}
			double dRfee = Double.parseDouble(oDet.getRfee()); //calculated amount
			double dPack = Double.parseDouble(oDet.getPackaging()); //calculated amount
			double dTotal = dSubTotal + dRfee + dPack;
			oDet.setSubTotalDisc(new BigDecimal(dSubTotalDisc));			
			oDet.setSubTotal(new BigDecimal(dSubTotal));
			oDet.setTotal(new BigDecimal(dTotal));
			dAmt += dTotal;
		}
		return new BigDecimal(dAmt).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * set Payment Due Date
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
	private static void setDueDate (MedOrder _oTR) 
		throws Exception 
	{
		boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oTR.getPaymentTypeId());
		if (bIsCredit) 
		{ 
			if (_oTR.getTransactionDate() != null)
			{
				//check default payment term for this customer
				String sDefaultTerm = CustomerTool.getDefaultTermByID(_oTR.getCustomerId(), null);
				int iDueDays = 0;
				if (StringUtil.isNotEmpty(sDefaultTerm) && 
						PaymentTermTool.isCashPaymentTerm(_oTR.getPaymentTermId())) 
				{
					iDueDays = PaymentTermTool.getNetPaymentDaysByID (sDefaultTerm);
					_oTR.setPaymentTermId(sDefaultTerm);
				}
				else 
				{
					iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oTR.getPaymentTermId());
				}
				Calendar oCal = new GregorianCalendar ();
				oCal.setTime (_oTR.getTransactionDate());
				oCal.add (Calendar.DATE, iDueDays);
				_oTR.setDueDate (oCal.getTime());		
			}
		}
		else
		{
			if (_oTR.getTransactionDate() != null)
			{
				_oTR.setDueDate(_oTR.getTransactionDate());
			}
			else
			{				
				_oTR.setDueDate(new Date());
			}
		}
	}
	
	/**
	 * generate transaction no
	 * 
	 * @param _oTR
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	private static String genTransNo (MedOrder _oTR, Connection _oConn) 
	throws Exception
	{
		return LastNumberTool.get(s_SO_FORMAT, LastNumberTool.i_SALES_ORDER, _oConn);
	}

	public static void validateMED(MedOrder _oTR)
			throws Exception
	{
		if (_oTR != null)
		{
			boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oTR.getPaymentTypeId());
			//validate Credit Limit first
			int iResult = CreditLimitValidator.validateTransAmount(_oTR.getCustomerId(), _oTR.getTotalAmount());
			if (iResult != CreditLimitValidator.OK && bIsCredit)
			{
				_oTR.setInvoiceNo("");
				_oTR.setStatus(AppAttributes.i_PENDING);		
				throw new Exception(CreditLimitValidator.getMessage(iResult));
			}
		}	
	}

	
	///////////////////////////////////////////////////////////////////////
	// Save Transaction
	///////////////////////////////////////////////////////////////////////
			
	public static void saveData (MedOrder _oTR, List _vTD)
		throws Exception 
	{
		saveData (_oTR, _vTD, null);
	} 
			
	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _vPMT
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (MedOrder _oTR, List _vTD, Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		boolean bNew = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{	
			if(StringUtil.isEmpty(_oTR.getMedOrderId())) bNew = true;
						
			//process save sales Data
			processSaveSalesData (_oTR, _vTD, _oConn);
			
			if ( _oTR.getStatus () == i_TRANS_PROCESSED && StringUtil.empty(_oTR.getInvoiceNo())) 
			{
				//validate credit limit
				validateMED(_oTR);
				
				//update invoice no
				_oTR.setTotalFee(bd_ZERO);
				_oTR.setInvoiceNo(_oTR.getOrderNo());
				_oTR.save(_oConn);
				
				//update reg status
				if (StringUtil.isNotEmpty(_oTR.getRegistrationId()))
				{					

				}
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			//restore status to unsaved
			_oTR.setStatus(i_PENDING);
			_oTR.setOrderNo("");
			_oTR.setInvoiceNo("");
			
			if (bNew)
			{
				_oTR.setNew(true);
				_oTR.setMedOrderId("");
			}
			if (bStartTrans) 
			{
				rollback ( _oConn );
			}			
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	/**
	 * process save
	 * <li>generate trans no
	 * <li>delete all pending details
	 * <li>iterate and save details
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveSalesData (MedOrder _oTR, List _vTD, Connection _oConn) 
		throws Exception
	{
		//check whether transaction is paid and invoice no is not generated yet
		//invoice no must be checked for synchronization process
		try 
		{
			if (_oTR.getOrderNo() == null) _oTR.setOrderNo ("");
			
			if(_oTR.getStatus() == i_TRANS_PROCESSED && StringUtil.isEmpty(_oTR.getOrderNo()))
			{
				_oTR.setOrderNo(genTransNo(_oTR, _oConn));
				_oTR.setInvoiceNo(_oTR.getOrderNo());
			}
			
			//delete all existing detail from pending transaction
			if (StringUtil.isNotEmpty(_oTR.getMedOrderId()))
			{
				deleteDetailsByID (_oTR.getMedOrderId(), _oConn);
			}			
			
			//generate Sys ID if this is not a previous pending trans or sync trans
			if (StringUtil.isEmpty(_oTR.getMedOrderId())) 
			{
				_oTR.setMedOrderId ( IDGenerator.generateSysID() );
			}
			validateID(getHeaderByID(_oTR.getMedOrderId(), _oConn), "invoiceNo");
			
			_oTR.setModified(true);
			_oTR.save (_oConn);
						
			for ( int i = 0; i < _vTD.size(); i++ ) 
			{
				MedOrderDetail oTD = (MedOrderDetail) _vTD.get (i);
				oTD.setModified (true);
				oTD.setNew (true);
				
				//Generate SYS Data
				oTD.setMedOrderDetailId (IDGenerator.generateSysID());
				oTD.setMedOrderId ( _oTR.getMedOrderId());
				oTD.save (_oConn);				
			}
		}
		catch (Exception _oEx)
		{
			log.error(_oEx);
			String sError = "Process Save Failed : " + _oEx.getMessage();
			throw new NestableException (sError, _oEx);
		}
	}
	
	
	/**
	 * cancel sales transaction
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @throws Exception
	 */
	public static void cancelSales(MedOrder _oTR, 
								   List _vTD, 
								   String _sCancelBy, 
								   Connection _oConn) 
	throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{	
			_oTR.setStatus(i_CANCELLED);
			_oTR.save(_oConn);
			
			if (bStartTrans) 
			{
				commit(_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}	
			_oEx.printStackTrace();
			throw new NestableException(_oEx.getMessage(), _oEx);
		}
	}
	
	/**
	 * get Status String 
	 * 
	 * @param _iStatusID
	 * @return status string
	 */
	public static String getStatusString (int _iStatusID)
	{	
		if(_iStatusID == i_MO_DELIVERED) return LocaleTool.getString("delivered");
		if(_iStatusID == i_MO_INVOICED) return LocaleTool.getString("invoiced");
		return TransactionTool.getStatusString(_iStatusID);
	}	
	
	/**
	 * Finder used by sales transaction view
	 * 
	 * @param _sInvNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _iLimit
	 * @param _sCashierName
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond,
										String _sKeywords,
										Date _dStart, 
										Date _dEnd, 
										String _sCustomerID, 
										String _sLocationID, 
										int _iStatus, 
										int _iLimit,
										int _iGroupBy,
										String  _sCashierName,
										String _sCurrencyID) 
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				MedOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);    	
		
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(MedOrderPeer.CUSTOMER_ID, _sCustomerID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(MedOrderPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sCashierName)) 
		{
			oCrit.add(MedOrderPeer.CASHIER_NAME, _sCashierName);	
		}
		if (_iStatus > 0) 
		{
			oCrit.add(MedOrderPeer.STATUS, _iStatus);	
		}		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		else
		{
			oCrit.addAscendingOrderByColumn(MedOrderPeer.TRANSACTION_DATE);
		}
		log.debug(oCrit);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.medical.om.MedOrderPeer");
	}
	
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sCashierName,
								  String _sCurrencyID,
                                  String _sDoctorName) 
    	throws Exception
	{
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	MedOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);   
        
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(MedOrderPeer.CUSTOMER_ID, _sCustomerID);	
		}
        if(StringUtil.isNotEmpty(_sLocationID))
        {
           oCrit.add(MedOrderPeer.LOCATION_ID, _sLocationID);
        }
        if (StringUtil.isNotEmpty(_sCashierName))
	    {
	    	oCrit.add(MedOrderPeer.CASHIER_NAME, _sCashierName);
	    }        
        if (StringUtil.isNotEmpty(_sDoctorName))
        {
            oCrit.add(MedOrderPeer.DOCTOR_NAME, (Object)_sDoctorName, Criteria.ILIKE);
        }
        if(_iStatus > 0)
        {
           oCrit.add(MedOrderPeer.STATUS, _iStatus);
        }
	    if (StringUtil.isNotEmpty(_sVendorID))
	    {
		    oCrit.addJoin(MedOrderPeer.MED_ORDER_ID,MedOrderDetailPeer.MED_ORDER_ID);
		    oCrit.addJoin(MedOrderDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
		    oCrit.add(ItemPeer.PREFERED_VENDOR_ID,_sVendorID);
	    }	   
	    oCrit.addAscendingOrderByColumn(MedOrderPeer.TRANSACTION_DATE);
        log.debug(oCrit);
		return MedOrderPeer.doSelect(oCrit);
	}
	
	public static double calcPriceIncFee (MedOrderDetail _oTD)
	{
		if (_oTD != null)
		{
			double dSubTotal = _oTD.getSubTotal().doubleValue();
			double dRfee = Double.valueOf(_oTD.getRfee());
			double dPack = Double.valueOf(_oTD.getPackaging());
			double dQty = _oTD.getQty().doubleValue();
			double dTax = _oTD.getSubTotalTax().doubleValue();
			return ((dSubTotal + dRfee + dPack + dTax) / dQty) ;
		}
		return 0;
	}
	
	public static double calcTotalRfeePack (String _sID, boolean _bRfee) 
		throws Exception
	{
		List vTD = getDetailsByID(_sID);
		double dTotal = 0;
		for (int i = 0; i < vTD.size(); i++)
		{
			MedOrderDetail oTD = (MedOrderDetail) vTD.get(i);			
			double dValue = 0;	
			if (_bRfee)  dValue = Double.valueOf(oTD.getRfee());
			if (!_bRfee) dValue = Double.valueOf(oTD.getPackaging());
			dTotal = dTotal + dValue;
		}
		return dTotal;
	}
			   
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
	throws Exception
	{
		return getPrevOrNextID(MedOrderPeer.class, MedOrderPeer.MED_ORDER_ID, _sID, _bIsNext);
	}				      
}