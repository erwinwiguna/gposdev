package com.ssti.enterprise.medical.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.GlConfigPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.journal.BaseJournalTool;
import com.ssti.enterprise.pos.tools.journal.JournalFailedException;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Tool Class contains Automatic Sales Journal Creation methods, used to create automatic GL
 * Transaction for DeliveryOrder, SalesTransaction
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesJournalTool.java,v 1.31 2009/05/04 02:04:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesJournalTool.java,v $
 * Revision 1.31  2009/05/04 02:04:46  albert
 * *** empty log message ***
 *
 * Revision 1.30  2008/08/20 16:03:43  albert
 * *** empty log message ***
 *
 * Revision 1.29  2008/06/29 07:15:44  albert
 * *** empty log message ***
 *
 * Revision 1.28  2008/03/04 02:50:22  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class MedSalesJournalTool extends BaseJournalTool
{
	private static Log log = LogFactory.getLog ( MedSalesJournalTool.class );
		
	private Currency m_oCurrency = null;
	private Currency m_oBaseCurrency = null;
	
	private double m_dRate = 1;
	private BigDecimal m_bdRate = bd_ONE;
	private Location m_oLoc = null;
	private boolean m_bConsignment = false;
	private Connection m_oConn = null;
	private Customer m_oCustomer = null;
	
	//rounding unbalance tolerance
	private static final double d_ROUNDING_TOLERANCE = 3;
	private GlTransaction m_oRoundingGLT = null;
		
	public MedSalesJournalTool(SalesTransaction _oTR, Connection _oConn)
		throws Exception
	{
        validate(_oConn);
        m_oConn = _oConn;
        
		m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getTransactionDate(), m_oConn);
        m_oCurrency = CurrencyTool.getCurrencyByID(_oTR.getCurrencyId(), m_oConn);
        m_oBaseCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
        m_dRate = _oTR.getCurrencyRate().doubleValue();		
        m_bdRate = new BigDecimal(m_dRate);
        m_oLoc = LocationTool.getLocationByID(_oTR.getLocationId(), m_oConn);    	
    	
        //set consignment flag if, location is consignment type then do journal inv & cogs only
        if (m_oLoc != null && m_oLoc.getLocationType() == i_CONSIGNMENT)
        {
        	m_bConsignment = true;
        }
        
        m_sTransID = _oTR.getSalesTransactionId();
        m_sTransNo = _oTR.getInvoiceNo();
    	m_sUserName = _oTR.getCashierName();
    	m_sLocationID = _oTR.getLocationId();
    	
        StringBuilder oRemark = new StringBuilder (_oTR.getRemark());
		oRemark.append (_oTR.getCustomerName()).append(" "); 

    	m_sRemark = oRemark.toString();

    	m_dTransDate = _oTR.getTransactionDate();
    	m_dCreate = new Date();

    	m_oCustomer = CustomerTool.getCustomerByID(_oTR.getCustomerId(), m_oConn);    	
	}

	/**
	 * Sales Transaction journal 
	 * <li> 1. COGS -> debit and INVENTORY -> credit 
	 * <li> 2. REVENUE -> credit and AR -> debit
	 * <li> 3. SALES TAX (VAT OUT) -> credit and AR base currency -> debit
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _vPayment
	 * @param _mGroup
	 * @param m_oConn
	 * @throws Exception
	 */
	public void createSalesJournal(SalesTransaction _oTR, 
								   List _vTD, 
								   List _vPayment, 
								   Map _mGroup) 
    	throws Exception
    {
		validate(m_oConn);
		try
        { 
			_vTD = TransactionTool.getDetailsByID(_oTR.getSalesTransactionId(), m_oConn);
			List vGLTrans = new ArrayList(_vTD.size());
            for (int i = 0; i < _vTD.size(); i++)
            {
            	SalesTransactionDetail oTD = (SalesTransactionDetail) _vTD.get(i);
                String sItemID = oTD.getItemId();
                Item oItem = ItemTool.getItemByID (sItemID, m_oConn);
                createSalesGLTrans (_oTR, oTD, oItem, vGLTrans, _mGroup);
            }
            if (!m_bConsignment)
            {
            	createTaxJournal (_oTR, vGLTrans);
            	createPaymentJournal (_oTR, _vPayment, vGLTrans);
            	createTotalDiscountJournal (_oTR, _vTD, vGLTrans);
            	createFreightJournal (_oTR, vGLTrans);
            }
            setGLTrans (vGLTrans);
            
            //validate unbalance
            fixUnbalance(vGLTrans);
            
            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
        }
        catch (Exception _oEx)
        {
            throw new JournalFailedException ("GL Journal Failed : " + _oEx.getMessage(), _oEx);
        }        
	}

	private void fixUnbalance(List _vGLTrans) 
		throws Exception 
	{
		double[] dTotalDC   = GlTransactionTool.getTotalDebitCredit(_vGLTrans, m_oConn);
		double dTotalDebit  = dTotalDC[0];
		double dTotalCredit = dTotalDC[1];
		double dUnbalanced  = 0; 
		boolean bDebitGT = false;
		boolean bCreditGT = false;
		
		if (dTotalDebit > dTotalCredit) {dUnbalanced = dTotalDebit - dTotalCredit;bDebitGT = true;}
		if (dTotalDebit < dTotalCredit) {dUnbalanced = dTotalCredit - dTotalDebit;bCreditGT = true;}
		
		if (dUnbalanced != 0)
		{			
			if (dUnbalanced > d_ROUNDING_TOLERANCE)
			{
				log.warn("WARNING: Unbalance Journal > Tolerance (" + d_ROUNDING_TOLERANCE + ")");
			}
			log.debug("Unbalance Journal : " + dUnbalanced);
			log.debug("ROUNDING GLT " + m_oRoundingGLT);
            if (m_oRoundingGLT != null) //no rounding exists
            {
    			for (int i = 0; i < _vGLTrans.size(); i++)
    			{
    				GlTransaction oGLT = (GlTransaction) _vGLTrans.get(i);
    				if (oGLT.getAccountId().equals(m_oRoundingGLT.getAccountId()))
    				{
    					int iDC  = oGLT.getDebitCredit();
    					double dAmt = oGLT.getAmount().doubleValue();
    					if ((iDC == i_DEBIT && bDebitGT) || (iDC == i_CREDIT && bCreditGT))
    					{
    						//reduce amount
    						dAmt  = dAmt - dUnbalanced;
    					}
    					else if ((iDC == i_DEBIT && bCreditGT) || (iDC == i_CREDIT && bDebitGT))
    					{
    						//add amount 
    						dAmt  = dAmt + dUnbalanced;
    					}
    					oGLT.setAmount(new BigDecimal(dAmt));
    					oGLT.setAmountBase(new BigDecimal(dAmt));
    				}
    			}		
            }
		}
	}

	private void createTaxJournal(SalesTransaction _oTR, List _vGLTrans) 
		throws Exception
	{
    	if (_oTR.getTotalTax().doubleValue() > 0)
    	{                   
    		//TAX -> CREDIT
    		Tax oTax = TaxTool.getDefaultSalesTax();
    		String sTax  = oTax.getSalesTaxAccount();
    		Account oTaxAcc = AccountTool.getAccountByID (sTax, m_oConn);
    		validate (oTaxAcc, oTax.getTaxCode(), TaxPeer.SALES_TAX_ACCOUNT);
    		
    		GlTransaction oTaxTrans = new GlTransaction();
    		oTaxTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
    		oTaxTrans.setProjectId        ("");
    		oTaxTrans.setDepartmentId     ("");
    		oTaxTrans.setAccountId        (oTaxAcc.getAccountId());
    		oTaxTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
    		oTaxTrans.setCurrencyRate     (bd_ONE);
    		oTaxTrans.setAmount           (_oTR.getTotalTax());
    		oTaxTrans.setAmountBase       (_oTR.getTotalTax());
    		oTaxTrans.setDebitCredit      (i_CREDIT);

    		addJournal (oTaxTrans, oTaxAcc, _vGLTrans);	
    	}
	}

	private void createSalesGLTrans (SalesTransaction _oTR,
									 SalesTransactionDetail _oTD, 
	                                 Item _oItem, 
	                                 List _vGLTrans,
	                                 Map _mGroup) 
    	throws Exception
    {
        double dTax  = _oTD.getSubTotalTax().doubleValue(); 
        double dDisc = _oTD.getSubTotalDisc().doubleValue(); 
        double dRevenue = _oTD.getSubTotal().doubleValue() + dDisc;
        
        double dTaxBase = dTax * m_dRate;
        double dDiscBase = dDisc * m_dRate;
        double dRevenueBase = dRevenue * m_dRate;
        
        //System.out.println (_oTR);
        //System.out.println (_oTD);
        /*
        System.out.println ("dTaxBase " + dTaxBase);
        System.out.println ("dDiscBase " + dDiscBase);
        System.out.println ("dRevBase " + dRevenueBase);
        */
        
        if(_oTR.getIsInclusiveTax())
        {
        	dRevenueBase = dRevenueBase - dTaxBase;
        	dRevenue = dRevenue - dTax;
        }
                
        BigDecimal dCost = _oTD.getSubTotalCost();
        
        //a direct sales do credit inventory then debit the COGS  
        if (StringUtil.empty(_oTD.getDeliveryOrderId()) && 
        	StringUtil.empty(_oTD.getDeliveryOrderDetailId()))
        {   
        	if (_oItem.getItemType() == i_INVENTORY_PART)
        	{
        		//Inventory -> CREDIT
        		String sInventory = _oItem.getInventoryAccount(); 
	            Account oInv = AccountTool.getAccountByID ( sInventory, m_oConn);        
	            validate (oInv, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);
				
	            GlTransaction oInvTrans       = new GlTransaction();
	            oInvTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
	            oInvTrans.setProjectId        (_oTD.getProjectId());
	            oInvTrans.setDepartmentId     (_oTD.getDepartmentId());
	            oInvTrans.setAccountId        (oInv.getAccountId());
	            oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	            oInvTrans.setCurrencyRate     (bd_ONE);
	            oInvTrans.setAmount           (dCost);
	            oInvTrans.setAmountBase       (dCost);
		        oInvTrans.setDebitCredit      (i_CREDIT);
        	
	            //COGS -> DEBIT
	            String sCOGS  = _oItem.getCogsAccount();
	            Account oCOGS = AccountTool.getAccountByID ( sCOGS, m_oConn);
	            validate (oCOGS, _oItem.getItemCode(), ItemPeer.COGS_ACCOUNT);
	
	            GlTransaction oCOGSTrans = new GlTransaction();
	            oCOGSTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
	            oCOGSTrans.setProjectId        (_oTD.getProjectId());
	            oCOGSTrans.setDepartmentId     (_oTD.getDepartmentId());
	            oCOGSTrans.setAccountId        (oCOGS.getAccountId());
	            oCOGSTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	            oCOGSTrans.setCurrencyRate     (bd_ONE);
	            oCOGSTrans.setAmount           (dCost);
	            oCOGSTrans.setAmountBase       (dCost);
		        oCOGSTrans.setDebitCredit      (i_DEBIT);
	            
	            addJournal (oInvTrans, oInv, _vGLTrans);
	            addJournal (oCOGSTrans, oCOGS, _vGLTrans);
	            
	            /*
	            if (log.isDebugEnabled())
	            {
	            	log.debug ("Sales Journal COGS : " + 
	            		AccountTool.getAccountNameByID (oCOGSTrans.getAccountId(), m_oConn) + " " + oCOGSTrans);
	            	
	            	log.debug ("Sales Journal Inventory : " + 
	            		AccountTool.getAccountNameByID (oInvTrans.getAccountId(), m_oConn)  + " " + oInvTrans);
	            }
	            */
        	}
        	else if (_oItem.getItemType() == i_GROUPING)
        	{
        		createGroupingJournal (_oTD.getProjectId(), _oTD.getDepartmentId(), 
        			_oTD.getQty().doubleValue(), _oItem, _vGLTrans, _mGroup, false, false, m_oConn);
        	}
        }
        else //if from DO 
        {
        	if (_oItem.getItemType() == i_INVENTORY_PART)
        	{
	        	String sCOGS = _oItem.getCogsAccount();
	        	Account oDO = AccountTool.getDeliveryOrderAccount(sCOGS, m_oConn);
	        	if (StringUtil.isNotEmpty(sCOGS) && oDO != null &&
	        			!sCOGS.equals(oDO.getAccountId())) //DO Account != COGS account
	        	{				
	        		//DO ACCOUNT -> CREDIT
		            GlTransaction oDOTrans       = new GlTransaction();
		            oDOTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
		            oDOTrans.setProjectId        (_oTD.getProjectId());
		            oDOTrans.setDepartmentId     (_oTD.getDepartmentId());
		            oDOTrans.setAccountId        (oDO.getAccountId());
		            oDOTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
		            oDOTrans.setCurrencyRate     (bd_ONE);
		            oDOTrans.setAmount           (dCost);
		            oDOTrans.setAmountBase       (dCost);
			        oDOTrans.setDebitCredit      (i_CREDIT);
	        	
		            //COGS -> DEBIT
		            Account oCOGS = AccountTool.getAccountByID (sCOGS, m_oConn);
		            validate (oCOGS, _oItem.getItemCode(), ItemPeer.COGS_ACCOUNT);
		
		            GlTransaction oCOGSTrans = new GlTransaction();
		            oCOGSTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
		            oCOGSTrans.setProjectId        (_oTD.getProjectId());
		            oCOGSTrans.setDepartmentId     (_oTD.getDepartmentId());
		            oCOGSTrans.setAccountId        (oCOGS.getAccountId());
		            oCOGSTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
		            oCOGSTrans.setCurrencyRate     (bd_ONE);
		            oCOGSTrans.setAmount           (dCost);
		            oCOGSTrans.setAmountBase       (dCost);
			        oCOGSTrans.setDebitCredit      (i_DEBIT);
		            
		            addJournal (oDOTrans, oDO, _vGLTrans);
		            addJournal (oCOGSTrans, oCOGS, _vGLTrans);   
	        	}
        	}
        	else if (_oItem.getItemType() == i_GROUPING)
        	{
        		if (StringUtil.isNotEmpty(GLConfigTool.getGLConfig(m_oConn).getDeliveryOrderAccount()))
        		{
	        		createGroupingJournal (_oTD.getProjectId(), _oTD.getDepartmentId(), 
	        			_oTD.getQty().doubleValue(), _oItem, _vGLTrans, _mGroup, false, true, m_oConn);
        		}
        	}        	
        }
        
        //TODO: TAX IN BASE CURRENCY
        if (!m_bConsignment)
        {
        	if (dDisc != 0)
        	{ 
        		//DISCOUNT -> DEBIT            
        		String sDisc  = _oItem.getItemDiscountAccount();
        		Account oDisc = AccountTool.getAccountByID (sDisc, m_oConn);
        		validate (oDisc, _oItem.getItemCode(), ItemPeer.ITEM_DISCOUNT_ACCOUNT);
        		
        		GlTransaction oDiscTrans = new GlTransaction();
        		oDiscTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
        		oDiscTrans.setProjectId        (_oTD.getProjectId());
        		oDiscTrans.setDepartmentId     (_oTD.getDepartmentId());
        		oDiscTrans.setAccountId        (oDisc.getAccountId());
        		oDiscTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
        		oDiscTrans.setCurrencyRate     (m_bdRate);
        		oDiscTrans.setAmount           (new BigDecimal(dDisc));
        		oDiscTrans.setAmountBase       (new BigDecimal(dDiscBase));
        		oDiscTrans.setDebitCredit      (i_DEBIT);
        		
        		addJournal (oDiscTrans, oDisc, _vGLTrans);	
        	}
        	
        	//REVENUE -> CREDIT
        	String sRevenue  = _oItem.getSalesAccount();
        	Account oRevenue = AccountTool.getAccountByID ( sRevenue, m_oConn );
        	validate (oRevenue, _oItem.getItemCode(), ItemPeer.SALES_ACCOUNT);
        	
        	GlTransaction oRevenueTrans = new GlTransaction();
        	oRevenueTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
        	oRevenueTrans.setProjectId        (_oTD.getProjectId());
        	oRevenueTrans.setDepartmentId     (_oTD.getDepartmentId());
        	oRevenueTrans.setAccountId        (oRevenue.getAccountId());
        	oRevenueTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
        	oRevenueTrans.setCurrencyRate     (m_bdRate);
        	oRevenueTrans.setAmount           (new BigDecimal(dRevenue));
        	oRevenueTrans.setAmountBase       (new BigDecimal(dRevenueBase));
        	oRevenueTrans.setDebitCredit      (i_CREDIT);
        	
        	//CHECK whether this item is ROUNDING ITEM used to clear unbalance amount
        	Item oRounding = MedConfigTool.getRoundingItem();
            if (oRounding != null && _oItem.getItemCode().equals(oRounding.getItemCode()))
            {
            	m_oRoundingGLT  = oRevenueTrans;
            }
            
        	addJournal (oRevenueTrans, oRevenue, _vGLTrans);	
        	
        	/*
        	if (log.isDebugEnabled())
        	{
        		log.debug ("Sales Journal Revenue : " + 
        			AccountTool.getAccountNameByID (oRevenueTrans.getAccountId(), m_oConn)  + " " + oRevenueTrans);
        	}*/
        }
    }//end if consignment
	
	/**
	 * 
	 * @param _sProjectID
	 * @param _sDepartmentID
	 * @param _dQty
	 * @param _oItem
	 * @param _vGLTrans
	 * @param _mGroup
	 * @param _bFromDO
	 * @param _bFromInvDO
	 * @param _oConn
	 * @throws Exception
	 */
	private void createGroupingJournal(String _sProjectID,
									   String _sDepartmentID,
									   double _dQty,
									   Item _oItem, 
									   List _vGLTrans,
									   Map _mGroup,
									   boolean _bFromDO,
									   boolean _bFromInvDO,
									   Connection _oConn) 
		throws Exception
	{
		List vGroup = (List) _mGroup.get(_oItem.getItemId());
		for (int i = 0; i < vGroup.size(); i++) 
		{	
			ItemGroup oItemGroup = (ItemGroup) vGroup.get (i);	 
			Item oItemPart = ItemTool.getItemByID(oItemGroup.getItemId(), _oConn);
			
			double dBaseValue = UnitTool.getBaseValue(oItemGroup.getItemId(), oItemGroup.getUnitId(), _oConn);
			double dGroupQty = oItemGroup.getQty().doubleValue();
			double dQtyBase = _dQty * dGroupQty * dBaseValue;
			
			double dCost = oItemGroup.getCost().doubleValue();
			//InventoryLocationTool.getItemCost(oItemPart.getItemId(), 
				//m_sLocationID, m_dTransDate, _oConn).doubleValue();
			
			BigDecimal  dAmount = new BigDecimal (dCost * dQtyBase);
			
			String sInventory = oItemPart.getInventoryAccount(); 
	        Account oInv = AccountTool.getAccountByID (sInventory, _oConn);        
			if (_bFromInvDO) //if from invoice with DO, use DO Account
			{
				oInv = AccountTool.getDeliveryOrderAccount(oItemPart.getCogsAccount(), _oConn);
			}
	        validate (oInv, oItemPart.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);
			
	        GlTransaction oInvTrans       = new GlTransaction();
	        oInvTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
	        oInvTrans.setProjectId        (_sProjectID);
	        oInvTrans.setDepartmentId     (_sDepartmentID);
	        oInvTrans.setAccountId        (oInv.getAccountId());
	        oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oInvTrans.setCurrencyRate     (bd_ONE);
	        oInvTrans.setAmount           (dAmount);
	        oInvTrans.setAmountBase       (dAmount);
	        oInvTrans.setDebitCredit      (i_CREDIT);

			
	        //COGS -> DEBIT -- COGS will go to group account
	        String sCOGS  = _oItem.getCogsAccount();	        	        
			String sCode  = _oItem.getItemCode();
	        if (i_GL_GROUPING_COST == 2) //cogs journaled to item part
			{
				sCOGS = oItemPart.getCogsAccount();
				sCode = oItemPart.getItemCode();
			}
	        
	        Account oCOGS = AccountTool.getAccountByID ( sCOGS, _oConn );
	        if (_bFromDO) //if from DO, use DO / COGS Account
	        {
	        	oCOGS = AccountTool.getDeliveryOrderAccount( sCOGS, _oConn );
	        }
	        validate (oCOGS, sCode, ItemPeer.COGS_ACCOUNT);

	        GlTransaction oCOGSTrans = new GlTransaction();
	        oCOGSTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
	        oCOGSTrans.setProjectId        (_sProjectID);
	        oCOGSTrans.setDepartmentId     (_sDepartmentID);
	        oCOGSTrans.setAccountId        (oCOGS.getAccountId());
	        oCOGSTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oCOGSTrans.setCurrencyRate     (bd_ONE);
	        oCOGSTrans.setAmount           (dAmount);
	        oCOGSTrans.setAmountBase       (dAmount);
	        oCOGSTrans.setDebitCredit      (i_DEBIT);
	        
	        addJournal (oInvTrans, oInv, _vGLTrans);
	        addJournal (oCOGSTrans, oCOGS, _vGLTrans);
	        
            if (log.isDebugEnabled())
            {
            	log.debug ("Grouping Journal COGS : " + 
            		AccountTool.getAccountNameByID (oCOGSTrans.getAccountId(), _oConn) + oCOGSTrans);
            	
            	log.debug ("Grouping Journal Inventory : " + 
            		AccountTool.getAccountNameByID (oInvTrans.getAccountId(), _oConn)  + oInvTrans);
            }	        
		}		
	}

	private void createPaymentJournal(SalesTransaction _oTR, List _vPayment, List _vGLTrans) 
    	throws Exception
    {
        //if this invoice using multiple payment then iterate all the payment type in _vPayment
        if (_vPayment.size() > 0)
        {
            for (int i = 0; i < _vPayment.size(); i++)
            {
                InvoicePayment oPmt = (InvoicePayment) _vPayment.get(i);
                log.debug ("Invoice Payment " + i + " : " + oPmt);

                double dAmount = oPmt.getPaymentAmount().doubleValue();
                double dAmountBase = dAmount * m_dRate;
            
                PaymentType oType = PaymentTypeTool.getPaymentTypeByID(oPmt.getPaymentTypeId(), m_oConn);
                GlTransaction oPaymentTrans = new GlTransaction();
                
                Account oPayment = null; 
                if (oType.getIsCredit())
                {                 
                    oPayment = AccountTool.getAccountReceivable(
                    	m_oCurrency.getCurrencyId(), m_oCustomer.getCustomerId(), m_oConn);
                    validate (oPayment, m_oCurrency.getCurrencyCode(), CurrencyPeer.AR_ACCOUNT);
                    oPaymentTrans.setAccountId (oPayment.getAccountId());
                    
                    //log.debug ("Sales Journal AR : " + oPayment.getAccountName()  + " " + oPaymentTrans);
                }         
                else //if cash
                {
                    oPayment = AccountTool.getDirectSalesAccount(oType,m_oConn);
                    validate (oPayment, "GL Config", GlConfigPeer.DIRECT_SALES_ACCOUNT);
                    oPaymentTrans.setAccountId (oPayment.getAccountId());               
                    
                    //log.debug ("Sales Journal Cash : " +  oPayment.getAccountName() + " "  + oPaymentTrans);    
                }
                
                oPaymentTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
                oPaymentTrans.setProjectId        ("");
                oPaymentTrans.setDepartmentId     ("");
                oPaymentTrans.setPeriodId         (m_oPeriod.getPeriodId());
                oPaymentTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
                oPaymentTrans.setCurrencyRate     (m_bdRate);
                oPaymentTrans.setAmount           (new BigDecimal(dAmount));
                oPaymentTrans.setAmountBase       (new BigDecimal(dAmountBase));
                oPaymentTrans.setUserName         (_oTR.getCashierName());
                oPaymentTrans.setDebitCredit      (i_DEBIT);
                
                addJournal (oPaymentTrans, oPayment, _vGLTrans);                   
            }
	    }
        else
        {                
            double dAmount = _oTR.getTotalAmount().doubleValue();
            double dAmountBase = dAmount * m_dRate;
            PaymentType oType = PaymentTypeTool.getPaymentTypeByID(_oTR.getPaymentTypeId(), m_oConn);
            GlTransaction oPaymentTrans = new GlTransaction();
            Account oPayment = null;
            
            if (oType.getIsCredit())
            {                    
                String sAR  = m_oCustomer.getArAccount();
                if (StringUtil.isEmpty(sAR))
                {
                	sAR = m_oCurrency.getArAccount();
                }
                oPayment = AccountTool.getAccountByID (sAR, m_oConn);
                validate (oPayment, m_oCurrency.getCurrencyCode(), CurrencyPeer.AR_ACCOUNT);
                oPaymentTrans.setAccountId (oPayment.getAccountId());
                
                //log.debug ("Sales Journal AR : " + oPayment.getAccountName() + " " + oPaymentTrans );
            }         
            else //if cash
            {
                oPayment = AccountTool.getDirectSalesAccount(oType, m_oConn);
                validate (oPayment, "GL Config", GlConfigPeer.DIRECT_SALES_ACCOUNT);
                oPaymentTrans.setAccountId (oPayment.getAccountId());               
                
                //log.debug ("Sales Journal Cash : " + oPayment.getAccountName() + " "  + oPaymentTrans );    
            }
            
            oPaymentTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
            oPaymentTrans.setProjectId        ("");
            oPaymentTrans.setDepartmentId     ("");
            oPaymentTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
            oPaymentTrans.setCurrencyRate     (m_bdRate);
            oPaymentTrans.setAmount           (new BigDecimal(dAmount));
            oPaymentTrans.setAmountBase       (new BigDecimal(dAmountBase));
            oPaymentTrans.setDebitCredit      (i_DEBIT);
            addJournal (oPaymentTrans, oPayment, _vGLTrans);	
        }    
	}

	/**
	 * @param _oTR Sales Transaction Object
	 * @param _vGLTrans GLTrans List
	 */
	private void createTotalDiscountJournal(SalesTransaction _oTR, List _vTD, List _vGLTrans) 
		throws Exception
	{
		double dTotalItemDisc = TransactionTool.countDiscount(_vTD).doubleValue();
		double dAmount = _oTR.getTotalDiscount().doubleValue() - dTotalItemDisc;
		double dAmountBase = dAmount * m_dRate;
		
		if (log.isDebugEnabled())
		{
			log.debug("createTotalDiscountJournal : TD "    + _oTR.getTotalDiscount().doubleValue());
			log.debug("createTotalDiscountJournal : TDPCT " + _oTR.getTotalDiscountPct());
			log.debug("createTotalDiscountJournal : INVAMT " + _oTR.getTotalAmount().doubleValue());		
			log.debug("createTotalDiscountJournal : DISAMT " + dAmount);
		}

		//if invoice total discount exist then create invoice sales discount journal
        if (dAmount != 0)
        {
        	String sInvDisc = _oTR.getInvoiceDiscAccId(); //use invoice discount account ID set in trans
        	if (StringUtil.isEmpty(sInvDisc))
        	{
        		sInvDisc = m_oCurrency.getSdAccount();
        	}
        	Account oInvDisc = AccountTool.getAccountByID(sInvDisc, m_oConn);
            validate (oInvDisc, m_oCurrency.getCurrencyCode(), CurrencyPeer.SD_ACCOUNT);

        	GlTransaction oDiscTrans = new GlTransaction();
            oDiscTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
            oDiscTrans.setProjectId        ("");
            oDiscTrans.setDepartmentId     ("");
            oDiscTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
            oDiscTrans.setAccountId        (oInvDisc.getAccountId());
            oDiscTrans.setCurrencyRate     (m_bdRate);
            oDiscTrans.setAmount           (new BigDecimal(dAmount));
            oDiscTrans.setAmountBase       (new BigDecimal(dAmountBase));
            oDiscTrans.setDebitCredit      (i_DEBIT);
            addJournal (oDiscTrans, oInvDisc, _vGLTrans);	
        }		
	}

	/**
	 * @param _oTR Sales Transaction Object
	 * @param _vGLTrans GLTrans List
	 */
	private void createFreightJournal(SalesTransaction _oTR, List _vGLTrans) 
		throws Exception
	{
		double dAmount = _oTR.getTotalExpense().doubleValue();
		double dAmountBase = dAmount * m_dRate;

		//if invoice total discount exist then create invoice sales discount journal
        if (dAmount > 0 && _oTR.getIsInclusiveFreight())
        {
        	Account oFreight = AccountTool.getAccountByID(_oTR.getFreightAccountId(), m_oConn);
            validate (oFreight, _oTR.getInvoiceNo(), SalesTransactionPeer.FREIGHT_ACCOUNT_ID);

        	GlTransaction oFreightTrans = new GlTransaction();
            oFreightTrans.setTransactionType  (i_GL_TRANS_SALES_INVOICE);
            oFreightTrans.setProjectId        ("");
            oFreightTrans.setDepartmentId     ("");
            oFreightTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
            oFreightTrans.setAccountId        (oFreight.getAccountId());
            oFreightTrans.setCurrencyRate     (m_bdRate);
            oFreightTrans.setAmount           (new BigDecimal(dAmount));
            oFreightTrans.setAmountBase       (new BigDecimal(dAmountBase));
            oFreightTrans.setDebitCredit      (i_CREDIT);
            
            addJournal (oFreightTrans, oFreight, _vGLTrans);	
        }		
	}
	
	/**
	 * delete Sals Journal Rollback, Update Account Balance 
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */	
	public static void deleteSalesJournal(int _iTransType, String _sTransID, Connection _oConn) 
    	throws Exception
    {
		deleteJournal (_iTransType, _sTransID, _oConn);
	}
}