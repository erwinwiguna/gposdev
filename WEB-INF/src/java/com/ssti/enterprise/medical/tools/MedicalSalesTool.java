package com.ssti.enterprise.medical.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.om.MedicineSalesDetail;
import com.ssti.enterprise.medical.om.MedicineSalesDetailPeer;
import com.ssti.enterprise.medical.om.MedicineSalesPeer;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.CustomerTypePeer;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.sales.CreditLimitValidator;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Business object controller for MedicineSales OM
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * 
 * 2018-06-16
 * - add txType & parentId field in MedicineSalesDetail to enable input pres & powder 
 *   directly from POS 
 * - add method getTxTypeCode & getTxTypeDesc 
 *    
 * 2017-02-22
 * - move addMedRecord from MedicalSalesTrans, so this method can be used by multiple screens
 * 
 * 2016-12-14
 * - add new module getInsurance()
 * 
 * 2016-12-08
 * - rename to MedicalSalesTool
 * - remove rounding Item usage, now in SalesTransaction there is rounding column
 * - remove the need of MedSalesJournalTool
 * 
 * 2015-10-10
 * - invoke MedSalesJournalTool from method saveData to remove dependencies
 * 
 * 2015-07-31
 * -Move validateMED from Credit Limit Validator to prevent linked module from POS to MED
 * </pre><br>
 */
public class MedicalSalesTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( MedicalSalesTool.class );
	
	public static final int i_PRS_TYPE = 5;
	
	public static final String s_ITEM = new StringBuilder(MedicineSalesPeer.MEDICINE_SALES_ID).append(",")
	.append(MedicineSalesDetailPeer.MEDICINE_SALES_ID).append(",")
	.append(MedicineSalesDetailPeer.ITEM_ID).toString();
	
	public static final String s_TAX = new StringBuilder(MedicineSalesPeer.MEDICINE_SALES_ID).append(",")
	.append(MedicineSalesDetailPeer.MEDICINE_SALES_ID).append(",")
	.append(MedicineSalesDetailPeer.TAX_ID).toString();
	
	//field to skip from set scale
	public static final String[] a_SKIP_FIELD = {"qty", "qtyBase"};
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), MedicineSalesPeer.INVOICE_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), MedicineSalesPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER), 	 MedicineSalesPeer.CUSTOMER_ID);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), MedicineSalesPeer.CASHIER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), MedicineSalesPeer.CASHIER_NAME);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), MedicineSalesPeer.LOCATION_ID);             
		
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), MedicineSalesPeer.PAYMENT_TYPE_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), MedicineSalesPeer.PAYMENT_TERM_ID);
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), MedicineSalesPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), MedicineSalesPeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), MedicineSalesPeer.CURRENCY_ID);
		m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), "");
		m_FIND_PEER.put (Integer.valueOf(i_SALESMAN   ), MedicineSalesPeer.SALES_ID);   
		
		//modules specific
		m_FIND_PEER.put (Integer.valueOf(50), MedicineSalesPeer.PATIENT_NAME);
		m_FIND_PEER.put (Integer.valueOf(51), MedicineSalesPeer.PATIENT_PHONE);		
		m_FIND_PEER.put (Integer.valueOf(52), MedicineSalesPeer.DOCTOR_NAME);
		addInv(m_FIND_PEER, s_ITEM);

		m_FIND_TEXT.put (Integer.valueOf(50), LocaleTool.getString("patient_name"));
		m_FIND_TEXT.put (Integer.valueOf(51), LocaleTool.getString("phone"));
		m_FIND_TEXT.put (Integer.valueOf(52), LocaleTool.getString("doctor_name"));
	}   
	
	protected static Map m_GROUP_PEER = new HashMap();
	
	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), MedicineSalesPeer.TRANSACTION_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), MedicineSalesPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), MedicineSalesPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), MedicineSalesPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), MedicineSalesPeer.CASHIER_NAME);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), "");               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), MedicineSalesPeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), MedicineSalesPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), MedicineSalesPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), MedicineSalesPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), MedicineSalesPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), MedicineSalesPeer.SALES_ID); 
	}
	
	static MedicalSalesTool instance = null;
	
	public static synchronized MedicalSalesTool getInstance() 
	{
		if (instance == null) instance = new MedicalSalesTool();
		return instance;
	}	
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
	
	public static MedicineSales getHeaderByID(String _sID) 
	throws Exception
	
	{
		return getHeaderByID (_sID, null);
	}
	
	/**
	 * get header by ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static MedicineSales getHeaderByID(String _sID, Connection _oConn) 
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedicineSalesPeer.MEDICINE_SALES_ID, _sID);
		List vData = MedicineSalesPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (MedicineSales) vData.get(0);
		}
		return null;	
	}
	
	public static List getDetailsByID(String _sID) 
	throws Exception
	{
		return getDetailsByID(_sID, null);
	}
	
	/**
	 * get details by id
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn) 
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedicineSalesDetailPeer.MEDICINE_SALES_ID, _sID);
		oCrit.addAscendingOrderByColumn(MedicineSalesDetailPeer.MEDICINE_SALES_DETAIL_ID);
		return MedicineSalesDetailPeer.doSelect(oCrit, _oConn);
	}
	
	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static MedicineSalesDetail getDetailByDetailID(String _sID) 
	throws Exception
	{
		return getDetailByDetailID(_sID, null) ;
	}	
	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static MedicineSalesDetail getDetailByDetailID(String _sID, Connection _oConn) 
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedicineSalesDetailPeer.MEDICINE_SALES_DETAIL_ID, _sID);
		List vData = MedicineSalesDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) return (MedicineSalesDetail) vData.get(0);
		return null;
	}		
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteDetailsByID (String _sID, Connection _oConn) 
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedicineSalesDetailPeer.MEDICINE_SALES_ID, _sID);
		MedicineSalesDetailPeer.doDelete (oCrit, _oConn);
	}
	
	/**
	 * update print times indicate how many times this invoice printed
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
	public static void updatePrintTimes (MedicineSales _oTR) 
	throws Exception
	{
		_oTR.setPrintTimes (_oTR.getPrintTimes() + 1);
		_oTR.save();
	}
	
	///////////////////////////////////////////////////////////////////////////
	////transaction processing method
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * set header properties from screen entry
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param data
	 * @param _iIsImportDO
	 * @throws Exception
	 */
	public static void setHeaderProperties (MedicineSales _oTR, List _vTD, RunData data) 
		throws Exception
	{
		try 
		{
			ValueParser formData = null;
			if (data != null) 
			{
				formData = data.getParameters();
				formData.setProperties  (_oTR);
				_oTR.setTransactionDate (CustomParser.parseDate(formData.getString("TransactionDate")));    		
				_oTR.setIsTaxable  		(formData.getBoolean("IsTaxable", false));
				_oTR.setIsInclusiveTax  (formData.getBoolean("IsInclusiveTax", false));			
			}			
			
			_oTR.setTotalQty 	    (new BigDecimal(countQty(_vTD)));			
			_oTR.setTotalCost	    (countCost(_vTD));
			_oTR.setTotalAmount     (countSubTotal(_vTD));
			_oTR.setTotalDiscount   (countDiscount(_vTD));
			setDueDate (_oTR);
			
			//if this is invoice not contains prescription, check if POS value discount exists
//			if (!containsPres(_vTD)) 
//			{
//				
//			}
			
			//count total amount after total discount percentage
			String sTotalDiscountPct = _oTR.getTotalDiscountPct();
			double dTotalDiscountAmount = 0;
			double dTotalAmount = _oTR.getTotalAmount().doubleValue();
			
			_oTR.setTotalTax (countTax(sTotalDiscountPct,_vTD,_oTR));
			
			if (StringUtil.isNotEmpty(sTotalDiscountPct)) 
			{
				//get discount for all 
				//count total discount from total amount
				dTotalDiscountAmount = Calculator.calculateDiscount(sTotalDiscountPct,dTotalAmount);
				double dSubTotalDiscount = _oTR.getTotalDiscount().doubleValue();
				_oTR.setTotalDiscount (new BigDecimal(dTotalDiscountAmount + dSubTotalDiscount));
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount - dTotalDiscountAmount));							
			}
			
			//count Total Amount with tax
			if(!_oTR.getIsInclusiveTax())
			{
				dTotalAmount = _oTR.getTotalAmount().doubleValue() + _oTR.getTotalTax().doubleValue();
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
			}
			
			//if freight cost > 0 add freight cost to total amount
			if(_oTR.getTotalExpense().doubleValue() > 0)
			{
				dTotalAmount = _oTR.getTotalAmount().doubleValue() + _oTR.getTotalExpense().doubleValue();
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
			}
			
			BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oTR);
			
			log.debug("Total Amount : " + dTotalAmount);
			
			//calculate rounding
			double dRounded = Calculator.roundNumber(_oTR.getTotalAmount(), 
					MedConfigTool.getRoundingScale(), 
					MedConfigTool.getRoundingMode());
			
			log.debug("Rounded Amount : " + dRounded);
			
			double dDelta = dRounded - _oTR.getTotalAmount().doubleValue();
			_oTR.setTotalAmount(new BigDecimal(dRounded));
			_oTR.setRoundingAmount(new BigDecimal(dDelta));
			//init / setPayment
			//InvoicePaymentTool.setPayment(_oTR, data); 
			
			//if CC Charges exist then reset payment amount
			if(_oTR.getTotalExpense().doubleValue() > 0)
			{
				_oTR.setPaymentAmount(_oTR.getTotalAmount());
				_oTR.setChangeAmount(bd_ZERO);
			}
			
			//recalculate Payment Amount & Change Amount
			if (_oTR.getTotalAmount().doubleValue() == 0 || 
					(_oTR.getTotalAmount().doubleValue() == _oTR.getPaymentAmount().doubleValue()))
			{
				_oTR.setPaymentAmount(_oTR.getTotalAmount());
				_oTR.setChangeAmount(bd_ZERO);            		            	
			}
			/*
			 if((_oTR.getTotalAmount().doubleValue() != _oTR.getPaymentAmount().doubleValue()) && 
			 _oTR.getTotalAmount().doubleValue() > 0)
			 {
			 double dChange = _oTR.getPaymentAmount().doubleValue() - _oTR.getTotalAmount().doubleValue();
			 if (dChange >= 0)
			 {
			 _oTR.setChangeAmount(new BigDecimal(dChange));
			 }
			 else
			 {
			 _oTR.setPaymentAmount(_oTR.getTotalAmount());
			 _oTR.setChangeAmount(bd_ZERO);            		
			 }
			 }
			 else
			 {
			 _oTR.setChangeAmount(bd_ZERO);            		            	
			 }
			 */
			
			//log.debug(" #$@#%#$%$^ " + _oTR);
			
			BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _vTD, a_SKIP_FIELD);
			BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oTR);
			
            if (data != null)
            {
			    //init / setPayment
			    InvoicePaymentTool.setPayment(
			    	_oTR.getPaymentTypeId(), _oTR.getPaymentTermId(), _oTR.getTotalAmount(), data); 
            }
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
	}
	
	/**
	 * update detail properties
	 * 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 
	 /**
	 * update detail properties
	 * 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 */
    public static void updateDetail (List _vDet, RunData data)
    	throws Exception
    {
    	ValueParser frm = data.getParameters();
    	boolean bIsIncTax = data.getParameters().getBoolean("IsInclusiveTax");
		try
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				MedicineSalesDetail oTD = (MedicineSalesDetail) _vDet.get(i);
				
				//log.debug ("Before update : "  + oDetail );
				//check null to prevent refresh from user that didn't submit param from data						
				if(frm.getString("Qty"       + (i + 1)) != null) oTD.setQty 	  (frm.getBigDecimal("Qty"          + (i + 1)));					
				if(frm.getString("ItemPrice" + (i + 1)) != null) oTD.setItemPrice (frm.getBigDecimal("ItemPrice"    + (i + 1)));
				if(frm.getString("TaxId"     + (i + 1)) != null) oTD.setTaxId     (frm.getString    ("TaxId"        + (i + 1)));
				if(frm.getString("TaxAmount" + (i + 1)) != null) oTD.setTaxAmount (frm.getBigDecimal("TaxAmount"    + (i + 1)));
				
				oTD.setQtyBase 		(UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
				//oTD.setSubTotalTax  ( formData.getBigDecimal("SubTotalTax"  + (i + 1)) );
				//oTD.setSubTotalDisc ( formData.getBigDecimal("SubTotalDisc" + (i + 1)) );
				//oTD.setSubTotal     ( formData.getBigDecimal("SubTotal"     + (i + 1)) );				
                
				BigDecimal bdSubTotal = oTD.getItemPrice().multiply(oTD.getQty());
				BigDecimal bdSubDisc = new BigDecimal(Calculator.calculateDiscount(oTD.getDiscount(), bdSubTotal));
				bdSubTotal = bdSubTotal.subtract(bdSubDisc);
				BigDecimal bdSubTax = bd_ZERO;
				if(bIsIncTax)
				{							
					bdSubTax = bdSubTotal.subtract(new BigDecimal(bdSubTotal.doubleValue() / ((100 + oTD.getTaxAmount().doubleValue()) / 100)));
				}
				else
				{
					bdSubTax = new BigDecimal(bdSubTotal.doubleValue() * (oTD.getTaxAmount().doubleValue() / 100));
				}	
				
				oTD.setSubTotalTax(bdSubTax);
				oTD.setSubTotalDisc(bdSubDisc);
				oTD.setSubTotal(bdSubTotal);
				
				//log.debug ("After update : "  + oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Update Detail Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
	/**
	 * count total qty 
	 * @param _vDetails
	 * @return
	 */
	private static boolean containsPres (List _vDetails) 
		throws Exception
	{
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedicineSalesDetail oTD = (MedicineSalesDetail) _vDetails.get(i);
			if (StringUtil.isNotEmpty(oTD.getPrescriptionId()))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * count total qty 
	 * @param _vDetails
	 * @return
	 */
	private static double countQty (List _vDetails) 
	throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedicineSalesDetail oTD = (MedicineSalesDetail) _vDetails.get(i);
			if (oTD.getQtyBase() == null) //if from add detail
			{
				oTD.setQtyBase (UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
			}
			//the best we can do is count the base qty
			dQty += oTD.getQtyBase().doubleValue();
		}
		return dQty;
	}
	
	/**
	 * count qty per item
	 * 
	 * @param _vDetails
	 * @param _sItemID
	 * @return
	 */
	private static double countQty (List _vDetails, String _sItemID)
	{
		return countQtyPerItem (_vDetails, _sItemID, "qty");
	}
	
	private static BigDecimal countTax (String _sDiscountPct,List _vTD,MedicineSales _oTR)
	{
		double dTotalAmount = _oTR.getTotalAmount().doubleValue();
		double dAmt = 0;
		for (int i = 0; i < _vTD.size(); i++)
		{
			MedicineSalesDetail _oTD = (MedicineSalesDetail) _vTD.get(i);
			double dSubTotal = _oTD.getSubTotal().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDiscountPct, dSubTotal);
			
			// if total amount
			if(!_sDiscountPct.contains(Calculator.s_PCT))
			{
				if (dTotalAmount > 0) //if dTotalAmount > 0, prevent divide by zero
				{
					dSubTotal = dSubTotal - (dDisc * (dSubTotal / dTotalAmount));
				}
			}
			else
			{
				dSubTotal = dSubTotal - dDisc;
			}
			
			
			double dSubTotalTax = 0;
			if(!_oTR.getIsInclusiveTax())
			{
				dSubTotalTax = dSubTotal * _oTD.getTaxAmount().doubleValue() / 100;
			}
			else
			{
				//calculate inclusive Tax
				dSubTotalTax = (dSubTotal * 100) / (_oTD.getTaxAmount().doubleValue() + 100);
				dSubTotalTax = dSubTotal - dSubTotalTax;  
			}
			
			_oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
			dAmt += _oTD.getSubTotalTax().doubleValue();
		}
		return new BigDecimal(dAmt);
	}
	
	private static BigDecimal countDiscount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedicineSalesDetail oDet = (MedicineSalesDetail) _vDetails.get(i);
			dAmt += oDet.getSubTotalDisc().doubleValue();
		}
		return new BigDecimal(dAmt);
	}
	
	private static BigDecimal countCost (List _vDetails) 
	throws Exception
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedicineSalesDetail oDet = (MedicineSalesDetail) _vDetails.get(i);
			double dCost = oDet.getItemCost().doubleValue();
			double dQtyBase = UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty().doubleValue());
			double dSubTotalCost = dCost * dQtyBase;
			oDet.setSubTotalCost(new BigDecimal(dSubTotalCost));
			
			dAmt += dSubTotalCost;
		}
		return new BigDecimal(dAmt);
	}
	
	private static BigDecimal countSubTotal (List _vDetails) 
		throws Exception 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			MedicineSalesDetail oDet = (MedicineSalesDetail) _vDetails.get(i);
			
			double dPrice = oDet.getItemPrice().doubleValue();
			String sDisc = oDet.getDiscount();
			double dTax	= oDet.getTaxAmount().doubleValue();
			
			double dQty = oDet.getQty().doubleValue();
			double dSubTotal = dQty * dPrice;
			double dSubTotalDisc = Calculator.calculateDiscount(sDisc, dSubTotal);
			dSubTotal = dSubTotal - dSubTotalDisc;
			
			if (oDet.getItemType() != i_PRS_TYPE)
			{
				MedConfigTool.calculateRfee(oDet, true, dQty, dSubTotal);
			}
			double dRfee = Double.parseDouble(oDet.getRfee()); //calculated amount
			double dPack = Double.parseDouble(oDet.getPackaging()); //calculated amount
			double dTotal = dSubTotal + dRfee + dPack;
			oDet.setSubTotalDisc(new BigDecimal(dSubTotalDisc));			
			oDet.setSubTotal(new BigDecimal(dSubTotal));
			oDet.setTotal(new BigDecimal(dTotal));
			dAmt += dTotal;
		}
		return new BigDecimal(dAmt).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * set Payment Due Date
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
	private static void setDueDate (MedicineSales _oTR) 
		throws Exception 
	{
		boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oTR.getPaymentTypeId());
		if (bIsCredit) 
		{ 
			if (_oTR.getTransactionDate() != null)
			{
				//check default payment term for this customer
				String sDefaultTerm = CustomerTool.getDefaultTermByID(_oTR.getCustomerId(), null);
				int iDueDays = 0;
				if (StringUtil.isNotEmpty(sDefaultTerm) && 
						PaymentTermTool.isCashPaymentTerm(_oTR.getPaymentTermId())) 
				{
					iDueDays = PaymentTermTool.getNetPaymentDaysByID (sDefaultTerm);
					_oTR.setPaymentTermId(sDefaultTerm);
				}
				else 
				{
					iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oTR.getPaymentTermId());
				}
				Calendar oCal = new GregorianCalendar ();
				oCal.setTime (_oTR.getTransactionDate());
				oCal.add (Calendar.DATE, iDueDays);
				_oTR.setDueDate (oCal.getTime());		
			}
		}
		else
		{
			if (_oTR.getTransactionDate() != null)
			{
				_oTR.setDueDate(_oTR.getTransactionDate());
			}
			else
			{				
				_oTR.setDueDate(new Date());
			}
		}
	}
	
	/**
	 * generate transaction no
	 * 
	 * @param _oTR
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	private static String generateTransactionNo (MedicineSales _oTR, Connection _oConn) 
	throws Exception
	{
		String sTransNo = "";
		
		//if using separate taxable invoice no
		if (TransactionAttributes.b_SALES_SEPARATE_TAX_INV)
		{
			if (_oTR.getIsTaxable() || (_oTR.getTotalTax().doubleValue() > 0))
			{
				sTransNo = LastNumberTool.get(s_TX_FORMAT, LastNumberTool.i_TAX_INVOICE, _oConn);			
				return sTransNo;
			}
		}
		sTransNo = LastNumberTool.get(s_SI_FORMAT, LastNumberTool.i_SALES_INVOICE, _oConn);	
		return sTransNo;
	}

	public static void validateMED(MedicineSales _oSI)
			throws Exception
	{
		if (_oSI != null)
		{
			boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oSI.getPaymentTypeId());
			//validate Credit Limit first
			int iResult = CreditLimitValidator.validateTransAmount(_oSI.getCustomerId(), _oSI.getTotalAmount());
			if (iResult != CreditLimitValidator.OK && bIsCredit)
			{
				_oSI.setInvoiceNo("");
				_oSI.setStatus(AppAttributes.i_PENDING);		
				throw new Exception(CreditLimitValidator.getMessage(iResult));
			}
		}	
	}

	
	///////////////////////////////////////////////////////////////////////
	// Save Transaction
	///////////////////////////////////////////////////////////////////////
	
	public static void validatePayment (MedicineSales _oTR, List _vPMT)
			throws Exception
	{
		if (_vPMT.size() > 0)
		{
			double dTotalPayment = InvoicePaymentTool.calculatePayment(_vPMT);
			double dTotalInvoice = _oTR.getTotalAmount().doubleValue();
			if (dTotalPayment != dTotalInvoice )
			{
				throw new Exception (
						"Payment Amount != Invoice Amount : " + CustomFormatter.formatNumber(dTotalPayment));
			}
		}
		
		PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(_oTR.getPaymentTypeId());
		if(oPT != null && !oPT.getShowInPos())
		{
			throw new Exception ("Payment Type " + oPT.getDescription() + " May not be used in POS ");
		}
	}
	
	public static void saveData (MedicineSales _oTR, List _vTD, List _vPayment)
	throws Exception 
	{
		saveData (_oTR, _vTD, _vPayment, null);
	} 
	
	public static void setRfeePackCommon (SalesTransactionDetail _oTDRP)
	throws Exception 
	{
		_oTDRP.setQty(bd_ONE);
		_oTDRP.setQtyBase(bd_ONE);
		_oTDRP.setUnitCode(UnitTool.getCodeByID(_oTDRP.getUnitId()));
		_oTDRP.setTaxAmount(bd_ZERO);
		_oTDRP.setItemCost(bd_ZERO);
		_oTDRP.setDiscount("0");
		_oTDRP.setDiscountId("");
		_oTDRP.setSalesTransactionDetailId(null);
	}
	
	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _vPMT
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (MedicineSales _oTR, List _vTD, List _vPMT, Connection _oConn) 
	throws Exception 
	{	
		boolean bStartTrans = false;
		boolean bNew = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{	
			if(StringUtil.isEmpty(_oTR.getMedicineSalesId())){
				bNew = true;
			}
			else //validate and make sure this transaction wont overwrite existing processed trans				
			{
				validateOverwrite(MedicineSalesPeer.MEDICINE_SALES_ID, 
					_oTR.getMedicineSalesId(), MedicineSalesPeer.class, i_PENDING, _oConn);
			}
			
			//validate _vPayment
			validatePayment (_oTR, _vPMT);
			
			//process save sales Data
			processSaveSalesData (_oTR, _vTD, _oConn);
			
			if ( _oTR.getStatus () == i_TRANS_PROCESSED && StringUtil.empty(_oTR.getInvoiceNo())) 
			{
				//validate credit limit
				validateMED(_oTR);

				//copy to sales transaction
				SalesTransaction oTR = new SalesTransaction();
				BeanUtil.setBean2BeanProperties(_oTR, oTR);
				oTR.setSalesTransactionId(_oTR.getMedicineSalesId());
				oTR.setNew(true);
				oTR.setModified(true);
				if(StringUtil.isNotEmpty(_oTR.getInsuranceId()) && _oTR.getInsurance() != null)
				{
					oTR.setCustomerId(_oTR.getInsuranceId());
					oTR.setCustomerName(_oTR.getInsuranceName());
					StringBuilder oRemark = new StringBuilder();
					oRemark.append(_oTR.getRemark()).append("\n")
						   .append(LocaleTool.getString("insurance")).append("\n")
						   .append(LocaleTool.getString("patient")).append(":")						
						   .append(_oTR.getCustomerName()).append(" / ")
						   .append(_oTR.getPatientName()).append(",").append(_oTR.getPolicyNo());
					oTR.setRemark(oRemark.toString());
				}
				
				Item oRFee = MedConfigTool.getRfeeItem();
				Item oPack = MedConfigTool.getPackagingItem();
				
				double dTotalRFee = 0;
				double dTotalPack = 0;
				List vTD = new ArrayList();
				for (int i = 0; i < _vTD.size(); i++)
				{
					SalesTransactionDetail oTD = new SalesTransactionDetail();
					MedicineSalesDetail oMTD = (MedicineSalesDetail) _vTD.get(i);
					
					double dRFee = Double.parseDouble(oMTD.getRfee());
					double dPack = Double.parseDouble(oMTD.getPackaging());
					
					dTotalRFee += dRFee; //.doubleValue();
					dTotalPack += dPack; //.doubleValue();
					
					BeanUtil.setBean2BeanProperties(oMTD, oTD);
					oTD.setSalesTransactionDetailId(null);					
					oTD.setItemName(ItemTool.getItemNameByID(oTD.getItemId())); 
					if(oMTD.getTxType() == MedicalAttributes.i_PRES_POWDER)
					{
						oTD.setDescription(oMTD.getItemName() + " " + oMTD.getDescription());
					}					
					vTD.add(oTD);
					
					//Rfee & Packaging Fee (PER ITEM)
					if (dRFee > 0)
					{
						if (oRFee != null)
						{
							SalesTransactionDetail oTDR = new SalesTransactionDetail();
							BeanUtil.setBean2BeanProperties(oRFee, oTDR);
							oTDR.setItemName(LocaleTool.getString("rfee") + " " + oTD.getItemName());
							oTDR.setItemPrice(new BigDecimal(dRFee));
							setRfeePackCommon(oTDR);
							vTD.add(oTDR);
						}
						else
						{
							throw new Exception(LocaleTool.getString("rfee_item_nf"));
						}
					}
					
					if (dPack > 0)
					{	
						if (oPack != null)
						{
							SalesTransactionDetail oTDP = new SalesTransactionDetail();
							BeanUtil.setBean2BeanProperties(oPack, oTDP);
							oTDP.setItemName(LocaleTool.getString("packaging") + " " + oTD.getItemName());
							oTDP.setItemPrice(new BigDecimal(dPack));
							setRfeePackCommon(oTDP);
							vTD.add(oTDP);
						}
						else
						{
							throw new Exception(LocaleTool.getString("pack_item_nf"));
						}
					}
					
				}
				//USE Rfee & Packaging Fee (TOTAL)
				/*
				 if (dTotalRFee > 0)
				 {
				 SalesTransactionDetail oTD = new SalesTransactionDetail();
				 Item oRFee = MedConfigTool.getRfeeItem();
				 if (oRFee != null)
				 {
				 BeanUtil.setBean2BeanProperties(oRFee, oTD);
				 oTD.setItemPrice(new BigDecimal(dTotalRFee));
				 setRfeePackCommon(oTDR);						
				 vTD.add(oTD);
				 }
				 else
				 {
				 throw new Exception(LocaleTool.getString("rfee_item_nf"));
				 }
				 }
				 
				 if (dTotalPack > 0)
				 {
				 SalesTransactionDetail oTD = new SalesTransactionDetail();
				 Item oPack = MedConfigTool.getPackagingItem();
				 if (oPack != null)
				 {
				 BeanUtil.setBean2BeanProperties(oPack, oTD);
				 oTD.setItemPrice(new BigDecimal(dTotalPack));
				 setRfeePackCommon(oTDR);
				 vTD.add(oTD);
				 }
				 else
				 {
				 throw new Exception(LocaleTool.getString("pack_item_nf"));
				 }
				 }
				 */
				
				if (_oTR.getRoundingAmount().doubleValue() != 0)
				{
					SalesTransactionDetail oTD = new SalesTransactionDetail();
					Item oRounding = MedConfigTool.getRoundingItem();
					if (oRounding != null)
					{
						BeanUtil.setBean2BeanProperties(oRounding, oTD);
						oTD.setItemPrice(_oTR.getRoundingAmount());
						setRfeePackCommon(oTD);
						vTD.add(oTD);
					}
					else
					{
						throw new Exception(LocaleTool.getString("round_item_nf"));
					}
				}				
				oTR.setRoundingAmount(bd_ZERO);
				
				BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _vTD, a_SKIP_FIELD);
				BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oTR);
				
				log.debug("BEFORE CALC");
				log.debug("oTR : " + oTR);
				log.debug("vTD : " + vTD);
				
				TransactionTool.setHeaderProperties(oTR, vTD, null);
				BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), vTD, a_SKIP_FIELD);
				BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), oTR);
				
				log.debug("AFTER CALC");
				log.debug("oTR : " + oTR);
				log.debug("vTD : " + vTD);

				Map mGroup = new HashMap();						
				
				//oTR.setStatus(i_PENDING);
				System.out.println(oTR + " " + vTD);
				TransactionTool.saveData(oTR, vTD, _vPMT, _oConn);
				
				//MedSalesJournalTool oSJ = new MedSalesJournalTool(oTR, _oConn);
				//oSJ.createSalesJournal(oTR, _vTD, _vPMT, mGroup);
				
				//update invoice no
				//_oTR.setStatus(i_PENDING);
				_oTR.setTotalFee(new BigDecimal(dTotalRFee + dTotalPack));
				_oTR.setInvoiceNo(oTR.getInvoiceNo());
				_oTR.save(_oConn);
				
				//update reg status
				if (StringUtil.isNotEmpty(_oTR.getRegistrationId()))
				{					
					//MedRegistrationTool.updateStatus(_oTR.getRegistrationId(),MedRegistrationTool.i_INVOICED,_oConn);
				}
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			//restore status to unsaved
			_oTR.setStatus(i_PENDING);
			_oTR.setInvoiceNo("");
			
			if (bNew)
			{
				_oTR.setNew(true);
				_oTR.setMedicineSalesId("");
			}
			if (bStartTrans) 
			{
				rollback ( _oConn );
			}			
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	/**
	 * process save
	 * <li>generate trans no
	 * <li>delete all pending details
	 * <li>iterate and save details
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveSalesData (MedicineSales _oTR, List _vTD, Connection _oConn) 
		throws Exception
	{
		//check whether transaction is paid and invoice no is not generated yet
		//invoice no must be checked for synchronization process
		try 
		{
			if(_oTR.getInvoiceNo() == null) _oTR.setInvoiceNo ("");
			
			//delete all existing detail from pending transaction
			if(StringUtil.isNotEmpty(_oTR.getMedicineSalesId()))
			{
				deleteDetailsByID(_oTR.getMedicineSalesId(), _oConn);
			}			
			
			//generate Sys ID if this is not a previous pending trans or sync trans
			if (StringUtil.isEmpty(_oTR.getMedicineSalesId())) 
			{
				_oTR.setMedicineSalesId(IDGenerator.generateSysID());
			}
			validateID(getHeaderByID(_oTR.getMedicineSalesId(), _oConn), "invoiceNo");
			
			_oTR.setModified(true);
			_oTR.save(_oConn);
			
			Set hPres = new HashSet(_vTD.size());
			MedicineSalesDetail oTD = null;			
			for (int i = 0; i < _vTD.size(); i++) 
			{
				oTD = (MedicineSalesDetail) _vTD.get (i);
				oTD.setModified(true);
				oTD.setNew(true);
				
				//Generate SYS Data
				oTD.setMedicineSalesDetailId(IDGenerator.generateSysID());								
				oTD.setMedicineSalesId(_oTR.getMedicineSalesId());
				oTD.save(_oConn);				
				
				if(StringUtil.isNotEmpty(oTD.getPrescriptionId()))
				{
					hPres.add(oTD.getPrescriptionId());
				}
			}

			Iterator iter = hPres.iterator();
			while(iter.hasNext())
			{
				String sPresID = (String) iter.next();
				//update prescription status, set to sold
				if(_oTR.getStatus () == i_TRANS_PROCESSED && 
					!StringUtil.empty(sPresID) &&					
					!StringUtil.equals(sPresID, _oTR.getMedicineSalesId()))
				{
					Prescription oPRS = PrescriptionTool.getHeaderByID(sPresID, _oConn);
					if(oPRS != null) {
						oPRS.setStatus(PrescriptionTool.i_PRS_SOLD);
						oPRS.save(_oConn);
					}					
				}
			}
		}
		catch (Exception _oEx)
		{
			log.error(_oEx);
			String sError = "Process Save Failed : " + _oEx.getMessage();
			throw new NestableException (sError, _oEx);
		}
	}
	
	
	/**
	 * cancel sales transaction
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @throws Exception
	 */
	public static void cancelSales(MedicineSales _oTR, 
								   List _vTD, 
								   String _sCancelBy, 
								   Connection _oConn) 
	throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{	
			_oTR.setStatus(i_CANCELLED);
			_oTR.save(_oConn);
			
			Set hPres = new HashSet(_vTD.size());
			MedicineSalesDetail oTD = null;
			for ( int i = 0; i < _vTD.size(); i++ ) 
			{
				oTD = (MedicineSalesDetail) _vTD.get (i);
				
				if(StringUtil.isNotEmpty(oTD.getPrescriptionId()))
				{
					hPres.add(oTD.getPrescriptionId());
				}								
			}

			Iterator iter = hPres.iterator();
			while(iter.hasNext())
			{
				String sPresID = (String) iter.next();
				//update prescription status, set to processed
				if(!StringUtil.empty(sPresID) &&					
				   !StringUtil.equals(sPresID, _oTR.getMedicineSalesId()))
				{
					Prescription oPRS = PrescriptionTool.getHeaderByID(sPresID, _oConn);
					if(oPRS != null) {
						oPRS.setStatus(i_PROCESSED);
						oPRS.save(_oConn);
					}					
				}
			}
			
			SalesTransaction oTR = TransactionTool.getHeaderByID(_oTR.getMedicineSalesId(), _oConn);
			List vTD = TransactionTool.getDetailsByID(_oTR.getMedicineSalesId(), _oConn);
			TransactionTool.cancelSales(oTR, vTD, _sCancelBy, _oConn);
			if (bStartTrans) 
			{
				commit(_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}	
			_oEx.printStackTrace();
			throw new NestableException(_oEx.getMessage(), _oEx);
		}
	}
	
	/**
	 * get Status String 
	 * 
	 * @param _iStatusID
	 * @return status string
	 */
	public static String getStatusString (int _iStatusID)
	{
		return TransactionTool.getStatusString(_iStatusID);
	}	
	
	/**
	 * 
	 * @param _iTxType
	 * @param _sItemID
	 * @return
	 */
	public static String getTxTypeCode(int _iTxType, String _sItemID)
	{		
		if(_iTxType == MedicalAttributes.i_PRES_NORMAL) return "/R";			
		if(_iTxType == MedicalAttributes.i_PRES_POWDER)
		{
			if(StringUtil.isNotEmpty(_sItemID) && PowderTool.isPowderItem(_sItemID))
			{
				return "/RC*";			
			}
			return "/RC";			
		}
		return "NR";		
	}
	
	public static String getTxTypeDesc(int _iType)
	{
		if(_iType == MedicalAttributes.i_NON_PRES) return LocaleTool.getString("non_prescription");
		if(_iType == MedicalAttributes.i_PRES_NORMAL) return LocaleTool.getString("prescription");
		if(_iType == MedicalAttributes.i_PRES_POWDER) return LocaleTool.getString("powder");
		return LocaleTool.getString("all");		
	}
	
	/**
	 * Finder used by sales transaction view
	 * 
	 * @param _sInvNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _iLimit
	 * @param _sCashierName
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond,
										String _sKeywords,
										Date _dStart, 
										Date _dEnd, 
										String _sCustomerID, 
										String _sLocationID, 
										int _iStatus, 
										int _iLimit,
										int _iGroupBy,
										String  _sCashierName,
										String _sCurrencyID) 
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				MedicineSalesPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);    	
		
		if(_iCond == 91 && StringUtil.isNotEmpty(_sKeywords)) //e-ticket queue no
		{
			oCrit.add(MedicineSalesPeer.PREP_QUEUE_NO, (Object)_sKeywords, Criteria.ILIKE);
		}
		
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(MedicineSalesPeer.CUSTOMER_ID, _sCustomerID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(MedicineSalesPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sCashierName)) 
		{
			oCrit.add(MedicineSalesPeer.CASHIER_NAME, _sCashierName);	
		}
		if (_iStatus > 0) 
		{
			oCrit.add(MedicineSalesPeer.STATUS, _iStatus);	
		}		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		else
		{
			oCrit.addAscendingOrderByColumn(MedicineSalesPeer.TRANSACTION_DATE);
		}
		log.debug(oCrit);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.medical.om.MedicineSalesPeer");
	}
	
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sCashierName,
								  String _sCurrencyID,
                                  String _sDoctorName) 
    	throws Exception
	{
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	MedicineSalesPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);   
        
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(MedicineSalesPeer.CUSTOMER_ID, _sCustomerID);	
		}
        if(StringUtil.isNotEmpty(_sLocationID))
        {
           oCrit.add(MedicineSalesPeer.LOCATION_ID, _sLocationID);
        }
        if (StringUtil.isNotEmpty(_sCashierName))
	    {
	    	oCrit.add(MedicineSalesPeer.CASHIER_NAME, _sCashierName);
	    }        
        if (StringUtil.isNotEmpty(_sDoctorName))
        {
            oCrit.add(MedicineSalesPeer.DOCTOR_NAME, (Object)_sDoctorName, Criteria.ILIKE);
        }
        if(_iStatus > 0)
        {
           oCrit.add(MedicineSalesPeer.STATUS, _iStatus);
        }
	    if (StringUtil.isNotEmpty(_sVendorID))
	    {
		    oCrit.addJoin(MedicineSalesPeer.MEDICINE_SALES_ID,MedicineSalesDetailPeer.MEDICINE_SALES_ID);
		    oCrit.addJoin(MedicineSalesDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
		    oCrit.add(ItemPeer.PREFERED_VENDOR_ID,_sVendorID);
	    }	   
	    oCrit.addAscendingOrderByColumn(MedicineSalesPeer.TRANSACTION_DATE);
        log.debug(oCrit);
		return MedicineSalesPeer.doSelect(oCrit);
	}
	
	public static double calcPriceIncFee (MedicineSalesDetail _oTD)
	{
		if (_oTD != null)
		{
			double dSubTotal = _oTD.getSubTotal().doubleValue();
			double dRfee = Double.valueOf(_oTD.getRfee());
			double dPack = Double.valueOf(_oTD.getPackaging());
			double dQty = _oTD.getQty().doubleValue();
			double dTax = _oTD.getSubTotalTax().doubleValue();
			return ((dSubTotal + dRfee + dPack + dTax) / dQty) ;
		}
		return 0;
	}
	
	public static double calcTotalRfeePack (String _sID, boolean _bRfee) 
		throws Exception
	{
		List vTD = getDetailsByID(_sID);
		double dTotal = 0;
		for (int i = 0; i < vTD.size(); i++)
		{
			MedicineSalesDetail oTD = (MedicineSalesDetail) vTD.get(i);			
			double dValue = 0;	
			if (_bRfee)  dValue = Double.valueOf(oTD.getRfee());
			if (!_bRfee) dValue = Double.valueOf(oTD.getPackaging());
			dTotal = dTotal + dValue;
		}
		return dTotal;
	}

	//report methods
	public static Prescription getMedSalesPrescription (String _sID) 
		throws Exception
	{
		List vTD = getDetailsByID(_sID);
		double dTotal = 0;
		for (int i = 0; i < vTD.size(); i++)
		{
			MedicineSalesDetail oTD = (MedicineSalesDetail) vTD.get(i);			
			if (StringUtil.isNotEmpty(oTD.getPrescriptionId()))
			{
				return PrescriptionTool.getHeaderByID(oTD.getPrescriptionId(), null);
			}
		}
		return null;
	}
	
	public static double calcTotalRfeePack (List _vTR, boolean _bRfee) 
		throws Exception
	{
		double dTotal = 0;
		for (int i = 0; i < _vTR.size(); i++)
		{
			SalesTransaction oTR = (SalesTransaction) _vTR.get(i);
			dTotal += calcTotalRfeePack(oTR.getSalesTransactionId(), _bRfee);
		}
		return dTotal;
	}
		
	//-------------------------------------------------------------------------
	// SALES SUMMARY & ANALYSIS
	//-------------------------------------------------------------------------	
	
	public static Hashtable getDailySalesSummary (Date _dDate, String _sLocationID)
		throws Exception
	{		
		List vData = findTrans(-1, "", DateUtil.getStartOfDayDate(_dDate), 
			DateUtil.getEndOfDayDate(_dDate),"", _sLocationID, i_PROCESSED,  "", "", "", "");
		
		double dTotalCash = 0;
		double dTotalNonCash = 0;
		double dTotalNett = 0;
		double dTotalDisc = 0;
		
		double dTotalReturn = 0;
		double dCashReturn = 0;
		double dNonCashReturn = 0;

		double dTotalTax = 0;
		double dTotalRfee = 0;
		double dTotalPack = 0;
		double dTotalRound = 0;
		        
		for (int i = 0; i < vData.size(); i++)
		{ 
			MedicineSales oTR = (MedicineSales) vData.get(i);
			PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(oTR.getPaymentTypeId());
			double dTotalAmt = oTR.getTotalAmount().doubleValue();

			if (oPT != null && oPT.getIsDefault()) dTotalCash += dTotalAmt;
			else dTotalNonCash += (dTotalAmt - oTR.getTotalExpense().doubleValue());

			dTotalNett += dTotalAmt - oTR.getTotalDiscount().doubleValue() - 
				oTR.getTotalTax().doubleValue() - oTR.getRoundingAmount().doubleValue() - oTR.getTotalFee().doubleValue();
			
			dTotalDisc += oTR.getTotalDiscount().doubleValue();
			dTotalTax += oTR.getTotalTax().doubleValue();
			dTotalRound += oTR.getRoundingAmount().doubleValue();
			
			List vTD = getDetailsByID(oTR.getMedicineSalesId());
			for (int j = 0; j < vTD.size(); j++)
			{
				MedicineSalesDetail oTD = (MedicineSalesDetail) vTD.get(j);				
				dTotalRfee += Double.valueOf(oTD.getRfee());
				dTotalPack += Double.valueOf(oTD.getPackaging());
			}
		}
		
		List vRet = SalesReturnTool.findTrans(DateUtil.getStartOfDayDate(_dDate), 
			DateUtil.getEndOfDayDate(_dDate),_sLocationID,"",i_PROCESSED);
		for (int i = 0; i < vRet.size(); i++)
		{ 
			SalesReturn oTR = (SalesReturn) vRet.get(i);
			double dAmount = oTR.getReturnedAmount().doubleValue() - oTR.getTotalTax().doubleValue(); 
			dTotalReturn += dAmount;
			if (oTR.getCashReturn()) dCashReturn += oTR.getReturnedAmount().doubleValue();
			else dNonCashReturn += oTR.getReturnedAmount().doubleValue();
		}
		
		Hashtable hSummary = new Hashtable(20);
		hSummary.put ("TotalCash", dTotalCash);
		hSummary.put ("TotalNonCash", dTotalNonCash);
		hSummary.put ("TotalNett", dTotalNett);
		hSummary.put ("TotalDisc", dTotalDisc);
		hSummary.put ("TotalReturn", dTotalReturn);
		hSummary.put ("TotalCashReturn", dCashReturn);
		hSummary.put ("TotalNonCashReturn", dNonCashReturn);		
		hSummary.put ("TotalTax", dTotalTax);
		hSummary.put ("TotalRfee", dTotalRfee);
		hSummary.put ("TotalPack", dTotalPack);
		hSummary.put ("TotalRound", dTotalRound);
		hSummary.put ("TotalPerPT", groupAmountByPaymentType(vData));
        hSummary.put ("vSales",vData);
        
		return hSummary;
	}	
    
    /**
     * create map contains payment type id and amount from sales invoices
     * if multiple payment then break down to each payment type
     * 
     * @param _vSales
     * @return
     * @throws Exception
     */
    public static Map groupAmountByPaymentType(List _vSales)
        throws Exception
    {
        Map mResult = new HashMap();
        List vPT = PaymentTypeTool.getAllPaymentType();
        for (int i = 0; i < vPT.size(); i++)
        {
            PaymentType oPT = (PaymentType)vPT.get(i);
            String sTypeID = oPT.getPaymentTypeId();
            List vSalesPT = BeanUtil.filterListByFieldValue(_vSales,"paymentTypeId",sTypeID);           
            if (!oPT.getIsMultiplePayment())
            {
                double dSalesPT = 0;
                for(int j = 0; j < vSalesPT.size(); j++)
                {
                    MedicineSales oTR = (MedicineSales) vSalesPT.get(j);
                    dSalesPT = dSalesPT + oTR.getTotalAmount().doubleValue();
                }
                //get existing
                if (mResult.containsKey(sTypeID))
                {
                    BigDecimal dExistPT = (BigDecimal) mResult.get(sTypeID);
                    double dAccumPT = dExistPT.doubleValue() + dSalesPT;
                    mResult.put(sTypeID, new BigDecimal(dAccumPT));
                }
                else
                {
                    mResult.put(sTypeID, new BigDecimal(dSalesPT));
                }
            }
            else
            {
                for(int j = 0; j < vSalesPT.size(); j++)
                {
                    MedicineSales oTR = (MedicineSales) vSalesPT.get(j);                  
                    List vPMT = InvoicePaymentTool.getTransPayment(oTR.getMedicineSalesId());
                    log.debug("MULTI : "  + oTR.getInvoiceNo());
                    for (int k = 0; k < vPMT.size(); k++)
                    {
                        InvoicePayment oPMT = (InvoicePayment) vPMT.get(k);
                        String sPMTTypeID = oPMT.getPaymentTypeId();
                        
                        //log.debug("oPMT : "  + PaymentTypeTool.getPaymentTypeByID(sPMTTypeID).getDescription());
                        
                        //get existing
                        if (mResult.containsKey(sPMTTypeID))
                        {
                            BigDecimal dExistPT = (BigDecimal) mResult.get(sPMTTypeID);                         
                            double dAccumPT = dExistPT.doubleValue() + oPMT.getPaymentAmount().doubleValue();
                            mResult.put(sPMTTypeID, new BigDecimal(dAccumPT));
                        }
                        else
                        {                           
                            mResult.put(sPMTTypeID, oPMT.getPaymentAmount());
                        }
                    }
                }
            }
        }
        return mResult;
    }
    
    /**
     * 
     * @param _dStart
     * @param _dEnd
     * @param _sLocationID
     * @param _sDepartmentID
     * @param _sProjectID
     * @param _sCustomerTypeID
     * @param _sEmployeeID
     * @param _sCashier
     * @param _bIncTax
     * @param _bDiscount
     * @param _iStatus
     * @param _iSortBy
     * @return
     * @throws Exception
     */
    public static List getTotalSalesPerDoctor (Date _dStart, 
                                               Date _dEnd, 
                                               String _sLocationID,                                            
                                               String _sDepartmentID,
                                               String _sProjectID,
                                               boolean _bIncTax,
                                               boolean _bDiscount,
                                               int _iStatus,
                                               int _iSortBy)
    throws Exception
    {       
        StringBuilder oSQL = new StringBuilder ();
        oSQL.append ("SELECT  ");
        oSQL.append (" s.doctor_name, ");
        oSQL.append (" SUM(s.total_qty), ");

        if (!_bIncTax)
        {
            oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((s.total_amount) - s.total_tax) ");
            oSQL.append (" ELSE SUM(s.total_amount) END ");
        }
        else
        {
            oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((s.total_amount) + s.total_tax) ");
            oSQL.append (" ELSE SUM(s.total_amount) END ");
        }
        
        oSQL.append (" ,SUM(s.total_cost) ");
        
        if (_bDiscount) //include discount
        {
            oSQL.append(",SUM(s.total_discount)");
        }
        oSQL.append (",COUNT(s.invoice_no) ");        
        oSQL.append (" FROM medicine_sales s ");
        oSQL.append (" WHERE ");
        
        SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
        
        oSQL.append (" GROUP BY s.doctor_name, s.is_inclusive_tax ");      
        log.debug (oSQL.toString());
        return SalesTransactionPeer.executeQuery(oSQL.toString());  
    }   
    
	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * get all transaction happened since last sync date 
	 * @param _iSyncType
	 * @return
	 * @throws Exception
	 */
	public static List getAllUnsynchronizedTransaction (int _iSyncType)
	throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(_iSyncType);
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null) 
		{
			oCrit.add(MedicineSalesPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
			oCrit.add(MedicineSalesPeer.STATUS, i_TRANS_PROCESSED);
		}
		return MedicineSalesPeer.doSelect(oCrit);
	}

    /////////////////////////////////////////////////////////////////////////////////////
    //synchronization methods
    /////////////////////////////////////////////////////////////////////////////////////
        
    /**
     * get all sales invoice created since last store 2 ho
     * 
     * @return List of SalesTransaction created since last store 2 ho
     * @throws Exception
     */
    public static List getStoreSalesInvoice ()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
        Criteria oCrit = new Criteria();
        if (dLastSyncDate != null) 
        {
            oCrit.add(MedicineSalesPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
        }
        oCrit.add(MedicineSalesPeer.STATUS, i_TRANS_PROCESSED);
        return MedicineSalesPeer.doSelect(oCrit);
    }
    
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
	throws Exception
	{
		return getPrevOrNextID(MedicineSalesPeer.class, MedicineSalesPeer.MEDICINE_SALES_ID, _sID, _bIsNext);
	}

	public static MedicineSales getByRegID(String _sID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(MedicineSalesPeer.REGISTRATION_ID, _sID);
		oCrit.add(MedicineSalesPeer.STATUS, i_CANCELLED, Criteria.NOT_EQUAL);
		List vData = MedicineSalesPeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return (MedicineSales) vData.get(0);
		}
		return null;	
	}	  	
	
	public static List getInsurance()
		throws Exception
	{
		List vIns = new ArrayList(1);
		Criteria oCrit = new Criteria();
		String sTypes = MedConfigTool.getConfig().getInsuranceCtype();
		if(StringUtil.isNotEmpty(sTypes))
		{
			List vTypes = StringUtil.toList(sTypes,",");
			oCrit.addJoin(CustomerPeer.CUSTOMER_TYPE_ID,CustomerTypePeer.CUSTOMER_TYPE_ID);
			oCrit.addIn(CustomerTypePeer.CUSTOMER_TYPE_CODE, vTypes);			
			vIns = CustomerPeer.doSelect(oCrit);
		}
		return vIns;
	}
    
    //-------------------------------------------------------------------------
    // PREP Methods
    //-------------------------------------------------------------------------
    public static void updateQueueNo(MedicineSales _oMS, int _iTxNo, String _sFormat)
    	throws Exception
    {
    	if(_oMS != null && StringUtil.empty(_oMS.getPrepQueueNo()))
    	{
    		Connection con = null;
	    	try 
	    	{
				con = beginTrans();
				_oMS.setPrepQueueNo(LastNumberTool.generateByLocation(_oMS.getLocationId(),_sFormat,_iTxNo,con));
				_oMS.setPrepStatus(i_PENDING);
				_oMS.setPrepStart(new Date());
				_oMS.save(con);
				commit(con);
			} 
	    	catch (Exception e) 
	    	{
	    		_oMS.setPrepQueueNo("");
	    		e.printStackTrace();
	    		rollback(con);
			}
	    	finally 
	    	{
	    		if(con != null && !con.isClosed()) {
	    			Torque.closeConnection(con);	    		
	    		}
	    	}
    	}
    }    
}