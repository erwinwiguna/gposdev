package com.ssti.enterprise.medical.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.medical.om.DoctorPeer;
import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.om.PrescriptionDetail;
import com.ssti.enterprise.medical.om.PrescriptionDetailPeer;
import com.ssti.enterprise.medical.om.PrescriptionPeer;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * 
 * 2018-07-03
 * - WARNING: Change processSavePresDate, change PresDetailId value = PresID + index
 *   change PowderDetailId = PresDetailID + index
 *   
 * 2015-04-15
 * - change method calcPriceIncFee to check for inclusive tax
 *
 * </pre><br>
 */
public class PrescriptionTool extends BaseTool 
{	
	static final Log log = LogFactory.getLog(PrescriptionTool.class);
	
	public static final int i_PRS_SOLD = 4;
	
	static final String s_PRS_FORMAT = MedConfigTool.getPresNoFormat();

	private static String s_PRS_CTL = "prescription_ctl";

	static PrescriptionTool instance = null;
	
	public static synchronized PrescriptionTool getInstance() 
	{
		if (instance == null) instance = new PrescriptionTool();
		return instance;
	}		
	
	public static List getAllPrescription()
		throws Exception
	{
		return PrescriptionPeer.doSelect(new Criteria());
	}
	
	public static Prescription getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PrescriptionPeer.PRESCRIPTION_ID, _sID);
		List vData = PrescriptionPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (Prescription) vData.get(0);
		}
		return null;
	}
	
	public static List getDetailsByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PrescriptionDetailPeer.PRESCRIPTION_ID, _sID);
		oCrit.addAscendingOrderByColumn(PrescriptionDetailPeer.PRESCRIPTION_DETAIL_ID);
		List vDet = PrescriptionDetailPeer.doSelect(oCrit, _oConn);
		for (int i = 0; i < vDet.size(); i++)
		{
			PrescriptionDetail oTD = (PrescriptionDetail) vDet.get(i);
			if (oTD.getPrescriptionType() == MedicalAttributes.i_PRES_POWDER)
			{
				oTD.setPowderDetail(PowderTool.getByPresDetID(oTD.getPrescriptionDetailId(), _oConn));
				vDet.set(i, oTD);
			}
		}
		return vDet;
	}	
	
	public static PrescriptionDetail getDetailByDetailID(String _sID, String _sDetID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PrescriptionDetailPeer.PRESCRIPTION_ID, _sID);		
		oCrit.add(PrescriptionDetailPeer.PRESCRIPTION_DETAIL_ID, _sDetID);
		List vDet =  PrescriptionDetailPeer.doSelect(oCrit, _oConn);
		if (vDet.size() > 0)
		{
			return (PrescriptionDetail) vDet.get(0);
		}
		return null;
	}	

	/**
	 * get trans details of this list of master
	 * 
	 * @param _vTrans
	 * @return List of trans details
	 * @throws Exception
	 */
	public static List getTransDetails (List _vTrans)
    	throws Exception
    { 
		List vTransID = new ArrayList (_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++) 
		{
			Prescription oTrans = (Prescription) _vTrans.get(i);			
			vTransID.add (oTrans.getPrescriptionId());
		}		
		Criteria oCrit = new Criteria();
        if(vTransID.size() > 0)
        {
		    oCrit.addIn (PrescriptionDetailPeer.PRESCRIPTION_ID, vTransID);
		}
		return PrescriptionDetailPeer.doSelect(oCrit);
	}
	
	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_PENDING) return LocaleTool.getString("pending");
		if (_iStatusID == i_CANCELLED) return LocaleTool.getString("cancelled");
		if (_iStatusID == i_PRS_SOLD) return LocaleTool.getString("medical", "sold");
		return LocaleTool.getString("processed");
	}		
	
	private static void deleteDetailsByID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PrescriptionDetailPeer.PRESCRIPTION_ID, _sID);
		PrescriptionDetailPeer.doDelete (oCrit, _oConn);
	}	
	
	public static LargeSelect findData(int _iCond, 
			 						   String _sKeywords, 
									   Date _dStart, 
									   Date _dEnd, 
									   String _sCustomerID,
									   String _sLocationID, 
									   String _sDoctorID,
									   String _sCreateBy,
									   int _iStatus,
									   int _iGroupBy,
									   int _iLimit) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
        if(StringUtil.isNotEmpty(_sKeywords))
        {
        	if (_iCond == 1)     
        	{
        		oCrit.add (PrescriptionPeer.PRESCRIPTION_NO, (Object)_sKeywords, Criteria.ILIKE);
        	}
        	if (_iCond == 2)     
        	{
        		oCrit.add (PrescriptionPeer.PATIENT_NAME, (Object)_sKeywords, Criteria.ILIKE);
        	}
        	if (_iCond == 3)
        	{
        		oCrit.addJoin (PrescriptionPeer.DOCTOR_ID, DoctorPeer.DOCTOR_ID);
        		oCrit.add (DoctorPeer.DOCTOR_NAME, (Object)_sKeywords, Criteria.ILIKE);
        	}        	
        	if (_iCond == 4) 
        	{
        		oCrit.addJoin(PrescriptionPeer.CUSTOMER_ID, CustomerPeer.CUSTOMER_ID);
        		oCrit.add(CustomerPeer.CUSTOMER_NAME, (Object)_sKeywords, Criteria.ILIKE);
        	}
        	if (_iCond == 5)        
        	{
        		oCrit.add (PrescriptionPeer.REMARK, (Object)_sKeywords, Criteria.ILIKE);
        	}
        	if (_iCond == 6)     
        	{
        		oCrit.add (PrescriptionPeer.CREATE_BY, (Object)_sKeywords, Criteria.ILIKE);
        	}
        	if (_iCond == 7)        
        	{
        		oCrit.addJoin(PrescriptionPeer.PRESCRIPTION_ID, PrescriptionDetailPeer.PRESCRIPTION_ID);
        		oCrit.add(PrescriptionDetailPeer.ITEM_CODE, (Object) _sKeywords, Criteria.ILIKE);        
        	}		
        }
		if (_dStart != null) 
		{
			oCrit.add(PrescriptionPeer.CREATE_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
		}
		if (_dEnd != null) 
		{
			oCrit.and(PrescriptionPeer.CREATE_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		}
		if (_iStatus > 0)
		{
			oCrit.add(PrescriptionPeer.STATUS, _iStatus);
		}
		if (StringUtil.isNotEmpty(_sCustomerID))
        {
        	oCrit.add(PrescriptionPeer.CUSTOMER_ID, _sCustomerID);  
        }
		if (StringUtil.isNotEmpty(_sLocationID))
        {
        	oCrit.add(PrescriptionPeer.LOCATION_ID, _sLocationID);  
        }
		if (StringUtil.isNotEmpty(_sDoctorID))
        {
        	oCrit.add(PrescriptionPeer.DOCTOR_ID, _sDoctorID);  
        }
        log.debug("Criteria : " + oCrit);
		LargeSelect oLS = new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.medical.om.PrescriptionPeer");
        
		return oLS;
	}
	
	/**
	 * 
	 * @param data
	 * @param _vTD
	 * @throws Exception
	 */
	public static void updateDetail( RunData data, List _vTD ) 
		throws Exception
	{		
		for (int i = 0; i < _vTD.size(); i++)
		{
			int iNo = i + 1;
			PrescriptionDetail oTD = (PrescriptionDetail) _vTD.get(i);
			oTD.setReqQty(data.getParameters().getBigDecimal("ReqQty" + iNo));
			oTD.setQty(data.getParameters().getBigDecimal("Qty" + iNo));					
			oTD.setRfee(data.getParameters().getString("Rfee" + iNo,"0"));						
			oTD.setPackaging(data.getParameters().getString("Packaging" + iNo,"0"));						
			
			double dSubTotal = oTD.getQty().doubleValue() * oTD.getItemPrice().doubleValue();				
			oTD.setSubTotal(new BigDecimal(dSubTotal).setScale(2,BigDecimal.ROUND_HALF_UP));
			_vTD.set(i, oTD);
		}
	}		
	
	/**
	 * 
	 * @param data
	 * @param _oTR
	 * @throws Exception
	 */
    public static void setHeader( RunData data, Prescription _oTR, List _vTD ) 
		throws Exception
	{
    	if (data != null)
    	{
    		data.getParameters().setProperties (_oTR);
    	}
		_oTR.setCreateDate(new Date());

		double dTotalAmount = 0;	
		double dTotalRFee = 0;	
		double dTotalPack = 0;	
		
		for (int i = 0; i < _vTD.size(); i++)
		{			
			PrescriptionDetail oTD = (PrescriptionDetail) _vTD.get(i);

			double dQty = oTD.getQty().doubleValue();
			double dSubTotal = dQty * oTD.getItemPrice().doubleValue();
			double dTax = dSubTotal * oTD.getTaxAmount().doubleValue() / 100;
			
			oTD.setSubTotal(new BigDecimal(dSubTotal).setScale(2, BigDecimal.ROUND_HALF_UP));
			if (oTD.getPresPowderDetails() == null || oTD.getPresPowderDetails().size() == 0)
			{
				MedConfigTool.calculateRfee(oTD, false, dQty, dSubTotal);
			}
			double dRFee = 0; if(!StringUtil.empty(oTD.getRfee())) dRFee = Double.valueOf(oTD.getRfee());
			double dPack = 0; if(!StringUtil.empty(oTD.getPackaging())) dPack = Double.valueOf(oTD.getPackaging());
			
			//check rfee 
			Item oItem = ItemTool.getItemByID(oTD.getItemId());
			String sCustID = _oTR.getCustomerId();
			boolean bApplyRfee = CustomerMarginTool.applyRfee(sCustID, _oTR.getLocationId(), oItem, 2);
			boolean bApplyPack = CustomerMarginTool.applyPack(sCustID, _oTR.getLocationId(), oItem, 2);
			if (!bApplyRfee) {dRFee = 0; oTD.setRfee("0");}
			if (!bApplyPack) {dPack = 0; oTD.setPackaging("0");}
			
			double dTotal = dSubTotal + dRFee + dPack;
			oTD.setTotal(new BigDecimal(dTotal).setScale(2, BigDecimal.ROUND_HALF_UP));

			dTotalAmount += oTD.getTotal().doubleValue();
			dTotalRFee += Double.valueOf(oTD.getRfee());
			dTotalPack += Double.valueOf(oTD.getPackaging());
		}		
		
		//if med disc setup not exist then sDisc will contains user-input discount value
		String sDisc = _oTR.getPresDiscount(); 
		double dDisc = 0;
		//sDisc = _oTR.getPresDiscount();
		if (StringUtil.isNotEmpty(sDisc))
		{
			dDisc = Calculator.calculateDiscount(sDisc,dTotalAmount);
		}
		dTotalAmount = dTotalAmount - dDisc;		
		
		//calculate tax
		double dTotalTax = calcTax(sDisc, dTotalAmount, _vTD).doubleValue();        
        if(!PreferenceTool.getSalesInclusiveTax())
        {
            dTotalAmount += dTotalTax;
        }
        
		_oTR.setTotalDiscount(new BigDecimal(dDisc));
		_oTR.setTotalRfee(new BigDecimal(dTotalRFee));
		_oTR.setTotalPackaging(new BigDecimal(dTotalPack));
		_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
		
        BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _vTD);
        BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oTR);
	}	    

	public static BigDecimal calcTax(String _sDisc, double _dTotalAmount, List _vTD)
	{
		double dTax = 0;
		for (int i = 0; i < _vTD.size(); i++)
		{
			PrescriptionDetail oTD = (PrescriptionDetail) _vTD.get(i);
			double dSubTotal = oTD.getSubTotal().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDisc, dSubTotal);
			
			//if dTotalAmount > 0, prevent divide by zero
			if(StringUtil.isNotEmpty(_sDisc) && 
			   !_sDisc.contains(Calculator.s_PCT) && _dTotalAmount > 0)
			{ 
				//if not pct discount then pro rate the discount amount towards sub total
				dSubTotal = dSubTotal - (dDisc * (dSubTotal / _dTotalAmount));
			}
			else
			{
				dSubTotal = dSubTotal - dDisc;
			}
			
			double dSubTotalTax = 0;
			dSubTotalTax = dSubTotal * oTD.getTaxAmount().doubleValue() / 100;
            if(!PreferenceTool.getSalesInclusiveTax())
            {
                dSubTotalTax = dSubTotal * oTD.getTaxAmount().doubleValue() / 100;
            }
            else
            {
                //calculate inclusive Tax
                dSubTotalTax = (dSubTotal * 100) / (oTD.getTaxAmount().doubleValue() + 100);
                dSubTotalTax = dSubTotal - dSubTotalTax;  
            }
			dTax += dSubTotalTax;
		}
		return new BigDecimal(dTax);
	}
    
    /**
     * 
     * @param _oTR
     * @param _vTD
     * @param _oConn
     * @throws Exception
     */
	public static void saveData (Prescription _oTR, List _vTD, Connection _oConn) 
		throws Exception 
	{	
		boolean bNew = false;
		boolean bNewNo = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans ();
			bStartTrans = true;
		}
		try 
		{
			if(StringUtil.isEmpty(_oTR.getPrescriptionId())) bNew = true;
			if(StringUtil.isEmpty(_oTR.getPrescriptionNo())) bNewNo = true;	

			//process save 
			processSavePrescriptionData (_oTR, _vTD, _oConn);

			//update MR
			if (StringUtil.isNotEmpty(_oTR.getMedicalRecordId()))
			{

			}
			
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			if (bNew)
			{
				_oTR.setNew(true);
				_oTR.setPrescriptionId("");				
			}
			if(bNewNo)
			{
				_oTR.setPrescriptionNo("");
			}			
			if (bStartTrans) 
			{
				rollback (_oConn);
			}			
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}

	//save data processor
	private static void processSavePrescriptionData (Prescription _oTR, List _vTD, Connection _oConn) 
		throws Exception
	{
		//check whether transaction is paid and invoice no is not generated yet
		//invoice no must be checked for synchronization process
		try 
		{
			//generate Sys ID if this is not a previous pending trans or sync trans
			if (StringUtil.isEmpty(_oTR.getPrescriptionId())) 
			{
				_oTR.setPrescriptionId ( IDGenerator.generateSysID() );
				validateID(getHeaderByID(_oTR.getPrescriptionId(), _oConn), "prescriptionNo");
			}
			
			if (_oTR.getPrescriptionNo() == null) _oTR.setPrescriptionNo ("");
			if ( _oTR.getStatus () == i_PROCESSED && _oTR.getPrescriptionNo().equals("")) 
			{				
				//validate QTY
				validateQty (_oTR, _vTD, _oConn);
				
				//generate Prescription No
				String sLocCode = LocationTool.getLocationCodeByID(_oTR.getLocationId(), _oConn);

				_oTR.setPrescriptionNo (LastNumberTool.generateByLocation(_oTR.getLocationId(), s_PRS_FORMAT, 90, _oConn));
				// (s_PRS_FORMAT, s_PRS_CTL, sLocCode, _oConn));				

				validateNo(PrescriptionPeer.PRESCRIPTION_NO,_oTR.getPrescriptionNo(),
						   PrescriptionPeer.class, _oConn);
				
				log.debug("TRANS NO " + _oTR.getPrescriptionNo());
				
				//notify client
				String sMsg = _oTR.getPrescriptionNo() + " " + _oTR.getPatientName() + " " + _oTR.getPatientAge();
				MedConfigTool.sendAlert(sMsg);
			}
			
			//delete all existing detail from pending transaction
			if (StringUtil.isNotEmpty(_oTR.getPrescriptionId()))
			{
				PowderTool.deleteByPresID(_oTR.getPrescriptionId(), _oConn);
				deleteDetailsByID (_oTR.getPrescriptionId(), _oConn);
			}
						
			_oTR.setUpdateDate(new Date());
			_oTR.save (_oConn);
			
			PrescriptionDetail oTD = null;
			
			for ( int i = 0; i < _vTD.size(); i++ ) 
			{
				oTD = (PrescriptionDetail) _vTD.get (i);
				oTD.setModified (true);
				oTD.setNew (true);
				
				double dQty = oTD.getQty().doubleValue();
				double dQtyBase = dQty * UnitTool.getBaseValue(oTD.getItemId(), oTD.getUnitId(), _oConn);
				oTD.setQtyBase (new BigDecimal(dQtyBase));
				
				//Generate Sys Data
				if (StringUtil.isEmpty(oTD.getPrescriptionDetailId())) 
				{
                    oTD.setPrescriptionDetailId(_oTR.getPrescriptionId() + StringUtil.formatNumberString(Integer.toString(i), 2));
                    //oTD.setPrescriptionDetailId(IDGenerator.generateSysID());
                    oTD.setPrescriptionId(_oTR.getPrescriptionId());
				}
				
				oTD.save (_oConn);

				//save powder detail
				if (oTD.getPrescriptionType() == MedicalAttributes.i_PRES_POWDER)
				{
					List vPwd = oTD.getPowderDetail();
					log.debug("POWDER : " + vPwd);
					if (vPwd != null && vPwd.size() > 0)
					{
						for (int j = 0; j < vPwd.size(); j++)
						{
							PresPowderDetail oPWDD = (PresPowderDetail) vPwd.get(j);
							oPWDD.setPrescriptionDetailId(oTD.getPrescriptionDetailId());
							oPWDD.setPrescriptionId(oTD.getPrescriptionId());
							//oPWDD.setPresPowderDetailId(IDGenerator.generateSysID());
							oPWDD.setPresPowderDetailId(oTD.getPrescriptionDetailId() + StringUtil.formatNumberString(Integer.toString(j), 2));
							oPWDD.setModified(true);
							oPWDD.setNew(true);
							log.debug("SAVED POWDER : " + oPWDD);
							
							oPWDD.save(_oConn);
						}
					}
					else
					{
						throw new Exception("Invalid / Empty Powder Contents ");
					}
				}
			}
		}
		catch (Exception _oEx)
		{
			String sError = "Process save Failed : " + _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}

    private static void validateQty(Prescription _oTR, List _vTD, Connection _oConn) 
    	throws Exception
    {
    	String sLocID = _oTR.getLocationId();
		for ( int i = 0; i < _vTD.size(); i++ ) 
		{
			PrescriptionDetail oTD = (PrescriptionDetail) _vTD.get (i);
			if (oTD.getPrescriptionType() == MedicalAttributes.i_PRES_NORMAL)
			{				
				if (oTD.getQtyBase() == null) //if qty base not set yet
				{
					double dQtyBase = oTD.getQty().doubleValue() * UnitTool.getBaseValue(oTD.getItemId(), oTD.getUnitId(), _oConn);
					oTD.setQtyBase (new BigDecimal(dQtyBase));
				}
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
				if (oItem.getItemType() == i_INVENTORY_PART)
				{
					double dPresQty = oTD.getQtyBase().doubleValue();
					double dQoH = InventoryLocationTool.getInventoryOnHand(oTD.getItemId(), sLocID, _oConn).doubleValue();
					if (dPresQty > dQoH)
					{
						throw new Exception (oTD.getItemCode() + " " + LocaleTool.getString("qty_oh_not_avail") + " Qty : " + dQoH);
					}
				}
			}
			//save powder detail
			else if (oTD.getPrescriptionType() == MedicalAttributes.i_PRES_POWDER)
			{
				List vPwd = oTD.getPowderDetail();
				
				//condition is possible where pres detail 
				if (StringUtil.isNotEmpty(oTD.getPrescriptionDetailId()) && vPwd == null)
				{
					vPwd = PowderTool.getByPresDetID(oTD.getPrescriptionDetailId(), _oConn);
					oTD.setPowderDetail(vPwd);
				}		
				for (int j = 0; j < vPwd.size(); j++)
				{
					PresPowderDetail oPWDD = (PresPowderDetail) vPwd.get(j);
					if (oPWDD.getQtyBase() == null) //if qty base not set yet
					{
						double dQtyBase = oTD.getQty().doubleValue() * UnitTool.getBaseValue(oTD.getItemId(), oTD.getUnitId(), _oConn);
						oPWDD.setQtyBase (new BigDecimal(dQtyBase));
					}	
					Item oItem = ItemTool.getItemByID(oPWDD.getItemId(), _oConn);
					if (oItem.getItemType() == i_INVENTORY_PART)
					{
						double dPwdQty = oPWDD.getQtyBase().doubleValue();
						double dQoH = InventoryLocationTool.getInventoryOnHand(oPWDD.getItemId(), sLocID, _oConn).doubleValue();
						if (dPwdQty > dQoH)
						{
							throw new Exception (oPWDD.getItemCode() + " " + LocaleTool.getString("qty_oh_not_avail") + " Qty : " + dQoH);
						}
					}
				}
			}			
		}
	}

    public static String getMedSalesID (Prescription _oTR) 
    	throws Exception 
    {
    	String sSQL = "SELECT tr.medicine_sales_id FROM medicine_sales_detail td, medicine_sales tr"  + 
    			      " WHERE prescription_id = '" + _oTR.getPrescriptionId() +"' AND td.medicine_sales_id = tr.medicine_sales_id " +
    			      " AND tr.status != 3";
    	List v = SqlUtil.executeQuery(sSQL);
    	if(v.size() > 0)
    	{
    		return ((Record)v.get(0)).getValue(1).asString();
    	}
    	return "";
    }
    
	/**
     * 
     * @param _oTR
     * @param _vTD
     * @param _oConn
     * @throws Exception
     */
	public static void editTrans (Prescription _oTR, List _vTD, String _sCancelBy, Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{
			String sMedSalesID = getMedSalesID(_oTR);
			if(StringUtil.empty(sMedSalesID))
			{
				_oTR.setStatus(i_PENDING);
				_oTR.save (_oConn);
			}
			else
			{
				String sNo = LocaleTool.getString("pending");
				MedicineSales oMS = MedicalSalesTool.getHeaderByID(sMedSalesID, _oConn);
				if(oMS != null) {
					sNo = oMS.getInvoiceNo();
					_oTR = PrescriptionTool.getHeaderByID(_oTR.getPrescriptionId(),_oConn); //refresh probably status already changed
				}												
				throw new Exception(LocaleTool.getString("prescription") + " " + LocaleTool.getString("sold") + " No:" + sNo);
			}
			
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}			
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
    /**
     * 
     * @param _oTR
     * @param _vTD
     * @param m_oConn
     * @throws Exception
     */
	public static void cancelData (Prescription _oTR, String _sCancelBy) 
		throws Exception 
	{	
		Connection _oConn = beginTrans ();		
		try 
		{
			//process save 
			if (_oTR != null)
			{
				_oTR.setStatus(i_CANCELLED);
				_oTR.setCancelBy(_sCancelBy);
				_oTR.setCancelDate(new Date());
				cancelledBy(_oTR.getRemark(), _sCancelBy);
				_oTR.save(_oConn);
			}
			commit (_oConn);
		}
		catch (Exception _oEx) 
		{
			rollback (_oConn);
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	public static double calcPriceIncFee (PrescriptionDetail _oTD)
	{
		if (_oTD != null)
		{
			double dSubTotal = _oTD.getSubTotal().doubleValue();
			double dRfee = Double.valueOf(_oTD.getRfee());
			double dPack = Double.valueOf(_oTD.getPackaging());
			double dQty = _oTD.getQty().doubleValue();
			double dTax = 0;
			if(!PreferenceTool.getSalesInclusiveTax()) 
			{
				dTax = _oTD.getTaxAmount().doubleValue() / 100 * dSubTotal;
			}
			return ((dSubTotal + dRfee + dPack + dTax) / dQty) ;
		}
		return 0;
	}
	
	public static BigDecimal calcTotalIncTax (PrescriptionDetail _oTD) 
		throws Exception
	{
		double dSubTotal = 0;
		double dTax = 0;
		double dTotal = 0;
		if (_oTD != null)
		{
			if (_oTD.getSubTotal() != null) dSubTotal = _oTD.getSubTotal().doubleValue();
			if (_oTD.getTaxAmount()!= null) dTax = _oTD.getTaxAmount().doubleValue() / 100 * dSubTotal;
			if (_oTD.getTotal() != null) {dTotal = _oTD.getTotal().doubleValue();}
			if (!PreferenceTool.getSalesInclusiveTax()) dTotal = dTotal + dTax;
			BigDecimal bdTotal = new BigDecimal(dTotal);
			return bdTotal.setScale(MedConfigTool.getDecimalScale(), BigDecimal.ROUND_HALF_UP);
		}
		return bd_ZERO;
	}
	
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PrescriptionPeer.class, PrescriptionPeer.PRESCRIPTION_ID, _sID, _bIsNext);
	}
	
	//-------------------------------------------------------------------------
	
	public static Prescription getByRegID(String _sRegID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PrescriptionPeer.REGISTRATION_ID, _sRegID);
		List vData = PrescriptionPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (Prescription) vData.get(0);
		}
		return null;
	}
}
