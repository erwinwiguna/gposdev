package com.ssti.enterprise.pos.chart.helper;

import java.awt.Color;
import java.awt.GradientPaint;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.ClusteredXYBarRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.framework.chart.helper.BaseChartHelper;
import com.ssti.framework.chart.helper.ChartHelper;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;


public class DailySalesSummaryChartHelper
	extends BaseChartHelper 
	implements ChartHelper 
{    
    public String getChartFileName (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aStartDate  = (String[]) oParam.get("StartDate");
		String[] aEndDate	 = (String[]) oParam.get("EndDate");
		String[] aLocationID = (String[]) oParam.get("LocationID");

		Date dStartDate = CustomParser.parseDate (aStartDate[0]);
		Date dEndDate = CustomParser.parseDate (aEndDate[0]);
		String sLocationID = aLocationID[0];
		
		List vDates = DateUtil.getDateInRange (dStartDate, dEndDate);
		
        TimeSeries series1 = new TimeSeries("Total Sales", Day.class);
        for (int i = 0; i < vDates.size(); i++)
        {
        	Date dDay = (Date) vDates.get(i);
        	series1.add(new Day(dDay), (BigDecimal)(SalesAnalysisTool.getDailySalesSummary(dDay, sLocationID).get("TotalSales")));
        }

        TimeSeries series2 = new TimeSeries("Total Cost", Day.class);
        for (int i = 0; i < vDates.size(); i++)
        {
        	Date dDay = (Date) vDates.get(i);
        	series2.add(new Day(dDay), (BigDecimal)(SalesAnalysisTool.getDailySalesSummary(dDay, sLocationID).get("TotalCost")));
        }
        
        TimeSeries series3 = new TimeSeries("Gross Profit", Day.class);
        for (int i = 0; i < vDates.size(); i++)
        {
        	Date dDay = (Date) vDates.get(i);
        	series3.add(new Day(dDay), (BigDecimal)(SalesAnalysisTool.getDailySalesSummary(dDay, sLocationID).get("TotalGrossProfit")));
        }

        
        TimeSeriesCollection data = new TimeSeriesCollection();
        data.setDomainIsPointsInTime(false);
        data.addSeries(series1);
        data.addSeries(series2);
        data.addSeries(series3);
        
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                                                  "Daily Sales Summary",  	    // chart title
                                                  "Date",    					// domain axis label
                                                  "Sales Data",       			// range axis label
                                                  data,       					// data
												  //PlotOrientation.VERTICAL,                                                  
                                                  true,          				// include legend
                                                  true,          				// tooltips
                                                  false          				// urls
                                              );

        XYPlot plot = chart.getXYPlot();
        plot.setDomainAxis(new DateAxis("Date"));
        plot.setRenderer(new ClusteredXYBarRenderer());
        
        // set the background color for the chart...
        //chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, new Color(100, 150, 200)));
        chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, Color.orange)); 
        plot.setNoDataMessage("No data available");
        
        return ServletUtilities.saveChartAsPNG(chart, i_WIDTH, i_HEIGHT, _oSession);
    }
}
