package com.ssti.enterprise.pos.chart.helper;

import java.awt.Color;
import java.awt.GradientPaint;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Pie3DPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.DefaultCategoryDataset;
import org.jfree.data.DefaultPieDataset;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;
import org.jfree.util.Rotation;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.framework.chart.helper.BaseChartHelper;
import com.ssti.framework.chart.helper.ChartHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;

public class SalesAnalysisReportChartHelper 
	extends BaseChartHelper implements ChartHelper 
{    
    private JFreeChart chart;
    private int iName   = 1;
    private int iValue  = 2;
    private int iType   = 1;
    private int iWidth  = i_WIDTH;
    private int iHeight = i_HEIGHT;
    private boolean bIncTax = false;
    private String sTitle = "Sales Analysis";
    
    public String getChartFileName (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aStartDate  = (String[]) oParam.get("StartDate");
		String[] aEndDate	 = (String[]) oParam.get("EndDate");
		String[] aSiteID	 = (String[]) oParam.get("SiteID");
		String[] aLocationID = (String[]) oParam.get("LocationID");
		String[] aKategoriID = (String[]) oParam.get("KategoriID");
		String[] aWidth		 = (String[]) oParam.get("Width");
		String[] aHeight	 = (String[]) oParam.get("Height");
		String[] aType	 	 = (String[]) oParam.get("Type");
		String[] aIncTax	 = (String[]) oParam.get("IncludeTax");
		
		Date dStartDate    = CustomParser.parseDate (aStartDate[0]);
		Date dEndDate 	   = CustomParser.parseDate (aEndDate[0]);
		String sSiteID	   = aSiteID	 [0];
		String sLocationID = aLocationID [0];
		String sKategoriID = aKategoriID [0];
		
		iType 	= Integer.parseInt (aType[0]);
		iWidth  = Integer.parseInt (aWidth[0]);
		iHeight = Integer.parseInt (aHeight[0]);

		if (aIncTax != null && aIncTax.length > 0)
		{
			String sIncTax  = aIncTax[0];
			bIncTax = Boolean.parseBoolean(sIncTax);
		}
		
		List vData = null;
		String sKategori = "";
		
		if (sKategoriID != null && !sKategoriID.equals("")) {
			vData = SalesAnalysisTool.getTotalSalesPerItem (dStartDate, dEndDate, sLocationID, sKategoriID,AppAttributes.i_TRANS_PROCESSED,bIncTax);
			iName  = 2;
			iValue = 3;
			sKategori = "Item";
		}   
		else if (sLocationID != null && !sLocationID.equals("")) {
			vData = SalesAnalysisTool.getTotalSalesPerKategori (dStartDate, dEndDate, sLocationID,AppAttributes.i_TRANS_PROCESSED,bIncTax);
			sKategori = "Kategori";
		}
		else if (sSiteID != null && !sSiteID.equals("")) {
			vData = SalesAnalysisTool.getTotalSalesPerLocation (dStartDate, dEndDate, sSiteID,AppAttributes.i_TRANS_PROCESSED,bIncTax);
			sKategori = "Location";
		}
		else {
			vData = SalesAnalysisTool.getTotalSalesPerSite (dStartDate, dEndDate,AppAttributes.i_TRANS_PROCESSED,bIncTax);	
			sKategori = "Site";
		}
		
		generateTitle (dStartDate, dEndDate, sKategori);
		
		if (iType == 1) {
			create3DPieChart(vData);
		}
		if (iType == 2) {
			createPieChart(vData);
		}		
		if (iType == 3) {
			createVerticalBarChart(vData, sKategori);
		}		
		if (iType == 4) {
			create3DVerticalBarChart(vData, sKategori);
		}
        return ServletUtilities.saveChartAsPNG(chart, iWidth, iHeight, _oSession);
    }

	private void generateTitle (Date _dStartDate, Date _dEndDate, String _sKategori)
	{
		StringBuilder oTitle = new StringBuilder ();
		oTitle.append (" Sales Analysis");
		oTitle.append (" From ");
		oTitle.append (CustomFormatter.formatDate(_dStartDate));
		oTitle.append (" To ");
		oTitle.append (CustomFormatter.formatDate(_dEndDate));
		oTitle.append (" [");
		oTitle.append (_sKategori);
		oTitle.append ("] ");

		sTitle = oTitle.toString();
    }
    
    private void create3DPieChart (List _vData)
    {
        DefaultPieDataset data = new DefaultPieDataset();		
		if (_vData != null)
		{
			for (int i = 0; i < _vData.size(); i++)
			{
        		List oData = (List) _vData.get(i);
        		data.setValue((String)oData.get(iName), (BigDecimal)oData.get(iValue));        		
			}
		}
        chart = ChartFactory.createPieChart3D(sTitle,  // chart title
            								  data,    // data
            								  true,    // include legend
            								  true,
            								  false);
            								  
        chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, Color.orange));
        Pie3DPlot plot = (Pie3DPlot) chart.getPlot();
        plot.setSectionLabelType(PiePlot.NAME_AND_PERCENT_LABELS);
        plot.setStartAngle(270);
        plot.setDirection(Rotation.ANTICLOCKWISE);
        plot.setForegroundAlpha(0.60f);
        plot.setInteriorGap(0.33);
        plot.setNoDataMessage("No data available");    
    }

    private void createPieChart (List _vData)
    {
        DefaultPieDataset data = new DefaultPieDataset();		
		if (_vData != null)
		{
			for (int i = 0; i < _vData.size(); i++)
			{
        		List oData = (List) _vData.get(i);
        		data.setValue((String)oData.get(iName), (BigDecimal)oData.get(iValue));        		
			}
		}
        chart = ChartFactory.createPieChart(sTitle,  // chart title
            								data,    // data
            								true,    // include legend
            								true,
            								false);
            								  
        chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, Color.orange));
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setSectionLabelType(PiePlot.NAME_AND_PERCENT_LABELS);
        plot.setStartAngle(270);
        plot.setDirection(Rotation.ANTICLOCKWISE);
        plot.setForegroundAlpha(0.60f);
        plot.setInteriorGap(0.33);
        plot.setNoDataMessage("No data available");    
    }

    private void createVerticalBarChart (List _vData, String _sKategori)
    {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		if (_vData != null)
		{
			for (int i = 0; i < _vData.size(); i++)
			{
        		List oData = (List) _vData.get(i);
        		dataset.addValue((BigDecimal)oData.get(iValue), (String)oData.get(iName), "");
			}
		}
        chart = ChartFactory.createBarChart(
            sTitle,                   // chart title
            _sKategori,               // domain axis label
            "Amount",                 // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, // orientation
            true,           		  // include legend
            true,
            false
        );
            								  
        chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, Color.orange));
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setMaxCategoryLabelWidthRatio(10.0f);
        //NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        //rangeAxis.setRange(0.0, 100.0);
        //rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    } 
 
    private void create3DVerticalBarChart (List _vData, String _sKategori)
    {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		if (_vData != null)
		{
			for (int i = 0; i < _vData.size(); i++)
			{
        		List oData = (List) _vData.get(i);
        		dataset.addValue((BigDecimal)oData.get(iValue), (String)oData.get(iName), "");
			}
		}
        chart = ChartFactory.createBarChart3D(
            sTitle,          		   // chart title
            _sKategori,                // domain axis label
            "Amount",                  // range axis label
            dataset,                   // data
            PlotOrientation.VERTICAL,  // orientation
            true,                      // include legend
            true,                      // tooltips
            false                      // urls
        );

		chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, Color.orange));
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setForegroundAlpha(1.0f);
        CategoryAxis axis = plot.getDomainAxis();
        CategoryLabelPositions p = axis.getCategoryLabelPositions();
        CategoryLabelPosition left = new CategoryLabelPosition( 
            RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT, TextAnchor.CENTER_LEFT, 0.0
        );
        axis.setCategoryLabelPositions(CategoryLabelPositions.replaceLeftPosition(p, left));
        axis.setMaxCategoryLabelWidthRatio(3.0f);
    } 
}