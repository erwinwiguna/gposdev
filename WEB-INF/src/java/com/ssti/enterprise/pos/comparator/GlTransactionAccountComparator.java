package com.ssti.enterprise.pos.comparator;

import java.util.Comparator;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.tools.gl.AccountTool;

public class GlTransactionAccountComparator implements Comparator 
{
	public int compare (Object o1, Object o2)
	{
		GlTransaction oGL1 = (GlTransaction) o1;
		GlTransaction oGL2 = (GlTransaction) o2;
		
		String sCode1 = "";
		String sCode2 = "";
		if (oGL1 != null && oGL2 != null)
		{
			try 
			{
				Account oACC1 = AccountTool.getAccountByID(oGL1.getAccountId());
				Account oACC2 = AccountTool.getAccountByID(oGL2.getAccountId());
				
				sCode1 = oACC1.getAccountCode();
				sCode2 = oACC2.getAccountCode();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return sCode1.compareTo(sCode2);
	}
}


