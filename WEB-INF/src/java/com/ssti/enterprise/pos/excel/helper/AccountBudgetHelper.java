package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GeneralLedgerTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class AccountBudgetHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aGLType  = (String[]) oParam.get("GLType");  
        int iGLType = Integer.parseInt(aGLType[0]);

        String[] aYear  = (String[]) oParam.get("Year");  
        int iYear = Integer.parseInt(aYear[0]);

        String[] aSub  = (String[]) oParam.get("id");  
        String sSubID = aSub[0];

        String[] aSubType  = (String[]) oParam.get("src");  
        String sSubType = aSubType[0];
        
        String sSubCode = "ALL";
        sSubCode = LocationTool.getCodeByID(sSubID);
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Account Budget");
        
        List vData = AccountTool.getAccountByGLType (iGLType, true, true);
        List vPeriods = PeriodTool.getPeriodByYear(iYear, null);
        
        HSSFRow oRow = oSheet.createRow(0);
        
        int iCol = 0;
		
		createHeaderCell(oWB, oRow, (short)iCol, sSubType);   	   iCol++;
        createHeaderCell(oWB, oRow, (short)iCol, "Account Code");    iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Account Name");    iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Year");    		   iCol++;
		
		for (int i= 0; i < vPeriods.size(); i++)
		{
			Period oPeriod = (Period) vPeriods.get(i);
			createHeaderCell(oWB, oRow, (short)iCol, 
				Integer.valueOf(oPeriod.getMonth()).toString());    
			
			iCol++;			
		}

        for (int i = 0; i < vData.size(); i++)
        {
            Account oAcc = (Account) vData.get(i);
            
            HSSFRow oRow2 = oSheet.createRow(1 + i); 
            
            iCol = 0;
            createCell(oWB, oRow2, (short)iCol, sSubCode); iCol++; 
            createCell(oWB, oRow2, (short)iCol, oAcc.getAccountCode ());  iCol++; 
            createCell(oWB, oRow2, (short)iCol, oAcc.getAccountName ());  iCol++; 
            createCell(oWB, oRow2, (short)iCol, Integer.valueOf(iYear).toString()); iCol++; 
            
    		for (int j = 0; j < vPeriods.size(); j++)
    		{
    			Period oPeriod = (Period) vPeriods.get(j);
    			BigDecimal dBudget = 
    				GeneralLedgerTool.getBudget(oPeriod.getPeriodId(), oAcc.getAccountId(), sSubType, sSubID);
    			
    			createCell(oWB, oRow2, (short)iCol,  dBudget.toString());        			
    			iCol++;			
    		}  
        }        
        return oWB;
    }
}
