package com.ssti.enterprise.pos.excel.helper;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GeneralLedgerTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source$
 * Purpose: 
 *
 * @author  $Author$
 * @version $Id$
 *
 * $Log$
 *
 */
public class AccountBudgetLoader extends BaseExcelHelper
{    
	private static Log log = LogFactory.getLog ( AccountBudgetLoader.class );
	
	private StringBuilder m_oResult = new StringBuilder();
	private StringBuilder m_oNew 	= new StringBuilder("\nBudget Data : \n");
	private StringBuilder m_oError  = new StringBuilder("\nERROR Budget : \n");
	
	private String m_sUserName;
	private int m_iRejected   = 0;
	private int m_iError   	  = 0;
	private int m_iUpdated    = 0;
	private int m_iNewAccount = 0;
	
	private int m_iTotalRows  = 0;
	private boolean m_bStart  = false;
	
	private String sSubType = "";
	
	//headers used to validate excel template
	private static final String[] aHeaders = {
		"SUB", "Account Code", "Account Name", "Year"
		//"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"
	};
	
	public AccountBudgetLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	public String getResult()
	{
		m_oResult.append (m_oNew);
		m_oResult.append ("\nTotal Budget Saved : ");
		m_oResult.append (m_iNewAccount);
		m_oResult.append ("\n");		
		m_oResult.append ("\nTotal ERROR : ");		
		m_oResult.append (m_iError);

		return m_oResult.toString();
	}
	
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		
		log.debug ("Num of rows : " + m_iTotalRows); 
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		log.debug ("Current Row : " + iRow); 

    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			short iFirstCellIdx = oRow.getFirstCellNum();
				HSSFCell oCell = oRow.getCell(iFirstCellIdx);
				if (!m_bStart) 
				{
					sSubType = oCell.getStringCellValue();
					log.debug("sSubType " + sSubType);
					if (sSubType != null && 
					       (sSubType.equals(aHeaders[0]) || 
					        sSubType.equals("loc")) 
					    )
					{ 
						if (validateHeader(oRow)) 
						{
						    m_bStart = true; 
					    }
                    }	
				}   
				else 
				{
					updateBudget (oRow, iFirstCellIdx);
				}
			}
    	} 
    }

    private boolean validateHeader (HSSFRow _oRow)	
    	throws Exception
    {
        short iFirst = _oRow.getFirstCellNum();
        short iLast  = _oRow.getLastCellNum();
        
        int iHeader = 0;
        int iHeaderLength = aHeaders.length; 
        
        //to prevent generated excel from account page display array out of bound
        if (iHeaderLength < (iLast - iFirst)) iLast = (short) (iFirst + iHeaderLength);        
        
        for (short i = iFirst; i < iLast; i++)
        {
            String sHeader = getString(_oRow, i);
            log.debug ("Requested Header : '" + aHeaders[iHeader] + "' Value In Excel File : '" + sHeader + "'");
            if (i != 0)
        	{
	            if (!aHeaders[iHeader].equals(sHeader))
	            {
	                throw new NestableException ("File Header Mismatch, Requested : '" + 
	                	aHeaders[iHeader] + "' In Excel : '" + getString(_oRow, i) + "'");
	            }
        	}
            iHeader++;
        }
        return true;
    }
    
    private void updateBudget (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {            		
		if (getString(_oRow, _iIdx) != null)				
		{
			short i = 0;
			
			String sSubCode 	 = getString(_oRow, _iIdx + i);  i++;
			String sAccountCode  = getString(_oRow, _iIdx + i);  i++;
			String sAccountName  = getString(_oRow, _iIdx + i);  i++;
			String sYear 	     = getString(_oRow, _iIdx + i);  i++;

	        String sSubID = "";	        
	        Location oLoc = LocationTool.getLocationByCode(sSubCode);
	        if (oLoc != null) sSubID = oLoc.getLocationId();
	        
			List vPeriods = PeriodTool.getPeriodByYear(sYear);
            
			Account oAccount = AccountTool.getAccountByCode(sAccountCode, false);
			
			if (oAccount != null)
			{
	    		for (int j = 0; j < vPeriods.size(); j++)
	    		{
	    			Period oPeriod = (Period) vPeriods.get(j);
	    			BigDecimal dBudget = getBigDecimal(_oRow, _iIdx + i);  i++;
	
	    			try
					{
	    				if (dBudget.doubleValue() != 0)
	    				{
		    				String sSubType2 = sSubType;
		    				if (sSubType == "ALL") sSubType2 = "";
		    				GeneralLedgerTool.setBudget(
		    					oPeriod.getPeriodId(), oAccount.getAccountId(), sSubType2, sSubID, dBudget
							);
		    				
							m_iNewAccount++;
							m_oNew.append (StringUtil.left(m_iNewAccount + ".", 4));					
							m_oNew.append (" ROW : ");
							m_oNew.append (StringUtil.left(Integer.valueOf(_oRow.getRowNum()).toString(), 4));	
							
							if (!sSubType2.equals(""))
							{
								m_oNew.append (StringUtil.left(sSubType2,5)).append(": ");
								m_oNew.append (StringUtil.left(sSubCode,8)).append(" ");
							}
	
							m_oNew.append (StringUtil.left(sAccountName,35));
							m_oNew.append (" Period : ");
							m_oNew.append (StringUtil.right(Integer.valueOf(oPeriod.getMonth()).toString(),2));					
							m_oNew.append ("  Budget : ");
							m_oNew.append (dBudget);					
							m_oNew.append ("\n");    				
	    				}
					}
					catch (Exception _oEx)
					{
						log.error(_oEx);
						
						m_iError++;
						m_oError.append (StringUtil.left(m_iError + ".", 4));					
						m_oError.append (" ROW : ");
						m_oError.append (StringUtil.left(Integer.valueOf(_oRow.getRowNum()).toString(), 4));					
						m_oError.append (StringUtil.left(sAccountCode,15));
						m_oError.append (StringUtil.left(sAccountName,35));
						m_oError.append (" Period : ");
						m_oError.append (StringUtil.right(Integer.valueOf(oPeriod.getMonth()).toString(),3));					
						m_oError.append (" Budget : ");
						m_oError.append (dBudget);					
						
						m_oError.append (", ERROR : " + _oEx.getMessage());
						m_oError.append ("\n");						
					}	
	    		}	  
			}
			else
			{
				m_iError++;
				m_oError.append (StringUtil.left(m_iError + ".", 4));					
				m_oError.append (" ROW : ");
				m_oError.append (StringUtil.left(Integer.valueOf(_oRow.getRowNum()).toString(), 4));					
				m_oError.append (StringUtil.left(sAccountCode,15));
				m_oError.append (StringUtil.left(sAccountName,25));
				m_oError.append ("\t ERROR : ACCOUNT NOT FOUND ");
				m_oError.append ("\n");					
			}
		}    
    }    
}
