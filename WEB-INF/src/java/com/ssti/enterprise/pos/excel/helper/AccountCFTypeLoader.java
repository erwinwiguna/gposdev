package com.ssti.enterprise.pos.excel.helper;

import java.sql.Connection;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountCashflow;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

public class AccountCFTypeLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (AccountCFTypeLoader.class );
	
	public static final String s_START_HEADING = "Account Code";
	
	public AccountCFTypeLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sAccCode = getString(_oRow, _iIdx + i); i++;
			String sCFTCode = getString(_oRow, _iIdx + i); i++;

			if (StringUtil.isNotEmpty(sAccCode) && StringUtil.isNotEmpty(sCFTCode))
			{		
                Connection oConn = null;
                try
                {
                    oConn = BaseTool.beginTrans();
                    Account oAcc = AccountTool.getAccountByCode(sAccCode);
                    CashFlowType oCFT = CashFlowTypeTool.getTypeByCode(sCFTCode, oConn);
                    
                    if (oAcc != null && oCFT != null)
                    {
                        AccountCashflow oACF = CashFlowTypeTool.getAccCFT(oAcc.getAccountId(), oCFT.getCashFlowTypeId());
                        boolean bNew = true;
                        boolean bValid = true;
                        
                        log.debug ("Column " + _iIdx + " CashFlowType Code : " + sAccCode);
                        if (oACF == null) 
                        {   
                            log.debug ("Code Not Exist, create new Code");
                            
                            oACF = new AccountCashflow();
                            oACF.setAccountCashflowId(IDGenerator.generateSysID());
                            oACF.setAccountId(oAcc.getAccountId());     
                            oACF.setCashFlowTypeId (oCFT.getCashFlowTypeId());
                            bNew = true;
                        }
                        else 
                        {
                            bNew = false;
                        }

                        if(bValid && StringUtil.isEqual(oEmpty.toString(),s_EMPTY_REJECT) 
                                  && StringUtil.isEqual(oLength.toString(),s_LENGTH_REJECT))
                        {
                            if (bNew) 
                            {   
                                m_iNewData++;
                                m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));                                      
                                m_oNewData.append (StringUtil.left(sAccCode,30));
                                m_oNewData.append (StringUtil.left(sCFTCode,100));
                                m_oNewData.append ("\n");
                            }
                            else 
                            {
                                m_iUpdated++;
                                m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));                                      
                                m_oUpdated.append (StringUtil.left(sAccCode,30));
                                m_oUpdated.append (StringUtil.left(sCFTCode,100));
                                m_oUpdated.append ("\n");
                            }                            
                            oACF.save(oConn);
                        }
                        BaseTool.commit(oConn);
                    }
                    else
                    {
                        m_iRejected++;
                        m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));                 
                        m_oRejected.append (StringUtil.left(sAccCode,30));
                        m_oRejected.append (StringUtil.left(sCFTCode,50));
                        m_oRejected.append ("\n");
                    }
                }
                catch (Exception e)
                {
                    BaseTool.rollback(oConn);
                    throw new NestableException(e);
                }

			}
		}    
	}
}
