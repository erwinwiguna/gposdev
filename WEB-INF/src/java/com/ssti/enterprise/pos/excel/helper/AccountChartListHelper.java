package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.AccountTypeTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;

public class AccountChartListHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aGLType  = (String[]) oParam.get("GLType");  
        int iGLType  = Integer.parseInt(aGLType[0]);
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Account Chart List");
        
        List vData = AccountTool.getAccountByGLType (iGLType, true, true);
        
        HSSFRow oRow = oSheet.createRow(0);
        
        int iCol = 0;
		
		createHeaderCell(oWB, oRow, (short)iCol, "Account Code");    iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Account Name");    iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Description");     iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Account Type");    iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Parent Code");     iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency");        iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Opening Balance"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "As Of");           iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "");                iCol++;
     
        for (int i = 0; i < vData.size(); i++)
        {
            Account oAcc = (Account) vData.get(i);
            Account oParent = AccountTool.getAccountByID (oAcc.getParentId());
            Currency oCurr  = CurrencyTool.getCurrencyByID(oAcc.getCurrencyId());
            AccountType oType = AccountTypeTool.getAccountType(oAcc.getAccountType());
            
            String sParentCode = "";
            if (oParent != null) sParentCode = oParent.getAccountCode();

            String sCurrencyCode = "";
            if (oCurr != null) sCurrencyCode = oCurr.getCurrencyCode();
                        
            HSSFRow oRow2 = oSheet.createRow(1 + i); 
            
            iCol = 0;
            createCell(oWB, oRow2, (short)iCol, oAcc.getAccountCode   ());                      iCol++; 
            createCell(oWB, oRow2, (short)iCol, oAcc.getAccountName   ());                      iCol++; 
            createCell(oWB, oRow2, (short)iCol, oAcc.getDescription());                         iCol++; 
            createCell(oWB, oRow2, (short)iCol, oType.getAccountTypeName());                    iCol++; 
            createCell(oWB, oRow2, (short)iCol, sParentCode);                                   iCol++; 
            createCell(oWB, oRow2, (short)iCol, sCurrencyCode);                                 iCol++; 
            createCell(oWB, oRow2, (short)iCol, oAcc.getOpeningBalance().toString());           iCol++; 
            createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oAcc.getAsDate()));  iCol++;             
            createCell(oWB, oRow2, (short)iCol, oAcc.getAltCode());                             iCol++; 
        }        
        return oWB;
    }
}
