package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class AdjustmentTypeHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = AdjustmentTypeTool.getAllAdjustmentType();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Adjustment Type Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
		createHeaderCell(oWB, oRow, (short)iCol,"Adjustment Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Default Type"); iCol++;				
		createHeaderCell(oWB, oRow, (short)iCol,"Adjustment Account"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol,"Is Cost Adjustment"); iCol++;
        createHeaderCell(oWB, oRow, (short)iCol,"Is Transfer"); iCol++;
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                AdjustmentType oAdj = (AdjustmentType) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oAdj.getAdjustmentTypeCode());   iCol++; 
                createCell(oWB, oRow2, (short)iCol, oAdj.getDescription());iCol++; 
                createCell(oWB, oRow2, (short)iCol, oAdj.getDefaultTransType());iCol++; 
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oAdj.getDefaultAccountId()));iCol++; 
                createCell(oWB, oRow2, (short)iCol, oAdj.getDefaultCostAdj());iCol++; 
                createCell(oWB, oRow2, (short)iCol, oAdj.getIsTransfer());iCol++;                 
            }        
        }
        return oWB;
    }
}
