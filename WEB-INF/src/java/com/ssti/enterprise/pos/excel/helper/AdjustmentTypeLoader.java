package com.ssti.enterprise.pos.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.AdjustmentTypeManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

public class AdjustmentTypeLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (AdjustmentTypeLoader.class );
	
	public static final String s_START_HEADING = "Adjustment Type Code";
	
	public AdjustmentTypeLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCode = getString(_oRow, _iIdx + i); i++;
			String sDesc = getString(_oRow, _iIdx + i); i++;
            
			if (StringUtil.isNotEmpty(sCode))
			{				
				AdjustmentType oAdj = AdjustmentTypeTool.getByCode(sCode);
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Adjustment Type Code : " + sCode);
				
				if (oAdj == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					oAdj = new AdjustmentType();
					oAdj.setAdjustmentTypeId (IDGenerator.generateSysID());
					oAdj.setAdjustmentTypeCode(sCode); 	
					bNew = true;
				}
				else 
				{
					bNew = false;
				}
				oAdj.setDescription(sDesc); 
				bValid = validateString("Description", oAdj.getDescription(), 100, oEmpty, oLength);

                String sType = getString(_oRow, _iIdx + i); i++;
                oAdj.setDefaultTransType(1);                
                
                String sAcc = getString(_oRow, _iIdx + i); i++;
                Account oAcc = AccountTool.getAccountByCode(sAcc, false);
                String sAccID = "";
                if (oAcc != null) sAccID = oAcc.getAccountId();
                oAdj.setDefaultAccountId(sAccID);

                String sIsCA = getString(_oRow, _iIdx + i); i++;
                if (StringUtil.isEqual(sIsCA,"1") || StringUtil.equalsIgnoreCase(sIsCA,"true"))
                {
                    oAdj.setDefaultCostAdj(true);
                }
                else
                {
                    oAdj.setDefaultCostAdj(false);
                }

                String sIsTRF = getString(_oRow, _iIdx + i); i++;
                if (StringUtil.isEqual(sIsTRF,"1") || StringUtil.equalsIgnoreCase(sIsTRF,"true"))
                {
                    oAdj.setDefaultCostAdj(true);
                }
                else
                {
                    oAdj.setDefaultCostAdj(false);
                }
                oAdj.save();
                AdjustmentTypeManager.getInstance().refreshCache(oAdj);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sCode,30));
						m_oNewData.append (StringUtil.left(sDesc,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sCode,30));
						m_oUpdated.append (StringUtil.left(sDesc,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCode,30));
				    m_oRejected.append (StringUtil.left(sDesc,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    		
	}
}
