package com.ssti.enterprise.pos.excel.helper;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelLoader;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/BaseExcelLoader.java,v $
 * Purpose: Base Class for all excel loader
 *
 * @author  $Author: albert $
 * @version $Id: BaseExcelLoader.java,v 1.10 2007/06/30 13:31:56 albert Exp $
 *
 * $Log: BaseExcelLoader.java,v $
 *  
 *  2017-08-25
 *  - change validateString, if string length < 100 and contains forbidden chars (enter, ', ") then 
 *    reject String
 *
 */
public abstract class BaseExcelLoader extends BaseExcelHelper implements ExcelLoader, AppAttributes
{   
	static Log log = LogFactory.getLog ( BaseExcelLoader.class );

	protected static final String s_EMPTY_REJECT  = "Invalid / Empty Fields : ";
	protected static final String s_LENGTH_REJECT = "Length > Max. : ";
	protected static final String s_ACCOUNT_REJECT = "Account Is Parent : ";
	
	protected HSSFRow m_oHeaderRow = null;
	
	protected String sHeading = "Item Code";
	
	protected StringBuilder m_oResult   = new StringBuilder();
	protected StringBuilder m_oUpdated  = new StringBuilder("\nUpdated Data : \n");
	protected StringBuilder m_oNewData  = new StringBuilder("\nNew Data : \n");
	protected StringBuilder m_oError    = new StringBuilder("\nERROR : \n");
	protected StringBuilder m_oRejected = new StringBuilder("\nRejected Data : \n");

	protected String m_sUserName;
	protected int m_iUpdated  = 0;
	protected int m_iNewData  = 0;
	protected int m_iRejected = 0;	
	protected int m_iError    = 0;
	
	protected List m_vDataList;
	protected int m_iTotalRows = 0;
	protected boolean m_bStart = false;
	
	protected boolean m_bValidate = false;
	protected String[] m_aHeaders = {};
	
    public String getSummary()
    {
    	StringBuilder oSummary = new StringBuilder();
    	oSummary.append(" (NEW : ").append(m_iNewData)
				.append(", UPDATED : ").append(m_iUpdated)
				.append(", REJECTED : ").append(m_iRejected)
				.append(", ERROR : ").append(m_iError).append(")");
    	
    	return oSummary.toString();
    }
	
	public String getResult()
	{
		m_oResult.append (m_oNewData);
		m_oResult.append ("\nTotal New Data : ");
		m_oResult.append (m_iNewData);
		m_oResult.append ("\n");
		m_oResult.append (m_oUpdated);
		m_oResult.append ("\nTotal Updated : ");
		m_oResult.append (m_iUpdated);
		m_oResult.append ("\n");
		m_oResult.append (m_oRejected);		
		m_oResult.append ("\nTotal REJECTED : ");
		m_oResult.append (m_iRejected);
		m_oResult.append ("\n");
		m_oResult.append (m_oError);
		m_oResult.append ("\nTotal ERROR : ");
		m_oResult.append (m_iError);

		return m_oResult.toString();
	}	
	
	public List getListResult()
	{
		return m_vDataList;
	}

	public int getTotalRows()
	{
		return m_iTotalRows;
	}                           
	
    public int getTotalError() 
    {
		return m_iError;
	}

    
	public String getErrorMessage() 
	{
		return m_oError.toString();
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			short iFirstCellIdx = oRow.getFirstCellNum();
    			if (!m_bStart) 
				{
					if (getString(oRow, iFirstCellIdx) != null && 
						getString(oRow, iFirstCellIdx).equals(sHeading)) 
					{ 
						m_oHeaderRow = oRow;
						m_bStart = true; 
						m_vDataList = new ArrayList(m_iTotalRows);
						
						if (m_bValidate)
						{
							validate();
						}
					}	
				}   
				else 
				{
					try
					{
						updateList (oRow, iFirstCellIdx);
					}
					catch (Exception _oEx)
					{
						log.error(_oEx);
						_oEx.printStackTrace();
						
						m_iError++;
						m_oError.append (m_iError);					
						m_oError.append (". ERROR Loading ROW : ");
						m_oError.append (iRow + 1);
						m_oError.append ("\n   " + _oEx.getMessage());
						m_oError.append ("\n");						
					}
				}
			}
    	} 
    }

    /**
     * validate headers
     * 
     * @param _aHeaders
     * @param _oRow
     * @throws Exception
     */
    protected void validate() 
    	throws Exception
    {
    	for (int i = 0; i < m_aHeaders.length; i++)
    	{
    		String sHeader = getString(m_oHeaderRow,i);
    		if (!StringUtil.isEqual(m_aHeaders[i], sHeader))
    		{
    			throw new Exception("Invalid Header: Column " + getColChar(i) + 
    				" Value In File [" + sHeader + "], Correct Header Value [" + m_aHeaders[i] + "]");
    		}
    	}
    }

    /**
     * method to override in every loader 
     * 
     * @param _oRow current Row to process
     * @param _iIdx start column index
     * @throws Exception
     */
    protected abstract void updateList (HSSFRow _oRow, short _iIdx) throws Exception;
    
    /**
     * method to check whether we should start load process
     * this will check whether current row value not equals to 
     * heading 
     * 
     * @param _oRow
     * @param _iIdx
     * @return
     */
    protected boolean started(HSSFRow _oRow, short _iIdx)
    {
    	if (getString(_oRow, _iIdx) != null && 
    			!getString(_oRow, _iIdx).equals(sHeading))
    	{
    		return true;
    	}
    	return false;
    }

    protected boolean validateString (String _sField, 
							    	  String _sValue, 
									  StringBuilder _sEmpty)
    {
    	return validateString (_sField, _sValue, -1, _sEmpty, null);
    }
    
    protected boolean validateString (String _sField, 
    								  String _sValue, 
									  int _iMax, 
									  StringBuilder _sEmpty,
									  StringBuilder _sLength)
	{
    	if (StringUtil.isEmpty(_sValue))
    	{
    		if (_sEmpty.length() > s_EMPTY_REJECT.length()) _sEmpty.append(",");
    		_sEmpty.append(" ").append(_sField);
    		return false;
    	}
    	else if (StringUtil.isNotEmpty(_sValue))
    	{
    		if (_iMax > 0 && _sLength != null && _sValue.length() > _iMax)
    		{
    			if (_sLength.length() > s_LENGTH_REJECT.length()) _sLength.append(",");
    			_sLength.append(" ").append(_sField).append("(").append(_iMax).append(")");
    			return false;    		
    		}
    		if(_iMax <= 255 &&
    		   (StringUtil.containsIgnoreCase(_sValue, "'") ||    				  
    			StringUtil.containsIgnoreCase(_sValue, "\"") ||
    			StringUtil.containsIgnoreCase(_sValue, "\r\n") || 
    			StringUtil.containsIgnoreCase(_sValue, "\n") ||
    			!_sValue.matches("\\A\\p{ASCII}*\\z")
    		    ))
    		{
    			if (_sLength.length() > s_LENGTH_REJECT.length()) _sLength.append(",");
    			_sLength.append(" ").append(_sField).append("(").append(" Forbidden Character i.e [', \", Enter, Non ASCII ] ").append(")");
    			return false;
    		}
    	}    	
    	return true;
	}
    
    protected boolean validateAccount(String _sAccID, StringBuilder _sAcc) 
    	throws Exception 
    {
    	Account oAcc = AccountTool.getAccountByID(_sAccID);
    	if (oAcc != null && oAcc.getHasChild())
    	{
    		if (_sAcc.length() > s_ACCOUNT_REJECT.length()) _sAcc.append(",");
    		_sAcc.append(" ").append(oAcc.getAccountCode()).append(" ").append(oAcc.getAccountName());
    		return false;
    	}
    	return true;
    }
    
    public static String getColChar(int _iCol)
    {
		byte[] b = {(byte)(65 + _iCol)};
		String col = new String(b, Charset.defaultCharset());
		return col.toUpperCase();
    }
}                                                                                           