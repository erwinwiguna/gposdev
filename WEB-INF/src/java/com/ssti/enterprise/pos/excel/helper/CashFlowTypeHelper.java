package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class CashFlowTypeHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = CashFlowTypeTool.getAllType();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Cash Flow Type");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iRow = 0;
        
		createHeaderCell(oWB, oRow, (short)iRow, "Cash Flow Type Code"); iRow++;
		createHeaderCell(oWB, oRow, (short)iRow, "Description"); iRow++;
		createHeaderCell(oWB, oRow, (short)iRow, "Group"); iRow++;				
		createHeaderCell(oWB, oRow, (short)iRow, "Default Type"); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Parent Code"); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, ""); iRow++;
        
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                CashFlowType oCFT = (CashFlowType) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                String sType = "Deposit";
                if (oCFT.getDefaultType() == AppAttributes.i_WITHDRAWAL)
                {
                	sType = "Withdrawal";
                }
                
                iRow = 0;
                createCell(oWB, oRow2, (short)iRow, oCFT.getCashFlowTypeCode()); iRow++; 
                createCell(oWB, oRow2, (short)iRow, oCFT.getDescription());      iRow++; 
                createCell(oWB, oRow2, (short)iRow, oCFT.getCashFlowTypeGroup());iRow++;                                
                createCell(oWB, oRow2, (short)iRow, sType); iRow++;                
                createCell(oWB, oRow2, (short)iRow, CashFlowTypeTool.getCodeByID(oCFT.getParentId()));  iRow++; 
                createCell(oWB, oRow2, (short)iRow, oCFT.getAltCode()); iRow++;
                //createCell(oWB, oRow2, (short)iRow, CashFlowTypeTool.getAccCodeByCFT(oCFT.getCashFlowTypeId()));  iRow++; 
            }        
        }
        return oWB;
    }
}
