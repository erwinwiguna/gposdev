package com.ssti.enterprise.pos.excel.helper;

import java.sql.Connection;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.CashFlowTypeManager;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.framework.tools.StringUtil;

public class CashFlowTypeLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog(CashFlowTypeLoader.class);
	
	public static final String s_START_HEADING = "Cash Flow Type Code";
	
	public CashFlowTypeLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCFTCode = getString(_oRow, _iIdx + i); i++;
			String sCFTName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCFTCode))
			{		
                Connection oConn = null;
                try
                {
                    oConn = BaseTool.beginTrans();
                    CashFlowType oCFT = CashFlowTypeTool.getTypeByCode(sCFTCode, oConn);
                    boolean bNew = true;
                    boolean bValid = true;
                    
                    log.debug ("Column " + _iIdx + " CashFlowType Code : " + sCFTCode);
                    
                    if (oCFT == null) 
                    {   
                        log.debug ("Code Not Exist, create new Code");
                        
                        oCFT = new CashFlowType();
                        oCFT.setCashFlowTypeId (IDGenerator.generateSysID());
                        oCFT.setCashFlowTypeCode(sCFTCode);     
                        oCFT.setHasChild(false);
                        bNew = true;
                    }
                    else 
                    {
                        bNew = false;
                    }
                    oCFT.setDescription(sCFTName); 
                    bValid = validateString("CashFlowType Name", oCFT.getDescription(), 100, oEmpty, oLength);

                    String sGroup = getString(_oRow, _iIdx + i); i++;
                    oCFT.setCashFlowTypeGroup(sGroup);
                    bValid = validateString("CashFlowType Group", oCFT.getCashFlowTypeGroup(), 100, oEmpty, oLength);
                    
                    String sType = getString(_oRow, _iIdx + i); i++;
                    int iType = AppAttributes.i_DEPOSIT;
                    if (StringUtil.isNotEmpty(sType) && StringUtil.equalsIgnoreCase(sType,"Withdrawal"))
                    {
                        iType = AppAttributes.i_WITHDRAWAL;
                    }
                    oCFT.setDefaultType(iType);

                    CashFlowType oParent = null;
                    String sParent = getString(_oRow, _iIdx + i); i++;
                    oCFT.setParentId("");
                    if(StringUtil.isNotEmpty(sParent))
                    {
                        oParent = CashFlowTypeTool.getTypeByCode(sParent,oConn);
                        if(oParent != null) oCFT.setParentId(oParent.getCashFlowTypeId());
                        bValid = validateString("Parent Code", oCFT.getParentId(), oEmpty);                    
                    }                    
                    
                    oCFT.setCashFlowLevel(CashFlowTypeTool.setChildLevel(oCFT.getParentId()));

                    String sAltCode = getString(_oRow, _iIdx + i); i++;
                    oCFT.setAltCode(sAltCode);
                    
                    if(bValid && StringUtil.isEqual(oEmpty.toString(),s_EMPTY_REJECT) 
                              && StringUtil.isEqual(oLength.toString(),s_LENGTH_REJECT))
                    {
                        if (bNew) 
                        {   
                            m_iNewData++;
                            m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));                                      
                            m_oNewData.append (StringUtil.left(sCFTCode,30));
                            m_oNewData.append (StringUtil.left(sCFTName,100));
                            m_oNewData.append ("\n");
                        }
                        else 
                        {
                            m_iUpdated++;
                            m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));                                      
                            m_oUpdated.append (StringUtil.left(sCFTCode,30));
                            m_oUpdated.append (StringUtil.left(sCFTName,100));
                            m_oUpdated.append ("\n");
                        }
                        
                        oCFT.save(oConn);
                        CashFlowTypeManager.getInstance().refreshCache(oCFT);
                        CashFlowTypeTool.updateParent(oParent,oConn);
                    }
                    else
                    {
                        m_iRejected++;
                        m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));                 
                        m_oRejected.append (StringUtil.left(sCFTCode,30));
                        m_oRejected.append (StringUtil.left(sCFTName,50));
                        if (oEmpty.length() > s_EMPTY_REJECT.length())
                        {                   
                            m_oRejected.append (oEmpty);
                        }
                        if (oLength.length() > s_LENGTH_REJECT.length())
                        {
                            m_oRejected.append ("; ");                  
                            m_oRejected.append (oLength);
                        }
                        m_oRejected.append ("\n");
                    }
                    BaseTool.commit(oConn);
                }
                catch (Exception e)
                {
                    BaseTool.rollback(oConn);
                    throw new NestableException(e);
                }
			}
		}    
	}
}
