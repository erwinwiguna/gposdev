package com.ssti.enterprise.pos.excel.helper;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerType;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class CustomerLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( CustomerLoader.class );
	
	public static final String s_START_HEADING = "Customer Code";
	
	public CustomerLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
    	m_bValidate = true;
    	m_aHeaders = CustomerViewHelper.a_HEADER;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {		
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCustomerCode = getString(_oRow, _iIdx + i); i++;
			String sCustomerName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCustomerCode))
			{				
				Customer oOld = null;
				Customer oCustomer = CustomerTool.getCustomerByCode(sCustomerCode);
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Customer Code : " + sCustomerCode);
				
				if (oCustomer == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					oCustomer = new Customer();
					oCustomer.setCustomerId (IDGenerator.generateSysID());
					oCustomer.setCustomerCode(sCustomerCode); 	
					oCustomer.setAddDate(new Date());
					oCustomer.setCreateBy(m_sUserName);
					bNew = true;
				}
				else 
				{
					bNew = false;
					//if customer name previously empty (using pre-registered card)
					if(StringUtil.isEmpty(oCustomer.getCustomerName()) && StringUtil.isNotEmpty(sCustomerName))
					{						
						oCustomer.setAddDate(new Date()); //set as registration date 	
					}
					oOld = oCustomer.copy();
				}
				oCustomer.setCustomerName(sCustomerName); 
				bValid = validateString("Customer Name", oCustomer.getCustomerName(), 100, oEmpty, oLength);

				oCustomer.setTaxNo (getString(_oRow, _iIdx + i)); i++;
				
                String sCustomerTypeCode = getString(_oRow, _iIdx + i); i++;
                CustomerType oCustomerType = CustomerTypeTool.getCustomerTypeByCode(sCustomerTypeCode);
                String sCustomerTypeID = "";
                if (oCustomerType != null) sCustomerTypeID = oCustomerType.getCustomerTypeId();
				oCustomer.setCustomerTypeId   (sCustomerTypeID); 
				bValid = validateString("Customer Type  '" + sCustomerTypeCode + "'", oCustomer.getCustomerTypeId(),  oEmpty);
				    
				oCustomer.setContactName1     (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setContactPhone1    (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setJobTitle1        (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setContactName2     (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setContactPhone2    (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setJobTitle2        (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setContactName3     (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setContactPhone3    (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setJobTitle3        (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setAddress          (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setZip              (getString(_oRow, _iIdx + i)); i++;
				oCustomer.setCountry          (getString(_oRow, _iIdx + i)); i++;

				AddressTool.setProvinceStr(oCustomer, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setRegencyStr (oCustomer, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setDistrictStr(oCustomer, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setVillageStr (oCustomer, getString(_oRow, _iIdx + i)); i++;
				
				oCustomer.setPhone1           (getString(_oRow, _iIdx + i)); i++;
                oCustomer.setPhone2           (getString(_oRow, _iIdx + i)); i++;
                oCustomer.setFax              (getString(_oRow, _iIdx + i)); i++;
                oCustomer.setEmail            (getString(_oRow, _iIdx + i)); i++;
                oCustomer.setWebSite          (getString(_oRow, _iIdx + i)); i++;
                
                //tax
                String sTaxCode = getString(_oRow, _iIdx + i); i++;
                oCustomer.setDefaultTaxId (TaxTool.getIDByCode(sTaxCode));
                
                //payment type
                String sPaymentType = getString(_oRow, _iIdx + i); i++;
                PaymentType oType = PaymentTypeTool.getPaymentTypeByCode(sPaymentType);
                String sTypeID = "";
                if (oType != null) sTypeID = oType.getPaymentTypeId();
                oCustomer.setDefaultTypeId(sTypeID);

                String sPaymentTerm = getString(_oRow, _iIdx + i); i++;
                PaymentTerm oTerm = PaymentTermTool.getPaymentTermByCode(sPaymentTerm);
                String sTermID = "";
                if (oTerm != null) sTermID = oTerm.getPaymentTermId();
                oCustomer.setDefaultTermId(sTermID);

                String sEmployeeCode = getString(_oRow, _iIdx + i); i++;
                Employee oEmp = EmployeeTool.getEmployeeByCode(sEmployeeCode, null);				
                String sEmpID = "";
                if (oEmp != null) sEmpID = oEmp.getEmployeeId();
                oCustomer.setDefaultEmployeeId(sEmpID);
                
                String sCode = getString(_oRow, _iIdx + i); i++;
                Currency oCurr = CurrencyTool.getCurrencyByCode(sCode);
				String sCurrID = "";
                if (oCurr != null) sCurrID = oCurr.getCurrencyId();
                oCustomer.setDefaultCurrencyId(sCurrID);
                
                oCustomer.setDefaultDiscountAmount ( getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setCreditLimit ( getBigDecimal(_oRow, _iIdx + i) ); i++;
                oCustomer.setMemo ( getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setInvoiceMessage ( getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setField1 ( getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setValue1 ( getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setField2 ( getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setValue2 ( getString(_oRow, _iIdx + i) ); i++;
                
                String sAR= getString(_oRow, _iIdx + i); i++;
                Account oAcc = AccountTool.getAccountByCode(sAR, false);
				String sAccID = "";
                if (oAcc != null) sAccID = oAcc.getAccountId();
                oCustomer.setArAccount(sAccID);
                
                oCustomer.setDefaultLocationId(LocationTool.getIDByCode(getString(_oRow, _iIdx + i))); i++;              
                
                //CRM Fields
                String sDate = getString(_oRow, _iIdx + i); i++;
                if (StringUtil.isNotEmpty(sDate))
                {
                	oCustomer.setBirthDate(CustomParser.parseDate(sDate));
                }
                oCustomer.setBirthPlace (getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setReligion (getString(_oRow, _iIdx + i) ); i++;
                oCustomer.setGender (CustomerTool.getGender(getString(_oRow, _iIdx + i))); i++;
                oCustomer.setOccupation(getString(_oRow, _iIdx + i)); i++;
                                
                if (PreferenceTool.useCustomerTrPrefix())
                {
                    oCustomer.setTransPrefix(getString(_oRow, _iIdx + i)); i++;
                }
                
                oCustomer.setLastUpdateLocationId(PreferenceTool.getLocationID());
                oCustomer.setLastUpdateBy(m_sUserName);
                oCustomer.setUpdateDate(new Date());
                oCustomer.save();
                
                if(oOld != null)
                {
                	UpdateHistoryTool.createHistory(oOld, oCustomer, m_sUserName, null);
                }
                CustomerManager.getInstance().refreshCache(oCustomer);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sCustomerCode,30));
						m_oNewData.append (StringUtil.left(sCustomerName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sCustomerCode,30));
						m_oUpdated.append (StringUtil.left(sCustomerName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCustomerCode,30));
				    m_oRejected.append (StringUtil.left(sCustomerName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    
    }
}
