package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.PointTransaction;
import com.ssti.enterprise.pos.om.PointType;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;


/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 
 * </pre><br>
 */
public class CustomerPointLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( CustomerPointLoader.class );
	
	public static final String s_START_HEADING = "Customer Code";
	    
	public CustomerPointLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
		
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {				
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);

		if (started (_oRow, _iIdx))	
		{
			String sCode = getString(_oRow, _iIdx);			
			if (StringUtil.isNotEmpty(sCode))
			{				
				short i = 1;
				String sName = getString(_oRow, _iIdx + i); i++;
				BigDecimal bdPoint = getBigDecimal(_oRow, _iIdx + i); i++;				
				String sTxCode = getString(_oRow, _iIdx + i); i++;
				String sDesc = getString(_oRow, _iIdx + i); i++;
				String sDate = getString(_oRow, _iIdx + i); i++;
				
				boolean bValid = true;
				Customer oCust = CustomerTool.getCustomerByCode(sCode);			    
				if(oCust == null) {bValid = false; m_oRejected.append("Customer ")
															  .append(sCode).append(" ")
															  .append(sName).append(" Not Found\n");}				
				
				log.debug ("Column " + _iIdx + " Customer Code : " + sCode);
												
                if(bValid)
				{
                	try
                	{
                		createTrans(oCust, bdPoint, sTxCode, sDesc, sDate);
                	}
                	catch (Exception _oEx)
                	{
                		m_iError++;
                    	m_oError.append (StringUtil.left(m_iError + ". ", 5));										
                    	m_oError.append (StringUtil.left(sCode,20));
                    	m_oError.append (StringUtil.left(sName,30));
                    	m_oError.append (StringUtil.left(CustomFormatter.formatAccounting(bdPoint),20));                	
                    	m_oError.append (_oEx.getMessage());                	                	
                    	m_oError.append ("\n");
                    	
                    	if (log.isDebugEnabled())
                    	{
                    		_oEx.printStackTrace();
                    	}
                	}
				}
                else
                {

				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCode,30));
				    m_oRejected.append (StringUtil.left(sName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                    
                }
			}
		}    
    }

	private void createTrans(Customer oCust, 
							 BigDecimal bdPoint, 
							 String _sTxCode, 
							 String _sDesc, 
							 String _sDate) 
		throws Exception 
	{
		Date dTrans = PreferenceTool.getStartDate();
		if(StringUtil.isNotEmpty(_sDate) && CustomParser.parseDate(_sDate) != null) 
		{			
			dTrans = CustomParser.parseDate(_sDate);
		}
		if(StringUtil.isEmpty(_sDesc)) _sDesc = "Point Opening Balance";
		if(StringUtil.isEmpty(_sTxCode)) _sTxCode = "OB";
				
		String sTxID = "";
		List v = PointRewardTool.getAllPointType();
		Iterator iter = v.iterator();		
		while(iter.hasNext())
		{
			PointType oPT = (PointType) iter.next();
			if(StringUtil.equalsIgnoreCase(oPT.getCode(), _sTxCode))
			{
				sTxID = oPT.getId();
			}
		}		

		PointTransaction oTR = new PointTransaction();
		oTR.setPointTypeId(sTxID);
		oTR.setCustomerId(oCust.getCustomerId());
		oTR.setLocationId(PreferenceTool.getLocationID());
		oTR.setTransactionDate(dTrans);
		oTR.setPointChanges(bdPoint);
		oTR.setDescription(_sDesc);
		oTR.setCreateDate(new Date());	    
		oTR.setUserName(m_sUserName);
		
		PointRewardTool.saveData(oTR, null);		
	}
}
