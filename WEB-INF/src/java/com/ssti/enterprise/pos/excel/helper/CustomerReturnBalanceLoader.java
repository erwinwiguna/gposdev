package com.ssti.enterprise.pos.excel.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.framework.tools.StringUtil;

public class CustomerReturnBalanceLoader extends ARPBalanceLoader
{    
	private static Log log = LogFactory.getLog (CustomerReturnBalanceLoader.class);
	
	public static final String s_START_HEADING = "Customer Code";
	
	public CustomerReturnBalanceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
		throws Exception
	{				
		if (started (_oRow, _iIdx))	
		{
			setParam(_oRow, _iIdx);
			if (StringUtil.isNotEmpty(sCode))
			{				
				setObject();
				
				boolean bValid = true;				
				log.debug ("Column " + _iIdx + " Customer Code : " + sCode);
				
				if (oCust == null) {bValid = false; oReject.append("Customer ").append(sCode).append(" Not Found\n");}
				bValid = validate(bValid);
				
	            if(bValid)
				{
	            	try
	            	{
	            		createTrans();
	            		logSuccess();
	            	}
	            	catch (Exception _oEx)
	            	{
	            		logError(_oEx);
	            	}
				}
	            else
	            {
	            	logReject();
	            }
			}
		}    
	}

	private void createTrans() throws Exception 
	{
		SalesReturn oTR = new SalesReturn();
		
		oTR.setReturnNo(sInvNo);
		oTR.setCustomerId(oCust.getCustomerId());
		oTR.setCustomerName(oCust.getCustomerName());
		oTR.setLocationId(oLoc.getLocationId());
		oTR.setReturnDate(dAsOf);
		oTR.setTransactionDate(dAsOf);
		oTR.setSalesId(sSalesID);
		oTR.setUserName(sCreateBy);
		oTR.setPaymentTypeId(sTypeID);
		//oTR.setPaymentTermId(sTermID);
		oTR.setTotalDiscountPct("0");
		oTR.setReturnedAmount(bdAmt);
		//oTR.setChangeAmount(bd_ZERO);
		oTR.setIsInclusiveTax(false);
		oTR.setIsTaxable(false);
		oTR.setCashReturn(false);
		oTR.setCreateCreditMemo(true);
		oTR.setTransactionId("");
		oTR.setTransactionNo("");
		oTR.setTransactionType(i_RET_FROM_NONE);		
    	oTR.setStatus(i_PROCESSED);
    	oTR.setRemark(sRemark + "\nImported Return Opening Balance Trans : " + sInvNo + " by " + m_sUserName);
    	    	
    	//updated by processPayment
    	oTR.setDueDate(oTR.getTransactionDate());
    	
	    oTR.setCurrencyId(oCurr.getCurrencyId());
	    oTR.setCurrencyRate(bdRate);
	    oTR.setFiscalRate(bdFisc);
	    
	    SalesReturnDetail oTD = new SalesReturnDetail();
	    
	    oTD.setItemId(oItem.getItemId());
	    oTD.setItemCode(oItem.getItemCode());
	    oTD.setItemName(oItem.getItemName());
	    oTD.setQty(bd_ONE);
	    oTD.setQtyBase(bd_ONE);
	    oTD.setUnitId(oItem.getUnitId());
	    oTD.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));
	    oTD.setItemPrice(bdAmt);
	    oTD.setReturnAmount(bdAmt);
	    oTD.setTaxId(oTax.getTaxId());
	    oTD.setTaxAmount(oTax.getAmount());
		oTD.setDiscountId("");
		oTD.setDiscount("0");
		oTD.setProjectId("");
		oTD.setDepartmentId("");
		//OTHER FIELD
		oTD.setTransactionDetailId("");
		oTD.setSubTotalDisc(bd_ZERO);
		oTD.setSubTotalTax(bd_ZERO);
		oTD.setItemCost(bd_ZERO);
		oTD.setSubTotalCost(bd_ZERO);

		List vTD = new ArrayList(1);
		List vPMT = new ArrayList(1);
		vTD.add(oTD);

		SalesReturnTool.setHeaderProperties(oTR, vTD, null);
		
		if (dDue != null)
		{
			oTR.setDueDate(dDue);
		}
		
		SalesReturnTool.saveData(oTR, vTD, null);
	}
}
