package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.financial.CustomerBalanceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.CustomFormatter;

public class CustomerViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{                 
    public static String[] a_HEADER = {
        "Customer Code"   ,
        "Customer Name"   , 
        "Tax No"          , 
        "Customer Type"   , 
        "Contact Name1"   , 
        "Contact Phone1"  , 
        "Job Title1"      , 
        "Contact Name2"   , 
        "Contact Phone2"  , 
        "Job Title2"      , 
        "Contact Name3"   , 
        "Contact Phone3"  , 
        "Job Title3"      , 
        "Address"         , 
        "Zip"             , 
        "Country"         , 
        "Province"        , 
        "City"            , 
        "District"        , 
        "Village"         , 
        "Phone1"          , 
        "Phone2"          , 
        "Fax"             , 
        "Email"           , 
        "Web Site"        , 
        "Default Tax"     , 
        "Payment Type"    , 
        "Payment Term"    , 
        "Related Employee", 
        "Currency"        , 
        "Default Discount", 
        "Credit Limit"    , 
        "Memo"            , 
        "Invoice Message" , 
        "Field1"          , 
        "Value1"          , 
        "Field2"          , 
        "Value2"          , 
        "AR Account"      , 
        "Location"             
    };
    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = (List) _oSession.getAttribute("Customers");
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Customer View Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        for (int i = 0; i < a_HEADER.length; i++)
        {
        	createHeaderCell(oWB, oRow, (short)iCol, a_HEADER[i]); iCol++;        	        
        }             

		createHeaderCell(oWB, oRow, (short)iCol, "Birth Date"    ); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Birth Place"   ); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Religion"      ); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Gender"        ); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Occupation"    ); iCol++;		
           
		if (PreferenceTool.useCustomerTrPrefix())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Trans Prefix"); iCol++;	
		}
		createHeaderCell(oWB, oRow, (short)iCol, "Current Balance"); iCol++;		
     
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Customer oCust = (Customer) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oCust.getCustomerCode   ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getCustomerName   ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getTaxNo          ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, CustomerTypeTool.getCustomerTypeCodeByID(oCust.getCustomerTypeId())); iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getContactName1   ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getContactPhone1  ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getJobTitle1   	());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getContactName2   ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getContactPhone2  ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getJobTitle2      ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getContactName3   ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getContactPhone3  ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getJobTitle3      ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getAddress        ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getZip       	    ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getCountry   	    ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, AddressTool.getProvStr(oCust.getProvince())); iCol++;
                createCell(oWB, oRow2, (short)iCol, AddressTool.getCityStr(oCust.getCity())); iCol++;
                createCell(oWB, oRow2, (short)iCol, AddressTool.getDistrictStr(oCust.getDistrict())); iCol++;
                createCell(oWB, oRow2, (short)iCol, AddressTool.getVillageStr(oCust.getVillage())); iCol++;                
                createCell(oWB, oRow2, (short)iCol, oCust.getPhone1   	    ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getPhone2   	    ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getFax   		    ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getEmail    	    ());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getWebSite		());          iCol++; 
                createCell(oWB, oRow2, (short)iCol, TaxTool.getCodeByID(oCust.getDefaultTaxId()));                      iCol++; 
                createCell(oWB, oRow2, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oCust.getDefaultTypeId()));  iCol++; 
                createCell(oWB, oRow2, (short)iCol, PaymentTermTool.getPaymentTermCodeByID(oCust.getDefaultTermId()));  iCol++; 
                createCell(oWB, oRow2, (short)iCol, EmployeeTool.getCodeByID(oCust.getDefaultEmployeeId()));    		iCol++; 
                createCell(oWB, oRow2, (short)iCol, CurrencyTool.getCodeByID(oCust.getDefaultCurrencyId()));            iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getDefaultDiscountAmount());    								iCol++; 
                createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oCust.getCreditLimit()));              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getMemo());                     								iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getInvoiceMessage());           								iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCust.getField1());                   								iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oCust.getValue1());                   								iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oCust.getField2());                   								iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oCust.getValue2());                   								iCol++;                 
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oCust.getArAccount()));                   	iCol++;    
                createCell(oWB, oRow2, (short)iCol, LocationTool.getCodeByID(oCust.getDefaultLocationId()));            iCol++;
            
                createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oCust.getBirthDate()));					iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oCust.getBirthPlace());                   							iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oCust.getReligion());                   							iCol++;                 
                createCell(oWB, oRow2, (short)iCol, CustomerTool.getGender(oCust.getGender()));                   		iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oCust.getOccupation());                   							iCol++;

                if (PreferenceTool.useCustomerTrPrefix())
        		{
                    createCell(oWB, oRow2, (short)iCol, oCust.getTransPrefix());                   					    iCol++;        
        		}
                createCell(oWB, oRow2, (short)iCol, CustomerBalanceTool.getBalance(oCust.getCustomerId()).toString());	iCol++;                 
            }        
        }
        return oWB;
    }
}
