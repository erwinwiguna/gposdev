package com.ssti.enterprise.pos.excel.helper;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.EmployeeManager;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.TurbineRole;
import com.ssti.enterprise.pos.om.TurbineRolePeer;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-03-03
 * - change method updateList call UpdateHistoryTool.createHistory 
 * </pre><br>
 */
public class EmployeeLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (EmployeeLoader.class );
	
	public static final String s_START_HEADING = "Employee Code";
	
	public EmployeeLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
		
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sBossCode = "";
			String sBossName = "";

			String sEmpCode = getString(_oRow, _iIdx + i); i++;
			String sEmpName = getString(_oRow, _iIdx + i); i++;			
			String sUsername = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sEmpCode) && 
				StringUtil.isNotEmpty(sEmpName))
			{				

				Employee oEmp = EmployeeTool.getEmployeeByCode(sEmpCode,null);
				Employee oOld = null;
				if (oEmp == null)
				{
					oEmp = EmployeeTool.getEmployeeByName(sEmpName);
				}
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Employee Name : " + sEmpName);
				
				if (oEmp == null) 
				{	
					log.debug ("Name Not Exist, create new Code");
					
					oEmp = new Employee();
					oEmp.setEmployeeId (IDGenerator.generateSysID());
					oEmp.setEmployeeCode(sEmpCode);
					oEmp.setEmployeeName(sEmpName); 	
					bNew = true;
				}
				else 
				{
					oOld = oEmp.copy();
					bNew = false;
					oEmp.setEmployeeName(sEmpName); 
				}
				if(StringUtil.isNotEmpty(sUsername))
				{
					oEmp.setUserName(sUsername); 
					bValid = validateString("User Name", oEmp.getUserName(), 30, oEmpty, oLength);
				}
				else
				{
					oEmp.setUserName("");
				}
    			oEmp.setParentId("");
    			if(StringUtil.isNotEmpty(sBossCode))  
    			{
    				oEmp.setParentId(EmployeeTool.getIDByCode(sBossCode));
    			}
				
				int iRole = 0;
				String sRoleName = getString(_oRow, _iIdx + i); i++;
				Criteria oCrit = new Criteria();
				oCrit.add(TurbineRolePeer.ROLE_NAME, sRoleName);
				List vRole = TurbineRolePeer.doSelect(oCrit);
				if (vRole.size() > 0)
				{
					TurbineRole oRole = (TurbineRole) vRole.get(0);
					iRole = oRole.getRoleId();
				}
				oEmp.setRoleId(iRole);
				
				oEmp.setJobTitle  (getString(_oRow, _iIdx + i)); i++;
				oEmp.setIsSalesman(Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;								
				
				oEmp.setGender    (EmployeeTool.getGenderCode(getString(_oRow, _iIdx + i))); i++;
				oEmp.setBirthPlace(getString(_oRow, _iIdx + i)); i++;
				oEmp.setBirthDate (CustomParser.parseDate(getString(_oRow, _iIdx + i))); i++;
				oEmp.setStartDate (CustomParser.parseDate(getString(_oRow, _iIdx + i))); i++;
				oEmp.setQuitDate  (CustomParser.parseDate(getString(_oRow, _iIdx + i))); i++;
				oEmp.setLocationId(LocationTool.getIDByCode(getString(_oRow, _iIdx + i))); i++;
								
				oEmp.setAddress     (getString(_oRow, _iIdx + i)); i++;
				oEmp.setZip         (getString(_oRow, _iIdx + i)); i++;
				oEmp.setCountry     (getString(_oRow, _iIdx + i)); i++;

				AddressTool.setProvinceStr(oEmp, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setRegencyStr (oEmp, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setDistrictStr(oEmp, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setVillageStr (oEmp, getString(_oRow, _iIdx + i)); i++;
				
				oEmp.setHomePhone   (getString(_oRow, _iIdx + i)); i++;
                oEmp.setMobilePhone (getString(_oRow, _iIdx + i)); i++;
                oEmp.setEmail       (getString(_oRow, _iIdx + i)); i++;
                oEmp.setMemo        (getString(_oRow, _iIdx + i)); i++;

                oEmp.setField1 (getString(_oRow, _iIdx + i) ); i++;
                oEmp.setValue1 (getString(_oRow, _iIdx + i) ); i++;
                oEmp.setField2 (getString(_oRow, _iIdx + i) ); i++;                
                oEmp.setValue2 (getString(_oRow, _iIdx + i) ); i++;

    			String sDeptCode = getString(_oRow, _iIdx + i); i++;
    			if(StringUtil.isNotEmpty(sDeptCode))
    			{
    				oEmp.setDepartmentId(sDeptCode);
    			}
    			
				oEmp.setEmployeeLevel(EmployeeTool.setChildLevel(oEmp.getParentId()));
                oEmp.setUpdateDate(new Date());
                oEmp.save();
                
                if(!bNew && oOld != null)
                {
                	UpdateHistoryTool.createHistory(oOld, oEmp, m_sUserName, null);
                }
                
                EmployeeTool.createUserFromEmployee(oEmp);
                EmployeeManager.getInstance().refreshCache(oEmp);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sEmpName,30));
						m_oNewData.append (StringUtil.left(sUsername,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sEmpName,30));
						m_oUpdated.append (StringUtil.left(sUsername,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sEmpName,30));
				    m_oRejected.append (StringUtil.left(sUsername,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}
