package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.TurbineRole;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

public class EmployeeViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        int iCond = -1;
        
        try 
        {
            iCond = Integer.valueOf(StringUtil.getString(oParam, "Condition"));			
		} 
        catch (Exception e) 
        {
		} 
        
		String sKeyword = StringUtil.getString(oParam, "Keywords");
		String sLocID = StringUtil.getString(oParam, "LocationId");
		
		List vData = EmployeeTool.findData (iCond, sKeyword, sLocID);
    	
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Employee View Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
        /*
        private String employeeName;
        private String userName;
        private String jobTitle;
        private int gender;
        private String birthPlace;
        private Date birthDate;
        private Date startDate;
        private Date quitDate;
        private String locationId;
        private String address;
        private String zip;
        private String city;
        private String province;
        private String country;
        private String homePhone;
        private String mobilePhone;
        private String email;
        private String memo;
        private int roleId;
        private String field1 = "";
        private String field2 = "";
        private String value1 = "";
        private String value2 = "";

        private Date addDate;
        private BigDecimal salesCommisionPct= bd_ZERO;
        private BigDecimal marginCommisionPct= bd_ZERO;
        private BigDecimal monthlySalary= bd_ZERO;
        private BigDecimal otherIncome= bd_ZERO;
        private int frequency = 0;
        */
        createHeaderCell(oWB, oRow, (short)iCol, "Employee Code"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Employee Name"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Username"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Role"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Job Title"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Is Salesman"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Gender"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Birth Place"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Birth Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Start Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Quit Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Address"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Zip"); iCol++;
		
		createHeaderCell(oWB, oRow, (short)iCol, "Country"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Province"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "City"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "District"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Village"); iCol++;		
		
		createHeaderCell(oWB, oRow, (short)iCol, "Home Phone"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Mobile Phone"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Email"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Memo"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Field1"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Value1"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Field2"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Value2"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Department"); iCol++;
		
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Employee oEmp = (Employee) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                String sRole = "";
                String sLoc = "";
                String sDep = "";
                try 
                {
                    
                	TurbineRole oRole = EmployeeTool.getRoleByID(oEmp.getRoleId());
                    if (oRole != null)
                    {
                    	sRole = oRole.getRoleName();
                    }
				} 
                catch (Exception e) 
                {
                    e.printStackTrace();
				}
                
                if (StringUtil.isNotEmpty(oEmp.getLocationId()))
                {
                    sLoc = LocationTool.getCodeByID(oEmp.getLocationId());
                }
                if (StringUtil.isNotEmpty(oEmp.getDepartmentId()))
                {
                	sDep = oEmp.getDepartmentId();
                }
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oEmp.getEmployeeCode()); iCol++;        		
        		createCell(oWB, oRow2, (short)iCol, oEmp.getEmployeeName()); iCol++;        		
        		createCell(oWB, oRow2, (short)iCol, oEmp.getUserName()    ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, sRole			      ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getJobTitle()    ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getIsSalesman()  ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, EmployeeTool.getGender(oEmp.getGender())); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getBirthPlace()  ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oEmp.getBirthDate())); iCol++;
        		createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oEmp.getStartDate())); iCol++;
        		createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oEmp.getQuitDate() )); iCol++;
        		createCell(oWB, oRow2, (short)iCol, sLoc   				  ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getAddress()     ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getZip()         ); iCol++;

        		createCell(oWB, oRow2, (short)iCol, oEmp.getCountry()     ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, AddressTool.getProvStr(oEmp.getProvince())); iCol++;
        		createCell(oWB, oRow2, (short)iCol, AddressTool.getCityStr(oEmp.getCity())); iCol++;
        		createCell(oWB, oRow2, (short)iCol, AddressTool.getDistrictStr(oEmp.getDistrict())); iCol++;
        		createCell(oWB, oRow2, (short)iCol, AddressTool.getVillageStr(oEmp.getVillage())); iCol++;
        		
        		createCell(oWB, oRow2, (short)iCol, oEmp.getHomePhone()   ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getMobilePhone() ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getEmail()       ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getMemo()        ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getField1()      ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getValue1()      ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getField2()      ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, oEmp.getValue2()      ); iCol++;
        		createCell(oWB, oRow2, (short)iCol, sDep				  ); iCol++;        		        		
            }        
        }
        return oWB;
    }
}
