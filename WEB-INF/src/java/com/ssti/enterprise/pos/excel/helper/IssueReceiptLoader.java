package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.BatchTransaction;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/IssueReceiptLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: IssueReceiptLoader.java,v 1.12 2008/08/17 02:17:21 albert Exp $
 *
 * $Log: IssueReceiptLoader.java,v $
 * Revision 1.12  2008/08/17 02:17:21  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/06/30 13:31:56  albert
 * *** empty log message ***
 *
 * Revision 1.10  2006/01/21 12:08:41  albert
 * *** empty log message ***
 *
 */
public class IssueReceiptLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (IssueReceiptLoader.class);
    private String sLocationID;
    private boolean bForceZero = false;
    
    public IssueReceiptLoader(String _sLocationID)
        throws Exception
    
    {
    	sLocationID = _sLocationID;
    }

    public IssueReceiptLoader(String _sLocationID, boolean _bForceZero)
        throws Exception    
    {
        sLocationID = _sLocationID;
        bForceZero = _bForceZero;
    }

    protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		IssueReceiptDetail oIRD = new IssueReceiptDetail();
		String sCode = getString(_oRow,_iIdx + i); i++;
		String sName = getString(_oRow,_iIdx + i);		
		double dQtyChg = getBigDecimal(_oRow,_iIdx + 5).doubleValue();
        
		Item oItem = ItemTool.getItemByCode(sCode);        
        if(oItem != null)
        {
            oIRD.setItemId(oItem.getItemId());
            oIRD.setItemCode(oItem.getItemCode());

            String sUnitID = oItem.getUnitId();
        	Unit oUnit = UnitTool.getUnitByID(sUnitID);
        	boolean bBase = true;
        	
            if(oUnit != null)
            {            
            	if (StringUtil.isEqual(oUnit.getUnitId(),oItem.getPurchaseUnitId()))
            	{
            		bBase = false;
            		oUnit = UnitTool.getUnitByID(oItem.getUnitId());
            		dQtyChg = dQtyChg * oItem.getUnitConversion().doubleValue();
            	}            	
                oIRD.setUnitId(oUnit.getUnitId());
                oIRD.setUnitCode(oUnit.getUnitCode());
            }
            else
            {
            	setError (iRow, sCode, sName, "Item contains invalid Unit ");
            }    
            oIRD.setQtyChanges(new BigDecimal(dQtyChg));
            
            double dCost = getBigDecimal(_oRow,_iIdx + 8).doubleValue();
            if(dCost > 0 || bForceZero)
            {
                dCost = getBigDecimal(_oRow,_iIdx + 8).doubleValue();
            }            
            else //try to get cost from inv loc
            {
                InventoryLocation oInvLoc = 
                	InventoryLocationTool.getDataByItemAndLocationID(oItem.getItemId(), sLocationID);
                
                if(oInvLoc != null)
                {
                    dCost = oInvLoc.getItemCost().doubleValue();
                }
                else
                {
                	if (oItem.getLastPurchasePrice() != null && oItem.getLastPurchaseRate() != null)
                	{
                		dCost = oItem.getLastPurchasePrice().doubleValue() * oItem.getLastPurchaseRate().doubleValue();
                	}
                }
            }
            oIRD.setCost(new BigDecimal(dCost));
            String sBatchNumber = getString(_oRow, _iIdx + 9);
            if (StringUtil.isNotEmpty(sBatchNumber))
            {
            	String sExp = getString(_oRow, _iIdx + 10);
            	Date dExp = CustomParser.parseDate(sExp);
            	
            	BatchTransaction oBT = BatchTransactionTool.newBatchTrans();
            	oBT.setBatchNo(sBatchNumber);
            	oBT.setExpiredDate(dExp);
            	oBT.setDescription("");
            	oIRD.setBatchTransaction(oBT);
            }            
            m_vDataList.add(oIRD);
        }
        else
        {
        	setError (iRow, sCode, sName, "Item Not Found");
        }
    }        
    
    void setError (int _iRow, String _sCode, String _sName, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append (_iRow + 1);
		oMsg.append (",ITEM CODE, " + _sCode );
		oMsg.append (",ITEM NAME, " + _sName );
		oMsg.append (_sMsg);
		oMsg.append ("\n");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}
