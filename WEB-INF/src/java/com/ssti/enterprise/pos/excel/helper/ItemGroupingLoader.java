package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemGroupingLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemGroupingLoader.java,v 1.1 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemGroupingLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:29  albert
 * *** empty log message ***
 *
 * Revision 1.22  2008/06/29 07:02:31  albert
 * *** empty log message ***
 *
 *
 */
public class ItemGroupingLoader extends BaseExcelLoader implements AppAttributes
{    
	private static Log log = LogFactory.getLog (ItemGroupingLoader.class);
	
	public static final String s_START_HEADING = "Group Code";
	
	public ItemGroupingLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);		
		
		if(started (_oRow, _iIdx))	
		{
			short i = 0;			
			String sGroup = getString(_oRow, _iIdx + i); i++;
			String sPart = getString(_oRow, _iIdx + i); i++;
			String sName = getString(_oRow, _iIdx + i); i++;
			BigDecimal dQty = getBigDecimal(_oRow, _iIdx + i); i++;
			String sUnit = getString(_oRow, _iIdx + i); i++;

			boolean bNew = true;
			boolean bValid = true;
			if (StringUtil.isNotEmpty(sGroup) && StringUtil.isNotEmpty(sPart))
			{				
				Item oGroup = ItemTool.getItemByCode(sGroup);
				Item oPart = ItemTool.getItemByCode(sPart);
				
				log.debug (" Group Code : " + sGroup + " Part Code: " + sPart + " Part Name: " + sName + " Qty: " + dQty + " " + sUnit);
				
				if (oGroup != null && oPart != null) 
				{	
					String sGroupID = oGroup.getItemId();
					String sPartID = oPart.getItemId();
					
					ItemGroup oItemGroup = ItemGroupTool.getItemGroup(sGroupID, sPartID, null);
					if (oItemGroup == null)
					{
						log.debug ("Group Not Exist, create new");
						bNew = true;
						oItemGroup = new ItemGroup();
						oItemGroup.setNew(true);
						oItemGroup.setItemGroupId(IDGenerator.generateSysID());
						
					}
					else
					{
						bNew = false;
					}
					oItemGroup.setGroupId(sGroupID);
					oItemGroup.setItemId(sPartID);
					oItemGroup.setItemCode(oPart.getItemCode());
					oItemGroup.setItemName(oPart.getItemName());
					oItemGroup.setQty(dQty);
					oItemGroup.setCost(bd_ZERO);
					
					Unit oUnit = UnitTool.getUnitByCode(sUnit);
					if (oUnit != null)
					{
						oItemGroup.setModified(true);
						oItemGroup.setUnitId(oUnit.getUnitId());
						oItemGroup.setUnitCode(oUnit.getUnitCode());
						oItemGroup.save();
						ItemManager.getInstance().refreshGroupCache(sGroupID);						
					}
					else
					{
						throw new Exception("Invalid Unit Code " + sUnit);
					}
				}
				else 
				{
					if (oGroup == null) throw new Exception("Invalid Group Code " + sGroup);
					if (oPart == null) throw new Exception("Invalid Part Code " + sPart);					
				}				            	
			}
            if(bValid)
			{
				if (bNew) 
				{	
					m_iNewData++;
					m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
					m_oNewData.append (StringUtil.left(sGroup,30));
					m_oNewData.append (StringUtil.left(sPart,30));
					m_oNewData.append (StringUtil.left(sName,80));					
					m_oNewData.append ("\n");
				}
				else 
				{
					m_iUpdated++;
					m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
					m_oUpdated.append (StringUtil.left(sGroup,30));
					m_oUpdated.append (StringUtil.left(sPart,30));
					m_oUpdated.append (StringUtil.left(sName,80));
					m_oUpdated.append ("\n");
				}
			}
            else
            {
			    m_iRejected++;
			    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
			    m_oRejected.append (StringUtil.left(sGroup,30));
			    m_oRejected.append (StringUtil.left(sPart,30));
			    m_oRejected.append (StringUtil.left(sName,80));			    
			    m_oRejected.append ("\n");
            }			
		}    		
	}
}
