package com.ssti.enterprise.pos.excel.helper;

import java.io.InputStream;
import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemGroupingLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemGroupingLoader.java,v 1.1 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemGroupingLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:29  albert
 * *** empty log message ***
 *
 * Revision 1.22  2008/06/29 07:02:31  albert
 * *** empty log message ***
 *
 *
 */
public class ItemGroupingSheetLoader extends BaseExcelLoader implements AppAttributes
{    
	private static Log log = LogFactory.getLog ( ItemGroupingSheetLoader.class );

	private static final String s_START_HEADING = "Item Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	
	public ItemGroupingSheetLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	@Override
    public void loadData (InputStream _oInput)	
		throws Exception
	{
		POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
		HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
		int iSheets = oWB.getNumberOfSheets();
		
		for (int i = 0; i < iSheets; i++)
		{
			HSSFSheet oSheet = oWB.getSheetAt(i);
			String sGroupCode = oWB.getSheetName(i);
			m_iTotalRows = oSheet.getPhysicalNumberOfRows();			
			try 
			{
				createGroup(oSheet, sGroupCode);
				m_iUpdated++;
			    m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 5));					
			    m_oUpdated.append (StringUtil.left(sGroupCode,15));
			    m_oUpdated.append ("\n");
			} 
			catch (Exception _oEx) 
			{
				log.error(_oEx);
				_oEx.printStackTrace();
				
				m_iError++;
				m_oError.append (m_iError);					
				m_oError.append (". ERROR Loading Sheet : ").append (i);
				m_oError.append (" Group Code ").append (sGroupCode);
				m_oError.append ("\n   " + _oEx.getMessage());
				m_oError.append ("\n");			
			}
		}
	}
	
	/**
	 * 
	 * @param _oSheet
	 * @param _sGroupCode
	 * @throws Exception
	 */
    private void createGroup(HSSFSheet _oSheet, String _sGroupCode)
    	throws Exception
    {
		Item oGroupItem = ItemTool.getItemByCode(_sGroupCode);
		if (oGroupItem != null && oGroupItem.getItemType() == i_GROUPING)
		{
			String sGroupID = oGroupItem.getItemId();
			for (int iRow = 0; iRow < m_iTotalRows; iRow++)
			{
				int i = 0;
				HSSFRow oRow = _oSheet.getRow(iRow);
				if (oRow != null)
				{					
					String sItemCode = getString(oRow, i); i++;
					if (StringUtil.isNotEmpty(sItemCode))
					{
						String sItemName = getString(oRow, i); i++;
						BigDecimal dQty = getBigDecimal(oRow, i); i++;
						String sUnitCode = getString(oRow, i); i++;
						
						Item oItem = ItemTool.getItemByCode(sItemCode);					
						if (oItem != null && oItem.getItemType() != i_GROUPING)
						{							
							String sItemID = oItem.getItemId();
							ItemGroup oItemGroup = ItemGroupTool.getItemGroup(sGroupID, sItemID, null);
							if (oItemGroup == null)
							{
								oItemGroup = new ItemGroup();
								oItemGroup.setNew(true);
								oItemGroup.setItemGroupId(IDGenerator.generateSysID());
							}
							oItemGroup.setGroupId(sGroupID);
							oItemGroup.setItemId(sItemID);
							oItemGroup.setItemCode(oItem.getItemCode());
							oItemGroup.setItemName(oItem.getItemName());
							oItemGroup.setQty(dQty);
							oItemGroup.setCost(bd_ZERO);
							
							Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
							if (oUnit != null)
							{
								oItemGroup.setModified(true);
								oItemGroup.setUnitId(oUnit.getUnitId());
								oItemGroup.setUnitCode(oUnit.getUnitCode());
								oItemGroup.save();
								ItemManager.getInstance().refreshGroupCache(sItemID);
							}
							else
							{
								throw new Exception ("Item " + sItemCode + " of Group " + _sGroupCode + 
										" has invalid Unit " + sUnitCode);
							}
						}
						else
						{
							throw new Exception ("Item " + sItemCode + 
									" of Group " + _sGroupCode + " not found/has invalid type!");
						}
					}
				}//end if row != nill
			}//end for 
		}
		else
		{
			throw new Exception ("Item Grouping " + _sGroupCode + " not found/has invalid type!");
		}
    }

	@Override
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		//not used
	}
}
