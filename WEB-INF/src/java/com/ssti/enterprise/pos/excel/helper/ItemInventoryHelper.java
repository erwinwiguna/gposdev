package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.StringUtil;

public class ItemInventoryHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = (List) _oSession.getAttribute("Items");
        int iDays = Integer.valueOf(StringUtil.getString(oParam,"AvgSales"));
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Item Inventory");

        HSSFRow oRow = oSheet.createRow(0);
        int iRow = 0;
        createHeaderCell(oWB, oRow, (short)iRow, "Item Code");     iRow++;           
        createHeaderCell(oWB, oRow, (short)iRow, "Item Name");     iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Location");      iRow++;  
        createHeaderCell(oWB, oRow, (short)iRow, "Avg Sales(" + iDays + ")"); iRow++;                          
        createHeaderCell(oWB, oRow, (short)iRow, "Qty OH");        iRow++;                  
        createHeaderCell(oWB, oRow, (short)iRow, "Minimum Stock"); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Reorder Point"); iRow++;       
        createHeaderCell(oWB, oRow, (short)iRow, "Maximum Stock"); iRow++;      
        createHeaderCell(oWB, oRow, (short)iRow, "Rack Location"); iRow++;              
        createHeaderCell(oWB, oRow, (short)iRow, "Status ");       iRow++;             
        
        String sLocID = StringUtil.getString(oParam, "LocationId");
        if (StringUtil.isEmpty(sLocID)) sLocID = PreferenceTool.getLocationID();
        Location oLoc = LocationTool.getLocationByID(sLocID);
        
        if (vData != null && oLoc != null)
        {            
            for (int i = 0; i < vData.size(); i++)
            {
                Item oItem = (Item) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                InventoryLocation oInvLoc = 
                	InventoryLocationTool.getDataByItemAndLocationID (oItem.getItemId(), sLocID);

                BigDecimal dQty = bd_ZERO;
				BigDecimal dMin = oItem.getMinimumStock();
				BigDecimal dRP = oItem.getReorderPoint();
				BigDecimal dMax = oItem.getMaximumStock();
    		    String sRack = oItem.getRackLocation();
				boolean bActive = true;
				
                ItemInventory oItemInv = 
                	ItemInventoryTool.getItemInventory(oItem.getItemId(), sLocID);       
                
                if (oItemInv != null)
                {
                    dMin = oItemInv.getMinimumQty();
                	dRP = oItemInv.getReorderPoint();
                	dMax = oItemInv.getMaximumQty();	
                	sRack = oItemInv.getRackLocation();
                    bActive = oItemInv.getActive();
                }
		           		        
                if (oInvLoc != null)
                {
					dQty = oInvLoc.getCurrentQty();
                }

                double dAvgSales = oItem.getAvgSales(sLocID, iDays);                
                BigDecimal bdAvg = new BigDecimal(dAvgSales).setScale(2, BigDecimal.ROUND_HALF_UP);
                
                iRow = 0;
                createCell(oWB, oRow2, (short)iRow, oItem.getItemCode()); iRow++;
                createCell(oWB, oRow2, (short)iRow, oItem.getItemName()); iRow++;
                createCell(oWB, oRow2, (short)iRow, oLoc.getLocationCode()); iRow++;   
                createCell(oWB, oRow2, (short)iRow, bdAvg); iRow++;                
                createCell(oWB, oRow2, (short)iRow, dQty.toString()); iRow++;
                createCell(oWB, oRow2, (short)iRow, dMin.toString()); iRow++;  
                createCell(oWB, oRow2, (short)iRow, dRP.toString());  iRow++;  
                createCell(oWB, oRow2, (short)iRow, dMax.toString()); iRow++; 
                createCell(oWB, oRow2, (short)iRow, sRack); iRow++;
                createCell(oWB, oRow2, (short)iRow, Boolean.toString(bActive)); iRow++;                                
           }        
        }
        return oWB;
    }
}
