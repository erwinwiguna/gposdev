package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Brand;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Principal;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomFieldTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemHistoryTool;
import com.ssti.enterprise.pos.tools.ItemStatusCodeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemLoader.java,v 1.23 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemLoader.java,v $
 * 
 * 2018-08-08
 * - add field brand in EXCEL FILE, change process manufacturer & brand record from master if exists
 * 
 * 2017-10-02
 * - add conversion validation if p.Unit == s.Unit then conversion must be 1, vice versa.
 * 
 * 2017-08-25
 * - Trim ItemCode, Barcode & Item name
 * - Changes in validateString, Reject Item Name contains special char
 * 
 * 2016-08-01
 * - Add Tags Column
 *
 * 2016-05-01
 * - Add Consignment, Discountinue field
 */
public class ItemLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( ItemLoader.class );

	private static final String s_START_HEADING = "Item Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	
	public ItemLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
    	m_bValidate = true;
    	m_aHeaders = ItemUpdateHelper.a_HEADER;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		Item oOldItemRef = null;
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		StringBuilder oAcc = new StringBuilder(s_ACCOUNT_REJECT);
		
		if (started (_oRow, _iIdx))				
		{
			short i = 0;
			String sItemCode = StringUtil.trim(getString(_oRow, _iIdx + i)); i++;
			String sBarcode  = StringUtil.trim(getString(_oRow, _iIdx + i)); i++;
			String sItemName = StringUtil.trim(getString(_oRow, _iIdx + i)); i++;
			
			if (StringUtil.isNotEmpty(sItemCode))
			{
				boolean bNewItem = false;
				boolean bValid = true;
				
				Item oItem = ItemTool.getItemByCode(sItemCode);
				
				log.debug ("Column " + _iIdx + " Item Code : " + sItemCode);
								
				if (oItem == null) 
				{	
					log.debug ("Code Not Exist, Create New Code");

					bNewItem = true;
					oItem = new Item();
					oItem.setItemId (IDGenerator.generateSysID());
					oItem.setItemCode(sItemCode); 
					oItem.setAddDate(new Date()); 
					oItem.setCreateBy(m_sUserName); 
					oItem.setItemStatus(true);
				}
				else 
				{
					oOldItemRef = oItem.copy();
				}
				oItem.setBarcode     (sBarcode);			
				oItem.setItemName    (sItemName);
				oItem.setDescription (getString(_oRow, _iIdx + i)); i++;
				
				bValid = validateString("Barcode", oItem.getBarcode(), 100, oEmpty, oLength);
				bValid = validateString("Item Name", oItem.getItemName(), 255, oEmpty, oLength);
				bValid = validateString("Description", oItem.getDescription(), 1000, oEmpty, oLength);

				//SKU
				oItem.setItemSku     (getString(_oRow, _iIdx + i)); i++;
				oItem.setItemSkuName (getString(_oRow, _iIdx + i)); i++;
				
				bValid = validateString("SKU", oItem.getItemSku(), 100, oEmpty, oLength);
				bValid = validateString("SKU Name", oItem.getItemSkuName(), 255, oEmpty, oLength);
				
				//item type
				String sItemType = getString(_oRow, _iIdx + i); i++;
				oItem.setItemType(ItemTool.getItemType(sItemType));
            	
				log.debug("Column " + (_iIdx + i) + " price : " + getBigDecimal (_oRow, _iIdx + i));
				//price
				oItem.setItemPrice (getBigDecimal (_oRow, (short)(_iIdx + i))); i++;

				log.debug("Column " + (_iIdx + i) + " min price : " + getBigDecimal (_oRow, _iIdx + i));
				//price
				oItem.setMinPrice (getBigDecimal (_oRow, (short)(_iIdx + i))); i++;
				
				log.debug("Column " + (_iIdx + i) + " last purchase : " + getBigDecimal (_oRow, _iIdx + i));            	
				//last purchase
				oItem.setLastPurchasePrice (getBigDecimal (_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " last purchase curr : " + getString (_oRow, _iIdx + i));            	
				//last purchase
                Currency oCurr = CurrencyTool.getCurrencyByCode(getString(_oRow, _iIdx + i)); i++;
                if (oCurr != null) oItem.setLastPurchaseCurr (oCurr.getCurrencyCode()); 
                else oItem.setLastPurchaseCurr("");
                
				log.debug("Column " + (_iIdx + i) + " last purchase : " + getBigDecimal (_oRow, _iIdx + i));            	
				//last purchase
				oItem.setLastPurchaseRate (getBigDecimal (_oRow, _iIdx + i)); i++;

				
				log.debug("Column " + (_iIdx + i) + " unit : " + getString(_oRow, _iIdx + i));            					
				//unit_id
				String sUnitCode = getString(_oRow, _iIdx + i); i++;
				String sUnitID = UnitTool.getIDByCode(sUnitCode);
				oItem.setUnitId(sUnitID);
				
				bValid = validateString("Unit '" + sUnitCode + "'", oItem.getUnitId(), oEmpty);
				log.debug("b Unit Valid '" + bValid + "'");
				
				log.debug("Column " + (_iIdx + i) + " purchase unit : " + getString(_oRow, _iIdx + i));            					
				//purchase_unit_id
				String sPurchaseUnitCode = getString(_oRow, _iIdx + i); i++;
				String sPurchaseUnitID = UnitTool.getIDByCode(sPurchaseUnitCode);
				oItem.setPurchaseUnitId(sPurchaseUnitID);

				BigDecimal bdConv = getBigDecimal(_oRow, _iIdx + i); i++;			
				
				//validate conversion
				if(StringUtil.isEqual(sUnitID, sPurchaseUnitID) && bdConv.compareTo(bd_ONE) != 0)
				{
					bValid = validateString("P.Unit:" + sPurchaseUnitCode + " = S.Unit:" + sUnitCode + ", Conversion must be 1 not " + bdConv, "", oEmpty);					
				}			
				if(!StringUtil.isEqual(sUnitID, sPurchaseUnitID) && bdConv.compareTo(bd_ONE) == 0)
				{
					bValid = validateString("P.Unit:" + sPurchaseUnitCode + " = S.Unit:" + sUnitCode + ", Conversion must not 1 ", "", oEmpty);					
				}
				
				
				if (bdConv.doubleValue() <= 1) bdConv = bd_ONE;
				oItem.setUnitConversion(bdConv);
				
				bValid = validateString(
					"Purchase Unit '" + sPurchaseUnitCode + "'", oItem.getPurchaseUnitId(), oEmpty);
				log.debug("Column " + (_iIdx + i) + " kategori : " + getString(_oRow, _iIdx + i));            					
				
				//kategori_id
				String sKategori = getString(_oRow, _iIdx + i); i++;

				String sKategoriID = "";
			    sKategoriID = KategoriTool.getKategoriIDFromCodeList(sKategori);
				oItem.setKategoriId(sKategoriID);

				bValid = validateString("Kategori '" + sKategori + "'", oItem.getKategoriId(), oEmpty);

				log.debug("Column " + (_iIdx + i) + " tax : " + getString(_oRow, _iIdx + i));            									
				//tax_id
				String sTaxCode = getString(_oRow, _iIdx + i); i++;
				String sTaxID = TaxTool.getIDByCode(sTaxCode);
				oItem.setTaxId(sTaxID);

				bValid = validateString("Tax '" + sTaxCode + "'", oItem.getTaxId(), oEmpty);

				log.debug("Column " + (_iIdx + i) + " purchase tax : " + getString(_oRow, _iIdx + i));            									            	
				//purchase_tax_id
				String sPurchaseTaxCode = getString(_oRow, _iIdx + i); i++;
				String sPurchaseTaxID = TaxTool.getIDByCode(sPurchaseTaxCode);
				oItem.setPurchaseTaxId(sPurchaseTaxID);				

				bValid = validateString(
					"Purchase Tax '" + sPurchaseTaxCode + "'", oItem.getPurchaseTaxId(), oEmpty);

				log.debug("Column " + (_iIdx + i) + " vendor : " + getString(_oRow, _iIdx + i));            									

				//vendor_id
				String sVendorID = getString(_oRow, _iIdx + i); i++;
				oItem.setPreferedVendorId(VendorTool.getIDByCode(sVendorID));

				log.debug("Column " + (_iIdx + i) + " vendor item code: " + getString(_oRow, _iIdx + i));            									
            	
				oItem.setVendorItemCode(getString(_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " vendor item name: " + getString(_oRow, _iIdx + i));            									

				oItem.setVendorItemName(getString(_oRow, _iIdx + i)); i++;
            	
            	//item status code
				log.debug("Column " + (_iIdx + i) + " item status code: " + getString(_oRow, _iIdx + i));            									
                
                String sItemStatusCode = getString(_oRow, _iIdx + i); i++;
				oItem.setItemStatusCodeId(ItemStatusCodeTool.getIDByCode(sItemStatusCode));

				//warehouse_id
				log.debug("Column " + (_iIdx + i) + " warehouse id : " + getString(_oRow, _iIdx + i));            									

				String sLocationCode = getString(_oRow, _iIdx + i); i++;
				oItem.setWarehouseId(LocationTool.getIDByCode(sLocationCode));

				log.debug("Column " + (_iIdx + i) + " rack location : " + getString(_oRow, _iIdx + i));            									
							
				oItem.setRackLocation(getString(_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " max stock : " + getBigDecimal (_oRow, _iIdx + i));            									
            	
				oItem.setMaximumStock (getBigDecimal (_oRow, _iIdx + i)); i++;
				
				log.debug("Column " + (_iIdx + i) + " min stock : " + getBigDecimal (_oRow, _iIdx + i));            									
            	
				oItem.setMinimumStock (getBigDecimal (_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " reorder : " + getBigDecimal (_oRow, _iIdx + i));            									

				oItem.setReorderPoint (getBigDecimal (_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " profit margin : " + getString(_oRow, _iIdx + i));            									

				oItem.setProfitMargin (getString(_oRow, _iIdx + i)); i++;
				
            	//Inventory Account code
				log.debug("Column " + (_iIdx + i) + " inventory account code: " + getString(_oRow, _iIdx + i));            									
                
                String sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setInventoryAccount(AccountTool.getIDByCode(sAccountCode));
				if (oItem.getItemType() == i_INVENTORY_PART)
				{
					bValid = validateString("Inventory Account '" + sAccountCode + "'", oItem.getInventoryAccount(), oEmpty);
					bValid = validateAccount(oItem.getInventoryAccount(), oAcc);
				}
            	//Sales Account code
				log.debug("Column " + (_iIdx + i) + " sales account code: " + getString(_oRow, _iIdx + i));            									
                
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setSalesAccount(AccountTool.getIDByCode(sAccountCode));
				bValid = validateString("Sales Account '" + sAccountCode + "'", oItem.getSalesAccount(), oEmpty);
				bValid = validateAccount(oItem.getSalesAccount(), oAcc);

            	//Sales Return Account code
				log.debug("Column " + (_iIdx + i) + " sales return account code: " + getString(_oRow, _iIdx + i));            									
                
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setSalesReturnAccount(AccountTool.getIDByCode(sAccountCode));
				bValid = validateString("SalesReturn Account '" + sAccountCode + "'", oItem.getSalesReturnAccount(), oEmpty);
				bValid = validateAccount(oItem.getSalesReturnAccount(), oAcc);

            	//Item Discount Account code
				log.debug("Column " + (_iIdx + i) + " item discount account code: " + getString(_oRow, _iIdx + i));            									
                
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setItemDiscountAccount(AccountTool.getIDByCode(sAccountCode));
				bValid = validateString("ItemDiscount Account '" + sAccountCode + "'", oItem.getItemDiscountAccount(), oEmpty);
				bValid = validateAccount(oItem.getItemDiscountAccount(), oAcc);
				
            	//COGS Account code
				log.debug("Column " + (_iIdx + i) + " cogs account code: " + getString(_oRow, _iIdx + i));            									                
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setCogsAccount(AccountTool.getIDByCode(sAccountCode));
				bValid = validateString("COGS Account '" + sAccountCode + "'", oItem.getCogsAccount(), oEmpty);
				bValid = validateAccount(oItem.getCogsAccount(), oAcc);
				
            	//Purchase Return Account code
				log.debug("Column " + (_iIdx + i) + " purchase return account code: " + getString(_oRow, _iIdx + i));
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setPurchaseReturnAccount(AccountTool.getIDByCode(sAccountCode));
				if (oItem.getItemType() == i_INVENTORY_PART)
				{
					bValid = validateString("Purchase Return Account '" + sAccountCode + "'", oItem.getPurchaseReturnAccount(), oEmpty);
					bValid = validateAccount(oItem.getPurchaseReturnAccount(), oAcc);
				}
            	//Expense Account code
				log.debug("Column " + (_iIdx + i) + " expense account code: " + getString(_oRow, _iIdx + i));            									
                
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setExpenseAccount(AccountTool.getIDByCode(sAccountCode));
				if (oItem.getItemType() == i_NON_INVENTORY_PART)
				{
					bValid = validateString("Expense Account '" + sAccountCode + "'", oItem.getExpenseAccount(), oEmpty);
					bValid = validateAccount(oItem.getExpenseAccount(), oAcc);
				}
				//Unbilled Goods Account code
				log.debug("Column " + (_iIdx + i) + " unbilled goods account code: " + getString(_oRow, _iIdx + i));            									
                
                sAccountCode = getString(_oRow, _iIdx + i); i++;
				oItem.setUnbilledGoodsAccount(AccountTool.getIDByCode(sAccountCode));
				bValid = validateString("Unbilled Goods '" + sAccountCode + "'", oItem.getUnbilledGoodsAccount(), oEmpty);
				bValid = validateAccount(oItem.getUnbilledGoodsAccount(), oAcc);
				
				List vManuf = ItemFieldTool.getAllBrand();
				String sManuf = getString (_oRow, _iIdx + i); i++;
				if(vManuf.size() > 0)
				{
					
					Principal oPrincipal = ItemFieldTool.getPrincipalByCode(sManuf);
					if(oPrincipal == null) oPrincipal = ItemFieldTool.getPrincipalByName(sManuf); //try by brand name
					if(oPrincipal != null) sManuf = oPrincipal.getPrincipalId();					
					oItem.setManufacturer(sManuf);
				}
				oItem.setManufacturer(sManuf);
				
				List vBrand = ItemFieldTool.getAllBrand();
				String sBrand = getString (_oRow, _iIdx + i); i++;
				if(vBrand.size() > 0)
				{
					
					Brand oBrand = ItemFieldTool.getBrandByCode(sBrand);
					if(oBrand == null) oBrand = ItemFieldTool.getBrandByName(sBrand); //try by brand name
					if(oBrand != null) sBrand = oBrand.getBrandId();										
				}
				oItem.setBrand(sBrand);
				oItem.setColor (getString (_oRow, _iIdx + i)); i++;
				oItem.setSize (getString(_oRow, _iIdx + i)); i++;
				oItem.setSizeUnit (getString(_oRow, _iIdx + i)); i++;
				oItem.setIsFixedPrice (Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;
				oItem.setTrackBatchNo (Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;
				oItem.setTrackSerialNo (Boolean.valueOf(getString(_oRow, _iIdx + i))); i++;
				
				boolean bStatus = true;
				String sStatus = getString(_oRow, _iIdx + i); i++;
				if (StringUtil.equalsIgnoreCase(sStatus,"false")) bStatus = false;
				oItem.setItemStatus (Boolean.valueOf(bStatus)); 

				boolean bDiscont = false;
				String sDiscont = getString(_oRow, _iIdx + i); i++;
				if (StringUtil.equalsIgnoreCase(sDiscont,"true")) bDiscont = true;
				oItem.setIsDiscontinue (Boolean.valueOf(bDiscont)); 
				
				boolean bConsign = false;
				String sConsign = getString(_oRow, _iIdx + i); i++;
				if (StringUtil.equalsIgnoreCase(sConsign,"true")) bConsign = true;
				oItem.setIsConsignment (Boolean.valueOf(bConsign)); 

				oItem.setItemPicture (getString(_oRow, _iIdx + i)); i++;
                
				String sTags = getString(_oRow, _iIdx + i); i++;
				if(StringUtil.isNotEmpty(sTags))
				{
					String sResult = ItemFieldTool.validateTags(sTags);
					if(StringUtil.isNotEmpty(sResult))
					{
						bValid = false;						
						oEmpty.append(sResult);
					}
				}
				oItem.setTags(sTags);
				
                String sDisc = getString(_oRow, _iIdx + i); i++;
                if (StringUtil.isEmpty(sDisc)) sDisc = "0";
                oItem.setDiscountAmount(sDisc); 
				
				//CUSTOM FIELD
				List vCField = CustomFieldTool.getAllCustomField();
				for (int j = 0; j < vCField.size(); j++)
				{
					//CustomField oField = (CustomField) vCField.get(j);
					String sFieldName = getString(m_oHeaderRow, _iIdx + i);					
					log.debug("Column " + (_iIdx + i) + " " +  sFieldName +  " : " + getString(_oRow, _iIdx + i));            									
					String sValue = getString(_oRow, _iIdx + i); i++;	
					ItemFieldTool.saveField(oItem.getItemId(), sFieldName, sValue);
				}
				oItem.setUpdateDate (new Date());
				
				if(bValid && StringUtil.isEqual(oEmpty.toString(),s_EMPTY_REJECT) 
						  && StringUtil.isEqual(oLength.toString(),s_LENGTH_REJECT))
				{
				    oItem.save();
				    ItemManager.getInstance().refreshCache(
				    	oItem.getItemId(), oItem.getItemCode(), oItem.getKategoriId()
					);
				    
				    if(bNewItem)
				    {
					    m_iNewData++;
					    m_oNewData.append (StringUtil.left(m_iNewData + ".", 5));					
					    m_oNewData.append (StringUtil.left(sItemCode,15));
					    m_oNewData.append (StringUtil.left(sItemName,30));
					    m_oNewData.append ("\n");
				    }
				    else
				    {
				        m_iUpdated++;
					    m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 5));					
					    m_oUpdated.append (StringUtil.left(sItemCode,15));
					    m_oUpdated.append (StringUtil.left(sItemName,30));
					    m_oUpdated.append ("\n");
				    }
				}
				else
				{
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sItemCode,15));
				    m_oRejected.append (StringUtil.left(sItemName,30));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
				}

				if (!bNewItem && (oOldItemRef != null) && bValid)
				{
					ItemHistoryTool.createItemHistory (oOldItemRef, oItem, m_sUserName, null);
				}				
			}
		}    
    }
}
