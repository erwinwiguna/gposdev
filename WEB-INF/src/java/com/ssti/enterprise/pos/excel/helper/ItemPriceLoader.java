package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.CustomField;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.CustomFieldTool;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemHistoryTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemLoader.java,v 1.23 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemLoader.java,v $
 * 2016-10-29
 * - Change ItemPriceLoader add Item Name column
 * - add Header Validation
 * - include custom field update if PreferenceTool.getSalesCustomPricingUse true 
 *
 */
public class ItemPriceLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( ItemPriceLoader.class );

	public static String[] a_HEADER = 
    {
        "Item Code"      ,
        "Item Name"      ,
        "Item Price"           
    };
	
	private static final String s_START_HEADING = "Item Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	
	public ItemPriceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;    	    	
    	if(PreferenceTool.getSalesCustomPricingUse())
    	{
    		try 
    		{
    			List<String> vHdr = new ArrayList(Arrays.asList(a_HEADER));
    			List vCField = CustomFieldTool.getAllCustomField();
    			for (int j = 0; j < vCField.size(); j++)
    			{
    				CustomField oField = (CustomField) vCField.get(j);
    				vHdr.add(oField.getFieldName());    				
    			}			
    			m_aHeaders = new String[vHdr.size()];
    			m_aHeaders = vHdr.toArray(m_aHeaders);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    			log.error(e);
    		}    		
    	}
    	else{
    		m_aHeaders = a_HEADER;	
    	}    	
    	m_bValidate = true;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		Item oOldItemRef = null;		
		if (started (_oRow, _iIdx))				
		{
			short iCol = 0;
			String sItemCode = getString(_oRow, _iIdx + iCol); iCol++;
			String sItemName = getString(_oRow, _iIdx + iCol); iCol++;			
			BigDecimal dPrice = getBigDecimal(_oRow, _iIdx + iCol); iCol++;
			
			if (StringUtil.isNotEmpty(sItemCode))
			{
				Item oItem = ItemTool.getItemByCode(sItemCode);
				if (oItem != null)
				{					
					oOldItemRef = oItem.copy();
					oItem.setItemPrice(dPrice);		
					
					//if custom pricing used then update custom field
					if(PreferenceTool.getSalesCustomPricingUse() && 
					   StringUtil.isNotEmpty(PreferenceTool.getSalesCustomPricingTpl()))
					{
						List vCField = CustomFieldTool.getAllCustomField();
						for (int j = 0; j < vCField.size(); j++)
						{
							//CustomField oField = (CustomField) vCField.get(j);
							String sFieldName = getString(m_oHeaderRow, _iIdx + iCol);					
							log.debug("Column " + (_iIdx + iCol) + " " +  sFieldName +  " : " + getString(_oRow, _iIdx + iCol));            									
							String sValue = getString(_oRow, _iIdx + iCol); iCol++;	
							ItemFieldTool.saveField(oItem.getItemId(), sFieldName, sValue);
						}
					}
					
					oItem.setUpdateDate (new Date());
					oItem.save();
				    ItemManager.getInstance().refreshCache(
				    	oItem.getItemId(), oItem.getItemCode(), oItem.getKategoriId()
					);
				    m_iUpdated++;
				    m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 5));					
				    m_oUpdated.append (StringUtil.left(sItemCode,15));
				    m_oUpdated.append (StringUtil.left(oItem.getItemName(),30));
				    m_oUpdated.append (StringUtil.right(oItem.getItemPrice().toString(),30));				    
				    m_oUpdated.append ("\n");
					if (oOldItemRef != null)
					{
						ItemHistoryTool.createItemHistory (oOldItemRef, oItem, m_sUserName, null);
					}				
				}
			}
		}    
    }
}
