package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.BatchTransaction;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemTransferDetailLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ItemTransferDetailLoader.java,v 1.12 2008/08/17 02:17:21 albert Exp $
 *
 * $Log: ItemTransferDetailLoader.java,v $
 *
 *
 */
public class ItemTransferDetailLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (ItemTransferDetailLoader.class);
    private String sLocationID;
    public ItemTransferDetailLoader(String _sLocationID)
        throws Exception
    
    {
    	sLocationID = _sLocationID;    	
    }

    protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		ItemTransferDetail oITD = new ItemTransferDetail();
		String sCode = getString(_oRow,_iIdx + i); i++;
		String sName = getString(_oRow,_iIdx + i);		
		double dQtyChg = getBigDecimal(_oRow,_iIdx + 5).doubleValue();
        
		Item oItem = ItemTool.getItemByCode(sCode);        
        if(oItem != null)
        {
            oITD.setItemId(oItem.getItemId());
            oITD.setItemCode(oItem.getItemCode());

            String sUnitID = oItem.getUnitId();
        	Unit oUnit = UnitTool.getUnitByID(sUnitID);        	
            if(oUnit != null)
            {            
                oITD.setUnitId(oUnit.getUnitId());
                oITD.setUnitCode(oUnit.getUnitCode());
            }
            else
            {
            	setError (iRow, sCode, sName, ",Item contains invalid Unit ");
            }    
            oITD.setQtyChanges(new BigDecimal(dQtyChg));
            oITD.setQtyBase(oITD.getQtyChanges());
            
            oITD.setCostPerUnit(bd_ZERO); //will be set when save
            
            double dCurrentQty = 0;
            InventoryLocation oIL = InventoryLocationTool.getDataByItemAndLocationID(oItem.getItemId(), sLocationID);
            if(oIL != null)
            {
            	dCurrentQty = oIL.getCurrentQty().doubleValue();
            	oITD.setQtyOH(oIL.getCurrentQty());
            }
            if(PreferenceTool.getProcessTransferAtReceive())
            {            	
            }
            if(dCurrentQty < dQtyChg)
            {
            	setError(iRow, sCode, sName, ",Qty not enough, TO Qty:" + CustomFormatter.fmt(dQtyChg) + 
            								 ", QtyOH:" + CustomFormatter.fmt(dCurrentQty));
            }
            else
            {            
	            String sBatchNumber = getString(_oRow, _iIdx + 9);
	            if (StringUtil.isNotEmpty(sBatchNumber))
	            {
	            	String sExp = getString(_oRow, _iIdx + 10);
	            	Date dExp = CustomParser.parseDate(sExp);
	            	
	            	BatchTransaction oBT = BatchTransactionTool.newBatchTrans();
	            	oBT.setBatchNo(sBatchNumber);
	            	oBT.setExpiredDate(dExp);
	            	oBT.setDescription("");
	            	oITD.setBatchTransaction(oBT);
	            }            
	            m_vDataList.add(oITD);
            }
        }
        else
        {
        	setError (iRow, sCode, sName, ",Item Not Found");
        }
    }        
    
    void setError (int _iRow, String _sCode, String _sName, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append (_iRow + 1);
		oMsg.append (",CODE:" + _sCode );
		oMsg.append (",NAME:" + _sName );
		oMsg.append (_sMsg);
		oMsg.append ("\n");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}
