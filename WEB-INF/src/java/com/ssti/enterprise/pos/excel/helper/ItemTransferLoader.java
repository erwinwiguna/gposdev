package com.ssti.enterprise.pos.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;

public class ItemTransferLoader extends BaseExcelLoader implements ItemListLoader
{    
	private static Log log = LogFactory.getLog ( ItemTransferLoader.class );
	
	private static final String[] aHeaders = 
	{
		"Item Code",
		"Item Name",
		"Full Name",
		"Qty",
		"New Qty",
		"+/-",
		"Unit",	
		"Description",	
		"Cost"
	};
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {		
		if (getString(_oRow, _iIdx) != null && !getString(_oRow, _iIdx).equals(sHeading))				
		{
			short iCol = 0;
			
			String sItemCode = getString(_oRow,_iIdx + iCol); iCol++; 
			String sItemName = getString(_oRow,_iIdx + iCol); iCol++; 
			
			if (sItemName != null && !sItemName.equals(""))
			{				
				Item oItem = ItemTool.getItemByCode(sItemCode);
				ItemTransferDetail oDetail = new ItemTransferDetail();
				
				if (oItem != null) 
				{
					oDetail.setItemId      (oItem.getItemId()); 
					oDetail.setItemCode    (sItemCode); 
					
					iCol = 5; // +/- column 
					oDetail.setQtyChanges  (getBigDecimal (_oRow, _iIdx + iCol)); iCol++;
					String sUnitCode = getString(_oRow, _iIdx + iCol); iCol++;
					oDetail.setUnitId(UnitTool.getIDByCode(sUnitCode));
					oDetail.setUnitCode(sUnitCode);
					m_vDataList.add(oDetail);
				}			
			}
		}    
    }
    
}
