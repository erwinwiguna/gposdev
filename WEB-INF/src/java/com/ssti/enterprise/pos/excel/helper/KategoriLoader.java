package com.ssti.enterprise.pos.excel.helper;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.KategoriManager;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.KategoriAccount;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/KategoriLoader.java,v $
 * Purpose: load kategori from excel
 *
 * @author  $Author: albert $
 * @version $Id: KategoriLoader.java,v 1.13 2008/06/29 07:02:31 albert Exp $
 *
 * $Log: KategoriLoader.java,v $
 * Revision 1.13  2008/06/29 07:02:31  albert
 * *** empty log message ***
 *
 * Revision 1.12  2007/06/30 13:31:56  albert
 * *** empty log message ***
 *
 */
public class KategoriLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( KategoriLoader.class );
	
	private StringBuilder m_oRejectedKategori = new StringBuilder("\nRejected Kategori : \n");

	protected String sHeading = "Kategori Code";

	private String m_sUserName;
	private int m_iRejected   = 0;
	private int m_iTotalRows = 0;
	private boolean m_bStart = false;
	private boolean m_bRejected = false;
	
	public KategoriLoader (String _sUserName)
	{
		super.sHeading = this.sHeading;
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	public String getResult()
	{
		m_oResult.append (m_oNewData);
		m_oResult.append ("\nTotal New Kategori : ");
		m_oResult.append (m_iNewData);
		m_oResult.append ("\n");
		m_oResult.append (m_oUpdated);
		m_oResult.append ("\nTotal Kategori Updated : ");
		m_oResult.append (m_iUpdated);
		m_oResult.append ("\n");
		m_oResult.append (m_oRejectedKategori);
		m_oResult.append ("\nTotal Kategori Rejected : ");
		m_oResult.append (m_iRejected);
		m_oResult.append ("\n");
		m_oResult.append (m_oError);
		m_oResult.append ("\nTotal ERROR : ");
		m_oResult.append (m_iError);

		return m_oResult.toString();
	}
	
    
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		log.debug (" updateList : " + getString(_oRow, _iIdx));
    	
		short i = 0;
		
		String sKategoriCode = getString(_oRow, _iIdx + i); i++;
		String sDescription = getString(_oRow, _iIdx + i); i++;
		String sParentCode = getString(_oRow, _iIdx + i); i++;
		
		log.debug (" Kategori Code : " + sKategoriCode);
		log.debug (" Description : " + sDescription);
		log.debug (" ParentCode : " + sParentCode);
		
		if (StringUtil.isNotEmpty(sKategoriCode)&& StringUtil.isNotEmpty(sDescription))
		{
			boolean bNewData = false;
			
			try
			{
				
				//get Parent Id
				String sParentID = "";
				sParentID = KategoriTool.getKategoriIDFromCodeList(sParentCode);
				
				if (!sParentCode.equals("") && StringUtil.isEmpty(sParentID))
				{
					m_bRejected = true;
				}
				
				Kategori oKategori = KategoriTool.getKategoriByCodeAndParentID(sKategoriCode, sParentID);
				
				if (oKategori == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					bNewData = true;
					oKategori = new Kategori();
					oKategori.setKategoriId (IDGenerator.generateSysID(false));
					oKategori.setKategoriCode(sKategoriCode); 
					oKategori.setAddDate (new Date());				    
				}
				
				oKategori.setDescription (sDescription);
				oKategori.setParentId(sParentID);
				oKategori.setKategoriLevel(KategoriTool.setChildLevel(sParentID));
                
				KategoriAccount oKatAcc = null; 				
				if (!bNewData)
				{
					oKatAcc = KategoriTool.getKategoriAccount(oKategori.getKategoriId());
				}
				
				if (oKatAcc == null)
				{
					oKatAcc = new KategoriAccount();
					oKatAcc.setKategoriId(oKategori.getKategoriId());
				}
				
				if(!m_bRejected)
				{
					oKategori.setInternalCode(KategoriTool.getInternalCode(oKategori));
					oKategori.save();

					log.debug("Column " + (_iIdx + i) + " inventory account code: " + getString(_oRow, _iIdx + i));            									
	                String sAccCode = getString(_oRow, _iIdx + i); i++;
					if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setInventoryAccount(AccountTool.getIDByCode(sAccCode));

					//Sales Account code
					log.debug("Column " + (_iIdx + i) + " sales account code: " + getString(_oRow, _iIdx + i));            									
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setSalesAccount(AccountTool.getIDByCode(sAccCode));

	            	//Sales Return Account code
					log.debug("Column " + (_iIdx + i) + " sales return account code: " + getString(_oRow, _iIdx + i));            									               
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setSalesReturnAccount(AccountTool.getIDByCode(sAccCode));

	            	//Item Discount Account code
					log.debug("Column " + (_iIdx + i) + " item discount account code: " + getString(_oRow, _iIdx + i));            									
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setItemDiscountAccount(AccountTool.getIDByCode(sAccCode));
					
	            	//COGS Account code
					log.debug("Column " + (_iIdx + i) + " cogs account code: " + getString(_oRow, _iIdx + i));            									                
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setCogsAccount(AccountTool.getIDByCode(sAccCode));

					//Purchase Return Account code
					log.debug("Column " + (_iIdx + i) + " purchase return account code: " + getString(_oRow, _iIdx + i));
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setPurchaseReturnAccount(AccountTool.getIDByCode(sAccCode));

					//Expense Account code
					log.debug("Column " + (_iIdx + i) + " expense account code: " + getString(_oRow, _iIdx + i));                
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setExpenseAccount(AccountTool.getIDByCode(sAccCode));

					//Unbilled Goods Account code
					log.debug("Column " + (_iIdx + i) + " unbilled goods account code: " + getString(_oRow, _iIdx + i));                
	                sAccCode = getString(_oRow, _iIdx + i); i++;
	                if(StringUtil.isNotEmpty(sAccCode)) oKatAcc.setUnbilledGoodsAccount(AccountTool.getIDByCode(sAccCode));
					oKatAcc.save();
	                
					if(bNewData)
					{
						m_iNewData++;
						m_oNewData.append (m_iNewData);					
						m_oNewData.append (". ");
						m_oNewData.append (sKategoriCode);
						m_oNewData.append ("\t");
						m_oNewData.append (sDescription);
						m_oNewData.append ("\t");
						m_oNewData.append (sParentCode);
						m_oNewData.append ("\n");
					}
					else
					{
						m_iUpdated++;
						m_oUpdated.append (m_iUpdated);					
						m_oUpdated.append (". ");
						m_oUpdated.append (sKategoriCode);
						m_oUpdated.append ("\t");
						m_oUpdated.append (sDescription);
						m_oUpdated.append ("\t");
						m_oUpdated.append (sParentCode);
						m_oUpdated.append ("\n");
					}
				}
				else
				{
					m_iRejected++;
					m_oRejectedKategori.append (m_iRejected);					
					m_oRejectedKategori.append (". ");
					m_oRejectedKategori.append (sKategoriCode);
					m_oRejectedKategori.append ("\t");
					m_oRejectedKategori.append (sDescription);
					m_oRejectedKategori.append ("\t");
					m_oRejectedKategori.append (sParentCode);
					m_oRejectedKategori.append ("\n");
					
				}
				KategoriManager.getInstance().refreshCache(oKategori);
			}
			catch (Exception _oEx)
			{
				m_iError++;
				m_oError.append (m_iError);					
				m_oError.append (". ");
				m_oError.append (sKategoriCode);
				m_oError.append ("\t");
				m_oError.append (sDescription);
				m_oError.append ("\t");
				m_oError.append (sParentCode);
				m_oError.append ("\t");
				m_oError.append (_oEx.getMessage());
				m_oError.append ("\n");
				
				log.error (_oEx);
				_oEx.printStackTrace();
			}
		}
    }
}
