package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class LocationViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = (List) _oSession.getAttribute("Locations");
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Location View Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Name"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Address"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Email"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Location URL"); iCol++;				
		createHeaderCell(oWB, oRow, (short)iCol, "Site"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Inventory Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "HO To Store LeadTime"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Parent Location"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Transfer Account"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Longitudes"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Latitudes"); iCol++;
		
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Location oLocation = (Location) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oLocation.getLocationCode   ()); iCol++; 
                createCell(oWB, oRow2, (short)iCol, oLocation.getLocationName   ()); iCol++; 
                createCell(oWB, oRow2, (short)iCol, oLocation.getLocationAddress()); iCol++;
                createCell(oWB, oRow2, (short)iCol, oLocation.getLocationEmail());   iCol++;
                createCell(oWB, oRow2, (short)iCol, oLocation.getLocationUrl());     iCol++;
                
                createCell(oWB, oRow2, (short)iCol, SiteTool.getSiteNameByID(oLocation.getSiteId())); iCol++;
                
                if(oLocation.getInventoryType() == 1){
                    createCell(oWB, oRow2, (short)iCol, "NORMAL");	iCol++; 
                }
                else if(oLocation.getInventoryType() == 2){
                    createCell(oWB, oRow2, (short)iCol, "MINUS");		iCol++; 
                }
                else if(oLocation.getInventoryType() == 3){
                    createCell(oWB, oRow2, (short)iCol, "DROPSHIP");	iCol++; 
                }

                createCell(oWB, oRow2, (short)iCol, Integer.toString(oLocation.getLocationType()));    iCol++;
                createCell(oWB, oRow2, (short)iCol, Integer.toString(oLocation.getHoStoreLeadtime())); iCol++;
                createCell(oWB, oRow2, (short)iCol, oLocation.getDescription()); iCol++;
                createCell(oWB, oRow2, (short)iCol, oLocation.getParentCode()); iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oLocation.getTransferAccount())); iCol++;
                createCell(oWB, oRow2, (short)iCol, oLocation.getLongitudes()); iCol++;
                createCell(oWB, oRow2, (short)iCol, oLocation.getLatitudes()); iCol++;
            }        
        }
        return oWB;
    }
}
