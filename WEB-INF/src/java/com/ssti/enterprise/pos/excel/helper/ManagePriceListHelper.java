package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PriceList;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class ManagePriceListHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    private static final String s_PARAM_ID = "id";
     
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {   
        String[] aID = (String[]) oParam.get(s_PARAM_ID);
        String sID = aID [0];
        
        List vData = PriceListTool.getDetails(sID);
        PriceList oPL = PriceListTool.getPriceListByID(sID);      
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Item View");

        HSSFRow oRow = oSheet.createRow(0);
        int iRow = 0;

        createHeaderCell(oWB, oRow, (short)iRow, "Price Code"); iRow++;        
        createHeaderCell(oWB, oRow, (short)iRow, "Item Code	"); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Item Name "); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Item Price"); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Item Cost "); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Last Purc."); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Dicount   "); iRow++;        
        createHeaderCell(oWB, oRow, (short)iRow, "New Price "); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "+/-       "); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Promo Qty "); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Total     "); iRow++;
        
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                PriceListDetail oDetail = (PriceListDetail) vData.get(i);
                
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                Item oItem = ItemTool.getItemByID (oDetail.getItemId());
                
                InventoryLocation oInvLoc = 
                	InventoryLocationTool.getDataByItemAndLocationID (oDetail.getItemId(), PreferenceTool.getLocationID());

                BigDecimal dItemCost = bd_ZERO;				
                if (oInvLoc != null)
                {
					dItemCost = oInvLoc.getItemCost();
                }

                iRow = 0;
                createCell(oWB, oRow2, (short)iRow, oPL.getPriceListCode());                  iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getItemCode());                     iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getDescription  ());                iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getItemPrice().toString());         iRow++;   
                createCell(oWB, oRow2, (short)iRow, dItemCost.toString());                    iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getLastPurchasePrice().toString()); iRow++;   
                createCell(oWB, oRow2, (short)iRow, oDetail.getDiscountAmount()); 			  iRow++;   
                createCell(oWB, oRow2, (short)iRow, oDetail.getNewPrice()); 			  	  iRow++;   
            }        
        }
        return oWB;
    }
}
