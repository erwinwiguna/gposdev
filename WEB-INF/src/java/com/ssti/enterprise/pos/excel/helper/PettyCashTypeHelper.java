package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.PettyCashType;
import com.ssti.enterprise.pos.tools.PettyCashTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class PettyCashTypeHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = PettyCashTypeTool.getAllPettyCashType();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("PettyCashType Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
		createHeaderCell(oWB, oRow, (short)iCol,"Petty Cash Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Flow"); iCol++;	
		createHeaderCell(oWB, oRow, (short)iCol,"Account Code"); iCol++;		
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                PettyCashType oPC = (PettyCashType) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                String sInOut = "Deposit";
                if(oPC.getIncomingOutgoing() == 2) sInOut = "Withdrawal";
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oPC.getPettyCashCode());   iCol++; 
                createCell(oWB, oRow2, (short)iCol, oPC.getDescription());iCol++; 
                createCell(oWB, oRow2, (short)iCol, Integer.toString(oPC.getIncomingOutgoing()));iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oPC.getAccountId()));iCol++; 
            }        
        }
        return oWB;
    }
}
