package com.ssti.enterprise.pos.excel.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemLoader.java,v 1.23 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemLoader.java,v $
 * Revision 1.23  2009/05/04 01:38:29  albert
 * *** empty log message ***
 *
 *
 */
public class SerialNoLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( SerialNoLoader.class );

	private static final String s_START_HEADING = "Item Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	
	public SerialNoLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	Map mSN = new HashMap();
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		if (started (_oRow, _iIdx))				
		{
			short i = 0;
			String sItemCode = getString(_oRow, _iIdx + i); i++;			
			String sSerialNo = getString(_oRow, _iIdx + i); i++;			
			int iLine = Integer.valueOf(getString(_oRow, _iIdx + i));
			
			if (StringUtil.isNotEmpty(sItemCode))
			{
				List vSN = (List)mSN.get(sItemCode);
				if (vSN == null) 
				{
					vSN = new ArrayList();
				}				
				vSN.add(sSerialNo);
				mSN.put(sItemCode, vSN);
			}
		}    
    }
    
    public Map getMapResult()
    {
    	return mSN;
    }
}
