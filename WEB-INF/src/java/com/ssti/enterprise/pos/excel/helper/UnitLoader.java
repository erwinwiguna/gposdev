package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.UnitManager;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;

public class UnitLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (UnitLoader.class );
	
	public static final String s_START_HEADING = "Unit Code";
	
	public UnitLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sUnitCode = getString(_oRow, _iIdx + i); i++;
			String sUnitName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sUnitCode))
			{				
				Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Unit Code : " + sUnitCode);
				
				if (oUnit == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					oUnit = new Unit();
					oUnit.setUnitId (IDGenerator.generateSysID());
					oUnit.setUnitCode(sUnitCode); 	
					bNew = true;
				}
				else 
				{
					bNew = false;
				}
				oUnit.setDescription(sUnitName); 
				bValid = validateString("Description", oUnit.getDescription(), 100, oEmpty, oLength);

				String sBaseUnit = getString(_oRow, _iIdx + i); i++;
				oUnit.setBaseUnit(Boolean.valueOf(sBaseUnit));

				BigDecimal oConv = getBigDecimal(_oRow, _iIdx + i); i++;
				oUnit.setValueToBase(oConv);

				String sAltUnit = getString(_oRow, _iIdx + i); i++;
				oUnit.setAlternateBase(UnitTool.getIDByCode(sAltUnit));

                oUnit.save();
                UnitManager.getInstance().refreshCache(oUnit);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sUnitCode,30));
						m_oNewData.append (StringUtil.left(sUnitName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sUnitCode,30));
						m_oUpdated.append (StringUtil.left(sUnitName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sUnitCode,30));
				    m_oRejected.append (StringUtil.left(sUnitName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}
