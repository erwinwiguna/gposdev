package com.ssti.enterprise.pos.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.StringUtil;

public class VATLoader extends BaseExcelLoader
{    
	private int iType = 0;
	
	private static Log log = LogFactory.getLog (VATLoader.class);
	
	private static final String s_START_HEADING = "No.";	
	
	public VATLoader (String _sUserName, int _iType)
	{
		iType = _iType;
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}

    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {		    	
		if (started (_oRow, _iIdx))	
		{
			short i = 1;			
			String sTaxNo = getString(_oRow, _iIdx + i); i++;
			String sInvNo = getString(_oRow, _iIdx + i); i++;					
			if(StringUtil.isNotEmpty(sTaxNo) && StringUtil.isNotEmpty(sInvNo))
			{	
				try
				{
					if (iType == 1)
					{
						String sID = PurchaseInvoiceTool.getIDByInvoiceNo(sInvNo);
						TaxTool.savePurchaseSerial(sID, sTaxNo);					
					}
					else
					{
						String sID = TransactionTool.getIDByInvoiceNo(sInvNo);
						TaxTool.saveSerial(sID, sTaxNo);
					}
					m_oUpdated.append("SUCCESS, Invoice : " + sInvNo + " TaxNo: " + sTaxNo); 
					m_iUpdated++;
				}
				catch (Exception e)
				{
					e.printStackTrace();
					m_oError.append("ERROR, Invoice: " + sInvNo + " TaxNo:" + sTaxNo + " MSG: " + e.getMessage());
					m_iError++;
				}
			}
		}    
    }    
}
