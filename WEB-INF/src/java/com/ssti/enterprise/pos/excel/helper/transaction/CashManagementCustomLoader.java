package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * </pre><br>
 */
public class CashManagementCustomLoader extends TransactionLoader
{    
    private static Log log = LogFactory.getLog (CashManagementCustomLoader.class);
    private static final String s_START_HEADING = "nopk"; 
    
    public CashManagementCustomLoader ()
    {
    }
    
    public CashManagementCustomLoader (String _sUserName)
    {
        m_sUserName = _sUserName + " from (Excel)";
    }
    
    Currency defCurr = null;
    HSSFSheet oSheet = null;
    CashFlow oTR = null;
    List vTD = null;
    
    String sTransNo = "";
    /**
     * load data from input stream
     * 
     * @param _oInput
     * @throws Exception
     */
    public void loadData (InputStream _oInput)  
        throws Exception
    {
        POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
        HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
        oSheet = oWB.getSheetAt(0);
        m_iTotalRows = oSheet.getPhysicalNumberOfRows();
        boolean bTransHeader = false;
        defCurr = CurrencyTool.getDefaultCurrency();
        
        for (int iRow = 1; iRow < m_iTotalRows; iRow++)
        {
            HSSFRow oRow = oSheet.getRow(iRow);
            if (oRow != null)
            {
                sTransNo = getString(oRow, i_START);
                if (StringUtil.isNotEmpty(sTransNo))
                {                   
                    log.debug("* TRANS NO: " + sTransNo);               

                    m_bTransError = false;
                    m_iTotalTrans++;

                    oTR = new CashFlow();
                    vTD = new ArrayList();

                    mapTrans (oRow, oTR, vTD);
                    
                    log.debug("* oTR " + oTR);
                    log.debug("* vTD " + vTD);
                    
                    if (!m_bTransError)
                    {
                        try
                        {
                            CashFlowTool.setHeaderProperties(oTR, vTD, null, true);
                            CashFlowTool.saveData(oTR, vTD);
                            logSuccess();
                        }
                        catch (Exception _oEx)
                        {
                            String sMsg = _oEx.getMessage();
                            log.error(_oEx);
                            logSaveError(sMsg);
                        }
                    }
                    else
                    {
                        logReject();
                    }
                    m_bTransError = false;
                }
                else 
                {
                    logInvalidFile();
                    return;
                }
            }
        } 
    }   
    
    protected void mapTrans (HSSFRow row, CashFlow cf, List vDet)
        throws Exception
    {
        /*
             nopk       001/TF/0042/12
             tglbayar   20/4/2012
             collector  KANTOR                  => lcod
             kodebank   BC
             angke      01
             nama       AMBORCIUS SIHOMBING
             namabrg    SMASH / 05
             byrang     148,000.00
             byrpok     97,921.00
             byrbung    50,079.00
             byrtagih   
             byrdenda   0 
             potongan   0             
             
             byramort   0
             byradm2    0
             provisi2   0
             bsurvey2   0

             total      0
             tgljt      4/20/2012

tglbayar    nopk                angke               nama        namabrg     byrpok      byrbung     potongan    byrang      byrdenda    By Tagih    total       tgljt   collector   kodebank    byramort    adm2    provisi2    bsurvey2
4/20/2012   001/TF/0061/12  01  001/TF/0061/12A01   ANDI LALA   SMASH / 04  97,921.00   50,079.00   0.00        148,000.00  2,500.00    10,000.00   160,500.00  4/20/2012   KANTOR  BC  38,053.00   24,953.00   3,743.00    9,357.00 

         
         * 
         */
        int col = 0;

        String nopk       = getString (row, col); col++; 
        String tglbayar   = getString (row, col); col++; 
        String lcod       = getString (row, col); col++; 
        String kodebank   = getString (row, col); col++; 
        String angke      = getString (row, col); col++; 
        String tenor      = getString (row, col); col++;         
        String nama       = getString (row, col); col++; 
        String nbrg       = getString (row, col); col++;

        BigDecimal total    = getBigDecimal(row, col); col++;
        BigDecimal byrang   = getBigDecimal(row, col); col++;
        BigDecimal byrpok   = getBigDecimal(row, col); col++;          
        BigDecimal byrbung  = getBigDecimal(row, col); col++;
        BigDecimal byrdenda = getBigDecimal(row, col); col++;
        BigDecimal byrtagih = getBigDecimal(row, col); col++;
        BigDecimal potongan = getBigDecimal(row, col); col++;

        BigDecimal admin2   = getBigDecimal(row, col); col++;
        BigDecimal provisi2 = getBigDecimal(row, col); col++;
        BigDecimal survey2  = getBigDecimal(row, col); col++;
        
        System.out.println("nopk " + nopk);
        System.out.println("lcod " + lcod);
        System.out.println("nama " + nama);
        System.out.println("nbrg " + nbrg);

        sTransNo = nopk;
        if (isExists()) logError("Trans Already Exist", sTransNo);

        String desc = "Pembayaran Kons a/n " + nama + " " + nopk + 
                      " Angs " +  angke + "/" + tenor + " (" + nbrg + ")";
        
        Location oLoc = LocationTool.getLocationByCode(lcod);

        Bank bank = BankTool.getBankByCode(kodebank);
        if(bank == null) logError("Invalid Bank ", kodebank);
        
        String transNo = nopk + "A" + angke;
        
        cf.setCashFlowNo(transNo);
        cf.setReferenceNo(nopk);
        cf.setBankId(bank.getBankId());
        cf.setBankIssuer(nopk);        
        cf.setDescription(desc);
        cf.setExpectedDate(CustomParser.parseDate(tglbayar));
        cf.setDueDate(cf.getExpectedDate());
        cf.setStatus(i_PROCESSED);
        cf.setUserName("");
        cf.setCurrencyRate(bd_ONE);
        cf.setCurrencyId(defCurr.getCurrencyId());
        cf.setCashFlowType(i_DEPOSIT);
        cf.setTransactionType(i_CF_NORMAL);
        cf.setTransactionId("");
        cf.setTransactionNo("");
        cf.setLocationId(oLoc.getLocationId());
        
        //amount
        cf.setCashFlowAmount(total);
        cf.setAmountBase(cf.getCashFlowAmount());        
        cf.setJournalAmount(cf.getCashFlowAmount());
        
/*
1.11.03.02.01   Kas Collection Cab Klender  160,500.00  
         1.13.01.01.00         Piutang PK               97,921.00 
         4.10.01.00.00         Pendapatan Bunga PK      50,079.00 
         4.40.01.01.00         Pendapatan Denda         2,500.00 
         4.40.01.03.00         Pendapatan Penagihan     10,000.00 
*/
        
        CashFlowJournal cf1 = new CashFlowJournal();
        Account ac1 = AccountTool.getAccountByCode("1.13.01.01.00"); //piutang
        cf1.setAccountId(ac1.getAccountId());
        cf1.setAccountCode(ac1.getAccountCode());
        cf1.setAccountName(ac1.getAccountName());
        cf1.setAmount(byrpok);
        cf1.setAmountBase(cf1.getAmount());
        cf1.setCurrencyId(defCurr.getCurrencyId());
        cf1.setCurrencyRate(bd_ONE);
        cf1.setDebitCredit(GlAttributes.i_CREDIT);
        cf1.setLocationId(oLoc.getLocationId());
        cf1.setDepartmentId("");
        cf1.setProjectId("");
        cf1.setMemo("");
        cf1.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.01.02",null));

        CashFlowJournal cf2 = new CashFlowJournal();
        Account ac2 = AccountTool.getAccountByCode("4.10.01.00.00"); //bunga
        cf2.setAccountId(ac2.getAccountId());
        cf2.setAccountCode(ac2.getAccountCode());
        cf2.setAccountName(ac2.getAccountName());
        cf2.setAmount(byrbung);
        cf2.setAmountBase(cf2.getAmount());
        cf2.setCurrencyId(defCurr.getCurrencyId());
        cf2.setCurrencyRate(bd_ONE);
        cf2.setDebitCredit(GlAttributes.i_CREDIT);
        cf2.setLocationId(oLoc.getLocationId());
        cf2.setDepartmentId("");
        cf2.setProjectId("");
        cf2.setMemo("");
        cf2.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.01.01",null));

        CashFlowJournal cf3 = new CashFlowJournal();
        Account ac3 = AccountTool.getAccountByCode("4.99.01.01.00"); //denda
        cf3.setAccountId(ac3.getAccountId());
        cf3.setAccountCode(ac3.getAccountCode());
        cf3.setAccountName(ac3.getAccountName());
        cf3.setAmount(byrdenda);
        cf3.setAmountBase(cf3.getAmount());
        cf3.setCurrencyId(defCurr.getCurrencyId());
        cf3.setCurrencyRate(bd_ONE);
        cf3.setDebitCredit(GlAttributes.i_CREDIT);
        cf3.setLocationId(oLoc.getLocationId());
        cf3.setDepartmentId("");
        cf3.setProjectId("");
        cf3.setMemo("");
        cf3.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.04.01.01",null));        
        
        CashFlowJournal cf4 = new CashFlowJournal();
        Account ac4 = AccountTool.getAccountByCode("4.99.01.03.00"); //tagihan
        cf4.setAccountId(ac4.getAccountId());
        cf4.setAccountCode(ac4.getAccountCode());
        cf4.setAccountName(ac4.getAccountName());
        cf4.setAmount(byrtagih);
        cf4.setAmountBase(cf4.getAmount());
        cf4.setCurrencyId(defCurr.getCurrencyId());
        cf4.setCurrencyRate(bd_ONE);
        cf4.setDebitCredit(GlAttributes.i_CREDIT);
        cf4.setLocationId(oLoc.getLocationId());
        cf4.setDepartmentId("");
        cf4.setProjectId("");
        cf4.setMemo("");
        cf4.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.04.01.03",null));
        
        /*
1.13.04.01.00   Piutang Amort PK-Adm    24,953.00   
         4.10.02.01.00           Pendapatan Adm Amort PK        24,953.00 
         */
        
        if (admin2 != null && admin2.doubleValue() != 0)
        {
            CashFlowJournal cf5 = new CashFlowJournal();
            Account ac5 = AccountTool.getAccountByCode("1.13.04.01.00");
            cf5.setAccountId(ac5.getAccountId());
            cf5.setAccountCode(ac5.getAccountCode());
            cf5.setAccountName(ac5.getAccountName());
            cf5.setAmount(new BigDecimal(admin2.doubleValue() * -1));
            cf5.setAmountBase(cf5.getAmount());
            cf5.setCurrencyId(defCurr.getCurrencyId());
            cf5.setCurrencyRate(bd_ONE);
            cf5.setDebitCredit(GlAttributes.i_CREDIT);
            cf5.setLocationId(oLoc.getLocationId());
            cf5.setDepartmentId("");
            cf5.setProjectId("");
            cf5.setMemo("");            
            cf5.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.02.04",null));
    
            CashFlowJournal cf5a = new CashFlowJournal();
            Account ac5a = AccountTool.getAccountByCode("4.10.02.01.00");
            cf5a.setAccountId(ac5a.getAccountId());
            cf5a.setAccountCode(ac5a.getAccountCode());
            cf5a.setAccountName(ac5a.getAccountName());
            cf5a.setAmount(admin2);
            cf5a.setAmountBase(cf5a.getAmount());
            cf5a.setCurrencyId(defCurr.getCurrencyId());
            cf5a.setCurrencyRate(bd_ONE);
            cf5a.setDebitCredit(GlAttributes.i_CREDIT);
            cf5a.setLocationId(oLoc.getLocationId());
            cf5a.setDepartmentId("");
            cf5a.setProjectId("");
            cf5a.setMemo("");
            cf5a.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.02.01",null));
            
            vDet.add(cf5a);        
            vDet.add(cf5);
        }

        /*
1.13.04.02.00   Piutang Amort PK-Provisi    3,743.00    
         4.10.02.02.00          Pendapatan Provisi Amort PK     3,743.00 

         */
        
        if (provisi2 != null && provisi2.doubleValue() != 0)
        {
            CashFlowJournal cf6 = new CashFlowJournal();
            Account ac6 = AccountTool.getAccountByCode("1.13.04.02.00");
            cf6.setAccountId(ac6.getAccountId());
            cf6.setAccountCode(ac6.getAccountCode());
            cf6.setAccountName(ac6.getAccountName());
            cf6.setAmount(new BigDecimal(provisi2.doubleValue() * -1));
            cf6.setAmountBase(cf6.getAmount());
            cf6.setCurrencyId(defCurr.getCurrencyId());
            cf6.setCurrencyRate(bd_ONE);
            cf6.setDebitCredit(GlAttributes.i_CREDIT);
            cf6.setLocationId(oLoc.getLocationId());
            cf6.setDepartmentId("");
            cf6.setProjectId("");
            cf6.setMemo("");
            cf6.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.02.05",null));
            
            CashFlowJournal cf6a = new CashFlowJournal();
            Account ac6a = AccountTool.getAccountByCode("4.10.02.02.00");
            cf6a.setAccountId(ac6a.getAccountId());
            cf6a.setAccountCode(ac6a.getAccountCode());
            cf6a.setAccountName(ac6a.getAccountName());
            cf6a.setAmount(provisi2);
            cf6a.setAmountBase(cf6a.getAmount());
            cf6a.setCurrencyId(defCurr.getCurrencyId());
            cf6a.setCurrencyRate(bd_ONE);
            cf6a.setDebitCredit(GlAttributes.i_CREDIT);
            cf6a.setLocationId(oLoc.getLocationId());
            cf6a.setDepartmentId("");
            cf6a.setProjectId("");
            cf6a.setMemo("");
            cf6a.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.02.02",null));

            vDet.add(cf6a);        
            vDet.add(cf6);
        }

/*
 1.13.04.03.00   Piutang Amort PK-By Survey  9,357.00    
         4.10.02.03.00         Pendapatan By Survey Amort PK        9,357.00 

 */        

        if (survey2 != null && survey2.doubleValue() != 0) //biaya survey
        {
            CashFlowJournal cf7 = new CashFlowJournal();
            Account ac7 = AccountTool.getAccountByCode("1.13.04.03.00");
            cf7.setAccountId(ac7.getAccountId());
            cf7.setAccountCode(ac7.getAccountCode());
            cf7.setAccountName(ac7.getAccountName());
            cf7.setAmount(new BigDecimal(survey2.doubleValue() * -1));
            cf7.setAmountBase(cf7.getAmount());
            cf7.setCurrencyId(defCurr.getCurrencyId());
            cf7.setCurrencyRate(bd_ONE);
            cf7.setDebitCredit(GlAttributes.i_CREDIT);
            cf7.setLocationId(oLoc.getLocationId());
            cf7.setDepartmentId("");
            cf7.setProjectId("");
            cf7.setMemo("");
            cf7.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.02.03",null));
            
            CashFlowJournal cf7a = new CashFlowJournal();
            Account ac7a = AccountTool.getAccountByCode("4.10.02.03.00");
            cf7a.setAccountId(ac7a.getAccountId());
            cf7a.setAccountCode(ac7a.getAccountCode());
            cf7a.setAccountName(ac7a.getAccountName());
            cf7a.setAmount(survey2);
            cf7a.setAmountBase(cf7a.getAmount());
            cf7a.setCurrencyId(defCurr.getCurrencyId());
            cf7a.setCurrencyRate(bd_ONE);
            cf7a.setDebitCredit(GlAttributes.i_CREDIT);
            cf7a.setLocationId(oLoc.getLocationId());
            cf7a.setDepartmentId("");
            cf7a.setProjectId("");
            cf7a.setMemo("");
            cf7a.setCashFlowTypeId(CashFlowTypeTool.getIDByCode("8.11.01.02.03",null));
            
            vDet.add(cf7a);
            vDet.add(cf7);
        }
        

        vDet.add(cf4);
        vDet.add(cf3);
        vDet.add(cf2);
        vDet.add(cf1);        
    }
    

    private BigDecimal parseNumber(String _sField, String _sNumber) 
        throws Exception
    {
        String sResult = "";
        log.debug("** PARSE NUMBER " + _sField + " " + _sNumber);
        if (StringUtil.isNotEmpty(_sNumber))
        {
            boolean bDecimal = false;
            sResult = StringUtil.replace(_sNumber,",00","");
            sResult = StringUtil.replace(sResult,"Rp. ","");
            sResult = StringUtil.replace(sResult,".","");
            if (StringUtil.contains(sResult,","))
            {
                sResult = StringUtil.replace(sResult,",",".");
                bDecimal = true;
            }
            log.debug("** PARSE NUMBER " + _sField + " " + sResult);
            try
            {
                BigDecimal bdResult = new BigDecimal(sResult);  
                if(!bDecimal)
                {
                    bdResult = bdResult.setScale(0,4);
                }
                else
                {
                    bdResult = bdResult.setScale(2,4);                    
                }
                return bdResult;
            }
            catch (Exception e)
            {
                logError(_sField,_sNumber);
            }
        }
        return bd_ZERO;
    }

    public boolean isExists()
        throws Exception
    {        
        if (StringUtil.isNotEmpty(sTransNo))
        {
            Criteria oCrit = new Criteria();
            oCrit.add(CashFlowPeer.TRANSACTION_NO, sTransNo);
            oCrit.add(CashFlowPeer.STATUS, i_PROCESSED);            
            List vData = CashFlowPeer.doSelect(oCrit);
            if (vData.size() > 0) 
            {
                return true;
            }
        }
        return false;
    }
}
