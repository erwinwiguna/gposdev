package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * 
 * </pre><br>
 */
public class CashManagementCustomLoader2 extends TransactionLoader
{    
    private static Log log = LogFactory.getLog (CashManagementCustomLoader2.class);
    private static final String s_START_HEADING = "nopk"; 
    
    public CashManagementCustomLoader2 ()
    {
    }
    
    public CashManagementCustomLoader2 (String _sUserName)
    {
        m_sUserName = _sUserName + " from (Excel)";
    }
    
    Currency defCurr = null;
    HSSFSheet oSheet = null;
    CashFlow oTR = null;
    List vTD = null;
    
    String sTransNo = "";
    /**
     * load data from input stream
     * 
     * @param _oInput
     * @throws Exception
     */
    public void loadData (InputStream _oInput)  
        throws Exception
    {
        POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
        HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
        oSheet = oWB.getSheetAt(0);
        m_iTotalRows = oSheet.getPhysicalNumberOfRows();
        boolean bTransHeader = false;
        defCurr = CurrencyTool.getDefaultCurrency();
        
        for (int iRow = 1; iRow < m_iTotalRows; iRow++)
        {
            HSSFRow oRow = oSheet.getRow(iRow);
            if (oRow != null)
            {
                sTransNo = getString(oRow, i_START);
                if (StringUtil.isNotEmpty(sTransNo))
                {                   
                    log.debug("* TRANS NO: " + sTransNo);               

                    m_bTransError = false;
                    m_iTotalTrans++;

                    oTR = new CashFlow();
                    vTD = new ArrayList();

                    mapTrans (oRow, oTR, vTD);
                    
                    log.debug("* oTR " + oTR);
                    log.debug("* vTD " + vTD);
                    
                    if (!m_bTransError)
                    {
                        try
                        {
                            CashFlowTool.setHeaderProperties(oTR, vTD, null, true);
                            CashFlowTool.saveData(oTR, vTD);
                            logSuccess();
                        }
                        catch (Exception _oEx)
                        {
                            String sMsg = _oEx.getMessage();
                            log.error(_oEx);
                            logSaveError(sMsg);
                        }
                    }
                    else
                    {
                        logReject();
                    }
                    m_bTransError = false;
                }
                else 
                {
                    logInvalidFile();
                    return;
                }
            }
        } 
    }   
    
    protected void mapTrans (HSSFRow row, CashFlow cf, List vDet)
        throws Exception
    {
        /*
         * nopk 
         * kodecbg 
         * tglcair 
         * nama    
         * merk    
         * tipe    
         * tahun   
         * tenor   
         * potstnk 
         * pencairan   
         * Total BCA         
         * 
         */
        int col = 0;

        String nopk       = getString (row, col); col++; 
        String lcod       = getString (row, col); col++; 
        String kodebank   = getString (row, col); col++; 
        String tglcair    = getString (row, col); col++;         
        String nama       = getString (row, col); col++; 
        String merk       = getString (row, col); col++;
        String tipe       = getString (row, col); col++;        
        String tahun      = getString (row, col); col++; 
        String tenor      = getString (row, col); col++; 
        
        BigDecimal stnk    = getBigDecimal(row, col); col++;
        BigDecimal cair    = getBigDecimal(row, col); col++;
        BigDecimal survey2 = getBigDecimal(row, col); col++;
        
        System.out.println("nopk " + nopk);
        System.out.println("lcod " + lcod);
        System.out.println("nama " + nama);
        System.out.println("nbrg " + merk);

        sTransNo = nopk;
        if (isExists()) logError("Trans Already Exist", sTransNo);

        Location oLoc = LocationTool.getLocationByCode(lcod);

        Bank bank = BankTool.getBankByCode(kodebank);
        if(bank == null) logError("Invalid Bank ", kodebank);
        
        String transNo = nopk;
        String accCode = "";
        String cfCode = "";
        String sTRCD = "AP";
        BigDecimal amt = bd_ZERO;
        if (stnk.doubleValue() != 0)
        {
            sTRCD = "ST";
            amt = stnk;
            transNo = nopk + sTRCD;
            accCode = "2.15.04.00.00";
            cfCode = "8.12.14.04.00";
        }
        if (cair.doubleValue() != 0)
        {
            sTRCD = "AP";
            amt = cair;
            transNo = nopk + sTRCD;
            accCode = "2.12.01.00.00";
            cfCode = "8.12.01.01.00";
        }
        
        //Byr AP 001/TF/0017/12 a/n Lukman Syarifuddin (Honda Supra 2005 Tenor 06) 3,075,000
        String desc = "Byr " + sTRCD + " " + nopk + " a/n " + nama + 
                      " (" +  merk + " " +  tipe + " " + tahun + " Tenor " + tenor + ") " + amt;
         
        cf.setCashFlowNo(transNo);
        cf.setReferenceNo(nopk);
        cf.setBankId(bank.getBankId());
        cf.setBankIssuer(nopk);        
        cf.setDescription(desc);
        cf.setExpectedDate(CustomParser.parseDate(tglcair));
        cf.setDueDate(cf.getExpectedDate());
        cf.setStatus(i_PROCESSED);
        cf.setUserName("");
        cf.setCurrencyRate(bd_ONE);
        cf.setCurrencyId(defCurr.getCurrencyId());
        cf.setCashFlowType(i_WITHDRAWAL);
        cf.setTransactionType(i_CF_NORMAL);
        cf.setTransactionId("");
        cf.setTransactionNo("");
        cf.setLocationId(oLoc.getLocationId());        
        //amount
        cf.setCashFlowAmount(amt);
        cf.setAmountBase(cf.getCashFlowAmount());        
        cf.setJournalAmount(cf.getCashFlowAmount());

        
        CashFlowJournal cf1 = new CashFlowJournal();
        Account ac1 = AccountTool.getAccountByCode(accCode); //hutang
        cf1.setAccountId(ac1.getAccountId());
        cf1.setAccountCode(ac1.getAccountCode());
        cf1.setAccountName(ac1.getAccountName());
        cf1.setAmount(amt);
        cf1.setAmountBase(cf1.getAmount());
        cf1.setCurrencyId(defCurr.getCurrencyId());
        cf1.setCurrencyRate(bd_ONE);
        cf1.setDebitCredit(GlAttributes.i_CREDIT);
        cf1.setLocationId(oLoc.getLocationId());
        cf1.setDepartmentId("");
        cf1.setProjectId("");
        cf1.setMemo("");
        cf1.setCashFlowTypeId(CashFlowTypeTool.getIDByCode(cfCode,null));

        vDet.add(cf1);        
    }
    
    private BigDecimal parseNumber(String _sField, String _sNumber) 
        throws Exception
    {
        String sResult = "";
        log.debug("** PARSE NUMBER " + _sField + " " + _sNumber);
        if (StringUtil.isNotEmpty(_sNumber))
        {
            boolean bDecimal = false;
            sResult = StringUtil.replace(_sNumber,",00","");
            sResult = StringUtil.replace(sResult,"Rp. ","");
            sResult = StringUtil.replace(sResult,".","");
            if (StringUtil.contains(sResult,","))
            {
                sResult = StringUtil.replace(sResult,",",".");
                bDecimal = true;
            }
            log.debug("** PARSE NUMBER " + _sField + " " + sResult);
            try
            {
                BigDecimal bdResult = new BigDecimal(sResult);  
                if(!bDecimal)
                {
                    bdResult = bdResult.setScale(0,4);
                }
                else
                {
                    bdResult = bdResult.setScale(2,4);                    
                }
                return bdResult;
            }
            catch (Exception e)
            {
                logError(_sField,_sNumber);
            }
        }
        return bd_ZERO;
    }

    public boolean isExists()
        throws Exception
    {        
        if (StringUtil.isNotEmpty(sTransNo))
        {
            Criteria oCrit = new Criteria();
            oCrit.add(CashFlowPeer.TRANSACTION_NO, sTransNo);
            oCrit.add(CashFlowPeer.STATUS, i_PROCESSED);            
            List vData = CashFlowPeer.doSelect(oCrit);
            if (vData.size() > 0) 
            {
                return true;
            }
        }
        return false;
    }
}
