package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashFlowLoader.java,v 1.1 2009/05/04 01:38:37 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashFlowLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class CashManagementLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( CashManagementLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Account Code";
	private static final String s_END_TRANS = "End Trans";	
	
	public CashManagementLoader()
	{		
	}
	
	public CashManagementLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		
		CashFlow oTR = null;
		List vTD = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new CashFlow();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{    				
    				CashFlowJournal oTD = new CashFlowJournal();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}    			
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			//CashFlowTool.setHeaderProperties(oTR, vTD, null);
    		    			//calculate CF amount
    		    			calculateCFAmount (oTR, vTD);
    		    			CashFlowTool.saveData(oTR, vTD, true, null);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			_oEx.printStackTrace();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    /**
     * 
     * @param _oTR
     * @param _vTD
     */
    protected void calculateCFAmount(CashFlow _oTR, List _vTD) 
    {
    	double dCFAmount = 0;
    	for (int i = 0; i < _vTD.size(); i++)
    	{
    		CashFlowJournal oCFD = (CashFlowJournal) _vTD.get(i);
    		dCFAmount = dCFAmount + oCFD.getAmount().doubleValue(); 
    	}
    	double dAmountBase = dCFAmount * _oTR.getCurrencyRate().doubleValue();
    	_oTR.setCashFlowAmount(new BigDecimal(dCFAmount));
    	_oTR.setAmountBase(new BigDecimal(dAmountBase));
    	_oTR.setJournalAmount(_oTR.getAmountBase());
    	_oTR.setSayAmount(CustomFormatter.numberToStr(PreferenceTool.getLocale(), dCFAmount));
	}

    /**
     * 
     * @param _oRow
     * @param _oTR
     * @throws Exception
     */
	protected void mapTrans (HSSFRow _oRow, CashFlow _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}

    	String sBankCode = getString(_oRow, iCol); iCol++;
    	Bank oBank = BankTool.getBankByCode(sBankCode);
    	if (oBank == null) {logError("Bank Code", sBankCode);}
    	else 
    	{
    		_oTR.setBankId(oBank.getBankId()); 
    	}

    	String sType = getString(_oRow, iCol); iCol++;
    	int iType = -1;    	
    	if (StringUtil.isNotEmpty(sType))
    	{
    		if (sType.equals("Deposit"))
    		{
    			iType = GlAttributes.i_DEPOSIT; 
    		}
    		else if (sType.equals("Withdrawal"))
    		{
    			iType = GlAttributes.i_WITHDRAWAL; 
    		}
    	}
    	if (iType != GlAttributes.i_DEPOSIT && iType != GlAttributes.i_WITHDRAWAL)
    	{logError("Trans Type", sType);}
    	_oTR.setCashFlowType(iType);
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setExpectedDate(dTrans);}

    	Date dDue = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dDue == null) {logError("Due Date", dDue);}
    	else {_oTR.setDueDate(dDue);}

    	String sRefNo = getString(_oRow, iCol); iCol++;
    	_oTR.setReferenceNo(sRefNo);

    	String sIssuer = getString(_oRow, iCol); iCol++;
    	_oTR.setBankIssuer(sIssuer);

    	String sUserName = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sUserName)) {logError("Create By", sUserName);}
    	else {_oTR.setUserName(sUserName);}
		
    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setDescription(sRemark);
    	
    	//currency
    	String sCurr = getString(_oRow, iCol); iCol++;    	
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurr);
    	if (oCurr != null)
    	{
    		_oTR.setCurrencyId(oCurr.getCurrencyId());
    		if (oCurr.getIsDefault())
    		{
    	    	_oTR.setCurrencyRate(bd_ONE); 			
    		}
    		else
    		{
    			BigDecimal bdRate = getBigDecimal(_oRow, iCol); iCol++;
    			if (bdRate == null || bdRate.doubleValue() <= 0) {logError("Currency Rate", bdRate);}
    			else {_oTR.setCurrencyRate(bdRate);}
    		}
    	}
    	else //user default currency
    	{
    		_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
	    	_oTR.setCurrencyRate(bd_ONE);
    	}
    	
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
    		if (_oTR != null)
    		{
    			if (DateUtil.isAfter(_oTR.getDueDate(), new Date()))
    			{
    				_oTR.setStatus (i_PAYMENT_RECEIVED);                    
    			}
    			else
    			{
    				_oTR.setStatus (i_PAYMENT_PROCESSED);                    
    			}
    		}
    		_oTR.setTransactionType(AppAttributes.i_CF_NORMAL);
	    	_oTR.setDescription(_oTR.getDescription() + setDesc(sTransNo, m_sUserName));
    	}
    }
    
	/**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @throws Exception
	 */
	protected void mapDetails (HSSFRow _oRow, CashFlow _oTR, CashFlowJournal _oTD)
		throws Exception
	{    	
    	mapDetails(_oRow,_oTR,_oTD, 0);
	}
	
	/**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @param _iStartCol
	 * @throws Exception
	 */
    protected void mapDetails (HSSFRow _oRow, CashFlow _oTR, CashFlowJournal _oTD, int _iStartCol)
		throws Exception
	{    	
    	int iCol = _iStartCol;
    	String sAccCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("account_code")).append(": ").append(sAccCode).append(s_LINE_SEPARATOR);
    	
    	Account oAcc = AccountTool.getAccountByCode(sAccCode);
    	if (oAcc == null) {logError("Account Code", sAccCode);}
    	else 
    	{
    		if (oAcc.getHasChild()) logInvalidAcc(sAccCode);
    		_oTD.setAccountId(oAcc.getAccountId());
    		_oTD.setAccountCode(oAcc.getAccountCode());
    		_oTD.setAccountName(oAcc.getAccountName());
    	}
    	
    	String sCurrCode = getString (_oRow, iCol); iCol++;
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrCode);
    	if (oCurr == null) {logError("Currency Code", sCurrCode);}
    	else 
    	{
    		_oTD.setCurrencyId(oCurr.getCurrencyId());
    	}

    	System.out.println("ROW: " + _oRow.getRowNum() + "COL " + iCol + " " + getString (_oRow, iCol));
    	BigDecimal dRate = getBigDecimal (_oRow, iCol); iCol++;
    	if (dRate == null) {logError("Currency Rate", dRate);}
    	else 
    	{
    		_oTD.setCurrencyRate(dRate);
    	}
    	
    	if (oCurr.getIsDefault() && dRate.doubleValue() != 1) 
    	{
    		logError("Default currency and rate not match ", oCurr.getCurrencyCode() + " : " + dRate);
    	}

    	BigDecimal dAmount = getBigDecimal (_oRow, iCol); iCol++;
    	_oTD.setAmount(dAmount);
    	    	
    	String sMemo= getString(_oRow, iCol); iCol++;
    	_oTD.setMemo(sMemo);

    	String sCFTCode = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sCFTCode))
    	{
	    	CashFlowType oCashFlowType = CashFlowTypeTool.getTypeByCode(sCFTCode, null);
	    	if (oCashFlowType == null) {logError("CashFlowType Code", sCFTCode);}
	    	else
	    	{
	    		_oTD.setCashFlowTypeId(oCashFlowType.getCashFlowTypeId());
	    	}
    	}
    	else
    	{
    		_oTD.setCashFlowTypeId("");
    	}
    	
    	String sLocationCode = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sLocationCode))
    	{
	    	Location oLocation = LocationTool.getLocationByCode(sLocationCode);
	    	if (oLocation == null) {logError("Location Code", sLocationCode);}
	    	else
	    	{
	    		_oTD.setLocationId(oLocation.getLocationId());
	    	}
    	}
    	else
    	{
    		_oTD.setLocationId("");
    	}
    	
    	String sProjectCode = getString(_oRow, iCol); iCol++;    	
    	_oTD.setProjectId("");
      	
    	String sDepartmentCode = getString(_oRow, iCol); iCol++;
    	_oTD.setDepartmentId("");
    	    	
    	if (!m_bTransError)
    	{
    		double dAmountBase = _oTD.getAmount().doubleValue() * _oTD.getCurrencyRate().doubleValue();    		
    		_oTD.setAmountBase(new BigDecimal(dAmountBase));
    		_oTD.setSubLedgerId("");
    		_oTD.setSubLedgerType(-1);
    		
            //if a CASH/BANK DEPOSIT then whe should credit any other account in journal
            if (_oTR.getCashFlowType() == i_DEPOSIT) 
            {
		        _oTD.setDebitCredit  (GlAttributes.i_CREDIT); 
            }		
            else //if CASH/BANK WITHDRAWAL we should debit any other account
            {
		        _oTD.setDebitCredit  (GlAttributes.i_DEBIT);             
            }
    	}
	}   
}
