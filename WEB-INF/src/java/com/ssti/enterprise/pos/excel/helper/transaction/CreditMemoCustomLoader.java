package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.2  2008/03/03 02:21:00  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/07/02 15:37:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CreditMemoCustomLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( CreditMemoCustomLoader.class );

	private static final String s_START_HEADING = "ROW_NUM";	

	public CreditMemoCustomLoader ()
	{
	}
	
	public CreditMemoCustomLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		
		CreditMemo oTR = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader)
    			{
    				bTransHeader = true;    				
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && !sValue.equals(s_START_HEADING) && bTransHeader)
    			{    				
    				m_bTransError = false;    				
    				m_iTotalTrans++;
    				
    				oTR = new CreditMemo();
    				mapTrans (oRow, oTR);

    				log.debug("ROW : " + iRow + " TR " + oTR);
    				
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			CreditMemoTool.saveCM(oTR, null);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;	
    			}
    			else 
    			{
    				logInvalidFile();
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, CreditMemo _oTR)
    	throws Exception
    {    	
    	int ROW_NUM	        = 0;
    	int MESSAGEID	    = 1;
    	int REQUESTID	    = 2;
    	int TIME_START	    = 3;
    	int TIME_FINISH	    = 4;
    	int NOM	            = 5;
    	int PRICE	        = 6;
    	int QTY	            = 7;
    	int TUJUAN	        = 8;
    	int PENGISI	        = 9;
    	int STORE_ID	    = 10;
    	int ENDING_BALANCE	= 11;
    	int USER_NAME	    = 12;
    	int FULL_NAME	    = 13;
    	int TRANS_STAT	    = 14;
    	int KET	            = 15;
    	int BERITA          = 16;
    	
    	int iStat = Integer.valueOf(getString(_oRow, TRANS_STAT));
    	
    	if (iStat == 901)
    	{    	
	    	String sTransNo = getString (_oRow, MESSAGEID); 
	    	m_oResult.append("MESSAGE_ID").append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
	    	m_oResult.append("STAT").append(": ").append(iStat).append(s_LINE_SEPARATOR);
	    	
	    	String sCustomerCode = getString(_oRow, TUJUAN).toString(); 
	    	Customer oCust = CustomerTool.getCustomerByCode(sCustomerCode);
	    	m_oResult.append("CUSTOMER(TUJUAN)").append(": ").append(sCustomerCode).append(s_LINE_SEPARATOR);
	    	if (oCust == null) {logError("Customer Code", sCustomerCode);}
	    	else 
	    	{
	    		_oTR.setCustomerId(oCust.getCustomerId()); 
	    		_oTR.setCustomerName(oCust.getCustomerName());
	    	}
	
	    	Date dTrans = CustomParser.parseCustom(getString(_oRow, TIME_FINISH),"dd-MM-yy hh:mm:ss"); 
	    	m_oResult.append("TIME_FINISH").append(": ").append(dTrans).append(s_LINE_SEPARATOR);
	    	if (dTrans == null) {logError("Transaction Date", dTrans);}
	    	else {_oTR.setTransactionDate(dTrans);}
	    	
	        _oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
	        _oTR.setCurrencyRate(bd_ONE);
	        _oTR.setFiscalRate(bd_ONE);
	
	    	BigDecimal dAmount = getBigDecimal(_oRow, PRICE); 
	    	m_oResult.append("PRICE").append(": ").append(dAmount).append(s_LINE_SEPARATOR);
	    	if (dAmount == null || dAmount.doubleValue() == 0) {logError("Amount", dAmount);}
	    	else {_oTR.setAmount(dAmount);}
	    	    	
	    	String sCreateBy = getString(_oRow, USER_NAME); 
	    	if (StringUtil.isEmpty(sCreateBy)) {logError("Create By", sCreateBy);}
	    	else {_oTR.setUserName(sCreateBy);}
			
	    	String sRemark = getString(_oRow, KET); 
	    	m_oResult.append("KET").append(": ").append(sRemark).append(s_LINE_SEPARATOR);
	    	if (sRemark != null) _oTR.setRemark(sRemark + " STAT:" + iStat); else _oTR.setRemark("");
	
	    	String sBankCode = getString(_oRow, PENGISI); 
	    	m_oResult.append("PENGISI").append(": ").append(dAmount).append(s_LINE_SEPARATOR);
	    	if (StringUtil.isNotEmpty(sBankCode))
	    	{
		    	Bank oBank = BankTool.getBankByCode(sBankCode);
		    	if (oBank == null) {logError("Bank Code", sBankCode);} 
		    	else {_oTR.setBankId(oBank.getBankId());}
	    	}
	    	else
	    	{
	    		_oTR.setBankId("");
	    	}
	    	
	    	String sAccount = GLConfigTool.getGLConfig().getCreditMemoAccount(); 
	    	Account oAccount = AccountTool.getAccountByID(sAccount);
	    	if (oAccount == null) {logError("Account", sAccount);}  
	    	else{_oTR.setAccountId(oAccount.getAccountId());}    	
	
	    	_oTR.setCreditMemoNo("");
	        _oTR.setCrossAccountId("");
	    	_oTR.setCashFlowTypeId("");
	    	_oTR.setBankIssuer("-");
	    	_oTR.setReferenceNo("-");
	    	_oTR.setDueDate(_oTR.getTransactionDate());

	    	//OTHER FIELD
	    	if (!m_bTransError)
	    	{
	    		double dAmountBase = _oTR.getAmount().doubleValue() * _oTR.getCurrencyRate().doubleValue();
	    		_oTR.setAmountBase(new BigDecimal(dAmountBase));
	    		
		    	_oTR.setStatus(i_MEMO_NEW);	    
		    	_oTR.setRemark(_oTR.getRemark() + setDesc(sTransNo, m_sUserName));
	    	}
    	}
    }
}
