package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;

public class GLTrans2JVHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Journal Voucher Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sUser = (String)getFromParam(oParam, "Username", null);
        String sTransNo = (String)getFromParam(oParam, "TransactionNo", null);
        String sSrc = (String)getFromParam(oParam, "src", null);
        String sSubID = (String)getFromParam(oParam, "id", null);
        
        
        List vData = GlTransactionTool.findData(-1, -1, "", sSrc, sSubID, sTransNo, "", dStart, dEnd);        
        int iRow = 0;
        
        HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
    	createMasterHeader(oRow);
    	
    	JournalVoucher oTR = new JournalVoucher();
    	oTR.setTransactionNo("");
    	oTR.setTransactionDate(new Date());
    	oTR.setUserName(sUser);
    	oTR.setDescription("");
    	
    	oRow = oSheet.createRow(iRow); iRow++;
    	createMasterContent(oRow, oTR);

		oRow = oSheet.createRow(iRow); iRow++;
		createDetailHeader(oRow);
    	
        for (int i = 0; i < vData.size(); i++)
        {
        	GlTransaction oTD = (GlTransaction) vData.get(i);
    		
        	oRow = oSheet.createRow(iRow); iRow++;
        	createDetailContent(oRow, oTD);            	        		
        }		
    	oRow = oSheet.createRow(iRow); iRow++;
    	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Transaction Date	
    	Remark	
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;	
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;			
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		
    }
    
	private void createMasterContent(HSSFRow oRow, JournalVoucher oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getDescription()); iCol++;
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Account Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Rate"); iCol++;
    	createHeaderCell(oWB, oRow, (short)iCol, "Debit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Credit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Memo"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Project Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Department Code"); iCol++;		
    }
    
    private void createDetailContent(HSSFRow oRow, GlTransaction oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, AccountTool.getCodeByID(oTD.getAccountId())); iCol++;
		createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTD.getCurrencyId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getCurrencyRate().toString()); iCol++;
		
		if (oTD.getDebitCredit() == GlAttributes.i_DEBIT)
		{
			createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		}
		else
		{
			createCell(oWB, oRow, (short)iCol, bd_ZERO.toString()); iCol++;
		}
		if (oTD.getDebitCredit() == GlAttributes.i_CREDIT)
		{
			createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		}
		else
		{
			createCell(oWB, oRow, (short)iCol, bd_ZERO.toString()); iCol++;
		}
		createCell(oWB, oRow, (short)iCol, oTD.getTransactionNo() + " " + oTD.getDescription()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTD.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, ""); iCol++;
		createCell(oWB, oRow, (short)iCol, ""); iCol++;
		createCell(oWB, oRow, (short)iCol, AccountTool.getAccountNameByID(oTD.getAccountId(),null)); iCol++;
    }
}
