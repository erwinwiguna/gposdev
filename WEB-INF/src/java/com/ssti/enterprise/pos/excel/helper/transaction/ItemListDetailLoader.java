package com.ssti.enterprise.pos.excel.helper.transaction;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.excel.helper.ItemListLoader;
import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class ItemListDetailLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (ItemListDetailLoader.class);
    private String sLocationID;
    private boolean bForceZero = false;
    
    
    public ItemListDetailLoader(String _sLocationID)
        throws Exception    
    {
    	sLocationID = _sLocationID;
    }

    protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		ItemList oILD = new ItemList();
		String sCode = getString(_oRow,_iIdx + i); i++;
				
		String sName = getString(_oRow,_iIdx + i);		
		double dQty = getBigDecimal(_oRow,_iIdx + 5).doubleValue();
		
		Item oItem = ItemTool.getItemByCode(sCode);        
        if(oItem != null)
        {
            oILD.setItemId(oItem.getItemId());
            oILD.setItemCode(oItem.getItemCode());
            oILD.setItemName(oItem.getItemName());
            oILD.setDescription(oItem.getDescription());
            oILD.setItem(oItem);
            String sUnitID = oItem.getUnitId();
        	Unit oUnit = UnitTool.getUnitByID(sUnitID);
        	boolean bBase = true;
        	
            if(oUnit != null)
            {            
            	if (StringUtil.isEqual(oUnit.getUnitId(),oItem.getPurchaseUnitId()))
            	{
            		bBase = false;
            		oUnit = UnitTool.getUnitByID(oItem.getUnitId());
            		dQty = dQty * oItem.getUnitConversion().doubleValue();
            	}            	
                oILD.setUnitId(oUnit.getUnitId());
                oILD.setUnitCode(oUnit.getUnitCode());
            }
            else
            {
            	setError (iRow, sCode, sName, "Item contains invalid Unit ");
            }    
			oILD.setQty(new BigDecimal(dQty));		   
			m_vDataList.add(oILD);
        }
        else
        {
        	setError (iRow, sCode, sName, "Item Not Found");
        }
    }        
    
    void setError (int _iRow, String _sCode, String _sName, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append (_iRow + 1);
		oMsg.append (",ITEM CODE, " + _sCode );
		oMsg.append (",ITEM NAME, " + _sName );
		oMsg.append (_sMsg);
		oMsg.append ("\n");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}
