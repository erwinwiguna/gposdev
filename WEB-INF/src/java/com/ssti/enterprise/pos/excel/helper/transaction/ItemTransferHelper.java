package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class ItemTransferHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Item Transfer Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
        String[] aStart = (String[]) oParam.get("StartDate");  
        String[] aEnd  = (String[]) oParam.get("EndDate");          
        String[] aStatus  = (String[]) oParam.get("Status");  
        
        Date dStart = null;
        Date dEnd = null;
        int iStatus = -1;
        
        if (aStart != null && aStart.length > 0) dStart = CustomParser.parseDate(aStart[0]);
        if (aEnd != null && aEnd.length > 0) dEnd = DateUtil.getEndOfDayDate(CustomParser.parseDate(aEnd[0]));       
        if (aStatus != null && aStatus.length > 0) iStatus = Integer.valueOf(aStatus[0]);        
        String sFrom = StringUtil.getString(oParam,"FromLocationId");
        String sTo = StringUtil.getString(oParam,"ToLocationId");
        
        List vData = ItemTransferTool.findData(dStart, dEnd, sFrom, sTo, "", iStatus); 
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	ItemTransfer oTR = (ItemTransfer) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = ItemTransferTool.getDetailsByID(oTR.getItemTransferId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		ItemTransferDetail oTD = (ItemTransferDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Transaction Date	
    	Remark	
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;	
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;			
		createHeaderCell(oWB, oRow, (short)iCol, "Adjustment Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "From Location"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "To Location"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		
    }
    
	private void createMasterContent(HSSFRow oRow, ItemTransfer oTR) 
		throws Exception
	{
		int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
		createCell(oWB, oRow, (short)iCol, AdjustmentTypeTool.getCodeByID(oTR.getAdjustmentTypeId())); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getFromLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getToLocationId())); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTR.getDescription()); iCol++;
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cost"); iCol++;
    }
    
    private void createDetailContent(HSSFRow oRow, ItemTransferDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getQtyChanges().toString()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getCostPerUnit().toString()); iCol++;
    }
}
