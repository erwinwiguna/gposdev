package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.enterprise.pos.tools.gl.SubLedgerTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;

public class JournalVoucherDetailHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Journal Voucher Worksheet");    
	
    private int trans = 0;
    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
    	List vData = (List)_oSession.getAttribute("vJV");
        int iRow = 0;
		HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        createHeader(oRow);            	        		

        for (int i = 0; i < vData.size(); i++)
        {
        	trans = i;
        	
        	JournalVoucher oTR = (JournalVoucher) vData.get(i);        	
        	List vTD = JournalVoucherTool.getDetailsByID(oTR.getJournalVoucherId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		JournalVoucherDetail oTD = (JournalVoucherDetail) vTD.get(j);
        		oRow = oSheet.createRow(iRow); iRow++;        	
            	createContent(oRow, oTR, oTD);            	        		
        	}
        }		
        return oWB;
    }
    
    private void createHeader(HSSFRow oRow)
    {
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Trans Date"); iCol++;	
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;			
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
    	createHeaderCell(oWB, oRow, (short)iCol, "Account Code"); iCol++;
    	createHeaderCell(oWB, oRow, (short)iCol, "Account Name"); iCol++;    
		createHeaderCell(oWB, oRow, (short)iCol, "Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Project"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Department"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "SubLedger Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "SubLedger Name"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "SubLedger Desc"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Reference No"); iCol++;		
    	createHeaderCell(oWB, oRow, (short)iCol, "Currency"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Rate"); iCol++;
    	createHeaderCell(oWB, oRow, (short)iCol, "Debit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Credit"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Amount Base"); iCol++;				
    }
    
	private void createContent(HSSFRow oRow, JournalVoucher oTR, JournalVoucherDetail oTD) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getDescription()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getAccountCode()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getAccountName()); iCol++;    	
		createCell(oWB, oRow, (short)iCol, oTD.getMemo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTD.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, ""); iCol++;
		createCell(oWB, oRow, (short)iCol, ""); iCol++;		
		createCell(oWB, oRow, (short)iCol, SubLedgerTool.getSLDesc(oTD.getSubLedgerType())); iCol++;				
		createCell(oWB, oRow, (short)iCol, SubLedgerTool.getSLName(oTD.getSubLedgerType(),oTD.getSubLedgerId())); iCol++;
		createCell(oWB, oRow, (short)iCol, SubLedgerTool.getSLCode(oTD.getSubLedgerType(),oTD.getSubLedgerId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getReferenceNo()); iCol++;		
    	createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTD.getCurrencyId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getCurrencyRate().toString()); iCol++;		
		if (oTD.getDebitCredit() == GlAttributes.i_DEBIT)
		{
			createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		}
		else
		{
			createCell(oWB, oRow, (short)iCol, bd_ZERO.toString()); iCol++;
		}
		if (oTD.getDebitCredit() == GlAttributes.i_CREDIT)
		{
			createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		}
		else
		{
			createCell(oWB, oRow, (short)iCol, bd_ZERO.toString()); iCol++;
		}
		createCell(oWB, oRow, (short)iCol, oTD.getAmountBase().toString()); iCol++;				
	}    
	
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, String _sValue)
    {                                                                                      
        HSSFCell oCell = _oRow.createCell(_iColumn);                                            
        oCell.setCellValue(_sValue);  
        
        if (trans % 2 != 0)
        {
            HSSFCellStyle oStyle = _oWB.createCellStyle();                                    
            oStyle.setFillForegroundColor(HSSFColor.AQUA.index);
            oStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);   
            oStyle.setWrapText(false);
            oCell.setCellStyle(oStyle);        	
        }	                                                   
    }
}
