package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseRequest;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class ProcRequestHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Item Request Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
		throws Exception
	{        
		int iRow = 0;
		
		String sRID = (String)getFromParam(oParam, "id", String.class);
		PurchaseRequest oTR = PurchaseRequestTool.getHeaderByID(sRID);
		
		if (oTR != null)
		{
	    	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
	    	//#set($headings = ["Item Code","Item Name","Request Qty","Unit","Trans No","Remark"])

	    	
	    	int iCol = 0;
			createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Item Name"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Request Qty"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Unit"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;		
			createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Last Disc"); iCol++;
	    	
	    	List vTD = PurchaseRequestTool.getDetailsByID(oTR.getPurchaseRequestId());        	
	    	for (int j = 0; j < vTD.size(); j++)
	    	{
	    		PurchaseRequestDetail oTD = (PurchaseRequestDetail) vTD.get(j);
	        	iCol = 0;
	        	oRow = oSheet.createRow(iRow); iRow++;
	        	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
	    		createCell(oWB, oRow, (short)iCol, oTD.getItemName()); iCol++;
	    		createCell(oWB, oRow, (short)iCol, oTD.getRequestQty()); iCol++;
	    		createCell(oWB, oRow, (short)iCol, UnitTool.getCodeByID(oTD.getUnitId())); iCol++;
	    		createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
	    		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;	    		
	    		createCell(oWB, oRow, (short)iCol, getLastDisc(oTD.getItemId())); iCol++;
	    		
//	    	    <td>$!rqd.ItemCode</td>
//	    	    <td>$!rqd.ItemName</td>
//	    	    <td>$!rqd.RequestQty</td>
//	    	    <td>$!unit.getCodeByID($!rqd.UnitId)</td>    
//	    	    <td>$!rq.TransactionNo</td>
//	    	    <td>$!rq.Remark</td>    
//	    		
	    	}
		}
	    return oWB;
	}
    
    public static String getLastDisc(String _sItemID)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.addJoin(PurchaseInvoicePeer.PURCHASE_INVOICE_ID,PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID);
    	oCrit.add(PurchaseInvoiceDetailPeer.ITEM_ID, _sItemID);
    	oCrit.addDescendingOrderByColumn(PurchaseInvoicePeer.PURCHASE_INVOICE_DATE);
    	oCrit.setLimit(1);
    	List v = PurchaseInvoiceDetailPeer.doSelect(oCrit);
    	if (v.size() > 0)
    	{
    		return ((PurchaseInvoiceDetail)v.get(0)).getDiscount();
    	}
    	return "";
    }
}
