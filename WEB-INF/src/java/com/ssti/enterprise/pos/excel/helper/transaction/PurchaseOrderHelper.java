package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;

public class PurchaseOrderHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Purchase Order Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
        String[] aStart = (String[]) oParam.get("StartDate");  
        String[] aEnd  = (String[]) oParam.get("EndDate"); 
        String[] aLocationID  = (String[]) oParam.get("LocationId");  
        String[] aVendorID  = (String[]) oParam.get("VendorId");          
        String[] aCreateBy  = (String[]) oParam.get("CreateBy");  
        String[] aStatus  = (String[]) oParam.get("Status");  
        
        Date dStart = null;
        Date dEnd = null;
        String sLocationID = "";
        String sVendorID = "";
        String sCreateBy = "";
        int iStatus = -1;
        
        if (aStart != null && aStart.length > 0) dStart = CustomParser.parseDate(aStart[0]);
        if (aEnd != null && aEnd.length > 0) dEnd = CustomParser.parseDate(aEnd[0]);
        if (aLocationID != null && aLocationID.length > 0) sLocationID = aLocationID[0];
        if (aVendorID != null && aVendorID.length > 0) sVendorID = aVendorID[0];        
        if (aCreateBy != null && aCreateBy.length > 0) sCreateBy = aCreateBy[0];
        if (aStatus != null && aStatus.length > 0) iStatus = Integer.valueOf(aStatus[0]);        
        
        List vData = PurchaseOrderTool.findData(-1, "", dStart, dEnd, sVendorID, sLocationID, iStatus);
        
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	PurchaseOrder oTR = (PurchaseOrder) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = PurchaseOrderTool.getDetailsByID(oTR.getPurchaseOrderId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		PurchaseOrderDetail oTD = (PurchaseOrderDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "PO No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Vendor Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Rate"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Term Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "PO Discount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Total Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Inclusive Tax"); iCol++;
    }
    
	private void createMasterContent(HSSFRow oRow, PurchaseOrder oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getPurchaseOrderNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, VendorTool.getVendorByID(oTR.getVendorId()).getVendorCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTR.getCurrencyId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getCurrencyRate().toString()); iCol++;		
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getCreateBy()); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oTR.getPaymentTypeId())); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTermTool.getPaymentTermCodeByID(oTR.getPaymentTermId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTR.getTotalDiscountPct()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getTotalAmount().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, Boolean.valueOf(oTR.getIsInclusiveTax()).toString()); iCol++;

	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Price"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Tax Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Receive Qty"); iCol++;
    }
    
    private void createDetailContent(HSSFRow oRow, PurchaseOrderDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getQty().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getItemPrice().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, TaxTool.getCodeByID(oTD.getTaxId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getDiscount()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getReceivedQty().toString()); iCol++;		
    }
}
