package com.ssti.enterprise.pos.excel.helper.transaction;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.excel.helper.ItemListLoader;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/PurchaseOrderLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseOrderLoader.java,v 1.12 2008/08/17 02:17:21 albert Exp $
 *
 * $Log: PurchaseOrderLoader.java,v $
 * Revision 1.12  2008/08/17 02:17:21  albert
 * *** empty log message ***
 */
public class PurchaseOrderItemLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (PurchaseOrderItemLoader.class);
    private String sLocationID = "";
    
    public PurchaseOrderItemLoader(String _sLocationID)
        throws Exception    
    {
    	sLocationID = _sLocationID;
    }

    PurchaseOrder oPO = null;
    public PurchaseOrderItemLoader(PurchaseOrder _oPO)
        throws Exception    
    {
    	if(_oPO != null)
    	{
    		oPO = _oPO;
    	}
    }
    
    protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		PurchaseOrderDetail oTD = new PurchaseOrderDetail();
		String sCode = getString(_oRow,_iIdx + i);
		String sDesc = getString(_oRow,_iIdx + 2);
        Item oItem = ItemTool.getItemByCode(sCode);
        double dQty = getBigDecimal(_oRow,_iIdx + 5).doubleValue();
        double dQtyBase = dQty;
        double dPrice = getBigDecimal(_oRow,_iIdx + 8).doubleValue();
        String sDisc = getString(_oRow,_iIdx + 9);   
        String sTax = getString(_oRow,_iIdx + 10);
        
        if(oItem != null)
        {
            oTD.setItemId(oItem.getItemId());
            oTD.setItemCode(oItem.getItemCode());
            oTD.setItemName(oItem.getItemName());
            oTD.setDescription(sDesc);
            if (StringUtil.isEmpty(sDesc))
            {
            	oTD.setDescription(oItem.getDescription());
            } 
            String sUnitID = oItem.getPurchaseUnitId();
        	Unit oUnit = UnitTool.getUnitByID(sUnitID);        	
            if(oUnit != null)
            {
            	dQtyBase = dQty * oItem.getUnitConversion().doubleValue();            	
                oTD.setUnitId(oUnit.getUnitId());
                oTD.setUnitCode(oUnit.getUnitCode());
            }
            else
            {
            	setError (iRow, sCode, "Item contains invalid Unit ");
            }    
            oTD.setQty(new BigDecimal(dQty));
            oTD.setQtyBase(new BigDecimal(dQtyBase));                        
            if (dPrice == 0)
            {
            	dPrice = oItem.getLastPurchasePrice().doubleValue();
            	dPrice = dPrice * oItem.getUnitConversion().doubleValue();
            }
            oTD.setItemPrice(new BigDecimal(dPrice));
            if(StringUtil.isEmpty(sDisc))
            {
            	sDisc = "0";            
            	if(oPO != null)
            	{
			        //check price & discount in vendor price list
				    VendorPriceListDetail oVPL = 
				    	VendorPriceListTool.getPriceListDetailByVendorID(oPO.getVendorId(), oItem.getItemId(), new Date());			   
				    			    			    
				    //price list is first priority
				    if(oVPL != null)
				    {
				    	sDisc = oVPL.getDiscountAmount();			    	
				    	if(oVPL.getPurchasePrice().doubleValue() >= 0)
				    	{    		        
				    		oTD.setItemPrice (oVPL.getPurchasePrice());
				    	}
				    }
				    else
				    {
				    	//check item discount in vendor
				    	if(oPO.getVendor() != null && StringUtil.isNotEmpty(oPO.getVendor().getDefaultItemDisc()))
				    	{
				    		sDisc = oPO.getVendor().getDefaultItemDisc();
				    	}			    	
				    }   
            	}
            }
            oTD.setDiscount(sDisc);
        	Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTax)); 	
        	if (oTax == null) oTax = TaxTool.getTaxByID(oItem.getPurchaseTaxId());
        	oTD.setTaxId(oTax.getTaxId());
        	oTD.setTaxAmount(oTax.getAmount());

    		double dSubTotal = oTD.getItemPrice().doubleValue() * oTD.getQty().doubleValue();
    		double dSubTotalDisc = Calculator.calculateDiscount(oTD.getDiscount(), dSubTotal);
    		double dSubTotalTax = oTD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
    		oTD.setSubTotal(new BigDecimal(dSubTotal));
    		oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
    		oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
    		
            double dDiscount = 0;
            //if discount was not % then divide discount with purchase qty 
            if(oTD.getDiscount().contains(Calculator.s_PCT))
            {
                dDiscount = Calculator.calculateDiscount(oTD.getDiscount(),oTD.getItemPrice().doubleValue());
            }
            else
            {
                dDiscount = Calculator.calculateDiscount(oTD.getDiscount(),oTD.getItemPrice().doubleValue()) / oTD.getQty().doubleValue();
            }
            
            double dCostPerUnit = oTD.getItemPrice().doubleValue() - dDiscount;
            dCostPerUnit = dCostPerUnit / oItem.getUnitConversion().doubleValue();
            double dRealCost = dCostPerUnit * 1;
            oTD.setCostPerUnit(new BigDecimal(dRealCost));	
        	oTD.setProjectId("");
        	oTD.setDepartmentId("");
        	
            m_vDataList.add(oTD);                        
        }
        else
        {
        	setError (iRow, sCode, "Item Not Found");
        }
    }        
    
    void setError (int _iRow, String _sCode, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append ("<tr><td>");
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append ("</td>");
		oMsg.append ("<td>");
		oMsg.append (_iRow + 1);
		oMsg.append ("</td>");
		oMsg.append ("<td> ITEM CODE </td><td>" + _sCode + "</td><td>" + _sMsg );
		oMsg.append ("</td></tr>");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}
