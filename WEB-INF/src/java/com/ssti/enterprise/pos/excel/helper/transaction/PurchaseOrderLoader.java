package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseOrderLoader.java,v 1.1 2008/08/20 15:53:28 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseOrderLoader.java,v $
 * Revision 1.1  2008/08/20 15:53:28  albert
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/17 12:57:57  albert
 * *** empty log message ***
 *
 * Revision 1.1  2008/01/29 23:27:38  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/02 15:37:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PurchaseOrderLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( PurchaseOrderLoader.class );

	private static final String s_START_HEADING = "PO No";	
	private static final String s_DETAIL_HEADING = "Item Code";
	private static final String s_END_TRANS = "End Trans";	
	
	public PurchaseOrderLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		
		PurchaseOrder oTR = null;
		List vTD = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new PurchaseOrder();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{    				
    				PurchaseOrderDetail oTD = new PurchaseOrderDetail();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			PurchaseOrderTool.setHeaderProperties(oTR, vTD, null);
    		    			PurchaseOrderTool.saveData(oTR, vTD, null);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;		
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, PurchaseOrder _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}
    	
    	String sVendorCode = getString(_oRow, iCol); iCol++;
    	Vendor oVendor = VendorTool.getVendorByCode(sVendorCode);
    	if (oVendor == null) {logError("Vendor Code", sVendorCode);}
    	else 
    	{
    		_oTR.setVendorId(oVendor.getVendorId()); 
    		_oTR.setVendorName(oVendor.getVendorName());
    	}
    	
    	String sCurrencyCode = getString(_oRow, iCol); iCol++;
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrencyCode);
    	if (oCurr == null) {logError("Currency Code", sCurrencyCode);}
    	else 
    	{
    		_oTR.setCurrencyId(oCurr.getCurrencyId()); 
    	}
    	
    	BigDecimal dRate = getBigDecimal(_oRow, iCol); iCol++;
    	if (dRate == null || dRate.equals(bd_ZERO)) {logError("Currency Rate",dRate);}
    	else
    	{
    		_oTR.setCurrencyRate(dRate);
    	}
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

    	String sCreateBy = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sCreateBy)) {logError("Create By", sCreateBy);}
    	else {_oTR.setCreateBy(sCreateBy);}
		
    	String sTypeCode = getString(_oRow, iCol); iCol++;
    	PaymentType oPT = PaymentTypeTool.getPaymentTypeByCode(sTypeCode);
    	if (oPT == null) {logError("Payment Type", sTypeCode);}
    	else {_oTR.setPaymentTypeId(oPT.getPaymentTypeId());}
    	
    	String sTermCode = getString(_oRow, iCol); iCol++;
    	PaymentTerm oPTerm = PaymentTermTool.getPaymentTermByCode(sTermCode);
    	if (oPTerm == null) {logError("Payment Term", sTermCode);}
    	else {_oTR.setPaymentTermId(oPTerm.getPaymentTermId());}

    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setRemark(sRemark);
    	
    	String sTotalDiscPct = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sTotalDiscPct)) sTotalDiscPct = "0";
    	_oTR.setTotalDiscountPct(sTotalDiscPct);
    	
    	BigDecimal dTotalAmount = getBigDecimal (_oRow, iCol); iCol++;
    	if (dTotalAmount == null) {logError("Total Amount", dTotalAmount);}
    	else {_oTR.setTotalAmount(dTotalAmount);}

    	String sInclusive = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sInclusive)) 
    	{
    		_oTR.setIsTaxable(Boolean.valueOf(sInclusive));
    		_oTR.setIsInclusiveTax(Boolean.valueOf(sInclusive));
    	}
    	
    	//OTHER FIELD    	
    	if (!m_bTransError)
    	{
	    	_oTR.setTransactionStatus(i_PO_NEW);
	    	_oTR.setCourierId("");
	    	_oTR.setRemark(_oTR.getRemark() + setDesc(sTransNo, m_sUserName));
	    	_oTR.setFobId("");
	    	_oTR.setTotalFee(bd_ZERO);
	    	_oTR.setConfirmBy("");
	    	
	    	//updated by set header
	    	_oTR.setTotalAmount(bd_ZERO); 
	    	_oTR.setDueDate(_oTR.getTransactionDate());
	    	
	    	//currency
	    	//_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency().getCurrencyId());
	    	//_oTR.setCurrencyRate(bd_ONE);
	    	//_oTR.setFiscalRate(bd_ONE);
    	}
    }
    
    protected void mapDetails (HSSFRow _oRow, PurchaseOrder _oTR, PurchaseOrderDetail _oTD)
		throws Exception
	{    	
    	int iCol = 0;
    	String sItemCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
    	
    	Item oItem = ItemTool.getItemByCode(sItemCode);
    	if (oItem == null) {logError("Item Code", sItemCode);}
    	else 
    	{
    		_oTD.setItemId(oItem.getItemId());
    		_oTD.setItemCode(oItem.getItemCode());
    		_oTD.setItemName(oItem.getItemName());
    	}
    	
    	if(oItem != null)
    	{
    		BigDecimal dQty = getBigDecimal (_oRow, iCol); iCol++;
    		double dConv = oItem.getUnitConversion().doubleValue();
    		if (dQty == null) {logError("Qty", dQty);}
    		else 
    		{
    			_oTD.setQty(dQty);
    			_oTD.setQtyBase(UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), dQty));
    		}

    		String sUnitCode = getString (_oRow, iCol); iCol++;
    		Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
    		if (oUnit == null) {logError("Unit Code", sUnitCode);}
    		else 
    		{
    			_oTD.setUnitId(oUnit.getUnitId());
    			_oTD.setUnitCode(oUnit.getUnitCode());
    		}

    		BigDecimal dPrice = getBigDecimal (_oRow, iCol); iCol++;
    		if (dPrice == null) {logError("Item Price", dPrice);}
    		else 
    		{
    			_oTD.setItemPrice(dPrice);
    		}

    		String sTaxCode = getString(_oRow, iCol); iCol++;
    		Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTaxCode));
    		if (oTax == null) {logError("Tax Code", sTaxCode);}
    		else
    		{
    			_oTD.setTaxId(oTax.getTaxId());
    			_oTD.setTaxAmount(oTax.getAmount());
    		}

    		String sDisc = getString(_oRow, iCol); iCol++;
    		if (StringUtil.isEmpty(sDisc)) {sDisc = "0";}
    		_oTD.setDiscount(sDisc);

    		BigDecimal dRcv = getBigDecimal (_oRow, iCol); iCol++;
    		if (dRcv != null) 
    		{
    			_oTD.setReceivedQty(dRcv);    	
    		} 
    		else
    		{
    		}
			_oTD.setReceivedQty(bd_ZERO);

    		//OTHER FIELD
    		_oTD.setProjectId("");
    		_oTD.setDepartmentId("");  
    		
    		//CALCULATED FIELD
        	if (!m_bTransError)
        	{
        		double dSubTotal = _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();
        		double dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), dSubTotal);
        		double dSubTotalTax = _oTD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
        		_oTD.setSubTotal(new BigDecimal(dSubTotal));
        		_oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
        		_oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
        		
                double dDiscount = 0;
                //if discount was not % then divide discount with purchase qty 
                if(_oTD.getDiscount().contains(Calculator.s_PCT))
                {
                    dDiscount = Calculator.calculateDiscount(_oTD.getDiscount(),_oTD.getItemPrice().doubleValue());
                }
                else
                {
                    dDiscount = Calculator.calculateDiscount(_oTD.getDiscount(),_oTD.getItemPrice().doubleValue()) / _oTD.getQty().doubleValue();
                }
                
                double dCostPerUnit = _oTD.getItemPrice().doubleValue() - dDiscount;

                dCostPerUnit = dCostPerUnit / dConv;
                double dRealCost = dCostPerUnit * _oTR.getCurrencyRate().doubleValue();
                _oTD.setCostPerUnit  ( new BigDecimal(dRealCost) );	
        	}
    	}
	}   
}
