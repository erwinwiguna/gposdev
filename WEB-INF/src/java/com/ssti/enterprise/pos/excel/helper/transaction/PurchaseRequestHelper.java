package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.PurchaseRequest;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;

public class PurchaseRequestHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Item Transfer Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
    	Date dStart = (Date)getFromParam(oParam, "StartDate", Date.class);
    	Date dEnd = (Date)getFromParam(oParam, "EndDate", Date.class); 
    	int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class);
    	String sLocID = (String) getFromParam(oParam, "LocationId", String.class);
        
        List vData = PurchaseRequestTool.findData(dStart, dEnd, sLocID, iStatus); 
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	PurchaseRequest oTR = (PurchaseRequest) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = PurchaseRequestTool.getDetailsByID(oTR.getPurchaseRequestId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		PurchaseRequestDetail oTD = (PurchaseRequestDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Transaction Date	
    	Remark	
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;	
		createHeaderCell(oWB, oRow, (short)iCol, "Location"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;			
		createHeaderCell(oWB, oRow, (short)iCol, "Approved By"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		
    }
    
	private void createMasterContent(HSSFRow oRow, PurchaseRequest oTR) 
		throws Exception
	{
		int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getRequestedBy()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getApprovedBy()); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Request Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Recommended Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Sent Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
    }
    
    private void createDetailContent(HSSFRow oRow, PurchaseRequestDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getRequestQty().toString()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getRecommendedQty().toString()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getSentQty().toString()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
    }
}
