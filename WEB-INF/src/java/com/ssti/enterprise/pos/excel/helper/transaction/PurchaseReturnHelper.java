package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;

public class PurchaseReturnHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Purchase Return Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
    	Date dStart = (Date)getFromParam(oParam, "StartReturnDate", Date.class);
    	Date dEnd = (Date)getFromParam(oParam, "EndReturnDate", Date.class);    	
    	int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class); 
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        int iLimit = (Integer) getFromParam(oParam, "ViewLimit", Integer.class);
        String sKey = (String) getFromParam(oParam, "Keywords", String.class);
        String sVendorID = (String) getFromParam(oParam, "VendorId", String.class);
        String sCurrID = (String) getFromParam(oParam, "CurrencyId", String.class);
        String sLocID = (String) getFromParam(oParam, "LocationId", String.class);        
        String sReasonID = (String) getFromParam(oParam, "ReturnReasonId", String.class);
        
    	LargeSelect oLS = PurchaseReturnTool.findData (iCond, sKey, dStart, dEnd, 
    		sVendorID, sLocID,iStatus, iLimit, sCurrID, sReasonID);
        List vData = oLS.getNextResults();
        
        int iRow = 0;        
        for (int i = 0; i < vData.size(); i++)
        {
        	PurchaseReturn oTR = (PurchaseReturn) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = PurchaseReturnTool.getDetailsByID(oTR.getPurchaseReturnId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		PurchaseReturnDetail oTD = (PurchaseReturnDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Vendor Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Rate"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Fiscal Rate"); iCol++;				
		createHeaderCell(oWB, oRow, (short)iCol, "Return Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Inclusive Tax"); iCol++;
    }
    
	private void createMasterContent(HSSFRow oRow, PurchaseReturn oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getReturnNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, VendorTool.getCodeByID(oTR.getVendorId())); iCol++;
		createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTR.getCurrencyId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getCurrencyRate().toString()); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTR.getFiscalRate().toString()); iCol++;				
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getReturnDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getCreateBy()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;
		createCell(oWB, oRow, (short)iCol, Boolean.valueOf(oTR.getIsInclusiveTax()).toString()); iCol++;

	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Price"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Tax Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount"); iCol++;
    }
    
    private void createDetailContent(HSSFRow oRow, PurchaseReturnDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getQty().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getItemPrice().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, TaxTool.getCodeByID(oTD.getTaxId())); iCol++;
    }
}
