package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SortingTool;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-03-02
 * -add sales code in detail if use salesman per item
 * </pre><br>
 */
public class SalesInvoiceHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Sales Transaction Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {                
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sLocationID = (String) getFromParam(oParam, "LocationId", null);
        String sCustomerID = (String) getFromParam(oParam, "CustomerId", null);        
        int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class);
        String sCashier = (String) getFromParam(oParam, "Cashier", null);
        String sCurrencyID = (String) getFromParam(oParam, "CurrencyId", null);
               
        List vData = TransactionTool.findTrans(iCond, sKeywords, dStart, dEnd, 
        	sCustomerID, sLocationID, iStatus,  "", sCashier, sCurrencyID);
        
        vData = SortingTool.sort(vData,"invoiceNo");
        
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	SalesTransaction oTR = (SalesTransaction) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = TransactionTool.getDetailsByID(oTR.getSalesTransactionId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		SalesTransactionDetail oTD = (SalesTransactionDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	
        	List vPmt = InvoicePaymentTool.getTransPayment(oTR.getSalesTransactionId());
        	if (vPmt.size() > 1)
        	{
            	for (int k = 0; k < vPmt.size(); k++)
            	{
            		InvoicePayment oTD = (InvoicePayment) vPmt.get(k);
            		if (k == 0)
            		{
            			oRow = oSheet.createRow(iRow); iRow++;
            			createPaymentHeader(oRow);
            		}
            		oRow = oSheet.createRow(iRow); iRow++;
                	createPaymentContent(oRow, oTD);            	        		
            	}        		
        	}
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Location Code	
    	Customer Code	
    	Transaction Date	
    	Salesman Code	
    	Cashier	
    	Payment Type Code	
    	Payment Term Code	
    	Remark	
    	Invoice Discount	
    	Payment Amount	
    	Change Amount	
    	Inclusive Tax
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Customer Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Salesman Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cashier"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Term Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Invoice Discount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Change Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Inclusive Tax"); iCol++;
		
		if (TransactionAttributes.b_SALES_MULTI_CURRENCY)
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Currency Rate"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Fiscal Rate"); iCol++;
		}
    }
    
	private void createMasterContent(HSSFRow oRow, SalesTransaction oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getInvoiceNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomerTool.getCustomerByID(oTR.getCustomerId()).getCustomerCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, EmployeeTool.getEmployeeNameByID(oTR.getSalesId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getCashierName()); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oTR.getPaymentTypeId())); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTermTool.getPaymentTermCodeByID(oTR.getPaymentTermId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getTotalDiscountPct()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getPaymentAmount().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getChangeAmount().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, Boolean.valueOf(oTR.getIsInclusiveTax()).toString()); iCol++;
		
		if (TransactionAttributes.b_SALES_MULTI_CURRENCY)
		{
			createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTR.getCurrencyId())); iCol++;
			createCell(oWB, oRow, (short)iCol, oTR.getCurrencyRate().toString()); iCol++;	
			createCell(oWB, oRow, (short)iCol, oTR.getFiscalRate().toString()); iCol++;	
		}
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Price"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Tax Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount"); iCol++;
		if (PreferenceTool.getMultiDept())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Department"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Project"); iCol++;
		}
		if (PreferenceTool.getSalesSalesmanPeritem())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Salesman Code"); iCol++;
		}
    }
    
    private void createDetailContent(HSSFRow oRow, SalesTransactionDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getQty().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getItemPrice().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, TaxTool.getCodeByID(oTD.getTaxId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getDiscount()); iCol++;
    }
    
    private void createPaymentHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Payment Type"); iCol++;
    	createHeaderCell(oWB, oRow, (short)iCol, "Payment Term"); iCol++;    	
		createHeaderCell(oWB, oRow, (short)iCol, "Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Reference No"); iCol++;
    }
    
    private void createPaymentContent(HSSFRow oRow, InvoicePayment oPMT) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oPMT.getPaymentTypeId())); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTermTool.getPaymentTermCodeByID(oPMT.getPaymentTermId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oPMT.getPaymentAmount().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oPMT.getReferenceNo()); iCol++;
    }    
}
