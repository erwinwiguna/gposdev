package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * 2016-03-02
 * -add load sales code in detail if use salesman per item
 * </pre><br>
 */
public class SalesInvoiceLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( SalesInvoiceLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Item Code";
	private static final String s_PAYMENT_HEADING = "Payment Type";
	private static final String s_END_TRANS = "End Trans";	
	
	public SalesInvoiceLoader ()
	{
	}
	
	public SalesInvoiceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		boolean bPaymentProcess = false;
		
		SalesTransaction oTR = null;
		List vTD = null;
		List vPMT = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new SalesTransaction();
    				vPMT = new ArrayList();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (sValue.equals(s_PAYMENT_HEADING) && bTransHeader && bTransProcessed && !bPaymentProcess)
    			{
    				vPMT = new ArrayList();
    				bPaymentProcess = true;
    				
    				log.debug("Found Payment HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess && !bPaymentProcess)
    			{    				
    				SalesTransactionDetail oTD = new SalesTransactionDetail();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bPaymentProcess)
    			{    				
    				InvoicePayment oPMT = new InvoicePayment();
    				mapPayment (oRow, oTR, oPMT);
    				if (!m_bTransError) vPMT.add(oPMT);
    				
    				log.debug("* PMT " + oPMT);
    			}
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			TransactionTool.setHeaderProperties(oTR, vTD, null);
    		    			if (vPMT == null || vPMT.size() <= 0)
    		    			{
    		    				vPMT = createInvoicePayment(oTR);
    		    			}
    		    			TransactionTool.saveData(oTR, vTD, vPMT);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;		
    				bPaymentProcess = false;
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, SalesTransaction _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}
    	
    	String sCustomerCode = getString(_oRow, iCol); iCol++;
    	Customer oCust = CustomerTool.getCustomerByCode(sCustomerCode);
    	if (oCust == null) {logError("Customer Code", sCustomerCode);}
    	else 
    	{
    		_oTR.setCustomerId(oCust.getCustomerId()); 
    		_oTR.setCustomerName(oCust.getCustomerName());
    	}
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

    	String sSales = getString(_oRow, iCol); iCol++;
    	Employee oSales = EmployeeTool.getEmployeeByName(sSales);
    	if (oSales != null) 
    	{
    		_oTR.setSalesId(oSales.getEmployeeId());
    	}
    	else 
    	{
    		_oTR.setSalesId("");
    	}

    	String sCashier = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sCashier)) {logError("Cashier", sCashier);}
    	else {_oTR.setCashierName(sCashier);}
		
    	String sTypeCode = getString(_oRow, iCol); iCol++;
    	PaymentType oPT = PaymentTypeTool.getPaymentTypeByCode(sTypeCode);
    	if (oPT == null) {logError("Payment Type", sTypeCode);}
    	else {_oTR.setPaymentTypeId(oPT.getPaymentTypeId());}
    	
    	String sTermCode = getString(_oRow, iCol); iCol++;
    	PaymentTerm oPTerm = PaymentTermTool.getPaymentTermByCode(sTermCode);
    	if (oPTerm == null) {logError("Payment Term", sTermCode);}
    	else {_oTR.setPaymentTermId(oPTerm.getPaymentTermId());}

    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setRemark(sRemark);
    	
    	String sTotalDiscPct = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sTotalDiscPct)) sTotalDiscPct = "0";
    	_oTR.setTotalDiscountPct(sTotalDiscPct);
    	
    	BigDecimal dPaymentAmount = getBigDecimal (_oRow, iCol); iCol++;
    	if (dPaymentAmount == null) {logError("Payment Amount", dPaymentAmount);}
    	else {_oTR.setPaymentAmount(dPaymentAmount);}
    	
    	BigDecimal dChangeAmount = getBigDecimal (_oRow, iCol); iCol++;
    	if (dChangeAmount == null) {logError("Change Amount" , dPaymentAmount);}
    	else {_oTR.setChangeAmount(dChangeAmount);}

    	String sInclusive = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sInclusive)) 
    	{
    		_oTR.setIsTaxable(Boolean.valueOf(sInclusive));
    		_oTR.setIsInclusiveTax(Boolean.valueOf(sInclusive));
    	}
    	
    	String sCurr = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sCurr))
    	{
	    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurr);
	    	if (oCurr == null) {logError("Currency Code" , sCurr);}
	    	else 
	    	{
	    		_oTR.setCurrencyId(oCurr.getCurrencyId());
	    		BigDecimal dRate = getBigDecimal (_oRow, iCol); iCol++;
	    		BigDecimal dFiscal = getBigDecimal (_oRow, iCol); iCol++;
	    		if (dRate == null) dRate = bd_ONE;
	    		if (dFiscal == null) dFiscal = bd_ONE;
	    		_oTR.setCurrencyRate(dRate);
	    		_oTR.setFiscalRate(dFiscal);
	    	}
    	}
    	
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
    		_oTR.setInvoiceNo(sTransNo);
    		
	    	_oTR.setStatus(i_PROCESSED);
	    	_oTR.setCourierId("");
	    	_oTR.setShippingPrice(bd_ZERO);
	    	_oTR.setRemark(_oTR.getRemark());
	    	_oTR.setShipTo("");
	    	_oTR.setFobId("");
	    	_oTR.setFreightAccountId("");
	    	
	    	//updated by processPayment
	    	_oTR.setPaidAmount(bd_ZERO); 
	    	_oTR.setPaymentAmount(bd_ZERO); //updated when saved
	    	_oTR.setPaymentDate(_oTR.getTransactionDate()); //updated when saved
	    	_oTR.setDueDate(_oTR.getTransactionDate());
	    	
	    	//currency
	    	if (StringUtil.isEmpty(_oTR.getCurrencyId()))
	    	{
		    	_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
		    	_oTR.setCurrencyRate(bd_ONE);
		    	_oTR.setFiscalRate(bd_ONE);
	    	}
    	}
    }
    /**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @throws Exception
	 */
	protected void mapDetails (HSSFRow _oRow, SalesTransaction _oTR, SalesTransactionDetail _oTD)
		throws Exception
	{    	
    	mapDetails(_oRow,_oTR,_oTD,0);
	}
	
	/**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @param _iStartCol
	 * @throws Exception
	 */
    protected void mapDetails (HSSFRow _oRow, SalesTransaction _oTR, SalesTransactionDetail _oTD,  int _iStartCol)
		throws Exception
	{        	
    	int iCol = _iStartCol;
    	String sItemCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
    	
    	Item oItem = ItemTool.getItemByCode(sItemCode);
    	if (oItem == null) {logError("Item Code", sItemCode);}
    	else 
    	{
    		_oTD.setItemId(oItem.getItemId());
    		_oTD.setItemCode(oItem.getItemCode());
    		_oTD.setItemName(oItem.getItemName());
    		_oTD.setDescription(oItem.getDescription());
    	}

    	BigDecimal dQty = getBigDecimal (_oRow, iCol); iCol++;
    	if (dQty == null) {logError("Qty", dQty);}
    	else 
    	{
    		_oTD.setQty(dQty);
    		_oTD.setQtyBase(UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), dQty));
    		_oTD.setReturnedQty(bd_ZERO);
    	}

    	String sUnitCode = getString (_oRow, iCol); iCol++;
    	Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
    	if (oUnit == null) {logError("Unit Code", sUnitCode);}
    	else 
    	{
    		_oTD.setUnitId(oUnit.getUnitId());
    		_oTD.setUnitCode(oUnit.getUnitCode());
    	}

    	BigDecimal dPrice = getBigDecimal (_oRow, iCol); iCol++;
    	if (dPrice == null) {logError("Item Price", dPrice);}
    	else 
    	{
    		_oTD.setItemPrice(dPrice);
    	}

    	String sTaxCode = getString(_oRow, iCol); iCol++;
    	Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTaxCode));
    	if (oTax == null) {logError("Tax Code", sTaxCode);}
    	else
    	{
    		_oTD.setTaxId(oTax.getTaxId());
    		_oTD.setTaxAmount(oTax.getAmount());
    	}
    	
    	String sDisc = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sDisc)) {sDisc = "0";}
    	_oTD.setDiscountId("");
    	_oTD.setDiscount(sDisc);    	       	
    	
    	BigDecimal bdQoH = InventoryTransactionTool.getQtyAsOf(_oTD.getItemId(), _oTR.getLocationId(), _oTR.getTransactionDate());
    	if(bdQoH == null || (bdQoH != null && (bdQoH.doubleValue() < _oTD.getQtyBase().doubleValue())))
    	{
    		logError("Qty not Enough ", _oTD.getItemCode() + " Qty: " + bdQoH);
    	}
    	
    	//OTHER FIELD
    	_oTD.setDeliveryOrderId("");
    	_oTD.setDeliveryOrderDetailId("");
    	
		//CALCULATED FIELD
    	if (!m_bTransError)
    	{
    		double dSubTotal = _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();
    		double dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), dSubTotal);
    		double dSubTotalTax = _oTD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
    		_oTD.setSubTotal(new BigDecimal(dSubTotal));
    		_oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
    		_oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
    		
    		//SKIP COST, will be recalculated when trans saved
    		_oTD.setItemCost(bd_ZERO);
    		_oTD.setSubTotalCost(bd_ZERO);
    	}
	}   

    protected void mapPayment (HSSFRow _oRow, SalesTransaction _oTR, InvoicePayment _oPMT)
    	throws Exception
    {    	
    	int iCol = 0;
    	String sTypeCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("payment_type")).append(": ").append(sTypeCode).append(s_LINE_SEPARATOR);
    	
    	PaymentType oPT = PaymentTypeTool.getPaymentTypeByCode(sTypeCode);
    	if (oPT == null) {logError("Payment Type Code", sTypeCode);}
    	else 
    	{
    		_oPMT.setPaymentTypeId(oPT.getPaymentTypeId());
    	}
    	
    	String sTermCode = getString (_oRow, iCol); iCol++;
    	PaymentTerm oPTRM = PaymentTermTool.getPaymentTermByCode(sTermCode);
    	if (oPT == null) {logError("Payment Term Code", sTermCode);}
    	else 
    	{
    		_oPMT.setPaymentTermId(oPTRM.getPaymentTermId());
    	}
    	
    	BigDecimal dAmount = getBigDecimal (_oRow, iCol); iCol++;
    	if (dAmount == null) {logError("Amount", dAmount);}
    	else 
    	{
    		_oPMT.setPaymentAmount(dAmount);
    	}

    	String sRefNo = getString (_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sRefNo)) {_oPMT.setReferenceNo(sRefNo);}
    	else {_oPMT.setReferenceNo("");}
    	
    	_oPMT.setTransTotalAmount (_oTR.getTotalAmount());
    	_oPMT.setVoucherId("");
    }   
    
    protected List createInvoicePayment (SalesTransaction _oTR)
		throws Exception
	{
    	List vPmt = new ArrayList (1);
        InvoicePayment oInv 	 = new InvoicePayment();
        oInv.setPaymentTypeId    (_oTR.getPaymentTypeId());
        oInv.setPaymentTermId    (_oTR.getPaymentTermId());
        oInv.setPaymentAmount    (_oTR.getTotalAmount());
        oInv.setTransTotalAmount (_oTR.getTotalAmount());
        oInv.setVoucherId        ("");
        oInv.setReferenceNo      ("");           
        vPmt.add (oInv);
        return vPmt;
	}
}
