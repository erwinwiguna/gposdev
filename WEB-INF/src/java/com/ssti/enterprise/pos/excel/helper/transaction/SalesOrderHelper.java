package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;

public class SalesOrderHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Sales Transaction Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {                
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sLocationID = (String) getFromParam(oParam, "LocationId", null);
        String sCustomerID = (String) getFromParam(oParam, "CustomerId", null);
        int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class);
        String sCashier = (String) getFromParam(oParam, "Cashier", null);
        String sCurrencyID = (String) getFromParam(oParam, "CurrencyId", null);
        
        List vData = SalesOrderTool.findTrans(iCond, sKeywords, dStart, dEnd, 
        	sCustomerID, sLocationID, iStatus,  "", sCashier, sCurrencyID);
        
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	SalesOrder oTR = (SalesOrder) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = SalesOrderTool.getDetailsByID(oTR.getSalesOrderId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		SalesOrderDetail oTD = (SalesOrderDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Location Code	
    	Customer Code	
    	Transaction Date	
    	Salesman Code	
    	Cashier	
    	Payment Type Code	
    	Payment Term Code	
    	Remark	
    	Invoice Discount	
    	Inclusive Tax
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Customer Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Salesman Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cashier"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Term Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Invoice Discount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Inclusive Tax"); iCol++;
		
		createHeaderCell(oWB, oRow, (short)iCol, "Freight Cost"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Freight Account"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Down Payment"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Bank"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "DP Account"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cash Flow Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Issuer/Dest"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "ReferenceNo"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Due Date"); iCol++;
    }
    
	private void createMasterContent(HSSFRow oRow, SalesOrder oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getSalesOrderNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomerTool.getCustomerByID(oTR.getCustomerId()).getCustomerCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, EmployeeTool.getEmployeeNameByID(oTR.getSalesId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oTR.getPaymentTypeId())); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTermTool.getPaymentTermCodeByID(oTR.getPaymentTermId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getTotalDiscountPct()); iCol++;
		createCell(oWB, oRow, (short)iCol, Boolean.valueOf(oTR.getIsInclusiveTax()).toString()); iCol++;
		
		createCell(oWB, oRow, (short)iCol, oTR.getEstimatedFreight().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, AccountTool.getCodeByID(oTR.getFreightAccountId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getDownPayment().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, BankTool.getBankNameByID(oTR.getBankId())); iCol++;
		createCell(oWB, oRow, (short)iCol, AccountTool.getCodeByID(oTR.getDpAccountId())); iCol++;
		createCell(oWB, oRow, (short)iCol, CashFlowTypeTool.getTypeCodeByID(oTR.getCashFlowTypeId(),null)); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTR.getBankIssuer()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getReferenceNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getDpDueDate())); iCol++;
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Price"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Tax Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount"); iCol++;
    }
    
    private void createDetailContent(HSSFRow oRow, SalesOrderDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getQty().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getItemPrice().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, TaxTool.getCodeByID(oTD.getTaxId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getDiscount()); iCol++;
    } 
}
