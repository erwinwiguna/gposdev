package com.ssti.enterprise.pos.excel.helper.transaction;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.excel.helper.ItemListLoader;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/SalesOrderLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesOrderLoader.java,v 1.12 2008/08/17 02:17:21 albert Exp $
 *
 * $Log: SalesOrderLoader.java,v $
 * Revision 1.12  2008/08/17 02:17:21  albert
 * *** empty log message ***
 */
public class SalesOrderItemLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (SalesOrderItemLoader.class);
    private String sLocationID;
    
    public SalesOrderItemLoader(String _sLocationID)
        throws Exception    
    {
    	sLocationID = _sLocationID;
    }
    
    SalesOrder oSO = null;
    public SalesOrderItemLoader(SalesOrder _oPO)
        throws Exception    
    {
    	if(_oPO != null)
    	{
    		oSO = _oPO;
    	}
    }
    
    protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		SalesOrderDetail oTD = new SalesOrderDetail();
		String sCode = getString(_oRow,_iIdx + i);
		String sDesc = getString(_oRow,_iIdx + 2);
        Item oItem = ItemTool.getItemByCode(sCode);
        double dQty = getBigDecimal(_oRow,_iIdx + 5).doubleValue();
        double dQtyBase = dQty;
        double dPrice = getBigDecimal(_oRow,_iIdx + 8).doubleValue();
        String sDisc = getString(_oRow,_iIdx + 9);   
        String sTax = getString(_oRow,_iIdx + 10);
        
        if(oItem != null)
        {
            oTD.setItemId(oItem.getItemId());
            oTD.setItemCode(oItem.getItemCode());
            oTD.setItemName(oItem.getItemName());
            oTD.setDescription(sDesc);
            if (StringUtil.isEmpty(sDesc))
            {
            	oTD.setDescription(oItem.getDescription());
            } 
            String sUnitID = oItem.getUnitId();
        	Unit oUnit = UnitTool.getUnitByID(sUnitID);        	
            if(oUnit != null)
            {
            	dQtyBase = dQty;       	
                oTD.setUnitId(oUnit.getUnitId());
                oTD.setUnitCode(oUnit.getUnitCode());
            }
            else
            {
            	setError (iRow, sCode, "Item contains invalid Unit ");
            }    
            oTD.setQty(new BigDecimal(dQty));
            oTD.setQtyBase(new BigDecimal(dQtyBase));                        
            if (dPrice == 0)
            {
            	dPrice = oItem.getItemPrice().doubleValue();            	
            }
            oTD.setItemPrice(new BigDecimal(dPrice));
            if(StringUtil.isEmpty(sDisc))
            {
            	sDisc = "0";            	          
            	if(oSO != null)
            	{
			        //check price & discount in vendor price list
				    PriceListDetail oPLD = 
				    	PriceListTool.getPLDetail(oSO.getCustomerId(), "", oSO.getLocationId(), oTD.getItemId(), new Date());			   
				    			    			    
				    //price list is first priority
				    if(oPLD != null)
				    {
				    	sDisc = oPLD.getDiscountAmount();			    	
				    	if(oPLD.getNewPrice().doubleValue() >= 0)
				    	{    		        
				    		oTD.setItemPrice (oPLD.getNewPrice());
				    	}
				    }				   
            	}                
            }
            oTD.setDiscount(sDisc);
        	Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTax)); 	
        	if (oTax == null) oTax = TaxTool.getTaxByID(oItem.getPurchaseTaxId());
        	oTD.setTaxId(oTax.getTaxId());
        	oTD.setTaxAmount(oTax.getAmount());

    		double dSubTotal = oTD.getItemPrice().doubleValue() * oTD.getQty().doubleValue();
    		double dSubTotalDisc = Calculator.calculateDiscount(oTD.getDiscount(), dSubTotal);
    		double dSubTotalTax = oTD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
    		oTD.setSubTotal(new BigDecimal(dSubTotal));
    		oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
    		oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
    		
            double dDiscount = 0;
            //if discount was not % then divide discount with purchase qty 
            if(oTD.getDiscount().contains(Calculator.s_PCT))
            {
                dDiscount = Calculator.calculateDiscount(oTD.getDiscount(),oTD.getItemPrice().doubleValue());
            }
            else
            {
                dDiscount = Calculator.calculateDiscount(oTD.getDiscount(),oTD.getItemPrice().doubleValue()) / oTD.getQty().doubleValue();
            }            
            oTD.setCostPerUnit(bd_ZERO);	
        	oTD.setProjectId("");
        	oTD.setDepartmentId("");
        	
            m_vDataList.add(oTD);                        
        }
        else
        {
        	setError (iRow, sCode, "Item Not Found");
        }
    }        
    
    void setError (int _iRow, String _sCode, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append (_iRow + 1);
		oMsg.append (" ITEM CODE " + _sCode + ", " + _sMsg );
		oMsg.append ("\n");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}
