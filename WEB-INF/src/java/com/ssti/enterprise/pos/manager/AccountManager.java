package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountPeer;
import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.om.AccountTypePeer;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.AccountTypeTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountManager.java,v 1.12 2008/03/03 01:54:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountManager.java,v $
 * Revision 1.12  2008/03/03 01:54:17  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/03/12 08:53:43  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class AccountManager
{
	private static Log log = LogFactory.getLog(AccountManager.class);
	        
    private static AccountManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "account";    

    private AccountManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static AccountManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (AccountManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new AccountManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllAccount ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get("AllAccount");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_CODE);	
			List vData = AccountPeer.doSelect(oCrit);
			oCache.put(new Element ("AllAccount", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	
	
	public List getAllChildAccount ()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get("AllChildAccount");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(AccountPeer.HAS_CHILD, false);
			oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_CODE);	
			List vData = AccountPeer.doSelect(oCrit);
			oCache.put(new Element ("AllChildAccount", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}		

	public Account getAccountByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("Account" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty (_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(AccountPeer.ACCOUNT_ID, _sID);
				List vData = AccountPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("Account" + _sID, (Serializable) vData.get(0))); 
					return (Account) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Account) oElement.getValue();
		}
	}

	public Account getAccountByCode(String _sCode, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);   
		Element oElement = oCache.get("Account" + _sCode);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
	        	oCrit.add(AccountPeer.ACCOUNT_CODE, _sCode);
	        	List vData = AccountPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("Account" + _sCode, (Serializable) vData.get(0))); 
					return (Account) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Account) oElement.getValue();
		}
	}

	public List getByParentID(String _sParentID,Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AccountParent" + _sParentID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	if (StringUtil.isNotEmpty(_sParentID))
        	{
        		oCrit.add(AccountPeer.PARENT_ID, _sParentID);
        	}
        	List vData = AccountPeer.doSelect(oCrit,_oConn);
			oCache.put(new Element ("AccountParent" + _sParentID, (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}
	
	public List getAccountByType(int _iType, boolean _bIncludeParent, Connection _oConn)
    	throws Exception
    {
        List vData = new ArrayList();
        Cache oCache = m_oManager.getCache(s_CACHE);   
		Element oElement = oCache.get("AccountByType" + _iType);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(AccountPeer.ACCOUNT_TYPE, _iType);
            if (!_bIncludeParent) 
            {   
                oCrit.add (AccountPeer.HAS_CHILD, false);
        	}
        	oCrit.addAscendingOrderByColumn(AccountPeer.ACCOUNT_CODE);
        	vData = AccountPeer.doSelect(oCrit, _oConn);
			oCache.put( new Element ("AccountByType" + _iType, (Serializable) vData)); 
			return vData;			
		}
		else {
			return (List) oElement.getValue();
		}
	}

	public List getAccountByGLType(int _iGLType, boolean _bIncludeParent, Connection _oConn)
    	throws Exception
    {
        List vData = new ArrayList();
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AccountByGLType" + _iGLType);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			if (_iGLType > 0)
            {
                oCrit.addJoin (AccountPeer.ACCOUNT_TYPE, AccountTypePeer.ACCOUNT_TYPE);
        	    oCrit.add(AccountTypePeer.GL_TYPE, _iGLType);
            }
            if (!_bIncludeParent) oCrit.add (AccountPeer.HAS_CHILD, false);
        	oCrit.addAscendingOrderByColumn(AccountPeer.ACCOUNT_CODE);
        	vData = AccountPeer.doSelect(oCrit, _oConn);
			oCache.put(new Element ("AccountByGLType" + _iGLType, (Serializable) vData)); 
			return vData;			
		}
		else {
			return (List) oElement.getValue();
		}
	}

	public String getAccountTree(int _iGLType)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AccountJSTree" + _iGLType);
		if (oElement == null)
		{
			String sTree = AccountTool.createJSTree(_iGLType);			
			oCache.put(new Element ("AccountJSTree" + _iGLType, sTree)); 
			return sTree;
		}
		else {
			return (String)	oElement.getValue();
		}
	}

	public String getAccountSBTree(String _sName, String _sSelectedID)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AccountSBTree");
		if (oElement == null)
		{
			String sTree = AccountTool.createSBTree(_sName, _sSelectedID);			
			oCache.put(new Element ("AccountSBTree", sTree)); 
			return sTree;
		}
		else {
			return (String)	oElement.getValue();
		}
	}	
    
	public List getChildIDList (List _vID, String _sID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("ChildID" + _sID);
		if (oElement == null)
		{		
		    List vID = AccountTool.getChildIDList(_vID, _sID);
			oCache.put (new Element ("ChildID" + _sID, (Serializable) vID));
			return vID;
		}
		else {
			return (List) oElement.getValue();
		}
	}  	
    
	public List getAccountTreeList (List _vAccount, String _sID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AccountTreeList" + _sID);
		if (oElement == null)
		{		
		    List vAccount = AccountTool.getAccountTreeList(_vAccount, _sID, -1);			
			oCache.put(new Element ("AccountTreeList" + _sID, (Serializable) vAccount)); 
			return vAccount;
		}
		else {
            return (List) oElement.getValue();
		}
	} 	

	//THIS METHOD MUST BE CALLED AFTER SAVE PROCESS     
	public void refreshCache(Account _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);              
		if (_oData != null)
		{
			 
            oCache.remove("AllAccount");
            oCache.remove("AllChildAccount");       
            oCache.remove("AccountSBTree");
			oCache.remove("Account"         + _oData.getAccountId());
			oCache.remove("Account"         + _oData.getAccountCode());
			oCache.remove("ChildID"         + _oData.getAccountId());
			oCache.remove("AccountTreeList" + _oData.getAccountId());
			oCache.remove("AccountByType"   + _oData.getAccountType());		
			
			AccountType oType = AccountTypeTool.getAccountType (_oData.getAccountType());
			if(oType != null)
			{
				oCache.remove("AccountByGLType" + oType.getGlType());		
				oCache.remove("AccountJSTree"   + oType.getGlType());		
	            oCache.remove("AccountByGLType" + 0);       
	            oCache.remove("AccountJSTree"   + 0);
			}
        }
        else
        {
            oCache.removeAll();
        }
    }   
}