package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.AdjustmentTypePeer;
import com.ssti.framework.tools.StringUtil;

public class AdjustmentTypeManager
{   
	private static Log log = LogFactory.getLog(AdjustmentTypeManager.class);
	 
    private static AdjustmentTypeManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private AdjustmentTypeManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static AdjustmentTypeManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (AdjustmentTypeManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new AdjustmentTypeManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllAdjustmentType ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("AllAdjustmentType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(AdjustmentTypePeer.ADJUSTMENT_TYPE_CODE);
			oCrit.addAscendingOrderByColumn(AdjustmentTypePeer.DESCRIPTION);
			List vData = AdjustmentTypePeer.doSelect(oCrit);
			oCache.put(new Element ("AllAdjustmentType", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}

	public List getTrfAdjustmentType ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("TrfAdjustmentType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(AdjustmentTypePeer.IS_TRANSFER, true);
			List vData = AdjustmentTypePeer.doSelect(oCrit);
			oCache.put(new Element ("TrfAdjustmentType", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}

	public AdjustmentType getAdjustmentTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("AdjustmentType" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				boolean bJoinTrans = true;
				Criteria oCrit = new Criteria();
				oCrit.add(AdjustmentTypePeer.ADJUSTMENT_TYPE_ID, _sID);
				List vData = AdjustmentTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("AdjustmentType" + _sID, (Serializable)vData.get(0))); 
					return (AdjustmentType) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (AdjustmentType) oElement.getValue();
		}
	}

	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (AdjustmentType _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getAdjustmentTypeId())) 
        {
            oCache.put(new Element("AdjustmentType" + _oData.getAdjustmentTypeId(), _oData));
            oCache.remove("AllAdjustmentType");
            oCache.remove("TrfAdjustmentType");
        }
        else
        {
            oCache.removeAll();
        }
    }
	
	//for deleete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("AdjustmentType" + _sID);
        oCache.remove("AllAdjustmentType");
        oCache.remove("TrfAdjustmentType");        
        if (StringUtil.isEmpty(_sID))
        {
            oCache.removeAll();
        }
    } 
}