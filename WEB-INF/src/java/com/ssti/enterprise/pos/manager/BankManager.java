package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankBook;
import com.ssti.enterprise.pos.om.BankBookPeer;
import com.ssti.enterprise.pos.om.BankPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.framework.tools.StringUtil;

public class BankManager
{   
	private static Log log = LogFactory.getLog(BankManager.class);
	 
    private static BankManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private BankManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static BankManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (BankManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new BankManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllBank ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllBank");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (BankPeer.BANK_CODE);
			oCrit.addAscendingOrderByColumn (BankPeer.CURRENCY_ID);			
			List vData = BankPeer.doSelect(oCrit);
			oCache.put(new Element ("AllBank", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	
	
	public List getBankByLocationID (String _sLocID)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("BankLocation" + _sLocID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (BankPeer.BANK_CODE);
			oCrit.addAscendingOrderByColumn (BankPeer.CURRENCY_ID);			
			oCrit.add(BankPeer.LOCATION_ID, _sLocID);
			List vData = BankPeer.doSelect(oCrit);
			oCache.put(new Element ("BankLocation" + _sLocID, (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}		
	
	public List getBankByCurrencyID(String _sCurrencyID)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE);         
        Element oElement = oCache.get("BankCurrency" + _sCurrencyID);			
        if (oElement == null)
        {		
        	Criteria oCrit = new Criteria();
        	oCrit.addAscendingOrderByColumn (BankPeer.BANK_CODE);					
        	oCrit.add (BankPeer.CURRENCY_ID, _sCurrencyID);
        	List vData = BankPeer.doSelect(oCrit);
        	oCache.put(new Element ("BankCurrency" + _sCurrencyID, (Serializable)vData)); 
        	return vData;
        }			
        else 
        {
        	return (List) oElement.getValue();
        }
	}

	public Bank getBankByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Bank" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(BankPeer.BANK_ID, _sID);
				List vData = BankPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Bank" + _sID, (Serializable)vData.get(0))); 
					return (Bank) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Bank) oElement.getValue();
		}
	}
	
	public BankBook getBankBook(String _sID, Connection _oConn)
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("BankBook" + _sID);
		
		if (oElement == null)
		{
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(BankBookPeer.BANK_ID, _sID);
				List vData = BankBookPeer.doSelect(oCrit, _oConn);
				if(vData.size() > 0)
				{
					BankBook oBalance = (BankBook) vData.get(0);
					oCache.put(new Element ("BankBook" + _sID, (Serializable)oBalance)); 
					return oBalance;
				}
				else
				{
					oCache.put(new Element ("BankBook" + _sID, null)); 				
				}
			}
			return null;
		}
		return (BankBook) oElement.getValue();
	}	
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Bank _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (_oData != null && StringUtil.isNotEmpty(_oData.getBankId())) 
        {
            oCache.put(new Element ("Bank" + _oData.getBankId(), _oData));
            oCache.remove("BankBook" + _oData.getBankId());
            oCache.remove("BankCurrency" + _oData.getCurrencyId());
            oCache.remove("BankLocation" + _oData.getLocationId());
            oCache.remove("AllBank");
        }
        else
        {
            oCache.removeAll();
        }
    }
	
	//for delete
	public void refreshCache (String _sID, String _sCurrencyID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (StringUtil.isNotEmpty(_sID))
        {
            oCache.remove("Bank" + _sID);     
            oCache.remove("BankBook" + _sID);     
            oCache.remove("BankCurrency" + _sCurrencyID);        
            List vLoc = LocationTool.getAllLocation();
            for (int i = 0; i < vLoc.size(); i++)
            {
            	Location oLoc = (Location) vLoc.get(i);
            	oCache.remove("BankLocation" + oLoc.getLocationId());             
            }
            oCache.remove("AllBank");
        }
        else
        {
            oCache.removeAll();
        }
    } 
}