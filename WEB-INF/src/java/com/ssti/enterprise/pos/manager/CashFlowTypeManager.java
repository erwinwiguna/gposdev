package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.CashFlowTypePeer;
import com.ssti.framework.tools.StringUtil;

public class CashFlowTypeManager
{   
	private static Log log = LogFactory.getLog(CashFlowTypeManager.class);
	 
    private static CashFlowTypeManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      
    private static final String s_KEY = "CashFlowType";      
    private static final String s_ALL = "AllCashFlowType";      
    private static final String s_CHILD = "AllChildCFT";      
    
    private CashFlowTypeManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static CashFlowTypeManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CashFlowTypeManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CashFlowTypeManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllType()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_ALL);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(CashFlowTypePeer.CASH_FLOW_TYPE_CODE);
			List vData = CashFlowTypePeer.doSelect(oCrit);			
			if (vData.size() > 0) oCache.put(new Element (s_ALL, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public CashFlowType getTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_KEY + _sID);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CashFlowTypePeer.CASH_FLOW_TYPE_ID, _sID);
				List vData = CashFlowTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0)
				{
					oCache.put(new Element (s_KEY + _sID, (Serializable)vData.get(0))); 
					return (CashFlowType) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (CashFlowType) oElement.getValue();
		}
	}

	public CashFlowType getTypeByCode(String _sCode, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_KEY + _sCode);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CashFlowTypePeer.CASH_FLOW_TYPE_CODE, _sCode);
				List vData = CashFlowTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0)
				{
					oCache.put(new Element (s_KEY + _sCode, (Serializable)vData.get(0))); 
					return (CashFlowType) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (CashFlowType) oElement.getValue();
		}
	}
    
    public List getAllChild()
        throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(s_CHILD);
        if (oElement == null)
        {       
            Criteria oCrit = new Criteria();
            oCrit.add(CashFlowTypePeer.HAS_CHILD, false);
            oCrit.addAscendingOrderByColumn (CashFlowTypePeer.CASH_FLOW_TYPE_CODE); 
            List vData = CashFlowTypePeer.doSelect(oCrit);
            oCache.put(new Element (s_CHILD, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            return (List) oElement.getValue();
        }
    }
    	
	public void refreshCache (CashFlowType _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getCashFlowTypeId()) ) 
        {
            oCache.remove(s_KEY + _oData.getCashFlowTypeId());
            oCache.put(new Element (s_KEY + _oData.getCashFlowTypeId(), _oData));
        
            oCache.remove(s_KEY + _oData.getCashFlowTypeCode());
            oCache.put(new Element (s_KEY + _oData.getCashFlowTypeCode(), _oData));
            oCache.remove(s_ALL);
            oCache.remove(s_CHILD);
        }
        else
        {
            oCache.removeAll();
        }        
    }
	
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (StringUtil.isNotEmpty(_sID))
        {
            oCache.remove(s_KEY + _sID);
            oCache.remove(s_ALL);
            oCache.remove(s_CHILD);
        }
        else
        {
            oCache.removeAll();
        }        
    }
}