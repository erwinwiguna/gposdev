package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PointType;
import com.ssti.enterprise.pos.om.PointTypePeer;
import com.ssti.framework.tools.StringUtil;

public class CommonManager
{   
	private static Log log = LogFactory.getLog(CommonManager.class);
	 
    private static CommonManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private CommonManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static CommonManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CommonManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CommonManager();
                }
            }
        }
        return m_oInstance;
    }

    //-------------------------------------------------------------------------
    // Point Type
    //-------------------------------------------------------------------------
    
    static final String s_ALL_PTYPE = "AllPointType";  
    static final String s_SI_PTYPE = "SiPointType";  
    static final String s_SR_PTYPE = "SrPointType"; 
    static final String s_ID_PTYPE = "IdPointType"; 
    
    final String[] a_PTYPE = {s_ALL_PTYPE, s_SI_PTYPE, s_SR_PTYPE, s_ID_PTYPE};    
    
    public List getAllPointType()
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
    	Element oElement = oCache.get(s_ALL_PTYPE);
		if (oElement == null)
		{	
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(PointTypePeer.CODE);
			//oCrit.add(PointTypePeer.FOR_SI, false);
			//oCrit.add(PointTypePeer.FOR_SR, false);		
			List v = PointTypePeer.doSelect(oCrit);
			oCache.put( new Element (s_ALL_PTYPE, (Serializable) v)); 
			return v;
		}
		else {
			return (List) oElement.getValue();
		}
	}

	public PointType getPointType(String _sID)
		throws Exception
	{
		PointType p = null;
		Cache oCache = m_oManager.getCache(s_CACHE); 
    	Element oElement = oCache.get(s_ID_PTYPE + _sID);
		if (oElement == null)
		{			
			Criteria oCrit = new Criteria();
			oCrit.add(PointTypePeer.POINT_TYPE_ID, _sID);
			List v = PointTypePeer.doSelect(oCrit);
			if(v.size() > 0)
			{
				p = (PointType) v.get(0);								
			}
			oCache.put( new Element (s_ID_PTYPE + _sID, (Serializable) p)); 
			return p;
		}
		else
		{
			return (PointType) oElement.getValue();
		}
	}

    
	public String getSITypeID()
		throws Exception
	{
		String sID = "";
		Cache oCache = m_oManager.getCache(s_CACHE); 
    	Element oElement = oCache.get(s_SI_PTYPE);
		if (oElement == null)
		{			
			Criteria oCrit = new Criteria();
			oCrit.add(PointTypePeer.FOR_SI, true);
			oCrit.add(PointTypePeer.FOR_SR, false);
			List v = PointTypePeer.doSelect(oCrit);
			if(v.size() > 0)
			{
				sID = ((PointType)v.get(0)).getPointTypeId();								
			}
			oCache.put( new Element (s_SI_PTYPE, (Serializable) sID)); 
			return sID;
		}
		else
		{
			return (String) oElement.getValue();
		}
	}

	public String getSRTypeID()
		throws Exception
	{
		String sID = "";
		Cache oCache = m_oManager.getCache(s_CACHE); 
    	Element oElement = oCache.get(s_SI_PTYPE);
		if (oElement == null)
		{			
			Criteria oCrit = new Criteria();
			oCrit.add(PointTypePeer.FOR_SI, false);
			oCrit.add(PointTypePeer.FOR_SR, true);
			List v = PointTypePeer.doSelect(oCrit);
			if(v.size() > 0)
			{
				sID = ((PointType)v.get(0)).getPointTypeId();								
			}
			oCache.put( new Element (s_SI_PTYPE, (Serializable) sID)); 
			return sID;
		}
		else
		{
			return (String) oElement.getValue();
		}
	}

	public void refreshPTCache()
	{
		Cache oCache = m_oManager.getCache(s_CACHE);
		List vKeys = oCache.getKeys();
		for(int i = 0; i < vKeys.size(); i++)
		{
			String sKey = (String) vKeys.get(i);
			if(StringUtil.isNotEmpty(sKey))
			{
				for (int j = 0; j < a_PTYPE.length; j++)
				{
					if(sKey.startsWith(a_PTYPE[j]))
					{
						oCache.remove(sKey);
					}
				}
			}		
		}
	}
    
    //-------------------------------------------------------------------------
    // Other
    //-------------------------------------------------------------------------

}