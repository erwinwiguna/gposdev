package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.CustomField;
import com.ssti.enterprise.pos.om.CustomFieldPeer;
import com.ssti.framework.tools.StringUtil;

public class CustomFieldManager
{   
	private static Log log = LogFactory.getLog(CustomFieldManager.class);
	 
    private static CustomFieldManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private CustomFieldManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static CustomFieldManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CustomFieldManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CustomFieldManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllCustomField ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("AllCustomField");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = CustomFieldPeer.doSelect(oCrit);
			oCache.put(new Element ("AllCustomField", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public CustomField getCustomFieldByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("CustomField" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomFieldPeer.CUSTOM_FIELD_ID, _sID);
				List vData = CustomFieldPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("CustomField" + _sID, (Serializable)vData.get(0))); 
					return (CustomField) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (CustomField) oElement.getValue();
		}
	}

	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (CustomField _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getCustomFieldId()) ) 
        {
            oCache.remove("CustomField" + _oData.getCustomFieldId());
            oCache.put(new Element ("CustomField" + _oData.getCustomFieldId(), _oData));
        }
        oCache.remove("AllCustomField");
    }
	
	//for deleete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("CustomField" + _sID);
        oCache.remove("AllCustomField");
    } 
}