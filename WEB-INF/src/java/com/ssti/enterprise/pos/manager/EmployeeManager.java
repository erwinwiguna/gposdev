package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.om.TurbineRole;
import com.ssti.enterprise.pos.om.TurbineRolePeer;
import com.ssti.framework.tools.StringUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-07-31
 * - add getRoleConfigForPO
 * 
 * </pre><br>
 */
public class EmployeeManager
{   
	private static Log log = LogFactory.getLog(EmployeeManager.class);
	 
    private static EmployeeManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private EmployeeManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static EmployeeManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (EmployeeManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new EmployeeManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllEmployee ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllEmployee");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (EmployeePeer.EMPLOYEE_NAME);	
			oCrit.add(EmployeePeer.IS_ACTIVE, true);
			List vData = EmployeePeer.doSelect(oCrit);
			oCache.put(new Element ("AllEmployee", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public List getSalesman ()	
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllSales");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (EmployeePeer.EMPLOYEE_NAME);
			oCrit.add(EmployeePeer.IS_SALESMAN, true);
			oCrit.add(EmployeePeer.IS_ACTIVE, true);
			List vData = EmployeePeer.doSelect(oCrit);
			oCache.put(new Element ("AllSales", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}
	
	public List getSalesmanByLocID(String _sLocID)	
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sLocID))
		{	
			Cache oCache = m_oManager.getCache(s_CACHE);     
			String sKey = "SalesByLoc" + _sLocID;
			Element oElement = oCache.get(sKey);
			if (oElement == null)
			{		
				Criteria oCrit = new Criteria();
				oCrit.addAscendingOrderByColumn (EmployeePeer.EMPLOYEE_NAME);
				oCrit.add(EmployeePeer.IS_SALESMAN, true);
				oCrit.add(EmployeePeer.IS_ACTIVE, true);
				oCrit.add(EmployeePeer.LOCATION_ID,_sLocID);					
				List vData = EmployeePeer.doSelect(oCrit);
				oCache.put(new Element (sKey, (Serializable)vData)); 
				return vData;
			}
			else {
				return (List) oElement.getValue();
			}
		}
		else
		{
			return getSalesman();
		}
	}
	
	public Employee getEmployeeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
        String sKey = "Employee" + _sID;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
	        	oCrit.add(EmployeePeer.EMPLOYEE_ID, _sID);
	        	List vData = EmployeePeer.doSelect(oCrit, _oConn);

				if (vData.size() > 0) {
					oCache.put(new Element(sKey, (Serializable)vData.get(0))); 
					return (Employee) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Employee) oElement.getValue();
		}
	}

	public Employee getEmployeeByCode(String _sCode, Connection _oConn) throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		String sKey = "Employee" + _sCode;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(EmployeePeer.EMPLOYEE_CODE, _sCode);
				List vData = EmployeePeer.doSelect(oCrit, _oConn);

				if (vData.size() > 0) {
					oCache.put(new Element(sKey, (Serializable)vData.get(0))); 
					return (Employee) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Employee) oElement.getValue();
		}
	}
		
	public Employee getEmployeeByUsername(String _sUsername, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
	    String sKey =  "EmployeeUsername" + _sUsername;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sUsername))
			{
				Criteria oCrit = new Criteria();
	        	oCrit.add(EmployeePeer.USER_NAME, _sUsername);
	        	List vData = EmployeePeer.doSelect(oCrit, _oConn);	
				if (vData.size() > 0) {
					oCache.put(new Element (sKey, (Serializable)vData.get(0))); 
					return (Employee) vData.get(0);
				}
				else
				{
					oCache.put(new Element (sKey, null));
				}
			}
			return null;
		}
		else 
		{
			return (Employee) oElement.getValue();
		}
	}
	
	public List getEmployeeByLocationID(String _sLocationID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("EmployeeLocation" + _sLocationID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	if (StringUtil.isNotEmpty(_sLocationID))
        	{
        		oCrit.add(EmployeePeer.LOCATION_ID, _sLocationID);
        	}
        	oCrit.add(EmployeePeer.IS_ACTIVE, true);
        	List vData = EmployeePeer.doSelect(oCrit);
			oCache.put(new Element ("EmployeeLocation" + _sLocationID, (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}

	public TurbineRole getRoleByID(int _iID) 
		throws Exception
	{		
		Cache oCache = m_oManager.getCache(s_CACHE); 
		String sKey = "Role" + _iID;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{
			if(_iID > 0)
			{
				Criteria oCrit = new Criteria();
				oCrit.add(TurbineRolePeer.ROLE_ID, _iID);
				List v = TurbineRolePeer.doSelect(oCrit);
				if (v.size() > 0)
				{
					TurbineRole oRole = (TurbineRole) v.get(0);
					oCache.put(new Element (sKey, (Serializable)oRole));
					return oRole;
				}
			}
			return null;
		}
		else
		{
			return (TurbineRole) oElement.getValue();
		}
	} 
	
	public List getByParentID(String _sParentID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("EmployeeParent" + _sParentID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(EmployeePeer.PARENT_ID, _sParentID);        	
        	oCrit.addAscendingOrderByColumn(EmployeePeer.EMPLOYEE_NAME);
        	oCrit.add(EmployeePeer.IS_ACTIVE, true);
        	List vData = EmployeePeer.doSelect(oCrit);
			oCache.put(new Element ("EmployeeParent" + _sParentID, (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}
		
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Employee _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if (_oData != null)
        {
	        if (StringUtil.isNotEmpty(_oData.getEmployeeId())) 
	        {
	            oCache.put(new Element ("Employee" + _oData.getEmployeeId(), _oData));
	        }
	        if (StringUtil.isNotEmpty(_oData.getEmployeeCode())) 
	        {
	            oCache.put(new Element ("Employee" + _oData.getEmployeeCode(), _oData));
	        }
	        if (StringUtil.isNotEmpty(_oData.getUserName()))
	        {
	            oCache.remove("EmployeeUsername" + _oData.getUserName());        	
	        }
	        oCache.remove("EmployeeParent");	        	        	        
	        oCache.remove("EmployeeParent" + _oData.getParentId());	        	        
	        oCache.remove("EmployeeParent" + _oData.getEmployeeId());	        
	        oCache.remove("EmployeeLocation" + _oData.getLocationId());
	        oCache.remove("SalesByLoc" + _oData.getLocationId());
	        oCache.remove("AllEmployee");
	        oCache.remove("AllSales");	        	       
        }
        else
        {
        	oCache.removeAll();
        }
    }
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        oCache.removeAll();
    }
}