package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.GlConfig;
import com.ssti.enterprise.pos.om.GlConfigPeer;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/manager/GLConfigManager.java,v $
 *
 * Purpose: GlConfig cache manager. Used to prevent Database Access 
 *
 * @author  $Author: albert $
 * @version $Id: GLConfigManager.java,v 1.5 2008/03/03 01:54:17 albert Exp $
 *
 * $Log: GLConfigManager.java,v $
 * Revision 1.5  2008/03/03 01:54:17  albert
 * *** empty log message ***
 *
 * Revision 1.4  2005/08/21 14:25:26  albert
 * *** empty log message ***
 *
 * Revision 1.3  2005/01/20 07:33:40  yudi
 * no message
 *
 * Revision 1.2  2004/11/08 02:20:32  Albert
 * organize import by eclipse
 *
 * Revision 1.1  2004/10/25 09:26:50  Albert
 * no message
 *
 */

public class GLConfigManager
{
	private static Log log = LogFactory.getLog(GLConfigManager.class);
	    
    private static GLConfigManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "preference";   
    private static final String s_KEY_NAME = "GlConfig";
    
    private GLConfigManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static GLConfigManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (GLConfigManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new GLConfigManager();
                }
            }
        }
        return m_oInstance;
    }

	public GlConfig getGlConfig (Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
        Element oElement = oCache.get(s_KEY_NAME);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = GlConfigPeer.doSelect(oCrit, _oConn);
			if (vData.size() > 0){
				oCache.put(new Element (s_KEY_NAME, (Serializable) vData.get(0))); 
				return (GlConfig)vData.get(0);
			}
			return null;
		}
		else 
		{
			return (GlConfig) oElement.getValue();
		}
	}	

	public void refreshCache () 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.remove(s_KEY_NAME);          
    }

	public void refreshCache (GlConfig _oPref) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.put(new Element (s_KEY_NAME, _oPref));        
    }
}