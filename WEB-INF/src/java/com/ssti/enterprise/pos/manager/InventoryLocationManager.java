package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-07-15
 * - add method getByItemID because used extensively by VehicleTool 
 * 
 * </pre><br>
 */
public class InventoryLocationManager
{   
	private static Log log = LogFactory.getLog(InventoryLocationManager.class);
	 
    private static InventoryLocationManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "inventory_location";      
    private static final String s_KEY = "InvLoc";      
    private static final String s_KEY2 = "InvLocByItem";      
    
    private InventoryLocationManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static InventoryLocationManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (InventoryLocationManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new InventoryLocationManager();
                }
            }
        }
        return m_oInstance;
    }

    public InventoryLocation getInventoryLocation (String _sItemID, 
    											   String _sLocationID,
    											   Connection _oConn)
	    throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_KEY + _sItemID + _sLocationID);
		if (oElement == null)
		{	    	
			Criteria oCrit = new Criteria();
		    oCrit.add(InventoryLocationPeer.ITEM_ID, _sItemID);
		    oCrit.add(InventoryLocationPeer.LOCATION_ID, _sLocationID);			
			List vData = InventoryLocationPeer.doSelect(oCrit, _oConn);
			InventoryLocation oData = null;
			if (vData.size() > 0)
			{
				oData = (InventoryLocation) vData.get(0);
			}
			oCache.put(new Element(s_KEY + _sItemID + _sLocationID, oData));
			return oData;
		}
		else 
		{
			return (InventoryLocation) oElement.getValue();
		}        
	}

    public List getByItemID (String _sItemID, Connection _oConn)
    	throws Exception
    {
    	Cache oCache = m_oManager.getCache(s_CACHE); 
    	Element oElement = oCache.get(s_KEY2 + _sItemID);
    	if (oElement == null)
    	{	    	
    		Criteria oCrit = new Criteria();
    		oCrit.add(InventoryLocationPeer.ITEM_ID, _sItemID);
    		List vData = InventoryLocationPeer.doSelect(oCrit, _oConn);
    		oCache.put(new Element(s_KEY2 + _sItemID, (Serializable) vData));
    		return vData;
    	}
    	else 
    	{
    		return (List) oElement.getValue();
    	}        
    }

    
	public void refreshCache (InventoryLocation _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getItemId()) ) 
        {
            oCache.remove(s_KEY + _oData.getItemId() + _oData.getLocationId());
            oCache.remove(s_KEY2 + _oData.getItemId());
        }
    }
	
	public void refreshCache (String _sItemID, String _sLocationID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove(s_KEY + _sItemID + _sLocationID);
        oCache.remove(s_KEY2 + _sItemID);
    }
	
	public void clearCache () 
		throws Exception
	{	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.removeAll();
	}
}