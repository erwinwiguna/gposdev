package com.ssti.enterprise.pos.manager;

import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;

public class InventoryTransManager
{   
	private static Log log = LogFactory.getLog(InventoryTransManager.class);
	 
    private static InventoryTransManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "inventory_location";      
    private static final String s_KEY = "InvTrans";      
    
    private InventoryTransManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }
    
    public static InventoryTransManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (InventoryTransManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new InventoryTransManager();
                }
            }
        }
        return m_oInstance;
    }
    
    public InventoryTransaction getByID (String _sID, Connection _oConn)
		throws Exception
	{
    	Cache oCache = m_oManager.getCache(s_CACHE); 	
		Element oElement = oCache.get(s_KEY + _sID);
		if (oElement == null)
		{
			Criteria oCrit = new Criteria ();
			oCrit.add (InventoryTransactionPeer.INVENTORY_TRANSACTION_ID, _sID);
			List vData = InventoryTransactionPeer.doSelect (oCrit, _oConn);
			if (vData.size() > 0)
			{
				InventoryTransaction oTrans = (InventoryTransaction) vData.get(0);
				oCache.put(new Element(s_KEY + _sID, oTrans));
				return oTrans;
			}	
		}
		else
		{
			return (InventoryTransaction) oElement.getValue();
		}
		return null;		
	}		
    		
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove(s_KEY + _sID);
    }
	
	public void clearCache () 
		throws Exception
	{	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.removeAll();
	}
}