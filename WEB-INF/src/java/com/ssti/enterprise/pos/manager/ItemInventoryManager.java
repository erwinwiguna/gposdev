package com.ssti.enterprise.pos.manager;

import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.ItemInventoryPeer;
import com.ssti.framework.tools.StringUtil;

public class ItemInventoryManager
{   
	private static Log log = LogFactory.getLog(ItemInventoryManager.class);
	 
    private static ItemInventoryManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      
    private static final String s_KEY = "ItemInventory";      
    
    private ItemInventoryManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static ItemInventoryManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (ItemInventoryManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new ItemInventoryManager();
                }
            }
        }
        return m_oInstance;
    }

    public ItemInventory getItemInventory (String _sItemID, String _sLocationID )
	    throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_KEY + _sItemID + _sLocationID);
		if (oElement == null)
		{	    	
			Criteria oCrit = new Criteria();
		    oCrit.add(ItemInventoryPeer.ITEM_ID, _sItemID);
		    oCrit.add(ItemInventoryPeer.LOCATION_ID, _sLocationID);
			oCrit.setLimit(1);				
			
			List vData = ItemInventoryPeer.doSelect(oCrit);
			ItemInventory oData = null;
			if (vData.size() > 0)
			{
				oData = (ItemInventory) vData.get(0);
			}
			oCache.put(new Element(s_KEY + _sItemID + _sLocationID, oData));
			return oData;
		}
		else 
		{
			return (ItemInventory) oElement.getValue();
		}        
	}
    
	public void refreshCache (ItemInventory _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getItemId()) ) 
        {
            oCache.remove(s_KEY + _oData.getItemId() + _oData.getLocationId());
        }
        else
        {
            oCache.removeAll();
        }
    }
	
	public void refreshCache (String _sItemID, String _sLocationID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove(s_KEY + _sItemID + _sLocationID);
    }
}