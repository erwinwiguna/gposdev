package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.ItemStatusCode;
import com.ssti.enterprise.pos.om.ItemStatusCodePeer;
import com.ssti.framework.tools.StringUtil;

public class ItemStatusCodeManager
{   
	private static Log log = LogFactory.getLog(ItemStatusCodeManager.class);
	 
    private static ItemStatusCodeManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      
    private static final String s_KEY = "ItemStatusCode";      
    private static final String s_ALL = "AllItemStatusCode";      
    
    private ItemStatusCodeManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static ItemStatusCodeManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (ItemStatusCodeManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new ItemStatusCodeManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllItemStatusCode ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_ALL);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(ItemStatusCodePeer.STATUS_CODE);
			List vData = ItemStatusCodePeer.doSelect(oCrit);			
			oCache.put(new Element (s_ALL, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public ItemStatusCode getItemStatusCodeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_KEY + _sID);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ItemStatusCodePeer.ITEM_STATUS_CODE_ID, _sID);
				List vData = ItemStatusCodePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0)
				{
					oCache.put(new Element (s_KEY + _sID, (Serializable)vData.get(0))); 
					return (ItemStatusCode) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (ItemStatusCode) oElement.getValue();
		}
	}

	public void refreshCache (ItemStatusCode _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getItemStatusCodeId()) ) 
        {
            oCache.remove(s_KEY + _oData.getItemStatusCodeId());
            oCache.put(new Element (s_KEY + _oData.getItemStatusCodeId(), _oData));
        }
        oCache.remove(s_ALL);
    }
	
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove(s_KEY + _sID);
        oCache.remove(s_ALL);
    }
}