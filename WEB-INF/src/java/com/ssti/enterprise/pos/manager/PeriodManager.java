package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.om.PeriodPeer;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-01-08
 * - add method getPeriodByMonthYear
 * 
 * </pre><br>
 */
public class PeriodManager
{   
	private static Log log = LogFactory.getLog(PeriodManager.class);
	 
    private static PeriodManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private PeriodManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static PeriodManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (PeriodManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new PeriodManager();
                }
            }
        }
        return m_oInstance;
    }

	public Period getPeriodByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
        String sKey  = "Period" + _sID;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(PeriodPeer.PERIOD_ID, _sID);
				List vData = PeriodPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element (sKey, (Serializable)vData.get(0))); 
					return (Period) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Period) oElement.getValue();
		}
	}

	/**
	 * 
	 * @param _iYear
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public List getPeriodByYear(int _iYear, Connection _oConn) 
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		String sKey = "PeriodYear" + _iYear;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			if (_iYear > 0)
			{
				Criteria oCrit = new Criteria();
				oCrit.addAscendingOrderByColumn(PeriodPeer.MONTH);
				oCrit.add(PeriodPeer.YEAR, _iYear);
				List vData = PeriodPeer.doSelect(oCrit, _oConn);
				oCache.put(new Element(sKey, (Serializable)vData));
				return vData;
			}
			return null;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	} 
	
	/**
	 * get last closed period
	 * 
	 * @return Last closed Period
	 * @throws Exception
	 */
	public Period getLastClosedPeriod()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("LastClosed");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
	        oCrit.add(PeriodPeer.IS_CLOSED, true);
			oCrit.addDescendingOrderByColumn(PeriodPeer.BEGIN_DATE);
			oCrit.setLimit(1);
			List vData = PeriodPeer.doSelect(oCrit);
			if (vData.size() > 0) 
			{
				oCache.put(new Element ("LastClosed", (Serializable)vData.get(0))); 
				return (Period) vData.get(0);
			}
		}
		else 
		{
			return (Period) oElement.getValue();
		}
		return null;
	}	

	public Period getPeriodByMonthYear(int _iYear, int _iMonth, Connection _oConn)
			throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		String sKey  = "Period" + _iYear + "-" + _iMonth;
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			if (_iYear > 1900 && _iMonth > 0 && _iMonth < 13)
			{
				Criteria oCrit = new Criteria();
				oCrit.add(PeriodPeer.YEAR, _iYear);
				oCrit.add(PeriodPeer.MONTH, _iMonth);				
				List vData = PeriodPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element (sKey, (Serializable)vData.get(0))); 
					return (Period) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Period) oElement.getValue();
		}
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Period _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getPeriodId())) 
        {
            oCache.remove("Period" + _oData.getPeriodId());
            oCache.put(new Element ("Period" + _oData.getPeriodId(), _oData));
            oCache.remove("PeriodYear" + _oData.getYear());
        }        
        oCache.remove("AllPeriod");
        oCache.remove("LastClosed");
        oCache.removeAll();
    }
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.removeAll();
    }
}