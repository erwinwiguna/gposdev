package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PettyCashBalance;
import com.ssti.enterprise.pos.om.PettyCashBalancePeer;
import com.ssti.enterprise.pos.om.PettyCashType;
import com.ssti.enterprise.pos.om.PettyCashTypePeer;
import com.ssti.framework.tools.StringUtil;

public class PettyCashManager
{   
	private static Log log = LogFactory.getLog(PettyCashManager.class);
	 
    private static PettyCashManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private PettyCashManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static PettyCashManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (PettyCashManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new PettyCashManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllPettyCashType ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllPettyCashType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (PettyCashTypePeer.DESCRIPTION);	
			List vData = PettyCashTypePeer.doSelect(oCrit);
			oCache.put(new Element ("AllPettyCashType", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public PettyCashType getPettyCashTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("PettyCashType" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(PettyCashTypePeer.PETTY_CASH_TYPE_ID, _sID);
				List vData = PettyCashTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("PettyCashType" + _sID, (Serializable)vData.get(0))); 
					return (PettyCashType) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (PettyCashType) oElement.getValue();
		}
	}
	
	public PettyCashBalance getBalanceByLocationID(String _sLocationID, Connection _oConn)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("PettyCashBalance" + _sLocationID);
		if (oElement == null)
		{	
			Criteria oCrit = new Criteria();
		    oCrit.add(PettyCashBalancePeer.LOCATION_ID, _sLocationID);
		    List vData = PettyCashBalancePeer.doSelect(oCrit, _oConn);
		    if (vData.size() > 0) 
		    {
		    	oCache.put(new Element ("PettyCashBalance" + _sLocationID, (Serializable)vData.get(0))); 
				return (PettyCashBalance) vData.get(0);
			}
		}
		else
		{
			return (PettyCashBalance) oElement.getValue();
		}
		return null;
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (PettyCashType _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (_oData != null && StringUtil.isNotEmpty(_oData.getPettyCashTypeId())) 
        {
            oCache.remove("PettyCashType" + _oData.getPettyCashTypeId());
            oCache.put(new Element ("PettyCashType" + _oData.getPettyCashTypeId(), _oData));
        }
        else
        {
            oCache.removeAll();
        }
        oCache.remove("AllPettyCashType");
    }
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("PettyCashType" + _sID);    
        oCache.remove("PettyCashBalance" + _sID);            
        oCache.remove("AllPettyCashType");
    }
	
	public void refreshBalance (String _sID) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);         
	    oCache.remove("PettyCashBalance" + _sID); 
	} 	
}