package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.CompanyData;
import com.ssti.enterprise.pos.om.CompanyDataPeer;
import com.ssti.enterprise.pos.om.Preference;
import com.ssti.enterprise.pos.om.PreferencePeer;
import com.ssti.enterprise.pos.om.SysConfig;
import com.ssti.enterprise.pos.om.SysConfigPeer;

public class PreferenceManager
{
	private static Log log = LogFactory.getLog(PreferenceManager.class);
	    
    private static PreferenceManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "preference";   
    
    private static final String s_KEY_NAME = "Preference";
    private static final String s_KEY_SYSCONF = "SysConfig";    
    private static final String s_KEY_COMPANY = "CompanyData";
    
    private PreferenceManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static PreferenceManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (PreferenceManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new PreferenceManager();
                }
            }
        }
        return m_oInstance;
    }

	public Preference getPreference ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
        Element oElement = oCache.get(s_KEY_NAME);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = PreferencePeer.doSelect(oCrit);
			if (vData.size() > 0){
				oCache.put(new Element (s_KEY_NAME, (Serializable) vData.get(0))); 
				return (Preference)vData.get(0);
			}
			return null;
		}
		else {
			return (Preference) oElement.getValue();
		}
	}	

	public SysConfig getSysConfig() 
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
        Element oElement = oCache.get(s_KEY_SYSCONF);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = SysConfigPeer.doSelect(oCrit);
			if (vData.size() > 0)
			{
				oCache.put(new Element (s_KEY_SYSCONF, (Serializable) vData.get(0))); 
				return (SysConfig)vData.get(0);
			}
			return null;
		}
		else {
			return (SysConfig) oElement.getValue();
		}
	}

	
	public CompanyData getCompany()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
        Element oElement = oCache.get(s_KEY_COMPANY);
		if (oElement == null)
		{		
			List vData = CompanyDataPeer.doSelect(new Criteria());
			if (vData.size() > 0)
			{
				oCache.put(new Element (s_KEY_COMPANY, (Serializable) vData.get(0))); 
				return (CompanyData)vData.get(0);
			}
			return null;
		}
		else {
			return (CompanyData) oElement.getValue();
		}
	}	

	public void refreshCache () 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.removeAll();          
    }

	public void refreshCache (Preference _oPref) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.put(new Element (s_KEY_NAME, _oPref));        
    }

	public void refreshCache (CompanyData _oCompany) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.put(new Element (s_KEY_COMPANY, _oCompany));        
    }    
	
	public void refreshCache (SysConfig _oSC) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);
	    oCache.put(new Element (s_KEY_SYSCONF, _oSC));        
	}    
}