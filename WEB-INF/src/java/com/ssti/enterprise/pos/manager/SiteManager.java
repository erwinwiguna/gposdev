package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Site;
import com.ssti.enterprise.pos.om.SitePeer;
import com.ssti.framework.tools.StringUtil;

public class SiteManager
{   
	private static Log log = LogFactory.getLog(SiteManager.class);
	 
    private static SiteManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private SiteManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static SiteManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (SiteManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new SiteManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllSite ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllSite");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (SitePeer.SITE_NAME);	
			List vData = SitePeer.doSelect(oCrit);
			oCache.put(new Element ("AllSite", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public Site getSiteByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Site" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(SitePeer.SITE_ID, _sID);
				List vData = SitePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Site" + _sID, (Serializable)vData.get(0))); 
					return (Site) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Site) oElement.getValue();
		}
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Site _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getSiteId())) 
        {
            oCache.remove("Site" + _oData.getSiteId());
        }
        oCache.put(new Element ("Site" + _oData.getSiteId(), _oData));
        oCache.remove("AllSite");
    }
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("Site" + _sID);     
        oCache.remove("AllSite");
    } 
}