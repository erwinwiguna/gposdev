package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.framework.tools.StringUtil;

public class TaxManager
{   
	private static Log log = LogFactory.getLog(TaxManager.class);
	 
    private static TaxManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private TaxManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static TaxManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (TaxManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new TaxManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllTax ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("AllTax");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = TaxPeer.doSelect(oCrit);
			oCache.put(new Element ("AllTax", (Serializable) vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public Tax getTaxByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Tax" + _sID);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(TaxPeer.TAX_ID, _sID);
				List vData = TaxPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Tax" + _sID, (Serializable)vData.get(0))); 
					return (Tax) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Tax) oElement.getValue();
		}
	}

	public Tax getTaxByCode(String _sCode, Connection _oConn)
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("TaxByCode" + _sCode);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(TaxPeer.TAX_CODE, _sCode);
				List vData = TaxPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("TaxByCode" + _sCode, (Serializable)vData.get(0))); 
					return (Tax) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Tax) oElement.getValue();
		}
	}	
	
	public Tax getDefaultSalesTax()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("SalesTax");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(TaxPeer.DEFAULT_SALES_TAX, true);
        	List vData = TaxPeer.doSelect(oCrit);
			
			if (vData.size() > 0) 
			{
				oCache.put(new Element ("SalesTax", (Serializable)vData.get(0))); 
				return (Tax) vData.get(0);
			}
			return null;
		}
		else {
			return (Tax) oElement.getValue();
		}
	}

	public Tax getDefaultPurchaseTax()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("PurchaseTax");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(TaxPeer.DEFAULT_PURCHASE_TAX, true);
        	List vData = TaxPeer.doSelect(oCrit);
			
			if (vData.size() > 0) 
			{
				oCache.put(new Element ("PurchaseTax", (Serializable)vData.get(0))); 
				return (Tax) vData.get(0);
			}
			return null;
		}
		else {
			return (Tax) oElement.getValue();
		}
	}	

	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Tax _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getTaxId()) ) 
        {
            oCache.remove("Tax" + _oData.getTaxId());
            oCache.remove("TaxByCode" + _oData.getTaxCode());   
        }
        oCache.put(new Element ("Tax" + _oData.getTaxId(), _oData));
        oCache.put(new Element ("TaxByCode" + _oData.getTaxCode(), _oData));

        oCache.remove("AllTax");
        oCache.remove("SalesTax");
        oCache.remove("PurchaseTax");
    }
	
	//for deleete
	public void refreshCache (String _sID, String _sCode) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("Tax" + _sID);
        oCache.remove("TaxByCode" + _sCode);
        oCache.remove("AllTax");
        oCache.remove("SalesTax");
        oCache.remove("PurchaseTax");
    } 
}