package com.ssti.enterprise.pos.model;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.CustomFormatter;


/**
 * This class used to create object that will show as report:
 *
 * [9/30/2004 10:30AM]
 * Created by : Yudi
 */
 
public class AccountReport
{
    private String accountId;
    private String accountCode;
    private String accountName;

    private String locationId;
    private String departmentId;
    private String projectId;
    private String siteId;
    private String subLedgerType;
    private String subLedgerId;
    
    private double totalDebit;
    private double totalCredit;
    private double totalDebitBase;
    private double totalCreditBase;
    
    private int normalBalance; //1 = debit 2 = credit
    
    //balance amount (FOR CURRENT BALANCE)
    private double primeBalance;
    private double baseBalance;
    
    //changes amount
    private double primeChanges;
    private double baseChanges;    
    
	public AccountReport(Account acc)
    {
		if (acc != null)
		{
			this.accountId = acc.getAccountId();
			this.accountCode = acc.getAccountCode();
			this.accountName = acc.getAccountName();
			this.normalBalance = acc.getNormalBalance();
		}	
    }
    
	public String getAccountCode() {
		return accountCode;
	}

	public String getAccountId() {
		return accountId;
	}
		
	public String getAccountName() {
		return accountName;
	}

	public double getBaseBalance() {
		return baseBalance;
	}
	public void setBaseBalance(double baseBalance) {
		this.baseBalance = baseBalance;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public double getPrimeBalance() {
		return primeBalance;
	}
	public void setPrimeBalance(double primeBalance) {
		this.primeBalance = primeBalance;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getSubLedgerId() {
		return subLedgerId;
	}
	public void setSubLedgerId(String subLedgerId) {
		this.subLedgerId = subLedgerId;
	}
	public String getSubLedgerType() {
		return subLedgerType;
	}
	public void setSubLedgerType(String subLedgerType) {
		this.subLedgerType = subLedgerType;
	}
	public double getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(double totalCredit) {
		this.totalCredit = totalCredit;
	}
	public double getTotalCreditBase() {
		return totalCreditBase;
	}
	public void setTotalCreditBase(double totalCreditBase) {
		this.totalCreditBase = totalCreditBase;
	}
	public double getTotalDebit() {
		return totalDebit;
	}
	public void setTotalDebit(double totalDebit) {
		this.totalDebit = totalDebit;
	}
	public double getTotalDebitBase() {
		return totalDebitBase;
	}
	public void setTotalDebitBase(double totalDebitBase) {
		this.totalDebitBase = totalDebitBase;
	}

    public double getBaseChanges() {
		return baseChanges;
	}

	public void setBaseChanges(double baseChanges) {
		this.baseChanges = baseChanges;
	}

	public double getPrimeChanges() {
		return primeChanges;
	}

	public void setPrimeChanges(double primeChanges) {
		this.primeChanges = primeChanges;
	}
	
	public int getNormalBalance() {
		return normalBalance;
	}
	
	public String toString()
	{
		String sSep = Attributes.s_LINE_SEPARATOR;
		StringBuilder sb = new StringBuilder();
		sb.append("Account: ")
		  .append(this.accountCode).append(" - ").append(this.accountName).append(sSep)
		  .append(" Balance: ").append(CustomFormatter.fmt(this.primeBalance)).append(" Base: ")
		  					   .append(CustomFormatter.fmt(this.baseBalance)).append(sSep)
		  .append(" Changes: ").append(CustomFormatter.fmt(this.totalDebitBase)).append(" Credit: ")
		  					   .append(CustomFormatter.fmt(this.totalCreditBase)).append(sSep)		  
		  ;
		
		return sb.toString();
	}
}
