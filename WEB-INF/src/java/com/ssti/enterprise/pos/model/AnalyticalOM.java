package com.ssti.enterprise.pos.model;

import org.apache.torque.om.Persistent;

/**
 * Project, Department, 
 * @author Albert
 *
 */
public interface AnalyticalOM extends Persistent
{
	public String getId();	
	public String getCode();
	public String getName();
}
