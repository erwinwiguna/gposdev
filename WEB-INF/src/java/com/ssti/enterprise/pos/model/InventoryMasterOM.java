package com.ssti.enterprise.pos.model;

import java.math.BigDecimal;
import java.util.List;

public interface InventoryMasterOM extends TransactionMasterOM
{
	public List getDetails();
	public BigDecimal getTotalQty();
	public BigDecimal getTotalCost();	
}
