package com.ssti.enterprise.pos.model;

import java.util.List;


/**
 * Project, Department, 
 * @author Albert
 *
 */
public interface MasterTreeOM extends MasterOM
{
	public String getId();	
	public String getCode();
	public Object getParent();
	public List getChilds();
	public int getLevel();
	public boolean getHasChild();
}
