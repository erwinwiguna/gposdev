package com.ssti.enterprise.pos.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ssti.enterprise.pos.tools.IDGenerator;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/packing/PLItem.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PLItem.java,v 1.3 2008/06/29 07:10:21 albert Exp $
 *
 * $Log: PLItem.java,v $
 * Revision 1.3  2008/06/29 07:10:21  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/10/10 04:25:46  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/01/30 03:41:04  albert
 * *** empty log message ***
 *
 */

public class PLItem implements Serializable
{
	private String Key;
	private String ItemId;
	private String ItemCode;
	private String ItemName;
	private BigDecimal Qty;
	private BigDecimal QtyBase;
	private String UnitId;
	private String UnitCode;

	public PLItem () throws Exception
	{
		Key = IDGenerator.generateSysID();
	}	
	
	public String getKey() {
		return Key;
	}

	public String getItemCode() {
		return ItemCode;
	}
	public void setItemCode(String itemCode) {
		ItemCode = itemCode;
	}
	public String getItemId() {
		return ItemId;
	}
	public void setItemId(String itemID) {
		ItemId = itemID;
	}
	public String getItemName() {
		return ItemName;
	}
	public void setItemName(String itemName) {
		ItemName = itemName;
	}
	public BigDecimal getQty() {
		return Qty;
	}
	public void setQty(BigDecimal qty) {
		Qty = qty;
	}
	public BigDecimal getQtyBase() {
		return QtyBase;
	}
	public void setQtyBase(BigDecimal qtyBase) {
		QtyBase = qtyBase;
	}
	public String getUnitCode() {
		return UnitCode;
	}
	public void setUnitCode(String unitCode) {
		UnitCode = unitCode;
	}
	public String getUnitId() {
		return UnitId;
	}
	public void setUnitId(String unitID) {
		UnitId = unitID;
	}
	
	public String toString()
	{
		StringBuilder oDesc = new StringBuilder();
		oDesc.append ("\nItem ID   : " + ItemId);
		oDesc.append ("\nItem Code : " + ItemCode);
		oDesc.append ("\nItem Name : " + ItemName);
		oDesc.append ("\nQty 	   : " + Qty);
		oDesc.append ("\nQty Base  : " + QtyBase);
		oDesc.append ("\nUnit ID   : " + UnitId);
		oDesc.append ("\nUnit Code : " + UnitCode);
		return oDesc.toString();
	}
}