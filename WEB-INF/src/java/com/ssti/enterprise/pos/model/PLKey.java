package com.ssti.enterprise.pos.model;

import java.io.Serializable;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/packing/PLKey.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PLKey.java,v 1.2 2005/10/10 04:25:46 albert Exp $
 *
 * $Log: PLKey.java,v $
 * Revision 1.2  2005/10/10 04:25:46  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/01/30 03:41:04  albert
 * *** empty log message ***
 *
 */

public class PLKey implements Serializable, Comparable
{

	private int Key;
	private String PackId;
	private String Remark;
	
	public PLKey (int _iKey, String _sPackID)
	{
		Key = _iKey;
		PackId = _sPackID;
	}
	
	public int getKey() {
		return Key;
	}
	
	public String getPackId() {
		return PackId;
	}
	
	public String getRemark() {
		return Remark;
	}
	
	public void setRemark(String remark) {
		Remark = remark;
	}
	
	public String toString()
	{
		StringBuilder oDesc = new StringBuilder();
		oDesc.append ("\nKey   	 : " + Key);
		oDesc.append ("\nPack ID : " + PackId);
		return oDesc.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o2) 
	{
		PLKey oKey2 = (PLKey) o2;
		int iResult = -1;
		if (this.Key > oKey2.getKey() )
		{
			iResult = 1;
		}
		else if ( this.Key == oKey2.getKey())
		{
			iResult = 0;
		}
		return iResult;
	}

}