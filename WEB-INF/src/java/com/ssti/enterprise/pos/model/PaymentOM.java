package com.ssti.enterprise.pos.model;

import java.sql.Connection;
import java.util.Date;

import org.apache.torque.TorqueException;

public interface PaymentOM extends MultiCurrencyMasterOM
{
	public Date getDueDate();
	
	public String getEntityId ();
	public String getEntityName ();
	
	public void setCfStatus(int _iStatus);
	public void setCfDate(Date _dCFDate);	
	public void setCashFlowId(String _sCFID);
	public void save(Connection _oConn) throws TorqueException;
}
