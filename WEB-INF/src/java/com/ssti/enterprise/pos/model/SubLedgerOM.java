package com.ssti.enterprise.pos.model;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.torque.om.Persistent;

public interface SubLedgerOM extends Persistent
{
	public void setTransactionId(String s);
	public void setTransactionNo(String s);

	public String getTransactionId();
	public String getRemark();
	
	public String getEntityId ();
	public String getEntityName ();

	public Date getTransactionDate();
	
	public String getCurrencyId();
	public BigDecimal getCurrencyRate();
	public BigDecimal getAmount();
	public BigDecimal getAmountBase();
}
