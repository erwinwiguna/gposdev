package com.ssti.enterprise.pos.model;

import java.util.Date;

import org.apache.torque.om.Persistent;

public interface TransactionMasterOM extends Persistent
{
	public String getTransactionId();	
	public String getTransactionNo();
	public Date getTransactionDate();
	public int getStatus();
	public String getLocationId();
	//public String getLocationName();	
	public String getCreateBy();
	public String getStatusStr();
	//cancel
	public String getCancelBy();
	public Date getCancelDate();	
}
