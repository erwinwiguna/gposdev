package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Account
 */
public abstract class BaseAccount extends BaseObject
{
    /** The Peer class */
    private static final AccountPeer peer =
        new AccountPeer();

        
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the accountCode field */
    private String accountCode;
      
    /** The value for the accountName field */
    private String accountName;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the accountType field */
    private int accountType;
      
    /** The value for the parentId field */
    private String parentId;
      
    /** The value for the hasChild field */
    private boolean hasChild;
      
    /** The value for the accountLevel field */
    private int accountLevel;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the normalBalance field */
    private int normalBalance;
      
    /** The value for the status field */
    private boolean status;
      
    /** The value for the openingBalance field */
    private BigDecimal openingBalance;
      
    /** The value for the asDate field */
    private Date asDate;
                                                
    /** The value for the obTransId field */
    private String obTransId = "";
                                                
          
    /** The value for the obRate field */
    private BigDecimal obRate= new BigDecimal(1);
                                                
    /** The value for the altCode field */
    private String altCode = "";
  
    
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountCode
     *
     * @return String
     */
    public String getAccountCode()
    {
        return accountCode;
    }

                        
    /**
     * Set the value of AccountCode
     *
     * @param v new value
     */
    public void setAccountCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountCode, v))
              {
            this.accountCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountName
     *
     * @return String
     */
    public String getAccountName()
    {
        return accountName;
    }

                        
    /**
     * Set the value of AccountName
     *
     * @param v new value
     */
    public void setAccountName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountName, v))
              {
            this.accountName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountType
     *
     * @return int
     */
    public int getAccountType()
    {
        return accountType;
    }

                        
    /**
     * Set the value of AccountType
     *
     * @param v new value
     */
    public void setAccountType(int v) 
    {
    
                  if (this.accountType != v)
              {
            this.accountType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ParentId
     *
     * @return String
     */
    public String getParentId()
    {
        return parentId;
    }

                        
    /**
     * Set the value of ParentId
     *
     * @param v new value
     */
    public void setParentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.parentId, v))
              {
            this.parentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HasChild
     *
     * @return boolean
     */
    public boolean getHasChild()
    {
        return hasChild;
    }

                        
    /**
     * Set the value of HasChild
     *
     * @param v new value
     */
    public void setHasChild(boolean v) 
    {
    
                  if (this.hasChild != v)
              {
            this.hasChild = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountLevel
     *
     * @return int
     */
    public int getAccountLevel()
    {
        return accountLevel;
    }

                        
    /**
     * Set the value of AccountLevel
     *
     * @param v new value
     */
    public void setAccountLevel(int v) 
    {
    
                  if (this.accountLevel != v)
              {
            this.accountLevel = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NormalBalance
     *
     * @return int
     */
    public int getNormalBalance()
    {
        return normalBalance;
    }

                        
    /**
     * Set the value of NormalBalance
     *
     * @param v new value
     */
    public void setNormalBalance(int v) 
    {
    
                  if (this.normalBalance != v)
              {
            this.normalBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return boolean
     */
    public boolean getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(boolean v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AsDate
     *
     * @return Date
     */
    public Date getAsDate()
    {
        return asDate;
    }

                        
    /**
     * Set the value of AsDate
     *
     * @param v new value
     */
    public void setAsDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.asDate, v))
              {
            this.asDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObTransId
     *
     * @return String
     */
    public String getObTransId()
    {
        return obTransId;
    }

                        
    /**
     * Set the value of ObTransId
     *
     * @param v new value
     */
    public void setObTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.obTransId, v))
              {
            this.obTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObRate
     *
     * @return BigDecimal
     */
    public BigDecimal getObRate()
    {
        return obRate;
    }

                        
    /**
     * Set the value of ObRate
     *
     * @param v new value
     */
    public void setObRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.obRate, v))
              {
            this.obRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AltCode
     *
     * @return String
     */
    public String getAltCode()
    {
        return altCode;
    }

                        
    /**
     * Set the value of AltCode
     *
     * @param v new value
     */
    public void setAltCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.altCode, v))
              {
            this.altCode = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AccountId");
              fieldNames.add("AccountCode");
              fieldNames.add("AccountName");
              fieldNames.add("Description");
              fieldNames.add("AccountType");
              fieldNames.add("ParentId");
              fieldNames.add("HasChild");
              fieldNames.add("AccountLevel");
              fieldNames.add("CurrencyId");
              fieldNames.add("NormalBalance");
              fieldNames.add("Status");
              fieldNames.add("OpeningBalance");
              fieldNames.add("AsDate");
              fieldNames.add("ObTransId");
              fieldNames.add("ObRate");
              fieldNames.add("AltCode");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("AccountCode"))
        {
                return getAccountCode();
            }
          if (name.equals("AccountName"))
        {
                return getAccountName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("AccountType"))
        {
                return Integer.valueOf(getAccountType());
            }
          if (name.equals("ParentId"))
        {
                return getParentId();
            }
          if (name.equals("HasChild"))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals("AccountLevel"))
        {
                return Integer.valueOf(getAccountLevel());
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("NormalBalance"))
        {
                return Integer.valueOf(getNormalBalance());
            }
          if (name.equals("Status"))
        {
                return Boolean.valueOf(getStatus());
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("AsDate"))
        {
                return getAsDate();
            }
          if (name.equals("ObTransId"))
        {
                return getObTransId();
            }
          if (name.equals("ObRate"))
        {
                return getObRate();
            }
          if (name.equals("AltCode"))
        {
                return getAltCode();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AccountPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(AccountPeer.ACCOUNT_CODE))
        {
                return getAccountCode();
            }
          if (name.equals(AccountPeer.ACCOUNT_NAME))
        {
                return getAccountName();
            }
          if (name.equals(AccountPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(AccountPeer.ACCOUNT_TYPE))
        {
                return Integer.valueOf(getAccountType());
            }
          if (name.equals(AccountPeer.PARENT_ID))
        {
                return getParentId();
            }
          if (name.equals(AccountPeer.HAS_CHILD))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals(AccountPeer.ACCOUNT_LEVEL))
        {
                return Integer.valueOf(getAccountLevel());
            }
          if (name.equals(AccountPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(AccountPeer.NORMAL_BALANCE))
        {
                return Integer.valueOf(getNormalBalance());
            }
          if (name.equals(AccountPeer.STATUS))
        {
                return Boolean.valueOf(getStatus());
            }
          if (name.equals(AccountPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(AccountPeer.AS_DATE))
        {
                return getAsDate();
            }
          if (name.equals(AccountPeer.OB_TRANS_ID))
        {
                return getObTransId();
            }
          if (name.equals(AccountPeer.OB_RATE))
        {
                return getObRate();
            }
          if (name.equals(AccountPeer.ALT_CODE))
        {
                return getAltCode();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAccountId();
            }
              if (pos == 1)
        {
                return getAccountCode();
            }
              if (pos == 2)
        {
                return getAccountName();
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getAccountType());
            }
              if (pos == 5)
        {
                return getParentId();
            }
              if (pos == 6)
        {
                return Boolean.valueOf(getHasChild());
            }
              if (pos == 7)
        {
                return Integer.valueOf(getAccountLevel());
            }
              if (pos == 8)
        {
                return getCurrencyId();
            }
              if (pos == 9)
        {
                return Integer.valueOf(getNormalBalance());
            }
              if (pos == 10)
        {
                return Boolean.valueOf(getStatus());
            }
              if (pos == 11)
        {
                return getOpeningBalance();
            }
              if (pos == 12)
        {
                return getAsDate();
            }
              if (pos == 13)
        {
                return getObTransId();
            }
              if (pos == 14)
        {
                return getObRate();
            }
              if (pos == 15)
        {
                return getAltCode();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AccountPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AccountPeer.doInsert((Account) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AccountPeer.doUpdate((Account) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key accountId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAccountId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAccountId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAccountId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Account copy() throws TorqueException
    {
        return copyInto(new Account());
    }
  
    protected Account copyInto(Account copyObj) throws TorqueException
    {
          copyObj.setAccountId(accountId);
          copyObj.setAccountCode(accountCode);
          copyObj.setAccountName(accountName);
          copyObj.setDescription(description);
          copyObj.setAccountType(accountType);
          copyObj.setParentId(parentId);
          copyObj.setHasChild(hasChild);
          copyObj.setAccountLevel(accountLevel);
          copyObj.setCurrencyId(currencyId);
          copyObj.setNormalBalance(normalBalance);
          copyObj.setStatus(status);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setAsDate(asDate);
          copyObj.setObTransId(obTransId);
          copyObj.setObRate(obRate);
          copyObj.setAltCode(altCode);
  
                    copyObj.setAccountId((String)null);
                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AccountPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Account\n");
        str.append("-------\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("AccountCode          : ")
           .append(getAccountCode())
           .append("\n")
           .append("AccountName          : ")
           .append(getAccountName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("AccountType          : ")
           .append(getAccountType())
           .append("\n")
           .append("ParentId             : ")
           .append(getParentId())
           .append("\n")
           .append("HasChild             : ")
           .append(getHasChild())
           .append("\n")
           .append("AccountLevel         : ")
           .append(getAccountLevel())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("NormalBalance        : ")
           .append(getNormalBalance())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("AsDate               : ")
           .append(getAsDate())
           .append("\n")
           .append("ObTransId            : ")
           .append(getObTransId())
           .append("\n")
           .append("ObRate               : ")
           .append(getObRate())
           .append("\n")
           .append("AltCode              : ")
           .append(getAltCode())
           .append("\n")
        ;
        return(str.toString());
    }
}
