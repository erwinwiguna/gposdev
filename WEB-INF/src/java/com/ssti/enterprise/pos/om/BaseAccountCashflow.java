package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AccountCashflow
 */
public abstract class BaseAccountCashflow extends BaseObject
{
    /** The Peer class */
    private static final AccountCashflowPeer peer =
        new AccountCashflowPeer();

        
    /** The value for the accountCashflowId field */
    private String accountCashflowId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId;
  
    
    /**
     * Get the AccountCashflowId
     *
     * @return String
     */
    public String getAccountCashflowId()
    {
        return accountCashflowId;
    }

                        
    /**
     * Set the value of AccountCashflowId
     *
     * @param v new value
     */
    public void setAccountCashflowId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountCashflowId, v))
              {
            this.accountCashflowId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AccountCashflowId");
              fieldNames.add("AccountId");
              fieldNames.add("CashFlowTypeId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AccountCashflowId"))
        {
                return getAccountCashflowId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AccountCashflowPeer.ACCOUNT_CASHFLOW_ID))
        {
                return getAccountCashflowId();
            }
          if (name.equals(AccountCashflowPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(AccountCashflowPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAccountCashflowId();
            }
              if (pos == 1)
        {
                return getAccountId();
            }
              if (pos == 2)
        {
                return getCashFlowTypeId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AccountCashflowPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AccountCashflowPeer.doInsert((AccountCashflow) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AccountCashflowPeer.doUpdate((AccountCashflow) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key accountCashflowId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAccountCashflowId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAccountCashflowId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAccountCashflowId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AccountCashflow copy() throws TorqueException
    {
        return copyInto(new AccountCashflow());
    }
  
    protected AccountCashflow copyInto(AccountCashflow copyObj) throws TorqueException
    {
          copyObj.setAccountCashflowId(accountCashflowId);
          copyObj.setAccountId(accountId);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
  
                    copyObj.setAccountCashflowId((String)null);
                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AccountCashflowPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AccountCashflow\n");
        str.append("---------------\n")
           .append("AccountCashflowId    : ")
           .append(getAccountCashflowId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
        ;
        return(str.toString());
    }
}
