package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AccountPayable
 */
public abstract class BaseAccountPayable extends BaseObject
{
    /** The Peer class */
    private static final AccountPayablePeer peer =
        new AccountPayablePeer();

        
    /** The value for the accountPayableId field */
    private String accountPayableId;
      
    /** The value for the transactionId field */
    private String transactionId;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the remark field */
    private String remark;
                                                
    /** The value for the locationId field */
    private String locationId = "";
  
    
    /**
     * Get the AccountPayableId
     *
     * @return String
     */
    public String getAccountPayableId()
    {
        return accountPayableId;
    }

                        
    /**
     * Set the value of AccountPayableId
     *
     * @param v new value
     */
    public void setAccountPayableId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountPayableId, v))
              {
            this.accountPayableId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AccountPayableId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionType");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("TransactionDate");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("DueDate");
              fieldNames.add("Remark");
              fieldNames.add("LocationId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AccountPayableId"))
        {
                return getAccountPayableId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AccountPayablePeer.ACCOUNT_PAYABLE_ID))
        {
                return getAccountPayableId();
            }
          if (name.equals(AccountPayablePeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(AccountPayablePeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(AccountPayablePeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(AccountPayablePeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(AccountPayablePeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(AccountPayablePeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(AccountPayablePeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(AccountPayablePeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(AccountPayablePeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(AccountPayablePeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(AccountPayablePeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(AccountPayablePeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(AccountPayablePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAccountPayableId();
            }
              if (pos == 1)
        {
                return getTransactionId();
            }
              if (pos == 2)
        {
                return getTransactionNo();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 4)
        {
                return getVendorId();
            }
              if (pos == 5)
        {
                return getVendorName();
            }
              if (pos == 6)
        {
                return getTransactionDate();
            }
              if (pos == 7)
        {
                return getCurrencyId();
            }
              if (pos == 8)
        {
                return getCurrencyRate();
            }
              if (pos == 9)
        {
                return getAmount();
            }
              if (pos == 10)
        {
                return getAmountBase();
            }
              if (pos == 11)
        {
                return getDueDate();
            }
              if (pos == 12)
        {
                return getRemark();
            }
              if (pos == 13)
        {
                return getLocationId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AccountPayablePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AccountPayablePeer.doInsert((AccountPayable) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AccountPayablePeer.doUpdate((AccountPayable) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key accountPayableId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAccountPayableId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAccountPayableId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAccountPayableId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AccountPayable copy() throws TorqueException
    {
        return copyInto(new AccountPayable());
    }
  
    protected AccountPayable copyInto(AccountPayable copyObj) throws TorqueException
    {
          copyObj.setAccountPayableId(accountPayableId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionType(transactionType);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setDueDate(dueDate);
          copyObj.setRemark(remark);
          copyObj.setLocationId(locationId);
  
                    copyObj.setAccountPayableId((String)null);
                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AccountPayablePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AccountPayable\n");
        str.append("--------------\n")
           .append("AccountPayableId     : ")
           .append(getAccountPayableId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
        ;
        return(str.toString());
    }
}
