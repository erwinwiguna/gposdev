package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.AccountMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseAccountPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "account";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(AccountMapBuilder.CLASS_NAME);
    }

      /** the column name for the ACCOUNT_ID field */
    public static final String ACCOUNT_ID;
      /** the column name for the ACCOUNT_CODE field */
    public static final String ACCOUNT_CODE;
      /** the column name for the ACCOUNT_NAME field */
    public static final String ACCOUNT_NAME;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the ACCOUNT_TYPE field */
    public static final String ACCOUNT_TYPE;
      /** the column name for the PARENT_ID field */
    public static final String PARENT_ID;
      /** the column name for the HAS_CHILD field */
    public static final String HAS_CHILD;
      /** the column name for the ACCOUNT_LEVEL field */
    public static final String ACCOUNT_LEVEL;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the NORMAL_BALANCE field */
    public static final String NORMAL_BALANCE;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the OPENING_BALANCE field */
    public static final String OPENING_BALANCE;
      /** the column name for the AS_DATE field */
    public static final String AS_DATE;
      /** the column name for the OB_TRANS_ID field */
    public static final String OB_TRANS_ID;
      /** the column name for the OB_RATE field */
    public static final String OB_RATE;
      /** the column name for the ALT_CODE field */
    public static final String ALT_CODE;
  
    static
    {
          ACCOUNT_ID = "account.ACCOUNT_ID";
          ACCOUNT_CODE = "account.ACCOUNT_CODE";
          ACCOUNT_NAME = "account.ACCOUNT_NAME";
          DESCRIPTION = "account.DESCRIPTION";
          ACCOUNT_TYPE = "account.ACCOUNT_TYPE";
          PARENT_ID = "account.PARENT_ID";
          HAS_CHILD = "account.HAS_CHILD";
          ACCOUNT_LEVEL = "account.ACCOUNT_LEVEL";
          CURRENCY_ID = "account.CURRENCY_ID";
          NORMAL_BALANCE = "account.NORMAL_BALANCE";
          STATUS = "account.STATUS";
          OPENING_BALANCE = "account.OPENING_BALANCE";
          AS_DATE = "account.AS_DATE";
          OB_TRANS_ID = "account.OB_TRANS_ID";
          OB_RATE = "account.OB_RATE";
          ALT_CODE = "account.ALT_CODE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(AccountMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(AccountMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  16;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Account";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseAccountPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(STATUS))
        {
            Object possibleBoolean = criteria.get(STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(ACCOUNT_ID);
          criteria.addSelectColumn(ACCOUNT_CODE);
          criteria.addSelectColumn(ACCOUNT_NAME);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(ACCOUNT_TYPE);
          criteria.addSelectColumn(PARENT_ID);
          criteria.addSelectColumn(HAS_CHILD);
          criteria.addSelectColumn(ACCOUNT_LEVEL);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(NORMAL_BALANCE);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(OPENING_BALANCE);
          criteria.addSelectColumn(AS_DATE);
          criteria.addSelectColumn(OB_TRANS_ID);
          criteria.addSelectColumn(OB_RATE);
          criteria.addSelectColumn(ALT_CODE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Account row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Account obj = (Account) cls.newInstance();
            AccountPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Account obj)
        throws TorqueException
    {
        try
        {
                obj.setAccountId(row.getValue(offset + 0).asString());
                  obj.setAccountCode(row.getValue(offset + 1).asString());
                  obj.setAccountName(row.getValue(offset + 2).asString());
                  obj.setDescription(row.getValue(offset + 3).asString());
                  obj.setAccountType(row.getValue(offset + 4).asInt());
                  obj.setParentId(row.getValue(offset + 5).asString());
                  obj.setHasChild(row.getValue(offset + 6).asBoolean());
                  obj.setAccountLevel(row.getValue(offset + 7).asInt());
                  obj.setCurrencyId(row.getValue(offset + 8).asString());
                  obj.setNormalBalance(row.getValue(offset + 9).asInt());
                  obj.setStatus(row.getValue(offset + 10).asBoolean());
                  obj.setOpeningBalance(row.getValue(offset + 11).asBigDecimal());
                  obj.setAsDate(row.getValue(offset + 12).asUtilDate());
                  obj.setObTransId(row.getValue(offset + 13).asString());
                  obj.setObRate(row.getValue(offset + 14).asBigDecimal());
                  obj.setAltCode(row.getValue(offset + 15).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseAccountPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(STATUS))
        {
            Object possibleBoolean = criteria.get(STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(AccountPeer.row2Object(row, 1,
                AccountPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseAccountPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(ACCOUNT_ID, criteria.remove(ACCOUNT_ID));
                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(STATUS))
        {
            Object possibleBoolean = criteria.get(STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         AccountPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(STATUS))
        {
            Object possibleBoolean = criteria.get(STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Account obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Account obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Account obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Account obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Account) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Account obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Account) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Account obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Account) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Account obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseAccountPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(ACCOUNT_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Account obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(ACCOUNT_ID, obj.getAccountId());
              criteria.add(ACCOUNT_CODE, obj.getAccountCode());
              criteria.add(ACCOUNT_NAME, obj.getAccountName());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(ACCOUNT_TYPE, obj.getAccountType());
              criteria.add(PARENT_ID, obj.getParentId());
              criteria.add(HAS_CHILD, obj.getHasChild());
              criteria.add(ACCOUNT_LEVEL, obj.getAccountLevel());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(NORMAL_BALANCE, obj.getNormalBalance());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
              criteria.add(AS_DATE, obj.getAsDate());
              criteria.add(OB_TRANS_ID, obj.getObTransId());
              criteria.add(OB_RATE, obj.getObRate());
              criteria.add(ALT_CODE, obj.getAltCode());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Account obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(ACCOUNT_ID, obj.getAccountId());
                          criteria.add(ACCOUNT_CODE, obj.getAccountCode());
                          criteria.add(ACCOUNT_NAME, obj.getAccountName());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(ACCOUNT_TYPE, obj.getAccountType());
                          criteria.add(PARENT_ID, obj.getParentId());
                          criteria.add(HAS_CHILD, obj.getHasChild());
                          criteria.add(ACCOUNT_LEVEL, obj.getAccountLevel());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(NORMAL_BALANCE, obj.getNormalBalance());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
                          criteria.add(AS_DATE, obj.getAsDate());
                          criteria.add(OB_TRANS_ID, obj.getObTransId());
                          criteria.add(OB_RATE, obj.getObRate());
                          criteria.add(ALT_CODE, obj.getAltCode());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Account retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Account retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Account retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Account retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Account retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Account)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( ACCOUNT_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
