package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AccountType
 */
public abstract class BaseAccountType extends BaseObject
{
    /** The Peer class */
    private static final AccountTypePeer peer =
        new AccountTypePeer();

        
    /** The value for the accountTypeId field */
    private String accountTypeId;
      
    /** The value for the accountType field */
    private int accountType;
      
    /** The value for the accountTypeCode field */
    private String accountTypeCode;
      
    /** The value for the accountTypeName field */
    private String accountTypeName;
      
    /** The value for the glType field */
    private int glType;
      
    /** The value for the normalBalance field */
    private int normalBalance;
  
    
    /**
     * Get the AccountTypeId
     *
     * @return String
     */
    public String getAccountTypeId()
    {
        return accountTypeId;
    }

                        
    /**
     * Set the value of AccountTypeId
     *
     * @param v new value
     */
    public void setAccountTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountTypeId, v))
              {
            this.accountTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountType
     *
     * @return int
     */
    public int getAccountType()
    {
        return accountType;
    }

                        
    /**
     * Set the value of AccountType
     *
     * @param v new value
     */
    public void setAccountType(int v) 
    {
    
                  if (this.accountType != v)
              {
            this.accountType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountTypeCode
     *
     * @return String
     */
    public String getAccountTypeCode()
    {
        return accountTypeCode;
    }

                        
    /**
     * Set the value of AccountTypeCode
     *
     * @param v new value
     */
    public void setAccountTypeCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountTypeCode, v))
              {
            this.accountTypeCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountTypeName
     *
     * @return String
     */
    public String getAccountTypeName()
    {
        return accountTypeName;
    }

                        
    /**
     * Set the value of AccountTypeName
     *
     * @param v new value
     */
    public void setAccountTypeName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountTypeName, v))
              {
            this.accountTypeName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GlType
     *
     * @return int
     */
    public int getGlType()
    {
        return glType;
    }

                        
    /**
     * Set the value of GlType
     *
     * @param v new value
     */
    public void setGlType(int v) 
    {
    
                  if (this.glType != v)
              {
            this.glType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NormalBalance
     *
     * @return int
     */
    public int getNormalBalance()
    {
        return normalBalance;
    }

                        
    /**
     * Set the value of NormalBalance
     *
     * @param v new value
     */
    public void setNormalBalance(int v) 
    {
    
                  if (this.normalBalance != v)
              {
            this.normalBalance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AccountTypeId");
              fieldNames.add("AccountType");
              fieldNames.add("AccountTypeCode");
              fieldNames.add("AccountTypeName");
              fieldNames.add("GlType");
              fieldNames.add("NormalBalance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AccountTypeId"))
        {
                return getAccountTypeId();
            }
          if (name.equals("AccountType"))
        {
                return Integer.valueOf(getAccountType());
            }
          if (name.equals("AccountTypeCode"))
        {
                return getAccountTypeCode();
            }
          if (name.equals("AccountTypeName"))
        {
                return getAccountTypeName();
            }
          if (name.equals("GlType"))
        {
                return Integer.valueOf(getGlType());
            }
          if (name.equals("NormalBalance"))
        {
                return Integer.valueOf(getNormalBalance());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AccountTypePeer.ACCOUNT_TYPE_ID))
        {
                return getAccountTypeId();
            }
          if (name.equals(AccountTypePeer.ACCOUNT_TYPE))
        {
                return Integer.valueOf(getAccountType());
            }
          if (name.equals(AccountTypePeer.ACCOUNT_TYPE_CODE))
        {
                return getAccountTypeCode();
            }
          if (name.equals(AccountTypePeer.ACCOUNT_TYPE_NAME))
        {
                return getAccountTypeName();
            }
          if (name.equals(AccountTypePeer.GL_TYPE))
        {
                return Integer.valueOf(getGlType());
            }
          if (name.equals(AccountTypePeer.NORMAL_BALANCE))
        {
                return Integer.valueOf(getNormalBalance());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAccountTypeId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getAccountType());
            }
              if (pos == 2)
        {
                return getAccountTypeCode();
            }
              if (pos == 3)
        {
                return getAccountTypeName();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getGlType());
            }
              if (pos == 5)
        {
                return Integer.valueOf(getNormalBalance());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AccountTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AccountTypePeer.doInsert((AccountType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AccountTypePeer.doUpdate((AccountType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key accountTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAccountTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAccountTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAccountTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AccountType copy() throws TorqueException
    {
        return copyInto(new AccountType());
    }
  
    protected AccountType copyInto(AccountType copyObj) throws TorqueException
    {
          copyObj.setAccountTypeId(accountTypeId);
          copyObj.setAccountType(accountType);
          copyObj.setAccountTypeCode(accountTypeCode);
          copyObj.setAccountTypeName(accountTypeName);
          copyObj.setGlType(glType);
          copyObj.setNormalBalance(normalBalance);
  
                    copyObj.setAccountTypeId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AccountTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AccountType\n");
        str.append("-----------\n")
           .append("AccountTypeId        : ")
           .append(getAccountTypeId())
           .append("\n")
           .append("AccountType          : ")
           .append(getAccountType())
           .append("\n")
           .append("AccountTypeCode      : ")
           .append(getAccountTypeCode())
           .append("\n")
           .append("AccountTypeName      : ")
           .append(getAccountTypeName())
           .append("\n")
           .append("GlType               : ")
           .append(getGlType())
           .append("\n")
           .append("NormalBalance        : ")
           .append(getNormalBalance())
           .append("\n")
        ;
        return(str.toString());
    }
}
