package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AdminLog
 */
public abstract class BaseAdminLog extends BaseObject
{
    /** The Peer class */
    private static final AdminLogPeer peer =
        new AdminLogPeer();

        
    /** The value for the adminLogId field */
    private String adminLogId;
      
    /** The value for the transDate field */
    private Date transDate;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the sqlCmd field */
    private String sqlCmd;
                                                
    /** The value for the sqlMsg field */
    private String sqlMsg = "";
  
    
    /**
     * Get the AdminLogId
     *
     * @return String
     */
    public String getAdminLogId()
    {
        return adminLogId;
    }

                        
    /**
     * Set the value of AdminLogId
     *
     * @param v new value
     */
    public void setAdminLogId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.adminLogId, v))
              {
            this.adminLogId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransDate
     *
     * @return Date
     */
    public Date getTransDate()
    {
        return transDate;
    }

                        
    /**
     * Set the value of TransDate
     *
     * @param v new value
     */
    public void setTransDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transDate, v))
              {
            this.transDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SqlCmd
     *
     * @return String
     */
    public String getSqlCmd()
    {
        return sqlCmd;
    }

                        
    /**
     * Set the value of SqlCmd
     *
     * @param v new value
     */
    public void setSqlCmd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sqlCmd, v))
              {
            this.sqlCmd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SqlMsg
     *
     * @return String
     */
    public String getSqlMsg()
    {
        return sqlMsg;
    }

                        
    /**
     * Set the value of SqlMsg
     *
     * @param v new value
     */
    public void setSqlMsg(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sqlMsg, v))
              {
            this.sqlMsg = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AdminLogId");
              fieldNames.add("TransDate");
              fieldNames.add("UserName");
              fieldNames.add("SqlCmd");
              fieldNames.add("SqlMsg");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AdminLogId"))
        {
                return getAdminLogId();
            }
          if (name.equals("TransDate"))
        {
                return getTransDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("SqlCmd"))
        {
                return getSqlCmd();
            }
          if (name.equals("SqlMsg"))
        {
                return getSqlMsg();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AdminLogPeer.ADMIN_LOG_ID))
        {
                return getAdminLogId();
            }
          if (name.equals(AdminLogPeer.TRANS_DATE))
        {
                return getTransDate();
            }
          if (name.equals(AdminLogPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(AdminLogPeer.SQL_CMD))
        {
                return getSqlCmd();
            }
          if (name.equals(AdminLogPeer.SQL_MSG))
        {
                return getSqlMsg();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAdminLogId();
            }
              if (pos == 1)
        {
                return getTransDate();
            }
              if (pos == 2)
        {
                return getUserName();
            }
              if (pos == 3)
        {
                return getSqlCmd();
            }
              if (pos == 4)
        {
                return getSqlMsg();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AdminLogPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AdminLogPeer.doInsert((AdminLog) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AdminLogPeer.doUpdate((AdminLog) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key adminLogId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAdminLogId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAdminLogId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAdminLogId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AdminLog copy() throws TorqueException
    {
        return copyInto(new AdminLog());
    }
  
    protected AdminLog copyInto(AdminLog copyObj) throws TorqueException
    {
          copyObj.setAdminLogId(adminLogId);
          copyObj.setTransDate(transDate);
          copyObj.setUserName(userName);
          copyObj.setSqlCmd(sqlCmd);
          copyObj.setSqlMsg(sqlMsg);
  
                    copyObj.setAdminLogId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AdminLogPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AdminLog\n");
        str.append("--------\n")
           .append("AdminLogId           : ")
           .append(getAdminLogId())
           .append("\n")
           .append("TransDate            : ")
           .append(getTransDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("SqlCmd               : ")
           .append(getSqlCmd())
           .append("\n")
           .append("SqlMsg               : ")
           .append(getSqlMsg())
           .append("\n")
        ;
        return(str.toString());
    }
}
