package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ApPayment
 */
public abstract class BaseApPayment extends BaseObject
{
    /** The Peer class */
    private static final ApPaymentPeer peer =
        new ApPaymentPeer();

        
    /** The value for the apPaymentId field */
    private String apPaymentId;
      
    /** The value for the apPaymentNo field */
    private String apPaymentNo;
      
    /** The value for the apPaymentDate field */
    private Date apPaymentDate;
      
    /** The value for the apPaymentDueDate field */
    private Date apPaymentDueDate;
      
    /** The value for the bankId field */
    private String bankId;
      
    /** The value for the bankIssuer field */
    private String bankIssuer;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the referenceNo field */
    private String referenceNo;
      
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount;
      
    /** The value for the totalDownPayment field */
    private BigDecimal totalDownPayment;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
      
    /** The value for the status field */
    private int status;
                                                
          
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount= bd_ZERO;
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                          
    /** The value for the cfStatus field */
    private int cfStatus = 1;
      
    /** The value for the cfDate field */
    private Date cfDate;
                                                
    /** The value for the cashFlowId field */
    private String cashFlowId = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the ApPaymentId
     *
     * @return String
     */
    public String getApPaymentId()
    {
        return apPaymentId;
    }

                                              
    /**
     * Set the value of ApPaymentId
     *
     * @param v new value
     */
    public void setApPaymentId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.apPaymentId, v))
              {
            this.apPaymentId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated ApPaymentDetail
        if (collApPaymentDetails != null)
        {
            for (int i = 0; i < collApPaymentDetails.size(); i++)
            {
                ((ApPaymentDetail) collApPaymentDetails.get(i))
                    .setApPaymentId(v);
            }
        }
                                }
  
    /**
     * Get the ApPaymentNo
     *
     * @return String
     */
    public String getApPaymentNo()
    {
        return apPaymentNo;
    }

                        
    /**
     * Set the value of ApPaymentNo
     *
     * @param v new value
     */
    public void setApPaymentNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.apPaymentNo, v))
              {
            this.apPaymentNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApPaymentDate
     *
     * @return Date
     */
    public Date getApPaymentDate()
    {
        return apPaymentDate;
    }

                        
    /**
     * Set the value of ApPaymentDate
     *
     * @param v new value
     */
    public void setApPaymentDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.apPaymentDate, v))
              {
            this.apPaymentDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApPaymentDueDate
     *
     * @return Date
     */
    public Date getApPaymentDueDate()
    {
        return apPaymentDueDate;
    }

                        
    /**
     * Set the value of ApPaymentDueDate
     *
     * @param v new value
     */
    public void setApPaymentDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.apPaymentDueDate, v))
              {
            this.apPaymentDueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDownPayment
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDownPayment()
    {
        return totalDownPayment;
    }

                        
    /**
     * Set the value of TotalDownPayment
     *
     * @param v new value
     */
    public void setTotalDownPayment(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDownPayment, v))
              {
            this.totalDownPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CfStatus
     *
     * @return int
     */
    public int getCfStatus()
    {
        return cfStatus;
    }

                        
    /**
     * Set the value of CfStatus
     *
     * @param v new value
     */
    public void setCfStatus(int v) 
    {
    
                  if (this.cfStatus != v)
              {
            this.cfStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CfDate
     *
     * @return Date
     */
    public Date getCfDate()
    {
        return cfDate;
    }

                        
    /**
     * Set the value of CfDate
     *
     * @param v new value
     */
    public void setCfDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cfDate, v))
              {
            this.cfDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowId
     *
     * @return String
     */
    public String getCashFlowId()
    {
        return cashFlowId;
    }

                        
    /**
     * Set the value of CashFlowId
     *
     * @param v new value
     */
    public void setCashFlowId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowId, v))
              {
            this.cashFlowId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collApPaymentDetails
     */
    protected List collApPaymentDetails;

    /**
     * Temporary storage of collApPaymentDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initApPaymentDetails()
    {
        if (collApPaymentDetails == null)
        {
            collApPaymentDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a ApPaymentDetail object to this object
     * through the ApPaymentDetail foreign key attribute
     *
     * @param l ApPaymentDetail
     * @throws TorqueException
     */
    public void addApPaymentDetail(ApPaymentDetail l) throws TorqueException
    {
        getApPaymentDetails().add(l);
        l.setApPayment((ApPayment) this);
    }

    /**
     * The criteria used to select the current contents of collApPaymentDetails
     */
    private Criteria lastApPaymentDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getApPaymentDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getApPaymentDetails() throws TorqueException
    {
              if (collApPaymentDetails == null)
        {
            collApPaymentDetails = getApPaymentDetails(new Criteria(10));
        }
        return collApPaymentDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApPayment has previously
     * been saved, it will retrieve related ApPaymentDetails from storage.
     * If this ApPayment is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getApPaymentDetails(Criteria criteria) throws TorqueException
    {
              if (collApPaymentDetails == null)
        {
            if (isNew())
            {
               collApPaymentDetails = new ArrayList();
            }
            else
            {
                        criteria.add(ApPaymentDetailPeer.AP_PAYMENT_ID, getApPaymentId() );
                        collApPaymentDetails = ApPaymentDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ApPaymentDetailPeer.AP_PAYMENT_ID, getApPaymentId());
                            if (!lastApPaymentDetailsCriteria.equals(criteria))
                {
                    collApPaymentDetails = ApPaymentDetailPeer.doSelect(criteria);
                }
            }
        }
        lastApPaymentDetailsCriteria = criteria;

        return collApPaymentDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getApPaymentDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getApPaymentDetails(Connection con) throws TorqueException
    {
              if (collApPaymentDetails == null)
        {
            collApPaymentDetails = getApPaymentDetails(new Criteria(10), con);
        }
        return collApPaymentDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApPayment has previously
     * been saved, it will retrieve related ApPaymentDetails from storage.
     * If this ApPayment is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getApPaymentDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collApPaymentDetails == null)
        {
            if (isNew())
            {
               collApPaymentDetails = new ArrayList();
            }
            else
            {
                         criteria.add(ApPaymentDetailPeer.AP_PAYMENT_ID, getApPaymentId());
                         collApPaymentDetails = ApPaymentDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ApPaymentDetailPeer.AP_PAYMENT_ID, getApPaymentId());
                             if (!lastApPaymentDetailsCriteria.equals(criteria))
                 {
                     collApPaymentDetails = ApPaymentDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastApPaymentDetailsCriteria = criteria;

         return collApPaymentDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApPayment is new, it will return
     * an empty collection; or if this ApPayment has previously
     * been saved, it will retrieve related ApPaymentDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApPayment.
     */
    protected List getApPaymentDetailsJoinApPayment(Criteria criteria)
        throws TorqueException
    {
                    if (collApPaymentDetails == null)
        {
            if (isNew())
            {
               collApPaymentDetails = new ArrayList();
            }
            else
            {
                              criteria.add(ApPaymentDetailPeer.AP_PAYMENT_ID, getApPaymentId());
                              collApPaymentDetails = ApPaymentDetailPeer.doSelectJoinApPayment(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ApPaymentDetailPeer.AP_PAYMENT_ID, getApPaymentId());
                                    if (!lastApPaymentDetailsCriteria.equals(criteria))
            {
                collApPaymentDetails = ApPaymentDetailPeer.doSelectJoinApPayment(criteria);
            }
        }
        lastApPaymentDetailsCriteria = criteria;

        return collApPaymentDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ApPaymentId");
              fieldNames.add("ApPaymentNo");
              fieldNames.add("ApPaymentDate");
              fieldNames.add("ApPaymentDueDate");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("ReferenceNo");
              fieldNames.add("PaymentAmount");
              fieldNames.add("TotalDownPayment");
              fieldNames.add("TotalAmount");
              fieldNames.add("UserName");
              fieldNames.add("Remark");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("FiscalRate");
              fieldNames.add("Status");
              fieldNames.add("TotalDiscount");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("LocationId");
              fieldNames.add("CfStatus");
              fieldNames.add("CfDate");
              fieldNames.add("CashFlowId");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ApPaymentId"))
        {
                return getApPaymentId();
            }
          if (name.equals("ApPaymentNo"))
        {
                return getApPaymentNo();
            }
          if (name.equals("ApPaymentDate"))
        {
                return getApPaymentDate();
            }
          if (name.equals("ApPaymentDueDate"))
        {
                return getApPaymentDueDate();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("TotalDownPayment"))
        {
                return getTotalDownPayment();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("CfStatus"))
        {
                return Integer.valueOf(getCfStatus());
            }
          if (name.equals("CfDate"))
        {
                return getCfDate();
            }
          if (name.equals("CashFlowId"))
        {
                return getCashFlowId();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ApPaymentPeer.AP_PAYMENT_ID))
        {
                return getApPaymentId();
            }
          if (name.equals(ApPaymentPeer.AP_PAYMENT_NO))
        {
                return getApPaymentNo();
            }
          if (name.equals(ApPaymentPeer.AP_PAYMENT_DATE))
        {
                return getApPaymentDate();
            }
          if (name.equals(ApPaymentPeer.AP_PAYMENT_DUE_DATE))
        {
                return getApPaymentDueDate();
            }
          if (name.equals(ApPaymentPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(ApPaymentPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(ApPaymentPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(ApPaymentPeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(ApPaymentPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(ApPaymentPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(ApPaymentPeer.TOTAL_DOWN_PAYMENT))
        {
                return getTotalDownPayment();
            }
          if (name.equals(ApPaymentPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(ApPaymentPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(ApPaymentPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(ApPaymentPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(ApPaymentPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(ApPaymentPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(ApPaymentPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(ApPaymentPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(ApPaymentPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(ApPaymentPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(ApPaymentPeer.CF_STATUS))
        {
                return Integer.valueOf(getCfStatus());
            }
          if (name.equals(ApPaymentPeer.CF_DATE))
        {
                return getCfDate();
            }
          if (name.equals(ApPaymentPeer.CASH_FLOW_ID))
        {
                return getCashFlowId();
            }
          if (name.equals(ApPaymentPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(ApPaymentPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getApPaymentId();
            }
              if (pos == 1)
        {
                return getApPaymentNo();
            }
              if (pos == 2)
        {
                return getApPaymentDate();
            }
              if (pos == 3)
        {
                return getApPaymentDueDate();
            }
              if (pos == 4)
        {
                return getBankId();
            }
              if (pos == 5)
        {
                return getBankIssuer();
            }
              if (pos == 6)
        {
                return getVendorId();
            }
              if (pos == 7)
        {
                return getVendorName();
            }
              if (pos == 8)
        {
                return getReferenceNo();
            }
              if (pos == 9)
        {
                return getPaymentAmount();
            }
              if (pos == 10)
        {
                return getTotalDownPayment();
            }
              if (pos == 11)
        {
                return getTotalAmount();
            }
              if (pos == 12)
        {
                return getUserName();
            }
              if (pos == 13)
        {
                return getRemark();
            }
              if (pos == 14)
        {
                return getCurrencyId();
            }
              if (pos == 15)
        {
                return getCurrencyRate();
            }
              if (pos == 16)
        {
                return getFiscalRate();
            }
              if (pos == 17)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 18)
        {
                return getTotalDiscount();
            }
              if (pos == 19)
        {
                return getCashFlowTypeId();
            }
              if (pos == 20)
        {
                return getLocationId();
            }
              if (pos == 21)
        {
                return Integer.valueOf(getCfStatus());
            }
              if (pos == 22)
        {
                return getCfDate();
            }
              if (pos == 23)
        {
                return getCashFlowId();
            }
              if (pos == 24)
        {
                return getCancelBy();
            }
              if (pos == 25)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ApPaymentPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ApPaymentPeer.doInsert((ApPayment) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ApPaymentPeer.doUpdate((ApPayment) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collApPaymentDetails != null)
            {
                for (int i = 0; i < collApPaymentDetails.size(); i++)
                {
                    ((ApPaymentDetail) collApPaymentDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key apPaymentId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setApPaymentId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setApPaymentId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getApPaymentId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ApPayment copy() throws TorqueException
    {
        return copyInto(new ApPayment());
    }
  
    protected ApPayment copyInto(ApPayment copyObj) throws TorqueException
    {
          copyObj.setApPaymentId(apPaymentId);
          copyObj.setApPaymentNo(apPaymentNo);
          copyObj.setApPaymentDate(apPaymentDate);
          copyObj.setApPaymentDueDate(apPaymentDueDate);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setTotalDownPayment(totalDownPayment);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setUserName(userName);
          copyObj.setRemark(remark);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setStatus(status);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setLocationId(locationId);
          copyObj.setCfStatus(cfStatus);
          copyObj.setCfDate(cfDate);
          copyObj.setCashFlowId(cashFlowId);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setApPaymentId((String)null);
                                                                                                                                                                  
                                      
                            
        List v = getApPaymentDetails();
        for (int i = 0; i < v.size(); i++)
        {
            ApPaymentDetail obj = (ApPaymentDetail) v.get(i);
            copyObj.addApPaymentDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ApPaymentPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ApPayment\n");
        str.append("---------\n")
           .append("ApPaymentId          : ")
           .append(getApPaymentId())
           .append("\n")
           .append("ApPaymentNo          : ")
           .append(getApPaymentNo())
           .append("\n")
           .append("ApPaymentDate        : ")
           .append(getApPaymentDate())
           .append("\n")
           .append("ApPaymentDueDate     : ")
           .append(getApPaymentDueDate())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("TotalDownPayment     : ")
           .append(getTotalDownPayment())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("CfStatus             : ")
           .append(getCfStatus())
           .append("\n")
           .append("CfDate               : ")
           .append(getCfDate())
           .append("\n")
           .append("CashFlowId           : ")
           .append(getCashFlowId())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
