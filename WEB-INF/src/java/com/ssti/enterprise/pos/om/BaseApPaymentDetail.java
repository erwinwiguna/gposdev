package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ApPaymentDetail
 */
public abstract class BaseApPaymentDetail extends BaseObject
{
    /** The Peer class */
    private static final ApPaymentDetailPeer peer =
        new ApPaymentDetailPeer();

        
    /** The value for the apPaymentDetailId field */
    private String apPaymentDetailId;
      
    /** The value for the apPaymentId field */
    private String apPaymentId;
      
    /** The value for the purchaseInvoiceId field */
    private String purchaseInvoiceId;
      
    /** The value for the purchaseInvoiceNo field */
    private String purchaseInvoiceNo;
      
    /** The value for the purchaseInvoiceAmount field */
    private BigDecimal purchaseInvoiceAmount;
                                                
          
    /** The value for the invoiceRate field */
    private BigDecimal invoiceRate= new BigDecimal(1);
      
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount;
      
    /** The value for the downPayment field */
    private BigDecimal downPayment;
      
    /** The value for the paidAmount field */
    private BigDecimal paidAmount;
                                                
          
    /** The value for the discountAmount field */
    private BigDecimal discountAmount= bd_ZERO;
                                                
    /** The value for the discountAccountId field */
    private String discountAccountId = "";
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
                                                
    /** The value for the projectId field */
    private String projectId = "";
                                                
    /** The value for the memoId field */
    private String memoId = "";
                                                        
    /** The value for the taxPayment field */
    private boolean taxPayment = false;
  
    
    /**
     * Get the ApPaymentDetailId
     *
     * @return String
     */
    public String getApPaymentDetailId()
    {
        return apPaymentDetailId;
    }

                                              
    /**
     * Set the value of ApPaymentDetailId
     *
     * @param v new value
     */
    public void setApPaymentDetailId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.apPaymentDetailId, v))
              {
            this.apPaymentDetailId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated ApPaymentOther
        if (collApPaymentOthers != null)
        {
            for (int i = 0; i < collApPaymentOthers.size(); i++)
            {
                ((ApPaymentOther) collApPaymentOthers.get(i))
                    .setApPaymentDetailId(v);
            }
        }
                                }
  
    /**
     * Get the ApPaymentId
     *
     * @return String
     */
    public String getApPaymentId()
    {
        return apPaymentId;
    }

                              
    /**
     * Set the value of ApPaymentId
     *
     * @param v new value
     */
    public void setApPaymentId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.apPaymentId, v))
              {
            this.apPaymentId = v;
            setModified(true);
        }
    
                          
                if (aApPayment != null && !ObjectUtils.equals(aApPayment.getApPaymentId(), v))
                {
            aApPayment = null;
        }
      
              }
  
    /**
     * Get the PurchaseInvoiceId
     *
     * @return String
     */
    public String getPurchaseInvoiceId()
    {
        return purchaseInvoiceId;
    }

                        
    /**
     * Set the value of PurchaseInvoiceId
     *
     * @param v new value
     */
    public void setPurchaseInvoiceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceId, v))
              {
            this.purchaseInvoiceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseInvoiceNo
     *
     * @return String
     */
    public String getPurchaseInvoiceNo()
    {
        return purchaseInvoiceNo;
    }

                        
    /**
     * Set the value of PurchaseInvoiceNo
     *
     * @param v new value
     */
    public void setPurchaseInvoiceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceNo, v))
              {
            this.purchaseInvoiceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseInvoiceAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPurchaseInvoiceAmount()
    {
        return purchaseInvoiceAmount;
    }

                        
    /**
     * Set the value of PurchaseInvoiceAmount
     *
     * @param v new value
     */
    public void setPurchaseInvoiceAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceAmount, v))
              {
            this.purchaseInvoiceAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceRate
     *
     * @return BigDecimal
     */
    public BigDecimal getInvoiceRate()
    {
        return invoiceRate;
    }

                        
    /**
     * Set the value of InvoiceRate
     *
     * @param v new value
     */
    public void setInvoiceRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceRate, v))
              {
            this.invoiceRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DownPayment
     *
     * @return BigDecimal
     */
    public BigDecimal getDownPayment()
    {
        return downPayment;
    }

                        
    /**
     * Set the value of DownPayment
     *
     * @param v new value
     */
    public void setDownPayment(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.downPayment, v))
              {
            this.downPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaidAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaidAmount()
    {
        return paidAmount;
    }

                        
    /**
     * Set the value of PaidAmount
     *
     * @param v new value
     */
    public void setPaidAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paidAmount, v))
              {
            this.paidAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getDiscountAmount()
    {
        return discountAmount;
    }

                        
    /**
     * Set the value of DiscountAmount
     *
     * @param v new value
     */
    public void setDiscountAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAmount, v))
              {
            this.discountAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAccountId
     *
     * @return String
     */
    public String getDiscountAccountId()
    {
        return discountAccountId;
    }

                        
    /**
     * Set the value of DiscountAccountId
     *
     * @param v new value
     */
    public void setDiscountAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAccountId, v))
              {
            this.discountAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MemoId
     *
     * @return String
     */
    public String getMemoId()
    {
        return memoId;
    }

                        
    /**
     * Set the value of MemoId
     *
     * @param v new value
     */
    public void setMemoId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memoId, v))
              {
            this.memoId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxPayment
     *
     * @return boolean
     */
    public boolean getTaxPayment()
    {
        return taxPayment;
    }

                        
    /**
     * Set the value of TaxPayment
     *
     * @param v new value
     */
    public void setTaxPayment(boolean v) 
    {
    
                  if (this.taxPayment != v)
              {
            this.taxPayment = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private ApPayment aApPayment;

    /**
     * Declares an association between this object and a ApPayment object
     *
     * @param v ApPayment
     * @throws TorqueException
     */
    public void setApPayment(ApPayment v) throws TorqueException
    {
            if (v == null)
        {
                  setApPaymentId((String) null);
              }
        else
        {
            setApPaymentId(v.getApPaymentId());
        }
            aApPayment = v;
    }

                                            
    /**
     * Get the associated ApPayment object
     *
     * @return the associated ApPayment object
     * @throws TorqueException
     */
    public ApPayment getApPayment() throws TorqueException
    {
        if (aApPayment == null && (!ObjectUtils.equals(this.apPaymentId, null)))
        {
                          aApPayment = ApPaymentPeer.retrieveByPK(SimpleKey.keyFor(this.apPaymentId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               ApPayment obj = ApPaymentPeer.retrieveByPK(this.apPaymentId);
               obj.addApPaymentDetails(this);
            */
        }
        return aApPayment;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setApPaymentKey(ObjectKey key) throws TorqueException
    {
      
                        setApPaymentId(key.toString());
                  }
       
                                
            
          /**
     * Collection to store aggregation of collApPaymentOthers
     */
    protected List collApPaymentOthers;

    /**
     * Temporary storage of collApPaymentOthers to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initApPaymentOthers()
    {
        if (collApPaymentOthers == null)
        {
            collApPaymentOthers = new ArrayList();
        }
    }

    /**
     * Method called to associate a ApPaymentOther object to this object
     * through the ApPaymentOther foreign key attribute
     *
     * @param l ApPaymentOther
     * @throws TorqueException
     */
    public void addApPaymentOther(ApPaymentOther l) throws TorqueException
    {
        getApPaymentOthers().add(l);
        l.setApPaymentDetail((ApPaymentDetail) this);
    }

    /**
     * The criteria used to select the current contents of collApPaymentOthers
     */
    private Criteria lastApPaymentOthersCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getApPaymentOthers(new Criteria())
     *
     * @throws TorqueException
     */
    public List getApPaymentOthers() throws TorqueException
    {
              if (collApPaymentOthers == null)
        {
            collApPaymentOthers = getApPaymentOthers(new Criteria(10));
        }
        return collApPaymentOthers;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApPaymentDetail has previously
     * been saved, it will retrieve related ApPaymentOthers from storage.
     * If this ApPaymentDetail is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getApPaymentOthers(Criteria criteria) throws TorqueException
    {
              if (collApPaymentOthers == null)
        {
            if (isNew())
            {
               collApPaymentOthers = new ArrayList();
            }
            else
            {
                        criteria.add(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID, getApPaymentDetailId() );
                        collApPaymentOthers = ApPaymentOtherPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID, getApPaymentDetailId());
                            if (!lastApPaymentOthersCriteria.equals(criteria))
                {
                    collApPaymentOthers = ApPaymentOtherPeer.doSelect(criteria);
                }
            }
        }
        lastApPaymentOthersCriteria = criteria;

        return collApPaymentOthers;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getApPaymentOthers(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getApPaymentOthers(Connection con) throws TorqueException
    {
              if (collApPaymentOthers == null)
        {
            collApPaymentOthers = getApPaymentOthers(new Criteria(10), con);
        }
        return collApPaymentOthers;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApPaymentDetail has previously
     * been saved, it will retrieve related ApPaymentOthers from storage.
     * If this ApPaymentDetail is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getApPaymentOthers(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collApPaymentOthers == null)
        {
            if (isNew())
            {
               collApPaymentOthers = new ArrayList();
            }
            else
            {
                         criteria.add(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID, getApPaymentDetailId());
                         collApPaymentOthers = ApPaymentOtherPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID, getApPaymentDetailId());
                             if (!lastApPaymentOthersCriteria.equals(criteria))
                 {
                     collApPaymentOthers = ApPaymentOtherPeer.doSelect(criteria, con);
                 }
             }
         }
         lastApPaymentOthersCriteria = criteria;

         return collApPaymentOthers;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApPaymentDetail is new, it will return
     * an empty collection; or if this ApPaymentDetail has previously
     * been saved, it will retrieve related ApPaymentOthers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApPaymentDetail.
     */
    protected List getApPaymentOthersJoinApPaymentDetail(Criteria criteria)
        throws TorqueException
    {
                    if (collApPaymentOthers == null)
        {
            if (isNew())
            {
               collApPaymentOthers = new ArrayList();
            }
            else
            {
                              criteria.add(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID, getApPaymentDetailId());
                              collApPaymentOthers = ApPaymentOtherPeer.doSelectJoinApPaymentDetail(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID, getApPaymentDetailId());
                                    if (!lastApPaymentOthersCriteria.equals(criteria))
            {
                collApPaymentOthers = ApPaymentOtherPeer.doSelectJoinApPaymentDetail(criteria);
            }
        }
        lastApPaymentOthersCriteria = criteria;

        return collApPaymentOthers;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ApPaymentDetailId");
              fieldNames.add("ApPaymentId");
              fieldNames.add("PurchaseInvoiceId");
              fieldNames.add("PurchaseInvoiceNo");
              fieldNames.add("PurchaseInvoiceAmount");
              fieldNames.add("InvoiceRate");
              fieldNames.add("PaymentAmount");
              fieldNames.add("DownPayment");
              fieldNames.add("PaidAmount");
              fieldNames.add("DiscountAmount");
              fieldNames.add("DiscountAccountId");
              fieldNames.add("DepartmentId");
              fieldNames.add("ProjectId");
              fieldNames.add("MemoId");
              fieldNames.add("TaxPayment");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ApPaymentDetailId"))
        {
                return getApPaymentDetailId();
            }
          if (name.equals("ApPaymentId"))
        {
                return getApPaymentId();
            }
          if (name.equals("PurchaseInvoiceId"))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals("PurchaseInvoiceNo"))
        {
                return getPurchaseInvoiceNo();
            }
          if (name.equals("PurchaseInvoiceAmount"))
        {
                return getPurchaseInvoiceAmount();
            }
          if (name.equals("InvoiceRate"))
        {
                return getInvoiceRate();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("DownPayment"))
        {
                return getDownPayment();
            }
          if (name.equals("PaidAmount"))
        {
                return getPaidAmount();
            }
          if (name.equals("DiscountAmount"))
        {
                return getDiscountAmount();
            }
          if (name.equals("DiscountAccountId"))
        {
                return getDiscountAccountId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("MemoId"))
        {
                return getMemoId();
            }
          if (name.equals("TaxPayment"))
        {
                return Boolean.valueOf(getTaxPayment());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ApPaymentDetailPeer.AP_PAYMENT_DETAIL_ID))
        {
                return getApPaymentDetailId();
            }
          if (name.equals(ApPaymentDetailPeer.AP_PAYMENT_ID))
        {
                return getApPaymentId();
            }
          if (name.equals(ApPaymentDetailPeer.PURCHASE_INVOICE_ID))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals(ApPaymentDetailPeer.PURCHASE_INVOICE_NO))
        {
                return getPurchaseInvoiceNo();
            }
          if (name.equals(ApPaymentDetailPeer.PURCHASE_INVOICE_AMOUNT))
        {
                return getPurchaseInvoiceAmount();
            }
          if (name.equals(ApPaymentDetailPeer.INVOICE_RATE))
        {
                return getInvoiceRate();
            }
          if (name.equals(ApPaymentDetailPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(ApPaymentDetailPeer.DOWN_PAYMENT))
        {
                return getDownPayment();
            }
          if (name.equals(ApPaymentDetailPeer.PAID_AMOUNT))
        {
                return getPaidAmount();
            }
          if (name.equals(ApPaymentDetailPeer.DISCOUNT_AMOUNT))
        {
                return getDiscountAmount();
            }
          if (name.equals(ApPaymentDetailPeer.DISCOUNT_ACCOUNT_ID))
        {
                return getDiscountAccountId();
            }
          if (name.equals(ApPaymentDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(ApPaymentDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(ApPaymentDetailPeer.MEMO_ID))
        {
                return getMemoId();
            }
          if (name.equals(ApPaymentDetailPeer.TAX_PAYMENT))
        {
                return Boolean.valueOf(getTaxPayment());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getApPaymentDetailId();
            }
              if (pos == 1)
        {
                return getApPaymentId();
            }
              if (pos == 2)
        {
                return getPurchaseInvoiceId();
            }
              if (pos == 3)
        {
                return getPurchaseInvoiceNo();
            }
              if (pos == 4)
        {
                return getPurchaseInvoiceAmount();
            }
              if (pos == 5)
        {
                return getInvoiceRate();
            }
              if (pos == 6)
        {
                return getPaymentAmount();
            }
              if (pos == 7)
        {
                return getDownPayment();
            }
              if (pos == 8)
        {
                return getPaidAmount();
            }
              if (pos == 9)
        {
                return getDiscountAmount();
            }
              if (pos == 10)
        {
                return getDiscountAccountId();
            }
              if (pos == 11)
        {
                return getDepartmentId();
            }
              if (pos == 12)
        {
                return getProjectId();
            }
              if (pos == 13)
        {
                return getMemoId();
            }
              if (pos == 14)
        {
                return Boolean.valueOf(getTaxPayment());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ApPaymentDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ApPaymentDetailPeer.doInsert((ApPaymentDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ApPaymentDetailPeer.doUpdate((ApPaymentDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collApPaymentOthers != null)
            {
                for (int i = 0; i < collApPaymentOthers.size(); i++)
                {
                    ((ApPaymentOther) collApPaymentOthers.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key apPaymentDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setApPaymentDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setApPaymentDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getApPaymentDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ApPaymentDetail copy() throws TorqueException
    {
        return copyInto(new ApPaymentDetail());
    }
  
    protected ApPaymentDetail copyInto(ApPaymentDetail copyObj) throws TorqueException
    {
          copyObj.setApPaymentDetailId(apPaymentDetailId);
          copyObj.setApPaymentId(apPaymentId);
          copyObj.setPurchaseInvoiceId(purchaseInvoiceId);
          copyObj.setPurchaseInvoiceNo(purchaseInvoiceNo);
          copyObj.setPurchaseInvoiceAmount(purchaseInvoiceAmount);
          copyObj.setInvoiceRate(invoiceRate);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setDownPayment(downPayment);
          copyObj.setPaidAmount(paidAmount);
          copyObj.setDiscountAmount(discountAmount);
          copyObj.setDiscountAccountId(discountAccountId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setProjectId(projectId);
          copyObj.setMemoId(memoId);
          copyObj.setTaxPayment(taxPayment);
  
                    copyObj.setApPaymentDetailId((String)null);
                                                                                                
                                      
                            
        List v = getApPaymentOthers();
        for (int i = 0; i < v.size(); i++)
        {
            ApPaymentOther obj = (ApPaymentOther) v.get(i);
            copyObj.addApPaymentOther(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ApPaymentDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ApPaymentDetail\n");
        str.append("---------------\n")
           .append("ApPaymentDetailId    : ")
           .append(getApPaymentDetailId())
           .append("\n")
           .append("ApPaymentId          : ")
           .append(getApPaymentId())
           .append("\n")
           .append("PurchaseInvoiceId    : ")
           .append(getPurchaseInvoiceId())
           .append("\n")
           .append("PurchaseInvoiceNo    : ")
           .append(getPurchaseInvoiceNo())
           .append("\n")
            .append("PurchaseInvoiceAmount   : ")
           .append(getPurchaseInvoiceAmount())
           .append("\n")
           .append("InvoiceRate          : ")
           .append(getInvoiceRate())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("DownPayment          : ")
           .append(getDownPayment())
           .append("\n")
           .append("PaidAmount           : ")
           .append(getPaidAmount())
           .append("\n")
           .append("DiscountAmount       : ")
           .append(getDiscountAmount())
           .append("\n")
           .append("DiscountAccountId    : ")
           .append(getDiscountAccountId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("MemoId               : ")
           .append(getMemoId())
           .append("\n")
           .append("TaxPayment           : ")
           .append(getTaxPayment())
           .append("\n")
        ;
        return(str.toString());
    }
}
