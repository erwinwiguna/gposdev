package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.ApPaymentDetailMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BaseApPaymentDetailPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "ap_payment_detail";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(ApPaymentDetailMapBuilder.CLASS_NAME);
    }

      /** the column name for the AP_PAYMENT_DETAIL_ID field */
    public static final String AP_PAYMENT_DETAIL_ID;
      /** the column name for the AP_PAYMENT_ID field */
    public static final String AP_PAYMENT_ID;
      /** the column name for the PURCHASE_INVOICE_ID field */
    public static final String PURCHASE_INVOICE_ID;
      /** the column name for the PURCHASE_INVOICE_NO field */
    public static final String PURCHASE_INVOICE_NO;
      /** the column name for the PURCHASE_INVOICE_AMOUNT field */
    public static final String PURCHASE_INVOICE_AMOUNT;
      /** the column name for the INVOICE_RATE field */
    public static final String INVOICE_RATE;
      /** the column name for the PAYMENT_AMOUNT field */
    public static final String PAYMENT_AMOUNT;
      /** the column name for the DOWN_PAYMENT field */
    public static final String DOWN_PAYMENT;
      /** the column name for the PAID_AMOUNT field */
    public static final String PAID_AMOUNT;
      /** the column name for the DISCOUNT_AMOUNT field */
    public static final String DISCOUNT_AMOUNT;
      /** the column name for the DISCOUNT_ACCOUNT_ID field */
    public static final String DISCOUNT_ACCOUNT_ID;
      /** the column name for the DEPARTMENT_ID field */
    public static final String DEPARTMENT_ID;
      /** the column name for the PROJECT_ID field */
    public static final String PROJECT_ID;
      /** the column name for the MEMO_ID field */
    public static final String MEMO_ID;
      /** the column name for the TAX_PAYMENT field */
    public static final String TAX_PAYMENT;
  
    static
    {
          AP_PAYMENT_DETAIL_ID = "ap_payment_detail.AP_PAYMENT_DETAIL_ID";
          AP_PAYMENT_ID = "ap_payment_detail.AP_PAYMENT_ID";
          PURCHASE_INVOICE_ID = "ap_payment_detail.PURCHASE_INVOICE_ID";
          PURCHASE_INVOICE_NO = "ap_payment_detail.PURCHASE_INVOICE_NO";
          PURCHASE_INVOICE_AMOUNT = "ap_payment_detail.PURCHASE_INVOICE_AMOUNT";
          INVOICE_RATE = "ap_payment_detail.INVOICE_RATE";
          PAYMENT_AMOUNT = "ap_payment_detail.PAYMENT_AMOUNT";
          DOWN_PAYMENT = "ap_payment_detail.DOWN_PAYMENT";
          PAID_AMOUNT = "ap_payment_detail.PAID_AMOUNT";
          DISCOUNT_AMOUNT = "ap_payment_detail.DISCOUNT_AMOUNT";
          DISCOUNT_ACCOUNT_ID = "ap_payment_detail.DISCOUNT_ACCOUNT_ID";
          DEPARTMENT_ID = "ap_payment_detail.DEPARTMENT_ID";
          PROJECT_ID = "ap_payment_detail.PROJECT_ID";
          MEMO_ID = "ap_payment_detail.MEMO_ID";
          TAX_PAYMENT = "ap_payment_detail.TAX_PAYMENT";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(ApPaymentDetailMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(ApPaymentDetailMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  15;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.ApPaymentDetail";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseApPaymentDetailPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TAX_PAYMENT))
        {
            Object possibleBoolean = criteria.get(TAX_PAYMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TAX_PAYMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(AP_PAYMENT_DETAIL_ID);
          criteria.addSelectColumn(AP_PAYMENT_ID);
          criteria.addSelectColumn(PURCHASE_INVOICE_ID);
          criteria.addSelectColumn(PURCHASE_INVOICE_NO);
          criteria.addSelectColumn(PURCHASE_INVOICE_AMOUNT);
          criteria.addSelectColumn(INVOICE_RATE);
          criteria.addSelectColumn(PAYMENT_AMOUNT);
          criteria.addSelectColumn(DOWN_PAYMENT);
          criteria.addSelectColumn(PAID_AMOUNT);
          criteria.addSelectColumn(DISCOUNT_AMOUNT);
          criteria.addSelectColumn(DISCOUNT_ACCOUNT_ID);
          criteria.addSelectColumn(DEPARTMENT_ID);
          criteria.addSelectColumn(PROJECT_ID);
          criteria.addSelectColumn(MEMO_ID);
          criteria.addSelectColumn(TAX_PAYMENT);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ApPaymentDetail row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            ApPaymentDetail obj = (ApPaymentDetail) cls.newInstance();
            ApPaymentDetailPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      ApPaymentDetail obj)
        throws TorqueException
    {
        try
        {
                obj.setApPaymentDetailId(row.getValue(offset + 0).asString());
                  obj.setApPaymentId(row.getValue(offset + 1).asString());
                  obj.setPurchaseInvoiceId(row.getValue(offset + 2).asString());
                  obj.setPurchaseInvoiceNo(row.getValue(offset + 3).asString());
                  obj.setPurchaseInvoiceAmount(row.getValue(offset + 4).asBigDecimal());
                  obj.setInvoiceRate(row.getValue(offset + 5).asBigDecimal());
                  obj.setPaymentAmount(row.getValue(offset + 6).asBigDecimal());
                  obj.setDownPayment(row.getValue(offset + 7).asBigDecimal());
                  obj.setPaidAmount(row.getValue(offset + 8).asBigDecimal());
                  obj.setDiscountAmount(row.getValue(offset + 9).asBigDecimal());
                  obj.setDiscountAccountId(row.getValue(offset + 10).asString());
                  obj.setDepartmentId(row.getValue(offset + 11).asString());
                  obj.setProjectId(row.getValue(offset + 12).asString());
                  obj.setMemoId(row.getValue(offset + 13).asString());
                  obj.setTaxPayment(row.getValue(offset + 14).asBoolean());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseApPaymentDetailPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TAX_PAYMENT))
        {
            Object possibleBoolean = criteria.get(TAX_PAYMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TAX_PAYMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(ApPaymentDetailPeer.row2Object(row, 1,
                ApPaymentDetailPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseApPaymentDetailPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(AP_PAYMENT_DETAIL_ID, criteria.remove(AP_PAYMENT_DETAIL_ID));
                                                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TAX_PAYMENT))
        {
            Object possibleBoolean = criteria.get(TAX_PAYMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TAX_PAYMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         ApPaymentDetailPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TAX_PAYMENT))
        {
            Object possibleBoolean = criteria.get(TAX_PAYMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TAX_PAYMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(ApPaymentDetail obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(ApPaymentDetail obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(ApPaymentDetail obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ApPaymentDetail obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(ApPaymentDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(ApPaymentDetail obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(ApPaymentDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(ApPaymentDetail obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ApPaymentDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ApPaymentDetail obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseApPaymentDetailPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(AP_PAYMENT_DETAIL_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( ApPaymentDetail obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(AP_PAYMENT_DETAIL_ID, obj.getApPaymentDetailId());
              criteria.add(AP_PAYMENT_ID, obj.getApPaymentId());
              criteria.add(PURCHASE_INVOICE_ID, obj.getPurchaseInvoiceId());
              criteria.add(PURCHASE_INVOICE_NO, obj.getPurchaseInvoiceNo());
              criteria.add(PURCHASE_INVOICE_AMOUNT, obj.getPurchaseInvoiceAmount());
              criteria.add(INVOICE_RATE, obj.getInvoiceRate());
              criteria.add(PAYMENT_AMOUNT, obj.getPaymentAmount());
              criteria.add(DOWN_PAYMENT, obj.getDownPayment());
              criteria.add(PAID_AMOUNT, obj.getPaidAmount());
              criteria.add(DISCOUNT_AMOUNT, obj.getDiscountAmount());
              criteria.add(DISCOUNT_ACCOUNT_ID, obj.getDiscountAccountId());
              criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
              criteria.add(PROJECT_ID, obj.getProjectId());
              criteria.add(MEMO_ID, obj.getMemoId());
              criteria.add(TAX_PAYMENT, obj.getTaxPayment());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( ApPaymentDetail obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(AP_PAYMENT_DETAIL_ID, obj.getApPaymentDetailId());
                          criteria.add(AP_PAYMENT_ID, obj.getApPaymentId());
                          criteria.add(PURCHASE_INVOICE_ID, obj.getPurchaseInvoiceId());
                          criteria.add(PURCHASE_INVOICE_NO, obj.getPurchaseInvoiceNo());
                          criteria.add(PURCHASE_INVOICE_AMOUNT, obj.getPurchaseInvoiceAmount());
                          criteria.add(INVOICE_RATE, obj.getInvoiceRate());
                          criteria.add(PAYMENT_AMOUNT, obj.getPaymentAmount());
                          criteria.add(DOWN_PAYMENT, obj.getDownPayment());
                          criteria.add(PAID_AMOUNT, obj.getPaidAmount());
                          criteria.add(DISCOUNT_AMOUNT, obj.getDiscountAmount());
                          criteria.add(DISCOUNT_ACCOUNT_ID, obj.getDiscountAccountId());
                          criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
                          criteria.add(PROJECT_ID, obj.getProjectId());
                          criteria.add(MEMO_ID, obj.getMemoId());
                          criteria.add(TAX_PAYMENT, obj.getTaxPayment());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static ApPaymentDetail retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static ApPaymentDetail retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static ApPaymentDetail retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        ApPaymentDetail retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static ApPaymentDetail retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (ApPaymentDetail)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( AP_PAYMENT_DETAIL_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of ApPaymentDetail objects pre-filled with their
     * ApPayment objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApPaymentDetailPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinApPayment(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        ApPaymentDetailPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        ApPaymentPeer.addSelectColumns(criteria);


                        criteria.addJoin(ApPaymentDetailPeer.AP_PAYMENT_ID,
            ApPaymentPeer.AP_PAYMENT_ID);
        

                                                                                                                                                                                                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TAX_PAYMENT))
        {
            Object possibleBoolean = criteria.get(TAX_PAYMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TAX_PAYMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = ApPaymentDetailPeer.getOMClass();
                    ApPaymentDetail obj1 = ApPaymentDetailPeer
                .row2Object(row, 1, omClass);
                     omClass = ApPaymentPeer.getOMClass();
                    ApPayment obj2 = ApPaymentPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                ApPaymentDetail temp_obj1 = (ApPaymentDetail)results.get(j);
                ApPayment temp_obj2 = temp_obj1.getApPayment();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addApPaymentDetail(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initApPaymentDetails();
                obj2.addApPaymentDetail(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
