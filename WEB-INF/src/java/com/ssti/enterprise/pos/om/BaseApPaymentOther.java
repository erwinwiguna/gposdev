package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ApPaymentOther
 */
public abstract class BaseApPaymentOther extends BaseObject
{
    /** The Peer class */
    private static final ApPaymentOtherPeer peer =
        new ApPaymentOtherPeer();

        
    /** The value for the apPaymentOtherId field */
    private String apPaymentOtherId;
      
    /** The value for the apPaymentDetailId field */
    private String apPaymentDetailId;
      
    /** The value for the apPaymentId field */
    private String apPaymentId;
                                                
          
    /** The value for the otherAmount field */
    private BigDecimal otherAmount= bd_ZERO;
                                                
    /** The value for the oterAccountId field */
    private String oterAccountId = "";
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
                                                
    /** The value for the projectId field */
    private String projectId = "";
                                                
    /** The value for the memo field */
    private String memo = "";
  
    
    /**
     * Get the ApPaymentOtherId
     *
     * @return String
     */
    public String getApPaymentOtherId()
    {
        return apPaymentOtherId;
    }

                        
    /**
     * Set the value of ApPaymentOtherId
     *
     * @param v new value
     */
    public void setApPaymentOtherId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.apPaymentOtherId, v))
              {
            this.apPaymentOtherId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApPaymentDetailId
     *
     * @return String
     */
    public String getApPaymentDetailId()
    {
        return apPaymentDetailId;
    }

                              
    /**
     * Set the value of ApPaymentDetailId
     *
     * @param v new value
     */
    public void setApPaymentDetailId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.apPaymentDetailId, v))
              {
            this.apPaymentDetailId = v;
            setModified(true);
        }
    
                          
                if (aApPaymentDetail != null && !ObjectUtils.equals(aApPaymentDetail.getApPaymentDetailId(), v))
                {
            aApPaymentDetail = null;
        }
      
              }
  
    /**
     * Get the ApPaymentId
     *
     * @return String
     */
    public String getApPaymentId()
    {
        return apPaymentId;
    }

                        
    /**
     * Set the value of ApPaymentId
     *
     * @param v new value
     */
    public void setApPaymentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.apPaymentId, v))
              {
            this.apPaymentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OtherAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getOtherAmount()
    {
        return otherAmount;
    }

                        
    /**
     * Set the value of OtherAmount
     *
     * @param v new value
     */
    public void setOtherAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.otherAmount, v))
              {
            this.otherAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OterAccountId
     *
     * @return String
     */
    public String getOterAccountId()
    {
        return oterAccountId;
    }

                        
    /**
     * Set the value of OterAccountId
     *
     * @param v new value
     */
    public void setOterAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.oterAccountId, v))
              {
            this.oterAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Memo
     *
     * @return String
     */
    public String getMemo()
    {
        return memo;
    }

                        
    /**
     * Set the value of Memo
     *
     * @param v new value
     */
    public void setMemo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memo, v))
              {
            this.memo = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private ApPaymentDetail aApPaymentDetail;

    /**
     * Declares an association between this object and a ApPaymentDetail object
     *
     * @param v ApPaymentDetail
     * @throws TorqueException
     */
    public void setApPaymentDetail(ApPaymentDetail v) throws TorqueException
    {
            if (v == null)
        {
                  setApPaymentDetailId((String) null);
              }
        else
        {
            setApPaymentDetailId(v.getApPaymentDetailId());
        }
            aApPaymentDetail = v;
    }

                                            
    /**
     * Get the associated ApPaymentDetail object
     *
     * @return the associated ApPaymentDetail object
     * @throws TorqueException
     */
    public ApPaymentDetail getApPaymentDetail() throws TorqueException
    {
        if (aApPaymentDetail == null && (!ObjectUtils.equals(this.apPaymentDetailId, null)))
        {
                          aApPaymentDetail = ApPaymentDetailPeer.retrieveByPK(SimpleKey.keyFor(this.apPaymentDetailId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               ApPaymentDetail obj = ApPaymentDetailPeer.retrieveByPK(this.apPaymentDetailId);
               obj.addApPaymentOthers(this);
            */
        }
        return aApPaymentDetail;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setApPaymentDetailKey(ObjectKey key) throws TorqueException
    {
      
                        setApPaymentDetailId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ApPaymentOtherId");
              fieldNames.add("ApPaymentDetailId");
              fieldNames.add("ApPaymentId");
              fieldNames.add("OtherAmount");
              fieldNames.add("OterAccountId");
              fieldNames.add("DepartmentId");
              fieldNames.add("ProjectId");
              fieldNames.add("Memo");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ApPaymentOtherId"))
        {
                return getApPaymentOtherId();
            }
          if (name.equals("ApPaymentDetailId"))
        {
                return getApPaymentDetailId();
            }
          if (name.equals("ApPaymentId"))
        {
                return getApPaymentId();
            }
          if (name.equals("OtherAmount"))
        {
                return getOtherAmount();
            }
          if (name.equals("OterAccountId"))
        {
                return getOterAccountId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("Memo"))
        {
                return getMemo();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ApPaymentOtherPeer.AP_PAYMENT_OTHER_ID))
        {
                return getApPaymentOtherId();
            }
          if (name.equals(ApPaymentOtherPeer.AP_PAYMENT_DETAIL_ID))
        {
                return getApPaymentDetailId();
            }
          if (name.equals(ApPaymentOtherPeer.AP_PAYMENT_ID))
        {
                return getApPaymentId();
            }
          if (name.equals(ApPaymentOtherPeer.OTHER_AMOUNT))
        {
                return getOtherAmount();
            }
          if (name.equals(ApPaymentOtherPeer.OTER_ACCOUNT_ID))
        {
                return getOterAccountId();
            }
          if (name.equals(ApPaymentOtherPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(ApPaymentOtherPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(ApPaymentOtherPeer.MEMO))
        {
                return getMemo();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getApPaymentOtherId();
            }
              if (pos == 1)
        {
                return getApPaymentDetailId();
            }
              if (pos == 2)
        {
                return getApPaymentId();
            }
              if (pos == 3)
        {
                return getOtherAmount();
            }
              if (pos == 4)
        {
                return getOterAccountId();
            }
              if (pos == 5)
        {
                return getDepartmentId();
            }
              if (pos == 6)
        {
                return getProjectId();
            }
              if (pos == 7)
        {
                return getMemo();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ApPaymentOtherPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ApPaymentOtherPeer.doInsert((ApPaymentOther) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ApPaymentOtherPeer.doUpdate((ApPaymentOther) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key apPaymentOtherId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setApPaymentOtherId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setApPaymentOtherId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getApPaymentOtherId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ApPaymentOther copy() throws TorqueException
    {
        return copyInto(new ApPaymentOther());
    }
  
    protected ApPaymentOther copyInto(ApPaymentOther copyObj) throws TorqueException
    {
          copyObj.setApPaymentOtherId(apPaymentOtherId);
          copyObj.setApPaymentDetailId(apPaymentDetailId);
          copyObj.setApPaymentId(apPaymentId);
          copyObj.setOtherAmount(otherAmount);
          copyObj.setOterAccountId(oterAccountId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setProjectId(projectId);
          copyObj.setMemo(memo);
  
                    copyObj.setApPaymentOtherId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ApPaymentOtherPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ApPaymentOther\n");
        str.append("--------------\n")
           .append("ApPaymentOtherId     : ")
           .append(getApPaymentOtherId())
           .append("\n")
           .append("ApPaymentDetailId    : ")
           .append(getApPaymentDetailId())
           .append("\n")
           .append("ApPaymentId          : ")
           .append(getApPaymentId())
           .append("\n")
           .append("OtherAmount          : ")
           .append(getOtherAmount())
           .append("\n")
           .append("OterAccountId        : ")
           .append(getOterAccountId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("Memo                 : ")
           .append(getMemo())
           .append("\n")
        ;
        return(str.toString());
    }
}
