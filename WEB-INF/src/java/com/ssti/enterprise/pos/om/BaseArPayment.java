package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ArPayment
 */
public abstract class BaseArPayment extends BaseObject
{
    /** The Peer class */
    private static final ArPaymentPeer peer =
        new ArPaymentPeer();

        
    /** The value for the arPaymentId field */
    private String arPaymentId;
      
    /** The value for the arPaymentNo field */
    private String arPaymentNo;
      
    /** The value for the arPaymentDate field */
    private Date arPaymentDate;
      
    /** The value for the arPaymentDueDate field */
    private Date arPaymentDueDate;
      
    /** The value for the bankId field */
    private String bankId;
      
    /** The value for the bankIssuer field */
    private String bankIssuer;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the referenceNo field */
    private String referenceNo;
      
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount;
      
    /** The value for the totalDownPayment field */
    private BigDecimal totalDownPayment;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
      
    /** The value for the status field */
    private int status;
                                                
          
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount= bd_ZERO;
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                          
    /** The value for the cfStatus field */
    private int cfStatus = 1;
      
    /** The value for the cfDate field */
    private Date cfDate;
                                                
    /** The value for the cashFlowId field */
    private String cashFlowId = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the ArPaymentId
     *
     * @return String
     */
    public String getArPaymentId()
    {
        return arPaymentId;
    }

                                              
    /**
     * Set the value of ArPaymentId
     *
     * @param v new value
     */
    public void setArPaymentId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.arPaymentId, v))
              {
            this.arPaymentId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated ArPaymentDetail
        if (collArPaymentDetails != null)
        {
            for (int i = 0; i < collArPaymentDetails.size(); i++)
            {
                ((ArPaymentDetail) collArPaymentDetails.get(i))
                    .setArPaymentId(v);
            }
        }
                                }
  
    /**
     * Get the ArPaymentNo
     *
     * @return String
     */
    public String getArPaymentNo()
    {
        return arPaymentNo;
    }

                        
    /**
     * Set the value of ArPaymentNo
     *
     * @param v new value
     */
    public void setArPaymentNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.arPaymentNo, v))
              {
            this.arPaymentNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ArPaymentDate
     *
     * @return Date
     */
    public Date getArPaymentDate()
    {
        return arPaymentDate;
    }

                        
    /**
     * Set the value of ArPaymentDate
     *
     * @param v new value
     */
    public void setArPaymentDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.arPaymentDate, v))
              {
            this.arPaymentDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ArPaymentDueDate
     *
     * @return Date
     */
    public Date getArPaymentDueDate()
    {
        return arPaymentDueDate;
    }

                        
    /**
     * Set the value of ArPaymentDueDate
     *
     * @param v new value
     */
    public void setArPaymentDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.arPaymentDueDate, v))
              {
            this.arPaymentDueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDownPayment
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDownPayment()
    {
        return totalDownPayment;
    }

                        
    /**
     * Set the value of TotalDownPayment
     *
     * @param v new value
     */
    public void setTotalDownPayment(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDownPayment, v))
              {
            this.totalDownPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CfStatus
     *
     * @return int
     */
    public int getCfStatus()
    {
        return cfStatus;
    }

                        
    /**
     * Set the value of CfStatus
     *
     * @param v new value
     */
    public void setCfStatus(int v) 
    {
    
                  if (this.cfStatus != v)
              {
            this.cfStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CfDate
     *
     * @return Date
     */
    public Date getCfDate()
    {
        return cfDate;
    }

                        
    /**
     * Set the value of CfDate
     *
     * @param v new value
     */
    public void setCfDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cfDate, v))
              {
            this.cfDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowId
     *
     * @return String
     */
    public String getCashFlowId()
    {
        return cashFlowId;
    }

                        
    /**
     * Set the value of CashFlowId
     *
     * @param v new value
     */
    public void setCashFlowId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowId, v))
              {
            this.cashFlowId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collArPaymentDetails
     */
    protected List collArPaymentDetails;

    /**
     * Temporary storage of collArPaymentDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initArPaymentDetails()
    {
        if (collArPaymentDetails == null)
        {
            collArPaymentDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a ArPaymentDetail object to this object
     * through the ArPaymentDetail foreign key attribute
     *
     * @param l ArPaymentDetail
     * @throws TorqueException
     */
    public void addArPaymentDetail(ArPaymentDetail l) throws TorqueException
    {
        getArPaymentDetails().add(l);
        l.setArPayment((ArPayment) this);
    }

    /**
     * The criteria used to select the current contents of collArPaymentDetails
     */
    private Criteria lastArPaymentDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getArPaymentDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getArPaymentDetails() throws TorqueException
    {
              if (collArPaymentDetails == null)
        {
            collArPaymentDetails = getArPaymentDetails(new Criteria(10));
        }
        return collArPaymentDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ArPayment has previously
     * been saved, it will retrieve related ArPaymentDetails from storage.
     * If this ArPayment is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getArPaymentDetails(Criteria criteria) throws TorqueException
    {
              if (collArPaymentDetails == null)
        {
            if (isNew())
            {
               collArPaymentDetails = new ArrayList();
            }
            else
            {
                        criteria.add(ArPaymentDetailPeer.AR_PAYMENT_ID, getArPaymentId() );
                        collArPaymentDetails = ArPaymentDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ArPaymentDetailPeer.AR_PAYMENT_ID, getArPaymentId());
                            if (!lastArPaymentDetailsCriteria.equals(criteria))
                {
                    collArPaymentDetails = ArPaymentDetailPeer.doSelect(criteria);
                }
            }
        }
        lastArPaymentDetailsCriteria = criteria;

        return collArPaymentDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getArPaymentDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getArPaymentDetails(Connection con) throws TorqueException
    {
              if (collArPaymentDetails == null)
        {
            collArPaymentDetails = getArPaymentDetails(new Criteria(10), con);
        }
        return collArPaymentDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ArPayment has previously
     * been saved, it will retrieve related ArPaymentDetails from storage.
     * If this ArPayment is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getArPaymentDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collArPaymentDetails == null)
        {
            if (isNew())
            {
               collArPaymentDetails = new ArrayList();
            }
            else
            {
                         criteria.add(ArPaymentDetailPeer.AR_PAYMENT_ID, getArPaymentId());
                         collArPaymentDetails = ArPaymentDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ArPaymentDetailPeer.AR_PAYMENT_ID, getArPaymentId());
                             if (!lastArPaymentDetailsCriteria.equals(criteria))
                 {
                     collArPaymentDetails = ArPaymentDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastArPaymentDetailsCriteria = criteria;

         return collArPaymentDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ArPayment is new, it will return
     * an empty collection; or if this ArPayment has previously
     * been saved, it will retrieve related ArPaymentDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ArPayment.
     */
    protected List getArPaymentDetailsJoinArPayment(Criteria criteria)
        throws TorqueException
    {
                    if (collArPaymentDetails == null)
        {
            if (isNew())
            {
               collArPaymentDetails = new ArrayList();
            }
            else
            {
                              criteria.add(ArPaymentDetailPeer.AR_PAYMENT_ID, getArPaymentId());
                              collArPaymentDetails = ArPaymentDetailPeer.doSelectJoinArPayment(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ArPaymentDetailPeer.AR_PAYMENT_ID, getArPaymentId());
                                    if (!lastArPaymentDetailsCriteria.equals(criteria))
            {
                collArPaymentDetails = ArPaymentDetailPeer.doSelectJoinArPayment(criteria);
            }
        }
        lastArPaymentDetailsCriteria = criteria;

        return collArPaymentDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ArPaymentId");
              fieldNames.add("ArPaymentNo");
              fieldNames.add("ArPaymentDate");
              fieldNames.add("ArPaymentDueDate");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("ReferenceNo");
              fieldNames.add("PaymentAmount");
              fieldNames.add("TotalDownPayment");
              fieldNames.add("TotalAmount");
              fieldNames.add("UserName");
              fieldNames.add("Remark");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("FiscalRate");
              fieldNames.add("Status");
              fieldNames.add("TotalDiscount");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("LocationId");
              fieldNames.add("CfStatus");
              fieldNames.add("CfDate");
              fieldNames.add("CashFlowId");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ArPaymentId"))
        {
                return getArPaymentId();
            }
          if (name.equals("ArPaymentNo"))
        {
                return getArPaymentNo();
            }
          if (name.equals("ArPaymentDate"))
        {
                return getArPaymentDate();
            }
          if (name.equals("ArPaymentDueDate"))
        {
                return getArPaymentDueDate();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("TotalDownPayment"))
        {
                return getTotalDownPayment();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("CfStatus"))
        {
                return Integer.valueOf(getCfStatus());
            }
          if (name.equals("CfDate"))
        {
                return getCfDate();
            }
          if (name.equals("CashFlowId"))
        {
                return getCashFlowId();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ArPaymentPeer.AR_PAYMENT_ID))
        {
                return getArPaymentId();
            }
          if (name.equals(ArPaymentPeer.AR_PAYMENT_NO))
        {
                return getArPaymentNo();
            }
          if (name.equals(ArPaymentPeer.AR_PAYMENT_DATE))
        {
                return getArPaymentDate();
            }
          if (name.equals(ArPaymentPeer.AR_PAYMENT_DUE_DATE))
        {
                return getArPaymentDueDate();
            }
          if (name.equals(ArPaymentPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(ArPaymentPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(ArPaymentPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(ArPaymentPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(ArPaymentPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(ArPaymentPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(ArPaymentPeer.TOTAL_DOWN_PAYMENT))
        {
                return getTotalDownPayment();
            }
          if (name.equals(ArPaymentPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(ArPaymentPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(ArPaymentPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(ArPaymentPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(ArPaymentPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(ArPaymentPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(ArPaymentPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(ArPaymentPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(ArPaymentPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(ArPaymentPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(ArPaymentPeer.CF_STATUS))
        {
                return Integer.valueOf(getCfStatus());
            }
          if (name.equals(ArPaymentPeer.CF_DATE))
        {
                return getCfDate();
            }
          if (name.equals(ArPaymentPeer.CASH_FLOW_ID))
        {
                return getCashFlowId();
            }
          if (name.equals(ArPaymentPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(ArPaymentPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getArPaymentId();
            }
              if (pos == 1)
        {
                return getArPaymentNo();
            }
              if (pos == 2)
        {
                return getArPaymentDate();
            }
              if (pos == 3)
        {
                return getArPaymentDueDate();
            }
              if (pos == 4)
        {
                return getBankId();
            }
              if (pos == 5)
        {
                return getBankIssuer();
            }
              if (pos == 6)
        {
                return getCustomerId();
            }
              if (pos == 7)
        {
                return getCustomerName();
            }
              if (pos == 8)
        {
                return getReferenceNo();
            }
              if (pos == 9)
        {
                return getPaymentAmount();
            }
              if (pos == 10)
        {
                return getTotalDownPayment();
            }
              if (pos == 11)
        {
                return getTotalAmount();
            }
              if (pos == 12)
        {
                return getUserName();
            }
              if (pos == 13)
        {
                return getRemark();
            }
              if (pos == 14)
        {
                return getCurrencyId();
            }
              if (pos == 15)
        {
                return getCurrencyRate();
            }
              if (pos == 16)
        {
                return getFiscalRate();
            }
              if (pos == 17)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 18)
        {
                return getTotalDiscount();
            }
              if (pos == 19)
        {
                return getCashFlowTypeId();
            }
              if (pos == 20)
        {
                return getLocationId();
            }
              if (pos == 21)
        {
                return Integer.valueOf(getCfStatus());
            }
              if (pos == 22)
        {
                return getCfDate();
            }
              if (pos == 23)
        {
                return getCashFlowId();
            }
              if (pos == 24)
        {
                return getCancelBy();
            }
              if (pos == 25)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ArPaymentPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ArPaymentPeer.doInsert((ArPayment) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ArPaymentPeer.doUpdate((ArPayment) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collArPaymentDetails != null)
            {
                for (int i = 0; i < collArPaymentDetails.size(); i++)
                {
                    ((ArPaymentDetail) collArPaymentDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key arPaymentId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setArPaymentId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setArPaymentId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getArPaymentId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ArPayment copy() throws TorqueException
    {
        return copyInto(new ArPayment());
    }
  
    protected ArPayment copyInto(ArPayment copyObj) throws TorqueException
    {
          copyObj.setArPaymentId(arPaymentId);
          copyObj.setArPaymentNo(arPaymentNo);
          copyObj.setArPaymentDate(arPaymentDate);
          copyObj.setArPaymentDueDate(arPaymentDueDate);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setTotalDownPayment(totalDownPayment);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setUserName(userName);
          copyObj.setRemark(remark);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setStatus(status);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setLocationId(locationId);
          copyObj.setCfStatus(cfStatus);
          copyObj.setCfDate(cfDate);
          copyObj.setCashFlowId(cashFlowId);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setArPaymentId((String)null);
                                                                                                                                                                  
                                      
                            
        List v = getArPaymentDetails();
        for (int i = 0; i < v.size(); i++)
        {
            ArPaymentDetail obj = (ArPaymentDetail) v.get(i);
            copyObj.addArPaymentDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ArPaymentPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ArPayment\n");
        str.append("---------\n")
           .append("ArPaymentId          : ")
           .append(getArPaymentId())
           .append("\n")
           .append("ArPaymentNo          : ")
           .append(getArPaymentNo())
           .append("\n")
           .append("ArPaymentDate        : ")
           .append(getArPaymentDate())
           .append("\n")
           .append("ArPaymentDueDate     : ")
           .append(getArPaymentDueDate())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("TotalDownPayment     : ")
           .append(getTotalDownPayment())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("CfStatus             : ")
           .append(getCfStatus())
           .append("\n")
           .append("CfDate               : ")
           .append(getCfDate())
           .append("\n")
           .append("CashFlowId           : ")
           .append(getCashFlowId())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
