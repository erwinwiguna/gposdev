package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to BankBook
 */
public abstract class BaseBankBook extends BaseObject
{
    /** The Peer class */
    private static final BankBookPeer peer =
        new BankBookPeer();

        
    /** The value for the bankBookId field */
    private String bankBookId;
      
    /** The value for the bankId field */
    private String bankId;
      
    /** The value for the bankName field */
    private String bankName;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the openingBalance field */
    private BigDecimal openingBalance;
      
    /** The value for the accountBalance field */
    private BigDecimal accountBalance;
      
    /** The value for the baseBalance field */
    private BigDecimal baseBalance;
  
    
    /**
     * Get the BankBookId
     *
     * @return String
     */
    public String getBankBookId()
    {
        return bankBookId;
    }

                        
    /**
     * Set the value of BankBookId
     *
     * @param v new value
     */
    public void setBankBookId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankBookId, v))
              {
            this.bankBookId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankName
     *
     * @return String
     */
    public String getBankName()
    {
        return bankName;
    }

                        
    /**
     * Set the value of BankName
     *
     * @param v new value
     */
    public void setBankName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankName, v))
              {
            this.bankName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getAccountBalance()
    {
        return accountBalance;
    }

                        
    /**
     * Set the value of AccountBalance
     *
     * @param v new value
     */
    public void setAccountBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.accountBalance, v))
              {
            this.accountBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BaseBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getBaseBalance()
    {
        return baseBalance;
    }

                        
    /**
     * Set the value of BaseBalance
     *
     * @param v new value
     */
    public void setBaseBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.baseBalance, v))
              {
            this.baseBalance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BankBookId");
              fieldNames.add("BankId");
              fieldNames.add("BankName");
              fieldNames.add("CurrencyId");
              fieldNames.add("OpeningBalance");
              fieldNames.add("AccountBalance");
              fieldNames.add("BaseBalance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BankBookId"))
        {
                return getBankBookId();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankName"))
        {
                return getBankName();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("AccountBalance"))
        {
                return getAccountBalance();
            }
          if (name.equals("BaseBalance"))
        {
                return getBaseBalance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BankBookPeer.BANK_BOOK_ID))
        {
                return getBankBookId();
            }
          if (name.equals(BankBookPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(BankBookPeer.BANK_NAME))
        {
                return getBankName();
            }
          if (name.equals(BankBookPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(BankBookPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(BankBookPeer.ACCOUNT_BALANCE))
        {
                return getAccountBalance();
            }
          if (name.equals(BankBookPeer.BASE_BALANCE))
        {
                return getBaseBalance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBankBookId();
            }
              if (pos == 1)
        {
                return getBankId();
            }
              if (pos == 2)
        {
                return getBankName();
            }
              if (pos == 3)
        {
                return getCurrencyId();
            }
              if (pos == 4)
        {
                return getOpeningBalance();
            }
              if (pos == 5)
        {
                return getAccountBalance();
            }
              if (pos == 6)
        {
                return getBaseBalance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BankBookPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BankBookPeer.doInsert((BankBook) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BankBookPeer.doUpdate((BankBook) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key bankBookId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBankBookId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBankBookId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBankBookId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public BankBook copy() throws TorqueException
    {
        return copyInto(new BankBook());
    }
  
    protected BankBook copyInto(BankBook copyObj) throws TorqueException
    {
          copyObj.setBankBookId(bankBookId);
          copyObj.setBankId(bankId);
          copyObj.setBankName(bankName);
          copyObj.setCurrencyId(currencyId);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setAccountBalance(accountBalance);
          copyObj.setBaseBalance(baseBalance);
  
                    copyObj.setBankBookId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BankBookPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("BankBook\n");
        str.append("--------\n")
           .append("BankBookId           : ")
           .append(getBankBookId())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankName             : ")
           .append(getBankName())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("AccountBalance       : ")
           .append(getAccountBalance())
           .append("\n")
           .append("BaseBalance          : ")
           .append(getBaseBalance())
           .append("\n")
        ;
        return(str.toString());
    }
}
