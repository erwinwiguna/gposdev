package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to BankReconcile
 */
public abstract class BaseBankReconcile extends BaseObject
{
    /** The Peer class */
    private static final BankReconcilePeer peer =
        new BankReconcilePeer();

        
    /** The value for the bankReconcileId field */
    private String bankReconcileId;
      
    /** The value for the bankId field */
    private String bankId;
      
    /** The value for the bankAmount field */
    private BigDecimal bankAmount;
      
    /** The value for the calculatedAmount field */
    private BigDecimal calculatedAmount;
      
    /** The value for the reconcileDate field */
    private Date reconcileDate;
      
    /** The value for the userName field */
    private String userName;
                                                
    /** The value for the description field */
    private String description = "";
  
    
    /**
     * Get the BankReconcileId
     *
     * @return String
     */
    public String getBankReconcileId()
    {
        return bankReconcileId;
    }

                        
    /**
     * Set the value of BankReconcileId
     *
     * @param v new value
     */
    public void setBankReconcileId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankReconcileId, v))
              {
            this.bankReconcileId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getBankAmount()
    {
        return bankAmount;
    }

                        
    /**
     * Set the value of BankAmount
     *
     * @param v new value
     */
    public void setBankAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.bankAmount, v))
              {
            this.bankAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CalculatedAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getCalculatedAmount()
    {
        return calculatedAmount;
    }

                        
    /**
     * Set the value of CalculatedAmount
     *
     * @param v new value
     */
    public void setCalculatedAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.calculatedAmount, v))
              {
            this.calculatedAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReconcileDate
     *
     * @return Date
     */
    public Date getReconcileDate()
    {
        return reconcileDate;
    }

                        
    /**
     * Set the value of ReconcileDate
     *
     * @param v new value
     */
    public void setReconcileDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.reconcileDate, v))
              {
            this.reconcileDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BankReconcileId");
              fieldNames.add("BankId");
              fieldNames.add("BankAmount");
              fieldNames.add("CalculatedAmount");
              fieldNames.add("ReconcileDate");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BankReconcileId"))
        {
                return getBankReconcileId();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankAmount"))
        {
                return getBankAmount();
            }
          if (name.equals("CalculatedAmount"))
        {
                return getCalculatedAmount();
            }
          if (name.equals("ReconcileDate"))
        {
                return getReconcileDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BankReconcilePeer.BANK_RECONCILE_ID))
        {
                return getBankReconcileId();
            }
          if (name.equals(BankReconcilePeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(BankReconcilePeer.BANK_AMOUNT))
        {
                return getBankAmount();
            }
          if (name.equals(BankReconcilePeer.CALCULATED_AMOUNT))
        {
                return getCalculatedAmount();
            }
          if (name.equals(BankReconcilePeer.RECONCILE_DATE))
        {
                return getReconcileDate();
            }
          if (name.equals(BankReconcilePeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(BankReconcilePeer.DESCRIPTION))
        {
                return getDescription();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBankReconcileId();
            }
              if (pos == 1)
        {
                return getBankId();
            }
              if (pos == 2)
        {
                return getBankAmount();
            }
              if (pos == 3)
        {
                return getCalculatedAmount();
            }
              if (pos == 4)
        {
                return getReconcileDate();
            }
              if (pos == 5)
        {
                return getUserName();
            }
              if (pos == 6)
        {
                return getDescription();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BankReconcilePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BankReconcilePeer.doInsert((BankReconcile) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BankReconcilePeer.doUpdate((BankReconcile) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key bankReconcileId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBankReconcileId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBankReconcileId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBankReconcileId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public BankReconcile copy() throws TorqueException
    {
        return copyInto(new BankReconcile());
    }
  
    protected BankReconcile copyInto(BankReconcile copyObj) throws TorqueException
    {
          copyObj.setBankReconcileId(bankReconcileId);
          copyObj.setBankId(bankId);
          copyObj.setBankAmount(bankAmount);
          copyObj.setCalculatedAmount(calculatedAmount);
          copyObj.setReconcileDate(reconcileDate);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
  
                    copyObj.setBankReconcileId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BankReconcilePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("BankReconcile\n");
        str.append("-------------\n")
           .append("BankReconcileId      : ")
           .append(getBankReconcileId())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankAmount           : ")
           .append(getBankAmount())
           .append("\n")
           .append("CalculatedAmount     : ")
           .append(getCalculatedAmount())
           .append("\n")
           .append("ReconcileDate        : ")
           .append(getReconcileDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
        ;
        return(str.toString());
    }
}
