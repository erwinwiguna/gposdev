package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to BarcodePaper
 */
public abstract class BaseBarcodePaper extends BaseObject
{
    /** The Peer class */
    private static final BarcodePaperPeer peer =
        new BarcodePaperPeer();

        
    /** The value for the barcodePaperId field */
    private String barcodePaperId;
      
    /** The value for the settingName field */
    private String settingName;
                                          
    /** The value for the paperType field */
    private int paperType = 1;
      
    /** The value for the paperColumn field */
    private int paperColumn;
      
    /** The value for the paperRow field */
    private int paperRow;
      
    /** The value for the columnWidth field */
    private String columnWidth;
      
    /** The value for the columnHeight field */
    private int columnHeight;
      
    /** The value for the columnSpacing field */
    private int columnSpacing;
      
    /** The value for the paperWidth field */
    private int paperWidth;
      
    /** The value for the paperHeight field */
    private int paperHeight;
      
    /** The value for the barcodeType field */
    private String barcodeType;
                                          
    /** The value for the barcodeWidth field */
    private int barcodeWidth = 1;
                                          
    /** The value for the barcodeHeight field */
    private int barcodeHeight = 50;
                                          
    /** The value for the resolution field */
    private int resolution = 50;
                                          
    /** The value for the leftMargin field */
    private int leftMargin = 10;
                                          
    /** The value for the topMargin field */
    private int topMargin = 20;
                                          
    /** The value for the bottomMargin field */
    private int bottomMargin = 20;
                                          
    /** The value for the border field */
    private int border = 1;
      
    /** The value for the isDefault field */
    private boolean isDefault;
  
    
    /**
     * Get the BarcodePaperId
     *
     * @return String
     */
    public String getBarcodePaperId()
    {
        return barcodePaperId;
    }

                        
    /**
     * Set the value of BarcodePaperId
     *
     * @param v new value
     */
    public void setBarcodePaperId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.barcodePaperId, v))
              {
            this.barcodePaperId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SettingName
     *
     * @return String
     */
    public String getSettingName()
    {
        return settingName;
    }

                        
    /**
     * Set the value of SettingName
     *
     * @param v new value
     */
    public void setSettingName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.settingName, v))
              {
            this.settingName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaperType
     *
     * @return int
     */
    public int getPaperType()
    {
        return paperType;
    }

                        
    /**
     * Set the value of PaperType
     *
     * @param v new value
     */
    public void setPaperType(int v) 
    {
    
                  if (this.paperType != v)
              {
            this.paperType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaperColumn
     *
     * @return int
     */
    public int getPaperColumn()
    {
        return paperColumn;
    }

                        
    /**
     * Set the value of PaperColumn
     *
     * @param v new value
     */
    public void setPaperColumn(int v) 
    {
    
                  if (this.paperColumn != v)
              {
            this.paperColumn = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaperRow
     *
     * @return int
     */
    public int getPaperRow()
    {
        return paperRow;
    }

                        
    /**
     * Set the value of PaperRow
     *
     * @param v new value
     */
    public void setPaperRow(int v) 
    {
    
                  if (this.paperRow != v)
              {
            this.paperRow = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ColumnWidth
     *
     * @return String
     */
    public String getColumnWidth()
    {
        return columnWidth;
    }

                        
    /**
     * Set the value of ColumnWidth
     *
     * @param v new value
     */
    public void setColumnWidth(String v) 
    {
    
                  if (!ObjectUtils.equals(this.columnWidth, v))
              {
            this.columnWidth = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ColumnHeight
     *
     * @return int
     */
    public int getColumnHeight()
    {
        return columnHeight;
    }

                        
    /**
     * Set the value of ColumnHeight
     *
     * @param v new value
     */
    public void setColumnHeight(int v) 
    {
    
                  if (this.columnHeight != v)
              {
            this.columnHeight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ColumnSpacing
     *
     * @return int
     */
    public int getColumnSpacing()
    {
        return columnSpacing;
    }

                        
    /**
     * Set the value of ColumnSpacing
     *
     * @param v new value
     */
    public void setColumnSpacing(int v) 
    {
    
                  if (this.columnSpacing != v)
              {
            this.columnSpacing = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaperWidth
     *
     * @return int
     */
    public int getPaperWidth()
    {
        return paperWidth;
    }

                        
    /**
     * Set the value of PaperWidth
     *
     * @param v new value
     */
    public void setPaperWidth(int v) 
    {
    
                  if (this.paperWidth != v)
              {
            this.paperWidth = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaperHeight
     *
     * @return int
     */
    public int getPaperHeight()
    {
        return paperHeight;
    }

                        
    /**
     * Set the value of PaperHeight
     *
     * @param v new value
     */
    public void setPaperHeight(int v) 
    {
    
                  if (this.paperHeight != v)
              {
            this.paperHeight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BarcodeType
     *
     * @return String
     */
    public String getBarcodeType()
    {
        return barcodeType;
    }

                        
    /**
     * Set the value of BarcodeType
     *
     * @param v new value
     */
    public void setBarcodeType(String v) 
    {
    
                  if (!ObjectUtils.equals(this.barcodeType, v))
              {
            this.barcodeType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BarcodeWidth
     *
     * @return int
     */
    public int getBarcodeWidth()
    {
        return barcodeWidth;
    }

                        
    /**
     * Set the value of BarcodeWidth
     *
     * @param v new value
     */
    public void setBarcodeWidth(int v) 
    {
    
                  if (this.barcodeWidth != v)
              {
            this.barcodeWidth = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BarcodeHeight
     *
     * @return int
     */
    public int getBarcodeHeight()
    {
        return barcodeHeight;
    }

                        
    /**
     * Set the value of BarcodeHeight
     *
     * @param v new value
     */
    public void setBarcodeHeight(int v) 
    {
    
                  if (this.barcodeHeight != v)
              {
            this.barcodeHeight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Resolution
     *
     * @return int
     */
    public int getResolution()
    {
        return resolution;
    }

                        
    /**
     * Set the value of Resolution
     *
     * @param v new value
     */
    public void setResolution(int v) 
    {
    
                  if (this.resolution != v)
              {
            this.resolution = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LeftMargin
     *
     * @return int
     */
    public int getLeftMargin()
    {
        return leftMargin;
    }

                        
    /**
     * Set the value of LeftMargin
     *
     * @param v new value
     */
    public void setLeftMargin(int v) 
    {
    
                  if (this.leftMargin != v)
              {
            this.leftMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TopMargin
     *
     * @return int
     */
    public int getTopMargin()
    {
        return topMargin;
    }

                        
    /**
     * Set the value of TopMargin
     *
     * @param v new value
     */
    public void setTopMargin(int v) 
    {
    
                  if (this.topMargin != v)
              {
            this.topMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BottomMargin
     *
     * @return int
     */
    public int getBottomMargin()
    {
        return bottomMargin;
    }

                        
    /**
     * Set the value of BottomMargin
     *
     * @param v new value
     */
    public void setBottomMargin(int v) 
    {
    
                  if (this.bottomMargin != v)
              {
            this.bottomMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Border
     *
     * @return int
     */
    public int getBorder()
    {
        return border;
    }

                        
    /**
     * Set the value of Border
     *
     * @param v new value
     */
    public void setBorder(int v) 
    {
    
                  if (this.border != v)
              {
            this.border = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDefault
     *
     * @return boolean
     */
    public boolean getIsDefault()
    {
        return isDefault;
    }

                        
    /**
     * Set the value of IsDefault
     *
     * @param v new value
     */
    public void setIsDefault(boolean v) 
    {
    
                  if (this.isDefault != v)
              {
            this.isDefault = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BarcodePaperId");
              fieldNames.add("SettingName");
              fieldNames.add("PaperType");
              fieldNames.add("PaperColumn");
              fieldNames.add("PaperRow");
              fieldNames.add("ColumnWidth");
              fieldNames.add("ColumnHeight");
              fieldNames.add("ColumnSpacing");
              fieldNames.add("PaperWidth");
              fieldNames.add("PaperHeight");
              fieldNames.add("BarcodeType");
              fieldNames.add("BarcodeWidth");
              fieldNames.add("BarcodeHeight");
              fieldNames.add("Resolution");
              fieldNames.add("LeftMargin");
              fieldNames.add("TopMargin");
              fieldNames.add("BottomMargin");
              fieldNames.add("Border");
              fieldNames.add("IsDefault");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BarcodePaperId"))
        {
                return getBarcodePaperId();
            }
          if (name.equals("SettingName"))
        {
                return getSettingName();
            }
          if (name.equals("PaperType"))
        {
                return Integer.valueOf(getPaperType());
            }
          if (name.equals("PaperColumn"))
        {
                return Integer.valueOf(getPaperColumn());
            }
          if (name.equals("PaperRow"))
        {
                return Integer.valueOf(getPaperRow());
            }
          if (name.equals("ColumnWidth"))
        {
                return getColumnWidth();
            }
          if (name.equals("ColumnHeight"))
        {
                return Integer.valueOf(getColumnHeight());
            }
          if (name.equals("ColumnSpacing"))
        {
                return Integer.valueOf(getColumnSpacing());
            }
          if (name.equals("PaperWidth"))
        {
                return Integer.valueOf(getPaperWidth());
            }
          if (name.equals("PaperHeight"))
        {
                return Integer.valueOf(getPaperHeight());
            }
          if (name.equals("BarcodeType"))
        {
                return getBarcodeType();
            }
          if (name.equals("BarcodeWidth"))
        {
                return Integer.valueOf(getBarcodeWidth());
            }
          if (name.equals("BarcodeHeight"))
        {
                return Integer.valueOf(getBarcodeHeight());
            }
          if (name.equals("Resolution"))
        {
                return Integer.valueOf(getResolution());
            }
          if (name.equals("LeftMargin"))
        {
                return Integer.valueOf(getLeftMargin());
            }
          if (name.equals("TopMargin"))
        {
                return Integer.valueOf(getTopMargin());
            }
          if (name.equals("BottomMargin"))
        {
                return Integer.valueOf(getBottomMargin());
            }
          if (name.equals("Border"))
        {
                return Integer.valueOf(getBorder());
            }
          if (name.equals("IsDefault"))
        {
                return Boolean.valueOf(getIsDefault());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BarcodePaperPeer.BARCODE_PAPER_ID))
        {
                return getBarcodePaperId();
            }
          if (name.equals(BarcodePaperPeer.SETTING_NAME))
        {
                return getSettingName();
            }
          if (name.equals(BarcodePaperPeer.PAPER_TYPE))
        {
                return Integer.valueOf(getPaperType());
            }
          if (name.equals(BarcodePaperPeer.PAPER_COLUMN))
        {
                return Integer.valueOf(getPaperColumn());
            }
          if (name.equals(BarcodePaperPeer.PAPER_ROW))
        {
                return Integer.valueOf(getPaperRow());
            }
          if (name.equals(BarcodePaperPeer.COLUMN_WIDTH))
        {
                return getColumnWidth();
            }
          if (name.equals(BarcodePaperPeer.COLUMN_HEIGHT))
        {
                return Integer.valueOf(getColumnHeight());
            }
          if (name.equals(BarcodePaperPeer.COLUMN_SPACING))
        {
                return Integer.valueOf(getColumnSpacing());
            }
          if (name.equals(BarcodePaperPeer.PAPER_WIDTH))
        {
                return Integer.valueOf(getPaperWidth());
            }
          if (name.equals(BarcodePaperPeer.PAPER_HEIGHT))
        {
                return Integer.valueOf(getPaperHeight());
            }
          if (name.equals(BarcodePaperPeer.BARCODE_TYPE))
        {
                return getBarcodeType();
            }
          if (name.equals(BarcodePaperPeer.BARCODE_WIDTH))
        {
                return Integer.valueOf(getBarcodeWidth());
            }
          if (name.equals(BarcodePaperPeer.BARCODE_HEIGHT))
        {
                return Integer.valueOf(getBarcodeHeight());
            }
          if (name.equals(BarcodePaperPeer.RESOLUTION))
        {
                return Integer.valueOf(getResolution());
            }
          if (name.equals(BarcodePaperPeer.LEFT_MARGIN))
        {
                return Integer.valueOf(getLeftMargin());
            }
          if (name.equals(BarcodePaperPeer.TOP_MARGIN))
        {
                return Integer.valueOf(getTopMargin());
            }
          if (name.equals(BarcodePaperPeer.BOTTOM_MARGIN))
        {
                return Integer.valueOf(getBottomMargin());
            }
          if (name.equals(BarcodePaperPeer.BORDER))
        {
                return Integer.valueOf(getBorder());
            }
          if (name.equals(BarcodePaperPeer.IS_DEFAULT))
        {
                return Boolean.valueOf(getIsDefault());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBarcodePaperId();
            }
              if (pos == 1)
        {
                return getSettingName();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getPaperType());
            }
              if (pos == 3)
        {
                return Integer.valueOf(getPaperColumn());
            }
              if (pos == 4)
        {
                return Integer.valueOf(getPaperRow());
            }
              if (pos == 5)
        {
                return getColumnWidth();
            }
              if (pos == 6)
        {
                return Integer.valueOf(getColumnHeight());
            }
              if (pos == 7)
        {
                return Integer.valueOf(getColumnSpacing());
            }
              if (pos == 8)
        {
                return Integer.valueOf(getPaperWidth());
            }
              if (pos == 9)
        {
                return Integer.valueOf(getPaperHeight());
            }
              if (pos == 10)
        {
                return getBarcodeType();
            }
              if (pos == 11)
        {
                return Integer.valueOf(getBarcodeWidth());
            }
              if (pos == 12)
        {
                return Integer.valueOf(getBarcodeHeight());
            }
              if (pos == 13)
        {
                return Integer.valueOf(getResolution());
            }
              if (pos == 14)
        {
                return Integer.valueOf(getLeftMargin());
            }
              if (pos == 15)
        {
                return Integer.valueOf(getTopMargin());
            }
              if (pos == 16)
        {
                return Integer.valueOf(getBottomMargin());
            }
              if (pos == 17)
        {
                return Integer.valueOf(getBorder());
            }
              if (pos == 18)
        {
                return Boolean.valueOf(getIsDefault());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BarcodePaperPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BarcodePaperPeer.doInsert((BarcodePaper) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BarcodePaperPeer.doUpdate((BarcodePaper) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key barcodePaperId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBarcodePaperId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBarcodePaperId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBarcodePaperId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public BarcodePaper copy() throws TorqueException
    {
        return copyInto(new BarcodePaper());
    }
  
    protected BarcodePaper copyInto(BarcodePaper copyObj) throws TorqueException
    {
          copyObj.setBarcodePaperId(barcodePaperId);
          copyObj.setSettingName(settingName);
          copyObj.setPaperType(paperType);
          copyObj.setPaperColumn(paperColumn);
          copyObj.setPaperRow(paperRow);
          copyObj.setColumnWidth(columnWidth);
          copyObj.setColumnHeight(columnHeight);
          copyObj.setColumnSpacing(columnSpacing);
          copyObj.setPaperWidth(paperWidth);
          copyObj.setPaperHeight(paperHeight);
          copyObj.setBarcodeType(barcodeType);
          copyObj.setBarcodeWidth(barcodeWidth);
          copyObj.setBarcodeHeight(barcodeHeight);
          copyObj.setResolution(resolution);
          copyObj.setLeftMargin(leftMargin);
          copyObj.setTopMargin(topMargin);
          copyObj.setBottomMargin(bottomMargin);
          copyObj.setBorder(border);
          copyObj.setIsDefault(isDefault);
  
                    copyObj.setBarcodePaperId((String)null);
                                                                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BarcodePaperPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("BarcodePaper\n");
        str.append("------------\n")
           .append("BarcodePaperId       : ")
           .append(getBarcodePaperId())
           .append("\n")
           .append("SettingName          : ")
           .append(getSettingName())
           .append("\n")
           .append("PaperType            : ")
           .append(getPaperType())
           .append("\n")
           .append("PaperColumn          : ")
           .append(getPaperColumn())
           .append("\n")
           .append("PaperRow             : ")
           .append(getPaperRow())
           .append("\n")
           .append("ColumnWidth          : ")
           .append(getColumnWidth())
           .append("\n")
           .append("ColumnHeight         : ")
           .append(getColumnHeight())
           .append("\n")
           .append("ColumnSpacing        : ")
           .append(getColumnSpacing())
           .append("\n")
           .append("PaperWidth           : ")
           .append(getPaperWidth())
           .append("\n")
           .append("PaperHeight          : ")
           .append(getPaperHeight())
           .append("\n")
           .append("BarcodeType          : ")
           .append(getBarcodeType())
           .append("\n")
           .append("BarcodeWidth         : ")
           .append(getBarcodeWidth())
           .append("\n")
           .append("BarcodeHeight        : ")
           .append(getBarcodeHeight())
           .append("\n")
           .append("Resolution           : ")
           .append(getResolution())
           .append("\n")
           .append("LeftMargin           : ")
           .append(getLeftMargin())
           .append("\n")
           .append("TopMargin            : ")
           .append(getTopMargin())
           .append("\n")
           .append("BottomMargin         : ")
           .append(getBottomMargin())
           .append("\n")
           .append("Border               : ")
           .append(getBorder())
           .append("\n")
           .append("IsDefault            : ")
           .append(getIsDefault())
           .append("\n")
        ;
        return(str.toString());
    }
}
