package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CashFlowTypeMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseCashFlowTypePeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "cash_flow_type";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CashFlowTypeMapBuilder.CLASS_NAME);
    }

      /** the column name for the CASH_FLOW_TYPE_ID field */
    public static final String CASH_FLOW_TYPE_ID;
      /** the column name for the CASH_FLOW_TYPE_CODE field */
    public static final String CASH_FLOW_TYPE_CODE;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the CASH_FLOW_TYPE_GROUP field */
    public static final String CASH_FLOW_TYPE_GROUP;
      /** the column name for the DEFAULT_TYPE field */
    public static final String DEFAULT_TYPE;
      /** the column name for the CASH_FLOW_LEVEL field */
    public static final String CASH_FLOW_LEVEL;
      /** the column name for the PARENT_ID field */
    public static final String PARENT_ID;
      /** the column name for the HAS_CHILD field */
    public static final String HAS_CHILD;
      /** the column name for the ALT_CODE field */
    public static final String ALT_CODE;
      /** the column name for the DEFAULT_CM field */
    public static final String DEFAULT_CM;
      /** the column name for the DEFAULT_DM field */
    public static final String DEFAULT_DM;
      /** the column name for the DEFAULT_RP field */
    public static final String DEFAULT_RP;
      /** the column name for the DEFAULT_PP field */
    public static final String DEFAULT_PP;
      /** the column name for the DEFAULT_CF field */
    public static final String DEFAULT_CF;
  
    static
    {
          CASH_FLOW_TYPE_ID = "cash_flow_type.CASH_FLOW_TYPE_ID";
          CASH_FLOW_TYPE_CODE = "cash_flow_type.CASH_FLOW_TYPE_CODE";
          DESCRIPTION = "cash_flow_type.DESCRIPTION";
          CASH_FLOW_TYPE_GROUP = "cash_flow_type.CASH_FLOW_TYPE_GROUP";
          DEFAULT_TYPE = "cash_flow_type.DEFAULT_TYPE";
          CASH_FLOW_LEVEL = "cash_flow_type.CASH_FLOW_LEVEL";
          PARENT_ID = "cash_flow_type.PARENT_ID";
          HAS_CHILD = "cash_flow_type.HAS_CHILD";
          ALT_CODE = "cash_flow_type.ALT_CODE";
          DEFAULT_CM = "cash_flow_type.DEFAULT_CM";
          DEFAULT_DM = "cash_flow_type.DEFAULT_DM";
          DEFAULT_RP = "cash_flow_type.DEFAULT_RP";
          DEFAULT_PP = "cash_flow_type.DEFAULT_PP";
          DEFAULT_CF = "cash_flow_type.DEFAULT_CF";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CashFlowTypeMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CashFlowTypeMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  14;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.CashFlowType";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCashFlowTypePeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_DM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_DM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_DM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_RP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_RP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_RP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_PP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_PP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_PP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CF))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CF);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CF, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CASH_FLOW_TYPE_ID);
          criteria.addSelectColumn(CASH_FLOW_TYPE_CODE);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(CASH_FLOW_TYPE_GROUP);
          criteria.addSelectColumn(DEFAULT_TYPE);
          criteria.addSelectColumn(CASH_FLOW_LEVEL);
          criteria.addSelectColumn(PARENT_ID);
          criteria.addSelectColumn(HAS_CHILD);
          criteria.addSelectColumn(ALT_CODE);
          criteria.addSelectColumn(DEFAULT_CM);
          criteria.addSelectColumn(DEFAULT_DM);
          criteria.addSelectColumn(DEFAULT_RP);
          criteria.addSelectColumn(DEFAULT_PP);
          criteria.addSelectColumn(DEFAULT_CF);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CashFlowType row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CashFlowType obj = (CashFlowType) cls.newInstance();
            CashFlowTypePeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CashFlowType obj)
        throws TorqueException
    {
        try
        {
                obj.setCashFlowTypeId(row.getValue(offset + 0).asString());
                  obj.setCashFlowTypeCode(row.getValue(offset + 1).asString());
                  obj.setDescription(row.getValue(offset + 2).asString());
                  obj.setCashFlowTypeGroup(row.getValue(offset + 3).asString());
                  obj.setDefaultType(row.getValue(offset + 4).asInt());
                  obj.setCashFlowLevel(row.getValue(offset + 5).asInt());
                  obj.setParentId(row.getValue(offset + 6).asString());
                  obj.setHasChild(row.getValue(offset + 7).asBoolean());
                  obj.setAltCode(row.getValue(offset + 8).asString());
                  obj.setDefaultCm(row.getValue(offset + 9).asBoolean());
                  obj.setDefaultDm(row.getValue(offset + 10).asBoolean());
                  obj.setDefaultRp(row.getValue(offset + 11).asBoolean());
                  obj.setDefaultPp(row.getValue(offset + 12).asBoolean());
                  obj.setDefaultCf(row.getValue(offset + 13).asBoolean());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCashFlowTypePeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_DM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_DM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_DM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_RP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_RP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_RP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_PP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_PP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_PP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CF))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CF);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CF, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CashFlowTypePeer.row2Object(row, 1,
                CashFlowTypePeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCashFlowTypePeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CASH_FLOW_TYPE_ID, criteria.remove(CASH_FLOW_TYPE_ID));
                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_DM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_DM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_DM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_RP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_RP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_RP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_PP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_PP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_PP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CF))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CF);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CF, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CashFlowTypePeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_DM))
        {
            Object possibleBoolean = criteria.get(DEFAULT_DM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_DM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_RP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_RP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_RP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_PP))
        {
            Object possibleBoolean = criteria.get(DEFAULT_PP);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_PP, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DEFAULT_CF))
        {
            Object possibleBoolean = criteria.get(DEFAULT_CF);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DEFAULT_CF, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CashFlowType obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CashFlowType obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CashFlowType obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CashFlowType obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CashFlowType) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CashFlowType obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CashFlowType) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CashFlowType obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CashFlowType) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CashFlowType obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCashFlowTypePeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CASH_FLOW_TYPE_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CashFlowType obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
              criteria.add(CASH_FLOW_TYPE_CODE, obj.getCashFlowTypeCode());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(CASH_FLOW_TYPE_GROUP, obj.getCashFlowTypeGroup());
              criteria.add(DEFAULT_TYPE, obj.getDefaultType());
              criteria.add(CASH_FLOW_LEVEL, obj.getCashFlowLevel());
              criteria.add(PARENT_ID, obj.getParentId());
              criteria.add(HAS_CHILD, obj.getHasChild());
              criteria.add(ALT_CODE, obj.getAltCode());
              criteria.add(DEFAULT_CM, obj.getDefaultCm());
              criteria.add(DEFAULT_DM, obj.getDefaultDm());
              criteria.add(DEFAULT_RP, obj.getDefaultRp());
              criteria.add(DEFAULT_PP, obj.getDefaultPp());
              criteria.add(DEFAULT_CF, obj.getDefaultCf());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CashFlowType obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
                          criteria.add(CASH_FLOW_TYPE_CODE, obj.getCashFlowTypeCode());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(CASH_FLOW_TYPE_GROUP, obj.getCashFlowTypeGroup());
                          criteria.add(DEFAULT_TYPE, obj.getDefaultType());
                          criteria.add(CASH_FLOW_LEVEL, obj.getCashFlowLevel());
                          criteria.add(PARENT_ID, obj.getParentId());
                          criteria.add(HAS_CHILD, obj.getHasChild());
                          criteria.add(ALT_CODE, obj.getAltCode());
                          criteria.add(DEFAULT_CM, obj.getDefaultCm());
                          criteria.add(DEFAULT_DM, obj.getDefaultDm());
                          criteria.add(DEFAULT_RP, obj.getDefaultRp());
                          criteria.add(DEFAULT_PP, obj.getDefaultPp());
                          criteria.add(DEFAULT_CF, obj.getDefaultCf());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowType retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowType retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowType retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CashFlowType retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowType retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CashFlowType)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CASH_FLOW_TYPE_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
