package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CashierCoupon
 */
public abstract class BaseCashierCoupon extends BaseObject
{
    /** The Peer class */
    private static final CashierCouponPeer peer =
        new CashierCouponPeer();

        
    /** The value for the cashierCouponId field */
    private String cashierCouponId;
      
    /** The value for the cashierBalanceId field */
    private String cashierBalanceId;
                                          
    /** The value for the couponType field */
    private int couponType = 0;
                                          
    /** The value for the couponValue field */
    private int couponValue = 0;
                                          
    /** The value for the trQty field */
    private int trQty = 0;
      
    /** The value for the trAmount field */
    private BigDecimal trAmount;
                                          
    /** The value for the inQty field */
    private int inQty = 0;
      
    /** The value for the inAmount field */
    private BigDecimal inAmount;
  
    
    /**
     * Get the CashierCouponId
     *
     * @return String
     */
    public String getCashierCouponId()
    {
        return cashierCouponId;
    }

                        
    /**
     * Set the value of CashierCouponId
     *
     * @param v new value
     */
    public void setCashierCouponId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierCouponId, v))
              {
            this.cashierCouponId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierBalanceId
     *
     * @return String
     */
    public String getCashierBalanceId()
    {
        return cashierBalanceId;
    }

                              
    /**
     * Set the value of CashierBalanceId
     *
     * @param v new value
     */
    public void setCashierBalanceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.cashierBalanceId, v))
              {
            this.cashierBalanceId = v;
            setModified(true);
        }
    
                          
                if (aCashierBalance != null && !ObjectUtils.equals(aCashierBalance.getCashierBalanceId(), v))
                {
            aCashierBalance = null;
        }
      
              }
  
    /**
     * Get the CouponType
     *
     * @return int
     */
    public int getCouponType()
    {
        return couponType;
    }

                        
    /**
     * Set the value of CouponType
     *
     * @param v new value
     */
    public void setCouponType(int v) 
    {
    
                  if (this.couponType != v)
              {
            this.couponType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponValue
     *
     * @return int
     */
    public int getCouponValue()
    {
        return couponValue;
    }

                        
    /**
     * Set the value of CouponValue
     *
     * @param v new value
     */
    public void setCouponValue(int v) 
    {
    
                  if (this.couponValue != v)
              {
            this.couponValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TrQty
     *
     * @return int
     */
    public int getTrQty()
    {
        return trQty;
    }

                        
    /**
     * Set the value of TrQty
     *
     * @param v new value
     */
    public void setTrQty(int v) 
    {
    
                  if (this.trQty != v)
              {
            this.trQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TrAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTrAmount()
    {
        return trAmount;
    }

                        
    /**
     * Set the value of TrAmount
     *
     * @param v new value
     */
    public void setTrAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.trAmount, v))
              {
            this.trAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InQty
     *
     * @return int
     */
    public int getInQty()
    {
        return inQty;
    }

                        
    /**
     * Set the value of InQty
     *
     * @param v new value
     */
    public void setInQty(int v) 
    {
    
                  if (this.inQty != v)
              {
            this.inQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getInAmount()
    {
        return inAmount;
    }

                        
    /**
     * Set the value of InAmount
     *
     * @param v new value
     */
    public void setInAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.inAmount, v))
              {
            this.inAmount = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private CashierBalance aCashierBalance;

    /**
     * Declares an association between this object and a CashierBalance object
     *
     * @param v CashierBalance
     * @throws TorqueException
     */
    public void setCashierBalance(CashierBalance v) throws TorqueException
    {
            if (v == null)
        {
                  setCashierBalanceId((String) null);
              }
        else
        {
            setCashierBalanceId(v.getCashierBalanceId());
        }
            aCashierBalance = v;
    }

                                            
    /**
     * Get the associated CashierBalance object
     *
     * @return the associated CashierBalance object
     * @throws TorqueException
     */
    public CashierBalance getCashierBalance() throws TorqueException
    {
        if (aCashierBalance == null && (!ObjectUtils.equals(this.cashierBalanceId, null)))
        {
                          aCashierBalance = CashierBalancePeer.retrieveByPK(SimpleKey.keyFor(this.cashierBalanceId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               CashierBalance obj = CashierBalancePeer.retrieveByPK(this.cashierBalanceId);
               obj.addCashierCoupons(this);
            */
        }
        return aCashierBalance;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCashierBalanceKey(ObjectKey key) throws TorqueException
    {
      
                        setCashierBalanceId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CashierCouponId");
              fieldNames.add("CashierBalanceId");
              fieldNames.add("CouponType");
              fieldNames.add("CouponValue");
              fieldNames.add("TrQty");
              fieldNames.add("TrAmount");
              fieldNames.add("InQty");
              fieldNames.add("InAmount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CashierCouponId"))
        {
                return getCashierCouponId();
            }
          if (name.equals("CashierBalanceId"))
        {
                return getCashierBalanceId();
            }
          if (name.equals("CouponType"))
        {
                return Integer.valueOf(getCouponType());
            }
          if (name.equals("CouponValue"))
        {
                return Integer.valueOf(getCouponValue());
            }
          if (name.equals("TrQty"))
        {
                return Integer.valueOf(getTrQty());
            }
          if (name.equals("TrAmount"))
        {
                return getTrAmount();
            }
          if (name.equals("InQty"))
        {
                return Integer.valueOf(getInQty());
            }
          if (name.equals("InAmount"))
        {
                return getInAmount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CashierCouponPeer.CASHIER_COUPON_ID))
        {
                return getCashierCouponId();
            }
          if (name.equals(CashierCouponPeer.CASHIER_BALANCE_ID))
        {
                return getCashierBalanceId();
            }
          if (name.equals(CashierCouponPeer.COUPON_TYPE))
        {
                return Integer.valueOf(getCouponType());
            }
          if (name.equals(CashierCouponPeer.COUPON_VALUE))
        {
                return Integer.valueOf(getCouponValue());
            }
          if (name.equals(CashierCouponPeer.TR_QTY))
        {
                return Integer.valueOf(getTrQty());
            }
          if (name.equals(CashierCouponPeer.TR_AMOUNT))
        {
                return getTrAmount();
            }
          if (name.equals(CashierCouponPeer.IN_QTY))
        {
                return Integer.valueOf(getInQty());
            }
          if (name.equals(CashierCouponPeer.IN_AMOUNT))
        {
                return getInAmount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCashierCouponId();
            }
              if (pos == 1)
        {
                return getCashierBalanceId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getCouponType());
            }
              if (pos == 3)
        {
                return Integer.valueOf(getCouponValue());
            }
              if (pos == 4)
        {
                return Integer.valueOf(getTrQty());
            }
              if (pos == 5)
        {
                return getTrAmount();
            }
              if (pos == 6)
        {
                return Integer.valueOf(getInQty());
            }
              if (pos == 7)
        {
                return getInAmount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CashierCouponPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CashierCouponPeer.doInsert((CashierCoupon) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CashierCouponPeer.doUpdate((CashierCoupon) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cashierCouponId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCashierCouponId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCashierCouponId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCashierCouponId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CashierCoupon copy() throws TorqueException
    {
        return copyInto(new CashierCoupon());
    }
  
    protected CashierCoupon copyInto(CashierCoupon copyObj) throws TorqueException
    {
          copyObj.setCashierCouponId(cashierCouponId);
          copyObj.setCashierBalanceId(cashierBalanceId);
          copyObj.setCouponType(couponType);
          copyObj.setCouponValue(couponValue);
          copyObj.setTrQty(trQty);
          copyObj.setTrAmount(trAmount);
          copyObj.setInQty(inQty);
          copyObj.setInAmount(inAmount);
  
                    copyObj.setCashierCouponId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CashierCouponPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CashierCoupon\n");
        str.append("-------------\n")
           .append("CashierCouponId      : ")
           .append(getCashierCouponId())
           .append("\n")
           .append("CashierBalanceId     : ")
           .append(getCashierBalanceId())
           .append("\n")
           .append("CouponType           : ")
           .append(getCouponType())
           .append("\n")
           .append("CouponValue          : ")
           .append(getCouponValue())
           .append("\n")
           .append("TrQty                : ")
           .append(getTrQty())
           .append("\n")
           .append("TrAmount             : ")
           .append(getTrAmount())
           .append("\n")
           .append("InQty                : ")
           .append(getInQty())
           .append("\n")
           .append("InAmount             : ")
           .append(getInAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}
