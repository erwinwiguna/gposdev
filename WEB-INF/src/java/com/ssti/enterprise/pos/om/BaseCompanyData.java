package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CompanyData
 */
public abstract class BaseCompanyData extends BaseObject
{
    /** The Peer class */
    private static final CompanyDataPeer peer =
        new CompanyDataPeer();

        
    /** The value for the companyDataId field */
    private String companyDataId;
                                                
    /** The value for the companyCode field */
    private String companyCode = "";
                                                
    /** The value for the companyName field */
    private String companyName = "";
                                                
    /** The value for the address field */
    private String address = "";
                                                
    /** The value for the country field */
    private String country = "";
                                                
    /** The value for the province field */
    private String province = "";
                                                
    /** The value for the city field */
    private String city = "";
                                                
    /** The value for the phone1 field */
    private String phone1 = "";
                                                
    /** The value for the phone2 field */
    private String phone2 = "";
                                                
    /** The value for the email field */
    private String email = "";
                                                
    /** The value for the website field */
    private String website = "";
                                                
    /** The value for the fax field */
    private String fax = "";
                                                
    /** The value for the taxNo field */
    private String taxNo = "";
                                                
    /** The value for the taxDate field */
    private String taxDate = "";
                                                
    /** The value for the companyLogo field */
    private String companyLogo = "";
                                                
    /** The value for the mainLogo field */
    private String mainLogo = "";
                                                
    /** The value for the posLogo field */
    private String posLogo = "";
                                                
    /** The value for the companyType field */
    private String companyType = "";
  
    
    /**
     * Get the CompanyDataId
     *
     * @return String
     */
    public String getCompanyDataId()
    {
        return companyDataId;
    }

                        
    /**
     * Set the value of CompanyDataId
     *
     * @param v new value
     */
    public void setCompanyDataId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.companyDataId, v))
              {
            this.companyDataId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CompanyCode
     *
     * @return String
     */
    public String getCompanyCode()
    {
        return companyCode;
    }

                        
    /**
     * Set the value of CompanyCode
     *
     * @param v new value
     */
    public void setCompanyCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.companyCode, v))
              {
            this.companyCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CompanyName
     *
     * @return String
     */
    public String getCompanyName()
    {
        return companyName;
    }

                        
    /**
     * Set the value of CompanyName
     *
     * @param v new value
     */
    public void setCompanyName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.companyName, v))
              {
            this.companyName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

                        
    /**
     * Set the value of Address
     *
     * @param v new value
     */
    public void setAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.address, v))
              {
            this.address = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Country
     *
     * @return String
     */
    public String getCountry()
    {
        return country;
    }

                        
    /**
     * Set the value of Country
     *
     * @param v new value
     */
    public void setCountry(String v) 
    {
    
                  if (!ObjectUtils.equals(this.country, v))
              {
            this.country = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Province
     *
     * @return String
     */
    public String getProvince()
    {
        return province;
    }

                        
    /**
     * Set the value of Province
     *
     * @param v new value
     */
    public void setProvince(String v) 
    {
    
                  if (!ObjectUtils.equals(this.province, v))
              {
            this.province = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the City
     *
     * @return String
     */
    public String getCity()
    {
        return city;
    }

                        
    /**
     * Set the value of City
     *
     * @param v new value
     */
    public void setCity(String v) 
    {
    
                  if (!ObjectUtils.equals(this.city, v))
              {
            this.city = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone1
     *
     * @return String
     */
    public String getPhone1()
    {
        return phone1;
    }

                        
    /**
     * Set the value of Phone1
     *
     * @param v new value
     */
    public void setPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone1, v))
              {
            this.phone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone2
     *
     * @return String
     */
    public String getPhone2()
    {
        return phone2;
    }

                        
    /**
     * Set the value of Phone2
     *
     * @param v new value
     */
    public void setPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone2, v))
              {
            this.phone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

                        
    /**
     * Set the value of Email
     *
     * @param v new value
     */
    public void setEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.email, v))
              {
            this.email = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Website
     *
     * @return String
     */
    public String getWebsite()
    {
        return website;
    }

                        
    /**
     * Set the value of Website
     *
     * @param v new value
     */
    public void setWebsite(String v) 
    {
    
                  if (!ObjectUtils.equals(this.website, v))
              {
            this.website = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Fax
     *
     * @return String
     */
    public String getFax()
    {
        return fax;
    }

                        
    /**
     * Set the value of Fax
     *
     * @param v new value
     */
    public void setFax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fax, v))
              {
            this.fax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxNo
     *
     * @return String
     */
    public String getTaxNo()
    {
        return taxNo;
    }

                        
    /**
     * Set the value of TaxNo
     *
     * @param v new value
     */
    public void setTaxNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxNo, v))
              {
            this.taxNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxDate
     *
     * @return String
     */
    public String getTaxDate()
    {
        return taxDate;
    }

                        
    /**
     * Set the value of TaxDate
     *
     * @param v new value
     */
    public void setTaxDate(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxDate, v))
              {
            this.taxDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CompanyLogo
     *
     * @return String
     */
    public String getCompanyLogo()
    {
        return companyLogo;
    }

                        
    /**
     * Set the value of CompanyLogo
     *
     * @param v new value
     */
    public void setCompanyLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.companyLogo, v))
              {
            this.companyLogo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MainLogo
     *
     * @return String
     */
    public String getMainLogo()
    {
        return mainLogo;
    }

                        
    /**
     * Set the value of MainLogo
     *
     * @param v new value
     */
    public void setMainLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.mainLogo, v))
              {
            this.mainLogo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosLogo
     *
     * @return String
     */
    public String getPosLogo()
    {
        return posLogo;
    }

                        
    /**
     * Set the value of PosLogo
     *
     * @param v new value
     */
    public void setPosLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posLogo, v))
              {
            this.posLogo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CompanyType
     *
     * @return String
     */
    public String getCompanyType()
    {
        return companyType;
    }

                        
    /**
     * Set the value of CompanyType
     *
     * @param v new value
     */
    public void setCompanyType(String v) 
    {
    
                  if (!ObjectUtils.equals(this.companyType, v))
              {
            this.companyType = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CompanyDataId");
              fieldNames.add("CompanyCode");
              fieldNames.add("CompanyName");
              fieldNames.add("Address");
              fieldNames.add("Country");
              fieldNames.add("Province");
              fieldNames.add("City");
              fieldNames.add("Phone1");
              fieldNames.add("Phone2");
              fieldNames.add("Email");
              fieldNames.add("Website");
              fieldNames.add("Fax");
              fieldNames.add("TaxNo");
              fieldNames.add("TaxDate");
              fieldNames.add("CompanyLogo");
              fieldNames.add("MainLogo");
              fieldNames.add("PosLogo");
              fieldNames.add("CompanyType");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CompanyDataId"))
        {
                return getCompanyDataId();
            }
          if (name.equals("CompanyCode"))
        {
                return getCompanyCode();
            }
          if (name.equals("CompanyName"))
        {
                return getCompanyName();
            }
          if (name.equals("Address"))
        {
                return getAddress();
            }
          if (name.equals("Country"))
        {
                return getCountry();
            }
          if (name.equals("Province"))
        {
                return getProvince();
            }
          if (name.equals("City"))
        {
                return getCity();
            }
          if (name.equals("Phone1"))
        {
                return getPhone1();
            }
          if (name.equals("Phone2"))
        {
                return getPhone2();
            }
          if (name.equals("Email"))
        {
                return getEmail();
            }
          if (name.equals("Website"))
        {
                return getWebsite();
            }
          if (name.equals("Fax"))
        {
                return getFax();
            }
          if (name.equals("TaxNo"))
        {
                return getTaxNo();
            }
          if (name.equals("TaxDate"))
        {
                return getTaxDate();
            }
          if (name.equals("CompanyLogo"))
        {
                return getCompanyLogo();
            }
          if (name.equals("MainLogo"))
        {
                return getMainLogo();
            }
          if (name.equals("PosLogo"))
        {
                return getPosLogo();
            }
          if (name.equals("CompanyType"))
        {
                return getCompanyType();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CompanyDataPeer.COMPANY_DATA_ID))
        {
                return getCompanyDataId();
            }
          if (name.equals(CompanyDataPeer.COMPANY_CODE))
        {
                return getCompanyCode();
            }
          if (name.equals(CompanyDataPeer.COMPANY_NAME))
        {
                return getCompanyName();
            }
          if (name.equals(CompanyDataPeer.ADDRESS))
        {
                return getAddress();
            }
          if (name.equals(CompanyDataPeer.COUNTRY))
        {
                return getCountry();
            }
          if (name.equals(CompanyDataPeer.PROVINCE))
        {
                return getProvince();
            }
          if (name.equals(CompanyDataPeer.CITY))
        {
                return getCity();
            }
          if (name.equals(CompanyDataPeer.PHONE1))
        {
                return getPhone1();
            }
          if (name.equals(CompanyDataPeer.PHONE2))
        {
                return getPhone2();
            }
          if (name.equals(CompanyDataPeer.EMAIL))
        {
                return getEmail();
            }
          if (name.equals(CompanyDataPeer.WEBSITE))
        {
                return getWebsite();
            }
          if (name.equals(CompanyDataPeer.FAX))
        {
                return getFax();
            }
          if (name.equals(CompanyDataPeer.TAX_NO))
        {
                return getTaxNo();
            }
          if (name.equals(CompanyDataPeer.TAX_DATE))
        {
                return getTaxDate();
            }
          if (name.equals(CompanyDataPeer.COMPANY_LOGO))
        {
                return getCompanyLogo();
            }
          if (name.equals(CompanyDataPeer.MAIN_LOGO))
        {
                return getMainLogo();
            }
          if (name.equals(CompanyDataPeer.POS_LOGO))
        {
                return getPosLogo();
            }
          if (name.equals(CompanyDataPeer.COMPANY_TYPE))
        {
                return getCompanyType();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCompanyDataId();
            }
              if (pos == 1)
        {
                return getCompanyCode();
            }
              if (pos == 2)
        {
                return getCompanyName();
            }
              if (pos == 3)
        {
                return getAddress();
            }
              if (pos == 4)
        {
                return getCountry();
            }
              if (pos == 5)
        {
                return getProvince();
            }
              if (pos == 6)
        {
                return getCity();
            }
              if (pos == 7)
        {
                return getPhone1();
            }
              if (pos == 8)
        {
                return getPhone2();
            }
              if (pos == 9)
        {
                return getEmail();
            }
              if (pos == 10)
        {
                return getWebsite();
            }
              if (pos == 11)
        {
                return getFax();
            }
              if (pos == 12)
        {
                return getTaxNo();
            }
              if (pos == 13)
        {
                return getTaxDate();
            }
              if (pos == 14)
        {
                return getCompanyLogo();
            }
              if (pos == 15)
        {
                return getMainLogo();
            }
              if (pos == 16)
        {
                return getPosLogo();
            }
              if (pos == 17)
        {
                return getCompanyType();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CompanyDataPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CompanyDataPeer.doInsert((CompanyData) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CompanyDataPeer.doUpdate((CompanyData) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key companyDataId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCompanyDataId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCompanyDataId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCompanyDataId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CompanyData copy() throws TorqueException
    {
        return copyInto(new CompanyData());
    }
  
    protected CompanyData copyInto(CompanyData copyObj) throws TorqueException
    {
          copyObj.setCompanyDataId(companyDataId);
          copyObj.setCompanyCode(companyCode);
          copyObj.setCompanyName(companyName);
          copyObj.setAddress(address);
          copyObj.setCountry(country);
          copyObj.setProvince(province);
          copyObj.setCity(city);
          copyObj.setPhone1(phone1);
          copyObj.setPhone2(phone2);
          copyObj.setEmail(email);
          copyObj.setWebsite(website);
          copyObj.setFax(fax);
          copyObj.setTaxNo(taxNo);
          copyObj.setTaxDate(taxDate);
          copyObj.setCompanyLogo(companyLogo);
          copyObj.setMainLogo(mainLogo);
          copyObj.setPosLogo(posLogo);
          copyObj.setCompanyType(companyType);
  
                    copyObj.setCompanyDataId((String)null);
                                                                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CompanyDataPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CompanyData\n");
        str.append("-----------\n")
           .append("CompanyDataId        : ")
           .append(getCompanyDataId())
           .append("\n")
           .append("CompanyCode          : ")
           .append(getCompanyCode())
           .append("\n")
           .append("CompanyName          : ")
           .append(getCompanyName())
           .append("\n")
           .append("Address              : ")
           .append(getAddress())
           .append("\n")
           .append("Country              : ")
           .append(getCountry())
           .append("\n")
           .append("Province             : ")
           .append(getProvince())
           .append("\n")
           .append("City                 : ")
           .append(getCity())
           .append("\n")
           .append("Phone1               : ")
           .append(getPhone1())
           .append("\n")
           .append("Phone2               : ")
           .append(getPhone2())
           .append("\n")
           .append("Email                : ")
           .append(getEmail())
           .append("\n")
           .append("Website              : ")
           .append(getWebsite())
           .append("\n")
           .append("Fax                  : ")
           .append(getFax())
           .append("\n")
           .append("TaxNo                : ")
           .append(getTaxNo())
           .append("\n")
           .append("TaxDate              : ")
           .append(getTaxDate())
           .append("\n")
           .append("CompanyLogo          : ")
           .append(getCompanyLogo())
           .append("\n")
           .append("MainLogo             : ")
           .append(getMainLogo())
           .append("\n")
           .append("PosLogo              : ")
           .append(getPosLogo())
           .append("\n")
           .append("CompanyType          : ")
           .append(getCompanyType())
           .append("\n")
        ;
        return(str.toString());
    }
}
