package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CreditMemoMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseCreditMemoPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "credit_memo";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CreditMemoMapBuilder.CLASS_NAME);
    }

      /** the column name for the CREDIT_MEMO_ID field */
    public static final String CREDIT_MEMO_ID;
      /** the column name for the CREDIT_MEMO_NO field */
    public static final String CREDIT_MEMO_NO;
      /** the column name for the TRANSACTION_DATE field */
    public static final String TRANSACTION_DATE;
      /** the column name for the TRANSACTION_ID field */
    public static final String TRANSACTION_ID;
      /** the column name for the TRANSACTION_NO field */
    public static final String TRANSACTION_NO;
      /** the column name for the TRANSACTION_TYPE field */
    public static final String TRANSACTION_TYPE;
      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the CUSTOMER_NAME field */
    public static final String CUSTOMER_NAME;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the AMOUNT field */
    public static final String AMOUNT;
      /** the column name for the AMOUNT_BASE field */
    public static final String AMOUNT_BASE;
      /** the column name for the USER_NAME field */
    public static final String USER_NAME;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the CLOSED_DATE field */
    public static final String CLOSED_DATE;
      /** the column name for the PAYMENT_TRANS_ID field */
    public static final String PAYMENT_TRANS_ID;
      /** the column name for the PAYMENT_INV_ID field */
    public static final String PAYMENT_INV_ID;
      /** the column name for the PAYMENT_TRANS_NO field */
    public static final String PAYMENT_TRANS_NO;
      /** the column name for the BANK_ID field */
    public static final String BANK_ID;
      /** the column name for the BANK_ISSUER field */
    public static final String BANK_ISSUER;
      /** the column name for the DUE_DATE field */
    public static final String DUE_DATE;
      /** the column name for the REFERENCE_NO field */
    public static final String REFERENCE_NO;
      /** the column name for the ACCOUNT_ID field */
    public static final String ACCOUNT_ID;
      /** the column name for the FISCAL_RATE field */
    public static final String FISCAL_RATE;
      /** the column name for the CLOSING_RATE field */
    public static final String CLOSING_RATE;
      /** the column name for the CASH_FLOW_TYPE_ID field */
    public static final String CASH_FLOW_TYPE_ID;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
      /** the column name for the FROM_PAYMENT_ID field */
    public static final String FROM_PAYMENT_ID;
      /** the column name for the CROSS_ACCOUNT_ID field */
    public static final String CROSS_ACCOUNT_ID;
  
    static
    {
          CREDIT_MEMO_ID = "credit_memo.CREDIT_MEMO_ID";
          CREDIT_MEMO_NO = "credit_memo.CREDIT_MEMO_NO";
          TRANSACTION_DATE = "credit_memo.TRANSACTION_DATE";
          TRANSACTION_ID = "credit_memo.TRANSACTION_ID";
          TRANSACTION_NO = "credit_memo.TRANSACTION_NO";
          TRANSACTION_TYPE = "credit_memo.TRANSACTION_TYPE";
          CUSTOMER_ID = "credit_memo.CUSTOMER_ID";
          CUSTOMER_NAME = "credit_memo.CUSTOMER_NAME";
          CURRENCY_ID = "credit_memo.CURRENCY_ID";
          CURRENCY_RATE = "credit_memo.CURRENCY_RATE";
          AMOUNT = "credit_memo.AMOUNT";
          AMOUNT_BASE = "credit_memo.AMOUNT_BASE";
          USER_NAME = "credit_memo.USER_NAME";
          REMARK = "credit_memo.REMARK";
          STATUS = "credit_memo.STATUS";
          CLOSED_DATE = "credit_memo.CLOSED_DATE";
          PAYMENT_TRANS_ID = "credit_memo.PAYMENT_TRANS_ID";
          PAYMENT_INV_ID = "credit_memo.PAYMENT_INV_ID";
          PAYMENT_TRANS_NO = "credit_memo.PAYMENT_TRANS_NO";
          BANK_ID = "credit_memo.BANK_ID";
          BANK_ISSUER = "credit_memo.BANK_ISSUER";
          DUE_DATE = "credit_memo.DUE_DATE";
          REFERENCE_NO = "credit_memo.REFERENCE_NO";
          ACCOUNT_ID = "credit_memo.ACCOUNT_ID";
          FISCAL_RATE = "credit_memo.FISCAL_RATE";
          CLOSING_RATE = "credit_memo.CLOSING_RATE";
          CASH_FLOW_TYPE_ID = "credit_memo.CASH_FLOW_TYPE_ID";
          LOCATION_ID = "credit_memo.LOCATION_ID";
          CANCEL_BY = "credit_memo.CANCEL_BY";
          CANCEL_DATE = "credit_memo.CANCEL_DATE";
          FROM_PAYMENT_ID = "credit_memo.FROM_PAYMENT_ID";
          CROSS_ACCOUNT_ID = "credit_memo.CROSS_ACCOUNT_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CreditMemoMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CreditMemoMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  32;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.CreditMemo";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCreditMemoPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                  
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CREDIT_MEMO_ID);
          criteria.addSelectColumn(CREDIT_MEMO_NO);
          criteria.addSelectColumn(TRANSACTION_DATE);
          criteria.addSelectColumn(TRANSACTION_ID);
          criteria.addSelectColumn(TRANSACTION_NO);
          criteria.addSelectColumn(TRANSACTION_TYPE);
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(CUSTOMER_NAME);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(AMOUNT);
          criteria.addSelectColumn(AMOUNT_BASE);
          criteria.addSelectColumn(USER_NAME);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(CLOSED_DATE);
          criteria.addSelectColumn(PAYMENT_TRANS_ID);
          criteria.addSelectColumn(PAYMENT_INV_ID);
          criteria.addSelectColumn(PAYMENT_TRANS_NO);
          criteria.addSelectColumn(BANK_ID);
          criteria.addSelectColumn(BANK_ISSUER);
          criteria.addSelectColumn(DUE_DATE);
          criteria.addSelectColumn(REFERENCE_NO);
          criteria.addSelectColumn(ACCOUNT_ID);
          criteria.addSelectColumn(FISCAL_RATE);
          criteria.addSelectColumn(CLOSING_RATE);
          criteria.addSelectColumn(CASH_FLOW_TYPE_ID);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
          criteria.addSelectColumn(FROM_PAYMENT_ID);
          criteria.addSelectColumn(CROSS_ACCOUNT_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CreditMemo row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CreditMemo obj = (CreditMemo) cls.newInstance();
            CreditMemoPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CreditMemo obj)
        throws TorqueException
    {
        try
        {
                obj.setCreditMemoId(row.getValue(offset + 0).asString());
                  obj.setCreditMemoNo(row.getValue(offset + 1).asString());
                  obj.setTransactionDate(row.getValue(offset + 2).asUtilDate());
                  obj.setTransactionId(row.getValue(offset + 3).asString());
                  obj.setTransactionNo(row.getValue(offset + 4).asString());
                  obj.setTransactionType(row.getValue(offset + 5).asInt());
                  obj.setCustomerId(row.getValue(offset + 6).asString());
                  obj.setCustomerName(row.getValue(offset + 7).asString());
                  obj.setCurrencyId(row.getValue(offset + 8).asString());
                  obj.setCurrencyRate(row.getValue(offset + 9).asBigDecimal());
                  obj.setAmount(row.getValue(offset + 10).asBigDecimal());
                  obj.setAmountBase(row.getValue(offset + 11).asBigDecimal());
                  obj.setUserName(row.getValue(offset + 12).asString());
                  obj.setRemark(row.getValue(offset + 13).asString());
                  obj.setStatus(row.getValue(offset + 14).asInt());
                  obj.setClosedDate(row.getValue(offset + 15).asUtilDate());
                  obj.setPaymentTransId(row.getValue(offset + 16).asString());
                  obj.setPaymentInvId(row.getValue(offset + 17).asString());
                  obj.setPaymentTransNo(row.getValue(offset + 18).asString());
                  obj.setBankId(row.getValue(offset + 19).asString());
                  obj.setBankIssuer(row.getValue(offset + 20).asString());
                  obj.setDueDate(row.getValue(offset + 21).asUtilDate());
                  obj.setReferenceNo(row.getValue(offset + 22).asString());
                  obj.setAccountId(row.getValue(offset + 23).asString());
                  obj.setFiscalRate(row.getValue(offset + 24).asBigDecimal());
                  obj.setClosingRate(row.getValue(offset + 25).asBigDecimal());
                  obj.setCashFlowTypeId(row.getValue(offset + 26).asString());
                  obj.setLocationId(row.getValue(offset + 27).asString());
                  obj.setCancelBy(row.getValue(offset + 28).asString());
                  obj.setCancelDate(row.getValue(offset + 29).asUtilDate());
                  obj.setFromPaymentId(row.getValue(offset + 30).asString());
                  obj.setCrossAccountId(row.getValue(offset + 31).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCreditMemoPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                  
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CreditMemoPeer.row2Object(row, 1,
                CreditMemoPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCreditMemoPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CREDIT_MEMO_ID, criteria.remove(CREDIT_MEMO_ID));
                                                                                                                                                                                                                                                                                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CreditMemoPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                  
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CreditMemo obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CreditMemo obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CreditMemo obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CreditMemo obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CreditMemo) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CreditMemo obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CreditMemo) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CreditMemo obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CreditMemo) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CreditMemo obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCreditMemoPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CREDIT_MEMO_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CreditMemo obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CREDIT_MEMO_ID, obj.getCreditMemoId());
              criteria.add(CREDIT_MEMO_NO, obj.getCreditMemoNo());
              criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
              criteria.add(TRANSACTION_ID, obj.getTransactionId());
              criteria.add(TRANSACTION_NO, obj.getTransactionNo());
              criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(CUSTOMER_NAME, obj.getCustomerName());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(AMOUNT, obj.getAmount());
              criteria.add(AMOUNT_BASE, obj.getAmountBase());
              criteria.add(USER_NAME, obj.getUserName());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(CLOSED_DATE, obj.getClosedDate());
              criteria.add(PAYMENT_TRANS_ID, obj.getPaymentTransId());
              criteria.add(PAYMENT_INV_ID, obj.getPaymentInvId());
              criteria.add(PAYMENT_TRANS_NO, obj.getPaymentTransNo());
              criteria.add(BANK_ID, obj.getBankId());
              criteria.add(BANK_ISSUER, obj.getBankIssuer());
              criteria.add(DUE_DATE, obj.getDueDate());
              criteria.add(REFERENCE_NO, obj.getReferenceNo());
              criteria.add(ACCOUNT_ID, obj.getAccountId());
              criteria.add(FISCAL_RATE, obj.getFiscalRate());
              criteria.add(CLOSING_RATE, obj.getClosingRate());
              criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
              criteria.add(FROM_PAYMENT_ID, obj.getFromPaymentId());
              criteria.add(CROSS_ACCOUNT_ID, obj.getCrossAccountId());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CreditMemo obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CREDIT_MEMO_ID, obj.getCreditMemoId());
                          criteria.add(CREDIT_MEMO_NO, obj.getCreditMemoNo());
                          criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
                          criteria.add(TRANSACTION_ID, obj.getTransactionId());
                          criteria.add(TRANSACTION_NO, obj.getTransactionNo());
                          criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
                          criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(CUSTOMER_NAME, obj.getCustomerName());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(AMOUNT, obj.getAmount());
                          criteria.add(AMOUNT_BASE, obj.getAmountBase());
                          criteria.add(USER_NAME, obj.getUserName());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(CLOSED_DATE, obj.getClosedDate());
                          criteria.add(PAYMENT_TRANS_ID, obj.getPaymentTransId());
                          criteria.add(PAYMENT_INV_ID, obj.getPaymentInvId());
                          criteria.add(PAYMENT_TRANS_NO, obj.getPaymentTransNo());
                          criteria.add(BANK_ID, obj.getBankId());
                          criteria.add(BANK_ISSUER, obj.getBankIssuer());
                          criteria.add(DUE_DATE, obj.getDueDate());
                          criteria.add(REFERENCE_NO, obj.getReferenceNo());
                          criteria.add(ACCOUNT_ID, obj.getAccountId());
                          criteria.add(FISCAL_RATE, obj.getFiscalRate());
                          criteria.add(CLOSING_RATE, obj.getClosingRate());
                          criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
                          criteria.add(FROM_PAYMENT_ID, obj.getFromPaymentId());
                          criteria.add(CROSS_ACCOUNT_ID, obj.getCrossAccountId());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CreditMemo retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CreditMemo retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CreditMemo retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CreditMemo retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CreditMemo retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CreditMemo)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CREDIT_MEMO_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
