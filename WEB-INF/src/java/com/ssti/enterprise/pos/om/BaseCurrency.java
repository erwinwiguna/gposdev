package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Currency
 */
public abstract class BaseCurrency extends BaseObject
{
    /** The Peer class */
    private static final CurrencyPeer peer =
        new CurrencyPeer();

        
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyCode field */
    private String currencyCode;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the symbol field */
    private String symbol;
      
    /** The value for the isActive field */
    private boolean isActive;
      
    /** The value for the isDefault field */
    private boolean isDefault;
                                                
    /** The value for the apAccount field */
    private String apAccount = "";
                                                
    /** The value for the arAccount field */
    private String arAccount = "";
                                                
    /** The value for the sdAccount field */
    private String sdAccount = "";
                                                
    /** The value for the rgAccount field */
    private String rgAccount = "";
                                                
    /** The value for the ugAccount field */
    private String ugAccount = "";
  
    
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                                              
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated CurrencyDetail
        if (collCurrencyDetails != null)
        {
            for (int i = 0; i < collCurrencyDetails.size(); i++)
            {
                ((CurrencyDetail) collCurrencyDetails.get(i))
                    .setCurrencyId(v);
            }
        }
                                }
  
    /**
     * Get the CurrencyCode
     *
     * @return String
     */
    public String getCurrencyCode()
    {
        return currencyCode;
    }

                        
    /**
     * Set the value of CurrencyCode
     *
     * @param v new value
     */
    public void setCurrencyCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyCode, v))
              {
            this.currencyCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Symbol
     *
     * @return String
     */
    public String getSymbol()
    {
        return symbol;
    }

                        
    /**
     * Set the value of Symbol
     *
     * @param v new value
     */
    public void setSymbol(String v) 
    {
    
                  if (!ObjectUtils.equals(this.symbol, v))
              {
            this.symbol = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsActive
     *
     * @return boolean
     */
    public boolean getIsActive()
    {
        return isActive;
    }

                        
    /**
     * Set the value of IsActive
     *
     * @param v new value
     */
    public void setIsActive(boolean v) 
    {
    
                  if (this.isActive != v)
              {
            this.isActive = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDefault
     *
     * @return boolean
     */
    public boolean getIsDefault()
    {
        return isDefault;
    }

                        
    /**
     * Set the value of IsDefault
     *
     * @param v new value
     */
    public void setIsDefault(boolean v) 
    {
    
                  if (this.isDefault != v)
              {
            this.isDefault = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApAccount
     *
     * @return String
     */
    public String getApAccount()
    {
        return apAccount;
    }

                        
    /**
     * Set the value of ApAccount
     *
     * @param v new value
     */
    public void setApAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.apAccount, v))
              {
            this.apAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ArAccount
     *
     * @return String
     */
    public String getArAccount()
    {
        return arAccount;
    }

                        
    /**
     * Set the value of ArAccount
     *
     * @param v new value
     */
    public void setArAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.arAccount, v))
              {
            this.arAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SdAccount
     *
     * @return String
     */
    public String getSdAccount()
    {
        return sdAccount;
    }

                        
    /**
     * Set the value of SdAccount
     *
     * @param v new value
     */
    public void setSdAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sdAccount, v))
              {
            this.sdAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RgAccount
     *
     * @return String
     */
    public String getRgAccount()
    {
        return rgAccount;
    }

                        
    /**
     * Set the value of RgAccount
     *
     * @param v new value
     */
    public void setRgAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.rgAccount, v))
              {
            this.rgAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UgAccount
     *
     * @return String
     */
    public String getUgAccount()
    {
        return ugAccount;
    }

                        
    /**
     * Set the value of UgAccount
     *
     * @param v new value
     */
    public void setUgAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.ugAccount, v))
              {
            this.ugAccount = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collCurrencyDetails
     */
    protected List collCurrencyDetails;

    /**
     * Temporary storage of collCurrencyDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCurrencyDetails()
    {
        if (collCurrencyDetails == null)
        {
            collCurrencyDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a CurrencyDetail object to this object
     * through the CurrencyDetail foreign key attribute
     *
     * @param l CurrencyDetail
     * @throws TorqueException
     */
    public void addCurrencyDetail(CurrencyDetail l) throws TorqueException
    {
        getCurrencyDetails().add(l);
        l.setCurrency((Currency) this);
    }

    /**
     * The criteria used to select the current contents of collCurrencyDetails
     */
    private Criteria lastCurrencyDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCurrencyDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCurrencyDetails() throws TorqueException
    {
              if (collCurrencyDetails == null)
        {
            collCurrencyDetails = getCurrencyDetails(new Criteria(10));
        }
        return collCurrencyDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Currency has previously
     * been saved, it will retrieve related CurrencyDetails from storage.
     * If this Currency is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCurrencyDetails(Criteria criteria) throws TorqueException
    {
              if (collCurrencyDetails == null)
        {
            if (isNew())
            {
               collCurrencyDetails = new ArrayList();
            }
            else
            {
                        criteria.add(CurrencyDetailPeer.CURRENCY_ID, getCurrencyId() );
                        collCurrencyDetails = CurrencyDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CurrencyDetailPeer.CURRENCY_ID, getCurrencyId());
                            if (!lastCurrencyDetailsCriteria.equals(criteria))
                {
                    collCurrencyDetails = CurrencyDetailPeer.doSelect(criteria);
                }
            }
        }
        lastCurrencyDetailsCriteria = criteria;

        return collCurrencyDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCurrencyDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCurrencyDetails(Connection con) throws TorqueException
    {
              if (collCurrencyDetails == null)
        {
            collCurrencyDetails = getCurrencyDetails(new Criteria(10), con);
        }
        return collCurrencyDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Currency has previously
     * been saved, it will retrieve related CurrencyDetails from storage.
     * If this Currency is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCurrencyDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCurrencyDetails == null)
        {
            if (isNew())
            {
               collCurrencyDetails = new ArrayList();
            }
            else
            {
                         criteria.add(CurrencyDetailPeer.CURRENCY_ID, getCurrencyId());
                         collCurrencyDetails = CurrencyDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CurrencyDetailPeer.CURRENCY_ID, getCurrencyId());
                             if (!lastCurrencyDetailsCriteria.equals(criteria))
                 {
                     collCurrencyDetails = CurrencyDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCurrencyDetailsCriteria = criteria;

         return collCurrencyDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Currency is new, it will return
     * an empty collection; or if this Currency has previously
     * been saved, it will retrieve related CurrencyDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Currency.
     */
    protected List getCurrencyDetailsJoinCurrency(Criteria criteria)
        throws TorqueException
    {
                    if (collCurrencyDetails == null)
        {
            if (isNew())
            {
               collCurrencyDetails = new ArrayList();
            }
            else
            {
                              criteria.add(CurrencyDetailPeer.CURRENCY_ID, getCurrencyId());
                              collCurrencyDetails = CurrencyDetailPeer.doSelectJoinCurrency(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CurrencyDetailPeer.CURRENCY_ID, getCurrencyId());
                                    if (!lastCurrencyDetailsCriteria.equals(criteria))
            {
                collCurrencyDetails = CurrencyDetailPeer.doSelectJoinCurrency(criteria);
            }
        }
        lastCurrencyDetailsCriteria = criteria;

        return collCurrencyDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyCode");
              fieldNames.add("Description");
              fieldNames.add("Symbol");
              fieldNames.add("IsActive");
              fieldNames.add("IsDefault");
              fieldNames.add("ApAccount");
              fieldNames.add("ArAccount");
              fieldNames.add("SdAccount");
              fieldNames.add("RgAccount");
              fieldNames.add("UgAccount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyCode"))
        {
                return getCurrencyCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Symbol"))
        {
                return getSymbol();
            }
          if (name.equals("IsActive"))
        {
                return Boolean.valueOf(getIsActive());
            }
          if (name.equals("IsDefault"))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals("ApAccount"))
        {
                return getApAccount();
            }
          if (name.equals("ArAccount"))
        {
                return getArAccount();
            }
          if (name.equals("SdAccount"))
        {
                return getSdAccount();
            }
          if (name.equals("RgAccount"))
        {
                return getRgAccount();
            }
          if (name.equals("UgAccount"))
        {
                return getUgAccount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CurrencyPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(CurrencyPeer.CURRENCY_CODE))
        {
                return getCurrencyCode();
            }
          if (name.equals(CurrencyPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(CurrencyPeer.SYMBOL))
        {
                return getSymbol();
            }
          if (name.equals(CurrencyPeer.IS_ACTIVE))
        {
                return Boolean.valueOf(getIsActive());
            }
          if (name.equals(CurrencyPeer.IS_DEFAULT))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals(CurrencyPeer.AP_ACCOUNT))
        {
                return getApAccount();
            }
          if (name.equals(CurrencyPeer.AR_ACCOUNT))
        {
                return getArAccount();
            }
          if (name.equals(CurrencyPeer.SD_ACCOUNT))
        {
                return getSdAccount();
            }
          if (name.equals(CurrencyPeer.RG_ACCOUNT))
        {
                return getRgAccount();
            }
          if (name.equals(CurrencyPeer.UG_ACCOUNT))
        {
                return getUgAccount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCurrencyId();
            }
              if (pos == 1)
        {
                return getCurrencyCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return getSymbol();
            }
              if (pos == 4)
        {
                return Boolean.valueOf(getIsActive());
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getIsDefault());
            }
              if (pos == 6)
        {
                return getApAccount();
            }
              if (pos == 7)
        {
                return getArAccount();
            }
              if (pos == 8)
        {
                return getSdAccount();
            }
              if (pos == 9)
        {
                return getRgAccount();
            }
              if (pos == 10)
        {
                return getUgAccount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CurrencyPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CurrencyPeer.doInsert((Currency) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CurrencyPeer.doUpdate((Currency) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collCurrencyDetails != null)
            {
                for (int i = 0; i < collCurrencyDetails.size(); i++)
                {
                    ((CurrencyDetail) collCurrencyDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key currencyId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setCurrencyId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setCurrencyId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCurrencyId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Currency copy() throws TorqueException
    {
        return copyInto(new Currency());
    }
  
    protected Currency copyInto(Currency copyObj) throws TorqueException
    {
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyCode(currencyCode);
          copyObj.setDescription(description);
          copyObj.setSymbol(symbol);
          copyObj.setIsActive(isActive);
          copyObj.setIsDefault(isDefault);
          copyObj.setApAccount(apAccount);
          copyObj.setArAccount(arAccount);
          copyObj.setSdAccount(sdAccount);
          copyObj.setRgAccount(rgAccount);
          copyObj.setUgAccount(ugAccount);
  
                    copyObj.setCurrencyId((String)null);
                                                                        
                                      
                            
        List v = getCurrencyDetails();
        for (int i = 0; i < v.size(); i++)
        {
            CurrencyDetail obj = (CurrencyDetail) v.get(i);
            copyObj.addCurrencyDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CurrencyPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Currency\n");
        str.append("--------\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyCode         : ")
           .append(getCurrencyCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Symbol               : ")
           .append(getSymbol())
           .append("\n")
           .append("IsActive             : ")
           .append(getIsActive())
           .append("\n")
           .append("IsDefault            : ")
           .append(getIsDefault())
           .append("\n")
           .append("ApAccount            : ")
           .append(getApAccount())
           .append("\n")
           .append("ArAccount            : ")
           .append(getArAccount())
           .append("\n")
           .append("SdAccount            : ")
           .append(getSdAccount())
           .append("\n")
           .append("RgAccount            : ")
           .append(getRgAccount())
           .append("\n")
           .append("UgAccount            : ")
           .append(getUgAccount())
           .append("\n")
        ;
        return(str.toString());
    }
}
