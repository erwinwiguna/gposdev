package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CurrencyDetail
 */
public abstract class BaseCurrencyDetail extends BaseObject
{
    /** The Peer class */
    private static final CurrencyDetailPeer peer =
        new CurrencyDetailPeer();

        
    /** The value for the currencyDetailId field */
    private String currencyDetailId;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the beginDate field */
    private Date beginDate;
      
    /** The value for the endDate field */
    private Date endDate;
      
    /** The value for the exchangeRate field */
    private BigDecimal exchangeRate;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the issueDate field */
    private Date issueDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the CurrencyDetailId
     *
     * @return String
     */
    public String getCurrencyDetailId()
    {
        return currencyDetailId;
    }

                        
    /**
     * Set the value of CurrencyDetailId
     *
     * @param v new value
     */
    public void setCurrencyDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyDetailId, v))
              {
            this.currencyDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                              
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
                          
                if (aCurrency != null && !ObjectUtils.equals(aCurrency.getCurrencyId(), v))
                {
            aCurrency = null;
        }
      
              }
  
    /**
     * Get the BeginDate
     *
     * @return Date
     */
    public Date getBeginDate()
    {
        return beginDate;
    }

                        
    /**
     * Set the value of BeginDate
     *
     * @param v new value
     */
    public void setBeginDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.beginDate, v))
              {
            this.beginDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndDate
     *
     * @return Date
     */
    public Date getEndDate()
    {
        return endDate;
    }

                        
    /**
     * Set the value of EndDate
     *
     * @param v new value
     */
    public void setEndDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.endDate, v))
              {
            this.endDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExchangeRate
     *
     * @return BigDecimal
     */
    public BigDecimal getExchangeRate()
    {
        return exchangeRate;
    }

                        
    /**
     * Set the value of ExchangeRate
     *
     * @param v new value
     */
    public void setExchangeRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.exchangeRate, v))
              {
            this.exchangeRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IssueDate
     *
     * @return Date
     */
    public Date getIssueDate()
    {
        return issueDate;
    }

                        
    /**
     * Set the value of IssueDate
     *
     * @param v new value
     */
    public void setIssueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.issueDate, v))
              {
            this.issueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Currency aCurrency;

    /**
     * Declares an association between this object and a Currency object
     *
     * @param v Currency
     * @throws TorqueException
     */
    public void setCurrency(Currency v) throws TorqueException
    {
            if (v == null)
        {
                  setCurrencyId((String) null);
              }
        else
        {
            setCurrencyId(v.getCurrencyId());
        }
            aCurrency = v;
    }

                                            
    /**
     * Get the associated Currency object
     *
     * @return the associated Currency object
     * @throws TorqueException
     */
    public Currency getCurrency() throws TorqueException
    {
        if (aCurrency == null && (!ObjectUtils.equals(this.currencyId, null)))
        {
                          aCurrency = CurrencyPeer.retrieveByPK(SimpleKey.keyFor(this.currencyId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Currency obj = CurrencyPeer.retrieveByPK(this.currencyId);
               obj.addCurrencyDetails(this);
            */
        }
        return aCurrency;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCurrencyKey(ObjectKey key) throws TorqueException
    {
      
                        setCurrencyId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CurrencyDetailId");
              fieldNames.add("CurrencyId");
              fieldNames.add("BeginDate");
              fieldNames.add("EndDate");
              fieldNames.add("ExchangeRate");
              fieldNames.add("FiscalRate");
              fieldNames.add("Description");
              fieldNames.add("IssueDate");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CurrencyDetailId"))
        {
                return getCurrencyDetailId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("BeginDate"))
        {
                return getBeginDate();
            }
          if (name.equals("EndDate"))
        {
                return getEndDate();
            }
          if (name.equals("ExchangeRate"))
        {
                return getExchangeRate();
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("IssueDate"))
        {
                return getIssueDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CurrencyDetailPeer.CURRENCY_DETAIL_ID))
        {
                return getCurrencyDetailId();
            }
          if (name.equals(CurrencyDetailPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(CurrencyDetailPeer.BEGIN_DATE))
        {
                return getBeginDate();
            }
          if (name.equals(CurrencyDetailPeer.END_DATE))
        {
                return getEndDate();
            }
          if (name.equals(CurrencyDetailPeer.EXCHANGE_RATE))
        {
                return getExchangeRate();
            }
          if (name.equals(CurrencyDetailPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(CurrencyDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(CurrencyDetailPeer.ISSUE_DATE))
        {
                return getIssueDate();
            }
          if (name.equals(CurrencyDetailPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCurrencyDetailId();
            }
              if (pos == 1)
        {
                return getCurrencyId();
            }
              if (pos == 2)
        {
                return getBeginDate();
            }
              if (pos == 3)
        {
                return getEndDate();
            }
              if (pos == 4)
        {
                return getExchangeRate();
            }
              if (pos == 5)
        {
                return getFiscalRate();
            }
              if (pos == 6)
        {
                return getDescription();
            }
              if (pos == 7)
        {
                return getIssueDate();
            }
              if (pos == 8)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CurrencyDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CurrencyDetailPeer.doInsert((CurrencyDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CurrencyDetailPeer.doUpdate((CurrencyDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key currencyDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCurrencyDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCurrencyDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCurrencyDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CurrencyDetail copy() throws TorqueException
    {
        return copyInto(new CurrencyDetail());
    }
  
    protected CurrencyDetail copyInto(CurrencyDetail copyObj) throws TorqueException
    {
          copyObj.setCurrencyDetailId(currencyDetailId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setBeginDate(beginDate);
          copyObj.setEndDate(endDate);
          copyObj.setExchangeRate(exchangeRate);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setDescription(description);
          copyObj.setIssueDate(issueDate);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setCurrencyDetailId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CurrencyDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CurrencyDetail\n");
        str.append("--------------\n")
           .append("CurrencyDetailId     : ")
           .append(getCurrencyDetailId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("BeginDate            : ")
           .append(getBeginDate())
           .append("\n")
           .append("EndDate              : ")
           .append(getEndDate())
           .append("\n")
           .append("ExchangeRate         : ")
           .append(getExchangeRate())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("IssueDate            : ")
           .append(getIssueDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
