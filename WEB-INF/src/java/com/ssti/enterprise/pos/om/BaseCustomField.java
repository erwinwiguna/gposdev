package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomField
 */
public abstract class BaseCustomField extends BaseObject
{
    /** The Peer class */
    private static final CustomFieldPeer peer =
        new CustomFieldPeer();

        
    /** The value for the customFieldId field */
    private String customFieldId;
      
    /** The value for the fieldName field */
    private String fieldName;
      
    /** The value for the fieldDefaultValue field */
    private String fieldDefaultValue;
                                          
    /** The value for the fieldType field */
    private int fieldType = 1;
                                          
    /** The value for the fieldNo field */
    private int fieldNo = 1;
                                                
    /** The value for the fieldGroup field */
    private String fieldGroup = "";
                                                
    /** The value for the kategoriId field */
    private String kategoriId = "";
                                                                
    /** The value for the required field */
    private boolean required = false;
  
    
    /**
     * Get the CustomFieldId
     *
     * @return String
     */
    public String getCustomFieldId()
    {
        return customFieldId;
    }

                        
    /**
     * Set the value of CustomFieldId
     *
     * @param v new value
     */
    public void setCustomFieldId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customFieldId, v))
              {
            this.customFieldId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldName
     *
     * @return String
     */
    public String getFieldName()
    {
        return fieldName;
    }

                        
    /**
     * Set the value of FieldName
     *
     * @param v new value
     */
    public void setFieldName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldName, v))
              {
            this.fieldName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldDefaultValue
     *
     * @return String
     */
    public String getFieldDefaultValue()
    {
        return fieldDefaultValue;
    }

                        
    /**
     * Set the value of FieldDefaultValue
     *
     * @param v new value
     */
    public void setFieldDefaultValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldDefaultValue, v))
              {
            this.fieldDefaultValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldType
     *
     * @return int
     */
    public int getFieldType()
    {
        return fieldType;
    }

                        
    /**
     * Set the value of FieldType
     *
     * @param v new value
     */
    public void setFieldType(int v) 
    {
    
                  if (this.fieldType != v)
              {
            this.fieldType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldNo
     *
     * @return int
     */
    public int getFieldNo()
    {
        return fieldNo;
    }

                        
    /**
     * Set the value of FieldNo
     *
     * @param v new value
     */
    public void setFieldNo(int v) 
    {
    
                  if (this.fieldNo != v)
              {
            this.fieldNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldGroup
     *
     * @return String
     */
    public String getFieldGroup()
    {
        return fieldGroup;
    }

                        
    /**
     * Set the value of FieldGroup
     *
     * @param v new value
     */
    public void setFieldGroup(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldGroup, v))
              {
            this.fieldGroup = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KategoriId
     *
     * @return String
     */
    public String getKategoriId()
    {
        return kategoriId;
    }

                        
    /**
     * Set the value of KategoriId
     *
     * @param v new value
     */
    public void setKategoriId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.kategoriId, v))
              {
            this.kategoriId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Required
     *
     * @return boolean
     */
    public boolean getRequired()
    {
        return required;
    }

                        
    /**
     * Set the value of Required
     *
     * @param v new value
     */
    public void setRequired(boolean v) 
    {
    
                  if (this.required != v)
              {
            this.required = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomFieldId");
              fieldNames.add("FieldName");
              fieldNames.add("FieldDefaultValue");
              fieldNames.add("FieldType");
              fieldNames.add("FieldNo");
              fieldNames.add("FieldGroup");
              fieldNames.add("KategoriId");
              fieldNames.add("Required");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomFieldId"))
        {
                return getCustomFieldId();
            }
          if (name.equals("FieldName"))
        {
                return getFieldName();
            }
          if (name.equals("FieldDefaultValue"))
        {
                return getFieldDefaultValue();
            }
          if (name.equals("FieldType"))
        {
                return Integer.valueOf(getFieldType());
            }
          if (name.equals("FieldNo"))
        {
                return Integer.valueOf(getFieldNo());
            }
          if (name.equals("FieldGroup"))
        {
                return getFieldGroup();
            }
          if (name.equals("KategoriId"))
        {
                return getKategoriId();
            }
          if (name.equals("Required"))
        {
                return Boolean.valueOf(getRequired());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomFieldPeer.CUSTOM_FIELD_ID))
        {
                return getCustomFieldId();
            }
          if (name.equals(CustomFieldPeer.FIELD_NAME))
        {
                return getFieldName();
            }
          if (name.equals(CustomFieldPeer.FIELD_DEFAULT_VALUE))
        {
                return getFieldDefaultValue();
            }
          if (name.equals(CustomFieldPeer.FIELD_TYPE))
        {
                return Integer.valueOf(getFieldType());
            }
          if (name.equals(CustomFieldPeer.FIELD_NO))
        {
                return Integer.valueOf(getFieldNo());
            }
          if (name.equals(CustomFieldPeer.FIELD_GROUP))
        {
                return getFieldGroup();
            }
          if (name.equals(CustomFieldPeer.KATEGORI_ID))
        {
                return getKategoriId();
            }
          if (name.equals(CustomFieldPeer.REQUIRED))
        {
                return Boolean.valueOf(getRequired());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomFieldId();
            }
              if (pos == 1)
        {
                return getFieldName();
            }
              if (pos == 2)
        {
                return getFieldDefaultValue();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getFieldType());
            }
              if (pos == 4)
        {
                return Integer.valueOf(getFieldNo());
            }
              if (pos == 5)
        {
                return getFieldGroup();
            }
              if (pos == 6)
        {
                return getKategoriId();
            }
              if (pos == 7)
        {
                return Boolean.valueOf(getRequired());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomFieldPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomFieldPeer.doInsert((CustomField) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomFieldPeer.doUpdate((CustomField) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customFieldId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomFieldId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomFieldId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomFieldId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomField copy() throws TorqueException
    {
        return copyInto(new CustomField());
    }
  
    protected CustomField copyInto(CustomField copyObj) throws TorqueException
    {
          copyObj.setCustomFieldId(customFieldId);
          copyObj.setFieldName(fieldName);
          copyObj.setFieldDefaultValue(fieldDefaultValue);
          copyObj.setFieldType(fieldType);
          copyObj.setFieldNo(fieldNo);
          copyObj.setFieldGroup(fieldGroup);
          copyObj.setKategoriId(kategoriId);
          copyObj.setRequired(required);
  
                    copyObj.setCustomFieldId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomFieldPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomField\n");
        str.append("-----------\n")
           .append("CustomFieldId        : ")
           .append(getCustomFieldId())
           .append("\n")
           .append("FieldName            : ")
           .append(getFieldName())
           .append("\n")
           .append("FieldDefaultValue    : ")
           .append(getFieldDefaultValue())
           .append("\n")
           .append("FieldType            : ")
           .append(getFieldType())
           .append("\n")
           .append("FieldNo              : ")
           .append(getFieldNo())
           .append("\n")
           .append("FieldGroup           : ")
           .append(getFieldGroup())
           .append("\n")
           .append("KategoriId           : ")
           .append(getKategoriId())
           .append("\n")
           .append("Required             : ")
           .append(getRequired())
           .append("\n")
        ;
        return(str.toString());
    }
}
