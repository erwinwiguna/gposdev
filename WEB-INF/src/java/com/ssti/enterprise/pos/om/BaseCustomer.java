package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Customer
 */
public abstract class BaseCustomer extends BaseObject
{
    /** The Peer class */
    private static final CustomerPeer peer =
        new CustomerPeer();

        
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerCode field */
    private String customerCode;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the customerTypeId field */
    private String customerTypeId;
                                                
    /** The value for the statusId field */
    private String statusId = "";
                                                
    /** The value for the invoiceAddress field */
    private String invoiceAddress = "";
                                                
    /** The value for the taxNo field */
    private String taxNo = "";
                                                
    /** The value for the contactName1 field */
    private String contactName1 = "";
                                                
    /** The value for the contactName2 field */
    private String contactName2 = "";
                                                
    /** The value for the contactName3 field */
    private String contactName3 = "";
                                                
    /** The value for the jobTitle1 field */
    private String jobTitle1 = "";
                                                
    /** The value for the jobTitle2 field */
    private String jobTitle2 = "";
                                                
    /** The value for the jobTitle3 field */
    private String jobTitle3 = "";
                                                
    /** The value for the contactPhone1 field */
    private String contactPhone1 = "";
                                                
    /** The value for the contactPhone2 field */
    private String contactPhone2 = "";
                                                
    /** The value for the contactPhone3 field */
    private String contactPhone3 = "";
                                                
    /** The value for the address field */
    private String address = "";
                                                
    /** The value for the shipTo field */
    private String shipTo = "";
                                                
    /** The value for the zip field */
    private String zip = "";
                                                
    /** The value for the country field */
    private String country = "";
                                                
    /** The value for the province field */
    private String province = "";
                                                
    /** The value for the city field */
    private String city = "";
                                                
    /** The value for the district field */
    private String district = "";
                                                
    /** The value for the village field */
    private String village = "";
                                                
    /** The value for the phone1 field */
    private String phone1 = "";
                                                
    /** The value for the phone2 field */
    private String phone2 = "";
                                                
    /** The value for the fax field */
    private String fax = "";
                                                
    /** The value for the email field */
    private String email = "";
                                                
    /** The value for the webSite field */
    private String webSite = "";
                                                
    /** The value for the defaultTaxId field */
    private String defaultTaxId = "";
                                                
    /** The value for the defaultTypeId field */
    private String defaultTypeId = "";
                                                
    /** The value for the defaultTermId field */
    private String defaultTermId = "";
                                                
    /** The value for the defaultEmployeeId field */
    private String defaultEmployeeId = "";
                                                
    /** The value for the defaultCurrencyId field */
    private String defaultCurrencyId = "";
                                                
    /** The value for the defaultLocationId field */
    private String defaultLocationId = "";
                                                
    /** The value for the defaultDiscountAmount field */
    private String defaultDiscountAmount = "0";
                                                
          
    /** The value for the creditLimit field */
    private BigDecimal creditLimit= bd_ZERO;
      
    /** The value for the memo field */
    private String memo;
      
    /** The value for the invoiceMessage field */
    private String invoiceMessage;
      
    /** The value for the addDate field */
    private Date addDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the lastUpdateLocationId field */
    private String lastUpdateLocationId;
      
    /** The value for the isDefault field */
    private boolean isDefault;
                                                                
    /** The value for the isActive field */
    private boolean isActive = true;
                                                
    /** The value for the field1 field */
    private String field1 = "";
                                                
    /** The value for the field2 field */
    private String field2 = "";
                                                
    /** The value for the value1 field */
    private String value1 = "";
                                                
    /** The value for the value2 field */
    private String value2 = "";
                                                
    /** The value for the arAccount field */
    private String arAccount = "";
                                                
          
    /** The value for the openingBalance field */
    private BigDecimal openingBalance= bd_ZERO;
      
    /** The value for the asDate field */
    private Date asDate;
                                                
    /** The value for the obTransId field */
    private String obTransId = "";
                                                
          
    /** The value for the obRate field */
    private BigDecimal obRate= new BigDecimal(1);
      
    /** The value for the birthDate field */
    private Date birthDate;
                                                
    /** The value for the birthPlace field */
    private String birthPlace = "";
                                                
    /** The value for the religion field */
    private String religion = "";
      
    /** The value for the gender field */
    private int gender;
                                                
    /** The value for the occupation field */
    private String occupation = "";
                                                
    /** The value for the hobby field */
    private String hobby = "";
                                                
    /** The value for the transPrefix field */
    private String transPrefix = "";
                                                
    /** The value for the createBy field */
    private String createBy = "";
                                                
    /** The value for the lastUpdateBy field */
    private String lastUpdateBy = "";
                                                
    /** The value for the salesAreaId field */
    private String salesAreaId = "";
                                                
    /** The value for the territoryId field */
    private String territoryId = "";
                                                
          
    /** The value for the longitudes field */
    private BigDecimal longitudes= bd_ZERO;
                                                
          
    /** The value for the latitudes field */
    private BigDecimal latitudes= bd_ZERO;
                                                
    /** The value for the billToId field */
    private String billToId = "";
                                                
    /** The value for the parentId field */
    private String parentId = "";
  
    
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                                              
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated CustomerContact
        if (collCustomerContacts != null)
        {
            for (int i = 0; i < collCustomerContacts.size(); i++)
            {
                ((CustomerContact) collCustomerContacts.get(i))
                    .setCustomerId(v);
            }
        }
                                                    
                  // update associated CustomerAddress
        if (collCustomerAddresss != null)
        {
            for (int i = 0; i < collCustomerAddresss.size(); i++)
            {
                ((CustomerAddress) collCustomerAddresss.get(i))
                    .setCustomerId(v);
            }
        }
                                                    
                  // update associated CustomerFollowup
        if (collCustomerFollowups != null)
        {
            for (int i = 0; i < collCustomerFollowups.size(); i++)
            {
                ((CustomerFollowup) collCustomerFollowups.get(i))
                    .setCustomerId(v);
            }
        }
                                }
  
    /**
     * Get the CustomerCode
     *
     * @return String
     */
    public String getCustomerCode()
    {
        return customerCode;
    }

                        
    /**
     * Set the value of CustomerCode
     *
     * @param v new value
     */
    public void setCustomerCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerCode, v))
              {
            this.customerCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StatusId
     *
     * @return String
     */
    public String getStatusId()
    {
        return statusId;
    }

                        
    /**
     * Set the value of StatusId
     *
     * @param v new value
     */
    public void setStatusId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.statusId, v))
              {
            this.statusId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceAddress
     *
     * @return String
     */
    public String getInvoiceAddress()
    {
        return invoiceAddress;
    }

                        
    /**
     * Set the value of InvoiceAddress
     *
     * @param v new value
     */
    public void setInvoiceAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceAddress, v))
              {
            this.invoiceAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxNo
     *
     * @return String
     */
    public String getTaxNo()
    {
        return taxNo;
    }

                        
    /**
     * Set the value of TaxNo
     *
     * @param v new value
     */
    public void setTaxNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxNo, v))
              {
            this.taxNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName1
     *
     * @return String
     */
    public String getContactName1()
    {
        return contactName1;
    }

                        
    /**
     * Set the value of ContactName1
     *
     * @param v new value
     */
    public void setContactName1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName1, v))
              {
            this.contactName1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName2
     *
     * @return String
     */
    public String getContactName2()
    {
        return contactName2;
    }

                        
    /**
     * Set the value of ContactName2
     *
     * @param v new value
     */
    public void setContactName2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName2, v))
              {
            this.contactName2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName3
     *
     * @return String
     */
    public String getContactName3()
    {
        return contactName3;
    }

                        
    /**
     * Set the value of ContactName3
     *
     * @param v new value
     */
    public void setContactName3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName3, v))
              {
            this.contactName3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle1
     *
     * @return String
     */
    public String getJobTitle1()
    {
        return jobTitle1;
    }

                        
    /**
     * Set the value of JobTitle1
     *
     * @param v new value
     */
    public void setJobTitle1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle1, v))
              {
            this.jobTitle1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle2
     *
     * @return String
     */
    public String getJobTitle2()
    {
        return jobTitle2;
    }

                        
    /**
     * Set the value of JobTitle2
     *
     * @param v new value
     */
    public void setJobTitle2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle2, v))
              {
            this.jobTitle2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle3
     *
     * @return String
     */
    public String getJobTitle3()
    {
        return jobTitle3;
    }

                        
    /**
     * Set the value of JobTitle3
     *
     * @param v new value
     */
    public void setJobTitle3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle3, v))
              {
            this.jobTitle3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone1
     *
     * @return String
     */
    public String getContactPhone1()
    {
        return contactPhone1;
    }

                        
    /**
     * Set the value of ContactPhone1
     *
     * @param v new value
     */
    public void setContactPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone1, v))
              {
            this.contactPhone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone2
     *
     * @return String
     */
    public String getContactPhone2()
    {
        return contactPhone2;
    }

                        
    /**
     * Set the value of ContactPhone2
     *
     * @param v new value
     */
    public void setContactPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone2, v))
              {
            this.contactPhone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone3
     *
     * @return String
     */
    public String getContactPhone3()
    {
        return contactPhone3;
    }

                        
    /**
     * Set the value of ContactPhone3
     *
     * @param v new value
     */
    public void setContactPhone3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone3, v))
              {
            this.contactPhone3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

                        
    /**
     * Set the value of Address
     *
     * @param v new value
     */
    public void setAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.address, v))
              {
            this.address = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipTo
     *
     * @return String
     */
    public String getShipTo()
    {
        return shipTo;
    }

                        
    /**
     * Set the value of ShipTo
     *
     * @param v new value
     */
    public void setShipTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shipTo, v))
              {
            this.shipTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Zip
     *
     * @return String
     */
    public String getZip()
    {
        return zip;
    }

                        
    /**
     * Set the value of Zip
     *
     * @param v new value
     */
    public void setZip(String v) 
    {
    
                  if (!ObjectUtils.equals(this.zip, v))
              {
            this.zip = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Country
     *
     * @return String
     */
    public String getCountry()
    {
        return country;
    }

                        
    /**
     * Set the value of Country
     *
     * @param v new value
     */
    public void setCountry(String v) 
    {
    
                  if (!ObjectUtils.equals(this.country, v))
              {
            this.country = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Province
     *
     * @return String
     */
    public String getProvince()
    {
        return province;
    }

                        
    /**
     * Set the value of Province
     *
     * @param v new value
     */
    public void setProvince(String v) 
    {
    
                  if (!ObjectUtils.equals(this.province, v))
              {
            this.province = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the City
     *
     * @return String
     */
    public String getCity()
    {
        return city;
    }

                        
    /**
     * Set the value of City
     *
     * @param v new value
     */
    public void setCity(String v) 
    {
    
                  if (!ObjectUtils.equals(this.city, v))
              {
            this.city = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the District
     *
     * @return String
     */
    public String getDistrict()
    {
        return district;
    }

                        
    /**
     * Set the value of District
     *
     * @param v new value
     */
    public void setDistrict(String v) 
    {
    
                  if (!ObjectUtils.equals(this.district, v))
              {
            this.district = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Village
     *
     * @return String
     */
    public String getVillage()
    {
        return village;
    }

                        
    /**
     * Set the value of Village
     *
     * @param v new value
     */
    public void setVillage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.village, v))
              {
            this.village = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone1
     *
     * @return String
     */
    public String getPhone1()
    {
        return phone1;
    }

                        
    /**
     * Set the value of Phone1
     *
     * @param v new value
     */
    public void setPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone1, v))
              {
            this.phone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone2
     *
     * @return String
     */
    public String getPhone2()
    {
        return phone2;
    }

                        
    /**
     * Set the value of Phone2
     *
     * @param v new value
     */
    public void setPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone2, v))
              {
            this.phone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Fax
     *
     * @return String
     */
    public String getFax()
    {
        return fax;
    }

                        
    /**
     * Set the value of Fax
     *
     * @param v new value
     */
    public void setFax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fax, v))
              {
            this.fax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

                        
    /**
     * Set the value of Email
     *
     * @param v new value
     */
    public void setEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.email, v))
              {
            this.email = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the WebSite
     *
     * @return String
     */
    public String getWebSite()
    {
        return webSite;
    }

                        
    /**
     * Set the value of WebSite
     *
     * @param v new value
     */
    public void setWebSite(String v) 
    {
    
                  if (!ObjectUtils.equals(this.webSite, v))
              {
            this.webSite = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTaxId
     *
     * @return String
     */
    public String getDefaultTaxId()
    {
        return defaultTaxId;
    }

                        
    /**
     * Set the value of DefaultTaxId
     *
     * @param v new value
     */
    public void setDefaultTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTaxId, v))
              {
            this.defaultTaxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTypeId
     *
     * @return String
     */
    public String getDefaultTypeId()
    {
        return defaultTypeId;
    }

                        
    /**
     * Set the value of DefaultTypeId
     *
     * @param v new value
     */
    public void setDefaultTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTypeId, v))
              {
            this.defaultTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTermId
     *
     * @return String
     */
    public String getDefaultTermId()
    {
        return defaultTermId;
    }

                        
    /**
     * Set the value of DefaultTermId
     *
     * @param v new value
     */
    public void setDefaultTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTermId, v))
              {
            this.defaultTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultEmployeeId
     *
     * @return String
     */
    public String getDefaultEmployeeId()
    {
        return defaultEmployeeId;
    }

                        
    /**
     * Set the value of DefaultEmployeeId
     *
     * @param v new value
     */
    public void setDefaultEmployeeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultEmployeeId, v))
              {
            this.defaultEmployeeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultCurrencyId
     *
     * @return String
     */
    public String getDefaultCurrencyId()
    {
        return defaultCurrencyId;
    }

                        
    /**
     * Set the value of DefaultCurrencyId
     *
     * @param v new value
     */
    public void setDefaultCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultCurrencyId, v))
              {
            this.defaultCurrencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultLocationId
     *
     * @return String
     */
    public String getDefaultLocationId()
    {
        return defaultLocationId;
    }

                        
    /**
     * Set the value of DefaultLocationId
     *
     * @param v new value
     */
    public void setDefaultLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultLocationId, v))
              {
            this.defaultLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultDiscountAmount
     *
     * @return String
     */
    public String getDefaultDiscountAmount()
    {
        return defaultDiscountAmount;
    }

                        
    /**
     * Set the value of DefaultDiscountAmount
     *
     * @param v new value
     */
    public void setDefaultDiscountAmount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultDiscountAmount, v))
              {
            this.defaultDiscountAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreditLimit
     *
     * @return BigDecimal
     */
    public BigDecimal getCreditLimit()
    {
        return creditLimit;
    }

                        
    /**
     * Set the value of CreditLimit
     *
     * @param v new value
     */
    public void setCreditLimit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.creditLimit, v))
              {
            this.creditLimit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Memo
     *
     * @return String
     */
    public String getMemo()
    {
        return memo;
    }

                        
    /**
     * Set the value of Memo
     *
     * @param v new value
     */
    public void setMemo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memo, v))
              {
            this.memo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceMessage
     *
     * @return String
     */
    public String getInvoiceMessage()
    {
        return invoiceMessage;
    }

                        
    /**
     * Set the value of InvoiceMessage
     *
     * @param v new value
     */
    public void setInvoiceMessage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceMessage, v))
              {
            this.invoiceMessage = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AddDate
     *
     * @return Date
     */
    public Date getAddDate()
    {
        return addDate;
    }

                        
    /**
     * Set the value of AddDate
     *
     * @param v new value
     */
    public void setAddDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.addDate, v))
              {
            this.addDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateLocationId
     *
     * @return String
     */
    public String getLastUpdateLocationId()
    {
        return lastUpdateLocationId;
    }

                        
    /**
     * Set the value of LastUpdateLocationId
     *
     * @param v new value
     */
    public void setLastUpdateLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateLocationId, v))
              {
            this.lastUpdateLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDefault
     *
     * @return boolean
     */
    public boolean getIsDefault()
    {
        return isDefault;
    }

                        
    /**
     * Set the value of IsDefault
     *
     * @param v new value
     */
    public void setIsDefault(boolean v) 
    {
    
                  if (this.isDefault != v)
              {
            this.isDefault = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsActive
     *
     * @return boolean
     */
    public boolean getIsActive()
    {
        return isActive;
    }

                        
    /**
     * Set the value of IsActive
     *
     * @param v new value
     */
    public void setIsActive(boolean v) 
    {
    
                  if (this.isActive != v)
              {
            this.isActive = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field1
     *
     * @return String
     */
    public String getField1()
    {
        return field1;
    }

                        
    /**
     * Set the value of Field1
     *
     * @param v new value
     */
    public void setField1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field1, v))
              {
            this.field1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field2
     *
     * @return String
     */
    public String getField2()
    {
        return field2;
    }

                        
    /**
     * Set the value of Field2
     *
     * @param v new value
     */
    public void setField2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field2, v))
              {
            this.field2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value1
     *
     * @return String
     */
    public String getValue1()
    {
        return value1;
    }

                        
    /**
     * Set the value of Value1
     *
     * @param v new value
     */
    public void setValue1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value1, v))
              {
            this.value1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value2
     *
     * @return String
     */
    public String getValue2()
    {
        return value2;
    }

                        
    /**
     * Set the value of Value2
     *
     * @param v new value
     */
    public void setValue2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value2, v))
              {
            this.value2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ArAccount
     *
     * @return String
     */
    public String getArAccount()
    {
        return arAccount;
    }

                        
    /**
     * Set the value of ArAccount
     *
     * @param v new value
     */
    public void setArAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.arAccount, v))
              {
            this.arAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AsDate
     *
     * @return Date
     */
    public Date getAsDate()
    {
        return asDate;
    }

                        
    /**
     * Set the value of AsDate
     *
     * @param v new value
     */
    public void setAsDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.asDate, v))
              {
            this.asDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObTransId
     *
     * @return String
     */
    public String getObTransId()
    {
        return obTransId;
    }

                        
    /**
     * Set the value of ObTransId
     *
     * @param v new value
     */
    public void setObTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.obTransId, v))
              {
            this.obTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObRate
     *
     * @return BigDecimal
     */
    public BigDecimal getObRate()
    {
        return obRate;
    }

                        
    /**
     * Set the value of ObRate
     *
     * @param v new value
     */
    public void setObRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.obRate, v))
              {
            this.obRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BirthDate
     *
     * @return Date
     */
    public Date getBirthDate()
    {
        return birthDate;
    }

                        
    /**
     * Set the value of BirthDate
     *
     * @param v new value
     */
    public void setBirthDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.birthDate, v))
              {
            this.birthDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BirthPlace
     *
     * @return String
     */
    public String getBirthPlace()
    {
        return birthPlace;
    }

                        
    /**
     * Set the value of BirthPlace
     *
     * @param v new value
     */
    public void setBirthPlace(String v) 
    {
    
                  if (!ObjectUtils.equals(this.birthPlace, v))
              {
            this.birthPlace = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Religion
     *
     * @return String
     */
    public String getReligion()
    {
        return religion;
    }

                        
    /**
     * Set the value of Religion
     *
     * @param v new value
     */
    public void setReligion(String v) 
    {
    
                  if (!ObjectUtils.equals(this.religion, v))
              {
            this.religion = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Gender
     *
     * @return int
     */
    public int getGender()
    {
        return gender;
    }

                        
    /**
     * Set the value of Gender
     *
     * @param v new value
     */
    public void setGender(int v) 
    {
    
                  if (this.gender != v)
              {
            this.gender = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Occupation
     *
     * @return String
     */
    public String getOccupation()
    {
        return occupation;
    }

                        
    /**
     * Set the value of Occupation
     *
     * @param v new value
     */
    public void setOccupation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.occupation, v))
              {
            this.occupation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Hobby
     *
     * @return String
     */
    public String getHobby()
    {
        return hobby;
    }

                        
    /**
     * Set the value of Hobby
     *
     * @param v new value
     */
    public void setHobby(String v) 
    {
    
                  if (!ObjectUtils.equals(this.hobby, v))
              {
            this.hobby = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransPrefix
     *
     * @return String
     */
    public String getTransPrefix()
    {
        return transPrefix;
    }

                        
    /**
     * Set the value of TransPrefix
     *
     * @param v new value
     */
    public void setTransPrefix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transPrefix, v))
              {
            this.transPrefix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateBy
     *
     * @return String
     */
    public String getLastUpdateBy()
    {
        return lastUpdateBy;
    }

                        
    /**
     * Set the value of LastUpdateBy
     *
     * @param v new value
     */
    public void setLastUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateBy, v))
              {
            this.lastUpdateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAreaId
     *
     * @return String
     */
    public String getSalesAreaId()
    {
        return salesAreaId;
    }

                        
    /**
     * Set the value of SalesAreaId
     *
     * @param v new value
     */
    public void setSalesAreaId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAreaId, v))
              {
            this.salesAreaId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TerritoryId
     *
     * @return String
     */
    public String getTerritoryId()
    {
        return territoryId;
    }

                        
    /**
     * Set the value of TerritoryId
     *
     * @param v new value
     */
    public void setTerritoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territoryId, v))
              {
            this.territoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Longitudes
     *
     * @return BigDecimal
     */
    public BigDecimal getLongitudes()
    {
        return longitudes;
    }

                        
    /**
     * Set the value of Longitudes
     *
     * @param v new value
     */
    public void setLongitudes(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.longitudes, v))
              {
            this.longitudes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Latitudes
     *
     * @return BigDecimal
     */
    public BigDecimal getLatitudes()
    {
        return latitudes;
    }

                        
    /**
     * Set the value of Latitudes
     *
     * @param v new value
     */
    public void setLatitudes(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.latitudes, v))
              {
            this.latitudes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BillToId
     *
     * @return String
     */
    public String getBillToId()
    {
        return billToId;
    }

                        
    /**
     * Set the value of BillToId
     *
     * @param v new value
     */
    public void setBillToId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.billToId, v))
              {
            this.billToId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ParentId
     *
     * @return String
     */
    public String getParentId()
    {
        return parentId;
    }

                        
    /**
     * Set the value of ParentId
     *
     * @param v new value
     */
    public void setParentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.parentId, v))
              {
            this.parentId = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collCustomerContacts
     */
    protected List collCustomerContacts;

    /**
     * Temporary storage of collCustomerContacts to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCustomerContacts()
    {
        if (collCustomerContacts == null)
        {
            collCustomerContacts = new ArrayList();
        }
    }

    /**
     * Method called to associate a CustomerContact object to this object
     * through the CustomerContact foreign key attribute
     *
     * @param l CustomerContact
     * @throws TorqueException
     */
    public void addCustomerContact(CustomerContact l) throws TorqueException
    {
        getCustomerContacts().add(l);
        l.setCustomer((Customer) this);
    }

    /**
     * The criteria used to select the current contents of collCustomerContacts
     */
    private Criteria lastCustomerContactsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCustomerContacts(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCustomerContacts() throws TorqueException
    {
              if (collCustomerContacts == null)
        {
            collCustomerContacts = getCustomerContacts(new Criteria(10));
        }
        return collCustomerContacts;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer has previously
     * been saved, it will retrieve related CustomerContacts from storage.
     * If this Customer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCustomerContacts(Criteria criteria) throws TorqueException
    {
              if (collCustomerContacts == null)
        {
            if (isNew())
            {
               collCustomerContacts = new ArrayList();
            }
            else
            {
                        criteria.add(CustomerContactPeer.CUSTOMER_ID, getCustomerId() );
                        collCustomerContacts = CustomerContactPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CustomerContactPeer.CUSTOMER_ID, getCustomerId());
                            if (!lastCustomerContactsCriteria.equals(criteria))
                {
                    collCustomerContacts = CustomerContactPeer.doSelect(criteria);
                }
            }
        }
        lastCustomerContactsCriteria = criteria;

        return collCustomerContacts;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCustomerContacts(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCustomerContacts(Connection con) throws TorqueException
    {
              if (collCustomerContacts == null)
        {
            collCustomerContacts = getCustomerContacts(new Criteria(10), con);
        }
        return collCustomerContacts;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer has previously
     * been saved, it will retrieve related CustomerContacts from storage.
     * If this Customer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCustomerContacts(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCustomerContacts == null)
        {
            if (isNew())
            {
               collCustomerContacts = new ArrayList();
            }
            else
            {
                         criteria.add(CustomerContactPeer.CUSTOMER_ID, getCustomerId());
                         collCustomerContacts = CustomerContactPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CustomerContactPeer.CUSTOMER_ID, getCustomerId());
                             if (!lastCustomerContactsCriteria.equals(criteria))
                 {
                     collCustomerContacts = CustomerContactPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCustomerContactsCriteria = criteria;

         return collCustomerContacts;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related CustomerContacts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     */
    protected List getCustomerContactsJoinCustomer(Criteria criteria)
        throws TorqueException
    {
                    if (collCustomerContacts == null)
        {
            if (isNew())
            {
               collCustomerContacts = new ArrayList();
            }
            else
            {
                              criteria.add(CustomerContactPeer.CUSTOMER_ID, getCustomerId());
                              collCustomerContacts = CustomerContactPeer.doSelectJoinCustomer(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CustomerContactPeer.CUSTOMER_ID, getCustomerId());
                                    if (!lastCustomerContactsCriteria.equals(criteria))
            {
                collCustomerContacts = CustomerContactPeer.doSelectJoinCustomer(criteria);
            }
        }
        lastCustomerContactsCriteria = criteria;

        return collCustomerContacts;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collCustomerAddresss
     */
    protected List collCustomerAddresss;

    /**
     * Temporary storage of collCustomerAddresss to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCustomerAddresss()
    {
        if (collCustomerAddresss == null)
        {
            collCustomerAddresss = new ArrayList();
        }
    }

    /**
     * Method called to associate a CustomerAddress object to this object
     * through the CustomerAddress foreign key attribute
     *
     * @param l CustomerAddress
     * @throws TorqueException
     */
    public void addCustomerAddress(CustomerAddress l) throws TorqueException
    {
        getCustomerAddresss().add(l);
        l.setCustomer((Customer) this);
    }

    /**
     * The criteria used to select the current contents of collCustomerAddresss
     */
    private Criteria lastCustomerAddresssCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCustomerAddresss(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCustomerAddresss() throws TorqueException
    {
              if (collCustomerAddresss == null)
        {
            collCustomerAddresss = getCustomerAddresss(new Criteria(10));
        }
        return collCustomerAddresss;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer has previously
     * been saved, it will retrieve related CustomerAddresss from storage.
     * If this Customer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCustomerAddresss(Criteria criteria) throws TorqueException
    {
              if (collCustomerAddresss == null)
        {
            if (isNew())
            {
               collCustomerAddresss = new ArrayList();
            }
            else
            {
                        criteria.add(CustomerAddressPeer.CUSTOMER_ID, getCustomerId() );
                        collCustomerAddresss = CustomerAddressPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CustomerAddressPeer.CUSTOMER_ID, getCustomerId());
                            if (!lastCustomerAddresssCriteria.equals(criteria))
                {
                    collCustomerAddresss = CustomerAddressPeer.doSelect(criteria);
                }
            }
        }
        lastCustomerAddresssCriteria = criteria;

        return collCustomerAddresss;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCustomerAddresss(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCustomerAddresss(Connection con) throws TorqueException
    {
              if (collCustomerAddresss == null)
        {
            collCustomerAddresss = getCustomerAddresss(new Criteria(10), con);
        }
        return collCustomerAddresss;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer has previously
     * been saved, it will retrieve related CustomerAddresss from storage.
     * If this Customer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCustomerAddresss(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCustomerAddresss == null)
        {
            if (isNew())
            {
               collCustomerAddresss = new ArrayList();
            }
            else
            {
                         criteria.add(CustomerAddressPeer.CUSTOMER_ID, getCustomerId());
                         collCustomerAddresss = CustomerAddressPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CustomerAddressPeer.CUSTOMER_ID, getCustomerId());
                             if (!lastCustomerAddresssCriteria.equals(criteria))
                 {
                     collCustomerAddresss = CustomerAddressPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCustomerAddresssCriteria = criteria;

         return collCustomerAddresss;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related CustomerAddresss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     */
    protected List getCustomerAddresssJoinCustomer(Criteria criteria)
        throws TorqueException
    {
                    if (collCustomerAddresss == null)
        {
            if (isNew())
            {
               collCustomerAddresss = new ArrayList();
            }
            else
            {
                              criteria.add(CustomerAddressPeer.CUSTOMER_ID, getCustomerId());
                              collCustomerAddresss = CustomerAddressPeer.doSelectJoinCustomer(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CustomerAddressPeer.CUSTOMER_ID, getCustomerId());
                                    if (!lastCustomerAddresssCriteria.equals(criteria))
            {
                collCustomerAddresss = CustomerAddressPeer.doSelectJoinCustomer(criteria);
            }
        }
        lastCustomerAddresssCriteria = criteria;

        return collCustomerAddresss;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collCustomerFollowups
     */
    protected List collCustomerFollowups;

    /**
     * Temporary storage of collCustomerFollowups to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCustomerFollowups()
    {
        if (collCustomerFollowups == null)
        {
            collCustomerFollowups = new ArrayList();
        }
    }

    /**
     * Method called to associate a CustomerFollowup object to this object
     * through the CustomerFollowup foreign key attribute
     *
     * @param l CustomerFollowup
     * @throws TorqueException
     */
    public void addCustomerFollowup(CustomerFollowup l) throws TorqueException
    {
        getCustomerFollowups().add(l);
        l.setCustomer((Customer) this);
    }

    /**
     * The criteria used to select the current contents of collCustomerFollowups
     */
    private Criteria lastCustomerFollowupsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCustomerFollowups(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCustomerFollowups() throws TorqueException
    {
              if (collCustomerFollowups == null)
        {
            collCustomerFollowups = getCustomerFollowups(new Criteria(10));
        }
        return collCustomerFollowups;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer has previously
     * been saved, it will retrieve related CustomerFollowups from storage.
     * If this Customer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCustomerFollowups(Criteria criteria) throws TorqueException
    {
              if (collCustomerFollowups == null)
        {
            if (isNew())
            {
               collCustomerFollowups = new ArrayList();
            }
            else
            {
                        criteria.add(CustomerFollowupPeer.CUSTOMER_ID, getCustomerId() );
                        collCustomerFollowups = CustomerFollowupPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CustomerFollowupPeer.CUSTOMER_ID, getCustomerId());
                            if (!lastCustomerFollowupsCriteria.equals(criteria))
                {
                    collCustomerFollowups = CustomerFollowupPeer.doSelect(criteria);
                }
            }
        }
        lastCustomerFollowupsCriteria = criteria;

        return collCustomerFollowups;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCustomerFollowups(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCustomerFollowups(Connection con) throws TorqueException
    {
              if (collCustomerFollowups == null)
        {
            collCustomerFollowups = getCustomerFollowups(new Criteria(10), con);
        }
        return collCustomerFollowups;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer has previously
     * been saved, it will retrieve related CustomerFollowups from storage.
     * If this Customer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCustomerFollowups(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCustomerFollowups == null)
        {
            if (isNew())
            {
               collCustomerFollowups = new ArrayList();
            }
            else
            {
                         criteria.add(CustomerFollowupPeer.CUSTOMER_ID, getCustomerId());
                         collCustomerFollowups = CustomerFollowupPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CustomerFollowupPeer.CUSTOMER_ID, getCustomerId());
                             if (!lastCustomerFollowupsCriteria.equals(criteria))
                 {
                     collCustomerFollowups = CustomerFollowupPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCustomerFollowupsCriteria = criteria;

         return collCustomerFollowups;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related CustomerFollowups from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     */
    protected List getCustomerFollowupsJoinCustomer(Criteria criteria)
        throws TorqueException
    {
                    if (collCustomerFollowups == null)
        {
            if (isNew())
            {
               collCustomerFollowups = new ArrayList();
            }
            else
            {
                              criteria.add(CustomerFollowupPeer.CUSTOMER_ID, getCustomerId());
                              collCustomerFollowups = CustomerFollowupPeer.doSelectJoinCustomer(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CustomerFollowupPeer.CUSTOMER_ID, getCustomerId());
                                    if (!lastCustomerFollowupsCriteria.equals(criteria))
            {
                collCustomerFollowups = CustomerFollowupPeer.doSelectJoinCustomer(criteria);
            }
        }
        lastCustomerFollowupsCriteria = criteria;

        return collCustomerFollowups;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerCode");
              fieldNames.add("CustomerName");
              fieldNames.add("CustomerTypeId");
              fieldNames.add("StatusId");
              fieldNames.add("InvoiceAddress");
              fieldNames.add("TaxNo");
              fieldNames.add("ContactName1");
              fieldNames.add("ContactName2");
              fieldNames.add("ContactName3");
              fieldNames.add("JobTitle1");
              fieldNames.add("JobTitle2");
              fieldNames.add("JobTitle3");
              fieldNames.add("ContactPhone1");
              fieldNames.add("ContactPhone2");
              fieldNames.add("ContactPhone3");
              fieldNames.add("Address");
              fieldNames.add("ShipTo");
              fieldNames.add("Zip");
              fieldNames.add("Country");
              fieldNames.add("Province");
              fieldNames.add("City");
              fieldNames.add("District");
              fieldNames.add("Village");
              fieldNames.add("Phone1");
              fieldNames.add("Phone2");
              fieldNames.add("Fax");
              fieldNames.add("Email");
              fieldNames.add("WebSite");
              fieldNames.add("DefaultTaxId");
              fieldNames.add("DefaultTypeId");
              fieldNames.add("DefaultTermId");
              fieldNames.add("DefaultEmployeeId");
              fieldNames.add("DefaultCurrencyId");
              fieldNames.add("DefaultLocationId");
              fieldNames.add("DefaultDiscountAmount");
              fieldNames.add("CreditLimit");
              fieldNames.add("Memo");
              fieldNames.add("InvoiceMessage");
              fieldNames.add("AddDate");
              fieldNames.add("UpdateDate");
              fieldNames.add("LastUpdateLocationId");
              fieldNames.add("IsDefault");
              fieldNames.add("IsActive");
              fieldNames.add("Field1");
              fieldNames.add("Field2");
              fieldNames.add("Value1");
              fieldNames.add("Value2");
              fieldNames.add("ArAccount");
              fieldNames.add("OpeningBalance");
              fieldNames.add("AsDate");
              fieldNames.add("ObTransId");
              fieldNames.add("ObRate");
              fieldNames.add("BirthDate");
              fieldNames.add("BirthPlace");
              fieldNames.add("Religion");
              fieldNames.add("Gender");
              fieldNames.add("Occupation");
              fieldNames.add("Hobby");
              fieldNames.add("TransPrefix");
              fieldNames.add("CreateBy");
              fieldNames.add("LastUpdateBy");
              fieldNames.add("SalesAreaId");
              fieldNames.add("TerritoryId");
              fieldNames.add("Longitudes");
              fieldNames.add("Latitudes");
              fieldNames.add("BillToId");
              fieldNames.add("ParentId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerCode"))
        {
                return getCustomerCode();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          if (name.equals("StatusId"))
        {
                return getStatusId();
            }
          if (name.equals("InvoiceAddress"))
        {
                return getInvoiceAddress();
            }
          if (name.equals("TaxNo"))
        {
                return getTaxNo();
            }
          if (name.equals("ContactName1"))
        {
                return getContactName1();
            }
          if (name.equals("ContactName2"))
        {
                return getContactName2();
            }
          if (name.equals("ContactName3"))
        {
                return getContactName3();
            }
          if (name.equals("JobTitle1"))
        {
                return getJobTitle1();
            }
          if (name.equals("JobTitle2"))
        {
                return getJobTitle2();
            }
          if (name.equals("JobTitle3"))
        {
                return getJobTitle3();
            }
          if (name.equals("ContactPhone1"))
        {
                return getContactPhone1();
            }
          if (name.equals("ContactPhone2"))
        {
                return getContactPhone2();
            }
          if (name.equals("ContactPhone3"))
        {
                return getContactPhone3();
            }
          if (name.equals("Address"))
        {
                return getAddress();
            }
          if (name.equals("ShipTo"))
        {
                return getShipTo();
            }
          if (name.equals("Zip"))
        {
                return getZip();
            }
          if (name.equals("Country"))
        {
                return getCountry();
            }
          if (name.equals("Province"))
        {
                return getProvince();
            }
          if (name.equals("City"))
        {
                return getCity();
            }
          if (name.equals("District"))
        {
                return getDistrict();
            }
          if (name.equals("Village"))
        {
                return getVillage();
            }
          if (name.equals("Phone1"))
        {
                return getPhone1();
            }
          if (name.equals("Phone2"))
        {
                return getPhone2();
            }
          if (name.equals("Fax"))
        {
                return getFax();
            }
          if (name.equals("Email"))
        {
                return getEmail();
            }
          if (name.equals("WebSite"))
        {
                return getWebSite();
            }
          if (name.equals("DefaultTaxId"))
        {
                return getDefaultTaxId();
            }
          if (name.equals("DefaultTypeId"))
        {
                return getDefaultTypeId();
            }
          if (name.equals("DefaultTermId"))
        {
                return getDefaultTermId();
            }
          if (name.equals("DefaultEmployeeId"))
        {
                return getDefaultEmployeeId();
            }
          if (name.equals("DefaultCurrencyId"))
        {
                return getDefaultCurrencyId();
            }
          if (name.equals("DefaultLocationId"))
        {
                return getDefaultLocationId();
            }
          if (name.equals("DefaultDiscountAmount"))
        {
                return getDefaultDiscountAmount();
            }
          if (name.equals("CreditLimit"))
        {
                return getCreditLimit();
            }
          if (name.equals("Memo"))
        {
                return getMemo();
            }
          if (name.equals("InvoiceMessage"))
        {
                return getInvoiceMessage();
            }
          if (name.equals("AddDate"))
        {
                return getAddDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LastUpdateLocationId"))
        {
                return getLastUpdateLocationId();
            }
          if (name.equals("IsDefault"))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals("IsActive"))
        {
                return Boolean.valueOf(getIsActive());
            }
          if (name.equals("Field1"))
        {
                return getField1();
            }
          if (name.equals("Field2"))
        {
                return getField2();
            }
          if (name.equals("Value1"))
        {
                return getValue1();
            }
          if (name.equals("Value2"))
        {
                return getValue2();
            }
          if (name.equals("ArAccount"))
        {
                return getArAccount();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("AsDate"))
        {
                return getAsDate();
            }
          if (name.equals("ObTransId"))
        {
                return getObTransId();
            }
          if (name.equals("ObRate"))
        {
                return getObRate();
            }
          if (name.equals("BirthDate"))
        {
                return getBirthDate();
            }
          if (name.equals("BirthPlace"))
        {
                return getBirthPlace();
            }
          if (name.equals("Religion"))
        {
                return getReligion();
            }
          if (name.equals("Gender"))
        {
                return Integer.valueOf(getGender());
            }
          if (name.equals("Occupation"))
        {
                return getOccupation();
            }
          if (name.equals("Hobby"))
        {
                return getHobby();
            }
          if (name.equals("TransPrefix"))
        {
                return getTransPrefix();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("LastUpdateBy"))
        {
                return getLastUpdateBy();
            }
          if (name.equals("SalesAreaId"))
        {
                return getSalesAreaId();
            }
          if (name.equals("TerritoryId"))
        {
                return getTerritoryId();
            }
          if (name.equals("Longitudes"))
        {
                return getLongitudes();
            }
          if (name.equals("Latitudes"))
        {
                return getLatitudes();
            }
          if (name.equals("BillToId"))
        {
                return getBillToId();
            }
          if (name.equals("ParentId"))
        {
                return getParentId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerPeer.CUSTOMER_CODE))
        {
                return getCustomerCode();
            }
          if (name.equals(CustomerPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(CustomerPeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          if (name.equals(CustomerPeer.STATUS_ID))
        {
                return getStatusId();
            }
          if (name.equals(CustomerPeer.INVOICE_ADDRESS))
        {
                return getInvoiceAddress();
            }
          if (name.equals(CustomerPeer.TAX_NO))
        {
                return getTaxNo();
            }
          if (name.equals(CustomerPeer.CONTACT_NAME1))
        {
                return getContactName1();
            }
          if (name.equals(CustomerPeer.CONTACT_NAME2))
        {
                return getContactName2();
            }
          if (name.equals(CustomerPeer.CONTACT_NAME3))
        {
                return getContactName3();
            }
          if (name.equals(CustomerPeer.JOB_TITLE1))
        {
                return getJobTitle1();
            }
          if (name.equals(CustomerPeer.JOB_TITLE2))
        {
                return getJobTitle2();
            }
          if (name.equals(CustomerPeer.JOB_TITLE3))
        {
                return getJobTitle3();
            }
          if (name.equals(CustomerPeer.CONTACT_PHONE1))
        {
                return getContactPhone1();
            }
          if (name.equals(CustomerPeer.CONTACT_PHONE2))
        {
                return getContactPhone2();
            }
          if (name.equals(CustomerPeer.CONTACT_PHONE3))
        {
                return getContactPhone3();
            }
          if (name.equals(CustomerPeer.ADDRESS))
        {
                return getAddress();
            }
          if (name.equals(CustomerPeer.SHIP_TO))
        {
                return getShipTo();
            }
          if (name.equals(CustomerPeer.ZIP))
        {
                return getZip();
            }
          if (name.equals(CustomerPeer.COUNTRY))
        {
                return getCountry();
            }
          if (name.equals(CustomerPeer.PROVINCE))
        {
                return getProvince();
            }
          if (name.equals(CustomerPeer.CITY))
        {
                return getCity();
            }
          if (name.equals(CustomerPeer.DISTRICT))
        {
                return getDistrict();
            }
          if (name.equals(CustomerPeer.VILLAGE))
        {
                return getVillage();
            }
          if (name.equals(CustomerPeer.PHONE1))
        {
                return getPhone1();
            }
          if (name.equals(CustomerPeer.PHONE2))
        {
                return getPhone2();
            }
          if (name.equals(CustomerPeer.FAX))
        {
                return getFax();
            }
          if (name.equals(CustomerPeer.EMAIL))
        {
                return getEmail();
            }
          if (name.equals(CustomerPeer.WEB_SITE))
        {
                return getWebSite();
            }
          if (name.equals(CustomerPeer.DEFAULT_TAX_ID))
        {
                return getDefaultTaxId();
            }
          if (name.equals(CustomerPeer.DEFAULT_TYPE_ID))
        {
                return getDefaultTypeId();
            }
          if (name.equals(CustomerPeer.DEFAULT_TERM_ID))
        {
                return getDefaultTermId();
            }
          if (name.equals(CustomerPeer.DEFAULT_EMPLOYEE_ID))
        {
                return getDefaultEmployeeId();
            }
          if (name.equals(CustomerPeer.DEFAULT_CURRENCY_ID))
        {
                return getDefaultCurrencyId();
            }
          if (name.equals(CustomerPeer.DEFAULT_LOCATION_ID))
        {
                return getDefaultLocationId();
            }
          if (name.equals(CustomerPeer.DEFAULT_DISCOUNT_AMOUNT))
        {
                return getDefaultDiscountAmount();
            }
          if (name.equals(CustomerPeer.CREDIT_LIMIT))
        {
                return getCreditLimit();
            }
          if (name.equals(CustomerPeer.MEMO))
        {
                return getMemo();
            }
          if (name.equals(CustomerPeer.INVOICE_MESSAGE))
        {
                return getInvoiceMessage();
            }
          if (name.equals(CustomerPeer.ADD_DATE))
        {
                return getAddDate();
            }
          if (name.equals(CustomerPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(CustomerPeer.LAST_UPDATE_LOCATION_ID))
        {
                return getLastUpdateLocationId();
            }
          if (name.equals(CustomerPeer.IS_DEFAULT))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals(CustomerPeer.IS_ACTIVE))
        {
                return Boolean.valueOf(getIsActive());
            }
          if (name.equals(CustomerPeer.FIELD1))
        {
                return getField1();
            }
          if (name.equals(CustomerPeer.FIELD2))
        {
                return getField2();
            }
          if (name.equals(CustomerPeer.VALUE1))
        {
                return getValue1();
            }
          if (name.equals(CustomerPeer.VALUE2))
        {
                return getValue2();
            }
          if (name.equals(CustomerPeer.AR_ACCOUNT))
        {
                return getArAccount();
            }
          if (name.equals(CustomerPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(CustomerPeer.AS_DATE))
        {
                return getAsDate();
            }
          if (name.equals(CustomerPeer.OB_TRANS_ID))
        {
                return getObTransId();
            }
          if (name.equals(CustomerPeer.OB_RATE))
        {
                return getObRate();
            }
          if (name.equals(CustomerPeer.BIRTH_DATE))
        {
                return getBirthDate();
            }
          if (name.equals(CustomerPeer.BIRTH_PLACE))
        {
                return getBirthPlace();
            }
          if (name.equals(CustomerPeer.RELIGION))
        {
                return getReligion();
            }
          if (name.equals(CustomerPeer.GENDER))
        {
                return Integer.valueOf(getGender());
            }
          if (name.equals(CustomerPeer.OCCUPATION))
        {
                return getOccupation();
            }
          if (name.equals(CustomerPeer.HOBBY))
        {
                return getHobby();
            }
          if (name.equals(CustomerPeer.TRANS_PREFIX))
        {
                return getTransPrefix();
            }
          if (name.equals(CustomerPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(CustomerPeer.LAST_UPDATE_BY))
        {
                return getLastUpdateBy();
            }
          if (name.equals(CustomerPeer.SALES_AREA_ID))
        {
                return getSalesAreaId();
            }
          if (name.equals(CustomerPeer.TERRITORY_ID))
        {
                return getTerritoryId();
            }
          if (name.equals(CustomerPeer.LONGITUDES))
        {
                return getLongitudes();
            }
          if (name.equals(CustomerPeer.LATITUDES))
        {
                return getLatitudes();
            }
          if (name.equals(CustomerPeer.BILL_TO_ID))
        {
                return getBillToId();
            }
          if (name.equals(CustomerPeer.PARENT_ID))
        {
                return getParentId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerId();
            }
              if (pos == 1)
        {
                return getCustomerCode();
            }
              if (pos == 2)
        {
                return getCustomerName();
            }
              if (pos == 3)
        {
                return getCustomerTypeId();
            }
              if (pos == 4)
        {
                return getStatusId();
            }
              if (pos == 5)
        {
                return getInvoiceAddress();
            }
              if (pos == 6)
        {
                return getTaxNo();
            }
              if (pos == 7)
        {
                return getContactName1();
            }
              if (pos == 8)
        {
                return getContactName2();
            }
              if (pos == 9)
        {
                return getContactName3();
            }
              if (pos == 10)
        {
                return getJobTitle1();
            }
              if (pos == 11)
        {
                return getJobTitle2();
            }
              if (pos == 12)
        {
                return getJobTitle3();
            }
              if (pos == 13)
        {
                return getContactPhone1();
            }
              if (pos == 14)
        {
                return getContactPhone2();
            }
              if (pos == 15)
        {
                return getContactPhone3();
            }
              if (pos == 16)
        {
                return getAddress();
            }
              if (pos == 17)
        {
                return getShipTo();
            }
              if (pos == 18)
        {
                return getZip();
            }
              if (pos == 19)
        {
                return getCountry();
            }
              if (pos == 20)
        {
                return getProvince();
            }
              if (pos == 21)
        {
                return getCity();
            }
              if (pos == 22)
        {
                return getDistrict();
            }
              if (pos == 23)
        {
                return getVillage();
            }
              if (pos == 24)
        {
                return getPhone1();
            }
              if (pos == 25)
        {
                return getPhone2();
            }
              if (pos == 26)
        {
                return getFax();
            }
              if (pos == 27)
        {
                return getEmail();
            }
              if (pos == 28)
        {
                return getWebSite();
            }
              if (pos == 29)
        {
                return getDefaultTaxId();
            }
              if (pos == 30)
        {
                return getDefaultTypeId();
            }
              if (pos == 31)
        {
                return getDefaultTermId();
            }
              if (pos == 32)
        {
                return getDefaultEmployeeId();
            }
              if (pos == 33)
        {
                return getDefaultCurrencyId();
            }
              if (pos == 34)
        {
                return getDefaultLocationId();
            }
              if (pos == 35)
        {
                return getDefaultDiscountAmount();
            }
              if (pos == 36)
        {
                return getCreditLimit();
            }
              if (pos == 37)
        {
                return getMemo();
            }
              if (pos == 38)
        {
                return getInvoiceMessage();
            }
              if (pos == 39)
        {
                return getAddDate();
            }
              if (pos == 40)
        {
                return getUpdateDate();
            }
              if (pos == 41)
        {
                return getLastUpdateLocationId();
            }
              if (pos == 42)
        {
                return Boolean.valueOf(getIsDefault());
            }
              if (pos == 43)
        {
                return Boolean.valueOf(getIsActive());
            }
              if (pos == 44)
        {
                return getField1();
            }
              if (pos == 45)
        {
                return getField2();
            }
              if (pos == 46)
        {
                return getValue1();
            }
              if (pos == 47)
        {
                return getValue2();
            }
              if (pos == 48)
        {
                return getArAccount();
            }
              if (pos == 49)
        {
                return getOpeningBalance();
            }
              if (pos == 50)
        {
                return getAsDate();
            }
              if (pos == 51)
        {
                return getObTransId();
            }
              if (pos == 52)
        {
                return getObRate();
            }
              if (pos == 53)
        {
                return getBirthDate();
            }
              if (pos == 54)
        {
                return getBirthPlace();
            }
              if (pos == 55)
        {
                return getReligion();
            }
              if (pos == 56)
        {
                return Integer.valueOf(getGender());
            }
              if (pos == 57)
        {
                return getOccupation();
            }
              if (pos == 58)
        {
                return getHobby();
            }
              if (pos == 59)
        {
                return getTransPrefix();
            }
              if (pos == 60)
        {
                return getCreateBy();
            }
              if (pos == 61)
        {
                return getLastUpdateBy();
            }
              if (pos == 62)
        {
                return getSalesAreaId();
            }
              if (pos == 63)
        {
                return getTerritoryId();
            }
              if (pos == 64)
        {
                return getLongitudes();
            }
              if (pos == 65)
        {
                return getLatitudes();
            }
              if (pos == 66)
        {
                return getBillToId();
            }
              if (pos == 67)
        {
                return getParentId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerPeer.doInsert((Customer) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerPeer.doUpdate((Customer) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collCustomerContacts != null)
            {
                for (int i = 0; i < collCustomerContacts.size(); i++)
                {
                    ((CustomerContact) collCustomerContacts.get(i)).save(con);
                }
            }
                                                  
                
                    if (collCustomerAddresss != null)
            {
                for (int i = 0; i < collCustomerAddresss.size(); i++)
                {
                    ((CustomerAddress) collCustomerAddresss.get(i)).save(con);
                }
            }
                                                  
                
                    if (collCustomerFollowups != null)
            {
                for (int i = 0; i < collCustomerFollowups.size(); i++)
                {
                    ((CustomerFollowup) collCustomerFollowups.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setCustomerId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setCustomerId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Customer copy() throws TorqueException
    {
        return copyInto(new Customer());
    }
  
    protected Customer copyInto(Customer copyObj) throws TorqueException
    {
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerCode(customerCode);
          copyObj.setCustomerName(customerName);
          copyObj.setCustomerTypeId(customerTypeId);
          copyObj.setStatusId(statusId);
          copyObj.setInvoiceAddress(invoiceAddress);
          copyObj.setTaxNo(taxNo);
          copyObj.setContactName1(contactName1);
          copyObj.setContactName2(contactName2);
          copyObj.setContactName3(contactName3);
          copyObj.setJobTitle1(jobTitle1);
          copyObj.setJobTitle2(jobTitle2);
          copyObj.setJobTitle3(jobTitle3);
          copyObj.setContactPhone1(contactPhone1);
          copyObj.setContactPhone2(contactPhone2);
          copyObj.setContactPhone3(contactPhone3);
          copyObj.setAddress(address);
          copyObj.setShipTo(shipTo);
          copyObj.setZip(zip);
          copyObj.setCountry(country);
          copyObj.setProvince(province);
          copyObj.setCity(city);
          copyObj.setDistrict(district);
          copyObj.setVillage(village);
          copyObj.setPhone1(phone1);
          copyObj.setPhone2(phone2);
          copyObj.setFax(fax);
          copyObj.setEmail(email);
          copyObj.setWebSite(webSite);
          copyObj.setDefaultTaxId(defaultTaxId);
          copyObj.setDefaultTypeId(defaultTypeId);
          copyObj.setDefaultTermId(defaultTermId);
          copyObj.setDefaultEmployeeId(defaultEmployeeId);
          copyObj.setDefaultCurrencyId(defaultCurrencyId);
          copyObj.setDefaultLocationId(defaultLocationId);
          copyObj.setDefaultDiscountAmount(defaultDiscountAmount);
          copyObj.setCreditLimit(creditLimit);
          copyObj.setMemo(memo);
          copyObj.setInvoiceMessage(invoiceMessage);
          copyObj.setAddDate(addDate);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLastUpdateLocationId(lastUpdateLocationId);
          copyObj.setIsDefault(isDefault);
          copyObj.setIsActive(isActive);
          copyObj.setField1(field1);
          copyObj.setField2(field2);
          copyObj.setValue1(value1);
          copyObj.setValue2(value2);
          copyObj.setArAccount(arAccount);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setAsDate(asDate);
          copyObj.setObTransId(obTransId);
          copyObj.setObRate(obRate);
          copyObj.setBirthDate(birthDate);
          copyObj.setBirthPlace(birthPlace);
          copyObj.setReligion(religion);
          copyObj.setGender(gender);
          copyObj.setOccupation(occupation);
          copyObj.setHobby(hobby);
          copyObj.setTransPrefix(transPrefix);
          copyObj.setCreateBy(createBy);
          copyObj.setLastUpdateBy(lastUpdateBy);
          copyObj.setSalesAreaId(salesAreaId);
          copyObj.setTerritoryId(territoryId);
          copyObj.setLongitudes(longitudes);
          copyObj.setLatitudes(latitudes);
          copyObj.setBillToId(billToId);
          copyObj.setParentId(parentId);
  
                    copyObj.setCustomerId((String)null);
                                                                                                                                                                                                                                                                                                                                                                                                                              
                                      
                            
        List v = getCustomerContacts();
        for (int i = 0; i < v.size(); i++)
        {
            CustomerContact obj = (CustomerContact) v.get(i);
            copyObj.addCustomerContact(obj.copy());
        }
                                                  
                            
        v = getCustomerAddresss();
        for (int i = 0; i < v.size(); i++)
        {
            CustomerAddress obj = (CustomerAddress) v.get(i);
            copyObj.addCustomerAddress(obj.copy());
        }
                                                  
                            
        v = getCustomerFollowups();
        for (int i = 0; i < v.size(); i++)
        {
            CustomerFollowup obj = (CustomerFollowup) v.get(i);
            copyObj.addCustomerFollowup(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Customer\n");
        str.append("--------\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerCode         : ")
           .append(getCustomerCode())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
           .append("StatusId             : ")
           .append(getStatusId())
           .append("\n")
           .append("InvoiceAddress       : ")
           .append(getInvoiceAddress())
           .append("\n")
           .append("TaxNo                : ")
           .append(getTaxNo())
           .append("\n")
           .append("ContactName1         : ")
           .append(getContactName1())
           .append("\n")
           .append("ContactName2         : ")
           .append(getContactName2())
           .append("\n")
           .append("ContactName3         : ")
           .append(getContactName3())
           .append("\n")
           .append("JobTitle1            : ")
           .append(getJobTitle1())
           .append("\n")
           .append("JobTitle2            : ")
           .append(getJobTitle2())
           .append("\n")
           .append("JobTitle3            : ")
           .append(getJobTitle3())
           .append("\n")
           .append("ContactPhone1        : ")
           .append(getContactPhone1())
           .append("\n")
           .append("ContactPhone2        : ")
           .append(getContactPhone2())
           .append("\n")
           .append("ContactPhone3        : ")
           .append(getContactPhone3())
           .append("\n")
           .append("Address              : ")
           .append(getAddress())
           .append("\n")
           .append("ShipTo               : ")
           .append(getShipTo())
           .append("\n")
           .append("Zip                  : ")
           .append(getZip())
           .append("\n")
           .append("Country              : ")
           .append(getCountry())
           .append("\n")
           .append("Province             : ")
           .append(getProvince())
           .append("\n")
           .append("City                 : ")
           .append(getCity())
           .append("\n")
           .append("District             : ")
           .append(getDistrict())
           .append("\n")
           .append("Village              : ")
           .append(getVillage())
           .append("\n")
           .append("Phone1               : ")
           .append(getPhone1())
           .append("\n")
           .append("Phone2               : ")
           .append(getPhone2())
           .append("\n")
           .append("Fax                  : ")
           .append(getFax())
           .append("\n")
           .append("Email                : ")
           .append(getEmail())
           .append("\n")
           .append("WebSite              : ")
           .append(getWebSite())
           .append("\n")
           .append("DefaultTaxId         : ")
           .append(getDefaultTaxId())
           .append("\n")
           .append("DefaultTypeId        : ")
           .append(getDefaultTypeId())
           .append("\n")
           .append("DefaultTermId        : ")
           .append(getDefaultTermId())
           .append("\n")
           .append("DefaultEmployeeId    : ")
           .append(getDefaultEmployeeId())
           .append("\n")
           .append("DefaultCurrencyId    : ")
           .append(getDefaultCurrencyId())
           .append("\n")
           .append("DefaultLocationId    : ")
           .append(getDefaultLocationId())
           .append("\n")
            .append("DefaultDiscountAmount   : ")
           .append(getDefaultDiscountAmount())
           .append("\n")
           .append("CreditLimit          : ")
           .append(getCreditLimit())
           .append("\n")
           .append("Memo                 : ")
           .append(getMemo())
           .append("\n")
           .append("InvoiceMessage       : ")
           .append(getInvoiceMessage())
           .append("\n")
           .append("AddDate              : ")
           .append(getAddDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LastUpdateLocationId   : ")
           .append(getLastUpdateLocationId())
           .append("\n")
           .append("IsDefault            : ")
           .append(getIsDefault())
           .append("\n")
           .append("IsActive             : ")
           .append(getIsActive())
           .append("\n")
           .append("Field1               : ")
           .append(getField1())
           .append("\n")
           .append("Field2               : ")
           .append(getField2())
           .append("\n")
           .append("Value1               : ")
           .append(getValue1())
           .append("\n")
           .append("Value2               : ")
           .append(getValue2())
           .append("\n")
           .append("ArAccount            : ")
           .append(getArAccount())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("AsDate               : ")
           .append(getAsDate())
           .append("\n")
           .append("ObTransId            : ")
           .append(getObTransId())
           .append("\n")
           .append("ObRate               : ")
           .append(getObRate())
           .append("\n")
           .append("BirthDate            : ")
           .append(getBirthDate())
           .append("\n")
           .append("BirthPlace           : ")
           .append(getBirthPlace())
           .append("\n")
           .append("Religion             : ")
           .append(getReligion())
           .append("\n")
           .append("Gender               : ")
           .append(getGender())
           .append("\n")
           .append("Occupation           : ")
           .append(getOccupation())
           .append("\n")
           .append("Hobby                : ")
           .append(getHobby())
           .append("\n")
           .append("TransPrefix          : ")
           .append(getTransPrefix())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("LastUpdateBy         : ")
           .append(getLastUpdateBy())
           .append("\n")
           .append("SalesAreaId          : ")
           .append(getSalesAreaId())
           .append("\n")
           .append("TerritoryId          : ")
           .append(getTerritoryId())
           .append("\n")
           .append("Longitudes           : ")
           .append(getLongitudes())
           .append("\n")
           .append("Latitudes            : ")
           .append(getLatitudes())
           .append("\n")
           .append("BillToId             : ")
           .append(getBillToId())
           .append("\n")
           .append("ParentId             : ")
           .append(getParentId())
           .append("\n")
        ;
        return(str.toString());
    }
}
