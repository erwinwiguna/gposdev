package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerAddress
 */
public abstract class BaseCustomerAddress extends BaseObject
{
    /** The Peer class */
    private static final CustomerAddressPeer peer =
        new CustomerAddressPeer();

        
    /** The value for the customerAddressId field */
    private String customerAddressId;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerContactId field */
    private String customerContactId;
                                                
    /** The value for the contactName field */
    private String contactName = "";
                                                
    /** The value for the addressType field */
    private String addressType = "100";
                                                
    /** The value for the address field */
    private String address = "";
      
    /** The value for the addressRemark field */
    private String addressRemark;
                                                
    /** The value for the zip field */
    private String zip = "";
                                                
    /** The value for the country field */
    private String country = "";
                                                
    /** The value for the province field */
    private String province = "";
                                                
    /** The value for the city field */
    private String city = "";
                                                
    /** The value for the district field */
    private String district = "";
                                                
    /** The value for the village field */
    private String village = "";
                                                
    /** The value for the phone1 field */
    private String phone1 = "";
                                                
    /** The value for the phone2 field */
    private String phone2 = "";
                                                
    /** The value for the fax field */
    private String fax = "";
                                                
    /** The value for the email field */
    private String email = "";
                                                
    /** The value for the webSite field */
    private String webSite = "";
                                                
    /** The value for the field1 field */
    private String field1 = "";
                                                
    /** The value for the field2 field */
    private String field2 = "";
                                                
    /** The value for the field3 field */
    private String field3 = "";
                                                
    /** The value for the field4 field */
    private String field4 = "";
                                                
    /** The value for the value1 field */
    private String value1 = "";
                                                
    /** The value for the value2 field */
    private String value2 = "";
                                                
    /** The value for the value3 field */
    private String value3 = "";
                                                
    /** The value for the value4 field */
    private String value4 = "";
  
    
    /**
     * Get the CustomerAddressId
     *
     * @return String
     */
    public String getCustomerAddressId()
    {
        return customerAddressId;
    }

                        
    /**
     * Set the value of CustomerAddressId
     *
     * @param v new value
     */
    public void setCustomerAddressId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerAddressId, v))
              {
            this.customerAddressId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                              
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
                          
                if (aCustomer != null && !ObjectUtils.equals(aCustomer.getCustomerId(), v))
                {
            aCustomer = null;
        }
      
              }
  
    /**
     * Get the CustomerContactId
     *
     * @return String
     */
    public String getCustomerContactId()
    {
        return customerContactId;
    }

                        
    /**
     * Set the value of CustomerContactId
     *
     * @param v new value
     */
    public void setCustomerContactId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerContactId, v))
              {
            this.customerContactId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName
     *
     * @return String
     */
    public String getContactName()
    {
        return contactName;
    }

                        
    /**
     * Set the value of ContactName
     *
     * @param v new value
     */
    public void setContactName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName, v))
              {
            this.contactName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AddressType
     *
     * @return String
     */
    public String getAddressType()
    {
        return addressType;
    }

                        
    /**
     * Set the value of AddressType
     *
     * @param v new value
     */
    public void setAddressType(String v) 
    {
    
                  if (!ObjectUtils.equals(this.addressType, v))
              {
            this.addressType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

                        
    /**
     * Set the value of Address
     *
     * @param v new value
     */
    public void setAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.address, v))
              {
            this.address = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AddressRemark
     *
     * @return String
     */
    public String getAddressRemark()
    {
        return addressRemark;
    }

                        
    /**
     * Set the value of AddressRemark
     *
     * @param v new value
     */
    public void setAddressRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.addressRemark, v))
              {
            this.addressRemark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Zip
     *
     * @return String
     */
    public String getZip()
    {
        return zip;
    }

                        
    /**
     * Set the value of Zip
     *
     * @param v new value
     */
    public void setZip(String v) 
    {
    
                  if (!ObjectUtils.equals(this.zip, v))
              {
            this.zip = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Country
     *
     * @return String
     */
    public String getCountry()
    {
        return country;
    }

                        
    /**
     * Set the value of Country
     *
     * @param v new value
     */
    public void setCountry(String v) 
    {
    
                  if (!ObjectUtils.equals(this.country, v))
              {
            this.country = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Province
     *
     * @return String
     */
    public String getProvince()
    {
        return province;
    }

                        
    /**
     * Set the value of Province
     *
     * @param v new value
     */
    public void setProvince(String v) 
    {
    
                  if (!ObjectUtils.equals(this.province, v))
              {
            this.province = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the City
     *
     * @return String
     */
    public String getCity()
    {
        return city;
    }

                        
    /**
     * Set the value of City
     *
     * @param v new value
     */
    public void setCity(String v) 
    {
    
                  if (!ObjectUtils.equals(this.city, v))
              {
            this.city = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the District
     *
     * @return String
     */
    public String getDistrict()
    {
        return district;
    }

                        
    /**
     * Set the value of District
     *
     * @param v new value
     */
    public void setDistrict(String v) 
    {
    
                  if (!ObjectUtils.equals(this.district, v))
              {
            this.district = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Village
     *
     * @return String
     */
    public String getVillage()
    {
        return village;
    }

                        
    /**
     * Set the value of Village
     *
     * @param v new value
     */
    public void setVillage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.village, v))
              {
            this.village = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone1
     *
     * @return String
     */
    public String getPhone1()
    {
        return phone1;
    }

                        
    /**
     * Set the value of Phone1
     *
     * @param v new value
     */
    public void setPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone1, v))
              {
            this.phone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone2
     *
     * @return String
     */
    public String getPhone2()
    {
        return phone2;
    }

                        
    /**
     * Set the value of Phone2
     *
     * @param v new value
     */
    public void setPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone2, v))
              {
            this.phone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Fax
     *
     * @return String
     */
    public String getFax()
    {
        return fax;
    }

                        
    /**
     * Set the value of Fax
     *
     * @param v new value
     */
    public void setFax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fax, v))
              {
            this.fax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

                        
    /**
     * Set the value of Email
     *
     * @param v new value
     */
    public void setEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.email, v))
              {
            this.email = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the WebSite
     *
     * @return String
     */
    public String getWebSite()
    {
        return webSite;
    }

                        
    /**
     * Set the value of WebSite
     *
     * @param v new value
     */
    public void setWebSite(String v) 
    {
    
                  if (!ObjectUtils.equals(this.webSite, v))
              {
            this.webSite = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field1
     *
     * @return String
     */
    public String getField1()
    {
        return field1;
    }

                        
    /**
     * Set the value of Field1
     *
     * @param v new value
     */
    public void setField1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field1, v))
              {
            this.field1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field2
     *
     * @return String
     */
    public String getField2()
    {
        return field2;
    }

                        
    /**
     * Set the value of Field2
     *
     * @param v new value
     */
    public void setField2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field2, v))
              {
            this.field2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field3
     *
     * @return String
     */
    public String getField3()
    {
        return field3;
    }

                        
    /**
     * Set the value of Field3
     *
     * @param v new value
     */
    public void setField3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field3, v))
              {
            this.field3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field4
     *
     * @return String
     */
    public String getField4()
    {
        return field4;
    }

                        
    /**
     * Set the value of Field4
     *
     * @param v new value
     */
    public void setField4(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field4, v))
              {
            this.field4 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value1
     *
     * @return String
     */
    public String getValue1()
    {
        return value1;
    }

                        
    /**
     * Set the value of Value1
     *
     * @param v new value
     */
    public void setValue1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value1, v))
              {
            this.value1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value2
     *
     * @return String
     */
    public String getValue2()
    {
        return value2;
    }

                        
    /**
     * Set the value of Value2
     *
     * @param v new value
     */
    public void setValue2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value2, v))
              {
            this.value2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value3
     *
     * @return String
     */
    public String getValue3()
    {
        return value3;
    }

                        
    /**
     * Set the value of Value3
     *
     * @param v new value
     */
    public void setValue3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value3, v))
              {
            this.value3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value4
     *
     * @return String
     */
    public String getValue4()
    {
        return value4;
    }

                        
    /**
     * Set the value of Value4
     *
     * @param v new value
     */
    public void setValue4(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value4, v))
              {
            this.value4 = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Customer aCustomer;

    /**
     * Declares an association between this object and a Customer object
     *
     * @param v Customer
     * @throws TorqueException
     */
    public void setCustomer(Customer v) throws TorqueException
    {
            if (v == null)
        {
                  setCustomerId((String) null);
              }
        else
        {
            setCustomerId(v.getCustomerId());
        }
            aCustomer = v;
    }

                                            
    /**
     * Get the associated Customer object
     *
     * @return the associated Customer object
     * @throws TorqueException
     */
    public Customer getCustomer() throws TorqueException
    {
        if (aCustomer == null && (!ObjectUtils.equals(this.customerId, null)))
        {
                          aCustomer = CustomerPeer.retrieveByPK(SimpleKey.keyFor(this.customerId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Customer obj = CustomerPeer.retrieveByPK(this.customerId);
               obj.addCustomerAddresss(this);
            */
        }
        return aCustomer;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCustomerKey(ObjectKey key) throws TorqueException
    {
      
                        setCustomerId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerAddressId");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerContactId");
              fieldNames.add("ContactName");
              fieldNames.add("AddressType");
              fieldNames.add("Address");
              fieldNames.add("AddressRemark");
              fieldNames.add("Zip");
              fieldNames.add("Country");
              fieldNames.add("Province");
              fieldNames.add("City");
              fieldNames.add("District");
              fieldNames.add("Village");
              fieldNames.add("Phone1");
              fieldNames.add("Phone2");
              fieldNames.add("Fax");
              fieldNames.add("Email");
              fieldNames.add("WebSite");
              fieldNames.add("Field1");
              fieldNames.add("Field2");
              fieldNames.add("Field3");
              fieldNames.add("Field4");
              fieldNames.add("Value1");
              fieldNames.add("Value2");
              fieldNames.add("Value3");
              fieldNames.add("Value4");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerAddressId"))
        {
                return getCustomerAddressId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerContactId"))
        {
                return getCustomerContactId();
            }
          if (name.equals("ContactName"))
        {
                return getContactName();
            }
          if (name.equals("AddressType"))
        {
                return getAddressType();
            }
          if (name.equals("Address"))
        {
                return getAddress();
            }
          if (name.equals("AddressRemark"))
        {
                return getAddressRemark();
            }
          if (name.equals("Zip"))
        {
                return getZip();
            }
          if (name.equals("Country"))
        {
                return getCountry();
            }
          if (name.equals("Province"))
        {
                return getProvince();
            }
          if (name.equals("City"))
        {
                return getCity();
            }
          if (name.equals("District"))
        {
                return getDistrict();
            }
          if (name.equals("Village"))
        {
                return getVillage();
            }
          if (name.equals("Phone1"))
        {
                return getPhone1();
            }
          if (name.equals("Phone2"))
        {
                return getPhone2();
            }
          if (name.equals("Fax"))
        {
                return getFax();
            }
          if (name.equals("Email"))
        {
                return getEmail();
            }
          if (name.equals("WebSite"))
        {
                return getWebSite();
            }
          if (name.equals("Field1"))
        {
                return getField1();
            }
          if (name.equals("Field2"))
        {
                return getField2();
            }
          if (name.equals("Field3"))
        {
                return getField3();
            }
          if (name.equals("Field4"))
        {
                return getField4();
            }
          if (name.equals("Value1"))
        {
                return getValue1();
            }
          if (name.equals("Value2"))
        {
                return getValue2();
            }
          if (name.equals("Value3"))
        {
                return getValue3();
            }
          if (name.equals("Value4"))
        {
                return getValue4();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerAddressPeer.CUSTOMER_ADDRESS_ID))
        {
                return getCustomerAddressId();
            }
          if (name.equals(CustomerAddressPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerAddressPeer.CUSTOMER_CONTACT_ID))
        {
                return getCustomerContactId();
            }
          if (name.equals(CustomerAddressPeer.CONTACT_NAME))
        {
                return getContactName();
            }
          if (name.equals(CustomerAddressPeer.ADDRESS_TYPE))
        {
                return getAddressType();
            }
          if (name.equals(CustomerAddressPeer.ADDRESS))
        {
                return getAddress();
            }
          if (name.equals(CustomerAddressPeer.ADDRESS_REMARK))
        {
                return getAddressRemark();
            }
          if (name.equals(CustomerAddressPeer.ZIP))
        {
                return getZip();
            }
          if (name.equals(CustomerAddressPeer.COUNTRY))
        {
                return getCountry();
            }
          if (name.equals(CustomerAddressPeer.PROVINCE))
        {
                return getProvince();
            }
          if (name.equals(CustomerAddressPeer.CITY))
        {
                return getCity();
            }
          if (name.equals(CustomerAddressPeer.DISTRICT))
        {
                return getDistrict();
            }
          if (name.equals(CustomerAddressPeer.VILLAGE))
        {
                return getVillage();
            }
          if (name.equals(CustomerAddressPeer.PHONE1))
        {
                return getPhone1();
            }
          if (name.equals(CustomerAddressPeer.PHONE2))
        {
                return getPhone2();
            }
          if (name.equals(CustomerAddressPeer.FAX))
        {
                return getFax();
            }
          if (name.equals(CustomerAddressPeer.EMAIL))
        {
                return getEmail();
            }
          if (name.equals(CustomerAddressPeer.WEB_SITE))
        {
                return getWebSite();
            }
          if (name.equals(CustomerAddressPeer.FIELD1))
        {
                return getField1();
            }
          if (name.equals(CustomerAddressPeer.FIELD2))
        {
                return getField2();
            }
          if (name.equals(CustomerAddressPeer.FIELD3))
        {
                return getField3();
            }
          if (name.equals(CustomerAddressPeer.FIELD4))
        {
                return getField4();
            }
          if (name.equals(CustomerAddressPeer.VALUE1))
        {
                return getValue1();
            }
          if (name.equals(CustomerAddressPeer.VALUE2))
        {
                return getValue2();
            }
          if (name.equals(CustomerAddressPeer.VALUE3))
        {
                return getValue3();
            }
          if (name.equals(CustomerAddressPeer.VALUE4))
        {
                return getValue4();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerAddressId();
            }
              if (pos == 1)
        {
                return getCustomerId();
            }
              if (pos == 2)
        {
                return getCustomerContactId();
            }
              if (pos == 3)
        {
                return getContactName();
            }
              if (pos == 4)
        {
                return getAddressType();
            }
              if (pos == 5)
        {
                return getAddress();
            }
              if (pos == 6)
        {
                return getAddressRemark();
            }
              if (pos == 7)
        {
                return getZip();
            }
              if (pos == 8)
        {
                return getCountry();
            }
              if (pos == 9)
        {
                return getProvince();
            }
              if (pos == 10)
        {
                return getCity();
            }
              if (pos == 11)
        {
                return getDistrict();
            }
              if (pos == 12)
        {
                return getVillage();
            }
              if (pos == 13)
        {
                return getPhone1();
            }
              if (pos == 14)
        {
                return getPhone2();
            }
              if (pos == 15)
        {
                return getFax();
            }
              if (pos == 16)
        {
                return getEmail();
            }
              if (pos == 17)
        {
                return getWebSite();
            }
              if (pos == 18)
        {
                return getField1();
            }
              if (pos == 19)
        {
                return getField2();
            }
              if (pos == 20)
        {
                return getField3();
            }
              if (pos == 21)
        {
                return getField4();
            }
              if (pos == 22)
        {
                return getValue1();
            }
              if (pos == 23)
        {
                return getValue2();
            }
              if (pos == 24)
        {
                return getValue3();
            }
              if (pos == 25)
        {
                return getValue4();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerAddressPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerAddressPeer.doInsert((CustomerAddress) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerAddressPeer.doUpdate((CustomerAddress) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerAddressId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerAddressId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerAddressId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerAddressId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerAddress copy() throws TorqueException
    {
        return copyInto(new CustomerAddress());
    }
  
    protected CustomerAddress copyInto(CustomerAddress copyObj) throws TorqueException
    {
          copyObj.setCustomerAddressId(customerAddressId);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerContactId(customerContactId);
          copyObj.setContactName(contactName);
          copyObj.setAddressType(addressType);
          copyObj.setAddress(address);
          copyObj.setAddressRemark(addressRemark);
          copyObj.setZip(zip);
          copyObj.setCountry(country);
          copyObj.setProvince(province);
          copyObj.setCity(city);
          copyObj.setDistrict(district);
          copyObj.setVillage(village);
          copyObj.setPhone1(phone1);
          copyObj.setPhone2(phone2);
          copyObj.setFax(fax);
          copyObj.setEmail(email);
          copyObj.setWebSite(webSite);
          copyObj.setField1(field1);
          copyObj.setField2(field2);
          copyObj.setField3(field3);
          copyObj.setField4(field4);
          copyObj.setValue1(value1);
          copyObj.setValue2(value2);
          copyObj.setValue3(value3);
          copyObj.setValue4(value4);
  
                    copyObj.setCustomerAddressId((String)null);
                                                                                                                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerAddressPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerAddress\n");
        str.append("---------------\n")
           .append("CustomerAddressId    : ")
           .append(getCustomerAddressId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerContactId    : ")
           .append(getCustomerContactId())
           .append("\n")
           .append("ContactName          : ")
           .append(getContactName())
           .append("\n")
           .append("AddressType          : ")
           .append(getAddressType())
           .append("\n")
           .append("Address              : ")
           .append(getAddress())
           .append("\n")
           .append("AddressRemark        : ")
           .append(getAddressRemark())
           .append("\n")
           .append("Zip                  : ")
           .append(getZip())
           .append("\n")
           .append("Country              : ")
           .append(getCountry())
           .append("\n")
           .append("Province             : ")
           .append(getProvince())
           .append("\n")
           .append("City                 : ")
           .append(getCity())
           .append("\n")
           .append("District             : ")
           .append(getDistrict())
           .append("\n")
           .append("Village              : ")
           .append(getVillage())
           .append("\n")
           .append("Phone1               : ")
           .append(getPhone1())
           .append("\n")
           .append("Phone2               : ")
           .append(getPhone2())
           .append("\n")
           .append("Fax                  : ")
           .append(getFax())
           .append("\n")
           .append("Email                : ")
           .append(getEmail())
           .append("\n")
           .append("WebSite              : ")
           .append(getWebSite())
           .append("\n")
           .append("Field1               : ")
           .append(getField1())
           .append("\n")
           .append("Field2               : ")
           .append(getField2())
           .append("\n")
           .append("Field3               : ")
           .append(getField3())
           .append("\n")
           .append("Field4               : ")
           .append(getField4())
           .append("\n")
           .append("Value1               : ")
           .append(getValue1())
           .append("\n")
           .append("Value2               : ")
           .append(getValue2())
           .append("\n")
           .append("Value3               : ")
           .append(getValue3())
           .append("\n")
           .append("Value4               : ")
           .append(getValue4())
           .append("\n")
        ;
        return(str.toString());
    }
}
