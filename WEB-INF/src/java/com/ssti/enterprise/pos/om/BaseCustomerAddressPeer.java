package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CustomerAddressMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BaseCustomerAddressPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "customer_address";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CustomerAddressMapBuilder.CLASS_NAME);
    }

      /** the column name for the CUSTOMER_ADDRESS_ID field */
    public static final String CUSTOMER_ADDRESS_ID;
      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the CUSTOMER_CONTACT_ID field */
    public static final String CUSTOMER_CONTACT_ID;
      /** the column name for the CONTACT_NAME field */
    public static final String CONTACT_NAME;
      /** the column name for the ADDRESS_TYPE field */
    public static final String ADDRESS_TYPE;
      /** the column name for the ADDRESS field */
    public static final String ADDRESS;
      /** the column name for the ADDRESS_REMARK field */
    public static final String ADDRESS_REMARK;
      /** the column name for the ZIP field */
    public static final String ZIP;
      /** the column name for the COUNTRY field */
    public static final String COUNTRY;
      /** the column name for the PROVINCE field */
    public static final String PROVINCE;
      /** the column name for the CITY field */
    public static final String CITY;
      /** the column name for the DISTRICT field */
    public static final String DISTRICT;
      /** the column name for the VILLAGE field */
    public static final String VILLAGE;
      /** the column name for the PHONE1 field */
    public static final String PHONE1;
      /** the column name for the PHONE2 field */
    public static final String PHONE2;
      /** the column name for the FAX field */
    public static final String FAX;
      /** the column name for the EMAIL field */
    public static final String EMAIL;
      /** the column name for the WEB_SITE field */
    public static final String WEB_SITE;
      /** the column name for the FIELD1 field */
    public static final String FIELD1;
      /** the column name for the FIELD2 field */
    public static final String FIELD2;
      /** the column name for the FIELD3 field */
    public static final String FIELD3;
      /** the column name for the FIELD4 field */
    public static final String FIELD4;
      /** the column name for the VALUE1 field */
    public static final String VALUE1;
      /** the column name for the VALUE2 field */
    public static final String VALUE2;
      /** the column name for the VALUE3 field */
    public static final String VALUE3;
      /** the column name for the VALUE4 field */
    public static final String VALUE4;
  
    static
    {
          CUSTOMER_ADDRESS_ID = "customer_address.CUSTOMER_ADDRESS_ID";
          CUSTOMER_ID = "customer_address.CUSTOMER_ID";
          CUSTOMER_CONTACT_ID = "customer_address.CUSTOMER_CONTACT_ID";
          CONTACT_NAME = "customer_address.CONTACT_NAME";
          ADDRESS_TYPE = "customer_address.ADDRESS_TYPE";
          ADDRESS = "customer_address.ADDRESS";
          ADDRESS_REMARK = "customer_address.ADDRESS_REMARK";
          ZIP = "customer_address.ZIP";
          COUNTRY = "customer_address.COUNTRY";
          PROVINCE = "customer_address.PROVINCE";
          CITY = "customer_address.CITY";
          DISTRICT = "customer_address.DISTRICT";
          VILLAGE = "customer_address.VILLAGE";
          PHONE1 = "customer_address.PHONE1";
          PHONE2 = "customer_address.PHONE2";
          FAX = "customer_address.FAX";
          EMAIL = "customer_address.EMAIL";
          WEB_SITE = "customer_address.WEB_SITE";
          FIELD1 = "customer_address.FIELD1";
          FIELD2 = "customer_address.FIELD2";
          FIELD3 = "customer_address.FIELD3";
          FIELD4 = "customer_address.FIELD4";
          VALUE1 = "customer_address.VALUE1";
          VALUE2 = "customer_address.VALUE2";
          VALUE3 = "customer_address.VALUE3";
          VALUE4 = "customer_address.VALUE4";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CustomerAddressMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CustomerAddressMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  26;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.CustomerAddress";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCustomerAddressPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                              
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CUSTOMER_ADDRESS_ID);
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(CUSTOMER_CONTACT_ID);
          criteria.addSelectColumn(CONTACT_NAME);
          criteria.addSelectColumn(ADDRESS_TYPE);
          criteria.addSelectColumn(ADDRESS);
          criteria.addSelectColumn(ADDRESS_REMARK);
          criteria.addSelectColumn(ZIP);
          criteria.addSelectColumn(COUNTRY);
          criteria.addSelectColumn(PROVINCE);
          criteria.addSelectColumn(CITY);
          criteria.addSelectColumn(DISTRICT);
          criteria.addSelectColumn(VILLAGE);
          criteria.addSelectColumn(PHONE1);
          criteria.addSelectColumn(PHONE2);
          criteria.addSelectColumn(FAX);
          criteria.addSelectColumn(EMAIL);
          criteria.addSelectColumn(WEB_SITE);
          criteria.addSelectColumn(FIELD1);
          criteria.addSelectColumn(FIELD2);
          criteria.addSelectColumn(FIELD3);
          criteria.addSelectColumn(FIELD4);
          criteria.addSelectColumn(VALUE1);
          criteria.addSelectColumn(VALUE2);
          criteria.addSelectColumn(VALUE3);
          criteria.addSelectColumn(VALUE4);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CustomerAddress row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CustomerAddress obj = (CustomerAddress) cls.newInstance();
            CustomerAddressPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CustomerAddress obj)
        throws TorqueException
    {
        try
        {
                obj.setCustomerAddressId(row.getValue(offset + 0).asString());
                  obj.setCustomerId(row.getValue(offset + 1).asString());
                  obj.setCustomerContactId(row.getValue(offset + 2).asString());
                  obj.setContactName(row.getValue(offset + 3).asString());
                  obj.setAddressType(row.getValue(offset + 4).asString());
                  obj.setAddress(row.getValue(offset + 5).asString());
                  obj.setAddressRemark(row.getValue(offset + 6).asString());
                  obj.setZip(row.getValue(offset + 7).asString());
                  obj.setCountry(row.getValue(offset + 8).asString());
                  obj.setProvince(row.getValue(offset + 9).asString());
                  obj.setCity(row.getValue(offset + 10).asString());
                  obj.setDistrict(row.getValue(offset + 11).asString());
                  obj.setVillage(row.getValue(offset + 12).asString());
                  obj.setPhone1(row.getValue(offset + 13).asString());
                  obj.setPhone2(row.getValue(offset + 14).asString());
                  obj.setFax(row.getValue(offset + 15).asString());
                  obj.setEmail(row.getValue(offset + 16).asString());
                  obj.setWebSite(row.getValue(offset + 17).asString());
                  obj.setField1(row.getValue(offset + 18).asString());
                  obj.setField2(row.getValue(offset + 19).asString());
                  obj.setField3(row.getValue(offset + 20).asString());
                  obj.setField4(row.getValue(offset + 21).asString());
                  obj.setValue1(row.getValue(offset + 22).asString());
                  obj.setValue2(row.getValue(offset + 23).asString());
                  obj.setValue3(row.getValue(offset + 24).asString());
                  obj.setValue4(row.getValue(offset + 25).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCustomerAddressPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                              
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CustomerAddressPeer.row2Object(row, 1,
                CustomerAddressPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCustomerAddressPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CUSTOMER_ADDRESS_ID, criteria.remove(CUSTOMER_ADDRESS_ID));
                                                                                                                                                                                                                                                                
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CustomerAddressPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                              
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CustomerAddress obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CustomerAddress obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CustomerAddress obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CustomerAddress obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CustomerAddress) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CustomerAddress obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CustomerAddress) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CustomerAddress obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CustomerAddress) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CustomerAddress obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCustomerAddressPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CUSTOMER_ADDRESS_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CustomerAddress obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CUSTOMER_ADDRESS_ID, obj.getCustomerAddressId());
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(CUSTOMER_CONTACT_ID, obj.getCustomerContactId());
              criteria.add(CONTACT_NAME, obj.getContactName());
              criteria.add(ADDRESS_TYPE, obj.getAddressType());
              criteria.add(ADDRESS, obj.getAddress());
              criteria.add(ADDRESS_REMARK, obj.getAddressRemark());
              criteria.add(ZIP, obj.getZip());
              criteria.add(COUNTRY, obj.getCountry());
              criteria.add(PROVINCE, obj.getProvince());
              criteria.add(CITY, obj.getCity());
              criteria.add(DISTRICT, obj.getDistrict());
              criteria.add(VILLAGE, obj.getVillage());
              criteria.add(PHONE1, obj.getPhone1());
              criteria.add(PHONE2, obj.getPhone2());
              criteria.add(FAX, obj.getFax());
              criteria.add(EMAIL, obj.getEmail());
              criteria.add(WEB_SITE, obj.getWebSite());
              criteria.add(FIELD1, obj.getField1());
              criteria.add(FIELD2, obj.getField2());
              criteria.add(FIELD3, obj.getField3());
              criteria.add(FIELD4, obj.getField4());
              criteria.add(VALUE1, obj.getValue1());
              criteria.add(VALUE2, obj.getValue2());
              criteria.add(VALUE3, obj.getValue3());
              criteria.add(VALUE4, obj.getValue4());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CustomerAddress obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CUSTOMER_ADDRESS_ID, obj.getCustomerAddressId());
                          criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(CUSTOMER_CONTACT_ID, obj.getCustomerContactId());
                          criteria.add(CONTACT_NAME, obj.getContactName());
                          criteria.add(ADDRESS_TYPE, obj.getAddressType());
                          criteria.add(ADDRESS, obj.getAddress());
                          criteria.add(ADDRESS_REMARK, obj.getAddressRemark());
                          criteria.add(ZIP, obj.getZip());
                          criteria.add(COUNTRY, obj.getCountry());
                          criteria.add(PROVINCE, obj.getProvince());
                          criteria.add(CITY, obj.getCity());
                          criteria.add(DISTRICT, obj.getDistrict());
                          criteria.add(VILLAGE, obj.getVillage());
                          criteria.add(PHONE1, obj.getPhone1());
                          criteria.add(PHONE2, obj.getPhone2());
                          criteria.add(FAX, obj.getFax());
                          criteria.add(EMAIL, obj.getEmail());
                          criteria.add(WEB_SITE, obj.getWebSite());
                          criteria.add(FIELD1, obj.getField1());
                          criteria.add(FIELD2, obj.getField2());
                          criteria.add(FIELD3, obj.getField3());
                          criteria.add(FIELD4, obj.getField4());
                          criteria.add(VALUE1, obj.getValue1());
                          criteria.add(VALUE2, obj.getValue2());
                          criteria.add(VALUE3, obj.getValue3());
                          criteria.add(VALUE4, obj.getValue4());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerAddress retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerAddress retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerAddress retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CustomerAddress retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerAddress retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CustomerAddress)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CUSTOMER_ADDRESS_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of CustomerAddress objects pre-filled with their
     * Customer objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CustomerAddressPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinCustomer(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        CustomerAddressPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        CustomerPeer.addSelectColumns(criteria);


                        criteria.addJoin(CustomerAddressPeer.CUSTOMER_ID,
            CustomerPeer.CUSTOMER_ID);
        

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = CustomerAddressPeer.getOMClass();
                    CustomerAddress obj1 = CustomerAddressPeer
                .row2Object(row, 1, omClass);
                     omClass = CustomerPeer.getOMClass();
                    Customer obj2 = CustomerPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                CustomerAddress temp_obj1 = (CustomerAddress)results.get(j);
                Customer temp_obj2 = temp_obj1.getCustomer();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addCustomerAddress(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initCustomerAddresss();
                obj2.addCustomerAddress(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
