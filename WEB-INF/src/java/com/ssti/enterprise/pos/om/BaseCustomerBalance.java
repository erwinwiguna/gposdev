package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerBalance
 */
public abstract class BaseCustomerBalance extends BaseObject
{
    /** The Peer class */
    private static final CustomerBalancePeer peer =
        new CustomerBalancePeer();

        
    /** The value for the customerBalanceId field */
    private String customerBalanceId;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the arBalance field */
    private BigDecimal arBalance;
      
    /** The value for the primeBalance field */
    private BigDecimal primeBalance;
  
    
    /**
     * Get the CustomerBalanceId
     *
     * @return String
     */
    public String getCustomerBalanceId()
    {
        return customerBalanceId;
    }

                        
    /**
     * Set the value of CustomerBalanceId
     *
     * @param v new value
     */
    public void setCustomerBalanceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerBalanceId, v))
              {
            this.customerBalanceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ArBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getArBalance()
    {
        return arBalance;
    }

                        
    /**
     * Set the value of ArBalance
     *
     * @param v new value
     */
    public void setArBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.arBalance, v))
              {
            this.arBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrimeBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getPrimeBalance()
    {
        return primeBalance;
    }

                        
    /**
     * Set the value of PrimeBalance
     *
     * @param v new value
     */
    public void setPrimeBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.primeBalance, v))
              {
            this.primeBalance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerBalanceId");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("ArBalance");
              fieldNames.add("PrimeBalance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerBalanceId"))
        {
                return getCustomerBalanceId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("ArBalance"))
        {
                return getArBalance();
            }
          if (name.equals("PrimeBalance"))
        {
                return getPrimeBalance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerBalancePeer.CUSTOMER_BALANCE_ID))
        {
                return getCustomerBalanceId();
            }
          if (name.equals(CustomerBalancePeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerBalancePeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(CustomerBalancePeer.AR_BALANCE))
        {
                return getArBalance();
            }
          if (name.equals(CustomerBalancePeer.PRIME_BALANCE))
        {
                return getPrimeBalance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerBalanceId();
            }
              if (pos == 1)
        {
                return getCustomerId();
            }
              if (pos == 2)
        {
                return getCustomerName();
            }
              if (pos == 3)
        {
                return getArBalance();
            }
              if (pos == 4)
        {
                return getPrimeBalance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerBalancePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerBalancePeer.doInsert((CustomerBalance) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerBalancePeer.doUpdate((CustomerBalance) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerBalanceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerBalanceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerBalanceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerBalanceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerBalance copy() throws TorqueException
    {
        return copyInto(new CustomerBalance());
    }
  
    protected CustomerBalance copyInto(CustomerBalance copyObj) throws TorqueException
    {
          copyObj.setCustomerBalanceId(customerBalanceId);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setArBalance(arBalance);
          copyObj.setPrimeBalance(primeBalance);
  
                    copyObj.setCustomerBalanceId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerBalancePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerBalance\n");
        str.append("---------------\n")
           .append("CustomerBalanceId    : ")
           .append(getCustomerBalanceId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("ArBalance            : ")
           .append(getArBalance())
           .append("\n")
           .append("PrimeBalance         : ")
           .append(getPrimeBalance())
           .append("\n")
        ;
        return(str.toString());
    }
}
