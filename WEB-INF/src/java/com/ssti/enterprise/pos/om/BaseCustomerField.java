package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerField
 */
public abstract class BaseCustomerField extends BaseObject
{
    /** The Peer class */
    private static final CustomerFieldPeer peer =
        new CustomerFieldPeer();

        
    /** The value for the customerFieldId field */
    private String customerFieldId;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the cfieldId field */
    private String cfieldId;
      
    /** The value for the fieldName field */
    private String fieldName;
                                                
    /** The value for the fieldValue field */
    private String fieldValue = "";
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the CustomerFieldId
     *
     * @return String
     */
    public String getCustomerFieldId()
    {
        return customerFieldId;
    }

                        
    /**
     * Set the value of CustomerFieldId
     *
     * @param v new value
     */
    public void setCustomerFieldId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerFieldId, v))
              {
            this.customerFieldId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CfieldId
     *
     * @return String
     */
    public String getCfieldId()
    {
        return cfieldId;
    }

                        
    /**
     * Set the value of CfieldId
     *
     * @param v new value
     */
    public void setCfieldId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cfieldId, v))
              {
            this.cfieldId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldName
     *
     * @return String
     */
    public String getFieldName()
    {
        return fieldName;
    }

                        
    /**
     * Set the value of FieldName
     *
     * @param v new value
     */
    public void setFieldName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldName, v))
              {
            this.fieldName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldValue
     *
     * @return String
     */
    public String getFieldValue()
    {
        return fieldValue;
    }

                        
    /**
     * Set the value of FieldValue
     *
     * @param v new value
     */
    public void setFieldValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldValue, v))
              {
            this.fieldValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerFieldId");
              fieldNames.add("CustomerId");
              fieldNames.add("CfieldId");
              fieldNames.add("FieldName");
              fieldNames.add("FieldValue");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerFieldId"))
        {
                return getCustomerFieldId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CfieldId"))
        {
                return getCfieldId();
            }
          if (name.equals("FieldName"))
        {
                return getFieldName();
            }
          if (name.equals("FieldValue"))
        {
                return getFieldValue();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerFieldPeer.CUSTOMER_FIELD_ID))
        {
                return getCustomerFieldId();
            }
          if (name.equals(CustomerFieldPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerFieldPeer.CFIELD_ID))
        {
                return getCfieldId();
            }
          if (name.equals(CustomerFieldPeer.FIELD_NAME))
        {
                return getFieldName();
            }
          if (name.equals(CustomerFieldPeer.FIELD_VALUE))
        {
                return getFieldValue();
            }
          if (name.equals(CustomerFieldPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerFieldId();
            }
              if (pos == 1)
        {
                return getCustomerId();
            }
              if (pos == 2)
        {
                return getCfieldId();
            }
              if (pos == 3)
        {
                return getFieldName();
            }
              if (pos == 4)
        {
                return getFieldValue();
            }
              if (pos == 5)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerFieldPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerFieldPeer.doInsert((CustomerField) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerFieldPeer.doUpdate((CustomerField) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerFieldId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerFieldId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerFieldId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerFieldId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerField copy() throws TorqueException
    {
        return copyInto(new CustomerField());
    }
  
    protected CustomerField copyInto(CustomerField copyObj) throws TorqueException
    {
          copyObj.setCustomerFieldId(customerFieldId);
          copyObj.setCustomerId(customerId);
          copyObj.setCfieldId(cfieldId);
          copyObj.setFieldName(fieldName);
          copyObj.setFieldValue(fieldValue);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setCustomerFieldId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerFieldPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerField\n");
        str.append("-------------\n")
           .append("CustomerFieldId      : ")
           .append(getCustomerFieldId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CfieldId             : ")
           .append(getCfieldId())
           .append("\n")
           .append("FieldName            : ")
           .append(getFieldName())
           .append("\n")
           .append("FieldValue           : ")
           .append(getFieldValue())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
