package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CustomerMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseCustomerPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "customer";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CustomerMapBuilder.CLASS_NAME);
    }

      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the CUSTOMER_CODE field */
    public static final String CUSTOMER_CODE;
      /** the column name for the CUSTOMER_NAME field */
    public static final String CUSTOMER_NAME;
      /** the column name for the CUSTOMER_TYPE_ID field */
    public static final String CUSTOMER_TYPE_ID;
      /** the column name for the STATUS_ID field */
    public static final String STATUS_ID;
      /** the column name for the INVOICE_ADDRESS field */
    public static final String INVOICE_ADDRESS;
      /** the column name for the TAX_NO field */
    public static final String TAX_NO;
      /** the column name for the CONTACT_NAME1 field */
    public static final String CONTACT_NAME1;
      /** the column name for the CONTACT_NAME2 field */
    public static final String CONTACT_NAME2;
      /** the column name for the CONTACT_NAME3 field */
    public static final String CONTACT_NAME3;
      /** the column name for the JOB_TITLE1 field */
    public static final String JOB_TITLE1;
      /** the column name for the JOB_TITLE2 field */
    public static final String JOB_TITLE2;
      /** the column name for the JOB_TITLE3 field */
    public static final String JOB_TITLE3;
      /** the column name for the CONTACT_PHONE1 field */
    public static final String CONTACT_PHONE1;
      /** the column name for the CONTACT_PHONE2 field */
    public static final String CONTACT_PHONE2;
      /** the column name for the CONTACT_PHONE3 field */
    public static final String CONTACT_PHONE3;
      /** the column name for the ADDRESS field */
    public static final String ADDRESS;
      /** the column name for the SHIP_TO field */
    public static final String SHIP_TO;
      /** the column name for the ZIP field */
    public static final String ZIP;
      /** the column name for the COUNTRY field */
    public static final String COUNTRY;
      /** the column name for the PROVINCE field */
    public static final String PROVINCE;
      /** the column name for the CITY field */
    public static final String CITY;
      /** the column name for the DISTRICT field */
    public static final String DISTRICT;
      /** the column name for the VILLAGE field */
    public static final String VILLAGE;
      /** the column name for the PHONE1 field */
    public static final String PHONE1;
      /** the column name for the PHONE2 field */
    public static final String PHONE2;
      /** the column name for the FAX field */
    public static final String FAX;
      /** the column name for the EMAIL field */
    public static final String EMAIL;
      /** the column name for the WEB_SITE field */
    public static final String WEB_SITE;
      /** the column name for the DEFAULT_TAX_ID field */
    public static final String DEFAULT_TAX_ID;
      /** the column name for the DEFAULT_TYPE_ID field */
    public static final String DEFAULT_TYPE_ID;
      /** the column name for the DEFAULT_TERM_ID field */
    public static final String DEFAULT_TERM_ID;
      /** the column name for the DEFAULT_EMPLOYEE_ID field */
    public static final String DEFAULT_EMPLOYEE_ID;
      /** the column name for the DEFAULT_CURRENCY_ID field */
    public static final String DEFAULT_CURRENCY_ID;
      /** the column name for the DEFAULT_LOCATION_ID field */
    public static final String DEFAULT_LOCATION_ID;
      /** the column name for the DEFAULT_DISCOUNT_AMOUNT field */
    public static final String DEFAULT_DISCOUNT_AMOUNT;
      /** the column name for the CREDIT_LIMIT field */
    public static final String CREDIT_LIMIT;
      /** the column name for the MEMO field */
    public static final String MEMO;
      /** the column name for the INVOICE_MESSAGE field */
    public static final String INVOICE_MESSAGE;
      /** the column name for the ADD_DATE field */
    public static final String ADD_DATE;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the LAST_UPDATE_LOCATION_ID field */
    public static final String LAST_UPDATE_LOCATION_ID;
      /** the column name for the IS_DEFAULT field */
    public static final String IS_DEFAULT;
      /** the column name for the IS_ACTIVE field */
    public static final String IS_ACTIVE;
      /** the column name for the FIELD1 field */
    public static final String FIELD1;
      /** the column name for the FIELD2 field */
    public static final String FIELD2;
      /** the column name for the VALUE1 field */
    public static final String VALUE1;
      /** the column name for the VALUE2 field */
    public static final String VALUE2;
      /** the column name for the AR_ACCOUNT field */
    public static final String AR_ACCOUNT;
      /** the column name for the OPENING_BALANCE field */
    public static final String OPENING_BALANCE;
      /** the column name for the AS_DATE field */
    public static final String AS_DATE;
      /** the column name for the OB_TRANS_ID field */
    public static final String OB_TRANS_ID;
      /** the column name for the OB_RATE field */
    public static final String OB_RATE;
      /** the column name for the BIRTH_DATE field */
    public static final String BIRTH_DATE;
      /** the column name for the BIRTH_PLACE field */
    public static final String BIRTH_PLACE;
      /** the column name for the RELIGION field */
    public static final String RELIGION;
      /** the column name for the GENDER field */
    public static final String GENDER;
      /** the column name for the OCCUPATION field */
    public static final String OCCUPATION;
      /** the column name for the HOBBY field */
    public static final String HOBBY;
      /** the column name for the TRANS_PREFIX field */
    public static final String TRANS_PREFIX;
      /** the column name for the CREATE_BY field */
    public static final String CREATE_BY;
      /** the column name for the LAST_UPDATE_BY field */
    public static final String LAST_UPDATE_BY;
      /** the column name for the SALES_AREA_ID field */
    public static final String SALES_AREA_ID;
      /** the column name for the TERRITORY_ID field */
    public static final String TERRITORY_ID;
      /** the column name for the LONGITUDES field */
    public static final String LONGITUDES;
      /** the column name for the LATITUDES field */
    public static final String LATITUDES;
      /** the column name for the BILL_TO_ID field */
    public static final String BILL_TO_ID;
      /** the column name for the PARENT_ID field */
    public static final String PARENT_ID;
  
    static
    {
          CUSTOMER_ID = "customer.CUSTOMER_ID";
          CUSTOMER_CODE = "customer.CUSTOMER_CODE";
          CUSTOMER_NAME = "customer.CUSTOMER_NAME";
          CUSTOMER_TYPE_ID = "customer.CUSTOMER_TYPE_ID";
          STATUS_ID = "customer.STATUS_ID";
          INVOICE_ADDRESS = "customer.INVOICE_ADDRESS";
          TAX_NO = "customer.TAX_NO";
          CONTACT_NAME1 = "customer.CONTACT_NAME1";
          CONTACT_NAME2 = "customer.CONTACT_NAME2";
          CONTACT_NAME3 = "customer.CONTACT_NAME3";
          JOB_TITLE1 = "customer.JOB_TITLE1";
          JOB_TITLE2 = "customer.JOB_TITLE2";
          JOB_TITLE3 = "customer.JOB_TITLE3";
          CONTACT_PHONE1 = "customer.CONTACT_PHONE1";
          CONTACT_PHONE2 = "customer.CONTACT_PHONE2";
          CONTACT_PHONE3 = "customer.CONTACT_PHONE3";
          ADDRESS = "customer.ADDRESS";
          SHIP_TO = "customer.SHIP_TO";
          ZIP = "customer.ZIP";
          COUNTRY = "customer.COUNTRY";
          PROVINCE = "customer.PROVINCE";
          CITY = "customer.CITY";
          DISTRICT = "customer.DISTRICT";
          VILLAGE = "customer.VILLAGE";
          PHONE1 = "customer.PHONE1";
          PHONE2 = "customer.PHONE2";
          FAX = "customer.FAX";
          EMAIL = "customer.EMAIL";
          WEB_SITE = "customer.WEB_SITE";
          DEFAULT_TAX_ID = "customer.DEFAULT_TAX_ID";
          DEFAULT_TYPE_ID = "customer.DEFAULT_TYPE_ID";
          DEFAULT_TERM_ID = "customer.DEFAULT_TERM_ID";
          DEFAULT_EMPLOYEE_ID = "customer.DEFAULT_EMPLOYEE_ID";
          DEFAULT_CURRENCY_ID = "customer.DEFAULT_CURRENCY_ID";
          DEFAULT_LOCATION_ID = "customer.DEFAULT_LOCATION_ID";
          DEFAULT_DISCOUNT_AMOUNT = "customer.DEFAULT_DISCOUNT_AMOUNT";
          CREDIT_LIMIT = "customer.CREDIT_LIMIT";
          MEMO = "customer.MEMO";
          INVOICE_MESSAGE = "customer.INVOICE_MESSAGE";
          ADD_DATE = "customer.ADD_DATE";
          UPDATE_DATE = "customer.UPDATE_DATE";
          LAST_UPDATE_LOCATION_ID = "customer.LAST_UPDATE_LOCATION_ID";
          IS_DEFAULT = "customer.IS_DEFAULT";
          IS_ACTIVE = "customer.IS_ACTIVE";
          FIELD1 = "customer.FIELD1";
          FIELD2 = "customer.FIELD2";
          VALUE1 = "customer.VALUE1";
          VALUE2 = "customer.VALUE2";
          AR_ACCOUNT = "customer.AR_ACCOUNT";
          OPENING_BALANCE = "customer.OPENING_BALANCE";
          AS_DATE = "customer.AS_DATE";
          OB_TRANS_ID = "customer.OB_TRANS_ID";
          OB_RATE = "customer.OB_RATE";
          BIRTH_DATE = "customer.BIRTH_DATE";
          BIRTH_PLACE = "customer.BIRTH_PLACE";
          RELIGION = "customer.RELIGION";
          GENDER = "customer.GENDER";
          OCCUPATION = "customer.OCCUPATION";
          HOBBY = "customer.HOBBY";
          TRANS_PREFIX = "customer.TRANS_PREFIX";
          CREATE_BY = "customer.CREATE_BY";
          LAST_UPDATE_BY = "customer.LAST_UPDATE_BY";
          SALES_AREA_ID = "customer.SALES_AREA_ID";
          TERRITORY_ID = "customer.TERRITORY_ID";
          LONGITUDES = "customer.LONGITUDES";
          LATITUDES = "customer.LATITUDES";
          BILL_TO_ID = "customer.BILL_TO_ID";
          PARENT_ID = "customer.PARENT_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CustomerMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CustomerMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  68;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Customer";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCustomerPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DEFAULT))
        {
            Object possibleBoolean = criteria.get(IS_DEFAULT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DEFAULT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(CUSTOMER_CODE);
          criteria.addSelectColumn(CUSTOMER_NAME);
          criteria.addSelectColumn(CUSTOMER_TYPE_ID);
          criteria.addSelectColumn(STATUS_ID);
          criteria.addSelectColumn(INVOICE_ADDRESS);
          criteria.addSelectColumn(TAX_NO);
          criteria.addSelectColumn(CONTACT_NAME1);
          criteria.addSelectColumn(CONTACT_NAME2);
          criteria.addSelectColumn(CONTACT_NAME3);
          criteria.addSelectColumn(JOB_TITLE1);
          criteria.addSelectColumn(JOB_TITLE2);
          criteria.addSelectColumn(JOB_TITLE3);
          criteria.addSelectColumn(CONTACT_PHONE1);
          criteria.addSelectColumn(CONTACT_PHONE2);
          criteria.addSelectColumn(CONTACT_PHONE3);
          criteria.addSelectColumn(ADDRESS);
          criteria.addSelectColumn(SHIP_TO);
          criteria.addSelectColumn(ZIP);
          criteria.addSelectColumn(COUNTRY);
          criteria.addSelectColumn(PROVINCE);
          criteria.addSelectColumn(CITY);
          criteria.addSelectColumn(DISTRICT);
          criteria.addSelectColumn(VILLAGE);
          criteria.addSelectColumn(PHONE1);
          criteria.addSelectColumn(PHONE2);
          criteria.addSelectColumn(FAX);
          criteria.addSelectColumn(EMAIL);
          criteria.addSelectColumn(WEB_SITE);
          criteria.addSelectColumn(DEFAULT_TAX_ID);
          criteria.addSelectColumn(DEFAULT_TYPE_ID);
          criteria.addSelectColumn(DEFAULT_TERM_ID);
          criteria.addSelectColumn(DEFAULT_EMPLOYEE_ID);
          criteria.addSelectColumn(DEFAULT_CURRENCY_ID);
          criteria.addSelectColumn(DEFAULT_LOCATION_ID);
          criteria.addSelectColumn(DEFAULT_DISCOUNT_AMOUNT);
          criteria.addSelectColumn(CREDIT_LIMIT);
          criteria.addSelectColumn(MEMO);
          criteria.addSelectColumn(INVOICE_MESSAGE);
          criteria.addSelectColumn(ADD_DATE);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(LAST_UPDATE_LOCATION_ID);
          criteria.addSelectColumn(IS_DEFAULT);
          criteria.addSelectColumn(IS_ACTIVE);
          criteria.addSelectColumn(FIELD1);
          criteria.addSelectColumn(FIELD2);
          criteria.addSelectColumn(VALUE1);
          criteria.addSelectColumn(VALUE2);
          criteria.addSelectColumn(AR_ACCOUNT);
          criteria.addSelectColumn(OPENING_BALANCE);
          criteria.addSelectColumn(AS_DATE);
          criteria.addSelectColumn(OB_TRANS_ID);
          criteria.addSelectColumn(OB_RATE);
          criteria.addSelectColumn(BIRTH_DATE);
          criteria.addSelectColumn(BIRTH_PLACE);
          criteria.addSelectColumn(RELIGION);
          criteria.addSelectColumn(GENDER);
          criteria.addSelectColumn(OCCUPATION);
          criteria.addSelectColumn(HOBBY);
          criteria.addSelectColumn(TRANS_PREFIX);
          criteria.addSelectColumn(CREATE_BY);
          criteria.addSelectColumn(LAST_UPDATE_BY);
          criteria.addSelectColumn(SALES_AREA_ID);
          criteria.addSelectColumn(TERRITORY_ID);
          criteria.addSelectColumn(LONGITUDES);
          criteria.addSelectColumn(LATITUDES);
          criteria.addSelectColumn(BILL_TO_ID);
          criteria.addSelectColumn(PARENT_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Customer row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Customer obj = (Customer) cls.newInstance();
            CustomerPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Customer obj)
        throws TorqueException
    {
        try
        {
                obj.setCustomerId(row.getValue(offset + 0).asString());
                  obj.setCustomerCode(row.getValue(offset + 1).asString());
                  obj.setCustomerName(row.getValue(offset + 2).asString());
                  obj.setCustomerTypeId(row.getValue(offset + 3).asString());
                  obj.setStatusId(row.getValue(offset + 4).asString());
                  obj.setInvoiceAddress(row.getValue(offset + 5).asString());
                  obj.setTaxNo(row.getValue(offset + 6).asString());
                  obj.setContactName1(row.getValue(offset + 7).asString());
                  obj.setContactName2(row.getValue(offset + 8).asString());
                  obj.setContactName3(row.getValue(offset + 9).asString());
                  obj.setJobTitle1(row.getValue(offset + 10).asString());
                  obj.setJobTitle2(row.getValue(offset + 11).asString());
                  obj.setJobTitle3(row.getValue(offset + 12).asString());
                  obj.setContactPhone1(row.getValue(offset + 13).asString());
                  obj.setContactPhone2(row.getValue(offset + 14).asString());
                  obj.setContactPhone3(row.getValue(offset + 15).asString());
                  obj.setAddress(row.getValue(offset + 16).asString());
                  obj.setShipTo(row.getValue(offset + 17).asString());
                  obj.setZip(row.getValue(offset + 18).asString());
                  obj.setCountry(row.getValue(offset + 19).asString());
                  obj.setProvince(row.getValue(offset + 20).asString());
                  obj.setCity(row.getValue(offset + 21).asString());
                  obj.setDistrict(row.getValue(offset + 22).asString());
                  obj.setVillage(row.getValue(offset + 23).asString());
                  obj.setPhone1(row.getValue(offset + 24).asString());
                  obj.setPhone2(row.getValue(offset + 25).asString());
                  obj.setFax(row.getValue(offset + 26).asString());
                  obj.setEmail(row.getValue(offset + 27).asString());
                  obj.setWebSite(row.getValue(offset + 28).asString());
                  obj.setDefaultTaxId(row.getValue(offset + 29).asString());
                  obj.setDefaultTypeId(row.getValue(offset + 30).asString());
                  obj.setDefaultTermId(row.getValue(offset + 31).asString());
                  obj.setDefaultEmployeeId(row.getValue(offset + 32).asString());
                  obj.setDefaultCurrencyId(row.getValue(offset + 33).asString());
                  obj.setDefaultLocationId(row.getValue(offset + 34).asString());
                  obj.setDefaultDiscountAmount(row.getValue(offset + 35).asString());
                  obj.setCreditLimit(row.getValue(offset + 36).asBigDecimal());
                  obj.setMemo(row.getValue(offset + 37).asString());
                  obj.setInvoiceMessage(row.getValue(offset + 38).asString());
                  obj.setAddDate(row.getValue(offset + 39).asUtilDate());
                  obj.setUpdateDate(row.getValue(offset + 40).asUtilDate());
                  obj.setLastUpdateLocationId(row.getValue(offset + 41).asString());
                  obj.setIsDefault(row.getValue(offset + 42).asBoolean());
                  obj.setIsActive(row.getValue(offset + 43).asBoolean());
                  obj.setField1(row.getValue(offset + 44).asString());
                  obj.setField2(row.getValue(offset + 45).asString());
                  obj.setValue1(row.getValue(offset + 46).asString());
                  obj.setValue2(row.getValue(offset + 47).asString());
                  obj.setArAccount(row.getValue(offset + 48).asString());
                  obj.setOpeningBalance(row.getValue(offset + 49).asBigDecimal());
                  obj.setAsDate(row.getValue(offset + 50).asUtilDate());
                  obj.setObTransId(row.getValue(offset + 51).asString());
                  obj.setObRate(row.getValue(offset + 52).asBigDecimal());
                  obj.setBirthDate(row.getValue(offset + 53).asUtilDate());
                  obj.setBirthPlace(row.getValue(offset + 54).asString());
                  obj.setReligion(row.getValue(offset + 55).asString());
                  obj.setGender(row.getValue(offset + 56).asInt());
                  obj.setOccupation(row.getValue(offset + 57).asString());
                  obj.setHobby(row.getValue(offset + 58).asString());
                  obj.setTransPrefix(row.getValue(offset + 59).asString());
                  obj.setCreateBy(row.getValue(offset + 60).asString());
                  obj.setLastUpdateBy(row.getValue(offset + 61).asString());
                  obj.setSalesAreaId(row.getValue(offset + 62).asString());
                  obj.setTerritoryId(row.getValue(offset + 63).asString());
                  obj.setLongitudes(row.getValue(offset + 64).asBigDecimal());
                  obj.setLatitudes(row.getValue(offset + 65).asBigDecimal());
                  obj.setBillToId(row.getValue(offset + 66).asString());
                  obj.setParentId(row.getValue(offset + 67).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCustomerPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DEFAULT))
        {
            Object possibleBoolean = criteria.get(IS_DEFAULT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DEFAULT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CustomerPeer.row2Object(row, 1,
                CustomerPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCustomerPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CUSTOMER_ID, criteria.remove(CUSTOMER_ID));
                                                                                                                                                                                                                                                                                                                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DEFAULT))
        {
            Object possibleBoolean = criteria.get(IS_DEFAULT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DEFAULT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CustomerPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DEFAULT))
        {
            Object possibleBoolean = criteria.get(IS_DEFAULT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DEFAULT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Customer obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Customer obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Customer obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Customer obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Customer) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Customer obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Customer) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Customer obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Customer) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Customer obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCustomerPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CUSTOMER_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Customer obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(CUSTOMER_CODE, obj.getCustomerCode());
              criteria.add(CUSTOMER_NAME, obj.getCustomerName());
              criteria.add(CUSTOMER_TYPE_ID, obj.getCustomerTypeId());
              criteria.add(STATUS_ID, obj.getStatusId());
              criteria.add(INVOICE_ADDRESS, obj.getInvoiceAddress());
              criteria.add(TAX_NO, obj.getTaxNo());
              criteria.add(CONTACT_NAME1, obj.getContactName1());
              criteria.add(CONTACT_NAME2, obj.getContactName2());
              criteria.add(CONTACT_NAME3, obj.getContactName3());
              criteria.add(JOB_TITLE1, obj.getJobTitle1());
              criteria.add(JOB_TITLE2, obj.getJobTitle2());
              criteria.add(JOB_TITLE3, obj.getJobTitle3());
              criteria.add(CONTACT_PHONE1, obj.getContactPhone1());
              criteria.add(CONTACT_PHONE2, obj.getContactPhone2());
              criteria.add(CONTACT_PHONE3, obj.getContactPhone3());
              criteria.add(ADDRESS, obj.getAddress());
              criteria.add(SHIP_TO, obj.getShipTo());
              criteria.add(ZIP, obj.getZip());
              criteria.add(COUNTRY, obj.getCountry());
              criteria.add(PROVINCE, obj.getProvince());
              criteria.add(CITY, obj.getCity());
              criteria.add(DISTRICT, obj.getDistrict());
              criteria.add(VILLAGE, obj.getVillage());
              criteria.add(PHONE1, obj.getPhone1());
              criteria.add(PHONE2, obj.getPhone2());
              criteria.add(FAX, obj.getFax());
              criteria.add(EMAIL, obj.getEmail());
              criteria.add(WEB_SITE, obj.getWebSite());
              criteria.add(DEFAULT_TAX_ID, obj.getDefaultTaxId());
              criteria.add(DEFAULT_TYPE_ID, obj.getDefaultTypeId());
              criteria.add(DEFAULT_TERM_ID, obj.getDefaultTermId());
              criteria.add(DEFAULT_EMPLOYEE_ID, obj.getDefaultEmployeeId());
              criteria.add(DEFAULT_CURRENCY_ID, obj.getDefaultCurrencyId());
              criteria.add(DEFAULT_LOCATION_ID, obj.getDefaultLocationId());
              criteria.add(DEFAULT_DISCOUNT_AMOUNT, obj.getDefaultDiscountAmount());
              criteria.add(CREDIT_LIMIT, obj.getCreditLimit());
              criteria.add(MEMO, obj.getMemo());
              criteria.add(INVOICE_MESSAGE, obj.getInvoiceMessage());
              criteria.add(ADD_DATE, obj.getAddDate());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(LAST_UPDATE_LOCATION_ID, obj.getLastUpdateLocationId());
              criteria.add(IS_DEFAULT, obj.getIsDefault());
              criteria.add(IS_ACTIVE, obj.getIsActive());
              criteria.add(FIELD1, obj.getField1());
              criteria.add(FIELD2, obj.getField2());
              criteria.add(VALUE1, obj.getValue1());
              criteria.add(VALUE2, obj.getValue2());
              criteria.add(AR_ACCOUNT, obj.getArAccount());
              criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
              criteria.add(AS_DATE, obj.getAsDate());
              criteria.add(OB_TRANS_ID, obj.getObTransId());
              criteria.add(OB_RATE, obj.getObRate());
              criteria.add(BIRTH_DATE, obj.getBirthDate());
              criteria.add(BIRTH_PLACE, obj.getBirthPlace());
              criteria.add(RELIGION, obj.getReligion());
              criteria.add(GENDER, obj.getGender());
              criteria.add(OCCUPATION, obj.getOccupation());
              criteria.add(HOBBY, obj.getHobby());
              criteria.add(TRANS_PREFIX, obj.getTransPrefix());
              criteria.add(CREATE_BY, obj.getCreateBy());
              criteria.add(LAST_UPDATE_BY, obj.getLastUpdateBy());
              criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
              criteria.add(TERRITORY_ID, obj.getTerritoryId());
              criteria.add(LONGITUDES, obj.getLongitudes());
              criteria.add(LATITUDES, obj.getLatitudes());
              criteria.add(BILL_TO_ID, obj.getBillToId());
              criteria.add(PARENT_ID, obj.getParentId());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Customer obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(CUSTOMER_CODE, obj.getCustomerCode());
                          criteria.add(CUSTOMER_NAME, obj.getCustomerName());
                          criteria.add(CUSTOMER_TYPE_ID, obj.getCustomerTypeId());
                          criteria.add(STATUS_ID, obj.getStatusId());
                          criteria.add(INVOICE_ADDRESS, obj.getInvoiceAddress());
                          criteria.add(TAX_NO, obj.getTaxNo());
                          criteria.add(CONTACT_NAME1, obj.getContactName1());
                          criteria.add(CONTACT_NAME2, obj.getContactName2());
                          criteria.add(CONTACT_NAME3, obj.getContactName3());
                          criteria.add(JOB_TITLE1, obj.getJobTitle1());
                          criteria.add(JOB_TITLE2, obj.getJobTitle2());
                          criteria.add(JOB_TITLE3, obj.getJobTitle3());
                          criteria.add(CONTACT_PHONE1, obj.getContactPhone1());
                          criteria.add(CONTACT_PHONE2, obj.getContactPhone2());
                          criteria.add(CONTACT_PHONE3, obj.getContactPhone3());
                          criteria.add(ADDRESS, obj.getAddress());
                          criteria.add(SHIP_TO, obj.getShipTo());
                          criteria.add(ZIP, obj.getZip());
                          criteria.add(COUNTRY, obj.getCountry());
                          criteria.add(PROVINCE, obj.getProvince());
                          criteria.add(CITY, obj.getCity());
                          criteria.add(DISTRICT, obj.getDistrict());
                          criteria.add(VILLAGE, obj.getVillage());
                          criteria.add(PHONE1, obj.getPhone1());
                          criteria.add(PHONE2, obj.getPhone2());
                          criteria.add(FAX, obj.getFax());
                          criteria.add(EMAIL, obj.getEmail());
                          criteria.add(WEB_SITE, obj.getWebSite());
                          criteria.add(DEFAULT_TAX_ID, obj.getDefaultTaxId());
                          criteria.add(DEFAULT_TYPE_ID, obj.getDefaultTypeId());
                          criteria.add(DEFAULT_TERM_ID, obj.getDefaultTermId());
                          criteria.add(DEFAULT_EMPLOYEE_ID, obj.getDefaultEmployeeId());
                          criteria.add(DEFAULT_CURRENCY_ID, obj.getDefaultCurrencyId());
                          criteria.add(DEFAULT_LOCATION_ID, obj.getDefaultLocationId());
                          criteria.add(DEFAULT_DISCOUNT_AMOUNT, obj.getDefaultDiscountAmount());
                          criteria.add(CREDIT_LIMIT, obj.getCreditLimit());
                          criteria.add(MEMO, obj.getMemo());
                          criteria.add(INVOICE_MESSAGE, obj.getInvoiceMessage());
                          criteria.add(ADD_DATE, obj.getAddDate());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(LAST_UPDATE_LOCATION_ID, obj.getLastUpdateLocationId());
                          criteria.add(IS_DEFAULT, obj.getIsDefault());
                          criteria.add(IS_ACTIVE, obj.getIsActive());
                          criteria.add(FIELD1, obj.getField1());
                          criteria.add(FIELD2, obj.getField2());
                          criteria.add(VALUE1, obj.getValue1());
                          criteria.add(VALUE2, obj.getValue2());
                          criteria.add(AR_ACCOUNT, obj.getArAccount());
                          criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
                          criteria.add(AS_DATE, obj.getAsDate());
                          criteria.add(OB_TRANS_ID, obj.getObTransId());
                          criteria.add(OB_RATE, obj.getObRate());
                          criteria.add(BIRTH_DATE, obj.getBirthDate());
                          criteria.add(BIRTH_PLACE, obj.getBirthPlace());
                          criteria.add(RELIGION, obj.getReligion());
                          criteria.add(GENDER, obj.getGender());
                          criteria.add(OCCUPATION, obj.getOccupation());
                          criteria.add(HOBBY, obj.getHobby());
                          criteria.add(TRANS_PREFIX, obj.getTransPrefix());
                          criteria.add(CREATE_BY, obj.getCreateBy());
                          criteria.add(LAST_UPDATE_BY, obj.getLastUpdateBy());
                          criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
                          criteria.add(TERRITORY_ID, obj.getTerritoryId());
                          criteria.add(LONGITUDES, obj.getLongitudes());
                          criteria.add(LATITUDES, obj.getLatitudes());
                          criteria.add(BILL_TO_ID, obj.getBillToId());
                          criteria.add(PARENT_ID, obj.getParentId());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Customer retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Customer retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Customer retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Customer retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Customer retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Customer)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CUSTOMER_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
