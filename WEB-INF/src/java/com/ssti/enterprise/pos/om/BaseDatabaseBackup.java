package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to DatabaseBackup
 */
public abstract class BaseDatabaseBackup extends BaseObject
{
    /** The Peer class */
    private static final DatabaseBackupPeer peer =
        new DatabaseBackupPeer();

        
    /** The value for the databaseBackupId field */
    private String databaseBackupId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the backupDate field */
    private Date backupDate;
      
    /** The value for the fileName field */
    private String fileName;
      
    /** The value for the userName field */
    private String userName;
                                                
    /** The value for the description field */
    private String description = "";
  
    
    /**
     * Get the DatabaseBackupId
     *
     * @return String
     */
    public String getDatabaseBackupId()
    {
        return databaseBackupId;
    }

                        
    /**
     * Set the value of DatabaseBackupId
     *
     * @param v new value
     */
    public void setDatabaseBackupId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.databaseBackupId, v))
              {
            this.databaseBackupId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BackupDate
     *
     * @return Date
     */
    public Date getBackupDate()
    {
        return backupDate;
    }

                        
    /**
     * Set the value of BackupDate
     *
     * @param v new value
     */
    public void setBackupDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.backupDate, v))
              {
            this.backupDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FileName
     *
     * @return String
     */
    public String getFileName()
    {
        return fileName;
    }

                        
    /**
     * Set the value of FileName
     *
     * @param v new value
     */
    public void setFileName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fileName, v))
              {
            this.fileName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DatabaseBackupId");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("BackupDate");
              fieldNames.add("FileName");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DatabaseBackupId"))
        {
                return getDatabaseBackupId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("BackupDate"))
        {
                return getBackupDate();
            }
          if (name.equals("FileName"))
        {
                return getFileName();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DatabaseBackupPeer.DATABASE_BACKUP_ID))
        {
                return getDatabaseBackupId();
            }
          if (name.equals(DatabaseBackupPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(DatabaseBackupPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(DatabaseBackupPeer.BACKUP_DATE))
        {
                return getBackupDate();
            }
          if (name.equals(DatabaseBackupPeer.FILE_NAME))
        {
                return getFileName();
            }
          if (name.equals(DatabaseBackupPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(DatabaseBackupPeer.DESCRIPTION))
        {
                return getDescription();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDatabaseBackupId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getLocationName();
            }
              if (pos == 3)
        {
                return getBackupDate();
            }
              if (pos == 4)
        {
                return getFileName();
            }
              if (pos == 5)
        {
                return getUserName();
            }
              if (pos == 6)
        {
                return getDescription();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DatabaseBackupPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DatabaseBackupPeer.doInsert((DatabaseBackup) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DatabaseBackupPeer.doUpdate((DatabaseBackup) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key databaseBackupId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDatabaseBackupId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDatabaseBackupId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDatabaseBackupId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public DatabaseBackup copy() throws TorqueException
    {
        return copyInto(new DatabaseBackup());
    }
  
    protected DatabaseBackup copyInto(DatabaseBackup copyObj) throws TorqueException
    {
          copyObj.setDatabaseBackupId(databaseBackupId);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setBackupDate(backupDate);
          copyObj.setFileName(fileName);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
  
                    copyObj.setDatabaseBackupId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DatabaseBackupPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("DatabaseBackup\n");
        str.append("--------------\n")
           .append("DatabaseBackupId     : ")
           .append(getDatabaseBackupId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("BackupDate           : ")
           .append(getBackupDate())
           .append("\n")
           .append("FileName             : ")
           .append(getFileName())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
        ;
        return(str.toString());
    }
}
