package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to DebitMemo
 */
public abstract class BaseDebitMemo extends BaseObject
{
    /** The Peer class */
    private static final DebitMemoPeer peer =
        new DebitMemoPeer();

        
    /** The value for the debitMemoId field */
    private String debitMemoId;
      
    /** The value for the debitMemoNo field */
    private String debitMemoNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
                                                
    /** The value for the transactionId field */
    private String transactionId = "";
                                                
    /** The value for the transactionNo field */
    private String transactionNo = "";
                                          
    /** The value for the transactionType field */
    private int transactionType = 0;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the closedDate field */
    private Date closedDate;
                                                
    /** The value for the paymentTransId field */
    private String paymentTransId = "";
                                                
    /** The value for the paymentTransNo field */
    private String paymentTransNo = "";
                                                
    /** The value for the paymentInvId field */
    private String paymentInvId = "";
                                                
    /** The value for the bankId field */
    private String bankId = "";
                                                
    /** The value for the bankIssuer field */
    private String bankIssuer = "";
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the referenceNo field */
    private String referenceNo;
                                                
    /** The value for the accountId field */
    private String accountId = "";
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
                                                
          
    /** The value for the closingRate field */
    private BigDecimal closingRate= new BigDecimal(1);
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
                                                
    /** The value for the fromPaymentId field */
    private String fromPaymentId = "";
                                                
    /** The value for the crossAccountId field */
    private String crossAccountId = "";
  
    
    /**
     * Get the DebitMemoId
     *
     * @return String
     */
    public String getDebitMemoId()
    {
        return debitMemoId;
    }

                        
    /**
     * Set the value of DebitMemoId
     *
     * @param v new value
     */
    public void setDebitMemoId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.debitMemoId, v))
              {
            this.debitMemoId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DebitMemoNo
     *
     * @return String
     */
    public String getDebitMemoNo()
    {
        return debitMemoNo;
    }

                        
    /**
     * Set the value of DebitMemoNo
     *
     * @param v new value
     */
    public void setDebitMemoNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.debitMemoNo, v))
              {
            this.debitMemoNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClosedDate
     *
     * @return Date
     */
    public Date getClosedDate()
    {
        return closedDate;
    }

                        
    /**
     * Set the value of ClosedDate
     *
     * @param v new value
     */
    public void setClosedDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.closedDate, v))
              {
            this.closedDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTransId
     *
     * @return String
     */
    public String getPaymentTransId()
    {
        return paymentTransId;
    }

                        
    /**
     * Set the value of PaymentTransId
     *
     * @param v new value
     */
    public void setPaymentTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTransId, v))
              {
            this.paymentTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTransNo
     *
     * @return String
     */
    public String getPaymentTransNo()
    {
        return paymentTransNo;
    }

                        
    /**
     * Set the value of PaymentTransNo
     *
     * @param v new value
     */
    public void setPaymentTransNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTransNo, v))
              {
            this.paymentTransNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentInvId
     *
     * @return String
     */
    public String getPaymentInvId()
    {
        return paymentInvId;
    }

                        
    /**
     * Set the value of PaymentInvId
     *
     * @param v new value
     */
    public void setPaymentInvId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentInvId, v))
              {
            this.paymentInvId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClosingRate
     *
     * @return BigDecimal
     */
    public BigDecimal getClosingRate()
    {
        return closingRate;
    }

                        
    /**
     * Set the value of ClosingRate
     *
     * @param v new value
     */
    public void setClosingRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.closingRate, v))
              {
            this.closingRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FromPaymentId
     *
     * @return String
     */
    public String getFromPaymentId()
    {
        return fromPaymentId;
    }

                        
    /**
     * Set the value of FromPaymentId
     *
     * @param v new value
     */
    public void setFromPaymentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fromPaymentId, v))
              {
            this.fromPaymentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossAccountId
     *
     * @return String
     */
    public String getCrossAccountId()
    {
        return crossAccountId;
    }

                        
    /**
     * Set the value of CrossAccountId
     *
     * @param v new value
     */
    public void setCrossAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossAccountId, v))
              {
            this.crossAccountId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DebitMemoId");
              fieldNames.add("DebitMemoNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionType");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("UserName");
              fieldNames.add("Remark");
              fieldNames.add("Status");
              fieldNames.add("ClosedDate");
              fieldNames.add("PaymentTransId");
              fieldNames.add("PaymentTransNo");
              fieldNames.add("PaymentInvId");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("DueDate");
              fieldNames.add("ReferenceNo");
              fieldNames.add("AccountId");
              fieldNames.add("FiscalRate");
              fieldNames.add("ClosingRate");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("LocationId");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("FromPaymentId");
              fieldNames.add("CrossAccountId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DebitMemoId"))
        {
                return getDebitMemoId();
            }
          if (name.equals("DebitMemoNo"))
        {
                return getDebitMemoNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("ClosedDate"))
        {
                return getClosedDate();
            }
          if (name.equals("PaymentTransId"))
        {
                return getPaymentTransId();
            }
          if (name.equals("PaymentTransNo"))
        {
                return getPaymentTransNo();
            }
          if (name.equals("PaymentInvId"))
        {
                return getPaymentInvId();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("ClosingRate"))
        {
                return getClosingRate();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("FromPaymentId"))
        {
                return getFromPaymentId();
            }
          if (name.equals("CrossAccountId"))
        {
                return getCrossAccountId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DebitMemoPeer.DEBIT_MEMO_ID))
        {
                return getDebitMemoId();
            }
          if (name.equals(DebitMemoPeer.DEBIT_MEMO_NO))
        {
                return getDebitMemoNo();
            }
          if (name.equals(DebitMemoPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(DebitMemoPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(DebitMemoPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(DebitMemoPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(DebitMemoPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(DebitMemoPeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(DebitMemoPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(DebitMemoPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(DebitMemoPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(DebitMemoPeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(DebitMemoPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(DebitMemoPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(DebitMemoPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(DebitMemoPeer.CLOSED_DATE))
        {
                return getClosedDate();
            }
          if (name.equals(DebitMemoPeer.PAYMENT_TRANS_ID))
        {
                return getPaymentTransId();
            }
          if (name.equals(DebitMemoPeer.PAYMENT_TRANS_NO))
        {
                return getPaymentTransNo();
            }
          if (name.equals(DebitMemoPeer.PAYMENT_INV_ID))
        {
                return getPaymentInvId();
            }
          if (name.equals(DebitMemoPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(DebitMemoPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(DebitMemoPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(DebitMemoPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(DebitMemoPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(DebitMemoPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(DebitMemoPeer.CLOSING_RATE))
        {
                return getClosingRate();
            }
          if (name.equals(DebitMemoPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(DebitMemoPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(DebitMemoPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(DebitMemoPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(DebitMemoPeer.FROM_PAYMENT_ID))
        {
                return getFromPaymentId();
            }
          if (name.equals(DebitMemoPeer.CROSS_ACCOUNT_ID))
        {
                return getCrossAccountId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDebitMemoId();
            }
              if (pos == 1)
        {
                return getDebitMemoNo();
            }
              if (pos == 2)
        {
                return getTransactionDate();
            }
              if (pos == 3)
        {
                return getTransactionId();
            }
              if (pos == 4)
        {
                return getTransactionNo();
            }
              if (pos == 5)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 6)
        {
                return getVendorId();
            }
              if (pos == 7)
        {
                return getVendorName();
            }
              if (pos == 8)
        {
                return getCurrencyId();
            }
              if (pos == 9)
        {
                return getCurrencyRate();
            }
              if (pos == 10)
        {
                return getAmount();
            }
              if (pos == 11)
        {
                return getAmountBase();
            }
              if (pos == 12)
        {
                return getUserName();
            }
              if (pos == 13)
        {
                return getRemark();
            }
              if (pos == 14)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 15)
        {
                return getClosedDate();
            }
              if (pos == 16)
        {
                return getPaymentTransId();
            }
              if (pos == 17)
        {
                return getPaymentTransNo();
            }
              if (pos == 18)
        {
                return getPaymentInvId();
            }
              if (pos == 19)
        {
                return getBankId();
            }
              if (pos == 20)
        {
                return getBankIssuer();
            }
              if (pos == 21)
        {
                return getDueDate();
            }
              if (pos == 22)
        {
                return getReferenceNo();
            }
              if (pos == 23)
        {
                return getAccountId();
            }
              if (pos == 24)
        {
                return getFiscalRate();
            }
              if (pos == 25)
        {
                return getClosingRate();
            }
              if (pos == 26)
        {
                return getCashFlowTypeId();
            }
              if (pos == 27)
        {
                return getLocationId();
            }
              if (pos == 28)
        {
                return getCancelBy();
            }
              if (pos == 29)
        {
                return getCancelDate();
            }
              if (pos == 30)
        {
                return getFromPaymentId();
            }
              if (pos == 31)
        {
                return getCrossAccountId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DebitMemoPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DebitMemoPeer.doInsert((DebitMemo) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DebitMemoPeer.doUpdate((DebitMemo) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key debitMemoId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDebitMemoId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDebitMemoId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDebitMemoId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public DebitMemo copy() throws TorqueException
    {
        return copyInto(new DebitMemo());
    }
  
    protected DebitMemo copyInto(DebitMemo copyObj) throws TorqueException
    {
          copyObj.setDebitMemoId(debitMemoId);
          copyObj.setDebitMemoNo(debitMemoNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionType(transactionType);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setUserName(userName);
          copyObj.setRemark(remark);
          copyObj.setStatus(status);
          copyObj.setClosedDate(closedDate);
          copyObj.setPaymentTransId(paymentTransId);
          copyObj.setPaymentTransNo(paymentTransNo);
          copyObj.setPaymentInvId(paymentInvId);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setDueDate(dueDate);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setAccountId(accountId);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setClosingRate(closingRate);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setLocationId(locationId);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setFromPaymentId(fromPaymentId);
          copyObj.setCrossAccountId(crossAccountId);
  
                    copyObj.setDebitMemoId((String)null);
                                                                                                                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DebitMemoPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("DebitMemo\n");
        str.append("---------\n")
           .append("DebitMemoId          : ")
           .append(getDebitMemoId())
           .append("\n")
           .append("DebitMemoNo          : ")
           .append(getDebitMemoNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("ClosedDate           : ")
           .append(getClosedDate())
           .append("\n")
           .append("PaymentTransId       : ")
           .append(getPaymentTransId())
           .append("\n")
           .append("PaymentTransNo       : ")
           .append(getPaymentTransNo())
           .append("\n")
           .append("PaymentInvId         : ")
           .append(getPaymentInvId())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("ClosingRate          : ")
           .append(getClosingRate())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("FromPaymentId        : ")
           .append(getFromPaymentId())
           .append("\n")
           .append("CrossAccountId       : ")
           .append(getCrossAccountId())
           .append("\n")
        ;
        return(str.toString());
    }
}
