package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Discount
 */
public abstract class BaseDiscount extends BaseObject
{
    /** The Peer class */
    private static final DiscountPeer peer =
        new DiscountPeer();

        
    /** The value for the discountId field */
    private String discountId;
      
    /** The value for the promoCode field */
    private String promoCode;
      
    /** The value for the discountCode field */
    private String discountCode;
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                                
    /** The value for the itemId field */
    private String itemId = "";
                                                
    /** The value for the itemSku field */
    private String itemSku = "";
                                                
    /** The value for the kategoriId field */
    private String kategoriId = "";
      
    /** The value for the customerTypeId field */
    private String customerTypeId;
                                                
    /** The value for the customers field */
    private String customers = "";
                                                
    /** The value for the items field */
    private String items = "";
                                                
    /** The value for the brands field */
    private String brands = "";
                                                
    /** The value for the manufacturers field */
    private String manufacturers = "";
                                                
    /** The value for the tags field */
    private String tags = "";
                                                
    /** The value for the paymentTypeId field */
    private String paymentTypeId = "";
                                                
    /** The value for the cardPrefix field */
    private String cardPrefix = "";
                                                
    /** The value for the discountDays field */
    private String discountDays = "";
                                                
    /** The value for the totalDiscScript field */
    private String totalDiscScript = "";
                                                        
    /** The value for the isTotalDisc field */
    private boolean isTotalDisc = false;
                                                        
    /** The value for the happyHour field */
    private boolean happyHour = false;
                                                
    /** The value for the startHour field */
    private String startHour = "";
                                                
    /** The value for the endHour field */
    private String endHour = "";
      
    /** The value for the discountType field */
    private int discountType;
                                                
          
    /** The value for the qtyAmount1 field */
    private BigDecimal qtyAmount1= bd_ZERO;
                                                
          
    /** The value for the qtyAmount2 field */
    private BigDecimal qtyAmount2= bd_ZERO;
                                                
          
    /** The value for the qtyAmount3 field */
    private BigDecimal qtyAmount3= bd_ZERO;
                                                
          
    /** The value for the qtyAmount4 field */
    private BigDecimal qtyAmount4= bd_ZERO;
                                                
          
    /** The value for the qtyAmount5 field */
    private BigDecimal qtyAmount5= bd_ZERO;
                                                
    /** The value for the discountValue1 field */
    private String discountValue1 = "0";
                                                
    /** The value for the discountValue2 field */
    private String discountValue2 = "0";
                                                
    /** The value for the discountValue3 field */
    private String discountValue3 = "0";
                                                
    /** The value for the discountValue4 field */
    private String discountValue4 = "0";
                                                
    /** The value for the discountValue5 field */
    private String discountValue5 = "0";
      
    /** The value for the eventBeginDate field */
    private Date eventBeginDate;
      
    /** The value for the eventEndDate field */
    private Date eventEndDate;
                                                
    /** The value for the eventDiscount field */
    private String eventDiscount = "0";
                                                        
    /** The value for the perItem field */
    private boolean perItem = true;
                                                        
    /** The value for the multiply field */
    private boolean multiply = false;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the updateDate field */
    private Date updateDate;
                                                
    /** The value for the lastUpdateBy field */
    private String lastUpdateBy = "";
                                                
          
    /** The value for the buyQty field */
    private BigDecimal buyQty= bd_ZERO;
                                                
          
    /** The value for the getQty field */
    private BigDecimal getQty= bd_ZERO;
                                                        
    /** The value for the isCcDisc field */
    private boolean isCcDisc = false;
                                                
          
    /** The value for the ccDiscComp field */
    private BigDecimal ccDiscComp= bd_ZERO;
                                                
          
    /** The value for the ccDiscBank field */
    private BigDecimal ccDiscBank= bd_ZERO;
                                                
          
    /** The value for the ccMinPurch field */
    private BigDecimal ccMinPurch= bd_ZERO;
                                          
    /** The value for the couponType field */
    private int couponType = 1;
                                                
    /** The value for the couponDesc field */
    private String couponDesc = "";
                                                
          
    /** The value for the couponPurchAmt field */
    private BigDecimal couponPurchAmt= bd_ZERO;
                                                
          
    /** The value for the couponValue field */
    private BigDecimal couponValue= bd_ZERO;
                                          
    /** The value for the couponQty field */
    private int couponQty = 1;
                                                
    /** The value for the couponMessage field */
    private String couponMessage = "";
                                          
    /** The value for the couponValidDays field */
    private int couponValidDays = 1;
      
    /** The value for the couponValidFrom field */
    private Date couponValidFrom;
      
    /** The value for the couponValidTo field */
    private Date couponValidTo;
                                                
    /** The value for the discScript field */
    private String discScript = "";
  
    
    /**
     * Get the DiscountId
     *
     * @return String
     */
    public String getDiscountId()
    {
        return discountId;
    }

                        
    /**
     * Set the value of DiscountId
     *
     * @param v new value
     */
    public void setDiscountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountId, v))
              {
            this.discountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PromoCode
     *
     * @return String
     */
    public String getPromoCode()
    {
        return promoCode;
    }

                        
    /**
     * Set the value of PromoCode
     *
     * @param v new value
     */
    public void setPromoCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.promoCode, v))
              {
            this.promoCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountCode
     *
     * @return String
     */
    public String getDiscountCode()
    {
        return discountCode;
    }

                        
    /**
     * Set the value of DiscountCode
     *
     * @param v new value
     */
    public void setDiscountCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountCode, v))
              {
            this.discountCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemSku
     *
     * @return String
     */
    public String getItemSku()
    {
        return itemSku;
    }

                        
    /**
     * Set the value of ItemSku
     *
     * @param v new value
     */
    public void setItemSku(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemSku, v))
              {
            this.itemSku = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KategoriId
     *
     * @return String
     */
    public String getKategoriId()
    {
        return kategoriId;
    }

                        
    /**
     * Set the value of KategoriId
     *
     * @param v new value
     */
    public void setKategoriId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.kategoriId, v))
              {
            this.kategoriId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Customers
     *
     * @return String
     */
    public String getCustomers()
    {
        return customers;
    }

                        
    /**
     * Set the value of Customers
     *
     * @param v new value
     */
    public void setCustomers(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customers, v))
              {
            this.customers = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Items
     *
     * @return String
     */
    public String getItems()
    {
        return items;
    }

                        
    /**
     * Set the value of Items
     *
     * @param v new value
     */
    public void setItems(String v) 
    {
    
                  if (!ObjectUtils.equals(this.items, v))
              {
            this.items = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Brands
     *
     * @return String
     */
    public String getBrands()
    {
        return brands;
    }

                        
    /**
     * Set the value of Brands
     *
     * @param v new value
     */
    public void setBrands(String v) 
    {
    
                  if (!ObjectUtils.equals(this.brands, v))
              {
            this.brands = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Manufacturers
     *
     * @return String
     */
    public String getManufacturers()
    {
        return manufacturers;
    }

                        
    /**
     * Set the value of Manufacturers
     *
     * @param v new value
     */
    public void setManufacturers(String v) 
    {
    
                  if (!ObjectUtils.equals(this.manufacturers, v))
              {
            this.manufacturers = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Tags
     *
     * @return String
     */
    public String getTags()
    {
        return tags;
    }

                        
    /**
     * Set the value of Tags
     *
     * @param v new value
     */
    public void setTags(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tags, v))
              {
            this.tags = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CardPrefix
     *
     * @return String
     */
    public String getCardPrefix()
    {
        return cardPrefix;
    }

                        
    /**
     * Set the value of CardPrefix
     *
     * @param v new value
     */
    public void setCardPrefix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cardPrefix, v))
              {
            this.cardPrefix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountDays
     *
     * @return String
     */
    public String getDiscountDays()
    {
        return discountDays;
    }

                        
    /**
     * Set the value of DiscountDays
     *
     * @param v new value
     */
    public void setDiscountDays(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountDays, v))
              {
            this.discountDays = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscScript
     *
     * @return String
     */
    public String getTotalDiscScript()
    {
        return totalDiscScript;
    }

                        
    /**
     * Set the value of TotalDiscScript
     *
     * @param v new value
     */
    public void setTotalDiscScript(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscScript, v))
              {
            this.totalDiscScript = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTotalDisc
     *
     * @return boolean
     */
    public boolean getIsTotalDisc()
    {
        return isTotalDisc;
    }

                        
    /**
     * Set the value of IsTotalDisc
     *
     * @param v new value
     */
    public void setIsTotalDisc(boolean v) 
    {
    
                  if (this.isTotalDisc != v)
              {
            this.isTotalDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HappyHour
     *
     * @return boolean
     */
    public boolean getHappyHour()
    {
        return happyHour;
    }

                        
    /**
     * Set the value of HappyHour
     *
     * @param v new value
     */
    public void setHappyHour(boolean v) 
    {
    
                  if (this.happyHour != v)
              {
            this.happyHour = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartHour
     *
     * @return String
     */
    public String getStartHour()
    {
        return startHour;
    }

                        
    /**
     * Set the value of StartHour
     *
     * @param v new value
     */
    public void setStartHour(String v) 
    {
    
                  if (!ObjectUtils.equals(this.startHour, v))
              {
            this.startHour = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndHour
     *
     * @return String
     */
    public String getEndHour()
    {
        return endHour;
    }

                        
    /**
     * Set the value of EndHour
     *
     * @param v new value
     */
    public void setEndHour(String v) 
    {
    
                  if (!ObjectUtils.equals(this.endHour, v))
              {
            this.endHour = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountType
     *
     * @return int
     */
    public int getDiscountType()
    {
        return discountType;
    }

                        
    /**
     * Set the value of DiscountType
     *
     * @param v new value
     */
    public void setDiscountType(int v) 
    {
    
                  if (this.discountType != v)
              {
            this.discountType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyAmount1
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyAmount1()
    {
        return qtyAmount1;
    }

                        
    /**
     * Set the value of QtyAmount1
     *
     * @param v new value
     */
    public void setQtyAmount1(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyAmount1, v))
              {
            this.qtyAmount1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyAmount2
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyAmount2()
    {
        return qtyAmount2;
    }

                        
    /**
     * Set the value of QtyAmount2
     *
     * @param v new value
     */
    public void setQtyAmount2(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyAmount2, v))
              {
            this.qtyAmount2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyAmount3
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyAmount3()
    {
        return qtyAmount3;
    }

                        
    /**
     * Set the value of QtyAmount3
     *
     * @param v new value
     */
    public void setQtyAmount3(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyAmount3, v))
              {
            this.qtyAmount3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyAmount4
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyAmount4()
    {
        return qtyAmount4;
    }

                        
    /**
     * Set the value of QtyAmount4
     *
     * @param v new value
     */
    public void setQtyAmount4(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyAmount4, v))
              {
            this.qtyAmount4 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyAmount5
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyAmount5()
    {
        return qtyAmount5;
    }

                        
    /**
     * Set the value of QtyAmount5
     *
     * @param v new value
     */
    public void setQtyAmount5(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyAmount5, v))
              {
            this.qtyAmount5 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountValue1
     *
     * @return String
     */
    public String getDiscountValue1()
    {
        return discountValue1;
    }

                        
    /**
     * Set the value of DiscountValue1
     *
     * @param v new value
     */
    public void setDiscountValue1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountValue1, v))
              {
            this.discountValue1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountValue2
     *
     * @return String
     */
    public String getDiscountValue2()
    {
        return discountValue2;
    }

                        
    /**
     * Set the value of DiscountValue2
     *
     * @param v new value
     */
    public void setDiscountValue2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountValue2, v))
              {
            this.discountValue2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountValue3
     *
     * @return String
     */
    public String getDiscountValue3()
    {
        return discountValue3;
    }

                        
    /**
     * Set the value of DiscountValue3
     *
     * @param v new value
     */
    public void setDiscountValue3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountValue3, v))
              {
            this.discountValue3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountValue4
     *
     * @return String
     */
    public String getDiscountValue4()
    {
        return discountValue4;
    }

                        
    /**
     * Set the value of DiscountValue4
     *
     * @param v new value
     */
    public void setDiscountValue4(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountValue4, v))
              {
            this.discountValue4 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountValue5
     *
     * @return String
     */
    public String getDiscountValue5()
    {
        return discountValue5;
    }

                        
    /**
     * Set the value of DiscountValue5
     *
     * @param v new value
     */
    public void setDiscountValue5(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountValue5, v))
              {
            this.discountValue5 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EventBeginDate
     *
     * @return Date
     */
    public Date getEventBeginDate()
    {
        return eventBeginDate;
    }

                        
    /**
     * Set the value of EventBeginDate
     *
     * @param v new value
     */
    public void setEventBeginDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.eventBeginDate, v))
              {
            this.eventBeginDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EventEndDate
     *
     * @return Date
     */
    public Date getEventEndDate()
    {
        return eventEndDate;
    }

                        
    /**
     * Set the value of EventEndDate
     *
     * @param v new value
     */
    public void setEventEndDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.eventEndDate, v))
              {
            this.eventEndDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EventDiscount
     *
     * @return String
     */
    public String getEventDiscount()
    {
        return eventDiscount;
    }

                        
    /**
     * Set the value of EventDiscount
     *
     * @param v new value
     */
    public void setEventDiscount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.eventDiscount, v))
              {
            this.eventDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PerItem
     *
     * @return boolean
     */
    public boolean getPerItem()
    {
        return perItem;
    }

                        
    /**
     * Set the value of PerItem
     *
     * @param v new value
     */
    public void setPerItem(boolean v) 
    {
    
                  if (this.perItem != v)
              {
            this.perItem = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Multiply
     *
     * @return boolean
     */
    public boolean getMultiply()
    {
        return multiply;
    }

                        
    /**
     * Set the value of Multiply
     *
     * @param v new value
     */
    public void setMultiply(boolean v) 
    {
    
                  if (this.multiply != v)
              {
            this.multiply = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateBy
     *
     * @return String
     */
    public String getLastUpdateBy()
    {
        return lastUpdateBy;
    }

                        
    /**
     * Set the value of LastUpdateBy
     *
     * @param v new value
     */
    public void setLastUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateBy, v))
              {
            this.lastUpdateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BuyQty
     *
     * @return BigDecimal
     */
    public BigDecimal getBuyQty()
    {
        return buyQty;
    }

                        
    /**
     * Set the value of BuyQty
     *
     * @param v new value
     */
    public void setBuyQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.buyQty, v))
              {
            this.buyQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GetQty
     *
     * @return BigDecimal
     */
    public BigDecimal getGetQty()
    {
        return getQty;
    }

                        
    /**
     * Set the value of GetQty
     *
     * @param v new value
     */
    public void setGetQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.getQty, v))
              {
            this.getQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsCcDisc
     *
     * @return boolean
     */
    public boolean getIsCcDisc()
    {
        return isCcDisc;
    }

                        
    /**
     * Set the value of IsCcDisc
     *
     * @param v new value
     */
    public void setIsCcDisc(boolean v) 
    {
    
                  if (this.isCcDisc != v)
              {
            this.isCcDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CcDiscComp
     *
     * @return BigDecimal
     */
    public BigDecimal getCcDiscComp()
    {
        return ccDiscComp;
    }

                        
    /**
     * Set the value of CcDiscComp
     *
     * @param v new value
     */
    public void setCcDiscComp(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.ccDiscComp, v))
              {
            this.ccDiscComp = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CcDiscBank
     *
     * @return BigDecimal
     */
    public BigDecimal getCcDiscBank()
    {
        return ccDiscBank;
    }

                        
    /**
     * Set the value of CcDiscBank
     *
     * @param v new value
     */
    public void setCcDiscBank(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.ccDiscBank, v))
              {
            this.ccDiscBank = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CcMinPurch
     *
     * @return BigDecimal
     */
    public BigDecimal getCcMinPurch()
    {
        return ccMinPurch;
    }

                        
    /**
     * Set the value of CcMinPurch
     *
     * @param v new value
     */
    public void setCcMinPurch(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.ccMinPurch, v))
              {
            this.ccMinPurch = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponType
     *
     * @return int
     */
    public int getCouponType()
    {
        return couponType;
    }

                        
    /**
     * Set the value of CouponType
     *
     * @param v new value
     */
    public void setCouponType(int v) 
    {
    
                  if (this.couponType != v)
              {
            this.couponType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponDesc
     *
     * @return String
     */
    public String getCouponDesc()
    {
        return couponDesc;
    }

                        
    /**
     * Set the value of CouponDesc
     *
     * @param v new value
     */
    public void setCouponDesc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.couponDesc, v))
              {
            this.couponDesc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponPurchAmt
     *
     * @return BigDecimal
     */
    public BigDecimal getCouponPurchAmt()
    {
        return couponPurchAmt;
    }

                        
    /**
     * Set the value of CouponPurchAmt
     *
     * @param v new value
     */
    public void setCouponPurchAmt(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.couponPurchAmt, v))
              {
            this.couponPurchAmt = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponValue
     *
     * @return BigDecimal
     */
    public BigDecimal getCouponValue()
    {
        return couponValue;
    }

                        
    /**
     * Set the value of CouponValue
     *
     * @param v new value
     */
    public void setCouponValue(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.couponValue, v))
              {
            this.couponValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponQty
     *
     * @return int
     */
    public int getCouponQty()
    {
        return couponQty;
    }

                        
    /**
     * Set the value of CouponQty
     *
     * @param v new value
     */
    public void setCouponQty(int v) 
    {
    
                  if (this.couponQty != v)
              {
            this.couponQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponMessage
     *
     * @return String
     */
    public String getCouponMessage()
    {
        return couponMessage;
    }

                        
    /**
     * Set the value of CouponMessage
     *
     * @param v new value
     */
    public void setCouponMessage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.couponMessage, v))
              {
            this.couponMessage = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponValidDays
     *
     * @return int
     */
    public int getCouponValidDays()
    {
        return couponValidDays;
    }

                        
    /**
     * Set the value of CouponValidDays
     *
     * @param v new value
     */
    public void setCouponValidDays(int v) 
    {
    
                  if (this.couponValidDays != v)
              {
            this.couponValidDays = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponValidFrom
     *
     * @return Date
     */
    public Date getCouponValidFrom()
    {
        return couponValidFrom;
    }

                        
    /**
     * Set the value of CouponValidFrom
     *
     * @param v new value
     */
    public void setCouponValidFrom(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.couponValidFrom, v))
              {
            this.couponValidFrom = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponValidTo
     *
     * @return Date
     */
    public Date getCouponValidTo()
    {
        return couponValidTo;
    }

                        
    /**
     * Set the value of CouponValidTo
     *
     * @param v new value
     */
    public void setCouponValidTo(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.couponValidTo, v))
              {
            this.couponValidTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscScript
     *
     * @return String
     */
    public String getDiscScript()
    {
        return discScript;
    }

                        
    /**
     * Set the value of DiscScript
     *
     * @param v new value
     */
    public void setDiscScript(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discScript, v))
              {
            this.discScript = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DiscountId");
              fieldNames.add("PromoCode");
              fieldNames.add("DiscountCode");
              fieldNames.add("LocationId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemSku");
              fieldNames.add("KategoriId");
              fieldNames.add("CustomerTypeId");
              fieldNames.add("Customers");
              fieldNames.add("Items");
              fieldNames.add("Brands");
              fieldNames.add("Manufacturers");
              fieldNames.add("Tags");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("CardPrefix");
              fieldNames.add("DiscountDays");
              fieldNames.add("TotalDiscScript");
              fieldNames.add("IsTotalDisc");
              fieldNames.add("HappyHour");
              fieldNames.add("StartHour");
              fieldNames.add("EndHour");
              fieldNames.add("DiscountType");
              fieldNames.add("QtyAmount1");
              fieldNames.add("QtyAmount2");
              fieldNames.add("QtyAmount3");
              fieldNames.add("QtyAmount4");
              fieldNames.add("QtyAmount5");
              fieldNames.add("DiscountValue1");
              fieldNames.add("DiscountValue2");
              fieldNames.add("DiscountValue3");
              fieldNames.add("DiscountValue4");
              fieldNames.add("DiscountValue5");
              fieldNames.add("EventBeginDate");
              fieldNames.add("EventEndDate");
              fieldNames.add("EventDiscount");
              fieldNames.add("PerItem");
              fieldNames.add("Multiply");
              fieldNames.add("Description");
              fieldNames.add("UpdateDate");
              fieldNames.add("LastUpdateBy");
              fieldNames.add("BuyQty");
              fieldNames.add("GetQty");
              fieldNames.add("IsCcDisc");
              fieldNames.add("CcDiscComp");
              fieldNames.add("CcDiscBank");
              fieldNames.add("CcMinPurch");
              fieldNames.add("CouponType");
              fieldNames.add("CouponDesc");
              fieldNames.add("CouponPurchAmt");
              fieldNames.add("CouponValue");
              fieldNames.add("CouponQty");
              fieldNames.add("CouponMessage");
              fieldNames.add("CouponValidDays");
              fieldNames.add("CouponValidFrom");
              fieldNames.add("CouponValidTo");
              fieldNames.add("DiscScript");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DiscountId"))
        {
                return getDiscountId();
            }
          if (name.equals("PromoCode"))
        {
                return getPromoCode();
            }
          if (name.equals("DiscountCode"))
        {
                return getDiscountCode();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemSku"))
        {
                return getItemSku();
            }
          if (name.equals("KategoriId"))
        {
                return getKategoriId();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          if (name.equals("Customers"))
        {
                return getCustomers();
            }
          if (name.equals("Items"))
        {
                return getItems();
            }
          if (name.equals("Brands"))
        {
                return getBrands();
            }
          if (name.equals("Manufacturers"))
        {
                return getManufacturers();
            }
          if (name.equals("Tags"))
        {
                return getTags();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("CardPrefix"))
        {
                return getCardPrefix();
            }
          if (name.equals("DiscountDays"))
        {
                return getDiscountDays();
            }
          if (name.equals("TotalDiscScript"))
        {
                return getTotalDiscScript();
            }
          if (name.equals("IsTotalDisc"))
        {
                return Boolean.valueOf(getIsTotalDisc());
            }
          if (name.equals("HappyHour"))
        {
                return Boolean.valueOf(getHappyHour());
            }
          if (name.equals("StartHour"))
        {
                return getStartHour();
            }
          if (name.equals("EndHour"))
        {
                return getEndHour();
            }
          if (name.equals("DiscountType"))
        {
                return Integer.valueOf(getDiscountType());
            }
          if (name.equals("QtyAmount1"))
        {
                return getQtyAmount1();
            }
          if (name.equals("QtyAmount2"))
        {
                return getQtyAmount2();
            }
          if (name.equals("QtyAmount3"))
        {
                return getQtyAmount3();
            }
          if (name.equals("QtyAmount4"))
        {
                return getQtyAmount4();
            }
          if (name.equals("QtyAmount5"))
        {
                return getQtyAmount5();
            }
          if (name.equals("DiscountValue1"))
        {
                return getDiscountValue1();
            }
          if (name.equals("DiscountValue2"))
        {
                return getDiscountValue2();
            }
          if (name.equals("DiscountValue3"))
        {
                return getDiscountValue3();
            }
          if (name.equals("DiscountValue4"))
        {
                return getDiscountValue4();
            }
          if (name.equals("DiscountValue5"))
        {
                return getDiscountValue5();
            }
          if (name.equals("EventBeginDate"))
        {
                return getEventBeginDate();
            }
          if (name.equals("EventEndDate"))
        {
                return getEventEndDate();
            }
          if (name.equals("EventDiscount"))
        {
                return getEventDiscount();
            }
          if (name.equals("PerItem"))
        {
                return Boolean.valueOf(getPerItem());
            }
          if (name.equals("Multiply"))
        {
                return Boolean.valueOf(getMultiply());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LastUpdateBy"))
        {
                return getLastUpdateBy();
            }
          if (name.equals("BuyQty"))
        {
                return getBuyQty();
            }
          if (name.equals("GetQty"))
        {
                return getGetQty();
            }
          if (name.equals("IsCcDisc"))
        {
                return Boolean.valueOf(getIsCcDisc());
            }
          if (name.equals("CcDiscComp"))
        {
                return getCcDiscComp();
            }
          if (name.equals("CcDiscBank"))
        {
                return getCcDiscBank();
            }
          if (name.equals("CcMinPurch"))
        {
                return getCcMinPurch();
            }
          if (name.equals("CouponType"))
        {
                return Integer.valueOf(getCouponType());
            }
          if (name.equals("CouponDesc"))
        {
                return getCouponDesc();
            }
          if (name.equals("CouponPurchAmt"))
        {
                return getCouponPurchAmt();
            }
          if (name.equals("CouponValue"))
        {
                return getCouponValue();
            }
          if (name.equals("CouponQty"))
        {
                return Integer.valueOf(getCouponQty());
            }
          if (name.equals("CouponMessage"))
        {
                return getCouponMessage();
            }
          if (name.equals("CouponValidDays"))
        {
                return Integer.valueOf(getCouponValidDays());
            }
          if (name.equals("CouponValidFrom"))
        {
                return getCouponValidFrom();
            }
          if (name.equals("CouponValidTo"))
        {
                return getCouponValidTo();
            }
          if (name.equals("DiscScript"))
        {
                return getDiscScript();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DiscountPeer.DISCOUNT_ID))
        {
                return getDiscountId();
            }
          if (name.equals(DiscountPeer.PROMO_CODE))
        {
                return getPromoCode();
            }
          if (name.equals(DiscountPeer.DISCOUNT_CODE))
        {
                return getDiscountCode();
            }
          if (name.equals(DiscountPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(DiscountPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(DiscountPeer.ITEM_SKU))
        {
                return getItemSku();
            }
          if (name.equals(DiscountPeer.KATEGORI_ID))
        {
                return getKategoriId();
            }
          if (name.equals(DiscountPeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          if (name.equals(DiscountPeer.CUSTOMERS))
        {
                return getCustomers();
            }
          if (name.equals(DiscountPeer.ITEMS))
        {
                return getItems();
            }
          if (name.equals(DiscountPeer.BRANDS))
        {
                return getBrands();
            }
          if (name.equals(DiscountPeer.MANUFACTURERS))
        {
                return getManufacturers();
            }
          if (name.equals(DiscountPeer.TAGS))
        {
                return getTags();
            }
          if (name.equals(DiscountPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(DiscountPeer.CARD_PREFIX))
        {
                return getCardPrefix();
            }
          if (name.equals(DiscountPeer.DISCOUNT_DAYS))
        {
                return getDiscountDays();
            }
          if (name.equals(DiscountPeer.TOTAL_DISC_SCRIPT))
        {
                return getTotalDiscScript();
            }
          if (name.equals(DiscountPeer.IS_TOTAL_DISC))
        {
                return Boolean.valueOf(getIsTotalDisc());
            }
          if (name.equals(DiscountPeer.HAPPY_HOUR))
        {
                return Boolean.valueOf(getHappyHour());
            }
          if (name.equals(DiscountPeer.START_HOUR))
        {
                return getStartHour();
            }
          if (name.equals(DiscountPeer.END_HOUR))
        {
                return getEndHour();
            }
          if (name.equals(DiscountPeer.DISCOUNT_TYPE))
        {
                return Integer.valueOf(getDiscountType());
            }
          if (name.equals(DiscountPeer.QTY_AMOUNT_1))
        {
                return getQtyAmount1();
            }
          if (name.equals(DiscountPeer.QTY_AMOUNT_2))
        {
                return getQtyAmount2();
            }
          if (name.equals(DiscountPeer.QTY_AMOUNT_3))
        {
                return getQtyAmount3();
            }
          if (name.equals(DiscountPeer.QTY_AMOUNT_4))
        {
                return getQtyAmount4();
            }
          if (name.equals(DiscountPeer.QTY_AMOUNT_5))
        {
                return getQtyAmount5();
            }
          if (name.equals(DiscountPeer.DISCOUNT_VALUE_1))
        {
                return getDiscountValue1();
            }
          if (name.equals(DiscountPeer.DISCOUNT_VALUE_2))
        {
                return getDiscountValue2();
            }
          if (name.equals(DiscountPeer.DISCOUNT_VALUE_3))
        {
                return getDiscountValue3();
            }
          if (name.equals(DiscountPeer.DISCOUNT_VALUE_4))
        {
                return getDiscountValue4();
            }
          if (name.equals(DiscountPeer.DISCOUNT_VALUE_5))
        {
                return getDiscountValue5();
            }
          if (name.equals(DiscountPeer.EVENT_BEGIN_DATE))
        {
                return getEventBeginDate();
            }
          if (name.equals(DiscountPeer.EVENT_END_DATE))
        {
                return getEventEndDate();
            }
          if (name.equals(DiscountPeer.EVENT_DISCOUNT))
        {
                return getEventDiscount();
            }
          if (name.equals(DiscountPeer.PER_ITEM))
        {
                return Boolean.valueOf(getPerItem());
            }
          if (name.equals(DiscountPeer.MULTIPLY))
        {
                return Boolean.valueOf(getMultiply());
            }
          if (name.equals(DiscountPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(DiscountPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(DiscountPeer.LAST_UPDATE_BY))
        {
                return getLastUpdateBy();
            }
          if (name.equals(DiscountPeer.BUY_QTY))
        {
                return getBuyQty();
            }
          if (name.equals(DiscountPeer.GET_QTY))
        {
                return getGetQty();
            }
          if (name.equals(DiscountPeer.IS_CC_DISC))
        {
                return Boolean.valueOf(getIsCcDisc());
            }
          if (name.equals(DiscountPeer.CC_DISC_COMP))
        {
                return getCcDiscComp();
            }
          if (name.equals(DiscountPeer.CC_DISC_BANK))
        {
                return getCcDiscBank();
            }
          if (name.equals(DiscountPeer.CC_MIN_PURCH))
        {
                return getCcMinPurch();
            }
          if (name.equals(DiscountPeer.COUPON_TYPE))
        {
                return Integer.valueOf(getCouponType());
            }
          if (name.equals(DiscountPeer.COUPON_DESC))
        {
                return getCouponDesc();
            }
          if (name.equals(DiscountPeer.COUPON_PURCH_AMT))
        {
                return getCouponPurchAmt();
            }
          if (name.equals(DiscountPeer.COUPON_VALUE))
        {
                return getCouponValue();
            }
          if (name.equals(DiscountPeer.COUPON_QTY))
        {
                return Integer.valueOf(getCouponQty());
            }
          if (name.equals(DiscountPeer.COUPON_MESSAGE))
        {
                return getCouponMessage();
            }
          if (name.equals(DiscountPeer.COUPON_VALID_DAYS))
        {
                return Integer.valueOf(getCouponValidDays());
            }
          if (name.equals(DiscountPeer.COUPON_VALID_FROM))
        {
                return getCouponValidFrom();
            }
          if (name.equals(DiscountPeer.COUPON_VALID_TO))
        {
                return getCouponValidTo();
            }
          if (name.equals(DiscountPeer.DISC_SCRIPT))
        {
                return getDiscScript();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDiscountId();
            }
              if (pos == 1)
        {
                return getPromoCode();
            }
              if (pos == 2)
        {
                return getDiscountCode();
            }
              if (pos == 3)
        {
                return getLocationId();
            }
              if (pos == 4)
        {
                return getItemId();
            }
              if (pos == 5)
        {
                return getItemSku();
            }
              if (pos == 6)
        {
                return getKategoriId();
            }
              if (pos == 7)
        {
                return getCustomerTypeId();
            }
              if (pos == 8)
        {
                return getCustomers();
            }
              if (pos == 9)
        {
                return getItems();
            }
              if (pos == 10)
        {
                return getBrands();
            }
              if (pos == 11)
        {
                return getManufacturers();
            }
              if (pos == 12)
        {
                return getTags();
            }
              if (pos == 13)
        {
                return getPaymentTypeId();
            }
              if (pos == 14)
        {
                return getCardPrefix();
            }
              if (pos == 15)
        {
                return getDiscountDays();
            }
              if (pos == 16)
        {
                return getTotalDiscScript();
            }
              if (pos == 17)
        {
                return Boolean.valueOf(getIsTotalDisc());
            }
              if (pos == 18)
        {
                return Boolean.valueOf(getHappyHour());
            }
              if (pos == 19)
        {
                return getStartHour();
            }
              if (pos == 20)
        {
                return getEndHour();
            }
              if (pos == 21)
        {
                return Integer.valueOf(getDiscountType());
            }
              if (pos == 22)
        {
                return getQtyAmount1();
            }
              if (pos == 23)
        {
                return getQtyAmount2();
            }
              if (pos == 24)
        {
                return getQtyAmount3();
            }
              if (pos == 25)
        {
                return getQtyAmount4();
            }
              if (pos == 26)
        {
                return getQtyAmount5();
            }
              if (pos == 27)
        {
                return getDiscountValue1();
            }
              if (pos == 28)
        {
                return getDiscountValue2();
            }
              if (pos == 29)
        {
                return getDiscountValue3();
            }
              if (pos == 30)
        {
                return getDiscountValue4();
            }
              if (pos == 31)
        {
                return getDiscountValue5();
            }
              if (pos == 32)
        {
                return getEventBeginDate();
            }
              if (pos == 33)
        {
                return getEventEndDate();
            }
              if (pos == 34)
        {
                return getEventDiscount();
            }
              if (pos == 35)
        {
                return Boolean.valueOf(getPerItem());
            }
              if (pos == 36)
        {
                return Boolean.valueOf(getMultiply());
            }
              if (pos == 37)
        {
                return getDescription();
            }
              if (pos == 38)
        {
                return getUpdateDate();
            }
              if (pos == 39)
        {
                return getLastUpdateBy();
            }
              if (pos == 40)
        {
                return getBuyQty();
            }
              if (pos == 41)
        {
                return getGetQty();
            }
              if (pos == 42)
        {
                return Boolean.valueOf(getIsCcDisc());
            }
              if (pos == 43)
        {
                return getCcDiscComp();
            }
              if (pos == 44)
        {
                return getCcDiscBank();
            }
              if (pos == 45)
        {
                return getCcMinPurch();
            }
              if (pos == 46)
        {
                return Integer.valueOf(getCouponType());
            }
              if (pos == 47)
        {
                return getCouponDesc();
            }
              if (pos == 48)
        {
                return getCouponPurchAmt();
            }
              if (pos == 49)
        {
                return getCouponValue();
            }
              if (pos == 50)
        {
                return Integer.valueOf(getCouponQty());
            }
              if (pos == 51)
        {
                return getCouponMessage();
            }
              if (pos == 52)
        {
                return Integer.valueOf(getCouponValidDays());
            }
              if (pos == 53)
        {
                return getCouponValidFrom();
            }
              if (pos == 54)
        {
                return getCouponValidTo();
            }
              if (pos == 55)
        {
                return getDiscScript();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DiscountPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DiscountPeer.doInsert((Discount) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DiscountPeer.doUpdate((Discount) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key discountId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDiscountId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDiscountId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDiscountId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Discount copy() throws TorqueException
    {
        return copyInto(new Discount());
    }
  
    protected Discount copyInto(Discount copyObj) throws TorqueException
    {
          copyObj.setDiscountId(discountId);
          copyObj.setPromoCode(promoCode);
          copyObj.setDiscountCode(discountCode);
          copyObj.setLocationId(locationId);
          copyObj.setItemId(itemId);
          copyObj.setItemSku(itemSku);
          copyObj.setKategoriId(kategoriId);
          copyObj.setCustomerTypeId(customerTypeId);
          copyObj.setCustomers(customers);
          copyObj.setItems(items);
          copyObj.setBrands(brands);
          copyObj.setManufacturers(manufacturers);
          copyObj.setTags(tags);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setCardPrefix(cardPrefix);
          copyObj.setDiscountDays(discountDays);
          copyObj.setTotalDiscScript(totalDiscScript);
          copyObj.setIsTotalDisc(isTotalDisc);
          copyObj.setHappyHour(happyHour);
          copyObj.setStartHour(startHour);
          copyObj.setEndHour(endHour);
          copyObj.setDiscountType(discountType);
          copyObj.setQtyAmount1(qtyAmount1);
          copyObj.setQtyAmount2(qtyAmount2);
          copyObj.setQtyAmount3(qtyAmount3);
          copyObj.setQtyAmount4(qtyAmount4);
          copyObj.setQtyAmount5(qtyAmount5);
          copyObj.setDiscountValue1(discountValue1);
          copyObj.setDiscountValue2(discountValue2);
          copyObj.setDiscountValue3(discountValue3);
          copyObj.setDiscountValue4(discountValue4);
          copyObj.setDiscountValue5(discountValue5);
          copyObj.setEventBeginDate(eventBeginDate);
          copyObj.setEventEndDate(eventEndDate);
          copyObj.setEventDiscount(eventDiscount);
          copyObj.setPerItem(perItem);
          copyObj.setMultiply(multiply);
          copyObj.setDescription(description);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLastUpdateBy(lastUpdateBy);
          copyObj.setBuyQty(buyQty);
          copyObj.setGetQty(getQty);
          copyObj.setIsCcDisc(isCcDisc);
          copyObj.setCcDiscComp(ccDiscComp);
          copyObj.setCcDiscBank(ccDiscBank);
          copyObj.setCcMinPurch(ccMinPurch);
          copyObj.setCouponType(couponType);
          copyObj.setCouponDesc(couponDesc);
          copyObj.setCouponPurchAmt(couponPurchAmt);
          copyObj.setCouponValue(couponValue);
          copyObj.setCouponQty(couponQty);
          copyObj.setCouponMessage(couponMessage);
          copyObj.setCouponValidDays(couponValidDays);
          copyObj.setCouponValidFrom(couponValidFrom);
          copyObj.setCouponValidTo(couponValidTo);
          copyObj.setDiscScript(discScript);
  
                    copyObj.setDiscountId((String)null);
                                                                                                                                                                                                                                                                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DiscountPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Discount\n");
        str.append("--------\n")
           .append("DiscountId           : ")
           .append(getDiscountId())
           .append("\n")
           .append("PromoCode            : ")
           .append(getPromoCode())
           .append("\n")
           .append("DiscountCode         : ")
           .append(getDiscountCode())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemSku              : ")
           .append(getItemSku())
           .append("\n")
           .append("KategoriId           : ")
           .append(getKategoriId())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
           .append("Customers            : ")
           .append(getCustomers())
           .append("\n")
           .append("Items                : ")
           .append(getItems())
           .append("\n")
           .append("Brands               : ")
           .append(getBrands())
           .append("\n")
           .append("Manufacturers        : ")
           .append(getManufacturers())
           .append("\n")
           .append("Tags                 : ")
           .append(getTags())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("CardPrefix           : ")
           .append(getCardPrefix())
           .append("\n")
           .append("DiscountDays         : ")
           .append(getDiscountDays())
           .append("\n")
           .append("TotalDiscScript      : ")
           .append(getTotalDiscScript())
           .append("\n")
           .append("IsTotalDisc          : ")
           .append(getIsTotalDisc())
           .append("\n")
           .append("HappyHour            : ")
           .append(getHappyHour())
           .append("\n")
           .append("StartHour            : ")
           .append(getStartHour())
           .append("\n")
           .append("EndHour              : ")
           .append(getEndHour())
           .append("\n")
           .append("DiscountType         : ")
           .append(getDiscountType())
           .append("\n")
           .append("QtyAmount1           : ")
           .append(getQtyAmount1())
           .append("\n")
           .append("QtyAmount2           : ")
           .append(getQtyAmount2())
           .append("\n")
           .append("QtyAmount3           : ")
           .append(getQtyAmount3())
           .append("\n")
           .append("QtyAmount4           : ")
           .append(getQtyAmount4())
           .append("\n")
           .append("QtyAmount5           : ")
           .append(getQtyAmount5())
           .append("\n")
           .append("DiscountValue1       : ")
           .append(getDiscountValue1())
           .append("\n")
           .append("DiscountValue2       : ")
           .append(getDiscountValue2())
           .append("\n")
           .append("DiscountValue3       : ")
           .append(getDiscountValue3())
           .append("\n")
           .append("DiscountValue4       : ")
           .append(getDiscountValue4())
           .append("\n")
           .append("DiscountValue5       : ")
           .append(getDiscountValue5())
           .append("\n")
           .append("EventBeginDate       : ")
           .append(getEventBeginDate())
           .append("\n")
           .append("EventEndDate         : ")
           .append(getEventEndDate())
           .append("\n")
           .append("EventDiscount        : ")
           .append(getEventDiscount())
           .append("\n")
           .append("PerItem              : ")
           .append(getPerItem())
           .append("\n")
           .append("Multiply             : ")
           .append(getMultiply())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LastUpdateBy         : ")
           .append(getLastUpdateBy())
           .append("\n")
           .append("BuyQty               : ")
           .append(getBuyQty())
           .append("\n")
           .append("GetQty               : ")
           .append(getGetQty())
           .append("\n")
           .append("IsCcDisc             : ")
           .append(getIsCcDisc())
           .append("\n")
           .append("CcDiscComp           : ")
           .append(getCcDiscComp())
           .append("\n")
           .append("CcDiscBank           : ")
           .append(getCcDiscBank())
           .append("\n")
           .append("CcMinPurch           : ")
           .append(getCcMinPurch())
           .append("\n")
           .append("CouponType           : ")
           .append(getCouponType())
           .append("\n")
           .append("CouponDesc           : ")
           .append(getCouponDesc())
           .append("\n")
           .append("CouponPurchAmt       : ")
           .append(getCouponPurchAmt())
           .append("\n")
           .append("CouponValue          : ")
           .append(getCouponValue())
           .append("\n")
           .append("CouponQty            : ")
           .append(getCouponQty())
           .append("\n")
           .append("CouponMessage        : ")
           .append(getCouponMessage())
           .append("\n")
           .append("CouponValidDays      : ")
           .append(getCouponValidDays())
           .append("\n")
           .append("CouponValidFrom      : ")
           .append(getCouponValidFrom())
           .append("\n")
           .append("CouponValidTo        : ")
           .append(getCouponValidTo())
           .append("\n")
           .append("DiscScript           : ")
           .append(getDiscScript())
           .append("\n")
        ;
        return(str.toString());
    }
}
