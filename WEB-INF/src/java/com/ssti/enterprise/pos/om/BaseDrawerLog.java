package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to DrawerLog
 */
public abstract class BaseDrawerLog extends BaseObject
{
    /** The Peer class */
    private static final DrawerLogPeer peer =
        new DrawerLogPeer();

        
    /** The value for the drawerLogId field */
    private String drawerLogId;
      
    /** The value for the openDate field */
    private Date openDate;
      
    /** The value for the userName field */
    private String userName;
                                          
    /** The value for the accessType field */
    private int accessType = 0;
                                                
          
    /** The value for the amount field */
    private BigDecimal amount= bd_ZERO;
      
    /** The value for the reason field */
    private String reason;
  
    
    /**
     * Get the DrawerLogId
     *
     * @return String
     */
    public String getDrawerLogId()
    {
        return drawerLogId;
    }

                        
    /**
     * Set the value of DrawerLogId
     *
     * @param v new value
     */
    public void setDrawerLogId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.drawerLogId, v))
              {
            this.drawerLogId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpenDate
     *
     * @return Date
     */
    public Date getOpenDate()
    {
        return openDate;
    }

                        
    /**
     * Set the value of OpenDate
     *
     * @param v new value
     */
    public void setOpenDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.openDate, v))
              {
            this.openDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccessType
     *
     * @return int
     */
    public int getAccessType()
    {
        return accessType;
    }

                        
    /**
     * Set the value of AccessType
     *
     * @param v new value
     */
    public void setAccessType(int v) 
    {
    
                  if (this.accessType != v)
              {
            this.accessType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Reason
     *
     * @return String
     */
    public String getReason()
    {
        return reason;
    }

                        
    /**
     * Set the value of Reason
     *
     * @param v new value
     */
    public void setReason(String v) 
    {
    
                  if (!ObjectUtils.equals(this.reason, v))
              {
            this.reason = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DrawerLogId");
              fieldNames.add("OpenDate");
              fieldNames.add("UserName");
              fieldNames.add("AccessType");
              fieldNames.add("Amount");
              fieldNames.add("Reason");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DrawerLogId"))
        {
                return getDrawerLogId();
            }
          if (name.equals("OpenDate"))
        {
                return getOpenDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("AccessType"))
        {
                return Integer.valueOf(getAccessType());
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("Reason"))
        {
                return getReason();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DrawerLogPeer.DRAWER_LOG_ID))
        {
                return getDrawerLogId();
            }
          if (name.equals(DrawerLogPeer.OPEN_DATE))
        {
                return getOpenDate();
            }
          if (name.equals(DrawerLogPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(DrawerLogPeer.ACCESS_TYPE))
        {
                return Integer.valueOf(getAccessType());
            }
          if (name.equals(DrawerLogPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(DrawerLogPeer.REASON))
        {
                return getReason();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDrawerLogId();
            }
              if (pos == 1)
        {
                return getOpenDate();
            }
              if (pos == 2)
        {
                return getUserName();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getAccessType());
            }
              if (pos == 4)
        {
                return getAmount();
            }
              if (pos == 5)
        {
                return getReason();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DrawerLogPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DrawerLogPeer.doInsert((DrawerLog) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DrawerLogPeer.doUpdate((DrawerLog) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key drawerLogId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDrawerLogId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDrawerLogId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDrawerLogId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public DrawerLog copy() throws TorqueException
    {
        return copyInto(new DrawerLog());
    }
  
    protected DrawerLog copyInto(DrawerLog copyObj) throws TorqueException
    {
          copyObj.setDrawerLogId(drawerLogId);
          copyObj.setOpenDate(openDate);
          copyObj.setUserName(userName);
          copyObj.setAccessType(accessType);
          copyObj.setAmount(amount);
          copyObj.setReason(reason);
  
                    copyObj.setDrawerLogId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DrawerLogPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("DrawerLog\n");
        str.append("---------\n")
           .append("DrawerLogId          : ")
           .append(getDrawerLogId())
           .append("\n")
           .append("OpenDate             : ")
           .append(getOpenDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("AccessType           : ")
           .append(getAccessType())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("Reason               : ")
           .append(getReason())
           .append("\n")
        ;
        return(str.toString());
    }
}
