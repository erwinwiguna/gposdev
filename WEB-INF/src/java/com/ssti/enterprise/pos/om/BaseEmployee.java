package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Employee
 */
public abstract class BaseEmployee extends BaseObject
{
    /** The Peer class */
    private static final EmployeePeer peer =
        new EmployeePeer();

        
    /** The value for the employeeId field */
    private String employeeId;
      
    /** The value for the employeeCode field */
    private String employeeCode;
      
    /** The value for the employeeName field */
    private String employeeName;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the jobTitle field */
    private String jobTitle;
      
    /** The value for the gender field */
    private int gender;
      
    /** The value for the birthPlace field */
    private String birthPlace;
      
    /** The value for the birthDate field */
    private Date birthDate;
      
    /** The value for the startDate field */
    private Date startDate;
      
    /** The value for the quitDate field */
    private Date quitDate;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the address field */
    private String address;
      
    /** The value for the zip field */
    private String zip;
      
    /** The value for the country field */
    private String country;
                                                
    /** The value for the province field */
    private String province = "";
                                                
    /** The value for the city field */
    private String city = "";
                                                
    /** The value for the district field */
    private String district = "";
                                                
    /** The value for the village field */
    private String village = "";
      
    /** The value for the homePhone field */
    private String homePhone;
      
    /** The value for the mobilePhone field */
    private String mobilePhone;
      
    /** The value for the email field */
    private String email;
                                                
          
    /** The value for the salesCommisionPct field */
    private BigDecimal salesCommisionPct= bd_ZERO;
                                                
          
    /** The value for the marginCommisionPct field */
    private BigDecimal marginCommisionPct= bd_ZERO;
                                                
          
    /** The value for the monthlySalary field */
    private BigDecimal monthlySalary= bd_ZERO;
                                                
          
    /** The value for the otherIncome field */
    private BigDecimal otherIncome= bd_ZERO;
                                          
    /** The value for the frequency field */
    private int frequency = 0;
      
    /** The value for the memo field */
    private String memo;
      
    /** The value for the addDate field */
    private Date addDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the roleId field */
    private int roleId;
                                                
    /** The value for the field1 field */
    private String field1 = "";
                                                
    /** The value for the field2 field */
    private String field2 = "";
                                                
    /** The value for the value1 field */
    private String value1 = "";
                                                
    /** The value for the value2 field */
    private String value2 = "";
                                                        
    /** The value for the isActive field */
    private boolean isActive = true;
                                                        
    /** The value for the isSalesman field */
    private boolean isSalesman = false;
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
                                                
    /** The value for the parentId field */
    private String parentId = "";
                                                        
    /** The value for the hasChild field */
    private boolean hasChild = false;
                                          
    /** The value for the employeeLevel field */
    private int employeeLevel = 1;
                                                
    /** The value for the salesAreaId field */
    private String salesAreaId = "";
                                                
    /** The value for the salesArea1Id field */
    private String salesArea1Id = "";
                                                
    /** The value for the salesArea2Id field */
    private String salesArea2Id = "";
                                                
    /** The value for the salesArea3Id field */
    private String salesArea3Id = "";
                                                
    /** The value for the territoryId field */
    private String territoryId = "";
                                                
    /** The value for the territory1Id field */
    private String territory1Id = "";
                                                
    /** The value for the territory2Id field */
    private String territory2Id = "";
                                                
    /** The value for the territory3Id field */
    private String territory3Id = "";
  
    
    /**
     * Get the EmployeeId
     *
     * @return String
     */
    public String getEmployeeId()
    {
        return employeeId;
    }

                        
    /**
     * Set the value of EmployeeId
     *
     * @param v new value
     */
    public void setEmployeeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.employeeId, v))
              {
            this.employeeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmployeeCode
     *
     * @return String
     */
    public String getEmployeeCode()
    {
        return employeeCode;
    }

                        
    /**
     * Set the value of EmployeeCode
     *
     * @param v new value
     */
    public void setEmployeeCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.employeeCode, v))
              {
            this.employeeCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmployeeName
     *
     * @return String
     */
    public String getEmployeeName()
    {
        return employeeName;
    }

                        
    /**
     * Set the value of EmployeeName
     *
     * @param v new value
     */
    public void setEmployeeName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.employeeName, v))
              {
            this.employeeName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle
     *
     * @return String
     */
    public String getJobTitle()
    {
        return jobTitle;
    }

                        
    /**
     * Set the value of JobTitle
     *
     * @param v new value
     */
    public void setJobTitle(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle, v))
              {
            this.jobTitle = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Gender
     *
     * @return int
     */
    public int getGender()
    {
        return gender;
    }

                        
    /**
     * Set the value of Gender
     *
     * @param v new value
     */
    public void setGender(int v) 
    {
    
                  if (this.gender != v)
              {
            this.gender = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BirthPlace
     *
     * @return String
     */
    public String getBirthPlace()
    {
        return birthPlace;
    }

                        
    /**
     * Set the value of BirthPlace
     *
     * @param v new value
     */
    public void setBirthPlace(String v) 
    {
    
                  if (!ObjectUtils.equals(this.birthPlace, v))
              {
            this.birthPlace = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BirthDate
     *
     * @return Date
     */
    public Date getBirthDate()
    {
        return birthDate;
    }

                        
    /**
     * Set the value of BirthDate
     *
     * @param v new value
     */
    public void setBirthDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.birthDate, v))
              {
            this.birthDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartDate
     *
     * @return Date
     */
    public Date getStartDate()
    {
        return startDate;
    }

                        
    /**
     * Set the value of StartDate
     *
     * @param v new value
     */
    public void setStartDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.startDate, v))
              {
            this.startDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QuitDate
     *
     * @return Date
     */
    public Date getQuitDate()
    {
        return quitDate;
    }

                        
    /**
     * Set the value of QuitDate
     *
     * @param v new value
     */
    public void setQuitDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.quitDate, v))
              {
            this.quitDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

                        
    /**
     * Set the value of Address
     *
     * @param v new value
     */
    public void setAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.address, v))
              {
            this.address = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Zip
     *
     * @return String
     */
    public String getZip()
    {
        return zip;
    }

                        
    /**
     * Set the value of Zip
     *
     * @param v new value
     */
    public void setZip(String v) 
    {
    
                  if (!ObjectUtils.equals(this.zip, v))
              {
            this.zip = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Country
     *
     * @return String
     */
    public String getCountry()
    {
        return country;
    }

                        
    /**
     * Set the value of Country
     *
     * @param v new value
     */
    public void setCountry(String v) 
    {
    
                  if (!ObjectUtils.equals(this.country, v))
              {
            this.country = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Province
     *
     * @return String
     */
    public String getProvince()
    {
        return province;
    }

                        
    /**
     * Set the value of Province
     *
     * @param v new value
     */
    public void setProvince(String v) 
    {
    
                  if (!ObjectUtils.equals(this.province, v))
              {
            this.province = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the City
     *
     * @return String
     */
    public String getCity()
    {
        return city;
    }

                        
    /**
     * Set the value of City
     *
     * @param v new value
     */
    public void setCity(String v) 
    {
    
                  if (!ObjectUtils.equals(this.city, v))
              {
            this.city = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the District
     *
     * @return String
     */
    public String getDistrict()
    {
        return district;
    }

                        
    /**
     * Set the value of District
     *
     * @param v new value
     */
    public void setDistrict(String v) 
    {
    
                  if (!ObjectUtils.equals(this.district, v))
              {
            this.district = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Village
     *
     * @return String
     */
    public String getVillage()
    {
        return village;
    }

                        
    /**
     * Set the value of Village
     *
     * @param v new value
     */
    public void setVillage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.village, v))
              {
            this.village = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HomePhone
     *
     * @return String
     */
    public String getHomePhone()
    {
        return homePhone;
    }

                        
    /**
     * Set the value of HomePhone
     *
     * @param v new value
     */
    public void setHomePhone(String v) 
    {
    
                  if (!ObjectUtils.equals(this.homePhone, v))
              {
            this.homePhone = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MobilePhone
     *
     * @return String
     */
    public String getMobilePhone()
    {
        return mobilePhone;
    }

                        
    /**
     * Set the value of MobilePhone
     *
     * @param v new value
     */
    public void setMobilePhone(String v) 
    {
    
                  if (!ObjectUtils.equals(this.mobilePhone, v))
              {
            this.mobilePhone = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

                        
    /**
     * Set the value of Email
     *
     * @param v new value
     */
    public void setEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.email, v))
              {
            this.email = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesCommisionPct
     *
     * @return BigDecimal
     */
    public BigDecimal getSalesCommisionPct()
    {
        return salesCommisionPct;
    }

                        
    /**
     * Set the value of SalesCommisionPct
     *
     * @param v new value
     */
    public void setSalesCommisionPct(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.salesCommisionPct, v))
              {
            this.salesCommisionPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MarginCommisionPct
     *
     * @return BigDecimal
     */
    public BigDecimal getMarginCommisionPct()
    {
        return marginCommisionPct;
    }

                        
    /**
     * Set the value of MarginCommisionPct
     *
     * @param v new value
     */
    public void setMarginCommisionPct(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.marginCommisionPct, v))
              {
            this.marginCommisionPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MonthlySalary
     *
     * @return BigDecimal
     */
    public BigDecimal getMonthlySalary()
    {
        return monthlySalary;
    }

                        
    /**
     * Set the value of MonthlySalary
     *
     * @param v new value
     */
    public void setMonthlySalary(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.monthlySalary, v))
              {
            this.monthlySalary = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OtherIncome
     *
     * @return BigDecimal
     */
    public BigDecimal getOtherIncome()
    {
        return otherIncome;
    }

                        
    /**
     * Set the value of OtherIncome
     *
     * @param v new value
     */
    public void setOtherIncome(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.otherIncome, v))
              {
            this.otherIncome = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Frequency
     *
     * @return int
     */
    public int getFrequency()
    {
        return frequency;
    }

                        
    /**
     * Set the value of Frequency
     *
     * @param v new value
     */
    public void setFrequency(int v) 
    {
    
                  if (this.frequency != v)
              {
            this.frequency = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Memo
     *
     * @return String
     */
    public String getMemo()
    {
        return memo;
    }

                        
    /**
     * Set the value of Memo
     *
     * @param v new value
     */
    public void setMemo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memo, v))
              {
            this.memo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AddDate
     *
     * @return Date
     */
    public Date getAddDate()
    {
        return addDate;
    }

                        
    /**
     * Set the value of AddDate
     *
     * @param v new value
     */
    public void setAddDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.addDate, v))
              {
            this.addDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoleId
     *
     * @return int
     */
    public int getRoleId()
    {
        return roleId;
    }

                        
    /**
     * Set the value of RoleId
     *
     * @param v new value
     */
    public void setRoleId(int v) 
    {
    
                  if (this.roleId != v)
              {
            this.roleId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field1
     *
     * @return String
     */
    public String getField1()
    {
        return field1;
    }

                        
    /**
     * Set the value of Field1
     *
     * @param v new value
     */
    public void setField1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field1, v))
              {
            this.field1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field2
     *
     * @return String
     */
    public String getField2()
    {
        return field2;
    }

                        
    /**
     * Set the value of Field2
     *
     * @param v new value
     */
    public void setField2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field2, v))
              {
            this.field2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value1
     *
     * @return String
     */
    public String getValue1()
    {
        return value1;
    }

                        
    /**
     * Set the value of Value1
     *
     * @param v new value
     */
    public void setValue1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value1, v))
              {
            this.value1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value2
     *
     * @return String
     */
    public String getValue2()
    {
        return value2;
    }

                        
    /**
     * Set the value of Value2
     *
     * @param v new value
     */
    public void setValue2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value2, v))
              {
            this.value2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsActive
     *
     * @return boolean
     */
    public boolean getIsActive()
    {
        return isActive;
    }

                        
    /**
     * Set the value of IsActive
     *
     * @param v new value
     */
    public void setIsActive(boolean v) 
    {
    
                  if (this.isActive != v)
              {
            this.isActive = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsSalesman
     *
     * @return boolean
     */
    public boolean getIsSalesman()
    {
        return isSalesman;
    }

                        
    /**
     * Set the value of IsSalesman
     *
     * @param v new value
     */
    public void setIsSalesman(boolean v) 
    {
    
                  if (this.isSalesman != v)
              {
            this.isSalesman = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ParentId
     *
     * @return String
     */
    public String getParentId()
    {
        return parentId;
    }

                        
    /**
     * Set the value of ParentId
     *
     * @param v new value
     */
    public void setParentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.parentId, v))
              {
            this.parentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HasChild
     *
     * @return boolean
     */
    public boolean getHasChild()
    {
        return hasChild;
    }

                        
    /**
     * Set the value of HasChild
     *
     * @param v new value
     */
    public void setHasChild(boolean v) 
    {
    
                  if (this.hasChild != v)
              {
            this.hasChild = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmployeeLevel
     *
     * @return int
     */
    public int getEmployeeLevel()
    {
        return employeeLevel;
    }

                        
    /**
     * Set the value of EmployeeLevel
     *
     * @param v new value
     */
    public void setEmployeeLevel(int v) 
    {
    
                  if (this.employeeLevel != v)
              {
            this.employeeLevel = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAreaId
     *
     * @return String
     */
    public String getSalesAreaId()
    {
        return salesAreaId;
    }

                        
    /**
     * Set the value of SalesAreaId
     *
     * @param v new value
     */
    public void setSalesAreaId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAreaId, v))
              {
            this.salesAreaId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesArea1Id
     *
     * @return String
     */
    public String getSalesArea1Id()
    {
        return salesArea1Id;
    }

                        
    /**
     * Set the value of SalesArea1Id
     *
     * @param v new value
     */
    public void setSalesArea1Id(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesArea1Id, v))
              {
            this.salesArea1Id = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesArea2Id
     *
     * @return String
     */
    public String getSalesArea2Id()
    {
        return salesArea2Id;
    }

                        
    /**
     * Set the value of SalesArea2Id
     *
     * @param v new value
     */
    public void setSalesArea2Id(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesArea2Id, v))
              {
            this.salesArea2Id = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesArea3Id
     *
     * @return String
     */
    public String getSalesArea3Id()
    {
        return salesArea3Id;
    }

                        
    /**
     * Set the value of SalesArea3Id
     *
     * @param v new value
     */
    public void setSalesArea3Id(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesArea3Id, v))
              {
            this.salesArea3Id = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TerritoryId
     *
     * @return String
     */
    public String getTerritoryId()
    {
        return territoryId;
    }

                        
    /**
     * Set the value of TerritoryId
     *
     * @param v new value
     */
    public void setTerritoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territoryId, v))
              {
            this.territoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Territory1Id
     *
     * @return String
     */
    public String getTerritory1Id()
    {
        return territory1Id;
    }

                        
    /**
     * Set the value of Territory1Id
     *
     * @param v new value
     */
    public void setTerritory1Id(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territory1Id, v))
              {
            this.territory1Id = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Territory2Id
     *
     * @return String
     */
    public String getTerritory2Id()
    {
        return territory2Id;
    }

                        
    /**
     * Set the value of Territory2Id
     *
     * @param v new value
     */
    public void setTerritory2Id(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territory2Id, v))
              {
            this.territory2Id = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Territory3Id
     *
     * @return String
     */
    public String getTerritory3Id()
    {
        return territory3Id;
    }

                        
    /**
     * Set the value of Territory3Id
     *
     * @param v new value
     */
    public void setTerritory3Id(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territory3Id, v))
              {
            this.territory3Id = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("EmployeeId");
              fieldNames.add("EmployeeCode");
              fieldNames.add("EmployeeName");
              fieldNames.add("UserName");
              fieldNames.add("JobTitle");
              fieldNames.add("Gender");
              fieldNames.add("BirthPlace");
              fieldNames.add("BirthDate");
              fieldNames.add("StartDate");
              fieldNames.add("QuitDate");
              fieldNames.add("LocationId");
              fieldNames.add("Address");
              fieldNames.add("Zip");
              fieldNames.add("Country");
              fieldNames.add("Province");
              fieldNames.add("City");
              fieldNames.add("District");
              fieldNames.add("Village");
              fieldNames.add("HomePhone");
              fieldNames.add("MobilePhone");
              fieldNames.add("Email");
              fieldNames.add("SalesCommisionPct");
              fieldNames.add("MarginCommisionPct");
              fieldNames.add("MonthlySalary");
              fieldNames.add("OtherIncome");
              fieldNames.add("Frequency");
              fieldNames.add("Memo");
              fieldNames.add("AddDate");
              fieldNames.add("UpdateDate");
              fieldNames.add("RoleId");
              fieldNames.add("Field1");
              fieldNames.add("Field2");
              fieldNames.add("Value1");
              fieldNames.add("Value2");
              fieldNames.add("IsActive");
              fieldNames.add("IsSalesman");
              fieldNames.add("DepartmentId");
              fieldNames.add("ParentId");
              fieldNames.add("HasChild");
              fieldNames.add("EmployeeLevel");
              fieldNames.add("SalesAreaId");
              fieldNames.add("SalesArea1Id");
              fieldNames.add("SalesArea2Id");
              fieldNames.add("SalesArea3Id");
              fieldNames.add("TerritoryId");
              fieldNames.add("Territory1Id");
              fieldNames.add("Territory2Id");
              fieldNames.add("Territory3Id");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("EmployeeId"))
        {
                return getEmployeeId();
            }
          if (name.equals("EmployeeCode"))
        {
                return getEmployeeCode();
            }
          if (name.equals("EmployeeName"))
        {
                return getEmployeeName();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("JobTitle"))
        {
                return getJobTitle();
            }
          if (name.equals("Gender"))
        {
                return Integer.valueOf(getGender());
            }
          if (name.equals("BirthPlace"))
        {
                return getBirthPlace();
            }
          if (name.equals("BirthDate"))
        {
                return getBirthDate();
            }
          if (name.equals("StartDate"))
        {
                return getStartDate();
            }
          if (name.equals("QuitDate"))
        {
                return getQuitDate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("Address"))
        {
                return getAddress();
            }
          if (name.equals("Zip"))
        {
                return getZip();
            }
          if (name.equals("Country"))
        {
                return getCountry();
            }
          if (name.equals("Province"))
        {
                return getProvince();
            }
          if (name.equals("City"))
        {
                return getCity();
            }
          if (name.equals("District"))
        {
                return getDistrict();
            }
          if (name.equals("Village"))
        {
                return getVillage();
            }
          if (name.equals("HomePhone"))
        {
                return getHomePhone();
            }
          if (name.equals("MobilePhone"))
        {
                return getMobilePhone();
            }
          if (name.equals("Email"))
        {
                return getEmail();
            }
          if (name.equals("SalesCommisionPct"))
        {
                return getSalesCommisionPct();
            }
          if (name.equals("MarginCommisionPct"))
        {
                return getMarginCommisionPct();
            }
          if (name.equals("MonthlySalary"))
        {
                return getMonthlySalary();
            }
          if (name.equals("OtherIncome"))
        {
                return getOtherIncome();
            }
          if (name.equals("Frequency"))
        {
                return Integer.valueOf(getFrequency());
            }
          if (name.equals("Memo"))
        {
                return getMemo();
            }
          if (name.equals("AddDate"))
        {
                return getAddDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("RoleId"))
        {
                return Integer.valueOf(getRoleId());
            }
          if (name.equals("Field1"))
        {
                return getField1();
            }
          if (name.equals("Field2"))
        {
                return getField2();
            }
          if (name.equals("Value1"))
        {
                return getValue1();
            }
          if (name.equals("Value2"))
        {
                return getValue2();
            }
          if (name.equals("IsActive"))
        {
                return Boolean.valueOf(getIsActive());
            }
          if (name.equals("IsSalesman"))
        {
                return Boolean.valueOf(getIsSalesman());
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ParentId"))
        {
                return getParentId();
            }
          if (name.equals("HasChild"))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals("EmployeeLevel"))
        {
                return Integer.valueOf(getEmployeeLevel());
            }
          if (name.equals("SalesAreaId"))
        {
                return getSalesAreaId();
            }
          if (name.equals("SalesArea1Id"))
        {
                return getSalesArea1Id();
            }
          if (name.equals("SalesArea2Id"))
        {
                return getSalesArea2Id();
            }
          if (name.equals("SalesArea3Id"))
        {
                return getSalesArea3Id();
            }
          if (name.equals("TerritoryId"))
        {
                return getTerritoryId();
            }
          if (name.equals("Territory1Id"))
        {
                return getTerritory1Id();
            }
          if (name.equals("Territory2Id"))
        {
                return getTerritory2Id();
            }
          if (name.equals("Territory3Id"))
        {
                return getTerritory3Id();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(EmployeePeer.EMPLOYEE_ID))
        {
                return getEmployeeId();
            }
          if (name.equals(EmployeePeer.EMPLOYEE_CODE))
        {
                return getEmployeeCode();
            }
          if (name.equals(EmployeePeer.EMPLOYEE_NAME))
        {
                return getEmployeeName();
            }
          if (name.equals(EmployeePeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(EmployeePeer.JOB_TITLE))
        {
                return getJobTitle();
            }
          if (name.equals(EmployeePeer.GENDER))
        {
                return Integer.valueOf(getGender());
            }
          if (name.equals(EmployeePeer.BIRTH_PLACE))
        {
                return getBirthPlace();
            }
          if (name.equals(EmployeePeer.BIRTH_DATE))
        {
                return getBirthDate();
            }
          if (name.equals(EmployeePeer.START_DATE))
        {
                return getStartDate();
            }
          if (name.equals(EmployeePeer.QUIT_DATE))
        {
                return getQuitDate();
            }
          if (name.equals(EmployeePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(EmployeePeer.ADDRESS))
        {
                return getAddress();
            }
          if (name.equals(EmployeePeer.ZIP))
        {
                return getZip();
            }
          if (name.equals(EmployeePeer.COUNTRY))
        {
                return getCountry();
            }
          if (name.equals(EmployeePeer.PROVINCE))
        {
                return getProvince();
            }
          if (name.equals(EmployeePeer.CITY))
        {
                return getCity();
            }
          if (name.equals(EmployeePeer.DISTRICT))
        {
                return getDistrict();
            }
          if (name.equals(EmployeePeer.VILLAGE))
        {
                return getVillage();
            }
          if (name.equals(EmployeePeer.HOME_PHONE))
        {
                return getHomePhone();
            }
          if (name.equals(EmployeePeer.MOBILE_PHONE))
        {
                return getMobilePhone();
            }
          if (name.equals(EmployeePeer.EMAIL))
        {
                return getEmail();
            }
          if (name.equals(EmployeePeer.SALES_COMMISION_PCT))
        {
                return getSalesCommisionPct();
            }
          if (name.equals(EmployeePeer.MARGIN_COMMISION_PCT))
        {
                return getMarginCommisionPct();
            }
          if (name.equals(EmployeePeer.MONTHLY_SALARY))
        {
                return getMonthlySalary();
            }
          if (name.equals(EmployeePeer.OTHER_INCOME))
        {
                return getOtherIncome();
            }
          if (name.equals(EmployeePeer.FREQUENCY))
        {
                return Integer.valueOf(getFrequency());
            }
          if (name.equals(EmployeePeer.MEMO))
        {
                return getMemo();
            }
          if (name.equals(EmployeePeer.ADD_DATE))
        {
                return getAddDate();
            }
          if (name.equals(EmployeePeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(EmployeePeer.ROLE_ID))
        {
                return Integer.valueOf(getRoleId());
            }
          if (name.equals(EmployeePeer.FIELD1))
        {
                return getField1();
            }
          if (name.equals(EmployeePeer.FIELD2))
        {
                return getField2();
            }
          if (name.equals(EmployeePeer.VALUE1))
        {
                return getValue1();
            }
          if (name.equals(EmployeePeer.VALUE2))
        {
                return getValue2();
            }
          if (name.equals(EmployeePeer.IS_ACTIVE))
        {
                return Boolean.valueOf(getIsActive());
            }
          if (name.equals(EmployeePeer.IS_SALESMAN))
        {
                return Boolean.valueOf(getIsSalesman());
            }
          if (name.equals(EmployeePeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(EmployeePeer.PARENT_ID))
        {
                return getParentId();
            }
          if (name.equals(EmployeePeer.HAS_CHILD))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals(EmployeePeer.EMPLOYEE_LEVEL))
        {
                return Integer.valueOf(getEmployeeLevel());
            }
          if (name.equals(EmployeePeer.SALES_AREA_ID))
        {
                return getSalesAreaId();
            }
          if (name.equals(EmployeePeer.SALES_AREA1_ID))
        {
                return getSalesArea1Id();
            }
          if (name.equals(EmployeePeer.SALES_AREA2_ID))
        {
                return getSalesArea2Id();
            }
          if (name.equals(EmployeePeer.SALES_AREA3_ID))
        {
                return getSalesArea3Id();
            }
          if (name.equals(EmployeePeer.TERRITORY_ID))
        {
                return getTerritoryId();
            }
          if (name.equals(EmployeePeer.TERRITORY1_ID))
        {
                return getTerritory1Id();
            }
          if (name.equals(EmployeePeer.TERRITORY2_ID))
        {
                return getTerritory2Id();
            }
          if (name.equals(EmployeePeer.TERRITORY3_ID))
        {
                return getTerritory3Id();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getEmployeeId();
            }
              if (pos == 1)
        {
                return getEmployeeCode();
            }
              if (pos == 2)
        {
                return getEmployeeName();
            }
              if (pos == 3)
        {
                return getUserName();
            }
              if (pos == 4)
        {
                return getJobTitle();
            }
              if (pos == 5)
        {
                return Integer.valueOf(getGender());
            }
              if (pos == 6)
        {
                return getBirthPlace();
            }
              if (pos == 7)
        {
                return getBirthDate();
            }
              if (pos == 8)
        {
                return getStartDate();
            }
              if (pos == 9)
        {
                return getQuitDate();
            }
              if (pos == 10)
        {
                return getLocationId();
            }
              if (pos == 11)
        {
                return getAddress();
            }
              if (pos == 12)
        {
                return getZip();
            }
              if (pos == 13)
        {
                return getCountry();
            }
              if (pos == 14)
        {
                return getProvince();
            }
              if (pos == 15)
        {
                return getCity();
            }
              if (pos == 16)
        {
                return getDistrict();
            }
              if (pos == 17)
        {
                return getVillage();
            }
              if (pos == 18)
        {
                return getHomePhone();
            }
              if (pos == 19)
        {
                return getMobilePhone();
            }
              if (pos == 20)
        {
                return getEmail();
            }
              if (pos == 21)
        {
                return getSalesCommisionPct();
            }
              if (pos == 22)
        {
                return getMarginCommisionPct();
            }
              if (pos == 23)
        {
                return getMonthlySalary();
            }
              if (pos == 24)
        {
                return getOtherIncome();
            }
              if (pos == 25)
        {
                return Integer.valueOf(getFrequency());
            }
              if (pos == 26)
        {
                return getMemo();
            }
              if (pos == 27)
        {
                return getAddDate();
            }
              if (pos == 28)
        {
                return getUpdateDate();
            }
              if (pos == 29)
        {
                return Integer.valueOf(getRoleId());
            }
              if (pos == 30)
        {
                return getField1();
            }
              if (pos == 31)
        {
                return getField2();
            }
              if (pos == 32)
        {
                return getValue1();
            }
              if (pos == 33)
        {
                return getValue2();
            }
              if (pos == 34)
        {
                return Boolean.valueOf(getIsActive());
            }
              if (pos == 35)
        {
                return Boolean.valueOf(getIsSalesman());
            }
              if (pos == 36)
        {
                return getDepartmentId();
            }
              if (pos == 37)
        {
                return getParentId();
            }
              if (pos == 38)
        {
                return Boolean.valueOf(getHasChild());
            }
              if (pos == 39)
        {
                return Integer.valueOf(getEmployeeLevel());
            }
              if (pos == 40)
        {
                return getSalesAreaId();
            }
              if (pos == 41)
        {
                return getSalesArea1Id();
            }
              if (pos == 42)
        {
                return getSalesArea2Id();
            }
              if (pos == 43)
        {
                return getSalesArea3Id();
            }
              if (pos == 44)
        {
                return getTerritoryId();
            }
              if (pos == 45)
        {
                return getTerritory1Id();
            }
              if (pos == 46)
        {
                return getTerritory2Id();
            }
              if (pos == 47)
        {
                return getTerritory3Id();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(EmployeePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        EmployeePeer.doInsert((Employee) this, con);
                        setNew(false);
                    }
                    else
                    {
                        EmployeePeer.doUpdate((Employee) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key employeeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setEmployeeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setEmployeeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getEmployeeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Employee copy() throws TorqueException
    {
        return copyInto(new Employee());
    }
  
    protected Employee copyInto(Employee copyObj) throws TorqueException
    {
          copyObj.setEmployeeId(employeeId);
          copyObj.setEmployeeCode(employeeCode);
          copyObj.setEmployeeName(employeeName);
          copyObj.setUserName(userName);
          copyObj.setJobTitle(jobTitle);
          copyObj.setGender(gender);
          copyObj.setBirthPlace(birthPlace);
          copyObj.setBirthDate(birthDate);
          copyObj.setStartDate(startDate);
          copyObj.setQuitDate(quitDate);
          copyObj.setLocationId(locationId);
          copyObj.setAddress(address);
          copyObj.setZip(zip);
          copyObj.setCountry(country);
          copyObj.setProvince(province);
          copyObj.setCity(city);
          copyObj.setDistrict(district);
          copyObj.setVillage(village);
          copyObj.setHomePhone(homePhone);
          copyObj.setMobilePhone(mobilePhone);
          copyObj.setEmail(email);
          copyObj.setSalesCommisionPct(salesCommisionPct);
          copyObj.setMarginCommisionPct(marginCommisionPct);
          copyObj.setMonthlySalary(monthlySalary);
          copyObj.setOtherIncome(otherIncome);
          copyObj.setFrequency(frequency);
          copyObj.setMemo(memo);
          copyObj.setAddDate(addDate);
          copyObj.setUpdateDate(updateDate);
          copyObj.setRoleId(roleId);
          copyObj.setField1(field1);
          copyObj.setField2(field2);
          copyObj.setValue1(value1);
          copyObj.setValue2(value2);
          copyObj.setIsActive(isActive);
          copyObj.setIsSalesman(isSalesman);
          copyObj.setDepartmentId(departmentId);
          copyObj.setParentId(parentId);
          copyObj.setHasChild(hasChild);
          copyObj.setEmployeeLevel(employeeLevel);
          copyObj.setSalesAreaId(salesAreaId);
          copyObj.setSalesArea1Id(salesArea1Id);
          copyObj.setSalesArea2Id(salesArea2Id);
          copyObj.setSalesArea3Id(salesArea3Id);
          copyObj.setTerritoryId(territoryId);
          copyObj.setTerritory1Id(territory1Id);
          copyObj.setTerritory2Id(territory2Id);
          copyObj.setTerritory3Id(territory3Id);
  
                    copyObj.setEmployeeId((String)null);
                                                                                                                                                                                                                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public EmployeePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Employee\n");
        str.append("--------\n")
           .append("EmployeeId           : ")
           .append(getEmployeeId())
           .append("\n")
           .append("EmployeeCode         : ")
           .append(getEmployeeCode())
           .append("\n")
           .append("EmployeeName         : ")
           .append(getEmployeeName())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("JobTitle             : ")
           .append(getJobTitle())
           .append("\n")
           .append("Gender               : ")
           .append(getGender())
           .append("\n")
           .append("BirthPlace           : ")
           .append(getBirthPlace())
           .append("\n")
           .append("BirthDate            : ")
           .append(getBirthDate())
           .append("\n")
           .append("StartDate            : ")
           .append(getStartDate())
           .append("\n")
           .append("QuitDate             : ")
           .append(getQuitDate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("Address              : ")
           .append(getAddress())
           .append("\n")
           .append("Zip                  : ")
           .append(getZip())
           .append("\n")
           .append("Country              : ")
           .append(getCountry())
           .append("\n")
           .append("Province             : ")
           .append(getProvince())
           .append("\n")
           .append("City                 : ")
           .append(getCity())
           .append("\n")
           .append("District             : ")
           .append(getDistrict())
           .append("\n")
           .append("Village              : ")
           .append(getVillage())
           .append("\n")
           .append("HomePhone            : ")
           .append(getHomePhone())
           .append("\n")
           .append("MobilePhone          : ")
           .append(getMobilePhone())
           .append("\n")
           .append("Email                : ")
           .append(getEmail())
           .append("\n")
           .append("SalesCommisionPct    : ")
           .append(getSalesCommisionPct())
           .append("\n")
           .append("MarginCommisionPct   : ")
           .append(getMarginCommisionPct())
           .append("\n")
           .append("MonthlySalary        : ")
           .append(getMonthlySalary())
           .append("\n")
           .append("OtherIncome          : ")
           .append(getOtherIncome())
           .append("\n")
           .append("Frequency            : ")
           .append(getFrequency())
           .append("\n")
           .append("Memo                 : ")
           .append(getMemo())
           .append("\n")
           .append("AddDate              : ")
           .append(getAddDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("RoleId               : ")
           .append(getRoleId())
           .append("\n")
           .append("Field1               : ")
           .append(getField1())
           .append("\n")
           .append("Field2               : ")
           .append(getField2())
           .append("\n")
           .append("Value1               : ")
           .append(getValue1())
           .append("\n")
           .append("Value2               : ")
           .append(getValue2())
           .append("\n")
           .append("IsActive             : ")
           .append(getIsActive())
           .append("\n")
           .append("IsSalesman           : ")
           .append(getIsSalesman())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ParentId             : ")
           .append(getParentId())
           .append("\n")
           .append("HasChild             : ")
           .append(getHasChild())
           .append("\n")
           .append("EmployeeLevel        : ")
           .append(getEmployeeLevel())
           .append("\n")
           .append("SalesAreaId          : ")
           .append(getSalesAreaId())
           .append("\n")
           .append("SalesArea1Id         : ")
           .append(getSalesArea1Id())
           .append("\n")
           .append("SalesArea2Id         : ")
           .append(getSalesArea2Id())
           .append("\n")
           .append("SalesArea3Id         : ")
           .append(getSalesArea3Id())
           .append("\n")
           .append("TerritoryId          : ")
           .append(getTerritoryId())
           .append("\n")
           .append("Territory1Id         : ")
           .append(getTerritory1Id())
           .append("\n")
           .append("Territory2Id         : ")
           .append(getTerritory2Id())
           .append("\n")
           .append("Territory3Id         : ")
           .append(getTerritory3Id())
           .append("\n")
        ;
        return(str.toString());
    }
}
