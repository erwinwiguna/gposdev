package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.GlConfigMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseGlConfigPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "gl_config";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(GlConfigMapBuilder.CLASS_NAME);
    }

      /** the column name for the GL_CONFIG_ID field */
    public static final String GL_CONFIG_ID;
      /** the column name for the DIRECT_SALES_ACCOUNT field */
    public static final String DIRECT_SALES_ACCOUNT;
      /** the column name for the DEFAULT_FREIGHT_ACCOUNT field */
    public static final String DEFAULT_FREIGHT_ACCOUNT;
      /** the column name for the INVENTORY_ACCOUNT field */
    public static final String INVENTORY_ACCOUNT;
      /** the column name for the SALES_ACCOUNT field */
    public static final String SALES_ACCOUNT;
      /** the column name for the SALES_RETURN_ACCOUNT field */
    public static final String SALES_RETURN_ACCOUNT;
      /** the column name for the ITEM_DISCOUNT_ACCOUNT field */
    public static final String ITEM_DISCOUNT_ACCOUNT;
      /** the column name for the COGS_ACCOUNT field */
    public static final String COGS_ACCOUNT;
      /** the column name for the PURCHASE_RETURN_ACCOUNT field */
    public static final String PURCHASE_RETURN_ACCOUNT;
      /** the column name for the EXPENSE_ACCOUNT field */
    public static final String EXPENSE_ACCOUNT;
      /** the column name for the UNBILLED_GOODS_ACCOUNT field */
    public static final String UNBILLED_GOODS_ACCOUNT;
      /** the column name for the OPENING_BALANCE field */
    public static final String OPENING_BALANCE;
      /** the column name for the RETAINED_EARNING field */
    public static final String RETAINED_EARNING;
      /** the column name for the CREDIT_MEMO_ACCOUNT field */
    public static final String CREDIT_MEMO_ACCOUNT;
      /** the column name for the DEBIT_MEMO_ACCOUNT field */
    public static final String DEBIT_MEMO_ACCOUNT;
      /** the column name for the DELIVERY_ORDER_ACCOUNT field */
    public static final String DELIVERY_ORDER_ACCOUNT;
      /** the column name for the POS_ROUNDING_ACCOUNT field */
    public static final String POS_ROUNDING_ACCOUNT;
      /** the column name for the AR_PAYMENT_TEMP field */
    public static final String AR_PAYMENT_TEMP;
      /** the column name for the AP_PAYMENT_TEMP field */
    public static final String AP_PAYMENT_TEMP;
      /** the column name for the PR_PI_VARIANCE field */
    public static final String PR_PI_VARIANCE;
  
    static
    {
          GL_CONFIG_ID = "gl_config.GL_CONFIG_ID";
          DIRECT_SALES_ACCOUNT = "gl_config.DIRECT_SALES_ACCOUNT";
          DEFAULT_FREIGHT_ACCOUNT = "gl_config.DEFAULT_FREIGHT_ACCOUNT";
          INVENTORY_ACCOUNT = "gl_config.INVENTORY_ACCOUNT";
          SALES_ACCOUNT = "gl_config.SALES_ACCOUNT";
          SALES_RETURN_ACCOUNT = "gl_config.SALES_RETURN_ACCOUNT";
          ITEM_DISCOUNT_ACCOUNT = "gl_config.ITEM_DISCOUNT_ACCOUNT";
          COGS_ACCOUNT = "gl_config.COGS_ACCOUNT";
          PURCHASE_RETURN_ACCOUNT = "gl_config.PURCHASE_RETURN_ACCOUNT";
          EXPENSE_ACCOUNT = "gl_config.EXPENSE_ACCOUNT";
          UNBILLED_GOODS_ACCOUNT = "gl_config.UNBILLED_GOODS_ACCOUNT";
          OPENING_BALANCE = "gl_config.OPENING_BALANCE";
          RETAINED_EARNING = "gl_config.RETAINED_EARNING";
          CREDIT_MEMO_ACCOUNT = "gl_config.CREDIT_MEMO_ACCOUNT";
          DEBIT_MEMO_ACCOUNT = "gl_config.DEBIT_MEMO_ACCOUNT";
          DELIVERY_ORDER_ACCOUNT = "gl_config.DELIVERY_ORDER_ACCOUNT";
          POS_ROUNDING_ACCOUNT = "gl_config.POS_ROUNDING_ACCOUNT";
          AR_PAYMENT_TEMP = "gl_config.AR_PAYMENT_TEMP";
          AP_PAYMENT_TEMP = "gl_config.AP_PAYMENT_TEMP";
          PR_PI_VARIANCE = "gl_config.PR_PI_VARIANCE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(GlConfigMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(GlConfigMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  20;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.GlConfig";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseGlConfigPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(GL_CONFIG_ID);
          criteria.addSelectColumn(DIRECT_SALES_ACCOUNT);
          criteria.addSelectColumn(DEFAULT_FREIGHT_ACCOUNT);
          criteria.addSelectColumn(INVENTORY_ACCOUNT);
          criteria.addSelectColumn(SALES_ACCOUNT);
          criteria.addSelectColumn(SALES_RETURN_ACCOUNT);
          criteria.addSelectColumn(ITEM_DISCOUNT_ACCOUNT);
          criteria.addSelectColumn(COGS_ACCOUNT);
          criteria.addSelectColumn(PURCHASE_RETURN_ACCOUNT);
          criteria.addSelectColumn(EXPENSE_ACCOUNT);
          criteria.addSelectColumn(UNBILLED_GOODS_ACCOUNT);
          criteria.addSelectColumn(OPENING_BALANCE);
          criteria.addSelectColumn(RETAINED_EARNING);
          criteria.addSelectColumn(CREDIT_MEMO_ACCOUNT);
          criteria.addSelectColumn(DEBIT_MEMO_ACCOUNT);
          criteria.addSelectColumn(DELIVERY_ORDER_ACCOUNT);
          criteria.addSelectColumn(POS_ROUNDING_ACCOUNT);
          criteria.addSelectColumn(AR_PAYMENT_TEMP);
          criteria.addSelectColumn(AP_PAYMENT_TEMP);
          criteria.addSelectColumn(PR_PI_VARIANCE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static GlConfig row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            GlConfig obj = (GlConfig) cls.newInstance();
            GlConfigPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      GlConfig obj)
        throws TorqueException
    {
        try
        {
                obj.setGlConfigId(row.getValue(offset + 0).asString());
                  obj.setDirectSalesAccount(row.getValue(offset + 1).asString());
                  obj.setDefaultFreightAccount(row.getValue(offset + 2).asString());
                  obj.setInventoryAccount(row.getValue(offset + 3).asString());
                  obj.setSalesAccount(row.getValue(offset + 4).asString());
                  obj.setSalesReturnAccount(row.getValue(offset + 5).asString());
                  obj.setItemDiscountAccount(row.getValue(offset + 6).asString());
                  obj.setCogsAccount(row.getValue(offset + 7).asString());
                  obj.setPurchaseReturnAccount(row.getValue(offset + 8).asString());
                  obj.setExpenseAccount(row.getValue(offset + 9).asString());
                  obj.setUnbilledGoodsAccount(row.getValue(offset + 10).asString());
                  obj.setOpeningBalance(row.getValue(offset + 11).asString());
                  obj.setRetainedEarning(row.getValue(offset + 12).asString());
                  obj.setCreditMemoAccount(row.getValue(offset + 13).asString());
                  obj.setDebitMemoAccount(row.getValue(offset + 14).asString());
                  obj.setDeliveryOrderAccount(row.getValue(offset + 15).asString());
                  obj.setPosRoundingAccount(row.getValue(offset + 16).asString());
                  obj.setArPaymentTemp(row.getValue(offset + 17).asString());
                  obj.setApPaymentTemp(row.getValue(offset + 18).asString());
                  obj.setPrPiVariance(row.getValue(offset + 19).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseGlConfigPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                          
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(GlConfigPeer.row2Object(row, 1,
                GlConfigPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseGlConfigPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(GL_CONFIG_ID, criteria.remove(GL_CONFIG_ID));
                                                                                                                                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         GlConfigPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(GlConfig obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(GlConfig obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(GlConfig obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(GlConfig obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(GlConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(GlConfig obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(GlConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(GlConfig obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(GlConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(GlConfig obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseGlConfigPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(GL_CONFIG_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( GlConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(GL_CONFIG_ID, obj.getGlConfigId());
              criteria.add(DIRECT_SALES_ACCOUNT, obj.getDirectSalesAccount());
              criteria.add(DEFAULT_FREIGHT_ACCOUNT, obj.getDefaultFreightAccount());
              criteria.add(INVENTORY_ACCOUNT, obj.getInventoryAccount());
              criteria.add(SALES_ACCOUNT, obj.getSalesAccount());
              criteria.add(SALES_RETURN_ACCOUNT, obj.getSalesReturnAccount());
              criteria.add(ITEM_DISCOUNT_ACCOUNT, obj.getItemDiscountAccount());
              criteria.add(COGS_ACCOUNT, obj.getCogsAccount());
              criteria.add(PURCHASE_RETURN_ACCOUNT, obj.getPurchaseReturnAccount());
              criteria.add(EXPENSE_ACCOUNT, obj.getExpenseAccount());
              criteria.add(UNBILLED_GOODS_ACCOUNT, obj.getUnbilledGoodsAccount());
              criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
              criteria.add(RETAINED_EARNING, obj.getRetainedEarning());
              criteria.add(CREDIT_MEMO_ACCOUNT, obj.getCreditMemoAccount());
              criteria.add(DEBIT_MEMO_ACCOUNT, obj.getDebitMemoAccount());
              criteria.add(DELIVERY_ORDER_ACCOUNT, obj.getDeliveryOrderAccount());
              criteria.add(POS_ROUNDING_ACCOUNT, obj.getPosRoundingAccount());
              criteria.add(AR_PAYMENT_TEMP, obj.getArPaymentTemp());
              criteria.add(AP_PAYMENT_TEMP, obj.getApPaymentTemp());
              criteria.add(PR_PI_VARIANCE, obj.getPrPiVariance());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( GlConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(GL_CONFIG_ID, obj.getGlConfigId());
                          criteria.add(DIRECT_SALES_ACCOUNT, obj.getDirectSalesAccount());
                          criteria.add(DEFAULT_FREIGHT_ACCOUNT, obj.getDefaultFreightAccount());
                          criteria.add(INVENTORY_ACCOUNT, obj.getInventoryAccount());
                          criteria.add(SALES_ACCOUNT, obj.getSalesAccount());
                          criteria.add(SALES_RETURN_ACCOUNT, obj.getSalesReturnAccount());
                          criteria.add(ITEM_DISCOUNT_ACCOUNT, obj.getItemDiscountAccount());
                          criteria.add(COGS_ACCOUNT, obj.getCogsAccount());
                          criteria.add(PURCHASE_RETURN_ACCOUNT, obj.getPurchaseReturnAccount());
                          criteria.add(EXPENSE_ACCOUNT, obj.getExpenseAccount());
                          criteria.add(UNBILLED_GOODS_ACCOUNT, obj.getUnbilledGoodsAccount());
                          criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
                          criteria.add(RETAINED_EARNING, obj.getRetainedEarning());
                          criteria.add(CREDIT_MEMO_ACCOUNT, obj.getCreditMemoAccount());
                          criteria.add(DEBIT_MEMO_ACCOUNT, obj.getDebitMemoAccount());
                          criteria.add(DELIVERY_ORDER_ACCOUNT, obj.getDeliveryOrderAccount());
                          criteria.add(POS_ROUNDING_ACCOUNT, obj.getPosRoundingAccount());
                          criteria.add(AR_PAYMENT_TEMP, obj.getArPaymentTemp());
                          criteria.add(AP_PAYMENT_TEMP, obj.getApPaymentTemp());
                          criteria.add(PR_PI_VARIANCE, obj.getPrPiVariance());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static GlConfig retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static GlConfig retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static GlConfig retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        GlConfig retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static GlConfig retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (GlConfig)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( GL_CONFIG_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
