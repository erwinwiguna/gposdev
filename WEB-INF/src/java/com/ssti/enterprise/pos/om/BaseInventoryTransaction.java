package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to InventoryTransaction
 */
public abstract class BaseInventoryTransaction extends BaseObject
{
    /** The Peer class */
    private static final InventoryTransactionPeer peer =
        new InventoryTransactionPeer();

        
    /** The value for the inventoryTransactionId field */
    private String inventoryTransactionId;
      
    /** The value for the transactionId field */
    private String transactionId;
                                                
    /** The value for the transactionNo field */
    private String transactionNo = "";
                                                
    /** The value for the transactionDetailId field */
    private String transactionDetailId = "";
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the qtyChanges field */
    private BigDecimal qtyChanges;
      
    /** The value for the price field */
    private BigDecimal price;
      
    /** The value for the cost field */
    private BigDecimal cost;
                                                
          
    /** The value for the totalCost field */
    private BigDecimal totalCost= bd_ZERO;
                                                
          
    /** The value for the qtyBalance field */
    private BigDecimal qtyBalance= bd_ZERO;
                                                
          
    /** The value for the valueBalance field */
    private BigDecimal valueBalance= bd_ZERO;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
                                                
          
    /** The value for the itemPrice field */
    private BigDecimal itemPrice= bd_ZERO;
  
    
    /**
     * Get the InventoryTransactionId
     *
     * @return String
     */
    public String getInventoryTransactionId()
    {
        return inventoryTransactionId;
    }

                                              
    /**
     * Set the value of InventoryTransactionId
     *
     * @param v new value
     */
    public void setInventoryTransactionId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.inventoryTransactionId, v))
              {
            this.inventoryTransactionId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated FifoIn
        if (collFifoIns != null)
        {
            for (int i = 0; i < collFifoIns.size(); i++)
            {
                ((FifoIn) collFifoIns.get(i))
                    .setInventoryTransactionId(v);
            }
        }
                                                    
                  // update associated FifoOut
        if (collFifoOuts != null)
        {
            for (int i = 0; i < collFifoOuts.size(); i++)
            {
                ((FifoOut) collFifoOuts.get(i))
                    .setInventoryTransactionId(v);
            }
        }
                                }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDetailId
     *
     * @return String
     */
    public String getTransactionDetailId()
    {
        return transactionDetailId;
    }

                        
    /**
     * Set the value of TransactionDetailId
     *
     * @param v new value
     */
    public void setTransactionDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDetailId, v))
              {
            this.transactionDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyChanges
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyChanges()
    {
        return qtyChanges;
    }

                        
    /**
     * Set the value of QtyChanges
     *
     * @param v new value
     */
    public void setQtyChanges(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyChanges, v))
              {
            this.qtyChanges = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Price
     *
     * @return BigDecimal
     */
    public BigDecimal getPrice()
    {
        return price;
    }

                        
    /**
     * Set the value of Price
     *
     * @param v new value
     */
    public void setPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.price, v))
              {
            this.price = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Cost
     *
     * @return BigDecimal
     */
    public BigDecimal getCost()
    {
        return cost;
    }

                        
    /**
     * Set the value of Cost
     *
     * @param v new value
     */
    public void setCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.cost, v))
              {
            this.cost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalCost
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalCost()
    {
        return totalCost;
    }

                        
    /**
     * Set the value of TotalCost
     *
     * @param v new value
     */
    public void setTotalCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalCost, v))
              {
            this.totalCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBalance()
    {
        return qtyBalance;
    }

                        
    /**
     * Set the value of QtyBalance
     *
     * @param v new value
     */
    public void setQtyBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBalance, v))
              {
            this.qtyBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ValueBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getValueBalance()
    {
        return valueBalance;
    }

                        
    /**
     * Set the value of ValueBalance
     *
     * @param v new value
     */
    public void setValueBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.valueBalance, v))
              {
            this.valueBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collFifoIns
     */
    protected List collFifoIns;

    /**
     * Temporary storage of collFifoIns to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initFifoIns()
    {
        if (collFifoIns == null)
        {
            collFifoIns = new ArrayList();
        }
    }

    /**
     * Method called to associate a FifoIn object to this object
     * through the FifoIn foreign key attribute
     *
     * @param l FifoIn
     * @throws TorqueException
     */
    public void addFifoIn(FifoIn l) throws TorqueException
    {
        getFifoIns().add(l);
        l.setInventoryTransaction((InventoryTransaction) this);
    }

    /**
     * The criteria used to select the current contents of collFifoIns
     */
    private Criteria lastFifoInsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getFifoIns(new Criteria())
     *
     * @throws TorqueException
     */
    public List getFifoIns() throws TorqueException
    {
              if (collFifoIns == null)
        {
            collFifoIns = getFifoIns(new Criteria(10));
        }
        return collFifoIns;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this InventoryTransaction has previously
     * been saved, it will retrieve related FifoIns from storage.
     * If this InventoryTransaction is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getFifoIns(Criteria criteria) throws TorqueException
    {
              if (collFifoIns == null)
        {
            if (isNew())
            {
               collFifoIns = new ArrayList();
            }
            else
            {
                        criteria.add(FifoInPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId() );
                        collFifoIns = FifoInPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(FifoInPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                            if (!lastFifoInsCriteria.equals(criteria))
                {
                    collFifoIns = FifoInPeer.doSelect(criteria);
                }
            }
        }
        lastFifoInsCriteria = criteria;

        return collFifoIns;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getFifoIns(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getFifoIns(Connection con) throws TorqueException
    {
              if (collFifoIns == null)
        {
            collFifoIns = getFifoIns(new Criteria(10), con);
        }
        return collFifoIns;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this InventoryTransaction has previously
     * been saved, it will retrieve related FifoIns from storage.
     * If this InventoryTransaction is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getFifoIns(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collFifoIns == null)
        {
            if (isNew())
            {
               collFifoIns = new ArrayList();
            }
            else
            {
                         criteria.add(FifoInPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                         collFifoIns = FifoInPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(FifoInPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                             if (!lastFifoInsCriteria.equals(criteria))
                 {
                     collFifoIns = FifoInPeer.doSelect(criteria, con);
                 }
             }
         }
         lastFifoInsCriteria = criteria;

         return collFifoIns;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this InventoryTransaction is new, it will return
     * an empty collection; or if this InventoryTransaction has previously
     * been saved, it will retrieve related FifoIns from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in InventoryTransaction.
     */
    protected List getFifoInsJoinInventoryTransaction(Criteria criteria)
        throws TorqueException
    {
                    if (collFifoIns == null)
        {
            if (isNew())
            {
               collFifoIns = new ArrayList();
            }
            else
            {
                              criteria.add(FifoInPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                              collFifoIns = FifoInPeer.doSelectJoinInventoryTransaction(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(FifoInPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                                    if (!lastFifoInsCriteria.equals(criteria))
            {
                collFifoIns = FifoInPeer.doSelectJoinInventoryTransaction(criteria);
            }
        }
        lastFifoInsCriteria = criteria;

        return collFifoIns;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collFifoOuts
     */
    protected List collFifoOuts;

    /**
     * Temporary storage of collFifoOuts to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initFifoOuts()
    {
        if (collFifoOuts == null)
        {
            collFifoOuts = new ArrayList();
        }
    }

    /**
     * Method called to associate a FifoOut object to this object
     * through the FifoOut foreign key attribute
     *
     * @param l FifoOut
     * @throws TorqueException
     */
    public void addFifoOut(FifoOut l) throws TorqueException
    {
        getFifoOuts().add(l);
        l.setInventoryTransaction((InventoryTransaction) this);
    }

    /**
     * The criteria used to select the current contents of collFifoOuts
     */
    private Criteria lastFifoOutsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getFifoOuts(new Criteria())
     *
     * @throws TorqueException
     */
    public List getFifoOuts() throws TorqueException
    {
              if (collFifoOuts == null)
        {
            collFifoOuts = getFifoOuts(new Criteria(10));
        }
        return collFifoOuts;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this InventoryTransaction has previously
     * been saved, it will retrieve related FifoOuts from storage.
     * If this InventoryTransaction is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getFifoOuts(Criteria criteria) throws TorqueException
    {
              if (collFifoOuts == null)
        {
            if (isNew())
            {
               collFifoOuts = new ArrayList();
            }
            else
            {
                        criteria.add(FifoOutPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId() );
                        collFifoOuts = FifoOutPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(FifoOutPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                            if (!lastFifoOutsCriteria.equals(criteria))
                {
                    collFifoOuts = FifoOutPeer.doSelect(criteria);
                }
            }
        }
        lastFifoOutsCriteria = criteria;

        return collFifoOuts;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getFifoOuts(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getFifoOuts(Connection con) throws TorqueException
    {
              if (collFifoOuts == null)
        {
            collFifoOuts = getFifoOuts(new Criteria(10), con);
        }
        return collFifoOuts;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this InventoryTransaction has previously
     * been saved, it will retrieve related FifoOuts from storage.
     * If this InventoryTransaction is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getFifoOuts(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collFifoOuts == null)
        {
            if (isNew())
            {
               collFifoOuts = new ArrayList();
            }
            else
            {
                         criteria.add(FifoOutPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                         collFifoOuts = FifoOutPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(FifoOutPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                             if (!lastFifoOutsCriteria.equals(criteria))
                 {
                     collFifoOuts = FifoOutPeer.doSelect(criteria, con);
                 }
             }
         }
         lastFifoOutsCriteria = criteria;

         return collFifoOuts;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this InventoryTransaction is new, it will return
     * an empty collection; or if this InventoryTransaction has previously
     * been saved, it will retrieve related FifoOuts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in InventoryTransaction.
     */
    protected List getFifoOutsJoinInventoryTransaction(Criteria criteria)
        throws TorqueException
    {
                    if (collFifoOuts == null)
        {
            if (isNew())
            {
               collFifoOuts = new ArrayList();
            }
            else
            {
                              criteria.add(FifoOutPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                              collFifoOuts = FifoOutPeer.doSelectJoinInventoryTransaction(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(FifoOutPeer.INVENTORY_TRANSACTION_ID, getInventoryTransactionId());
                                    if (!lastFifoOutsCriteria.equals(criteria))
            {
                collFifoOuts = FifoOutPeer.doSelectJoinInventoryTransaction(criteria);
            }
        }
        lastFifoOutsCriteria = criteria;

        return collFifoOuts;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("InventoryTransactionId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDetailId");
              fieldNames.add("TransactionType");
              fieldNames.add("TransactionDate");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("QtyChanges");
              fieldNames.add("Price");
              fieldNames.add("Cost");
              fieldNames.add("TotalCost");
              fieldNames.add("QtyBalance");
              fieldNames.add("ValueBalance");
              fieldNames.add("Description");
              fieldNames.add("CreateDate");
              fieldNames.add("UpdateDate");
              fieldNames.add("ItemPrice");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("InventoryTransactionId"))
        {
                return getInventoryTransactionId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDetailId"))
        {
                return getTransactionDetailId();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("QtyChanges"))
        {
                return getQtyChanges();
            }
          if (name.equals("Price"))
        {
                return getPrice();
            }
          if (name.equals("Cost"))
        {
                return getCost();
            }
          if (name.equals("TotalCost"))
        {
                return getTotalCost();
            }
          if (name.equals("QtyBalance"))
        {
                return getQtyBalance();
            }
          if (name.equals("ValueBalance"))
        {
                return getValueBalance();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID))
        {
                return getInventoryTransactionId();
            }
          if (name.equals(InventoryTransactionPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(InventoryTransactionPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(InventoryTransactionPeer.TRANSACTION_DETAIL_ID))
        {
                return getTransactionDetailId();
            }
          if (name.equals(InventoryTransactionPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(InventoryTransactionPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(InventoryTransactionPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(InventoryTransactionPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(InventoryTransactionPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(InventoryTransactionPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(InventoryTransactionPeer.QTY_CHANGES))
        {
                return getQtyChanges();
            }
          if (name.equals(InventoryTransactionPeer.PRICE))
        {
                return getPrice();
            }
          if (name.equals(InventoryTransactionPeer.COST))
        {
                return getCost();
            }
          if (name.equals(InventoryTransactionPeer.TOTAL_COST))
        {
                return getTotalCost();
            }
          if (name.equals(InventoryTransactionPeer.QTY_BALANCE))
        {
                return getQtyBalance();
            }
          if (name.equals(InventoryTransactionPeer.VALUE_BALANCE))
        {
                return getValueBalance();
            }
          if (name.equals(InventoryTransactionPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(InventoryTransactionPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(InventoryTransactionPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(InventoryTransactionPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getInventoryTransactionId();
            }
              if (pos == 1)
        {
                return getTransactionId();
            }
              if (pos == 2)
        {
                return getTransactionNo();
            }
              if (pos == 3)
        {
                return getTransactionDetailId();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 5)
        {
                return getTransactionDate();
            }
              if (pos == 6)
        {
                return getLocationId();
            }
              if (pos == 7)
        {
                return getLocationName();
            }
              if (pos == 8)
        {
                return getItemId();
            }
              if (pos == 9)
        {
                return getItemCode();
            }
              if (pos == 10)
        {
                return getQtyChanges();
            }
              if (pos == 11)
        {
                return getPrice();
            }
              if (pos == 12)
        {
                return getCost();
            }
              if (pos == 13)
        {
                return getTotalCost();
            }
              if (pos == 14)
        {
                return getQtyBalance();
            }
              if (pos == 15)
        {
                return getValueBalance();
            }
              if (pos == 16)
        {
                return getDescription();
            }
              if (pos == 17)
        {
                return getCreateDate();
            }
              if (pos == 18)
        {
                return getUpdateDate();
            }
              if (pos == 19)
        {
                return getItemPrice();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(InventoryTransactionPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        InventoryTransactionPeer.doInsert((InventoryTransaction) this, con);
                        setNew(false);
                    }
                    else
                    {
                        InventoryTransactionPeer.doUpdate((InventoryTransaction) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collFifoIns != null)
            {
                for (int i = 0; i < collFifoIns.size(); i++)
                {
                    ((FifoIn) collFifoIns.get(i)).save(con);
                }
            }
                                                  
                
                    if (collFifoOuts != null)
            {
                for (int i = 0; i < collFifoOuts.size(); i++)
                {
                    ((FifoOut) collFifoOuts.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key inventoryTransactionId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setInventoryTransactionId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setInventoryTransactionId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getInventoryTransactionId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public InventoryTransaction copy() throws TorqueException
    {
        return copyInto(new InventoryTransaction());
    }
  
    protected InventoryTransaction copyInto(InventoryTransaction copyObj) throws TorqueException
    {
          copyObj.setInventoryTransactionId(inventoryTransactionId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDetailId(transactionDetailId);
          copyObj.setTransactionType(transactionType);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setQtyChanges(qtyChanges);
          copyObj.setPrice(price);
          copyObj.setCost(cost);
          copyObj.setTotalCost(totalCost);
          copyObj.setQtyBalance(qtyBalance);
          copyObj.setValueBalance(valueBalance);
          copyObj.setDescription(description);
          copyObj.setCreateDate(createDate);
          copyObj.setUpdateDate(updateDate);
          copyObj.setItemPrice(itemPrice);
  
                    copyObj.setInventoryTransactionId((String)null);
                                                                                                                              
                                      
                            
        List v = getFifoIns();
        for (int i = 0; i < v.size(); i++)
        {
            FifoIn obj = (FifoIn) v.get(i);
            copyObj.addFifoIn(obj.copy());
        }
                                                  
                            
        v = getFifoOuts();
        for (int i = 0; i < v.size(); i++)
        {
            FifoOut obj = (FifoOut) v.get(i);
            copyObj.addFifoOut(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public InventoryTransactionPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("InventoryTransaction\n");
        str.append("--------------------\n")
            .append("InventoryTransactionId   : ")
           .append(getInventoryTransactionId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDetailId  : ")
           .append(getTransactionDetailId())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("QtyChanges           : ")
           .append(getQtyChanges())
           .append("\n")
           .append("Price                : ")
           .append(getPrice())
           .append("\n")
           .append("Cost                 : ")
           .append(getCost())
           .append("\n")
           .append("TotalCost            : ")
           .append(getTotalCost())
           .append("\n")
           .append("QtyBalance           : ")
           .append(getQtyBalance())
           .append("\n")
           .append("ValueBalance         : ")
           .append(getValueBalance())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
        ;
        return(str.toString());
    }
}
