package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to InvoicePayment
 */
public abstract class BaseInvoicePayment extends BaseObject
{
    /** The Peer class */
    private static final InvoicePaymentPeer peer =
        new InvoicePaymentPeer();

        
    /** The value for the invoicePaymentId field */
    private String invoicePaymentId;
      
    /** The value for the transactionId field */
    private String transactionId;
      
    /** The value for the transactionType field */
    private int transactionType;
                                                
    /** The value for the paymentTypeId field */
    private String paymentTypeId = "";
                                                
    /** The value for the paymentTermId field */
    private String paymentTermId = "";
                                                
    /** The value for the bankId field */
    private String bankId = "";
                                                
    /** The value for the voucherId field */
    private String voucherId = "";
      
    /** The value for the transTotalAmount field */
    private BigDecimal transTotalAmount;
      
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount;
                                                
          
    /** The value for the cashAmount field */
    private BigDecimal cashAmount= bd_ZERO;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the dueDate field */
    private Date dueDate;
                                                
    /** The value for the bankIssuer field */
    private String bankIssuer = "";
                                                
          
    /** The value for the mdrAmount field */
    private BigDecimal mdrAmount= bd_ZERO;
                                                
    /** The value for the referenceNo field */
    private String referenceNo = "";
                                                
    /** The value for the approvalNo field */
    private String approvalNo = "";
                                                
    /** The value for the terminalId field */
    private String terminalId = "";
                                                
    /** The value for the traceNo field */
    private String traceNo = "";
                                                
    /** The value for the entryMode field */
    private String entryMode = "";
  
    
    /**
     * Get the InvoicePaymentId
     *
     * @return String
     */
    public String getInvoicePaymentId()
    {
        return invoicePaymentId;
    }

                        
    /**
     * Set the value of InvoicePaymentId
     *
     * @param v new value
     */
    public void setInvoicePaymentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoicePaymentId, v))
              {
            this.invoicePaymentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VoucherId
     *
     * @return String
     */
    public String getVoucherId()
    {
        return voucherId;
    }

                        
    /**
     * Set the value of VoucherId
     *
     * @param v new value
     */
    public void setVoucherId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voucherId, v))
              {
            this.voucherId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransTotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTransTotalAmount()
    {
        return transTotalAmount;
    }

                        
    /**
     * Set the value of TransTotalAmount
     *
     * @param v new value
     */
    public void setTransTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.transTotalAmount, v))
              {
            this.transTotalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getCashAmount()
    {
        return cashAmount;
    }

                        
    /**
     * Set the value of CashAmount
     *
     * @param v new value
     */
    public void setCashAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.cashAmount, v))
              {
            this.cashAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MdrAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getMdrAmount()
    {
        return mdrAmount;
    }

                        
    /**
     * Set the value of MdrAmount
     *
     * @param v new value
     */
    public void setMdrAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.mdrAmount, v))
              {
            this.mdrAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApprovalNo
     *
     * @return String
     */
    public String getApprovalNo()
    {
        return approvalNo;
    }

                        
    /**
     * Set the value of ApprovalNo
     *
     * @param v new value
     */
    public void setApprovalNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.approvalNo, v))
              {
            this.approvalNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TerminalId
     *
     * @return String
     */
    public String getTerminalId()
    {
        return terminalId;
    }

                        
    /**
     * Set the value of TerminalId
     *
     * @param v new value
     */
    public void setTerminalId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.terminalId, v))
              {
            this.terminalId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TraceNo
     *
     * @return String
     */
    public String getTraceNo()
    {
        return traceNo;
    }

                        
    /**
     * Set the value of TraceNo
     *
     * @param v new value
     */
    public void setTraceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.traceNo, v))
              {
            this.traceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EntryMode
     *
     * @return String
     */
    public String getEntryMode()
    {
        return entryMode;
    }

                        
    /**
     * Set the value of EntryMode
     *
     * @param v new value
     */
    public void setEntryMode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.entryMode, v))
              {
            this.entryMode = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("InvoicePaymentId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionType");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("BankId");
              fieldNames.add("VoucherId");
              fieldNames.add("TransTotalAmount");
              fieldNames.add("PaymentAmount");
              fieldNames.add("CashAmount");
              fieldNames.add("TransactionDate");
              fieldNames.add("DueDate");
              fieldNames.add("BankIssuer");
              fieldNames.add("MdrAmount");
              fieldNames.add("ReferenceNo");
              fieldNames.add("ApprovalNo");
              fieldNames.add("TerminalId");
              fieldNames.add("TraceNo");
              fieldNames.add("EntryMode");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("InvoicePaymentId"))
        {
                return getInvoicePaymentId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("VoucherId"))
        {
                return getVoucherId();
            }
          if (name.equals("TransTotalAmount"))
        {
                return getTransTotalAmount();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("CashAmount"))
        {
                return getCashAmount();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("MdrAmount"))
        {
                return getMdrAmount();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("ApprovalNo"))
        {
                return getApprovalNo();
            }
          if (name.equals("TerminalId"))
        {
                return getTerminalId();
            }
          if (name.equals("TraceNo"))
        {
                return getTraceNo();
            }
          if (name.equals("EntryMode"))
        {
                return getEntryMode();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(InvoicePaymentPeer.INVOICE_PAYMENT_ID))
        {
                return getInvoicePaymentId();
            }
          if (name.equals(InvoicePaymentPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(InvoicePaymentPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(InvoicePaymentPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(InvoicePaymentPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(InvoicePaymentPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(InvoicePaymentPeer.VOUCHER_ID))
        {
                return getVoucherId();
            }
          if (name.equals(InvoicePaymentPeer.TRANS_TOTAL_AMOUNT))
        {
                return getTransTotalAmount();
            }
          if (name.equals(InvoicePaymentPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(InvoicePaymentPeer.CASH_AMOUNT))
        {
                return getCashAmount();
            }
          if (name.equals(InvoicePaymentPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(InvoicePaymentPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(InvoicePaymentPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(InvoicePaymentPeer.MDR_AMOUNT))
        {
                return getMdrAmount();
            }
          if (name.equals(InvoicePaymentPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(InvoicePaymentPeer.APPROVAL_NO))
        {
                return getApprovalNo();
            }
          if (name.equals(InvoicePaymentPeer.TERMINAL_ID))
        {
                return getTerminalId();
            }
          if (name.equals(InvoicePaymentPeer.TRACE_NO))
        {
                return getTraceNo();
            }
          if (name.equals(InvoicePaymentPeer.ENTRY_MODE))
        {
                return getEntryMode();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getInvoicePaymentId();
            }
              if (pos == 1)
        {
                return getTransactionId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 3)
        {
                return getPaymentTypeId();
            }
              if (pos == 4)
        {
                return getPaymentTermId();
            }
              if (pos == 5)
        {
                return getBankId();
            }
              if (pos == 6)
        {
                return getVoucherId();
            }
              if (pos == 7)
        {
                return getTransTotalAmount();
            }
              if (pos == 8)
        {
                return getPaymentAmount();
            }
              if (pos == 9)
        {
                return getCashAmount();
            }
              if (pos == 10)
        {
                return getTransactionDate();
            }
              if (pos == 11)
        {
                return getDueDate();
            }
              if (pos == 12)
        {
                return getBankIssuer();
            }
              if (pos == 13)
        {
                return getMdrAmount();
            }
              if (pos == 14)
        {
                return getReferenceNo();
            }
              if (pos == 15)
        {
                return getApprovalNo();
            }
              if (pos == 16)
        {
                return getTerminalId();
            }
              if (pos == 17)
        {
                return getTraceNo();
            }
              if (pos == 18)
        {
                return getEntryMode();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(InvoicePaymentPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        InvoicePaymentPeer.doInsert((InvoicePayment) this, con);
                        setNew(false);
                    }
                    else
                    {
                        InvoicePaymentPeer.doUpdate((InvoicePayment) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key invoicePaymentId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setInvoicePaymentId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setInvoicePaymentId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getInvoicePaymentId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public InvoicePayment copy() throws TorqueException
    {
        return copyInto(new InvoicePayment());
    }
  
    protected InvoicePayment copyInto(InvoicePayment copyObj) throws TorqueException
    {
          copyObj.setInvoicePaymentId(invoicePaymentId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionType(transactionType);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setBankId(bankId);
          copyObj.setVoucherId(voucherId);
          copyObj.setTransTotalAmount(transTotalAmount);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setCashAmount(cashAmount);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setDueDate(dueDate);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setMdrAmount(mdrAmount);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setApprovalNo(approvalNo);
          copyObj.setTerminalId(terminalId);
          copyObj.setTraceNo(traceNo);
          copyObj.setEntryMode(entryMode);
  
                    copyObj.setInvoicePaymentId((String)null);
                                                                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public InvoicePaymentPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("InvoicePayment\n");
        str.append("--------------\n")
           .append("InvoicePaymentId     : ")
           .append(getInvoicePaymentId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("VoucherId            : ")
           .append(getVoucherId())
           .append("\n")
           .append("TransTotalAmount     : ")
           .append(getTransTotalAmount())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("CashAmount           : ")
           .append(getCashAmount())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("MdrAmount            : ")
           .append(getMdrAmount())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("ApprovalNo           : ")
           .append(getApprovalNo())
           .append("\n")
           .append("TerminalId           : ")
           .append(getTerminalId())
           .append("\n")
           .append("TraceNo              : ")
           .append(getTraceNo())
           .append("\n")
           .append("EntryMode            : ")
           .append(getEntryMode())
           .append("\n")
        ;
        return(str.toString());
    }
}
