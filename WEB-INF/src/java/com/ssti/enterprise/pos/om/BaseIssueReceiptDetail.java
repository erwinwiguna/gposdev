package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to IssueReceiptDetail
 */
public abstract class BaseIssueReceiptDetail extends BaseObject
{
    /** The Peer class */
    private static final IssueReceiptDetailPeer peer =
        new IssueReceiptDetailPeer();

        
    /** The value for the issueReceiptDetailId field */
    private String issueReceiptDetailId;
      
    /** The value for the issueReceiptId field */
    private String issueReceiptId;
      
    /** The value for the indexNo field */
    private int indexNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the qtyChanges field */
    private BigDecimal qtyChanges;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the cost field */
    private BigDecimal cost;
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
                                                
    /** The value for the projectId field */
    private String projectId = "";
  
    
    /**
     * Get the IssueReceiptDetailId
     *
     * @return String
     */
    public String getIssueReceiptDetailId()
    {
        return issueReceiptDetailId;
    }

                        
    /**
     * Set the value of IssueReceiptDetailId
     *
     * @param v new value
     */
    public void setIssueReceiptDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.issueReceiptDetailId, v))
              {
            this.issueReceiptDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IssueReceiptId
     *
     * @return String
     */
    public String getIssueReceiptId()
    {
        return issueReceiptId;
    }

                              
    /**
     * Set the value of IssueReceiptId
     *
     * @param v new value
     */
    public void setIssueReceiptId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.issueReceiptId, v))
              {
            this.issueReceiptId = v;
            setModified(true);
        }
    
                          
                if (aIssueReceipt != null && !ObjectUtils.equals(aIssueReceipt.getIssueReceiptId(), v))
                {
            aIssueReceipt = null;
        }
      
              }
  
    /**
     * Get the IndexNo
     *
     * @return int
     */
    public int getIndexNo()
    {
        return indexNo;
    }

                        
    /**
     * Set the value of IndexNo
     *
     * @param v new value
     */
    public void setIndexNo(int v) 
    {
    
                  if (this.indexNo != v)
              {
            this.indexNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyChanges
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyChanges()
    {
        return qtyChanges;
    }

                        
    /**
     * Set the value of QtyChanges
     *
     * @param v new value
     */
    public void setQtyChanges(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyChanges, v))
              {
            this.qtyChanges = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Cost
     *
     * @return BigDecimal
     */
    public BigDecimal getCost()
    {
        return cost;
    }

                        
    /**
     * Set the value of Cost
     *
     * @param v new value
     */
    public void setCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.cost, v))
              {
            this.cost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private IssueReceipt aIssueReceipt;

    /**
     * Declares an association between this object and a IssueReceipt object
     *
     * @param v IssueReceipt
     * @throws TorqueException
     */
    public void setIssueReceipt(IssueReceipt v) throws TorqueException
    {
            if (v == null)
        {
                  setIssueReceiptId((String) null);
              }
        else
        {
            setIssueReceiptId(v.getIssueReceiptId());
        }
            aIssueReceipt = v;
    }

                                            
    /**
     * Get the associated IssueReceipt object
     *
     * @return the associated IssueReceipt object
     * @throws TorqueException
     */
    public IssueReceipt getIssueReceipt() throws TorqueException
    {
        if (aIssueReceipt == null && (!ObjectUtils.equals(this.issueReceiptId, null)))
        {
                          aIssueReceipt = IssueReceiptPeer.retrieveByPK(SimpleKey.keyFor(this.issueReceiptId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               IssueReceipt obj = IssueReceiptPeer.retrieveByPK(this.issueReceiptId);
               obj.addIssueReceiptDetails(this);
            */
        }
        return aIssueReceipt;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setIssueReceiptKey(ObjectKey key) throws TorqueException
    {
      
                        setIssueReceiptId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("IssueReceiptDetailId");
              fieldNames.add("IssueReceiptId");
              fieldNames.add("IndexNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("QtyChanges");
              fieldNames.add("QtyBase");
              fieldNames.add("Cost");
              fieldNames.add("DepartmentId");
              fieldNames.add("ProjectId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("IssueReceiptDetailId"))
        {
                return getIssueReceiptDetailId();
            }
          if (name.equals("IssueReceiptId"))
        {
                return getIssueReceiptId();
            }
          if (name.equals("IndexNo"))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("QtyChanges"))
        {
                return getQtyChanges();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("Cost"))
        {
                return getCost();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(IssueReceiptDetailPeer.ISSUE_RECEIPT_DETAIL_ID))
        {
                return getIssueReceiptDetailId();
            }
          if (name.equals(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID))
        {
                return getIssueReceiptId();
            }
          if (name.equals(IssueReceiptDetailPeer.INDEX_NO))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals(IssueReceiptDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(IssueReceiptDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(IssueReceiptDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(IssueReceiptDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(IssueReceiptDetailPeer.QTY_CHANGES))
        {
                return getQtyChanges();
            }
          if (name.equals(IssueReceiptDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(IssueReceiptDetailPeer.COST))
        {
                return getCost();
            }
          if (name.equals(IssueReceiptDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(IssueReceiptDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getIssueReceiptDetailId();
            }
              if (pos == 1)
        {
                return getIssueReceiptId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getIndexNo());
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getUnitId();
            }
              if (pos == 6)
        {
                return getUnitCode();
            }
              if (pos == 7)
        {
                return getQtyChanges();
            }
              if (pos == 8)
        {
                return getQtyBase();
            }
              if (pos == 9)
        {
                return getCost();
            }
              if (pos == 10)
        {
                return getDepartmentId();
            }
              if (pos == 11)
        {
                return getProjectId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(IssueReceiptDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        IssueReceiptDetailPeer.doInsert((IssueReceiptDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        IssueReceiptDetailPeer.doUpdate((IssueReceiptDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key issueReceiptDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setIssueReceiptDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setIssueReceiptDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getIssueReceiptDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public IssueReceiptDetail copy() throws TorqueException
    {
        return copyInto(new IssueReceiptDetail());
    }
  
    protected IssueReceiptDetail copyInto(IssueReceiptDetail copyObj) throws TorqueException
    {
          copyObj.setIssueReceiptDetailId(issueReceiptDetailId);
          copyObj.setIssueReceiptId(issueReceiptId);
          copyObj.setIndexNo(indexNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setQtyChanges(qtyChanges);
          copyObj.setQtyBase(qtyBase);
          copyObj.setCost(cost);
          copyObj.setDepartmentId(departmentId);
          copyObj.setProjectId(projectId);
  
                    copyObj.setIssueReceiptDetailId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public IssueReceiptDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("IssueReceiptDetail\n");
        str.append("------------------\n")
           .append("IssueReceiptDetailId   : ")
           .append(getIssueReceiptDetailId())
           .append("\n")
           .append("IssueReceiptId       : ")
           .append(getIssueReceiptId())
           .append("\n")
           .append("IndexNo              : ")
           .append(getIndexNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("QtyChanges           : ")
           .append(getQtyChanges())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("Cost                 : ")
           .append(getCost())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
        ;
        return(str.toString());
    }
}
