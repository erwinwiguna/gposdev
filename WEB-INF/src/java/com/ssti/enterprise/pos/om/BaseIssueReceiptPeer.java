package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.IssueReceiptMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseIssueReceiptPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "issue_receipt";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(IssueReceiptMapBuilder.CLASS_NAME);
    }

      /** the column name for the ISSUE_RECEIPT_ID field */
    public static final String ISSUE_RECEIPT_ID;
      /** the column name for the ADJUSTMENT_TYPE_ID field */
    public static final String ADJUSTMENT_TYPE_ID;
      /** the column name for the TRANSACTION_DATE field */
    public static final String TRANSACTION_DATE;
      /** the column name for the TRANSACTION_NO field */
    public static final String TRANSACTION_NO;
      /** the column name for the TRANSACTION_TYPE field */
    public static final String TRANSACTION_TYPE;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the LOCATION_NAME field */
    public static final String LOCATION_NAME;
      /** the column name for the USER_NAME field */
    public static final String USER_NAME;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the ACCOUNT_ID field */
    public static final String ACCOUNT_ID;
      /** the column name for the COST_ADJUSTMENT field */
    public static final String COST_ADJUSTMENT;
      /** the column name for the CONFIRM_BY field */
    public static final String CONFIRM_BY;
      /** the column name for the CONFIRM_DATE field */
    public static final String CONFIRM_DATE;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
      /** the column name for the INTERNAL_TRANSFER field */
    public static final String INTERNAL_TRANSFER;
      /** the column name for the REQUEST_ID field */
    public static final String REQUEST_ID;
      /** the column name for the CROSS_ENTITY_ID field */
    public static final String CROSS_ENTITY_ID;
      /** the column name for the CROSS_ENTITY_CODE field */
    public static final String CROSS_ENTITY_CODE;
      /** the column name for the CROSS_TRANS_ID field */
    public static final String CROSS_TRANS_ID;
      /** the column name for the CROSS_TRANS_NO field */
    public static final String CROSS_TRANS_NO;
      /** the column name for the CROSS_CONFIRM_BY field */
    public static final String CROSS_CONFIRM_BY;
      /** the column name for the CROSS_CONFIRM_DATE field */
    public static final String CROSS_CONFIRM_DATE;
      /** the column name for the FILE_NAME field */
    public static final String FILE_NAME;
  
    static
    {
          ISSUE_RECEIPT_ID = "issue_receipt.ISSUE_RECEIPT_ID";
          ADJUSTMENT_TYPE_ID = "issue_receipt.ADJUSTMENT_TYPE_ID";
          TRANSACTION_DATE = "issue_receipt.TRANSACTION_DATE";
          TRANSACTION_NO = "issue_receipt.TRANSACTION_NO";
          TRANSACTION_TYPE = "issue_receipt.TRANSACTION_TYPE";
          LOCATION_ID = "issue_receipt.LOCATION_ID";
          LOCATION_NAME = "issue_receipt.LOCATION_NAME";
          USER_NAME = "issue_receipt.USER_NAME";
          DESCRIPTION = "issue_receipt.DESCRIPTION";
          STATUS = "issue_receipt.STATUS";
          ACCOUNT_ID = "issue_receipt.ACCOUNT_ID";
          COST_ADJUSTMENT = "issue_receipt.COST_ADJUSTMENT";
          CONFIRM_BY = "issue_receipt.CONFIRM_BY";
          CONFIRM_DATE = "issue_receipt.CONFIRM_DATE";
          CANCEL_BY = "issue_receipt.CANCEL_BY";
          CANCEL_DATE = "issue_receipt.CANCEL_DATE";
          INTERNAL_TRANSFER = "issue_receipt.INTERNAL_TRANSFER";
          REQUEST_ID = "issue_receipt.REQUEST_ID";
          CROSS_ENTITY_ID = "issue_receipt.CROSS_ENTITY_ID";
          CROSS_ENTITY_CODE = "issue_receipt.CROSS_ENTITY_CODE";
          CROSS_TRANS_ID = "issue_receipt.CROSS_TRANS_ID";
          CROSS_TRANS_NO = "issue_receipt.CROSS_TRANS_NO";
          CROSS_CONFIRM_BY = "issue_receipt.CROSS_CONFIRM_BY";
          CROSS_CONFIRM_DATE = "issue_receipt.CROSS_CONFIRM_DATE";
          FILE_NAME = "issue_receipt.FILE_NAME";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(IssueReceiptMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(IssueReceiptMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  25;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.IssueReceipt";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseIssueReceiptPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(COST_ADJUSTMENT))
        {
            Object possibleBoolean = criteria.get(COST_ADJUSTMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(COST_ADJUSTMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(INTERNAL_TRANSFER))
        {
            Object possibleBoolean = criteria.get(INTERNAL_TRANSFER);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(INTERNAL_TRANSFER, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(ISSUE_RECEIPT_ID);
          criteria.addSelectColumn(ADJUSTMENT_TYPE_ID);
          criteria.addSelectColumn(TRANSACTION_DATE);
          criteria.addSelectColumn(TRANSACTION_NO);
          criteria.addSelectColumn(TRANSACTION_TYPE);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(LOCATION_NAME);
          criteria.addSelectColumn(USER_NAME);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(ACCOUNT_ID);
          criteria.addSelectColumn(COST_ADJUSTMENT);
          criteria.addSelectColumn(CONFIRM_BY);
          criteria.addSelectColumn(CONFIRM_DATE);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
          criteria.addSelectColumn(INTERNAL_TRANSFER);
          criteria.addSelectColumn(REQUEST_ID);
          criteria.addSelectColumn(CROSS_ENTITY_ID);
          criteria.addSelectColumn(CROSS_ENTITY_CODE);
          criteria.addSelectColumn(CROSS_TRANS_ID);
          criteria.addSelectColumn(CROSS_TRANS_NO);
          criteria.addSelectColumn(CROSS_CONFIRM_BY);
          criteria.addSelectColumn(CROSS_CONFIRM_DATE);
          criteria.addSelectColumn(FILE_NAME);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static IssueReceipt row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            IssueReceipt obj = (IssueReceipt) cls.newInstance();
            IssueReceiptPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      IssueReceipt obj)
        throws TorqueException
    {
        try
        {
                obj.setIssueReceiptId(row.getValue(offset + 0).asString());
                  obj.setAdjustmentTypeId(row.getValue(offset + 1).asString());
                  obj.setTransactionDate(row.getValue(offset + 2).asUtilDate());
                  obj.setTransactionNo(row.getValue(offset + 3).asString());
                  obj.setTransactionType(row.getValue(offset + 4).asInt());
                  obj.setLocationId(row.getValue(offset + 5).asString());
                  obj.setLocationName(row.getValue(offset + 6).asString());
                  obj.setUserName(row.getValue(offset + 7).asString());
                  obj.setDescription(row.getValue(offset + 8).asString());
                  obj.setStatus(row.getValue(offset + 9).asInt());
                  obj.setAccountId(row.getValue(offset + 10).asString());
                  obj.setCostAdjustment(row.getValue(offset + 11).asBoolean());
                  obj.setConfirmBy(row.getValue(offset + 12).asString());
                  obj.setConfirmDate(row.getValue(offset + 13).asUtilDate());
                  obj.setCancelBy(row.getValue(offset + 14).asString());
                  obj.setCancelDate(row.getValue(offset + 15).asUtilDate());
                  obj.setInternalTransfer(row.getValue(offset + 16).asBoolean());
                  obj.setRequestId(row.getValue(offset + 17).asString());
                  obj.setCrossEntityId(row.getValue(offset + 18).asString());
                  obj.setCrossEntityCode(row.getValue(offset + 19).asString());
                  obj.setCrossTransId(row.getValue(offset + 20).asString());
                  obj.setCrossTransNo(row.getValue(offset + 21).asString());
                  obj.setCrossConfirmBy(row.getValue(offset + 22).asString());
                  obj.setCrossConfirmDate(row.getValue(offset + 23).asUtilDate());
                  obj.setFileName(row.getValue(offset + 24).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseIssueReceiptPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(COST_ADJUSTMENT))
        {
            Object possibleBoolean = criteria.get(COST_ADJUSTMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(COST_ADJUSTMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(INTERNAL_TRANSFER))
        {
            Object possibleBoolean = criteria.get(INTERNAL_TRANSFER);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(INTERNAL_TRANSFER, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(IssueReceiptPeer.row2Object(row, 1,
                IssueReceiptPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseIssueReceiptPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(ISSUE_RECEIPT_ID, criteria.remove(ISSUE_RECEIPT_ID));
                                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(COST_ADJUSTMENT))
        {
            Object possibleBoolean = criteria.get(COST_ADJUSTMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(COST_ADJUSTMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(INTERNAL_TRANSFER))
        {
            Object possibleBoolean = criteria.get(INTERNAL_TRANSFER);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(INTERNAL_TRANSFER, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         IssueReceiptPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(COST_ADJUSTMENT))
        {
            Object possibleBoolean = criteria.get(COST_ADJUSTMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(COST_ADJUSTMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(INTERNAL_TRANSFER))
        {
            Object possibleBoolean = criteria.get(INTERNAL_TRANSFER);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(INTERNAL_TRANSFER, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(IssueReceipt obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(IssueReceipt obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(IssueReceipt obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(IssueReceipt obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(IssueReceipt) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(IssueReceipt obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(IssueReceipt) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(IssueReceipt obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(IssueReceipt) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(IssueReceipt obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseIssueReceiptPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(ISSUE_RECEIPT_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( IssueReceipt obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(ISSUE_RECEIPT_ID, obj.getIssueReceiptId());
              criteria.add(ADJUSTMENT_TYPE_ID, obj.getAdjustmentTypeId());
              criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
              criteria.add(TRANSACTION_NO, obj.getTransactionNo());
              criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(LOCATION_NAME, obj.getLocationName());
              criteria.add(USER_NAME, obj.getUserName());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(ACCOUNT_ID, obj.getAccountId());
              criteria.add(COST_ADJUSTMENT, obj.getCostAdjustment());
              criteria.add(CONFIRM_BY, obj.getConfirmBy());
              criteria.add(CONFIRM_DATE, obj.getConfirmDate());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
              criteria.add(INTERNAL_TRANSFER, obj.getInternalTransfer());
              criteria.add(REQUEST_ID, obj.getRequestId());
              criteria.add(CROSS_ENTITY_ID, obj.getCrossEntityId());
              criteria.add(CROSS_ENTITY_CODE, obj.getCrossEntityCode());
              criteria.add(CROSS_TRANS_ID, obj.getCrossTransId());
              criteria.add(CROSS_TRANS_NO, obj.getCrossTransNo());
              criteria.add(CROSS_CONFIRM_BY, obj.getCrossConfirmBy());
              criteria.add(CROSS_CONFIRM_DATE, obj.getCrossConfirmDate());
              criteria.add(FILE_NAME, obj.getFileName());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( IssueReceipt obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(ISSUE_RECEIPT_ID, obj.getIssueReceiptId());
                          criteria.add(ADJUSTMENT_TYPE_ID, obj.getAdjustmentTypeId());
                          criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
                          criteria.add(TRANSACTION_NO, obj.getTransactionNo());
                          criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(LOCATION_NAME, obj.getLocationName());
                          criteria.add(USER_NAME, obj.getUserName());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(ACCOUNT_ID, obj.getAccountId());
                          criteria.add(COST_ADJUSTMENT, obj.getCostAdjustment());
                          criteria.add(CONFIRM_BY, obj.getConfirmBy());
                          criteria.add(CONFIRM_DATE, obj.getConfirmDate());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
                          criteria.add(INTERNAL_TRANSFER, obj.getInternalTransfer());
                          criteria.add(REQUEST_ID, obj.getRequestId());
                          criteria.add(CROSS_ENTITY_ID, obj.getCrossEntityId());
                          criteria.add(CROSS_ENTITY_CODE, obj.getCrossEntityCode());
                          criteria.add(CROSS_TRANS_ID, obj.getCrossTransId());
                          criteria.add(CROSS_TRANS_NO, obj.getCrossTransNo());
                          criteria.add(CROSS_CONFIRM_BY, obj.getCrossConfirmBy());
                          criteria.add(CROSS_CONFIRM_DATE, obj.getCrossConfirmDate());
                          criteria.add(FILE_NAME, obj.getFileName());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static IssueReceipt retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static IssueReceipt retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static IssueReceipt retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        IssueReceipt retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static IssueReceipt retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (IssueReceipt)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( ISSUE_RECEIPT_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
