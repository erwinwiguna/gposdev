package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Item
 */
public abstract class BaseItem extends BaseObject
{
    /** The Peer class */
    private static final ItemPeer peer =
        new ItemPeer();

        
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
                                                
    /** The value for the barcode field */
    private String barcode = "";
      
    /** The value for the itemName field */
    private String itemName;
      
    /** The value for the description field */
    private String description;
                                                
    /** The value for the itemSku field */
    private String itemSku = "";
                                                
    /** The value for the itemSkuName field */
    private String itemSkuName = "";
                                                
    /** The value for the vendorItemCode field */
    private String vendorItemCode = "";
                                                
    /** The value for the vendorItemName field */
    private String vendorItemName = "";
      
    /** The value for the itemType field */
    private int itemType;
      
    /** The value for the kategoriId field */
    private String kategoriId;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the purchaseUnitId field */
    private String purchaseUnitId;
                                                
    /** The value for the taxId field */
    private String taxId = "";
                                                
    /** The value for the purchaseTaxId field */
    private String purchaseTaxId = "";
                                                
    /** The value for the preferedVendorId field */
    private String preferedVendorId = "";
      
    /** The value for the itemPrice field */
    private BigDecimal itemPrice;
                                                
          
    /** The value for the minPrice field */
    private BigDecimal minPrice= bd_ZERO;
      
    /** The value for the isFixedPrice field */
    private boolean isFixedPrice;
                                                                
    /** The value for the isDiscontinue field */
    private boolean isDiscontinue = false;
                                                                
    /** The value for the isConsignment field */
    private boolean isConsignment = false;
                                                
    /** The value for the itemPicture field */
    private String itemPicture = "";
                                                
    /** The value for the tags field */
    private String tags = "";
                                                                
    /** The value for the displayStore field */
    private boolean displayStore = true;
                                                
          
    /** The value for the lastPurchasePrice field */
    private BigDecimal lastPurchasePrice= bd_ZERO;
                                                
    /** The value for the lastPurchaseCurr field */
    private String lastPurchaseCurr = "";
                                                
          
    /** The value for the lastPurchaseRate field */
    private BigDecimal lastPurchaseRate= new BigDecimal(1);
                                                
    /** The value for the profitMargin field */
    private String profitMargin = "";
                                                
          
    /** The value for the minimumStock field */
    private BigDecimal minimumStock= bd_ZERO;
                                                
          
    /** The value for the reorderPoint field */
    private BigDecimal reorderPoint= bd_ZERO;
                                                
          
    /** The value for the maximumStock field */
    private BigDecimal maximumStock= bd_ZERO;
                                                
    /** The value for the rackLocation field */
    private String rackLocation = "";
                                                
    /** The value for the warehouseId field */
    private String warehouseId = "";
                                          
    /** The value for the deliveryType field */
    private int deliveryType = 0;
                                                
    /** The value for the itemStatusCodeId field */
    private String itemStatusCodeId = "";
                                                
    /** The value for the statusColor field */
    private String statusColor = "";
                                                
    /** The value for the discountAmount field */
    private String discountAmount = "0";
                                                
    /** The value for the createBy field */
    private String createBy = "";
      
    /** The value for the addDate field */
    private Date addDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the lastAdjustment field */
    private Date lastAdjustment;
                                                
    /** The value for the size field */
    private String size = "";
                                                
    /** The value for the color field */
    private String color = "";
                                                
    /** The value for the sizeUnit field */
    private String sizeUnit = "";
                                                
          
    /** The value for the weight field */
    private BigDecimal weight= bd_ZERO;
                                                
          
    /** The value for the volume field */
    private BigDecimal volume= bd_ZERO;
                                                
          
    /** The value for the unitConversion field */
    private BigDecimal unitConversion= new BigDecimal(1);
                                                
    /** The value for the brand field */
    private String brand = "";
                                                
    /** The value for the manufacturer field */
    private String manufacturer = "";
      
    /** The value for the trackSerialNo field */
    private boolean trackSerialNo;
      
    /** The value for the trackBatchNo field */
    private boolean trackBatchNo;
      
    /** The value for the itemStatus field */
    private boolean itemStatus;
                                                
    /** The value for the inventoryAccount field */
    private String inventoryAccount = "";
                                                
    /** The value for the salesAccount field */
    private String salesAccount = "";
                                                
    /** The value for the salesReturnAccount field */
    private String salesReturnAccount = "";
                                                
    /** The value for the itemDiscountAccount field */
    private String itemDiscountAccount = "";
                                                
    /** The value for the cogsAccount field */
    private String cogsAccount = "";
                                                
    /** The value for the purchaseReturnAccount field */
    private String purchaseReturnAccount = "";
                                                
    /** The value for the expenseAccount field */
    private String expenseAccount = "";
                                                
    /** The value for the unbilledGoodsAccount field */
    private String unbilledGoodsAccount = "";
  
    
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                                              
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated ItemGroup
        if (collItemGroupsRelatedByGroupId != null)
        {
            for (int i = 0; i < collItemGroupsRelatedByGroupId.size(); i++)
            {
                ((ItemGroup) collItemGroupsRelatedByGroupId.get(i))
                    .setGroupId(v);
            }
        }
                                                    
                  // update associated ItemGroup
        if (collItemGroupsRelatedByItemId != null)
        {
            for (int i = 0; i < collItemGroupsRelatedByItemId.size(); i++)
            {
                ((ItemGroup) collItemGroupsRelatedByItemId.get(i))
                    .setItemId(v);
            }
        }
                                }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Barcode
     *
     * @return String
     */
    public String getBarcode()
    {
        return barcode;
    }

                        
    /**
     * Set the value of Barcode
     *
     * @param v new value
     */
    public void setBarcode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.barcode, v))
              {
            this.barcode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemSku
     *
     * @return String
     */
    public String getItemSku()
    {
        return itemSku;
    }

                        
    /**
     * Set the value of ItemSku
     *
     * @param v new value
     */
    public void setItemSku(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemSku, v))
              {
            this.itemSku = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemSkuName
     *
     * @return String
     */
    public String getItemSkuName()
    {
        return itemSkuName;
    }

                        
    /**
     * Set the value of ItemSkuName
     *
     * @param v new value
     */
    public void setItemSkuName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemSkuName, v))
              {
            this.itemSkuName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorItemCode
     *
     * @return String
     */
    public String getVendorItemCode()
    {
        return vendorItemCode;
    }

                        
    /**
     * Set the value of VendorItemCode
     *
     * @param v new value
     */
    public void setVendorItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorItemCode, v))
              {
            this.vendorItemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorItemName
     *
     * @return String
     */
    public String getVendorItemName()
    {
        return vendorItemName;
    }

                        
    /**
     * Set the value of VendorItemName
     *
     * @param v new value
     */
    public void setVendorItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorItemName, v))
              {
            this.vendorItemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemType
     *
     * @return int
     */
    public int getItemType()
    {
        return itemType;
    }

                        
    /**
     * Set the value of ItemType
     *
     * @param v new value
     */
    public void setItemType(int v) 
    {
    
                  if (this.itemType != v)
              {
            this.itemType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KategoriId
     *
     * @return String
     */
    public String getKategoriId()
    {
        return kategoriId;
    }

                              
    /**
     * Set the value of KategoriId
     *
     * @param v new value
     */
    public void setKategoriId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.kategoriId, v))
              {
            this.kategoriId = v;
            setModified(true);
        }
    
                          
                if (aKategori != null && !ObjectUtils.equals(aKategori.getKategoriId(), v))
                {
            aKategori = null;
        }
      
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseUnitId
     *
     * @return String
     */
    public String getPurchaseUnitId()
    {
        return purchaseUnitId;
    }

                        
    /**
     * Set the value of PurchaseUnitId
     *
     * @param v new value
     */
    public void setPurchaseUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseUnitId, v))
              {
            this.purchaseUnitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseTaxId
     *
     * @return String
     */
    public String getPurchaseTaxId()
    {
        return purchaseTaxId;
    }

                        
    /**
     * Set the value of PurchaseTaxId
     *
     * @param v new value
     */
    public void setPurchaseTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseTaxId, v))
              {
            this.purchaseTaxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PreferedVendorId
     *
     * @return String
     */
    public String getPreferedVendorId()
    {
        return preferedVendorId;
    }

                        
    /**
     * Set the value of PreferedVendorId
     *
     * @param v new value
     */
    public void setPreferedVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.preferedVendorId, v))
              {
            this.preferedVendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MinPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getMinPrice()
    {
        return minPrice;
    }

                        
    /**
     * Set the value of MinPrice
     *
     * @param v new value
     */
    public void setMinPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.minPrice, v))
              {
            this.minPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsFixedPrice
     *
     * @return boolean
     */
    public boolean getIsFixedPrice()
    {
        return isFixedPrice;
    }

                        
    /**
     * Set the value of IsFixedPrice
     *
     * @param v new value
     */
    public void setIsFixedPrice(boolean v) 
    {
    
                  if (this.isFixedPrice != v)
              {
            this.isFixedPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDiscontinue
     *
     * @return boolean
     */
    public boolean getIsDiscontinue()
    {
        return isDiscontinue;
    }

                        
    /**
     * Set the value of IsDiscontinue
     *
     * @param v new value
     */
    public void setIsDiscontinue(boolean v) 
    {
    
                  if (this.isDiscontinue != v)
              {
            this.isDiscontinue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsConsignment
     *
     * @return boolean
     */
    public boolean getIsConsignment()
    {
        return isConsignment;
    }

                        
    /**
     * Set the value of IsConsignment
     *
     * @param v new value
     */
    public void setIsConsignment(boolean v) 
    {
    
                  if (this.isConsignment != v)
              {
            this.isConsignment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPicture
     *
     * @return String
     */
    public String getItemPicture()
    {
        return itemPicture;
    }

                        
    /**
     * Set the value of ItemPicture
     *
     * @param v new value
     */
    public void setItemPicture(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPicture, v))
              {
            this.itemPicture = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Tags
     *
     * @return String
     */
    public String getTags()
    {
        return tags;
    }

                        
    /**
     * Set the value of Tags
     *
     * @param v new value
     */
    public void setTags(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tags, v))
              {
            this.tags = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayStore
     *
     * @return boolean
     */
    public boolean getDisplayStore()
    {
        return displayStore;
    }

                        
    /**
     * Set the value of DisplayStore
     *
     * @param v new value
     */
    public void setDisplayStore(boolean v) 
    {
    
                  if (this.displayStore != v)
              {
            this.displayStore = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastPurchasePrice
     *
     * @return BigDecimal
     */
    public BigDecimal getLastPurchasePrice()
    {
        return lastPurchasePrice;
    }

                        
    /**
     * Set the value of LastPurchasePrice
     *
     * @param v new value
     */
    public void setLastPurchasePrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.lastPurchasePrice, v))
              {
            this.lastPurchasePrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastPurchaseCurr
     *
     * @return String
     */
    public String getLastPurchaseCurr()
    {
        return lastPurchaseCurr;
    }

                        
    /**
     * Set the value of LastPurchaseCurr
     *
     * @param v new value
     */
    public void setLastPurchaseCurr(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastPurchaseCurr, v))
              {
            this.lastPurchaseCurr = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastPurchaseRate
     *
     * @return BigDecimal
     */
    public BigDecimal getLastPurchaseRate()
    {
        return lastPurchaseRate;
    }

                        
    /**
     * Set the value of LastPurchaseRate
     *
     * @param v new value
     */
    public void setLastPurchaseRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.lastPurchaseRate, v))
              {
            this.lastPurchaseRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProfitMargin
     *
     * @return String
     */
    public String getProfitMargin()
    {
        return profitMargin;
    }

                        
    /**
     * Set the value of ProfitMargin
     *
     * @param v new value
     */
    public void setProfitMargin(String v) 
    {
    
                  if (!ObjectUtils.equals(this.profitMargin, v))
              {
            this.profitMargin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MinimumStock
     *
     * @return BigDecimal
     */
    public BigDecimal getMinimumStock()
    {
        return minimumStock;
    }

                        
    /**
     * Set the value of MinimumStock
     *
     * @param v new value
     */
    public void setMinimumStock(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.minimumStock, v))
              {
            this.minimumStock = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReorderPoint
     *
     * @return BigDecimal
     */
    public BigDecimal getReorderPoint()
    {
        return reorderPoint;
    }

                        
    /**
     * Set the value of ReorderPoint
     *
     * @param v new value
     */
    public void setReorderPoint(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.reorderPoint, v))
              {
            this.reorderPoint = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MaximumStock
     *
     * @return BigDecimal
     */
    public BigDecimal getMaximumStock()
    {
        return maximumStock;
    }

                        
    /**
     * Set the value of MaximumStock
     *
     * @param v new value
     */
    public void setMaximumStock(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.maximumStock, v))
              {
            this.maximumStock = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RackLocation
     *
     * @return String
     */
    public String getRackLocation()
    {
        return rackLocation;
    }

                        
    /**
     * Set the value of RackLocation
     *
     * @param v new value
     */
    public void setRackLocation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.rackLocation, v))
              {
            this.rackLocation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the WarehouseId
     *
     * @return String
     */
    public String getWarehouseId()
    {
        return warehouseId;
    }

                        
    /**
     * Set the value of WarehouseId
     *
     * @param v new value
     */
    public void setWarehouseId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.warehouseId, v))
              {
            this.warehouseId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryType
     *
     * @return int
     */
    public int getDeliveryType()
    {
        return deliveryType;
    }

                        
    /**
     * Set the value of DeliveryType
     *
     * @param v new value
     */
    public void setDeliveryType(int v) 
    {
    
                  if (this.deliveryType != v)
              {
            this.deliveryType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemStatusCodeId
     *
     * @return String
     */
    public String getItemStatusCodeId()
    {
        return itemStatusCodeId;
    }

                        
    /**
     * Set the value of ItemStatusCodeId
     *
     * @param v new value
     */
    public void setItemStatusCodeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemStatusCodeId, v))
              {
            this.itemStatusCodeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StatusColor
     *
     * @return String
     */
    public String getStatusColor()
    {
        return statusColor;
    }

                        
    /**
     * Set the value of StatusColor
     *
     * @param v new value
     */
    public void setStatusColor(String v) 
    {
    
                  if (!ObjectUtils.equals(this.statusColor, v))
              {
            this.statusColor = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAmount
     *
     * @return String
     */
    public String getDiscountAmount()
    {
        return discountAmount;
    }

                        
    /**
     * Set the value of DiscountAmount
     *
     * @param v new value
     */
    public void setDiscountAmount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAmount, v))
              {
            this.discountAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AddDate
     *
     * @return Date
     */
    public Date getAddDate()
    {
        return addDate;
    }

                        
    /**
     * Set the value of AddDate
     *
     * @param v new value
     */
    public void setAddDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.addDate, v))
              {
            this.addDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastAdjustment
     *
     * @return Date
     */
    public Date getLastAdjustment()
    {
        return lastAdjustment;
    }

                        
    /**
     * Set the value of LastAdjustment
     *
     * @param v new value
     */
    public void setLastAdjustment(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastAdjustment, v))
              {
            this.lastAdjustment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Size
     *
     * @return String
     */
    public String getSize()
    {
        return size;
    }

                        
    /**
     * Set the value of Size
     *
     * @param v new value
     */
    public void setSize(String v) 
    {
    
                  if (!ObjectUtils.equals(this.size, v))
              {
            this.size = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Color
     *
     * @return String
     */
    public String getColor()
    {
        return color;
    }

                        
    /**
     * Set the value of Color
     *
     * @param v new value
     */
    public void setColor(String v) 
    {
    
                  if (!ObjectUtils.equals(this.color, v))
              {
            this.color = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SizeUnit
     *
     * @return String
     */
    public String getSizeUnit()
    {
        return sizeUnit;
    }

                        
    /**
     * Set the value of SizeUnit
     *
     * @param v new value
     */
    public void setSizeUnit(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sizeUnit, v))
              {
            this.sizeUnit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Weight
     *
     * @return BigDecimal
     */
    public BigDecimal getWeight()
    {
        return weight;
    }

                        
    /**
     * Set the value of Weight
     *
     * @param v new value
     */
    public void setWeight(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.weight, v))
              {
            this.weight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Volume
     *
     * @return BigDecimal
     */
    public BigDecimal getVolume()
    {
        return volume;
    }

                        
    /**
     * Set the value of Volume
     *
     * @param v new value
     */
    public void setVolume(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.volume, v))
              {
            this.volume = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitConversion
     *
     * @return BigDecimal
     */
    public BigDecimal getUnitConversion()
    {
        return unitConversion;
    }

                        
    /**
     * Set the value of UnitConversion
     *
     * @param v new value
     */
    public void setUnitConversion(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.unitConversion, v))
              {
            this.unitConversion = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Brand
     *
     * @return String
     */
    public String getBrand()
    {
        return brand;
    }

                        
    /**
     * Set the value of Brand
     *
     * @param v new value
     */
    public void setBrand(String v) 
    {
    
                  if (!ObjectUtils.equals(this.brand, v))
              {
            this.brand = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Manufacturer
     *
     * @return String
     */
    public String getManufacturer()
    {
        return manufacturer;
    }

                        
    /**
     * Set the value of Manufacturer
     *
     * @param v new value
     */
    public void setManufacturer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.manufacturer, v))
              {
            this.manufacturer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TrackSerialNo
     *
     * @return boolean
     */
    public boolean getTrackSerialNo()
    {
        return trackSerialNo;
    }

                        
    /**
     * Set the value of TrackSerialNo
     *
     * @param v new value
     */
    public void setTrackSerialNo(boolean v) 
    {
    
                  if (this.trackSerialNo != v)
              {
            this.trackSerialNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TrackBatchNo
     *
     * @return boolean
     */
    public boolean getTrackBatchNo()
    {
        return trackBatchNo;
    }

                        
    /**
     * Set the value of TrackBatchNo
     *
     * @param v new value
     */
    public void setTrackBatchNo(boolean v) 
    {
    
                  if (this.trackBatchNo != v)
              {
            this.trackBatchNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemStatus
     *
     * @return boolean
     */
    public boolean getItemStatus()
    {
        return itemStatus;
    }

                        
    /**
     * Set the value of ItemStatus
     *
     * @param v new value
     */
    public void setItemStatus(boolean v) 
    {
    
                  if (this.itemStatus != v)
              {
            this.itemStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryAccount
     *
     * @return String
     */
    public String getInventoryAccount()
    {
        return inventoryAccount;
    }

                        
    /**
     * Set the value of InventoryAccount
     *
     * @param v new value
     */
    public void setInventoryAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inventoryAccount, v))
              {
            this.inventoryAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAccount
     *
     * @return String
     */
    public String getSalesAccount()
    {
        return salesAccount;
    }

                        
    /**
     * Set the value of SalesAccount
     *
     * @param v new value
     */
    public void setSalesAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAccount, v))
              {
            this.salesAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesReturnAccount
     *
     * @return String
     */
    public String getSalesReturnAccount()
    {
        return salesReturnAccount;
    }

                        
    /**
     * Set the value of SalesReturnAccount
     *
     * @param v new value
     */
    public void setSalesReturnAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesReturnAccount, v))
              {
            this.salesReturnAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemDiscountAccount
     *
     * @return String
     */
    public String getItemDiscountAccount()
    {
        return itemDiscountAccount;
    }

                        
    /**
     * Set the value of ItemDiscountAccount
     *
     * @param v new value
     */
    public void setItemDiscountAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemDiscountAccount, v))
              {
            this.itemDiscountAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CogsAccount
     *
     * @return String
     */
    public String getCogsAccount()
    {
        return cogsAccount;
    }

                        
    /**
     * Set the value of CogsAccount
     *
     * @param v new value
     */
    public void setCogsAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cogsAccount, v))
              {
            this.cogsAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseReturnAccount
     *
     * @return String
     */
    public String getPurchaseReturnAccount()
    {
        return purchaseReturnAccount;
    }

                        
    /**
     * Set the value of PurchaseReturnAccount
     *
     * @param v new value
     */
    public void setPurchaseReturnAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseReturnAccount, v))
              {
            this.purchaseReturnAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpenseAccount
     *
     * @return String
     */
    public String getExpenseAccount()
    {
        return expenseAccount;
    }

                        
    /**
     * Set the value of ExpenseAccount
     *
     * @param v new value
     */
    public void setExpenseAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.expenseAccount, v))
              {
            this.expenseAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnbilledGoodsAccount
     *
     * @return String
     */
    public String getUnbilledGoodsAccount()
    {
        return unbilledGoodsAccount;
    }

                        
    /**
     * Set the value of UnbilledGoodsAccount
     *
     * @param v new value
     */
    public void setUnbilledGoodsAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unbilledGoodsAccount, v))
              {
            this.unbilledGoodsAccount = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Kategori aKategori;

    /**
     * Declares an association between this object and a Kategori object
     *
     * @param v Kategori
     * @throws TorqueException
     */
    public void setKategori(Kategori v) throws TorqueException
    {
            if (v == null)
        {
                  setKategoriId((String) null);
              }
        else
        {
            setKategoriId(v.getKategoriId());
        }
            aKategori = v;
    }

                                            
    /**
     * Get the associated Kategori object
     *
     * @return the associated Kategori object
     * @throws TorqueException
     */
    public Kategori getKategori() throws TorqueException
    {
        if (aKategori == null && (!ObjectUtils.equals(this.kategoriId, null)))
        {
                          aKategori = KategoriPeer.retrieveByPK(SimpleKey.keyFor(this.kategoriId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Kategori obj = KategoriPeer.retrieveByPK(this.kategoriId);
               obj.addItems(this);
            */
        }
        return aKategori;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setKategoriKey(ObjectKey key) throws TorqueException
    {
      
                        setKategoriId(key.toString());
                  }
       
                                        
            
          /**
     * Collection to store aggregation of collItemGroupsRelatedByGroupId
     */
    protected List collItemGroupsRelatedByGroupId;

    /**
     * Temporary storage of collItemGroupsRelatedByGroupId to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initItemGroupsRelatedByGroupId()
    {
        if (collItemGroupsRelatedByGroupId == null)
        {
            collItemGroupsRelatedByGroupId = new ArrayList();
        }
    }

    /**
     * Method called to associate a ItemGroup object to this object
     * through the ItemGroup foreign key attribute
     *
     * @param l ItemGroup
     * @throws TorqueException
     */
    public void addItemGroupRelatedByGroupId(ItemGroup l) throws TorqueException
    {
        getItemGroupsRelatedByGroupId().add(l);
        l.setItemRelatedByGroupId((Item) this);
    }

    /**
     * The criteria used to select the current contents of collItemGroupsRelatedByGroupId
     */
    private Criteria lastItemGroupsRelatedByGroupIdCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItemGroupsRelatedByGroupId(new Criteria())
     *
     * @throws TorqueException
     */
    public List getItemGroupsRelatedByGroupId() throws TorqueException
    {
              if (collItemGroupsRelatedByGroupId == null)
        {
            collItemGroupsRelatedByGroupId = getItemGroupsRelatedByGroupId(new Criteria(10));
        }
        return collItemGroupsRelatedByGroupId;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Item has previously
     * been saved, it will retrieve related ItemGroupsRelatedByGroupId from storage.
     * If this Item is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getItemGroupsRelatedByGroupId(Criteria criteria) throws TorqueException
    {
              if (collItemGroupsRelatedByGroupId == null)
        {
            if (isNew())
            {
               collItemGroupsRelatedByGroupId = new ArrayList();
            }
            else
            {
                        criteria.add(ItemGroupPeer.GROUP_ID, getItemId() );
                        collItemGroupsRelatedByGroupId = ItemGroupPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ItemGroupPeer.GROUP_ID, getItemId());
                            if (!lastItemGroupsRelatedByGroupIdCriteria.equals(criteria))
                {
                    collItemGroupsRelatedByGroupId = ItemGroupPeer.doSelect(criteria);
                }
            }
        }
        lastItemGroupsRelatedByGroupIdCriteria = criteria;

        return collItemGroupsRelatedByGroupId;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItemGroupsRelatedByGroupId(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItemGroupsRelatedByGroupId(Connection con) throws TorqueException
    {
              if (collItemGroupsRelatedByGroupId == null)
        {
            collItemGroupsRelatedByGroupId = getItemGroupsRelatedByGroupId(new Criteria(10), con);
        }
        return collItemGroupsRelatedByGroupId;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Item has previously
     * been saved, it will retrieve related ItemGroupsRelatedByGroupId from storage.
     * If this Item is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItemGroupsRelatedByGroupId(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collItemGroupsRelatedByGroupId == null)
        {
            if (isNew())
            {
               collItemGroupsRelatedByGroupId = new ArrayList();
            }
            else
            {
                         criteria.add(ItemGroupPeer.GROUP_ID, getItemId());
                         collItemGroupsRelatedByGroupId = ItemGroupPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ItemGroupPeer.GROUP_ID, getItemId());
                             if (!lastItemGroupsRelatedByGroupIdCriteria.equals(criteria))
                 {
                     collItemGroupsRelatedByGroupId = ItemGroupPeer.doSelect(criteria, con);
                 }
             }
         }
         lastItemGroupsRelatedByGroupIdCriteria = criteria;

         return collItemGroupsRelatedByGroupId;
           }

                        
              
                    
                              
                                            
                                                                          
                                        
                    
                                            
                  
                    
                              
                                            
                                                                          
                                        
                    
                                
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Item is new, it will return
     * an empty collection; or if this Item has previously
     * been saved, it will retrieve related ItemGroupsRelatedByGroupId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Item.
     */
    protected List getItemGroupsRelatedByGroupIdJoinItemRelatedByItemId(Criteria criteria)
        throws TorqueException
    {
                    if (collItemGroupsRelatedByGroupId == null)
        {
            if (isNew())
            {
               collItemGroupsRelatedByGroupId = new ArrayList();
            }
            else
            {
                              criteria.add(ItemGroupPeer.GROUP_ID, getItemId());
                              collItemGroupsRelatedByGroupId = ItemGroupPeer.doSelectJoinItemRelatedByItemId(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ItemGroupPeer.GROUP_ID, getItemId());
                                    if (!lastItemGroupsRelatedByGroupIdCriteria.equals(criteria))
            {
                collItemGroupsRelatedByGroupId = ItemGroupPeer.doSelectJoinItemRelatedByItemId(criteria);
            }
        }
        lastItemGroupsRelatedByGroupIdCriteria = criteria;

        return collItemGroupsRelatedByGroupId;
                }
                            


                                  
            
          /**
     * Collection to store aggregation of collItemGroupsRelatedByItemId
     */
    protected List collItemGroupsRelatedByItemId;

    /**
     * Temporary storage of collItemGroupsRelatedByItemId to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initItemGroupsRelatedByItemId()
    {
        if (collItemGroupsRelatedByItemId == null)
        {
            collItemGroupsRelatedByItemId = new ArrayList();
        }
    }

    /**
     * Method called to associate a ItemGroup object to this object
     * through the ItemGroup foreign key attribute
     *
     * @param l ItemGroup
     * @throws TorqueException
     */
    public void addItemGroupRelatedByItemId(ItemGroup l) throws TorqueException
    {
        getItemGroupsRelatedByItemId().add(l);
        l.setItemRelatedByItemId((Item) this);
    }

    /**
     * The criteria used to select the current contents of collItemGroupsRelatedByItemId
     */
    private Criteria lastItemGroupsRelatedByItemIdCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItemGroupsRelatedByItemId(new Criteria())
     *
     * @throws TorqueException
     */
    public List getItemGroupsRelatedByItemId() throws TorqueException
    {
              if (collItemGroupsRelatedByItemId == null)
        {
            collItemGroupsRelatedByItemId = getItemGroupsRelatedByItemId(new Criteria(10));
        }
        return collItemGroupsRelatedByItemId;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Item has previously
     * been saved, it will retrieve related ItemGroupsRelatedByItemId from storage.
     * If this Item is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getItemGroupsRelatedByItemId(Criteria criteria) throws TorqueException
    {
              if (collItemGroupsRelatedByItemId == null)
        {
            if (isNew())
            {
               collItemGroupsRelatedByItemId = new ArrayList();
            }
            else
            {
                        criteria.add(ItemGroupPeer.ITEM_ID, getItemId() );
                        collItemGroupsRelatedByItemId = ItemGroupPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ItemGroupPeer.ITEM_ID, getItemId());
                            if (!lastItemGroupsRelatedByItemIdCriteria.equals(criteria))
                {
                    collItemGroupsRelatedByItemId = ItemGroupPeer.doSelect(criteria);
                }
            }
        }
        lastItemGroupsRelatedByItemIdCriteria = criteria;

        return collItemGroupsRelatedByItemId;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItemGroupsRelatedByItemId(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItemGroupsRelatedByItemId(Connection con) throws TorqueException
    {
              if (collItemGroupsRelatedByItemId == null)
        {
            collItemGroupsRelatedByItemId = getItemGroupsRelatedByItemId(new Criteria(10), con);
        }
        return collItemGroupsRelatedByItemId;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Item has previously
     * been saved, it will retrieve related ItemGroupsRelatedByItemId from storage.
     * If this Item is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItemGroupsRelatedByItemId(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collItemGroupsRelatedByItemId == null)
        {
            if (isNew())
            {
               collItemGroupsRelatedByItemId = new ArrayList();
            }
            else
            {
                         criteria.add(ItemGroupPeer.ITEM_ID, getItemId());
                         collItemGroupsRelatedByItemId = ItemGroupPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ItemGroupPeer.ITEM_ID, getItemId());
                             if (!lastItemGroupsRelatedByItemIdCriteria.equals(criteria))
                 {
                     collItemGroupsRelatedByItemId = ItemGroupPeer.doSelect(criteria, con);
                 }
             }
         }
         lastItemGroupsRelatedByItemIdCriteria = criteria;

         return collItemGroupsRelatedByItemId;
           }

                        
              
                    
                              
                                            
                                                                          
                                        
                    
                                
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Item is new, it will return
     * an empty collection; or if this Item has previously
     * been saved, it will retrieve related ItemGroupsRelatedByItemId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Item.
     */
    protected List getItemGroupsRelatedByItemIdJoinItemRelatedByGroupId(Criteria criteria)
        throws TorqueException
    {
                    if (collItemGroupsRelatedByItemId == null)
        {
            if (isNew())
            {
               collItemGroupsRelatedByItemId = new ArrayList();
            }
            else
            {
                              criteria.add(ItemGroupPeer.ITEM_ID, getItemId());
                              collItemGroupsRelatedByItemId = ItemGroupPeer.doSelectJoinItemRelatedByGroupId(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ItemGroupPeer.ITEM_ID, getItemId());
                                    if (!lastItemGroupsRelatedByItemIdCriteria.equals(criteria))
            {
                collItemGroupsRelatedByItemId = ItemGroupPeer.doSelectJoinItemRelatedByGroupId(criteria);
            }
        }
        lastItemGroupsRelatedByItemIdCriteria = criteria;

        return collItemGroupsRelatedByItemId;
                }
                  
                    
                              
                                            
                                                                          
                                        
                    
                                            
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("Barcode");
              fieldNames.add("ItemName");
              fieldNames.add("Description");
              fieldNames.add("ItemSku");
              fieldNames.add("ItemSkuName");
              fieldNames.add("VendorItemCode");
              fieldNames.add("VendorItemName");
              fieldNames.add("ItemType");
              fieldNames.add("KategoriId");
              fieldNames.add("UnitId");
              fieldNames.add("PurchaseUnitId");
              fieldNames.add("TaxId");
              fieldNames.add("PurchaseTaxId");
              fieldNames.add("PreferedVendorId");
              fieldNames.add("ItemPrice");
              fieldNames.add("MinPrice");
              fieldNames.add("IsFixedPrice");
              fieldNames.add("IsDiscontinue");
              fieldNames.add("IsConsignment");
              fieldNames.add("ItemPicture");
              fieldNames.add("Tags");
              fieldNames.add("DisplayStore");
              fieldNames.add("LastPurchasePrice");
              fieldNames.add("LastPurchaseCurr");
              fieldNames.add("LastPurchaseRate");
              fieldNames.add("ProfitMargin");
              fieldNames.add("MinimumStock");
              fieldNames.add("ReorderPoint");
              fieldNames.add("MaximumStock");
              fieldNames.add("RackLocation");
              fieldNames.add("WarehouseId");
              fieldNames.add("DeliveryType");
              fieldNames.add("ItemStatusCodeId");
              fieldNames.add("StatusColor");
              fieldNames.add("DiscountAmount");
              fieldNames.add("CreateBy");
              fieldNames.add("AddDate");
              fieldNames.add("UpdateDate");
              fieldNames.add("LastAdjustment");
              fieldNames.add("Size");
              fieldNames.add("Color");
              fieldNames.add("SizeUnit");
              fieldNames.add("Weight");
              fieldNames.add("Volume");
              fieldNames.add("UnitConversion");
              fieldNames.add("Brand");
              fieldNames.add("Manufacturer");
              fieldNames.add("TrackSerialNo");
              fieldNames.add("TrackBatchNo");
              fieldNames.add("ItemStatus");
              fieldNames.add("InventoryAccount");
              fieldNames.add("SalesAccount");
              fieldNames.add("SalesReturnAccount");
              fieldNames.add("ItemDiscountAccount");
              fieldNames.add("CogsAccount");
              fieldNames.add("PurchaseReturnAccount");
              fieldNames.add("ExpenseAccount");
              fieldNames.add("UnbilledGoodsAccount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("Barcode"))
        {
                return getBarcode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("ItemSku"))
        {
                return getItemSku();
            }
          if (name.equals("ItemSkuName"))
        {
                return getItemSkuName();
            }
          if (name.equals("VendorItemCode"))
        {
                return getVendorItemCode();
            }
          if (name.equals("VendorItemName"))
        {
                return getVendorItemName();
            }
          if (name.equals("ItemType"))
        {
                return Integer.valueOf(getItemType());
            }
          if (name.equals("KategoriId"))
        {
                return getKategoriId();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("PurchaseUnitId"))
        {
                return getPurchaseUnitId();
            }
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("PurchaseTaxId"))
        {
                return getPurchaseTaxId();
            }
          if (name.equals("PreferedVendorId"))
        {
                return getPreferedVendorId();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          if (name.equals("MinPrice"))
        {
                return getMinPrice();
            }
          if (name.equals("IsFixedPrice"))
        {
                return Boolean.valueOf(getIsFixedPrice());
            }
          if (name.equals("IsDiscontinue"))
        {
                return Boolean.valueOf(getIsDiscontinue());
            }
          if (name.equals("IsConsignment"))
        {
                return Boolean.valueOf(getIsConsignment());
            }
          if (name.equals("ItemPicture"))
        {
                return getItemPicture();
            }
          if (name.equals("Tags"))
        {
                return getTags();
            }
          if (name.equals("DisplayStore"))
        {
                return Boolean.valueOf(getDisplayStore());
            }
          if (name.equals("LastPurchasePrice"))
        {
                return getLastPurchasePrice();
            }
          if (name.equals("LastPurchaseCurr"))
        {
                return getLastPurchaseCurr();
            }
          if (name.equals("LastPurchaseRate"))
        {
                return getLastPurchaseRate();
            }
          if (name.equals("ProfitMargin"))
        {
                return getProfitMargin();
            }
          if (name.equals("MinimumStock"))
        {
                return getMinimumStock();
            }
          if (name.equals("ReorderPoint"))
        {
                return getReorderPoint();
            }
          if (name.equals("MaximumStock"))
        {
                return getMaximumStock();
            }
          if (name.equals("RackLocation"))
        {
                return getRackLocation();
            }
          if (name.equals("WarehouseId"))
        {
                return getWarehouseId();
            }
          if (name.equals("DeliveryType"))
        {
                return Integer.valueOf(getDeliveryType());
            }
          if (name.equals("ItemStatusCodeId"))
        {
                return getItemStatusCodeId();
            }
          if (name.equals("StatusColor"))
        {
                return getStatusColor();
            }
          if (name.equals("DiscountAmount"))
        {
                return getDiscountAmount();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("AddDate"))
        {
                return getAddDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LastAdjustment"))
        {
                return getLastAdjustment();
            }
          if (name.equals("Size"))
        {
                return getSize();
            }
          if (name.equals("Color"))
        {
                return getColor();
            }
          if (name.equals("SizeUnit"))
        {
                return getSizeUnit();
            }
          if (name.equals("Weight"))
        {
                return getWeight();
            }
          if (name.equals("Volume"))
        {
                return getVolume();
            }
          if (name.equals("UnitConversion"))
        {
                return getUnitConversion();
            }
          if (name.equals("Brand"))
        {
                return getBrand();
            }
          if (name.equals("Manufacturer"))
        {
                return getManufacturer();
            }
          if (name.equals("TrackSerialNo"))
        {
                return Boolean.valueOf(getTrackSerialNo());
            }
          if (name.equals("TrackBatchNo"))
        {
                return Boolean.valueOf(getTrackBatchNo());
            }
          if (name.equals("ItemStatus"))
        {
                return Boolean.valueOf(getItemStatus());
            }
          if (name.equals("InventoryAccount"))
        {
                return getInventoryAccount();
            }
          if (name.equals("SalesAccount"))
        {
                return getSalesAccount();
            }
          if (name.equals("SalesReturnAccount"))
        {
                return getSalesReturnAccount();
            }
          if (name.equals("ItemDiscountAccount"))
        {
                return getItemDiscountAccount();
            }
          if (name.equals("CogsAccount"))
        {
                return getCogsAccount();
            }
          if (name.equals("PurchaseReturnAccount"))
        {
                return getPurchaseReturnAccount();
            }
          if (name.equals("ExpenseAccount"))
        {
                return getExpenseAccount();
            }
          if (name.equals("UnbilledGoodsAccount"))
        {
                return getUnbilledGoodsAccount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(ItemPeer.BARCODE))
        {
                return getBarcode();
            }
          if (name.equals(ItemPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(ItemPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(ItemPeer.ITEM_SKU))
        {
                return getItemSku();
            }
          if (name.equals(ItemPeer.ITEM_SKU_NAME))
        {
                return getItemSkuName();
            }
          if (name.equals(ItemPeer.VENDOR_ITEM_CODE))
        {
                return getVendorItemCode();
            }
          if (name.equals(ItemPeer.VENDOR_ITEM_NAME))
        {
                return getVendorItemName();
            }
          if (name.equals(ItemPeer.ITEM_TYPE))
        {
                return Integer.valueOf(getItemType());
            }
          if (name.equals(ItemPeer.KATEGORI_ID))
        {
                return getKategoriId();
            }
          if (name.equals(ItemPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(ItemPeer.PURCHASE_UNIT_ID))
        {
                return getPurchaseUnitId();
            }
          if (name.equals(ItemPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(ItemPeer.PURCHASE_TAX_ID))
        {
                return getPurchaseTaxId();
            }
          if (name.equals(ItemPeer.PREFERED_VENDOR_ID))
        {
                return getPreferedVendorId();
            }
          if (name.equals(ItemPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          if (name.equals(ItemPeer.MIN_PRICE))
        {
                return getMinPrice();
            }
          if (name.equals(ItemPeer.IS_FIXED_PRICE))
        {
                return Boolean.valueOf(getIsFixedPrice());
            }
          if (name.equals(ItemPeer.IS_DISCONTINUE))
        {
                return Boolean.valueOf(getIsDiscontinue());
            }
          if (name.equals(ItemPeer.IS_CONSIGNMENT))
        {
                return Boolean.valueOf(getIsConsignment());
            }
          if (name.equals(ItemPeer.ITEM_PICTURE))
        {
                return getItemPicture();
            }
          if (name.equals(ItemPeer.TAGS))
        {
                return getTags();
            }
          if (name.equals(ItemPeer.DISPLAY_STORE))
        {
                return Boolean.valueOf(getDisplayStore());
            }
          if (name.equals(ItemPeer.LAST_PURCHASE_PRICE))
        {
                return getLastPurchasePrice();
            }
          if (name.equals(ItemPeer.LAST_PURCHASE_CURR))
        {
                return getLastPurchaseCurr();
            }
          if (name.equals(ItemPeer.LAST_PURCHASE_RATE))
        {
                return getLastPurchaseRate();
            }
          if (name.equals(ItemPeer.PROFIT_MARGIN))
        {
                return getProfitMargin();
            }
          if (name.equals(ItemPeer.MINIMUM_STOCK))
        {
                return getMinimumStock();
            }
          if (name.equals(ItemPeer.REORDER_POINT))
        {
                return getReorderPoint();
            }
          if (name.equals(ItemPeer.MAXIMUM_STOCK))
        {
                return getMaximumStock();
            }
          if (name.equals(ItemPeer.RACK_LOCATION))
        {
                return getRackLocation();
            }
          if (name.equals(ItemPeer.WAREHOUSE_ID))
        {
                return getWarehouseId();
            }
          if (name.equals(ItemPeer.DELIVERY_TYPE))
        {
                return Integer.valueOf(getDeliveryType());
            }
          if (name.equals(ItemPeer.ITEM_STATUS_CODE_ID))
        {
                return getItemStatusCodeId();
            }
          if (name.equals(ItemPeer.STATUS_COLOR))
        {
                return getStatusColor();
            }
          if (name.equals(ItemPeer.DISCOUNT_AMOUNT))
        {
                return getDiscountAmount();
            }
          if (name.equals(ItemPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(ItemPeer.ADD_DATE))
        {
                return getAddDate();
            }
          if (name.equals(ItemPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(ItemPeer.LAST_ADJUSTMENT))
        {
                return getLastAdjustment();
            }
          if (name.equals(ItemPeer.SIZE))
        {
                return getSize();
            }
          if (name.equals(ItemPeer.COLOR))
        {
                return getColor();
            }
          if (name.equals(ItemPeer.SIZE_UNIT))
        {
                return getSizeUnit();
            }
          if (name.equals(ItemPeer.WEIGHT))
        {
                return getWeight();
            }
          if (name.equals(ItemPeer.VOLUME))
        {
                return getVolume();
            }
          if (name.equals(ItemPeer.UNIT_CONVERSION))
        {
                return getUnitConversion();
            }
          if (name.equals(ItemPeer.BRAND))
        {
                return getBrand();
            }
          if (name.equals(ItemPeer.MANUFACTURER))
        {
                return getManufacturer();
            }
          if (name.equals(ItemPeer.TRACK_SERIAL_NO))
        {
                return Boolean.valueOf(getTrackSerialNo());
            }
          if (name.equals(ItemPeer.TRACK_BATCH_NO))
        {
                return Boolean.valueOf(getTrackBatchNo());
            }
          if (name.equals(ItemPeer.ITEM_STATUS))
        {
                return Boolean.valueOf(getItemStatus());
            }
          if (name.equals(ItemPeer.INVENTORY_ACCOUNT))
        {
                return getInventoryAccount();
            }
          if (name.equals(ItemPeer.SALES_ACCOUNT))
        {
                return getSalesAccount();
            }
          if (name.equals(ItemPeer.SALES_RETURN_ACCOUNT))
        {
                return getSalesReturnAccount();
            }
          if (name.equals(ItemPeer.ITEM_DISCOUNT_ACCOUNT))
        {
                return getItemDiscountAccount();
            }
          if (name.equals(ItemPeer.COGS_ACCOUNT))
        {
                return getCogsAccount();
            }
          if (name.equals(ItemPeer.PURCHASE_RETURN_ACCOUNT))
        {
                return getPurchaseReturnAccount();
            }
          if (name.equals(ItemPeer.EXPENSE_ACCOUNT))
        {
                return getExpenseAccount();
            }
          if (name.equals(ItemPeer.UNBILLED_GOODS_ACCOUNT))
        {
                return getUnbilledGoodsAccount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemId();
            }
              if (pos == 1)
        {
                return getItemCode();
            }
              if (pos == 2)
        {
                return getBarcode();
            }
              if (pos == 3)
        {
                return getItemName();
            }
              if (pos == 4)
        {
                return getDescription();
            }
              if (pos == 5)
        {
                return getItemSku();
            }
              if (pos == 6)
        {
                return getItemSkuName();
            }
              if (pos == 7)
        {
                return getVendorItemCode();
            }
              if (pos == 8)
        {
                return getVendorItemName();
            }
              if (pos == 9)
        {
                return Integer.valueOf(getItemType());
            }
              if (pos == 10)
        {
                return getKategoriId();
            }
              if (pos == 11)
        {
                return getUnitId();
            }
              if (pos == 12)
        {
                return getPurchaseUnitId();
            }
              if (pos == 13)
        {
                return getTaxId();
            }
              if (pos == 14)
        {
                return getPurchaseTaxId();
            }
              if (pos == 15)
        {
                return getPreferedVendorId();
            }
              if (pos == 16)
        {
                return getItemPrice();
            }
              if (pos == 17)
        {
                return getMinPrice();
            }
              if (pos == 18)
        {
                return Boolean.valueOf(getIsFixedPrice());
            }
              if (pos == 19)
        {
                return Boolean.valueOf(getIsDiscontinue());
            }
              if (pos == 20)
        {
                return Boolean.valueOf(getIsConsignment());
            }
              if (pos == 21)
        {
                return getItemPicture();
            }
              if (pos == 22)
        {
                return getTags();
            }
              if (pos == 23)
        {
                return Boolean.valueOf(getDisplayStore());
            }
              if (pos == 24)
        {
                return getLastPurchasePrice();
            }
              if (pos == 25)
        {
                return getLastPurchaseCurr();
            }
              if (pos == 26)
        {
                return getLastPurchaseRate();
            }
              if (pos == 27)
        {
                return getProfitMargin();
            }
              if (pos == 28)
        {
                return getMinimumStock();
            }
              if (pos == 29)
        {
                return getReorderPoint();
            }
              if (pos == 30)
        {
                return getMaximumStock();
            }
              if (pos == 31)
        {
                return getRackLocation();
            }
              if (pos == 32)
        {
                return getWarehouseId();
            }
              if (pos == 33)
        {
                return Integer.valueOf(getDeliveryType());
            }
              if (pos == 34)
        {
                return getItemStatusCodeId();
            }
              if (pos == 35)
        {
                return getStatusColor();
            }
              if (pos == 36)
        {
                return getDiscountAmount();
            }
              if (pos == 37)
        {
                return getCreateBy();
            }
              if (pos == 38)
        {
                return getAddDate();
            }
              if (pos == 39)
        {
                return getUpdateDate();
            }
              if (pos == 40)
        {
                return getLastAdjustment();
            }
              if (pos == 41)
        {
                return getSize();
            }
              if (pos == 42)
        {
                return getColor();
            }
              if (pos == 43)
        {
                return getSizeUnit();
            }
              if (pos == 44)
        {
                return getWeight();
            }
              if (pos == 45)
        {
                return getVolume();
            }
              if (pos == 46)
        {
                return getUnitConversion();
            }
              if (pos == 47)
        {
                return getBrand();
            }
              if (pos == 48)
        {
                return getManufacturer();
            }
              if (pos == 49)
        {
                return Boolean.valueOf(getTrackSerialNo());
            }
              if (pos == 50)
        {
                return Boolean.valueOf(getTrackBatchNo());
            }
              if (pos == 51)
        {
                return Boolean.valueOf(getItemStatus());
            }
              if (pos == 52)
        {
                return getInventoryAccount();
            }
              if (pos == 53)
        {
                return getSalesAccount();
            }
              if (pos == 54)
        {
                return getSalesReturnAccount();
            }
              if (pos == 55)
        {
                return getItemDiscountAccount();
            }
              if (pos == 56)
        {
                return getCogsAccount();
            }
              if (pos == 57)
        {
                return getPurchaseReturnAccount();
            }
              if (pos == 58)
        {
                return getExpenseAccount();
            }
              if (pos == 59)
        {
                return getUnbilledGoodsAccount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemPeer.doInsert((Item) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemPeer.doUpdate((Item) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                                
                
                    if (collItemGroupsRelatedByGroupId != null)
            {
                for (int i = 0; i < collItemGroupsRelatedByGroupId.size(); i++)
                {
                    ((ItemGroup) collItemGroupsRelatedByGroupId.get(i)).save(con);
                }
            }
                                                            
                
                    if (collItemGroupsRelatedByItemId != null)
            {
                for (int i = 0; i < collItemGroupsRelatedByItemId.size(); i++)
                {
                    ((ItemGroup) collItemGroupsRelatedByItemId.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setItemId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setItemId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Item copy() throws TorqueException
    {
        return copyInto(new Item());
    }
  
    protected Item copyInto(Item copyObj) throws TorqueException
    {
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setBarcode(barcode);
          copyObj.setItemName(itemName);
          copyObj.setDescription(description);
          copyObj.setItemSku(itemSku);
          copyObj.setItemSkuName(itemSkuName);
          copyObj.setVendorItemCode(vendorItemCode);
          copyObj.setVendorItemName(vendorItemName);
          copyObj.setItemType(itemType);
          copyObj.setKategoriId(kategoriId);
          copyObj.setUnitId(unitId);
          copyObj.setPurchaseUnitId(purchaseUnitId);
          copyObj.setTaxId(taxId);
          copyObj.setPurchaseTaxId(purchaseTaxId);
          copyObj.setPreferedVendorId(preferedVendorId);
          copyObj.setItemPrice(itemPrice);
          copyObj.setMinPrice(minPrice);
          copyObj.setIsFixedPrice(isFixedPrice);
          copyObj.setIsDiscontinue(isDiscontinue);
          copyObj.setIsConsignment(isConsignment);
          copyObj.setItemPicture(itemPicture);
          copyObj.setTags(tags);
          copyObj.setDisplayStore(displayStore);
          copyObj.setLastPurchasePrice(lastPurchasePrice);
          copyObj.setLastPurchaseCurr(lastPurchaseCurr);
          copyObj.setLastPurchaseRate(lastPurchaseRate);
          copyObj.setProfitMargin(profitMargin);
          copyObj.setMinimumStock(minimumStock);
          copyObj.setReorderPoint(reorderPoint);
          copyObj.setMaximumStock(maximumStock);
          copyObj.setRackLocation(rackLocation);
          copyObj.setWarehouseId(warehouseId);
          copyObj.setDeliveryType(deliveryType);
          copyObj.setItemStatusCodeId(itemStatusCodeId);
          copyObj.setStatusColor(statusColor);
          copyObj.setDiscountAmount(discountAmount);
          copyObj.setCreateBy(createBy);
          copyObj.setAddDate(addDate);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLastAdjustment(lastAdjustment);
          copyObj.setSize(size);
          copyObj.setColor(color);
          copyObj.setSizeUnit(sizeUnit);
          copyObj.setWeight(weight);
          copyObj.setVolume(volume);
          copyObj.setUnitConversion(unitConversion);
          copyObj.setBrand(brand);
          copyObj.setManufacturer(manufacturer);
          copyObj.setTrackSerialNo(trackSerialNo);
          copyObj.setTrackBatchNo(trackBatchNo);
          copyObj.setItemStatus(itemStatus);
          copyObj.setInventoryAccount(inventoryAccount);
          copyObj.setSalesAccount(salesAccount);
          copyObj.setSalesReturnAccount(salesReturnAccount);
          copyObj.setItemDiscountAccount(itemDiscountAccount);
          copyObj.setCogsAccount(cogsAccount);
          copyObj.setPurchaseReturnAccount(purchaseReturnAccount);
          copyObj.setExpenseAccount(expenseAccount);
          copyObj.setUnbilledGoodsAccount(unbilledGoodsAccount);
  
                    copyObj.setItemId((String)null);
                                                                                                                                                                                                                                                                                                                                                                              
                                                
                            
        List v = getItemGroupsRelatedByGroupId();
        for (int i = 0; i < v.size(); i++)
        {
            ItemGroup obj = (ItemGroup) v.get(i);
            copyObj.addItemGroupRelatedByGroupId(obj.copy());
        }
                                                            
                            
        v = getItemGroupsRelatedByItemId();
        for (int i = 0; i < v.size(); i++)
        {
            ItemGroup obj = (ItemGroup) v.get(i);
            copyObj.addItemGroupRelatedByItemId(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Item\n");
        str.append("----\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("Barcode              : ")
           .append(getBarcode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("ItemSku              : ")
           .append(getItemSku())
           .append("\n")
           .append("ItemSkuName          : ")
           .append(getItemSkuName())
           .append("\n")
           .append("VendorItemCode       : ")
           .append(getVendorItemCode())
           .append("\n")
           .append("VendorItemName       : ")
           .append(getVendorItemName())
           .append("\n")
           .append("ItemType             : ")
           .append(getItemType())
           .append("\n")
           .append("KategoriId           : ")
           .append(getKategoriId())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("PurchaseUnitId       : ")
           .append(getPurchaseUnitId())
           .append("\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("PurchaseTaxId        : ")
           .append(getPurchaseTaxId())
           .append("\n")
           .append("PreferedVendorId     : ")
           .append(getPreferedVendorId())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
           .append("MinPrice             : ")
           .append(getMinPrice())
           .append("\n")
           .append("IsFixedPrice         : ")
           .append(getIsFixedPrice())
           .append("\n")
           .append("IsDiscontinue        : ")
           .append(getIsDiscontinue())
           .append("\n")
           .append("IsConsignment        : ")
           .append(getIsConsignment())
           .append("\n")
           .append("ItemPicture          : ")
           .append(getItemPicture())
           .append("\n")
           .append("Tags                 : ")
           .append(getTags())
           .append("\n")
           .append("DisplayStore         : ")
           .append(getDisplayStore())
           .append("\n")
           .append("LastPurchasePrice    : ")
           .append(getLastPurchasePrice())
           .append("\n")
           .append("LastPurchaseCurr     : ")
           .append(getLastPurchaseCurr())
           .append("\n")
           .append("LastPurchaseRate     : ")
           .append(getLastPurchaseRate())
           .append("\n")
           .append("ProfitMargin         : ")
           .append(getProfitMargin())
           .append("\n")
           .append("MinimumStock         : ")
           .append(getMinimumStock())
           .append("\n")
           .append("ReorderPoint         : ")
           .append(getReorderPoint())
           .append("\n")
           .append("MaximumStock         : ")
           .append(getMaximumStock())
           .append("\n")
           .append("RackLocation         : ")
           .append(getRackLocation())
           .append("\n")
           .append("WarehouseId          : ")
           .append(getWarehouseId())
           .append("\n")
           .append("DeliveryType         : ")
           .append(getDeliveryType())
           .append("\n")
           .append("ItemStatusCodeId     : ")
           .append(getItemStatusCodeId())
           .append("\n")
           .append("StatusColor          : ")
           .append(getStatusColor())
           .append("\n")
           .append("DiscountAmount       : ")
           .append(getDiscountAmount())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("AddDate              : ")
           .append(getAddDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LastAdjustment       : ")
           .append(getLastAdjustment())
           .append("\n")
           .append("Size                 : ")
           .append(getSize())
           .append("\n")
           .append("Color                : ")
           .append(getColor())
           .append("\n")
           .append("SizeUnit             : ")
           .append(getSizeUnit())
           .append("\n")
           .append("Weight               : ")
           .append(getWeight())
           .append("\n")
           .append("Volume               : ")
           .append(getVolume())
           .append("\n")
           .append("UnitConversion       : ")
           .append(getUnitConversion())
           .append("\n")
           .append("Brand                : ")
           .append(getBrand())
           .append("\n")
           .append("Manufacturer         : ")
           .append(getManufacturer())
           .append("\n")
           .append("TrackSerialNo        : ")
           .append(getTrackSerialNo())
           .append("\n")
           .append("TrackBatchNo         : ")
           .append(getTrackBatchNo())
           .append("\n")
           .append("ItemStatus           : ")
           .append(getItemStatus())
           .append("\n")
           .append("InventoryAccount     : ")
           .append(getInventoryAccount())
           .append("\n")
           .append("SalesAccount         : ")
           .append(getSalesAccount())
           .append("\n")
           .append("SalesReturnAccount   : ")
           .append(getSalesReturnAccount())
           .append("\n")
           .append("ItemDiscountAccount  : ")
           .append(getItemDiscountAccount())
           .append("\n")
           .append("CogsAccount          : ")
           .append(getCogsAccount())
           .append("\n")
            .append("PurchaseReturnAccount   : ")
           .append(getPurchaseReturnAccount())
           .append("\n")
           .append("ExpenseAccount       : ")
           .append(getExpenseAccount())
           .append("\n")
           .append("UnbilledGoodsAccount   : ")
           .append(getUnbilledGoodsAccount())
           .append("\n")
        ;
        return(str.toString());
    }
}
