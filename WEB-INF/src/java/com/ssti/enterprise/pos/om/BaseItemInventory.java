package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemInventory
 */
public abstract class BaseItemInventory extends BaseObject
{
    /** The Peer class */
    private static final ItemInventoryPeer peer =
        new ItemInventoryPeer();

        
    /** The value for the itemInventoryId field */
    private String itemInventoryId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
                                                
          
    /** The value for the maximumQty field */
    private BigDecimal maximumQty= bd_ZERO;
                                                
          
    /** The value for the minimumQty field */
    private BigDecimal minimumQty= bd_ZERO;
                                                
          
    /** The value for the reorderPoint field */
    private BigDecimal reorderPoint= bd_ZERO;
                                                
    /** The value for the rackLocation field */
    private String rackLocation = "";
      
    /** The value for the active field */
    private boolean active;
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the ItemInventoryId
     *
     * @return String
     */
    public String getItemInventoryId()
    {
        return itemInventoryId;
    }

                        
    /**
     * Set the value of ItemInventoryId
     *
     * @param v new value
     */
    public void setItemInventoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemInventoryId, v))
              {
            this.itemInventoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MaximumQty
     *
     * @return BigDecimal
     */
    public BigDecimal getMaximumQty()
    {
        return maximumQty;
    }

                        
    /**
     * Set the value of MaximumQty
     *
     * @param v new value
     */
    public void setMaximumQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.maximumQty, v))
              {
            this.maximumQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MinimumQty
     *
     * @return BigDecimal
     */
    public BigDecimal getMinimumQty()
    {
        return minimumQty;
    }

                        
    /**
     * Set the value of MinimumQty
     *
     * @param v new value
     */
    public void setMinimumQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.minimumQty, v))
              {
            this.minimumQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReorderPoint
     *
     * @return BigDecimal
     */
    public BigDecimal getReorderPoint()
    {
        return reorderPoint;
    }

                        
    /**
     * Set the value of ReorderPoint
     *
     * @param v new value
     */
    public void setReorderPoint(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.reorderPoint, v))
              {
            this.reorderPoint = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RackLocation
     *
     * @return String
     */
    public String getRackLocation()
    {
        return rackLocation;
    }

                        
    /**
     * Set the value of RackLocation
     *
     * @param v new value
     */
    public void setRackLocation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.rackLocation, v))
              {
            this.rackLocation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Active
     *
     * @return boolean
     */
    public boolean getActive()
    {
        return active;
    }

                        
    /**
     * Set the value of Active
     *
     * @param v new value
     */
    public void setActive(boolean v) 
    {
    
                  if (this.active != v)
              {
            this.active = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemInventoryId");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("MaximumQty");
              fieldNames.add("MinimumQty");
              fieldNames.add("ReorderPoint");
              fieldNames.add("RackLocation");
              fieldNames.add("Active");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemInventoryId"))
        {
                return getItemInventoryId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("MaximumQty"))
        {
                return getMaximumQty();
            }
          if (name.equals("MinimumQty"))
        {
                return getMinimumQty();
            }
          if (name.equals("ReorderPoint"))
        {
                return getReorderPoint();
            }
          if (name.equals("RackLocation"))
        {
                return getRackLocation();
            }
          if (name.equals("Active"))
        {
                return Boolean.valueOf(getActive());
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemInventoryPeer.ITEM_INVENTORY_ID))
        {
                return getItemInventoryId();
            }
          if (name.equals(ItemInventoryPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(ItemInventoryPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(ItemInventoryPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemInventoryPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(ItemInventoryPeer.MAXIMUM_QTY))
        {
                return getMaximumQty();
            }
          if (name.equals(ItemInventoryPeer.MINIMUM_QTY))
        {
                return getMinimumQty();
            }
          if (name.equals(ItemInventoryPeer.REORDER_POINT))
        {
                return getReorderPoint();
            }
          if (name.equals(ItemInventoryPeer.RACK_LOCATION))
        {
                return getRackLocation();
            }
          if (name.equals(ItemInventoryPeer.ACTIVE))
        {
                return Boolean.valueOf(getActive());
            }
          if (name.equals(ItemInventoryPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemInventoryId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getLocationName();
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getMaximumQty();
            }
              if (pos == 6)
        {
                return getMinimumQty();
            }
              if (pos == 7)
        {
                return getReorderPoint();
            }
              if (pos == 8)
        {
                return getRackLocation();
            }
              if (pos == 9)
        {
                return Boolean.valueOf(getActive());
            }
              if (pos == 10)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemInventoryPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemInventoryPeer.doInsert((ItemInventory) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemInventoryPeer.doUpdate((ItemInventory) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemInventoryId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemInventoryId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemInventoryId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemInventoryId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemInventory copy() throws TorqueException
    {
        return copyInto(new ItemInventory());
    }
  
    protected ItemInventory copyInto(ItemInventory copyObj) throws TorqueException
    {
          copyObj.setItemInventoryId(itemInventoryId);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setMaximumQty(maximumQty);
          copyObj.setMinimumQty(minimumQty);
          copyObj.setReorderPoint(reorderPoint);
          copyObj.setRackLocation(rackLocation);
          copyObj.setActive(active);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setItemInventoryId((String)null);
                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemInventoryPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemInventory\n");
        str.append("-------------\n")
           .append("ItemInventoryId      : ")
           .append(getItemInventoryId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("MaximumQty           : ")
           .append(getMaximumQty())
           .append("\n")
           .append("MinimumQty           : ")
           .append(getMinimumQty())
           .append("\n")
           .append("ReorderPoint         : ")
           .append(getReorderPoint())
           .append("\n")
           .append("RackLocation         : ")
           .append(getRackLocation())
           .append("\n")
           .append("Active               : ")
           .append(getActive())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
