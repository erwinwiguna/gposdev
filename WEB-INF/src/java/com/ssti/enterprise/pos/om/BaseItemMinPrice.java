package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemMinPrice
 */
public abstract class BaseItemMinPrice extends BaseObject
{
    /** The Peer class */
    private static final ItemMinPricePeer peer =
        new ItemMinPricePeer();

        
    /** The value for the itemMinPriceId field */
    private String itemMinPriceId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the customerTypeId field */
    private String customerTypeId;
      
    /** The value for the minPrice field */
    private BigDecimal minPrice;
  
    
    /**
     * Get the ItemMinPriceId
     *
     * @return String
     */
    public String getItemMinPriceId()
    {
        return itemMinPriceId;
    }

                        
    /**
     * Set the value of ItemMinPriceId
     *
     * @param v new value
     */
    public void setItemMinPriceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemMinPriceId, v))
              {
            this.itemMinPriceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MinPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getMinPrice()
    {
        return minPrice;
    }

                        
    /**
     * Set the value of MinPrice
     *
     * @param v new value
     */
    public void setMinPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.minPrice, v))
              {
            this.minPrice = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemMinPriceId");
              fieldNames.add("ItemId");
              fieldNames.add("LocationId");
              fieldNames.add("CustomerTypeId");
              fieldNames.add("MinPrice");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemMinPriceId"))
        {
                return getItemMinPriceId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          if (name.equals("MinPrice"))
        {
                return getMinPrice();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemMinPricePeer.ITEM_MIN_PRICE_ID))
        {
                return getItemMinPriceId();
            }
          if (name.equals(ItemMinPricePeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemMinPricePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(ItemMinPricePeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          if (name.equals(ItemMinPricePeer.MIN_PRICE))
        {
                return getMinPrice();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemMinPriceId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getLocationId();
            }
              if (pos == 3)
        {
                return getCustomerTypeId();
            }
              if (pos == 4)
        {
                return getMinPrice();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemMinPricePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemMinPricePeer.doInsert((ItemMinPrice) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemMinPricePeer.doUpdate((ItemMinPrice) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemMinPriceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemMinPriceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemMinPriceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemMinPriceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemMinPrice copy() throws TorqueException
    {
        return copyInto(new ItemMinPrice());
    }
  
    protected ItemMinPrice copyInto(ItemMinPrice copyObj) throws TorqueException
    {
          copyObj.setItemMinPriceId(itemMinPriceId);
          copyObj.setItemId(itemId);
          copyObj.setLocationId(locationId);
          copyObj.setCustomerTypeId(customerTypeId);
          copyObj.setMinPrice(minPrice);
  
                    copyObj.setItemMinPriceId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemMinPricePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemMinPrice\n");
        str.append("------------\n")
           .append("ItemMinPriceId       : ")
           .append(getItemMinPriceId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
           .append("MinPrice             : ")
           .append(getMinPrice())
           .append("\n")
        ;
        return(str.toString());
    }
}
