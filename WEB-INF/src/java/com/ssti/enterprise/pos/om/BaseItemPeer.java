package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.ItemMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BaseItemPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "item";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(ItemMapBuilder.CLASS_NAME);
    }

      /** the column name for the ITEM_ID field */
    public static final String ITEM_ID;
      /** the column name for the ITEM_CODE field */
    public static final String ITEM_CODE;
      /** the column name for the BARCODE field */
    public static final String BARCODE;
      /** the column name for the ITEM_NAME field */
    public static final String ITEM_NAME;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the ITEM_SKU field */
    public static final String ITEM_SKU;
      /** the column name for the ITEM_SKU_NAME field */
    public static final String ITEM_SKU_NAME;
      /** the column name for the VENDOR_ITEM_CODE field */
    public static final String VENDOR_ITEM_CODE;
      /** the column name for the VENDOR_ITEM_NAME field */
    public static final String VENDOR_ITEM_NAME;
      /** the column name for the ITEM_TYPE field */
    public static final String ITEM_TYPE;
      /** the column name for the KATEGORI_ID field */
    public static final String KATEGORI_ID;
      /** the column name for the UNIT_ID field */
    public static final String UNIT_ID;
      /** the column name for the PURCHASE_UNIT_ID field */
    public static final String PURCHASE_UNIT_ID;
      /** the column name for the TAX_ID field */
    public static final String TAX_ID;
      /** the column name for the PURCHASE_TAX_ID field */
    public static final String PURCHASE_TAX_ID;
      /** the column name for the PREFERED_VENDOR_ID field */
    public static final String PREFERED_VENDOR_ID;
      /** the column name for the ITEM_PRICE field */
    public static final String ITEM_PRICE;
      /** the column name for the MIN_PRICE field */
    public static final String MIN_PRICE;
      /** the column name for the IS_FIXED_PRICE field */
    public static final String IS_FIXED_PRICE;
      /** the column name for the IS_DISCONTINUE field */
    public static final String IS_DISCONTINUE;
      /** the column name for the IS_CONSIGNMENT field */
    public static final String IS_CONSIGNMENT;
      /** the column name for the ITEM_PICTURE field */
    public static final String ITEM_PICTURE;
      /** the column name for the TAGS field */
    public static final String TAGS;
      /** the column name for the DISPLAY_STORE field */
    public static final String DISPLAY_STORE;
      /** the column name for the LAST_PURCHASE_PRICE field */
    public static final String LAST_PURCHASE_PRICE;
      /** the column name for the LAST_PURCHASE_CURR field */
    public static final String LAST_PURCHASE_CURR;
      /** the column name for the LAST_PURCHASE_RATE field */
    public static final String LAST_PURCHASE_RATE;
      /** the column name for the PROFIT_MARGIN field */
    public static final String PROFIT_MARGIN;
      /** the column name for the MINIMUM_STOCK field */
    public static final String MINIMUM_STOCK;
      /** the column name for the REORDER_POINT field */
    public static final String REORDER_POINT;
      /** the column name for the MAXIMUM_STOCK field */
    public static final String MAXIMUM_STOCK;
      /** the column name for the RACK_LOCATION field */
    public static final String RACK_LOCATION;
      /** the column name for the WAREHOUSE_ID field */
    public static final String WAREHOUSE_ID;
      /** the column name for the DELIVERY_TYPE field */
    public static final String DELIVERY_TYPE;
      /** the column name for the ITEM_STATUS_CODE_ID field */
    public static final String ITEM_STATUS_CODE_ID;
      /** the column name for the STATUS_COLOR field */
    public static final String STATUS_COLOR;
      /** the column name for the DISCOUNT_AMOUNT field */
    public static final String DISCOUNT_AMOUNT;
      /** the column name for the CREATE_BY field */
    public static final String CREATE_BY;
      /** the column name for the ADD_DATE field */
    public static final String ADD_DATE;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the LAST_ADJUSTMENT field */
    public static final String LAST_ADJUSTMENT;
      /** the column name for the SIZE field */
    public static final String SIZE;
      /** the column name for the COLOR field */
    public static final String COLOR;
      /** the column name for the SIZE_UNIT field */
    public static final String SIZE_UNIT;
      /** the column name for the WEIGHT field */
    public static final String WEIGHT;
      /** the column name for the VOLUME field */
    public static final String VOLUME;
      /** the column name for the UNIT_CONVERSION field */
    public static final String UNIT_CONVERSION;
      /** the column name for the BRAND field */
    public static final String BRAND;
      /** the column name for the MANUFACTURER field */
    public static final String MANUFACTURER;
      /** the column name for the TRACK_SERIAL_NO field */
    public static final String TRACK_SERIAL_NO;
      /** the column name for the TRACK_BATCH_NO field */
    public static final String TRACK_BATCH_NO;
      /** the column name for the ITEM_STATUS field */
    public static final String ITEM_STATUS;
      /** the column name for the INVENTORY_ACCOUNT field */
    public static final String INVENTORY_ACCOUNT;
      /** the column name for the SALES_ACCOUNT field */
    public static final String SALES_ACCOUNT;
      /** the column name for the SALES_RETURN_ACCOUNT field */
    public static final String SALES_RETURN_ACCOUNT;
      /** the column name for the ITEM_DISCOUNT_ACCOUNT field */
    public static final String ITEM_DISCOUNT_ACCOUNT;
      /** the column name for the COGS_ACCOUNT field */
    public static final String COGS_ACCOUNT;
      /** the column name for the PURCHASE_RETURN_ACCOUNT field */
    public static final String PURCHASE_RETURN_ACCOUNT;
      /** the column name for the EXPENSE_ACCOUNT field */
    public static final String EXPENSE_ACCOUNT;
      /** the column name for the UNBILLED_GOODS_ACCOUNT field */
    public static final String UNBILLED_GOODS_ACCOUNT;
  
    static
    {
          ITEM_ID = "item.ITEM_ID";
          ITEM_CODE = "item.ITEM_CODE";
          BARCODE = "item.BARCODE";
          ITEM_NAME = "item.ITEM_NAME";
          DESCRIPTION = "item.DESCRIPTION";
          ITEM_SKU = "item.ITEM_SKU";
          ITEM_SKU_NAME = "item.ITEM_SKU_NAME";
          VENDOR_ITEM_CODE = "item.VENDOR_ITEM_CODE";
          VENDOR_ITEM_NAME = "item.VENDOR_ITEM_NAME";
          ITEM_TYPE = "item.ITEM_TYPE";
          KATEGORI_ID = "item.KATEGORI_ID";
          UNIT_ID = "item.UNIT_ID";
          PURCHASE_UNIT_ID = "item.PURCHASE_UNIT_ID";
          TAX_ID = "item.TAX_ID";
          PURCHASE_TAX_ID = "item.PURCHASE_TAX_ID";
          PREFERED_VENDOR_ID = "item.PREFERED_VENDOR_ID";
          ITEM_PRICE = "item.ITEM_PRICE";
          MIN_PRICE = "item.MIN_PRICE";
          IS_FIXED_PRICE = "item.IS_FIXED_PRICE";
          IS_DISCONTINUE = "item.IS_DISCONTINUE";
          IS_CONSIGNMENT = "item.IS_CONSIGNMENT";
          ITEM_PICTURE = "item.ITEM_PICTURE";
          TAGS = "item.TAGS";
          DISPLAY_STORE = "item.DISPLAY_STORE";
          LAST_PURCHASE_PRICE = "item.LAST_PURCHASE_PRICE";
          LAST_PURCHASE_CURR = "item.LAST_PURCHASE_CURR";
          LAST_PURCHASE_RATE = "item.LAST_PURCHASE_RATE";
          PROFIT_MARGIN = "item.PROFIT_MARGIN";
          MINIMUM_STOCK = "item.MINIMUM_STOCK";
          REORDER_POINT = "item.REORDER_POINT";
          MAXIMUM_STOCK = "item.MAXIMUM_STOCK";
          RACK_LOCATION = "item.RACK_LOCATION";
          WAREHOUSE_ID = "item.WAREHOUSE_ID";
          DELIVERY_TYPE = "item.DELIVERY_TYPE";
          ITEM_STATUS_CODE_ID = "item.ITEM_STATUS_CODE_ID";
          STATUS_COLOR = "item.STATUS_COLOR";
          DISCOUNT_AMOUNT = "item.DISCOUNT_AMOUNT";
          CREATE_BY = "item.CREATE_BY";
          ADD_DATE = "item.ADD_DATE";
          UPDATE_DATE = "item.UPDATE_DATE";
          LAST_ADJUSTMENT = "item.LAST_ADJUSTMENT";
          SIZE = "item.SIZE";
          COLOR = "item.COLOR";
          SIZE_UNIT = "item.SIZE_UNIT";
          WEIGHT = "item.WEIGHT";
          VOLUME = "item.VOLUME";
          UNIT_CONVERSION = "item.UNIT_CONVERSION";
          BRAND = "item.BRAND";
          MANUFACTURER = "item.MANUFACTURER";
          TRACK_SERIAL_NO = "item.TRACK_SERIAL_NO";
          TRACK_BATCH_NO = "item.TRACK_BATCH_NO";
          ITEM_STATUS = "item.ITEM_STATUS";
          INVENTORY_ACCOUNT = "item.INVENTORY_ACCOUNT";
          SALES_ACCOUNT = "item.SALES_ACCOUNT";
          SALES_RETURN_ACCOUNT = "item.SALES_RETURN_ACCOUNT";
          ITEM_DISCOUNT_ACCOUNT = "item.ITEM_DISCOUNT_ACCOUNT";
          COGS_ACCOUNT = "item.COGS_ACCOUNT";
          PURCHASE_RETURN_ACCOUNT = "item.PURCHASE_RETURN_ACCOUNT";
          EXPENSE_ACCOUNT = "item.EXPENSE_ACCOUNT";
          UNBILLED_GOODS_ACCOUNT = "item.UNBILLED_GOODS_ACCOUNT";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(ItemMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(ItemMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  60;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Item";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseItemPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FIXED_PRICE))
        {
            Object possibleBoolean = criteria.get(IS_FIXED_PRICE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FIXED_PRICE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DISCONTINUE))
        {
            Object possibleBoolean = criteria.get(IS_DISCONTINUE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DISCONTINUE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CONSIGNMENT))
        {
            Object possibleBoolean = criteria.get(IS_CONSIGNMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CONSIGNMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DISPLAY_STORE))
        {
            Object possibleBoolean = criteria.get(DISPLAY_STORE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DISPLAY_STORE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_SERIAL_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_SERIAL_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_SERIAL_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_BATCH_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_BATCH_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_BATCH_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ITEM_STATUS))
        {
            Object possibleBoolean = criteria.get(ITEM_STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ITEM_STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(ITEM_ID);
          criteria.addSelectColumn(ITEM_CODE);
          criteria.addSelectColumn(BARCODE);
          criteria.addSelectColumn(ITEM_NAME);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(ITEM_SKU);
          criteria.addSelectColumn(ITEM_SKU_NAME);
          criteria.addSelectColumn(VENDOR_ITEM_CODE);
          criteria.addSelectColumn(VENDOR_ITEM_NAME);
          criteria.addSelectColumn(ITEM_TYPE);
          criteria.addSelectColumn(KATEGORI_ID);
          criteria.addSelectColumn(UNIT_ID);
          criteria.addSelectColumn(PURCHASE_UNIT_ID);
          criteria.addSelectColumn(TAX_ID);
          criteria.addSelectColumn(PURCHASE_TAX_ID);
          criteria.addSelectColumn(PREFERED_VENDOR_ID);
          criteria.addSelectColumn(ITEM_PRICE);
          criteria.addSelectColumn(MIN_PRICE);
          criteria.addSelectColumn(IS_FIXED_PRICE);
          criteria.addSelectColumn(IS_DISCONTINUE);
          criteria.addSelectColumn(IS_CONSIGNMENT);
          criteria.addSelectColumn(ITEM_PICTURE);
          criteria.addSelectColumn(TAGS);
          criteria.addSelectColumn(DISPLAY_STORE);
          criteria.addSelectColumn(LAST_PURCHASE_PRICE);
          criteria.addSelectColumn(LAST_PURCHASE_CURR);
          criteria.addSelectColumn(LAST_PURCHASE_RATE);
          criteria.addSelectColumn(PROFIT_MARGIN);
          criteria.addSelectColumn(MINIMUM_STOCK);
          criteria.addSelectColumn(REORDER_POINT);
          criteria.addSelectColumn(MAXIMUM_STOCK);
          criteria.addSelectColumn(RACK_LOCATION);
          criteria.addSelectColumn(WAREHOUSE_ID);
          criteria.addSelectColumn(DELIVERY_TYPE);
          criteria.addSelectColumn(ITEM_STATUS_CODE_ID);
          criteria.addSelectColumn(STATUS_COLOR);
          criteria.addSelectColumn(DISCOUNT_AMOUNT);
          criteria.addSelectColumn(CREATE_BY);
          criteria.addSelectColumn(ADD_DATE);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(LAST_ADJUSTMENT);
          criteria.addSelectColumn(SIZE);
          criteria.addSelectColumn(COLOR);
          criteria.addSelectColumn(SIZE_UNIT);
          criteria.addSelectColumn(WEIGHT);
          criteria.addSelectColumn(VOLUME);
          criteria.addSelectColumn(UNIT_CONVERSION);
          criteria.addSelectColumn(BRAND);
          criteria.addSelectColumn(MANUFACTURER);
          criteria.addSelectColumn(TRACK_SERIAL_NO);
          criteria.addSelectColumn(TRACK_BATCH_NO);
          criteria.addSelectColumn(ITEM_STATUS);
          criteria.addSelectColumn(INVENTORY_ACCOUNT);
          criteria.addSelectColumn(SALES_ACCOUNT);
          criteria.addSelectColumn(SALES_RETURN_ACCOUNT);
          criteria.addSelectColumn(ITEM_DISCOUNT_ACCOUNT);
          criteria.addSelectColumn(COGS_ACCOUNT);
          criteria.addSelectColumn(PURCHASE_RETURN_ACCOUNT);
          criteria.addSelectColumn(EXPENSE_ACCOUNT);
          criteria.addSelectColumn(UNBILLED_GOODS_ACCOUNT);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Item row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Item obj = (Item) cls.newInstance();
            ItemPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Item obj)
        throws TorqueException
    {
        try
        {
                obj.setItemId(row.getValue(offset + 0).asString());
                  obj.setItemCode(row.getValue(offset + 1).asString());
                  obj.setBarcode(row.getValue(offset + 2).asString());
                  obj.setItemName(row.getValue(offset + 3).asString());
                  obj.setDescription(row.getValue(offset + 4).asString());
                  obj.setItemSku(row.getValue(offset + 5).asString());
                  obj.setItemSkuName(row.getValue(offset + 6).asString());
                  obj.setVendorItemCode(row.getValue(offset + 7).asString());
                  obj.setVendorItemName(row.getValue(offset + 8).asString());
                  obj.setItemType(row.getValue(offset + 9).asInt());
                  obj.setKategoriId(row.getValue(offset + 10).asString());
                  obj.setUnitId(row.getValue(offset + 11).asString());
                  obj.setPurchaseUnitId(row.getValue(offset + 12).asString());
                  obj.setTaxId(row.getValue(offset + 13).asString());
                  obj.setPurchaseTaxId(row.getValue(offset + 14).asString());
                  obj.setPreferedVendorId(row.getValue(offset + 15).asString());
                  obj.setItemPrice(row.getValue(offset + 16).asBigDecimal());
                  obj.setMinPrice(row.getValue(offset + 17).asBigDecimal());
                  obj.setIsFixedPrice(row.getValue(offset + 18).asBoolean());
                  obj.setIsDiscontinue(row.getValue(offset + 19).asBoolean());
                  obj.setIsConsignment(row.getValue(offset + 20).asBoolean());
                  obj.setItemPicture(row.getValue(offset + 21).asString());
                  obj.setTags(row.getValue(offset + 22).asString());
                  obj.setDisplayStore(row.getValue(offset + 23).asBoolean());
                  obj.setLastPurchasePrice(row.getValue(offset + 24).asBigDecimal());
                  obj.setLastPurchaseCurr(row.getValue(offset + 25).asString());
                  obj.setLastPurchaseRate(row.getValue(offset + 26).asBigDecimal());
                  obj.setProfitMargin(row.getValue(offset + 27).asString());
                  obj.setMinimumStock(row.getValue(offset + 28).asBigDecimal());
                  obj.setReorderPoint(row.getValue(offset + 29).asBigDecimal());
                  obj.setMaximumStock(row.getValue(offset + 30).asBigDecimal());
                  obj.setRackLocation(row.getValue(offset + 31).asString());
                  obj.setWarehouseId(row.getValue(offset + 32).asString());
                  obj.setDeliveryType(row.getValue(offset + 33).asInt());
                  obj.setItemStatusCodeId(row.getValue(offset + 34).asString());
                  obj.setStatusColor(row.getValue(offset + 35).asString());
                  obj.setDiscountAmount(row.getValue(offset + 36).asString());
                  obj.setCreateBy(row.getValue(offset + 37).asString());
                  obj.setAddDate(row.getValue(offset + 38).asUtilDate());
                  obj.setUpdateDate(row.getValue(offset + 39).asUtilDate());
                  obj.setLastAdjustment(row.getValue(offset + 40).asUtilDate());
                  obj.setSize(row.getValue(offset + 41).asString());
                  obj.setColor(row.getValue(offset + 42).asString());
                  obj.setSizeUnit(row.getValue(offset + 43).asString());
                  obj.setWeight(row.getValue(offset + 44).asBigDecimal());
                  obj.setVolume(row.getValue(offset + 45).asBigDecimal());
                  obj.setUnitConversion(row.getValue(offset + 46).asBigDecimal());
                  obj.setBrand(row.getValue(offset + 47).asString());
                  obj.setManufacturer(row.getValue(offset + 48).asString());
                  obj.setTrackSerialNo(row.getValue(offset + 49).asBoolean());
                  obj.setTrackBatchNo(row.getValue(offset + 50).asBoolean());
                  obj.setItemStatus(row.getValue(offset + 51).asBoolean());
                  obj.setInventoryAccount(row.getValue(offset + 52).asString());
                  obj.setSalesAccount(row.getValue(offset + 53).asString());
                  obj.setSalesReturnAccount(row.getValue(offset + 54).asString());
                  obj.setItemDiscountAccount(row.getValue(offset + 55).asString());
                  obj.setCogsAccount(row.getValue(offset + 56).asString());
                  obj.setPurchaseReturnAccount(row.getValue(offset + 57).asString());
                  obj.setExpenseAccount(row.getValue(offset + 58).asString());
                  obj.setUnbilledGoodsAccount(row.getValue(offset + 59).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseItemPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FIXED_PRICE))
        {
            Object possibleBoolean = criteria.get(IS_FIXED_PRICE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FIXED_PRICE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DISCONTINUE))
        {
            Object possibleBoolean = criteria.get(IS_DISCONTINUE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DISCONTINUE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CONSIGNMENT))
        {
            Object possibleBoolean = criteria.get(IS_CONSIGNMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CONSIGNMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DISPLAY_STORE))
        {
            Object possibleBoolean = criteria.get(DISPLAY_STORE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DISPLAY_STORE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_SERIAL_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_SERIAL_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_SERIAL_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_BATCH_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_BATCH_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_BATCH_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ITEM_STATUS))
        {
            Object possibleBoolean = criteria.get(ITEM_STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ITEM_STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(ItemPeer.row2Object(row, 1,
                ItemPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseItemPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(ITEM_ID, criteria.remove(ITEM_ID));
                                                                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FIXED_PRICE))
        {
            Object possibleBoolean = criteria.get(IS_FIXED_PRICE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FIXED_PRICE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DISCONTINUE))
        {
            Object possibleBoolean = criteria.get(IS_DISCONTINUE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DISCONTINUE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CONSIGNMENT))
        {
            Object possibleBoolean = criteria.get(IS_CONSIGNMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CONSIGNMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DISPLAY_STORE))
        {
            Object possibleBoolean = criteria.get(DISPLAY_STORE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DISPLAY_STORE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                                                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_SERIAL_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_SERIAL_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_SERIAL_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_BATCH_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_BATCH_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_BATCH_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ITEM_STATUS))
        {
            Object possibleBoolean = criteria.get(ITEM_STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ITEM_STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         ItemPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FIXED_PRICE))
        {
            Object possibleBoolean = criteria.get(IS_FIXED_PRICE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FIXED_PRICE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DISCONTINUE))
        {
            Object possibleBoolean = criteria.get(IS_DISCONTINUE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DISCONTINUE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CONSIGNMENT))
        {
            Object possibleBoolean = criteria.get(IS_CONSIGNMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CONSIGNMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DISPLAY_STORE))
        {
            Object possibleBoolean = criteria.get(DISPLAY_STORE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DISPLAY_STORE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_SERIAL_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_SERIAL_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_SERIAL_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_BATCH_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_BATCH_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_BATCH_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ITEM_STATUS))
        {
            Object possibleBoolean = criteria.get(ITEM_STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ITEM_STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Item obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Item obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Item obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Item obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Item) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Item obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Item) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Item obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Item) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Item obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseItemPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(ITEM_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Item obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(ITEM_ID, obj.getItemId());
              criteria.add(ITEM_CODE, obj.getItemCode());
              criteria.add(BARCODE, obj.getBarcode());
              criteria.add(ITEM_NAME, obj.getItemName());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(ITEM_SKU, obj.getItemSku());
              criteria.add(ITEM_SKU_NAME, obj.getItemSkuName());
              criteria.add(VENDOR_ITEM_CODE, obj.getVendorItemCode());
              criteria.add(VENDOR_ITEM_NAME, obj.getVendorItemName());
              criteria.add(ITEM_TYPE, obj.getItemType());
              criteria.add(KATEGORI_ID, obj.getKategoriId());
              criteria.add(UNIT_ID, obj.getUnitId());
              criteria.add(PURCHASE_UNIT_ID, obj.getPurchaseUnitId());
              criteria.add(TAX_ID, obj.getTaxId());
              criteria.add(PURCHASE_TAX_ID, obj.getPurchaseTaxId());
              criteria.add(PREFERED_VENDOR_ID, obj.getPreferedVendorId());
              criteria.add(ITEM_PRICE, obj.getItemPrice());
              criteria.add(MIN_PRICE, obj.getMinPrice());
              criteria.add(IS_FIXED_PRICE, obj.getIsFixedPrice());
              criteria.add(IS_DISCONTINUE, obj.getIsDiscontinue());
              criteria.add(IS_CONSIGNMENT, obj.getIsConsignment());
              criteria.add(ITEM_PICTURE, obj.getItemPicture());
              criteria.add(TAGS, obj.getTags());
              criteria.add(DISPLAY_STORE, obj.getDisplayStore());
              criteria.add(LAST_PURCHASE_PRICE, obj.getLastPurchasePrice());
              criteria.add(LAST_PURCHASE_CURR, obj.getLastPurchaseCurr());
              criteria.add(LAST_PURCHASE_RATE, obj.getLastPurchaseRate());
              criteria.add(PROFIT_MARGIN, obj.getProfitMargin());
              criteria.add(MINIMUM_STOCK, obj.getMinimumStock());
              criteria.add(REORDER_POINT, obj.getReorderPoint());
              criteria.add(MAXIMUM_STOCK, obj.getMaximumStock());
              criteria.add(RACK_LOCATION, obj.getRackLocation());
              criteria.add(WAREHOUSE_ID, obj.getWarehouseId());
              criteria.add(DELIVERY_TYPE, obj.getDeliveryType());
              criteria.add(ITEM_STATUS_CODE_ID, obj.getItemStatusCodeId());
              criteria.add(STATUS_COLOR, obj.getStatusColor());
              criteria.add(DISCOUNT_AMOUNT, obj.getDiscountAmount());
              criteria.add(CREATE_BY, obj.getCreateBy());
              criteria.add(ADD_DATE, obj.getAddDate());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(LAST_ADJUSTMENT, obj.getLastAdjustment());
              criteria.add(SIZE, obj.getSize());
              criteria.add(COLOR, obj.getColor());
              criteria.add(SIZE_UNIT, obj.getSizeUnit());
              criteria.add(WEIGHT, obj.getWeight());
              criteria.add(VOLUME, obj.getVolume());
              criteria.add(UNIT_CONVERSION, obj.getUnitConversion());
              criteria.add(BRAND, obj.getBrand());
              criteria.add(MANUFACTURER, obj.getManufacturer());
              criteria.add(TRACK_SERIAL_NO, obj.getTrackSerialNo());
              criteria.add(TRACK_BATCH_NO, obj.getTrackBatchNo());
              criteria.add(ITEM_STATUS, obj.getItemStatus());
              criteria.add(INVENTORY_ACCOUNT, obj.getInventoryAccount());
              criteria.add(SALES_ACCOUNT, obj.getSalesAccount());
              criteria.add(SALES_RETURN_ACCOUNT, obj.getSalesReturnAccount());
              criteria.add(ITEM_DISCOUNT_ACCOUNT, obj.getItemDiscountAccount());
              criteria.add(COGS_ACCOUNT, obj.getCogsAccount());
              criteria.add(PURCHASE_RETURN_ACCOUNT, obj.getPurchaseReturnAccount());
              criteria.add(EXPENSE_ACCOUNT, obj.getExpenseAccount());
              criteria.add(UNBILLED_GOODS_ACCOUNT, obj.getUnbilledGoodsAccount());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Item obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(ITEM_ID, obj.getItemId());
                          criteria.add(ITEM_CODE, obj.getItemCode());
                          criteria.add(BARCODE, obj.getBarcode());
                          criteria.add(ITEM_NAME, obj.getItemName());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(ITEM_SKU, obj.getItemSku());
                          criteria.add(ITEM_SKU_NAME, obj.getItemSkuName());
                          criteria.add(VENDOR_ITEM_CODE, obj.getVendorItemCode());
                          criteria.add(VENDOR_ITEM_NAME, obj.getVendorItemName());
                          criteria.add(ITEM_TYPE, obj.getItemType());
                          criteria.add(KATEGORI_ID, obj.getKategoriId());
                          criteria.add(UNIT_ID, obj.getUnitId());
                          criteria.add(PURCHASE_UNIT_ID, obj.getPurchaseUnitId());
                          criteria.add(TAX_ID, obj.getTaxId());
                          criteria.add(PURCHASE_TAX_ID, obj.getPurchaseTaxId());
                          criteria.add(PREFERED_VENDOR_ID, obj.getPreferedVendorId());
                          criteria.add(ITEM_PRICE, obj.getItemPrice());
                          criteria.add(MIN_PRICE, obj.getMinPrice());
                          criteria.add(IS_FIXED_PRICE, obj.getIsFixedPrice());
                          criteria.add(IS_DISCONTINUE, obj.getIsDiscontinue());
                          criteria.add(IS_CONSIGNMENT, obj.getIsConsignment());
                          criteria.add(ITEM_PICTURE, obj.getItemPicture());
                          criteria.add(TAGS, obj.getTags());
                          criteria.add(DISPLAY_STORE, obj.getDisplayStore());
                          criteria.add(LAST_PURCHASE_PRICE, obj.getLastPurchasePrice());
                          criteria.add(LAST_PURCHASE_CURR, obj.getLastPurchaseCurr());
                          criteria.add(LAST_PURCHASE_RATE, obj.getLastPurchaseRate());
                          criteria.add(PROFIT_MARGIN, obj.getProfitMargin());
                          criteria.add(MINIMUM_STOCK, obj.getMinimumStock());
                          criteria.add(REORDER_POINT, obj.getReorderPoint());
                          criteria.add(MAXIMUM_STOCK, obj.getMaximumStock());
                          criteria.add(RACK_LOCATION, obj.getRackLocation());
                          criteria.add(WAREHOUSE_ID, obj.getWarehouseId());
                          criteria.add(DELIVERY_TYPE, obj.getDeliveryType());
                          criteria.add(ITEM_STATUS_CODE_ID, obj.getItemStatusCodeId());
                          criteria.add(STATUS_COLOR, obj.getStatusColor());
                          criteria.add(DISCOUNT_AMOUNT, obj.getDiscountAmount());
                          criteria.add(CREATE_BY, obj.getCreateBy());
                          criteria.add(ADD_DATE, obj.getAddDate());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(LAST_ADJUSTMENT, obj.getLastAdjustment());
                          criteria.add(SIZE, obj.getSize());
                          criteria.add(COLOR, obj.getColor());
                          criteria.add(SIZE_UNIT, obj.getSizeUnit());
                          criteria.add(WEIGHT, obj.getWeight());
                          criteria.add(VOLUME, obj.getVolume());
                          criteria.add(UNIT_CONVERSION, obj.getUnitConversion());
                          criteria.add(BRAND, obj.getBrand());
                          criteria.add(MANUFACTURER, obj.getManufacturer());
                          criteria.add(TRACK_SERIAL_NO, obj.getTrackSerialNo());
                          criteria.add(TRACK_BATCH_NO, obj.getTrackBatchNo());
                          criteria.add(ITEM_STATUS, obj.getItemStatus());
                          criteria.add(INVENTORY_ACCOUNT, obj.getInventoryAccount());
                          criteria.add(SALES_ACCOUNT, obj.getSalesAccount());
                          criteria.add(SALES_RETURN_ACCOUNT, obj.getSalesReturnAccount());
                          criteria.add(ITEM_DISCOUNT_ACCOUNT, obj.getItemDiscountAccount());
                          criteria.add(COGS_ACCOUNT, obj.getCogsAccount());
                          criteria.add(PURCHASE_RETURN_ACCOUNT, obj.getPurchaseReturnAccount());
                          criteria.add(EXPENSE_ACCOUNT, obj.getExpenseAccount());
                          criteria.add(UNBILLED_GOODS_ACCOUNT, obj.getUnbilledGoodsAccount());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Item retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Item retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Item retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Item retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Item retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Item)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( ITEM_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of Item objects pre-filled with their
     * Kategori objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ItemPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinKategori(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        ItemPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        KategoriPeer.addSelectColumns(criteria);


                        criteria.addJoin(ItemPeer.KATEGORI_ID,
            KategoriPeer.KATEGORI_ID);
        

                                                                                                                                                                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FIXED_PRICE))
        {
            Object possibleBoolean = criteria.get(IS_FIXED_PRICE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FIXED_PRICE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_DISCONTINUE))
        {
            Object possibleBoolean = criteria.get(IS_DISCONTINUE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_DISCONTINUE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CONSIGNMENT))
        {
            Object possibleBoolean = criteria.get(IS_CONSIGNMENT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CONSIGNMENT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(DISPLAY_STORE))
        {
            Object possibleBoolean = criteria.get(DISPLAY_STORE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(DISPLAY_STORE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_SERIAL_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_SERIAL_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_SERIAL_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(TRACK_BATCH_NO))
        {
            Object possibleBoolean = criteria.get(TRACK_BATCH_NO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(TRACK_BATCH_NO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ITEM_STATUS))
        {
            Object possibleBoolean = criteria.get(ITEM_STATUS);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ITEM_STATUS, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                  
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = ItemPeer.getOMClass();
                    Item obj1 = ItemPeer
                .row2Object(row, 1, omClass);
                     omClass = KategoriPeer.getOMClass();
                    Kategori obj2 = KategoriPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                Item temp_obj1 = (Item)results.get(j);
                Kategori temp_obj2 = temp_obj1.getKategori();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addItem(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initItems();
                obj2.addItem(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
