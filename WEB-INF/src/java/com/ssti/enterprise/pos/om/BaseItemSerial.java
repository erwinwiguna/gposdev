package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemSerial
 */
public abstract class BaseItemSerial extends BaseObject
{
    /** The Peer class */
    private static final ItemSerialPeer peer =
        new ItemSerialPeer();

        
    /** The value for the itemSerialId field */
    private String itemSerialId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the serialNo field */
    private String serialNo;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the status field */
    private boolean status;
  
    
    /**
     * Get the ItemSerialId
     *
     * @return String
     */
    public String getItemSerialId()
    {
        return itemSerialId;
    }

                        
    /**
     * Set the value of ItemSerialId
     *
     * @param v new value
     */
    public void setItemSerialId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemSerialId, v))
              {
            this.itemSerialId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SerialNo
     *
     * @return String
     */
    public String getSerialNo()
    {
        return serialNo;
    }

                        
    /**
     * Set the value of SerialNo
     *
     * @param v new value
     */
    public void setSerialNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.serialNo, v))
              {
            this.serialNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return boolean
     */
    public boolean getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(boolean v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemSerialId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("SerialNo");
              fieldNames.add("LocationId");
              fieldNames.add("Status");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemSerialId"))
        {
                return getItemSerialId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("SerialNo"))
        {
                return getSerialNo();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("Status"))
        {
                return Boolean.valueOf(getStatus());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemSerialPeer.ITEM_SERIAL_ID))
        {
                return getItemSerialId();
            }
          if (name.equals(ItemSerialPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemSerialPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(ItemSerialPeer.SERIAL_NO))
        {
                return getSerialNo();
            }
          if (name.equals(ItemSerialPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(ItemSerialPeer.STATUS))
        {
                return Boolean.valueOf(getStatus());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemSerialId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getItemCode();
            }
              if (pos == 3)
        {
                return getSerialNo();
            }
              if (pos == 4)
        {
                return getLocationId();
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getStatus());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemSerialPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemSerialPeer.doInsert((ItemSerial) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemSerialPeer.doUpdate((ItemSerial) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemSerialId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemSerialId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemSerialId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemSerialId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemSerial copy() throws TorqueException
    {
        return copyInto(new ItemSerial());
    }
  
    protected ItemSerial copyInto(ItemSerial copyObj) throws TorqueException
    {
          copyObj.setItemSerialId(itemSerialId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setSerialNo(serialNo);
          copyObj.setLocationId(locationId);
          copyObj.setStatus(status);
  
                    copyObj.setItemSerialId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemSerialPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemSerial\n");
        str.append("----------\n")
           .append("ItemSerialId         : ")
           .append(getItemSerialId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("SerialNo             : ")
           .append(getSerialNo())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
        ;
        return(str.toString());
    }
}
