package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to JournalVoucher
 */
public abstract class BaseJournalVoucher extends BaseObject
{
    /** The Peer class */
    private static final JournalVoucherPeer peer =
        new JournalVoucherPeer();

        
    /** The value for the journalVoucherId field */
    private String journalVoucherId;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the totalDebit field */
    private BigDecimal totalDebit;
      
    /** The value for the totalCredit field */
    private BigDecimal totalCredit;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the recurring field */
    private boolean recurring;
      
    /** The value for the nextRecurringDate field */
    private Date nextRecurringDate;
                                          
    /** The value for the transType field */
    private int transType = 1;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the JournalVoucherId
     *
     * @return String
     */
    public String getJournalVoucherId()
    {
        return journalVoucherId;
    }

                                              
    /**
     * Set the value of JournalVoucherId
     *
     * @param v new value
     */
    public void setJournalVoucherId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.journalVoucherId, v))
              {
            this.journalVoucherId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated JournalVoucherDetail
        if (collJournalVoucherDetails != null)
        {
            for (int i = 0; i < collJournalVoucherDetails.size(); i++)
            {
                ((JournalVoucherDetail) collJournalVoucherDetails.get(i))
                    .setJournalVoucherId(v);
            }
        }
                                }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDebit
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDebit()
    {
        return totalDebit;
    }

                        
    /**
     * Set the value of TotalDebit
     *
     * @param v new value
     */
    public void setTotalDebit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDebit, v))
              {
            this.totalDebit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalCredit
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalCredit()
    {
        return totalCredit;
    }

                        
    /**
     * Set the value of TotalCredit
     *
     * @param v new value
     */
    public void setTotalCredit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalCredit, v))
              {
            this.totalCredit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Recurring
     *
     * @return boolean
     */
    public boolean getRecurring()
    {
        return recurring;
    }

                        
    /**
     * Set the value of Recurring
     *
     * @param v new value
     */
    public void setRecurring(boolean v) 
    {
    
                  if (this.recurring != v)
              {
            this.recurring = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NextRecurringDate
     *
     * @return Date
     */
    public Date getNextRecurringDate()
    {
        return nextRecurringDate;
    }

                        
    /**
     * Set the value of NextRecurringDate
     *
     * @param v new value
     */
    public void setNextRecurringDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.nextRecurringDate, v))
              {
            this.nextRecurringDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransType
     *
     * @return int
     */
    public int getTransType()
    {
        return transType;
    }

                        
    /**
     * Set the value of TransType
     *
     * @param v new value
     */
    public void setTransType(int v) 
    {
    
                  if (this.transType != v)
              {
            this.transType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collJournalVoucherDetails
     */
    protected List collJournalVoucherDetails;

    /**
     * Temporary storage of collJournalVoucherDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initJournalVoucherDetails()
    {
        if (collJournalVoucherDetails == null)
        {
            collJournalVoucherDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a JournalVoucherDetail object to this object
     * through the JournalVoucherDetail foreign key attribute
     *
     * @param l JournalVoucherDetail
     * @throws TorqueException
     */
    public void addJournalVoucherDetail(JournalVoucherDetail l) throws TorqueException
    {
        getJournalVoucherDetails().add(l);
        l.setJournalVoucher((JournalVoucher) this);
    }

    /**
     * The criteria used to select the current contents of collJournalVoucherDetails
     */
    private Criteria lastJournalVoucherDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getJournalVoucherDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getJournalVoucherDetails() throws TorqueException
    {
              if (collJournalVoucherDetails == null)
        {
            collJournalVoucherDetails = getJournalVoucherDetails(new Criteria(10));
        }
        return collJournalVoucherDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JournalVoucher has previously
     * been saved, it will retrieve related JournalVoucherDetails from storage.
     * If this JournalVoucher is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getJournalVoucherDetails(Criteria criteria) throws TorqueException
    {
              if (collJournalVoucherDetails == null)
        {
            if (isNew())
            {
               collJournalVoucherDetails = new ArrayList();
            }
            else
            {
                        criteria.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, getJournalVoucherId() );
                        collJournalVoucherDetails = JournalVoucherDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, getJournalVoucherId());
                            if (!lastJournalVoucherDetailsCriteria.equals(criteria))
                {
                    collJournalVoucherDetails = JournalVoucherDetailPeer.doSelect(criteria);
                }
            }
        }
        lastJournalVoucherDetailsCriteria = criteria;

        return collJournalVoucherDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getJournalVoucherDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getJournalVoucherDetails(Connection con) throws TorqueException
    {
              if (collJournalVoucherDetails == null)
        {
            collJournalVoucherDetails = getJournalVoucherDetails(new Criteria(10), con);
        }
        return collJournalVoucherDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JournalVoucher has previously
     * been saved, it will retrieve related JournalVoucherDetails from storage.
     * If this JournalVoucher is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getJournalVoucherDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collJournalVoucherDetails == null)
        {
            if (isNew())
            {
               collJournalVoucherDetails = new ArrayList();
            }
            else
            {
                         criteria.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, getJournalVoucherId());
                         collJournalVoucherDetails = JournalVoucherDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, getJournalVoucherId());
                             if (!lastJournalVoucherDetailsCriteria.equals(criteria))
                 {
                     collJournalVoucherDetails = JournalVoucherDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastJournalVoucherDetailsCriteria = criteria;

         return collJournalVoucherDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JournalVoucher is new, it will return
     * an empty collection; or if this JournalVoucher has previously
     * been saved, it will retrieve related JournalVoucherDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JournalVoucher.
     */
    protected List getJournalVoucherDetailsJoinJournalVoucher(Criteria criteria)
        throws TorqueException
    {
                    if (collJournalVoucherDetails == null)
        {
            if (isNew())
            {
               collJournalVoucherDetails = new ArrayList();
            }
            else
            {
                              criteria.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, getJournalVoucherId());
                              collJournalVoucherDetails = JournalVoucherDetailPeer.doSelectJoinJournalVoucher(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, getJournalVoucherId());
                                    if (!lastJournalVoucherDetailsCriteria.equals(criteria))
            {
                collJournalVoucherDetails = JournalVoucherDetailPeer.doSelectJoinJournalVoucher(criteria);
            }
        }
        lastJournalVoucherDetailsCriteria = criteria;

        return collJournalVoucherDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("JournalVoucherId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("Status");
              fieldNames.add("TotalDebit");
              fieldNames.add("TotalCredit");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames.add("Recurring");
              fieldNames.add("NextRecurringDate");
              fieldNames.add("TransType");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("JournalVoucherId"))
        {
                return getJournalVoucherId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TotalDebit"))
        {
                return getTotalDebit();
            }
          if (name.equals("TotalCredit"))
        {
                return getTotalCredit();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Recurring"))
        {
                return Boolean.valueOf(getRecurring());
            }
          if (name.equals("NextRecurringDate"))
        {
                return getNextRecurringDate();
            }
          if (name.equals("TransType"))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(JournalVoucherPeer.JOURNAL_VOUCHER_ID))
        {
                return getJournalVoucherId();
            }
          if (name.equals(JournalVoucherPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(JournalVoucherPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(JournalVoucherPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(JournalVoucherPeer.TOTAL_DEBIT))
        {
                return getTotalDebit();
            }
          if (name.equals(JournalVoucherPeer.TOTAL_CREDIT))
        {
                return getTotalCredit();
            }
          if (name.equals(JournalVoucherPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(JournalVoucherPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(JournalVoucherPeer.RECURRING))
        {
                return Boolean.valueOf(getRecurring());
            }
          if (name.equals(JournalVoucherPeer.NEXT_RECURRING_DATE))
        {
                return getNextRecurringDate();
            }
          if (name.equals(JournalVoucherPeer.TRANS_TYPE))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals(JournalVoucherPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(JournalVoucherPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getJournalVoucherId();
            }
              if (pos == 1)
        {
                return getTransactionNo();
            }
              if (pos == 2)
        {
                return getTransactionDate();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 4)
        {
                return getTotalDebit();
            }
              if (pos == 5)
        {
                return getTotalCredit();
            }
              if (pos == 6)
        {
                return getUserName();
            }
              if (pos == 7)
        {
                return getDescription();
            }
              if (pos == 8)
        {
                return Boolean.valueOf(getRecurring());
            }
              if (pos == 9)
        {
                return getNextRecurringDate();
            }
              if (pos == 10)
        {
                return Integer.valueOf(getTransType());
            }
              if (pos == 11)
        {
                return getCancelBy();
            }
              if (pos == 12)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(JournalVoucherPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        JournalVoucherPeer.doInsert((JournalVoucher) this, con);
                        setNew(false);
                    }
                    else
                    {
                        JournalVoucherPeer.doUpdate((JournalVoucher) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collJournalVoucherDetails != null)
            {
                for (int i = 0; i < collJournalVoucherDetails.size(); i++)
                {
                    ((JournalVoucherDetail) collJournalVoucherDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key journalVoucherId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setJournalVoucherId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setJournalVoucherId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getJournalVoucherId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public JournalVoucher copy() throws TorqueException
    {
        return copyInto(new JournalVoucher());
    }
  
    protected JournalVoucher copyInto(JournalVoucher copyObj) throws TorqueException
    {
          copyObj.setJournalVoucherId(journalVoucherId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setStatus(status);
          copyObj.setTotalDebit(totalDebit);
          copyObj.setTotalCredit(totalCredit);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
          copyObj.setRecurring(recurring);
          copyObj.setNextRecurringDate(nextRecurringDate);
          copyObj.setTransType(transType);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setJournalVoucherId((String)null);
                                                                                    
                                      
                            
        List v = getJournalVoucherDetails();
        for (int i = 0; i < v.size(); i++)
        {
            JournalVoucherDetail obj = (JournalVoucherDetail) v.get(i);
            copyObj.addJournalVoucherDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public JournalVoucherPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("JournalVoucher\n");
        str.append("--------------\n")
           .append("JournalVoucherId     : ")
           .append(getJournalVoucherId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TotalDebit           : ")
           .append(getTotalDebit())
           .append("\n")
           .append("TotalCredit          : ")
           .append(getTotalCredit())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Recurring            : ")
           .append(getRecurring())
           .append("\n")
           .append("NextRecurringDate    : ")
           .append(getNextRecurringDate())
           .append("\n")
           .append("TransType            : ")
           .append(getTransType())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
