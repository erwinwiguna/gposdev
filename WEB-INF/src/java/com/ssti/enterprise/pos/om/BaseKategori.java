package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Kategori
 */
public abstract class BaseKategori extends BaseObject
{
    /** The Peer class */
    private static final KategoriPeer peer =
        new KategoriPeer();

        
    /** The value for the kategoriId field */
    private String kategoriId;
      
    /** The value for the kategoriCode field */
    private String kategoriCode;
      
    /** The value for the kategoriLevel field */
    private int kategoriLevel;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the parentId field */
    private String parentId;
      
    /** The value for the hasChild field */
    private boolean hasChild;
      
    /** The value for the addDate field */
    private Date addDate;
      
    /** The value for the internalCode field */
    private String internalCode;
                                                                
    /** The value for the displayStore field */
    private boolean displayStore = true;
                                                
    /** The value for the displayDesc field */
    private String displayDesc = "";
                                                
    /** The value for the picture field */
    private String picture = "";
                                                
    /** The value for the thumbnail field */
    private String thumbnail = "";
  
    
    /**
     * Get the KategoriId
     *
     * @return String
     */
    public String getKategoriId()
    {
        return kategoriId;
    }

                                              
    /**
     * Set the value of KategoriId
     *
     * @param v new value
     */
    public void setKategoriId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.kategoriId, v))
              {
            this.kategoriId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated Item
        if (collItems != null)
        {
            for (int i = 0; i < collItems.size(); i++)
            {
                ((Item) collItems.get(i))
                    .setKategoriId(v);
            }
        }
                                }
  
    /**
     * Get the KategoriCode
     *
     * @return String
     */
    public String getKategoriCode()
    {
        return kategoriCode;
    }

                        
    /**
     * Set the value of KategoriCode
     *
     * @param v new value
     */
    public void setKategoriCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.kategoriCode, v))
              {
            this.kategoriCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KategoriLevel
     *
     * @return int
     */
    public int getKategoriLevel()
    {
        return kategoriLevel;
    }

                        
    /**
     * Set the value of KategoriLevel
     *
     * @param v new value
     */
    public void setKategoriLevel(int v) 
    {
    
                  if (this.kategoriLevel != v)
              {
            this.kategoriLevel = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ParentId
     *
     * @return String
     */
    public String getParentId()
    {
        return parentId;
    }

                        
    /**
     * Set the value of ParentId
     *
     * @param v new value
     */
    public void setParentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.parentId, v))
              {
            this.parentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HasChild
     *
     * @return boolean
     */
    public boolean getHasChild()
    {
        return hasChild;
    }

                        
    /**
     * Set the value of HasChild
     *
     * @param v new value
     */
    public void setHasChild(boolean v) 
    {
    
                  if (this.hasChild != v)
              {
            this.hasChild = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AddDate
     *
     * @return Date
     */
    public Date getAddDate()
    {
        return addDate;
    }

                        
    /**
     * Set the value of AddDate
     *
     * @param v new value
     */
    public void setAddDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.addDate, v))
              {
            this.addDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InternalCode
     *
     * @return String
     */
    public String getInternalCode()
    {
        return internalCode;
    }

                        
    /**
     * Set the value of InternalCode
     *
     * @param v new value
     */
    public void setInternalCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.internalCode, v))
              {
            this.internalCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayStore
     *
     * @return boolean
     */
    public boolean getDisplayStore()
    {
        return displayStore;
    }

                        
    /**
     * Set the value of DisplayStore
     *
     * @param v new value
     */
    public void setDisplayStore(boolean v) 
    {
    
                  if (this.displayStore != v)
              {
            this.displayStore = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayDesc
     *
     * @return String
     */
    public String getDisplayDesc()
    {
        return displayDesc;
    }

                        
    /**
     * Set the value of DisplayDesc
     *
     * @param v new value
     */
    public void setDisplayDesc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayDesc, v))
              {
            this.displayDesc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Picture
     *
     * @return String
     */
    public String getPicture()
    {
        return picture;
    }

                        
    /**
     * Set the value of Picture
     *
     * @param v new value
     */
    public void setPicture(String v) 
    {
    
                  if (!ObjectUtils.equals(this.picture, v))
              {
            this.picture = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Thumbnail
     *
     * @return String
     */
    public String getThumbnail()
    {
        return thumbnail;
    }

                        
    /**
     * Set the value of Thumbnail
     *
     * @param v new value
     */
    public void setThumbnail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.thumbnail, v))
              {
            this.thumbnail = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collItems
     */
    protected List collItems;

    /**
     * Temporary storage of collItems to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initItems()
    {
        if (collItems == null)
        {
            collItems = new ArrayList();
        }
    }

    /**
     * Method called to associate a Item object to this object
     * through the Item foreign key attribute
     *
     * @param l Item
     * @throws TorqueException
     */
    public void addItem(Item l) throws TorqueException
    {
        getItems().add(l);
        l.setKategori((Kategori) this);
    }

    /**
     * The criteria used to select the current contents of collItems
     */
    private Criteria lastItemsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItems(new Criteria())
     *
     * @throws TorqueException
     */
    public List getItems() throws TorqueException
    {
              if (collItems == null)
        {
            collItems = getItems(new Criteria(10));
        }
        return collItems;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategori has previously
     * been saved, it will retrieve related Items from storage.
     * If this Kategori is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getItems(Criteria criteria) throws TorqueException
    {
              if (collItems == null)
        {
            if (isNew())
            {
               collItems = new ArrayList();
            }
            else
            {
                        criteria.add(ItemPeer.KATEGORI_ID, getKategoriId() );
                        collItems = ItemPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ItemPeer.KATEGORI_ID, getKategoriId());
                            if (!lastItemsCriteria.equals(criteria))
                {
                    collItems = ItemPeer.doSelect(criteria);
                }
            }
        }
        lastItemsCriteria = criteria;

        return collItems;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItems(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItems(Connection con) throws TorqueException
    {
              if (collItems == null)
        {
            collItems = getItems(new Criteria(10), con);
        }
        return collItems;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategori has previously
     * been saved, it will retrieve related Items from storage.
     * If this Kategori is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItems(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collItems == null)
        {
            if (isNew())
            {
               collItems = new ArrayList();
            }
            else
            {
                         criteria.add(ItemPeer.KATEGORI_ID, getKategoriId());
                         collItems = ItemPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ItemPeer.KATEGORI_ID, getKategoriId());
                             if (!lastItemsCriteria.equals(criteria))
                 {
                     collItems = ItemPeer.doSelect(criteria, con);
                 }
             }
         }
         lastItemsCriteria = criteria;

         return collItems;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategori is new, it will return
     * an empty collection; or if this Kategori has previously
     * been saved, it will retrieve related Items from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Kategori.
     */
    protected List getItemsJoinKategori(Criteria criteria)
        throws TorqueException
    {
                    if (collItems == null)
        {
            if (isNew())
            {
               collItems = new ArrayList();
            }
            else
            {
                              criteria.add(ItemPeer.KATEGORI_ID, getKategoriId());
                              collItems = ItemPeer.doSelectJoinKategori(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ItemPeer.KATEGORI_ID, getKategoriId());
                                    if (!lastItemsCriteria.equals(criteria))
            {
                collItems = ItemPeer.doSelectJoinKategori(criteria);
            }
        }
        lastItemsCriteria = criteria;

        return collItems;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("KategoriId");
              fieldNames.add("KategoriCode");
              fieldNames.add("KategoriLevel");
              fieldNames.add("Description");
              fieldNames.add("ParentId");
              fieldNames.add("HasChild");
              fieldNames.add("AddDate");
              fieldNames.add("InternalCode");
              fieldNames.add("DisplayStore");
              fieldNames.add("DisplayDesc");
              fieldNames.add("Picture");
              fieldNames.add("Thumbnail");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("KategoriId"))
        {
                return getKategoriId();
            }
          if (name.equals("KategoriCode"))
        {
                return getKategoriCode();
            }
          if (name.equals("KategoriLevel"))
        {
                return Integer.valueOf(getKategoriLevel());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("ParentId"))
        {
                return getParentId();
            }
          if (name.equals("HasChild"))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals("AddDate"))
        {
                return getAddDate();
            }
          if (name.equals("InternalCode"))
        {
                return getInternalCode();
            }
          if (name.equals("DisplayStore"))
        {
                return Boolean.valueOf(getDisplayStore());
            }
          if (name.equals("DisplayDesc"))
        {
                return getDisplayDesc();
            }
          if (name.equals("Picture"))
        {
                return getPicture();
            }
          if (name.equals("Thumbnail"))
        {
                return getThumbnail();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(KategoriPeer.KATEGORI_ID))
        {
                return getKategoriId();
            }
          if (name.equals(KategoriPeer.KATEGORI_CODE))
        {
                return getKategoriCode();
            }
          if (name.equals(KategoriPeer.KATEGORI_LEVEL))
        {
                return Integer.valueOf(getKategoriLevel());
            }
          if (name.equals(KategoriPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(KategoriPeer.PARENT_ID))
        {
                return getParentId();
            }
          if (name.equals(KategoriPeer.HAS_CHILD))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals(KategoriPeer.ADD_DATE))
        {
                return getAddDate();
            }
          if (name.equals(KategoriPeer.INTERNAL_CODE))
        {
                return getInternalCode();
            }
          if (name.equals(KategoriPeer.DISPLAY_STORE))
        {
                return Boolean.valueOf(getDisplayStore());
            }
          if (name.equals(KategoriPeer.DISPLAY_DESC))
        {
                return getDisplayDesc();
            }
          if (name.equals(KategoriPeer.PICTURE))
        {
                return getPicture();
            }
          if (name.equals(KategoriPeer.THUMBNAIL))
        {
                return getThumbnail();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getKategoriId();
            }
              if (pos == 1)
        {
                return getKategoriCode();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getKategoriLevel());
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return getParentId();
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getHasChild());
            }
              if (pos == 6)
        {
                return getAddDate();
            }
              if (pos == 7)
        {
                return getInternalCode();
            }
              if (pos == 8)
        {
                return Boolean.valueOf(getDisplayStore());
            }
              if (pos == 9)
        {
                return getDisplayDesc();
            }
              if (pos == 10)
        {
                return getPicture();
            }
              if (pos == 11)
        {
                return getThumbnail();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(KategoriPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        KategoriPeer.doInsert((Kategori) this, con);
                        setNew(false);
                    }
                    else
                    {
                        KategoriPeer.doUpdate((Kategori) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collItems != null)
            {
                for (int i = 0; i < collItems.size(); i++)
                {
                    ((Item) collItems.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key kategoriId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setKategoriId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setKategoriId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getKategoriId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Kategori copy() throws TorqueException
    {
        return copyInto(new Kategori());
    }
  
    protected Kategori copyInto(Kategori copyObj) throws TorqueException
    {
          copyObj.setKategoriId(kategoriId);
          copyObj.setKategoriCode(kategoriCode);
          copyObj.setKategoriLevel(kategoriLevel);
          copyObj.setDescription(description);
          copyObj.setParentId(parentId);
          copyObj.setHasChild(hasChild);
          copyObj.setAddDate(addDate);
          copyObj.setInternalCode(internalCode);
          copyObj.setDisplayStore(displayStore);
          copyObj.setDisplayDesc(displayDesc);
          copyObj.setPicture(picture);
          copyObj.setThumbnail(thumbnail);
  
                    copyObj.setKategoriId((String)null);
                                                                              
                                      
                            
        List v = getItems();
        for (int i = 0; i < v.size(); i++)
        {
            Item obj = (Item) v.get(i);
            copyObj.addItem(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public KategoriPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Kategori\n");
        str.append("--------\n")
           .append("KategoriId           : ")
           .append(getKategoriId())
           .append("\n")
           .append("KategoriCode         : ")
           .append(getKategoriCode())
           .append("\n")
           .append("KategoriLevel        : ")
           .append(getKategoriLevel())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("ParentId             : ")
           .append(getParentId())
           .append("\n")
           .append("HasChild             : ")
           .append(getHasChild())
           .append("\n")
           .append("AddDate              : ")
           .append(getAddDate())
           .append("\n")
           .append("InternalCode         : ")
           .append(getInternalCode())
           .append("\n")
           .append("DisplayStore         : ")
           .append(getDisplayStore())
           .append("\n")
           .append("DisplayDesc          : ")
           .append(getDisplayDesc())
           .append("\n")
           .append("Picture              : ")
           .append(getPicture())
           .append("\n")
           .append("Thumbnail            : ")
           .append(getThumbnail())
           .append("\n")
        ;
        return(str.toString());
    }
}
