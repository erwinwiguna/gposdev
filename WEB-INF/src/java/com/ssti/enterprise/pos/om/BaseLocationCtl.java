package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to LocationCtl
 */
public abstract class BaseLocationCtl extends BaseObject
{
    /** The Peer class */
    private static final LocationCtlPeer peer =
        new LocationCtlPeer();

        
    /** The value for the locationCtlId field */
    private String locationCtlId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the transCode field */
    private String transCode;
      
    /** The value for the ctlTable field */
    private String ctlTable;
  
    
    /**
     * Get the LocationCtlId
     *
     * @return String
     */
    public String getLocationCtlId()
    {
        return locationCtlId;
    }

                        
    /**
     * Set the value of LocationCtlId
     *
     * @param v new value
     */
    public void setLocationCtlId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationCtlId, v))
              {
            this.locationCtlId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransCode
     *
     * @return String
     */
    public String getTransCode()
    {
        return transCode;
    }

                        
    /**
     * Set the value of TransCode
     *
     * @param v new value
     */
    public void setTransCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transCode, v))
              {
            this.transCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CtlTable
     *
     * @return String
     */
    public String getCtlTable()
    {
        return ctlTable;
    }

                        
    /**
     * Set the value of CtlTable
     *
     * @param v new value
     */
    public void setCtlTable(String v) 
    {
    
                  if (!ObjectUtils.equals(this.ctlTable, v))
              {
            this.ctlTable = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("LocationCtlId");
              fieldNames.add("LocationId");
              fieldNames.add("TransCode");
              fieldNames.add("CtlTable");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("LocationCtlId"))
        {
                return getLocationCtlId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("TransCode"))
        {
                return getTransCode();
            }
          if (name.equals("CtlTable"))
        {
                return getCtlTable();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(LocationCtlPeer.LOCATION_CTL_ID))
        {
                return getLocationCtlId();
            }
          if (name.equals(LocationCtlPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(LocationCtlPeer.TRANS_CODE))
        {
                return getTransCode();
            }
          if (name.equals(LocationCtlPeer.CTL_TABLE))
        {
                return getCtlTable();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getLocationCtlId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getTransCode();
            }
              if (pos == 3)
        {
                return getCtlTable();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(LocationCtlPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        LocationCtlPeer.doInsert((LocationCtl) this, con);
                        setNew(false);
                    }
                    else
                    {
                        LocationCtlPeer.doUpdate((LocationCtl) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key locationCtlId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setLocationCtlId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setLocationCtlId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getLocationCtlId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public LocationCtl copy() throws TorqueException
    {
        return copyInto(new LocationCtl());
    }
  
    protected LocationCtl copyInto(LocationCtl copyObj) throws TorqueException
    {
          copyObj.setLocationCtlId(locationCtlId);
          copyObj.setLocationId(locationId);
          copyObj.setTransCode(transCode);
          copyObj.setCtlTable(ctlTable);
  
                    copyObj.setLocationCtlId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public LocationCtlPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("LocationCtl\n");
        str.append("-----------\n")
           .append("LocationCtlId        : ")
           .append(getLocationCtlId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("TransCode            : ")
           .append(getTransCode())
           .append("\n")
           .append("CtlTable             : ")
           .append(getCtlTable())
           .append("\n")
        ;
        return(str.toString());
    }
}
