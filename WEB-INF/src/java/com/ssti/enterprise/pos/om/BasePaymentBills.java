package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PaymentBills
 */
public abstract class BasePaymentBills extends BaseObject
{
    /** The Peer class */
    private static final PaymentBillsPeer peer =
        new PaymentBillsPeer();

        
    /** The value for the paymentBillsId field */
    private String paymentBillsId;
      
    /** The value for the description field */
    private String description;
                                          
    /** The value for the amount field */
    private int amount = 1;
  
    
    /**
     * Get the PaymentBillsId
     *
     * @return String
     */
    public String getPaymentBillsId()
    {
        return paymentBillsId;
    }

                                              
    /**
     * Set the value of PaymentBillsId
     *
     * @param v new value
     */
    public void setPaymentBillsId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.paymentBillsId, v))
              {
            this.paymentBillsId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated CashierBills
        if (collCashierBillss != null)
        {
            for (int i = 0; i < collCashierBillss.size(); i++)
            {
                ((CashierBills) collCashierBillss.get(i))
                    .setPaymentBillsId(v);
            }
        }
                                }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return int
     */
    public int getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(int v) 
    {
    
                  if (this.amount != v)
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collCashierBillss
     */
    protected List collCashierBillss;

    /**
     * Temporary storage of collCashierBillss to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCashierBillss()
    {
        if (collCashierBillss == null)
        {
            collCashierBillss = new ArrayList();
        }
    }

    /**
     * Method called to associate a CashierBills object to this object
     * through the CashierBills foreign key attribute
     *
     * @param l CashierBills
     * @throws TorqueException
     */
    public void addCashierBills(CashierBills l) throws TorqueException
    {
        getCashierBillss().add(l);
        l.setPaymentBills((PaymentBills) this);
    }

    /**
     * The criteria used to select the current contents of collCashierBillss
     */
    private Criteria lastCashierBillssCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashierBillss(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCashierBillss() throws TorqueException
    {
              if (collCashierBillss == null)
        {
            collCashierBillss = getCashierBillss(new Criteria(10));
        }
        return collCashierBillss;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PaymentBills has previously
     * been saved, it will retrieve related CashierBillss from storage.
     * If this PaymentBills is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCashierBillss(Criteria criteria) throws TorqueException
    {
              if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                        criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId() );
                        collCashierBillss = CashierBillsPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                            if (!lastCashierBillssCriteria.equals(criteria))
                {
                    collCashierBillss = CashierBillsPeer.doSelect(criteria);
                }
            }
        }
        lastCashierBillssCriteria = criteria;

        return collCashierBillss;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashierBillss(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashierBillss(Connection con) throws TorqueException
    {
              if (collCashierBillss == null)
        {
            collCashierBillss = getCashierBillss(new Criteria(10), con);
        }
        return collCashierBillss;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PaymentBills has previously
     * been saved, it will retrieve related CashierBillss from storage.
     * If this PaymentBills is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashierBillss(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                         criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                         collCashierBillss = CashierBillsPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                             if (!lastCashierBillssCriteria.equals(criteria))
                 {
                     collCashierBillss = CashierBillsPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCashierBillssCriteria = criteria;

         return collCashierBillss;
           }

                        
              
                    
                    
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PaymentBills is new, it will return
     * an empty collection; or if this PaymentBills has previously
     * been saved, it will retrieve related CashierBillss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PaymentBills.
     */
    protected List getCashierBillssJoinCashierBalance(Criteria criteria)
        throws TorqueException
    {
                    if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                              criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                              collCashierBillss = CashierBillsPeer.doSelectJoinCashierBalance(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                                    if (!lastCashierBillssCriteria.equals(criteria))
            {
                collCashierBillss = CashierBillsPeer.doSelectJoinCashierBalance(criteria);
            }
        }
        lastCashierBillssCriteria = criteria;

        return collCashierBillss;
                }
                  
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PaymentBills is new, it will return
     * an empty collection; or if this PaymentBills has previously
     * been saved, it will retrieve related CashierBillss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PaymentBills.
     */
    protected List getCashierBillssJoinPaymentBills(Criteria criteria)
        throws TorqueException
    {
                    if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                              criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                              collCashierBillss = CashierBillsPeer.doSelectJoinPaymentBills(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CashierBillsPeer.PAYMENT_BILLS_ID, getPaymentBillsId());
                                    if (!lastCashierBillssCriteria.equals(criteria))
            {
                collCashierBillss = CashierBillsPeer.doSelectJoinPaymentBills(criteria);
            }
        }
        lastCashierBillssCriteria = criteria;

        return collCashierBillss;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PaymentBillsId");
              fieldNames.add("Description");
              fieldNames.add("Amount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PaymentBillsId"))
        {
                return getPaymentBillsId();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Amount"))
        {
                return Integer.valueOf(getAmount());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PaymentBillsPeer.PAYMENT_BILLS_ID))
        {
                return getPaymentBillsId();
            }
          if (name.equals(PaymentBillsPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PaymentBillsPeer.AMOUNT))
        {
                return Integer.valueOf(getAmount());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPaymentBillsId();
            }
              if (pos == 1)
        {
                return getDescription();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getAmount());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PaymentBillsPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PaymentBillsPeer.doInsert((PaymentBills) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PaymentBillsPeer.doUpdate((PaymentBills) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collCashierBillss != null)
            {
                for (int i = 0; i < collCashierBillss.size(); i++)
                {
                    ((CashierBills) collCashierBillss.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key paymentBillsId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPaymentBillsId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPaymentBillsId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPaymentBillsId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PaymentBills copy() throws TorqueException
    {
        return copyInto(new PaymentBills());
    }
  
    protected PaymentBills copyInto(PaymentBills copyObj) throws TorqueException
    {
          copyObj.setPaymentBillsId(paymentBillsId);
          copyObj.setDescription(description);
          copyObj.setAmount(amount);
  
                    copyObj.setPaymentBillsId((String)null);
                        
                                      
                            
        List v = getCashierBillss();
        for (int i = 0; i < v.size(); i++)
        {
            CashierBills obj = (CashierBills) v.get(i);
            copyObj.addCashierBills(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PaymentBillsPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PaymentBills\n");
        str.append("------------\n")
           .append("PaymentBillsId       : ")
           .append(getPaymentBillsId())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}
