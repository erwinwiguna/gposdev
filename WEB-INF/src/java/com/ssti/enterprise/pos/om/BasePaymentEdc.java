package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PaymentEdc
 */
public abstract class BasePaymentEdc extends BaseObject
{
    /** The Peer class */
    private static final PaymentEdcPeer peer =
        new PaymentEdcPeer();

        
    /** The value for the paymentEdcId field */
    private String paymentEdcId;
      
    /** The value for the startDate field */
    private Date startDate;
      
    /** The value for the endDate field */
    private Date endDate;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
                                                
    /** The value for the issuerId field */
    private String issuerId = "";
                                                
    /** The value for the description field */
    private String description = "";
                                                
          
    /** The value for the rate field */
    private BigDecimal rate= new BigDecimal(1);
                                          
    /** The value for the paymentDay field */
    private int paymentDay = 1;
                                                        
    /** The value for the payHoliday field */
    private boolean payHoliday = false;
  
    
    /**
     * Get the PaymentEdcId
     *
     * @return String
     */
    public String getPaymentEdcId()
    {
        return paymentEdcId;
    }

                        
    /**
     * Set the value of PaymentEdcId
     *
     * @param v new value
     */
    public void setPaymentEdcId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentEdcId, v))
              {
            this.paymentEdcId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartDate
     *
     * @return Date
     */
    public Date getStartDate()
    {
        return startDate;
    }

                        
    /**
     * Set the value of StartDate
     *
     * @param v new value
     */
    public void setStartDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.startDate, v))
              {
            this.startDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndDate
     *
     * @return Date
     */
    public Date getEndDate()
    {
        return endDate;
    }

                        
    /**
     * Set the value of EndDate
     *
     * @param v new value
     */
    public void setEndDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.endDate, v))
              {
            this.endDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IssuerId
     *
     * @return String
     */
    public String getIssuerId()
    {
        return issuerId;
    }

                        
    /**
     * Set the value of IssuerId
     *
     * @param v new value
     */
    public void setIssuerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.issuerId, v))
              {
            this.issuerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Rate
     *
     * @return BigDecimal
     */
    public BigDecimal getRate()
    {
        return rate;
    }

                        
    /**
     * Set the value of Rate
     *
     * @param v new value
     */
    public void setRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.rate, v))
              {
            this.rate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentDay
     *
     * @return int
     */
    public int getPaymentDay()
    {
        return paymentDay;
    }

                        
    /**
     * Set the value of PaymentDay
     *
     * @param v new value
     */
    public void setPaymentDay(int v) 
    {
    
                  if (this.paymentDay != v)
              {
            this.paymentDay = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PayHoliday
     *
     * @return boolean
     */
    public boolean getPayHoliday()
    {
        return payHoliday;
    }

                        
    /**
     * Set the value of PayHoliday
     *
     * @param v new value
     */
    public void setPayHoliday(boolean v) 
    {
    
                  if (this.payHoliday != v)
              {
            this.payHoliday = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PaymentEdcId");
              fieldNames.add("StartDate");
              fieldNames.add("EndDate");
              fieldNames.add("PaymentTermId");
              fieldNames.add("IssuerId");
              fieldNames.add("Description");
              fieldNames.add("Rate");
              fieldNames.add("PaymentDay");
              fieldNames.add("PayHoliday");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PaymentEdcId"))
        {
                return getPaymentEdcId();
            }
          if (name.equals("StartDate"))
        {
                return getStartDate();
            }
          if (name.equals("EndDate"))
        {
                return getEndDate();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("IssuerId"))
        {
                return getIssuerId();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Rate"))
        {
                return getRate();
            }
          if (name.equals("PaymentDay"))
        {
                return Integer.valueOf(getPaymentDay());
            }
          if (name.equals("PayHoliday"))
        {
                return Boolean.valueOf(getPayHoliday());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PaymentEdcPeer.PAYMENT_EDC_ID))
        {
                return getPaymentEdcId();
            }
          if (name.equals(PaymentEdcPeer.START_DATE))
        {
                return getStartDate();
            }
          if (name.equals(PaymentEdcPeer.END_DATE))
        {
                return getEndDate();
            }
          if (name.equals(PaymentEdcPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(PaymentEdcPeer.ISSUER_ID))
        {
                return getIssuerId();
            }
          if (name.equals(PaymentEdcPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PaymentEdcPeer.RATE))
        {
                return getRate();
            }
          if (name.equals(PaymentEdcPeer.PAYMENT_DAY))
        {
                return Integer.valueOf(getPaymentDay());
            }
          if (name.equals(PaymentEdcPeer.PAY_HOLIDAY))
        {
                return Boolean.valueOf(getPayHoliday());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPaymentEdcId();
            }
              if (pos == 1)
        {
                return getStartDate();
            }
              if (pos == 2)
        {
                return getEndDate();
            }
              if (pos == 3)
        {
                return getPaymentTermId();
            }
              if (pos == 4)
        {
                return getIssuerId();
            }
              if (pos == 5)
        {
                return getDescription();
            }
              if (pos == 6)
        {
                return getRate();
            }
              if (pos == 7)
        {
                return Integer.valueOf(getPaymentDay());
            }
              if (pos == 8)
        {
                return Boolean.valueOf(getPayHoliday());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PaymentEdcPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PaymentEdcPeer.doInsert((PaymentEdc) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PaymentEdcPeer.doUpdate((PaymentEdc) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key paymentEdcId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPaymentEdcId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPaymentEdcId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPaymentEdcId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PaymentEdc copy() throws TorqueException
    {
        return copyInto(new PaymentEdc());
    }
  
    protected PaymentEdc copyInto(PaymentEdc copyObj) throws TorqueException
    {
          copyObj.setPaymentEdcId(paymentEdcId);
          copyObj.setStartDate(startDate);
          copyObj.setEndDate(endDate);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setIssuerId(issuerId);
          copyObj.setDescription(description);
          copyObj.setRate(rate);
          copyObj.setPaymentDay(paymentDay);
          copyObj.setPayHoliday(payHoliday);
  
                    copyObj.setPaymentEdcId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PaymentEdcPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PaymentEdc\n");
        str.append("----------\n")
           .append("PaymentEdcId         : ")
           .append(getPaymentEdcId())
           .append("\n")
           .append("StartDate            : ")
           .append(getStartDate())
           .append("\n")
           .append("EndDate              : ")
           .append(getEndDate())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("IssuerId             : ")
           .append(getIssuerId())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Rate                 : ")
           .append(getRate())
           .append("\n")
           .append("PaymentDay           : ")
           .append(getPaymentDay())
           .append("\n")
           .append("PayHoliday           : ")
           .append(getPayHoliday())
           .append("\n")
        ;
        return(str.toString());
    }
}
