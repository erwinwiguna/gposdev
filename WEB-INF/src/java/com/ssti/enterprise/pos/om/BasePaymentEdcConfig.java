package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PaymentEdcConfig
 */
public abstract class BasePaymentEdcConfig extends BaseObject
{
    /** The Peer class */
    private static final PaymentEdcConfigPeer peer =
        new PaymentEdcConfigPeer();

        
    /** The value for the configId field */
    private String configId;
                                                
    /** The value for the paymentTermId field */
    private String paymentTermId = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                                
    /** The value for the merchantId field */
    private String merchantId = "";
  
    
    /**
     * Get the ConfigId
     *
     * @return String
     */
    public String getConfigId()
    {
        return configId;
    }

                        
    /**
     * Set the value of ConfigId
     *
     * @param v new value
     */
    public void setConfigId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.configId, v))
              {
            this.configId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MerchantId
     *
     * @return String
     */
    public String getMerchantId()
    {
        return merchantId;
    }

                        
    /**
     * Set the value of MerchantId
     *
     * @param v new value
     */
    public void setMerchantId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.merchantId, v))
              {
            this.merchantId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ConfigId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("LocationId");
              fieldNames.add("MerchantId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ConfigId"))
        {
                return getConfigId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("MerchantId"))
        {
                return getMerchantId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PaymentEdcConfigPeer.CONFIG_ID))
        {
                return getConfigId();
            }
          if (name.equals(PaymentEdcConfigPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(PaymentEdcConfigPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PaymentEdcConfigPeer.MERCHANT_ID))
        {
                return getMerchantId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getConfigId();
            }
              if (pos == 1)
        {
                return getPaymentTermId();
            }
              if (pos == 2)
        {
                return getLocationId();
            }
              if (pos == 3)
        {
                return getMerchantId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PaymentEdcConfigPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PaymentEdcConfigPeer.doInsert((PaymentEdcConfig) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PaymentEdcConfigPeer.doUpdate((PaymentEdcConfig) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key configId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setConfigId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setConfigId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getConfigId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PaymentEdcConfig copy() throws TorqueException
    {
        return copyInto(new PaymentEdcConfig());
    }
  
    protected PaymentEdcConfig copyInto(PaymentEdcConfig copyObj) throws TorqueException
    {
          copyObj.setConfigId(configId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setLocationId(locationId);
          copyObj.setMerchantId(merchantId);
  
                    copyObj.setConfigId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PaymentEdcConfigPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PaymentEdcConfig\n");
        str.append("----------------\n")
           .append("ConfigId             : ")
           .append(getConfigId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("MerchantId           : ")
           .append(getMerchantId())
           .append("\n")
        ;
        return(str.toString());
    }
}
