package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.PeriodMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BasePeriodPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "period";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PeriodMapBuilder.CLASS_NAME);
    }

      /** the column name for the PERIOD_ID field */
    public static final String PERIOD_ID;
      /** the column name for the BEGIN_DATE field */
    public static final String BEGIN_DATE;
      /** the column name for the END_DATE field */
    public static final String END_DATE;
      /** the column name for the MONTH field */
    public static final String MONTH;
      /** the column name for the YEAR field */
    public static final String YEAR;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the IS_CLOSED field */
    public static final String IS_CLOSED;
      /** the column name for the PERIOD_CODE field */
    public static final String PERIOD_CODE;
      /** the column name for the CLOSED_BY field */
    public static final String CLOSED_BY;
      /** the column name for the CLOSED_DATE field */
    public static final String CLOSED_DATE;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the CLOSE_TRANS_ID field */
    public static final String CLOSE_TRANS_ID;
      /** the column name for the DEPR_TRANS_ID field */
    public static final String DEPR_TRANS_ID;
      /** the column name for the IS_FA_CLOSED field */
    public static final String IS_FA_CLOSED;
      /** the column name for the FA_CLOSED_BY field */
    public static final String FA_CLOSED_BY;
      /** the column name for the FA_CLOSED_DATE field */
    public static final String FA_CLOSED_DATE;
      /** the column name for the FA_CLOSING_REMARK field */
    public static final String FA_CLOSING_REMARK;
      /** the column name for the IS_YEAR_CLOSED field */
    public static final String IS_YEAR_CLOSED;
      /** the column name for the CLOSE_YEAR_TRANS_ID field */
    public static final String CLOSE_YEAR_TRANS_ID;
      /** the column name for the CLOSE_YEAR_REMARK field */
    public static final String CLOSE_YEAR_REMARK;
  
    static
    {
          PERIOD_ID = "period.PERIOD_ID";
          BEGIN_DATE = "period.BEGIN_DATE";
          END_DATE = "period.END_DATE";
          MONTH = "period.MONTH";
          YEAR = "period.YEAR";
          DESCRIPTION = "period.DESCRIPTION";
          IS_CLOSED = "period.IS_CLOSED";
          PERIOD_CODE = "period.PERIOD_CODE";
          CLOSED_BY = "period.CLOSED_BY";
          CLOSED_DATE = "period.CLOSED_DATE";
          REMARK = "period.REMARK";
          CLOSE_TRANS_ID = "period.CLOSE_TRANS_ID";
          DEPR_TRANS_ID = "period.DEPR_TRANS_ID";
          IS_FA_CLOSED = "period.IS_FA_CLOSED";
          FA_CLOSED_BY = "period.FA_CLOSED_BY";
          FA_CLOSED_DATE = "period.FA_CLOSED_DATE";
          FA_CLOSING_REMARK = "period.FA_CLOSING_REMARK";
          IS_YEAR_CLOSED = "period.IS_YEAR_CLOSED";
          CLOSE_YEAR_TRANS_ID = "period.CLOSE_YEAR_TRANS_ID";
          CLOSE_YEAR_REMARK = "period.CLOSE_YEAR_REMARK";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PeriodMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PeriodMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  20;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Period";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePeriodPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FA_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_FA_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FA_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_YEAR_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_YEAR_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_YEAR_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PERIOD_ID);
          criteria.addSelectColumn(BEGIN_DATE);
          criteria.addSelectColumn(END_DATE);
          criteria.addSelectColumn(MONTH);
          criteria.addSelectColumn(YEAR);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(IS_CLOSED);
          criteria.addSelectColumn(PERIOD_CODE);
          criteria.addSelectColumn(CLOSED_BY);
          criteria.addSelectColumn(CLOSED_DATE);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(CLOSE_TRANS_ID);
          criteria.addSelectColumn(DEPR_TRANS_ID);
          criteria.addSelectColumn(IS_FA_CLOSED);
          criteria.addSelectColumn(FA_CLOSED_BY);
          criteria.addSelectColumn(FA_CLOSED_DATE);
          criteria.addSelectColumn(FA_CLOSING_REMARK);
          criteria.addSelectColumn(IS_YEAR_CLOSED);
          criteria.addSelectColumn(CLOSE_YEAR_TRANS_ID);
          criteria.addSelectColumn(CLOSE_YEAR_REMARK);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Period row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Period obj = (Period) cls.newInstance();
            PeriodPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Period obj)
        throws TorqueException
    {
        try
        {
                obj.setPeriodId(row.getValue(offset + 0).asString());
                  obj.setBeginDate(row.getValue(offset + 1).asUtilDate());
                  obj.setEndDate(row.getValue(offset + 2).asUtilDate());
                  obj.setMonth(row.getValue(offset + 3).asInt());
                  obj.setYear(row.getValue(offset + 4).asInt());
                  obj.setDescription(row.getValue(offset + 5).asString());
                  obj.setIsClosed(row.getValue(offset + 6).asBoolean());
                  obj.setPeriodCode(row.getValue(offset + 7).asInt());
                  obj.setClosedBy(row.getValue(offset + 8).asString());
                  obj.setClosedDate(row.getValue(offset + 9).asUtilDate());
                  obj.setRemark(row.getValue(offset + 10).asString());
                  obj.setCloseTransId(row.getValue(offset + 11).asString());
                  obj.setDeprTransId(row.getValue(offset + 12).asString());
                  obj.setIsFaClosed(row.getValue(offset + 13).asBoolean());
                  obj.setFaClosedBy(row.getValue(offset + 14).asString());
                  obj.setFaClosedDate(row.getValue(offset + 15).asUtilDate());
                  obj.setFaClosingRemark(row.getValue(offset + 16).asString());
                  obj.setIsYearClosed(row.getValue(offset + 17).asBoolean());
                  obj.setCloseYearTransId(row.getValue(offset + 18).asString());
                  obj.setCloseYearRemark(row.getValue(offset + 19).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePeriodPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FA_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_FA_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FA_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_YEAR_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_YEAR_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_YEAR_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PeriodPeer.row2Object(row, 1,
                PeriodPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePeriodPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PERIOD_ID, criteria.remove(PERIOD_ID));
                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FA_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_FA_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FA_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_YEAR_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_YEAR_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_YEAR_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PeriodPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_FA_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_FA_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_FA_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_YEAR_CLOSED))
        {
            Object possibleBoolean = criteria.get(IS_YEAR_CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_YEAR_CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Period obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Period obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Period obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Period obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Period) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Period obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Period) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Period obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Period) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Period obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePeriodPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PERIOD_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Period obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PERIOD_ID, obj.getPeriodId());
              criteria.add(BEGIN_DATE, obj.getBeginDate());
              criteria.add(END_DATE, obj.getEndDate());
              criteria.add(MONTH, obj.getMonth());
              criteria.add(YEAR, obj.getYear());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(IS_CLOSED, obj.getIsClosed());
              criteria.add(PERIOD_CODE, obj.getPeriodCode());
              criteria.add(CLOSED_BY, obj.getClosedBy());
              criteria.add(CLOSED_DATE, obj.getClosedDate());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(CLOSE_TRANS_ID, obj.getCloseTransId());
              criteria.add(DEPR_TRANS_ID, obj.getDeprTransId());
              criteria.add(IS_FA_CLOSED, obj.getIsFaClosed());
              criteria.add(FA_CLOSED_BY, obj.getFaClosedBy());
              criteria.add(FA_CLOSED_DATE, obj.getFaClosedDate());
              criteria.add(FA_CLOSING_REMARK, obj.getFaClosingRemark());
              criteria.add(IS_YEAR_CLOSED, obj.getIsYearClosed());
              criteria.add(CLOSE_YEAR_TRANS_ID, obj.getCloseYearTransId());
              criteria.add(CLOSE_YEAR_REMARK, obj.getCloseYearRemark());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Period obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PERIOD_ID, obj.getPeriodId());
                          criteria.add(BEGIN_DATE, obj.getBeginDate());
                          criteria.add(END_DATE, obj.getEndDate());
                          criteria.add(MONTH, obj.getMonth());
                          criteria.add(YEAR, obj.getYear());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(IS_CLOSED, obj.getIsClosed());
                          criteria.add(PERIOD_CODE, obj.getPeriodCode());
                          criteria.add(CLOSED_BY, obj.getClosedBy());
                          criteria.add(CLOSED_DATE, obj.getClosedDate());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(CLOSE_TRANS_ID, obj.getCloseTransId());
                          criteria.add(DEPR_TRANS_ID, obj.getDeprTransId());
                          criteria.add(IS_FA_CLOSED, obj.getIsFaClosed());
                          criteria.add(FA_CLOSED_BY, obj.getFaClosedBy());
                          criteria.add(FA_CLOSED_DATE, obj.getFaClosedDate());
                          criteria.add(FA_CLOSING_REMARK, obj.getFaClosingRemark());
                          criteria.add(IS_YEAR_CLOSED, obj.getIsYearClosed());
                          criteria.add(CLOSE_YEAR_TRANS_ID, obj.getCloseYearTransId());
                          criteria.add(CLOSE_YEAR_REMARK, obj.getCloseYearRemark());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Period retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Period retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Period retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Period retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Period retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Period)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PERIOD_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
