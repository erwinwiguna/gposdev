package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PettyCash
 */
public abstract class BasePettyCash extends BaseObject
{
    /** The Peer class */
    private static final PettyCashPeer peer =
        new PettyCashPeer();

        
    /** The value for the pettyCashId field */
    private String pettyCashId;
      
    /** The value for the pettyCashNo field */
    private String pettyCashNo;
      
    /** The value for the incomingOutgoing field */
    private int incomingOutgoing;
      
    /** The value for the pettyCashTypeId field */
    private String pettyCashTypeId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the referenceNo field */
    private String referenceNo;
      
    /** The value for the pettyCashAmount field */
    private BigDecimal pettyCashAmount;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the description field */
    private String description;
                                                
    /** The value for the employeeId field */
    private String employeeId = "";
      
    /** The value for the status field */
    private int status;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the PettyCashId
     *
     * @return String
     */
    public String getPettyCashId()
    {
        return pettyCashId;
    }

                        
    /**
     * Set the value of PettyCashId
     *
     * @param v new value
     */
    public void setPettyCashId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashId, v))
              {
            this.pettyCashId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PettyCashNo
     *
     * @return String
     */
    public String getPettyCashNo()
    {
        return pettyCashNo;
    }

                        
    /**
     * Set the value of PettyCashNo
     *
     * @param v new value
     */
    public void setPettyCashNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashNo, v))
              {
            this.pettyCashNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IncomingOutgoing
     *
     * @return int
     */
    public int getIncomingOutgoing()
    {
        return incomingOutgoing;
    }

                        
    /**
     * Set the value of IncomingOutgoing
     *
     * @param v new value
     */
    public void setIncomingOutgoing(int v) 
    {
    
                  if (this.incomingOutgoing != v)
              {
            this.incomingOutgoing = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PettyCashTypeId
     *
     * @return String
     */
    public String getPettyCashTypeId()
    {
        return pettyCashTypeId;
    }

                        
    /**
     * Set the value of PettyCashTypeId
     *
     * @param v new value
     */
    public void setPettyCashTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashTypeId, v))
              {
            this.pettyCashTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PettyCashAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPettyCashAmount()
    {
        return pettyCashAmount;
    }

                        
    /**
     * Set the value of PettyCashAmount
     *
     * @param v new value
     */
    public void setPettyCashAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashAmount, v))
              {
            this.pettyCashAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmployeeId
     *
     * @return String
     */
    public String getEmployeeId()
    {
        return employeeId;
    }

                        
    /**
     * Set the value of EmployeeId
     *
     * @param v new value
     */
    public void setEmployeeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.employeeId, v))
              {
            this.employeeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PettyCashId");
              fieldNames.add("PettyCashNo");
              fieldNames.add("IncomingOutgoing");
              fieldNames.add("PettyCashTypeId");
              fieldNames.add("LocationId");
              fieldNames.add("TransactionDate");
              fieldNames.add("ReferenceNo");
              fieldNames.add("PettyCashAmount");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames.add("EmployeeId");
              fieldNames.add("Status");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PettyCashId"))
        {
                return getPettyCashId();
            }
          if (name.equals("PettyCashNo"))
        {
                return getPettyCashNo();
            }
          if (name.equals("IncomingOutgoing"))
        {
                return Integer.valueOf(getIncomingOutgoing());
            }
          if (name.equals("PettyCashTypeId"))
        {
                return getPettyCashTypeId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("PettyCashAmount"))
        {
                return getPettyCashAmount();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("EmployeeId"))
        {
                return getEmployeeId();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PettyCashPeer.PETTY_CASH_ID))
        {
                return getPettyCashId();
            }
          if (name.equals(PettyCashPeer.PETTY_CASH_NO))
        {
                return getPettyCashNo();
            }
          if (name.equals(PettyCashPeer.INCOMING_OUTGOING))
        {
                return Integer.valueOf(getIncomingOutgoing());
            }
          if (name.equals(PettyCashPeer.PETTY_CASH_TYPE_ID))
        {
                return getPettyCashTypeId();
            }
          if (name.equals(PettyCashPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PettyCashPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(PettyCashPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(PettyCashPeer.PETTY_CASH_AMOUNT))
        {
                return getPettyCashAmount();
            }
          if (name.equals(PettyCashPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(PettyCashPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PettyCashPeer.EMPLOYEE_ID))
        {
                return getEmployeeId();
            }
          if (name.equals(PettyCashPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(PettyCashPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(PettyCashPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPettyCashId();
            }
              if (pos == 1)
        {
                return getPettyCashNo();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getIncomingOutgoing());
            }
              if (pos == 3)
        {
                return getPettyCashTypeId();
            }
              if (pos == 4)
        {
                return getLocationId();
            }
              if (pos == 5)
        {
                return getTransactionDate();
            }
              if (pos == 6)
        {
                return getReferenceNo();
            }
              if (pos == 7)
        {
                return getPettyCashAmount();
            }
              if (pos == 8)
        {
                return getUserName();
            }
              if (pos == 9)
        {
                return getDescription();
            }
              if (pos == 10)
        {
                return getEmployeeId();
            }
              if (pos == 11)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 12)
        {
                return getCancelBy();
            }
              if (pos == 13)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PettyCashPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PettyCashPeer.doInsert((PettyCash) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PettyCashPeer.doUpdate((PettyCash) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pettyCashId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPettyCashId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPettyCashId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPettyCashId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PettyCash copy() throws TorqueException
    {
        return copyInto(new PettyCash());
    }
  
    protected PettyCash copyInto(PettyCash copyObj) throws TorqueException
    {
          copyObj.setPettyCashId(pettyCashId);
          copyObj.setPettyCashNo(pettyCashNo);
          copyObj.setIncomingOutgoing(incomingOutgoing);
          copyObj.setPettyCashTypeId(pettyCashTypeId);
          copyObj.setLocationId(locationId);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setPettyCashAmount(pettyCashAmount);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
          copyObj.setEmployeeId(employeeId);
          copyObj.setStatus(status);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setPettyCashId((String)null);
                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PettyCashPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PettyCash\n");
        str.append("---------\n")
           .append("PettyCashId          : ")
           .append(getPettyCashId())
           .append("\n")
           .append("PettyCashNo          : ")
           .append(getPettyCashNo())
           .append("\n")
           .append("IncomingOutgoing     : ")
           .append(getIncomingOutgoing())
           .append("\n")
           .append("PettyCashTypeId      : ")
           .append(getPettyCashTypeId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("PettyCashAmount      : ")
           .append(getPettyCashAmount())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("EmployeeId           : ")
           .append(getEmployeeId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
