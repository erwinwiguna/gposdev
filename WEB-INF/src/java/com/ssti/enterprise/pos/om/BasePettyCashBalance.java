package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PettyCashBalance
 */
public abstract class BasePettyCashBalance extends BaseObject
{
    /** The Peer class */
    private static final PettyCashBalancePeer peer =
        new PettyCashBalancePeer();

        
    /** The value for the pettyCashBalanceId field */
    private String pettyCashBalanceId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the openingBalance field */
    private BigDecimal openingBalance;
      
    /** The value for the pettyCashBalance field */
    private BigDecimal pettyCashBalance;
  
    
    /**
     * Get the PettyCashBalanceId
     *
     * @return String
     */
    public String getPettyCashBalanceId()
    {
        return pettyCashBalanceId;
    }

                        
    /**
     * Set the value of PettyCashBalanceId
     *
     * @param v new value
     */
    public void setPettyCashBalanceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashBalanceId, v))
              {
            this.pettyCashBalanceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PettyCashBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getPettyCashBalance()
    {
        return pettyCashBalance;
    }

                        
    /**
     * Set the value of PettyCashBalance
     *
     * @param v new value
     */
    public void setPettyCashBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashBalance, v))
              {
            this.pettyCashBalance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PettyCashBalanceId");
              fieldNames.add("LocationId");
              fieldNames.add("OpeningBalance");
              fieldNames.add("PettyCashBalance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PettyCashBalanceId"))
        {
                return getPettyCashBalanceId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("PettyCashBalance"))
        {
                return getPettyCashBalance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PettyCashBalancePeer.PETTY_CASH_BALANCE_ID))
        {
                return getPettyCashBalanceId();
            }
          if (name.equals(PettyCashBalancePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PettyCashBalancePeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(PettyCashBalancePeer.PETTY_CASH_BALANCE))
        {
                return getPettyCashBalance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPettyCashBalanceId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getOpeningBalance();
            }
              if (pos == 3)
        {
                return getPettyCashBalance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PettyCashBalancePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PettyCashBalancePeer.doInsert((PettyCashBalance) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PettyCashBalancePeer.doUpdate((PettyCashBalance) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pettyCashBalanceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPettyCashBalanceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPettyCashBalanceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPettyCashBalanceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PettyCashBalance copy() throws TorqueException
    {
        return copyInto(new PettyCashBalance());
    }
  
    protected PettyCashBalance copyInto(PettyCashBalance copyObj) throws TorqueException
    {
          copyObj.setPettyCashBalanceId(pettyCashBalanceId);
          copyObj.setLocationId(locationId);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setPettyCashBalance(pettyCashBalance);
  
                    copyObj.setPettyCashBalanceId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PettyCashBalancePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PettyCashBalance\n");
        str.append("----------------\n")
           .append("PettyCashBalanceId   : ")
           .append(getPettyCashBalanceId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("PettyCashBalance     : ")
           .append(getPettyCashBalance())
           .append("\n")
        ;
        return(str.toString());
    }
}
