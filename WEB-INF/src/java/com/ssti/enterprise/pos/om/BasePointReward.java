package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PointReward
 */
public abstract class BasePointReward extends BaseObject
{
    /** The Peer class */
    private static final PointRewardPeer peer =
        new PointRewardPeer();

        
    /** The value for the pointRewardId field */
    private String pointRewardId;
      
    /** The value for the fromAmount field */
    private BigDecimal fromAmount;
      
    /** The value for the toAmount field */
    private BigDecimal toAmount;
      
    /** The value for the multiplyAmount field */
    private BigDecimal multiplyAmount;
      
    /** The value for the point field */
    private BigDecimal point;
  
    
    /**
     * Get the PointRewardId
     *
     * @return String
     */
    public String getPointRewardId()
    {
        return pointRewardId;
    }

                        
    /**
     * Set the value of PointRewardId
     *
     * @param v new value
     */
    public void setPointRewardId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointRewardId, v))
              {
            this.pointRewardId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FromAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getFromAmount()
    {
        return fromAmount;
    }

                        
    /**
     * Set the value of FromAmount
     *
     * @param v new value
     */
    public void setFromAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fromAmount, v))
              {
            this.fromAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ToAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getToAmount()
    {
        return toAmount;
    }

                        
    /**
     * Set the value of ToAmount
     *
     * @param v new value
     */
    public void setToAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.toAmount, v))
              {
            this.toAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MultiplyAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getMultiplyAmount()
    {
        return multiplyAmount;
    }

                        
    /**
     * Set the value of MultiplyAmount
     *
     * @param v new value
     */
    public void setMultiplyAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.multiplyAmount, v))
              {
            this.multiplyAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Point
     *
     * @return BigDecimal
     */
    public BigDecimal getPoint()
    {
        return point;
    }

                        
    /**
     * Set the value of Point
     *
     * @param v new value
     */
    public void setPoint(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.point, v))
              {
            this.point = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PointRewardId");
              fieldNames.add("FromAmount");
              fieldNames.add("ToAmount");
              fieldNames.add("MultiplyAmount");
              fieldNames.add("Point");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PointRewardId"))
        {
                return getPointRewardId();
            }
          if (name.equals("FromAmount"))
        {
                return getFromAmount();
            }
          if (name.equals("ToAmount"))
        {
                return getToAmount();
            }
          if (name.equals("MultiplyAmount"))
        {
                return getMultiplyAmount();
            }
          if (name.equals("Point"))
        {
                return getPoint();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PointRewardPeer.POINT_REWARD_ID))
        {
                return getPointRewardId();
            }
          if (name.equals(PointRewardPeer.FROM_AMOUNT))
        {
                return getFromAmount();
            }
          if (name.equals(PointRewardPeer.TO_AMOUNT))
        {
                return getToAmount();
            }
          if (name.equals(PointRewardPeer.MULTIPLY_AMOUNT))
        {
                return getMultiplyAmount();
            }
          if (name.equals(PointRewardPeer.POINT))
        {
                return getPoint();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPointRewardId();
            }
              if (pos == 1)
        {
                return getFromAmount();
            }
              if (pos == 2)
        {
                return getToAmount();
            }
              if (pos == 3)
        {
                return getMultiplyAmount();
            }
              if (pos == 4)
        {
                return getPoint();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PointRewardPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PointRewardPeer.doInsert((PointReward) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PointRewardPeer.doUpdate((PointReward) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pointRewardId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPointRewardId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPointRewardId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPointRewardId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PointReward copy() throws TorqueException
    {
        return copyInto(new PointReward());
    }
  
    protected PointReward copyInto(PointReward copyObj) throws TorqueException
    {
          copyObj.setPointRewardId(pointRewardId);
          copyObj.setFromAmount(fromAmount);
          copyObj.setToAmount(toAmount);
          copyObj.setMultiplyAmount(multiplyAmount);
          copyObj.setPoint(point);
  
                    copyObj.setPointRewardId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PointRewardPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PointReward\n");
        str.append("-----------\n")
           .append("PointRewardId        : ")
           .append(getPointRewardId())
           .append("\n")
           .append("FromAmount           : ")
           .append(getFromAmount())
           .append("\n")
           .append("ToAmount             : ")
           .append(getToAmount())
           .append("\n")
           .append("MultiplyAmount       : ")
           .append(getMultiplyAmount())
           .append("\n")
           .append("Point                : ")
           .append(getPoint())
           .append("\n")
        ;
        return(str.toString());
    }
}
