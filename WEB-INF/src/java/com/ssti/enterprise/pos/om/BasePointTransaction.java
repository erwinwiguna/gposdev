package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PointTransaction
 */
public abstract class BasePointTransaction extends BaseObject
{
    /** The Peer class */
    private static final PointTransactionPeer peer =
        new PointTransactionPeer();

        
    /** The value for the pointTransactionId field */
    private String pointTransactionId;
                                                
    /** The value for the pointTypeId field */
    private String pointTypeId = "";
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the customerId field */
    private String customerId;
                                                
    /** The value for the transactionId field */
    private String transactionId = "";
                                                
    /** The value for the transactionNo field */
    private String transactionNo = "";
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the pointBefore field */
    private BigDecimal pointBefore;
      
    /** The value for the pointChanges field */
    private BigDecimal pointChanges;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the userName field */
    private String userName;
  
    
    /**
     * Get the PointTransactionId
     *
     * @return String
     */
    public String getPointTransactionId()
    {
        return pointTransactionId;
    }

                        
    /**
     * Set the value of PointTransactionId
     *
     * @param v new value
     */
    public void setPointTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointTransactionId, v))
              {
            this.pointTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointTypeId
     *
     * @return String
     */
    public String getPointTypeId()
    {
        return pointTypeId;
    }

                        
    /**
     * Set the value of PointTypeId
     *
     * @param v new value
     */
    public void setPointTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointTypeId, v))
              {
            this.pointTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointBefore
     *
     * @return BigDecimal
     */
    public BigDecimal getPointBefore()
    {
        return pointBefore;
    }

                        
    /**
     * Set the value of PointBefore
     *
     * @param v new value
     */
    public void setPointBefore(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.pointBefore, v))
              {
            this.pointBefore = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointChanges
     *
     * @return BigDecimal
     */
    public BigDecimal getPointChanges()
    {
        return pointChanges;
    }

                        
    /**
     * Set the value of PointChanges
     *
     * @param v new value
     */
    public void setPointChanges(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.pointChanges, v))
              {
            this.pointChanges = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PointTransactionId");
              fieldNames.add("PointTypeId");
              fieldNames.add("TransactionType");
              fieldNames.add("CustomerId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("LocationId");
              fieldNames.add("PointBefore");
              fieldNames.add("PointChanges");
              fieldNames.add("Description");
              fieldNames.add("CreateDate");
              fieldNames.add("UserName");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PointTransactionId"))
        {
                return getPointTransactionId();
            }
          if (name.equals("PointTypeId"))
        {
                return getPointTypeId();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("PointBefore"))
        {
                return getPointBefore();
            }
          if (name.equals("PointChanges"))
        {
                return getPointChanges();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PointTransactionPeer.POINT_TRANSACTION_ID))
        {
                return getPointTransactionId();
            }
          if (name.equals(PointTransactionPeer.POINT_TYPE_ID))
        {
                return getPointTypeId();
            }
          if (name.equals(PointTransactionPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(PointTransactionPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(PointTransactionPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(PointTransactionPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(PointTransactionPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(PointTransactionPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PointTransactionPeer.POINT_BEFORE))
        {
                return getPointBefore();
            }
          if (name.equals(PointTransactionPeer.POINT_CHANGES))
        {
                return getPointChanges();
            }
          if (name.equals(PointTransactionPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PointTransactionPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(PointTransactionPeer.USER_NAME))
        {
                return getUserName();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPointTransactionId();
            }
              if (pos == 1)
        {
                return getPointTypeId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 3)
        {
                return getCustomerId();
            }
              if (pos == 4)
        {
                return getTransactionId();
            }
              if (pos == 5)
        {
                return getTransactionNo();
            }
              if (pos == 6)
        {
                return getTransactionDate();
            }
              if (pos == 7)
        {
                return getLocationId();
            }
              if (pos == 8)
        {
                return getPointBefore();
            }
              if (pos == 9)
        {
                return getPointChanges();
            }
              if (pos == 10)
        {
                return getDescription();
            }
              if (pos == 11)
        {
                return getCreateDate();
            }
              if (pos == 12)
        {
                return getUserName();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PointTransactionPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PointTransactionPeer.doInsert((PointTransaction) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PointTransactionPeer.doUpdate((PointTransaction) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pointTransactionId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPointTransactionId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPointTransactionId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPointTransactionId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PointTransaction copy() throws TorqueException
    {
        return copyInto(new PointTransaction());
    }
  
    protected PointTransaction copyInto(PointTransaction copyObj) throws TorqueException
    {
          copyObj.setPointTransactionId(pointTransactionId);
          copyObj.setPointTypeId(pointTypeId);
          copyObj.setTransactionType(transactionType);
          copyObj.setCustomerId(customerId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setLocationId(locationId);
          copyObj.setPointBefore(pointBefore);
          copyObj.setPointChanges(pointChanges);
          copyObj.setDescription(description);
          copyObj.setCreateDate(createDate);
          copyObj.setUserName(userName);
  
                    copyObj.setPointTransactionId((String)null);
                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PointTransactionPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PointTransaction\n");
        str.append("----------------\n")
           .append("PointTransactionId   : ")
           .append(getPointTransactionId())
           .append("\n")
           .append("PointTypeId          : ")
           .append(getPointTypeId())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("PointBefore          : ")
           .append(getPointBefore())
           .append("\n")
           .append("PointChanges         : ")
           .append(getPointChanges())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
        ;
        return(str.toString());
    }
}
