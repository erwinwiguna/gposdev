package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PointType
 */
public abstract class BasePointType extends BaseObject
{
    /** The Peer class */
    private static final PointTypePeer peer =
        new PointTypePeer();

        
    /** The value for the pointTypeId field */
    private String pointTypeId;
      
    /** The value for the code field */
    private String code;
      
    /** The value for the description field */
    private String description;
                                                                
    /** The value for the forSi field */
    private boolean forSi = false;
                                                                
    /** The value for the forSr field */
    private boolean forSr = false;
  
    
    /**
     * Get the PointTypeId
     *
     * @return String
     */
    public String getPointTypeId()
    {
        return pointTypeId;
    }

                        
    /**
     * Set the value of PointTypeId
     *
     * @param v new value
     */
    public void setPointTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointTypeId, v))
              {
            this.pointTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Code
     *
     * @return String
     */
    public String getCode()
    {
        return code;
    }

                        
    /**
     * Set the value of Code
     *
     * @param v new value
     */
    public void setCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.code, v))
              {
            this.code = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ForSi
     *
     * @return boolean
     */
    public boolean getForSi()
    {
        return forSi;
    }

                        
    /**
     * Set the value of ForSi
     *
     * @param v new value
     */
    public void setForSi(boolean v) 
    {
    
                  if (this.forSi != v)
              {
            this.forSi = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ForSr
     *
     * @return boolean
     */
    public boolean getForSr()
    {
        return forSr;
    }

                        
    /**
     * Set the value of ForSr
     *
     * @param v new value
     */
    public void setForSr(boolean v) 
    {
    
                  if (this.forSr != v)
              {
            this.forSr = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PointTypeId");
              fieldNames.add("Code");
              fieldNames.add("Description");
              fieldNames.add("ForSi");
              fieldNames.add("ForSr");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PointTypeId"))
        {
                return getPointTypeId();
            }
          if (name.equals("Code"))
        {
                return getCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("ForSi"))
        {
                return Boolean.valueOf(getForSi());
            }
          if (name.equals("ForSr"))
        {
                return Boolean.valueOf(getForSr());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PointTypePeer.POINT_TYPE_ID))
        {
                return getPointTypeId();
            }
          if (name.equals(PointTypePeer.CODE))
        {
                return getCode();
            }
          if (name.equals(PointTypePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PointTypePeer.FOR_SI))
        {
                return Boolean.valueOf(getForSi());
            }
          if (name.equals(PointTypePeer.FOR_SR))
        {
                return Boolean.valueOf(getForSr());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPointTypeId();
            }
              if (pos == 1)
        {
                return getCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return Boolean.valueOf(getForSi());
            }
              if (pos == 4)
        {
                return Boolean.valueOf(getForSr());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PointTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PointTypePeer.doInsert((PointType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PointTypePeer.doUpdate((PointType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pointTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPointTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPointTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPointTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PointType copy() throws TorqueException
    {
        return copyInto(new PointType());
    }
  
    protected PointType copyInto(PointType copyObj) throws TorqueException
    {
          copyObj.setPointTypeId(pointTypeId);
          copyObj.setCode(code);
          copyObj.setDescription(description);
          copyObj.setForSi(forSi);
          copyObj.setForSr(forSr);
  
                    copyObj.setPointTypeId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PointTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PointType\n");
        str.append("---------\n")
           .append("PointTypeId          : ")
           .append(getPointTypeId())
           .append("\n")
           .append("Code                 : ")
           .append(getCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("ForSi                : ")
           .append(getForSi())
           .append("\n")
           .append("ForSr                : ")
           .append(getForSr())
           .append("\n")
        ;
        return(str.toString());
    }
}
