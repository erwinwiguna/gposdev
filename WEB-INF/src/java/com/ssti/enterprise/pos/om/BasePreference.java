package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Preference
 */
public abstract class BasePreference extends BaseObject
{
    /** The Peer class */
    private static final PreferencePeer peer =
        new PreferencePeer();

        
    /** The value for the preferenceId field */
    private String preferenceId;
      
    /** The value for the languageId field */
    private int languageId;
      
    /** The value for the hqHostname field */
    private String hqHostname;
      
    /** The value for the hqIpAddress field */
    private String hqIpAddress;
      
    /** The value for the costingMethod field */
    private int costingMethod;
      
    /** The value for the siteId field */
    private String siteId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the inventoryType field */
    private int inventoryType;
      
    /** The value for the locationFunction field */
    private int locationFunction;
      
    /** The value for the auditLog field */
    private boolean auditLog;
      
    /** The value for the startDate field */
    private Date startDate;
                                                
    /** The value for the cashierPassword field */
    private String cashierPassword = "";
  
    
    /**
     * Get the PreferenceId
     *
     * @return String
     */
    public String getPreferenceId()
    {
        return preferenceId;
    }

                        
    /**
     * Set the value of PreferenceId
     *
     * @param v new value
     */
    public void setPreferenceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.preferenceId, v))
              {
            this.preferenceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LanguageId
     *
     * @return int
     */
    public int getLanguageId()
    {
        return languageId;
    }

                        
    /**
     * Set the value of LanguageId
     *
     * @param v new value
     */
    public void setLanguageId(int v) 
    {
    
                  if (this.languageId != v)
              {
            this.languageId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HqHostname
     *
     * @return String
     */
    public String getHqHostname()
    {
        return hqHostname;
    }

                        
    /**
     * Set the value of HqHostname
     *
     * @param v new value
     */
    public void setHqHostname(String v) 
    {
    
                  if (!ObjectUtils.equals(this.hqHostname, v))
              {
            this.hqHostname = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HqIpAddress
     *
     * @return String
     */
    public String getHqIpAddress()
    {
        return hqIpAddress;
    }

                        
    /**
     * Set the value of HqIpAddress
     *
     * @param v new value
     */
    public void setHqIpAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.hqIpAddress, v))
              {
            this.hqIpAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CostingMethod
     *
     * @return int
     */
    public int getCostingMethod()
    {
        return costingMethod;
    }

                        
    /**
     * Set the value of CostingMethod
     *
     * @param v new value
     */
    public void setCostingMethod(int v) 
    {
    
                  if (this.costingMethod != v)
              {
            this.costingMethod = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SiteId
     *
     * @return String
     */
    public String getSiteId()
    {
        return siteId;
    }

                        
    /**
     * Set the value of SiteId
     *
     * @param v new value
     */
    public void setSiteId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.siteId, v))
              {
            this.siteId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryType
     *
     * @return int
     */
    public int getInventoryType()
    {
        return inventoryType;
    }

                        
    /**
     * Set the value of InventoryType
     *
     * @param v new value
     */
    public void setInventoryType(int v) 
    {
    
                  if (this.inventoryType != v)
              {
            this.inventoryType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationFunction
     *
     * @return int
     */
    public int getLocationFunction()
    {
        return locationFunction;
    }

                        
    /**
     * Set the value of LocationFunction
     *
     * @param v new value
     */
    public void setLocationFunction(int v) 
    {
    
                  if (this.locationFunction != v)
              {
            this.locationFunction = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AuditLog
     *
     * @return boolean
     */
    public boolean getAuditLog()
    {
        return auditLog;
    }

                        
    /**
     * Set the value of AuditLog
     *
     * @param v new value
     */
    public void setAuditLog(boolean v) 
    {
    
                  if (this.auditLog != v)
              {
            this.auditLog = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartDate
     *
     * @return Date
     */
    public Date getStartDate()
    {
        return startDate;
    }

                        
    /**
     * Set the value of StartDate
     *
     * @param v new value
     */
    public void setStartDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.startDate, v))
              {
            this.startDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierPassword
     *
     * @return String
     */
    public String getCashierPassword()
    {
        return cashierPassword;
    }

                        
    /**
     * Set the value of CashierPassword
     *
     * @param v new value
     */
    public void setCashierPassword(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierPassword, v))
              {
            this.cashierPassword = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PreferenceId");
              fieldNames.add("LanguageId");
              fieldNames.add("HqHostname");
              fieldNames.add("HqIpAddress");
              fieldNames.add("CostingMethod");
              fieldNames.add("SiteId");
              fieldNames.add("LocationId");
              fieldNames.add("InventoryType");
              fieldNames.add("LocationFunction");
              fieldNames.add("AuditLog");
              fieldNames.add("StartDate");
              fieldNames.add("CashierPassword");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PreferenceId"))
        {
                return getPreferenceId();
            }
          if (name.equals("LanguageId"))
        {
                return Integer.valueOf(getLanguageId());
            }
          if (name.equals("HqHostname"))
        {
                return getHqHostname();
            }
          if (name.equals("HqIpAddress"))
        {
                return getHqIpAddress();
            }
          if (name.equals("CostingMethod"))
        {
                return Integer.valueOf(getCostingMethod());
            }
          if (name.equals("SiteId"))
        {
                return getSiteId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("InventoryType"))
        {
                return Integer.valueOf(getInventoryType());
            }
          if (name.equals("LocationFunction"))
        {
                return Integer.valueOf(getLocationFunction());
            }
          if (name.equals("AuditLog"))
        {
                return Boolean.valueOf(getAuditLog());
            }
          if (name.equals("StartDate"))
        {
                return getStartDate();
            }
          if (name.equals("CashierPassword"))
        {
                return getCashierPassword();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PreferencePeer.PREFERENCE_ID))
        {
                return getPreferenceId();
            }
          if (name.equals(PreferencePeer.LANGUAGE_ID))
        {
                return Integer.valueOf(getLanguageId());
            }
          if (name.equals(PreferencePeer.HQ_HOSTNAME))
        {
                return getHqHostname();
            }
          if (name.equals(PreferencePeer.HQ_IP_ADDRESS))
        {
                return getHqIpAddress();
            }
          if (name.equals(PreferencePeer.COSTING_METHOD))
        {
                return Integer.valueOf(getCostingMethod());
            }
          if (name.equals(PreferencePeer.SITE_ID))
        {
                return getSiteId();
            }
          if (name.equals(PreferencePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PreferencePeer.INVENTORY_TYPE))
        {
                return Integer.valueOf(getInventoryType());
            }
          if (name.equals(PreferencePeer.LOCATION_FUNCTION))
        {
                return Integer.valueOf(getLocationFunction());
            }
          if (name.equals(PreferencePeer.AUDIT_LOG))
        {
                return Boolean.valueOf(getAuditLog());
            }
          if (name.equals(PreferencePeer.START_DATE))
        {
                return getStartDate();
            }
          if (name.equals(PreferencePeer.CASHIER_PASSWORD))
        {
                return getCashierPassword();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPreferenceId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getLanguageId());
            }
              if (pos == 2)
        {
                return getHqHostname();
            }
              if (pos == 3)
        {
                return getHqIpAddress();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getCostingMethod());
            }
              if (pos == 5)
        {
                return getSiteId();
            }
              if (pos == 6)
        {
                return getLocationId();
            }
              if (pos == 7)
        {
                return Integer.valueOf(getInventoryType());
            }
              if (pos == 8)
        {
                return Integer.valueOf(getLocationFunction());
            }
              if (pos == 9)
        {
                return Boolean.valueOf(getAuditLog());
            }
              if (pos == 10)
        {
                return getStartDate();
            }
              if (pos == 11)
        {
                return getCashierPassword();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PreferencePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PreferencePeer.doInsert((Preference) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PreferencePeer.doUpdate((Preference) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key preferenceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPreferenceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPreferenceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPreferenceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Preference copy() throws TorqueException
    {
        return copyInto(new Preference());
    }
  
    protected Preference copyInto(Preference copyObj) throws TorqueException
    {
          copyObj.setPreferenceId(preferenceId);
          copyObj.setLanguageId(languageId);
          copyObj.setHqHostname(hqHostname);
          copyObj.setHqIpAddress(hqIpAddress);
          copyObj.setCostingMethod(costingMethod);
          copyObj.setSiteId(siteId);
          copyObj.setLocationId(locationId);
          copyObj.setInventoryType(inventoryType);
          copyObj.setLocationFunction(locationFunction);
          copyObj.setAuditLog(auditLog);
          copyObj.setStartDate(startDate);
          copyObj.setCashierPassword(cashierPassword);
  
                    copyObj.setPreferenceId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PreferencePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Preference\n");
        str.append("----------\n")
           .append("PreferenceId         : ")
           .append(getPreferenceId())
           .append("\n")
           .append("LanguageId           : ")
           .append(getLanguageId())
           .append("\n")
           .append("HqHostname           : ")
           .append(getHqHostname())
           .append("\n")
           .append("HqIpAddress          : ")
           .append(getHqIpAddress())
           .append("\n")
           .append("CostingMethod        : ")
           .append(getCostingMethod())
           .append("\n")
           .append("SiteId               : ")
           .append(getSiteId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("InventoryType        : ")
           .append(getInventoryType())
           .append("\n")
           .append("LocationFunction     : ")
           .append(getLocationFunction())
           .append("\n")
           .append("AuditLog             : ")
           .append(getAuditLog())
           .append("\n")
           .append("StartDate            : ")
           .append(getStartDate())
           .append("\n")
           .append("CashierPassword      : ")
           .append(getCashierPassword())
           .append("\n")
        ;
        return(str.toString());
    }
}
