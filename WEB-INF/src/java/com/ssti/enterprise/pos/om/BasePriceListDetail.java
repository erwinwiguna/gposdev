package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PriceListDetail
 */
public abstract class BasePriceListDetail extends BaseObject
{
    /** The Peer class */
    private static final PriceListDetailPeer peer =
        new PriceListDetailPeer();

        
    /** The value for the priceListDetailId field */
    private String priceListDetailId;
      
    /** The value for the priceListId field */
    private String priceListId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
      
    /** The value for the oldPrice field */
    private BigDecimal oldPrice;
      
    /** The value for the newPrice field */
    private BigDecimal newPrice;
                                                
    /** The value for the discountAmount field */
    private String discountAmount = "0";
  
    
    /**
     * Get the PriceListDetailId
     *
     * @return String
     */
    public String getPriceListDetailId()
    {
        return priceListDetailId;
    }

                        
    /**
     * Set the value of PriceListDetailId
     *
     * @param v new value
     */
    public void setPriceListDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.priceListDetailId, v))
              {
            this.priceListDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PriceListId
     *
     * @return String
     */
    public String getPriceListId()
    {
        return priceListId;
    }

                              
    /**
     * Set the value of PriceListId
     *
     * @param v new value
     */
    public void setPriceListId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.priceListId, v))
              {
            this.priceListId = v;
            setModified(true);
        }
    
                          
                if (aPriceList != null && !ObjectUtils.equals(aPriceList.getPriceListId(), v))
                {
            aPriceList = null;
        }
      
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OldPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getOldPrice()
    {
        return oldPrice;
    }

                        
    /**
     * Set the value of OldPrice
     *
     * @param v new value
     */
    public void setOldPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.oldPrice, v))
              {
            this.oldPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NewPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getNewPrice()
    {
        return newPrice;
    }

                        
    /**
     * Set the value of NewPrice
     *
     * @param v new value
     */
    public void setNewPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.newPrice, v))
              {
            this.newPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAmount
     *
     * @return String
     */
    public String getDiscountAmount()
    {
        return discountAmount;
    }

                        
    /**
     * Set the value of DiscountAmount
     *
     * @param v new value
     */
    public void setDiscountAmount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAmount, v))
              {
            this.discountAmount = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PriceList aPriceList;

    /**
     * Declares an association between this object and a PriceList object
     *
     * @param v PriceList
     * @throws TorqueException
     */
    public void setPriceList(PriceList v) throws TorqueException
    {
            if (v == null)
        {
                  setPriceListId((String) null);
              }
        else
        {
            setPriceListId(v.getPriceListId());
        }
            aPriceList = v;
    }

                                            
    /**
     * Get the associated PriceList object
     *
     * @return the associated PriceList object
     * @throws TorqueException
     */
    public PriceList getPriceList() throws TorqueException
    {
        if (aPriceList == null && (!ObjectUtils.equals(this.priceListId, null)))
        {
                          aPriceList = PriceListPeer.retrieveByPK(SimpleKey.keyFor(this.priceListId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PriceList obj = PriceListPeer.retrieveByPK(this.priceListId);
               obj.addPriceListDetails(this);
            */
        }
        return aPriceList;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPriceListKey(ObjectKey key) throws TorqueException
    {
      
                        setPriceListId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PriceListDetailId");
              fieldNames.add("PriceListId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("OldPrice");
              fieldNames.add("NewPrice");
              fieldNames.add("DiscountAmount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PriceListDetailId"))
        {
                return getPriceListDetailId();
            }
          if (name.equals("PriceListId"))
        {
                return getPriceListId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("OldPrice"))
        {
                return getOldPrice();
            }
          if (name.equals("NewPrice"))
        {
                return getNewPrice();
            }
          if (name.equals("DiscountAmount"))
        {
                return getDiscountAmount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PriceListDetailPeer.PRICE_LIST_DETAIL_ID))
        {
                return getPriceListDetailId();
            }
          if (name.equals(PriceListDetailPeer.PRICE_LIST_ID))
        {
                return getPriceListId();
            }
          if (name.equals(PriceListDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PriceListDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PriceListDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(PriceListDetailPeer.OLD_PRICE))
        {
                return getOldPrice();
            }
          if (name.equals(PriceListDetailPeer.NEW_PRICE))
        {
                return getNewPrice();
            }
          if (name.equals(PriceListDetailPeer.DISCOUNT_AMOUNT))
        {
                return getDiscountAmount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPriceListDetailId();
            }
              if (pos == 1)
        {
                return getPriceListId();
            }
              if (pos == 2)
        {
                return getItemId();
            }
              if (pos == 3)
        {
                return getItemCode();
            }
              if (pos == 4)
        {
                return getItemName();
            }
              if (pos == 5)
        {
                return getOldPrice();
            }
              if (pos == 6)
        {
                return getNewPrice();
            }
              if (pos == 7)
        {
                return getDiscountAmount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PriceListDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PriceListDetailPeer.doInsert((PriceListDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PriceListDetailPeer.doUpdate((PriceListDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key priceListDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPriceListDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPriceListDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPriceListDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PriceListDetail copy() throws TorqueException
    {
        return copyInto(new PriceListDetail());
    }
  
    protected PriceListDetail copyInto(PriceListDetail copyObj) throws TorqueException
    {
          copyObj.setPriceListDetailId(priceListDetailId);
          copyObj.setPriceListId(priceListId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setOldPrice(oldPrice);
          copyObj.setNewPrice(newPrice);
          copyObj.setDiscountAmount(discountAmount);
  
                    copyObj.setPriceListDetailId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PriceListDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PriceListDetail\n");
        str.append("---------------\n")
           .append("PriceListDetailId    : ")
           .append(getPriceListDetailId())
           .append("\n")
           .append("PriceListId          : ")
           .append(getPriceListId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("OldPrice             : ")
           .append(getOldPrice())
           .append("\n")
           .append("NewPrice             : ")
           .append(getNewPrice())
           .append("\n")
           .append("DiscountAmount       : ")
           .append(getDiscountAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}
