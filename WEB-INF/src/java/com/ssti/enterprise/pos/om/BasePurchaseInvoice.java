package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseInvoice
 */
public abstract class BasePurchaseInvoice extends BaseObject
{
    /** The Peer class */
    private static final PurchaseInvoicePeer peer =
        new PurchaseInvoicePeer();

        
    /** The value for the purchaseInvoiceId field */
    private String purchaseInvoiceId;
                                          
    /** The value for the paymentStatus field */
    private int paymentStatus = 1;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the vendorInvoiceNo field */
    private String vendorInvoiceNo;
      
    /** The value for the purchaseInvoiceNo field */
    private String purchaseInvoiceNo;
      
    /** The value for the purchaseInvoiceDate field */
    private Date purchaseInvoiceDate;
      
    /** The value for the paymentDueDate field */
    private Date paymentDueDate;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalTax field */
    private BigDecimal totalTax;
      
    /** The value for the courierId field */
    private String courierId;
      
    /** The value for the shippingPrice field */
    private BigDecimal shippingPrice;
      
    /** The value for the totalExpense field */
    private BigDecimal totalExpense;
      
    /** The value for the createBy field */
    private String createBy;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
                                                
          
    /** The value for the paidAmount field */
    private BigDecimal paidAmount= bd_ZERO;
                                          
    /** The value for the status field */
    private int status = 1;
                                                
    /** The value for the bankId field */
    private String bankId = "";
                                                
    /** The value for the bankIssuer field */
    private String bankIssuer = "";
      
    /** The value for the dueDate field */
    private Date dueDate;
                                                
    /** The value for the referenceNo field */
    private String referenceNo = "";
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
      
    /** The value for the freightAsCost field */
    private boolean freightAsCost;
                                                
    /** The value for the freightAccountId field */
    private String freightAccountId = "";
                                                
    /** The value for the freightVendorId field */
    private String freightVendorId = "";
                                                
    /** The value for the freightCurrencyId field */
    private String freightCurrencyId = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the PurchaseInvoiceId
     *
     * @return String
     */
    public String getPurchaseInvoiceId()
    {
        return purchaseInvoiceId;
    }

                                              
    /**
     * Set the value of PurchaseInvoiceId
     *
     * @param v new value
     */
    public void setPurchaseInvoiceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceId, v))
              {
            this.purchaseInvoiceId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PurchaseInvoiceDetail
        if (collPurchaseInvoiceDetails != null)
        {
            for (int i = 0; i < collPurchaseInvoiceDetails.size(); i++)
            {
                ((PurchaseInvoiceDetail) collPurchaseInvoiceDetails.get(i))
                    .setPurchaseInvoiceId(v);
            }
        }
                                                    
                  // update associated PurchaseInvoiceExp
        if (collPurchaseInvoiceExps != null)
        {
            for (int i = 0; i < collPurchaseInvoiceExps.size(); i++)
            {
                ((PurchaseInvoiceExp) collPurchaseInvoiceExps.get(i))
                    .setPurchaseInvoiceId(v);
            }
        }
                                                    
                  // update associated PurchaseInvoiceFreight
        if (collPurchaseInvoiceFreights != null)
        {
            for (int i = 0; i < collPurchaseInvoiceFreights.size(); i++)
            {
                ((PurchaseInvoiceFreight) collPurchaseInvoiceFreights.get(i))
                    .setPurchaseInvoiceId(v);
            }
        }
                                }
  
    /**
     * Get the PaymentStatus
     *
     * @return int
     */
    public int getPaymentStatus()
    {
        return paymentStatus;
    }

                        
    /**
     * Set the value of PaymentStatus
     *
     * @param v new value
     */
    public void setPaymentStatus(int v) 
    {
    
                  if (this.paymentStatus != v)
              {
            this.paymentStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorInvoiceNo
     *
     * @return String
     */
    public String getVendorInvoiceNo()
    {
        return vendorInvoiceNo;
    }

                        
    /**
     * Set the value of VendorInvoiceNo
     *
     * @param v new value
     */
    public void setVendorInvoiceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorInvoiceNo, v))
              {
            this.vendorInvoiceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseInvoiceNo
     *
     * @return String
     */
    public String getPurchaseInvoiceNo()
    {
        return purchaseInvoiceNo;
    }

                        
    /**
     * Set the value of PurchaseInvoiceNo
     *
     * @param v new value
     */
    public void setPurchaseInvoiceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceNo, v))
              {
            this.purchaseInvoiceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseInvoiceDate
     *
     * @return Date
     */
    public Date getPurchaseInvoiceDate()
    {
        return purchaseInvoiceDate;
    }

                        
    /**
     * Set the value of PurchaseInvoiceDate
     *
     * @param v new value
     */
    public void setPurchaseInvoiceDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceDate, v))
              {
            this.purchaseInvoiceDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentDueDate
     *
     * @return Date
     */
    public Date getPaymentDueDate()
    {
        return paymentDueDate;
    }

                        
    /**
     * Set the value of PaymentDueDate
     *
     * @param v new value
     */
    public void setPaymentDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentDueDate, v))
              {
            this.paymentDueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalTax()
    {
        return totalTax;
    }

                        
    /**
     * Set the value of TotalTax
     *
     * @param v new value
     */
    public void setTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalTax, v))
              {
            this.totalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShippingPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getShippingPrice()
    {
        return shippingPrice;
    }

                        
    /**
     * Set the value of ShippingPrice
     *
     * @param v new value
     */
    public void setShippingPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.shippingPrice, v))
              {
            this.shippingPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalExpense
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalExpense()
    {
        return totalExpense;
    }

                        
    /**
     * Set the value of TotalExpense
     *
     * @param v new value
     */
    public void setTotalExpense(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalExpense, v))
              {
            this.totalExpense = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaidAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaidAmount()
    {
        return paidAmount;
    }

                        
    /**
     * Set the value of PaidAmount
     *
     * @param v new value
     */
    public void setPaidAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paidAmount, v))
              {
            this.paidAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAsCost
     *
     * @return boolean
     */
    public boolean getFreightAsCost()
    {
        return freightAsCost;
    }

                        
    /**
     * Set the value of FreightAsCost
     *
     * @param v new value
     */
    public void setFreightAsCost(boolean v) 
    {
    
                  if (this.freightAsCost != v)
              {
            this.freightAsCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAccountId
     *
     * @return String
     */
    public String getFreightAccountId()
    {
        return freightAccountId;
    }

                        
    /**
     * Set the value of FreightAccountId
     *
     * @param v new value
     */
    public void setFreightAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightAccountId, v))
              {
            this.freightAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightVendorId
     *
     * @return String
     */
    public String getFreightVendorId()
    {
        return freightVendorId;
    }

                        
    /**
     * Set the value of FreightVendorId
     *
     * @param v new value
     */
    public void setFreightVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightVendorId, v))
              {
            this.freightVendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightCurrencyId
     *
     * @return String
     */
    public String getFreightCurrencyId()
    {
        return freightCurrencyId;
    }

                        
    /**
     * Set the value of FreightCurrencyId
     *
     * @param v new value
     */
    public void setFreightCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightCurrencyId, v))
              {
            this.freightCurrencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPurchaseInvoiceDetails
     */
    protected List collPurchaseInvoiceDetails;

    /**
     * Temporary storage of collPurchaseInvoiceDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPurchaseInvoiceDetails()
    {
        if (collPurchaseInvoiceDetails == null)
        {
            collPurchaseInvoiceDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PurchaseInvoiceDetail object to this object
     * through the PurchaseInvoiceDetail foreign key attribute
     *
     * @param l PurchaseInvoiceDetail
     * @throws TorqueException
     */
    public void addPurchaseInvoiceDetail(PurchaseInvoiceDetail l) throws TorqueException
    {
        getPurchaseInvoiceDetails().add(l);
        l.setPurchaseInvoice((PurchaseInvoice) this);
    }

    /**
     * The criteria used to select the current contents of collPurchaseInvoiceDetails
     */
    private Criteria lastPurchaseInvoiceDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseInvoiceDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPurchaseInvoiceDetails() throws TorqueException
    {
              if (collPurchaseInvoiceDetails == null)
        {
            collPurchaseInvoiceDetails = getPurchaseInvoiceDetails(new Criteria(10));
        }
        return collPurchaseInvoiceDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceDetails from storage.
     * If this PurchaseInvoice is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPurchaseInvoiceDetails(Criteria criteria) throws TorqueException
    {
              if (collPurchaseInvoiceDetails == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId() );
                        collPurchaseInvoiceDetails = PurchaseInvoiceDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                            if (!lastPurchaseInvoiceDetailsCriteria.equals(criteria))
                {
                    collPurchaseInvoiceDetails = PurchaseInvoiceDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPurchaseInvoiceDetailsCriteria = criteria;

        return collPurchaseInvoiceDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseInvoiceDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseInvoiceDetails(Connection con) throws TorqueException
    {
              if (collPurchaseInvoiceDetails == null)
        {
            collPurchaseInvoiceDetails = getPurchaseInvoiceDetails(new Criteria(10), con);
        }
        return collPurchaseInvoiceDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceDetails from storage.
     * If this PurchaseInvoice is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseInvoiceDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPurchaseInvoiceDetails == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                         collPurchaseInvoiceDetails = PurchaseInvoiceDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                             if (!lastPurchaseInvoiceDetailsCriteria.equals(criteria))
                 {
                     collPurchaseInvoiceDetails = PurchaseInvoiceDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPurchaseInvoiceDetailsCriteria = criteria;

         return collPurchaseInvoiceDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice is new, it will return
     * an empty collection; or if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseInvoice.
     */
    protected List getPurchaseInvoiceDetailsJoinPurchaseInvoice(Criteria criteria)
        throws TorqueException
    {
                    if (collPurchaseInvoiceDetails == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                              collPurchaseInvoiceDetails = PurchaseInvoiceDetailPeer.doSelectJoinPurchaseInvoice(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                                    if (!lastPurchaseInvoiceDetailsCriteria.equals(criteria))
            {
                collPurchaseInvoiceDetails = PurchaseInvoiceDetailPeer.doSelectJoinPurchaseInvoice(criteria);
            }
        }
        lastPurchaseInvoiceDetailsCriteria = criteria;

        return collPurchaseInvoiceDetails;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collPurchaseInvoiceExps
     */
    protected List collPurchaseInvoiceExps;

    /**
     * Temporary storage of collPurchaseInvoiceExps to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPurchaseInvoiceExps()
    {
        if (collPurchaseInvoiceExps == null)
        {
            collPurchaseInvoiceExps = new ArrayList();
        }
    }

    /**
     * Method called to associate a PurchaseInvoiceExp object to this object
     * through the PurchaseInvoiceExp foreign key attribute
     *
     * @param l PurchaseInvoiceExp
     * @throws TorqueException
     */
    public void addPurchaseInvoiceExp(PurchaseInvoiceExp l) throws TorqueException
    {
        getPurchaseInvoiceExps().add(l);
        l.setPurchaseInvoice((PurchaseInvoice) this);
    }

    /**
     * The criteria used to select the current contents of collPurchaseInvoiceExps
     */
    private Criteria lastPurchaseInvoiceExpsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseInvoiceExps(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPurchaseInvoiceExps() throws TorqueException
    {
              if (collPurchaseInvoiceExps == null)
        {
            collPurchaseInvoiceExps = getPurchaseInvoiceExps(new Criteria(10));
        }
        return collPurchaseInvoiceExps;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceExps from storage.
     * If this PurchaseInvoice is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPurchaseInvoiceExps(Criteria criteria) throws TorqueException
    {
              if (collPurchaseInvoiceExps == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceExps = new ArrayList();
            }
            else
            {
                        criteria.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId() );
                        collPurchaseInvoiceExps = PurchaseInvoiceExpPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                            if (!lastPurchaseInvoiceExpsCriteria.equals(criteria))
                {
                    collPurchaseInvoiceExps = PurchaseInvoiceExpPeer.doSelect(criteria);
                }
            }
        }
        lastPurchaseInvoiceExpsCriteria = criteria;

        return collPurchaseInvoiceExps;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseInvoiceExps(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseInvoiceExps(Connection con) throws TorqueException
    {
              if (collPurchaseInvoiceExps == null)
        {
            collPurchaseInvoiceExps = getPurchaseInvoiceExps(new Criteria(10), con);
        }
        return collPurchaseInvoiceExps;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceExps from storage.
     * If this PurchaseInvoice is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseInvoiceExps(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPurchaseInvoiceExps == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceExps = new ArrayList();
            }
            else
            {
                         criteria.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                         collPurchaseInvoiceExps = PurchaseInvoiceExpPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                             if (!lastPurchaseInvoiceExpsCriteria.equals(criteria))
                 {
                     collPurchaseInvoiceExps = PurchaseInvoiceExpPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPurchaseInvoiceExpsCriteria = criteria;

         return collPurchaseInvoiceExps;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice is new, it will return
     * an empty collection; or if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceExps from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseInvoice.
     */
    protected List getPurchaseInvoiceExpsJoinPurchaseInvoice(Criteria criteria)
        throws TorqueException
    {
                    if (collPurchaseInvoiceExps == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceExps = new ArrayList();
            }
            else
            {
                              criteria.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                              collPurchaseInvoiceExps = PurchaseInvoiceExpPeer.doSelectJoinPurchaseInvoice(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                                    if (!lastPurchaseInvoiceExpsCriteria.equals(criteria))
            {
                collPurchaseInvoiceExps = PurchaseInvoiceExpPeer.doSelectJoinPurchaseInvoice(criteria);
            }
        }
        lastPurchaseInvoiceExpsCriteria = criteria;

        return collPurchaseInvoiceExps;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collPurchaseInvoiceFreights
     */
    protected List collPurchaseInvoiceFreights;

    /**
     * Temporary storage of collPurchaseInvoiceFreights to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPurchaseInvoiceFreights()
    {
        if (collPurchaseInvoiceFreights == null)
        {
            collPurchaseInvoiceFreights = new ArrayList();
        }
    }

    /**
     * Method called to associate a PurchaseInvoiceFreight object to this object
     * through the PurchaseInvoiceFreight foreign key attribute
     *
     * @param l PurchaseInvoiceFreight
     * @throws TorqueException
     */
    public void addPurchaseInvoiceFreight(PurchaseInvoiceFreight l) throws TorqueException
    {
        getPurchaseInvoiceFreights().add(l);
        l.setPurchaseInvoice((PurchaseInvoice) this);
    }

    /**
     * The criteria used to select the current contents of collPurchaseInvoiceFreights
     */
    private Criteria lastPurchaseInvoiceFreightsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseInvoiceFreights(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPurchaseInvoiceFreights() throws TorqueException
    {
              if (collPurchaseInvoiceFreights == null)
        {
            collPurchaseInvoiceFreights = getPurchaseInvoiceFreights(new Criteria(10));
        }
        return collPurchaseInvoiceFreights;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceFreights from storage.
     * If this PurchaseInvoice is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPurchaseInvoiceFreights(Criteria criteria) throws TorqueException
    {
              if (collPurchaseInvoiceFreights == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceFreights = new ArrayList();
            }
            else
            {
                        criteria.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId() );
                        collPurchaseInvoiceFreights = PurchaseInvoiceFreightPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                            if (!lastPurchaseInvoiceFreightsCriteria.equals(criteria))
                {
                    collPurchaseInvoiceFreights = PurchaseInvoiceFreightPeer.doSelect(criteria);
                }
            }
        }
        lastPurchaseInvoiceFreightsCriteria = criteria;

        return collPurchaseInvoiceFreights;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseInvoiceFreights(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseInvoiceFreights(Connection con) throws TorqueException
    {
              if (collPurchaseInvoiceFreights == null)
        {
            collPurchaseInvoiceFreights = getPurchaseInvoiceFreights(new Criteria(10), con);
        }
        return collPurchaseInvoiceFreights;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceFreights from storage.
     * If this PurchaseInvoice is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseInvoiceFreights(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPurchaseInvoiceFreights == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceFreights = new ArrayList();
            }
            else
            {
                         criteria.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                         collPurchaseInvoiceFreights = PurchaseInvoiceFreightPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                             if (!lastPurchaseInvoiceFreightsCriteria.equals(criteria))
                 {
                     collPurchaseInvoiceFreights = PurchaseInvoiceFreightPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPurchaseInvoiceFreightsCriteria = criteria;

         return collPurchaseInvoiceFreights;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseInvoice is new, it will return
     * an empty collection; or if this PurchaseInvoice has previously
     * been saved, it will retrieve related PurchaseInvoiceFreights from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseInvoice.
     */
    protected List getPurchaseInvoiceFreightsJoinPurchaseInvoice(Criteria criteria)
        throws TorqueException
    {
                    if (collPurchaseInvoiceFreights == null)
        {
            if (isNew())
            {
               collPurchaseInvoiceFreights = new ArrayList();
            }
            else
            {
                              criteria.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                              collPurchaseInvoiceFreights = PurchaseInvoiceFreightPeer.doSelectJoinPurchaseInvoice(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, getPurchaseInvoiceId());
                                    if (!lastPurchaseInvoiceFreightsCriteria.equals(criteria))
            {
                collPurchaseInvoiceFreights = PurchaseInvoiceFreightPeer.doSelectJoinPurchaseInvoice(criteria);
            }
        }
        lastPurchaseInvoiceFreightsCriteria = criteria;

        return collPurchaseInvoiceFreights;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseInvoiceId");
              fieldNames.add("PaymentStatus");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("VendorInvoiceNo");
              fieldNames.add("PurchaseInvoiceNo");
              fieldNames.add("PurchaseInvoiceDate");
              fieldNames.add("PaymentDueDate");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("TotalQty");
              fieldNames.add("TotalAmount");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalTax");
              fieldNames.add("CourierId");
              fieldNames.add("ShippingPrice");
              fieldNames.add("TotalExpense");
              fieldNames.add("CreateBy");
              fieldNames.add("Remark");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("PaidAmount");
              fieldNames.add("Status");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("DueDate");
              fieldNames.add("ReferenceNo");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("FobId");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("FiscalRate");
              fieldNames.add("FreightAsCost");
              fieldNames.add("FreightAccountId");
              fieldNames.add("FreightVendorId");
              fieldNames.add("FreightCurrencyId");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseInvoiceId"))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals("PaymentStatus"))
        {
                return Integer.valueOf(getPaymentStatus());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("VendorInvoiceNo"))
        {
                return getVendorInvoiceNo();
            }
          if (name.equals("PurchaseInvoiceNo"))
        {
                return getPurchaseInvoiceNo();
            }
          if (name.equals("PurchaseInvoiceDate"))
        {
                return getPurchaseInvoiceDate();
            }
          if (name.equals("PaymentDueDate"))
        {
                return getPaymentDueDate();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalTax"))
        {
                return getTotalTax();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("ShippingPrice"))
        {
                return getShippingPrice();
            }
          if (name.equals("TotalExpense"))
        {
                return getTotalExpense();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("PaidAmount"))
        {
                return getPaidAmount();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("FreightAsCost"))
        {
                return Boolean.valueOf(getFreightAsCost());
            }
          if (name.equals("FreightAccountId"))
        {
                return getFreightAccountId();
            }
          if (name.equals("FreightVendorId"))
        {
                return getFreightVendorId();
            }
          if (name.equals("FreightCurrencyId"))
        {
                return getFreightCurrencyId();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseInvoicePeer.PURCHASE_INVOICE_ID))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals(PurchaseInvoicePeer.PAYMENT_STATUS))
        {
                return Integer.valueOf(getPaymentStatus());
            }
          if (name.equals(PurchaseInvoicePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PurchaseInvoicePeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(PurchaseInvoicePeer.VENDOR_INVOICE_NO))
        {
                return getVendorInvoiceNo();
            }
          if (name.equals(PurchaseInvoicePeer.PURCHASE_INVOICE_NO))
        {
                return getPurchaseInvoiceNo();
            }
          if (name.equals(PurchaseInvoicePeer.PURCHASE_INVOICE_DATE))
        {
                return getPurchaseInvoiceDate();
            }
          if (name.equals(PurchaseInvoicePeer.PAYMENT_DUE_DATE))
        {
                return getPaymentDueDate();
            }
          if (name.equals(PurchaseInvoicePeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(PurchaseInvoicePeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(PurchaseInvoicePeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(PurchaseInvoicePeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(PurchaseInvoicePeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(PurchaseInvoicePeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(PurchaseInvoicePeer.TOTAL_TAX))
        {
                return getTotalTax();
            }
          if (name.equals(PurchaseInvoicePeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(PurchaseInvoicePeer.SHIPPING_PRICE))
        {
                return getShippingPrice();
            }
          if (name.equals(PurchaseInvoicePeer.TOTAL_EXPENSE))
        {
                return getTotalExpense();
            }
          if (name.equals(PurchaseInvoicePeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(PurchaseInvoicePeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PurchaseInvoicePeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(PurchaseInvoicePeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(PurchaseInvoicePeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(PurchaseInvoicePeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(PurchaseInvoicePeer.PAID_AMOUNT))
        {
                return getPaidAmount();
            }
          if (name.equals(PurchaseInvoicePeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(PurchaseInvoicePeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(PurchaseInvoicePeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(PurchaseInvoicePeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(PurchaseInvoicePeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(PurchaseInvoicePeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(PurchaseInvoicePeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(PurchaseInvoicePeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(PurchaseInvoicePeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(PurchaseInvoicePeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(PurchaseInvoicePeer.FREIGHT_AS_COST))
        {
                return Boolean.valueOf(getFreightAsCost());
            }
          if (name.equals(PurchaseInvoicePeer.FREIGHT_ACCOUNT_ID))
        {
                return getFreightAccountId();
            }
          if (name.equals(PurchaseInvoicePeer.FREIGHT_VENDOR_ID))
        {
                return getFreightVendorId();
            }
          if (name.equals(PurchaseInvoicePeer.FREIGHT_CURRENCY_ID))
        {
                return getFreightCurrencyId();
            }
          if (name.equals(PurchaseInvoicePeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(PurchaseInvoicePeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseInvoiceId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getPaymentStatus());
            }
              if (pos == 2)
        {
                return getLocationId();
            }
              if (pos == 3)
        {
                return getLocationName();
            }
              if (pos == 4)
        {
                return getVendorInvoiceNo();
            }
              if (pos == 5)
        {
                return getPurchaseInvoiceNo();
            }
              if (pos == 6)
        {
                return getPurchaseInvoiceDate();
            }
              if (pos == 7)
        {
                return getPaymentDueDate();
            }
              if (pos == 8)
        {
                return getVendorId();
            }
              if (pos == 9)
        {
                return getVendorName();
            }
              if (pos == 10)
        {
                return getTotalQty();
            }
              if (pos == 11)
        {
                return getTotalAmount();
            }
              if (pos == 12)
        {
                return getTotalDiscountPct();
            }
              if (pos == 13)
        {
                return getTotalDiscount();
            }
              if (pos == 14)
        {
                return getTotalTax();
            }
              if (pos == 15)
        {
                return getCourierId();
            }
              if (pos == 16)
        {
                return getShippingPrice();
            }
              if (pos == 17)
        {
                return getTotalExpense();
            }
              if (pos == 18)
        {
                return getCreateBy();
            }
              if (pos == 19)
        {
                return getRemark();
            }
              if (pos == 20)
        {
                return getPaymentTypeId();
            }
              if (pos == 21)
        {
                return getPaymentTermId();
            }
              if (pos == 22)
        {
                return getCurrencyId();
            }
              if (pos == 23)
        {
                return getCurrencyRate();
            }
              if (pos == 24)
        {
                return getPaidAmount();
            }
              if (pos == 25)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 26)
        {
                return getBankId();
            }
              if (pos == 27)
        {
                return getBankIssuer();
            }
              if (pos == 28)
        {
                return getDueDate();
            }
              if (pos == 29)
        {
                return getReferenceNo();
            }
              if (pos == 30)
        {
                return getCashFlowTypeId();
            }
              if (pos == 31)
        {
                return getFobId();
            }
              if (pos == 32)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 33)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 34)
        {
                return getFiscalRate();
            }
              if (pos == 35)
        {
                return Boolean.valueOf(getFreightAsCost());
            }
              if (pos == 36)
        {
                return getFreightAccountId();
            }
              if (pos == 37)
        {
                return getFreightVendorId();
            }
              if (pos == 38)
        {
                return getFreightCurrencyId();
            }
              if (pos == 39)
        {
                return getCancelBy();
            }
              if (pos == 40)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseInvoicePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseInvoicePeer.doInsert((PurchaseInvoice) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseInvoicePeer.doUpdate((PurchaseInvoice) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPurchaseInvoiceDetails != null)
            {
                for (int i = 0; i < collPurchaseInvoiceDetails.size(); i++)
                {
                    ((PurchaseInvoiceDetail) collPurchaseInvoiceDetails.get(i)).save(con);
                }
            }
                                                  
                
                    if (collPurchaseInvoiceExps != null)
            {
                for (int i = 0; i < collPurchaseInvoiceExps.size(); i++)
                {
                    ((PurchaseInvoiceExp) collPurchaseInvoiceExps.get(i)).save(con);
                }
            }
                                                  
                
                    if (collPurchaseInvoiceFreights != null)
            {
                for (int i = 0; i < collPurchaseInvoiceFreights.size(); i++)
                {
                    ((PurchaseInvoiceFreight) collPurchaseInvoiceFreights.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseInvoiceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPurchaseInvoiceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPurchaseInvoiceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseInvoiceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseInvoice copy() throws TorqueException
    {
        return copyInto(new PurchaseInvoice());
    }
  
    protected PurchaseInvoice copyInto(PurchaseInvoice copyObj) throws TorqueException
    {
          copyObj.setPurchaseInvoiceId(purchaseInvoiceId);
          copyObj.setPaymentStatus(paymentStatus);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setVendorInvoiceNo(vendorInvoiceNo);
          copyObj.setPurchaseInvoiceNo(purchaseInvoiceNo);
          copyObj.setPurchaseInvoiceDate(purchaseInvoiceDate);
          copyObj.setPaymentDueDate(paymentDueDate);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setTotalQty(totalQty);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalTax(totalTax);
          copyObj.setCourierId(courierId);
          copyObj.setShippingPrice(shippingPrice);
          copyObj.setTotalExpense(totalExpense);
          copyObj.setCreateBy(createBy);
          copyObj.setRemark(remark);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setPaidAmount(paidAmount);
          copyObj.setStatus(status);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setDueDate(dueDate);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setFobId(fobId);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setFreightAsCost(freightAsCost);
          copyObj.setFreightAccountId(freightAccountId);
          copyObj.setFreightVendorId(freightVendorId);
          copyObj.setFreightCurrencyId(freightCurrencyId);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setPurchaseInvoiceId((String)null);
                                                                                                                                                                                                                                                            
                                      
                            
        List v = getPurchaseInvoiceDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PurchaseInvoiceDetail obj = (PurchaseInvoiceDetail) v.get(i);
            copyObj.addPurchaseInvoiceDetail(obj.copy());
        }
                                                  
                            
        v = getPurchaseInvoiceExps();
        for (int i = 0; i < v.size(); i++)
        {
            PurchaseInvoiceExp obj = (PurchaseInvoiceExp) v.get(i);
            copyObj.addPurchaseInvoiceExp(obj.copy());
        }
                                                  
                            
        v = getPurchaseInvoiceFreights();
        for (int i = 0; i < v.size(); i++)
        {
            PurchaseInvoiceFreight obj = (PurchaseInvoiceFreight) v.get(i);
            copyObj.addPurchaseInvoiceFreight(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseInvoicePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseInvoice\n");
        str.append("---------------\n")
           .append("PurchaseInvoiceId    : ")
           .append(getPurchaseInvoiceId())
           .append("\n")
           .append("PaymentStatus        : ")
           .append(getPaymentStatus())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("VendorInvoiceNo      : ")
           .append(getVendorInvoiceNo())
           .append("\n")
           .append("PurchaseInvoiceNo    : ")
           .append(getPurchaseInvoiceNo())
           .append("\n")
           .append("PurchaseInvoiceDate  : ")
           .append(getPurchaseInvoiceDate())
           .append("\n")
           .append("PaymentDueDate       : ")
           .append(getPaymentDueDate())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalTax             : ")
           .append(getTotalTax())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("ShippingPrice        : ")
           .append(getShippingPrice())
           .append("\n")
           .append("TotalExpense         : ")
           .append(getTotalExpense())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("PaidAmount           : ")
           .append(getPaidAmount())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("FreightAsCost        : ")
           .append(getFreightAsCost())
           .append("\n")
           .append("FreightAccountId     : ")
           .append(getFreightAccountId())
           .append("\n")
           .append("FreightVendorId      : ")
           .append(getFreightVendorId())
           .append("\n")
           .append("FreightCurrencyId    : ")
           .append(getFreightCurrencyId())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
