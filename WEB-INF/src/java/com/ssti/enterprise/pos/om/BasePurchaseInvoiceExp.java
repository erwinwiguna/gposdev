package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseInvoiceExp
 */
public abstract class BasePurchaseInvoiceExp extends BaseObject
{
    /** The Peer class */
    private static final PurchaseInvoiceExpPeer peer =
        new PurchaseInvoiceExpPeer();

        
    /** The value for the purchaseInvoiceExpId field */
    private String purchaseInvoiceExpId;
      
    /** The value for the purchaseInvoiceId field */
    private String purchaseInvoiceId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the projectId field */
    private String projectId;
      
    /** The value for the departmentId field */
    private String departmentId;
  
    
    /**
     * Get the PurchaseInvoiceExpId
     *
     * @return String
     */
    public String getPurchaseInvoiceExpId()
    {
        return purchaseInvoiceExpId;
    }

                        
    /**
     * Set the value of PurchaseInvoiceExpId
     *
     * @param v new value
     */
    public void setPurchaseInvoiceExpId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceExpId, v))
              {
            this.purchaseInvoiceExpId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseInvoiceId
     *
     * @return String
     */
    public String getPurchaseInvoiceId()
    {
        return purchaseInvoiceId;
    }

                              
    /**
     * Set the value of PurchaseInvoiceId
     *
     * @param v new value
     */
    public void setPurchaseInvoiceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceId, v))
              {
            this.purchaseInvoiceId = v;
            setModified(true);
        }
    
                          
                if (aPurchaseInvoice != null && !ObjectUtils.equals(aPurchaseInvoice.getPurchaseInvoiceId(), v))
                {
            aPurchaseInvoice = null;
        }
      
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PurchaseInvoice aPurchaseInvoice;

    /**
     * Declares an association between this object and a PurchaseInvoice object
     *
     * @param v PurchaseInvoice
     * @throws TorqueException
     */
    public void setPurchaseInvoice(PurchaseInvoice v) throws TorqueException
    {
            if (v == null)
        {
                  setPurchaseInvoiceId((String) null);
              }
        else
        {
            setPurchaseInvoiceId(v.getPurchaseInvoiceId());
        }
            aPurchaseInvoice = v;
    }

                                            
    /**
     * Get the associated PurchaseInvoice object
     *
     * @return the associated PurchaseInvoice object
     * @throws TorqueException
     */
    public PurchaseInvoice getPurchaseInvoice() throws TorqueException
    {
        if (aPurchaseInvoice == null && (!ObjectUtils.equals(this.purchaseInvoiceId, null)))
        {
                          aPurchaseInvoice = PurchaseInvoicePeer.retrieveByPK(SimpleKey.keyFor(this.purchaseInvoiceId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PurchaseInvoice obj = PurchaseInvoicePeer.retrieveByPK(this.purchaseInvoiceId);
               obj.addPurchaseInvoiceExps(this);
            */
        }
        return aPurchaseInvoice;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPurchaseInvoiceKey(ObjectKey key) throws TorqueException
    {
      
                        setPurchaseInvoiceId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseInvoiceExpId");
              fieldNames.add("PurchaseInvoiceId");
              fieldNames.add("AccountId");
              fieldNames.add("Description");
              fieldNames.add("Amount");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseInvoiceExpId"))
        {
                return getPurchaseInvoiceExpId();
            }
          if (name.equals("PurchaseInvoiceId"))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_EXP_ID))
        {
                return getPurchaseInvoiceExpId();
            }
          if (name.equals(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals(PurchaseInvoiceExpPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(PurchaseInvoiceExpPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PurchaseInvoiceExpPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(PurchaseInvoiceExpPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(PurchaseInvoiceExpPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseInvoiceExpId();
            }
              if (pos == 1)
        {
                return getPurchaseInvoiceId();
            }
              if (pos == 2)
        {
                return getAccountId();
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return getAmount();
            }
              if (pos == 5)
        {
                return getProjectId();
            }
              if (pos == 6)
        {
                return getDepartmentId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseInvoiceExpPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseInvoiceExpPeer.doInsert((PurchaseInvoiceExp) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseInvoiceExpPeer.doUpdate((PurchaseInvoiceExp) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseInvoiceExpId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPurchaseInvoiceExpId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPurchaseInvoiceExpId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseInvoiceExpId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseInvoiceExp copy() throws TorqueException
    {
        return copyInto(new PurchaseInvoiceExp());
    }
  
    protected PurchaseInvoiceExp copyInto(PurchaseInvoiceExp copyObj) throws TorqueException
    {
          copyObj.setPurchaseInvoiceExpId(purchaseInvoiceExpId);
          copyObj.setPurchaseInvoiceId(purchaseInvoiceId);
          copyObj.setAccountId(accountId);
          copyObj.setDescription(description);
          copyObj.setAmount(amount);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
  
                    copyObj.setPurchaseInvoiceExpId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseInvoiceExpPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseInvoiceExp\n");
        str.append("------------------\n")
           .append("PurchaseInvoiceExpId   : ")
           .append(getPurchaseInvoiceExpId())
           .append("\n")
           .append("PurchaseInvoiceId    : ")
           .append(getPurchaseInvoiceId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
        ;
        return(str.toString());
    }
}
