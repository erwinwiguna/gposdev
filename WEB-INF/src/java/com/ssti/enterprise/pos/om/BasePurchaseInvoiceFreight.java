package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseInvoiceFreight
 */
public abstract class BasePurchaseInvoiceFreight extends BaseObject
{
    /** The Peer class */
    private static final PurchaseInvoiceFreightPeer peer =
        new PurchaseInvoiceFreightPeer();

        
    /** The value for the purchaseInvoiceFreightId field */
    private String purchaseInvoiceFreightId;
      
    /** The value for the purchaseInvoiceId field */
    private String purchaseInvoiceId;
      
    /** The value for the freightPiId field */
    private String freightPiId;
      
    /** The value for the freightPiNo field */
    private String freightPiNo;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the departmentId field */
    private String departmentId;
      
    /** The value for the projectId field */
    private String projectId;
      
    /** The value for the freightAmount field */
    private BigDecimal freightAmount;
  
    
    /**
     * Get the PurchaseInvoiceFreightId
     *
     * @return String
     */
    public String getPurchaseInvoiceFreightId()
    {
        return purchaseInvoiceFreightId;
    }

                        
    /**
     * Set the value of PurchaseInvoiceFreightId
     *
     * @param v new value
     */
    public void setPurchaseInvoiceFreightId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceFreightId, v))
              {
            this.purchaseInvoiceFreightId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseInvoiceId
     *
     * @return String
     */
    public String getPurchaseInvoiceId()
    {
        return purchaseInvoiceId;
    }

                              
    /**
     * Set the value of PurchaseInvoiceId
     *
     * @param v new value
     */
    public void setPurchaseInvoiceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseInvoiceId, v))
              {
            this.purchaseInvoiceId = v;
            setModified(true);
        }
    
                          
                if (aPurchaseInvoice != null && !ObjectUtils.equals(aPurchaseInvoice.getPurchaseInvoiceId(), v))
                {
            aPurchaseInvoice = null;
        }
      
              }
  
    /**
     * Get the FreightPiId
     *
     * @return String
     */
    public String getFreightPiId()
    {
        return freightPiId;
    }

                        
    /**
     * Set the value of FreightPiId
     *
     * @param v new value
     */
    public void setFreightPiId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightPiId, v))
              {
            this.freightPiId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightPiNo
     *
     * @return String
     */
    public String getFreightPiNo()
    {
        return freightPiNo;
    }

                        
    /**
     * Set the value of FreightPiNo
     *
     * @param v new value
     */
    public void setFreightPiNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightPiNo, v))
              {
            this.freightPiNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getFreightAmount()
    {
        return freightAmount;
    }

                        
    /**
     * Set the value of FreightAmount
     *
     * @param v new value
     */
    public void setFreightAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.freightAmount, v))
              {
            this.freightAmount = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PurchaseInvoice aPurchaseInvoice;

    /**
     * Declares an association between this object and a PurchaseInvoice object
     *
     * @param v PurchaseInvoice
     * @throws TorqueException
     */
    public void setPurchaseInvoice(PurchaseInvoice v) throws TorqueException
    {
            if (v == null)
        {
                  setPurchaseInvoiceId((String) null);
              }
        else
        {
            setPurchaseInvoiceId(v.getPurchaseInvoiceId());
        }
            aPurchaseInvoice = v;
    }

                                            
    /**
     * Get the associated PurchaseInvoice object
     *
     * @return the associated PurchaseInvoice object
     * @throws TorqueException
     */
    public PurchaseInvoice getPurchaseInvoice() throws TorqueException
    {
        if (aPurchaseInvoice == null && (!ObjectUtils.equals(this.purchaseInvoiceId, null)))
        {
                          aPurchaseInvoice = PurchaseInvoicePeer.retrieveByPK(SimpleKey.keyFor(this.purchaseInvoiceId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PurchaseInvoice obj = PurchaseInvoicePeer.retrieveByPK(this.purchaseInvoiceId);
               obj.addPurchaseInvoiceFreights(this);
            */
        }
        return aPurchaseInvoice;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPurchaseInvoiceKey(ObjectKey key) throws TorqueException
    {
      
                        setPurchaseInvoiceId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseInvoiceFreightId");
              fieldNames.add("PurchaseInvoiceId");
              fieldNames.add("FreightPiId");
              fieldNames.add("FreightPiNo");
              fieldNames.add("AccountId");
              fieldNames.add("DepartmentId");
              fieldNames.add("ProjectId");
              fieldNames.add("FreightAmount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseInvoiceFreightId"))
        {
                return getPurchaseInvoiceFreightId();
            }
          if (name.equals("PurchaseInvoiceId"))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals("FreightPiId"))
        {
                return getFreightPiId();
            }
          if (name.equals("FreightPiNo"))
        {
                return getFreightPiNo();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("FreightAmount"))
        {
                return getFreightAmount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_FREIGHT_ID))
        {
                return getPurchaseInvoiceFreightId();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID))
        {
                return getPurchaseInvoiceId();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.FREIGHT_PI_ID))
        {
                return getFreightPiId();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.FREIGHT_PI_NO))
        {
                return getFreightPiNo();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(PurchaseInvoiceFreightPeer.FREIGHT_AMOUNT))
        {
                return getFreightAmount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseInvoiceFreightId();
            }
              if (pos == 1)
        {
                return getPurchaseInvoiceId();
            }
              if (pos == 2)
        {
                return getFreightPiId();
            }
              if (pos == 3)
        {
                return getFreightPiNo();
            }
              if (pos == 4)
        {
                return getAccountId();
            }
              if (pos == 5)
        {
                return getDepartmentId();
            }
              if (pos == 6)
        {
                return getProjectId();
            }
              if (pos == 7)
        {
                return getFreightAmount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseInvoiceFreightPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseInvoiceFreightPeer.doInsert((PurchaseInvoiceFreight) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseInvoiceFreightPeer.doUpdate((PurchaseInvoiceFreight) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseInvoiceFreightId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPurchaseInvoiceFreightId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPurchaseInvoiceFreightId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseInvoiceFreightId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseInvoiceFreight copy() throws TorqueException
    {
        return copyInto(new PurchaseInvoiceFreight());
    }
  
    protected PurchaseInvoiceFreight copyInto(PurchaseInvoiceFreight copyObj) throws TorqueException
    {
          copyObj.setPurchaseInvoiceFreightId(purchaseInvoiceFreightId);
          copyObj.setPurchaseInvoiceId(purchaseInvoiceId);
          copyObj.setFreightPiId(freightPiId);
          copyObj.setFreightPiNo(freightPiNo);
          copyObj.setAccountId(accountId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setProjectId(projectId);
          copyObj.setFreightAmount(freightAmount);
  
                    copyObj.setPurchaseInvoiceFreightId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseInvoiceFreightPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseInvoiceFreight\n");
        str.append("----------------------\n")
            .append("PurchaseInvoiceFreightId   : ")
           .append(getPurchaseInvoiceFreightId())
           .append("\n")
           .append("PurchaseInvoiceId    : ")
           .append(getPurchaseInvoiceId())
           .append("\n")
           .append("FreightPiId          : ")
           .append(getFreightPiId())
           .append("\n")
           .append("FreightPiNo          : ")
           .append(getFreightPiNo())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("FreightAmount        : ")
           .append(getFreightAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}
