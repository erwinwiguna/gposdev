package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.PurchaseInvoiceMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BasePurchaseInvoicePeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "purchase_invoice";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PurchaseInvoiceMapBuilder.CLASS_NAME);
    }

      /** the column name for the PURCHASE_INVOICE_ID field */
    public static final String PURCHASE_INVOICE_ID;
      /** the column name for the PAYMENT_STATUS field */
    public static final String PAYMENT_STATUS;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the LOCATION_NAME field */
    public static final String LOCATION_NAME;
      /** the column name for the VENDOR_INVOICE_NO field */
    public static final String VENDOR_INVOICE_NO;
      /** the column name for the PURCHASE_INVOICE_NO field */
    public static final String PURCHASE_INVOICE_NO;
      /** the column name for the PURCHASE_INVOICE_DATE field */
    public static final String PURCHASE_INVOICE_DATE;
      /** the column name for the PAYMENT_DUE_DATE field */
    public static final String PAYMENT_DUE_DATE;
      /** the column name for the VENDOR_ID field */
    public static final String VENDOR_ID;
      /** the column name for the VENDOR_NAME field */
    public static final String VENDOR_NAME;
      /** the column name for the TOTAL_QTY field */
    public static final String TOTAL_QTY;
      /** the column name for the TOTAL_AMOUNT field */
    public static final String TOTAL_AMOUNT;
      /** the column name for the TOTAL_DISCOUNT_PCT field */
    public static final String TOTAL_DISCOUNT_PCT;
      /** the column name for the TOTAL_DISCOUNT field */
    public static final String TOTAL_DISCOUNT;
      /** the column name for the TOTAL_TAX field */
    public static final String TOTAL_TAX;
      /** the column name for the COURIER_ID field */
    public static final String COURIER_ID;
      /** the column name for the SHIPPING_PRICE field */
    public static final String SHIPPING_PRICE;
      /** the column name for the TOTAL_EXPENSE field */
    public static final String TOTAL_EXPENSE;
      /** the column name for the CREATE_BY field */
    public static final String CREATE_BY;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the PAYMENT_TYPE_ID field */
    public static final String PAYMENT_TYPE_ID;
      /** the column name for the PAYMENT_TERM_ID field */
    public static final String PAYMENT_TERM_ID;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the PAID_AMOUNT field */
    public static final String PAID_AMOUNT;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the BANK_ID field */
    public static final String BANK_ID;
      /** the column name for the BANK_ISSUER field */
    public static final String BANK_ISSUER;
      /** the column name for the DUE_DATE field */
    public static final String DUE_DATE;
      /** the column name for the REFERENCE_NO field */
    public static final String REFERENCE_NO;
      /** the column name for the CASH_FLOW_TYPE_ID field */
    public static final String CASH_FLOW_TYPE_ID;
      /** the column name for the FOB_ID field */
    public static final String FOB_ID;
      /** the column name for the IS_TAXABLE field */
    public static final String IS_TAXABLE;
      /** the column name for the IS_INCLUSIVE_TAX field */
    public static final String IS_INCLUSIVE_TAX;
      /** the column name for the FISCAL_RATE field */
    public static final String FISCAL_RATE;
      /** the column name for the FREIGHT_AS_COST field */
    public static final String FREIGHT_AS_COST;
      /** the column name for the FREIGHT_ACCOUNT_ID field */
    public static final String FREIGHT_ACCOUNT_ID;
      /** the column name for the FREIGHT_VENDOR_ID field */
    public static final String FREIGHT_VENDOR_ID;
      /** the column name for the FREIGHT_CURRENCY_ID field */
    public static final String FREIGHT_CURRENCY_ID;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
  
    static
    {
          PURCHASE_INVOICE_ID = "purchase_invoice.PURCHASE_INVOICE_ID";
          PAYMENT_STATUS = "purchase_invoice.PAYMENT_STATUS";
          LOCATION_ID = "purchase_invoice.LOCATION_ID";
          LOCATION_NAME = "purchase_invoice.LOCATION_NAME";
          VENDOR_INVOICE_NO = "purchase_invoice.VENDOR_INVOICE_NO";
          PURCHASE_INVOICE_NO = "purchase_invoice.PURCHASE_INVOICE_NO";
          PURCHASE_INVOICE_DATE = "purchase_invoice.PURCHASE_INVOICE_DATE";
          PAYMENT_DUE_DATE = "purchase_invoice.PAYMENT_DUE_DATE";
          VENDOR_ID = "purchase_invoice.VENDOR_ID";
          VENDOR_NAME = "purchase_invoice.VENDOR_NAME";
          TOTAL_QTY = "purchase_invoice.TOTAL_QTY";
          TOTAL_AMOUNT = "purchase_invoice.TOTAL_AMOUNT";
          TOTAL_DISCOUNT_PCT = "purchase_invoice.TOTAL_DISCOUNT_PCT";
          TOTAL_DISCOUNT = "purchase_invoice.TOTAL_DISCOUNT";
          TOTAL_TAX = "purchase_invoice.TOTAL_TAX";
          COURIER_ID = "purchase_invoice.COURIER_ID";
          SHIPPING_PRICE = "purchase_invoice.SHIPPING_PRICE";
          TOTAL_EXPENSE = "purchase_invoice.TOTAL_EXPENSE";
          CREATE_BY = "purchase_invoice.CREATE_BY";
          REMARK = "purchase_invoice.REMARK";
          PAYMENT_TYPE_ID = "purchase_invoice.PAYMENT_TYPE_ID";
          PAYMENT_TERM_ID = "purchase_invoice.PAYMENT_TERM_ID";
          CURRENCY_ID = "purchase_invoice.CURRENCY_ID";
          CURRENCY_RATE = "purchase_invoice.CURRENCY_RATE";
          PAID_AMOUNT = "purchase_invoice.PAID_AMOUNT";
          STATUS = "purchase_invoice.STATUS";
          BANK_ID = "purchase_invoice.BANK_ID";
          BANK_ISSUER = "purchase_invoice.BANK_ISSUER";
          DUE_DATE = "purchase_invoice.DUE_DATE";
          REFERENCE_NO = "purchase_invoice.REFERENCE_NO";
          CASH_FLOW_TYPE_ID = "purchase_invoice.CASH_FLOW_TYPE_ID";
          FOB_ID = "purchase_invoice.FOB_ID";
          IS_TAXABLE = "purchase_invoice.IS_TAXABLE";
          IS_INCLUSIVE_TAX = "purchase_invoice.IS_INCLUSIVE_TAX";
          FISCAL_RATE = "purchase_invoice.FISCAL_RATE";
          FREIGHT_AS_COST = "purchase_invoice.FREIGHT_AS_COST";
          FREIGHT_ACCOUNT_ID = "purchase_invoice.FREIGHT_ACCOUNT_ID";
          FREIGHT_VENDOR_ID = "purchase_invoice.FREIGHT_VENDOR_ID";
          FREIGHT_CURRENCY_ID = "purchase_invoice.FREIGHT_CURRENCY_ID";
          CANCEL_BY = "purchase_invoice.CANCEL_BY";
          CANCEL_DATE = "purchase_invoice.CANCEL_DATE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PurchaseInvoiceMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PurchaseInvoiceMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  41;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.PurchaseInvoice";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseInvoicePeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(FREIGHT_AS_COST))
        {
            Object possibleBoolean = criteria.get(FREIGHT_AS_COST);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(FREIGHT_AS_COST, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PURCHASE_INVOICE_ID);
          criteria.addSelectColumn(PAYMENT_STATUS);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(LOCATION_NAME);
          criteria.addSelectColumn(VENDOR_INVOICE_NO);
          criteria.addSelectColumn(PURCHASE_INVOICE_NO);
          criteria.addSelectColumn(PURCHASE_INVOICE_DATE);
          criteria.addSelectColumn(PAYMENT_DUE_DATE);
          criteria.addSelectColumn(VENDOR_ID);
          criteria.addSelectColumn(VENDOR_NAME);
          criteria.addSelectColumn(TOTAL_QTY);
          criteria.addSelectColumn(TOTAL_AMOUNT);
          criteria.addSelectColumn(TOTAL_DISCOUNT_PCT);
          criteria.addSelectColumn(TOTAL_DISCOUNT);
          criteria.addSelectColumn(TOTAL_TAX);
          criteria.addSelectColumn(COURIER_ID);
          criteria.addSelectColumn(SHIPPING_PRICE);
          criteria.addSelectColumn(TOTAL_EXPENSE);
          criteria.addSelectColumn(CREATE_BY);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(PAYMENT_TYPE_ID);
          criteria.addSelectColumn(PAYMENT_TERM_ID);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(PAID_AMOUNT);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(BANK_ID);
          criteria.addSelectColumn(BANK_ISSUER);
          criteria.addSelectColumn(DUE_DATE);
          criteria.addSelectColumn(REFERENCE_NO);
          criteria.addSelectColumn(CASH_FLOW_TYPE_ID);
          criteria.addSelectColumn(FOB_ID);
          criteria.addSelectColumn(IS_TAXABLE);
          criteria.addSelectColumn(IS_INCLUSIVE_TAX);
          criteria.addSelectColumn(FISCAL_RATE);
          criteria.addSelectColumn(FREIGHT_AS_COST);
          criteria.addSelectColumn(FREIGHT_ACCOUNT_ID);
          criteria.addSelectColumn(FREIGHT_VENDOR_ID);
          criteria.addSelectColumn(FREIGHT_CURRENCY_ID);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static PurchaseInvoice row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            PurchaseInvoice obj = (PurchaseInvoice) cls.newInstance();
            PurchaseInvoicePeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      PurchaseInvoice obj)
        throws TorqueException
    {
        try
        {
                obj.setPurchaseInvoiceId(row.getValue(offset + 0).asString());
                  obj.setPaymentStatus(row.getValue(offset + 1).asInt());
                  obj.setLocationId(row.getValue(offset + 2).asString());
                  obj.setLocationName(row.getValue(offset + 3).asString());
                  obj.setVendorInvoiceNo(row.getValue(offset + 4).asString());
                  obj.setPurchaseInvoiceNo(row.getValue(offset + 5).asString());
                  obj.setPurchaseInvoiceDate(row.getValue(offset + 6).asUtilDate());
                  obj.setPaymentDueDate(row.getValue(offset + 7).asUtilDate());
                  obj.setVendorId(row.getValue(offset + 8).asString());
                  obj.setVendorName(row.getValue(offset + 9).asString());
                  obj.setTotalQty(row.getValue(offset + 10).asBigDecimal());
                  obj.setTotalAmount(row.getValue(offset + 11).asBigDecimal());
                  obj.setTotalDiscountPct(row.getValue(offset + 12).asString());
                  obj.setTotalDiscount(row.getValue(offset + 13).asBigDecimal());
                  obj.setTotalTax(row.getValue(offset + 14).asBigDecimal());
                  obj.setCourierId(row.getValue(offset + 15).asString());
                  obj.setShippingPrice(row.getValue(offset + 16).asBigDecimal());
                  obj.setTotalExpense(row.getValue(offset + 17).asBigDecimal());
                  obj.setCreateBy(row.getValue(offset + 18).asString());
                  obj.setRemark(row.getValue(offset + 19).asString());
                  obj.setPaymentTypeId(row.getValue(offset + 20).asString());
                  obj.setPaymentTermId(row.getValue(offset + 21).asString());
                  obj.setCurrencyId(row.getValue(offset + 22).asString());
                  obj.setCurrencyRate(row.getValue(offset + 23).asBigDecimal());
                  obj.setPaidAmount(row.getValue(offset + 24).asBigDecimal());
                  obj.setStatus(row.getValue(offset + 25).asInt());
                  obj.setBankId(row.getValue(offset + 26).asString());
                  obj.setBankIssuer(row.getValue(offset + 27).asString());
                  obj.setDueDate(row.getValue(offset + 28).asUtilDate());
                  obj.setReferenceNo(row.getValue(offset + 29).asString());
                  obj.setCashFlowTypeId(row.getValue(offset + 30).asString());
                  obj.setFobId(row.getValue(offset + 31).asString());
                  obj.setIsTaxable(row.getValue(offset + 32).asBoolean());
                  obj.setIsInclusiveTax(row.getValue(offset + 33).asBoolean());
                  obj.setFiscalRate(row.getValue(offset + 34).asBigDecimal());
                  obj.setFreightAsCost(row.getValue(offset + 35).asBoolean());
                  obj.setFreightAccountId(row.getValue(offset + 36).asString());
                  obj.setFreightVendorId(row.getValue(offset + 37).asString());
                  obj.setFreightCurrencyId(row.getValue(offset + 38).asString());
                  obj.setCancelBy(row.getValue(offset + 39).asString());
                  obj.setCancelDate(row.getValue(offset + 40).asUtilDate());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseInvoicePeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(FREIGHT_AS_COST))
        {
            Object possibleBoolean = criteria.get(FREIGHT_AS_COST);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(FREIGHT_AS_COST, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PurchaseInvoicePeer.row2Object(row, 1,
                PurchaseInvoicePeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePurchaseInvoicePeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PURCHASE_INVOICE_ID, criteria.remove(PURCHASE_INVOICE_ID));
                                                                                                                                                                                                                                                                                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(FREIGHT_AS_COST))
        {
            Object possibleBoolean = criteria.get(FREIGHT_AS_COST);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(FREIGHT_AS_COST, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PurchaseInvoicePeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(FREIGHT_AS_COST))
        {
            Object possibleBoolean = criteria.get(FREIGHT_AS_COST);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(FREIGHT_AS_COST, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(PurchaseInvoice obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseInvoice obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseInvoice obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseInvoice obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(PurchaseInvoice) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseInvoice obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(PurchaseInvoice) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseInvoice obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(PurchaseInvoice) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseInvoice obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePurchaseInvoicePeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PURCHASE_INVOICE_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( PurchaseInvoice obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PURCHASE_INVOICE_ID, obj.getPurchaseInvoiceId());
              criteria.add(PAYMENT_STATUS, obj.getPaymentStatus());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(LOCATION_NAME, obj.getLocationName());
              criteria.add(VENDOR_INVOICE_NO, obj.getVendorInvoiceNo());
              criteria.add(PURCHASE_INVOICE_NO, obj.getPurchaseInvoiceNo());
              criteria.add(PURCHASE_INVOICE_DATE, obj.getPurchaseInvoiceDate());
              criteria.add(PAYMENT_DUE_DATE, obj.getPaymentDueDate());
              criteria.add(VENDOR_ID, obj.getVendorId());
              criteria.add(VENDOR_NAME, obj.getVendorName());
              criteria.add(TOTAL_QTY, obj.getTotalQty());
              criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
              criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
              criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
              criteria.add(TOTAL_TAX, obj.getTotalTax());
              criteria.add(COURIER_ID, obj.getCourierId());
              criteria.add(SHIPPING_PRICE, obj.getShippingPrice());
              criteria.add(TOTAL_EXPENSE, obj.getTotalExpense());
              criteria.add(CREATE_BY, obj.getCreateBy());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
              criteria.add(PAYMENT_TERM_ID, obj.getPaymentTermId());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(PAID_AMOUNT, obj.getPaidAmount());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(BANK_ID, obj.getBankId());
              criteria.add(BANK_ISSUER, obj.getBankIssuer());
              criteria.add(DUE_DATE, obj.getDueDate());
              criteria.add(REFERENCE_NO, obj.getReferenceNo());
              criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
              criteria.add(FOB_ID, obj.getFobId());
              criteria.add(IS_TAXABLE, obj.getIsTaxable());
              criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
              criteria.add(FISCAL_RATE, obj.getFiscalRate());
              criteria.add(FREIGHT_AS_COST, obj.getFreightAsCost());
              criteria.add(FREIGHT_ACCOUNT_ID, obj.getFreightAccountId());
              criteria.add(FREIGHT_VENDOR_ID, obj.getFreightVendorId());
              criteria.add(FREIGHT_CURRENCY_ID, obj.getFreightCurrencyId());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( PurchaseInvoice obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PURCHASE_INVOICE_ID, obj.getPurchaseInvoiceId());
                          criteria.add(PAYMENT_STATUS, obj.getPaymentStatus());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(LOCATION_NAME, obj.getLocationName());
                          criteria.add(VENDOR_INVOICE_NO, obj.getVendorInvoiceNo());
                          criteria.add(PURCHASE_INVOICE_NO, obj.getPurchaseInvoiceNo());
                          criteria.add(PURCHASE_INVOICE_DATE, obj.getPurchaseInvoiceDate());
                          criteria.add(PAYMENT_DUE_DATE, obj.getPaymentDueDate());
                          criteria.add(VENDOR_ID, obj.getVendorId());
                          criteria.add(VENDOR_NAME, obj.getVendorName());
                          criteria.add(TOTAL_QTY, obj.getTotalQty());
                          criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
                          criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
                          criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
                          criteria.add(TOTAL_TAX, obj.getTotalTax());
                          criteria.add(COURIER_ID, obj.getCourierId());
                          criteria.add(SHIPPING_PRICE, obj.getShippingPrice());
                          criteria.add(TOTAL_EXPENSE, obj.getTotalExpense());
                          criteria.add(CREATE_BY, obj.getCreateBy());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
                          criteria.add(PAYMENT_TERM_ID, obj.getPaymentTermId());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(PAID_AMOUNT, obj.getPaidAmount());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(BANK_ID, obj.getBankId());
                          criteria.add(BANK_ISSUER, obj.getBankIssuer());
                          criteria.add(DUE_DATE, obj.getDueDate());
                          criteria.add(REFERENCE_NO, obj.getReferenceNo());
                          criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
                          criteria.add(FOB_ID, obj.getFobId());
                          criteria.add(IS_TAXABLE, obj.getIsTaxable());
                          criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
                          criteria.add(FISCAL_RATE, obj.getFiscalRate());
                          criteria.add(FREIGHT_AS_COST, obj.getFreightAsCost());
                          criteria.add(FREIGHT_ACCOUNT_ID, obj.getFreightAccountId());
                          criteria.add(FREIGHT_VENDOR_ID, obj.getFreightVendorId());
                          criteria.add(FREIGHT_CURRENCY_ID, obj.getFreightCurrencyId());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseInvoice retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseInvoice retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseInvoice retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        PurchaseInvoice retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseInvoice retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (PurchaseInvoice)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PURCHASE_INVOICE_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
