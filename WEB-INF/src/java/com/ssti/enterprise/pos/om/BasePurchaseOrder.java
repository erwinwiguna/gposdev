package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseOrder
 */
public abstract class BasePurchaseOrder extends BaseObject
{
    /** The Peer class */
    private static final PurchaseOrderPeer peer =
        new PurchaseOrderPeer();

        
    /** The value for the purchaseOrderId field */
    private String purchaseOrderId;
                                                
    /** The value for the purchaseRequestId field */
    private String purchaseRequestId = "";
      
    /** The value for the transactionStatus field */
    private int transactionStatus;
      
    /** The value for the purchaseOrderNo field */
    private String purchaseOrderNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalFee field */
    private BigDecimal totalFee;
      
    /** The value for the totalTax field */
    private BigDecimal totalTax;
      
    /** The value for the createBy field */
    private String createBy;
                                                
    /** The value for the confirmBy field */
    private String confirmBy = "";
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
      
    /** The value for the createReceipt field */
    private boolean createReceipt;
      
    /** The value for the locationId field */
    private String locationId;
                                          
    /** The value for the printTimes field */
    private int printTimes = 0;
                                          
    /** The value for the sentStatus field */
    private int sentStatus = 0;
                                                
          
    /** The value for the downPayment field */
    private BigDecimal downPayment= bd_ZERO;
                                                
    /** The value for the bankId field */
    private String bankId = "";
                                                
    /** The value for the bankIssuer field */
    private String bankIssuer = "";
                                                
    /** The value for the dpAccountId field */
    private String dpAccountId = "";
      
    /** The value for the dpDueDate field */
    private Date dpDueDate;
                                                
    /** The value for the referenceNo field */
    private String referenceNo = "";
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
    /** The value for the courierId field */
    private String courierId = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
                                                
          
    /** The value for the estimatedFreight field */
    private BigDecimal estimatedFreight= bd_ZERO;
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
      
    /** The value for the confirmDate field */
    private Date confirmDate;
      
    /** The value for the deliveryDate field */
    private Date deliveryDate;
                                                
    /** The value for the shipTo field */
    private String shipTo = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
      
    /** The value for the closedDate field */
    private Date closedDate;
      
    /** The value for the expiredDate field */
    private Date expiredDate;
      
    /** The value for the receiptDate field */
    private Date receiptDate;
  
    
    /**
     * Get the PurchaseOrderId
     *
     * @return String
     */
    public String getPurchaseOrderId()
    {
        return purchaseOrderId;
    }

                                              
    /**
     * Set the value of PurchaseOrderId
     *
     * @param v new value
     */
    public void setPurchaseOrderId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseOrderId, v))
              {
            this.purchaseOrderId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PurchaseOrderDetail
        if (collPurchaseOrderDetails != null)
        {
            for (int i = 0; i < collPurchaseOrderDetails.size(); i++)
            {
                ((PurchaseOrderDetail) collPurchaseOrderDetails.get(i))
                    .setPurchaseOrderId(v);
            }
        }
                                }
  
    /**
     * Get the PurchaseRequestId
     *
     * @return String
     */
    public String getPurchaseRequestId()
    {
        return purchaseRequestId;
    }

                        
    /**
     * Set the value of PurchaseRequestId
     *
     * @param v new value
     */
    public void setPurchaseRequestId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseRequestId, v))
              {
            this.purchaseRequestId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionStatus
     *
     * @return int
     */
    public int getTransactionStatus()
    {
        return transactionStatus;
    }

                        
    /**
     * Set the value of TransactionStatus
     *
     * @param v new value
     */
    public void setTransactionStatus(int v) 
    {
    
                  if (this.transactionStatus != v)
              {
            this.transactionStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseOrderNo
     *
     * @return String
     */
    public String getPurchaseOrderNo()
    {
        return purchaseOrderNo;
    }

                        
    /**
     * Set the value of PurchaseOrderNo
     *
     * @param v new value
     */
    public void setPurchaseOrderNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseOrderNo, v))
              {
            this.purchaseOrderNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalFee
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalFee()
    {
        return totalFee;
    }

                        
    /**
     * Set the value of TotalFee
     *
     * @param v new value
     */
    public void setTotalFee(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalFee, v))
              {
            this.totalFee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalTax()
    {
        return totalTax;
    }

                        
    /**
     * Set the value of TotalTax
     *
     * @param v new value
     */
    public void setTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalTax, v))
              {
            this.totalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmBy
     *
     * @return String
     */
    public String getConfirmBy()
    {
        return confirmBy;
    }

                        
    /**
     * Set the value of ConfirmBy
     *
     * @param v new value
     */
    public void setConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmBy, v))
              {
            this.confirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateReceipt
     *
     * @return boolean
     */
    public boolean getCreateReceipt()
    {
        return createReceipt;
    }

                        
    /**
     * Set the value of CreateReceipt
     *
     * @param v new value
     */
    public void setCreateReceipt(boolean v) 
    {
    
                  if (this.createReceipt != v)
              {
            this.createReceipt = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrintTimes
     *
     * @return int
     */
    public int getPrintTimes()
    {
        return printTimes;
    }

                        
    /**
     * Set the value of PrintTimes
     *
     * @param v new value
     */
    public void setPrintTimes(int v) 
    {
    
                  if (this.printTimes != v)
              {
            this.printTimes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SentStatus
     *
     * @return int
     */
    public int getSentStatus()
    {
        return sentStatus;
    }

                        
    /**
     * Set the value of SentStatus
     *
     * @param v new value
     */
    public void setSentStatus(int v) 
    {
    
                  if (this.sentStatus != v)
              {
            this.sentStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DownPayment
     *
     * @return BigDecimal
     */
    public BigDecimal getDownPayment()
    {
        return downPayment;
    }

                        
    /**
     * Set the value of DownPayment
     *
     * @param v new value
     */
    public void setDownPayment(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.downPayment, v))
              {
            this.downPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DpAccountId
     *
     * @return String
     */
    public String getDpAccountId()
    {
        return dpAccountId;
    }

                        
    /**
     * Set the value of DpAccountId
     *
     * @param v new value
     */
    public void setDpAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.dpAccountId, v))
              {
            this.dpAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DpDueDate
     *
     * @return Date
     */
    public Date getDpDueDate()
    {
        return dpDueDate;
    }

                        
    /**
     * Set the value of DpDueDate
     *
     * @param v new value
     */
    public void setDpDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dpDueDate, v))
              {
            this.dpDueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EstimatedFreight
     *
     * @return BigDecimal
     */
    public BigDecimal getEstimatedFreight()
    {
        return estimatedFreight;
    }

                        
    /**
     * Set the value of EstimatedFreight
     *
     * @param v new value
     */
    public void setEstimatedFreight(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.estimatedFreight, v))
              {
            this.estimatedFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryDate
     *
     * @return Date
     */
    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

                        
    /**
     * Set the value of DeliveryDate
     *
     * @param v new value
     */
    public void setDeliveryDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryDate, v))
              {
            this.deliveryDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipTo
     *
     * @return String
     */
    public String getShipTo()
    {
        return shipTo;
    }

                        
    /**
     * Set the value of ShipTo
     *
     * @param v new value
     */
    public void setShipTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shipTo, v))
              {
            this.shipTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClosedDate
     *
     * @return Date
     */
    public Date getClosedDate()
    {
        return closedDate;
    }

                        
    /**
     * Set the value of ClosedDate
     *
     * @param v new value
     */
    public void setClosedDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.closedDate, v))
              {
            this.closedDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpiredDate
     *
     * @return Date
     */
    public Date getExpiredDate()
    {
        return expiredDate;
    }

                        
    /**
     * Set the value of ExpiredDate
     *
     * @param v new value
     */
    public void setExpiredDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expiredDate, v))
              {
            this.expiredDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReceiptDate
     *
     * @return Date
     */
    public Date getReceiptDate()
    {
        return receiptDate;
    }

                        
    /**
     * Set the value of ReceiptDate
     *
     * @param v new value
     */
    public void setReceiptDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.receiptDate, v))
              {
            this.receiptDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPurchaseOrderDetails
     */
    protected List collPurchaseOrderDetails;

    /**
     * Temporary storage of collPurchaseOrderDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPurchaseOrderDetails()
    {
        if (collPurchaseOrderDetails == null)
        {
            collPurchaseOrderDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PurchaseOrderDetail object to this object
     * through the PurchaseOrderDetail foreign key attribute
     *
     * @param l PurchaseOrderDetail
     * @throws TorqueException
     */
    public void addPurchaseOrderDetail(PurchaseOrderDetail l) throws TorqueException
    {
        getPurchaseOrderDetails().add(l);
        l.setPurchaseOrder((PurchaseOrder) this);
    }

    /**
     * The criteria used to select the current contents of collPurchaseOrderDetails
     */
    private Criteria lastPurchaseOrderDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseOrderDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPurchaseOrderDetails() throws TorqueException
    {
              if (collPurchaseOrderDetails == null)
        {
            collPurchaseOrderDetails = getPurchaseOrderDetails(new Criteria(10));
        }
        return collPurchaseOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseOrder has previously
     * been saved, it will retrieve related PurchaseOrderDetails from storage.
     * If this PurchaseOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPurchaseOrderDetails(Criteria criteria) throws TorqueException
    {
              if (collPurchaseOrderDetails == null)
        {
            if (isNew())
            {
               collPurchaseOrderDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, getPurchaseOrderId() );
                        collPurchaseOrderDetails = PurchaseOrderDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, getPurchaseOrderId());
                            if (!lastPurchaseOrderDetailsCriteria.equals(criteria))
                {
                    collPurchaseOrderDetails = PurchaseOrderDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPurchaseOrderDetailsCriteria = criteria;

        return collPurchaseOrderDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseOrderDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseOrderDetails(Connection con) throws TorqueException
    {
              if (collPurchaseOrderDetails == null)
        {
            collPurchaseOrderDetails = getPurchaseOrderDetails(new Criteria(10), con);
        }
        return collPurchaseOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseOrder has previously
     * been saved, it will retrieve related PurchaseOrderDetails from storage.
     * If this PurchaseOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseOrderDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPurchaseOrderDetails == null)
        {
            if (isNew())
            {
               collPurchaseOrderDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, getPurchaseOrderId());
                         collPurchaseOrderDetails = PurchaseOrderDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, getPurchaseOrderId());
                             if (!lastPurchaseOrderDetailsCriteria.equals(criteria))
                 {
                     collPurchaseOrderDetails = PurchaseOrderDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPurchaseOrderDetailsCriteria = criteria;

         return collPurchaseOrderDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseOrder is new, it will return
     * an empty collection; or if this PurchaseOrder has previously
     * been saved, it will retrieve related PurchaseOrderDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseOrder.
     */
    protected List getPurchaseOrderDetailsJoinPurchaseOrder(Criteria criteria)
        throws TorqueException
    {
                    if (collPurchaseOrderDetails == null)
        {
            if (isNew())
            {
               collPurchaseOrderDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, getPurchaseOrderId());
                              collPurchaseOrderDetails = PurchaseOrderDetailPeer.doSelectJoinPurchaseOrder(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, getPurchaseOrderId());
                                    if (!lastPurchaseOrderDetailsCriteria.equals(criteria))
            {
                collPurchaseOrderDetails = PurchaseOrderDetailPeer.doSelectJoinPurchaseOrder(criteria);
            }
        }
        lastPurchaseOrderDetailsCriteria = criteria;

        return collPurchaseOrderDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseOrderId");
              fieldNames.add("PurchaseRequestId");
              fieldNames.add("TransactionStatus");
              fieldNames.add("PurchaseOrderNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("DueDate");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("TotalQty");
              fieldNames.add("TotalAmount");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalFee");
              fieldNames.add("TotalTax");
              fieldNames.add("CreateBy");
              fieldNames.add("ConfirmBy");
              fieldNames.add("Remark");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("CreateReceipt");
              fieldNames.add("LocationId");
              fieldNames.add("PrintTimes");
              fieldNames.add("SentStatus");
              fieldNames.add("DownPayment");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("DpAccountId");
              fieldNames.add("DpDueDate");
              fieldNames.add("ReferenceNo");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("CourierId");
              fieldNames.add("FobId");
              fieldNames.add("EstimatedFreight");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("FiscalRate");
              fieldNames.add("ConfirmDate");
              fieldNames.add("DeliveryDate");
              fieldNames.add("ShipTo");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("ClosedDate");
              fieldNames.add("ExpiredDate");
              fieldNames.add("ReceiptDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseOrderId"))
        {
                return getPurchaseOrderId();
            }
          if (name.equals("PurchaseRequestId"))
        {
                return getPurchaseRequestId();
            }
          if (name.equals("TransactionStatus"))
        {
                return Integer.valueOf(getTransactionStatus());
            }
          if (name.equals("PurchaseOrderNo"))
        {
                return getPurchaseOrderNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalFee"))
        {
                return getTotalFee();
            }
          if (name.equals("TotalTax"))
        {
                return getTotalTax();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("ConfirmBy"))
        {
                return getConfirmBy();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("CreateReceipt"))
        {
                return Boolean.valueOf(getCreateReceipt());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("PrintTimes"))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals("SentStatus"))
        {
                return Integer.valueOf(getSentStatus());
            }
          if (name.equals("DownPayment"))
        {
                return getDownPayment();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("DpAccountId"))
        {
                return getDpAccountId();
            }
          if (name.equals("DpDueDate"))
        {
                return getDpDueDate();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("EstimatedFreight"))
        {
                return getEstimatedFreight();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("DeliveryDate"))
        {
                return getDeliveryDate();
            }
          if (name.equals("ShipTo"))
        {
                return getShipTo();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("ClosedDate"))
        {
                return getClosedDate();
            }
          if (name.equals("ExpiredDate"))
        {
                return getExpiredDate();
            }
          if (name.equals("ReceiptDate"))
        {
                return getReceiptDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseOrderPeer.PURCHASE_ORDER_ID))
        {
                return getPurchaseOrderId();
            }
          if (name.equals(PurchaseOrderPeer.PURCHASE_REQUEST_ID))
        {
                return getPurchaseRequestId();
            }
          if (name.equals(PurchaseOrderPeer.TRANSACTION_STATUS))
        {
                return Integer.valueOf(getTransactionStatus());
            }
          if (name.equals(PurchaseOrderPeer.PURCHASE_ORDER_NO))
        {
                return getPurchaseOrderNo();
            }
          if (name.equals(PurchaseOrderPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(PurchaseOrderPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(PurchaseOrderPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(PurchaseOrderPeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(PurchaseOrderPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(PurchaseOrderPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(PurchaseOrderPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(PurchaseOrderPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(PurchaseOrderPeer.TOTAL_FEE))
        {
                return getTotalFee();
            }
          if (name.equals(PurchaseOrderPeer.TOTAL_TAX))
        {
                return getTotalTax();
            }
          if (name.equals(PurchaseOrderPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(PurchaseOrderPeer.CONFIRM_BY))
        {
                return getConfirmBy();
            }
          if (name.equals(PurchaseOrderPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PurchaseOrderPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(PurchaseOrderPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(PurchaseOrderPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(PurchaseOrderPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(PurchaseOrderPeer.CREATE_RECEIPT))
        {
                return Boolean.valueOf(getCreateReceipt());
            }
          if (name.equals(PurchaseOrderPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PurchaseOrderPeer.PRINT_TIMES))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals(PurchaseOrderPeer.SENT_STATUS))
        {
                return Integer.valueOf(getSentStatus());
            }
          if (name.equals(PurchaseOrderPeer.DOWN_PAYMENT))
        {
                return getDownPayment();
            }
          if (name.equals(PurchaseOrderPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(PurchaseOrderPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(PurchaseOrderPeer.DP_ACCOUNT_ID))
        {
                return getDpAccountId();
            }
          if (name.equals(PurchaseOrderPeer.DP_DUE_DATE))
        {
                return getDpDueDate();
            }
          if (name.equals(PurchaseOrderPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(PurchaseOrderPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(PurchaseOrderPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(PurchaseOrderPeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(PurchaseOrderPeer.ESTIMATED_FREIGHT))
        {
                return getEstimatedFreight();
            }
          if (name.equals(PurchaseOrderPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(PurchaseOrderPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(PurchaseOrderPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(PurchaseOrderPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(PurchaseOrderPeer.DELIVERY_DATE))
        {
                return getDeliveryDate();
            }
          if (name.equals(PurchaseOrderPeer.SHIP_TO))
        {
                return getShipTo();
            }
          if (name.equals(PurchaseOrderPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(PurchaseOrderPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(PurchaseOrderPeer.CLOSED_DATE))
        {
                return getClosedDate();
            }
          if (name.equals(PurchaseOrderPeer.EXPIRED_DATE))
        {
                return getExpiredDate();
            }
          if (name.equals(PurchaseOrderPeer.RECEIPT_DATE))
        {
                return getReceiptDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseOrderId();
            }
              if (pos == 1)
        {
                return getPurchaseRequestId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getTransactionStatus());
            }
              if (pos == 3)
        {
                return getPurchaseOrderNo();
            }
              if (pos == 4)
        {
                return getTransactionDate();
            }
              if (pos == 5)
        {
                return getDueDate();
            }
              if (pos == 6)
        {
                return getVendorId();
            }
              if (pos == 7)
        {
                return getVendorName();
            }
              if (pos == 8)
        {
                return getTotalQty();
            }
              if (pos == 9)
        {
                return getTotalAmount();
            }
              if (pos == 10)
        {
                return getTotalDiscountPct();
            }
              if (pos == 11)
        {
                return getTotalDiscount();
            }
              if (pos == 12)
        {
                return getTotalFee();
            }
              if (pos == 13)
        {
                return getTotalTax();
            }
              if (pos == 14)
        {
                return getCreateBy();
            }
              if (pos == 15)
        {
                return getConfirmBy();
            }
              if (pos == 16)
        {
                return getRemark();
            }
              if (pos == 17)
        {
                return getPaymentTypeId();
            }
              if (pos == 18)
        {
                return getPaymentTermId();
            }
              if (pos == 19)
        {
                return getCurrencyId();
            }
              if (pos == 20)
        {
                return getCurrencyRate();
            }
              if (pos == 21)
        {
                return Boolean.valueOf(getCreateReceipt());
            }
              if (pos == 22)
        {
                return getLocationId();
            }
              if (pos == 23)
        {
                return Integer.valueOf(getPrintTimes());
            }
              if (pos == 24)
        {
                return Integer.valueOf(getSentStatus());
            }
              if (pos == 25)
        {
                return getDownPayment();
            }
              if (pos == 26)
        {
                return getBankId();
            }
              if (pos == 27)
        {
                return getBankIssuer();
            }
              if (pos == 28)
        {
                return getDpAccountId();
            }
              if (pos == 29)
        {
                return getDpDueDate();
            }
              if (pos == 30)
        {
                return getReferenceNo();
            }
              if (pos == 31)
        {
                return getCashFlowTypeId();
            }
              if (pos == 32)
        {
                return getCourierId();
            }
              if (pos == 33)
        {
                return getFobId();
            }
              if (pos == 34)
        {
                return getEstimatedFreight();
            }
              if (pos == 35)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 36)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 37)
        {
                return getFiscalRate();
            }
              if (pos == 38)
        {
                return getConfirmDate();
            }
              if (pos == 39)
        {
                return getDeliveryDate();
            }
              if (pos == 40)
        {
                return getShipTo();
            }
              if (pos == 41)
        {
                return getCancelBy();
            }
              if (pos == 42)
        {
                return getCancelDate();
            }
              if (pos == 43)
        {
                return getClosedDate();
            }
              if (pos == 44)
        {
                return getExpiredDate();
            }
              if (pos == 45)
        {
                return getReceiptDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseOrderPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseOrderPeer.doInsert((PurchaseOrder) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseOrderPeer.doUpdate((PurchaseOrder) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPurchaseOrderDetails != null)
            {
                for (int i = 0; i < collPurchaseOrderDetails.size(); i++)
                {
                    ((PurchaseOrderDetail) collPurchaseOrderDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseOrderId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPurchaseOrderId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPurchaseOrderId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseOrderId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseOrder copy() throws TorqueException
    {
        return copyInto(new PurchaseOrder());
    }
  
    protected PurchaseOrder copyInto(PurchaseOrder copyObj) throws TorqueException
    {
          copyObj.setPurchaseOrderId(purchaseOrderId);
          copyObj.setPurchaseRequestId(purchaseRequestId);
          copyObj.setTransactionStatus(transactionStatus);
          copyObj.setPurchaseOrderNo(purchaseOrderNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setDueDate(dueDate);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setTotalQty(totalQty);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalFee(totalFee);
          copyObj.setTotalTax(totalTax);
          copyObj.setCreateBy(createBy);
          copyObj.setConfirmBy(confirmBy);
          copyObj.setRemark(remark);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setCreateReceipt(createReceipt);
          copyObj.setLocationId(locationId);
          copyObj.setPrintTimes(printTimes);
          copyObj.setSentStatus(sentStatus);
          copyObj.setDownPayment(downPayment);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setDpAccountId(dpAccountId);
          copyObj.setDpDueDate(dpDueDate);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setCourierId(courierId);
          copyObj.setFobId(fobId);
          copyObj.setEstimatedFreight(estimatedFreight);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setDeliveryDate(deliveryDate);
          copyObj.setShipTo(shipTo);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setClosedDate(closedDate);
          copyObj.setExpiredDate(expiredDate);
          copyObj.setReceiptDate(receiptDate);
  
                    copyObj.setPurchaseOrderId((String)null);
                                                                                                                                                                                                                                                                                          
                                      
                            
        List v = getPurchaseOrderDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PurchaseOrderDetail obj = (PurchaseOrderDetail) v.get(i);
            copyObj.addPurchaseOrderDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseOrderPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseOrder\n");
        str.append("-------------\n")
           .append("PurchaseOrderId      : ")
           .append(getPurchaseOrderId())
           .append("\n")
           .append("PurchaseRequestId    : ")
           .append(getPurchaseRequestId())
           .append("\n")
           .append("TransactionStatus    : ")
           .append(getTransactionStatus())
           .append("\n")
           .append("PurchaseOrderNo      : ")
           .append(getPurchaseOrderNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalFee             : ")
           .append(getTotalFee())
           .append("\n")
           .append("TotalTax             : ")
           .append(getTotalTax())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("ConfirmBy            : ")
           .append(getConfirmBy())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("CreateReceipt        : ")
           .append(getCreateReceipt())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("PrintTimes           : ")
           .append(getPrintTimes())
           .append("\n")
           .append("SentStatus           : ")
           .append(getSentStatus())
           .append("\n")
           .append("DownPayment          : ")
           .append(getDownPayment())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("DpAccountId          : ")
           .append(getDpAccountId())
           .append("\n")
           .append("DpDueDate            : ")
           .append(getDpDueDate())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("EstimatedFreight     : ")
           .append(getEstimatedFreight())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("DeliveryDate         : ")
           .append(getDeliveryDate())
           .append("\n")
           .append("ShipTo               : ")
           .append(getShipTo())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("ClosedDate           : ")
           .append(getClosedDate())
           .append("\n")
           .append("ExpiredDate          : ")
           .append(getExpiredDate())
           .append("\n")
           .append("ReceiptDate          : ")
           .append(getReceiptDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
