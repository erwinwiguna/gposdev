package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.PurchaseOrderMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BasePurchaseOrderPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "purchase_order";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PurchaseOrderMapBuilder.CLASS_NAME);
    }

      /** the column name for the PURCHASE_ORDER_ID field */
    public static final String PURCHASE_ORDER_ID;
      /** the column name for the PURCHASE_REQUEST_ID field */
    public static final String PURCHASE_REQUEST_ID;
      /** the column name for the TRANSACTION_STATUS field */
    public static final String TRANSACTION_STATUS;
      /** the column name for the PURCHASE_ORDER_NO field */
    public static final String PURCHASE_ORDER_NO;
      /** the column name for the TRANSACTION_DATE field */
    public static final String TRANSACTION_DATE;
      /** the column name for the DUE_DATE field */
    public static final String DUE_DATE;
      /** the column name for the VENDOR_ID field */
    public static final String VENDOR_ID;
      /** the column name for the VENDOR_NAME field */
    public static final String VENDOR_NAME;
      /** the column name for the TOTAL_QTY field */
    public static final String TOTAL_QTY;
      /** the column name for the TOTAL_AMOUNT field */
    public static final String TOTAL_AMOUNT;
      /** the column name for the TOTAL_DISCOUNT_PCT field */
    public static final String TOTAL_DISCOUNT_PCT;
      /** the column name for the TOTAL_DISCOUNT field */
    public static final String TOTAL_DISCOUNT;
      /** the column name for the TOTAL_FEE field */
    public static final String TOTAL_FEE;
      /** the column name for the TOTAL_TAX field */
    public static final String TOTAL_TAX;
      /** the column name for the CREATE_BY field */
    public static final String CREATE_BY;
      /** the column name for the CONFIRM_BY field */
    public static final String CONFIRM_BY;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the PAYMENT_TYPE_ID field */
    public static final String PAYMENT_TYPE_ID;
      /** the column name for the PAYMENT_TERM_ID field */
    public static final String PAYMENT_TERM_ID;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the CREATE_RECEIPT field */
    public static final String CREATE_RECEIPT;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the PRINT_TIMES field */
    public static final String PRINT_TIMES;
      /** the column name for the SENT_STATUS field */
    public static final String SENT_STATUS;
      /** the column name for the DOWN_PAYMENT field */
    public static final String DOWN_PAYMENT;
      /** the column name for the BANK_ID field */
    public static final String BANK_ID;
      /** the column name for the BANK_ISSUER field */
    public static final String BANK_ISSUER;
      /** the column name for the DP_ACCOUNT_ID field */
    public static final String DP_ACCOUNT_ID;
      /** the column name for the DP_DUE_DATE field */
    public static final String DP_DUE_DATE;
      /** the column name for the REFERENCE_NO field */
    public static final String REFERENCE_NO;
      /** the column name for the CASH_FLOW_TYPE_ID field */
    public static final String CASH_FLOW_TYPE_ID;
      /** the column name for the COURIER_ID field */
    public static final String COURIER_ID;
      /** the column name for the FOB_ID field */
    public static final String FOB_ID;
      /** the column name for the ESTIMATED_FREIGHT field */
    public static final String ESTIMATED_FREIGHT;
      /** the column name for the IS_TAXABLE field */
    public static final String IS_TAXABLE;
      /** the column name for the IS_INCLUSIVE_TAX field */
    public static final String IS_INCLUSIVE_TAX;
      /** the column name for the FISCAL_RATE field */
    public static final String FISCAL_RATE;
      /** the column name for the CONFIRM_DATE field */
    public static final String CONFIRM_DATE;
      /** the column name for the DELIVERY_DATE field */
    public static final String DELIVERY_DATE;
      /** the column name for the SHIP_TO field */
    public static final String SHIP_TO;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
      /** the column name for the CLOSED_DATE field */
    public static final String CLOSED_DATE;
      /** the column name for the EXPIRED_DATE field */
    public static final String EXPIRED_DATE;
      /** the column name for the RECEIPT_DATE field */
    public static final String RECEIPT_DATE;
  
    static
    {
          PURCHASE_ORDER_ID = "purchase_order.PURCHASE_ORDER_ID";
          PURCHASE_REQUEST_ID = "purchase_order.PURCHASE_REQUEST_ID";
          TRANSACTION_STATUS = "purchase_order.TRANSACTION_STATUS";
          PURCHASE_ORDER_NO = "purchase_order.PURCHASE_ORDER_NO";
          TRANSACTION_DATE = "purchase_order.TRANSACTION_DATE";
          DUE_DATE = "purchase_order.DUE_DATE";
          VENDOR_ID = "purchase_order.VENDOR_ID";
          VENDOR_NAME = "purchase_order.VENDOR_NAME";
          TOTAL_QTY = "purchase_order.TOTAL_QTY";
          TOTAL_AMOUNT = "purchase_order.TOTAL_AMOUNT";
          TOTAL_DISCOUNT_PCT = "purchase_order.TOTAL_DISCOUNT_PCT";
          TOTAL_DISCOUNT = "purchase_order.TOTAL_DISCOUNT";
          TOTAL_FEE = "purchase_order.TOTAL_FEE";
          TOTAL_TAX = "purchase_order.TOTAL_TAX";
          CREATE_BY = "purchase_order.CREATE_BY";
          CONFIRM_BY = "purchase_order.CONFIRM_BY";
          REMARK = "purchase_order.REMARK";
          PAYMENT_TYPE_ID = "purchase_order.PAYMENT_TYPE_ID";
          PAYMENT_TERM_ID = "purchase_order.PAYMENT_TERM_ID";
          CURRENCY_ID = "purchase_order.CURRENCY_ID";
          CURRENCY_RATE = "purchase_order.CURRENCY_RATE";
          CREATE_RECEIPT = "purchase_order.CREATE_RECEIPT";
          LOCATION_ID = "purchase_order.LOCATION_ID";
          PRINT_TIMES = "purchase_order.PRINT_TIMES";
          SENT_STATUS = "purchase_order.SENT_STATUS";
          DOWN_PAYMENT = "purchase_order.DOWN_PAYMENT";
          BANK_ID = "purchase_order.BANK_ID";
          BANK_ISSUER = "purchase_order.BANK_ISSUER";
          DP_ACCOUNT_ID = "purchase_order.DP_ACCOUNT_ID";
          DP_DUE_DATE = "purchase_order.DP_DUE_DATE";
          REFERENCE_NO = "purchase_order.REFERENCE_NO";
          CASH_FLOW_TYPE_ID = "purchase_order.CASH_FLOW_TYPE_ID";
          COURIER_ID = "purchase_order.COURIER_ID";
          FOB_ID = "purchase_order.FOB_ID";
          ESTIMATED_FREIGHT = "purchase_order.ESTIMATED_FREIGHT";
          IS_TAXABLE = "purchase_order.IS_TAXABLE";
          IS_INCLUSIVE_TAX = "purchase_order.IS_INCLUSIVE_TAX";
          FISCAL_RATE = "purchase_order.FISCAL_RATE";
          CONFIRM_DATE = "purchase_order.CONFIRM_DATE";
          DELIVERY_DATE = "purchase_order.DELIVERY_DATE";
          SHIP_TO = "purchase_order.SHIP_TO";
          CANCEL_BY = "purchase_order.CANCEL_BY";
          CANCEL_DATE = "purchase_order.CANCEL_DATE";
          CLOSED_DATE = "purchase_order.CLOSED_DATE";
          EXPIRED_DATE = "purchase_order.EXPIRED_DATE";
          RECEIPT_DATE = "purchase_order.RECEIPT_DATE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PurchaseOrderMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PurchaseOrderMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  46;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.PurchaseOrder";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseOrderPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_RECEIPT))
        {
            Object possibleBoolean = criteria.get(CREATE_RECEIPT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_RECEIPT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PURCHASE_ORDER_ID);
          criteria.addSelectColumn(PURCHASE_REQUEST_ID);
          criteria.addSelectColumn(TRANSACTION_STATUS);
          criteria.addSelectColumn(PURCHASE_ORDER_NO);
          criteria.addSelectColumn(TRANSACTION_DATE);
          criteria.addSelectColumn(DUE_DATE);
          criteria.addSelectColumn(VENDOR_ID);
          criteria.addSelectColumn(VENDOR_NAME);
          criteria.addSelectColumn(TOTAL_QTY);
          criteria.addSelectColumn(TOTAL_AMOUNT);
          criteria.addSelectColumn(TOTAL_DISCOUNT_PCT);
          criteria.addSelectColumn(TOTAL_DISCOUNT);
          criteria.addSelectColumn(TOTAL_FEE);
          criteria.addSelectColumn(TOTAL_TAX);
          criteria.addSelectColumn(CREATE_BY);
          criteria.addSelectColumn(CONFIRM_BY);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(PAYMENT_TYPE_ID);
          criteria.addSelectColumn(PAYMENT_TERM_ID);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(CREATE_RECEIPT);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(PRINT_TIMES);
          criteria.addSelectColumn(SENT_STATUS);
          criteria.addSelectColumn(DOWN_PAYMENT);
          criteria.addSelectColumn(BANK_ID);
          criteria.addSelectColumn(BANK_ISSUER);
          criteria.addSelectColumn(DP_ACCOUNT_ID);
          criteria.addSelectColumn(DP_DUE_DATE);
          criteria.addSelectColumn(REFERENCE_NO);
          criteria.addSelectColumn(CASH_FLOW_TYPE_ID);
          criteria.addSelectColumn(COURIER_ID);
          criteria.addSelectColumn(FOB_ID);
          criteria.addSelectColumn(ESTIMATED_FREIGHT);
          criteria.addSelectColumn(IS_TAXABLE);
          criteria.addSelectColumn(IS_INCLUSIVE_TAX);
          criteria.addSelectColumn(FISCAL_RATE);
          criteria.addSelectColumn(CONFIRM_DATE);
          criteria.addSelectColumn(DELIVERY_DATE);
          criteria.addSelectColumn(SHIP_TO);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
          criteria.addSelectColumn(CLOSED_DATE);
          criteria.addSelectColumn(EXPIRED_DATE);
          criteria.addSelectColumn(RECEIPT_DATE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static PurchaseOrder row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            PurchaseOrder obj = (PurchaseOrder) cls.newInstance();
            PurchaseOrderPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      PurchaseOrder obj)
        throws TorqueException
    {
        try
        {
                obj.setPurchaseOrderId(row.getValue(offset + 0).asString());
                  obj.setPurchaseRequestId(row.getValue(offset + 1).asString());
                  obj.setTransactionStatus(row.getValue(offset + 2).asInt());
                  obj.setPurchaseOrderNo(row.getValue(offset + 3).asString());
                  obj.setTransactionDate(row.getValue(offset + 4).asUtilDate());
                  obj.setDueDate(row.getValue(offset + 5).asUtilDate());
                  obj.setVendorId(row.getValue(offset + 6).asString());
                  obj.setVendorName(row.getValue(offset + 7).asString());
                  obj.setTotalQty(row.getValue(offset + 8).asBigDecimal());
                  obj.setTotalAmount(row.getValue(offset + 9).asBigDecimal());
                  obj.setTotalDiscountPct(row.getValue(offset + 10).asString());
                  obj.setTotalDiscount(row.getValue(offset + 11).asBigDecimal());
                  obj.setTotalFee(row.getValue(offset + 12).asBigDecimal());
                  obj.setTotalTax(row.getValue(offset + 13).asBigDecimal());
                  obj.setCreateBy(row.getValue(offset + 14).asString());
                  obj.setConfirmBy(row.getValue(offset + 15).asString());
                  obj.setRemark(row.getValue(offset + 16).asString());
                  obj.setPaymentTypeId(row.getValue(offset + 17).asString());
                  obj.setPaymentTermId(row.getValue(offset + 18).asString());
                  obj.setCurrencyId(row.getValue(offset + 19).asString());
                  obj.setCurrencyRate(row.getValue(offset + 20).asBigDecimal());
                  obj.setCreateReceipt(row.getValue(offset + 21).asBoolean());
                  obj.setLocationId(row.getValue(offset + 22).asString());
                  obj.setPrintTimes(row.getValue(offset + 23).asInt());
                  obj.setSentStatus(row.getValue(offset + 24).asInt());
                  obj.setDownPayment(row.getValue(offset + 25).asBigDecimal());
                  obj.setBankId(row.getValue(offset + 26).asString());
                  obj.setBankIssuer(row.getValue(offset + 27).asString());
                  obj.setDpAccountId(row.getValue(offset + 28).asString());
                  obj.setDpDueDate(row.getValue(offset + 29).asUtilDate());
                  obj.setReferenceNo(row.getValue(offset + 30).asString());
                  obj.setCashFlowTypeId(row.getValue(offset + 31).asString());
                  obj.setCourierId(row.getValue(offset + 32).asString());
                  obj.setFobId(row.getValue(offset + 33).asString());
                  obj.setEstimatedFreight(row.getValue(offset + 34).asBigDecimal());
                  obj.setIsTaxable(row.getValue(offset + 35).asBoolean());
                  obj.setIsInclusiveTax(row.getValue(offset + 36).asBoolean());
                  obj.setFiscalRate(row.getValue(offset + 37).asBigDecimal());
                  obj.setConfirmDate(row.getValue(offset + 38).asUtilDate());
                  obj.setDeliveryDate(row.getValue(offset + 39).asUtilDate());
                  obj.setShipTo(row.getValue(offset + 40).asString());
                  obj.setCancelBy(row.getValue(offset + 41).asString());
                  obj.setCancelDate(row.getValue(offset + 42).asUtilDate());
                  obj.setClosedDate(row.getValue(offset + 43).asUtilDate());
                  obj.setExpiredDate(row.getValue(offset + 44).asUtilDate());
                  obj.setReceiptDate(row.getValue(offset + 45).asUtilDate());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseOrderPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_RECEIPT))
        {
            Object possibleBoolean = criteria.get(CREATE_RECEIPT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_RECEIPT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PurchaseOrderPeer.row2Object(row, 1,
                PurchaseOrderPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePurchaseOrderPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PURCHASE_ORDER_ID, criteria.remove(PURCHASE_ORDER_ID));
                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_RECEIPT))
        {
            Object possibleBoolean = criteria.get(CREATE_RECEIPT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_RECEIPT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PurchaseOrderPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_RECEIPT))
        {
            Object possibleBoolean = criteria.get(CREATE_RECEIPT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_RECEIPT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(PurchaseOrder obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseOrder obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseOrder obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseOrder obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(PurchaseOrder) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseOrder obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(PurchaseOrder) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseOrder obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(PurchaseOrder) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseOrder obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePurchaseOrderPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PURCHASE_ORDER_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( PurchaseOrder obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PURCHASE_ORDER_ID, obj.getPurchaseOrderId());
              criteria.add(PURCHASE_REQUEST_ID, obj.getPurchaseRequestId());
              criteria.add(TRANSACTION_STATUS, obj.getTransactionStatus());
              criteria.add(PURCHASE_ORDER_NO, obj.getPurchaseOrderNo());
              criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
              criteria.add(DUE_DATE, obj.getDueDate());
              criteria.add(VENDOR_ID, obj.getVendorId());
              criteria.add(VENDOR_NAME, obj.getVendorName());
              criteria.add(TOTAL_QTY, obj.getTotalQty());
              criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
              criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
              criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
              criteria.add(TOTAL_FEE, obj.getTotalFee());
              criteria.add(TOTAL_TAX, obj.getTotalTax());
              criteria.add(CREATE_BY, obj.getCreateBy());
              criteria.add(CONFIRM_BY, obj.getConfirmBy());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
              criteria.add(PAYMENT_TERM_ID, obj.getPaymentTermId());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(CREATE_RECEIPT, obj.getCreateReceipt());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(PRINT_TIMES, obj.getPrintTimes());
              criteria.add(SENT_STATUS, obj.getSentStatus());
              criteria.add(DOWN_PAYMENT, obj.getDownPayment());
              criteria.add(BANK_ID, obj.getBankId());
              criteria.add(BANK_ISSUER, obj.getBankIssuer());
              criteria.add(DP_ACCOUNT_ID, obj.getDpAccountId());
              criteria.add(DP_DUE_DATE, obj.getDpDueDate());
              criteria.add(REFERENCE_NO, obj.getReferenceNo());
              criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
              criteria.add(COURIER_ID, obj.getCourierId());
              criteria.add(FOB_ID, obj.getFobId());
              criteria.add(ESTIMATED_FREIGHT, obj.getEstimatedFreight());
              criteria.add(IS_TAXABLE, obj.getIsTaxable());
              criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
              criteria.add(FISCAL_RATE, obj.getFiscalRate());
              criteria.add(CONFIRM_DATE, obj.getConfirmDate());
              criteria.add(DELIVERY_DATE, obj.getDeliveryDate());
              criteria.add(SHIP_TO, obj.getShipTo());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
              criteria.add(CLOSED_DATE, obj.getClosedDate());
              criteria.add(EXPIRED_DATE, obj.getExpiredDate());
              criteria.add(RECEIPT_DATE, obj.getReceiptDate());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( PurchaseOrder obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PURCHASE_ORDER_ID, obj.getPurchaseOrderId());
                          criteria.add(PURCHASE_REQUEST_ID, obj.getPurchaseRequestId());
                          criteria.add(TRANSACTION_STATUS, obj.getTransactionStatus());
                          criteria.add(PURCHASE_ORDER_NO, obj.getPurchaseOrderNo());
                          criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
                          criteria.add(DUE_DATE, obj.getDueDate());
                          criteria.add(VENDOR_ID, obj.getVendorId());
                          criteria.add(VENDOR_NAME, obj.getVendorName());
                          criteria.add(TOTAL_QTY, obj.getTotalQty());
                          criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
                          criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
                          criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
                          criteria.add(TOTAL_FEE, obj.getTotalFee());
                          criteria.add(TOTAL_TAX, obj.getTotalTax());
                          criteria.add(CREATE_BY, obj.getCreateBy());
                          criteria.add(CONFIRM_BY, obj.getConfirmBy());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
                          criteria.add(PAYMENT_TERM_ID, obj.getPaymentTermId());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(CREATE_RECEIPT, obj.getCreateReceipt());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(PRINT_TIMES, obj.getPrintTimes());
                          criteria.add(SENT_STATUS, obj.getSentStatus());
                          criteria.add(DOWN_PAYMENT, obj.getDownPayment());
                          criteria.add(BANK_ID, obj.getBankId());
                          criteria.add(BANK_ISSUER, obj.getBankIssuer());
                          criteria.add(DP_ACCOUNT_ID, obj.getDpAccountId());
                          criteria.add(DP_DUE_DATE, obj.getDpDueDate());
                          criteria.add(REFERENCE_NO, obj.getReferenceNo());
                          criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
                          criteria.add(COURIER_ID, obj.getCourierId());
                          criteria.add(FOB_ID, obj.getFobId());
                          criteria.add(ESTIMATED_FREIGHT, obj.getEstimatedFreight());
                          criteria.add(IS_TAXABLE, obj.getIsTaxable());
                          criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
                          criteria.add(FISCAL_RATE, obj.getFiscalRate());
                          criteria.add(CONFIRM_DATE, obj.getConfirmDate());
                          criteria.add(DELIVERY_DATE, obj.getDeliveryDate());
                          criteria.add(SHIP_TO, obj.getShipTo());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
                          criteria.add(CLOSED_DATE, obj.getClosedDate());
                          criteria.add(EXPIRED_DATE, obj.getExpiredDate());
                          criteria.add(RECEIPT_DATE, obj.getReceiptDate());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrder retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrder retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrder retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        PurchaseOrder retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrder retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (PurchaseOrder)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PURCHASE_ORDER_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
