package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseRequestDetail
 */
public abstract class BasePurchaseRequestDetail extends BaseObject
{
    /** The Peer class */
    private static final PurchaseRequestDetailPeer peer =
        new PurchaseRequestDetailPeer();

        
    /** The value for the purchaseRequestDetailId field */
    private String purchaseRequestDetailId;
      
    /** The value for the purchaseRequestId field */
    private String purchaseRequestId;
      
    /** The value for the indexNo field */
    private int indexNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the minimumStock field */
    private BigDecimal minimumStock;
      
    /** The value for the maximumStock field */
    private BigDecimal maximumStock;
      
    /** The value for the reorderPoint field */
    private BigDecimal reorderPoint;
      
    /** The value for the currentQty field */
    private BigDecimal currentQty;
      
    /** The value for the recommendedQty field */
    private BigDecimal recommendedQty;
      
    /** The value for the requestQty field */
    private BigDecimal requestQty;
      
    /** The value for the sentQty field */
    private BigDecimal sentQty;
      
    /** The value for the costPerUnit field */
    private BigDecimal costPerUnit;
  
    
    /**
     * Get the PurchaseRequestDetailId
     *
     * @return String
     */
    public String getPurchaseRequestDetailId()
    {
        return purchaseRequestDetailId;
    }

                        
    /**
     * Set the value of PurchaseRequestDetailId
     *
     * @param v new value
     */
    public void setPurchaseRequestDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseRequestDetailId, v))
              {
            this.purchaseRequestDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseRequestId
     *
     * @return String
     */
    public String getPurchaseRequestId()
    {
        return purchaseRequestId;
    }

                              
    /**
     * Set the value of PurchaseRequestId
     *
     * @param v new value
     */
    public void setPurchaseRequestId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseRequestId, v))
              {
            this.purchaseRequestId = v;
            setModified(true);
        }
    
                          
                if (aPurchaseRequest != null && !ObjectUtils.equals(aPurchaseRequest.getPurchaseRequestId(), v))
                {
            aPurchaseRequest = null;
        }
      
              }
  
    /**
     * Get the IndexNo
     *
     * @return int
     */
    public int getIndexNo()
    {
        return indexNo;
    }

                        
    /**
     * Set the value of IndexNo
     *
     * @param v new value
     */
    public void setIndexNo(int v) 
    {
    
                  if (this.indexNo != v)
              {
            this.indexNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MinimumStock
     *
     * @return BigDecimal
     */
    public BigDecimal getMinimumStock()
    {
        return minimumStock;
    }

                        
    /**
     * Set the value of MinimumStock
     *
     * @param v new value
     */
    public void setMinimumStock(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.minimumStock, v))
              {
            this.minimumStock = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MaximumStock
     *
     * @return BigDecimal
     */
    public BigDecimal getMaximumStock()
    {
        return maximumStock;
    }

                        
    /**
     * Set the value of MaximumStock
     *
     * @param v new value
     */
    public void setMaximumStock(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.maximumStock, v))
              {
            this.maximumStock = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReorderPoint
     *
     * @return BigDecimal
     */
    public BigDecimal getReorderPoint()
    {
        return reorderPoint;
    }

                        
    /**
     * Set the value of ReorderPoint
     *
     * @param v new value
     */
    public void setReorderPoint(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.reorderPoint, v))
              {
            this.reorderPoint = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrentQty
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrentQty()
    {
        return currentQty;
    }

                        
    /**
     * Set the value of CurrentQty
     *
     * @param v new value
     */
    public void setCurrentQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currentQty, v))
              {
            this.currentQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RecommendedQty
     *
     * @return BigDecimal
     */
    public BigDecimal getRecommendedQty()
    {
        return recommendedQty;
    }

                        
    /**
     * Set the value of RecommendedQty
     *
     * @param v new value
     */
    public void setRecommendedQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.recommendedQty, v))
              {
            this.recommendedQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RequestQty
     *
     * @return BigDecimal
     */
    public BigDecimal getRequestQty()
    {
        return requestQty;
    }

                        
    /**
     * Set the value of RequestQty
     *
     * @param v new value
     */
    public void setRequestQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.requestQty, v))
              {
            this.requestQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SentQty
     *
     * @return BigDecimal
     */
    public BigDecimal getSentQty()
    {
        return sentQty;
    }

                        
    /**
     * Set the value of SentQty
     *
     * @param v new value
     */
    public void setSentQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.sentQty, v))
              {
            this.sentQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CostPerUnit
     *
     * @return BigDecimal
     */
    public BigDecimal getCostPerUnit()
    {
        return costPerUnit;
    }

                        
    /**
     * Set the value of CostPerUnit
     *
     * @param v new value
     */
    public void setCostPerUnit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.costPerUnit, v))
              {
            this.costPerUnit = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PurchaseRequest aPurchaseRequest;

    /**
     * Declares an association between this object and a PurchaseRequest object
     *
     * @param v PurchaseRequest
     * @throws TorqueException
     */
    public void setPurchaseRequest(PurchaseRequest v) throws TorqueException
    {
            if (v == null)
        {
                  setPurchaseRequestId((String) null);
              }
        else
        {
            setPurchaseRequestId(v.getPurchaseRequestId());
        }
            aPurchaseRequest = v;
    }

                                            
    /**
     * Get the associated PurchaseRequest object
     *
     * @return the associated PurchaseRequest object
     * @throws TorqueException
     */
    public PurchaseRequest getPurchaseRequest() throws TorqueException
    {
        if (aPurchaseRequest == null && (!ObjectUtils.equals(this.purchaseRequestId, null)))
        {
                          aPurchaseRequest = PurchaseRequestPeer.retrieveByPK(SimpleKey.keyFor(this.purchaseRequestId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PurchaseRequest obj = PurchaseRequestPeer.retrieveByPK(this.purchaseRequestId);
               obj.addPurchaseRequestDetails(this);
            */
        }
        return aPurchaseRequest;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPurchaseRequestKey(ObjectKey key) throws TorqueException
    {
      
                        setPurchaseRequestId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseRequestDetailId");
              fieldNames.add("PurchaseRequestId");
              fieldNames.add("IndexNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("Description");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("MinimumStock");
              fieldNames.add("MaximumStock");
              fieldNames.add("ReorderPoint");
              fieldNames.add("CurrentQty");
              fieldNames.add("RecommendedQty");
              fieldNames.add("RequestQty");
              fieldNames.add("SentQty");
              fieldNames.add("CostPerUnit");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseRequestDetailId"))
        {
                return getPurchaseRequestDetailId();
            }
          if (name.equals("PurchaseRequestId"))
        {
                return getPurchaseRequestId();
            }
          if (name.equals("IndexNo"))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("MinimumStock"))
        {
                return getMinimumStock();
            }
          if (name.equals("MaximumStock"))
        {
                return getMaximumStock();
            }
          if (name.equals("ReorderPoint"))
        {
                return getReorderPoint();
            }
          if (name.equals("CurrentQty"))
        {
                return getCurrentQty();
            }
          if (name.equals("RecommendedQty"))
        {
                return getRecommendedQty();
            }
          if (name.equals("RequestQty"))
        {
                return getRequestQty();
            }
          if (name.equals("SentQty"))
        {
                return getSentQty();
            }
          if (name.equals("CostPerUnit"))
        {
                return getCostPerUnit();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseRequestDetailPeer.PURCHASE_REQUEST_DETAIL_ID))
        {
                return getPurchaseRequestDetailId();
            }
          if (name.equals(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID))
        {
                return getPurchaseRequestId();
            }
          if (name.equals(PurchaseRequestDetailPeer.INDEX_NO))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals(PurchaseRequestDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PurchaseRequestDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PurchaseRequestDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(PurchaseRequestDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PurchaseRequestDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(PurchaseRequestDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(PurchaseRequestDetailPeer.MINIMUM_STOCK))
        {
                return getMinimumStock();
            }
          if (name.equals(PurchaseRequestDetailPeer.MAXIMUM_STOCK))
        {
                return getMaximumStock();
            }
          if (name.equals(PurchaseRequestDetailPeer.REORDER_POINT))
        {
                return getReorderPoint();
            }
          if (name.equals(PurchaseRequestDetailPeer.CURRENT_QTY))
        {
                return getCurrentQty();
            }
          if (name.equals(PurchaseRequestDetailPeer.RECOMMENDED_QTY))
        {
                return getRecommendedQty();
            }
          if (name.equals(PurchaseRequestDetailPeer.REQUEST_QTY))
        {
                return getRequestQty();
            }
          if (name.equals(PurchaseRequestDetailPeer.SENT_QTY))
        {
                return getSentQty();
            }
          if (name.equals(PurchaseRequestDetailPeer.COST_PER_UNIT))
        {
                return getCostPerUnit();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseRequestDetailId();
            }
              if (pos == 1)
        {
                return getPurchaseRequestId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getIndexNo());
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getItemName();
            }
              if (pos == 6)
        {
                return getDescription();
            }
              if (pos == 7)
        {
                return getUnitId();
            }
              if (pos == 8)
        {
                return getUnitCode();
            }
              if (pos == 9)
        {
                return getMinimumStock();
            }
              if (pos == 10)
        {
                return getMaximumStock();
            }
              if (pos == 11)
        {
                return getReorderPoint();
            }
              if (pos == 12)
        {
                return getCurrentQty();
            }
              if (pos == 13)
        {
                return getRecommendedQty();
            }
              if (pos == 14)
        {
                return getRequestQty();
            }
              if (pos == 15)
        {
                return getSentQty();
            }
              if (pos == 16)
        {
                return getCostPerUnit();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseRequestDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseRequestDetailPeer.doInsert((PurchaseRequestDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseRequestDetailPeer.doUpdate((PurchaseRequestDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseRequestDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPurchaseRequestDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPurchaseRequestDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseRequestDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseRequestDetail copy() throws TorqueException
    {
        return copyInto(new PurchaseRequestDetail());
    }
  
    protected PurchaseRequestDetail copyInto(PurchaseRequestDetail copyObj) throws TorqueException
    {
          copyObj.setPurchaseRequestDetailId(purchaseRequestDetailId);
          copyObj.setPurchaseRequestId(purchaseRequestId);
          copyObj.setIndexNo(indexNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setDescription(description);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setMinimumStock(minimumStock);
          copyObj.setMaximumStock(maximumStock);
          copyObj.setReorderPoint(reorderPoint);
          copyObj.setCurrentQty(currentQty);
          copyObj.setRecommendedQty(recommendedQty);
          copyObj.setRequestQty(requestQty);
          copyObj.setSentQty(sentQty);
          copyObj.setCostPerUnit(costPerUnit);
  
                    copyObj.setPurchaseRequestDetailId((String)null);
                                                                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseRequestDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseRequestDetail\n");
        str.append("---------------------\n")
            .append("PurchaseRequestDetailId   : ")
           .append(getPurchaseRequestDetailId())
           .append("\n")
           .append("PurchaseRequestId    : ")
           .append(getPurchaseRequestId())
           .append("\n")
           .append("IndexNo              : ")
           .append(getIndexNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("MinimumStock         : ")
           .append(getMinimumStock())
           .append("\n")
           .append("MaximumStock         : ")
           .append(getMaximumStock())
           .append("\n")
           .append("ReorderPoint         : ")
           .append(getReorderPoint())
           .append("\n")
           .append("CurrentQty           : ")
           .append(getCurrentQty())
           .append("\n")
           .append("RecommendedQty       : ")
           .append(getRecommendedQty())
           .append("\n")
           .append("RequestQty           : ")
           .append(getRequestQty())
           .append("\n")
           .append("SentQty              : ")
           .append(getSentQty())
           .append("\n")
           .append("CostPerUnit          : ")
           .append(getCostPerUnit())
           .append("\n")
        ;
        return(str.toString());
    }
}
