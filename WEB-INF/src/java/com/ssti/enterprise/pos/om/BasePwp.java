package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Pwp
 */
public abstract class BasePwp extends BaseObject
{
    /** The Peer class */
    private static final PwpPeer peer =
        new PwpPeer();

        
    /** The value for the pwpId field */
    private String pwpId;
      
    /** The value for the pwpCode field */
    private String pwpCode;
      
    /** The value for the pwpType field */
    private int pwpType;
                                                
    /** The value for the description field */
    private String description = "";
  
    
    /**
     * Get the PwpId
     *
     * @return String
     */
    public String getPwpId()
    {
        return pwpId;
    }

                                              
    /**
     * Set the value of PwpId
     *
     * @param v new value
     */
    public void setPwpId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.pwpId, v))
              {
            this.pwpId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PwpBuy
        if (collPwpBuys != null)
        {
            for (int i = 0; i < collPwpBuys.size(); i++)
            {
                ((PwpBuy) collPwpBuys.get(i))
                    .setPwpId(v);
            }
        }
                                                    
                  // update associated PwpGet
        if (collPwpGets != null)
        {
            for (int i = 0; i < collPwpGets.size(); i++)
            {
                ((PwpGet) collPwpGets.get(i))
                    .setPwpId(v);
            }
        }
                                }
  
    /**
     * Get the PwpCode
     *
     * @return String
     */
    public String getPwpCode()
    {
        return pwpCode;
    }

                        
    /**
     * Set the value of PwpCode
     *
     * @param v new value
     */
    public void setPwpCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pwpCode, v))
              {
            this.pwpCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PwpType
     *
     * @return int
     */
    public int getPwpType()
    {
        return pwpType;
    }

                        
    /**
     * Set the value of PwpType
     *
     * @param v new value
     */
    public void setPwpType(int v) 
    {
    
                  if (this.pwpType != v)
              {
            this.pwpType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPwpBuys
     */
    protected List collPwpBuys;

    /**
     * Temporary storage of collPwpBuys to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPwpBuys()
    {
        if (collPwpBuys == null)
        {
            collPwpBuys = new ArrayList();
        }
    }

    /**
     * Method called to associate a PwpBuy object to this object
     * through the PwpBuy foreign key attribute
     *
     * @param l PwpBuy
     * @throws TorqueException
     */
    public void addPwpBuy(PwpBuy l) throws TorqueException
    {
        getPwpBuys().add(l);
        l.setPwp((Pwp) this);
    }

    /**
     * The criteria used to select the current contents of collPwpBuys
     */
    private Criteria lastPwpBuysCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPwpBuys(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPwpBuys() throws TorqueException
    {
              if (collPwpBuys == null)
        {
            collPwpBuys = getPwpBuys(new Criteria(10));
        }
        return collPwpBuys;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pwp has previously
     * been saved, it will retrieve related PwpBuys from storage.
     * If this Pwp is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPwpBuys(Criteria criteria) throws TorqueException
    {
              if (collPwpBuys == null)
        {
            if (isNew())
            {
               collPwpBuys = new ArrayList();
            }
            else
            {
                        criteria.add(PwpBuyPeer.PWP_ID, getPwpId() );
                        collPwpBuys = PwpBuyPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PwpBuyPeer.PWP_ID, getPwpId());
                            if (!lastPwpBuysCriteria.equals(criteria))
                {
                    collPwpBuys = PwpBuyPeer.doSelect(criteria);
                }
            }
        }
        lastPwpBuysCriteria = criteria;

        return collPwpBuys;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPwpBuys(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPwpBuys(Connection con) throws TorqueException
    {
              if (collPwpBuys == null)
        {
            collPwpBuys = getPwpBuys(new Criteria(10), con);
        }
        return collPwpBuys;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pwp has previously
     * been saved, it will retrieve related PwpBuys from storage.
     * If this Pwp is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPwpBuys(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPwpBuys == null)
        {
            if (isNew())
            {
               collPwpBuys = new ArrayList();
            }
            else
            {
                         criteria.add(PwpBuyPeer.PWP_ID, getPwpId());
                         collPwpBuys = PwpBuyPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PwpBuyPeer.PWP_ID, getPwpId());
                             if (!lastPwpBuysCriteria.equals(criteria))
                 {
                     collPwpBuys = PwpBuyPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPwpBuysCriteria = criteria;

         return collPwpBuys;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pwp is new, it will return
     * an empty collection; or if this Pwp has previously
     * been saved, it will retrieve related PwpBuys from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pwp.
     */
    protected List getPwpBuysJoinPwp(Criteria criteria)
        throws TorqueException
    {
                    if (collPwpBuys == null)
        {
            if (isNew())
            {
               collPwpBuys = new ArrayList();
            }
            else
            {
                              criteria.add(PwpBuyPeer.PWP_ID, getPwpId());
                              collPwpBuys = PwpBuyPeer.doSelectJoinPwp(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PwpBuyPeer.PWP_ID, getPwpId());
                                    if (!lastPwpBuysCriteria.equals(criteria))
            {
                collPwpBuys = PwpBuyPeer.doSelectJoinPwp(criteria);
            }
        }
        lastPwpBuysCriteria = criteria;

        return collPwpBuys;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collPwpGets
     */
    protected List collPwpGets;

    /**
     * Temporary storage of collPwpGets to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPwpGets()
    {
        if (collPwpGets == null)
        {
            collPwpGets = new ArrayList();
        }
    }

    /**
     * Method called to associate a PwpGet object to this object
     * through the PwpGet foreign key attribute
     *
     * @param l PwpGet
     * @throws TorqueException
     */
    public void addPwpGet(PwpGet l) throws TorqueException
    {
        getPwpGets().add(l);
        l.setPwp((Pwp) this);
    }

    /**
     * The criteria used to select the current contents of collPwpGets
     */
    private Criteria lastPwpGetsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPwpGets(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPwpGets() throws TorqueException
    {
              if (collPwpGets == null)
        {
            collPwpGets = getPwpGets(new Criteria(10));
        }
        return collPwpGets;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pwp has previously
     * been saved, it will retrieve related PwpGets from storage.
     * If this Pwp is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPwpGets(Criteria criteria) throws TorqueException
    {
              if (collPwpGets == null)
        {
            if (isNew())
            {
               collPwpGets = new ArrayList();
            }
            else
            {
                        criteria.add(PwpGetPeer.PWP_ID, getPwpId() );
                        collPwpGets = PwpGetPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PwpGetPeer.PWP_ID, getPwpId());
                            if (!lastPwpGetsCriteria.equals(criteria))
                {
                    collPwpGets = PwpGetPeer.doSelect(criteria);
                }
            }
        }
        lastPwpGetsCriteria = criteria;

        return collPwpGets;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPwpGets(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPwpGets(Connection con) throws TorqueException
    {
              if (collPwpGets == null)
        {
            collPwpGets = getPwpGets(new Criteria(10), con);
        }
        return collPwpGets;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pwp has previously
     * been saved, it will retrieve related PwpGets from storage.
     * If this Pwp is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPwpGets(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPwpGets == null)
        {
            if (isNew())
            {
               collPwpGets = new ArrayList();
            }
            else
            {
                         criteria.add(PwpGetPeer.PWP_ID, getPwpId());
                         collPwpGets = PwpGetPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PwpGetPeer.PWP_ID, getPwpId());
                             if (!lastPwpGetsCriteria.equals(criteria))
                 {
                     collPwpGets = PwpGetPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPwpGetsCriteria = criteria;

         return collPwpGets;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pwp is new, it will return
     * an empty collection; or if this Pwp has previously
     * been saved, it will retrieve related PwpGets from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pwp.
     */
    protected List getPwpGetsJoinPwp(Criteria criteria)
        throws TorqueException
    {
                    if (collPwpGets == null)
        {
            if (isNew())
            {
               collPwpGets = new ArrayList();
            }
            else
            {
                              criteria.add(PwpGetPeer.PWP_ID, getPwpId());
                              collPwpGets = PwpGetPeer.doSelectJoinPwp(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PwpGetPeer.PWP_ID, getPwpId());
                                    if (!lastPwpGetsCriteria.equals(criteria))
            {
                collPwpGets = PwpGetPeer.doSelectJoinPwp(criteria);
            }
        }
        lastPwpGetsCriteria = criteria;

        return collPwpGets;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PwpId");
              fieldNames.add("PwpCode");
              fieldNames.add("PwpType");
              fieldNames.add("Description");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PwpId"))
        {
                return getPwpId();
            }
          if (name.equals("PwpCode"))
        {
                return getPwpCode();
            }
          if (name.equals("PwpType"))
        {
                return Integer.valueOf(getPwpType());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PwpPeer.PWP_ID))
        {
                return getPwpId();
            }
          if (name.equals(PwpPeer.PWP_CODE))
        {
                return getPwpCode();
            }
          if (name.equals(PwpPeer.PWP_TYPE))
        {
                return Integer.valueOf(getPwpType());
            }
          if (name.equals(PwpPeer.DESCRIPTION))
        {
                return getDescription();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPwpId();
            }
              if (pos == 1)
        {
                return getPwpCode();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getPwpType());
            }
              if (pos == 3)
        {
                return getDescription();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PwpPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PwpPeer.doInsert((Pwp) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PwpPeer.doUpdate((Pwp) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPwpBuys != null)
            {
                for (int i = 0; i < collPwpBuys.size(); i++)
                {
                    ((PwpBuy) collPwpBuys.get(i)).save(con);
                }
            }
                                                  
                
                    if (collPwpGets != null)
            {
                for (int i = 0; i < collPwpGets.size(); i++)
                {
                    ((PwpGet) collPwpGets.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pwpId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPwpId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPwpId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPwpId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Pwp copy() throws TorqueException
    {
        return copyInto(new Pwp());
    }
  
    protected Pwp copyInto(Pwp copyObj) throws TorqueException
    {
          copyObj.setPwpId(pwpId);
          copyObj.setPwpCode(pwpCode);
          copyObj.setPwpType(pwpType);
          copyObj.setDescription(description);
  
                    copyObj.setPwpId((String)null);
                              
                                      
                            
        List v = getPwpBuys();
        for (int i = 0; i < v.size(); i++)
        {
            PwpBuy obj = (PwpBuy) v.get(i);
            copyObj.addPwpBuy(obj.copy());
        }
                                                  
                            
        v = getPwpGets();
        for (int i = 0; i < v.size(); i++)
        {
            PwpGet obj = (PwpGet) v.get(i);
            copyObj.addPwpGet(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PwpPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Pwp\n");
        str.append("---\n")
           .append("PwpId                : ")
           .append(getPwpId())
           .append("\n")
           .append("PwpCode              : ")
           .append(getPwpCode())
           .append("\n")
           .append("PwpType              : ")
           .append(getPwpType())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
        ;
        return(str.toString());
    }
}
