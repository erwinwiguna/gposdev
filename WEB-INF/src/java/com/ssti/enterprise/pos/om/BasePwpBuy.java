package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PwpBuy
 */
public abstract class BasePwpBuy extends BaseObject
{
    /** The Peer class */
    private static final PwpBuyPeer peer =
        new PwpBuyPeer();

        
    /** The value for the pwpBuyId field */
    private String pwpBuyId;
      
    /** The value for the pwpId field */
    private String pwpId;
      
    /** The value for the buyItemId field */
    private String buyItemId;
      
    /** The value for the buyQty field */
    private BigDecimal buyQty;
      
    /** The value for the buyPrice field */
    private BigDecimal buyPrice;
  
    
    /**
     * Get the PwpBuyId
     *
     * @return String
     */
    public String getPwpBuyId()
    {
        return pwpBuyId;
    }

                        
    /**
     * Set the value of PwpBuyId
     *
     * @param v new value
     */
    public void setPwpBuyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pwpBuyId, v))
              {
            this.pwpBuyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PwpId
     *
     * @return String
     */
    public String getPwpId()
    {
        return pwpId;
    }

                              
    /**
     * Set the value of PwpId
     *
     * @param v new value
     */
    public void setPwpId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.pwpId, v))
              {
            this.pwpId = v;
            setModified(true);
        }
    
                          
                if (aPwp != null && !ObjectUtils.equals(aPwp.getPwpId(), v))
                {
            aPwp = null;
        }
      
              }
  
    /**
     * Get the BuyItemId
     *
     * @return String
     */
    public String getBuyItemId()
    {
        return buyItemId;
    }

                        
    /**
     * Set the value of BuyItemId
     *
     * @param v new value
     */
    public void setBuyItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.buyItemId, v))
              {
            this.buyItemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BuyQty
     *
     * @return BigDecimal
     */
    public BigDecimal getBuyQty()
    {
        return buyQty;
    }

                        
    /**
     * Set the value of BuyQty
     *
     * @param v new value
     */
    public void setBuyQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.buyQty, v))
              {
            this.buyQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BuyPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getBuyPrice()
    {
        return buyPrice;
    }

                        
    /**
     * Set the value of BuyPrice
     *
     * @param v new value
     */
    public void setBuyPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.buyPrice, v))
              {
            this.buyPrice = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Pwp aPwp;

    /**
     * Declares an association between this object and a Pwp object
     *
     * @param v Pwp
     * @throws TorqueException
     */
    public void setPwp(Pwp v) throws TorqueException
    {
            if (v == null)
        {
                  setPwpId((String) null);
              }
        else
        {
            setPwpId(v.getPwpId());
        }
            aPwp = v;
    }

                                            
    /**
     * Get the associated Pwp object
     *
     * @return the associated Pwp object
     * @throws TorqueException
     */
    public Pwp getPwp() throws TorqueException
    {
        if (aPwp == null && (!ObjectUtils.equals(this.pwpId, null)))
        {
                          aPwp = PwpPeer.retrieveByPK(SimpleKey.keyFor(this.pwpId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Pwp obj = PwpPeer.retrieveByPK(this.pwpId);
               obj.addPwpBuys(this);
            */
        }
        return aPwp;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPwpKey(ObjectKey key) throws TorqueException
    {
      
                        setPwpId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PwpBuyId");
              fieldNames.add("PwpId");
              fieldNames.add("BuyItemId");
              fieldNames.add("BuyQty");
              fieldNames.add("BuyPrice");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PwpBuyId"))
        {
                return getPwpBuyId();
            }
          if (name.equals("PwpId"))
        {
                return getPwpId();
            }
          if (name.equals("BuyItemId"))
        {
                return getBuyItemId();
            }
          if (name.equals("BuyQty"))
        {
                return getBuyQty();
            }
          if (name.equals("BuyPrice"))
        {
                return getBuyPrice();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PwpBuyPeer.PWP_BUY_ID))
        {
                return getPwpBuyId();
            }
          if (name.equals(PwpBuyPeer.PWP_ID))
        {
                return getPwpId();
            }
          if (name.equals(PwpBuyPeer.BUY_ITEM_ID))
        {
                return getBuyItemId();
            }
          if (name.equals(PwpBuyPeer.BUY_QTY))
        {
                return getBuyQty();
            }
          if (name.equals(PwpBuyPeer.BUY_PRICE))
        {
                return getBuyPrice();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPwpBuyId();
            }
              if (pos == 1)
        {
                return getPwpId();
            }
              if (pos == 2)
        {
                return getBuyItemId();
            }
              if (pos == 3)
        {
                return getBuyQty();
            }
              if (pos == 4)
        {
                return getBuyPrice();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PwpBuyPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PwpBuyPeer.doInsert((PwpBuy) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PwpBuyPeer.doUpdate((PwpBuy) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pwpBuyId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPwpBuyId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPwpBuyId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPwpBuyId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PwpBuy copy() throws TorqueException
    {
        return copyInto(new PwpBuy());
    }
  
    protected PwpBuy copyInto(PwpBuy copyObj) throws TorqueException
    {
          copyObj.setPwpBuyId(pwpBuyId);
          copyObj.setPwpId(pwpId);
          copyObj.setBuyItemId(buyItemId);
          copyObj.setBuyQty(buyQty);
          copyObj.setBuyPrice(buyPrice);
  
                    copyObj.setPwpBuyId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PwpBuyPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PwpBuy\n");
        str.append("------\n")
           .append("PwpBuyId             : ")
           .append(getPwpBuyId())
           .append("\n")
           .append("PwpId                : ")
           .append(getPwpId())
           .append("\n")
           .append("BuyItemId            : ")
           .append(getBuyItemId())
           .append("\n")
           .append("BuyQty               : ")
           .append(getBuyQty())
           .append("\n")
           .append("BuyPrice             : ")
           .append(getBuyPrice())
           .append("\n")
        ;
        return(str.toString());
    }
}
