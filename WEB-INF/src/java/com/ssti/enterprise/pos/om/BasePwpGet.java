package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PwpGet
 */
public abstract class BasePwpGet extends BaseObject
{
    /** The Peer class */
    private static final PwpGetPeer peer =
        new PwpGetPeer();

        
    /** The value for the pwpGetId field */
    private String pwpGetId;
      
    /** The value for the pwpId field */
    private String pwpId;
      
    /** The value for the getItemId field */
    private String getItemId;
      
    /** The value for the getQty field */
    private BigDecimal getQty;
      
    /** The value for the getPrice field */
    private BigDecimal getPrice;
                                                
    /** The value for the getDisc field */
    private String getDisc = "0";
  
    
    /**
     * Get the PwpGetId
     *
     * @return String
     */
    public String getPwpGetId()
    {
        return pwpGetId;
    }

                        
    /**
     * Set the value of PwpGetId
     *
     * @param v new value
     */
    public void setPwpGetId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pwpGetId, v))
              {
            this.pwpGetId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PwpId
     *
     * @return String
     */
    public String getPwpId()
    {
        return pwpId;
    }

                              
    /**
     * Set the value of PwpId
     *
     * @param v new value
     */
    public void setPwpId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.pwpId, v))
              {
            this.pwpId = v;
            setModified(true);
        }
    
                          
                if (aPwp != null && !ObjectUtils.equals(aPwp.getPwpId(), v))
                {
            aPwp = null;
        }
      
              }
  
    /**
     * Get the GetItemId
     *
     * @return String
     */
    public String getGetItemId()
    {
        return getItemId;
    }

                        
    /**
     * Set the value of GetItemId
     *
     * @param v new value
     */
    public void setGetItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.getItemId, v))
              {
            this.getItemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GetQty
     *
     * @return BigDecimal
     */
    public BigDecimal getGetQty()
    {
        return getQty;
    }

                        
    /**
     * Set the value of GetQty
     *
     * @param v new value
     */
    public void setGetQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.getQty, v))
              {
            this.getQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GetPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getGetPrice()
    {
        return getPrice;
    }

                        
    /**
     * Set the value of GetPrice
     *
     * @param v new value
     */
    public void setGetPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.getPrice, v))
              {
            this.getPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GetDisc
     *
     * @return String
     */
    public String getGetDisc()
    {
        return getDisc;
    }

                        
    /**
     * Set the value of GetDisc
     *
     * @param v new value
     */
    public void setGetDisc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.getDisc, v))
              {
            this.getDisc = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Pwp aPwp;

    /**
     * Declares an association between this object and a Pwp object
     *
     * @param v Pwp
     * @throws TorqueException
     */
    public void setPwp(Pwp v) throws TorqueException
    {
            if (v == null)
        {
                  setPwpId((String) null);
              }
        else
        {
            setPwpId(v.getPwpId());
        }
            aPwp = v;
    }

                                            
    /**
     * Get the associated Pwp object
     *
     * @return the associated Pwp object
     * @throws TorqueException
     */
    public Pwp getPwp() throws TorqueException
    {
        if (aPwp == null && (!ObjectUtils.equals(this.pwpId, null)))
        {
                          aPwp = PwpPeer.retrieveByPK(SimpleKey.keyFor(this.pwpId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Pwp obj = PwpPeer.retrieveByPK(this.pwpId);
               obj.addPwpGets(this);
            */
        }
        return aPwp;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPwpKey(ObjectKey key) throws TorqueException
    {
      
                        setPwpId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PwpGetId");
              fieldNames.add("PwpId");
              fieldNames.add("GetItemId");
              fieldNames.add("GetQty");
              fieldNames.add("GetPrice");
              fieldNames.add("GetDisc");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PwpGetId"))
        {
                return getPwpGetId();
            }
          if (name.equals("PwpId"))
        {
                return getPwpId();
            }
          if (name.equals("GetItemId"))
        {
                return getGetItemId();
            }
          if (name.equals("GetQty"))
        {
                return getGetQty();
            }
          if (name.equals("GetPrice"))
        {
                return getGetPrice();
            }
          if (name.equals("GetDisc"))
        {
                return getGetDisc();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PwpGetPeer.PWP_GET_ID))
        {
                return getPwpGetId();
            }
          if (name.equals(PwpGetPeer.PWP_ID))
        {
                return getPwpId();
            }
          if (name.equals(PwpGetPeer.GET_ITEM_ID))
        {
                return getGetItemId();
            }
          if (name.equals(PwpGetPeer.GET_QTY))
        {
                return getGetQty();
            }
          if (name.equals(PwpGetPeer.GET_PRICE))
        {
                return getGetPrice();
            }
          if (name.equals(PwpGetPeer.GET_DISC))
        {
                return getGetDisc();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPwpGetId();
            }
              if (pos == 1)
        {
                return getPwpId();
            }
              if (pos == 2)
        {
                return getGetItemId();
            }
              if (pos == 3)
        {
                return getGetQty();
            }
              if (pos == 4)
        {
                return getGetPrice();
            }
              if (pos == 5)
        {
                return getGetDisc();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PwpGetPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PwpGetPeer.doInsert((PwpGet) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PwpGetPeer.doUpdate((PwpGet) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pwpGetId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPwpGetId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPwpGetId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPwpGetId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PwpGet copy() throws TorqueException
    {
        return copyInto(new PwpGet());
    }
  
    protected PwpGet copyInto(PwpGet copyObj) throws TorqueException
    {
          copyObj.setPwpGetId(pwpGetId);
          copyObj.setPwpId(pwpId);
          copyObj.setGetItemId(getItemId);
          copyObj.setGetQty(getQty);
          copyObj.setGetPrice(getPrice);
          copyObj.setGetDisc(getDisc);
  
                    copyObj.setPwpGetId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PwpGetPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PwpGet\n");
        str.append("------\n")
           .append("PwpGetId             : ")
           .append(getPwpGetId())
           .append("\n")
           .append("PwpId                : ")
           .append(getPwpId())
           .append("\n")
           .append("GetItemId            : ")
           .append(getGetItemId())
           .append("\n")
           .append("GetQty               : ")
           .append(getGetQty())
           .append("\n")
           .append("GetPrice             : ")
           .append(getGetPrice())
           .append("\n")
           .append("GetDisc              : ")
           .append(getGetDisc())
           .append("\n")
        ;
        return(str.toString());
    }
}
