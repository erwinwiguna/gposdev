package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to RemoteStoreData
 */
public abstract class BaseRemoteStoreData extends BaseObject
{
    /** The Peer class */
    private static final RemoteStoreDataPeer peer =
        new RemoteStoreDataPeer();

        
    /** The value for the remoteStoreDataId field */
    private String remoteStoreDataId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the filePath field */
    private String filePath;
      
    /** The value for the setupDate field */
    private Date setupDate;
      
    /** The value for the logFile field */
    private String logFile;
  
    
    /**
     * Get the RemoteStoreDataId
     *
     * @return String
     */
    public String getRemoteStoreDataId()
    {
        return remoteStoreDataId;
    }

                        
    /**
     * Set the value of RemoteStoreDataId
     *
     * @param v new value
     */
    public void setRemoteStoreDataId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remoteStoreDataId, v))
              {
            this.remoteStoreDataId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FilePath
     *
     * @return String
     */
    public String getFilePath()
    {
        return filePath;
    }

                        
    /**
     * Set the value of FilePath
     *
     * @param v new value
     */
    public void setFilePath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.filePath, v))
              {
            this.filePath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SetupDate
     *
     * @return Date
     */
    public Date getSetupDate()
    {
        return setupDate;
    }

                        
    /**
     * Set the value of SetupDate
     *
     * @param v new value
     */
    public void setSetupDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.setupDate, v))
              {
            this.setupDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LogFile
     *
     * @return String
     */
    public String getLogFile()
    {
        return logFile;
    }

                        
    /**
     * Set the value of LogFile
     *
     * @param v new value
     */
    public void setLogFile(String v) 
    {
    
                  if (!ObjectUtils.equals(this.logFile, v))
              {
            this.logFile = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("RemoteStoreDataId");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("UserName");
              fieldNames.add("FilePath");
              fieldNames.add("SetupDate");
              fieldNames.add("LogFile");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("RemoteStoreDataId"))
        {
                return getRemoteStoreDataId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("FilePath"))
        {
                return getFilePath();
            }
          if (name.equals("SetupDate"))
        {
                return getSetupDate();
            }
          if (name.equals("LogFile"))
        {
                return getLogFile();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(RemoteStoreDataPeer.REMOTE_STORE_DATA_ID))
        {
                return getRemoteStoreDataId();
            }
          if (name.equals(RemoteStoreDataPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(RemoteStoreDataPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(RemoteStoreDataPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(RemoteStoreDataPeer.FILE_PATH))
        {
                return getFilePath();
            }
          if (name.equals(RemoteStoreDataPeer.SETUP_DATE))
        {
                return getSetupDate();
            }
          if (name.equals(RemoteStoreDataPeer.LOG_FILE))
        {
                return getLogFile();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getRemoteStoreDataId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getLocationName();
            }
              if (pos == 3)
        {
                return getUserName();
            }
              if (pos == 4)
        {
                return getFilePath();
            }
              if (pos == 5)
        {
                return getSetupDate();
            }
              if (pos == 6)
        {
                return getLogFile();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(RemoteStoreDataPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        RemoteStoreDataPeer.doInsert((RemoteStoreData) this, con);
                        setNew(false);
                    }
                    else
                    {
                        RemoteStoreDataPeer.doUpdate((RemoteStoreData) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key remoteStoreDataId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setRemoteStoreDataId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setRemoteStoreDataId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getRemoteStoreDataId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public RemoteStoreData copy() throws TorqueException
    {
        return copyInto(new RemoteStoreData());
    }
  
    protected RemoteStoreData copyInto(RemoteStoreData copyObj) throws TorqueException
    {
          copyObj.setRemoteStoreDataId(remoteStoreDataId);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setUserName(userName);
          copyObj.setFilePath(filePath);
          copyObj.setSetupDate(setupDate);
          copyObj.setLogFile(logFile);
  
                    copyObj.setRemoteStoreDataId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public RemoteStoreDataPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("RemoteStoreData\n");
        str.append("---------------\n")
           .append("RemoteStoreDataId    : ")
           .append(getRemoteStoreDataId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("FilePath             : ")
           .append(getFilePath())
           .append("\n")
           .append("SetupDate            : ")
           .append(getSetupDate())
           .append("\n")
           .append("LogFile              : ")
           .append(getLogFile())
           .append("\n")
        ;
        return(str.toString());
    }
}
