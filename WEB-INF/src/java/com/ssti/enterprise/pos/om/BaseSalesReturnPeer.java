package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.SalesReturnMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseSalesReturnPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "sales_return";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(SalesReturnMapBuilder.CLASS_NAME);
    }

      /** the column name for the SALES_RETURN_ID field */
    public static final String SALES_RETURN_ID;
      /** the column name for the TRANSACTION_TYPE field */
    public static final String TRANSACTION_TYPE;
      /** the column name for the TRANSACTION_ID field */
    public static final String TRANSACTION_ID;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the TRANSACTION_NO field */
    public static final String TRANSACTION_NO;
      /** the column name for the TRANSACTION_DATE field */
    public static final String TRANSACTION_DATE;
      /** the column name for the RETURN_NO field */
    public static final String RETURN_NO;
      /** the column name for the RETURN_DATE field */
    public static final String RETURN_DATE;
      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the CUSTOMER_NAME field */
    public static final String CUSTOMER_NAME;
      /** the column name for the TOTAL_QTY field */
    public static final String TOTAL_QTY;
      /** the column name for the TOTAL_TAX field */
    public static final String TOTAL_TAX;
      /** the column name for the TOTAL_DISCOUNT_PCT field */
    public static final String TOTAL_DISCOUNT_PCT;
      /** the column name for the TOTAL_DISCOUNT field */
    public static final String TOTAL_DISCOUNT;
      /** the column name for the TOTAL_AMOUNT field */
    public static final String TOTAL_AMOUNT;
      /** the column name for the USER_NAME field */
    public static final String USER_NAME;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the PAYMENT_TYPE_ID field */
    public static final String PAYMENT_TYPE_ID;
      /** the column name for the CREATE_CREDIT_MEMO field */
    public static final String CREATE_CREDIT_MEMO;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the SALES_ID field */
    public static final String SALES_ID;
      /** the column name for the RETURN_REASON_ID field */
    public static final String RETURN_REASON_ID;
      /** the column name for the CASH_RETURN field */
    public static final String CASH_RETURN;
      /** the column name for the BANK_ID field */
    public static final String BANK_ID;
      /** the column name for the BANK_ISSUER field */
    public static final String BANK_ISSUER;
      /** the column name for the DUE_DATE field */
    public static final String DUE_DATE;
      /** the column name for the REFERENCE_NO field */
    public static final String REFERENCE_NO;
      /** the column name for the CASH_FLOW_TYPE_ID field */
    public static final String CASH_FLOW_TYPE_ID;
      /** the column name for the ROUNDING_AMOUNT field */
    public static final String ROUNDING_AMOUNT;
      /** the column name for the RETURNED_AMOUNT field */
    public static final String RETURNED_AMOUNT;
      /** the column name for the FISCAL_RATE field */
    public static final String FISCAL_RATE;
      /** the column name for the IS_TAXABLE field */
    public static final String IS_TAXABLE;
      /** the column name for the IS_INCLUSIVE_TAX field */
    public static final String IS_INCLUSIVE_TAX;
      /** the column name for the RETURN_DISC_ACC_ID field */
    public static final String RETURN_DISC_ACC_ID;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
  
    static
    {
          SALES_RETURN_ID = "sales_return.SALES_RETURN_ID";
          TRANSACTION_TYPE = "sales_return.TRANSACTION_TYPE";
          TRANSACTION_ID = "sales_return.TRANSACTION_ID";
          LOCATION_ID = "sales_return.LOCATION_ID";
          STATUS = "sales_return.STATUS";
          TRANSACTION_NO = "sales_return.TRANSACTION_NO";
          TRANSACTION_DATE = "sales_return.TRANSACTION_DATE";
          RETURN_NO = "sales_return.RETURN_NO";
          RETURN_DATE = "sales_return.RETURN_DATE";
          CUSTOMER_ID = "sales_return.CUSTOMER_ID";
          CUSTOMER_NAME = "sales_return.CUSTOMER_NAME";
          TOTAL_QTY = "sales_return.TOTAL_QTY";
          TOTAL_TAX = "sales_return.TOTAL_TAX";
          TOTAL_DISCOUNT_PCT = "sales_return.TOTAL_DISCOUNT_PCT";
          TOTAL_DISCOUNT = "sales_return.TOTAL_DISCOUNT";
          TOTAL_AMOUNT = "sales_return.TOTAL_AMOUNT";
          USER_NAME = "sales_return.USER_NAME";
          REMARK = "sales_return.REMARK";
          PAYMENT_TYPE_ID = "sales_return.PAYMENT_TYPE_ID";
          CREATE_CREDIT_MEMO = "sales_return.CREATE_CREDIT_MEMO";
          CURRENCY_ID = "sales_return.CURRENCY_ID";
          CURRENCY_RATE = "sales_return.CURRENCY_RATE";
          SALES_ID = "sales_return.SALES_ID";
          RETURN_REASON_ID = "sales_return.RETURN_REASON_ID";
          CASH_RETURN = "sales_return.CASH_RETURN";
          BANK_ID = "sales_return.BANK_ID";
          BANK_ISSUER = "sales_return.BANK_ISSUER";
          DUE_DATE = "sales_return.DUE_DATE";
          REFERENCE_NO = "sales_return.REFERENCE_NO";
          CASH_FLOW_TYPE_ID = "sales_return.CASH_FLOW_TYPE_ID";
          ROUNDING_AMOUNT = "sales_return.ROUNDING_AMOUNT";
          RETURNED_AMOUNT = "sales_return.RETURNED_AMOUNT";
          FISCAL_RATE = "sales_return.FISCAL_RATE";
          IS_TAXABLE = "sales_return.IS_TAXABLE";
          IS_INCLUSIVE_TAX = "sales_return.IS_INCLUSIVE_TAX";
          RETURN_DISC_ACC_ID = "sales_return.RETURN_DISC_ACC_ID";
          CANCEL_BY = "sales_return.CANCEL_BY";
          CANCEL_DATE = "sales_return.CANCEL_DATE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(SalesReturnMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(SalesReturnMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  38;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.SalesReturn";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseSalesReturnPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_CREDIT_MEMO))
        {
            Object possibleBoolean = criteria.get(CREATE_CREDIT_MEMO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_CREDIT_MEMO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CASH_RETURN))
        {
            Object possibleBoolean = criteria.get(CASH_RETURN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CASH_RETURN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(SALES_RETURN_ID);
          criteria.addSelectColumn(TRANSACTION_TYPE);
          criteria.addSelectColumn(TRANSACTION_ID);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(TRANSACTION_NO);
          criteria.addSelectColumn(TRANSACTION_DATE);
          criteria.addSelectColumn(RETURN_NO);
          criteria.addSelectColumn(RETURN_DATE);
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(CUSTOMER_NAME);
          criteria.addSelectColumn(TOTAL_QTY);
          criteria.addSelectColumn(TOTAL_TAX);
          criteria.addSelectColumn(TOTAL_DISCOUNT_PCT);
          criteria.addSelectColumn(TOTAL_DISCOUNT);
          criteria.addSelectColumn(TOTAL_AMOUNT);
          criteria.addSelectColumn(USER_NAME);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(PAYMENT_TYPE_ID);
          criteria.addSelectColumn(CREATE_CREDIT_MEMO);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(SALES_ID);
          criteria.addSelectColumn(RETURN_REASON_ID);
          criteria.addSelectColumn(CASH_RETURN);
          criteria.addSelectColumn(BANK_ID);
          criteria.addSelectColumn(BANK_ISSUER);
          criteria.addSelectColumn(DUE_DATE);
          criteria.addSelectColumn(REFERENCE_NO);
          criteria.addSelectColumn(CASH_FLOW_TYPE_ID);
          criteria.addSelectColumn(ROUNDING_AMOUNT);
          criteria.addSelectColumn(RETURNED_AMOUNT);
          criteria.addSelectColumn(FISCAL_RATE);
          criteria.addSelectColumn(IS_TAXABLE);
          criteria.addSelectColumn(IS_INCLUSIVE_TAX);
          criteria.addSelectColumn(RETURN_DISC_ACC_ID);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static SalesReturn row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            SalesReturn obj = (SalesReturn) cls.newInstance();
            SalesReturnPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      SalesReturn obj)
        throws TorqueException
    {
        try
        {
                obj.setSalesReturnId(row.getValue(offset + 0).asString());
                  obj.setTransactionType(row.getValue(offset + 1).asInt());
                  obj.setTransactionId(row.getValue(offset + 2).asString());
                  obj.setLocationId(row.getValue(offset + 3).asString());
                  obj.setStatus(row.getValue(offset + 4).asInt());
                  obj.setTransactionNo(row.getValue(offset + 5).asString());
                  obj.setTransactionDate(row.getValue(offset + 6).asUtilDate());
                  obj.setReturnNo(row.getValue(offset + 7).asString());
                  obj.setReturnDate(row.getValue(offset + 8).asUtilDate());
                  obj.setCustomerId(row.getValue(offset + 9).asString());
                  obj.setCustomerName(row.getValue(offset + 10).asString());
                  obj.setTotalQty(row.getValue(offset + 11).asBigDecimal());
                  obj.setTotalTax(row.getValue(offset + 12).asBigDecimal());
                  obj.setTotalDiscountPct(row.getValue(offset + 13).asString());
                  obj.setTotalDiscount(row.getValue(offset + 14).asBigDecimal());
                  obj.setTotalAmount(row.getValue(offset + 15).asBigDecimal());
                  obj.setUserName(row.getValue(offset + 16).asString());
                  obj.setRemark(row.getValue(offset + 17).asString());
                  obj.setPaymentTypeId(row.getValue(offset + 18).asString());
                  obj.setCreateCreditMemo(row.getValue(offset + 19).asBoolean());
                  obj.setCurrencyId(row.getValue(offset + 20).asString());
                  obj.setCurrencyRate(row.getValue(offset + 21).asBigDecimal());
                  obj.setSalesId(row.getValue(offset + 22).asString());
                  obj.setReturnReasonId(row.getValue(offset + 23).asString());
                  obj.setCashReturn(row.getValue(offset + 24).asBoolean());
                  obj.setBankId(row.getValue(offset + 25).asString());
                  obj.setBankIssuer(row.getValue(offset + 26).asString());
                  obj.setDueDate(row.getValue(offset + 27).asUtilDate());
                  obj.setReferenceNo(row.getValue(offset + 28).asString());
                  obj.setCashFlowTypeId(row.getValue(offset + 29).asString());
                  obj.setRoundingAmount(row.getValue(offset + 30).asBigDecimal());
                  obj.setReturnedAmount(row.getValue(offset + 31).asBigDecimal());
                  obj.setFiscalRate(row.getValue(offset + 32).asBigDecimal());
                  obj.setIsTaxable(row.getValue(offset + 33).asBoolean());
                  obj.setIsInclusiveTax(row.getValue(offset + 34).asBoolean());
                  obj.setReturnDiscAccId(row.getValue(offset + 35).asString());
                  obj.setCancelBy(row.getValue(offset + 36).asString());
                  obj.setCancelDate(row.getValue(offset + 37).asUtilDate());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseSalesReturnPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_CREDIT_MEMO))
        {
            Object possibleBoolean = criteria.get(CREATE_CREDIT_MEMO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_CREDIT_MEMO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CASH_RETURN))
        {
            Object possibleBoolean = criteria.get(CASH_RETURN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CASH_RETURN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(SalesReturnPeer.row2Object(row, 1,
                SalesReturnPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseSalesReturnPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(SALES_RETURN_ID, criteria.remove(SALES_RETURN_ID));
                                                                                                                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_CREDIT_MEMO))
        {
            Object possibleBoolean = criteria.get(CREATE_CREDIT_MEMO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_CREDIT_MEMO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CASH_RETURN))
        {
            Object possibleBoolean = criteria.get(CASH_RETURN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CASH_RETURN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                        
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         SalesReturnPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CREATE_CREDIT_MEMO))
        {
            Object possibleBoolean = criteria.get(CREATE_CREDIT_MEMO);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CREATE_CREDIT_MEMO, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CASH_RETURN))
        {
            Object possibleBoolean = criteria.get(CASH_RETURN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CASH_RETURN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(SalesReturn obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(SalesReturn obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(SalesReturn obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(SalesReturn obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(SalesReturn) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(SalesReturn obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(SalesReturn) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(SalesReturn obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(SalesReturn) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(SalesReturn obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseSalesReturnPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(SALES_RETURN_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( SalesReturn obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(SALES_RETURN_ID, obj.getSalesReturnId());
              criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
              criteria.add(TRANSACTION_ID, obj.getTransactionId());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(TRANSACTION_NO, obj.getTransactionNo());
              criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
              criteria.add(RETURN_NO, obj.getReturnNo());
              criteria.add(RETURN_DATE, obj.getReturnDate());
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(CUSTOMER_NAME, obj.getCustomerName());
              criteria.add(TOTAL_QTY, obj.getTotalQty());
              criteria.add(TOTAL_TAX, obj.getTotalTax());
              criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
              criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
              criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
              criteria.add(USER_NAME, obj.getUserName());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
              criteria.add(CREATE_CREDIT_MEMO, obj.getCreateCreditMemo());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(SALES_ID, obj.getSalesId());
              criteria.add(RETURN_REASON_ID, obj.getReturnReasonId());
              criteria.add(CASH_RETURN, obj.getCashReturn());
              criteria.add(BANK_ID, obj.getBankId());
              criteria.add(BANK_ISSUER, obj.getBankIssuer());
              criteria.add(DUE_DATE, obj.getDueDate());
              criteria.add(REFERENCE_NO, obj.getReferenceNo());
              criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
              criteria.add(ROUNDING_AMOUNT, obj.getRoundingAmount());
              criteria.add(RETURNED_AMOUNT, obj.getReturnedAmount());
              criteria.add(FISCAL_RATE, obj.getFiscalRate());
              criteria.add(IS_TAXABLE, obj.getIsTaxable());
              criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
              criteria.add(RETURN_DISC_ACC_ID, obj.getReturnDiscAccId());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( SalesReturn obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(SALES_RETURN_ID, obj.getSalesReturnId());
                          criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
                          criteria.add(TRANSACTION_ID, obj.getTransactionId());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(TRANSACTION_NO, obj.getTransactionNo());
                          criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
                          criteria.add(RETURN_NO, obj.getReturnNo());
                          criteria.add(RETURN_DATE, obj.getReturnDate());
                          criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(CUSTOMER_NAME, obj.getCustomerName());
                          criteria.add(TOTAL_QTY, obj.getTotalQty());
                          criteria.add(TOTAL_TAX, obj.getTotalTax());
                          criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
                          criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
                          criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
                          criteria.add(USER_NAME, obj.getUserName());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
                          criteria.add(CREATE_CREDIT_MEMO, obj.getCreateCreditMemo());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(SALES_ID, obj.getSalesId());
                          criteria.add(RETURN_REASON_ID, obj.getReturnReasonId());
                          criteria.add(CASH_RETURN, obj.getCashReturn());
                          criteria.add(BANK_ID, obj.getBankId());
                          criteria.add(BANK_ISSUER, obj.getBankIssuer());
                          criteria.add(DUE_DATE, obj.getDueDate());
                          criteria.add(REFERENCE_NO, obj.getReferenceNo());
                          criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
                          criteria.add(ROUNDING_AMOUNT, obj.getRoundingAmount());
                          criteria.add(RETURNED_AMOUNT, obj.getReturnedAmount());
                          criteria.add(FISCAL_RATE, obj.getFiscalRate());
                          criteria.add(IS_TAXABLE, obj.getIsTaxable());
                          criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
                          criteria.add(RETURN_DISC_ACC_ID, obj.getReturnDiscAccId());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SalesReturn retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SalesReturn retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SalesReturn retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        SalesReturn retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SalesReturn retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (SalesReturn)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( SALES_RETURN_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
