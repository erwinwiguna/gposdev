package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to SalesTransaction
 */
public abstract class BaseSalesTransaction extends BaseObject
{
    /** The Peer class */
    private static final SalesTransactionPeer peer =
        new SalesTransactionPeer();

        
    /** The value for the salesTransactionId field */
    private String salesTransactionId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the invoiceNo field */
    private String invoiceNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
                                                
    /** The value for the totalDiscountId field */
    private String totalDiscountId = "";
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalTax field */
    private BigDecimal totalTax;
                                                
    /** The value for the courierId field */
    private String courierId = "";
                                                
    /** The value for the freightNo field */
    private String freightNo = "";
                                                
          
    /** The value for the shippingPrice field */
    private BigDecimal shippingPrice= bd_ZERO;
                                                
          
    /** The value for the totalExpense field */
    private BigDecimal totalExpense= bd_ZERO;
      
    /** The value for the totalCost field */
    private BigDecimal totalCost;
      
    /** The value for the salesId field */
    private String salesId;
                                                
    /** The value for the cashierName field */
    private String cashierName = "";
                                                
    /** The value for the remark field */
    private String remark = "";
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
                                          
    /** The value for the printTimes field */
    private int printTimes = 0;
                                                
          
    /** The value for the paidAmount field */
    private BigDecimal paidAmount= bd_ZERO;
                                                
          
    /** The value for the roundingAmount field */
    private BigDecimal roundingAmount= bd_ZERO;
                                                
          
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount= bd_ZERO;
                                                
          
    /** The value for the changeAmount field */
    private BigDecimal changeAmount= bd_ZERO;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
                                                
    /** The value for the shipTo field */
    private String shipTo = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
      
    /** The value for the paymentDate field */
    private Date paymentDate;
      
    /** The value for the paymentStatus field */
    private int paymentStatus;
                                                
    /** The value for the freightAccountId field */
    private String freightAccountId = "";
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
                                                
    /** The value for the sourceTransId field */
    private String sourceTransId = "";
                                                
    /** The value for the invoiceDiscAccId field */
    private String invoiceDiscAccId = "";
      
    /** The value for the isInclusiveFreight field */
    private boolean isInclusiveFreight;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
                                                        
    /** The value for the isPosTrans field */
    private boolean isPosTrans = false;
      
    /** The value for the deliveryDate field */
    private Date deliveryDate;
  
    
    /**
     * Get the SalesTransactionId
     *
     * @return String
     */
    public String getSalesTransactionId()
    {
        return salesTransactionId;
    }

                                              
    /**
     * Set the value of SalesTransactionId
     *
     * @param v new value
     */
    public void setSalesTransactionId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionId, v))
              {
            this.salesTransactionId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated SalesTransactionDetail
        if (collSalesTransactionDetails != null)
        {
            for (int i = 0; i < collSalesTransactionDetails.size(); i++)
            {
                ((SalesTransactionDetail) collSalesTransactionDetails.get(i))
                    .setSalesTransactionId(v);
            }
        }
                                }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceNo
     *
     * @return String
     */
    public String getInvoiceNo()
    {
        return invoiceNo;
    }

                        
    /**
     * Set the value of InvoiceNo
     *
     * @param v new value
     */
    public void setInvoiceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceNo, v))
              {
            this.invoiceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountId
     *
     * @return String
     */
    public String getTotalDiscountId()
    {
        return totalDiscountId;
    }

                        
    /**
     * Set the value of TotalDiscountId
     *
     * @param v new value
     */
    public void setTotalDiscountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountId, v))
              {
            this.totalDiscountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalTax()
    {
        return totalTax;
    }

                        
    /**
     * Set the value of TotalTax
     *
     * @param v new value
     */
    public void setTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalTax, v))
              {
            this.totalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightNo
     *
     * @return String
     */
    public String getFreightNo()
    {
        return freightNo;
    }

                        
    /**
     * Set the value of FreightNo
     *
     * @param v new value
     */
    public void setFreightNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightNo, v))
              {
            this.freightNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShippingPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getShippingPrice()
    {
        return shippingPrice;
    }

                        
    /**
     * Set the value of ShippingPrice
     *
     * @param v new value
     */
    public void setShippingPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.shippingPrice, v))
              {
            this.shippingPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalExpense
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalExpense()
    {
        return totalExpense;
    }

                        
    /**
     * Set the value of TotalExpense
     *
     * @param v new value
     */
    public void setTotalExpense(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalExpense, v))
              {
            this.totalExpense = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalCost
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalCost()
    {
        return totalCost;
    }

                        
    /**
     * Set the value of TotalCost
     *
     * @param v new value
     */
    public void setTotalCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalCost, v))
              {
            this.totalCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesId
     *
     * @return String
     */
    public String getSalesId()
    {
        return salesId;
    }

                        
    /**
     * Set the value of SalesId
     *
     * @param v new value
     */
    public void setSalesId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesId, v))
              {
            this.salesId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierName
     *
     * @return String
     */
    public String getCashierName()
    {
        return cashierName;
    }

                        
    /**
     * Set the value of CashierName
     *
     * @param v new value
     */
    public void setCashierName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierName, v))
              {
            this.cashierName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrintTimes
     *
     * @return int
     */
    public int getPrintTimes()
    {
        return printTimes;
    }

                        
    /**
     * Set the value of PrintTimes
     *
     * @param v new value
     */
    public void setPrintTimes(int v) 
    {
    
                  if (this.printTimes != v)
              {
            this.printTimes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaidAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaidAmount()
    {
        return paidAmount;
    }

                        
    /**
     * Set the value of PaidAmount
     *
     * @param v new value
     */
    public void setPaidAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paidAmount, v))
              {
            this.paidAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoundingAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRoundingAmount()
    {
        return roundingAmount;
    }

                        
    /**
     * Set the value of RoundingAmount
     *
     * @param v new value
     */
    public void setRoundingAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.roundingAmount, v))
              {
            this.roundingAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ChangeAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getChangeAmount()
    {
        return changeAmount;
    }

                        
    /**
     * Set the value of ChangeAmount
     *
     * @param v new value
     */
    public void setChangeAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.changeAmount, v))
              {
            this.changeAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipTo
     *
     * @return String
     */
    public String getShipTo()
    {
        return shipTo;
    }

                        
    /**
     * Set the value of ShipTo
     *
     * @param v new value
     */
    public void setShipTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shipTo, v))
              {
            this.shipTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentDate
     *
     * @return Date
     */
    public Date getPaymentDate()
    {
        return paymentDate;
    }

                        
    /**
     * Set the value of PaymentDate
     *
     * @param v new value
     */
    public void setPaymentDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentDate, v))
              {
            this.paymentDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentStatus
     *
     * @return int
     */
    public int getPaymentStatus()
    {
        return paymentStatus;
    }

                        
    /**
     * Set the value of PaymentStatus
     *
     * @param v new value
     */
    public void setPaymentStatus(int v) 
    {
    
                  if (this.paymentStatus != v)
              {
            this.paymentStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAccountId
     *
     * @return String
     */
    public String getFreightAccountId()
    {
        return freightAccountId;
    }

                        
    /**
     * Set the value of FreightAccountId
     *
     * @param v new value
     */
    public void setFreightAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightAccountId, v))
              {
            this.freightAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SourceTransId
     *
     * @return String
     */
    public String getSourceTransId()
    {
        return sourceTransId;
    }

                        
    /**
     * Set the value of SourceTransId
     *
     * @param v new value
     */
    public void setSourceTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sourceTransId, v))
              {
            this.sourceTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceDiscAccId
     *
     * @return String
     */
    public String getInvoiceDiscAccId()
    {
        return invoiceDiscAccId;
    }

                        
    /**
     * Set the value of InvoiceDiscAccId
     *
     * @param v new value
     */
    public void setInvoiceDiscAccId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceDiscAccId, v))
              {
            this.invoiceDiscAccId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveFreight
     *
     * @return boolean
     */
    public boolean getIsInclusiveFreight()
    {
        return isInclusiveFreight;
    }

                        
    /**
     * Set the value of IsInclusiveFreight
     *
     * @param v new value
     */
    public void setIsInclusiveFreight(boolean v) 
    {
    
                  if (this.isInclusiveFreight != v)
              {
            this.isInclusiveFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsPosTrans
     *
     * @return boolean
     */
    public boolean getIsPosTrans()
    {
        return isPosTrans;
    }

                        
    /**
     * Set the value of IsPosTrans
     *
     * @param v new value
     */
    public void setIsPosTrans(boolean v) 
    {
    
                  if (this.isPosTrans != v)
              {
            this.isPosTrans = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryDate
     *
     * @return Date
     */
    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

                        
    /**
     * Set the value of DeliveryDate
     *
     * @param v new value
     */
    public void setDeliveryDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryDate, v))
              {
            this.deliveryDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collSalesTransactionDetails
     */
    protected List collSalesTransactionDetails;

    /**
     * Temporary storage of collSalesTransactionDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initSalesTransactionDetails()
    {
        if (collSalesTransactionDetails == null)
        {
            collSalesTransactionDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a SalesTransactionDetail object to this object
     * through the SalesTransactionDetail foreign key attribute
     *
     * @param l SalesTransactionDetail
     * @throws TorqueException
     */
    public void addSalesTransactionDetail(SalesTransactionDetail l) throws TorqueException
    {
        getSalesTransactionDetails().add(l);
        l.setSalesTransaction((SalesTransaction) this);
    }

    /**
     * The criteria used to select the current contents of collSalesTransactionDetails
     */
    private Criteria lastSalesTransactionDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getSalesTransactionDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getSalesTransactionDetails() throws TorqueException
    {
              if (collSalesTransactionDetails == null)
        {
            collSalesTransactionDetails = getSalesTransactionDetails(new Criteria(10));
        }
        return collSalesTransactionDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesTransaction has previously
     * been saved, it will retrieve related SalesTransactionDetails from storage.
     * If this SalesTransaction is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getSalesTransactionDetails(Criteria criteria) throws TorqueException
    {
              if (collSalesTransactionDetails == null)
        {
            if (isNew())
            {
               collSalesTransactionDetails = new ArrayList();
            }
            else
            {
                        criteria.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, getSalesTransactionId() );
                        collSalesTransactionDetails = SalesTransactionDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, getSalesTransactionId());
                            if (!lastSalesTransactionDetailsCriteria.equals(criteria))
                {
                    collSalesTransactionDetails = SalesTransactionDetailPeer.doSelect(criteria);
                }
            }
        }
        lastSalesTransactionDetailsCriteria = criteria;

        return collSalesTransactionDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getSalesTransactionDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getSalesTransactionDetails(Connection con) throws TorqueException
    {
              if (collSalesTransactionDetails == null)
        {
            collSalesTransactionDetails = getSalesTransactionDetails(new Criteria(10), con);
        }
        return collSalesTransactionDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesTransaction has previously
     * been saved, it will retrieve related SalesTransactionDetails from storage.
     * If this SalesTransaction is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getSalesTransactionDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collSalesTransactionDetails == null)
        {
            if (isNew())
            {
               collSalesTransactionDetails = new ArrayList();
            }
            else
            {
                         criteria.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, getSalesTransactionId());
                         collSalesTransactionDetails = SalesTransactionDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, getSalesTransactionId());
                             if (!lastSalesTransactionDetailsCriteria.equals(criteria))
                 {
                     collSalesTransactionDetails = SalesTransactionDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastSalesTransactionDetailsCriteria = criteria;

         return collSalesTransactionDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesTransaction is new, it will return
     * an empty collection; or if this SalesTransaction has previously
     * been saved, it will retrieve related SalesTransactionDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SalesTransaction.
     */
    protected List getSalesTransactionDetailsJoinSalesTransaction(Criteria criteria)
        throws TorqueException
    {
                    if (collSalesTransactionDetails == null)
        {
            if (isNew())
            {
               collSalesTransactionDetails = new ArrayList();
            }
            else
            {
                              criteria.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, getSalesTransactionId());
                              collSalesTransactionDetails = SalesTransactionDetailPeer.doSelectJoinSalesTransaction(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, getSalesTransactionId());
                                    if (!lastSalesTransactionDetailsCriteria.equals(criteria))
            {
                collSalesTransactionDetails = SalesTransactionDetailPeer.doSelectJoinSalesTransaction(criteria);
            }
        }
        lastSalesTransactionDetailsCriteria = criteria;

        return collSalesTransactionDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SalesTransactionId");
              fieldNames.add("LocationId");
              fieldNames.add("Status");
              fieldNames.add("InvoiceNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("DueDate");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("TotalQty");
              fieldNames.add("TotalAmount");
              fieldNames.add("TotalDiscountId");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalTax");
              fieldNames.add("CourierId");
              fieldNames.add("FreightNo");
              fieldNames.add("ShippingPrice");
              fieldNames.add("TotalExpense");
              fieldNames.add("TotalCost");
              fieldNames.add("SalesId");
              fieldNames.add("CashierName");
              fieldNames.add("Remark");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("PrintTimes");
              fieldNames.add("PaidAmount");
              fieldNames.add("RoundingAmount");
              fieldNames.add("PaymentAmount");
              fieldNames.add("ChangeAmount");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("ShipTo");
              fieldNames.add("FobId");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("PaymentDate");
              fieldNames.add("PaymentStatus");
              fieldNames.add("FreightAccountId");
              fieldNames.add("FiscalRate");
              fieldNames.add("SourceTransId");
              fieldNames.add("InvoiceDiscAccId");
              fieldNames.add("IsInclusiveFreight");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("IsPosTrans");
              fieldNames.add("DeliveryDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SalesTransactionId"))
        {
                return getSalesTransactionId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("InvoiceNo"))
        {
                return getInvoiceNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("TotalDiscountId"))
        {
                return getTotalDiscountId();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalTax"))
        {
                return getTotalTax();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("FreightNo"))
        {
                return getFreightNo();
            }
          if (name.equals("ShippingPrice"))
        {
                return getShippingPrice();
            }
          if (name.equals("TotalExpense"))
        {
                return getTotalExpense();
            }
          if (name.equals("TotalCost"))
        {
                return getTotalCost();
            }
          if (name.equals("SalesId"))
        {
                return getSalesId();
            }
          if (name.equals("CashierName"))
        {
                return getCashierName();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("PrintTimes"))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals("PaidAmount"))
        {
                return getPaidAmount();
            }
          if (name.equals("RoundingAmount"))
        {
                return getRoundingAmount();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("ChangeAmount"))
        {
                return getChangeAmount();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("ShipTo"))
        {
                return getShipTo();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("PaymentDate"))
        {
                return getPaymentDate();
            }
          if (name.equals("PaymentStatus"))
        {
                return Integer.valueOf(getPaymentStatus());
            }
          if (name.equals("FreightAccountId"))
        {
                return getFreightAccountId();
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("SourceTransId"))
        {
                return getSourceTransId();
            }
          if (name.equals("InvoiceDiscAccId"))
        {
                return getInvoiceDiscAccId();
            }
          if (name.equals("IsInclusiveFreight"))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("IsPosTrans"))
        {
                return Boolean.valueOf(getIsPosTrans());
            }
          if (name.equals("DeliveryDate"))
        {
                return getDeliveryDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SalesTransactionPeer.SALES_TRANSACTION_ID))
        {
                return getSalesTransactionId();
            }
          if (name.equals(SalesTransactionPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(SalesTransactionPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(SalesTransactionPeer.INVOICE_NO))
        {
                return getInvoiceNo();
            }
          if (name.equals(SalesTransactionPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(SalesTransactionPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(SalesTransactionPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(SalesTransactionPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_DISCOUNT_ID))
        {
                return getTotalDiscountId();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_TAX))
        {
                return getTotalTax();
            }
          if (name.equals(SalesTransactionPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(SalesTransactionPeer.FREIGHT_NO))
        {
                return getFreightNo();
            }
          if (name.equals(SalesTransactionPeer.SHIPPING_PRICE))
        {
                return getShippingPrice();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_EXPENSE))
        {
                return getTotalExpense();
            }
          if (name.equals(SalesTransactionPeer.TOTAL_COST))
        {
                return getTotalCost();
            }
          if (name.equals(SalesTransactionPeer.SALES_ID))
        {
                return getSalesId();
            }
          if (name.equals(SalesTransactionPeer.CASHIER_NAME))
        {
                return getCashierName();
            }
          if (name.equals(SalesTransactionPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(SalesTransactionPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(SalesTransactionPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(SalesTransactionPeer.PRINT_TIMES))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals(SalesTransactionPeer.PAID_AMOUNT))
        {
                return getPaidAmount();
            }
          if (name.equals(SalesTransactionPeer.ROUNDING_AMOUNT))
        {
                return getRoundingAmount();
            }
          if (name.equals(SalesTransactionPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(SalesTransactionPeer.CHANGE_AMOUNT))
        {
                return getChangeAmount();
            }
          if (name.equals(SalesTransactionPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(SalesTransactionPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(SalesTransactionPeer.SHIP_TO))
        {
                return getShipTo();
            }
          if (name.equals(SalesTransactionPeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(SalesTransactionPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(SalesTransactionPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(SalesTransactionPeer.PAYMENT_DATE))
        {
                return getPaymentDate();
            }
          if (name.equals(SalesTransactionPeer.PAYMENT_STATUS))
        {
                return Integer.valueOf(getPaymentStatus());
            }
          if (name.equals(SalesTransactionPeer.FREIGHT_ACCOUNT_ID))
        {
                return getFreightAccountId();
            }
          if (name.equals(SalesTransactionPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(SalesTransactionPeer.SOURCE_TRANS_ID))
        {
                return getSourceTransId();
            }
          if (name.equals(SalesTransactionPeer.INVOICE_DISC_ACC_ID))
        {
                return getInvoiceDiscAccId();
            }
          if (name.equals(SalesTransactionPeer.IS_INCLUSIVE_FREIGHT))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals(SalesTransactionPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(SalesTransactionPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(SalesTransactionPeer.IS_POS_TRANS))
        {
                return Boolean.valueOf(getIsPosTrans());
            }
          if (name.equals(SalesTransactionPeer.DELIVERY_DATE))
        {
                return getDeliveryDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSalesTransactionId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 3)
        {
                return getInvoiceNo();
            }
              if (pos == 4)
        {
                return getTransactionDate();
            }
              if (pos == 5)
        {
                return getDueDate();
            }
              if (pos == 6)
        {
                return getCustomerId();
            }
              if (pos == 7)
        {
                return getCustomerName();
            }
              if (pos == 8)
        {
                return getTotalQty();
            }
              if (pos == 9)
        {
                return getTotalAmount();
            }
              if (pos == 10)
        {
                return getTotalDiscountId();
            }
              if (pos == 11)
        {
                return getTotalDiscountPct();
            }
              if (pos == 12)
        {
                return getTotalDiscount();
            }
              if (pos == 13)
        {
                return getTotalTax();
            }
              if (pos == 14)
        {
                return getCourierId();
            }
              if (pos == 15)
        {
                return getFreightNo();
            }
              if (pos == 16)
        {
                return getShippingPrice();
            }
              if (pos == 17)
        {
                return getTotalExpense();
            }
              if (pos == 18)
        {
                return getTotalCost();
            }
              if (pos == 19)
        {
                return getSalesId();
            }
              if (pos == 20)
        {
                return getCashierName();
            }
              if (pos == 21)
        {
                return getRemark();
            }
              if (pos == 22)
        {
                return getPaymentTypeId();
            }
              if (pos == 23)
        {
                return getPaymentTermId();
            }
              if (pos == 24)
        {
                return Integer.valueOf(getPrintTimes());
            }
              if (pos == 25)
        {
                return getPaidAmount();
            }
              if (pos == 26)
        {
                return getRoundingAmount();
            }
              if (pos == 27)
        {
                return getPaymentAmount();
            }
              if (pos == 28)
        {
                return getChangeAmount();
            }
              if (pos == 29)
        {
                return getCurrencyId();
            }
              if (pos == 30)
        {
                return getCurrencyRate();
            }
              if (pos == 31)
        {
                return getShipTo();
            }
              if (pos == 32)
        {
                return getFobId();
            }
              if (pos == 33)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 34)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 35)
        {
                return getPaymentDate();
            }
              if (pos == 36)
        {
                return Integer.valueOf(getPaymentStatus());
            }
              if (pos == 37)
        {
                return getFreightAccountId();
            }
              if (pos == 38)
        {
                return getFiscalRate();
            }
              if (pos == 39)
        {
                return getSourceTransId();
            }
              if (pos == 40)
        {
                return getInvoiceDiscAccId();
            }
              if (pos == 41)
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
              if (pos == 42)
        {
                return getCancelBy();
            }
              if (pos == 43)
        {
                return getCancelDate();
            }
              if (pos == 44)
        {
                return Boolean.valueOf(getIsPosTrans());
            }
              if (pos == 45)
        {
                return getDeliveryDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SalesTransactionPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SalesTransactionPeer.doInsert((SalesTransaction) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SalesTransactionPeer.doUpdate((SalesTransaction) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collSalesTransactionDetails != null)
            {
                for (int i = 0; i < collSalesTransactionDetails.size(); i++)
                {
                    ((SalesTransactionDetail) collSalesTransactionDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key salesTransactionId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setSalesTransactionId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setSalesTransactionId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSalesTransactionId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public SalesTransaction copy() throws TorqueException
    {
        return copyInto(new SalesTransaction());
    }
  
    protected SalesTransaction copyInto(SalesTransaction copyObj) throws TorqueException
    {
          copyObj.setSalesTransactionId(salesTransactionId);
          copyObj.setLocationId(locationId);
          copyObj.setStatus(status);
          copyObj.setInvoiceNo(invoiceNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setDueDate(dueDate);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setTotalQty(totalQty);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setTotalDiscountId(totalDiscountId);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalTax(totalTax);
          copyObj.setCourierId(courierId);
          copyObj.setFreightNo(freightNo);
          copyObj.setShippingPrice(shippingPrice);
          copyObj.setTotalExpense(totalExpense);
          copyObj.setTotalCost(totalCost);
          copyObj.setSalesId(salesId);
          copyObj.setCashierName(cashierName);
          copyObj.setRemark(remark);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setPrintTimes(printTimes);
          copyObj.setPaidAmount(paidAmount);
          copyObj.setRoundingAmount(roundingAmount);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setChangeAmount(changeAmount);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setShipTo(shipTo);
          copyObj.setFobId(fobId);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setPaymentDate(paymentDate);
          copyObj.setPaymentStatus(paymentStatus);
          copyObj.setFreightAccountId(freightAccountId);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setSourceTransId(sourceTransId);
          copyObj.setInvoiceDiscAccId(invoiceDiscAccId);
          copyObj.setIsInclusiveFreight(isInclusiveFreight);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setIsPosTrans(isPosTrans);
          copyObj.setDeliveryDate(deliveryDate);
  
                    copyObj.setSalesTransactionId((String)null);
                                                                                                                                                                                                                                                                                          
                                      
                            
        List v = getSalesTransactionDetails();
        for (int i = 0; i < v.size(); i++)
        {
            SalesTransactionDetail obj = (SalesTransactionDetail) v.get(i);
            copyObj.addSalesTransactionDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SalesTransactionPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("SalesTransaction\n");
        str.append("----------------\n")
           .append("SalesTransactionId   : ")
           .append(getSalesTransactionId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("InvoiceNo            : ")
           .append(getInvoiceNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("TotalDiscountId      : ")
           .append(getTotalDiscountId())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalTax             : ")
           .append(getTotalTax())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("FreightNo            : ")
           .append(getFreightNo())
           .append("\n")
           .append("ShippingPrice        : ")
           .append(getShippingPrice())
           .append("\n")
           .append("TotalExpense         : ")
           .append(getTotalExpense())
           .append("\n")
           .append("TotalCost            : ")
           .append(getTotalCost())
           .append("\n")
           .append("SalesId              : ")
           .append(getSalesId())
           .append("\n")
           .append("CashierName          : ")
           .append(getCashierName())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("PrintTimes           : ")
           .append(getPrintTimes())
           .append("\n")
           .append("PaidAmount           : ")
           .append(getPaidAmount())
           .append("\n")
           .append("RoundingAmount       : ")
           .append(getRoundingAmount())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("ChangeAmount         : ")
           .append(getChangeAmount())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("ShipTo               : ")
           .append(getShipTo())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("PaymentDate          : ")
           .append(getPaymentDate())
           .append("\n")
           .append("PaymentStatus        : ")
           .append(getPaymentStatus())
           .append("\n")
           .append("FreightAccountId     : ")
           .append(getFreightAccountId())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("SourceTransId        : ")
           .append(getSourceTransId())
           .append("\n")
           .append("InvoiceDiscAccId     : ")
           .append(getInvoiceDiscAccId())
           .append("\n")
           .append("IsInclusiveFreight   : ")
           .append(getIsInclusiveFreight())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("IsPosTrans           : ")
           .append(getIsPosTrans())
           .append("\n")
           .append("DeliveryDate         : ")
           .append(getDeliveryDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
