package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to SalesTransactionDetail
 */
public abstract class BaseSalesTransactionDetail extends BaseObject
{
    /** The Peer class */
    private static final SalesTransactionDetailPeer peer =
        new SalesTransactionDetailPeer();

        
    /** The value for the salesTransactionDetailId field */
    private String salesTransactionDetailId;
                                                
    /** The value for the salesOrderId field */
    private String salesOrderId = "";
                                                
    /** The value for the deliveryOrderId field */
    private String deliveryOrderId = "";
                                                
    /** The value for the deliveryOrderDetailId field */
    private String deliveryOrderDetailId = "";
      
    /** The value for the salesTransactionId field */
    private String salesTransactionId;
      
    /** The value for the indexNo field */
    private int indexNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
                                                
          
    /** The value for the returnedQty field */
    private BigDecimal returnedQty= bd_ZERO;
      
    /** The value for the itemPrice field */
    private BigDecimal itemPrice;
      
    /** The value for the taxId field */
    private String taxId;
      
    /** The value for the taxAmount field */
    private BigDecimal taxAmount;
      
    /** The value for the subTotalTax field */
    private BigDecimal subTotalTax;
                                                
    /** The value for the discountId field */
    private String discountId = "";
                                                
    /** The value for the discount field */
    private String discount = "0";
      
    /** The value for the subTotalDisc field */
    private BigDecimal subTotalDisc;
      
    /** The value for the itemCost field */
    private BigDecimal itemCost;
      
    /** The value for the subTotalCost field */
    private BigDecimal subTotalCost;
      
    /** The value for the subTotal field */
    private BigDecimal subTotal;
                                                
    /** The value for the projectId field */
    private String projectId = "";
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
                                                
    /** The value for the employeeId field */
    private String employeeId = "";
      
    /** The value for the changedManual field */
    private boolean changedManual;
  
    
    /**
     * Get the SalesTransactionDetailId
     *
     * @return String
     */
    public String getSalesTransactionDetailId()
    {
        return salesTransactionDetailId;
    }

                        
    /**
     * Set the value of SalesTransactionDetailId
     *
     * @param v new value
     */
    public void setSalesTransactionDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionDetailId, v))
              {
            this.salesTransactionDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesOrderId
     *
     * @return String
     */
    public String getSalesOrderId()
    {
        return salesOrderId;
    }

                        
    /**
     * Set the value of SalesOrderId
     *
     * @param v new value
     */
    public void setSalesOrderId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesOrderId, v))
              {
            this.salesOrderId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryOrderId
     *
     * @return String
     */
    public String getDeliveryOrderId()
    {
        return deliveryOrderId;
    }

                        
    /**
     * Set the value of DeliveryOrderId
     *
     * @param v new value
     */
    public void setDeliveryOrderId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryOrderId, v))
              {
            this.deliveryOrderId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryOrderDetailId
     *
     * @return String
     */
    public String getDeliveryOrderDetailId()
    {
        return deliveryOrderDetailId;
    }

                        
    /**
     * Set the value of DeliveryOrderDetailId
     *
     * @param v new value
     */
    public void setDeliveryOrderDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryOrderDetailId, v))
              {
            this.deliveryOrderDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesTransactionId
     *
     * @return String
     */
    public String getSalesTransactionId()
    {
        return salesTransactionId;
    }

                              
    /**
     * Set the value of SalesTransactionId
     *
     * @param v new value
     */
    public void setSalesTransactionId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionId, v))
              {
            this.salesTransactionId = v;
            setModified(true);
        }
    
                          
                if (aSalesTransaction != null && !ObjectUtils.equals(aSalesTransaction.getSalesTransactionId(), v))
                {
            aSalesTransaction = null;
        }
      
              }
  
    /**
     * Get the IndexNo
     *
     * @return int
     */
    public int getIndexNo()
    {
        return indexNo;
    }

                        
    /**
     * Set the value of IndexNo
     *
     * @param v new value
     */
    public void setIndexNo(int v) 
    {
    
                  if (this.indexNo != v)
              {
            this.indexNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnedQty
     *
     * @return BigDecimal
     */
    public BigDecimal getReturnedQty()
    {
        return returnedQty;
    }

                        
    /**
     * Set the value of ReturnedQty
     *
     * @param v new value
     */
    public void setReturnedQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.returnedQty, v))
              {
            this.returnedQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

                        
    /**
     * Set the value of TaxAmount
     *
     * @param v new value
     */
    public void setTaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.taxAmount, v))
              {
            this.taxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotalTax()
    {
        return subTotalTax;
    }

                        
    /**
     * Set the value of SubTotalTax
     *
     * @param v new value
     */
    public void setSubTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotalTax, v))
              {
            this.subTotalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountId
     *
     * @return String
     */
    public String getDiscountId()
    {
        return discountId;
    }

                        
    /**
     * Set the value of DiscountId
     *
     * @param v new value
     */
    public void setDiscountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountId, v))
              {
            this.discountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Discount
     *
     * @return String
     */
    public String getDiscount()
    {
        return discount;
    }

                        
    /**
     * Set the value of Discount
     *
     * @param v new value
     */
    public void setDiscount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discount, v))
              {
            this.discount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotalDisc
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotalDisc()
    {
        return subTotalDisc;
    }

                        
    /**
     * Set the value of SubTotalDisc
     *
     * @param v new value
     */
    public void setSubTotalDisc(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotalDisc, v))
              {
            this.subTotalDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCost
     *
     * @return BigDecimal
     */
    public BigDecimal getItemCost()
    {
        return itemCost;
    }

                        
    /**
     * Set the value of ItemCost
     *
     * @param v new value
     */
    public void setItemCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCost, v))
              {
            this.itemCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotalCost
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotalCost()
    {
        return subTotalCost;
    }

                        
    /**
     * Set the value of SubTotalCost
     *
     * @param v new value
     */
    public void setSubTotalCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotalCost, v))
              {
            this.subTotalCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotal
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotal()
    {
        return subTotal;
    }

                        
    /**
     * Set the value of SubTotal
     *
     * @param v new value
     */
    public void setSubTotal(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotal, v))
              {
            this.subTotal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmployeeId
     *
     * @return String
     */
    public String getEmployeeId()
    {
        return employeeId;
    }

                        
    /**
     * Set the value of EmployeeId
     *
     * @param v new value
     */
    public void setEmployeeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.employeeId, v))
              {
            this.employeeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ChangedManual
     *
     * @return boolean
     */
    public boolean getChangedManual()
    {
        return changedManual;
    }

                        
    /**
     * Set the value of ChangedManual
     *
     * @param v new value
     */
    public void setChangedManual(boolean v) 
    {
    
                  if (this.changedManual != v)
              {
            this.changedManual = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private SalesTransaction aSalesTransaction;

    /**
     * Declares an association between this object and a SalesTransaction object
     *
     * @param v SalesTransaction
     * @throws TorqueException
     */
    public void setSalesTransaction(SalesTransaction v) throws TorqueException
    {
            if (v == null)
        {
                  setSalesTransactionId((String) null);
              }
        else
        {
            setSalesTransactionId(v.getSalesTransactionId());
        }
            aSalesTransaction = v;
    }

                                            
    /**
     * Get the associated SalesTransaction object
     *
     * @return the associated SalesTransaction object
     * @throws TorqueException
     */
    public SalesTransaction getSalesTransaction() throws TorqueException
    {
        if (aSalesTransaction == null && (!ObjectUtils.equals(this.salesTransactionId, null)))
        {
                          aSalesTransaction = SalesTransactionPeer.retrieveByPK(SimpleKey.keyFor(this.salesTransactionId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               SalesTransaction obj = SalesTransactionPeer.retrieveByPK(this.salesTransactionId);
               obj.addSalesTransactionDetails(this);
            */
        }
        return aSalesTransaction;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setSalesTransactionKey(ObjectKey key) throws TorqueException
    {
      
                        setSalesTransactionId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SalesTransactionDetailId");
              fieldNames.add("SalesOrderId");
              fieldNames.add("DeliveryOrderId");
              fieldNames.add("DeliveryOrderDetailId");
              fieldNames.add("SalesTransactionId");
              fieldNames.add("IndexNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("Description");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("Qty");
              fieldNames.add("QtyBase");
              fieldNames.add("ReturnedQty");
              fieldNames.add("ItemPrice");
              fieldNames.add("TaxId");
              fieldNames.add("TaxAmount");
              fieldNames.add("SubTotalTax");
              fieldNames.add("DiscountId");
              fieldNames.add("Discount");
              fieldNames.add("SubTotalDisc");
              fieldNames.add("ItemCost");
              fieldNames.add("SubTotalCost");
              fieldNames.add("SubTotal");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames.add("EmployeeId");
              fieldNames.add("ChangedManual");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SalesTransactionDetailId"))
        {
                return getSalesTransactionDetailId();
            }
          if (name.equals("SalesOrderId"))
        {
                return getSalesOrderId();
            }
          if (name.equals("DeliveryOrderId"))
        {
                return getDeliveryOrderId();
            }
          if (name.equals("DeliveryOrderDetailId"))
        {
                return getDeliveryOrderDetailId();
            }
          if (name.equals("SalesTransactionId"))
        {
                return getSalesTransactionId();
            }
          if (name.equals("IndexNo"))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("ReturnedQty"))
        {
                return getReturnedQty();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("TaxAmount"))
        {
                return getTaxAmount();
            }
          if (name.equals("SubTotalTax"))
        {
                return getSubTotalTax();
            }
          if (name.equals("DiscountId"))
        {
                return getDiscountId();
            }
          if (name.equals("Discount"))
        {
                return getDiscount();
            }
          if (name.equals("SubTotalDisc"))
        {
                return getSubTotalDisc();
            }
          if (name.equals("ItemCost"))
        {
                return getItemCost();
            }
          if (name.equals("SubTotalCost"))
        {
                return getSubTotalCost();
            }
          if (name.equals("SubTotal"))
        {
                return getSubTotal();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("EmployeeId"))
        {
                return getEmployeeId();
            }
          if (name.equals("ChangedManual"))
        {
                return Boolean.valueOf(getChangedManual());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SalesTransactionDetailPeer.SALES_TRANSACTION_DETAIL_ID))
        {
                return getSalesTransactionDetailId();
            }
          if (name.equals(SalesTransactionDetailPeer.SALES_ORDER_ID))
        {
                return getSalesOrderId();
            }
          if (name.equals(SalesTransactionDetailPeer.DELIVERY_ORDER_ID))
        {
                return getDeliveryOrderId();
            }
          if (name.equals(SalesTransactionDetailPeer.DELIVERY_ORDER_DETAIL_ID))
        {
                return getDeliveryOrderDetailId();
            }
          if (name.equals(SalesTransactionDetailPeer.SALES_TRANSACTION_ID))
        {
                return getSalesTransactionId();
            }
          if (name.equals(SalesTransactionDetailPeer.INDEX_NO))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals(SalesTransactionDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(SalesTransactionDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(SalesTransactionDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(SalesTransactionDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(SalesTransactionDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(SalesTransactionDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(SalesTransactionDetailPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(SalesTransactionDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(SalesTransactionDetailPeer.RETURNED_QTY))
        {
                return getReturnedQty();
            }
          if (name.equals(SalesTransactionDetailPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          if (name.equals(SalesTransactionDetailPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(SalesTransactionDetailPeer.TAX_AMOUNT))
        {
                return getTaxAmount();
            }
          if (name.equals(SalesTransactionDetailPeer.SUB_TOTAL_TAX))
        {
                return getSubTotalTax();
            }
          if (name.equals(SalesTransactionDetailPeer.DISCOUNT_ID))
        {
                return getDiscountId();
            }
          if (name.equals(SalesTransactionDetailPeer.DISCOUNT))
        {
                return getDiscount();
            }
          if (name.equals(SalesTransactionDetailPeer.SUB_TOTAL_DISC))
        {
                return getSubTotalDisc();
            }
          if (name.equals(SalesTransactionDetailPeer.ITEM_COST))
        {
                return getItemCost();
            }
          if (name.equals(SalesTransactionDetailPeer.SUB_TOTAL_COST))
        {
                return getSubTotalCost();
            }
          if (name.equals(SalesTransactionDetailPeer.SUB_TOTAL))
        {
                return getSubTotal();
            }
          if (name.equals(SalesTransactionDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(SalesTransactionDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(SalesTransactionDetailPeer.EMPLOYEE_ID))
        {
                return getEmployeeId();
            }
          if (name.equals(SalesTransactionDetailPeer.CHANGED_MANUAL))
        {
                return Boolean.valueOf(getChangedManual());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSalesTransactionDetailId();
            }
              if (pos == 1)
        {
                return getSalesOrderId();
            }
              if (pos == 2)
        {
                return getDeliveryOrderId();
            }
              if (pos == 3)
        {
                return getDeliveryOrderDetailId();
            }
              if (pos == 4)
        {
                return getSalesTransactionId();
            }
              if (pos == 5)
        {
                return Integer.valueOf(getIndexNo());
            }
              if (pos == 6)
        {
                return getItemId();
            }
              if (pos == 7)
        {
                return getItemCode();
            }
              if (pos == 8)
        {
                return getItemName();
            }
              if (pos == 9)
        {
                return getDescription();
            }
              if (pos == 10)
        {
                return getUnitId();
            }
              if (pos == 11)
        {
                return getUnitCode();
            }
              if (pos == 12)
        {
                return getQty();
            }
              if (pos == 13)
        {
                return getQtyBase();
            }
              if (pos == 14)
        {
                return getReturnedQty();
            }
              if (pos == 15)
        {
                return getItemPrice();
            }
              if (pos == 16)
        {
                return getTaxId();
            }
              if (pos == 17)
        {
                return getTaxAmount();
            }
              if (pos == 18)
        {
                return getSubTotalTax();
            }
              if (pos == 19)
        {
                return getDiscountId();
            }
              if (pos == 20)
        {
                return getDiscount();
            }
              if (pos == 21)
        {
                return getSubTotalDisc();
            }
              if (pos == 22)
        {
                return getItemCost();
            }
              if (pos == 23)
        {
                return getSubTotalCost();
            }
              if (pos == 24)
        {
                return getSubTotal();
            }
              if (pos == 25)
        {
                return getProjectId();
            }
              if (pos == 26)
        {
                return getDepartmentId();
            }
              if (pos == 27)
        {
                return getEmployeeId();
            }
              if (pos == 28)
        {
                return Boolean.valueOf(getChangedManual());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SalesTransactionDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SalesTransactionDetailPeer.doInsert((SalesTransactionDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SalesTransactionDetailPeer.doUpdate((SalesTransactionDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key salesTransactionDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setSalesTransactionDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setSalesTransactionDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSalesTransactionDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public SalesTransactionDetail copy() throws TorqueException
    {
        return copyInto(new SalesTransactionDetail());
    }
  
    protected SalesTransactionDetail copyInto(SalesTransactionDetail copyObj) throws TorqueException
    {
          copyObj.setSalesTransactionDetailId(salesTransactionDetailId);
          copyObj.setSalesOrderId(salesOrderId);
          copyObj.setDeliveryOrderId(deliveryOrderId);
          copyObj.setDeliveryOrderDetailId(deliveryOrderDetailId);
          copyObj.setSalesTransactionId(salesTransactionId);
          copyObj.setIndexNo(indexNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setDescription(description);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setQty(qty);
          copyObj.setQtyBase(qtyBase);
          copyObj.setReturnedQty(returnedQty);
          copyObj.setItemPrice(itemPrice);
          copyObj.setTaxId(taxId);
          copyObj.setTaxAmount(taxAmount);
          copyObj.setSubTotalTax(subTotalTax);
          copyObj.setDiscountId(discountId);
          copyObj.setDiscount(discount);
          copyObj.setSubTotalDisc(subTotalDisc);
          copyObj.setItemCost(itemCost);
          copyObj.setSubTotalCost(subTotalCost);
          copyObj.setSubTotal(subTotal);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setEmployeeId(employeeId);
          copyObj.setChangedManual(changedManual);
  
                    copyObj.setSalesTransactionDetailId((String)null);
                                                                                                                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SalesTransactionDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("SalesTransactionDetail\n");
        str.append("----------------------\n")
            .append("SalesTransactionDetailId   : ")
           .append(getSalesTransactionDetailId())
           .append("\n")
           .append("SalesOrderId         : ")
           .append(getSalesOrderId())
           .append("\n")
           .append("DeliveryOrderId      : ")
           .append(getDeliveryOrderId())
           .append("\n")
            .append("DeliveryOrderDetailId   : ")
           .append(getDeliveryOrderDetailId())
           .append("\n")
           .append("SalesTransactionId   : ")
           .append(getSalesTransactionId())
           .append("\n")
           .append("IndexNo              : ")
           .append(getIndexNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("ReturnedQty          : ")
           .append(getReturnedQty())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("TaxAmount            : ")
           .append(getTaxAmount())
           .append("\n")
           .append("SubTotalTax          : ")
           .append(getSubTotalTax())
           .append("\n")
           .append("DiscountId           : ")
           .append(getDiscountId())
           .append("\n")
           .append("Discount             : ")
           .append(getDiscount())
           .append("\n")
           .append("SubTotalDisc         : ")
           .append(getSubTotalDisc())
           .append("\n")
           .append("ItemCost             : ")
           .append(getItemCost())
           .append("\n")
           .append("SubTotalCost         : ")
           .append(getSubTotalCost())
           .append("\n")
           .append("SubTotal             : ")
           .append(getSubTotal())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("EmployeeId           : ")
           .append(getEmployeeId())
           .append("\n")
           .append("ChangedManual        : ")
           .append(getChangedManual())
           .append("\n")
        ;
        return(str.toString());
    }
}
