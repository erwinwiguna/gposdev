package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Shift
 */
public abstract class BaseShift extends BaseObject
{
    /** The Peer class */
    private static final ShiftPeer peer =
        new ShiftPeer();

        
    /** The value for the shiftId field */
    private String shiftId;
      
    /** The value for the shiftCode field */
    private String shiftCode;
      
    /** The value for the shiftName field */
    private String shiftName;
      
    /** The value for the startHour field */
    private String startHour;
      
    /** The value for the endHour field */
    private String endHour;
  
    
    /**
     * Get the ShiftId
     *
     * @return String
     */
    public String getShiftId()
    {
        return shiftId;
    }

                        
    /**
     * Set the value of ShiftId
     *
     * @param v new value
     */
    public void setShiftId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shiftId, v))
              {
            this.shiftId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShiftCode
     *
     * @return String
     */
    public String getShiftCode()
    {
        return shiftCode;
    }

                        
    /**
     * Set the value of ShiftCode
     *
     * @param v new value
     */
    public void setShiftCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shiftCode, v))
              {
            this.shiftCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShiftName
     *
     * @return String
     */
    public String getShiftName()
    {
        return shiftName;
    }

                        
    /**
     * Set the value of ShiftName
     *
     * @param v new value
     */
    public void setShiftName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shiftName, v))
              {
            this.shiftName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartHour
     *
     * @return String
     */
    public String getStartHour()
    {
        return startHour;
    }

                        
    /**
     * Set the value of StartHour
     *
     * @param v new value
     */
    public void setStartHour(String v) 
    {
    
                  if (!ObjectUtils.equals(this.startHour, v))
              {
            this.startHour = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndHour
     *
     * @return String
     */
    public String getEndHour()
    {
        return endHour;
    }

                        
    /**
     * Set the value of EndHour
     *
     * @param v new value
     */
    public void setEndHour(String v) 
    {
    
                  if (!ObjectUtils.equals(this.endHour, v))
              {
            this.endHour = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ShiftId");
              fieldNames.add("ShiftCode");
              fieldNames.add("ShiftName");
              fieldNames.add("StartHour");
              fieldNames.add("EndHour");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ShiftId"))
        {
                return getShiftId();
            }
          if (name.equals("ShiftCode"))
        {
                return getShiftCode();
            }
          if (name.equals("ShiftName"))
        {
                return getShiftName();
            }
          if (name.equals("StartHour"))
        {
                return getStartHour();
            }
          if (name.equals("EndHour"))
        {
                return getEndHour();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ShiftPeer.SHIFT_ID))
        {
                return getShiftId();
            }
          if (name.equals(ShiftPeer.SHIFT_CODE))
        {
                return getShiftCode();
            }
          if (name.equals(ShiftPeer.SHIFT_NAME))
        {
                return getShiftName();
            }
          if (name.equals(ShiftPeer.START_HOUR))
        {
                return getStartHour();
            }
          if (name.equals(ShiftPeer.END_HOUR))
        {
                return getEndHour();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getShiftId();
            }
              if (pos == 1)
        {
                return getShiftCode();
            }
              if (pos == 2)
        {
                return getShiftName();
            }
              if (pos == 3)
        {
                return getStartHour();
            }
              if (pos == 4)
        {
                return getEndHour();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ShiftPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ShiftPeer.doInsert((Shift) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ShiftPeer.doUpdate((Shift) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key shiftId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setShiftId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setShiftId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getShiftId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Shift copy() throws TorqueException
    {
        return copyInto(new Shift());
    }
  
    protected Shift copyInto(Shift copyObj) throws TorqueException
    {
          copyObj.setShiftId(shiftId);
          copyObj.setShiftCode(shiftCode);
          copyObj.setShiftName(shiftName);
          copyObj.setStartHour(startHour);
          copyObj.setEndHour(endHour);
  
                    copyObj.setShiftId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ShiftPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Shift\n");
        str.append("-----\n")
           .append("ShiftId              : ")
           .append(getShiftId())
           .append("\n")
           .append("ShiftCode            : ")
           .append(getShiftCode())
           .append("\n")
           .append("ShiftName            : ")
           .append(getShiftName())
           .append("\n")
           .append("StartHour            : ")
           .append(getStartHour())
           .append("\n")
           .append("EndHour              : ")
           .append(getEndHour())
           .append("\n")
        ;
        return(str.toString());
    }
}
