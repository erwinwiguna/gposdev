package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Synchronization
 */
public abstract class BaseSynchronization extends BaseObject
{
    /** The Peer class */
    private static final SynchronizationPeer peer =
        new SynchronizationPeer();

        
    /** The value for the synchronizationId field */
    private String synchronizationId;
      
    /** The value for the syncType field */
    private int syncType;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the syncDate field */
    private Date syncDate;
      
    /** The value for the fileName field */
    private String fileName;
      
    /** The value for the isProcessed field */
    private boolean isProcessed;
                                                
    /** The value for the userName field */
    private String userName = "";
  
    
    /**
     * Get the SynchronizationId
     *
     * @return String
     */
    public String getSynchronizationId()
    {
        return synchronizationId;
    }

                        
    /**
     * Set the value of SynchronizationId
     *
     * @param v new value
     */
    public void setSynchronizationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.synchronizationId, v))
              {
            this.synchronizationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncType
     *
     * @return int
     */
    public int getSyncType()
    {
        return syncType;
    }

                        
    /**
     * Set the value of SyncType
     *
     * @param v new value
     */
    public void setSyncType(int v) 
    {
    
                  if (this.syncType != v)
              {
            this.syncType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncDate
     *
     * @return Date
     */
    public Date getSyncDate()
    {
        return syncDate;
    }

                        
    /**
     * Set the value of SyncDate
     *
     * @param v new value
     */
    public void setSyncDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.syncDate, v))
              {
            this.syncDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FileName
     *
     * @return String
     */
    public String getFileName()
    {
        return fileName;
    }

                        
    /**
     * Set the value of FileName
     *
     * @param v new value
     */
    public void setFileName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fileName, v))
              {
            this.fileName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsProcessed
     *
     * @return boolean
     */
    public boolean getIsProcessed()
    {
        return isProcessed;
    }

                        
    /**
     * Set the value of IsProcessed
     *
     * @param v new value
     */
    public void setIsProcessed(boolean v) 
    {
    
                  if (this.isProcessed != v)
              {
            this.isProcessed = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SynchronizationId");
              fieldNames.add("SyncType");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("SyncDate");
              fieldNames.add("FileName");
              fieldNames.add("IsProcessed");
              fieldNames.add("UserName");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SynchronizationId"))
        {
                return getSynchronizationId();
            }
          if (name.equals("SyncType"))
        {
                return Integer.valueOf(getSyncType());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("SyncDate"))
        {
                return getSyncDate();
            }
          if (name.equals("FileName"))
        {
                return getFileName();
            }
          if (name.equals("IsProcessed"))
        {
                return Boolean.valueOf(getIsProcessed());
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SynchronizationPeer.SYNCHRONIZATION_ID))
        {
                return getSynchronizationId();
            }
          if (name.equals(SynchronizationPeer.SYNC_TYPE))
        {
                return Integer.valueOf(getSyncType());
            }
          if (name.equals(SynchronizationPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(SynchronizationPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(SynchronizationPeer.SYNC_DATE))
        {
                return getSyncDate();
            }
          if (name.equals(SynchronizationPeer.FILE_NAME))
        {
                return getFileName();
            }
          if (name.equals(SynchronizationPeer.IS_PROCESSED))
        {
                return Boolean.valueOf(getIsProcessed());
            }
          if (name.equals(SynchronizationPeer.USER_NAME))
        {
                return getUserName();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSynchronizationId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getSyncType());
            }
              if (pos == 2)
        {
                return getLocationId();
            }
              if (pos == 3)
        {
                return getLocationName();
            }
              if (pos == 4)
        {
                return getSyncDate();
            }
              if (pos == 5)
        {
                return getFileName();
            }
              if (pos == 6)
        {
                return Boolean.valueOf(getIsProcessed());
            }
              if (pos == 7)
        {
                return getUserName();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SynchronizationPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SynchronizationPeer.doInsert((Synchronization) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SynchronizationPeer.doUpdate((Synchronization) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key synchronizationId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setSynchronizationId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setSynchronizationId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSynchronizationId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Synchronization copy() throws TorqueException
    {
        return copyInto(new Synchronization());
    }
  
    protected Synchronization copyInto(Synchronization copyObj) throws TorqueException
    {
          copyObj.setSynchronizationId(synchronizationId);
          copyObj.setSyncType(syncType);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setSyncDate(syncDate);
          copyObj.setFileName(fileName);
          copyObj.setIsProcessed(isProcessed);
          copyObj.setUserName(userName);
  
                    copyObj.setSynchronizationId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SynchronizationPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Synchronization\n");
        str.append("---------------\n")
           .append("SynchronizationId    : ")
           .append(getSynchronizationId())
           .append("\n")
           .append("SyncType             : ")
           .append(getSyncType())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("SyncDate             : ")
           .append(getSyncDate())
           .append("\n")
           .append("FileName             : ")
           .append(getFileName())
           .append("\n")
           .append("IsProcessed          : ")
           .append(getIsProcessed())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
        ;
        return(str.toString());
    }
}
