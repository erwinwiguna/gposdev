package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to SysConfig
 */
public abstract class BaseSysConfig extends BaseObject
{
    /** The Peer class */
    private static final SysConfigPeer peer =
        new SysConfigPeer();

        
    /** The value for the sysConfigId field */
    private String sysConfigId;
                                                
    /** The value for the posDefaultScreen field */
    private String posDefaultScreen = "PointOfSales.vm";
                                                
    /** The value for the posDefaultScreenFocus field */
    private String posDefaultScreenFocus = "1";
                                                
    /** The value for the posDefaultOpenCashierAmt field */
    private String posDefaultOpenCashierAmt = "400000";
                                                
    /** The value for the posDefaultItemDisplay field */
    private String posDefaultItemDisplay = "1";
                                                
    /** The value for the posDefaultItemSort field */
    private String posDefaultItemSort = "3";
                                                
    /** The value for the posDefaultPrint field */
    private String posDefaultPrint = "1";
                                                
    /** The value for the posConfirmQty field */
    private String posConfirmQty = "true";
                                                
    /** The value for the posConfirmCost field */
    private String posConfirmCost = "false";
                                                
    /** The value for the posChangeValidation field */
    private String posChangeValidation = "true";
                                                
    /** The value for the posDeleteValidation field */
    private String posDeleteValidation = "true";
                                                
    /** The value for the posOpenCashierValidation field */
    private String posOpenCashierValidation = "true";
                                                
    /** The value for the posSaveDirectPrint field */
    private String posSaveDirectPrint = "true";
                                                
    /** The value for the posPrinterDetailLine field */
    private String posPrinterDetailLine = "-1";
                                                
    /** The value for the posDirectAdd field */
    private String posDirectAdd = "false";
                                                
    /** The value for the posRoundingTo field */
    private String posRoundingTo = "0";
                                                
    /** The value for the posRoundingMode field */
    private String posRoundingMode = "1";
                                                
    /** The value for the posSpcDiscCode field */
    private String posSpcDiscCode = "SPCDISC";
                                                
    /** The value for the complexBarcodeUse field */
    private String complexBarcodeUse = "true";
                                                
    /** The value for the complexBarcodePrefix field */
    private String complexBarcodePrefix = "29";
                                                
    /** The value for the complexBarcodePluStart field */
    private String complexBarcodePluStart = "2";
                                                
    /** The value for the complexBarcodePluLength field */
    private String complexBarcodePluLength = "5";
                                                
    /** The value for the complexBarcodeQtyStart field */
    private String complexBarcodeQtyStart = "7";
                                                
    /** The value for the complexBarcodeQtyLength field */
    private String complexBarcodeQtyLength = "4";
                                                
    /** The value for the complexBarcodeQtyDecPos field */
    private String complexBarcodeQtyDecPos = "2";
                                                
    /** The value for the complexBarcodeOtherStart field */
    private String complexBarcodeOtherStart = "7";
                                                
    /** The value for the complexBarcodeOtherLength field */
    private String complexBarcodeOtherLength = "4";
                                                
    /** The value for the complexBarcodeOtherDecPos field */
    private String complexBarcodeOtherDecPos = "2";
                                                
    /** The value for the complexBarcodeOtherType field */
    private String complexBarcodeOtherType = "2";
                                                
    /** The value for the complexBarcodeSuffix field */
    private String complexBarcodeSuffix = "9";
                                                
    /** The value for the complexBarcodeTotalLength field */
    private String complexBarcodeTotalLength = "13";
                                                
    /** The value for the backupOutputDir field */
    private String backupOutputDir = "C:/Data/Backup/";
                                                
    /** The value for the backupFilePref field */
    private String backupFilePref = "system_data_";
                                                
    /** The value for the picturePath field */
    private String picturePath = "/images/ItemPictures/";
                                                
    /** The value for the b2bPoPath field */
    private String b2bPoPath = "C:/Data/B2BPO/";
                                                
    /** The value for the ireportPath field */
    private String ireportPath = "C:/Retailsoft/iReport/";
                                                
    /** The value for the onlineHelpUrl field */
    private String onlineHelpUrl = "http://localhost:8089/help/";
                                                
    /** The value for the smsGatewayUrl field */
    private String smsGatewayUrl = "";
                                                
    /** The value for the emailServer field */
    private String emailServer = "smtp.gmail.com";
                                                
    /** The value for the emailUser field */
    private String emailUser = "retailsoft.smtp@gmail.com";
                                                
    /** The value for the emailPwd field */
    private String emailPwd = "";
                                                
    /** The value for the syncAllowPendingTrans field */
    private String syncAllowPendingTrans = "false";
                                                
    /** The value for the syncFullInventory field */
    private String syncFullInventory = "true";
                                                
    /** The value for the syncAllowStoreAdj field */
    private String syncAllowStoreAdj = "true";
                                                
    /** The value for the syncAllowStoreReceipt field */
    private String syncAllowStoreReceipt = "true";
                                                
    /** The value for the syncAllowFreeTransfer field */
    private String syncAllowFreeTransfer = "true";
                                                
    /** The value for the syncHodataPath field */
    private String syncHodataPath = "C:/Data/HOData/";
                                                
    /** The value for the syncStoredataPath field */
    private String syncStoredataPath = "C:/Data/StoreData/";
                                                
    /** The value for the syncStoresetupPath field */
    private String syncStoresetupPath = "C:/Data/StoreSetup/";
                                                
    /** The value for the syncStoretrfPath field */
    private String syncStoretrfPath = "C:/Data/StoreTRF/";
                                                
    /** The value for the syncDestination field */
    private String syncDestination = "none";
                                                
    /** The value for the syncDiskDestination field */
    private String syncDiskDestination = "F:/";
                                                
    /** The value for the syncHoUploadUrl field */
    private String syncHoUploadUrl = "";
                                                
    /** The value for the syncHoEmail field */
    private String syncHoEmail = "";
                                                
    /** The value for the syncStoreEmailServer field */
    private String syncStoreEmailServer = "";
                                                
    /** The value for the syncStoreEmailUser field */
    private String syncStoreEmailUser = "";
                                                
    /** The value for the syncStoreEmailPwd field */
    private String syncStoreEmailPwd = "";
                                                
    /** The value for the syncDailyClosing field */
    private String syncDailyClosing = "false";
                                                
    /** The value for the useLocationCtltable field */
    private String useLocationCtltable = "false";
                                                
    /** The value for the useCustomerTrPrefix field */
    private String useCustomerTrPrefix = "false";
                                                
    /** The value for the useVendorTrPrefix field */
    private String useVendorTrPrefix = "false";
                                                
    /** The value for the useVendorTax field */
    private String useVendorTax = "false";
                                                
    /** The value for the useCustomerTax field */
    private String useCustomerTax = "false";
                                                
    /** The value for the useCustomerLocation field */
    private String useCustomerLocation = "false";
                                                
    /** The value for the useCfInOut field */
    private String useCfInOut = "false";
                                                
    /** The value for the useDistModules field */
    private String useDistModules = "false";
                                                
    /** The value for the useWstModules field */
    private String useWstModules = "false";
                                                
    /** The value for the useEdiModules field */
    private String useEdiModules = "false";
                                                
    /** The value for the useBom field */
    private String useBom = "false";
                                                
    /** The value for the usePdt field */
    private String usePdt = "false";
                                                
    /** The value for the confirmTrfAtToloc field */
    private String confirmTrfAtToloc = "false";
                                                
    /** The value for the siAllowImportSo field */
    private String siAllowImportSo = "false";
                                                
    /** The value for the siDefaultScreen field */
    private String siDefaultScreen = "SalesInvoiceAjax.vm";
                                                
    /** The value for the itemColorLtMin field */
    private String itemColorLtMin = "#FFEEFF";
                                                
    /** The value for the itemColorLtRop field */
    private String itemColorLtRop = "#FFEEDD";
                                                
    /** The value for the itemColorGtMax field */
    private String itemColorGtMax = "#EEEEFF";
                                                
    /** The value for the itemCodeLength field */
    private String itemCodeLength = "4";
                                                
    /** The value for the custCodeLength field */
    private String custCodeLength = "4";
                                                
    /** The value for the vendCodeLength field */
    private String vendCodeLength = "4";
                                                
    /** The value for the othrCodeLength field */
    private String othrCodeLength = "4";
                                                
    /** The value for the salesInclusiveTax field */
    private String salesInclusiveTax = "true";
                                                
    /** The value for the purchInclusiveTax field */
    private String purchInclusiveTax = "false";
                                                
    /** The value for the salesMultiCurrency field */
    private String salesMultiCurrency = "false";
                                                
    /** The value for the purchMultiCurrency field */
    private String purchMultiCurrency = "true";
                                                
    /** The value for the salesMergeSameItem field */
    private String salesMergeSameItem = "1";
                                                
    /** The value for the purchMergeSameItem field */
    private String purchMergeSameItem = "1";
                                                
    /** The value for the salesSortBy field */
    private String salesSortBy = "4";
                                                
    /** The value for the purchSortBy field */
    private String purchSortBy = "4";
                                                
    /** The value for the salesFirstlineInsert field */
    private String salesFirstlineInsert = "true";
                                                
    /** The value for the purchFirstlineInsert field */
    private String purchFirstlineInsert = "true";
                                                
    /** The value for the invenFirstlineInsert field */
    private String invenFirstlineInsert = "true";
                                                
    /** The value for the salesFobCourier field */
    private String salesFobCourier = "false";
                                                
    /** The value for the purchFobCourier field */
    private String purchFobCourier = "true";
                                                
    /** The value for the salesCommaScale field */
    private String salesCommaScale = "2";
                                                
    /** The value for the purchCommaScale field */
    private String purchCommaScale = "2";
                                                
    /** The value for the invenCommaScale field */
    private String invenCommaScale = "2";
                                                
    /** The value for the custMaxSbMode field */
    private String custMaxSbMode = "1";
                                                
    /** The value for the vendMaxSbMode field */
    private String vendMaxSbMode = "100";
                                                
    /** The value for the purchPrQtyGtPo field */
    private String purchPrQtyGtPo = "false";
                                                
    /** The value for the purchPrQtyEqualPo field */
    private String purchPrQtyEqualPo = "false";
                                                
    /** The value for the purchLimitByEmp field */
    private String purchLimitByEmp = "false";
                                                
    /** The value for the piAllowImportPo field */
    private String piAllowImportPo = "false";
                                                
    /** The value for the piLastPurchMethod field */
    private String piLastPurchMethod = "1";
                                                
    /** The value for the piUpdateLastPurchase field */
    private String piUpdateLastPurchase = "true";
                                                
    /** The value for the piUpdateSalesPrice field */
    private String piUpdateSalesPrice = "false";
                                                
    /** The value for the piUpdatePrCost field */
    private String piUpdatePrCost = "true";
                                                
    /** The value for the salesUseSalesman field */
    private String salesUseSalesman = "true";
                                                
    /** The value for the salesUseMinPrice field */
    private String salesUseMinPrice = "false";
                                                
    /** The value for the salesSalesmanPeritem field */
    private String salesSalesmanPeritem = "false";
                                                
    /** The value for the salesAllowMultipmtDisc field */
    private String salesAllowMultipmtDisc = "true";
                                                
    /** The value for the salesEmplevelDisc field */
    private String salesEmplevelDisc = "false";
                                                
    /** The value for the salesCustomPricingUse field */
    private String salesCustomPricingUse = "false";
                                                
    /** The value for the salesCustomPricingTpl field */
    private String salesCustomPricingTpl = "false";
                                                
    /** The value for the pointrewardUse field */
    private String pointrewardUse = "false";
                                                
    /** The value for the pointrewardByitem field */
    private String pointrewardByitem = "false";
                                                
    /** The value for the pointrewardField field */
    private String pointrewardField = "";
                                                
    /** The value for the pointrewardPvalue field */
    private String pointrewardPvalue = "1";
                                                
    /** The value for the defaultSearchCond field */
    private String defaultSearchCond = "2";
                                                
    /** The value for the keyNewTrans field */
    private String keyNewTrans = "F2";
                                                
    /** The value for the keyOpenLookup field */
    private String keyOpenLookup = "F3";
                                                
    /** The value for the keyPaymentFocus field */
    private String keyPaymentFocus = "F4";
                                                
    /** The value for the keySaveTrans field */
    private String keySaveTrans = "F5";
                                                
    /** The value for the keyPrintTrans field */
    private String keyPrintTrans = "F6";
                                                
    /** The value for the keyFindTrans field */
    private String keyFindTrans = "F7";
                                                
    /** The value for the keySelectPayment field */
    private String keySelectPayment = "F8";
  
    
    /**
     * Get the SysConfigId
     *
     * @return String
     */
    public String getSysConfigId()
    {
        return sysConfigId;
    }

                        
    /**
     * Set the value of SysConfigId
     *
     * @param v new value
     */
    public void setSysConfigId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sysConfigId, v))
              {
            this.sysConfigId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultScreen
     *
     * @return String
     */
    public String getPosDefaultScreen()
    {
        return posDefaultScreen;
    }

                        
    /**
     * Set the value of PosDefaultScreen
     *
     * @param v new value
     */
    public void setPosDefaultScreen(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultScreen, v))
              {
            this.posDefaultScreen = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultScreenFocus
     *
     * @return String
     */
    public String getPosDefaultScreenFocus()
    {
        return posDefaultScreenFocus;
    }

                        
    /**
     * Set the value of PosDefaultScreenFocus
     *
     * @param v new value
     */
    public void setPosDefaultScreenFocus(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultScreenFocus, v))
              {
            this.posDefaultScreenFocus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultOpenCashierAmt
     *
     * @return String
     */
    public String getPosDefaultOpenCashierAmt()
    {
        return posDefaultOpenCashierAmt;
    }

                        
    /**
     * Set the value of PosDefaultOpenCashierAmt
     *
     * @param v new value
     */
    public void setPosDefaultOpenCashierAmt(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultOpenCashierAmt, v))
              {
            this.posDefaultOpenCashierAmt = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultItemDisplay
     *
     * @return String
     */
    public String getPosDefaultItemDisplay()
    {
        return posDefaultItemDisplay;
    }

                        
    /**
     * Set the value of PosDefaultItemDisplay
     *
     * @param v new value
     */
    public void setPosDefaultItemDisplay(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultItemDisplay, v))
              {
            this.posDefaultItemDisplay = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultItemSort
     *
     * @return String
     */
    public String getPosDefaultItemSort()
    {
        return posDefaultItemSort;
    }

                        
    /**
     * Set the value of PosDefaultItemSort
     *
     * @param v new value
     */
    public void setPosDefaultItemSort(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultItemSort, v))
              {
            this.posDefaultItemSort = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDefaultPrint
     *
     * @return String
     */
    public String getPosDefaultPrint()
    {
        return posDefaultPrint;
    }

                        
    /**
     * Set the value of PosDefaultPrint
     *
     * @param v new value
     */
    public void setPosDefaultPrint(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDefaultPrint, v))
              {
            this.posDefaultPrint = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosConfirmQty
     *
     * @return String
     */
    public String getPosConfirmQty()
    {
        return posConfirmQty;
    }

                        
    /**
     * Set the value of PosConfirmQty
     *
     * @param v new value
     */
    public void setPosConfirmQty(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posConfirmQty, v))
              {
            this.posConfirmQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosConfirmCost
     *
     * @return String
     */
    public String getPosConfirmCost()
    {
        return posConfirmCost;
    }

                        
    /**
     * Set the value of PosConfirmCost
     *
     * @param v new value
     */
    public void setPosConfirmCost(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posConfirmCost, v))
              {
            this.posConfirmCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosChangeValidation
     *
     * @return String
     */
    public String getPosChangeValidation()
    {
        return posChangeValidation;
    }

                        
    /**
     * Set the value of PosChangeValidation
     *
     * @param v new value
     */
    public void setPosChangeValidation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posChangeValidation, v))
              {
            this.posChangeValidation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDeleteValidation
     *
     * @return String
     */
    public String getPosDeleteValidation()
    {
        return posDeleteValidation;
    }

                        
    /**
     * Set the value of PosDeleteValidation
     *
     * @param v new value
     */
    public void setPosDeleteValidation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDeleteValidation, v))
              {
            this.posDeleteValidation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosOpenCashierValidation
     *
     * @return String
     */
    public String getPosOpenCashierValidation()
    {
        return posOpenCashierValidation;
    }

                        
    /**
     * Set the value of PosOpenCashierValidation
     *
     * @param v new value
     */
    public void setPosOpenCashierValidation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posOpenCashierValidation, v))
              {
            this.posOpenCashierValidation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosSaveDirectPrint
     *
     * @return String
     */
    public String getPosSaveDirectPrint()
    {
        return posSaveDirectPrint;
    }

                        
    /**
     * Set the value of PosSaveDirectPrint
     *
     * @param v new value
     */
    public void setPosSaveDirectPrint(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posSaveDirectPrint, v))
              {
            this.posSaveDirectPrint = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosPrinterDetailLine
     *
     * @return String
     */
    public String getPosPrinterDetailLine()
    {
        return posPrinterDetailLine;
    }

                        
    /**
     * Set the value of PosPrinterDetailLine
     *
     * @param v new value
     */
    public void setPosPrinterDetailLine(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posPrinterDetailLine, v))
              {
            this.posPrinterDetailLine = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosDirectAdd
     *
     * @return String
     */
    public String getPosDirectAdd()
    {
        return posDirectAdd;
    }

                        
    /**
     * Set the value of PosDirectAdd
     *
     * @param v new value
     */
    public void setPosDirectAdd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posDirectAdd, v))
              {
            this.posDirectAdd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRoundingTo
     *
     * @return String
     */
    public String getPosRoundingTo()
    {
        return posRoundingTo;
    }

                        
    /**
     * Set the value of PosRoundingTo
     *
     * @param v new value
     */
    public void setPosRoundingTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posRoundingTo, v))
              {
            this.posRoundingTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRoundingMode
     *
     * @return String
     */
    public String getPosRoundingMode()
    {
        return posRoundingMode;
    }

                        
    /**
     * Set the value of PosRoundingMode
     *
     * @param v new value
     */
    public void setPosRoundingMode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posRoundingMode, v))
              {
            this.posRoundingMode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosSpcDiscCode
     *
     * @return String
     */
    public String getPosSpcDiscCode()
    {
        return posSpcDiscCode;
    }

                        
    /**
     * Set the value of PosSpcDiscCode
     *
     * @param v new value
     */
    public void setPosSpcDiscCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posSpcDiscCode, v))
              {
            this.posSpcDiscCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeUse
     *
     * @return String
     */
    public String getComplexBarcodeUse()
    {
        return complexBarcodeUse;
    }

                        
    /**
     * Set the value of ComplexBarcodeUse
     *
     * @param v new value
     */
    public void setComplexBarcodeUse(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeUse, v))
              {
            this.complexBarcodeUse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodePrefix
     *
     * @return String
     */
    public String getComplexBarcodePrefix()
    {
        return complexBarcodePrefix;
    }

                        
    /**
     * Set the value of ComplexBarcodePrefix
     *
     * @param v new value
     */
    public void setComplexBarcodePrefix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodePrefix, v))
              {
            this.complexBarcodePrefix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodePluStart
     *
     * @return String
     */
    public String getComplexBarcodePluStart()
    {
        return complexBarcodePluStart;
    }

                        
    /**
     * Set the value of ComplexBarcodePluStart
     *
     * @param v new value
     */
    public void setComplexBarcodePluStart(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodePluStart, v))
              {
            this.complexBarcodePluStart = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodePluLength
     *
     * @return String
     */
    public String getComplexBarcodePluLength()
    {
        return complexBarcodePluLength;
    }

                        
    /**
     * Set the value of ComplexBarcodePluLength
     *
     * @param v new value
     */
    public void setComplexBarcodePluLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodePluLength, v))
              {
            this.complexBarcodePluLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeQtyStart
     *
     * @return String
     */
    public String getComplexBarcodeQtyStart()
    {
        return complexBarcodeQtyStart;
    }

                        
    /**
     * Set the value of ComplexBarcodeQtyStart
     *
     * @param v new value
     */
    public void setComplexBarcodeQtyStart(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeQtyStart, v))
              {
            this.complexBarcodeQtyStart = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeQtyLength
     *
     * @return String
     */
    public String getComplexBarcodeQtyLength()
    {
        return complexBarcodeQtyLength;
    }

                        
    /**
     * Set the value of ComplexBarcodeQtyLength
     *
     * @param v new value
     */
    public void setComplexBarcodeQtyLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeQtyLength, v))
              {
            this.complexBarcodeQtyLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeQtyDecPos
     *
     * @return String
     */
    public String getComplexBarcodeQtyDecPos()
    {
        return complexBarcodeQtyDecPos;
    }

                        
    /**
     * Set the value of ComplexBarcodeQtyDecPos
     *
     * @param v new value
     */
    public void setComplexBarcodeQtyDecPos(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeQtyDecPos, v))
              {
            this.complexBarcodeQtyDecPos = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeOtherStart
     *
     * @return String
     */
    public String getComplexBarcodeOtherStart()
    {
        return complexBarcodeOtherStart;
    }

                        
    /**
     * Set the value of ComplexBarcodeOtherStart
     *
     * @param v new value
     */
    public void setComplexBarcodeOtherStart(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeOtherStart, v))
              {
            this.complexBarcodeOtherStart = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeOtherLength
     *
     * @return String
     */
    public String getComplexBarcodeOtherLength()
    {
        return complexBarcodeOtherLength;
    }

                        
    /**
     * Set the value of ComplexBarcodeOtherLength
     *
     * @param v new value
     */
    public void setComplexBarcodeOtherLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeOtherLength, v))
              {
            this.complexBarcodeOtherLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeOtherDecPos
     *
     * @return String
     */
    public String getComplexBarcodeOtherDecPos()
    {
        return complexBarcodeOtherDecPos;
    }

                        
    /**
     * Set the value of ComplexBarcodeOtherDecPos
     *
     * @param v new value
     */
    public void setComplexBarcodeOtherDecPos(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeOtherDecPos, v))
              {
            this.complexBarcodeOtherDecPos = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeOtherType
     *
     * @return String
     */
    public String getComplexBarcodeOtherType()
    {
        return complexBarcodeOtherType;
    }

                        
    /**
     * Set the value of ComplexBarcodeOtherType
     *
     * @param v new value
     */
    public void setComplexBarcodeOtherType(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeOtherType, v))
              {
            this.complexBarcodeOtherType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeSuffix
     *
     * @return String
     */
    public String getComplexBarcodeSuffix()
    {
        return complexBarcodeSuffix;
    }

                        
    /**
     * Set the value of ComplexBarcodeSuffix
     *
     * @param v new value
     */
    public void setComplexBarcodeSuffix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeSuffix, v))
              {
            this.complexBarcodeSuffix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ComplexBarcodeTotalLength
     *
     * @return String
     */
    public String getComplexBarcodeTotalLength()
    {
        return complexBarcodeTotalLength;
    }

                        
    /**
     * Set the value of ComplexBarcodeTotalLength
     *
     * @param v new value
     */
    public void setComplexBarcodeTotalLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.complexBarcodeTotalLength, v))
              {
            this.complexBarcodeTotalLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BackupOutputDir
     *
     * @return String
     */
    public String getBackupOutputDir()
    {
        return backupOutputDir;
    }

                        
    /**
     * Set the value of BackupOutputDir
     *
     * @param v new value
     */
    public void setBackupOutputDir(String v) 
    {
    
                  if (!ObjectUtils.equals(this.backupOutputDir, v))
              {
            this.backupOutputDir = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BackupFilePref
     *
     * @return String
     */
    public String getBackupFilePref()
    {
        return backupFilePref;
    }

                        
    /**
     * Set the value of BackupFilePref
     *
     * @param v new value
     */
    public void setBackupFilePref(String v) 
    {
    
                  if (!ObjectUtils.equals(this.backupFilePref, v))
              {
            this.backupFilePref = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PicturePath
     *
     * @return String
     */
    public String getPicturePath()
    {
        return picturePath;
    }

                        
    /**
     * Set the value of PicturePath
     *
     * @param v new value
     */
    public void setPicturePath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.picturePath, v))
              {
            this.picturePath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the B2bPoPath
     *
     * @return String
     */
    public String getB2bPoPath()
    {
        return b2bPoPath;
    }

                        
    /**
     * Set the value of B2bPoPath
     *
     * @param v new value
     */
    public void setB2bPoPath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.b2bPoPath, v))
              {
            this.b2bPoPath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IreportPath
     *
     * @return String
     */
    public String getIreportPath()
    {
        return ireportPath;
    }

                        
    /**
     * Set the value of IreportPath
     *
     * @param v new value
     */
    public void setIreportPath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.ireportPath, v))
              {
            this.ireportPath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OnlineHelpUrl
     *
     * @return String
     */
    public String getOnlineHelpUrl()
    {
        return onlineHelpUrl;
    }

                        
    /**
     * Set the value of OnlineHelpUrl
     *
     * @param v new value
     */
    public void setOnlineHelpUrl(String v) 
    {
    
                  if (!ObjectUtils.equals(this.onlineHelpUrl, v))
              {
            this.onlineHelpUrl = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SmsGatewayUrl
     *
     * @return String
     */
    public String getSmsGatewayUrl()
    {
        return smsGatewayUrl;
    }

                        
    /**
     * Set the value of SmsGatewayUrl
     *
     * @param v new value
     */
    public void setSmsGatewayUrl(String v) 
    {
    
                  if (!ObjectUtils.equals(this.smsGatewayUrl, v))
              {
            this.smsGatewayUrl = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmailServer
     *
     * @return String
     */
    public String getEmailServer()
    {
        return emailServer;
    }

                        
    /**
     * Set the value of EmailServer
     *
     * @param v new value
     */
    public void setEmailServer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.emailServer, v))
              {
            this.emailServer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmailUser
     *
     * @return String
     */
    public String getEmailUser()
    {
        return emailUser;
    }

                        
    /**
     * Set the value of EmailUser
     *
     * @param v new value
     */
    public void setEmailUser(String v) 
    {
    
                  if (!ObjectUtils.equals(this.emailUser, v))
              {
            this.emailUser = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmailPwd
     *
     * @return String
     */
    public String getEmailPwd()
    {
        return emailPwd;
    }

                        
    /**
     * Set the value of EmailPwd
     *
     * @param v new value
     */
    public void setEmailPwd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.emailPwd, v))
              {
            this.emailPwd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncAllowPendingTrans
     *
     * @return String
     */
    public String getSyncAllowPendingTrans()
    {
        return syncAllowPendingTrans;
    }

                        
    /**
     * Set the value of SyncAllowPendingTrans
     *
     * @param v new value
     */
    public void setSyncAllowPendingTrans(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncAllowPendingTrans, v))
              {
            this.syncAllowPendingTrans = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncFullInventory
     *
     * @return String
     */
    public String getSyncFullInventory()
    {
        return syncFullInventory;
    }

                        
    /**
     * Set the value of SyncFullInventory
     *
     * @param v new value
     */
    public void setSyncFullInventory(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncFullInventory, v))
              {
            this.syncFullInventory = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncAllowStoreAdj
     *
     * @return String
     */
    public String getSyncAllowStoreAdj()
    {
        return syncAllowStoreAdj;
    }

                        
    /**
     * Set the value of SyncAllowStoreAdj
     *
     * @param v new value
     */
    public void setSyncAllowStoreAdj(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncAllowStoreAdj, v))
              {
            this.syncAllowStoreAdj = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncAllowStoreReceipt
     *
     * @return String
     */
    public String getSyncAllowStoreReceipt()
    {
        return syncAllowStoreReceipt;
    }

                        
    /**
     * Set the value of SyncAllowStoreReceipt
     *
     * @param v new value
     */
    public void setSyncAllowStoreReceipt(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncAllowStoreReceipt, v))
              {
            this.syncAllowStoreReceipt = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncAllowFreeTransfer
     *
     * @return String
     */
    public String getSyncAllowFreeTransfer()
    {
        return syncAllowFreeTransfer;
    }

                        
    /**
     * Set the value of SyncAllowFreeTransfer
     *
     * @param v new value
     */
    public void setSyncAllowFreeTransfer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncAllowFreeTransfer, v))
              {
            this.syncAllowFreeTransfer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncHodataPath
     *
     * @return String
     */
    public String getSyncHodataPath()
    {
        return syncHodataPath;
    }

                        
    /**
     * Set the value of SyncHodataPath
     *
     * @param v new value
     */
    public void setSyncHodataPath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncHodataPath, v))
              {
            this.syncHodataPath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncStoredataPath
     *
     * @return String
     */
    public String getSyncStoredataPath()
    {
        return syncStoredataPath;
    }

                        
    /**
     * Set the value of SyncStoredataPath
     *
     * @param v new value
     */
    public void setSyncStoredataPath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncStoredataPath, v))
              {
            this.syncStoredataPath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncStoresetupPath
     *
     * @return String
     */
    public String getSyncStoresetupPath()
    {
        return syncStoresetupPath;
    }

                        
    /**
     * Set the value of SyncStoresetupPath
     *
     * @param v new value
     */
    public void setSyncStoresetupPath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncStoresetupPath, v))
              {
            this.syncStoresetupPath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncStoretrfPath
     *
     * @return String
     */
    public String getSyncStoretrfPath()
    {
        return syncStoretrfPath;
    }

                        
    /**
     * Set the value of SyncStoretrfPath
     *
     * @param v new value
     */
    public void setSyncStoretrfPath(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncStoretrfPath, v))
              {
            this.syncStoretrfPath = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncDestination
     *
     * @return String
     */
    public String getSyncDestination()
    {
        return syncDestination;
    }

                        
    /**
     * Set the value of SyncDestination
     *
     * @param v new value
     */
    public void setSyncDestination(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncDestination, v))
              {
            this.syncDestination = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncDiskDestination
     *
     * @return String
     */
    public String getSyncDiskDestination()
    {
        return syncDiskDestination;
    }

                        
    /**
     * Set the value of SyncDiskDestination
     *
     * @param v new value
     */
    public void setSyncDiskDestination(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncDiskDestination, v))
              {
            this.syncDiskDestination = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncHoUploadUrl
     *
     * @return String
     */
    public String getSyncHoUploadUrl()
    {
        return syncHoUploadUrl;
    }

                        
    /**
     * Set the value of SyncHoUploadUrl
     *
     * @param v new value
     */
    public void setSyncHoUploadUrl(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncHoUploadUrl, v))
              {
            this.syncHoUploadUrl = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncHoEmail
     *
     * @return String
     */
    public String getSyncHoEmail()
    {
        return syncHoEmail;
    }

                        
    /**
     * Set the value of SyncHoEmail
     *
     * @param v new value
     */
    public void setSyncHoEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncHoEmail, v))
              {
            this.syncHoEmail = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncStoreEmailServer
     *
     * @return String
     */
    public String getSyncStoreEmailServer()
    {
        return syncStoreEmailServer;
    }

                        
    /**
     * Set the value of SyncStoreEmailServer
     *
     * @param v new value
     */
    public void setSyncStoreEmailServer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncStoreEmailServer, v))
              {
            this.syncStoreEmailServer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncStoreEmailUser
     *
     * @return String
     */
    public String getSyncStoreEmailUser()
    {
        return syncStoreEmailUser;
    }

                        
    /**
     * Set the value of SyncStoreEmailUser
     *
     * @param v new value
     */
    public void setSyncStoreEmailUser(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncStoreEmailUser, v))
              {
            this.syncStoreEmailUser = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncStoreEmailPwd
     *
     * @return String
     */
    public String getSyncStoreEmailPwd()
    {
        return syncStoreEmailPwd;
    }

                        
    /**
     * Set the value of SyncStoreEmailPwd
     *
     * @param v new value
     */
    public void setSyncStoreEmailPwd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncStoreEmailPwd, v))
              {
            this.syncStoreEmailPwd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SyncDailyClosing
     *
     * @return String
     */
    public String getSyncDailyClosing()
    {
        return syncDailyClosing;
    }

                        
    /**
     * Set the value of SyncDailyClosing
     *
     * @param v new value
     */
    public void setSyncDailyClosing(String v) 
    {
    
                  if (!ObjectUtils.equals(this.syncDailyClosing, v))
              {
            this.syncDailyClosing = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseLocationCtltable
     *
     * @return String
     */
    public String getUseLocationCtltable()
    {
        return useLocationCtltable;
    }

                        
    /**
     * Set the value of UseLocationCtltable
     *
     * @param v new value
     */
    public void setUseLocationCtltable(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useLocationCtltable, v))
              {
            this.useLocationCtltable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseCustomerTrPrefix
     *
     * @return String
     */
    public String getUseCustomerTrPrefix()
    {
        return useCustomerTrPrefix;
    }

                        
    /**
     * Set the value of UseCustomerTrPrefix
     *
     * @param v new value
     */
    public void setUseCustomerTrPrefix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useCustomerTrPrefix, v))
              {
            this.useCustomerTrPrefix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseVendorTrPrefix
     *
     * @return String
     */
    public String getUseVendorTrPrefix()
    {
        return useVendorTrPrefix;
    }

                        
    /**
     * Set the value of UseVendorTrPrefix
     *
     * @param v new value
     */
    public void setUseVendorTrPrefix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useVendorTrPrefix, v))
              {
            this.useVendorTrPrefix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseVendorTax
     *
     * @return String
     */
    public String getUseVendorTax()
    {
        return useVendorTax;
    }

                        
    /**
     * Set the value of UseVendorTax
     *
     * @param v new value
     */
    public void setUseVendorTax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useVendorTax, v))
              {
            this.useVendorTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseCustomerTax
     *
     * @return String
     */
    public String getUseCustomerTax()
    {
        return useCustomerTax;
    }

                        
    /**
     * Set the value of UseCustomerTax
     *
     * @param v new value
     */
    public void setUseCustomerTax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useCustomerTax, v))
              {
            this.useCustomerTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseCustomerLocation
     *
     * @return String
     */
    public String getUseCustomerLocation()
    {
        return useCustomerLocation;
    }

                        
    /**
     * Set the value of UseCustomerLocation
     *
     * @param v new value
     */
    public void setUseCustomerLocation(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useCustomerLocation, v))
              {
            this.useCustomerLocation = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseCfInOut
     *
     * @return String
     */
    public String getUseCfInOut()
    {
        return useCfInOut;
    }

                        
    /**
     * Set the value of UseCfInOut
     *
     * @param v new value
     */
    public void setUseCfInOut(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useCfInOut, v))
              {
            this.useCfInOut = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseDistModules
     *
     * @return String
     */
    public String getUseDistModules()
    {
        return useDistModules;
    }

                        
    /**
     * Set the value of UseDistModules
     *
     * @param v new value
     */
    public void setUseDistModules(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useDistModules, v))
              {
            this.useDistModules = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseWstModules
     *
     * @return String
     */
    public String getUseWstModules()
    {
        return useWstModules;
    }

                        
    /**
     * Set the value of UseWstModules
     *
     * @param v new value
     */
    public void setUseWstModules(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useWstModules, v))
              {
            this.useWstModules = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseEdiModules
     *
     * @return String
     */
    public String getUseEdiModules()
    {
        return useEdiModules;
    }

                        
    /**
     * Set the value of UseEdiModules
     *
     * @param v new value
     */
    public void setUseEdiModules(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useEdiModules, v))
              {
            this.useEdiModules = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseBom
     *
     * @return String
     */
    public String getUseBom()
    {
        return useBom;
    }

                        
    /**
     * Set the value of UseBom
     *
     * @param v new value
     */
    public void setUseBom(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useBom, v))
              {
            this.useBom = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UsePdt
     *
     * @return String
     */
    public String getUsePdt()
    {
        return usePdt;
    }

                        
    /**
     * Set the value of UsePdt
     *
     * @param v new value
     */
    public void setUsePdt(String v) 
    {
    
                  if (!ObjectUtils.equals(this.usePdt, v))
              {
            this.usePdt = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmTrfAtToloc
     *
     * @return String
     */
    public String getConfirmTrfAtToloc()
    {
        return confirmTrfAtToloc;
    }

                        
    /**
     * Set the value of ConfirmTrfAtToloc
     *
     * @param v new value
     */
    public void setConfirmTrfAtToloc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmTrfAtToloc, v))
              {
            this.confirmTrfAtToloc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SiAllowImportSo
     *
     * @return String
     */
    public String getSiAllowImportSo()
    {
        return siAllowImportSo;
    }

                        
    /**
     * Set the value of SiAllowImportSo
     *
     * @param v new value
     */
    public void setSiAllowImportSo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.siAllowImportSo, v))
              {
            this.siAllowImportSo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SiDefaultScreen
     *
     * @return String
     */
    public String getSiDefaultScreen()
    {
        return siDefaultScreen;
    }

                        
    /**
     * Set the value of SiDefaultScreen
     *
     * @param v new value
     */
    public void setSiDefaultScreen(String v) 
    {
    
                  if (!ObjectUtils.equals(this.siDefaultScreen, v))
              {
            this.siDefaultScreen = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemColorLtMin
     *
     * @return String
     */
    public String getItemColorLtMin()
    {
        return itemColorLtMin;
    }

                        
    /**
     * Set the value of ItemColorLtMin
     *
     * @param v new value
     */
    public void setItemColorLtMin(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemColorLtMin, v))
              {
            this.itemColorLtMin = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemColorLtRop
     *
     * @return String
     */
    public String getItemColorLtRop()
    {
        return itemColorLtRop;
    }

                        
    /**
     * Set the value of ItemColorLtRop
     *
     * @param v new value
     */
    public void setItemColorLtRop(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemColorLtRop, v))
              {
            this.itemColorLtRop = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemColorGtMax
     *
     * @return String
     */
    public String getItemColorGtMax()
    {
        return itemColorGtMax;
    }

                        
    /**
     * Set the value of ItemColorGtMax
     *
     * @param v new value
     */
    public void setItemColorGtMax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemColorGtMax, v))
              {
            this.itemColorGtMax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCodeLength
     *
     * @return String
     */
    public String getItemCodeLength()
    {
        return itemCodeLength;
    }

                        
    /**
     * Set the value of ItemCodeLength
     *
     * @param v new value
     */
    public void setItemCodeLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCodeLength, v))
              {
            this.itemCodeLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustCodeLength
     *
     * @return String
     */
    public String getCustCodeLength()
    {
        return custCodeLength;
    }

                        
    /**
     * Set the value of CustCodeLength
     *
     * @param v new value
     */
    public void setCustCodeLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.custCodeLength, v))
              {
            this.custCodeLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendCodeLength
     *
     * @return String
     */
    public String getVendCodeLength()
    {
        return vendCodeLength;
    }

                        
    /**
     * Set the value of VendCodeLength
     *
     * @param v new value
     */
    public void setVendCodeLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendCodeLength, v))
              {
            this.vendCodeLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OthrCodeLength
     *
     * @return String
     */
    public String getOthrCodeLength()
    {
        return othrCodeLength;
    }

                        
    /**
     * Set the value of OthrCodeLength
     *
     * @param v new value
     */
    public void setOthrCodeLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.othrCodeLength, v))
              {
            this.othrCodeLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesInclusiveTax
     *
     * @return String
     */
    public String getSalesInclusiveTax()
    {
        return salesInclusiveTax;
    }

                        
    /**
     * Set the value of SalesInclusiveTax
     *
     * @param v new value
     */
    public void setSalesInclusiveTax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesInclusiveTax, v))
              {
            this.salesInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchInclusiveTax
     *
     * @return String
     */
    public String getPurchInclusiveTax()
    {
        return purchInclusiveTax;
    }

                        
    /**
     * Set the value of PurchInclusiveTax
     *
     * @param v new value
     */
    public void setPurchInclusiveTax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchInclusiveTax, v))
              {
            this.purchInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesMultiCurrency
     *
     * @return String
     */
    public String getSalesMultiCurrency()
    {
        return salesMultiCurrency;
    }

                        
    /**
     * Set the value of SalesMultiCurrency
     *
     * @param v new value
     */
    public void setSalesMultiCurrency(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesMultiCurrency, v))
              {
            this.salesMultiCurrency = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchMultiCurrency
     *
     * @return String
     */
    public String getPurchMultiCurrency()
    {
        return purchMultiCurrency;
    }

                        
    /**
     * Set the value of PurchMultiCurrency
     *
     * @param v new value
     */
    public void setPurchMultiCurrency(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchMultiCurrency, v))
              {
            this.purchMultiCurrency = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesMergeSameItem
     *
     * @return String
     */
    public String getSalesMergeSameItem()
    {
        return salesMergeSameItem;
    }

                        
    /**
     * Set the value of SalesMergeSameItem
     *
     * @param v new value
     */
    public void setSalesMergeSameItem(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesMergeSameItem, v))
              {
            this.salesMergeSameItem = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchMergeSameItem
     *
     * @return String
     */
    public String getPurchMergeSameItem()
    {
        return purchMergeSameItem;
    }

                        
    /**
     * Set the value of PurchMergeSameItem
     *
     * @param v new value
     */
    public void setPurchMergeSameItem(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchMergeSameItem, v))
              {
            this.purchMergeSameItem = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesSortBy
     *
     * @return String
     */
    public String getSalesSortBy()
    {
        return salesSortBy;
    }

                        
    /**
     * Set the value of SalesSortBy
     *
     * @param v new value
     */
    public void setSalesSortBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesSortBy, v))
              {
            this.salesSortBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchSortBy
     *
     * @return String
     */
    public String getPurchSortBy()
    {
        return purchSortBy;
    }

                        
    /**
     * Set the value of PurchSortBy
     *
     * @param v new value
     */
    public void setPurchSortBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchSortBy, v))
              {
            this.purchSortBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesFirstlineInsert
     *
     * @return String
     */
    public String getSalesFirstlineInsert()
    {
        return salesFirstlineInsert;
    }

                        
    /**
     * Set the value of SalesFirstlineInsert
     *
     * @param v new value
     */
    public void setSalesFirstlineInsert(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesFirstlineInsert, v))
              {
            this.salesFirstlineInsert = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchFirstlineInsert
     *
     * @return String
     */
    public String getPurchFirstlineInsert()
    {
        return purchFirstlineInsert;
    }

                        
    /**
     * Set the value of PurchFirstlineInsert
     *
     * @param v new value
     */
    public void setPurchFirstlineInsert(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchFirstlineInsert, v))
              {
            this.purchFirstlineInsert = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvenFirstlineInsert
     *
     * @return String
     */
    public String getInvenFirstlineInsert()
    {
        return invenFirstlineInsert;
    }

                        
    /**
     * Set the value of InvenFirstlineInsert
     *
     * @param v new value
     */
    public void setInvenFirstlineInsert(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invenFirstlineInsert, v))
              {
            this.invenFirstlineInsert = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesFobCourier
     *
     * @return String
     */
    public String getSalesFobCourier()
    {
        return salesFobCourier;
    }

                        
    /**
     * Set the value of SalesFobCourier
     *
     * @param v new value
     */
    public void setSalesFobCourier(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesFobCourier, v))
              {
            this.salesFobCourier = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchFobCourier
     *
     * @return String
     */
    public String getPurchFobCourier()
    {
        return purchFobCourier;
    }

                        
    /**
     * Set the value of PurchFobCourier
     *
     * @param v new value
     */
    public void setPurchFobCourier(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchFobCourier, v))
              {
            this.purchFobCourier = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesCommaScale
     *
     * @return String
     */
    public String getSalesCommaScale()
    {
        return salesCommaScale;
    }

                        
    /**
     * Set the value of SalesCommaScale
     *
     * @param v new value
     */
    public void setSalesCommaScale(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesCommaScale, v))
              {
            this.salesCommaScale = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchCommaScale
     *
     * @return String
     */
    public String getPurchCommaScale()
    {
        return purchCommaScale;
    }

                        
    /**
     * Set the value of PurchCommaScale
     *
     * @param v new value
     */
    public void setPurchCommaScale(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchCommaScale, v))
              {
            this.purchCommaScale = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvenCommaScale
     *
     * @return String
     */
    public String getInvenCommaScale()
    {
        return invenCommaScale;
    }

                        
    /**
     * Set the value of InvenCommaScale
     *
     * @param v new value
     */
    public void setInvenCommaScale(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invenCommaScale, v))
              {
            this.invenCommaScale = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustMaxSbMode
     *
     * @return String
     */
    public String getCustMaxSbMode()
    {
        return custMaxSbMode;
    }

                        
    /**
     * Set the value of CustMaxSbMode
     *
     * @param v new value
     */
    public void setCustMaxSbMode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.custMaxSbMode, v))
              {
            this.custMaxSbMode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendMaxSbMode
     *
     * @return String
     */
    public String getVendMaxSbMode()
    {
        return vendMaxSbMode;
    }

                        
    /**
     * Set the value of VendMaxSbMode
     *
     * @param v new value
     */
    public void setVendMaxSbMode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendMaxSbMode, v))
              {
            this.vendMaxSbMode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchPrQtyGtPo
     *
     * @return String
     */
    public String getPurchPrQtyGtPo()
    {
        return purchPrQtyGtPo;
    }

                        
    /**
     * Set the value of PurchPrQtyGtPo
     *
     * @param v new value
     */
    public void setPurchPrQtyGtPo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchPrQtyGtPo, v))
              {
            this.purchPrQtyGtPo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchPrQtyEqualPo
     *
     * @return String
     */
    public String getPurchPrQtyEqualPo()
    {
        return purchPrQtyEqualPo;
    }

                        
    /**
     * Set the value of PurchPrQtyEqualPo
     *
     * @param v new value
     */
    public void setPurchPrQtyEqualPo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchPrQtyEqualPo, v))
              {
            this.purchPrQtyEqualPo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchLimitByEmp
     *
     * @return String
     */
    public String getPurchLimitByEmp()
    {
        return purchLimitByEmp;
    }

                        
    /**
     * Set the value of PurchLimitByEmp
     *
     * @param v new value
     */
    public void setPurchLimitByEmp(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchLimitByEmp, v))
              {
            this.purchLimitByEmp = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PiAllowImportPo
     *
     * @return String
     */
    public String getPiAllowImportPo()
    {
        return piAllowImportPo;
    }

                        
    /**
     * Set the value of PiAllowImportPo
     *
     * @param v new value
     */
    public void setPiAllowImportPo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.piAllowImportPo, v))
              {
            this.piAllowImportPo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PiLastPurchMethod
     *
     * @return String
     */
    public String getPiLastPurchMethod()
    {
        return piLastPurchMethod;
    }

                        
    /**
     * Set the value of PiLastPurchMethod
     *
     * @param v new value
     */
    public void setPiLastPurchMethod(String v) 
    {
    
                  if (!ObjectUtils.equals(this.piLastPurchMethod, v))
              {
            this.piLastPurchMethod = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PiUpdateLastPurchase
     *
     * @return String
     */
    public String getPiUpdateLastPurchase()
    {
        return piUpdateLastPurchase;
    }

                        
    /**
     * Set the value of PiUpdateLastPurchase
     *
     * @param v new value
     */
    public void setPiUpdateLastPurchase(String v) 
    {
    
                  if (!ObjectUtils.equals(this.piUpdateLastPurchase, v))
              {
            this.piUpdateLastPurchase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PiUpdateSalesPrice
     *
     * @return String
     */
    public String getPiUpdateSalesPrice()
    {
        return piUpdateSalesPrice;
    }

                        
    /**
     * Set the value of PiUpdateSalesPrice
     *
     * @param v new value
     */
    public void setPiUpdateSalesPrice(String v) 
    {
    
                  if (!ObjectUtils.equals(this.piUpdateSalesPrice, v))
              {
            this.piUpdateSalesPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PiUpdatePrCost
     *
     * @return String
     */
    public String getPiUpdatePrCost()
    {
        return piUpdatePrCost;
    }

                        
    /**
     * Set the value of PiUpdatePrCost
     *
     * @param v new value
     */
    public void setPiUpdatePrCost(String v) 
    {
    
                  if (!ObjectUtils.equals(this.piUpdatePrCost, v))
              {
            this.piUpdatePrCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesUseSalesman
     *
     * @return String
     */
    public String getSalesUseSalesman()
    {
        return salesUseSalesman;
    }

                        
    /**
     * Set the value of SalesUseSalesman
     *
     * @param v new value
     */
    public void setSalesUseSalesman(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesUseSalesman, v))
              {
            this.salesUseSalesman = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesUseMinPrice
     *
     * @return String
     */
    public String getSalesUseMinPrice()
    {
        return salesUseMinPrice;
    }

                        
    /**
     * Set the value of SalesUseMinPrice
     *
     * @param v new value
     */
    public void setSalesUseMinPrice(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesUseMinPrice, v))
              {
            this.salesUseMinPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesSalesmanPeritem
     *
     * @return String
     */
    public String getSalesSalesmanPeritem()
    {
        return salesSalesmanPeritem;
    }

                        
    /**
     * Set the value of SalesSalesmanPeritem
     *
     * @param v new value
     */
    public void setSalesSalesmanPeritem(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesSalesmanPeritem, v))
              {
            this.salesSalesmanPeritem = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAllowMultipmtDisc
     *
     * @return String
     */
    public String getSalesAllowMultipmtDisc()
    {
        return salesAllowMultipmtDisc;
    }

                        
    /**
     * Set the value of SalesAllowMultipmtDisc
     *
     * @param v new value
     */
    public void setSalesAllowMultipmtDisc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAllowMultipmtDisc, v))
              {
            this.salesAllowMultipmtDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesEmplevelDisc
     *
     * @return String
     */
    public String getSalesEmplevelDisc()
    {
        return salesEmplevelDisc;
    }

                        
    /**
     * Set the value of SalesEmplevelDisc
     *
     * @param v new value
     */
    public void setSalesEmplevelDisc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesEmplevelDisc, v))
              {
            this.salesEmplevelDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesCustomPricingUse
     *
     * @return String
     */
    public String getSalesCustomPricingUse()
    {
        return salesCustomPricingUse;
    }

                        
    /**
     * Set the value of SalesCustomPricingUse
     *
     * @param v new value
     */
    public void setSalesCustomPricingUse(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesCustomPricingUse, v))
              {
            this.salesCustomPricingUse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesCustomPricingTpl
     *
     * @return String
     */
    public String getSalesCustomPricingTpl()
    {
        return salesCustomPricingTpl;
    }

                        
    /**
     * Set the value of SalesCustomPricingTpl
     *
     * @param v new value
     */
    public void setSalesCustomPricingTpl(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesCustomPricingTpl, v))
              {
            this.salesCustomPricingTpl = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointrewardUse
     *
     * @return String
     */
    public String getPointrewardUse()
    {
        return pointrewardUse;
    }

                        
    /**
     * Set the value of PointrewardUse
     *
     * @param v new value
     */
    public void setPointrewardUse(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointrewardUse, v))
              {
            this.pointrewardUse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointrewardByitem
     *
     * @return String
     */
    public String getPointrewardByitem()
    {
        return pointrewardByitem;
    }

                        
    /**
     * Set the value of PointrewardByitem
     *
     * @param v new value
     */
    public void setPointrewardByitem(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointrewardByitem, v))
              {
            this.pointrewardByitem = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointrewardField
     *
     * @return String
     */
    public String getPointrewardField()
    {
        return pointrewardField;
    }

                        
    /**
     * Set the value of PointrewardField
     *
     * @param v new value
     */
    public void setPointrewardField(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointrewardField, v))
              {
            this.pointrewardField = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PointrewardPvalue
     *
     * @return String
     */
    public String getPointrewardPvalue()
    {
        return pointrewardPvalue;
    }

                        
    /**
     * Set the value of PointrewardPvalue
     *
     * @param v new value
     */
    public void setPointrewardPvalue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pointrewardPvalue, v))
              {
            this.pointrewardPvalue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultSearchCond
     *
     * @return String
     */
    public String getDefaultSearchCond()
    {
        return defaultSearchCond;
    }

                        
    /**
     * Set the value of DefaultSearchCond
     *
     * @param v new value
     */
    public void setDefaultSearchCond(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultSearchCond, v))
              {
            this.defaultSearchCond = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeyNewTrans
     *
     * @return String
     */
    public String getKeyNewTrans()
    {
        return keyNewTrans;
    }

                        
    /**
     * Set the value of KeyNewTrans
     *
     * @param v new value
     */
    public void setKeyNewTrans(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keyNewTrans, v))
              {
            this.keyNewTrans = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeyOpenLookup
     *
     * @return String
     */
    public String getKeyOpenLookup()
    {
        return keyOpenLookup;
    }

                        
    /**
     * Set the value of KeyOpenLookup
     *
     * @param v new value
     */
    public void setKeyOpenLookup(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keyOpenLookup, v))
              {
            this.keyOpenLookup = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeyPaymentFocus
     *
     * @return String
     */
    public String getKeyPaymentFocus()
    {
        return keyPaymentFocus;
    }

                        
    /**
     * Set the value of KeyPaymentFocus
     *
     * @param v new value
     */
    public void setKeyPaymentFocus(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keyPaymentFocus, v))
              {
            this.keyPaymentFocus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeySaveTrans
     *
     * @return String
     */
    public String getKeySaveTrans()
    {
        return keySaveTrans;
    }

                        
    /**
     * Set the value of KeySaveTrans
     *
     * @param v new value
     */
    public void setKeySaveTrans(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keySaveTrans, v))
              {
            this.keySaveTrans = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeyPrintTrans
     *
     * @return String
     */
    public String getKeyPrintTrans()
    {
        return keyPrintTrans;
    }

                        
    /**
     * Set the value of KeyPrintTrans
     *
     * @param v new value
     */
    public void setKeyPrintTrans(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keyPrintTrans, v))
              {
            this.keyPrintTrans = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeyFindTrans
     *
     * @return String
     */
    public String getKeyFindTrans()
    {
        return keyFindTrans;
    }

                        
    /**
     * Set the value of KeyFindTrans
     *
     * @param v new value
     */
    public void setKeyFindTrans(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keyFindTrans, v))
              {
            this.keyFindTrans = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the KeySelectPayment
     *
     * @return String
     */
    public String getKeySelectPayment()
    {
        return keySelectPayment;
    }

                        
    /**
     * Set the value of KeySelectPayment
     *
     * @param v new value
     */
    public void setKeySelectPayment(String v) 
    {
    
                  if (!ObjectUtils.equals(this.keySelectPayment, v))
              {
            this.keySelectPayment = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SysConfigId");
              fieldNames.add("PosDefaultScreen");
              fieldNames.add("PosDefaultScreenFocus");
              fieldNames.add("PosDefaultOpenCashierAmt");
              fieldNames.add("PosDefaultItemDisplay");
              fieldNames.add("PosDefaultItemSort");
              fieldNames.add("PosDefaultPrint");
              fieldNames.add("PosConfirmQty");
              fieldNames.add("PosConfirmCost");
              fieldNames.add("PosChangeValidation");
              fieldNames.add("PosDeleteValidation");
              fieldNames.add("PosOpenCashierValidation");
              fieldNames.add("PosSaveDirectPrint");
              fieldNames.add("PosPrinterDetailLine");
              fieldNames.add("PosDirectAdd");
              fieldNames.add("PosRoundingTo");
              fieldNames.add("PosRoundingMode");
              fieldNames.add("PosSpcDiscCode");
              fieldNames.add("ComplexBarcodeUse");
              fieldNames.add("ComplexBarcodePrefix");
              fieldNames.add("ComplexBarcodePluStart");
              fieldNames.add("ComplexBarcodePluLength");
              fieldNames.add("ComplexBarcodeQtyStart");
              fieldNames.add("ComplexBarcodeQtyLength");
              fieldNames.add("ComplexBarcodeQtyDecPos");
              fieldNames.add("ComplexBarcodeOtherStart");
              fieldNames.add("ComplexBarcodeOtherLength");
              fieldNames.add("ComplexBarcodeOtherDecPos");
              fieldNames.add("ComplexBarcodeOtherType");
              fieldNames.add("ComplexBarcodeSuffix");
              fieldNames.add("ComplexBarcodeTotalLength");
              fieldNames.add("BackupOutputDir");
              fieldNames.add("BackupFilePref");
              fieldNames.add("PicturePath");
              fieldNames.add("B2bPoPath");
              fieldNames.add("IreportPath");
              fieldNames.add("OnlineHelpUrl");
              fieldNames.add("SmsGatewayUrl");
              fieldNames.add("EmailServer");
              fieldNames.add("EmailUser");
              fieldNames.add("EmailPwd");
              fieldNames.add("SyncAllowPendingTrans");
              fieldNames.add("SyncFullInventory");
              fieldNames.add("SyncAllowStoreAdj");
              fieldNames.add("SyncAllowStoreReceipt");
              fieldNames.add("SyncAllowFreeTransfer");
              fieldNames.add("SyncHodataPath");
              fieldNames.add("SyncStoredataPath");
              fieldNames.add("SyncStoresetupPath");
              fieldNames.add("SyncStoretrfPath");
              fieldNames.add("SyncDestination");
              fieldNames.add("SyncDiskDestination");
              fieldNames.add("SyncHoUploadUrl");
              fieldNames.add("SyncHoEmail");
              fieldNames.add("SyncStoreEmailServer");
              fieldNames.add("SyncStoreEmailUser");
              fieldNames.add("SyncStoreEmailPwd");
              fieldNames.add("SyncDailyClosing");
              fieldNames.add("UseLocationCtltable");
              fieldNames.add("UseCustomerTrPrefix");
              fieldNames.add("UseVendorTrPrefix");
              fieldNames.add("UseVendorTax");
              fieldNames.add("UseCustomerTax");
              fieldNames.add("UseCustomerLocation");
              fieldNames.add("UseCfInOut");
              fieldNames.add("UseDistModules");
              fieldNames.add("UseWstModules");
              fieldNames.add("UseEdiModules");
              fieldNames.add("UseBom");
              fieldNames.add("UsePdt");
              fieldNames.add("ConfirmTrfAtToloc");
              fieldNames.add("SiAllowImportSo");
              fieldNames.add("SiDefaultScreen");
              fieldNames.add("ItemColorLtMin");
              fieldNames.add("ItemColorLtRop");
              fieldNames.add("ItemColorGtMax");
              fieldNames.add("ItemCodeLength");
              fieldNames.add("CustCodeLength");
              fieldNames.add("VendCodeLength");
              fieldNames.add("OthrCodeLength");
              fieldNames.add("SalesInclusiveTax");
              fieldNames.add("PurchInclusiveTax");
              fieldNames.add("SalesMultiCurrency");
              fieldNames.add("PurchMultiCurrency");
              fieldNames.add("SalesMergeSameItem");
              fieldNames.add("PurchMergeSameItem");
              fieldNames.add("SalesSortBy");
              fieldNames.add("PurchSortBy");
              fieldNames.add("SalesFirstlineInsert");
              fieldNames.add("PurchFirstlineInsert");
              fieldNames.add("InvenFirstlineInsert");
              fieldNames.add("SalesFobCourier");
              fieldNames.add("PurchFobCourier");
              fieldNames.add("SalesCommaScale");
              fieldNames.add("PurchCommaScale");
              fieldNames.add("InvenCommaScale");
              fieldNames.add("CustMaxSbMode");
              fieldNames.add("VendMaxSbMode");
              fieldNames.add("PurchPrQtyGtPo");
              fieldNames.add("PurchPrQtyEqualPo");
              fieldNames.add("PurchLimitByEmp");
              fieldNames.add("PiAllowImportPo");
              fieldNames.add("PiLastPurchMethod");
              fieldNames.add("PiUpdateLastPurchase");
              fieldNames.add("PiUpdateSalesPrice");
              fieldNames.add("PiUpdatePrCost");
              fieldNames.add("SalesUseSalesman");
              fieldNames.add("SalesUseMinPrice");
              fieldNames.add("SalesSalesmanPeritem");
              fieldNames.add("SalesAllowMultipmtDisc");
              fieldNames.add("SalesEmplevelDisc");
              fieldNames.add("SalesCustomPricingUse");
              fieldNames.add("SalesCustomPricingTpl");
              fieldNames.add("PointrewardUse");
              fieldNames.add("PointrewardByitem");
              fieldNames.add("PointrewardField");
              fieldNames.add("PointrewardPvalue");
              fieldNames.add("DefaultSearchCond");
              fieldNames.add("KeyNewTrans");
              fieldNames.add("KeyOpenLookup");
              fieldNames.add("KeyPaymentFocus");
              fieldNames.add("KeySaveTrans");
              fieldNames.add("KeyPrintTrans");
              fieldNames.add("KeyFindTrans");
              fieldNames.add("KeySelectPayment");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SysConfigId"))
        {
                return getSysConfigId();
            }
          if (name.equals("PosDefaultScreen"))
        {
                return getPosDefaultScreen();
            }
          if (name.equals("PosDefaultScreenFocus"))
        {
                return getPosDefaultScreenFocus();
            }
          if (name.equals("PosDefaultOpenCashierAmt"))
        {
                return getPosDefaultOpenCashierAmt();
            }
          if (name.equals("PosDefaultItemDisplay"))
        {
                return getPosDefaultItemDisplay();
            }
          if (name.equals("PosDefaultItemSort"))
        {
                return getPosDefaultItemSort();
            }
          if (name.equals("PosDefaultPrint"))
        {
                return getPosDefaultPrint();
            }
          if (name.equals("PosConfirmQty"))
        {
                return getPosConfirmQty();
            }
          if (name.equals("PosConfirmCost"))
        {
                return getPosConfirmCost();
            }
          if (name.equals("PosChangeValidation"))
        {
                return getPosChangeValidation();
            }
          if (name.equals("PosDeleteValidation"))
        {
                return getPosDeleteValidation();
            }
          if (name.equals("PosOpenCashierValidation"))
        {
                return getPosOpenCashierValidation();
            }
          if (name.equals("PosSaveDirectPrint"))
        {
                return getPosSaveDirectPrint();
            }
          if (name.equals("PosPrinterDetailLine"))
        {
                return getPosPrinterDetailLine();
            }
          if (name.equals("PosDirectAdd"))
        {
                return getPosDirectAdd();
            }
          if (name.equals("PosRoundingTo"))
        {
                return getPosRoundingTo();
            }
          if (name.equals("PosRoundingMode"))
        {
                return getPosRoundingMode();
            }
          if (name.equals("PosSpcDiscCode"))
        {
                return getPosSpcDiscCode();
            }
          if (name.equals("ComplexBarcodeUse"))
        {
                return getComplexBarcodeUse();
            }
          if (name.equals("ComplexBarcodePrefix"))
        {
                return getComplexBarcodePrefix();
            }
          if (name.equals("ComplexBarcodePluStart"))
        {
                return getComplexBarcodePluStart();
            }
          if (name.equals("ComplexBarcodePluLength"))
        {
                return getComplexBarcodePluLength();
            }
          if (name.equals("ComplexBarcodeQtyStart"))
        {
                return getComplexBarcodeQtyStart();
            }
          if (name.equals("ComplexBarcodeQtyLength"))
        {
                return getComplexBarcodeQtyLength();
            }
          if (name.equals("ComplexBarcodeQtyDecPos"))
        {
                return getComplexBarcodeQtyDecPos();
            }
          if (name.equals("ComplexBarcodeOtherStart"))
        {
                return getComplexBarcodeOtherStart();
            }
          if (name.equals("ComplexBarcodeOtherLength"))
        {
                return getComplexBarcodeOtherLength();
            }
          if (name.equals("ComplexBarcodeOtherDecPos"))
        {
                return getComplexBarcodeOtherDecPos();
            }
          if (name.equals("ComplexBarcodeOtherType"))
        {
                return getComplexBarcodeOtherType();
            }
          if (name.equals("ComplexBarcodeSuffix"))
        {
                return getComplexBarcodeSuffix();
            }
          if (name.equals("ComplexBarcodeTotalLength"))
        {
                return getComplexBarcodeTotalLength();
            }
          if (name.equals("BackupOutputDir"))
        {
                return getBackupOutputDir();
            }
          if (name.equals("BackupFilePref"))
        {
                return getBackupFilePref();
            }
          if (name.equals("PicturePath"))
        {
                return getPicturePath();
            }
          if (name.equals("B2bPoPath"))
        {
                return getB2bPoPath();
            }
          if (name.equals("IreportPath"))
        {
                return getIreportPath();
            }
          if (name.equals("OnlineHelpUrl"))
        {
                return getOnlineHelpUrl();
            }
          if (name.equals("SmsGatewayUrl"))
        {
                return getSmsGatewayUrl();
            }
          if (name.equals("EmailServer"))
        {
                return getEmailServer();
            }
          if (name.equals("EmailUser"))
        {
                return getEmailUser();
            }
          if (name.equals("EmailPwd"))
        {
                return getEmailPwd();
            }
          if (name.equals("SyncAllowPendingTrans"))
        {
                return getSyncAllowPendingTrans();
            }
          if (name.equals("SyncFullInventory"))
        {
                return getSyncFullInventory();
            }
          if (name.equals("SyncAllowStoreAdj"))
        {
                return getSyncAllowStoreAdj();
            }
          if (name.equals("SyncAllowStoreReceipt"))
        {
                return getSyncAllowStoreReceipt();
            }
          if (name.equals("SyncAllowFreeTransfer"))
        {
                return getSyncAllowFreeTransfer();
            }
          if (name.equals("SyncHodataPath"))
        {
                return getSyncHodataPath();
            }
          if (name.equals("SyncStoredataPath"))
        {
                return getSyncStoredataPath();
            }
          if (name.equals("SyncStoresetupPath"))
        {
                return getSyncStoresetupPath();
            }
          if (name.equals("SyncStoretrfPath"))
        {
                return getSyncStoretrfPath();
            }
          if (name.equals("SyncDestination"))
        {
                return getSyncDestination();
            }
          if (name.equals("SyncDiskDestination"))
        {
                return getSyncDiskDestination();
            }
          if (name.equals("SyncHoUploadUrl"))
        {
                return getSyncHoUploadUrl();
            }
          if (name.equals("SyncHoEmail"))
        {
                return getSyncHoEmail();
            }
          if (name.equals("SyncStoreEmailServer"))
        {
                return getSyncStoreEmailServer();
            }
          if (name.equals("SyncStoreEmailUser"))
        {
                return getSyncStoreEmailUser();
            }
          if (name.equals("SyncStoreEmailPwd"))
        {
                return getSyncStoreEmailPwd();
            }
          if (name.equals("SyncDailyClosing"))
        {
                return getSyncDailyClosing();
            }
          if (name.equals("UseLocationCtltable"))
        {
                return getUseLocationCtltable();
            }
          if (name.equals("UseCustomerTrPrefix"))
        {
                return getUseCustomerTrPrefix();
            }
          if (name.equals("UseVendorTrPrefix"))
        {
                return getUseVendorTrPrefix();
            }
          if (name.equals("UseVendorTax"))
        {
                return getUseVendorTax();
            }
          if (name.equals("UseCustomerTax"))
        {
                return getUseCustomerTax();
            }
          if (name.equals("UseCustomerLocation"))
        {
                return getUseCustomerLocation();
            }
          if (name.equals("UseCfInOut"))
        {
                return getUseCfInOut();
            }
          if (name.equals("UseDistModules"))
        {
                return getUseDistModules();
            }
          if (name.equals("UseWstModules"))
        {
                return getUseWstModules();
            }
          if (name.equals("UseEdiModules"))
        {
                return getUseEdiModules();
            }
          if (name.equals("UseBom"))
        {
                return getUseBom();
            }
          if (name.equals("UsePdt"))
        {
                return getUsePdt();
            }
          if (name.equals("ConfirmTrfAtToloc"))
        {
                return getConfirmTrfAtToloc();
            }
          if (name.equals("SiAllowImportSo"))
        {
                return getSiAllowImportSo();
            }
          if (name.equals("SiDefaultScreen"))
        {
                return getSiDefaultScreen();
            }
          if (name.equals("ItemColorLtMin"))
        {
                return getItemColorLtMin();
            }
          if (name.equals("ItemColorLtRop"))
        {
                return getItemColorLtRop();
            }
          if (name.equals("ItemColorGtMax"))
        {
                return getItemColorGtMax();
            }
          if (name.equals("ItemCodeLength"))
        {
                return getItemCodeLength();
            }
          if (name.equals("CustCodeLength"))
        {
                return getCustCodeLength();
            }
          if (name.equals("VendCodeLength"))
        {
                return getVendCodeLength();
            }
          if (name.equals("OthrCodeLength"))
        {
                return getOthrCodeLength();
            }
          if (name.equals("SalesInclusiveTax"))
        {
                return getSalesInclusiveTax();
            }
          if (name.equals("PurchInclusiveTax"))
        {
                return getPurchInclusiveTax();
            }
          if (name.equals("SalesMultiCurrency"))
        {
                return getSalesMultiCurrency();
            }
          if (name.equals("PurchMultiCurrency"))
        {
                return getPurchMultiCurrency();
            }
          if (name.equals("SalesMergeSameItem"))
        {
                return getSalesMergeSameItem();
            }
          if (name.equals("PurchMergeSameItem"))
        {
                return getPurchMergeSameItem();
            }
          if (name.equals("SalesSortBy"))
        {
                return getSalesSortBy();
            }
          if (name.equals("PurchSortBy"))
        {
                return getPurchSortBy();
            }
          if (name.equals("SalesFirstlineInsert"))
        {
                return getSalesFirstlineInsert();
            }
          if (name.equals("PurchFirstlineInsert"))
        {
                return getPurchFirstlineInsert();
            }
          if (name.equals("InvenFirstlineInsert"))
        {
                return getInvenFirstlineInsert();
            }
          if (name.equals("SalesFobCourier"))
        {
                return getSalesFobCourier();
            }
          if (name.equals("PurchFobCourier"))
        {
                return getPurchFobCourier();
            }
          if (name.equals("SalesCommaScale"))
        {
                return getSalesCommaScale();
            }
          if (name.equals("PurchCommaScale"))
        {
                return getPurchCommaScale();
            }
          if (name.equals("InvenCommaScale"))
        {
                return getInvenCommaScale();
            }
          if (name.equals("CustMaxSbMode"))
        {
                return getCustMaxSbMode();
            }
          if (name.equals("VendMaxSbMode"))
        {
                return getVendMaxSbMode();
            }
          if (name.equals("PurchPrQtyGtPo"))
        {
                return getPurchPrQtyGtPo();
            }
          if (name.equals("PurchPrQtyEqualPo"))
        {
                return getPurchPrQtyEqualPo();
            }
          if (name.equals("PurchLimitByEmp"))
        {
                return getPurchLimitByEmp();
            }
          if (name.equals("PiAllowImportPo"))
        {
                return getPiAllowImportPo();
            }
          if (name.equals("PiLastPurchMethod"))
        {
                return getPiLastPurchMethod();
            }
          if (name.equals("PiUpdateLastPurchase"))
        {
                return getPiUpdateLastPurchase();
            }
          if (name.equals("PiUpdateSalesPrice"))
        {
                return getPiUpdateSalesPrice();
            }
          if (name.equals("PiUpdatePrCost"))
        {
                return getPiUpdatePrCost();
            }
          if (name.equals("SalesUseSalesman"))
        {
                return getSalesUseSalesman();
            }
          if (name.equals("SalesUseMinPrice"))
        {
                return getSalesUseMinPrice();
            }
          if (name.equals("SalesSalesmanPeritem"))
        {
                return getSalesSalesmanPeritem();
            }
          if (name.equals("SalesAllowMultipmtDisc"))
        {
                return getSalesAllowMultipmtDisc();
            }
          if (name.equals("SalesEmplevelDisc"))
        {
                return getSalesEmplevelDisc();
            }
          if (name.equals("SalesCustomPricingUse"))
        {
                return getSalesCustomPricingUse();
            }
          if (name.equals("SalesCustomPricingTpl"))
        {
                return getSalesCustomPricingTpl();
            }
          if (name.equals("PointrewardUse"))
        {
                return getPointrewardUse();
            }
          if (name.equals("PointrewardByitem"))
        {
                return getPointrewardByitem();
            }
          if (name.equals("PointrewardField"))
        {
                return getPointrewardField();
            }
          if (name.equals("PointrewardPvalue"))
        {
                return getPointrewardPvalue();
            }
          if (name.equals("DefaultSearchCond"))
        {
                return getDefaultSearchCond();
            }
          if (name.equals("KeyNewTrans"))
        {
                return getKeyNewTrans();
            }
          if (name.equals("KeyOpenLookup"))
        {
                return getKeyOpenLookup();
            }
          if (name.equals("KeyPaymentFocus"))
        {
                return getKeyPaymentFocus();
            }
          if (name.equals("KeySaveTrans"))
        {
                return getKeySaveTrans();
            }
          if (name.equals("KeyPrintTrans"))
        {
                return getKeyPrintTrans();
            }
          if (name.equals("KeyFindTrans"))
        {
                return getKeyFindTrans();
            }
          if (name.equals("KeySelectPayment"))
        {
                return getKeySelectPayment();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SysConfigPeer.SYS_CONFIG_ID))
        {
                return getSysConfigId();
            }
          if (name.equals(SysConfigPeer.POS_DEFAULT_SCREEN))
        {
                return getPosDefaultScreen();
            }
          if (name.equals(SysConfigPeer.POS_DEFAULT_SCREEN_FOCUS))
        {
                return getPosDefaultScreenFocus();
            }
          if (name.equals(SysConfigPeer.POS_DEFAULT_OPEN_CASHIER_AMT))
        {
                return getPosDefaultOpenCashierAmt();
            }
          if (name.equals(SysConfigPeer.POS_DEFAULT_ITEM_DISPLAY))
        {
                return getPosDefaultItemDisplay();
            }
          if (name.equals(SysConfigPeer.POS_DEFAULT_ITEM_SORT))
        {
                return getPosDefaultItemSort();
            }
          if (name.equals(SysConfigPeer.POS_DEFAULT_PRINT))
        {
                return getPosDefaultPrint();
            }
          if (name.equals(SysConfigPeer.POS_CONFIRM_QTY))
        {
                return getPosConfirmQty();
            }
          if (name.equals(SysConfigPeer.POS_CONFIRM_COST))
        {
                return getPosConfirmCost();
            }
          if (name.equals(SysConfigPeer.POS_CHANGE_VALIDATION))
        {
                return getPosChangeValidation();
            }
          if (name.equals(SysConfigPeer.POS_DELETE_VALIDATION))
        {
                return getPosDeleteValidation();
            }
          if (name.equals(SysConfigPeer.POS_OPEN_CASHIER_VALIDATION))
        {
                return getPosOpenCashierValidation();
            }
          if (name.equals(SysConfigPeer.POS_SAVE_DIRECT_PRINT))
        {
                return getPosSaveDirectPrint();
            }
          if (name.equals(SysConfigPeer.POS_PRINTER_DETAIL_LINE))
        {
                return getPosPrinterDetailLine();
            }
          if (name.equals(SysConfigPeer.POS_DIRECT_ADD))
        {
                return getPosDirectAdd();
            }
          if (name.equals(SysConfigPeer.POS_ROUNDING_TO))
        {
                return getPosRoundingTo();
            }
          if (name.equals(SysConfigPeer.POS_ROUNDING_MODE))
        {
                return getPosRoundingMode();
            }
          if (name.equals(SysConfigPeer.POS_SPC_DISC_CODE))
        {
                return getPosSpcDiscCode();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_USE))
        {
                return getComplexBarcodeUse();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_PREFIX))
        {
                return getComplexBarcodePrefix();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_PLU_START))
        {
                return getComplexBarcodePluStart();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_PLU_LENGTH))
        {
                return getComplexBarcodePluLength();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_QTY_START))
        {
                return getComplexBarcodeQtyStart();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_QTY_LENGTH))
        {
                return getComplexBarcodeQtyLength();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_QTY_DEC_POS))
        {
                return getComplexBarcodeQtyDecPos();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_OTHER_START))
        {
                return getComplexBarcodeOtherStart();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_OTHER_LENGTH))
        {
                return getComplexBarcodeOtherLength();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_OTHER_DEC_POS))
        {
                return getComplexBarcodeOtherDecPos();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_OTHER_TYPE))
        {
                return getComplexBarcodeOtherType();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_SUFFIX))
        {
                return getComplexBarcodeSuffix();
            }
          if (name.equals(SysConfigPeer.COMPLEX_BARCODE_TOTAL_LENGTH))
        {
                return getComplexBarcodeTotalLength();
            }
          if (name.equals(SysConfigPeer.BACKUP_OUTPUT_DIR))
        {
                return getBackupOutputDir();
            }
          if (name.equals(SysConfigPeer.BACKUP_FILE_PREF))
        {
                return getBackupFilePref();
            }
          if (name.equals(SysConfigPeer.PICTURE_PATH))
        {
                return getPicturePath();
            }
          if (name.equals(SysConfigPeer.B2B_PO_PATH))
        {
                return getB2bPoPath();
            }
          if (name.equals(SysConfigPeer.IREPORT_PATH))
        {
                return getIreportPath();
            }
          if (name.equals(SysConfigPeer.ONLINE_HELP_URL))
        {
                return getOnlineHelpUrl();
            }
          if (name.equals(SysConfigPeer.SMS_GATEWAY_URL))
        {
                return getSmsGatewayUrl();
            }
          if (name.equals(SysConfigPeer.EMAIL_SERVER))
        {
                return getEmailServer();
            }
          if (name.equals(SysConfigPeer.EMAIL_USER))
        {
                return getEmailUser();
            }
          if (name.equals(SysConfigPeer.EMAIL_PWD))
        {
                return getEmailPwd();
            }
          if (name.equals(SysConfigPeer.SYNC_ALLOW_PENDING_TRANS))
        {
                return getSyncAllowPendingTrans();
            }
          if (name.equals(SysConfigPeer.SYNC_FULL_INVENTORY))
        {
                return getSyncFullInventory();
            }
          if (name.equals(SysConfigPeer.SYNC_ALLOW_STORE_ADJ))
        {
                return getSyncAllowStoreAdj();
            }
          if (name.equals(SysConfigPeer.SYNC_ALLOW_STORE_RECEIPT))
        {
                return getSyncAllowStoreReceipt();
            }
          if (name.equals(SysConfigPeer.SYNC_ALLOW_FREE_TRANSFER))
        {
                return getSyncAllowFreeTransfer();
            }
          if (name.equals(SysConfigPeer.SYNC_HODATA_PATH))
        {
                return getSyncHodataPath();
            }
          if (name.equals(SysConfigPeer.SYNC_STOREDATA_PATH))
        {
                return getSyncStoredataPath();
            }
          if (name.equals(SysConfigPeer.SYNC_STORESETUP_PATH))
        {
                return getSyncStoresetupPath();
            }
          if (name.equals(SysConfigPeer.SYNC_STORETRF_PATH))
        {
                return getSyncStoretrfPath();
            }
          if (name.equals(SysConfigPeer.SYNC_DESTINATION))
        {
                return getSyncDestination();
            }
          if (name.equals(SysConfigPeer.SYNC_DISK_DESTINATION))
        {
                return getSyncDiskDestination();
            }
          if (name.equals(SysConfigPeer.SYNC_HO_UPLOAD_URL))
        {
                return getSyncHoUploadUrl();
            }
          if (name.equals(SysConfigPeer.SYNC_HO_EMAIL))
        {
                return getSyncHoEmail();
            }
          if (name.equals(SysConfigPeer.SYNC_STORE_EMAIL_SERVER))
        {
                return getSyncStoreEmailServer();
            }
          if (name.equals(SysConfigPeer.SYNC_STORE_EMAIL_USER))
        {
                return getSyncStoreEmailUser();
            }
          if (name.equals(SysConfigPeer.SYNC_STORE_EMAIL_PWD))
        {
                return getSyncStoreEmailPwd();
            }
          if (name.equals(SysConfigPeer.SYNC_DAILY_CLOSING))
        {
                return getSyncDailyClosing();
            }
          if (name.equals(SysConfigPeer.USE_LOCATION_CTLTABLE))
        {
                return getUseLocationCtltable();
            }
          if (name.equals(SysConfigPeer.USE_CUSTOMER_TR_PREFIX))
        {
                return getUseCustomerTrPrefix();
            }
          if (name.equals(SysConfigPeer.USE_VENDOR_TR_PREFIX))
        {
                return getUseVendorTrPrefix();
            }
          if (name.equals(SysConfigPeer.USE_VENDOR_TAX))
        {
                return getUseVendorTax();
            }
          if (name.equals(SysConfigPeer.USE_CUSTOMER_TAX))
        {
                return getUseCustomerTax();
            }
          if (name.equals(SysConfigPeer.USE_CUSTOMER_LOCATION))
        {
                return getUseCustomerLocation();
            }
          if (name.equals(SysConfigPeer.USE_CF_IN_OUT))
        {
                return getUseCfInOut();
            }
          if (name.equals(SysConfigPeer.USE_DIST_MODULES))
        {
                return getUseDistModules();
            }
          if (name.equals(SysConfigPeer.USE_WST_MODULES))
        {
                return getUseWstModules();
            }
          if (name.equals(SysConfigPeer.USE_EDI_MODULES))
        {
                return getUseEdiModules();
            }
          if (name.equals(SysConfigPeer.USE_BOM))
        {
                return getUseBom();
            }
          if (name.equals(SysConfigPeer.USE_PDT))
        {
                return getUsePdt();
            }
          if (name.equals(SysConfigPeer.CONFIRM_TRF_AT_TOLOC))
        {
                return getConfirmTrfAtToloc();
            }
          if (name.equals(SysConfigPeer.SI_ALLOW_IMPORT_SO))
        {
                return getSiAllowImportSo();
            }
          if (name.equals(SysConfigPeer.SI_DEFAULT_SCREEN))
        {
                return getSiDefaultScreen();
            }
          if (name.equals(SysConfigPeer.ITEM_COLOR_LT_MIN))
        {
                return getItemColorLtMin();
            }
          if (name.equals(SysConfigPeer.ITEM_COLOR_LT_ROP))
        {
                return getItemColorLtRop();
            }
          if (name.equals(SysConfigPeer.ITEM_COLOR_GT_MAX))
        {
                return getItemColorGtMax();
            }
          if (name.equals(SysConfigPeer.ITEM_CODE_LENGTH))
        {
                return getItemCodeLength();
            }
          if (name.equals(SysConfigPeer.CUST_CODE_LENGTH))
        {
                return getCustCodeLength();
            }
          if (name.equals(SysConfigPeer.VEND_CODE_LENGTH))
        {
                return getVendCodeLength();
            }
          if (name.equals(SysConfigPeer.OTHR_CODE_LENGTH))
        {
                return getOthrCodeLength();
            }
          if (name.equals(SysConfigPeer.SALES_INCLUSIVE_TAX))
        {
                return getSalesInclusiveTax();
            }
          if (name.equals(SysConfigPeer.PURCH_INCLUSIVE_TAX))
        {
                return getPurchInclusiveTax();
            }
          if (name.equals(SysConfigPeer.SALES_MULTI_CURRENCY))
        {
                return getSalesMultiCurrency();
            }
          if (name.equals(SysConfigPeer.PURCH_MULTI_CURRENCY))
        {
                return getPurchMultiCurrency();
            }
          if (name.equals(SysConfigPeer.SALES_MERGE_SAME_ITEM))
        {
                return getSalesMergeSameItem();
            }
          if (name.equals(SysConfigPeer.PURCH_MERGE_SAME_ITEM))
        {
                return getPurchMergeSameItem();
            }
          if (name.equals(SysConfigPeer.SALES_SORT_BY))
        {
                return getSalesSortBy();
            }
          if (name.equals(SysConfigPeer.PURCH_SORT_BY))
        {
                return getPurchSortBy();
            }
          if (name.equals(SysConfigPeer.SALES_FIRSTLINE_INSERT))
        {
                return getSalesFirstlineInsert();
            }
          if (name.equals(SysConfigPeer.PURCH_FIRSTLINE_INSERT))
        {
                return getPurchFirstlineInsert();
            }
          if (name.equals(SysConfigPeer.INVEN_FIRSTLINE_INSERT))
        {
                return getInvenFirstlineInsert();
            }
          if (name.equals(SysConfigPeer.SALES_FOB_COURIER))
        {
                return getSalesFobCourier();
            }
          if (name.equals(SysConfigPeer.PURCH_FOB_COURIER))
        {
                return getPurchFobCourier();
            }
          if (name.equals(SysConfigPeer.SALES_COMMA_SCALE))
        {
                return getSalesCommaScale();
            }
          if (name.equals(SysConfigPeer.PURCH_COMMA_SCALE))
        {
                return getPurchCommaScale();
            }
          if (name.equals(SysConfigPeer.INVEN_COMMA_SCALE))
        {
                return getInvenCommaScale();
            }
          if (name.equals(SysConfigPeer.CUST_MAX_SB_MODE))
        {
                return getCustMaxSbMode();
            }
          if (name.equals(SysConfigPeer.VEND_MAX_SB_MODE))
        {
                return getVendMaxSbMode();
            }
          if (name.equals(SysConfigPeer.PURCH_PR_QTY_GT_PO))
        {
                return getPurchPrQtyGtPo();
            }
          if (name.equals(SysConfigPeer.PURCH_PR_QTY_EQUAL_PO))
        {
                return getPurchPrQtyEqualPo();
            }
          if (name.equals(SysConfigPeer.PURCH_LIMIT_BY_EMP))
        {
                return getPurchLimitByEmp();
            }
          if (name.equals(SysConfigPeer.PI_ALLOW_IMPORT_PO))
        {
                return getPiAllowImportPo();
            }
          if (name.equals(SysConfigPeer.PI_LAST_PURCH_METHOD))
        {
                return getPiLastPurchMethod();
            }
          if (name.equals(SysConfigPeer.PI_UPDATE_LAST_PURCHASE))
        {
                return getPiUpdateLastPurchase();
            }
          if (name.equals(SysConfigPeer.PI_UPDATE_SALES_PRICE))
        {
                return getPiUpdateSalesPrice();
            }
          if (name.equals(SysConfigPeer.PI_UPDATE_PR_COST))
        {
                return getPiUpdatePrCost();
            }
          if (name.equals(SysConfigPeer.SALES_USE_SALESMAN))
        {
                return getSalesUseSalesman();
            }
          if (name.equals(SysConfigPeer.SALES_USE_MIN_PRICE))
        {
                return getSalesUseMinPrice();
            }
          if (name.equals(SysConfigPeer.SALES_SALESMAN_PERITEM))
        {
                return getSalesSalesmanPeritem();
            }
          if (name.equals(SysConfigPeer.SALES_ALLOW_MULTIPMT_DISC))
        {
                return getSalesAllowMultipmtDisc();
            }
          if (name.equals(SysConfigPeer.SALES_EMPLEVEL_DISC))
        {
                return getSalesEmplevelDisc();
            }
          if (name.equals(SysConfigPeer.SALES_CUSTOM_PRICING_USE))
        {
                return getSalesCustomPricingUse();
            }
          if (name.equals(SysConfigPeer.SALES_CUSTOM_PRICING_TPL))
        {
                return getSalesCustomPricingTpl();
            }
          if (name.equals(SysConfigPeer.POINTREWARD_USE))
        {
                return getPointrewardUse();
            }
          if (name.equals(SysConfigPeer.POINTREWARD_BYITEM))
        {
                return getPointrewardByitem();
            }
          if (name.equals(SysConfigPeer.POINTREWARD_FIELD))
        {
                return getPointrewardField();
            }
          if (name.equals(SysConfigPeer.POINTREWARD_PVALUE))
        {
                return getPointrewardPvalue();
            }
          if (name.equals(SysConfigPeer.DEFAULT_SEARCH_COND))
        {
                return getDefaultSearchCond();
            }
          if (name.equals(SysConfigPeer.KEY_NEW_TRANS))
        {
                return getKeyNewTrans();
            }
          if (name.equals(SysConfigPeer.KEY_OPEN_LOOKUP))
        {
                return getKeyOpenLookup();
            }
          if (name.equals(SysConfigPeer.KEY_PAYMENT_FOCUS))
        {
                return getKeyPaymentFocus();
            }
          if (name.equals(SysConfigPeer.KEY_SAVE_TRANS))
        {
                return getKeySaveTrans();
            }
          if (name.equals(SysConfigPeer.KEY_PRINT_TRANS))
        {
                return getKeyPrintTrans();
            }
          if (name.equals(SysConfigPeer.KEY_FIND_TRANS))
        {
                return getKeyFindTrans();
            }
          if (name.equals(SysConfigPeer.KEY_SELECT_PAYMENT))
        {
                return getKeySelectPayment();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSysConfigId();
            }
              if (pos == 1)
        {
                return getPosDefaultScreen();
            }
              if (pos == 2)
        {
                return getPosDefaultScreenFocus();
            }
              if (pos == 3)
        {
                return getPosDefaultOpenCashierAmt();
            }
              if (pos == 4)
        {
                return getPosDefaultItemDisplay();
            }
              if (pos == 5)
        {
                return getPosDefaultItemSort();
            }
              if (pos == 6)
        {
                return getPosDefaultPrint();
            }
              if (pos == 7)
        {
                return getPosConfirmQty();
            }
              if (pos == 8)
        {
                return getPosConfirmCost();
            }
              if (pos == 9)
        {
                return getPosChangeValidation();
            }
              if (pos == 10)
        {
                return getPosDeleteValidation();
            }
              if (pos == 11)
        {
                return getPosOpenCashierValidation();
            }
              if (pos == 12)
        {
                return getPosSaveDirectPrint();
            }
              if (pos == 13)
        {
                return getPosPrinterDetailLine();
            }
              if (pos == 14)
        {
                return getPosDirectAdd();
            }
              if (pos == 15)
        {
                return getPosRoundingTo();
            }
              if (pos == 16)
        {
                return getPosRoundingMode();
            }
              if (pos == 17)
        {
                return getPosSpcDiscCode();
            }
              if (pos == 18)
        {
                return getComplexBarcodeUse();
            }
              if (pos == 19)
        {
                return getComplexBarcodePrefix();
            }
              if (pos == 20)
        {
                return getComplexBarcodePluStart();
            }
              if (pos == 21)
        {
                return getComplexBarcodePluLength();
            }
              if (pos == 22)
        {
                return getComplexBarcodeQtyStart();
            }
              if (pos == 23)
        {
                return getComplexBarcodeQtyLength();
            }
              if (pos == 24)
        {
                return getComplexBarcodeQtyDecPos();
            }
              if (pos == 25)
        {
                return getComplexBarcodeOtherStart();
            }
              if (pos == 26)
        {
                return getComplexBarcodeOtherLength();
            }
              if (pos == 27)
        {
                return getComplexBarcodeOtherDecPos();
            }
              if (pos == 28)
        {
                return getComplexBarcodeOtherType();
            }
              if (pos == 29)
        {
                return getComplexBarcodeSuffix();
            }
              if (pos == 30)
        {
                return getComplexBarcodeTotalLength();
            }
              if (pos == 31)
        {
                return getBackupOutputDir();
            }
              if (pos == 32)
        {
                return getBackupFilePref();
            }
              if (pos == 33)
        {
                return getPicturePath();
            }
              if (pos == 34)
        {
                return getB2bPoPath();
            }
              if (pos == 35)
        {
                return getIreportPath();
            }
              if (pos == 36)
        {
                return getOnlineHelpUrl();
            }
              if (pos == 37)
        {
                return getSmsGatewayUrl();
            }
              if (pos == 38)
        {
                return getEmailServer();
            }
              if (pos == 39)
        {
                return getEmailUser();
            }
              if (pos == 40)
        {
                return getEmailPwd();
            }
              if (pos == 41)
        {
                return getSyncAllowPendingTrans();
            }
              if (pos == 42)
        {
                return getSyncFullInventory();
            }
              if (pos == 43)
        {
                return getSyncAllowStoreAdj();
            }
              if (pos == 44)
        {
                return getSyncAllowStoreReceipt();
            }
              if (pos == 45)
        {
                return getSyncAllowFreeTransfer();
            }
              if (pos == 46)
        {
                return getSyncHodataPath();
            }
              if (pos == 47)
        {
                return getSyncStoredataPath();
            }
              if (pos == 48)
        {
                return getSyncStoresetupPath();
            }
              if (pos == 49)
        {
                return getSyncStoretrfPath();
            }
              if (pos == 50)
        {
                return getSyncDestination();
            }
              if (pos == 51)
        {
                return getSyncDiskDestination();
            }
              if (pos == 52)
        {
                return getSyncHoUploadUrl();
            }
              if (pos == 53)
        {
                return getSyncHoEmail();
            }
              if (pos == 54)
        {
                return getSyncStoreEmailServer();
            }
              if (pos == 55)
        {
                return getSyncStoreEmailUser();
            }
              if (pos == 56)
        {
                return getSyncStoreEmailPwd();
            }
              if (pos == 57)
        {
                return getSyncDailyClosing();
            }
              if (pos == 58)
        {
                return getUseLocationCtltable();
            }
              if (pos == 59)
        {
                return getUseCustomerTrPrefix();
            }
              if (pos == 60)
        {
                return getUseVendorTrPrefix();
            }
              if (pos == 61)
        {
                return getUseVendorTax();
            }
              if (pos == 62)
        {
                return getUseCustomerTax();
            }
              if (pos == 63)
        {
                return getUseCustomerLocation();
            }
              if (pos == 64)
        {
                return getUseCfInOut();
            }
              if (pos == 65)
        {
                return getUseDistModules();
            }
              if (pos == 66)
        {
                return getUseWstModules();
            }
              if (pos == 67)
        {
                return getUseEdiModules();
            }
              if (pos == 68)
        {
                return getUseBom();
            }
              if (pos == 69)
        {
                return getUsePdt();
            }
              if (pos == 70)
        {
                return getConfirmTrfAtToloc();
            }
              if (pos == 71)
        {
                return getSiAllowImportSo();
            }
              if (pos == 72)
        {
                return getSiDefaultScreen();
            }
              if (pos == 73)
        {
                return getItemColorLtMin();
            }
              if (pos == 74)
        {
                return getItemColorLtRop();
            }
              if (pos == 75)
        {
                return getItemColorGtMax();
            }
              if (pos == 76)
        {
                return getItemCodeLength();
            }
              if (pos == 77)
        {
                return getCustCodeLength();
            }
              if (pos == 78)
        {
                return getVendCodeLength();
            }
              if (pos == 79)
        {
                return getOthrCodeLength();
            }
              if (pos == 80)
        {
                return getSalesInclusiveTax();
            }
              if (pos == 81)
        {
                return getPurchInclusiveTax();
            }
              if (pos == 82)
        {
                return getSalesMultiCurrency();
            }
              if (pos == 83)
        {
                return getPurchMultiCurrency();
            }
              if (pos == 84)
        {
                return getSalesMergeSameItem();
            }
              if (pos == 85)
        {
                return getPurchMergeSameItem();
            }
              if (pos == 86)
        {
                return getSalesSortBy();
            }
              if (pos == 87)
        {
                return getPurchSortBy();
            }
              if (pos == 88)
        {
                return getSalesFirstlineInsert();
            }
              if (pos == 89)
        {
                return getPurchFirstlineInsert();
            }
              if (pos == 90)
        {
                return getInvenFirstlineInsert();
            }
              if (pos == 91)
        {
                return getSalesFobCourier();
            }
              if (pos == 92)
        {
                return getPurchFobCourier();
            }
              if (pos == 93)
        {
                return getSalesCommaScale();
            }
              if (pos == 94)
        {
                return getPurchCommaScale();
            }
              if (pos == 95)
        {
                return getInvenCommaScale();
            }
              if (pos == 96)
        {
                return getCustMaxSbMode();
            }
              if (pos == 97)
        {
                return getVendMaxSbMode();
            }
              if (pos == 98)
        {
                return getPurchPrQtyGtPo();
            }
              if (pos == 99)
        {
                return getPurchPrQtyEqualPo();
            }
              if (pos == 100)
        {
                return getPurchLimitByEmp();
            }
              if (pos == 101)
        {
                return getPiAllowImportPo();
            }
              if (pos == 102)
        {
                return getPiLastPurchMethod();
            }
              if (pos == 103)
        {
                return getPiUpdateLastPurchase();
            }
              if (pos == 104)
        {
                return getPiUpdateSalesPrice();
            }
              if (pos == 105)
        {
                return getPiUpdatePrCost();
            }
              if (pos == 106)
        {
                return getSalesUseSalesman();
            }
              if (pos == 107)
        {
                return getSalesUseMinPrice();
            }
              if (pos == 108)
        {
                return getSalesSalesmanPeritem();
            }
              if (pos == 109)
        {
                return getSalesAllowMultipmtDisc();
            }
              if (pos == 110)
        {
                return getSalesEmplevelDisc();
            }
              if (pos == 111)
        {
                return getSalesCustomPricingUse();
            }
              if (pos == 112)
        {
                return getSalesCustomPricingTpl();
            }
              if (pos == 113)
        {
                return getPointrewardUse();
            }
              if (pos == 114)
        {
                return getPointrewardByitem();
            }
              if (pos == 115)
        {
                return getPointrewardField();
            }
              if (pos == 116)
        {
                return getPointrewardPvalue();
            }
              if (pos == 117)
        {
                return getDefaultSearchCond();
            }
              if (pos == 118)
        {
                return getKeyNewTrans();
            }
              if (pos == 119)
        {
                return getKeyOpenLookup();
            }
              if (pos == 120)
        {
                return getKeyPaymentFocus();
            }
              if (pos == 121)
        {
                return getKeySaveTrans();
            }
              if (pos == 122)
        {
                return getKeyPrintTrans();
            }
              if (pos == 123)
        {
                return getKeyFindTrans();
            }
              if (pos == 124)
        {
                return getKeySelectPayment();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SysConfigPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SysConfigPeer.doInsert((SysConfig) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SysConfigPeer.doUpdate((SysConfig) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key sysConfigId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setSysConfigId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setSysConfigId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSysConfigId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public SysConfig copy() throws TorqueException
    {
        return copyInto(new SysConfig());
    }
  
    protected SysConfig copyInto(SysConfig copyObj) throws TorqueException
    {
          copyObj.setSysConfigId(sysConfigId);
          copyObj.setPosDefaultScreen(posDefaultScreen);
          copyObj.setPosDefaultScreenFocus(posDefaultScreenFocus);
          copyObj.setPosDefaultOpenCashierAmt(posDefaultOpenCashierAmt);
          copyObj.setPosDefaultItemDisplay(posDefaultItemDisplay);
          copyObj.setPosDefaultItemSort(posDefaultItemSort);
          copyObj.setPosDefaultPrint(posDefaultPrint);
          copyObj.setPosConfirmQty(posConfirmQty);
          copyObj.setPosConfirmCost(posConfirmCost);
          copyObj.setPosChangeValidation(posChangeValidation);
          copyObj.setPosDeleteValidation(posDeleteValidation);
          copyObj.setPosOpenCashierValidation(posOpenCashierValidation);
          copyObj.setPosSaveDirectPrint(posSaveDirectPrint);
          copyObj.setPosPrinterDetailLine(posPrinterDetailLine);
          copyObj.setPosDirectAdd(posDirectAdd);
          copyObj.setPosRoundingTo(posRoundingTo);
          copyObj.setPosRoundingMode(posRoundingMode);
          copyObj.setPosSpcDiscCode(posSpcDiscCode);
          copyObj.setComplexBarcodeUse(complexBarcodeUse);
          copyObj.setComplexBarcodePrefix(complexBarcodePrefix);
          copyObj.setComplexBarcodePluStart(complexBarcodePluStart);
          copyObj.setComplexBarcodePluLength(complexBarcodePluLength);
          copyObj.setComplexBarcodeQtyStart(complexBarcodeQtyStart);
          copyObj.setComplexBarcodeQtyLength(complexBarcodeQtyLength);
          copyObj.setComplexBarcodeQtyDecPos(complexBarcodeQtyDecPos);
          copyObj.setComplexBarcodeOtherStart(complexBarcodeOtherStart);
          copyObj.setComplexBarcodeOtherLength(complexBarcodeOtherLength);
          copyObj.setComplexBarcodeOtherDecPos(complexBarcodeOtherDecPos);
          copyObj.setComplexBarcodeOtherType(complexBarcodeOtherType);
          copyObj.setComplexBarcodeSuffix(complexBarcodeSuffix);
          copyObj.setComplexBarcodeTotalLength(complexBarcodeTotalLength);
          copyObj.setBackupOutputDir(backupOutputDir);
          copyObj.setBackupFilePref(backupFilePref);
          copyObj.setPicturePath(picturePath);
          copyObj.setB2bPoPath(b2bPoPath);
          copyObj.setIreportPath(ireportPath);
          copyObj.setOnlineHelpUrl(onlineHelpUrl);
          copyObj.setSmsGatewayUrl(smsGatewayUrl);
          copyObj.setEmailServer(emailServer);
          copyObj.setEmailUser(emailUser);
          copyObj.setEmailPwd(emailPwd);
          copyObj.setSyncAllowPendingTrans(syncAllowPendingTrans);
          copyObj.setSyncFullInventory(syncFullInventory);
          copyObj.setSyncAllowStoreAdj(syncAllowStoreAdj);
          copyObj.setSyncAllowStoreReceipt(syncAllowStoreReceipt);
          copyObj.setSyncAllowFreeTransfer(syncAllowFreeTransfer);
          copyObj.setSyncHodataPath(syncHodataPath);
          copyObj.setSyncStoredataPath(syncStoredataPath);
          copyObj.setSyncStoresetupPath(syncStoresetupPath);
          copyObj.setSyncStoretrfPath(syncStoretrfPath);
          copyObj.setSyncDestination(syncDestination);
          copyObj.setSyncDiskDestination(syncDiskDestination);
          copyObj.setSyncHoUploadUrl(syncHoUploadUrl);
          copyObj.setSyncHoEmail(syncHoEmail);
          copyObj.setSyncStoreEmailServer(syncStoreEmailServer);
          copyObj.setSyncStoreEmailUser(syncStoreEmailUser);
          copyObj.setSyncStoreEmailPwd(syncStoreEmailPwd);
          copyObj.setSyncDailyClosing(syncDailyClosing);
          copyObj.setUseLocationCtltable(useLocationCtltable);
          copyObj.setUseCustomerTrPrefix(useCustomerTrPrefix);
          copyObj.setUseVendorTrPrefix(useVendorTrPrefix);
          copyObj.setUseVendorTax(useVendorTax);
          copyObj.setUseCustomerTax(useCustomerTax);
          copyObj.setUseCustomerLocation(useCustomerLocation);
          copyObj.setUseCfInOut(useCfInOut);
          copyObj.setUseDistModules(useDistModules);
          copyObj.setUseWstModules(useWstModules);
          copyObj.setUseEdiModules(useEdiModules);
          copyObj.setUseBom(useBom);
          copyObj.setUsePdt(usePdt);
          copyObj.setConfirmTrfAtToloc(confirmTrfAtToloc);
          copyObj.setSiAllowImportSo(siAllowImportSo);
          copyObj.setSiDefaultScreen(siDefaultScreen);
          copyObj.setItemColorLtMin(itemColorLtMin);
          copyObj.setItemColorLtRop(itemColorLtRop);
          copyObj.setItemColorGtMax(itemColorGtMax);
          copyObj.setItemCodeLength(itemCodeLength);
          copyObj.setCustCodeLength(custCodeLength);
          copyObj.setVendCodeLength(vendCodeLength);
          copyObj.setOthrCodeLength(othrCodeLength);
          copyObj.setSalesInclusiveTax(salesInclusiveTax);
          copyObj.setPurchInclusiveTax(purchInclusiveTax);
          copyObj.setSalesMultiCurrency(salesMultiCurrency);
          copyObj.setPurchMultiCurrency(purchMultiCurrency);
          copyObj.setSalesMergeSameItem(salesMergeSameItem);
          copyObj.setPurchMergeSameItem(purchMergeSameItem);
          copyObj.setSalesSortBy(salesSortBy);
          copyObj.setPurchSortBy(purchSortBy);
          copyObj.setSalesFirstlineInsert(salesFirstlineInsert);
          copyObj.setPurchFirstlineInsert(purchFirstlineInsert);
          copyObj.setInvenFirstlineInsert(invenFirstlineInsert);
          copyObj.setSalesFobCourier(salesFobCourier);
          copyObj.setPurchFobCourier(purchFobCourier);
          copyObj.setSalesCommaScale(salesCommaScale);
          copyObj.setPurchCommaScale(purchCommaScale);
          copyObj.setInvenCommaScale(invenCommaScale);
          copyObj.setCustMaxSbMode(custMaxSbMode);
          copyObj.setVendMaxSbMode(vendMaxSbMode);
          copyObj.setPurchPrQtyGtPo(purchPrQtyGtPo);
          copyObj.setPurchPrQtyEqualPo(purchPrQtyEqualPo);
          copyObj.setPurchLimitByEmp(purchLimitByEmp);
          copyObj.setPiAllowImportPo(piAllowImportPo);
          copyObj.setPiLastPurchMethod(piLastPurchMethod);
          copyObj.setPiUpdateLastPurchase(piUpdateLastPurchase);
          copyObj.setPiUpdateSalesPrice(piUpdateSalesPrice);
          copyObj.setPiUpdatePrCost(piUpdatePrCost);
          copyObj.setSalesUseSalesman(salesUseSalesman);
          copyObj.setSalesUseMinPrice(salesUseMinPrice);
          copyObj.setSalesSalesmanPeritem(salesSalesmanPeritem);
          copyObj.setSalesAllowMultipmtDisc(salesAllowMultipmtDisc);
          copyObj.setSalesEmplevelDisc(salesEmplevelDisc);
          copyObj.setSalesCustomPricingUse(salesCustomPricingUse);
          copyObj.setSalesCustomPricingTpl(salesCustomPricingTpl);
          copyObj.setPointrewardUse(pointrewardUse);
          copyObj.setPointrewardByitem(pointrewardByitem);
          copyObj.setPointrewardField(pointrewardField);
          copyObj.setPointrewardPvalue(pointrewardPvalue);
          copyObj.setDefaultSearchCond(defaultSearchCond);
          copyObj.setKeyNewTrans(keyNewTrans);
          copyObj.setKeyOpenLookup(keyOpenLookup);
          copyObj.setKeyPaymentFocus(keyPaymentFocus);
          copyObj.setKeySaveTrans(keySaveTrans);
          copyObj.setKeyPrintTrans(keyPrintTrans);
          copyObj.setKeyFindTrans(keyFindTrans);
          copyObj.setKeySelectPayment(keySelectPayment);
  
                    copyObj.setSysConfigId((String)null);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SysConfigPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("SysConfig\n");
        str.append("---------\n")
           .append("SysConfigId          : ")
           .append(getSysConfigId())
           .append("\n")
           .append("PosDefaultScreen     : ")
           .append(getPosDefaultScreen())
           .append("\n")
            .append("PosDefaultScreenFocus   : ")
           .append(getPosDefaultScreenFocus())
           .append("\n")
            .append("PosDefaultOpenCashierAmt   : ")
           .append(getPosDefaultOpenCashierAmt())
           .append("\n")
            .append("PosDefaultItemDisplay   : ")
           .append(getPosDefaultItemDisplay())
           .append("\n")
           .append("PosDefaultItemSort   : ")
           .append(getPosDefaultItemSort())
           .append("\n")
           .append("PosDefaultPrint      : ")
           .append(getPosDefaultPrint())
           .append("\n")
           .append("PosConfirmQty        : ")
           .append(getPosConfirmQty())
           .append("\n")
           .append("PosConfirmCost       : ")
           .append(getPosConfirmCost())
           .append("\n")
           .append("PosChangeValidation  : ")
           .append(getPosChangeValidation())
           .append("\n")
           .append("PosDeleteValidation  : ")
           .append(getPosDeleteValidation())
           .append("\n")
            .append("PosOpenCashierValidation   : ")
           .append(getPosOpenCashierValidation())
           .append("\n")
           .append("PosSaveDirectPrint   : ")
           .append(getPosSaveDirectPrint())
           .append("\n")
           .append("PosPrinterDetailLine   : ")
           .append(getPosPrinterDetailLine())
           .append("\n")
           .append("PosDirectAdd         : ")
           .append(getPosDirectAdd())
           .append("\n")
           .append("PosRoundingTo        : ")
           .append(getPosRoundingTo())
           .append("\n")
           .append("PosRoundingMode      : ")
           .append(getPosRoundingMode())
           .append("\n")
           .append("PosSpcDiscCode       : ")
           .append(getPosSpcDiscCode())
           .append("\n")
           .append("ComplexBarcodeUse    : ")
           .append(getComplexBarcodeUse())
           .append("\n")
           .append("ComplexBarcodePrefix   : ")
           .append(getComplexBarcodePrefix())
           .append("\n")
            .append("ComplexBarcodePluStart   : ")
           .append(getComplexBarcodePluStart())
           .append("\n")
            .append("ComplexBarcodePluLength   : ")
           .append(getComplexBarcodePluLength())
           .append("\n")
            .append("ComplexBarcodeQtyStart   : ")
           .append(getComplexBarcodeQtyStart())
           .append("\n")
            .append("ComplexBarcodeQtyLength   : ")
           .append(getComplexBarcodeQtyLength())
           .append("\n")
            .append("ComplexBarcodeQtyDecPos   : ")
           .append(getComplexBarcodeQtyDecPos())
           .append("\n")
            .append("ComplexBarcodeOtherStart   : ")
           .append(getComplexBarcodeOtherStart())
           .append("\n")
            .append("ComplexBarcodeOtherLength   : ")
           .append(getComplexBarcodeOtherLength())
           .append("\n")
            .append("ComplexBarcodeOtherDecPos   : ")
           .append(getComplexBarcodeOtherDecPos())
           .append("\n")
            .append("ComplexBarcodeOtherType   : ")
           .append(getComplexBarcodeOtherType())
           .append("\n")
           .append("ComplexBarcodeSuffix   : ")
           .append(getComplexBarcodeSuffix())
           .append("\n")
            .append("ComplexBarcodeTotalLength   : ")
           .append(getComplexBarcodeTotalLength())
           .append("\n")
           .append("BackupOutputDir      : ")
           .append(getBackupOutputDir())
           .append("\n")
           .append("BackupFilePref       : ")
           .append(getBackupFilePref())
           .append("\n")
           .append("PicturePath          : ")
           .append(getPicturePath())
           .append("\n")
           .append("B2bPoPath            : ")
           .append(getB2bPoPath())
           .append("\n")
           .append("IreportPath          : ")
           .append(getIreportPath())
           .append("\n")
           .append("OnlineHelpUrl        : ")
           .append(getOnlineHelpUrl())
           .append("\n")
           .append("SmsGatewayUrl        : ")
           .append(getSmsGatewayUrl())
           .append("\n")
           .append("EmailServer          : ")
           .append(getEmailServer())
           .append("\n")
           .append("EmailUser            : ")
           .append(getEmailUser())
           .append("\n")
           .append("EmailPwd             : ")
           .append(getEmailPwd())
           .append("\n")
            .append("SyncAllowPendingTrans   : ")
           .append(getSyncAllowPendingTrans())
           .append("\n")
           .append("SyncFullInventory    : ")
           .append(getSyncFullInventory())
           .append("\n")
           .append("SyncAllowStoreAdj    : ")
           .append(getSyncAllowStoreAdj())
           .append("\n")
            .append("SyncAllowStoreReceipt   : ")
           .append(getSyncAllowStoreReceipt())
           .append("\n")
            .append("SyncAllowFreeTransfer   : ")
           .append(getSyncAllowFreeTransfer())
           .append("\n")
           .append("SyncHodataPath       : ")
           .append(getSyncHodataPath())
           .append("\n")
           .append("SyncStoredataPath    : ")
           .append(getSyncStoredataPath())
           .append("\n")
           .append("SyncStoresetupPath   : ")
           .append(getSyncStoresetupPath())
           .append("\n")
           .append("SyncStoretrfPath     : ")
           .append(getSyncStoretrfPath())
           .append("\n")
           .append("SyncDestination      : ")
           .append(getSyncDestination())
           .append("\n")
           .append("SyncDiskDestination  : ")
           .append(getSyncDiskDestination())
           .append("\n")
           .append("SyncHoUploadUrl      : ")
           .append(getSyncHoUploadUrl())
           .append("\n")
           .append("SyncHoEmail          : ")
           .append(getSyncHoEmail())
           .append("\n")
           .append("SyncStoreEmailServer   : ")
           .append(getSyncStoreEmailServer())
           .append("\n")
           .append("SyncStoreEmailUser   : ")
           .append(getSyncStoreEmailUser())
           .append("\n")
           .append("SyncStoreEmailPwd    : ")
           .append(getSyncStoreEmailPwd())
           .append("\n")
           .append("SyncDailyClosing     : ")
           .append(getSyncDailyClosing())
           .append("\n")
           .append("UseLocationCtltable  : ")
           .append(getUseLocationCtltable())
           .append("\n")
           .append("UseCustomerTrPrefix  : ")
           .append(getUseCustomerTrPrefix())
           .append("\n")
           .append("UseVendorTrPrefix    : ")
           .append(getUseVendorTrPrefix())
           .append("\n")
           .append("UseVendorTax         : ")
           .append(getUseVendorTax())
           .append("\n")
           .append("UseCustomerTax       : ")
           .append(getUseCustomerTax())
           .append("\n")
           .append("UseCustomerLocation  : ")
           .append(getUseCustomerLocation())
           .append("\n")
           .append("UseCfInOut           : ")
           .append(getUseCfInOut())
           .append("\n")
           .append("UseDistModules       : ")
           .append(getUseDistModules())
           .append("\n")
           .append("UseWstModules        : ")
           .append(getUseWstModules())
           .append("\n")
           .append("UseEdiModules        : ")
           .append(getUseEdiModules())
           .append("\n")
           .append("UseBom               : ")
           .append(getUseBom())
           .append("\n")
           .append("UsePdt               : ")
           .append(getUsePdt())
           .append("\n")
           .append("ConfirmTrfAtToloc    : ")
           .append(getConfirmTrfAtToloc())
           .append("\n")
           .append("SiAllowImportSo      : ")
           .append(getSiAllowImportSo())
           .append("\n")
           .append("SiDefaultScreen      : ")
           .append(getSiDefaultScreen())
           .append("\n")
           .append("ItemColorLtMin       : ")
           .append(getItemColorLtMin())
           .append("\n")
           .append("ItemColorLtRop       : ")
           .append(getItemColorLtRop())
           .append("\n")
           .append("ItemColorGtMax       : ")
           .append(getItemColorGtMax())
           .append("\n")
           .append("ItemCodeLength       : ")
           .append(getItemCodeLength())
           .append("\n")
           .append("CustCodeLength       : ")
           .append(getCustCodeLength())
           .append("\n")
           .append("VendCodeLength       : ")
           .append(getVendCodeLength())
           .append("\n")
           .append("OthrCodeLength       : ")
           .append(getOthrCodeLength())
           .append("\n")
           .append("SalesInclusiveTax    : ")
           .append(getSalesInclusiveTax())
           .append("\n")
           .append("PurchInclusiveTax    : ")
           .append(getPurchInclusiveTax())
           .append("\n")
           .append("SalesMultiCurrency   : ")
           .append(getSalesMultiCurrency())
           .append("\n")
           .append("PurchMultiCurrency   : ")
           .append(getPurchMultiCurrency())
           .append("\n")
           .append("SalesMergeSameItem   : ")
           .append(getSalesMergeSameItem())
           .append("\n")
           .append("PurchMergeSameItem   : ")
           .append(getPurchMergeSameItem())
           .append("\n")
           .append("SalesSortBy          : ")
           .append(getSalesSortBy())
           .append("\n")
           .append("PurchSortBy          : ")
           .append(getPurchSortBy())
           .append("\n")
           .append("SalesFirstlineInsert   : ")
           .append(getSalesFirstlineInsert())
           .append("\n")
           .append("PurchFirstlineInsert   : ")
           .append(getPurchFirstlineInsert())
           .append("\n")
           .append("InvenFirstlineInsert   : ")
           .append(getInvenFirstlineInsert())
           .append("\n")
           .append("SalesFobCourier      : ")
           .append(getSalesFobCourier())
           .append("\n")
           .append("PurchFobCourier      : ")
           .append(getPurchFobCourier())
           .append("\n")
           .append("SalesCommaScale      : ")
           .append(getSalesCommaScale())
           .append("\n")
           .append("PurchCommaScale      : ")
           .append(getPurchCommaScale())
           .append("\n")
           .append("InvenCommaScale      : ")
           .append(getInvenCommaScale())
           .append("\n")
           .append("CustMaxSbMode        : ")
           .append(getCustMaxSbMode())
           .append("\n")
           .append("VendMaxSbMode        : ")
           .append(getVendMaxSbMode())
           .append("\n")
           .append("PurchPrQtyGtPo       : ")
           .append(getPurchPrQtyGtPo())
           .append("\n")
           .append("PurchPrQtyEqualPo    : ")
           .append(getPurchPrQtyEqualPo())
           .append("\n")
           .append("PurchLimitByEmp      : ")
           .append(getPurchLimitByEmp())
           .append("\n")
           .append("PiAllowImportPo      : ")
           .append(getPiAllowImportPo())
           .append("\n")
           .append("PiLastPurchMethod    : ")
           .append(getPiLastPurchMethod())
           .append("\n")
           .append("PiUpdateLastPurchase   : ")
           .append(getPiUpdateLastPurchase())
           .append("\n")
           .append("PiUpdateSalesPrice   : ")
           .append(getPiUpdateSalesPrice())
           .append("\n")
           .append("PiUpdatePrCost       : ")
           .append(getPiUpdatePrCost())
           .append("\n")
           .append("SalesUseSalesman     : ")
           .append(getSalesUseSalesman())
           .append("\n")
           .append("SalesUseMinPrice     : ")
           .append(getSalesUseMinPrice())
           .append("\n")
           .append("SalesSalesmanPeritem   : ")
           .append(getSalesSalesmanPeritem())
           .append("\n")
            .append("SalesAllowMultipmtDisc   : ")
           .append(getSalesAllowMultipmtDisc())
           .append("\n")
           .append("SalesEmplevelDisc    : ")
           .append(getSalesEmplevelDisc())
           .append("\n")
            .append("SalesCustomPricingUse   : ")
           .append(getSalesCustomPricingUse())
           .append("\n")
            .append("SalesCustomPricingTpl   : ")
           .append(getSalesCustomPricingTpl())
           .append("\n")
           .append("PointrewardUse       : ")
           .append(getPointrewardUse())
           .append("\n")
           .append("PointrewardByitem    : ")
           .append(getPointrewardByitem())
           .append("\n")
           .append("PointrewardField     : ")
           .append(getPointrewardField())
           .append("\n")
           .append("PointrewardPvalue    : ")
           .append(getPointrewardPvalue())
           .append("\n")
           .append("DefaultSearchCond    : ")
           .append(getDefaultSearchCond())
           .append("\n")
           .append("KeyNewTrans          : ")
           .append(getKeyNewTrans())
           .append("\n")
           .append("KeyOpenLookup        : ")
           .append(getKeyOpenLookup())
           .append("\n")
           .append("KeyPaymentFocus      : ")
           .append(getKeyPaymentFocus())
           .append("\n")
           .append("KeySaveTrans         : ")
           .append(getKeySaveTrans())
           .append("\n")
           .append("KeyPrintTrans        : ")
           .append(getKeyPrintTrans())
           .append("\n")
           .append("KeyFindTrans         : ")
           .append(getKeyFindTrans())
           .append("\n")
           .append("KeySelectPayment     : ")
           .append(getKeySelectPayment())
           .append("\n")
        ;
        return(str.toString());
    }
}
