package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.SysConfigMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseSysConfigPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "sys_config";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(SysConfigMapBuilder.CLASS_NAME);
    }

      /** the column name for the SYS_CONFIG_ID field */
    public static final String SYS_CONFIG_ID;
      /** the column name for the POS_DEFAULT_SCREEN field */
    public static final String POS_DEFAULT_SCREEN;
      /** the column name for the POS_DEFAULT_SCREEN_FOCUS field */
    public static final String POS_DEFAULT_SCREEN_FOCUS;
      /** the column name for the POS_DEFAULT_OPEN_CASHIER_AMT field */
    public static final String POS_DEFAULT_OPEN_CASHIER_AMT;
      /** the column name for the POS_DEFAULT_ITEM_DISPLAY field */
    public static final String POS_DEFAULT_ITEM_DISPLAY;
      /** the column name for the POS_DEFAULT_ITEM_SORT field */
    public static final String POS_DEFAULT_ITEM_SORT;
      /** the column name for the POS_DEFAULT_PRINT field */
    public static final String POS_DEFAULT_PRINT;
      /** the column name for the POS_CONFIRM_QTY field */
    public static final String POS_CONFIRM_QTY;
      /** the column name for the POS_CONFIRM_COST field */
    public static final String POS_CONFIRM_COST;
      /** the column name for the POS_CHANGE_VALIDATION field */
    public static final String POS_CHANGE_VALIDATION;
      /** the column name for the POS_DELETE_VALIDATION field */
    public static final String POS_DELETE_VALIDATION;
      /** the column name for the POS_OPEN_CASHIER_VALIDATION field */
    public static final String POS_OPEN_CASHIER_VALIDATION;
      /** the column name for the POS_SAVE_DIRECT_PRINT field */
    public static final String POS_SAVE_DIRECT_PRINT;
      /** the column name for the POS_PRINTER_DETAIL_LINE field */
    public static final String POS_PRINTER_DETAIL_LINE;
      /** the column name for the POS_DIRECT_ADD field */
    public static final String POS_DIRECT_ADD;
      /** the column name for the POS_ROUNDING_TO field */
    public static final String POS_ROUNDING_TO;
      /** the column name for the POS_ROUNDING_MODE field */
    public static final String POS_ROUNDING_MODE;
      /** the column name for the POS_SPC_DISC_CODE field */
    public static final String POS_SPC_DISC_CODE;
      /** the column name for the COMPLEX_BARCODE_USE field */
    public static final String COMPLEX_BARCODE_USE;
      /** the column name for the COMPLEX_BARCODE_PREFIX field */
    public static final String COMPLEX_BARCODE_PREFIX;
      /** the column name for the COMPLEX_BARCODE_PLU_START field */
    public static final String COMPLEX_BARCODE_PLU_START;
      /** the column name for the COMPLEX_BARCODE_PLU_LENGTH field */
    public static final String COMPLEX_BARCODE_PLU_LENGTH;
      /** the column name for the COMPLEX_BARCODE_QTY_START field */
    public static final String COMPLEX_BARCODE_QTY_START;
      /** the column name for the COMPLEX_BARCODE_QTY_LENGTH field */
    public static final String COMPLEX_BARCODE_QTY_LENGTH;
      /** the column name for the COMPLEX_BARCODE_QTY_DEC_POS field */
    public static final String COMPLEX_BARCODE_QTY_DEC_POS;
      /** the column name for the COMPLEX_BARCODE_OTHER_START field */
    public static final String COMPLEX_BARCODE_OTHER_START;
      /** the column name for the COMPLEX_BARCODE_OTHER_LENGTH field */
    public static final String COMPLEX_BARCODE_OTHER_LENGTH;
      /** the column name for the COMPLEX_BARCODE_OTHER_DEC_POS field */
    public static final String COMPLEX_BARCODE_OTHER_DEC_POS;
      /** the column name for the COMPLEX_BARCODE_OTHER_TYPE field */
    public static final String COMPLEX_BARCODE_OTHER_TYPE;
      /** the column name for the COMPLEX_BARCODE_SUFFIX field */
    public static final String COMPLEX_BARCODE_SUFFIX;
      /** the column name for the COMPLEX_BARCODE_TOTAL_LENGTH field */
    public static final String COMPLEX_BARCODE_TOTAL_LENGTH;
      /** the column name for the BACKUP_OUTPUT_DIR field */
    public static final String BACKUP_OUTPUT_DIR;
      /** the column name for the BACKUP_FILE_PREF field */
    public static final String BACKUP_FILE_PREF;
      /** the column name for the PICTURE_PATH field */
    public static final String PICTURE_PATH;
      /** the column name for the B2B_PO_PATH field */
    public static final String B2B_PO_PATH;
      /** the column name for the IREPORT_PATH field */
    public static final String IREPORT_PATH;
      /** the column name for the ONLINE_HELP_URL field */
    public static final String ONLINE_HELP_URL;
      /** the column name for the SMS_GATEWAY_URL field */
    public static final String SMS_GATEWAY_URL;
      /** the column name for the EMAIL_SERVER field */
    public static final String EMAIL_SERVER;
      /** the column name for the EMAIL_USER field */
    public static final String EMAIL_USER;
      /** the column name for the EMAIL_PWD field */
    public static final String EMAIL_PWD;
      /** the column name for the SYNC_ALLOW_PENDING_TRANS field */
    public static final String SYNC_ALLOW_PENDING_TRANS;
      /** the column name for the SYNC_FULL_INVENTORY field */
    public static final String SYNC_FULL_INVENTORY;
      /** the column name for the SYNC_ALLOW_STORE_ADJ field */
    public static final String SYNC_ALLOW_STORE_ADJ;
      /** the column name for the SYNC_ALLOW_STORE_RECEIPT field */
    public static final String SYNC_ALLOW_STORE_RECEIPT;
      /** the column name for the SYNC_ALLOW_FREE_TRANSFER field */
    public static final String SYNC_ALLOW_FREE_TRANSFER;
      /** the column name for the SYNC_HODATA_PATH field */
    public static final String SYNC_HODATA_PATH;
      /** the column name for the SYNC_STOREDATA_PATH field */
    public static final String SYNC_STOREDATA_PATH;
      /** the column name for the SYNC_STORESETUP_PATH field */
    public static final String SYNC_STORESETUP_PATH;
      /** the column name for the SYNC_STORETRF_PATH field */
    public static final String SYNC_STORETRF_PATH;
      /** the column name for the SYNC_DESTINATION field */
    public static final String SYNC_DESTINATION;
      /** the column name for the SYNC_DISK_DESTINATION field */
    public static final String SYNC_DISK_DESTINATION;
      /** the column name for the SYNC_HO_UPLOAD_URL field */
    public static final String SYNC_HO_UPLOAD_URL;
      /** the column name for the SYNC_HO_EMAIL field */
    public static final String SYNC_HO_EMAIL;
      /** the column name for the SYNC_STORE_EMAIL_SERVER field */
    public static final String SYNC_STORE_EMAIL_SERVER;
      /** the column name for the SYNC_STORE_EMAIL_USER field */
    public static final String SYNC_STORE_EMAIL_USER;
      /** the column name for the SYNC_STORE_EMAIL_PWD field */
    public static final String SYNC_STORE_EMAIL_PWD;
      /** the column name for the SYNC_DAILY_CLOSING field */
    public static final String SYNC_DAILY_CLOSING;
      /** the column name for the USE_LOCATION_CTLTABLE field */
    public static final String USE_LOCATION_CTLTABLE;
      /** the column name for the USE_CUSTOMER_TR_PREFIX field */
    public static final String USE_CUSTOMER_TR_PREFIX;
      /** the column name for the USE_VENDOR_TR_PREFIX field */
    public static final String USE_VENDOR_TR_PREFIX;
      /** the column name for the USE_VENDOR_TAX field */
    public static final String USE_VENDOR_TAX;
      /** the column name for the USE_CUSTOMER_TAX field */
    public static final String USE_CUSTOMER_TAX;
      /** the column name for the USE_CUSTOMER_LOCATION field */
    public static final String USE_CUSTOMER_LOCATION;
      /** the column name for the USE_CF_IN_OUT field */
    public static final String USE_CF_IN_OUT;
      /** the column name for the USE_DIST_MODULES field */
    public static final String USE_DIST_MODULES;
      /** the column name for the USE_WST_MODULES field */
    public static final String USE_WST_MODULES;
      /** the column name for the USE_EDI_MODULES field */
    public static final String USE_EDI_MODULES;
      /** the column name for the USE_BOM field */
    public static final String USE_BOM;
      /** the column name for the USE_PDT field */
    public static final String USE_PDT;
      /** the column name for the CONFIRM_TRF_AT_TOLOC field */
    public static final String CONFIRM_TRF_AT_TOLOC;
      /** the column name for the SI_ALLOW_IMPORT_SO field */
    public static final String SI_ALLOW_IMPORT_SO;
      /** the column name for the SI_DEFAULT_SCREEN field */
    public static final String SI_DEFAULT_SCREEN;
      /** the column name for the ITEM_COLOR_LT_MIN field */
    public static final String ITEM_COLOR_LT_MIN;
      /** the column name for the ITEM_COLOR_LT_ROP field */
    public static final String ITEM_COLOR_LT_ROP;
      /** the column name for the ITEM_COLOR_GT_MAX field */
    public static final String ITEM_COLOR_GT_MAX;
      /** the column name for the ITEM_CODE_LENGTH field */
    public static final String ITEM_CODE_LENGTH;
      /** the column name for the CUST_CODE_LENGTH field */
    public static final String CUST_CODE_LENGTH;
      /** the column name for the VEND_CODE_LENGTH field */
    public static final String VEND_CODE_LENGTH;
      /** the column name for the OTHR_CODE_LENGTH field */
    public static final String OTHR_CODE_LENGTH;
      /** the column name for the SALES_INCLUSIVE_TAX field */
    public static final String SALES_INCLUSIVE_TAX;
      /** the column name for the PURCH_INCLUSIVE_TAX field */
    public static final String PURCH_INCLUSIVE_TAX;
      /** the column name for the SALES_MULTI_CURRENCY field */
    public static final String SALES_MULTI_CURRENCY;
      /** the column name for the PURCH_MULTI_CURRENCY field */
    public static final String PURCH_MULTI_CURRENCY;
      /** the column name for the SALES_MERGE_SAME_ITEM field */
    public static final String SALES_MERGE_SAME_ITEM;
      /** the column name for the PURCH_MERGE_SAME_ITEM field */
    public static final String PURCH_MERGE_SAME_ITEM;
      /** the column name for the SALES_SORT_BY field */
    public static final String SALES_SORT_BY;
      /** the column name for the PURCH_SORT_BY field */
    public static final String PURCH_SORT_BY;
      /** the column name for the SALES_FIRSTLINE_INSERT field */
    public static final String SALES_FIRSTLINE_INSERT;
      /** the column name for the PURCH_FIRSTLINE_INSERT field */
    public static final String PURCH_FIRSTLINE_INSERT;
      /** the column name for the INVEN_FIRSTLINE_INSERT field */
    public static final String INVEN_FIRSTLINE_INSERT;
      /** the column name for the SALES_FOB_COURIER field */
    public static final String SALES_FOB_COURIER;
      /** the column name for the PURCH_FOB_COURIER field */
    public static final String PURCH_FOB_COURIER;
      /** the column name for the SALES_COMMA_SCALE field */
    public static final String SALES_COMMA_SCALE;
      /** the column name for the PURCH_COMMA_SCALE field */
    public static final String PURCH_COMMA_SCALE;
      /** the column name for the INVEN_COMMA_SCALE field */
    public static final String INVEN_COMMA_SCALE;
      /** the column name for the CUST_MAX_SB_MODE field */
    public static final String CUST_MAX_SB_MODE;
      /** the column name for the VEND_MAX_SB_MODE field */
    public static final String VEND_MAX_SB_MODE;
      /** the column name for the PURCH_PR_QTY_GT_PO field */
    public static final String PURCH_PR_QTY_GT_PO;
      /** the column name for the PURCH_PR_QTY_EQUAL_PO field */
    public static final String PURCH_PR_QTY_EQUAL_PO;
      /** the column name for the PURCH_LIMIT_BY_EMP field */
    public static final String PURCH_LIMIT_BY_EMP;
      /** the column name for the PI_ALLOW_IMPORT_PO field */
    public static final String PI_ALLOW_IMPORT_PO;
      /** the column name for the PI_LAST_PURCH_METHOD field */
    public static final String PI_LAST_PURCH_METHOD;
      /** the column name for the PI_UPDATE_LAST_PURCHASE field */
    public static final String PI_UPDATE_LAST_PURCHASE;
      /** the column name for the PI_UPDATE_SALES_PRICE field */
    public static final String PI_UPDATE_SALES_PRICE;
      /** the column name for the PI_UPDATE_PR_COST field */
    public static final String PI_UPDATE_PR_COST;
      /** the column name for the SALES_USE_SALESMAN field */
    public static final String SALES_USE_SALESMAN;
      /** the column name for the SALES_USE_MIN_PRICE field */
    public static final String SALES_USE_MIN_PRICE;
      /** the column name for the SALES_SALESMAN_PERITEM field */
    public static final String SALES_SALESMAN_PERITEM;
      /** the column name for the SALES_ALLOW_MULTIPMT_DISC field */
    public static final String SALES_ALLOW_MULTIPMT_DISC;
      /** the column name for the SALES_EMPLEVEL_DISC field */
    public static final String SALES_EMPLEVEL_DISC;
      /** the column name for the SALES_CUSTOM_PRICING_USE field */
    public static final String SALES_CUSTOM_PRICING_USE;
      /** the column name for the SALES_CUSTOM_PRICING_TPL field */
    public static final String SALES_CUSTOM_PRICING_TPL;
      /** the column name for the POINTREWARD_USE field */
    public static final String POINTREWARD_USE;
      /** the column name for the POINTREWARD_BYITEM field */
    public static final String POINTREWARD_BYITEM;
      /** the column name for the POINTREWARD_FIELD field */
    public static final String POINTREWARD_FIELD;
      /** the column name for the POINTREWARD_PVALUE field */
    public static final String POINTREWARD_PVALUE;
      /** the column name for the DEFAULT_SEARCH_COND field */
    public static final String DEFAULT_SEARCH_COND;
      /** the column name for the KEY_NEW_TRANS field */
    public static final String KEY_NEW_TRANS;
      /** the column name for the KEY_OPEN_LOOKUP field */
    public static final String KEY_OPEN_LOOKUP;
      /** the column name for the KEY_PAYMENT_FOCUS field */
    public static final String KEY_PAYMENT_FOCUS;
      /** the column name for the KEY_SAVE_TRANS field */
    public static final String KEY_SAVE_TRANS;
      /** the column name for the KEY_PRINT_TRANS field */
    public static final String KEY_PRINT_TRANS;
      /** the column name for the KEY_FIND_TRANS field */
    public static final String KEY_FIND_TRANS;
      /** the column name for the KEY_SELECT_PAYMENT field */
    public static final String KEY_SELECT_PAYMENT;
  
    static
    {
          SYS_CONFIG_ID = "sys_config.SYS_CONFIG_ID";
          POS_DEFAULT_SCREEN = "sys_config.POS_DEFAULT_SCREEN";
          POS_DEFAULT_SCREEN_FOCUS = "sys_config.POS_DEFAULT_SCREEN_FOCUS";
          POS_DEFAULT_OPEN_CASHIER_AMT = "sys_config.POS_DEFAULT_OPEN_CASHIER_AMT";
          POS_DEFAULT_ITEM_DISPLAY = "sys_config.POS_DEFAULT_ITEM_DISPLAY";
          POS_DEFAULT_ITEM_SORT = "sys_config.POS_DEFAULT_ITEM_SORT";
          POS_DEFAULT_PRINT = "sys_config.POS_DEFAULT_PRINT";
          POS_CONFIRM_QTY = "sys_config.POS_CONFIRM_QTY";
          POS_CONFIRM_COST = "sys_config.POS_CONFIRM_COST";
          POS_CHANGE_VALIDATION = "sys_config.POS_CHANGE_VALIDATION";
          POS_DELETE_VALIDATION = "sys_config.POS_DELETE_VALIDATION";
          POS_OPEN_CASHIER_VALIDATION = "sys_config.POS_OPEN_CASHIER_VALIDATION";
          POS_SAVE_DIRECT_PRINT = "sys_config.POS_SAVE_DIRECT_PRINT";
          POS_PRINTER_DETAIL_LINE = "sys_config.POS_PRINTER_DETAIL_LINE";
          POS_DIRECT_ADD = "sys_config.POS_DIRECT_ADD";
          POS_ROUNDING_TO = "sys_config.POS_ROUNDING_TO";
          POS_ROUNDING_MODE = "sys_config.POS_ROUNDING_MODE";
          POS_SPC_DISC_CODE = "sys_config.POS_SPC_DISC_CODE";
          COMPLEX_BARCODE_USE = "sys_config.COMPLEX_BARCODE_USE";
          COMPLEX_BARCODE_PREFIX = "sys_config.COMPLEX_BARCODE_PREFIX";
          COMPLEX_BARCODE_PLU_START = "sys_config.COMPLEX_BARCODE_PLU_START";
          COMPLEX_BARCODE_PLU_LENGTH = "sys_config.COMPLEX_BARCODE_PLU_LENGTH";
          COMPLEX_BARCODE_QTY_START = "sys_config.COMPLEX_BARCODE_QTY_START";
          COMPLEX_BARCODE_QTY_LENGTH = "sys_config.COMPLEX_BARCODE_QTY_LENGTH";
          COMPLEX_BARCODE_QTY_DEC_POS = "sys_config.COMPLEX_BARCODE_QTY_DEC_POS";
          COMPLEX_BARCODE_OTHER_START = "sys_config.COMPLEX_BARCODE_OTHER_START";
          COMPLEX_BARCODE_OTHER_LENGTH = "sys_config.COMPLEX_BARCODE_OTHER_LENGTH";
          COMPLEX_BARCODE_OTHER_DEC_POS = "sys_config.COMPLEX_BARCODE_OTHER_DEC_POS";
          COMPLEX_BARCODE_OTHER_TYPE = "sys_config.COMPLEX_BARCODE_OTHER_TYPE";
          COMPLEX_BARCODE_SUFFIX = "sys_config.COMPLEX_BARCODE_SUFFIX";
          COMPLEX_BARCODE_TOTAL_LENGTH = "sys_config.COMPLEX_BARCODE_TOTAL_LENGTH";
          BACKUP_OUTPUT_DIR = "sys_config.BACKUP_OUTPUT_DIR";
          BACKUP_FILE_PREF = "sys_config.BACKUP_FILE_PREF";
          PICTURE_PATH = "sys_config.PICTURE_PATH";
          B2B_PO_PATH = "sys_config.B2B_PO_PATH";
          IREPORT_PATH = "sys_config.IREPORT_PATH";
          ONLINE_HELP_URL = "sys_config.ONLINE_HELP_URL";
          SMS_GATEWAY_URL = "sys_config.SMS_GATEWAY_URL";
          EMAIL_SERVER = "sys_config.EMAIL_SERVER";
          EMAIL_USER = "sys_config.EMAIL_USER";
          EMAIL_PWD = "sys_config.EMAIL_PWD";
          SYNC_ALLOW_PENDING_TRANS = "sys_config.SYNC_ALLOW_PENDING_TRANS";
          SYNC_FULL_INVENTORY = "sys_config.SYNC_FULL_INVENTORY";
          SYNC_ALLOW_STORE_ADJ = "sys_config.SYNC_ALLOW_STORE_ADJ";
          SYNC_ALLOW_STORE_RECEIPT = "sys_config.SYNC_ALLOW_STORE_RECEIPT";
          SYNC_ALLOW_FREE_TRANSFER = "sys_config.SYNC_ALLOW_FREE_TRANSFER";
          SYNC_HODATA_PATH = "sys_config.SYNC_HODATA_PATH";
          SYNC_STOREDATA_PATH = "sys_config.SYNC_STOREDATA_PATH";
          SYNC_STORESETUP_PATH = "sys_config.SYNC_STORESETUP_PATH";
          SYNC_STORETRF_PATH = "sys_config.SYNC_STORETRF_PATH";
          SYNC_DESTINATION = "sys_config.SYNC_DESTINATION";
          SYNC_DISK_DESTINATION = "sys_config.SYNC_DISK_DESTINATION";
          SYNC_HO_UPLOAD_URL = "sys_config.SYNC_HO_UPLOAD_URL";
          SYNC_HO_EMAIL = "sys_config.SYNC_HO_EMAIL";
          SYNC_STORE_EMAIL_SERVER = "sys_config.SYNC_STORE_EMAIL_SERVER";
          SYNC_STORE_EMAIL_USER = "sys_config.SYNC_STORE_EMAIL_USER";
          SYNC_STORE_EMAIL_PWD = "sys_config.SYNC_STORE_EMAIL_PWD";
          SYNC_DAILY_CLOSING = "sys_config.SYNC_DAILY_CLOSING";
          USE_LOCATION_CTLTABLE = "sys_config.USE_LOCATION_CTLTABLE";
          USE_CUSTOMER_TR_PREFIX = "sys_config.USE_CUSTOMER_TR_PREFIX";
          USE_VENDOR_TR_PREFIX = "sys_config.USE_VENDOR_TR_PREFIX";
          USE_VENDOR_TAX = "sys_config.USE_VENDOR_TAX";
          USE_CUSTOMER_TAX = "sys_config.USE_CUSTOMER_TAX";
          USE_CUSTOMER_LOCATION = "sys_config.USE_CUSTOMER_LOCATION";
          USE_CF_IN_OUT = "sys_config.USE_CF_IN_OUT";
          USE_DIST_MODULES = "sys_config.USE_DIST_MODULES";
          USE_WST_MODULES = "sys_config.USE_WST_MODULES";
          USE_EDI_MODULES = "sys_config.USE_EDI_MODULES";
          USE_BOM = "sys_config.USE_BOM";
          USE_PDT = "sys_config.USE_PDT";
          CONFIRM_TRF_AT_TOLOC = "sys_config.CONFIRM_TRF_AT_TOLOC";
          SI_ALLOW_IMPORT_SO = "sys_config.SI_ALLOW_IMPORT_SO";
          SI_DEFAULT_SCREEN = "sys_config.SI_DEFAULT_SCREEN";
          ITEM_COLOR_LT_MIN = "sys_config.ITEM_COLOR_LT_MIN";
          ITEM_COLOR_LT_ROP = "sys_config.ITEM_COLOR_LT_ROP";
          ITEM_COLOR_GT_MAX = "sys_config.ITEM_COLOR_GT_MAX";
          ITEM_CODE_LENGTH = "sys_config.ITEM_CODE_LENGTH";
          CUST_CODE_LENGTH = "sys_config.CUST_CODE_LENGTH";
          VEND_CODE_LENGTH = "sys_config.VEND_CODE_LENGTH";
          OTHR_CODE_LENGTH = "sys_config.OTHR_CODE_LENGTH";
          SALES_INCLUSIVE_TAX = "sys_config.SALES_INCLUSIVE_TAX";
          PURCH_INCLUSIVE_TAX = "sys_config.PURCH_INCLUSIVE_TAX";
          SALES_MULTI_CURRENCY = "sys_config.SALES_MULTI_CURRENCY";
          PURCH_MULTI_CURRENCY = "sys_config.PURCH_MULTI_CURRENCY";
          SALES_MERGE_SAME_ITEM = "sys_config.SALES_MERGE_SAME_ITEM";
          PURCH_MERGE_SAME_ITEM = "sys_config.PURCH_MERGE_SAME_ITEM";
          SALES_SORT_BY = "sys_config.SALES_SORT_BY";
          PURCH_SORT_BY = "sys_config.PURCH_SORT_BY";
          SALES_FIRSTLINE_INSERT = "sys_config.SALES_FIRSTLINE_INSERT";
          PURCH_FIRSTLINE_INSERT = "sys_config.PURCH_FIRSTLINE_INSERT";
          INVEN_FIRSTLINE_INSERT = "sys_config.INVEN_FIRSTLINE_INSERT";
          SALES_FOB_COURIER = "sys_config.SALES_FOB_COURIER";
          PURCH_FOB_COURIER = "sys_config.PURCH_FOB_COURIER";
          SALES_COMMA_SCALE = "sys_config.SALES_COMMA_SCALE";
          PURCH_COMMA_SCALE = "sys_config.PURCH_COMMA_SCALE";
          INVEN_COMMA_SCALE = "sys_config.INVEN_COMMA_SCALE";
          CUST_MAX_SB_MODE = "sys_config.CUST_MAX_SB_MODE";
          VEND_MAX_SB_MODE = "sys_config.VEND_MAX_SB_MODE";
          PURCH_PR_QTY_GT_PO = "sys_config.PURCH_PR_QTY_GT_PO";
          PURCH_PR_QTY_EQUAL_PO = "sys_config.PURCH_PR_QTY_EQUAL_PO";
          PURCH_LIMIT_BY_EMP = "sys_config.PURCH_LIMIT_BY_EMP";
          PI_ALLOW_IMPORT_PO = "sys_config.PI_ALLOW_IMPORT_PO";
          PI_LAST_PURCH_METHOD = "sys_config.PI_LAST_PURCH_METHOD";
          PI_UPDATE_LAST_PURCHASE = "sys_config.PI_UPDATE_LAST_PURCHASE";
          PI_UPDATE_SALES_PRICE = "sys_config.PI_UPDATE_SALES_PRICE";
          PI_UPDATE_PR_COST = "sys_config.PI_UPDATE_PR_COST";
          SALES_USE_SALESMAN = "sys_config.SALES_USE_SALESMAN";
          SALES_USE_MIN_PRICE = "sys_config.SALES_USE_MIN_PRICE";
          SALES_SALESMAN_PERITEM = "sys_config.SALES_SALESMAN_PERITEM";
          SALES_ALLOW_MULTIPMT_DISC = "sys_config.SALES_ALLOW_MULTIPMT_DISC";
          SALES_EMPLEVEL_DISC = "sys_config.SALES_EMPLEVEL_DISC";
          SALES_CUSTOM_PRICING_USE = "sys_config.SALES_CUSTOM_PRICING_USE";
          SALES_CUSTOM_PRICING_TPL = "sys_config.SALES_CUSTOM_PRICING_TPL";
          POINTREWARD_USE = "sys_config.POINTREWARD_USE";
          POINTREWARD_BYITEM = "sys_config.POINTREWARD_BYITEM";
          POINTREWARD_FIELD = "sys_config.POINTREWARD_FIELD";
          POINTREWARD_PVALUE = "sys_config.POINTREWARD_PVALUE";
          DEFAULT_SEARCH_COND = "sys_config.DEFAULT_SEARCH_COND";
          KEY_NEW_TRANS = "sys_config.KEY_NEW_TRANS";
          KEY_OPEN_LOOKUP = "sys_config.KEY_OPEN_LOOKUP";
          KEY_PAYMENT_FOCUS = "sys_config.KEY_PAYMENT_FOCUS";
          KEY_SAVE_TRANS = "sys_config.KEY_SAVE_TRANS";
          KEY_PRINT_TRANS = "sys_config.KEY_PRINT_TRANS";
          KEY_FIND_TRANS = "sys_config.KEY_FIND_TRANS";
          KEY_SELECT_PAYMENT = "sys_config.KEY_SELECT_PAYMENT";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(SysConfigMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(SysConfigMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  125;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.SysConfig";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseSysConfigPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(SYS_CONFIG_ID);
          criteria.addSelectColumn(POS_DEFAULT_SCREEN);
          criteria.addSelectColumn(POS_DEFAULT_SCREEN_FOCUS);
          criteria.addSelectColumn(POS_DEFAULT_OPEN_CASHIER_AMT);
          criteria.addSelectColumn(POS_DEFAULT_ITEM_DISPLAY);
          criteria.addSelectColumn(POS_DEFAULT_ITEM_SORT);
          criteria.addSelectColumn(POS_DEFAULT_PRINT);
          criteria.addSelectColumn(POS_CONFIRM_QTY);
          criteria.addSelectColumn(POS_CONFIRM_COST);
          criteria.addSelectColumn(POS_CHANGE_VALIDATION);
          criteria.addSelectColumn(POS_DELETE_VALIDATION);
          criteria.addSelectColumn(POS_OPEN_CASHIER_VALIDATION);
          criteria.addSelectColumn(POS_SAVE_DIRECT_PRINT);
          criteria.addSelectColumn(POS_PRINTER_DETAIL_LINE);
          criteria.addSelectColumn(POS_DIRECT_ADD);
          criteria.addSelectColumn(POS_ROUNDING_TO);
          criteria.addSelectColumn(POS_ROUNDING_MODE);
          criteria.addSelectColumn(POS_SPC_DISC_CODE);
          criteria.addSelectColumn(COMPLEX_BARCODE_USE);
          criteria.addSelectColumn(COMPLEX_BARCODE_PREFIX);
          criteria.addSelectColumn(COMPLEX_BARCODE_PLU_START);
          criteria.addSelectColumn(COMPLEX_BARCODE_PLU_LENGTH);
          criteria.addSelectColumn(COMPLEX_BARCODE_QTY_START);
          criteria.addSelectColumn(COMPLEX_BARCODE_QTY_LENGTH);
          criteria.addSelectColumn(COMPLEX_BARCODE_QTY_DEC_POS);
          criteria.addSelectColumn(COMPLEX_BARCODE_OTHER_START);
          criteria.addSelectColumn(COMPLEX_BARCODE_OTHER_LENGTH);
          criteria.addSelectColumn(COMPLEX_BARCODE_OTHER_DEC_POS);
          criteria.addSelectColumn(COMPLEX_BARCODE_OTHER_TYPE);
          criteria.addSelectColumn(COMPLEX_BARCODE_SUFFIX);
          criteria.addSelectColumn(COMPLEX_BARCODE_TOTAL_LENGTH);
          criteria.addSelectColumn(BACKUP_OUTPUT_DIR);
          criteria.addSelectColumn(BACKUP_FILE_PREF);
          criteria.addSelectColumn(PICTURE_PATH);
          criteria.addSelectColumn(B2B_PO_PATH);
          criteria.addSelectColumn(IREPORT_PATH);
          criteria.addSelectColumn(ONLINE_HELP_URL);
          criteria.addSelectColumn(SMS_GATEWAY_URL);
          criteria.addSelectColumn(EMAIL_SERVER);
          criteria.addSelectColumn(EMAIL_USER);
          criteria.addSelectColumn(EMAIL_PWD);
          criteria.addSelectColumn(SYNC_ALLOW_PENDING_TRANS);
          criteria.addSelectColumn(SYNC_FULL_INVENTORY);
          criteria.addSelectColumn(SYNC_ALLOW_STORE_ADJ);
          criteria.addSelectColumn(SYNC_ALLOW_STORE_RECEIPT);
          criteria.addSelectColumn(SYNC_ALLOW_FREE_TRANSFER);
          criteria.addSelectColumn(SYNC_HODATA_PATH);
          criteria.addSelectColumn(SYNC_STOREDATA_PATH);
          criteria.addSelectColumn(SYNC_STORESETUP_PATH);
          criteria.addSelectColumn(SYNC_STORETRF_PATH);
          criteria.addSelectColumn(SYNC_DESTINATION);
          criteria.addSelectColumn(SYNC_DISK_DESTINATION);
          criteria.addSelectColumn(SYNC_HO_UPLOAD_URL);
          criteria.addSelectColumn(SYNC_HO_EMAIL);
          criteria.addSelectColumn(SYNC_STORE_EMAIL_SERVER);
          criteria.addSelectColumn(SYNC_STORE_EMAIL_USER);
          criteria.addSelectColumn(SYNC_STORE_EMAIL_PWD);
          criteria.addSelectColumn(SYNC_DAILY_CLOSING);
          criteria.addSelectColumn(USE_LOCATION_CTLTABLE);
          criteria.addSelectColumn(USE_CUSTOMER_TR_PREFIX);
          criteria.addSelectColumn(USE_VENDOR_TR_PREFIX);
          criteria.addSelectColumn(USE_VENDOR_TAX);
          criteria.addSelectColumn(USE_CUSTOMER_TAX);
          criteria.addSelectColumn(USE_CUSTOMER_LOCATION);
          criteria.addSelectColumn(USE_CF_IN_OUT);
          criteria.addSelectColumn(USE_DIST_MODULES);
          criteria.addSelectColumn(USE_WST_MODULES);
          criteria.addSelectColumn(USE_EDI_MODULES);
          criteria.addSelectColumn(USE_BOM);
          criteria.addSelectColumn(USE_PDT);
          criteria.addSelectColumn(CONFIRM_TRF_AT_TOLOC);
          criteria.addSelectColumn(SI_ALLOW_IMPORT_SO);
          criteria.addSelectColumn(SI_DEFAULT_SCREEN);
          criteria.addSelectColumn(ITEM_COLOR_LT_MIN);
          criteria.addSelectColumn(ITEM_COLOR_LT_ROP);
          criteria.addSelectColumn(ITEM_COLOR_GT_MAX);
          criteria.addSelectColumn(ITEM_CODE_LENGTH);
          criteria.addSelectColumn(CUST_CODE_LENGTH);
          criteria.addSelectColumn(VEND_CODE_LENGTH);
          criteria.addSelectColumn(OTHR_CODE_LENGTH);
          criteria.addSelectColumn(SALES_INCLUSIVE_TAX);
          criteria.addSelectColumn(PURCH_INCLUSIVE_TAX);
          criteria.addSelectColumn(SALES_MULTI_CURRENCY);
          criteria.addSelectColumn(PURCH_MULTI_CURRENCY);
          criteria.addSelectColumn(SALES_MERGE_SAME_ITEM);
          criteria.addSelectColumn(PURCH_MERGE_SAME_ITEM);
          criteria.addSelectColumn(SALES_SORT_BY);
          criteria.addSelectColumn(PURCH_SORT_BY);
          criteria.addSelectColumn(SALES_FIRSTLINE_INSERT);
          criteria.addSelectColumn(PURCH_FIRSTLINE_INSERT);
          criteria.addSelectColumn(INVEN_FIRSTLINE_INSERT);
          criteria.addSelectColumn(SALES_FOB_COURIER);
          criteria.addSelectColumn(PURCH_FOB_COURIER);
          criteria.addSelectColumn(SALES_COMMA_SCALE);
          criteria.addSelectColumn(PURCH_COMMA_SCALE);
          criteria.addSelectColumn(INVEN_COMMA_SCALE);
          criteria.addSelectColumn(CUST_MAX_SB_MODE);
          criteria.addSelectColumn(VEND_MAX_SB_MODE);
          criteria.addSelectColumn(PURCH_PR_QTY_GT_PO);
          criteria.addSelectColumn(PURCH_PR_QTY_EQUAL_PO);
          criteria.addSelectColumn(PURCH_LIMIT_BY_EMP);
          criteria.addSelectColumn(PI_ALLOW_IMPORT_PO);
          criteria.addSelectColumn(PI_LAST_PURCH_METHOD);
          criteria.addSelectColumn(PI_UPDATE_LAST_PURCHASE);
          criteria.addSelectColumn(PI_UPDATE_SALES_PRICE);
          criteria.addSelectColumn(PI_UPDATE_PR_COST);
          criteria.addSelectColumn(SALES_USE_SALESMAN);
          criteria.addSelectColumn(SALES_USE_MIN_PRICE);
          criteria.addSelectColumn(SALES_SALESMAN_PERITEM);
          criteria.addSelectColumn(SALES_ALLOW_MULTIPMT_DISC);
          criteria.addSelectColumn(SALES_EMPLEVEL_DISC);
          criteria.addSelectColumn(SALES_CUSTOM_PRICING_USE);
          criteria.addSelectColumn(SALES_CUSTOM_PRICING_TPL);
          criteria.addSelectColumn(POINTREWARD_USE);
          criteria.addSelectColumn(POINTREWARD_BYITEM);
          criteria.addSelectColumn(POINTREWARD_FIELD);
          criteria.addSelectColumn(POINTREWARD_PVALUE);
          criteria.addSelectColumn(DEFAULT_SEARCH_COND);
          criteria.addSelectColumn(KEY_NEW_TRANS);
          criteria.addSelectColumn(KEY_OPEN_LOOKUP);
          criteria.addSelectColumn(KEY_PAYMENT_FOCUS);
          criteria.addSelectColumn(KEY_SAVE_TRANS);
          criteria.addSelectColumn(KEY_PRINT_TRANS);
          criteria.addSelectColumn(KEY_FIND_TRANS);
          criteria.addSelectColumn(KEY_SELECT_PAYMENT);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static SysConfig row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            SysConfig obj = (SysConfig) cls.newInstance();
            SysConfigPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      SysConfig obj)
        throws TorqueException
    {
        try
        {
                obj.setSysConfigId(row.getValue(offset + 0).asString());
                  obj.setPosDefaultScreen(row.getValue(offset + 1).asString());
                  obj.setPosDefaultScreenFocus(row.getValue(offset + 2).asString());
                  obj.setPosDefaultOpenCashierAmt(row.getValue(offset + 3).asString());
                  obj.setPosDefaultItemDisplay(row.getValue(offset + 4).asString());
                  obj.setPosDefaultItemSort(row.getValue(offset + 5).asString());
                  obj.setPosDefaultPrint(row.getValue(offset + 6).asString());
                  obj.setPosConfirmQty(row.getValue(offset + 7).asString());
                  obj.setPosConfirmCost(row.getValue(offset + 8).asString());
                  obj.setPosChangeValidation(row.getValue(offset + 9).asString());
                  obj.setPosDeleteValidation(row.getValue(offset + 10).asString());
                  obj.setPosOpenCashierValidation(row.getValue(offset + 11).asString());
                  obj.setPosSaveDirectPrint(row.getValue(offset + 12).asString());
                  obj.setPosPrinterDetailLine(row.getValue(offset + 13).asString());
                  obj.setPosDirectAdd(row.getValue(offset + 14).asString());
                  obj.setPosRoundingTo(row.getValue(offset + 15).asString());
                  obj.setPosRoundingMode(row.getValue(offset + 16).asString());
                  obj.setPosSpcDiscCode(row.getValue(offset + 17).asString());
                  obj.setComplexBarcodeUse(row.getValue(offset + 18).asString());
                  obj.setComplexBarcodePrefix(row.getValue(offset + 19).asString());
                  obj.setComplexBarcodePluStart(row.getValue(offset + 20).asString());
                  obj.setComplexBarcodePluLength(row.getValue(offset + 21).asString());
                  obj.setComplexBarcodeQtyStart(row.getValue(offset + 22).asString());
                  obj.setComplexBarcodeQtyLength(row.getValue(offset + 23).asString());
                  obj.setComplexBarcodeQtyDecPos(row.getValue(offset + 24).asString());
                  obj.setComplexBarcodeOtherStart(row.getValue(offset + 25).asString());
                  obj.setComplexBarcodeOtherLength(row.getValue(offset + 26).asString());
                  obj.setComplexBarcodeOtherDecPos(row.getValue(offset + 27).asString());
                  obj.setComplexBarcodeOtherType(row.getValue(offset + 28).asString());
                  obj.setComplexBarcodeSuffix(row.getValue(offset + 29).asString());
                  obj.setComplexBarcodeTotalLength(row.getValue(offset + 30).asString());
                  obj.setBackupOutputDir(row.getValue(offset + 31).asString());
                  obj.setBackupFilePref(row.getValue(offset + 32).asString());
                  obj.setPicturePath(row.getValue(offset + 33).asString());
                  obj.setB2bPoPath(row.getValue(offset + 34).asString());
                  obj.setIreportPath(row.getValue(offset + 35).asString());
                  obj.setOnlineHelpUrl(row.getValue(offset + 36).asString());
                  obj.setSmsGatewayUrl(row.getValue(offset + 37).asString());
                  obj.setEmailServer(row.getValue(offset + 38).asString());
                  obj.setEmailUser(row.getValue(offset + 39).asString());
                  obj.setEmailPwd(row.getValue(offset + 40).asString());
                  obj.setSyncAllowPendingTrans(row.getValue(offset + 41).asString());
                  obj.setSyncFullInventory(row.getValue(offset + 42).asString());
                  obj.setSyncAllowStoreAdj(row.getValue(offset + 43).asString());
                  obj.setSyncAllowStoreReceipt(row.getValue(offset + 44).asString());
                  obj.setSyncAllowFreeTransfer(row.getValue(offset + 45).asString());
                  obj.setSyncHodataPath(row.getValue(offset + 46).asString());
                  obj.setSyncStoredataPath(row.getValue(offset + 47).asString());
                  obj.setSyncStoresetupPath(row.getValue(offset + 48).asString());
                  obj.setSyncStoretrfPath(row.getValue(offset + 49).asString());
                  obj.setSyncDestination(row.getValue(offset + 50).asString());
                  obj.setSyncDiskDestination(row.getValue(offset + 51).asString());
                  obj.setSyncHoUploadUrl(row.getValue(offset + 52).asString());
                  obj.setSyncHoEmail(row.getValue(offset + 53).asString());
                  obj.setSyncStoreEmailServer(row.getValue(offset + 54).asString());
                  obj.setSyncStoreEmailUser(row.getValue(offset + 55).asString());
                  obj.setSyncStoreEmailPwd(row.getValue(offset + 56).asString());
                  obj.setSyncDailyClosing(row.getValue(offset + 57).asString());
                  obj.setUseLocationCtltable(row.getValue(offset + 58).asString());
                  obj.setUseCustomerTrPrefix(row.getValue(offset + 59).asString());
                  obj.setUseVendorTrPrefix(row.getValue(offset + 60).asString());
                  obj.setUseVendorTax(row.getValue(offset + 61).asString());
                  obj.setUseCustomerTax(row.getValue(offset + 62).asString());
                  obj.setUseCustomerLocation(row.getValue(offset + 63).asString());
                  obj.setUseCfInOut(row.getValue(offset + 64).asString());
                  obj.setUseDistModules(row.getValue(offset + 65).asString());
                  obj.setUseWstModules(row.getValue(offset + 66).asString());
                  obj.setUseEdiModules(row.getValue(offset + 67).asString());
                  obj.setUseBom(row.getValue(offset + 68).asString());
                  obj.setUsePdt(row.getValue(offset + 69).asString());
                  obj.setConfirmTrfAtToloc(row.getValue(offset + 70).asString());
                  obj.setSiAllowImportSo(row.getValue(offset + 71).asString());
                  obj.setSiDefaultScreen(row.getValue(offset + 72).asString());
                  obj.setItemColorLtMin(row.getValue(offset + 73).asString());
                  obj.setItemColorLtRop(row.getValue(offset + 74).asString());
                  obj.setItemColorGtMax(row.getValue(offset + 75).asString());
                  obj.setItemCodeLength(row.getValue(offset + 76).asString());
                  obj.setCustCodeLength(row.getValue(offset + 77).asString());
                  obj.setVendCodeLength(row.getValue(offset + 78).asString());
                  obj.setOthrCodeLength(row.getValue(offset + 79).asString());
                  obj.setSalesInclusiveTax(row.getValue(offset + 80).asString());
                  obj.setPurchInclusiveTax(row.getValue(offset + 81).asString());
                  obj.setSalesMultiCurrency(row.getValue(offset + 82).asString());
                  obj.setPurchMultiCurrency(row.getValue(offset + 83).asString());
                  obj.setSalesMergeSameItem(row.getValue(offset + 84).asString());
                  obj.setPurchMergeSameItem(row.getValue(offset + 85).asString());
                  obj.setSalesSortBy(row.getValue(offset + 86).asString());
                  obj.setPurchSortBy(row.getValue(offset + 87).asString());
                  obj.setSalesFirstlineInsert(row.getValue(offset + 88).asString());
                  obj.setPurchFirstlineInsert(row.getValue(offset + 89).asString());
                  obj.setInvenFirstlineInsert(row.getValue(offset + 90).asString());
                  obj.setSalesFobCourier(row.getValue(offset + 91).asString());
                  obj.setPurchFobCourier(row.getValue(offset + 92).asString());
                  obj.setSalesCommaScale(row.getValue(offset + 93).asString());
                  obj.setPurchCommaScale(row.getValue(offset + 94).asString());
                  obj.setInvenCommaScale(row.getValue(offset + 95).asString());
                  obj.setCustMaxSbMode(row.getValue(offset + 96).asString());
                  obj.setVendMaxSbMode(row.getValue(offset + 97).asString());
                  obj.setPurchPrQtyGtPo(row.getValue(offset + 98).asString());
                  obj.setPurchPrQtyEqualPo(row.getValue(offset + 99).asString());
                  obj.setPurchLimitByEmp(row.getValue(offset + 100).asString());
                  obj.setPiAllowImportPo(row.getValue(offset + 101).asString());
                  obj.setPiLastPurchMethod(row.getValue(offset + 102).asString());
                  obj.setPiUpdateLastPurchase(row.getValue(offset + 103).asString());
                  obj.setPiUpdateSalesPrice(row.getValue(offset + 104).asString());
                  obj.setPiUpdatePrCost(row.getValue(offset + 105).asString());
                  obj.setSalesUseSalesman(row.getValue(offset + 106).asString());
                  obj.setSalesUseMinPrice(row.getValue(offset + 107).asString());
                  obj.setSalesSalesmanPeritem(row.getValue(offset + 108).asString());
                  obj.setSalesAllowMultipmtDisc(row.getValue(offset + 109).asString());
                  obj.setSalesEmplevelDisc(row.getValue(offset + 110).asString());
                  obj.setSalesCustomPricingUse(row.getValue(offset + 111).asString());
                  obj.setSalesCustomPricingTpl(row.getValue(offset + 112).asString());
                  obj.setPointrewardUse(row.getValue(offset + 113).asString());
                  obj.setPointrewardByitem(row.getValue(offset + 114).asString());
                  obj.setPointrewardField(row.getValue(offset + 115).asString());
                  obj.setPointrewardPvalue(row.getValue(offset + 116).asString());
                  obj.setDefaultSearchCond(row.getValue(offset + 117).asString());
                  obj.setKeyNewTrans(row.getValue(offset + 118).asString());
                  obj.setKeyOpenLookup(row.getValue(offset + 119).asString());
                  obj.setKeyPaymentFocus(row.getValue(offset + 120).asString());
                  obj.setKeySaveTrans(row.getValue(offset + 121).asString());
                  obj.setKeyPrintTrans(row.getValue(offset + 122).asString());
                  obj.setKeyFindTrans(row.getValue(offset + 123).asString());
                  obj.setKeySelectPayment(row.getValue(offset + 124).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseSysConfigPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(SysConfigPeer.row2Object(row, 1,
                SysConfigPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseSysConfigPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(SYS_CONFIG_ID, criteria.remove(SYS_CONFIG_ID));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         SysConfigPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(SysConfig obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(SysConfig obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(SysConfig obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(SysConfig obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(SysConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(SysConfig obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(SysConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(SysConfig obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(SysConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(SysConfig obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseSysConfigPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(SYS_CONFIG_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( SysConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(SYS_CONFIG_ID, obj.getSysConfigId());
              criteria.add(POS_DEFAULT_SCREEN, obj.getPosDefaultScreen());
              criteria.add(POS_DEFAULT_SCREEN_FOCUS, obj.getPosDefaultScreenFocus());
              criteria.add(POS_DEFAULT_OPEN_CASHIER_AMT, obj.getPosDefaultOpenCashierAmt());
              criteria.add(POS_DEFAULT_ITEM_DISPLAY, obj.getPosDefaultItemDisplay());
              criteria.add(POS_DEFAULT_ITEM_SORT, obj.getPosDefaultItemSort());
              criteria.add(POS_DEFAULT_PRINT, obj.getPosDefaultPrint());
              criteria.add(POS_CONFIRM_QTY, obj.getPosConfirmQty());
              criteria.add(POS_CONFIRM_COST, obj.getPosConfirmCost());
              criteria.add(POS_CHANGE_VALIDATION, obj.getPosChangeValidation());
              criteria.add(POS_DELETE_VALIDATION, obj.getPosDeleteValidation());
              criteria.add(POS_OPEN_CASHIER_VALIDATION, obj.getPosOpenCashierValidation());
              criteria.add(POS_SAVE_DIRECT_PRINT, obj.getPosSaveDirectPrint());
              criteria.add(POS_PRINTER_DETAIL_LINE, obj.getPosPrinterDetailLine());
              criteria.add(POS_DIRECT_ADD, obj.getPosDirectAdd());
              criteria.add(POS_ROUNDING_TO, obj.getPosRoundingTo());
              criteria.add(POS_ROUNDING_MODE, obj.getPosRoundingMode());
              criteria.add(POS_SPC_DISC_CODE, obj.getPosSpcDiscCode());
              criteria.add(COMPLEX_BARCODE_USE, obj.getComplexBarcodeUse());
              criteria.add(COMPLEX_BARCODE_PREFIX, obj.getComplexBarcodePrefix());
              criteria.add(COMPLEX_BARCODE_PLU_START, obj.getComplexBarcodePluStart());
              criteria.add(COMPLEX_BARCODE_PLU_LENGTH, obj.getComplexBarcodePluLength());
              criteria.add(COMPLEX_BARCODE_QTY_START, obj.getComplexBarcodeQtyStart());
              criteria.add(COMPLEX_BARCODE_QTY_LENGTH, obj.getComplexBarcodeQtyLength());
              criteria.add(COMPLEX_BARCODE_QTY_DEC_POS, obj.getComplexBarcodeQtyDecPos());
              criteria.add(COMPLEX_BARCODE_OTHER_START, obj.getComplexBarcodeOtherStart());
              criteria.add(COMPLEX_BARCODE_OTHER_LENGTH, obj.getComplexBarcodeOtherLength());
              criteria.add(COMPLEX_BARCODE_OTHER_DEC_POS, obj.getComplexBarcodeOtherDecPos());
              criteria.add(COMPLEX_BARCODE_OTHER_TYPE, obj.getComplexBarcodeOtherType());
              criteria.add(COMPLEX_BARCODE_SUFFIX, obj.getComplexBarcodeSuffix());
              criteria.add(COMPLEX_BARCODE_TOTAL_LENGTH, obj.getComplexBarcodeTotalLength());
              criteria.add(BACKUP_OUTPUT_DIR, obj.getBackupOutputDir());
              criteria.add(BACKUP_FILE_PREF, obj.getBackupFilePref());
              criteria.add(PICTURE_PATH, obj.getPicturePath());
              criteria.add(B2B_PO_PATH, obj.getB2bPoPath());
              criteria.add(IREPORT_PATH, obj.getIreportPath());
              criteria.add(ONLINE_HELP_URL, obj.getOnlineHelpUrl());
              criteria.add(SMS_GATEWAY_URL, obj.getSmsGatewayUrl());
              criteria.add(EMAIL_SERVER, obj.getEmailServer());
              criteria.add(EMAIL_USER, obj.getEmailUser());
              criteria.add(EMAIL_PWD, obj.getEmailPwd());
              criteria.add(SYNC_ALLOW_PENDING_TRANS, obj.getSyncAllowPendingTrans());
              criteria.add(SYNC_FULL_INVENTORY, obj.getSyncFullInventory());
              criteria.add(SYNC_ALLOW_STORE_ADJ, obj.getSyncAllowStoreAdj());
              criteria.add(SYNC_ALLOW_STORE_RECEIPT, obj.getSyncAllowStoreReceipt());
              criteria.add(SYNC_ALLOW_FREE_TRANSFER, obj.getSyncAllowFreeTransfer());
              criteria.add(SYNC_HODATA_PATH, obj.getSyncHodataPath());
              criteria.add(SYNC_STOREDATA_PATH, obj.getSyncStoredataPath());
              criteria.add(SYNC_STORESETUP_PATH, obj.getSyncStoresetupPath());
              criteria.add(SYNC_STORETRF_PATH, obj.getSyncStoretrfPath());
              criteria.add(SYNC_DESTINATION, obj.getSyncDestination());
              criteria.add(SYNC_DISK_DESTINATION, obj.getSyncDiskDestination());
              criteria.add(SYNC_HO_UPLOAD_URL, obj.getSyncHoUploadUrl());
              criteria.add(SYNC_HO_EMAIL, obj.getSyncHoEmail());
              criteria.add(SYNC_STORE_EMAIL_SERVER, obj.getSyncStoreEmailServer());
              criteria.add(SYNC_STORE_EMAIL_USER, obj.getSyncStoreEmailUser());
              criteria.add(SYNC_STORE_EMAIL_PWD, obj.getSyncStoreEmailPwd());
              criteria.add(SYNC_DAILY_CLOSING, obj.getSyncDailyClosing());
              criteria.add(USE_LOCATION_CTLTABLE, obj.getUseLocationCtltable());
              criteria.add(USE_CUSTOMER_TR_PREFIX, obj.getUseCustomerTrPrefix());
              criteria.add(USE_VENDOR_TR_PREFIX, obj.getUseVendorTrPrefix());
              criteria.add(USE_VENDOR_TAX, obj.getUseVendorTax());
              criteria.add(USE_CUSTOMER_TAX, obj.getUseCustomerTax());
              criteria.add(USE_CUSTOMER_LOCATION, obj.getUseCustomerLocation());
              criteria.add(USE_CF_IN_OUT, obj.getUseCfInOut());
              criteria.add(USE_DIST_MODULES, obj.getUseDistModules());
              criteria.add(USE_WST_MODULES, obj.getUseWstModules());
              criteria.add(USE_EDI_MODULES, obj.getUseEdiModules());
              criteria.add(USE_BOM, obj.getUseBom());
              criteria.add(USE_PDT, obj.getUsePdt());
              criteria.add(CONFIRM_TRF_AT_TOLOC, obj.getConfirmTrfAtToloc());
              criteria.add(SI_ALLOW_IMPORT_SO, obj.getSiAllowImportSo());
              criteria.add(SI_DEFAULT_SCREEN, obj.getSiDefaultScreen());
              criteria.add(ITEM_COLOR_LT_MIN, obj.getItemColorLtMin());
              criteria.add(ITEM_COLOR_LT_ROP, obj.getItemColorLtRop());
              criteria.add(ITEM_COLOR_GT_MAX, obj.getItemColorGtMax());
              criteria.add(ITEM_CODE_LENGTH, obj.getItemCodeLength());
              criteria.add(CUST_CODE_LENGTH, obj.getCustCodeLength());
              criteria.add(VEND_CODE_LENGTH, obj.getVendCodeLength());
              criteria.add(OTHR_CODE_LENGTH, obj.getOthrCodeLength());
              criteria.add(SALES_INCLUSIVE_TAX, obj.getSalesInclusiveTax());
              criteria.add(PURCH_INCLUSIVE_TAX, obj.getPurchInclusiveTax());
              criteria.add(SALES_MULTI_CURRENCY, obj.getSalesMultiCurrency());
              criteria.add(PURCH_MULTI_CURRENCY, obj.getPurchMultiCurrency());
              criteria.add(SALES_MERGE_SAME_ITEM, obj.getSalesMergeSameItem());
              criteria.add(PURCH_MERGE_SAME_ITEM, obj.getPurchMergeSameItem());
              criteria.add(SALES_SORT_BY, obj.getSalesSortBy());
              criteria.add(PURCH_SORT_BY, obj.getPurchSortBy());
              criteria.add(SALES_FIRSTLINE_INSERT, obj.getSalesFirstlineInsert());
              criteria.add(PURCH_FIRSTLINE_INSERT, obj.getPurchFirstlineInsert());
              criteria.add(INVEN_FIRSTLINE_INSERT, obj.getInvenFirstlineInsert());
              criteria.add(SALES_FOB_COURIER, obj.getSalesFobCourier());
              criteria.add(PURCH_FOB_COURIER, obj.getPurchFobCourier());
              criteria.add(SALES_COMMA_SCALE, obj.getSalesCommaScale());
              criteria.add(PURCH_COMMA_SCALE, obj.getPurchCommaScale());
              criteria.add(INVEN_COMMA_SCALE, obj.getInvenCommaScale());
              criteria.add(CUST_MAX_SB_MODE, obj.getCustMaxSbMode());
              criteria.add(VEND_MAX_SB_MODE, obj.getVendMaxSbMode());
              criteria.add(PURCH_PR_QTY_GT_PO, obj.getPurchPrQtyGtPo());
              criteria.add(PURCH_PR_QTY_EQUAL_PO, obj.getPurchPrQtyEqualPo());
              criteria.add(PURCH_LIMIT_BY_EMP, obj.getPurchLimitByEmp());
              criteria.add(PI_ALLOW_IMPORT_PO, obj.getPiAllowImportPo());
              criteria.add(PI_LAST_PURCH_METHOD, obj.getPiLastPurchMethod());
              criteria.add(PI_UPDATE_LAST_PURCHASE, obj.getPiUpdateLastPurchase());
              criteria.add(PI_UPDATE_SALES_PRICE, obj.getPiUpdateSalesPrice());
              criteria.add(PI_UPDATE_PR_COST, obj.getPiUpdatePrCost());
              criteria.add(SALES_USE_SALESMAN, obj.getSalesUseSalesman());
              criteria.add(SALES_USE_MIN_PRICE, obj.getSalesUseMinPrice());
              criteria.add(SALES_SALESMAN_PERITEM, obj.getSalesSalesmanPeritem());
              criteria.add(SALES_ALLOW_MULTIPMT_DISC, obj.getSalesAllowMultipmtDisc());
              criteria.add(SALES_EMPLEVEL_DISC, obj.getSalesEmplevelDisc());
              criteria.add(SALES_CUSTOM_PRICING_USE, obj.getSalesCustomPricingUse());
              criteria.add(SALES_CUSTOM_PRICING_TPL, obj.getSalesCustomPricingTpl());
              criteria.add(POINTREWARD_USE, obj.getPointrewardUse());
              criteria.add(POINTREWARD_BYITEM, obj.getPointrewardByitem());
              criteria.add(POINTREWARD_FIELD, obj.getPointrewardField());
              criteria.add(POINTREWARD_PVALUE, obj.getPointrewardPvalue());
              criteria.add(DEFAULT_SEARCH_COND, obj.getDefaultSearchCond());
              criteria.add(KEY_NEW_TRANS, obj.getKeyNewTrans());
              criteria.add(KEY_OPEN_LOOKUP, obj.getKeyOpenLookup());
              criteria.add(KEY_PAYMENT_FOCUS, obj.getKeyPaymentFocus());
              criteria.add(KEY_SAVE_TRANS, obj.getKeySaveTrans());
              criteria.add(KEY_PRINT_TRANS, obj.getKeyPrintTrans());
              criteria.add(KEY_FIND_TRANS, obj.getKeyFindTrans());
              criteria.add(KEY_SELECT_PAYMENT, obj.getKeySelectPayment());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( SysConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(SYS_CONFIG_ID, obj.getSysConfigId());
                          criteria.add(POS_DEFAULT_SCREEN, obj.getPosDefaultScreen());
                          criteria.add(POS_DEFAULT_SCREEN_FOCUS, obj.getPosDefaultScreenFocus());
                          criteria.add(POS_DEFAULT_OPEN_CASHIER_AMT, obj.getPosDefaultOpenCashierAmt());
                          criteria.add(POS_DEFAULT_ITEM_DISPLAY, obj.getPosDefaultItemDisplay());
                          criteria.add(POS_DEFAULT_ITEM_SORT, obj.getPosDefaultItemSort());
                          criteria.add(POS_DEFAULT_PRINT, obj.getPosDefaultPrint());
                          criteria.add(POS_CONFIRM_QTY, obj.getPosConfirmQty());
                          criteria.add(POS_CONFIRM_COST, obj.getPosConfirmCost());
                          criteria.add(POS_CHANGE_VALIDATION, obj.getPosChangeValidation());
                          criteria.add(POS_DELETE_VALIDATION, obj.getPosDeleteValidation());
                          criteria.add(POS_OPEN_CASHIER_VALIDATION, obj.getPosOpenCashierValidation());
                          criteria.add(POS_SAVE_DIRECT_PRINT, obj.getPosSaveDirectPrint());
                          criteria.add(POS_PRINTER_DETAIL_LINE, obj.getPosPrinterDetailLine());
                          criteria.add(POS_DIRECT_ADD, obj.getPosDirectAdd());
                          criteria.add(POS_ROUNDING_TO, obj.getPosRoundingTo());
                          criteria.add(POS_ROUNDING_MODE, obj.getPosRoundingMode());
                          criteria.add(POS_SPC_DISC_CODE, obj.getPosSpcDiscCode());
                          criteria.add(COMPLEX_BARCODE_USE, obj.getComplexBarcodeUse());
                          criteria.add(COMPLEX_BARCODE_PREFIX, obj.getComplexBarcodePrefix());
                          criteria.add(COMPLEX_BARCODE_PLU_START, obj.getComplexBarcodePluStart());
                          criteria.add(COMPLEX_BARCODE_PLU_LENGTH, obj.getComplexBarcodePluLength());
                          criteria.add(COMPLEX_BARCODE_QTY_START, obj.getComplexBarcodeQtyStart());
                          criteria.add(COMPLEX_BARCODE_QTY_LENGTH, obj.getComplexBarcodeQtyLength());
                          criteria.add(COMPLEX_BARCODE_QTY_DEC_POS, obj.getComplexBarcodeQtyDecPos());
                          criteria.add(COMPLEX_BARCODE_OTHER_START, obj.getComplexBarcodeOtherStart());
                          criteria.add(COMPLEX_BARCODE_OTHER_LENGTH, obj.getComplexBarcodeOtherLength());
                          criteria.add(COMPLEX_BARCODE_OTHER_DEC_POS, obj.getComplexBarcodeOtherDecPos());
                          criteria.add(COMPLEX_BARCODE_OTHER_TYPE, obj.getComplexBarcodeOtherType());
                          criteria.add(COMPLEX_BARCODE_SUFFIX, obj.getComplexBarcodeSuffix());
                          criteria.add(COMPLEX_BARCODE_TOTAL_LENGTH, obj.getComplexBarcodeTotalLength());
                          criteria.add(BACKUP_OUTPUT_DIR, obj.getBackupOutputDir());
                          criteria.add(BACKUP_FILE_PREF, obj.getBackupFilePref());
                          criteria.add(PICTURE_PATH, obj.getPicturePath());
                          criteria.add(B2B_PO_PATH, obj.getB2bPoPath());
                          criteria.add(IREPORT_PATH, obj.getIreportPath());
                          criteria.add(ONLINE_HELP_URL, obj.getOnlineHelpUrl());
                          criteria.add(SMS_GATEWAY_URL, obj.getSmsGatewayUrl());
                          criteria.add(EMAIL_SERVER, obj.getEmailServer());
                          criteria.add(EMAIL_USER, obj.getEmailUser());
                          criteria.add(EMAIL_PWD, obj.getEmailPwd());
                          criteria.add(SYNC_ALLOW_PENDING_TRANS, obj.getSyncAllowPendingTrans());
                          criteria.add(SYNC_FULL_INVENTORY, obj.getSyncFullInventory());
                          criteria.add(SYNC_ALLOW_STORE_ADJ, obj.getSyncAllowStoreAdj());
                          criteria.add(SYNC_ALLOW_STORE_RECEIPT, obj.getSyncAllowStoreReceipt());
                          criteria.add(SYNC_ALLOW_FREE_TRANSFER, obj.getSyncAllowFreeTransfer());
                          criteria.add(SYNC_HODATA_PATH, obj.getSyncHodataPath());
                          criteria.add(SYNC_STOREDATA_PATH, obj.getSyncStoredataPath());
                          criteria.add(SYNC_STORESETUP_PATH, obj.getSyncStoresetupPath());
                          criteria.add(SYNC_STORETRF_PATH, obj.getSyncStoretrfPath());
                          criteria.add(SYNC_DESTINATION, obj.getSyncDestination());
                          criteria.add(SYNC_DISK_DESTINATION, obj.getSyncDiskDestination());
                          criteria.add(SYNC_HO_UPLOAD_URL, obj.getSyncHoUploadUrl());
                          criteria.add(SYNC_HO_EMAIL, obj.getSyncHoEmail());
                          criteria.add(SYNC_STORE_EMAIL_SERVER, obj.getSyncStoreEmailServer());
                          criteria.add(SYNC_STORE_EMAIL_USER, obj.getSyncStoreEmailUser());
                          criteria.add(SYNC_STORE_EMAIL_PWD, obj.getSyncStoreEmailPwd());
                          criteria.add(SYNC_DAILY_CLOSING, obj.getSyncDailyClosing());
                          criteria.add(USE_LOCATION_CTLTABLE, obj.getUseLocationCtltable());
                          criteria.add(USE_CUSTOMER_TR_PREFIX, obj.getUseCustomerTrPrefix());
                          criteria.add(USE_VENDOR_TR_PREFIX, obj.getUseVendorTrPrefix());
                          criteria.add(USE_VENDOR_TAX, obj.getUseVendorTax());
                          criteria.add(USE_CUSTOMER_TAX, obj.getUseCustomerTax());
                          criteria.add(USE_CUSTOMER_LOCATION, obj.getUseCustomerLocation());
                          criteria.add(USE_CF_IN_OUT, obj.getUseCfInOut());
                          criteria.add(USE_DIST_MODULES, obj.getUseDistModules());
                          criteria.add(USE_WST_MODULES, obj.getUseWstModules());
                          criteria.add(USE_EDI_MODULES, obj.getUseEdiModules());
                          criteria.add(USE_BOM, obj.getUseBom());
                          criteria.add(USE_PDT, obj.getUsePdt());
                          criteria.add(CONFIRM_TRF_AT_TOLOC, obj.getConfirmTrfAtToloc());
                          criteria.add(SI_ALLOW_IMPORT_SO, obj.getSiAllowImportSo());
                          criteria.add(SI_DEFAULT_SCREEN, obj.getSiDefaultScreen());
                          criteria.add(ITEM_COLOR_LT_MIN, obj.getItemColorLtMin());
                          criteria.add(ITEM_COLOR_LT_ROP, obj.getItemColorLtRop());
                          criteria.add(ITEM_COLOR_GT_MAX, obj.getItemColorGtMax());
                          criteria.add(ITEM_CODE_LENGTH, obj.getItemCodeLength());
                          criteria.add(CUST_CODE_LENGTH, obj.getCustCodeLength());
                          criteria.add(VEND_CODE_LENGTH, obj.getVendCodeLength());
                          criteria.add(OTHR_CODE_LENGTH, obj.getOthrCodeLength());
                          criteria.add(SALES_INCLUSIVE_TAX, obj.getSalesInclusiveTax());
                          criteria.add(PURCH_INCLUSIVE_TAX, obj.getPurchInclusiveTax());
                          criteria.add(SALES_MULTI_CURRENCY, obj.getSalesMultiCurrency());
                          criteria.add(PURCH_MULTI_CURRENCY, obj.getPurchMultiCurrency());
                          criteria.add(SALES_MERGE_SAME_ITEM, obj.getSalesMergeSameItem());
                          criteria.add(PURCH_MERGE_SAME_ITEM, obj.getPurchMergeSameItem());
                          criteria.add(SALES_SORT_BY, obj.getSalesSortBy());
                          criteria.add(PURCH_SORT_BY, obj.getPurchSortBy());
                          criteria.add(SALES_FIRSTLINE_INSERT, obj.getSalesFirstlineInsert());
                          criteria.add(PURCH_FIRSTLINE_INSERT, obj.getPurchFirstlineInsert());
                          criteria.add(INVEN_FIRSTLINE_INSERT, obj.getInvenFirstlineInsert());
                          criteria.add(SALES_FOB_COURIER, obj.getSalesFobCourier());
                          criteria.add(PURCH_FOB_COURIER, obj.getPurchFobCourier());
                          criteria.add(SALES_COMMA_SCALE, obj.getSalesCommaScale());
                          criteria.add(PURCH_COMMA_SCALE, obj.getPurchCommaScale());
                          criteria.add(INVEN_COMMA_SCALE, obj.getInvenCommaScale());
                          criteria.add(CUST_MAX_SB_MODE, obj.getCustMaxSbMode());
                          criteria.add(VEND_MAX_SB_MODE, obj.getVendMaxSbMode());
                          criteria.add(PURCH_PR_QTY_GT_PO, obj.getPurchPrQtyGtPo());
                          criteria.add(PURCH_PR_QTY_EQUAL_PO, obj.getPurchPrQtyEqualPo());
                          criteria.add(PURCH_LIMIT_BY_EMP, obj.getPurchLimitByEmp());
                          criteria.add(PI_ALLOW_IMPORT_PO, obj.getPiAllowImportPo());
                          criteria.add(PI_LAST_PURCH_METHOD, obj.getPiLastPurchMethod());
                          criteria.add(PI_UPDATE_LAST_PURCHASE, obj.getPiUpdateLastPurchase());
                          criteria.add(PI_UPDATE_SALES_PRICE, obj.getPiUpdateSalesPrice());
                          criteria.add(PI_UPDATE_PR_COST, obj.getPiUpdatePrCost());
                          criteria.add(SALES_USE_SALESMAN, obj.getSalesUseSalesman());
                          criteria.add(SALES_USE_MIN_PRICE, obj.getSalesUseMinPrice());
                          criteria.add(SALES_SALESMAN_PERITEM, obj.getSalesSalesmanPeritem());
                          criteria.add(SALES_ALLOW_MULTIPMT_DISC, obj.getSalesAllowMultipmtDisc());
                          criteria.add(SALES_EMPLEVEL_DISC, obj.getSalesEmplevelDisc());
                          criteria.add(SALES_CUSTOM_PRICING_USE, obj.getSalesCustomPricingUse());
                          criteria.add(SALES_CUSTOM_PRICING_TPL, obj.getSalesCustomPricingTpl());
                          criteria.add(POINTREWARD_USE, obj.getPointrewardUse());
                          criteria.add(POINTREWARD_BYITEM, obj.getPointrewardByitem());
                          criteria.add(POINTREWARD_FIELD, obj.getPointrewardField());
                          criteria.add(POINTREWARD_PVALUE, obj.getPointrewardPvalue());
                          criteria.add(DEFAULT_SEARCH_COND, obj.getDefaultSearchCond());
                          criteria.add(KEY_NEW_TRANS, obj.getKeyNewTrans());
                          criteria.add(KEY_OPEN_LOOKUP, obj.getKeyOpenLookup());
                          criteria.add(KEY_PAYMENT_FOCUS, obj.getKeyPaymentFocus());
                          criteria.add(KEY_SAVE_TRANS, obj.getKeySaveTrans());
                          criteria.add(KEY_PRINT_TRANS, obj.getKeyPrintTrans());
                          criteria.add(KEY_FIND_TRANS, obj.getKeyFindTrans());
                          criteria.add(KEY_SELECT_PAYMENT, obj.getKeySelectPayment());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SysConfig retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SysConfig retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SysConfig retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        SysConfig retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static SysConfig retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (SysConfig)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( SYS_CONFIG_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
