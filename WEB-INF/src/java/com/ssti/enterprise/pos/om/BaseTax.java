package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Tax
 */
public abstract class BaseTax extends BaseObject
{
    /** The Peer class */
    private static final TaxPeer peer =
        new TaxPeer();

        
    /** The value for the taxId field */
    private String taxId;
      
    /** The value for the taxCode field */
    private String taxCode;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the defaultSalesTax field */
    private boolean defaultSalesTax;
      
    /** The value for the defaultPurchaseTax field */
    private boolean defaultPurchaseTax;
                                                
    /** The value for the salesTaxAccount field */
    private String salesTaxAccount = "";
                                                
    /** The value for the purchaseTaxAccount field */
    private String purchaseTaxAccount = "";
  
    
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxCode
     *
     * @return String
     */
    public String getTaxCode()
    {
        return taxCode;
    }

                        
    /**
     * Set the value of TaxCode
     *
     * @param v new value
     */
    public void setTaxCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxCode, v))
              {
            this.taxCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultSalesTax
     *
     * @return boolean
     */
    public boolean getDefaultSalesTax()
    {
        return defaultSalesTax;
    }

                        
    /**
     * Set the value of DefaultSalesTax
     *
     * @param v new value
     */
    public void setDefaultSalesTax(boolean v) 
    {
    
                  if (this.defaultSalesTax != v)
              {
            this.defaultSalesTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultPurchaseTax
     *
     * @return boolean
     */
    public boolean getDefaultPurchaseTax()
    {
        return defaultPurchaseTax;
    }

                        
    /**
     * Set the value of DefaultPurchaseTax
     *
     * @param v new value
     */
    public void setDefaultPurchaseTax(boolean v) 
    {
    
                  if (this.defaultPurchaseTax != v)
              {
            this.defaultPurchaseTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesTaxAccount
     *
     * @return String
     */
    public String getSalesTaxAccount()
    {
        return salesTaxAccount;
    }

                        
    /**
     * Set the value of SalesTaxAccount
     *
     * @param v new value
     */
    public void setSalesTaxAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesTaxAccount, v))
              {
            this.salesTaxAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseTaxAccount
     *
     * @return String
     */
    public String getPurchaseTaxAccount()
    {
        return purchaseTaxAccount;
    }

                        
    /**
     * Set the value of PurchaseTaxAccount
     *
     * @param v new value
     */
    public void setPurchaseTaxAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseTaxAccount, v))
              {
            this.purchaseTaxAccount = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("TaxId");
              fieldNames.add("TaxCode");
              fieldNames.add("Amount");
              fieldNames.add("Description");
              fieldNames.add("DefaultSalesTax");
              fieldNames.add("DefaultPurchaseTax");
              fieldNames.add("SalesTaxAccount");
              fieldNames.add("PurchaseTaxAccount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("TaxCode"))
        {
                return getTaxCode();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("DefaultSalesTax"))
        {
                return Boolean.valueOf(getDefaultSalesTax());
            }
          if (name.equals("DefaultPurchaseTax"))
        {
                return Boolean.valueOf(getDefaultPurchaseTax());
            }
          if (name.equals("SalesTaxAccount"))
        {
                return getSalesTaxAccount();
            }
          if (name.equals("PurchaseTaxAccount"))
        {
                return getPurchaseTaxAccount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(TaxPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(TaxPeer.TAX_CODE))
        {
                return getTaxCode();
            }
          if (name.equals(TaxPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(TaxPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(TaxPeer.DEFAULT_SALES_TAX))
        {
                return Boolean.valueOf(getDefaultSalesTax());
            }
          if (name.equals(TaxPeer.DEFAULT_PURCHASE_TAX))
        {
                return Boolean.valueOf(getDefaultPurchaseTax());
            }
          if (name.equals(TaxPeer.SALES_TAX_ACCOUNT))
        {
                return getSalesTaxAccount();
            }
          if (name.equals(TaxPeer.PURCHASE_TAX_ACCOUNT))
        {
                return getPurchaseTaxAccount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getTaxId();
            }
              if (pos == 1)
        {
                return getTaxCode();
            }
              if (pos == 2)
        {
                return getAmount();
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return Boolean.valueOf(getDefaultSalesTax());
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getDefaultPurchaseTax());
            }
              if (pos == 6)
        {
                return getSalesTaxAccount();
            }
              if (pos == 7)
        {
                return getPurchaseTaxAccount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(TaxPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        TaxPeer.doInsert((Tax) this, con);
                        setNew(false);
                    }
                    else
                    {
                        TaxPeer.doUpdate((Tax) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key taxId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setTaxId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setTaxId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getTaxId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Tax copy() throws TorqueException
    {
        return copyInto(new Tax());
    }
  
    protected Tax copyInto(Tax copyObj) throws TorqueException
    {
          copyObj.setTaxId(taxId);
          copyObj.setTaxCode(taxCode);
          copyObj.setAmount(amount);
          copyObj.setDescription(description);
          copyObj.setDefaultSalesTax(defaultSalesTax);
          copyObj.setDefaultPurchaseTax(defaultPurchaseTax);
          copyObj.setSalesTaxAccount(salesTaxAccount);
          copyObj.setPurchaseTaxAccount(purchaseTaxAccount);
  
                    copyObj.setTaxId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public TaxPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Tax\n");
        str.append("---\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("TaxCode              : ")
           .append(getTaxCode())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("DefaultSalesTax      : ")
           .append(getDefaultSalesTax())
           .append("\n")
           .append("DefaultPurchaseTax   : ")
           .append(getDefaultPurchaseTax())
           .append("\n")
           .append("SalesTaxAccount      : ")
           .append(getSalesTaxAccount())
           .append("\n")
           .append("PurchaseTaxAccount   : ")
           .append(getPurchaseTaxAccount())
           .append("\n")
        ;
        return(str.toString());
    }
}
