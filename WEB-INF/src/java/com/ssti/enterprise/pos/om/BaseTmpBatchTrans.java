package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to TmpBatchTrans
 */
public abstract class BaseTmpBatchTrans extends BaseObject
{
    /** The Peer class */
    private static final TmpBatchTransPeer peer =
        new TmpBatchTransPeer();

        
    /** The value for the tmpBatchTransId field */
    private String tmpBatchTransId;
      
    /** The value for the itemId field */
    private String itemId;
                                                
    /** The value for the batchNo field */
    private String batchNo = "";
      
    /** The value for the expiredDate field */
    private Date expiredDate;
      
    /** The value for the description field */
    private String description;
                                                
    /** The value for the transactionDetailId field */
    private String transactionDetailId = "";
                                                
    /** The value for the transactionId field */
    private String transactionId = "";
      
    /** The value for the transactionType field */
    private int transactionType;
  
    
    /**
     * Get the TmpBatchTransId
     *
     * @return String
     */
    public String getTmpBatchTransId()
    {
        return tmpBatchTransId;
    }

                        
    /**
     * Set the value of TmpBatchTransId
     *
     * @param v new value
     */
    public void setTmpBatchTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tmpBatchTransId, v))
              {
            this.tmpBatchTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BatchNo
     *
     * @return String
     */
    public String getBatchNo()
    {
        return batchNo;
    }

                        
    /**
     * Set the value of BatchNo
     *
     * @param v new value
     */
    public void setBatchNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchNo, v))
              {
            this.batchNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpiredDate
     *
     * @return Date
     */
    public Date getExpiredDate()
    {
        return expiredDate;
    }

                        
    /**
     * Set the value of ExpiredDate
     *
     * @param v new value
     */
    public void setExpiredDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expiredDate, v))
              {
            this.expiredDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDetailId
     *
     * @return String
     */
    public String getTransactionDetailId()
    {
        return transactionDetailId;
    }

                        
    /**
     * Set the value of TransactionDetailId
     *
     * @param v new value
     */
    public void setTransactionDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDetailId, v))
              {
            this.transactionDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("TmpBatchTransId");
              fieldNames.add("ItemId");
              fieldNames.add("BatchNo");
              fieldNames.add("ExpiredDate");
              fieldNames.add("Description");
              fieldNames.add("TransactionDetailId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionType");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("TmpBatchTransId"))
        {
                return getTmpBatchTransId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("BatchNo"))
        {
                return getBatchNo();
            }
          if (name.equals("ExpiredDate"))
        {
                return getExpiredDate();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("TransactionDetailId"))
        {
                return getTransactionDetailId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(TmpBatchTransPeer.TMP_BATCH_TRANS_ID))
        {
                return getTmpBatchTransId();
            }
          if (name.equals(TmpBatchTransPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(TmpBatchTransPeer.BATCH_NO))
        {
                return getBatchNo();
            }
          if (name.equals(TmpBatchTransPeer.EXPIRED_DATE))
        {
                return getExpiredDate();
            }
          if (name.equals(TmpBatchTransPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(TmpBatchTransPeer.TRANSACTION_DETAIL_ID))
        {
                return getTransactionDetailId();
            }
          if (name.equals(TmpBatchTransPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(TmpBatchTransPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getTmpBatchTransId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getBatchNo();
            }
              if (pos == 3)
        {
                return getExpiredDate();
            }
              if (pos == 4)
        {
                return getDescription();
            }
              if (pos == 5)
        {
                return getTransactionDetailId();
            }
              if (pos == 6)
        {
                return getTransactionId();
            }
              if (pos == 7)
        {
                return Integer.valueOf(getTransactionType());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(TmpBatchTransPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        TmpBatchTransPeer.doInsert((TmpBatchTrans) this, con);
                        setNew(false);
                    }
                    else
                    {
                        TmpBatchTransPeer.doUpdate((TmpBatchTrans) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key tmpBatchTransId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setTmpBatchTransId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setTmpBatchTransId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getTmpBatchTransId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public TmpBatchTrans copy() throws TorqueException
    {
        return copyInto(new TmpBatchTrans());
    }
  
    protected TmpBatchTrans copyInto(TmpBatchTrans copyObj) throws TorqueException
    {
          copyObj.setTmpBatchTransId(tmpBatchTransId);
          copyObj.setItemId(itemId);
          copyObj.setBatchNo(batchNo);
          copyObj.setExpiredDate(expiredDate);
          copyObj.setDescription(description);
          copyObj.setTransactionDetailId(transactionDetailId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionType(transactionType);
  
                    copyObj.setTmpBatchTransId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public TmpBatchTransPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("TmpBatchTrans\n");
        str.append("-------------\n")
           .append("TmpBatchTransId      : ")
           .append(getTmpBatchTransId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("BatchNo              : ")
           .append(getBatchNo())
           .append("\n")
           .append("ExpiredDate          : ")
           .append(getExpiredDate())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("TransactionDetailId  : ")
           .append(getTransactionDetailId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
        ;
        return(str.toString());
    }
}
