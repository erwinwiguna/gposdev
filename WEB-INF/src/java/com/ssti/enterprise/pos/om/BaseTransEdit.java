package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to TransEdit
 */
public abstract class BaseTransEdit extends BaseObject
{
    /** The Peer class */
    private static final TransEditPeer peer =
        new TransEditPeer();

        
    /** The value for the transEditId field */
    private String transEditId;
      
    /** The value for the transId field */
    private String transId;
      
    /** The value for the transNo field */
    private String transNo;
      
    /** The value for the transType field */
    private int transType;
      
    /** The value for the editDate field */
    private Date editDate;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the fieldName field */
    private String fieldName;
      
    /** The value for the oldValue field */
    private String oldValue;
      
    /** The value for the newValue field */
    private String newValue;
  
    
    /**
     * Get the TransEditId
     *
     * @return String
     */
    public String getTransEditId()
    {
        return transEditId;
    }

                        
    /**
     * Set the value of TransEditId
     *
     * @param v new value
     */
    public void setTransEditId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transEditId, v))
              {
            this.transEditId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransId
     *
     * @return String
     */
    public String getTransId()
    {
        return transId;
    }

                        
    /**
     * Set the value of TransId
     *
     * @param v new value
     */
    public void setTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transId, v))
              {
            this.transId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransNo
     *
     * @return String
     */
    public String getTransNo()
    {
        return transNo;
    }

                        
    /**
     * Set the value of TransNo
     *
     * @param v new value
     */
    public void setTransNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transNo, v))
              {
            this.transNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransType
     *
     * @return int
     */
    public int getTransType()
    {
        return transType;
    }

                        
    /**
     * Set the value of TransType
     *
     * @param v new value
     */
    public void setTransType(int v) 
    {
    
                  if (this.transType != v)
              {
            this.transType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EditDate
     *
     * @return Date
     */
    public Date getEditDate()
    {
        return editDate;
    }

                        
    /**
     * Set the value of EditDate
     *
     * @param v new value
     */
    public void setEditDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.editDate, v))
              {
            this.editDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldName
     *
     * @return String
     */
    public String getFieldName()
    {
        return fieldName;
    }

                        
    /**
     * Set the value of FieldName
     *
     * @param v new value
     */
    public void setFieldName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldName, v))
              {
            this.fieldName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OldValue
     *
     * @return String
     */
    public String getOldValue()
    {
        return oldValue;
    }

                        
    /**
     * Set the value of OldValue
     *
     * @param v new value
     */
    public void setOldValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.oldValue, v))
              {
            this.oldValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NewValue
     *
     * @return String
     */
    public String getNewValue()
    {
        return newValue;
    }

                        
    /**
     * Set the value of NewValue
     *
     * @param v new value
     */
    public void setNewValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.newValue, v))
              {
            this.newValue = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("TransEditId");
              fieldNames.add("TransId");
              fieldNames.add("TransNo");
              fieldNames.add("TransType");
              fieldNames.add("EditDate");
              fieldNames.add("UserName");
              fieldNames.add("FieldName");
              fieldNames.add("OldValue");
              fieldNames.add("NewValue");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("TransEditId"))
        {
                return getTransEditId();
            }
          if (name.equals("TransId"))
        {
                return getTransId();
            }
          if (name.equals("TransNo"))
        {
                return getTransNo();
            }
          if (name.equals("TransType"))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals("EditDate"))
        {
                return getEditDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("FieldName"))
        {
                return getFieldName();
            }
          if (name.equals("OldValue"))
        {
                return getOldValue();
            }
          if (name.equals("NewValue"))
        {
                return getNewValue();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(TransEditPeer.TRANS_EDIT_ID))
        {
                return getTransEditId();
            }
          if (name.equals(TransEditPeer.TRANS_ID))
        {
                return getTransId();
            }
          if (name.equals(TransEditPeer.TRANS_NO))
        {
                return getTransNo();
            }
          if (name.equals(TransEditPeer.TRANS_TYPE))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals(TransEditPeer.EDIT_DATE))
        {
                return getEditDate();
            }
          if (name.equals(TransEditPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(TransEditPeer.FIELD_NAME))
        {
                return getFieldName();
            }
          if (name.equals(TransEditPeer.OLD_VALUE))
        {
                return getOldValue();
            }
          if (name.equals(TransEditPeer.NEW_VALUE))
        {
                return getNewValue();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getTransEditId();
            }
              if (pos == 1)
        {
                return getTransId();
            }
              if (pos == 2)
        {
                return getTransNo();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getTransType());
            }
              if (pos == 4)
        {
                return getEditDate();
            }
              if (pos == 5)
        {
                return getUserName();
            }
              if (pos == 6)
        {
                return getFieldName();
            }
              if (pos == 7)
        {
                return getOldValue();
            }
              if (pos == 8)
        {
                return getNewValue();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(TransEditPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        TransEditPeer.doInsert((TransEdit) this, con);
                        setNew(false);
                    }
                    else
                    {
                        TransEditPeer.doUpdate((TransEdit) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key transEditId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setTransEditId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setTransEditId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getTransEditId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public TransEdit copy() throws TorqueException
    {
        return copyInto(new TransEdit());
    }
  
    protected TransEdit copyInto(TransEdit copyObj) throws TorqueException
    {
          copyObj.setTransEditId(transEditId);
          copyObj.setTransId(transId);
          copyObj.setTransNo(transNo);
          copyObj.setTransType(transType);
          copyObj.setEditDate(editDate);
          copyObj.setUserName(userName);
          copyObj.setFieldName(fieldName);
          copyObj.setOldValue(oldValue);
          copyObj.setNewValue(newValue);
  
                    copyObj.setTransEditId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public TransEditPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("TransEdit\n");
        str.append("---------\n")
           .append("TransEditId          : ")
           .append(getTransEditId())
           .append("\n")
           .append("TransId              : ")
           .append(getTransId())
           .append("\n")
           .append("TransNo              : ")
           .append(getTransNo())
           .append("\n")
           .append("TransType            : ")
           .append(getTransType())
           .append("\n")
           .append("EditDate             : ")
           .append(getEditDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("FieldName            : ")
           .append(getFieldName())
           .append("\n")
           .append("OldValue             : ")
           .append(getOldValue())
           .append("\n")
           .append("NewValue             : ")
           .append(getNewValue())
           .append("\n")
        ;
        return(str.toString());
    }
}
