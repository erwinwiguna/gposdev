package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Unit
 */
public abstract class BaseUnit extends BaseObject
{
    /** The Peer class */
    private static final UnitPeer peer =
        new UnitPeer();

        
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the baseUnit field */
    private boolean baseUnit;
      
    /** The value for the valueToBase field */
    private BigDecimal valueToBase;
      
    /** The value for the alternateBase field */
    private String alternateBase;
  
    
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BaseUnit
     *
     * @return boolean
     */
    public boolean getBaseUnit()
    {
        return baseUnit;
    }

                        
    /**
     * Set the value of BaseUnit
     *
     * @param v new value
     */
    public void setBaseUnit(boolean v) 
    {
    
                  if (this.baseUnit != v)
              {
            this.baseUnit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ValueToBase
     *
     * @return BigDecimal
     */
    public BigDecimal getValueToBase()
    {
        return valueToBase;
    }

                        
    /**
     * Set the value of ValueToBase
     *
     * @param v new value
     */
    public void setValueToBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.valueToBase, v))
              {
            this.valueToBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AlternateBase
     *
     * @return String
     */
    public String getAlternateBase()
    {
        return alternateBase;
    }

                        
    /**
     * Set the value of AlternateBase
     *
     * @param v new value
     */
    public void setAlternateBase(String v) 
    {
    
                  if (!ObjectUtils.equals(this.alternateBase, v))
              {
            this.alternateBase = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("Description");
              fieldNames.add("BaseUnit");
              fieldNames.add("ValueToBase");
              fieldNames.add("AlternateBase");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("BaseUnit"))
        {
                return Boolean.valueOf(getBaseUnit());
            }
          if (name.equals("ValueToBase"))
        {
                return getValueToBase();
            }
          if (name.equals("AlternateBase"))
        {
                return getAlternateBase();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(UnitPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(UnitPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(UnitPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(UnitPeer.BASE_UNIT))
        {
                return Boolean.valueOf(getBaseUnit());
            }
          if (name.equals(UnitPeer.VALUE_TO_BASE))
        {
                return getValueToBase();
            }
          if (name.equals(UnitPeer.ALTERNATE_BASE))
        {
                return getAlternateBase();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getUnitId();
            }
              if (pos == 1)
        {
                return getUnitCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return Boolean.valueOf(getBaseUnit());
            }
              if (pos == 4)
        {
                return getValueToBase();
            }
              if (pos == 5)
        {
                return getAlternateBase();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(UnitPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        UnitPeer.doInsert((Unit) this, con);
                        setNew(false);
                    }
                    else
                    {
                        UnitPeer.doUpdate((Unit) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key unitId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setUnitId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setUnitId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getUnitId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Unit copy() throws TorqueException
    {
        return copyInto(new Unit());
    }
  
    protected Unit copyInto(Unit copyObj) throws TorqueException
    {
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setDescription(description);
          copyObj.setBaseUnit(baseUnit);
          copyObj.setValueToBase(valueToBase);
          copyObj.setAlternateBase(alternateBase);
  
                    copyObj.setUnitId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public UnitPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Unit\n");
        str.append("----\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("BaseUnit             : ")
           .append(getBaseUnit())
           .append("\n")
           .append("ValueToBase          : ")
           .append(getValueToBase())
           .append("\n")
           .append("AlternateBase        : ")
           .append(getAlternateBase())
           .append("\n")
        ;
        return(str.toString());
    }
}
