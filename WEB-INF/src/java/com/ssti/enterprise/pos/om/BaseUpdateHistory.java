package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to UpdateHistory
 */
public abstract class BaseUpdateHistory extends BaseObject
{
    /** The Peer class */
    private static final UpdateHistoryPeer peer =
        new UpdateHistoryPeer();

        
    /** The value for the historyId field */
    private String historyId;
      
    /** The value for the masterType field */
    private String masterType;
      
    /** The value for the masterId field */
    private String masterId;
      
    /** The value for the masterCode field */
    private String masterCode;
      
    /** The value for the updatePart field */
    private String updatePart;
      
    /** The value for the updateDesc field */
    private String updateDesc;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the userName field */
    private String userName;
                                                
    /** The value for the oldValue field */
    private String oldValue = "";
                                                
    /** The value for the newValue field */
    private String newValue = "";
  
    
    /**
     * Get the HistoryId
     *
     * @return String
     */
    public String getHistoryId()
    {
        return historyId;
    }

                        
    /**
     * Set the value of HistoryId
     *
     * @param v new value
     */
    public void setHistoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.historyId, v))
              {
            this.historyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MasterType
     *
     * @return String
     */
    public String getMasterType()
    {
        return masterType;
    }

                        
    /**
     * Set the value of MasterType
     *
     * @param v new value
     */
    public void setMasterType(String v) 
    {
    
                  if (!ObjectUtils.equals(this.masterType, v))
              {
            this.masterType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MasterId
     *
     * @return String
     */
    public String getMasterId()
    {
        return masterId;
    }

                        
    /**
     * Set the value of MasterId
     *
     * @param v new value
     */
    public void setMasterId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.masterId, v))
              {
            this.masterId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MasterCode
     *
     * @return String
     */
    public String getMasterCode()
    {
        return masterCode;
    }

                        
    /**
     * Set the value of MasterCode
     *
     * @param v new value
     */
    public void setMasterCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.masterCode, v))
              {
            this.masterCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdatePart
     *
     * @return String
     */
    public String getUpdatePart()
    {
        return updatePart;
    }

                        
    /**
     * Set the value of UpdatePart
     *
     * @param v new value
     */
    public void setUpdatePart(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updatePart, v))
              {
            this.updatePart = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDesc
     *
     * @return String
     */
    public String getUpdateDesc()
    {
        return updateDesc;
    }

                        
    /**
     * Set the value of UpdateDesc
     *
     * @param v new value
     */
    public void setUpdateDesc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDesc, v))
              {
            this.updateDesc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OldValue
     *
     * @return String
     */
    public String getOldValue()
    {
        return oldValue;
    }

                        
    /**
     * Set the value of OldValue
     *
     * @param v new value
     */
    public void setOldValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.oldValue, v))
              {
            this.oldValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NewValue
     *
     * @return String
     */
    public String getNewValue()
    {
        return newValue;
    }

                        
    /**
     * Set the value of NewValue
     *
     * @param v new value
     */
    public void setNewValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.newValue, v))
              {
            this.newValue = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("HistoryId");
              fieldNames.add("MasterType");
              fieldNames.add("MasterId");
              fieldNames.add("MasterCode");
              fieldNames.add("UpdatePart");
              fieldNames.add("UpdateDesc");
              fieldNames.add("UpdateDate");
              fieldNames.add("UserName");
              fieldNames.add("OldValue");
              fieldNames.add("NewValue");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("HistoryId"))
        {
                return getHistoryId();
            }
          if (name.equals("MasterType"))
        {
                return getMasterType();
            }
          if (name.equals("MasterId"))
        {
                return getMasterId();
            }
          if (name.equals("MasterCode"))
        {
                return getMasterCode();
            }
          if (name.equals("UpdatePart"))
        {
                return getUpdatePart();
            }
          if (name.equals("UpdateDesc"))
        {
                return getUpdateDesc();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("OldValue"))
        {
                return getOldValue();
            }
          if (name.equals("NewValue"))
        {
                return getNewValue();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(UpdateHistoryPeer.HISTORY_ID))
        {
                return getHistoryId();
            }
          if (name.equals(UpdateHistoryPeer.MASTER_TYPE))
        {
                return getMasterType();
            }
          if (name.equals(UpdateHistoryPeer.MASTER_ID))
        {
                return getMasterId();
            }
          if (name.equals(UpdateHistoryPeer.MASTER_CODE))
        {
                return getMasterCode();
            }
          if (name.equals(UpdateHistoryPeer.UPDATE_PART))
        {
                return getUpdatePart();
            }
          if (name.equals(UpdateHistoryPeer.UPDATE_DESC))
        {
                return getUpdateDesc();
            }
          if (name.equals(UpdateHistoryPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(UpdateHistoryPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(UpdateHistoryPeer.OLD_VALUE))
        {
                return getOldValue();
            }
          if (name.equals(UpdateHistoryPeer.NEW_VALUE))
        {
                return getNewValue();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getHistoryId();
            }
              if (pos == 1)
        {
                return getMasterType();
            }
              if (pos == 2)
        {
                return getMasterId();
            }
              if (pos == 3)
        {
                return getMasterCode();
            }
              if (pos == 4)
        {
                return getUpdatePart();
            }
              if (pos == 5)
        {
                return getUpdateDesc();
            }
              if (pos == 6)
        {
                return getUpdateDate();
            }
              if (pos == 7)
        {
                return getUserName();
            }
              if (pos == 8)
        {
                return getOldValue();
            }
              if (pos == 9)
        {
                return getNewValue();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(UpdateHistoryPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        UpdateHistoryPeer.doInsert((UpdateHistory) this, con);
                        setNew(false);
                    }
                    else
                    {
                        UpdateHistoryPeer.doUpdate((UpdateHistory) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key historyId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setHistoryId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setHistoryId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getHistoryId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public UpdateHistory copy() throws TorqueException
    {
        return copyInto(new UpdateHistory());
    }
  
    protected UpdateHistory copyInto(UpdateHistory copyObj) throws TorqueException
    {
          copyObj.setHistoryId(historyId);
          copyObj.setMasterType(masterType);
          copyObj.setMasterId(masterId);
          copyObj.setMasterCode(masterCode);
          copyObj.setUpdatePart(updatePart);
          copyObj.setUpdateDesc(updateDesc);
          copyObj.setUpdateDate(updateDate);
          copyObj.setUserName(userName);
          copyObj.setOldValue(oldValue);
          copyObj.setNewValue(newValue);
  
                    copyObj.setHistoryId((String)null);
                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public UpdateHistoryPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("UpdateHistory\n");
        str.append("-------------\n")
           .append("HistoryId            : ")
           .append(getHistoryId())
           .append("\n")
           .append("MasterType           : ")
           .append(getMasterType())
           .append("\n")
           .append("MasterId             : ")
           .append(getMasterId())
           .append("\n")
           .append("MasterCode           : ")
           .append(getMasterCode())
           .append("\n")
           .append("UpdatePart           : ")
           .append(getUpdatePart())
           .append("\n")
           .append("UpdateDesc           : ")
           .append(getUpdateDesc())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("OldValue             : ")
           .append(getOldValue())
           .append("\n")
           .append("NewValue             : ")
           .append(getNewValue())
           .append("\n")
        ;
        return(str.toString());
    }
}
