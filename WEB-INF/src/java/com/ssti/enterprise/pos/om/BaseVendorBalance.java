package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to VendorBalance
 */
public abstract class BaseVendorBalance extends BaseObject
{
    /** The Peer class */
    private static final VendorBalancePeer peer =
        new VendorBalancePeer();

        
    /** The value for the vendorBalanceId field */
    private String vendorBalanceId;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the apBalance field */
    private BigDecimal apBalance;
      
    /** The value for the primeBalance field */
    private BigDecimal primeBalance;
  
    
    /**
     * Get the VendorBalanceId
     *
     * @return String
     */
    public String getVendorBalanceId()
    {
        return vendorBalanceId;
    }

                        
    /**
     * Set the value of VendorBalanceId
     *
     * @param v new value
     */
    public void setVendorBalanceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorBalanceId, v))
              {
            this.vendorBalanceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getApBalance()
    {
        return apBalance;
    }

                        
    /**
     * Set the value of ApBalance
     *
     * @param v new value
     */
    public void setApBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.apBalance, v))
              {
            this.apBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrimeBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getPrimeBalance()
    {
        return primeBalance;
    }

                        
    /**
     * Set the value of PrimeBalance
     *
     * @param v new value
     */
    public void setPrimeBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.primeBalance, v))
              {
            this.primeBalance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VendorBalanceId");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("ApBalance");
              fieldNames.add("PrimeBalance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VendorBalanceId"))
        {
                return getVendorBalanceId();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("ApBalance"))
        {
                return getApBalance();
            }
          if (name.equals("PrimeBalance"))
        {
                return getPrimeBalance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VendorBalancePeer.VENDOR_BALANCE_ID))
        {
                return getVendorBalanceId();
            }
          if (name.equals(VendorBalancePeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(VendorBalancePeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(VendorBalancePeer.AP_BALANCE))
        {
                return getApBalance();
            }
          if (name.equals(VendorBalancePeer.PRIME_BALANCE))
        {
                return getPrimeBalance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVendorBalanceId();
            }
              if (pos == 1)
        {
                return getVendorId();
            }
              if (pos == 2)
        {
                return getVendorName();
            }
              if (pos == 3)
        {
                return getApBalance();
            }
              if (pos == 4)
        {
                return getPrimeBalance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VendorBalancePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VendorBalancePeer.doInsert((VendorBalance) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VendorBalancePeer.doUpdate((VendorBalance) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key vendorBalanceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVendorBalanceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVendorBalanceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVendorBalanceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public VendorBalance copy() throws TorqueException
    {
        return copyInto(new VendorBalance());
    }
  
    protected VendorBalance copyInto(VendorBalance copyObj) throws TorqueException
    {
          copyObj.setVendorBalanceId(vendorBalanceId);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setApBalance(apBalance);
          copyObj.setPrimeBalance(primeBalance);
  
                    copyObj.setVendorBalanceId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VendorBalancePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("VendorBalance\n");
        str.append("-------------\n")
           .append("VendorBalanceId      : ")
           .append(getVendorBalanceId())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("ApBalance            : ")
           .append(getApBalance())
           .append("\n")
           .append("PrimeBalance         : ")
           .append(getPrimeBalance())
           .append("\n")
        ;
        return(str.toString());
    }
}
