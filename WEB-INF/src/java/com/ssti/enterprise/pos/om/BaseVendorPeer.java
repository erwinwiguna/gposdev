package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.VendorMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseVendorPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "vendor";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(VendorMapBuilder.CLASS_NAME);
    }

      /** the column name for the VENDOR_ID field */
    public static final String VENDOR_ID;
      /** the column name for the VENDOR_TYPE_ID field */
    public static final String VENDOR_TYPE_ID;
      /** the column name for the VENDOR_CODE field */
    public static final String VENDOR_CODE;
      /** the column name for the VENDOR_NAME field */
    public static final String VENDOR_NAME;
      /** the column name for the TAX_NO field */
    public static final String TAX_NO;
      /** the column name for the VENDOR_STATUS field */
    public static final String VENDOR_STATUS;
      /** the column name for the CONTACT_NAME1 field */
    public static final String CONTACT_NAME1;
      /** the column name for the CONTACT_PHONE1 field */
    public static final String CONTACT_PHONE1;
      /** the column name for the JOB_TITLE1 field */
    public static final String JOB_TITLE1;
      /** the column name for the CONTACT_NAME2 field */
    public static final String CONTACT_NAME2;
      /** the column name for the CONTACT_PHONE2 field */
    public static final String CONTACT_PHONE2;
      /** the column name for the JOB_TITLE2 field */
    public static final String JOB_TITLE2;
      /** the column name for the CONTACT_NAME3 field */
    public static final String CONTACT_NAME3;
      /** the column name for the CONTACT_PHONE3 field */
    public static final String CONTACT_PHONE3;
      /** the column name for the JOB_TITLE3 field */
    public static final String JOB_TITLE3;
      /** the column name for the ADDRESS field */
    public static final String ADDRESS;
      /** the column name for the ZIP field */
    public static final String ZIP;
      /** the column name for the COUNTRY field */
    public static final String COUNTRY;
      /** the column name for the PROVINCE field */
    public static final String PROVINCE;
      /** the column name for the CITY field */
    public static final String CITY;
      /** the column name for the DISTRICT field */
    public static final String DISTRICT;
      /** the column name for the VILLAGE field */
    public static final String VILLAGE;
      /** the column name for the PHONE1 field */
    public static final String PHONE1;
      /** the column name for the PHONE2 field */
    public static final String PHONE2;
      /** the column name for the FAX field */
    public static final String FAX;
      /** the column name for the EMAIL field */
    public static final String EMAIL;
      /** the column name for the WEB_SITE field */
    public static final String WEB_SITE;
      /** the column name for the DEFAULT_TAX_ID field */
    public static final String DEFAULT_TAX_ID;
      /** the column name for the DEFAULT_TYPE_ID field */
    public static final String DEFAULT_TYPE_ID;
      /** the column name for the DEFAULT_TERM_ID field */
    public static final String DEFAULT_TERM_ID;
      /** the column name for the DEFAULT_EMPLOYEE_ID field */
    public static final String DEFAULT_EMPLOYEE_ID;
      /** the column name for the DEFAULT_CURRENCY_ID field */
    public static final String DEFAULT_CURRENCY_ID;
      /** the column name for the DEFAULT_LOCATION_ID field */
    public static final String DEFAULT_LOCATION_ID;
      /** the column name for the DEFAULT_ITEM_DISC field */
    public static final String DEFAULT_ITEM_DISC;
      /** the column name for the DEFAULT_DISCOUNT_AMOUNT field */
    public static final String DEFAULT_DISCOUNT_AMOUNT;
      /** the column name for the CREDIT_LIMIT field */
    public static final String CREDIT_LIMIT;
      /** the column name for the MEMO field */
    public static final String MEMO;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the LAST_UPDATE_LOCATION_ID field */
    public static final String LAST_UPDATE_LOCATION_ID;
      /** the column name for the FIELD1 field */
    public static final String FIELD1;
      /** the column name for the FIELD2 field */
    public static final String FIELD2;
      /** the column name for the VALUE1 field */
    public static final String VALUE1;
      /** the column name for the VALUE2 field */
    public static final String VALUE2;
      /** the column name for the AP_ACCOUNT field */
    public static final String AP_ACCOUNT;
      /** the column name for the OPENING_BALANCE field */
    public static final String OPENING_BALANCE;
      /** the column name for the AS_DATE field */
    public static final String AS_DATE;
      /** the column name for the OB_TRANS_ID field */
    public static final String OB_TRANS_ID;
      /** the column name for the OB_RATE field */
    public static final String OB_RATE;
      /** the column name for the SALES_AREA_ID field */
    public static final String SALES_AREA_ID;
      /** the column name for the TERRITORY_ID field */
    public static final String TERRITORY_ID;
      /** the column name for the LONGITUDES field */
    public static final String LONGITUDES;
      /** the column name for the LATITUDES field */
    public static final String LATITUDES;
      /** the column name for the TRANS_PREFIX field */
    public static final String TRANS_PREFIX;
      /** the column name for the CREATE_BY field */
    public static final String CREATE_BY;
      /** the column name for the LAST_UPDATE_BY field */
    public static final String LAST_UPDATE_BY;
      /** the column name for the ALLOW_RETURN field */
    public static final String ALLOW_RETURN;
      /** the column name for the ITEM_TYPE field */
    public static final String ITEM_TYPE;
      /** the column name for the CONSIGN_DISC field */
    public static final String CONSIGN_DISC;
      /** the column name for the CONSIGN_FIELD field */
    public static final String CONSIGN_FIELD;
      /** the column name for the PAYMENT_ACC_BANK field */
    public static final String PAYMENT_ACC_BANK;
      /** the column name for the PAYMENT_ACC_NO field */
    public static final String PAYMENT_ACC_NO;
      /** the column name for the PAYMENT_ACC_NAME field */
    public static final String PAYMENT_ACC_NAME;
      /** the column name for the PAYMENT_EMAIL_TO field */
    public static final String PAYMENT_EMAIL_TO;
  
    static
    {
          VENDOR_ID = "vendor.VENDOR_ID";
          VENDOR_TYPE_ID = "vendor.VENDOR_TYPE_ID";
          VENDOR_CODE = "vendor.VENDOR_CODE";
          VENDOR_NAME = "vendor.VENDOR_NAME";
          TAX_NO = "vendor.TAX_NO";
          VENDOR_STATUS = "vendor.VENDOR_STATUS";
          CONTACT_NAME1 = "vendor.CONTACT_NAME1";
          CONTACT_PHONE1 = "vendor.CONTACT_PHONE1";
          JOB_TITLE1 = "vendor.JOB_TITLE1";
          CONTACT_NAME2 = "vendor.CONTACT_NAME2";
          CONTACT_PHONE2 = "vendor.CONTACT_PHONE2";
          JOB_TITLE2 = "vendor.JOB_TITLE2";
          CONTACT_NAME3 = "vendor.CONTACT_NAME3";
          CONTACT_PHONE3 = "vendor.CONTACT_PHONE3";
          JOB_TITLE3 = "vendor.JOB_TITLE3";
          ADDRESS = "vendor.ADDRESS";
          ZIP = "vendor.ZIP";
          COUNTRY = "vendor.COUNTRY";
          PROVINCE = "vendor.PROVINCE";
          CITY = "vendor.CITY";
          DISTRICT = "vendor.DISTRICT";
          VILLAGE = "vendor.VILLAGE";
          PHONE1 = "vendor.PHONE1";
          PHONE2 = "vendor.PHONE2";
          FAX = "vendor.FAX";
          EMAIL = "vendor.EMAIL";
          WEB_SITE = "vendor.WEB_SITE";
          DEFAULT_TAX_ID = "vendor.DEFAULT_TAX_ID";
          DEFAULT_TYPE_ID = "vendor.DEFAULT_TYPE_ID";
          DEFAULT_TERM_ID = "vendor.DEFAULT_TERM_ID";
          DEFAULT_EMPLOYEE_ID = "vendor.DEFAULT_EMPLOYEE_ID";
          DEFAULT_CURRENCY_ID = "vendor.DEFAULT_CURRENCY_ID";
          DEFAULT_LOCATION_ID = "vendor.DEFAULT_LOCATION_ID";
          DEFAULT_ITEM_DISC = "vendor.DEFAULT_ITEM_DISC";
          DEFAULT_DISCOUNT_AMOUNT = "vendor.DEFAULT_DISCOUNT_AMOUNT";
          CREDIT_LIMIT = "vendor.CREDIT_LIMIT";
          MEMO = "vendor.MEMO";
          UPDATE_DATE = "vendor.UPDATE_DATE";
          LAST_UPDATE_LOCATION_ID = "vendor.LAST_UPDATE_LOCATION_ID";
          FIELD1 = "vendor.FIELD1";
          FIELD2 = "vendor.FIELD2";
          VALUE1 = "vendor.VALUE1";
          VALUE2 = "vendor.VALUE2";
          AP_ACCOUNT = "vendor.AP_ACCOUNT";
          OPENING_BALANCE = "vendor.OPENING_BALANCE";
          AS_DATE = "vendor.AS_DATE";
          OB_TRANS_ID = "vendor.OB_TRANS_ID";
          OB_RATE = "vendor.OB_RATE";
          SALES_AREA_ID = "vendor.SALES_AREA_ID";
          TERRITORY_ID = "vendor.TERRITORY_ID";
          LONGITUDES = "vendor.LONGITUDES";
          LATITUDES = "vendor.LATITUDES";
          TRANS_PREFIX = "vendor.TRANS_PREFIX";
          CREATE_BY = "vendor.CREATE_BY";
          LAST_UPDATE_BY = "vendor.LAST_UPDATE_BY";
          ALLOW_RETURN = "vendor.ALLOW_RETURN";
          ITEM_TYPE = "vendor.ITEM_TYPE";
          CONSIGN_DISC = "vendor.CONSIGN_DISC";
          CONSIGN_FIELD = "vendor.CONSIGN_FIELD";
          PAYMENT_ACC_BANK = "vendor.PAYMENT_ACC_BANK";
          PAYMENT_ACC_NO = "vendor.PAYMENT_ACC_NO";
          PAYMENT_ACC_NAME = "vendor.PAYMENT_ACC_NAME";
          PAYMENT_EMAIL_TO = "vendor.PAYMENT_EMAIL_TO";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(VendorMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(VendorMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  63;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Vendor";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseVendorPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                                                                                                                                                                                                            
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(VENDOR_ID);
          criteria.addSelectColumn(VENDOR_TYPE_ID);
          criteria.addSelectColumn(VENDOR_CODE);
          criteria.addSelectColumn(VENDOR_NAME);
          criteria.addSelectColumn(TAX_NO);
          criteria.addSelectColumn(VENDOR_STATUS);
          criteria.addSelectColumn(CONTACT_NAME1);
          criteria.addSelectColumn(CONTACT_PHONE1);
          criteria.addSelectColumn(JOB_TITLE1);
          criteria.addSelectColumn(CONTACT_NAME2);
          criteria.addSelectColumn(CONTACT_PHONE2);
          criteria.addSelectColumn(JOB_TITLE2);
          criteria.addSelectColumn(CONTACT_NAME3);
          criteria.addSelectColumn(CONTACT_PHONE3);
          criteria.addSelectColumn(JOB_TITLE3);
          criteria.addSelectColumn(ADDRESS);
          criteria.addSelectColumn(ZIP);
          criteria.addSelectColumn(COUNTRY);
          criteria.addSelectColumn(PROVINCE);
          criteria.addSelectColumn(CITY);
          criteria.addSelectColumn(DISTRICT);
          criteria.addSelectColumn(VILLAGE);
          criteria.addSelectColumn(PHONE1);
          criteria.addSelectColumn(PHONE2);
          criteria.addSelectColumn(FAX);
          criteria.addSelectColumn(EMAIL);
          criteria.addSelectColumn(WEB_SITE);
          criteria.addSelectColumn(DEFAULT_TAX_ID);
          criteria.addSelectColumn(DEFAULT_TYPE_ID);
          criteria.addSelectColumn(DEFAULT_TERM_ID);
          criteria.addSelectColumn(DEFAULT_EMPLOYEE_ID);
          criteria.addSelectColumn(DEFAULT_CURRENCY_ID);
          criteria.addSelectColumn(DEFAULT_LOCATION_ID);
          criteria.addSelectColumn(DEFAULT_ITEM_DISC);
          criteria.addSelectColumn(DEFAULT_DISCOUNT_AMOUNT);
          criteria.addSelectColumn(CREDIT_LIMIT);
          criteria.addSelectColumn(MEMO);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(LAST_UPDATE_LOCATION_ID);
          criteria.addSelectColumn(FIELD1);
          criteria.addSelectColumn(FIELD2);
          criteria.addSelectColumn(VALUE1);
          criteria.addSelectColumn(VALUE2);
          criteria.addSelectColumn(AP_ACCOUNT);
          criteria.addSelectColumn(OPENING_BALANCE);
          criteria.addSelectColumn(AS_DATE);
          criteria.addSelectColumn(OB_TRANS_ID);
          criteria.addSelectColumn(OB_RATE);
          criteria.addSelectColumn(SALES_AREA_ID);
          criteria.addSelectColumn(TERRITORY_ID);
          criteria.addSelectColumn(LONGITUDES);
          criteria.addSelectColumn(LATITUDES);
          criteria.addSelectColumn(TRANS_PREFIX);
          criteria.addSelectColumn(CREATE_BY);
          criteria.addSelectColumn(LAST_UPDATE_BY);
          criteria.addSelectColumn(ALLOW_RETURN);
          criteria.addSelectColumn(ITEM_TYPE);
          criteria.addSelectColumn(CONSIGN_DISC);
          criteria.addSelectColumn(CONSIGN_FIELD);
          criteria.addSelectColumn(PAYMENT_ACC_BANK);
          criteria.addSelectColumn(PAYMENT_ACC_NO);
          criteria.addSelectColumn(PAYMENT_ACC_NAME);
          criteria.addSelectColumn(PAYMENT_EMAIL_TO);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Vendor row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Vendor obj = (Vendor) cls.newInstance();
            VendorPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Vendor obj)
        throws TorqueException
    {
        try
        {
                obj.setVendorId(row.getValue(offset + 0).asString());
                  obj.setVendorTypeId(row.getValue(offset + 1).asString());
                  obj.setVendorCode(row.getValue(offset + 2).asString());
                  obj.setVendorName(row.getValue(offset + 3).asString());
                  obj.setTaxNo(row.getValue(offset + 4).asString());
                  obj.setVendorStatus(row.getValue(offset + 5).asInt());
                  obj.setContactName1(row.getValue(offset + 6).asString());
                  obj.setContactPhone1(row.getValue(offset + 7).asString());
                  obj.setJobTitle1(row.getValue(offset + 8).asString());
                  obj.setContactName2(row.getValue(offset + 9).asString());
                  obj.setContactPhone2(row.getValue(offset + 10).asString());
                  obj.setJobTitle2(row.getValue(offset + 11).asString());
                  obj.setContactName3(row.getValue(offset + 12).asString());
                  obj.setContactPhone3(row.getValue(offset + 13).asString());
                  obj.setJobTitle3(row.getValue(offset + 14).asString());
                  obj.setAddress(row.getValue(offset + 15).asString());
                  obj.setZip(row.getValue(offset + 16).asString());
                  obj.setCountry(row.getValue(offset + 17).asString());
                  obj.setProvince(row.getValue(offset + 18).asString());
                  obj.setCity(row.getValue(offset + 19).asString());
                  obj.setDistrict(row.getValue(offset + 20).asString());
                  obj.setVillage(row.getValue(offset + 21).asString());
                  obj.setPhone1(row.getValue(offset + 22).asString());
                  obj.setPhone2(row.getValue(offset + 23).asString());
                  obj.setFax(row.getValue(offset + 24).asString());
                  obj.setEmail(row.getValue(offset + 25).asString());
                  obj.setWebSite(row.getValue(offset + 26).asString());
                  obj.setDefaultTaxId(row.getValue(offset + 27).asString());
                  obj.setDefaultTypeId(row.getValue(offset + 28).asString());
                  obj.setDefaultTermId(row.getValue(offset + 29).asString());
                  obj.setDefaultEmployeeId(row.getValue(offset + 30).asString());
                  obj.setDefaultCurrencyId(row.getValue(offset + 31).asString());
                  obj.setDefaultLocationId(row.getValue(offset + 32).asString());
                  obj.setDefaultItemDisc(row.getValue(offset + 33).asString());
                  obj.setDefaultDiscountAmount(row.getValue(offset + 34).asString());
                  obj.setCreditLimit(row.getValue(offset + 35).asBigDecimal());
                  obj.setMemo(row.getValue(offset + 36).asString());
                  obj.setUpdateDate(row.getValue(offset + 37).asUtilDate());
                  obj.setLastUpdateLocationId(row.getValue(offset + 38).asString());
                  obj.setField1(row.getValue(offset + 39).asString());
                  obj.setField2(row.getValue(offset + 40).asString());
                  obj.setValue1(row.getValue(offset + 41).asString());
                  obj.setValue2(row.getValue(offset + 42).asString());
                  obj.setApAccount(row.getValue(offset + 43).asString());
                  obj.setOpeningBalance(row.getValue(offset + 44).asBigDecimal());
                  obj.setAsDate(row.getValue(offset + 45).asUtilDate());
                  obj.setObTransId(row.getValue(offset + 46).asString());
                  obj.setObRate(row.getValue(offset + 47).asBigDecimal());
                  obj.setSalesAreaId(row.getValue(offset + 48).asString());
                  obj.setTerritoryId(row.getValue(offset + 49).asString());
                  obj.setLongitudes(row.getValue(offset + 50).asBigDecimal());
                  obj.setLatitudes(row.getValue(offset + 51).asBigDecimal());
                  obj.setTransPrefix(row.getValue(offset + 52).asString());
                  obj.setCreateBy(row.getValue(offset + 53).asString());
                  obj.setLastUpdateBy(row.getValue(offset + 54).asString());
                  obj.setAllowReturn(row.getValue(offset + 55).asInt());
                  obj.setItemType(row.getValue(offset + 56).asInt());
                  obj.setConsignDisc(row.getValue(offset + 57).asString());
                  obj.setConsignField(row.getValue(offset + 58).asString());
                  obj.setPaymentAccBank(row.getValue(offset + 59).asString());
                  obj.setPaymentAccNo(row.getValue(offset + 60).asString());
                  obj.setPaymentAccName(row.getValue(offset + 61).asString());
                  obj.setPaymentEmailTo(row.getValue(offset + 62).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseVendorPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                                                                                                                                                                                                            
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(VendorPeer.row2Object(row, 1,
                VendorPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseVendorPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(VENDOR_ID, criteria.remove(VENDOR_ID));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         VendorPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                                                                                                                                                                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Vendor obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Vendor obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Vendor obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Vendor obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Vendor) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Vendor obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Vendor) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Vendor obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Vendor) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Vendor obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseVendorPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(VENDOR_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Vendor obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(VENDOR_ID, obj.getVendorId());
              criteria.add(VENDOR_TYPE_ID, obj.getVendorTypeId());
              criteria.add(VENDOR_CODE, obj.getVendorCode());
              criteria.add(VENDOR_NAME, obj.getVendorName());
              criteria.add(TAX_NO, obj.getTaxNo());
              criteria.add(VENDOR_STATUS, obj.getVendorStatus());
              criteria.add(CONTACT_NAME1, obj.getContactName1());
              criteria.add(CONTACT_PHONE1, obj.getContactPhone1());
              criteria.add(JOB_TITLE1, obj.getJobTitle1());
              criteria.add(CONTACT_NAME2, obj.getContactName2());
              criteria.add(CONTACT_PHONE2, obj.getContactPhone2());
              criteria.add(JOB_TITLE2, obj.getJobTitle2());
              criteria.add(CONTACT_NAME3, obj.getContactName3());
              criteria.add(CONTACT_PHONE3, obj.getContactPhone3());
              criteria.add(JOB_TITLE3, obj.getJobTitle3());
              criteria.add(ADDRESS, obj.getAddress());
              criteria.add(ZIP, obj.getZip());
              criteria.add(COUNTRY, obj.getCountry());
              criteria.add(PROVINCE, obj.getProvince());
              criteria.add(CITY, obj.getCity());
              criteria.add(DISTRICT, obj.getDistrict());
              criteria.add(VILLAGE, obj.getVillage());
              criteria.add(PHONE1, obj.getPhone1());
              criteria.add(PHONE2, obj.getPhone2());
              criteria.add(FAX, obj.getFax());
              criteria.add(EMAIL, obj.getEmail());
              criteria.add(WEB_SITE, obj.getWebSite());
              criteria.add(DEFAULT_TAX_ID, obj.getDefaultTaxId());
              criteria.add(DEFAULT_TYPE_ID, obj.getDefaultTypeId());
              criteria.add(DEFAULT_TERM_ID, obj.getDefaultTermId());
              criteria.add(DEFAULT_EMPLOYEE_ID, obj.getDefaultEmployeeId());
              criteria.add(DEFAULT_CURRENCY_ID, obj.getDefaultCurrencyId());
              criteria.add(DEFAULT_LOCATION_ID, obj.getDefaultLocationId());
              criteria.add(DEFAULT_ITEM_DISC, obj.getDefaultItemDisc());
              criteria.add(DEFAULT_DISCOUNT_AMOUNT, obj.getDefaultDiscountAmount());
              criteria.add(CREDIT_LIMIT, obj.getCreditLimit());
              criteria.add(MEMO, obj.getMemo());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(LAST_UPDATE_LOCATION_ID, obj.getLastUpdateLocationId());
              criteria.add(FIELD1, obj.getField1());
              criteria.add(FIELD2, obj.getField2());
              criteria.add(VALUE1, obj.getValue1());
              criteria.add(VALUE2, obj.getValue2());
              criteria.add(AP_ACCOUNT, obj.getApAccount());
              criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
              criteria.add(AS_DATE, obj.getAsDate());
              criteria.add(OB_TRANS_ID, obj.getObTransId());
              criteria.add(OB_RATE, obj.getObRate());
              criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
              criteria.add(TERRITORY_ID, obj.getTerritoryId());
              criteria.add(LONGITUDES, obj.getLongitudes());
              criteria.add(LATITUDES, obj.getLatitudes());
              criteria.add(TRANS_PREFIX, obj.getTransPrefix());
              criteria.add(CREATE_BY, obj.getCreateBy());
              criteria.add(LAST_UPDATE_BY, obj.getLastUpdateBy());
              criteria.add(ALLOW_RETURN, obj.getAllowReturn());
              criteria.add(ITEM_TYPE, obj.getItemType());
              criteria.add(CONSIGN_DISC, obj.getConsignDisc());
              criteria.add(CONSIGN_FIELD, obj.getConsignField());
              criteria.add(PAYMENT_ACC_BANK, obj.getPaymentAccBank());
              criteria.add(PAYMENT_ACC_NO, obj.getPaymentAccNo());
              criteria.add(PAYMENT_ACC_NAME, obj.getPaymentAccName());
              criteria.add(PAYMENT_EMAIL_TO, obj.getPaymentEmailTo());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Vendor obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(VENDOR_ID, obj.getVendorId());
                          criteria.add(VENDOR_TYPE_ID, obj.getVendorTypeId());
                          criteria.add(VENDOR_CODE, obj.getVendorCode());
                          criteria.add(VENDOR_NAME, obj.getVendorName());
                          criteria.add(TAX_NO, obj.getTaxNo());
                          criteria.add(VENDOR_STATUS, obj.getVendorStatus());
                          criteria.add(CONTACT_NAME1, obj.getContactName1());
                          criteria.add(CONTACT_PHONE1, obj.getContactPhone1());
                          criteria.add(JOB_TITLE1, obj.getJobTitle1());
                          criteria.add(CONTACT_NAME2, obj.getContactName2());
                          criteria.add(CONTACT_PHONE2, obj.getContactPhone2());
                          criteria.add(JOB_TITLE2, obj.getJobTitle2());
                          criteria.add(CONTACT_NAME3, obj.getContactName3());
                          criteria.add(CONTACT_PHONE3, obj.getContactPhone3());
                          criteria.add(JOB_TITLE3, obj.getJobTitle3());
                          criteria.add(ADDRESS, obj.getAddress());
                          criteria.add(ZIP, obj.getZip());
                          criteria.add(COUNTRY, obj.getCountry());
                          criteria.add(PROVINCE, obj.getProvince());
                          criteria.add(CITY, obj.getCity());
                          criteria.add(DISTRICT, obj.getDistrict());
                          criteria.add(VILLAGE, obj.getVillage());
                          criteria.add(PHONE1, obj.getPhone1());
                          criteria.add(PHONE2, obj.getPhone2());
                          criteria.add(FAX, obj.getFax());
                          criteria.add(EMAIL, obj.getEmail());
                          criteria.add(WEB_SITE, obj.getWebSite());
                          criteria.add(DEFAULT_TAX_ID, obj.getDefaultTaxId());
                          criteria.add(DEFAULT_TYPE_ID, obj.getDefaultTypeId());
                          criteria.add(DEFAULT_TERM_ID, obj.getDefaultTermId());
                          criteria.add(DEFAULT_EMPLOYEE_ID, obj.getDefaultEmployeeId());
                          criteria.add(DEFAULT_CURRENCY_ID, obj.getDefaultCurrencyId());
                          criteria.add(DEFAULT_LOCATION_ID, obj.getDefaultLocationId());
                          criteria.add(DEFAULT_ITEM_DISC, obj.getDefaultItemDisc());
                          criteria.add(DEFAULT_DISCOUNT_AMOUNT, obj.getDefaultDiscountAmount());
                          criteria.add(CREDIT_LIMIT, obj.getCreditLimit());
                          criteria.add(MEMO, obj.getMemo());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(LAST_UPDATE_LOCATION_ID, obj.getLastUpdateLocationId());
                          criteria.add(FIELD1, obj.getField1());
                          criteria.add(FIELD2, obj.getField2());
                          criteria.add(VALUE1, obj.getValue1());
                          criteria.add(VALUE2, obj.getValue2());
                          criteria.add(AP_ACCOUNT, obj.getApAccount());
                          criteria.add(OPENING_BALANCE, obj.getOpeningBalance());
                          criteria.add(AS_DATE, obj.getAsDate());
                          criteria.add(OB_TRANS_ID, obj.getObTransId());
                          criteria.add(OB_RATE, obj.getObRate());
                          criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
                          criteria.add(TERRITORY_ID, obj.getTerritoryId());
                          criteria.add(LONGITUDES, obj.getLongitudes());
                          criteria.add(LATITUDES, obj.getLatitudes());
                          criteria.add(TRANS_PREFIX, obj.getTransPrefix());
                          criteria.add(CREATE_BY, obj.getCreateBy());
                          criteria.add(LAST_UPDATE_BY, obj.getLastUpdateBy());
                          criteria.add(ALLOW_RETURN, obj.getAllowReturn());
                          criteria.add(ITEM_TYPE, obj.getItemType());
                          criteria.add(CONSIGN_DISC, obj.getConsignDisc());
                          criteria.add(CONSIGN_FIELD, obj.getConsignField());
                          criteria.add(PAYMENT_ACC_BANK, obj.getPaymentAccBank());
                          criteria.add(PAYMENT_ACC_NO, obj.getPaymentAccNo());
                          criteria.add(PAYMENT_ACC_NAME, obj.getPaymentAccName());
                          criteria.add(PAYMENT_EMAIL_TO, obj.getPaymentEmailTo());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Vendor retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Vendor retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Vendor retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Vendor retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Vendor retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Vendor)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( VENDOR_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
