package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to VendorPriceList
 */
public abstract class BaseVendorPriceList extends BaseObject
{
    /** The Peer class */
    private static final VendorPriceListPeer peer =
        new VendorPriceListPeer();

        
    /** The value for the vendorPriceListId field */
    private String vendorPriceListId;
      
    /** The value for the vendorPlCode field */
    private String vendorPlCode;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the expiredDate field */
    private Date expiredDate;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the VendorPriceListId
     *
     * @return String
     */
    public String getVendorPriceListId()
    {
        return vendorPriceListId;
    }

                                              
    /**
     * Set the value of VendorPriceListId
     *
     * @param v new value
     */
    public void setVendorPriceListId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.vendorPriceListId, v))
              {
            this.vendorPriceListId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated VendorPriceListDetail
        if (collVendorPriceListDetails != null)
        {
            for (int i = 0; i < collVendorPriceListDetails.size(); i++)
            {
                ((VendorPriceListDetail) collVendorPriceListDetails.get(i))
                    .setVendorPriceListId(v);
            }
        }
                                }
  
    /**
     * Get the VendorPlCode
     *
     * @return String
     */
    public String getVendorPlCode()
    {
        return vendorPlCode;
    }

                        
    /**
     * Set the value of VendorPlCode
     *
     * @param v new value
     */
    public void setVendorPlCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorPlCode, v))
              {
            this.vendorPlCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpiredDate
     *
     * @return Date
     */
    public Date getExpiredDate()
    {
        return expiredDate;
    }

                        
    /**
     * Set the value of ExpiredDate
     *
     * @param v new value
     */
    public void setExpiredDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expiredDate, v))
              {
            this.expiredDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collVendorPriceListDetails
     */
    protected List collVendorPriceListDetails;

    /**
     * Temporary storage of collVendorPriceListDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initVendorPriceListDetails()
    {
        if (collVendorPriceListDetails == null)
        {
            collVendorPriceListDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a VendorPriceListDetail object to this object
     * through the VendorPriceListDetail foreign key attribute
     *
     * @param l VendorPriceListDetail
     * @throws TorqueException
     */
    public void addVendorPriceListDetail(VendorPriceListDetail l) throws TorqueException
    {
        getVendorPriceListDetails().add(l);
        l.setVendorPriceList((VendorPriceList) this);
    }

    /**
     * The criteria used to select the current contents of collVendorPriceListDetails
     */
    private Criteria lastVendorPriceListDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getVendorPriceListDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getVendorPriceListDetails() throws TorqueException
    {
              if (collVendorPriceListDetails == null)
        {
            collVendorPriceListDetails = getVendorPriceListDetails(new Criteria(10));
        }
        return collVendorPriceListDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this VendorPriceList has previously
     * been saved, it will retrieve related VendorPriceListDetails from storage.
     * If this VendorPriceList is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getVendorPriceListDetails(Criteria criteria) throws TorqueException
    {
              if (collVendorPriceListDetails == null)
        {
            if (isNew())
            {
               collVendorPriceListDetails = new ArrayList();
            }
            else
            {
                        criteria.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, getVendorPriceListId() );
                        collVendorPriceListDetails = VendorPriceListDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, getVendorPriceListId());
                            if (!lastVendorPriceListDetailsCriteria.equals(criteria))
                {
                    collVendorPriceListDetails = VendorPriceListDetailPeer.doSelect(criteria);
                }
            }
        }
        lastVendorPriceListDetailsCriteria = criteria;

        return collVendorPriceListDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getVendorPriceListDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getVendorPriceListDetails(Connection con) throws TorqueException
    {
              if (collVendorPriceListDetails == null)
        {
            collVendorPriceListDetails = getVendorPriceListDetails(new Criteria(10), con);
        }
        return collVendorPriceListDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this VendorPriceList has previously
     * been saved, it will retrieve related VendorPriceListDetails from storage.
     * If this VendorPriceList is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getVendorPriceListDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collVendorPriceListDetails == null)
        {
            if (isNew())
            {
               collVendorPriceListDetails = new ArrayList();
            }
            else
            {
                         criteria.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, getVendorPriceListId());
                         collVendorPriceListDetails = VendorPriceListDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, getVendorPriceListId());
                             if (!lastVendorPriceListDetailsCriteria.equals(criteria))
                 {
                     collVendorPriceListDetails = VendorPriceListDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastVendorPriceListDetailsCriteria = criteria;

         return collVendorPriceListDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this VendorPriceList is new, it will return
     * an empty collection; or if this VendorPriceList has previously
     * been saved, it will retrieve related VendorPriceListDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in VendorPriceList.
     */
    protected List getVendorPriceListDetailsJoinVendorPriceList(Criteria criteria)
        throws TorqueException
    {
                    if (collVendorPriceListDetails == null)
        {
            if (isNew())
            {
               collVendorPriceListDetails = new ArrayList();
            }
            else
            {
                              criteria.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, getVendorPriceListId());
                              collVendorPriceListDetails = VendorPriceListDetailPeer.doSelectJoinVendorPriceList(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, getVendorPriceListId());
                                    if (!lastVendorPriceListDetailsCriteria.equals(criteria))
            {
                collVendorPriceListDetails = VendorPriceListDetailPeer.doSelectJoinVendorPriceList(criteria);
            }
        }
        lastVendorPriceListDetailsCriteria = criteria;

        return collVendorPriceListDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VendorPriceListId");
              fieldNames.add("VendorPlCode");
              fieldNames.add("VendorId");
              fieldNames.add("CreateDate");
              fieldNames.add("ExpiredDate");
              fieldNames.add("Remark");
              fieldNames.add("CurrencyId");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VendorPriceListId"))
        {
                return getVendorPriceListId();
            }
          if (name.equals("VendorPlCode"))
        {
                return getVendorPlCode();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("ExpiredDate"))
        {
                return getExpiredDate();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VendorPriceListPeer.VENDOR_PRICE_LIST_ID))
        {
                return getVendorPriceListId();
            }
          if (name.equals(VendorPriceListPeer.VENDOR_PL_CODE))
        {
                return getVendorPlCode();
            }
          if (name.equals(VendorPriceListPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(VendorPriceListPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(VendorPriceListPeer.EXPIRED_DATE))
        {
                return getExpiredDate();
            }
          if (name.equals(VendorPriceListPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(VendorPriceListPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(VendorPriceListPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVendorPriceListId();
            }
              if (pos == 1)
        {
                return getVendorPlCode();
            }
              if (pos == 2)
        {
                return getVendorId();
            }
              if (pos == 3)
        {
                return getCreateDate();
            }
              if (pos == 4)
        {
                return getExpiredDate();
            }
              if (pos == 5)
        {
                return getRemark();
            }
              if (pos == 6)
        {
                return getCurrencyId();
            }
              if (pos == 7)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VendorPriceListPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VendorPriceListPeer.doInsert((VendorPriceList) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VendorPriceListPeer.doUpdate((VendorPriceList) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collVendorPriceListDetails != null)
            {
                for (int i = 0; i < collVendorPriceListDetails.size(); i++)
                {
                    ((VendorPriceListDetail) collVendorPriceListDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key vendorPriceListId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setVendorPriceListId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setVendorPriceListId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVendorPriceListId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public VendorPriceList copy() throws TorqueException
    {
        return copyInto(new VendorPriceList());
    }
  
    protected VendorPriceList copyInto(VendorPriceList copyObj) throws TorqueException
    {
          copyObj.setVendorPriceListId(vendorPriceListId);
          copyObj.setVendorPlCode(vendorPlCode);
          copyObj.setVendorId(vendorId);
          copyObj.setCreateDate(createDate);
          copyObj.setExpiredDate(expiredDate);
          copyObj.setRemark(remark);
          copyObj.setCurrencyId(currencyId);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setVendorPriceListId((String)null);
                                                      
                                      
                            
        List v = getVendorPriceListDetails();
        for (int i = 0; i < v.size(); i++)
        {
            VendorPriceListDetail obj = (VendorPriceListDetail) v.get(i);
            copyObj.addVendorPriceListDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VendorPriceListPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("VendorPriceList\n");
        str.append("---------------\n")
           .append("VendorPriceListId    : ")
           .append(getVendorPriceListId())
           .append("\n")
           .append("VendorPlCode         : ")
           .append(getVendorPlCode())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("ExpiredDate          : ")
           .append(getExpiredDate())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
