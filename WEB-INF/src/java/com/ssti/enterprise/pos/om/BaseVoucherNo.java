package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to VoucherNo
 */
public abstract class BaseVoucherNo extends BaseObject
{
    /** The Peer class */
    private static final VoucherNoPeer peer =
        new VoucherNoPeer();

        
    /** The value for the voucherNoId field */
    private String voucherNoId;
      
    /** The value for the voucherId field */
    private String voucherId;
      
    /** The value for the voucherNo field */
    private String voucherNo;
      
    /** The value for the validFrom field */
    private Date validFrom;
      
    /** The value for the validTo field */
    private Date validTo;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the createId field */
    private String createId;
      
    /** The value for the createBy field */
    private String createBy;
      
    /** The value for the createDate field */
    private Date createDate;
                                                
    /** The value for the usePtypeId field */
    private String usePtypeId = "";
                                                
    /** The value for the useLocId field */
    private String useLocId = "";
                                                
    /** The value for the useTransId field */
    private String useTransId = "";
                                                
    /** The value for the useCashier field */
    private String useCashier = "";
      
    /** The value for the useDate field */
    private Date useDate;
  
    
    /**
     * Get the VoucherNoId
     *
     * @return String
     */
    public String getVoucherNoId()
    {
        return voucherNoId;
    }

                        
    /**
     * Set the value of VoucherNoId
     *
     * @param v new value
     */
    public void setVoucherNoId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voucherNoId, v))
              {
            this.voucherNoId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VoucherId
     *
     * @return String
     */
    public String getVoucherId()
    {
        return voucherId;
    }

                              
    /**
     * Set the value of VoucherId
     *
     * @param v new value
     */
    public void setVoucherId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.voucherId, v))
              {
            this.voucherId = v;
            setModified(true);
        }
    
                          
                if (aVoucher != null && !ObjectUtils.equals(aVoucher.getVoucherId(), v))
                {
            aVoucher = null;
        }
      
              }
  
    /**
     * Get the VoucherNo
     *
     * @return String
     */
    public String getVoucherNo()
    {
        return voucherNo;
    }

                        
    /**
     * Set the value of VoucherNo
     *
     * @param v new value
     */
    public void setVoucherNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voucherNo, v))
              {
            this.voucherNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ValidFrom
     *
     * @return Date
     */
    public Date getValidFrom()
    {
        return validFrom;
    }

                        
    /**
     * Set the value of ValidFrom
     *
     * @param v new value
     */
    public void setValidFrom(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.validFrom, v))
              {
            this.validFrom = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ValidTo
     *
     * @return Date
     */
    public Date getValidTo()
    {
        return validTo;
    }

                        
    /**
     * Set the value of ValidTo
     *
     * @param v new value
     */
    public void setValidTo(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.validTo, v))
              {
            this.validTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateId
     *
     * @return String
     */
    public String getCreateId()
    {
        return createId;
    }

                        
    /**
     * Set the value of CreateId
     *
     * @param v new value
     */
    public void setCreateId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createId, v))
              {
            this.createId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UsePtypeId
     *
     * @return String
     */
    public String getUsePtypeId()
    {
        return usePtypeId;
    }

                        
    /**
     * Set the value of UsePtypeId
     *
     * @param v new value
     */
    public void setUsePtypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.usePtypeId, v))
              {
            this.usePtypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseLocId
     *
     * @return String
     */
    public String getUseLocId()
    {
        return useLocId;
    }

                        
    /**
     * Set the value of UseLocId
     *
     * @param v new value
     */
    public void setUseLocId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useLocId, v))
              {
            this.useLocId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseTransId
     *
     * @return String
     */
    public String getUseTransId()
    {
        return useTransId;
    }

                        
    /**
     * Set the value of UseTransId
     *
     * @param v new value
     */
    public void setUseTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useTransId, v))
              {
            this.useTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseCashier
     *
     * @return String
     */
    public String getUseCashier()
    {
        return useCashier;
    }

                        
    /**
     * Set the value of UseCashier
     *
     * @param v new value
     */
    public void setUseCashier(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useCashier, v))
              {
            this.useCashier = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseDate
     *
     * @return Date
     */
    public Date getUseDate()
    {
        return useDate;
    }

                        
    /**
     * Set the value of UseDate
     *
     * @param v new value
     */
    public void setUseDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.useDate, v))
              {
            this.useDate = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Voucher aVoucher;

    /**
     * Declares an association between this object and a Voucher object
     *
     * @param v Voucher
     * @throws TorqueException
     */
    public void setVoucher(Voucher v) throws TorqueException
    {
            if (v == null)
        {
                  setVoucherId((String) null);
              }
        else
        {
            setVoucherId(v.getVoucherId());
        }
            aVoucher = v;
    }

                                            
    /**
     * Get the associated Voucher object
     *
     * @return the associated Voucher object
     * @throws TorqueException
     */
    public Voucher getVoucher() throws TorqueException
    {
        if (aVoucher == null && (!ObjectUtils.equals(this.voucherId, null)))
        {
                          aVoucher = VoucherPeer.retrieveByPK(SimpleKey.keyFor(this.voucherId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Voucher obj = VoucherPeer.retrieveByPK(this.voucherId);
               obj.addVoucherNos(this);
            */
        }
        return aVoucher;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setVoucherKey(ObjectKey key) throws TorqueException
    {
      
                        setVoucherId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VoucherNoId");
              fieldNames.add("VoucherId");
              fieldNames.add("VoucherNo");
              fieldNames.add("ValidFrom");
              fieldNames.add("ValidTo");
              fieldNames.add("Amount");
              fieldNames.add("CreateId");
              fieldNames.add("CreateBy");
              fieldNames.add("CreateDate");
              fieldNames.add("UsePtypeId");
              fieldNames.add("UseLocId");
              fieldNames.add("UseTransId");
              fieldNames.add("UseCashier");
              fieldNames.add("UseDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VoucherNoId"))
        {
                return getVoucherNoId();
            }
          if (name.equals("VoucherId"))
        {
                return getVoucherId();
            }
          if (name.equals("VoucherNo"))
        {
                return getVoucherNo();
            }
          if (name.equals("ValidFrom"))
        {
                return getValidFrom();
            }
          if (name.equals("ValidTo"))
        {
                return getValidTo();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("CreateId"))
        {
                return getCreateId();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("UsePtypeId"))
        {
                return getUsePtypeId();
            }
          if (name.equals("UseLocId"))
        {
                return getUseLocId();
            }
          if (name.equals("UseTransId"))
        {
                return getUseTransId();
            }
          if (name.equals("UseCashier"))
        {
                return getUseCashier();
            }
          if (name.equals("UseDate"))
        {
                return getUseDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VoucherNoPeer.VOUCHER_NO_ID))
        {
                return getVoucherNoId();
            }
          if (name.equals(VoucherNoPeer.VOUCHER_ID))
        {
                return getVoucherId();
            }
          if (name.equals(VoucherNoPeer.VOUCHER_NO))
        {
                return getVoucherNo();
            }
          if (name.equals(VoucherNoPeer.VALID_FROM))
        {
                return getValidFrom();
            }
          if (name.equals(VoucherNoPeer.VALID_TO))
        {
                return getValidTo();
            }
          if (name.equals(VoucherNoPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(VoucherNoPeer.CREATE_ID))
        {
                return getCreateId();
            }
          if (name.equals(VoucherNoPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(VoucherNoPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(VoucherNoPeer.USE_PTYPE_ID))
        {
                return getUsePtypeId();
            }
          if (name.equals(VoucherNoPeer.USE_LOC_ID))
        {
                return getUseLocId();
            }
          if (name.equals(VoucherNoPeer.USE_TRANS_ID))
        {
                return getUseTransId();
            }
          if (name.equals(VoucherNoPeer.USE_CASHIER))
        {
                return getUseCashier();
            }
          if (name.equals(VoucherNoPeer.USE_DATE))
        {
                return getUseDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVoucherNoId();
            }
              if (pos == 1)
        {
                return getVoucherId();
            }
              if (pos == 2)
        {
                return getVoucherNo();
            }
              if (pos == 3)
        {
                return getValidFrom();
            }
              if (pos == 4)
        {
                return getValidTo();
            }
              if (pos == 5)
        {
                return getAmount();
            }
              if (pos == 6)
        {
                return getCreateId();
            }
              if (pos == 7)
        {
                return getCreateBy();
            }
              if (pos == 8)
        {
                return getCreateDate();
            }
              if (pos == 9)
        {
                return getUsePtypeId();
            }
              if (pos == 10)
        {
                return getUseLocId();
            }
              if (pos == 11)
        {
                return getUseTransId();
            }
              if (pos == 12)
        {
                return getUseCashier();
            }
              if (pos == 13)
        {
                return getUseDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VoucherNoPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VoucherNoPeer.doInsert((VoucherNo) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VoucherNoPeer.doUpdate((VoucherNo) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key voucherNoId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVoucherNoId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVoucherNoId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVoucherNoId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public VoucherNo copy() throws TorqueException
    {
        return copyInto(new VoucherNo());
    }
  
    protected VoucherNo copyInto(VoucherNo copyObj) throws TorqueException
    {
          copyObj.setVoucherNoId(voucherNoId);
          copyObj.setVoucherId(voucherId);
          copyObj.setVoucherNo(voucherNo);
          copyObj.setValidFrom(validFrom);
          copyObj.setValidTo(validTo);
          copyObj.setAmount(amount);
          copyObj.setCreateId(createId);
          copyObj.setCreateBy(createBy);
          copyObj.setCreateDate(createDate);
          copyObj.setUsePtypeId(usePtypeId);
          copyObj.setUseLocId(useLocId);
          copyObj.setUseTransId(useTransId);
          copyObj.setUseCashier(useCashier);
          copyObj.setUseDate(useDate);
  
                    copyObj.setVoucherNoId((String)null);
                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VoucherNoPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("VoucherNo\n");
        str.append("---------\n")
           .append("VoucherNoId          : ")
           .append(getVoucherNoId())
           .append("\n")
           .append("VoucherId            : ")
           .append(getVoucherId())
           .append("\n")
           .append("VoucherNo            : ")
           .append(getVoucherNo())
           .append("\n")
           .append("ValidFrom            : ")
           .append(getValidFrom())
           .append("\n")
           .append("ValidTo              : ")
           .append(getValidTo())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("CreateId             : ")
           .append(getCreateId())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("UsePtypeId           : ")
           .append(getUsePtypeId())
           .append("\n")
           .append("UseLocId             : ")
           .append(getUseLocId())
           .append("\n")
           .append("UseTransId           : ")
           .append(getUseTransId())
           .append("\n")
           .append("UseCashier           : ")
           .append(getUseCashier())
           .append("\n")
           .append("UseDate              : ")
           .append(getUseDate())
           .append("\n")
        ;
        return(str.toString());
    }
}
