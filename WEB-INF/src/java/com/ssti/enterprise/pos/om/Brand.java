
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class Brand
    extends com.ssti.enterprise.pos.om.BaseBrand
    implements Persistent, MasterOM
{

	@Override
	public String getId() {
		return getBrandId();
	}

	@Override
	public String getCode() {
		return getBrandCode();
	}

	public String getName() {
		return getBrandName();
	}
	
	public int getTotalItems(String _sKatID, boolean _bWebStore)
	{
		try 
		{
			return ItemFieldTool.getTotalItemsByBrand(getBrandId(), _sKatID, _bWebStore);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return 0;
	}
	
	Principal principal = null;
	public Principal getPrincipal()
	{
		if(principal == null && StringUtil.isNotEmpty(getPrincipalId()))
		{
			try 
			{
				principal =  ItemFieldTool.getPrincipalByID(getPrincipalId());
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}	
		}
		return principal;
	}
}
