
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.tools.PaymentBillsTool;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class CashierBills
    extends com.ssti.enterprise.pos.om.BaseCashierBills
    implements Persistent
{
	PaymentBills paymentBills;
	public PaymentBills getPaymentBills()
	{
		if(paymentBills == null)
		{
			try
			{
				paymentBills = PaymentBillsTool.getByID(getPaymentBillsId());				
			}
			catch (Exception e)
			{
			}
		}
		return paymentBills;
	}
}
