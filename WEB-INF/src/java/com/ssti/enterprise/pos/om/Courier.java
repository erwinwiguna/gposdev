
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.framework.tools.StringUtil;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Mon Feb 07 14:16:35 ICT 2005]
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class Courier
    extends com.ssti.enterprise.pos.om.BaseCourier
    implements Persistent, MasterOM
{
	public String getId()
	{
		return getCourierId();
	}

	public String getCode()
	{
		return getCourierName();
	}
	
	public String getName()
	{
		return getCourierName();
	}
	
    Employee employee = null;
    public Employee getEmployee()
    {
    	try
		{
    		if(employee == null || (employee != null && !StringUtil.isEqual(getEmployeeId(),employee.getId())) )
    		{
    			employee = EmployeeTool.getEmployeeByID(getEmployeeId());	
    		}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    	return employee;
    }
    
    public String getEmployeeName()
    {
    	try
		{
    		getEmployee();
    		if(employee != null)
    		{
    			return employee.getEmployeeName();	
    		}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    	return "";
    }	
}
