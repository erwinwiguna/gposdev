
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MemoOM;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2017-10-20
 * - add bank, cashFlowType, crossAccount
 * </pre><br>
 */
public  class CreditMemo
    extends com.ssti.enterprise.pos.om.BaseCreditMemo
    implements Persistent, MemoOM
{
	public String getEntityId() 
	{
		return getCustomerId();
	}

	public String getEntityName() 
	{
		return getCustomerName();
	}
	
	public String getMemoId() 
	{
		return getCreditMemoId();
	}
	
	public String getMemoNo() 
	{
		return getCreditMemoNo();
	}	
	
	Bank bank = null;
	public Bank getBank()
	{
		try
		{
			if(bank == null)
			{
				bank = BankTool.getBankByID(getBankId());	
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return bank;
	}

	CashFlowType cashFlowType = null;
	public CashFlowType getCashFlowType()
	{
		try
		{
			if(cashFlowType == null && StringUtil.isNotEmpty(getCashFlowTypeId()))
			{
				cashFlowType = CashFlowTypeTool.getTypeByID(getCashFlowTypeId(),null);	
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return cashFlowType;
	}	
	
	Account account;
	public Account getAccount()
	{
		if(account == null && StringUtil.isNotEmpty(getAccountId()))
		{
			try 
			{
				account = AccountTool.getAccountByID(getAccountId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return account;
	}
	
	Account crossAccount;
	public Account getCrossAccount()
	{
		if(crossAccount == null && StringUtil.isNotEmpty(getCrossAccountId()))
		{
			try 
			{
				crossAccount = AccountTool.getAccountByID(getCrossAccountId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return crossAccount;
	}

	SalesOrder orderTrans;
	public SalesOrder getOrderTrans()	
	{
		if(orderTrans == null && StringUtil.isNotEmpty(getTransactionId()))
		{
			try 
			{
				orderTrans = SalesOrderTool.getHeaderByID(getTransactionId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orderTrans;
	}
	
	SalesReturn returnTrans;
	public SalesReturn getReturnTrans()	
	{
		if(returnTrans == null && StringUtil.isNotEmpty(getTransactionId()))
		{
			try 
			{
				returnTrans = SalesReturnTool.getHeaderByID(getTransactionId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnTrans;
	}

	ArPayment paymentTrans;
	public ArPayment gePaymentTrans()	
	{
		if(paymentTrans == null && getStatus() != AppAttributes.i_MEMO_CANCELLED)
		{
			try 
			{
				paymentTrans = ReceivablePaymentTool.getHeaderByID(getPaymentTransId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return paymentTrans;
	}
	
	public String getStatusStr()
	{
		return CreditMemoTool.getStatusString(getStatus());
	}	
	
	boolean updateOrder = false;
	public boolean getUpdateOrder() {
		return updateOrder;
	}

	public void setUpdateOrder(boolean updateOrder) {
		this.updateOrder = updateOrder;
	}		
}
