
package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.InventoryMasterOM;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * InvTransUpdate to record updates in inventory transaction data
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public  class InvTransUpdate
    extends com.ssti.enterprise.pos.om.BaseInvTransUpdate
    implements Persistent, AppAttributes
{
	InventoryTransaction invTrans;
	
	InvTransUpdate linkedTrans;
	
	public InventoryTransaction getInvTrans() 
	{
		return invTrans;
	}

	public InvTransUpdate getLinkedTrans() 
	{
		return linkedTrans;
	}

	boolean isValid = true;
	
	public InvTransUpdate() {}
    public InvTransUpdate(InventoryTransaction _invTrans,     						   
    					  double _oldSubCost, 
    				      double _newSubCost, 
    					  InventoryMasterOM _srcTrans,
    				      Connection _conn)
        throws Exception				      
    {
    	this.invTrans = _invTrans;
    	if(invTrans != null)
    	{    		
    		setInvtransId(invTrans.getInventoryTransactionId());
    		setTransId(invTrans.getTransactionId());
    		setTransdetId(invTrans.getTransactionDetailId());
    		setTransNo(invTrans.getTransactionNo());
    		setItemId(invTrans.getItemId());
    		setLocationId(invTrans.getLocationId());
    		setOldSubcost(new BigDecimal(_oldSubCost));
    		setNewSubcost(new BigDecimal(_newSubCost));
    		setTxType(invTrans.getTransactionType());  
    		setUpdateDate(new Date());
    		if (_srcTrans != null)
    		{
    			setUpdateBy(_srcTrans.getCreateBy());
    			setUpdateTx(_srcTrans.getTransactionNo());
    		}
    		try 
    		{
        		Item item = ItemTool.getItemByID(getItemId(), _conn);
        		setInvAccount(item.getInventoryAccount());
        		setOthAccount(item.getCogsAccount());

        		if(getTxType() == i_INV_TRANS_DELIVERY_ORDER)
    			{
    				Account oDO = AccountTool.getDeliveryOrderAccount(item.getCogsAccount(), _conn);
    				if (oDO != null) setOthAccount(oDO.getAccountId());
    				setGlType(GlAttributes.i_GL_TRANS_DELIVERY_ORDER);    				
    				invTrans.setTransData(_conn);
    				if(invTrans.getTrans() != null)
    				{
    					DeliveryOrder oTR = (DeliveryOrder) invTrans.getTrans();
    					//if DO already Invoiced and setting use DO Account not COGS then we need to update SI GL journal 
    					//so we need to create a linked trans 
    					if(oTR.getStatus() == i_DO_INVOICED && !StringUtil.isEqual(item.getCogsAccount(), getOthAccount()))
    					{
    						SalesTransactionDetail oTD = TransactionTool.getDetailByDODetailID(invTrans.getTransactionDetailId(), _conn);
    						SalesTransaction oSI = TransactionTool.getHeaderByID(oTD.getSalesTransactionId(), _conn);
    						if(oSI != null && oTD != null)
    						{
	    						linkedTrans = new InvTransUpdate();
	    						setLinkedTransFields();
	    						linkedTrans.setInvAccount(getOthAccount());
	    						linkedTrans.setOthAccount(item.getCogsAccount());
	    			    		linkedTrans.setTxType(i_INV_TRANS_SALES_INVOICE);
	    						linkedTrans.setGlType(GlAttributes.i_GL_TRANS_SALES_INVOICE);    						
	    						linkedTrans.setTransId(oTD.getSalesTransactionId());
	    						linkedTrans.setTransNo(oSI.getInvoiceNo());
	    						linkedTrans.setTransdetId(oTD.getSalesTransactionDetailId());
	    						linkedTrans.setUpdateId(IDGenerator.generateSysID());
	    						linkedTrans.save(_conn);
    						}
    					}
    				}
    			}
    			if(getTxType() == i_INV_TRANS_SALES_INVOICE)
    			{
    				setGlType(GlAttributes.i_GL_TRANS_SALES_INVOICE);
    			}
    			if(getTxType() == i_INV_TRANS_SALES_RETURN)
    			{
    				setGlType(GlAttributes.i_GL_TRANS_SALES_RETURN);
    				invTrans.setTransData(_conn);
    				if(invTrans.getTrans() != null)
    				{
    					SalesReturn oTR = (SalesReturn) invTrans.getTrans();
    					if(oTR.getTransactionType() == i_RET_FROM_PR_DO)
    					{
    						Account oDO = AccountTool.getDeliveryOrderAccount(item.getCogsAccount(), _conn);
    	    				if (oDO != null) setOthAccount(oDO.getAccountId());
    					}
    				}
    			}    			
    			if(getTxType() == i_INV_TRANS_RECEIPT_UNPLANNED || getTxType() == i_INV_TRANS_ISSUE_UNPLANNED)
    			{
    				IssueReceipt oIR = null;
    				if (invTrans.getTrans() != null)
    				{
    					oIR = (IssueReceipt) invTrans.getTrans();
    				}
    				else
    				{
    					oIR = IssueReceiptTool.getHeaderByID(invTrans.getTransactionId(), _conn);
    				}
    				if(oIR != null)
    				{
    					setOthAccount(oIR.getAccountId());
    					setGlType(GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED);
    				}
    			}
    			if(getTxType() == i_INV_TRANS_PURCHASE_RETURN)
    			{
    				setOthAccount(item.getPurchaseReturnAccount());
    				setGlType(GlAttributes.i_GL_TRANS_PURCHASE_RETURN);
    			}
    			
    			setUpdateId(IDGenerator.generateSysID());
    			save(_conn);
    		} 
    		catch (Exception e) 
    		{
    			e.printStackTrace();
    			throw new Exception ("Error Creating InvTransUpdate Record: " + e.getMessage());    			
    		}			
    	}
    }    

    public double getDifference()
    {
    	if(getOldSubcost() != null && getNewSubcost() != null)
    	{
    		return (getNewSubcost().doubleValue() - getOldSubcost().doubleValue());    			
    	}
    	return 0;
    }
    
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	
	public static void save(List invTransToRejournal, Connection _oConn)
	{
//		PreparedStatement pstmt = _oConn.prepareStatement("INSERT INTO inv_trans_update VALUES")
	}

	private void setLinkedTransFields()
	{
		if(linkedTrans != null)
		{
			linkedTrans.setInvtransId(getInvtransId());
			linkedTrans.setTransNo(getTransNo());
			linkedTrans.setItemId(getItemId());
			linkedTrans.setLocationId(getLocationId());
			linkedTrans.setOldSubcost(getOldSubcost());
			linkedTrans.setNewSubcost(getNewSubcost());
			linkedTrans.setUpdateDate(getUpdateDate());
			linkedTrans.setUpdateBy(getUpdateBy());
			linkedTrans.setUpdateTx(getUpdateTx());
		}
	}
}
