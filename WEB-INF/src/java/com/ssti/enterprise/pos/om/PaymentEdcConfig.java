
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class PaymentEdcConfig
    extends com.ssti.enterprise.pos.om.BasePaymentEdcConfig
    implements Persistent,MasterOM
{

	@Override
	public String getId() {
		return getConfigId();
	}

	@Override
	public String getCode() {
		return getMerchantId();
	}
	
	PaymentTerm paymentTerm = null;	
	public PaymentTerm getEDC()
	{
		if(paymentTerm == null && StringUtil.isNotEmpty(getPaymentTermId()))
		{
			try
			{
				paymentTerm = PaymentTermTool.getPaymentTermByID(getPaymentTermId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return paymentTerm;
	}    

	Location location = null;	
	public Location getLocation()
	{
		if(location == null && StringUtil.isNotEmpty(getLocationId()))
		{
			try
			{
				location = LocationTool.getLocationByID(getLocationId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return location;
	}    
}
