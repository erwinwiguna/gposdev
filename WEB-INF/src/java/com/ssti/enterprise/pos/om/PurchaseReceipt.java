
package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.ssti.enterprise.pos.model.InventoryMasterOM;
import com.ssti.enterprise.pos.model.MultiCurrencyMasterOM;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Mon Feb 07 14:16:35 ICT 2005]
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class PurchaseReceipt
    extends com.ssti.enterprise.pos.om.BasePurchaseReceipt
    implements MultiCurrencyMasterOM,InventoryMasterOM
{
	List detail = null;
	
	public String getTransactionId() 
	{
		return getPurchaseReceiptId();
	}

	public String getTransactionNo() 
	{
		return getReceiptNo();
	}

	public Date getTransactionDate() 
	{
		return getReceiptDate();
	}
	
	public String getPurchaseOrderNo() 
	{
		if(StringUtil.isNotEmpty(getPurchaseOrderId()))
		{
			try 
			{
				return PurchaseOrderTool.getPONoByID(getPurchaseOrderId());				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return "";
	}	
	
	public List getDetail()
	{
		return detail;
	}

	public void setDetail(List _detail)
	{
		this.detail = _detail;
	}
	
	public String getStatusStr()
	{
		return PurchaseReceiptTool.getStatusString(getStatus());
	}

	public BigDecimal getTotalCost() 
	{
		double totalCost = 0;
		getDetails();
		if(getDetails() == null && details.size() > 0)
		{
    		for(int i = 0; i < details.size(); i++)
    		{
    			PurchaseReceiptDetail oTD = (PurchaseReceiptDetail) details.get(i);
    			totalCost = Calculator.add(totalCost, oTD.getSubTotalCost());    			
    		}
		}		
		return null;
	}
	
	List details = null; 
    public List getDetails()
    {
    	if(StringUtil.isNotEmpty(getTransactionId()) && details == null)
    	{
    		try 
    		{
				details = DeliveryOrderTool.getDetailsByID(getTransactionId());
			} 
    		catch (Exception e) 
    		{
    			e.printStackTrace();
			}
    	}
    	return details;
    }	
	
	
    Location location = null;
    public Location getLocation()
    {
    	try
		{
    		if(location == null)
    		{
    			location = LocationTool.getLocationByID(getLocationId());	
    		}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    	return location;
    }

    public String getLocationName()
    {
    	try
		{
    		getLocation();
    		if(location != null)
    		{
    			return location.getLocationName();	
    		}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    	return "";
    }
    
	PaymentType paymentType = null;	
	public PaymentType getPaymentType()
	{
		if(paymentType == null && StringUtil.isNotEmpty(getPaymentTypeId()))
		{
			try
			{
				paymentType = PaymentTypeTool.getPaymentTypeByID(getPaymentTypeId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return paymentType;
	}
	
	PaymentTerm paymentTerm = null;	
	public PaymentTerm getPaymentTerm()
	{
		if(paymentTerm == null && StringUtil.isNotEmpty(getPaymentTermId()))
		{
			try
			{
				paymentTerm = PaymentTermTool.getPaymentTermByID(getPaymentTermId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return paymentTerm;
	}
    
    Vendor vendor = null;
    public Vendor getVendor()
    {
    	try
		{
    		if(vendor == null)
    		{
    			vendor = VendorTool.getVendorByID(getVendorId());	
    		}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    	return vendor;
    }
    
    public Object getEntity()
    {
    	return getVendor();
    }
}

