package com.ssti.enterprise.pos.om;

import com.ssti.enterprise.pos.model.TransactionPeerOM;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Mon Feb 07 14:16:35 ICT 2005]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class SalesOrderPeer
    extends com.ssti.enterprise.pos.om.BaseSalesOrderPeer
    implements TransactionPeerOM
{
	public String getTableName()
	{
		return TABLE_NAME;
	}
	
	public String getIDColumn() 
	{
		return SALES_ORDER_ID;
	}
	
	public String getNoColumn() 
	{
		return SALES_ORDER_NO;
	}

	public String getDateColumn() 
	{
		return TRANSACTION_DATE;
	}

	public String getCreateByColumn() 
	{
		return USER_NAME;
	}

	public String getStatusColumn() 
	{
		return TRANSACTION_STATUS;
	}	
	
}
