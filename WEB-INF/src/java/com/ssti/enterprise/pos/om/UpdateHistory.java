
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class UpdateHistory
    extends com.ssti.enterprise.pos.om.BaseUpdateHistory
    implements Persistent,MasterOM
{

	@Override
	public String getId() {
		return getHistoryId();
	}

	@Override
	public String getCode() {		
		return getHistoryId();
	}
	
	
}
