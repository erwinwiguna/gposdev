
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class VendorType
    extends com.ssti.enterprise.pos.om.BaseVendorType
    implements Persistent, MasterOM
{
	public static int i_ALLOW_ALL = 0;
	public static int i_ALLOW_INV = 1;
	public static int i_ALLOW_NON = 2;

	@Override
	public String getId() {
		return getVendorTypeId();
	}

	@Override
	public String getCode() {
		return getVendorTypeCode();
	}	
}
