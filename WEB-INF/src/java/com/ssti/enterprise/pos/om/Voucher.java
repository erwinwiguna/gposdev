
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class Voucher
    extends com.ssti.enterprise.pos.om.BaseVoucher
    implements Persistent,MasterOM
{
	public String getId()
	{
		return getVoucherId();
	}

	public String getCode()
	{
		return getVoucherCode();
	}	
}
