
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentVoucherTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class VoucherNo
    extends com.ssti.enterprise.pos.om.BaseVoucherNo
    implements Persistent
{
	Voucher voucher = null;
	public Voucher getVoucher()
	{
		if (voucher == null)
		{
			try
			{
				voucher = PaymentVoucherTool.getByID(getVoucherId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return voucher;
	}
	
	public String getValidFromStr()
	{
		if(getValidFrom() != null) return CustomFormatter.fmt(getValidFrom());
		return "";
	}

	public String getValidToStr()
	{
		if(getValidTo() != null) return CustomFormatter.fmt(getValidTo());
		return "";
	}

	public String getStatusStr()
	{
		if(StringUtil.isNotEmpty(getUseTransId())) return "Used";
		else return LocaleTool.getString("new");
	}
	
}
