package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AccountCashflowMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AccountCashflowMapBuilder";

    /**
     * Item
     * @deprecated use AccountCashflowPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "account_cashflow";
    }

  
    /**
     * account_cashflow.ACCOUNT_CASHFLOW_ID
     * @return the column name for the ACCOUNT_CASHFLOW_ID field
     * @deprecated use AccountCashflowPeer.account_cashflow.ACCOUNT_CASHFLOW_ID constant
     */
    public static String getAccountCashflow_AccountCashflowId()
    {
        return "account_cashflow.ACCOUNT_CASHFLOW_ID";
    }
  
    /**
     * account_cashflow.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use AccountCashflowPeer.account_cashflow.ACCOUNT_ID constant
     */
    public static String getAccountCashflow_AccountId()
    {
        return "account_cashflow.ACCOUNT_ID";
    }
  
    /**
     * account_cashflow.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use AccountCashflowPeer.account_cashflow.CASH_FLOW_TYPE_ID constant
     */
    public static String getAccountCashflow_CashFlowTypeId()
    {
        return "account_cashflow.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("account_cashflow");
        TableMap tMap = dbMap.getTable("account_cashflow");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("account_cashflow.ACCOUNT_CASHFLOW_ID", "");
                          tMap.addColumn("account_cashflow.ACCOUNT_ID", "");
                          tMap.addColumn("account_cashflow.CASH_FLOW_TYPE_ID", "");
          }
}
