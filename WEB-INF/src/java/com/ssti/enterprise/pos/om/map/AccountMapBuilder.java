package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AccountMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AccountMapBuilder";

    /**
     * Item
     * @deprecated use AccountPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "account";
    }

  
    /**
     * account.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use AccountPeer.account.ACCOUNT_ID constant
     */
    public static String getAccount_AccountId()
    {
        return "account.ACCOUNT_ID";
    }
  
    /**
     * account.ACCOUNT_CODE
     * @return the column name for the ACCOUNT_CODE field
     * @deprecated use AccountPeer.account.ACCOUNT_CODE constant
     */
    public static String getAccount_AccountCode()
    {
        return "account.ACCOUNT_CODE";
    }
  
    /**
     * account.ACCOUNT_NAME
     * @return the column name for the ACCOUNT_NAME field
     * @deprecated use AccountPeer.account.ACCOUNT_NAME constant
     */
    public static String getAccount_AccountName()
    {
        return "account.ACCOUNT_NAME";
    }
  
    /**
     * account.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use AccountPeer.account.DESCRIPTION constant
     */
    public static String getAccount_Description()
    {
        return "account.DESCRIPTION";
    }
  
    /**
     * account.ACCOUNT_TYPE
     * @return the column name for the ACCOUNT_TYPE field
     * @deprecated use AccountPeer.account.ACCOUNT_TYPE constant
     */
    public static String getAccount_AccountType()
    {
        return "account.ACCOUNT_TYPE";
    }
  
    /**
     * account.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use AccountPeer.account.PARENT_ID constant
     */
    public static String getAccount_ParentId()
    {
        return "account.PARENT_ID";
    }
  
    /**
     * account.HAS_CHILD
     * @return the column name for the HAS_CHILD field
     * @deprecated use AccountPeer.account.HAS_CHILD constant
     */
    public static String getAccount_HasChild()
    {
        return "account.HAS_CHILD";
    }
  
    /**
     * account.ACCOUNT_LEVEL
     * @return the column name for the ACCOUNT_LEVEL field
     * @deprecated use AccountPeer.account.ACCOUNT_LEVEL constant
     */
    public static String getAccount_AccountLevel()
    {
        return "account.ACCOUNT_LEVEL";
    }
  
    /**
     * account.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use AccountPeer.account.CURRENCY_ID constant
     */
    public static String getAccount_CurrencyId()
    {
        return "account.CURRENCY_ID";
    }
  
    /**
     * account.NORMAL_BALANCE
     * @return the column name for the NORMAL_BALANCE field
     * @deprecated use AccountPeer.account.NORMAL_BALANCE constant
     */
    public static String getAccount_NormalBalance()
    {
        return "account.NORMAL_BALANCE";
    }
  
    /**
     * account.STATUS
     * @return the column name for the STATUS field
     * @deprecated use AccountPeer.account.STATUS constant
     */
    public static String getAccount_Status()
    {
        return "account.STATUS";
    }
  
    /**
     * account.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use AccountPeer.account.OPENING_BALANCE constant
     */
    public static String getAccount_OpeningBalance()
    {
        return "account.OPENING_BALANCE";
    }
  
    /**
     * account.AS_DATE
     * @return the column name for the AS_DATE field
     * @deprecated use AccountPeer.account.AS_DATE constant
     */
    public static String getAccount_AsDate()
    {
        return "account.AS_DATE";
    }
  
    /**
     * account.OB_TRANS_ID
     * @return the column name for the OB_TRANS_ID field
     * @deprecated use AccountPeer.account.OB_TRANS_ID constant
     */
    public static String getAccount_ObTransId()
    {
        return "account.OB_TRANS_ID";
    }
  
    /**
     * account.OB_RATE
     * @return the column name for the OB_RATE field
     * @deprecated use AccountPeer.account.OB_RATE constant
     */
    public static String getAccount_ObRate()
    {
        return "account.OB_RATE";
    }
  
    /**
     * account.ALT_CODE
     * @return the column name for the ALT_CODE field
     * @deprecated use AccountPeer.account.ALT_CODE constant
     */
    public static String getAccount_AltCode()
    {
        return "account.ALT_CODE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("account");
        TableMap tMap = dbMap.getTable("account");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("account.ACCOUNT_ID", "");
                          tMap.addColumn("account.ACCOUNT_CODE", "");
                          tMap.addColumn("account.ACCOUNT_NAME", "");
                          tMap.addColumn("account.DESCRIPTION", "");
                            tMap.addColumn("account.ACCOUNT_TYPE", Integer.valueOf(0));
                          tMap.addColumn("account.PARENT_ID", "");
                          tMap.addColumn("account.HAS_CHILD", Boolean.TRUE);
                            tMap.addColumn("account.ACCOUNT_LEVEL", Integer.valueOf(0));
                          tMap.addColumn("account.CURRENCY_ID", "");
                            tMap.addColumn("account.NORMAL_BALANCE", Integer.valueOf(0));
                          tMap.addColumn("account.STATUS", Boolean.TRUE);
                            tMap.addColumn("account.OPENING_BALANCE", bd_ZERO);
                          tMap.addColumn("account.AS_DATE", new Date());
                          tMap.addColumn("account.OB_TRANS_ID", "");
                            tMap.addColumn("account.OB_RATE", bd_ZERO);
                          tMap.addColumn("account.ALT_CODE", "");
          }
}
