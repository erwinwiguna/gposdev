package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AccountPayableMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AccountPayableMapBuilder";

    /**
     * Item
     * @deprecated use AccountPayablePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "account_payable";
    }

  
    /**
     * account_payable.ACCOUNT_PAYABLE_ID
     * @return the column name for the ACCOUNT_PAYABLE_ID field
     * @deprecated use AccountPayablePeer.account_payable.ACCOUNT_PAYABLE_ID constant
     */
    public static String getAccountPayable_AccountPayableId()
    {
        return "account_payable.ACCOUNT_PAYABLE_ID";
    }
  
    /**
     * account_payable.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use AccountPayablePeer.account_payable.TRANSACTION_ID constant
     */
    public static String getAccountPayable_TransactionId()
    {
        return "account_payable.TRANSACTION_ID";
    }
  
    /**
     * account_payable.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use AccountPayablePeer.account_payable.TRANSACTION_NO constant
     */
    public static String getAccountPayable_TransactionNo()
    {
        return "account_payable.TRANSACTION_NO";
    }
  
    /**
     * account_payable.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use AccountPayablePeer.account_payable.TRANSACTION_TYPE constant
     */
    public static String getAccountPayable_TransactionType()
    {
        return "account_payable.TRANSACTION_TYPE";
    }
  
    /**
     * account_payable.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use AccountPayablePeer.account_payable.VENDOR_ID constant
     */
    public static String getAccountPayable_VendorId()
    {
        return "account_payable.VENDOR_ID";
    }
  
    /**
     * account_payable.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use AccountPayablePeer.account_payable.VENDOR_NAME constant
     */
    public static String getAccountPayable_VendorName()
    {
        return "account_payable.VENDOR_NAME";
    }
  
    /**
     * account_payable.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use AccountPayablePeer.account_payable.TRANSACTION_DATE constant
     */
    public static String getAccountPayable_TransactionDate()
    {
        return "account_payable.TRANSACTION_DATE";
    }
  
    /**
     * account_payable.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use AccountPayablePeer.account_payable.CURRENCY_ID constant
     */
    public static String getAccountPayable_CurrencyId()
    {
        return "account_payable.CURRENCY_ID";
    }
  
    /**
     * account_payable.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use AccountPayablePeer.account_payable.CURRENCY_RATE constant
     */
    public static String getAccountPayable_CurrencyRate()
    {
        return "account_payable.CURRENCY_RATE";
    }
  
    /**
     * account_payable.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use AccountPayablePeer.account_payable.AMOUNT constant
     */
    public static String getAccountPayable_Amount()
    {
        return "account_payable.AMOUNT";
    }
  
    /**
     * account_payable.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use AccountPayablePeer.account_payable.AMOUNT_BASE constant
     */
    public static String getAccountPayable_AmountBase()
    {
        return "account_payable.AMOUNT_BASE";
    }
  
    /**
     * account_payable.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use AccountPayablePeer.account_payable.DUE_DATE constant
     */
    public static String getAccountPayable_DueDate()
    {
        return "account_payable.DUE_DATE";
    }
  
    /**
     * account_payable.REMARK
     * @return the column name for the REMARK field
     * @deprecated use AccountPayablePeer.account_payable.REMARK constant
     */
    public static String getAccountPayable_Remark()
    {
        return "account_payable.REMARK";
    }
  
    /**
     * account_payable.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use AccountPayablePeer.account_payable.LOCATION_ID constant
     */
    public static String getAccountPayable_LocationId()
    {
        return "account_payable.LOCATION_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("account_payable");
        TableMap tMap = dbMap.getTable("account_payable");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("account_payable.ACCOUNT_PAYABLE_ID", "");
                          tMap.addColumn("account_payable.TRANSACTION_ID", "");
                          tMap.addColumn("account_payable.TRANSACTION_NO", "");
                            tMap.addColumn("account_payable.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("account_payable.VENDOR_ID", "");
                          tMap.addColumn("account_payable.VENDOR_NAME", "");
                          tMap.addColumn("account_payable.TRANSACTION_DATE", new Date());
                          tMap.addColumn("account_payable.CURRENCY_ID", "");
                            tMap.addColumn("account_payable.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("account_payable.AMOUNT", bd_ZERO);
                            tMap.addColumn("account_payable.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("account_payable.DUE_DATE", new Date());
                          tMap.addColumn("account_payable.REMARK", "");
                          tMap.addColumn("account_payable.LOCATION_ID", "");
          }
}
