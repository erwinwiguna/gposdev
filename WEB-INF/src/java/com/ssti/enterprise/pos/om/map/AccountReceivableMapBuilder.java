package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AccountReceivableMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AccountReceivableMapBuilder";

    /**
     * Item
     * @deprecated use AccountReceivablePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "account_receivable";
    }

  
    /**
     * account_receivable.ACCOUNT_RECEIVABLE_ID
     * @return the column name for the ACCOUNT_RECEIVABLE_ID field
     * @deprecated use AccountReceivablePeer.account_receivable.ACCOUNT_RECEIVABLE_ID constant
     */
    public static String getAccountReceivable_AccountReceivableId()
    {
        return "account_receivable.ACCOUNT_RECEIVABLE_ID";
    }
  
    /**
     * account_receivable.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use AccountReceivablePeer.account_receivable.TRANSACTION_ID constant
     */
    public static String getAccountReceivable_TransactionId()
    {
        return "account_receivable.TRANSACTION_ID";
    }
  
    /**
     * account_receivable.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use AccountReceivablePeer.account_receivable.TRANSACTION_NO constant
     */
    public static String getAccountReceivable_TransactionNo()
    {
        return "account_receivable.TRANSACTION_NO";
    }
  
    /**
     * account_receivable.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use AccountReceivablePeer.account_receivable.TRANSACTION_DATE constant
     */
    public static String getAccountReceivable_TransactionDate()
    {
        return "account_receivable.TRANSACTION_DATE";
    }
  
    /**
     * account_receivable.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use AccountReceivablePeer.account_receivable.TRANSACTION_TYPE constant
     */
    public static String getAccountReceivable_TransactionType()
    {
        return "account_receivable.TRANSACTION_TYPE";
    }
  
    /**
     * account_receivable.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use AccountReceivablePeer.account_receivable.CUSTOMER_ID constant
     */
    public static String getAccountReceivable_CustomerId()
    {
        return "account_receivable.CUSTOMER_ID";
    }
  
    /**
     * account_receivable.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use AccountReceivablePeer.account_receivable.CUSTOMER_NAME constant
     */
    public static String getAccountReceivable_CustomerName()
    {
        return "account_receivable.CUSTOMER_NAME";
    }
  
    /**
     * account_receivable.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use AccountReceivablePeer.account_receivable.CURRENCY_ID constant
     */
    public static String getAccountReceivable_CurrencyId()
    {
        return "account_receivable.CURRENCY_ID";
    }
  
    /**
     * account_receivable.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use AccountReceivablePeer.account_receivable.CURRENCY_RATE constant
     */
    public static String getAccountReceivable_CurrencyRate()
    {
        return "account_receivable.CURRENCY_RATE";
    }
  
    /**
     * account_receivable.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use AccountReceivablePeer.account_receivable.AMOUNT constant
     */
    public static String getAccountReceivable_Amount()
    {
        return "account_receivable.AMOUNT";
    }
  
    /**
     * account_receivable.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use AccountReceivablePeer.account_receivable.AMOUNT_BASE constant
     */
    public static String getAccountReceivable_AmountBase()
    {
        return "account_receivable.AMOUNT_BASE";
    }
  
    /**
     * account_receivable.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use AccountReceivablePeer.account_receivable.DUE_DATE constant
     */
    public static String getAccountReceivable_DueDate()
    {
        return "account_receivable.DUE_DATE";
    }
  
    /**
     * account_receivable.REMARK
     * @return the column name for the REMARK field
     * @deprecated use AccountReceivablePeer.account_receivable.REMARK constant
     */
    public static String getAccountReceivable_Remark()
    {
        return "account_receivable.REMARK";
    }
  
    /**
     * account_receivable.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use AccountReceivablePeer.account_receivable.LOCATION_ID constant
     */
    public static String getAccountReceivable_LocationId()
    {
        return "account_receivable.LOCATION_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("account_receivable");
        TableMap tMap = dbMap.getTable("account_receivable");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("account_receivable.ACCOUNT_RECEIVABLE_ID", "");
                          tMap.addColumn("account_receivable.TRANSACTION_ID", "");
                          tMap.addColumn("account_receivable.TRANSACTION_NO", "");
                          tMap.addColumn("account_receivable.TRANSACTION_DATE", new Date());
                            tMap.addColumn("account_receivable.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("account_receivable.CUSTOMER_ID", "");
                          tMap.addColumn("account_receivable.CUSTOMER_NAME", "");
                          tMap.addColumn("account_receivable.CURRENCY_ID", "");
                            tMap.addColumn("account_receivable.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("account_receivable.AMOUNT", bd_ZERO);
                            tMap.addColumn("account_receivable.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("account_receivable.DUE_DATE", new Date());
                          tMap.addColumn("account_receivable.REMARK", "");
                          tMap.addColumn("account_receivable.LOCATION_ID", "");
          }
}
