package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AdjustmentTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AdjustmentTypeMapBuilder";

    /**
     * Item
     * @deprecated use AdjustmentTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "adjustment_type";
    }

  
    /**
     * adjustment_type.ADJUSTMENT_TYPE_ID
     * @return the column name for the ADJUSTMENT_TYPE_ID field
     * @deprecated use AdjustmentTypePeer.adjustment_type.ADJUSTMENT_TYPE_ID constant
     */
    public static String getAdjustmentType_AdjustmentTypeId()
    {
        return "adjustment_type.ADJUSTMENT_TYPE_ID";
    }
  
    /**
     * adjustment_type.ADJUSTMENT_TYPE_CODE
     * @return the column name for the ADJUSTMENT_TYPE_CODE field
     * @deprecated use AdjustmentTypePeer.adjustment_type.ADJUSTMENT_TYPE_CODE constant
     */
    public static String getAdjustmentType_AdjustmentTypeCode()
    {
        return "adjustment_type.ADJUSTMENT_TYPE_CODE";
    }
  
    /**
     * adjustment_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use AdjustmentTypePeer.adjustment_type.DESCRIPTION constant
     */
    public static String getAdjustmentType_Description()
    {
        return "adjustment_type.DESCRIPTION";
    }
  
    /**
     * adjustment_type.DEFAULT_ACCOUNT_ID
     * @return the column name for the DEFAULT_ACCOUNT_ID field
     * @deprecated use AdjustmentTypePeer.adjustment_type.DEFAULT_ACCOUNT_ID constant
     */
    public static String getAdjustmentType_DefaultAccountId()
    {
        return "adjustment_type.DEFAULT_ACCOUNT_ID";
    }
  
    /**
     * adjustment_type.DEFAULT_TRANS_TYPE
     * @return the column name for the DEFAULT_TRANS_TYPE field
     * @deprecated use AdjustmentTypePeer.adjustment_type.DEFAULT_TRANS_TYPE constant
     */
    public static String getAdjustmentType_DefaultTransType()
    {
        return "adjustment_type.DEFAULT_TRANS_TYPE";
    }
  
    /**
     * adjustment_type.DEFAULT_COST_ADJ
     * @return the column name for the DEFAULT_COST_ADJ field
     * @deprecated use AdjustmentTypePeer.adjustment_type.DEFAULT_COST_ADJ constant
     */
    public static String getAdjustmentType_DefaultCostAdj()
    {
        return "adjustment_type.DEFAULT_COST_ADJ";
    }
  
    /**
     * adjustment_type.IS_TRANSFER
     * @return the column name for the IS_TRANSFER field
     * @deprecated use AdjustmentTypePeer.adjustment_type.IS_TRANSFER constant
     */
    public static String getAdjustmentType_IsTransfer()
    {
        return "adjustment_type.IS_TRANSFER";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("adjustment_type");
        TableMap tMap = dbMap.getTable("adjustment_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("adjustment_type.ADJUSTMENT_TYPE_ID", "");
                          tMap.addColumn("adjustment_type.ADJUSTMENT_TYPE_CODE", "");
                          tMap.addColumn("adjustment_type.DESCRIPTION", "");
                          tMap.addColumn("adjustment_type.DEFAULT_ACCOUNT_ID", "");
                            tMap.addColumn("adjustment_type.DEFAULT_TRANS_TYPE", Integer.valueOf(0));
                          tMap.addColumn("adjustment_type.DEFAULT_COST_ADJ", Boolean.TRUE);
                          tMap.addColumn("adjustment_type.IS_TRANSFER", Boolean.TRUE);
          }
}
