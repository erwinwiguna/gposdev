package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ApPaymentDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ApPaymentDetailMapBuilder";

    /**
     * Item
     * @deprecated use ApPaymentDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "ap_payment_detail";
    }

  
    /**
     * ap_payment_detail.AP_PAYMENT_DETAIL_ID
     * @return the column name for the AP_PAYMENT_DETAIL_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.AP_PAYMENT_DETAIL_ID constant
     */
    public static String getApPaymentDetail_ApPaymentDetailId()
    {
        return "ap_payment_detail.AP_PAYMENT_DETAIL_ID";
    }
  
    /**
     * ap_payment_detail.AP_PAYMENT_ID
     * @return the column name for the AP_PAYMENT_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.AP_PAYMENT_ID constant
     */
    public static String getApPaymentDetail_ApPaymentId()
    {
        return "ap_payment_detail.AP_PAYMENT_ID";
    }
  
    /**
     * ap_payment_detail.PURCHASE_INVOICE_ID
     * @return the column name for the PURCHASE_INVOICE_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.PURCHASE_INVOICE_ID constant
     */
    public static String getApPaymentDetail_PurchaseInvoiceId()
    {
        return "ap_payment_detail.PURCHASE_INVOICE_ID";
    }
  
    /**
     * ap_payment_detail.PURCHASE_INVOICE_NO
     * @return the column name for the PURCHASE_INVOICE_NO field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.PURCHASE_INVOICE_NO constant
     */
    public static String getApPaymentDetail_PurchaseInvoiceNo()
    {
        return "ap_payment_detail.PURCHASE_INVOICE_NO";
    }
  
    /**
     * ap_payment_detail.PURCHASE_INVOICE_AMOUNT
     * @return the column name for the PURCHASE_INVOICE_AMOUNT field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.PURCHASE_INVOICE_AMOUNT constant
     */
    public static String getApPaymentDetail_PurchaseInvoiceAmount()
    {
        return "ap_payment_detail.PURCHASE_INVOICE_AMOUNT";
    }
  
    /**
     * ap_payment_detail.INVOICE_RATE
     * @return the column name for the INVOICE_RATE field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.INVOICE_RATE constant
     */
    public static String getApPaymentDetail_InvoiceRate()
    {
        return "ap_payment_detail.INVOICE_RATE";
    }
  
    /**
     * ap_payment_detail.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.PAYMENT_AMOUNT constant
     */
    public static String getApPaymentDetail_PaymentAmount()
    {
        return "ap_payment_detail.PAYMENT_AMOUNT";
    }
  
    /**
     * ap_payment_detail.DOWN_PAYMENT
     * @return the column name for the DOWN_PAYMENT field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.DOWN_PAYMENT constant
     */
    public static String getApPaymentDetail_DownPayment()
    {
        return "ap_payment_detail.DOWN_PAYMENT";
    }
  
    /**
     * ap_payment_detail.PAID_AMOUNT
     * @return the column name for the PAID_AMOUNT field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.PAID_AMOUNT constant
     */
    public static String getApPaymentDetail_PaidAmount()
    {
        return "ap_payment_detail.PAID_AMOUNT";
    }
  
    /**
     * ap_payment_detail.DISCOUNT_AMOUNT
     * @return the column name for the DISCOUNT_AMOUNT field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.DISCOUNT_AMOUNT constant
     */
    public static String getApPaymentDetail_DiscountAmount()
    {
        return "ap_payment_detail.DISCOUNT_AMOUNT";
    }
  
    /**
     * ap_payment_detail.DISCOUNT_ACCOUNT_ID
     * @return the column name for the DISCOUNT_ACCOUNT_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.DISCOUNT_ACCOUNT_ID constant
     */
    public static String getApPaymentDetail_DiscountAccountId()
    {
        return "ap_payment_detail.DISCOUNT_ACCOUNT_ID";
    }
  
    /**
     * ap_payment_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.DEPARTMENT_ID constant
     */
    public static String getApPaymentDetail_DepartmentId()
    {
        return "ap_payment_detail.DEPARTMENT_ID";
    }
  
    /**
     * ap_payment_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.PROJECT_ID constant
     */
    public static String getApPaymentDetail_ProjectId()
    {
        return "ap_payment_detail.PROJECT_ID";
    }
  
    /**
     * ap_payment_detail.MEMO_ID
     * @return the column name for the MEMO_ID field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.MEMO_ID constant
     */
    public static String getApPaymentDetail_MemoId()
    {
        return "ap_payment_detail.MEMO_ID";
    }
  
    /**
     * ap_payment_detail.TAX_PAYMENT
     * @return the column name for the TAX_PAYMENT field
     * @deprecated use ApPaymentDetailPeer.ap_payment_detail.TAX_PAYMENT constant
     */
    public static String getApPaymentDetail_TaxPayment()
    {
        return "ap_payment_detail.TAX_PAYMENT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("ap_payment_detail");
        TableMap tMap = dbMap.getTable("ap_payment_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("ap_payment_detail.AP_PAYMENT_DETAIL_ID", "");
                          tMap.addForeignKey(
                "ap_payment_detail.AP_PAYMENT_ID", "" , "ap_payment" ,
                "ap_payment_id");
                          tMap.addColumn("ap_payment_detail.PURCHASE_INVOICE_ID", "");
                          tMap.addColumn("ap_payment_detail.PURCHASE_INVOICE_NO", "");
                            tMap.addColumn("ap_payment_detail.PURCHASE_INVOICE_AMOUNT", bd_ZERO);
                            tMap.addColumn("ap_payment_detail.INVOICE_RATE", bd_ZERO);
                            tMap.addColumn("ap_payment_detail.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("ap_payment_detail.DOWN_PAYMENT", bd_ZERO);
                            tMap.addColumn("ap_payment_detail.PAID_AMOUNT", bd_ZERO);
                            tMap.addColumn("ap_payment_detail.DISCOUNT_AMOUNT", bd_ZERO);
                          tMap.addColumn("ap_payment_detail.DISCOUNT_ACCOUNT_ID", "");
                          tMap.addColumn("ap_payment_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("ap_payment_detail.PROJECT_ID", "");
                          tMap.addColumn("ap_payment_detail.MEMO_ID", "");
                          tMap.addColumn("ap_payment_detail.TAX_PAYMENT", Boolean.TRUE);
          }
}
