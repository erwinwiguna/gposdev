package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ApPaymentMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ApPaymentMapBuilder";

    /**
     * Item
     * @deprecated use ApPaymentPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "ap_payment";
    }

  
    /**
     * ap_payment.AP_PAYMENT_ID
     * @return the column name for the AP_PAYMENT_ID field
     * @deprecated use ApPaymentPeer.ap_payment.AP_PAYMENT_ID constant
     */
    public static String getApPayment_ApPaymentId()
    {
        return "ap_payment.AP_PAYMENT_ID";
    }
  
    /**
     * ap_payment.AP_PAYMENT_NO
     * @return the column name for the AP_PAYMENT_NO field
     * @deprecated use ApPaymentPeer.ap_payment.AP_PAYMENT_NO constant
     */
    public static String getApPayment_ApPaymentNo()
    {
        return "ap_payment.AP_PAYMENT_NO";
    }
  
    /**
     * ap_payment.AP_PAYMENT_DATE
     * @return the column name for the AP_PAYMENT_DATE field
     * @deprecated use ApPaymentPeer.ap_payment.AP_PAYMENT_DATE constant
     */
    public static String getApPayment_ApPaymentDate()
    {
        return "ap_payment.AP_PAYMENT_DATE";
    }
  
    /**
     * ap_payment.AP_PAYMENT_DUE_DATE
     * @return the column name for the AP_PAYMENT_DUE_DATE field
     * @deprecated use ApPaymentPeer.ap_payment.AP_PAYMENT_DUE_DATE constant
     */
    public static String getApPayment_ApPaymentDueDate()
    {
        return "ap_payment.AP_PAYMENT_DUE_DATE";
    }
  
    /**
     * ap_payment.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use ApPaymentPeer.ap_payment.BANK_ID constant
     */
    public static String getApPayment_BankId()
    {
        return "ap_payment.BANK_ID";
    }
  
    /**
     * ap_payment.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use ApPaymentPeer.ap_payment.BANK_ISSUER constant
     */
    public static String getApPayment_BankIssuer()
    {
        return "ap_payment.BANK_ISSUER";
    }
  
    /**
     * ap_payment.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use ApPaymentPeer.ap_payment.VENDOR_ID constant
     */
    public static String getApPayment_VendorId()
    {
        return "ap_payment.VENDOR_ID";
    }
  
    /**
     * ap_payment.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use ApPaymentPeer.ap_payment.VENDOR_NAME constant
     */
    public static String getApPayment_VendorName()
    {
        return "ap_payment.VENDOR_NAME";
    }
  
    /**
     * ap_payment.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use ApPaymentPeer.ap_payment.REFERENCE_NO constant
     */
    public static String getApPayment_ReferenceNo()
    {
        return "ap_payment.REFERENCE_NO";
    }
  
    /**
     * ap_payment.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use ApPaymentPeer.ap_payment.PAYMENT_AMOUNT constant
     */
    public static String getApPayment_PaymentAmount()
    {
        return "ap_payment.PAYMENT_AMOUNT";
    }
  
    /**
     * ap_payment.TOTAL_DOWN_PAYMENT
     * @return the column name for the TOTAL_DOWN_PAYMENT field
     * @deprecated use ApPaymentPeer.ap_payment.TOTAL_DOWN_PAYMENT constant
     */
    public static String getApPayment_TotalDownPayment()
    {
        return "ap_payment.TOTAL_DOWN_PAYMENT";
    }
  
    /**
     * ap_payment.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use ApPaymentPeer.ap_payment.TOTAL_AMOUNT constant
     */
    public static String getApPayment_TotalAmount()
    {
        return "ap_payment.TOTAL_AMOUNT";
    }
  
    /**
     * ap_payment.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use ApPaymentPeer.ap_payment.USER_NAME constant
     */
    public static String getApPayment_UserName()
    {
        return "ap_payment.USER_NAME";
    }
  
    /**
     * ap_payment.REMARK
     * @return the column name for the REMARK field
     * @deprecated use ApPaymentPeer.ap_payment.REMARK constant
     */
    public static String getApPayment_Remark()
    {
        return "ap_payment.REMARK";
    }
  
    /**
     * ap_payment.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use ApPaymentPeer.ap_payment.CURRENCY_ID constant
     */
    public static String getApPayment_CurrencyId()
    {
        return "ap_payment.CURRENCY_ID";
    }
  
    /**
     * ap_payment.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use ApPaymentPeer.ap_payment.CURRENCY_RATE constant
     */
    public static String getApPayment_CurrencyRate()
    {
        return "ap_payment.CURRENCY_RATE";
    }
  
    /**
     * ap_payment.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use ApPaymentPeer.ap_payment.FISCAL_RATE constant
     */
    public static String getApPayment_FiscalRate()
    {
        return "ap_payment.FISCAL_RATE";
    }
  
    /**
     * ap_payment.STATUS
     * @return the column name for the STATUS field
     * @deprecated use ApPaymentPeer.ap_payment.STATUS constant
     */
    public static String getApPayment_Status()
    {
        return "ap_payment.STATUS";
    }
  
    /**
     * ap_payment.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use ApPaymentPeer.ap_payment.TOTAL_DISCOUNT constant
     */
    public static String getApPayment_TotalDiscount()
    {
        return "ap_payment.TOTAL_DISCOUNT";
    }
  
    /**
     * ap_payment.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use ApPaymentPeer.ap_payment.CASH_FLOW_TYPE_ID constant
     */
    public static String getApPayment_CashFlowTypeId()
    {
        return "ap_payment.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * ap_payment.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use ApPaymentPeer.ap_payment.LOCATION_ID constant
     */
    public static String getApPayment_LocationId()
    {
        return "ap_payment.LOCATION_ID";
    }
  
    /**
     * ap_payment.CF_STATUS
     * @return the column name for the CF_STATUS field
     * @deprecated use ApPaymentPeer.ap_payment.CF_STATUS constant
     */
    public static String getApPayment_CfStatus()
    {
        return "ap_payment.CF_STATUS";
    }
  
    /**
     * ap_payment.CF_DATE
     * @return the column name for the CF_DATE field
     * @deprecated use ApPaymentPeer.ap_payment.CF_DATE constant
     */
    public static String getApPayment_CfDate()
    {
        return "ap_payment.CF_DATE";
    }
  
    /**
     * ap_payment.CASH_FLOW_ID
     * @return the column name for the CASH_FLOW_ID field
     * @deprecated use ApPaymentPeer.ap_payment.CASH_FLOW_ID constant
     */
    public static String getApPayment_CashFlowId()
    {
        return "ap_payment.CASH_FLOW_ID";
    }
  
    /**
     * ap_payment.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use ApPaymentPeer.ap_payment.CANCEL_BY constant
     */
    public static String getApPayment_CancelBy()
    {
        return "ap_payment.CANCEL_BY";
    }
  
    /**
     * ap_payment.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use ApPaymentPeer.ap_payment.CANCEL_DATE constant
     */
    public static String getApPayment_CancelDate()
    {
        return "ap_payment.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("ap_payment");
        TableMap tMap = dbMap.getTable("ap_payment");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("ap_payment.AP_PAYMENT_ID", "");
                          tMap.addColumn("ap_payment.AP_PAYMENT_NO", "");
                          tMap.addColumn("ap_payment.AP_PAYMENT_DATE", new Date());
                          tMap.addColumn("ap_payment.AP_PAYMENT_DUE_DATE", new Date());
                          tMap.addColumn("ap_payment.BANK_ID", "");
                          tMap.addColumn("ap_payment.BANK_ISSUER", "");
                          tMap.addColumn("ap_payment.VENDOR_ID", "");
                          tMap.addColumn("ap_payment.VENDOR_NAME", "");
                          tMap.addColumn("ap_payment.REFERENCE_NO", "");
                            tMap.addColumn("ap_payment.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("ap_payment.TOTAL_DOWN_PAYMENT", bd_ZERO);
                            tMap.addColumn("ap_payment.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("ap_payment.USER_NAME", "");
                          tMap.addColumn("ap_payment.REMARK", "");
                          tMap.addColumn("ap_payment.CURRENCY_ID", "");
                            tMap.addColumn("ap_payment.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("ap_payment.FISCAL_RATE", bd_ZERO);
                            tMap.addColumn("ap_payment.STATUS", Integer.valueOf(0));
                            tMap.addColumn("ap_payment.TOTAL_DISCOUNT", bd_ZERO);
                          tMap.addColumn("ap_payment.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("ap_payment.LOCATION_ID", "");
                            tMap.addColumn("ap_payment.CF_STATUS", Integer.valueOf(0));
                          tMap.addColumn("ap_payment.CF_DATE", new Date());
                          tMap.addColumn("ap_payment.CASH_FLOW_ID", "");
                          tMap.addColumn("ap_payment.CANCEL_BY", "");
                          tMap.addColumn("ap_payment.CANCEL_DATE", new Date());
          }
}
