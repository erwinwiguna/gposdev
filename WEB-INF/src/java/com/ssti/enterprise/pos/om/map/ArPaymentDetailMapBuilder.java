package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ArPaymentDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ArPaymentDetailMapBuilder";

    /**
     * Item
     * @deprecated use ArPaymentDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "ar_payment_detail";
    }

  
    /**
     * ar_payment_detail.AR_PAYMENT_DETAIL_ID
     * @return the column name for the AR_PAYMENT_DETAIL_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.AR_PAYMENT_DETAIL_ID constant
     */
    public static String getArPaymentDetail_ArPaymentDetailId()
    {
        return "ar_payment_detail.AR_PAYMENT_DETAIL_ID";
    }
  
    /**
     * ar_payment_detail.AR_PAYMENT_ID
     * @return the column name for the AR_PAYMENT_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.AR_PAYMENT_ID constant
     */
    public static String getArPaymentDetail_ArPaymentId()
    {
        return "ar_payment_detail.AR_PAYMENT_ID";
    }
  
    /**
     * ar_payment_detail.SALES_TRANSACTION_ID
     * @return the column name for the SALES_TRANSACTION_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.SALES_TRANSACTION_ID constant
     */
    public static String getArPaymentDetail_SalesTransactionId()
    {
        return "ar_payment_detail.SALES_TRANSACTION_ID";
    }
  
    /**
     * ar_payment_detail.INVOICE_NO
     * @return the column name for the INVOICE_NO field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.INVOICE_NO constant
     */
    public static String getArPaymentDetail_InvoiceNo()
    {
        return "ar_payment_detail.INVOICE_NO";
    }
  
    /**
     * ar_payment_detail.SALES_TRANSACTION_AMOUNT
     * @return the column name for the SALES_TRANSACTION_AMOUNT field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.SALES_TRANSACTION_AMOUNT constant
     */
    public static String getArPaymentDetail_SalesTransactionAmount()
    {
        return "ar_payment_detail.SALES_TRANSACTION_AMOUNT";
    }
  
    /**
     * ar_payment_detail.INVOICE_RATE
     * @return the column name for the INVOICE_RATE field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.INVOICE_RATE constant
     */
    public static String getArPaymentDetail_InvoiceRate()
    {
        return "ar_payment_detail.INVOICE_RATE";
    }
  
    /**
     * ar_payment_detail.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.PAYMENT_AMOUNT constant
     */
    public static String getArPaymentDetail_PaymentAmount()
    {
        return "ar_payment_detail.PAYMENT_AMOUNT";
    }
  
    /**
     * ar_payment_detail.DOWN_PAYMENT
     * @return the column name for the DOWN_PAYMENT field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.DOWN_PAYMENT constant
     */
    public static String getArPaymentDetail_DownPayment()
    {
        return "ar_payment_detail.DOWN_PAYMENT";
    }
  
    /**
     * ar_payment_detail.PAID_AMOUNT
     * @return the column name for the PAID_AMOUNT field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.PAID_AMOUNT constant
     */
    public static String getArPaymentDetail_PaidAmount()
    {
        return "ar_payment_detail.PAID_AMOUNT";
    }
  
    /**
     * ar_payment_detail.DISCOUNT_AMOUNT
     * @return the column name for the DISCOUNT_AMOUNT field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.DISCOUNT_AMOUNT constant
     */
    public static String getArPaymentDetail_DiscountAmount()
    {
        return "ar_payment_detail.DISCOUNT_AMOUNT";
    }
  
    /**
     * ar_payment_detail.DISCOUNT_ACCOUNT_ID
     * @return the column name for the DISCOUNT_ACCOUNT_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.DISCOUNT_ACCOUNT_ID constant
     */
    public static String getArPaymentDetail_DiscountAccountId()
    {
        return "ar_payment_detail.DISCOUNT_ACCOUNT_ID";
    }
  
    /**
     * ar_payment_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.DEPARTMENT_ID constant
     */
    public static String getArPaymentDetail_DepartmentId()
    {
        return "ar_payment_detail.DEPARTMENT_ID";
    }
  
    /**
     * ar_payment_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.PROJECT_ID constant
     */
    public static String getArPaymentDetail_ProjectId()
    {
        return "ar_payment_detail.PROJECT_ID";
    }
  
    /**
     * ar_payment_detail.MEMO_ID
     * @return the column name for the MEMO_ID field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.MEMO_ID constant
     */
    public static String getArPaymentDetail_MemoId()
    {
        return "ar_payment_detail.MEMO_ID";
    }
  
    /**
     * ar_payment_detail.TAX_PAYMENT
     * @return the column name for the TAX_PAYMENT field
     * @deprecated use ArPaymentDetailPeer.ar_payment_detail.TAX_PAYMENT constant
     */
    public static String getArPaymentDetail_TaxPayment()
    {
        return "ar_payment_detail.TAX_PAYMENT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("ar_payment_detail");
        TableMap tMap = dbMap.getTable("ar_payment_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("ar_payment_detail.AR_PAYMENT_DETAIL_ID", "");
                          tMap.addForeignKey(
                "ar_payment_detail.AR_PAYMENT_ID", "" , "ar_payment" ,
                "ar_payment_id");
                          tMap.addColumn("ar_payment_detail.SALES_TRANSACTION_ID", "");
                          tMap.addColumn("ar_payment_detail.INVOICE_NO", "");
                            tMap.addColumn("ar_payment_detail.SALES_TRANSACTION_AMOUNT", bd_ZERO);
                            tMap.addColumn("ar_payment_detail.INVOICE_RATE", bd_ZERO);
                            tMap.addColumn("ar_payment_detail.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("ar_payment_detail.DOWN_PAYMENT", bd_ZERO);
                            tMap.addColumn("ar_payment_detail.PAID_AMOUNT", bd_ZERO);
                            tMap.addColumn("ar_payment_detail.DISCOUNT_AMOUNT", bd_ZERO);
                          tMap.addColumn("ar_payment_detail.DISCOUNT_ACCOUNT_ID", "");
                          tMap.addColumn("ar_payment_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("ar_payment_detail.PROJECT_ID", "");
                          tMap.addColumn("ar_payment_detail.MEMO_ID", "");
                          tMap.addColumn("ar_payment_detail.TAX_PAYMENT", Boolean.TRUE);
          }
}
