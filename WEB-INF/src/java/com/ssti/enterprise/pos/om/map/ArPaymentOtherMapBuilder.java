package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ArPaymentOtherMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ArPaymentOtherMapBuilder";

    /**
     * Item
     * @deprecated use ArPaymentOtherPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "ar_payment_other";
    }

  
    /**
     * ar_payment_other.AR_PAYMENT_OTHER_ID
     * @return the column name for the AR_PAYMENT_OTHER_ID field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.AR_PAYMENT_OTHER_ID constant
     */
    public static String getArPaymentOther_ArPaymentOtherId()
    {
        return "ar_payment_other.AR_PAYMENT_OTHER_ID";
    }
  
    /**
     * ar_payment_other.AR_PAYMENT_DETAIL_ID
     * @return the column name for the AR_PAYMENT_DETAIL_ID field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.AR_PAYMENT_DETAIL_ID constant
     */
    public static String getArPaymentOther_ArPaymentDetailId()
    {
        return "ar_payment_other.AR_PAYMENT_DETAIL_ID";
    }
  
    /**
     * ar_payment_other.AR_PAYMENT_ID
     * @return the column name for the AR_PAYMENT_ID field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.AR_PAYMENT_ID constant
     */
    public static String getArPaymentOther_ArPaymentId()
    {
        return "ar_payment_other.AR_PAYMENT_ID";
    }
  
    /**
     * ar_payment_other.OTHER_AMOUNT
     * @return the column name for the OTHER_AMOUNT field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.OTHER_AMOUNT constant
     */
    public static String getArPaymentOther_OtherAmount()
    {
        return "ar_payment_other.OTHER_AMOUNT";
    }
  
    /**
     * ar_payment_other.OTER_ACCOUNT_ID
     * @return the column name for the OTER_ACCOUNT_ID field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.OTER_ACCOUNT_ID constant
     */
    public static String getArPaymentOther_OterAccountId()
    {
        return "ar_payment_other.OTER_ACCOUNT_ID";
    }
  
    /**
     * ar_payment_other.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.DEPARTMENT_ID constant
     */
    public static String getArPaymentOther_DepartmentId()
    {
        return "ar_payment_other.DEPARTMENT_ID";
    }
  
    /**
     * ar_payment_other.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.PROJECT_ID constant
     */
    public static String getArPaymentOther_ProjectId()
    {
        return "ar_payment_other.PROJECT_ID";
    }
  
    /**
     * ar_payment_other.MEMO
     * @return the column name for the MEMO field
     * @deprecated use ArPaymentOtherPeer.ar_payment_other.MEMO constant
     */
    public static String getArPaymentOther_Memo()
    {
        return "ar_payment_other.MEMO";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("ar_payment_other");
        TableMap tMap = dbMap.getTable("ar_payment_other");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("ar_payment_other.AR_PAYMENT_OTHER_ID", "");
                          tMap.addForeignKey(
                "ar_payment_other.AR_PAYMENT_DETAIL_ID", "" , "ar_payment_detail" ,
                "ar_payment_detail_id");
                          tMap.addColumn("ar_payment_other.AR_PAYMENT_ID", "");
                            tMap.addColumn("ar_payment_other.OTHER_AMOUNT", bd_ZERO);
                          tMap.addColumn("ar_payment_other.OTER_ACCOUNT_ID", "");
                          tMap.addColumn("ar_payment_other.DEPARTMENT_ID", "");
                          tMap.addColumn("ar_payment_other.PROJECT_ID", "");
                          tMap.addColumn("ar_payment_other.MEMO", "");
          }
}
