package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BankTransferMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BankTransferMapBuilder";

    /**
     * Item
     * @deprecated use BankTransferPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "bank_transfer";
    }

  
    /**
     * bank_transfer.BANK_TRANSFER_ID
     * @return the column name for the BANK_TRANSFER_ID field
     * @deprecated use BankTransferPeer.bank_transfer.BANK_TRANSFER_ID constant
     */
    public static String getBankTransfer_BankTransferId()
    {
        return "bank_transfer.BANK_TRANSFER_ID";
    }
  
    /**
     * bank_transfer.TRANS_NO
     * @return the column name for the TRANS_NO field
     * @deprecated use BankTransferPeer.bank_transfer.TRANS_NO constant
     */
    public static String getBankTransfer_TransNo()
    {
        return "bank_transfer.TRANS_NO";
    }
  
    /**
     * bank_transfer.TRANS_DATE
     * @return the column name for the TRANS_DATE field
     * @deprecated use BankTransferPeer.bank_transfer.TRANS_DATE constant
     */
    public static String getBankTransfer_TransDate()
    {
        return "bank_transfer.TRANS_DATE";
    }
  
    /**
     * bank_transfer.OUT_BANK_ID
     * @return the column name for the OUT_BANK_ID field
     * @deprecated use BankTransferPeer.bank_transfer.OUT_BANK_ID constant
     */
    public static String getBankTransfer_OutBankId()
    {
        return "bank_transfer.OUT_BANK_ID";
    }
  
    /**
     * bank_transfer.OUT_ACC_ID
     * @return the column name for the OUT_ACC_ID field
     * @deprecated use BankTransferPeer.bank_transfer.OUT_ACC_ID constant
     */
    public static String getBankTransfer_OutAccId()
    {
        return "bank_transfer.OUT_ACC_ID";
    }
  
    /**
     * bank_transfer.OUT_LOC_ID
     * @return the column name for the OUT_LOC_ID field
     * @deprecated use BankTransferPeer.bank_transfer.OUT_LOC_ID constant
     */
    public static String getBankTransfer_OutLocId()
    {
        return "bank_transfer.OUT_LOC_ID";
    }
  
    /**
     * bank_transfer.OUT_CFT_ID
     * @return the column name for the OUT_CFT_ID field
     * @deprecated use BankTransferPeer.bank_transfer.OUT_CFT_ID constant
     */
    public static String getBankTransfer_OutCftId()
    {
        return "bank_transfer.OUT_CFT_ID";
    }
  
    /**
     * bank_transfer.OUT_CF_ID
     * @return the column name for the OUT_CF_ID field
     * @deprecated use BankTransferPeer.bank_transfer.OUT_CF_ID constant
     */
    public static String getBankTransfer_OutCfId()
    {
        return "bank_transfer.OUT_CF_ID";
    }
  
    /**
     * bank_transfer.IN_BANK_ID
     * @return the column name for the IN_BANK_ID field
     * @deprecated use BankTransferPeer.bank_transfer.IN_BANK_ID constant
     */
    public static String getBankTransfer_InBankId()
    {
        return "bank_transfer.IN_BANK_ID";
    }
  
    /**
     * bank_transfer.IN_ACC_ID
     * @return the column name for the IN_ACC_ID field
     * @deprecated use BankTransferPeer.bank_transfer.IN_ACC_ID constant
     */
    public static String getBankTransfer_InAccId()
    {
        return "bank_transfer.IN_ACC_ID";
    }
  
    /**
     * bank_transfer.IN_LOC_ID
     * @return the column name for the IN_LOC_ID field
     * @deprecated use BankTransferPeer.bank_transfer.IN_LOC_ID constant
     */
    public static String getBankTransfer_InLocId()
    {
        return "bank_transfer.IN_LOC_ID";
    }
  
    /**
     * bank_transfer.IN_CFT_ID
     * @return the column name for the IN_CFT_ID field
     * @deprecated use BankTransferPeer.bank_transfer.IN_CFT_ID constant
     */
    public static String getBankTransfer_InCftId()
    {
        return "bank_transfer.IN_CFT_ID";
    }
  
    /**
     * bank_transfer.IN_CF_ID
     * @return the column name for the IN_CF_ID field
     * @deprecated use BankTransferPeer.bank_transfer.IN_CF_ID constant
     */
    public static String getBankTransfer_InCfId()
    {
        return "bank_transfer.IN_CF_ID";
    }
  
    /**
     * bank_transfer.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use BankTransferPeer.bank_transfer.CURRENCY_ID constant
     */
    public static String getBankTransfer_CurrencyId()
    {
        return "bank_transfer.CURRENCY_ID";
    }
  
    /**
     * bank_transfer.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use BankTransferPeer.bank_transfer.CURRENCY_RATE constant
     */
    public static String getBankTransfer_CurrencyRate()
    {
        return "bank_transfer.CURRENCY_RATE";
    }
  
    /**
     * bank_transfer.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use BankTransferPeer.bank_transfer.AMOUNT constant
     */
    public static String getBankTransfer_Amount()
    {
        return "bank_transfer.AMOUNT";
    }
  
    /**
     * bank_transfer.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use BankTransferPeer.bank_transfer.AMOUNT_BASE constant
     */
    public static String getBankTransfer_AmountBase()
    {
        return "bank_transfer.AMOUNT_BASE";
    }
  
    /**
     * bank_transfer.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use BankTransferPeer.bank_transfer.USER_NAME constant
     */
    public static String getBankTransfer_UserName()
    {
        return "bank_transfer.USER_NAME";
    }
  
    /**
     * bank_transfer.STATUS
     * @return the column name for the STATUS field
     * @deprecated use BankTransferPeer.bank_transfer.STATUS constant
     */
    public static String getBankTransfer_Status()
    {
        return "bank_transfer.STATUS";
    }
  
    /**
     * bank_transfer.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use BankTransferPeer.bank_transfer.DESCRIPTION constant
     */
    public static String getBankTransfer_Description()
    {
        return "bank_transfer.DESCRIPTION";
    }
  
    /**
     * bank_transfer.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use BankTransferPeer.bank_transfer.CANCEL_BY constant
     */
    public static String getBankTransfer_CancelBy()
    {
        return "bank_transfer.CANCEL_BY";
    }
  
    /**
     * bank_transfer.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use BankTransferPeer.bank_transfer.CANCEL_DATE constant
     */
    public static String getBankTransfer_CancelDate()
    {
        return "bank_transfer.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("bank_transfer");
        TableMap tMap = dbMap.getTable("bank_transfer");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("bank_transfer.BANK_TRANSFER_ID", "");
                          tMap.addColumn("bank_transfer.TRANS_NO", "");
                          tMap.addColumn("bank_transfer.TRANS_DATE", new Date());
                          tMap.addColumn("bank_transfer.OUT_BANK_ID", "");
                          tMap.addColumn("bank_transfer.OUT_ACC_ID", "");
                          tMap.addColumn("bank_transfer.OUT_LOC_ID", "");
                          tMap.addColumn("bank_transfer.OUT_CFT_ID", "");
                          tMap.addColumn("bank_transfer.OUT_CF_ID", "");
                          tMap.addColumn("bank_transfer.IN_BANK_ID", "");
                          tMap.addColumn("bank_transfer.IN_ACC_ID", "");
                          tMap.addColumn("bank_transfer.IN_LOC_ID", "");
                          tMap.addColumn("bank_transfer.IN_CFT_ID", "");
                          tMap.addColumn("bank_transfer.IN_CF_ID", "");
                          tMap.addColumn("bank_transfer.CURRENCY_ID", "");
                            tMap.addColumn("bank_transfer.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("bank_transfer.AMOUNT", bd_ZERO);
                            tMap.addColumn("bank_transfer.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("bank_transfer.USER_NAME", "");
                            tMap.addColumn("bank_transfer.STATUS", Integer.valueOf(0));
                          tMap.addColumn("bank_transfer.DESCRIPTION", "");
                          tMap.addColumn("bank_transfer.CANCEL_BY", "");
                          tMap.addColumn("bank_transfer.CANCEL_DATE", new Date());
          }
}
