package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BrandMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BrandMapBuilder";

    /**
     * Item
     * @deprecated use BrandPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "brand";
    }

  
    /**
     * brand.BRAND_ID
     * @return the column name for the BRAND_ID field
     * @deprecated use BrandPeer.brand.BRAND_ID constant
     */
    public static String getBrand_BrandId()
    {
        return "brand.BRAND_ID";
    }
  
    /**
     * brand.BRAND_CODE
     * @return the column name for the BRAND_CODE field
     * @deprecated use BrandPeer.brand.BRAND_CODE constant
     */
    public static String getBrand_BrandCode()
    {
        return "brand.BRAND_CODE";
    }
  
    /**
     * brand.BRAND_NAME
     * @return the column name for the BRAND_NAME field
     * @deprecated use BrandPeer.brand.BRAND_NAME constant
     */
    public static String getBrand_BrandName()
    {
        return "brand.BRAND_NAME";
    }
  
    /**
     * brand.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use BrandPeer.brand.DESCRIPTION constant
     */
    public static String getBrand_Description()
    {
        return "brand.DESCRIPTION";
    }
  
    /**
     * brand.BRAND_LOGO
     * @return the column name for the BRAND_LOGO field
     * @deprecated use BrandPeer.brand.BRAND_LOGO constant
     */
    public static String getBrand_BrandLogo()
    {
        return "brand.BRAND_LOGO";
    }
  
    /**
     * brand.PRINCIPAL_ID
     * @return the column name for the PRINCIPAL_ID field
     * @deprecated use BrandPeer.brand.PRINCIPAL_ID constant
     */
    public static String getBrand_PrincipalId()
    {
        return "brand.PRINCIPAL_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("brand");
        TableMap tMap = dbMap.getTable("brand");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("brand.BRAND_ID", "");
                          tMap.addColumn("brand.BRAND_CODE", "");
                          tMap.addColumn("brand.BRAND_NAME", "");
                          tMap.addColumn("brand.DESCRIPTION", "");
                          tMap.addColumn("brand.BRAND_LOGO", "");
                          tMap.addColumn("brand.PRINCIPAL_ID", "");
          }
}
