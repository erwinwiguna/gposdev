package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CashierCouponMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CashierCouponMapBuilder";

    /**
     * Item
     * @deprecated use CashierCouponPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cashier_coupon";
    }

  
    /**
     * cashier_coupon.CASHIER_COUPON_ID
     * @return the column name for the CASHIER_COUPON_ID field
     * @deprecated use CashierCouponPeer.cashier_coupon.CASHIER_COUPON_ID constant
     */
    public static String getCashierCoupon_CashierCouponId()
    {
        return "cashier_coupon.CASHIER_COUPON_ID";
    }
  
    /**
     * cashier_coupon.CASHIER_BALANCE_ID
     * @return the column name for the CASHIER_BALANCE_ID field
     * @deprecated use CashierCouponPeer.cashier_coupon.CASHIER_BALANCE_ID constant
     */
    public static String getCashierCoupon_CashierBalanceId()
    {
        return "cashier_coupon.CASHIER_BALANCE_ID";
    }
  
    /**
     * cashier_coupon.COUPON_TYPE
     * @return the column name for the COUPON_TYPE field
     * @deprecated use CashierCouponPeer.cashier_coupon.COUPON_TYPE constant
     */
    public static String getCashierCoupon_CouponType()
    {
        return "cashier_coupon.COUPON_TYPE";
    }
  
    /**
     * cashier_coupon.COUPON_VALUE
     * @return the column name for the COUPON_VALUE field
     * @deprecated use CashierCouponPeer.cashier_coupon.COUPON_VALUE constant
     */
    public static String getCashierCoupon_CouponValue()
    {
        return "cashier_coupon.COUPON_VALUE";
    }
  
    /**
     * cashier_coupon.TR_QTY
     * @return the column name for the TR_QTY field
     * @deprecated use CashierCouponPeer.cashier_coupon.TR_QTY constant
     */
    public static String getCashierCoupon_TrQty()
    {
        return "cashier_coupon.TR_QTY";
    }
  
    /**
     * cashier_coupon.TR_AMOUNT
     * @return the column name for the TR_AMOUNT field
     * @deprecated use CashierCouponPeer.cashier_coupon.TR_AMOUNT constant
     */
    public static String getCashierCoupon_TrAmount()
    {
        return "cashier_coupon.TR_AMOUNT";
    }
  
    /**
     * cashier_coupon.IN_QTY
     * @return the column name for the IN_QTY field
     * @deprecated use CashierCouponPeer.cashier_coupon.IN_QTY constant
     */
    public static String getCashierCoupon_InQty()
    {
        return "cashier_coupon.IN_QTY";
    }
  
    /**
     * cashier_coupon.IN_AMOUNT
     * @return the column name for the IN_AMOUNT field
     * @deprecated use CashierCouponPeer.cashier_coupon.IN_AMOUNT constant
     */
    public static String getCashierCoupon_InAmount()
    {
        return "cashier_coupon.IN_AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cashier_coupon");
        TableMap tMap = dbMap.getTable("cashier_coupon");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cashier_coupon.CASHIER_COUPON_ID", "");
                          tMap.addForeignKey(
                "cashier_coupon.CASHIER_BALANCE_ID", "" , "cashier_balance" ,
                "cashier_balance_id");
                            tMap.addColumn("cashier_coupon.COUPON_TYPE", Integer.valueOf(0));
                            tMap.addColumn("cashier_coupon.COUPON_VALUE", Integer.valueOf(0));
                            tMap.addColumn("cashier_coupon.TR_QTY", Integer.valueOf(0));
                            tMap.addColumn("cashier_coupon.TR_AMOUNT", bd_ZERO);
                            tMap.addColumn("cashier_coupon.IN_QTY", Integer.valueOf(0));
                            tMap.addColumn("cashier_coupon.IN_AMOUNT", bd_ZERO);
          }
}
