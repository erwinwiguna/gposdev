package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CompanyDataMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CompanyDataMapBuilder";

    /**
     * Item
     * @deprecated use CompanyDataPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "company_data";
    }

  
    /**
     * company_data.COMPANY_DATA_ID
     * @return the column name for the COMPANY_DATA_ID field
     * @deprecated use CompanyDataPeer.company_data.COMPANY_DATA_ID constant
     */
    public static String getCompanyData_CompanyDataId()
    {
        return "company_data.COMPANY_DATA_ID";
    }
  
    /**
     * company_data.COMPANY_CODE
     * @return the column name for the COMPANY_CODE field
     * @deprecated use CompanyDataPeer.company_data.COMPANY_CODE constant
     */
    public static String getCompanyData_CompanyCode()
    {
        return "company_data.COMPANY_CODE";
    }
  
    /**
     * company_data.COMPANY_NAME
     * @return the column name for the COMPANY_NAME field
     * @deprecated use CompanyDataPeer.company_data.COMPANY_NAME constant
     */
    public static String getCompanyData_CompanyName()
    {
        return "company_data.COMPANY_NAME";
    }
  
    /**
     * company_data.ADDRESS
     * @return the column name for the ADDRESS field
     * @deprecated use CompanyDataPeer.company_data.ADDRESS constant
     */
    public static String getCompanyData_Address()
    {
        return "company_data.ADDRESS";
    }
  
    /**
     * company_data.COUNTRY
     * @return the column name for the COUNTRY field
     * @deprecated use CompanyDataPeer.company_data.COUNTRY constant
     */
    public static String getCompanyData_Country()
    {
        return "company_data.COUNTRY";
    }
  
    /**
     * company_data.PROVINCE
     * @return the column name for the PROVINCE field
     * @deprecated use CompanyDataPeer.company_data.PROVINCE constant
     */
    public static String getCompanyData_Province()
    {
        return "company_data.PROVINCE";
    }
  
    /**
     * company_data.CITY
     * @return the column name for the CITY field
     * @deprecated use CompanyDataPeer.company_data.CITY constant
     */
    public static String getCompanyData_City()
    {
        return "company_data.CITY";
    }
  
    /**
     * company_data.PHONE1
     * @return the column name for the PHONE1 field
     * @deprecated use CompanyDataPeer.company_data.PHONE1 constant
     */
    public static String getCompanyData_Phone1()
    {
        return "company_data.PHONE1";
    }
  
    /**
     * company_data.PHONE2
     * @return the column name for the PHONE2 field
     * @deprecated use CompanyDataPeer.company_data.PHONE2 constant
     */
    public static String getCompanyData_Phone2()
    {
        return "company_data.PHONE2";
    }
  
    /**
     * company_data.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use CompanyDataPeer.company_data.EMAIL constant
     */
    public static String getCompanyData_Email()
    {
        return "company_data.EMAIL";
    }
  
    /**
     * company_data.WEBSITE
     * @return the column name for the WEBSITE field
     * @deprecated use CompanyDataPeer.company_data.WEBSITE constant
     */
    public static String getCompanyData_Website()
    {
        return "company_data.WEBSITE";
    }
  
    /**
     * company_data.FAX
     * @return the column name for the FAX field
     * @deprecated use CompanyDataPeer.company_data.FAX constant
     */
    public static String getCompanyData_Fax()
    {
        return "company_data.FAX";
    }
  
    /**
     * company_data.TAX_NO
     * @return the column name for the TAX_NO field
     * @deprecated use CompanyDataPeer.company_data.TAX_NO constant
     */
    public static String getCompanyData_TaxNo()
    {
        return "company_data.TAX_NO";
    }
  
    /**
     * company_data.TAX_DATE
     * @return the column name for the TAX_DATE field
     * @deprecated use CompanyDataPeer.company_data.TAX_DATE constant
     */
    public static String getCompanyData_TaxDate()
    {
        return "company_data.TAX_DATE";
    }
  
    /**
     * company_data.COMPANY_LOGO
     * @return the column name for the COMPANY_LOGO field
     * @deprecated use CompanyDataPeer.company_data.COMPANY_LOGO constant
     */
    public static String getCompanyData_CompanyLogo()
    {
        return "company_data.COMPANY_LOGO";
    }
  
    /**
     * company_data.MAIN_LOGO
     * @return the column name for the MAIN_LOGO field
     * @deprecated use CompanyDataPeer.company_data.MAIN_LOGO constant
     */
    public static String getCompanyData_MainLogo()
    {
        return "company_data.MAIN_LOGO";
    }
  
    /**
     * company_data.POS_LOGO
     * @return the column name for the POS_LOGO field
     * @deprecated use CompanyDataPeer.company_data.POS_LOGO constant
     */
    public static String getCompanyData_PosLogo()
    {
        return "company_data.POS_LOGO";
    }
  
    /**
     * company_data.COMPANY_TYPE
     * @return the column name for the COMPANY_TYPE field
     * @deprecated use CompanyDataPeer.company_data.COMPANY_TYPE constant
     */
    public static String getCompanyData_CompanyType()
    {
        return "company_data.COMPANY_TYPE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("company_data");
        TableMap tMap = dbMap.getTable("company_data");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("company_data.COMPANY_DATA_ID", "");
                          tMap.addColumn("company_data.COMPANY_CODE", "");
                          tMap.addColumn("company_data.COMPANY_NAME", "");
                          tMap.addColumn("company_data.ADDRESS", "");
                          tMap.addColumn("company_data.COUNTRY", "");
                          tMap.addColumn("company_data.PROVINCE", "");
                          tMap.addColumn("company_data.CITY", "");
                          tMap.addColumn("company_data.PHONE1", "");
                          tMap.addColumn("company_data.PHONE2", "");
                          tMap.addColumn("company_data.EMAIL", "");
                          tMap.addColumn("company_data.WEBSITE", "");
                          tMap.addColumn("company_data.FAX", "");
                          tMap.addColumn("company_data.TAX_NO", "");
                          tMap.addColumn("company_data.TAX_DATE", "");
                          tMap.addColumn("company_data.COMPANY_LOGO", "");
                          tMap.addColumn("company_data.MAIN_LOGO", "");
                          tMap.addColumn("company_data.POS_LOGO", "");
                          tMap.addColumn("company_data.COMPANY_TYPE", "");
          }
}
