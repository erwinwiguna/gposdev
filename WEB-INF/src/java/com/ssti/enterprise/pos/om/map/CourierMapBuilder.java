package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CourierMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CourierMapBuilder";

    /**
     * Item
     * @deprecated use CourierPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "courier";
    }

  
    /**
     * courier.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use CourierPeer.courier.COURIER_ID constant
     */
    public static String getCourier_CourierId()
    {
        return "courier.COURIER_ID";
    }
  
    /**
     * courier.COURIER_NAME
     * @return the column name for the COURIER_NAME field
     * @deprecated use CourierPeer.courier.COURIER_NAME constant
     */
    public static String getCourier_CourierName()
    {
        return "courier.COURIER_NAME";
    }
  
    /**
     * courier.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CourierPeer.courier.DESCRIPTION constant
     */
    public static String getCourier_Description()
    {
        return "courier.DESCRIPTION";
    }
  
    /**
     * courier.SHIPPING_PRICE
     * @return the column name for the SHIPPING_PRICE field
     * @deprecated use CourierPeer.courier.SHIPPING_PRICE constant
     */
    public static String getCourier_ShippingPrice()
    {
        return "courier.SHIPPING_PRICE";
    }
  
    /**
     * courier.CAR_NO
     * @return the column name for the CAR_NO field
     * @deprecated use CourierPeer.courier.CAR_NO constant
     */
    public static String getCourier_CarNo()
    {
        return "courier.CAR_NO";
    }
  
    /**
     * courier.IS_INTERNAL
     * @return the column name for the IS_INTERNAL field
     * @deprecated use CourierPeer.courier.IS_INTERNAL constant
     */
    public static String getCourier_IsInternal()
    {
        return "courier.IS_INTERNAL";
    }
  
    /**
     * courier.EMPLOYEE_ID
     * @return the column name for the EMPLOYEE_ID field
     * @deprecated use CourierPeer.courier.EMPLOYEE_ID constant
     */
    public static String getCourier_EmployeeId()
    {
        return "courier.EMPLOYEE_ID";
    }
  
    /**
     * courier.ADDRESS
     * @return the column name for the ADDRESS field
     * @deprecated use CourierPeer.courier.ADDRESS constant
     */
    public static String getCourier_Address()
    {
        return "courier.ADDRESS";
    }
  
    /**
     * courier.PHONE1
     * @return the column name for the PHONE1 field
     * @deprecated use CourierPeer.courier.PHONE1 constant
     */
    public static String getCourier_Phone1()
    {
        return "courier.PHONE1";
    }
  
    /**
     * courier.PHONE2
     * @return the column name for the PHONE2 field
     * @deprecated use CourierPeer.courier.PHONE2 constant
     */
    public static String getCourier_Phone2()
    {
        return "courier.PHONE2";
    }
  
    /**
     * courier.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use CourierPeer.courier.EMAIL constant
     */
    public static String getCourier_Email()
    {
        return "courier.EMAIL";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("courier");
        TableMap tMap = dbMap.getTable("courier");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("courier.COURIER_ID", "");
                          tMap.addColumn("courier.COURIER_NAME", "");
                          tMap.addColumn("courier.DESCRIPTION", "");
                            tMap.addColumn("courier.SHIPPING_PRICE", bd_ZERO);
                          tMap.addColumn("courier.CAR_NO", "");
                          tMap.addColumn("courier.IS_INTERNAL", Boolean.TRUE);
                          tMap.addColumn("courier.EMPLOYEE_ID", "");
                          tMap.addColumn("courier.ADDRESS", "");
                          tMap.addColumn("courier.PHONE1", "");
                          tMap.addColumn("courier.PHONE2", "");
                          tMap.addColumn("courier.EMAIL", "");
          }
}
