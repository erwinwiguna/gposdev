package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CreditMemoMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CreditMemoMapBuilder";

    /**
     * Item
     * @deprecated use CreditMemoPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "credit_memo";
    }

  
    /**
     * credit_memo.CREDIT_MEMO_ID
     * @return the column name for the CREDIT_MEMO_ID field
     * @deprecated use CreditMemoPeer.credit_memo.CREDIT_MEMO_ID constant
     */
    public static String getCreditMemo_CreditMemoId()
    {
        return "credit_memo.CREDIT_MEMO_ID";
    }
  
    /**
     * credit_memo.CREDIT_MEMO_NO
     * @return the column name for the CREDIT_MEMO_NO field
     * @deprecated use CreditMemoPeer.credit_memo.CREDIT_MEMO_NO constant
     */
    public static String getCreditMemo_CreditMemoNo()
    {
        return "credit_memo.CREDIT_MEMO_NO";
    }
  
    /**
     * credit_memo.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use CreditMemoPeer.credit_memo.TRANSACTION_DATE constant
     */
    public static String getCreditMemo_TransactionDate()
    {
        return "credit_memo.TRANSACTION_DATE";
    }
  
    /**
     * credit_memo.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use CreditMemoPeer.credit_memo.TRANSACTION_ID constant
     */
    public static String getCreditMemo_TransactionId()
    {
        return "credit_memo.TRANSACTION_ID";
    }
  
    /**
     * credit_memo.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use CreditMemoPeer.credit_memo.TRANSACTION_NO constant
     */
    public static String getCreditMemo_TransactionNo()
    {
        return "credit_memo.TRANSACTION_NO";
    }
  
    /**
     * credit_memo.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use CreditMemoPeer.credit_memo.TRANSACTION_TYPE constant
     */
    public static String getCreditMemo_TransactionType()
    {
        return "credit_memo.TRANSACTION_TYPE";
    }
  
    /**
     * credit_memo.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CreditMemoPeer.credit_memo.CUSTOMER_ID constant
     */
    public static String getCreditMemo_CustomerId()
    {
        return "credit_memo.CUSTOMER_ID";
    }
  
    /**
     * credit_memo.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use CreditMemoPeer.credit_memo.CUSTOMER_NAME constant
     */
    public static String getCreditMemo_CustomerName()
    {
        return "credit_memo.CUSTOMER_NAME";
    }
  
    /**
     * credit_memo.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use CreditMemoPeer.credit_memo.CURRENCY_ID constant
     */
    public static String getCreditMemo_CurrencyId()
    {
        return "credit_memo.CURRENCY_ID";
    }
  
    /**
     * credit_memo.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use CreditMemoPeer.credit_memo.CURRENCY_RATE constant
     */
    public static String getCreditMemo_CurrencyRate()
    {
        return "credit_memo.CURRENCY_RATE";
    }
  
    /**
     * credit_memo.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use CreditMemoPeer.credit_memo.AMOUNT constant
     */
    public static String getCreditMemo_Amount()
    {
        return "credit_memo.AMOUNT";
    }
  
    /**
     * credit_memo.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use CreditMemoPeer.credit_memo.AMOUNT_BASE constant
     */
    public static String getCreditMemo_AmountBase()
    {
        return "credit_memo.AMOUNT_BASE";
    }
  
    /**
     * credit_memo.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use CreditMemoPeer.credit_memo.USER_NAME constant
     */
    public static String getCreditMemo_UserName()
    {
        return "credit_memo.USER_NAME";
    }
  
    /**
     * credit_memo.REMARK
     * @return the column name for the REMARK field
     * @deprecated use CreditMemoPeer.credit_memo.REMARK constant
     */
    public static String getCreditMemo_Remark()
    {
        return "credit_memo.REMARK";
    }
  
    /**
     * credit_memo.STATUS
     * @return the column name for the STATUS field
     * @deprecated use CreditMemoPeer.credit_memo.STATUS constant
     */
    public static String getCreditMemo_Status()
    {
        return "credit_memo.STATUS";
    }
  
    /**
     * credit_memo.CLOSED_DATE
     * @return the column name for the CLOSED_DATE field
     * @deprecated use CreditMemoPeer.credit_memo.CLOSED_DATE constant
     */
    public static String getCreditMemo_ClosedDate()
    {
        return "credit_memo.CLOSED_DATE";
    }
  
    /**
     * credit_memo.PAYMENT_TRANS_ID
     * @return the column name for the PAYMENT_TRANS_ID field
     * @deprecated use CreditMemoPeer.credit_memo.PAYMENT_TRANS_ID constant
     */
    public static String getCreditMemo_PaymentTransId()
    {
        return "credit_memo.PAYMENT_TRANS_ID";
    }
  
    /**
     * credit_memo.PAYMENT_INV_ID
     * @return the column name for the PAYMENT_INV_ID field
     * @deprecated use CreditMemoPeer.credit_memo.PAYMENT_INV_ID constant
     */
    public static String getCreditMemo_PaymentInvId()
    {
        return "credit_memo.PAYMENT_INV_ID";
    }
  
    /**
     * credit_memo.PAYMENT_TRANS_NO
     * @return the column name for the PAYMENT_TRANS_NO field
     * @deprecated use CreditMemoPeer.credit_memo.PAYMENT_TRANS_NO constant
     */
    public static String getCreditMemo_PaymentTransNo()
    {
        return "credit_memo.PAYMENT_TRANS_NO";
    }
  
    /**
     * credit_memo.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use CreditMemoPeer.credit_memo.BANK_ID constant
     */
    public static String getCreditMemo_BankId()
    {
        return "credit_memo.BANK_ID";
    }
  
    /**
     * credit_memo.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use CreditMemoPeer.credit_memo.BANK_ISSUER constant
     */
    public static String getCreditMemo_BankIssuer()
    {
        return "credit_memo.BANK_ISSUER";
    }
  
    /**
     * credit_memo.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use CreditMemoPeer.credit_memo.DUE_DATE constant
     */
    public static String getCreditMemo_DueDate()
    {
        return "credit_memo.DUE_DATE";
    }
  
    /**
     * credit_memo.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use CreditMemoPeer.credit_memo.REFERENCE_NO constant
     */
    public static String getCreditMemo_ReferenceNo()
    {
        return "credit_memo.REFERENCE_NO";
    }
  
    /**
     * credit_memo.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use CreditMemoPeer.credit_memo.ACCOUNT_ID constant
     */
    public static String getCreditMemo_AccountId()
    {
        return "credit_memo.ACCOUNT_ID";
    }
  
    /**
     * credit_memo.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use CreditMemoPeer.credit_memo.FISCAL_RATE constant
     */
    public static String getCreditMemo_FiscalRate()
    {
        return "credit_memo.FISCAL_RATE";
    }
  
    /**
     * credit_memo.CLOSING_RATE
     * @return the column name for the CLOSING_RATE field
     * @deprecated use CreditMemoPeer.credit_memo.CLOSING_RATE constant
     */
    public static String getCreditMemo_ClosingRate()
    {
        return "credit_memo.CLOSING_RATE";
    }
  
    /**
     * credit_memo.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use CreditMemoPeer.credit_memo.CASH_FLOW_TYPE_ID constant
     */
    public static String getCreditMemo_CashFlowTypeId()
    {
        return "credit_memo.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * credit_memo.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use CreditMemoPeer.credit_memo.LOCATION_ID constant
     */
    public static String getCreditMemo_LocationId()
    {
        return "credit_memo.LOCATION_ID";
    }
  
    /**
     * credit_memo.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use CreditMemoPeer.credit_memo.CANCEL_BY constant
     */
    public static String getCreditMemo_CancelBy()
    {
        return "credit_memo.CANCEL_BY";
    }
  
    /**
     * credit_memo.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use CreditMemoPeer.credit_memo.CANCEL_DATE constant
     */
    public static String getCreditMemo_CancelDate()
    {
        return "credit_memo.CANCEL_DATE";
    }
  
    /**
     * credit_memo.FROM_PAYMENT_ID
     * @return the column name for the FROM_PAYMENT_ID field
     * @deprecated use CreditMemoPeer.credit_memo.FROM_PAYMENT_ID constant
     */
    public static String getCreditMemo_FromPaymentId()
    {
        return "credit_memo.FROM_PAYMENT_ID";
    }
  
    /**
     * credit_memo.CROSS_ACCOUNT_ID
     * @return the column name for the CROSS_ACCOUNT_ID field
     * @deprecated use CreditMemoPeer.credit_memo.CROSS_ACCOUNT_ID constant
     */
    public static String getCreditMemo_CrossAccountId()
    {
        return "credit_memo.CROSS_ACCOUNT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("credit_memo");
        TableMap tMap = dbMap.getTable("credit_memo");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("credit_memo.CREDIT_MEMO_ID", "");
                          tMap.addColumn("credit_memo.CREDIT_MEMO_NO", "");
                          tMap.addColumn("credit_memo.TRANSACTION_DATE", new Date());
                          tMap.addColumn("credit_memo.TRANSACTION_ID", "");
                          tMap.addColumn("credit_memo.TRANSACTION_NO", "");
                            tMap.addColumn("credit_memo.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("credit_memo.CUSTOMER_ID", "");
                          tMap.addColumn("credit_memo.CUSTOMER_NAME", "");
                          tMap.addColumn("credit_memo.CURRENCY_ID", "");
                            tMap.addColumn("credit_memo.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("credit_memo.AMOUNT", bd_ZERO);
                            tMap.addColumn("credit_memo.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("credit_memo.USER_NAME", "");
                          tMap.addColumn("credit_memo.REMARK", "");
                            tMap.addColumn("credit_memo.STATUS", Integer.valueOf(0));
                          tMap.addColumn("credit_memo.CLOSED_DATE", new Date());
                          tMap.addColumn("credit_memo.PAYMENT_TRANS_ID", "");
                          tMap.addColumn("credit_memo.PAYMENT_INV_ID", "");
                          tMap.addColumn("credit_memo.PAYMENT_TRANS_NO", "");
                          tMap.addColumn("credit_memo.BANK_ID", "");
                          tMap.addColumn("credit_memo.BANK_ISSUER", "");
                          tMap.addColumn("credit_memo.DUE_DATE", new Date());
                          tMap.addColumn("credit_memo.REFERENCE_NO", "");
                          tMap.addColumn("credit_memo.ACCOUNT_ID", "");
                            tMap.addColumn("credit_memo.FISCAL_RATE", bd_ZERO);
                            tMap.addColumn("credit_memo.CLOSING_RATE", bd_ZERO);
                          tMap.addColumn("credit_memo.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("credit_memo.LOCATION_ID", "");
                          tMap.addColumn("credit_memo.CANCEL_BY", "");
                          tMap.addColumn("credit_memo.CANCEL_DATE", new Date());
                          tMap.addColumn("credit_memo.FROM_PAYMENT_ID", "");
                          tMap.addColumn("credit_memo.CROSS_ACCOUNT_ID", "");
          }
}
