package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CurrencyDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CurrencyDetailMapBuilder";

    /**
     * Item
     * @deprecated use CurrencyDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "currency_detail";
    }

  
    /**
     * currency_detail.CURRENCY_DETAIL_ID
     * @return the column name for the CURRENCY_DETAIL_ID field
     * @deprecated use CurrencyDetailPeer.currency_detail.CURRENCY_DETAIL_ID constant
     */
    public static String getCurrencyDetail_CurrencyDetailId()
    {
        return "currency_detail.CURRENCY_DETAIL_ID";
    }
  
    /**
     * currency_detail.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use CurrencyDetailPeer.currency_detail.CURRENCY_ID constant
     */
    public static String getCurrencyDetail_CurrencyId()
    {
        return "currency_detail.CURRENCY_ID";
    }
  
    /**
     * currency_detail.BEGIN_DATE
     * @return the column name for the BEGIN_DATE field
     * @deprecated use CurrencyDetailPeer.currency_detail.BEGIN_DATE constant
     */
    public static String getCurrencyDetail_BeginDate()
    {
        return "currency_detail.BEGIN_DATE";
    }
  
    /**
     * currency_detail.END_DATE
     * @return the column name for the END_DATE field
     * @deprecated use CurrencyDetailPeer.currency_detail.END_DATE constant
     */
    public static String getCurrencyDetail_EndDate()
    {
        return "currency_detail.END_DATE";
    }
  
    /**
     * currency_detail.EXCHANGE_RATE
     * @return the column name for the EXCHANGE_RATE field
     * @deprecated use CurrencyDetailPeer.currency_detail.EXCHANGE_RATE constant
     */
    public static String getCurrencyDetail_ExchangeRate()
    {
        return "currency_detail.EXCHANGE_RATE";
    }
  
    /**
     * currency_detail.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use CurrencyDetailPeer.currency_detail.FISCAL_RATE constant
     */
    public static String getCurrencyDetail_FiscalRate()
    {
        return "currency_detail.FISCAL_RATE";
    }
  
    /**
     * currency_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CurrencyDetailPeer.currency_detail.DESCRIPTION constant
     */
    public static String getCurrencyDetail_Description()
    {
        return "currency_detail.DESCRIPTION";
    }
  
    /**
     * currency_detail.ISSUE_DATE
     * @return the column name for the ISSUE_DATE field
     * @deprecated use CurrencyDetailPeer.currency_detail.ISSUE_DATE constant
     */
    public static String getCurrencyDetail_IssueDate()
    {
        return "currency_detail.ISSUE_DATE";
    }
  
    /**
     * currency_detail.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use CurrencyDetailPeer.currency_detail.UPDATE_DATE constant
     */
    public static String getCurrencyDetail_UpdateDate()
    {
        return "currency_detail.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("currency_detail");
        TableMap tMap = dbMap.getTable("currency_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("currency_detail.CURRENCY_DETAIL_ID", "");
                          tMap.addForeignKey(
                "currency_detail.CURRENCY_ID", "" , "currency" ,
                "currency_id");
                          tMap.addColumn("currency_detail.BEGIN_DATE", new Date());
                          tMap.addColumn("currency_detail.END_DATE", new Date());
                            tMap.addColumn("currency_detail.EXCHANGE_RATE", bd_ZERO);
                            tMap.addColumn("currency_detail.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("currency_detail.DESCRIPTION", "");
                          tMap.addColumn("currency_detail.ISSUE_DATE", new Date());
                          tMap.addColumn("currency_detail.UPDATE_DATE", new Date());
          }
}
