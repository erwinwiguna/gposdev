package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerAddressMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerAddressMapBuilder";

    /**
     * Item
     * @deprecated use CustomerAddressPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_address";
    }

  
    /**
     * customer_address.CUSTOMER_ADDRESS_ID
     * @return the column name for the CUSTOMER_ADDRESS_ID field
     * @deprecated use CustomerAddressPeer.customer_address.CUSTOMER_ADDRESS_ID constant
     */
    public static String getCustomerAddress_CustomerAddressId()
    {
        return "customer_address.CUSTOMER_ADDRESS_ID";
    }
  
    /**
     * customer_address.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerAddressPeer.customer_address.CUSTOMER_ID constant
     */
    public static String getCustomerAddress_CustomerId()
    {
        return "customer_address.CUSTOMER_ID";
    }
  
    /**
     * customer_address.CUSTOMER_CONTACT_ID
     * @return the column name for the CUSTOMER_CONTACT_ID field
     * @deprecated use CustomerAddressPeer.customer_address.CUSTOMER_CONTACT_ID constant
     */
    public static String getCustomerAddress_CustomerContactId()
    {
        return "customer_address.CUSTOMER_CONTACT_ID";
    }
  
    /**
     * customer_address.CONTACT_NAME
     * @return the column name for the CONTACT_NAME field
     * @deprecated use CustomerAddressPeer.customer_address.CONTACT_NAME constant
     */
    public static String getCustomerAddress_ContactName()
    {
        return "customer_address.CONTACT_NAME";
    }
  
    /**
     * customer_address.ADDRESS_TYPE
     * @return the column name for the ADDRESS_TYPE field
     * @deprecated use CustomerAddressPeer.customer_address.ADDRESS_TYPE constant
     */
    public static String getCustomerAddress_AddressType()
    {
        return "customer_address.ADDRESS_TYPE";
    }
  
    /**
     * customer_address.ADDRESS
     * @return the column name for the ADDRESS field
     * @deprecated use CustomerAddressPeer.customer_address.ADDRESS constant
     */
    public static String getCustomerAddress_Address()
    {
        return "customer_address.ADDRESS";
    }
  
    /**
     * customer_address.ADDRESS_REMARK
     * @return the column name for the ADDRESS_REMARK field
     * @deprecated use CustomerAddressPeer.customer_address.ADDRESS_REMARK constant
     */
    public static String getCustomerAddress_AddressRemark()
    {
        return "customer_address.ADDRESS_REMARK";
    }
  
    /**
     * customer_address.ZIP
     * @return the column name for the ZIP field
     * @deprecated use CustomerAddressPeer.customer_address.ZIP constant
     */
    public static String getCustomerAddress_Zip()
    {
        return "customer_address.ZIP";
    }
  
    /**
     * customer_address.COUNTRY
     * @return the column name for the COUNTRY field
     * @deprecated use CustomerAddressPeer.customer_address.COUNTRY constant
     */
    public static String getCustomerAddress_Country()
    {
        return "customer_address.COUNTRY";
    }
  
    /**
     * customer_address.PROVINCE
     * @return the column name for the PROVINCE field
     * @deprecated use CustomerAddressPeer.customer_address.PROVINCE constant
     */
    public static String getCustomerAddress_Province()
    {
        return "customer_address.PROVINCE";
    }
  
    /**
     * customer_address.CITY
     * @return the column name for the CITY field
     * @deprecated use CustomerAddressPeer.customer_address.CITY constant
     */
    public static String getCustomerAddress_City()
    {
        return "customer_address.CITY";
    }
  
    /**
     * customer_address.DISTRICT
     * @return the column name for the DISTRICT field
     * @deprecated use CustomerAddressPeer.customer_address.DISTRICT constant
     */
    public static String getCustomerAddress_District()
    {
        return "customer_address.DISTRICT";
    }
  
    /**
     * customer_address.VILLAGE
     * @return the column name for the VILLAGE field
     * @deprecated use CustomerAddressPeer.customer_address.VILLAGE constant
     */
    public static String getCustomerAddress_Village()
    {
        return "customer_address.VILLAGE";
    }
  
    /**
     * customer_address.PHONE1
     * @return the column name for the PHONE1 field
     * @deprecated use CustomerAddressPeer.customer_address.PHONE1 constant
     */
    public static String getCustomerAddress_Phone1()
    {
        return "customer_address.PHONE1";
    }
  
    /**
     * customer_address.PHONE2
     * @return the column name for the PHONE2 field
     * @deprecated use CustomerAddressPeer.customer_address.PHONE2 constant
     */
    public static String getCustomerAddress_Phone2()
    {
        return "customer_address.PHONE2";
    }
  
    /**
     * customer_address.FAX
     * @return the column name for the FAX field
     * @deprecated use CustomerAddressPeer.customer_address.FAX constant
     */
    public static String getCustomerAddress_Fax()
    {
        return "customer_address.FAX";
    }
  
    /**
     * customer_address.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use CustomerAddressPeer.customer_address.EMAIL constant
     */
    public static String getCustomerAddress_Email()
    {
        return "customer_address.EMAIL";
    }
  
    /**
     * customer_address.WEB_SITE
     * @return the column name for the WEB_SITE field
     * @deprecated use CustomerAddressPeer.customer_address.WEB_SITE constant
     */
    public static String getCustomerAddress_WebSite()
    {
        return "customer_address.WEB_SITE";
    }
  
    /**
     * customer_address.FIELD1
     * @return the column name for the FIELD1 field
     * @deprecated use CustomerAddressPeer.customer_address.FIELD1 constant
     */
    public static String getCustomerAddress_Field1()
    {
        return "customer_address.FIELD1";
    }
  
    /**
     * customer_address.FIELD2
     * @return the column name for the FIELD2 field
     * @deprecated use CustomerAddressPeer.customer_address.FIELD2 constant
     */
    public static String getCustomerAddress_Field2()
    {
        return "customer_address.FIELD2";
    }
  
    /**
     * customer_address.FIELD3
     * @return the column name for the FIELD3 field
     * @deprecated use CustomerAddressPeer.customer_address.FIELD3 constant
     */
    public static String getCustomerAddress_Field3()
    {
        return "customer_address.FIELD3";
    }
  
    /**
     * customer_address.FIELD4
     * @return the column name for the FIELD4 field
     * @deprecated use CustomerAddressPeer.customer_address.FIELD4 constant
     */
    public static String getCustomerAddress_Field4()
    {
        return "customer_address.FIELD4";
    }
  
    /**
     * customer_address.VALUE1
     * @return the column name for the VALUE1 field
     * @deprecated use CustomerAddressPeer.customer_address.VALUE1 constant
     */
    public static String getCustomerAddress_Value1()
    {
        return "customer_address.VALUE1";
    }
  
    /**
     * customer_address.VALUE2
     * @return the column name for the VALUE2 field
     * @deprecated use CustomerAddressPeer.customer_address.VALUE2 constant
     */
    public static String getCustomerAddress_Value2()
    {
        return "customer_address.VALUE2";
    }
  
    /**
     * customer_address.VALUE3
     * @return the column name for the VALUE3 field
     * @deprecated use CustomerAddressPeer.customer_address.VALUE3 constant
     */
    public static String getCustomerAddress_Value3()
    {
        return "customer_address.VALUE3";
    }
  
    /**
     * customer_address.VALUE4
     * @return the column name for the VALUE4 field
     * @deprecated use CustomerAddressPeer.customer_address.VALUE4 constant
     */
    public static String getCustomerAddress_Value4()
    {
        return "customer_address.VALUE4";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_address");
        TableMap tMap = dbMap.getTable("customer_address");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_address.CUSTOMER_ADDRESS_ID", "");
                          tMap.addForeignKey(
                "customer_address.CUSTOMER_ID", "" , "customer" ,
                "customer_id");
                          tMap.addColumn("customer_address.CUSTOMER_CONTACT_ID", "");
                          tMap.addColumn("customer_address.CONTACT_NAME", "");
                          tMap.addColumn("customer_address.ADDRESS_TYPE", "");
                          tMap.addColumn("customer_address.ADDRESS", "");
                          tMap.addColumn("customer_address.ADDRESS_REMARK", "");
                          tMap.addColumn("customer_address.ZIP", "");
                          tMap.addColumn("customer_address.COUNTRY", "");
                          tMap.addColumn("customer_address.PROVINCE", "");
                          tMap.addColumn("customer_address.CITY", "");
                          tMap.addColumn("customer_address.DISTRICT", "");
                          tMap.addColumn("customer_address.VILLAGE", "");
                          tMap.addColumn("customer_address.PHONE1", "");
                          tMap.addColumn("customer_address.PHONE2", "");
                          tMap.addColumn("customer_address.FAX", "");
                          tMap.addColumn("customer_address.EMAIL", "");
                          tMap.addColumn("customer_address.WEB_SITE", "");
                          tMap.addColumn("customer_address.FIELD1", "");
                          tMap.addColumn("customer_address.FIELD2", "");
                          tMap.addColumn("customer_address.FIELD3", "");
                          tMap.addColumn("customer_address.FIELD4", "");
                          tMap.addColumn("customer_address.VALUE1", "");
                          tMap.addColumn("customer_address.VALUE2", "");
                          tMap.addColumn("customer_address.VALUE3", "");
                          tMap.addColumn("customer_address.VALUE4", "");
          }
}
