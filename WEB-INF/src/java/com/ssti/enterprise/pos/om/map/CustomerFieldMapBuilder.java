package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerFieldMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerFieldMapBuilder";

    /**
     * Item
     * @deprecated use CustomerFieldPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_field";
    }

  
    /**
     * customer_field.CUSTOMER_FIELD_ID
     * @return the column name for the CUSTOMER_FIELD_ID field
     * @deprecated use CustomerFieldPeer.customer_field.CUSTOMER_FIELD_ID constant
     */
    public static String getCustomerField_CustomerFieldId()
    {
        return "customer_field.CUSTOMER_FIELD_ID";
    }
  
    /**
     * customer_field.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerFieldPeer.customer_field.CUSTOMER_ID constant
     */
    public static String getCustomerField_CustomerId()
    {
        return "customer_field.CUSTOMER_ID";
    }
  
    /**
     * customer_field.CFIELD_ID
     * @return the column name for the CFIELD_ID field
     * @deprecated use CustomerFieldPeer.customer_field.CFIELD_ID constant
     */
    public static String getCustomerField_CfieldId()
    {
        return "customer_field.CFIELD_ID";
    }
  
    /**
     * customer_field.FIELD_NAME
     * @return the column name for the FIELD_NAME field
     * @deprecated use CustomerFieldPeer.customer_field.FIELD_NAME constant
     */
    public static String getCustomerField_FieldName()
    {
        return "customer_field.FIELD_NAME";
    }
  
    /**
     * customer_field.FIELD_VALUE
     * @return the column name for the FIELD_VALUE field
     * @deprecated use CustomerFieldPeer.customer_field.FIELD_VALUE constant
     */
    public static String getCustomerField_FieldValue()
    {
        return "customer_field.FIELD_VALUE";
    }
  
    /**
     * customer_field.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use CustomerFieldPeer.customer_field.UPDATE_DATE constant
     */
    public static String getCustomerField_UpdateDate()
    {
        return "customer_field.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_field");
        TableMap tMap = dbMap.getTable("customer_field");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_field.CUSTOMER_FIELD_ID", "");
                          tMap.addColumn("customer_field.CUSTOMER_ID", "");
                          tMap.addColumn("customer_field.CFIELD_ID", "");
                          tMap.addColumn("customer_field.FIELD_NAME", "");
                          tMap.addColumn("customer_field.FIELD_VALUE", "");
                          tMap.addColumn("customer_field.UPDATE_DATE", new Date());
          }
}
