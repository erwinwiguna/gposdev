package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerMapBuilder";

    /**
     * Item
     * @deprecated use CustomerPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer";
    }

  
    /**
     * customer.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerPeer.customer.CUSTOMER_ID constant
     */
    public static String getCustomer_CustomerId()
    {
        return "customer.CUSTOMER_ID";
    }
  
    /**
     * customer.CUSTOMER_CODE
     * @return the column name for the CUSTOMER_CODE field
     * @deprecated use CustomerPeer.customer.CUSTOMER_CODE constant
     */
    public static String getCustomer_CustomerCode()
    {
        return "customer.CUSTOMER_CODE";
    }
  
    /**
     * customer.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use CustomerPeer.customer.CUSTOMER_NAME constant
     */
    public static String getCustomer_CustomerName()
    {
        return "customer.CUSTOMER_NAME";
    }
  
    /**
     * customer.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use CustomerPeer.customer.CUSTOMER_TYPE_ID constant
     */
    public static String getCustomer_CustomerTypeId()
    {
        return "customer.CUSTOMER_TYPE_ID";
    }
  
    /**
     * customer.STATUS_ID
     * @return the column name for the STATUS_ID field
     * @deprecated use CustomerPeer.customer.STATUS_ID constant
     */
    public static String getCustomer_StatusId()
    {
        return "customer.STATUS_ID";
    }
  
    /**
     * customer.INVOICE_ADDRESS
     * @return the column name for the INVOICE_ADDRESS field
     * @deprecated use CustomerPeer.customer.INVOICE_ADDRESS constant
     */
    public static String getCustomer_InvoiceAddress()
    {
        return "customer.INVOICE_ADDRESS";
    }
  
    /**
     * customer.TAX_NO
     * @return the column name for the TAX_NO field
     * @deprecated use CustomerPeer.customer.TAX_NO constant
     */
    public static String getCustomer_TaxNo()
    {
        return "customer.TAX_NO";
    }
  
    /**
     * customer.CONTACT_NAME1
     * @return the column name for the CONTACT_NAME1 field
     * @deprecated use CustomerPeer.customer.CONTACT_NAME1 constant
     */
    public static String getCustomer_ContactName1()
    {
        return "customer.CONTACT_NAME1";
    }
  
    /**
     * customer.CONTACT_NAME2
     * @return the column name for the CONTACT_NAME2 field
     * @deprecated use CustomerPeer.customer.CONTACT_NAME2 constant
     */
    public static String getCustomer_ContactName2()
    {
        return "customer.CONTACT_NAME2";
    }
  
    /**
     * customer.CONTACT_NAME3
     * @return the column name for the CONTACT_NAME3 field
     * @deprecated use CustomerPeer.customer.CONTACT_NAME3 constant
     */
    public static String getCustomer_ContactName3()
    {
        return "customer.CONTACT_NAME3";
    }
  
    /**
     * customer.JOB_TITLE1
     * @return the column name for the JOB_TITLE1 field
     * @deprecated use CustomerPeer.customer.JOB_TITLE1 constant
     */
    public static String getCustomer_JobTitle1()
    {
        return "customer.JOB_TITLE1";
    }
  
    /**
     * customer.JOB_TITLE2
     * @return the column name for the JOB_TITLE2 field
     * @deprecated use CustomerPeer.customer.JOB_TITLE2 constant
     */
    public static String getCustomer_JobTitle2()
    {
        return "customer.JOB_TITLE2";
    }
  
    /**
     * customer.JOB_TITLE3
     * @return the column name for the JOB_TITLE3 field
     * @deprecated use CustomerPeer.customer.JOB_TITLE3 constant
     */
    public static String getCustomer_JobTitle3()
    {
        return "customer.JOB_TITLE3";
    }
  
    /**
     * customer.CONTACT_PHONE1
     * @return the column name for the CONTACT_PHONE1 field
     * @deprecated use CustomerPeer.customer.CONTACT_PHONE1 constant
     */
    public static String getCustomer_ContactPhone1()
    {
        return "customer.CONTACT_PHONE1";
    }
  
    /**
     * customer.CONTACT_PHONE2
     * @return the column name for the CONTACT_PHONE2 field
     * @deprecated use CustomerPeer.customer.CONTACT_PHONE2 constant
     */
    public static String getCustomer_ContactPhone2()
    {
        return "customer.CONTACT_PHONE2";
    }
  
    /**
     * customer.CONTACT_PHONE3
     * @return the column name for the CONTACT_PHONE3 field
     * @deprecated use CustomerPeer.customer.CONTACT_PHONE3 constant
     */
    public static String getCustomer_ContactPhone3()
    {
        return "customer.CONTACT_PHONE3";
    }
  
    /**
     * customer.ADDRESS
     * @return the column name for the ADDRESS field
     * @deprecated use CustomerPeer.customer.ADDRESS constant
     */
    public static String getCustomer_Address()
    {
        return "customer.ADDRESS";
    }
  
    /**
     * customer.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use CustomerPeer.customer.SHIP_TO constant
     */
    public static String getCustomer_ShipTo()
    {
        return "customer.SHIP_TO";
    }
  
    /**
     * customer.ZIP
     * @return the column name for the ZIP field
     * @deprecated use CustomerPeer.customer.ZIP constant
     */
    public static String getCustomer_Zip()
    {
        return "customer.ZIP";
    }
  
    /**
     * customer.COUNTRY
     * @return the column name for the COUNTRY field
     * @deprecated use CustomerPeer.customer.COUNTRY constant
     */
    public static String getCustomer_Country()
    {
        return "customer.COUNTRY";
    }
  
    /**
     * customer.PROVINCE
     * @return the column name for the PROVINCE field
     * @deprecated use CustomerPeer.customer.PROVINCE constant
     */
    public static String getCustomer_Province()
    {
        return "customer.PROVINCE";
    }
  
    /**
     * customer.CITY
     * @return the column name for the CITY field
     * @deprecated use CustomerPeer.customer.CITY constant
     */
    public static String getCustomer_City()
    {
        return "customer.CITY";
    }
  
    /**
     * customer.DISTRICT
     * @return the column name for the DISTRICT field
     * @deprecated use CustomerPeer.customer.DISTRICT constant
     */
    public static String getCustomer_District()
    {
        return "customer.DISTRICT";
    }
  
    /**
     * customer.VILLAGE
     * @return the column name for the VILLAGE field
     * @deprecated use CustomerPeer.customer.VILLAGE constant
     */
    public static String getCustomer_Village()
    {
        return "customer.VILLAGE";
    }
  
    /**
     * customer.PHONE1
     * @return the column name for the PHONE1 field
     * @deprecated use CustomerPeer.customer.PHONE1 constant
     */
    public static String getCustomer_Phone1()
    {
        return "customer.PHONE1";
    }
  
    /**
     * customer.PHONE2
     * @return the column name for the PHONE2 field
     * @deprecated use CustomerPeer.customer.PHONE2 constant
     */
    public static String getCustomer_Phone2()
    {
        return "customer.PHONE2";
    }
  
    /**
     * customer.FAX
     * @return the column name for the FAX field
     * @deprecated use CustomerPeer.customer.FAX constant
     */
    public static String getCustomer_Fax()
    {
        return "customer.FAX";
    }
  
    /**
     * customer.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use CustomerPeer.customer.EMAIL constant
     */
    public static String getCustomer_Email()
    {
        return "customer.EMAIL";
    }
  
    /**
     * customer.WEB_SITE
     * @return the column name for the WEB_SITE field
     * @deprecated use CustomerPeer.customer.WEB_SITE constant
     */
    public static String getCustomer_WebSite()
    {
        return "customer.WEB_SITE";
    }
  
    /**
     * customer.DEFAULT_TAX_ID
     * @return the column name for the DEFAULT_TAX_ID field
     * @deprecated use CustomerPeer.customer.DEFAULT_TAX_ID constant
     */
    public static String getCustomer_DefaultTaxId()
    {
        return "customer.DEFAULT_TAX_ID";
    }
  
    /**
     * customer.DEFAULT_TYPE_ID
     * @return the column name for the DEFAULT_TYPE_ID field
     * @deprecated use CustomerPeer.customer.DEFAULT_TYPE_ID constant
     */
    public static String getCustomer_DefaultTypeId()
    {
        return "customer.DEFAULT_TYPE_ID";
    }
  
    /**
     * customer.DEFAULT_TERM_ID
     * @return the column name for the DEFAULT_TERM_ID field
     * @deprecated use CustomerPeer.customer.DEFAULT_TERM_ID constant
     */
    public static String getCustomer_DefaultTermId()
    {
        return "customer.DEFAULT_TERM_ID";
    }
  
    /**
     * customer.DEFAULT_EMPLOYEE_ID
     * @return the column name for the DEFAULT_EMPLOYEE_ID field
     * @deprecated use CustomerPeer.customer.DEFAULT_EMPLOYEE_ID constant
     */
    public static String getCustomer_DefaultEmployeeId()
    {
        return "customer.DEFAULT_EMPLOYEE_ID";
    }
  
    /**
     * customer.DEFAULT_CURRENCY_ID
     * @return the column name for the DEFAULT_CURRENCY_ID field
     * @deprecated use CustomerPeer.customer.DEFAULT_CURRENCY_ID constant
     */
    public static String getCustomer_DefaultCurrencyId()
    {
        return "customer.DEFAULT_CURRENCY_ID";
    }
  
    /**
     * customer.DEFAULT_LOCATION_ID
     * @return the column name for the DEFAULT_LOCATION_ID field
     * @deprecated use CustomerPeer.customer.DEFAULT_LOCATION_ID constant
     */
    public static String getCustomer_DefaultLocationId()
    {
        return "customer.DEFAULT_LOCATION_ID";
    }
  
    /**
     * customer.DEFAULT_DISCOUNT_AMOUNT
     * @return the column name for the DEFAULT_DISCOUNT_AMOUNT field
     * @deprecated use CustomerPeer.customer.DEFAULT_DISCOUNT_AMOUNT constant
     */
    public static String getCustomer_DefaultDiscountAmount()
    {
        return "customer.DEFAULT_DISCOUNT_AMOUNT";
    }
  
    /**
     * customer.CREDIT_LIMIT
     * @return the column name for the CREDIT_LIMIT field
     * @deprecated use CustomerPeer.customer.CREDIT_LIMIT constant
     */
    public static String getCustomer_CreditLimit()
    {
        return "customer.CREDIT_LIMIT";
    }
  
    /**
     * customer.MEMO
     * @return the column name for the MEMO field
     * @deprecated use CustomerPeer.customer.MEMO constant
     */
    public static String getCustomer_Memo()
    {
        return "customer.MEMO";
    }
  
    /**
     * customer.INVOICE_MESSAGE
     * @return the column name for the INVOICE_MESSAGE field
     * @deprecated use CustomerPeer.customer.INVOICE_MESSAGE constant
     */
    public static String getCustomer_InvoiceMessage()
    {
        return "customer.INVOICE_MESSAGE";
    }
  
    /**
     * customer.ADD_DATE
     * @return the column name for the ADD_DATE field
     * @deprecated use CustomerPeer.customer.ADD_DATE constant
     */
    public static String getCustomer_AddDate()
    {
        return "customer.ADD_DATE";
    }
  
    /**
     * customer.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use CustomerPeer.customer.UPDATE_DATE constant
     */
    public static String getCustomer_UpdateDate()
    {
        return "customer.UPDATE_DATE";
    }
  
    /**
     * customer.LAST_UPDATE_LOCATION_ID
     * @return the column name for the LAST_UPDATE_LOCATION_ID field
     * @deprecated use CustomerPeer.customer.LAST_UPDATE_LOCATION_ID constant
     */
    public static String getCustomer_LastUpdateLocationId()
    {
        return "customer.LAST_UPDATE_LOCATION_ID";
    }
  
    /**
     * customer.IS_DEFAULT
     * @return the column name for the IS_DEFAULT field
     * @deprecated use CustomerPeer.customer.IS_DEFAULT constant
     */
    public static String getCustomer_IsDefault()
    {
        return "customer.IS_DEFAULT";
    }
  
    /**
     * customer.IS_ACTIVE
     * @return the column name for the IS_ACTIVE field
     * @deprecated use CustomerPeer.customer.IS_ACTIVE constant
     */
    public static String getCustomer_IsActive()
    {
        return "customer.IS_ACTIVE";
    }
  
    /**
     * customer.FIELD1
     * @return the column name for the FIELD1 field
     * @deprecated use CustomerPeer.customer.FIELD1 constant
     */
    public static String getCustomer_Field1()
    {
        return "customer.FIELD1";
    }
  
    /**
     * customer.FIELD2
     * @return the column name for the FIELD2 field
     * @deprecated use CustomerPeer.customer.FIELD2 constant
     */
    public static String getCustomer_Field2()
    {
        return "customer.FIELD2";
    }
  
    /**
     * customer.VALUE1
     * @return the column name for the VALUE1 field
     * @deprecated use CustomerPeer.customer.VALUE1 constant
     */
    public static String getCustomer_Value1()
    {
        return "customer.VALUE1";
    }
  
    /**
     * customer.VALUE2
     * @return the column name for the VALUE2 field
     * @deprecated use CustomerPeer.customer.VALUE2 constant
     */
    public static String getCustomer_Value2()
    {
        return "customer.VALUE2";
    }
  
    /**
     * customer.AR_ACCOUNT
     * @return the column name for the AR_ACCOUNT field
     * @deprecated use CustomerPeer.customer.AR_ACCOUNT constant
     */
    public static String getCustomer_ArAccount()
    {
        return "customer.AR_ACCOUNT";
    }
  
    /**
     * customer.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use CustomerPeer.customer.OPENING_BALANCE constant
     */
    public static String getCustomer_OpeningBalance()
    {
        return "customer.OPENING_BALANCE";
    }
  
    /**
     * customer.AS_DATE
     * @return the column name for the AS_DATE field
     * @deprecated use CustomerPeer.customer.AS_DATE constant
     */
    public static String getCustomer_AsDate()
    {
        return "customer.AS_DATE";
    }
  
    /**
     * customer.OB_TRANS_ID
     * @return the column name for the OB_TRANS_ID field
     * @deprecated use CustomerPeer.customer.OB_TRANS_ID constant
     */
    public static String getCustomer_ObTransId()
    {
        return "customer.OB_TRANS_ID";
    }
  
    /**
     * customer.OB_RATE
     * @return the column name for the OB_RATE field
     * @deprecated use CustomerPeer.customer.OB_RATE constant
     */
    public static String getCustomer_ObRate()
    {
        return "customer.OB_RATE";
    }
  
    /**
     * customer.BIRTH_DATE
     * @return the column name for the BIRTH_DATE field
     * @deprecated use CustomerPeer.customer.BIRTH_DATE constant
     */
    public static String getCustomer_BirthDate()
    {
        return "customer.BIRTH_DATE";
    }
  
    /**
     * customer.BIRTH_PLACE
     * @return the column name for the BIRTH_PLACE field
     * @deprecated use CustomerPeer.customer.BIRTH_PLACE constant
     */
    public static String getCustomer_BirthPlace()
    {
        return "customer.BIRTH_PLACE";
    }
  
    /**
     * customer.RELIGION
     * @return the column name for the RELIGION field
     * @deprecated use CustomerPeer.customer.RELIGION constant
     */
    public static String getCustomer_Religion()
    {
        return "customer.RELIGION";
    }
  
    /**
     * customer.GENDER
     * @return the column name for the GENDER field
     * @deprecated use CustomerPeer.customer.GENDER constant
     */
    public static String getCustomer_Gender()
    {
        return "customer.GENDER";
    }
  
    /**
     * customer.OCCUPATION
     * @return the column name for the OCCUPATION field
     * @deprecated use CustomerPeer.customer.OCCUPATION constant
     */
    public static String getCustomer_Occupation()
    {
        return "customer.OCCUPATION";
    }
  
    /**
     * customer.HOBBY
     * @return the column name for the HOBBY field
     * @deprecated use CustomerPeer.customer.HOBBY constant
     */
    public static String getCustomer_Hobby()
    {
        return "customer.HOBBY";
    }
  
    /**
     * customer.TRANS_PREFIX
     * @return the column name for the TRANS_PREFIX field
     * @deprecated use CustomerPeer.customer.TRANS_PREFIX constant
     */
    public static String getCustomer_TransPrefix()
    {
        return "customer.TRANS_PREFIX";
    }
  
    /**
     * customer.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use CustomerPeer.customer.CREATE_BY constant
     */
    public static String getCustomer_CreateBy()
    {
        return "customer.CREATE_BY";
    }
  
    /**
     * customer.LAST_UPDATE_BY
     * @return the column name for the LAST_UPDATE_BY field
     * @deprecated use CustomerPeer.customer.LAST_UPDATE_BY constant
     */
    public static String getCustomer_LastUpdateBy()
    {
        return "customer.LAST_UPDATE_BY";
    }
  
    /**
     * customer.SALES_AREA_ID
     * @return the column name for the SALES_AREA_ID field
     * @deprecated use CustomerPeer.customer.SALES_AREA_ID constant
     */
    public static String getCustomer_SalesAreaId()
    {
        return "customer.SALES_AREA_ID";
    }
  
    /**
     * customer.TERRITORY_ID
     * @return the column name for the TERRITORY_ID field
     * @deprecated use CustomerPeer.customer.TERRITORY_ID constant
     */
    public static String getCustomer_TerritoryId()
    {
        return "customer.TERRITORY_ID";
    }
  
    /**
     * customer.LONGITUDES
     * @return the column name for the LONGITUDES field
     * @deprecated use CustomerPeer.customer.LONGITUDES constant
     */
    public static String getCustomer_Longitudes()
    {
        return "customer.LONGITUDES";
    }
  
    /**
     * customer.LATITUDES
     * @return the column name for the LATITUDES field
     * @deprecated use CustomerPeer.customer.LATITUDES constant
     */
    public static String getCustomer_Latitudes()
    {
        return "customer.LATITUDES";
    }
  
    /**
     * customer.BILL_TO_ID
     * @return the column name for the BILL_TO_ID field
     * @deprecated use CustomerPeer.customer.BILL_TO_ID constant
     */
    public static String getCustomer_BillToId()
    {
        return "customer.BILL_TO_ID";
    }
  
    /**
     * customer.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use CustomerPeer.customer.PARENT_ID constant
     */
    public static String getCustomer_ParentId()
    {
        return "customer.PARENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer");
        TableMap tMap = dbMap.getTable("customer");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer.CUSTOMER_ID", "");
                          tMap.addColumn("customer.CUSTOMER_CODE", "");
                          tMap.addColumn("customer.CUSTOMER_NAME", "");
                          tMap.addColumn("customer.CUSTOMER_TYPE_ID", "");
                          tMap.addColumn("customer.STATUS_ID", "");
                          tMap.addColumn("customer.INVOICE_ADDRESS", "");
                          tMap.addColumn("customer.TAX_NO", "");
                          tMap.addColumn("customer.CONTACT_NAME1", "");
                          tMap.addColumn("customer.CONTACT_NAME2", "");
                          tMap.addColumn("customer.CONTACT_NAME3", "");
                          tMap.addColumn("customer.JOB_TITLE1", "");
                          tMap.addColumn("customer.JOB_TITLE2", "");
                          tMap.addColumn("customer.JOB_TITLE3", "");
                          tMap.addColumn("customer.CONTACT_PHONE1", "");
                          tMap.addColumn("customer.CONTACT_PHONE2", "");
                          tMap.addColumn("customer.CONTACT_PHONE3", "");
                          tMap.addColumn("customer.ADDRESS", "");
                          tMap.addColumn("customer.SHIP_TO", "");
                          tMap.addColumn("customer.ZIP", "");
                          tMap.addColumn("customer.COUNTRY", "");
                          tMap.addColumn("customer.PROVINCE", "");
                          tMap.addColumn("customer.CITY", "");
                          tMap.addColumn("customer.DISTRICT", "");
                          tMap.addColumn("customer.VILLAGE", "");
                          tMap.addColumn("customer.PHONE1", "");
                          tMap.addColumn("customer.PHONE2", "");
                          tMap.addColumn("customer.FAX", "");
                          tMap.addColumn("customer.EMAIL", "");
                          tMap.addColumn("customer.WEB_SITE", "");
                          tMap.addColumn("customer.DEFAULT_TAX_ID", "");
                          tMap.addColumn("customer.DEFAULT_TYPE_ID", "");
                          tMap.addColumn("customer.DEFAULT_TERM_ID", "");
                          tMap.addColumn("customer.DEFAULT_EMPLOYEE_ID", "");
                          tMap.addColumn("customer.DEFAULT_CURRENCY_ID", "");
                          tMap.addColumn("customer.DEFAULT_LOCATION_ID", "");
                          tMap.addColumn("customer.DEFAULT_DISCOUNT_AMOUNT", "");
                            tMap.addColumn("customer.CREDIT_LIMIT", bd_ZERO);
                          tMap.addColumn("customer.MEMO", "");
                          tMap.addColumn("customer.INVOICE_MESSAGE", "");
                          tMap.addColumn("customer.ADD_DATE", new Date());
                          tMap.addColumn("customer.UPDATE_DATE", new Date());
                          tMap.addColumn("customer.LAST_UPDATE_LOCATION_ID", "");
                          tMap.addColumn("customer.IS_DEFAULT", Boolean.TRUE);
                          tMap.addColumn("customer.IS_ACTIVE", Boolean.TRUE);
                          tMap.addColumn("customer.FIELD1", "");
                          tMap.addColumn("customer.FIELD2", "");
                          tMap.addColumn("customer.VALUE1", "");
                          tMap.addColumn("customer.VALUE2", "");
                          tMap.addColumn("customer.AR_ACCOUNT", "");
                            tMap.addColumn("customer.OPENING_BALANCE", bd_ZERO);
                          tMap.addColumn("customer.AS_DATE", new Date());
                          tMap.addColumn("customer.OB_TRANS_ID", "");
                            tMap.addColumn("customer.OB_RATE", bd_ZERO);
                          tMap.addColumn("customer.BIRTH_DATE", new Date());
                          tMap.addColumn("customer.BIRTH_PLACE", "");
                          tMap.addColumn("customer.RELIGION", "");
                            tMap.addColumn("customer.GENDER", Integer.valueOf(0));
                          tMap.addColumn("customer.OCCUPATION", "");
                          tMap.addColumn("customer.HOBBY", "");
                          tMap.addColumn("customer.TRANS_PREFIX", "");
                          tMap.addColumn("customer.CREATE_BY", "");
                          tMap.addColumn("customer.LAST_UPDATE_BY", "");
                          tMap.addColumn("customer.SALES_AREA_ID", "");
                          tMap.addColumn("customer.TERRITORY_ID", "");
                            tMap.addColumn("customer.LONGITUDES", bd_ZERO);
                            tMap.addColumn("customer.LATITUDES", bd_ZERO);
                          tMap.addColumn("customer.BILL_TO_ID", "");
                          tMap.addColumn("customer.PARENT_ID", "");
          }
}
