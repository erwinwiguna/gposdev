package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DatabaseBackupMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DatabaseBackupMapBuilder";

    /**
     * Item
     * @deprecated use DatabaseBackupPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "database_backup";
    }

  
    /**
     * database_backup.DATABASE_BACKUP_ID
     * @return the column name for the DATABASE_BACKUP_ID field
     * @deprecated use DatabaseBackupPeer.database_backup.DATABASE_BACKUP_ID constant
     */
    public static String getDatabaseBackup_DatabaseBackupId()
    {
        return "database_backup.DATABASE_BACKUP_ID";
    }
  
    /**
     * database_backup.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use DatabaseBackupPeer.database_backup.LOCATION_ID constant
     */
    public static String getDatabaseBackup_LocationId()
    {
        return "database_backup.LOCATION_ID";
    }
  
    /**
     * database_backup.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use DatabaseBackupPeer.database_backup.LOCATION_NAME constant
     */
    public static String getDatabaseBackup_LocationName()
    {
        return "database_backup.LOCATION_NAME";
    }
  
    /**
     * database_backup.BACKUP_DATE
     * @return the column name for the BACKUP_DATE field
     * @deprecated use DatabaseBackupPeer.database_backup.BACKUP_DATE constant
     */
    public static String getDatabaseBackup_BackupDate()
    {
        return "database_backup.BACKUP_DATE";
    }
  
    /**
     * database_backup.FILE_NAME
     * @return the column name for the FILE_NAME field
     * @deprecated use DatabaseBackupPeer.database_backup.FILE_NAME constant
     */
    public static String getDatabaseBackup_FileName()
    {
        return "database_backup.FILE_NAME";
    }
  
    /**
     * database_backup.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use DatabaseBackupPeer.database_backup.USER_NAME constant
     */
    public static String getDatabaseBackup_UserName()
    {
        return "database_backup.USER_NAME";
    }
  
    /**
     * database_backup.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use DatabaseBackupPeer.database_backup.DESCRIPTION constant
     */
    public static String getDatabaseBackup_Description()
    {
        return "database_backup.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("database_backup");
        TableMap tMap = dbMap.getTable("database_backup");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("database_backup.DATABASE_BACKUP_ID", "");
                          tMap.addColumn("database_backup.LOCATION_ID", "");
                          tMap.addColumn("database_backup.LOCATION_NAME", "");
                          tMap.addColumn("database_backup.BACKUP_DATE", new Date());
                          tMap.addColumn("database_backup.FILE_NAME", "");
                          tMap.addColumn("database_backup.USER_NAME", "");
                          tMap.addColumn("database_backup.DESCRIPTION", "");
          }
}
