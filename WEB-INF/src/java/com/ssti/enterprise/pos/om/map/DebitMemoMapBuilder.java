package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DebitMemoMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DebitMemoMapBuilder";

    /**
     * Item
     * @deprecated use DebitMemoPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "debit_memo";
    }

  
    /**
     * debit_memo.DEBIT_MEMO_ID
     * @return the column name for the DEBIT_MEMO_ID field
     * @deprecated use DebitMemoPeer.debit_memo.DEBIT_MEMO_ID constant
     */
    public static String getDebitMemo_DebitMemoId()
    {
        return "debit_memo.DEBIT_MEMO_ID";
    }
  
    /**
     * debit_memo.DEBIT_MEMO_NO
     * @return the column name for the DEBIT_MEMO_NO field
     * @deprecated use DebitMemoPeer.debit_memo.DEBIT_MEMO_NO constant
     */
    public static String getDebitMemo_DebitMemoNo()
    {
        return "debit_memo.DEBIT_MEMO_NO";
    }
  
    /**
     * debit_memo.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use DebitMemoPeer.debit_memo.TRANSACTION_DATE constant
     */
    public static String getDebitMemo_TransactionDate()
    {
        return "debit_memo.TRANSACTION_DATE";
    }
  
    /**
     * debit_memo.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use DebitMemoPeer.debit_memo.TRANSACTION_ID constant
     */
    public static String getDebitMemo_TransactionId()
    {
        return "debit_memo.TRANSACTION_ID";
    }
  
    /**
     * debit_memo.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use DebitMemoPeer.debit_memo.TRANSACTION_NO constant
     */
    public static String getDebitMemo_TransactionNo()
    {
        return "debit_memo.TRANSACTION_NO";
    }
  
    /**
     * debit_memo.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use DebitMemoPeer.debit_memo.TRANSACTION_TYPE constant
     */
    public static String getDebitMemo_TransactionType()
    {
        return "debit_memo.TRANSACTION_TYPE";
    }
  
    /**
     * debit_memo.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use DebitMemoPeer.debit_memo.VENDOR_ID constant
     */
    public static String getDebitMemo_VendorId()
    {
        return "debit_memo.VENDOR_ID";
    }
  
    /**
     * debit_memo.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use DebitMemoPeer.debit_memo.VENDOR_NAME constant
     */
    public static String getDebitMemo_VendorName()
    {
        return "debit_memo.VENDOR_NAME";
    }
  
    /**
     * debit_memo.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use DebitMemoPeer.debit_memo.CURRENCY_ID constant
     */
    public static String getDebitMemo_CurrencyId()
    {
        return "debit_memo.CURRENCY_ID";
    }
  
    /**
     * debit_memo.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use DebitMemoPeer.debit_memo.CURRENCY_RATE constant
     */
    public static String getDebitMemo_CurrencyRate()
    {
        return "debit_memo.CURRENCY_RATE";
    }
  
    /**
     * debit_memo.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use DebitMemoPeer.debit_memo.AMOUNT constant
     */
    public static String getDebitMemo_Amount()
    {
        return "debit_memo.AMOUNT";
    }
  
    /**
     * debit_memo.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use DebitMemoPeer.debit_memo.AMOUNT_BASE constant
     */
    public static String getDebitMemo_AmountBase()
    {
        return "debit_memo.AMOUNT_BASE";
    }
  
    /**
     * debit_memo.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use DebitMemoPeer.debit_memo.USER_NAME constant
     */
    public static String getDebitMemo_UserName()
    {
        return "debit_memo.USER_NAME";
    }
  
    /**
     * debit_memo.REMARK
     * @return the column name for the REMARK field
     * @deprecated use DebitMemoPeer.debit_memo.REMARK constant
     */
    public static String getDebitMemo_Remark()
    {
        return "debit_memo.REMARK";
    }
  
    /**
     * debit_memo.STATUS
     * @return the column name for the STATUS field
     * @deprecated use DebitMemoPeer.debit_memo.STATUS constant
     */
    public static String getDebitMemo_Status()
    {
        return "debit_memo.STATUS";
    }
  
    /**
     * debit_memo.CLOSED_DATE
     * @return the column name for the CLOSED_DATE field
     * @deprecated use DebitMemoPeer.debit_memo.CLOSED_DATE constant
     */
    public static String getDebitMemo_ClosedDate()
    {
        return "debit_memo.CLOSED_DATE";
    }
  
    /**
     * debit_memo.PAYMENT_TRANS_ID
     * @return the column name for the PAYMENT_TRANS_ID field
     * @deprecated use DebitMemoPeer.debit_memo.PAYMENT_TRANS_ID constant
     */
    public static String getDebitMemo_PaymentTransId()
    {
        return "debit_memo.PAYMENT_TRANS_ID";
    }
  
    /**
     * debit_memo.PAYMENT_TRANS_NO
     * @return the column name for the PAYMENT_TRANS_NO field
     * @deprecated use DebitMemoPeer.debit_memo.PAYMENT_TRANS_NO constant
     */
    public static String getDebitMemo_PaymentTransNo()
    {
        return "debit_memo.PAYMENT_TRANS_NO";
    }
  
    /**
     * debit_memo.PAYMENT_INV_ID
     * @return the column name for the PAYMENT_INV_ID field
     * @deprecated use DebitMemoPeer.debit_memo.PAYMENT_INV_ID constant
     */
    public static String getDebitMemo_PaymentInvId()
    {
        return "debit_memo.PAYMENT_INV_ID";
    }
  
    /**
     * debit_memo.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use DebitMemoPeer.debit_memo.BANK_ID constant
     */
    public static String getDebitMemo_BankId()
    {
        return "debit_memo.BANK_ID";
    }
  
    /**
     * debit_memo.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use DebitMemoPeer.debit_memo.BANK_ISSUER constant
     */
    public static String getDebitMemo_BankIssuer()
    {
        return "debit_memo.BANK_ISSUER";
    }
  
    /**
     * debit_memo.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use DebitMemoPeer.debit_memo.DUE_DATE constant
     */
    public static String getDebitMemo_DueDate()
    {
        return "debit_memo.DUE_DATE";
    }
  
    /**
     * debit_memo.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use DebitMemoPeer.debit_memo.REFERENCE_NO constant
     */
    public static String getDebitMemo_ReferenceNo()
    {
        return "debit_memo.REFERENCE_NO";
    }
  
    /**
     * debit_memo.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use DebitMemoPeer.debit_memo.ACCOUNT_ID constant
     */
    public static String getDebitMemo_AccountId()
    {
        return "debit_memo.ACCOUNT_ID";
    }
  
    /**
     * debit_memo.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use DebitMemoPeer.debit_memo.FISCAL_RATE constant
     */
    public static String getDebitMemo_FiscalRate()
    {
        return "debit_memo.FISCAL_RATE";
    }
  
    /**
     * debit_memo.CLOSING_RATE
     * @return the column name for the CLOSING_RATE field
     * @deprecated use DebitMemoPeer.debit_memo.CLOSING_RATE constant
     */
    public static String getDebitMemo_ClosingRate()
    {
        return "debit_memo.CLOSING_RATE";
    }
  
    /**
     * debit_memo.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use DebitMemoPeer.debit_memo.CASH_FLOW_TYPE_ID constant
     */
    public static String getDebitMemo_CashFlowTypeId()
    {
        return "debit_memo.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * debit_memo.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use DebitMemoPeer.debit_memo.LOCATION_ID constant
     */
    public static String getDebitMemo_LocationId()
    {
        return "debit_memo.LOCATION_ID";
    }
  
    /**
     * debit_memo.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use DebitMemoPeer.debit_memo.CANCEL_BY constant
     */
    public static String getDebitMemo_CancelBy()
    {
        return "debit_memo.CANCEL_BY";
    }
  
    /**
     * debit_memo.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use DebitMemoPeer.debit_memo.CANCEL_DATE constant
     */
    public static String getDebitMemo_CancelDate()
    {
        return "debit_memo.CANCEL_DATE";
    }
  
    /**
     * debit_memo.FROM_PAYMENT_ID
     * @return the column name for the FROM_PAYMENT_ID field
     * @deprecated use DebitMemoPeer.debit_memo.FROM_PAYMENT_ID constant
     */
    public static String getDebitMemo_FromPaymentId()
    {
        return "debit_memo.FROM_PAYMENT_ID";
    }
  
    /**
     * debit_memo.CROSS_ACCOUNT_ID
     * @return the column name for the CROSS_ACCOUNT_ID field
     * @deprecated use DebitMemoPeer.debit_memo.CROSS_ACCOUNT_ID constant
     */
    public static String getDebitMemo_CrossAccountId()
    {
        return "debit_memo.CROSS_ACCOUNT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("debit_memo");
        TableMap tMap = dbMap.getTable("debit_memo");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("debit_memo.DEBIT_MEMO_ID", "");
                          tMap.addColumn("debit_memo.DEBIT_MEMO_NO", "");
                          tMap.addColumn("debit_memo.TRANSACTION_DATE", new Date());
                          tMap.addColumn("debit_memo.TRANSACTION_ID", "");
                          tMap.addColumn("debit_memo.TRANSACTION_NO", "");
                            tMap.addColumn("debit_memo.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("debit_memo.VENDOR_ID", "");
                          tMap.addColumn("debit_memo.VENDOR_NAME", "");
                          tMap.addColumn("debit_memo.CURRENCY_ID", "");
                            tMap.addColumn("debit_memo.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("debit_memo.AMOUNT", bd_ZERO);
                            tMap.addColumn("debit_memo.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("debit_memo.USER_NAME", "");
                          tMap.addColumn("debit_memo.REMARK", "");
                            tMap.addColumn("debit_memo.STATUS", Integer.valueOf(0));
                          tMap.addColumn("debit_memo.CLOSED_DATE", new Date());
                          tMap.addColumn("debit_memo.PAYMENT_TRANS_ID", "");
                          tMap.addColumn("debit_memo.PAYMENT_TRANS_NO", "");
                          tMap.addColumn("debit_memo.PAYMENT_INV_ID", "");
                          tMap.addColumn("debit_memo.BANK_ID", "");
                          tMap.addColumn("debit_memo.BANK_ISSUER", "");
                          tMap.addColumn("debit_memo.DUE_DATE", new Date());
                          tMap.addColumn("debit_memo.REFERENCE_NO", "");
                          tMap.addColumn("debit_memo.ACCOUNT_ID", "");
                            tMap.addColumn("debit_memo.FISCAL_RATE", bd_ZERO);
                            tMap.addColumn("debit_memo.CLOSING_RATE", bd_ZERO);
                          tMap.addColumn("debit_memo.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("debit_memo.LOCATION_ID", "");
                          tMap.addColumn("debit_memo.CANCEL_BY", "");
                          tMap.addColumn("debit_memo.CANCEL_DATE", new Date());
                          tMap.addColumn("debit_memo.FROM_PAYMENT_ID", "");
                          tMap.addColumn("debit_memo.CROSS_ACCOUNT_ID", "");
          }
}
