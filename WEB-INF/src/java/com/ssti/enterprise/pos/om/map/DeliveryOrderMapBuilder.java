package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DeliveryOrderMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DeliveryOrderMapBuilder";

    /**
     * Item
     * @deprecated use DeliveryOrderPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "delivery_order";
    }

  
    /**
     * delivery_order.DELIVERY_ORDER_ID
     * @return the column name for the DELIVERY_ORDER_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.DELIVERY_ORDER_ID constant
     */
    public static String getDeliveryOrder_DeliveryOrderId()
    {
        return "delivery_order.DELIVERY_ORDER_ID";
    }
  
    /**
     * delivery_order.SALES_ORDER_ID
     * @return the column name for the SALES_ORDER_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.SALES_ORDER_ID constant
     */
    public static String getDeliveryOrder_SalesOrderId()
    {
        return "delivery_order.SALES_ORDER_ID";
    }
  
    /**
     * delivery_order.CUSTOMER_PO_NO
     * @return the column name for the CUSTOMER_PO_NO field
     * @deprecated use DeliveryOrderPeer.delivery_order.CUSTOMER_PO_NO constant
     */
    public static String getDeliveryOrder_CustomerPoNo()
    {
        return "delivery_order.CUSTOMER_PO_NO";
    }
  
    /**
     * delivery_order.CUSTOMER_PO_DATE
     * @return the column name for the CUSTOMER_PO_DATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.CUSTOMER_PO_DATE constant
     */
    public static String getDeliveryOrder_CustomerPoDate()
    {
        return "delivery_order.CUSTOMER_PO_DATE";
    }
  
    /**
     * delivery_order.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.LOCATION_ID constant
     */
    public static String getDeliveryOrder_LocationId()
    {
        return "delivery_order.LOCATION_ID";
    }
  
    /**
     * delivery_order.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use DeliveryOrderPeer.delivery_order.LOCATION_NAME constant
     */
    public static String getDeliveryOrder_LocationName()
    {
        return "delivery_order.LOCATION_NAME";
    }
  
    /**
     * delivery_order.DELIVERY_ORDER_NO
     * @return the column name for the DELIVERY_ORDER_NO field
     * @deprecated use DeliveryOrderPeer.delivery_order.DELIVERY_ORDER_NO constant
     */
    public static String getDeliveryOrder_DeliveryOrderNo()
    {
        return "delivery_order.DELIVERY_ORDER_NO";
    }
  
    /**
     * delivery_order.DELIVERY_ORDER_DATE
     * @return the column name for the DELIVERY_ORDER_DATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.DELIVERY_ORDER_DATE constant
     */
    public static String getDeliveryOrder_DeliveryOrderDate()
    {
        return "delivery_order.DELIVERY_ORDER_DATE";
    }
  
    /**
     * delivery_order.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.CUSTOMER_ID constant
     */
    public static String getDeliveryOrder_CustomerId()
    {
        return "delivery_order.CUSTOMER_ID";
    }
  
    /**
     * delivery_order.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use DeliveryOrderPeer.delivery_order.CUSTOMER_NAME constant
     */
    public static String getDeliveryOrder_CustomerName()
    {
        return "delivery_order.CUSTOMER_NAME";
    }
  
    /**
     * delivery_order.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use DeliveryOrderPeer.delivery_order.CREATE_BY constant
     */
    public static String getDeliveryOrder_CreateBy()
    {
        return "delivery_order.CREATE_BY";
    }
  
    /**
     * delivery_order.CONFIRM_BY
     * @return the column name for the CONFIRM_BY field
     * @deprecated use DeliveryOrderPeer.delivery_order.CONFIRM_BY constant
     */
    public static String getDeliveryOrder_ConfirmBy()
    {
        return "delivery_order.CONFIRM_BY";
    }
  
    /**
     * delivery_order.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.CREATE_DATE constant
     */
    public static String getDeliveryOrder_CreateDate()
    {
        return "delivery_order.CREATE_DATE";
    }
  
    /**
     * delivery_order.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.CONFIRM_DATE constant
     */
    public static String getDeliveryOrder_ConfirmDate()
    {
        return "delivery_order.CONFIRM_DATE";
    }
  
    /**
     * delivery_order.STATUS
     * @return the column name for the STATUS field
     * @deprecated use DeliveryOrderPeer.delivery_order.STATUS constant
     */
    public static String getDeliveryOrder_Status()
    {
        return "delivery_order.STATUS";
    }
  
    /**
     * delivery_order.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use DeliveryOrderPeer.delivery_order.TOTAL_QTY constant
     */
    public static String getDeliveryOrder_TotalQty()
    {
        return "delivery_order.TOTAL_QTY";
    }
  
    /**
     * delivery_order.REMARK
     * @return the column name for the REMARK field
     * @deprecated use DeliveryOrderPeer.delivery_order.REMARK constant
     */
    public static String getDeliveryOrder_Remark()
    {
        return "delivery_order.REMARK";
    }
  
    /**
     * delivery_order.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use DeliveryOrderPeer.delivery_order.TOTAL_DISCOUNT_PCT constant
     */
    public static String getDeliveryOrder_TotalDiscountPct()
    {
        return "delivery_order.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * delivery_order.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.PAYMENT_TYPE_ID constant
     */
    public static String getDeliveryOrder_PaymentTypeId()
    {
        return "delivery_order.PAYMENT_TYPE_ID";
    }
  
    /**
     * delivery_order.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.PAYMENT_TERM_ID constant
     */
    public static String getDeliveryOrder_PaymentTermId()
    {
        return "delivery_order.PAYMENT_TERM_ID";
    }
  
    /**
     * delivery_order.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.CURRENCY_ID constant
     */
    public static String getDeliveryOrder_CurrencyId()
    {
        return "delivery_order.CURRENCY_ID";
    }
  
    /**
     * delivery_order.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.CURRENCY_RATE constant
     */
    public static String getDeliveryOrder_CurrencyRate()
    {
        return "delivery_order.CURRENCY_RATE";
    }
  
    /**
     * delivery_order.IS_BILL
     * @return the column name for the IS_BILL field
     * @deprecated use DeliveryOrderPeer.delivery_order.IS_BILL constant
     */
    public static String getDeliveryOrder_IsBill()
    {
        return "delivery_order.IS_BILL";
    }
  
    /**
     * delivery_order.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use DeliveryOrderPeer.delivery_order.SHIP_TO constant
     */
    public static String getDeliveryOrder_ShipTo()
    {
        return "delivery_order.SHIP_TO";
    }
  
    /**
     * delivery_order.PRINT_TIMES
     * @return the column name for the PRINT_TIMES field
     * @deprecated use DeliveryOrderPeer.delivery_order.PRINT_TIMES constant
     */
    public static String getDeliveryOrder_PrintTimes()
    {
        return "delivery_order.PRINT_TIMES";
    }
  
    /**
     * delivery_order.SALES_ID
     * @return the column name for the SALES_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.SALES_ID constant
     */
    public static String getDeliveryOrder_SalesId()
    {
        return "delivery_order.SALES_ID";
    }
  
    /**
     * delivery_order.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.COURIER_ID constant
     */
    public static String getDeliveryOrder_CourierId()
    {
        return "delivery_order.COURIER_ID";
    }
  
    /**
     * delivery_order.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.FOB_ID constant
     */
    public static String getDeliveryOrder_FobId()
    {
        return "delivery_order.FOB_ID";
    }
  
    /**
     * delivery_order.FREIGHT_NO
     * @return the column name for the FREIGHT_NO field
     * @deprecated use DeliveryOrderPeer.delivery_order.FREIGHT_NO constant
     */
    public static String getDeliveryOrder_FreightNo()
    {
        return "delivery_order.FREIGHT_NO";
    }
  
    /**
     * delivery_order.ESTIMATED_FREIGHT
     * @return the column name for the ESTIMATED_FREIGHT field
     * @deprecated use DeliveryOrderPeer.delivery_order.ESTIMATED_FREIGHT constant
     */
    public static String getDeliveryOrder_EstimatedFreight()
    {
        return "delivery_order.ESTIMATED_FREIGHT";
    }
  
    /**
     * delivery_order.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use DeliveryOrderPeer.delivery_order.IS_TAXABLE constant
     */
    public static String getDeliveryOrder_IsTaxable()
    {
        return "delivery_order.IS_TAXABLE";
    }
  
    /**
     * delivery_order.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use DeliveryOrderPeer.delivery_order.IS_INCLUSIVE_TAX constant
     */
    public static String getDeliveryOrder_IsInclusiveTax()
    {
        return "delivery_order.IS_INCLUSIVE_TAX";
    }
  
    /**
     * delivery_order.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.FISCAL_RATE constant
     */
    public static String getDeliveryOrder_FiscalRate()
    {
        return "delivery_order.FISCAL_RATE";
    }
  
    /**
     * delivery_order.FREIGHT_ACCOUNT_ID
     * @return the column name for the FREIGHT_ACCOUNT_ID field
     * @deprecated use DeliveryOrderPeer.delivery_order.FREIGHT_ACCOUNT_ID constant
     */
    public static String getDeliveryOrder_FreightAccountId()
    {
        return "delivery_order.FREIGHT_ACCOUNT_ID";
    }
  
    /**
     * delivery_order.IS_INCLUSIVE_FREIGHT
     * @return the column name for the IS_INCLUSIVE_FREIGHT field
     * @deprecated use DeliveryOrderPeer.delivery_order.IS_INCLUSIVE_FREIGHT constant
     */
    public static String getDeliveryOrder_IsInclusiveFreight()
    {
        return "delivery_order.IS_INCLUSIVE_FREIGHT";
    }
  
    /**
     * delivery_order.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use DeliveryOrderPeer.delivery_order.CANCEL_BY constant
     */
    public static String getDeliveryOrder_CancelBy()
    {
        return "delivery_order.CANCEL_BY";
    }
  
    /**
     * delivery_order.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use DeliveryOrderPeer.delivery_order.CANCEL_DATE constant
     */
    public static String getDeliveryOrder_CancelDate()
    {
        return "delivery_order.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("delivery_order");
        TableMap tMap = dbMap.getTable("delivery_order");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("delivery_order.DELIVERY_ORDER_ID", "");
                          tMap.addColumn("delivery_order.SALES_ORDER_ID", "");
                          tMap.addColumn("delivery_order.CUSTOMER_PO_NO", "");
                          tMap.addColumn("delivery_order.CUSTOMER_PO_DATE", new Date());
                          tMap.addColumn("delivery_order.LOCATION_ID", "");
                          tMap.addColumn("delivery_order.LOCATION_NAME", "");
                          tMap.addColumn("delivery_order.DELIVERY_ORDER_NO", "");
                          tMap.addColumn("delivery_order.DELIVERY_ORDER_DATE", new Date());
                          tMap.addColumn("delivery_order.CUSTOMER_ID", "");
                          tMap.addColumn("delivery_order.CUSTOMER_NAME", "");
                          tMap.addColumn("delivery_order.CREATE_BY", "");
                          tMap.addColumn("delivery_order.CONFIRM_BY", "");
                          tMap.addColumn("delivery_order.CREATE_DATE", new Date());
                          tMap.addColumn("delivery_order.CONFIRM_DATE", new Date());
                            tMap.addColumn("delivery_order.STATUS", Integer.valueOf(0));
                            tMap.addColumn("delivery_order.TOTAL_QTY", bd_ZERO);
                          tMap.addColumn("delivery_order.REMARK", "");
                          tMap.addColumn("delivery_order.TOTAL_DISCOUNT_PCT", "");
                          tMap.addColumn("delivery_order.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("delivery_order.PAYMENT_TERM_ID", "");
                          tMap.addColumn("delivery_order.CURRENCY_ID", "");
                            tMap.addColumn("delivery_order.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("delivery_order.IS_BILL", Boolean.TRUE);
                          tMap.addColumn("delivery_order.SHIP_TO", "");
                            tMap.addColumn("delivery_order.PRINT_TIMES", Integer.valueOf(0));
                          tMap.addColumn("delivery_order.SALES_ID", "");
                          tMap.addColumn("delivery_order.COURIER_ID", "");
                          tMap.addColumn("delivery_order.FOB_ID", "");
                          tMap.addColumn("delivery_order.FREIGHT_NO", "");
                            tMap.addColumn("delivery_order.ESTIMATED_FREIGHT", bd_ZERO);
                          tMap.addColumn("delivery_order.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("delivery_order.IS_INCLUSIVE_TAX", Boolean.TRUE);
                            tMap.addColumn("delivery_order.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("delivery_order.FREIGHT_ACCOUNT_ID", "");
                          tMap.addColumn("delivery_order.IS_INCLUSIVE_FREIGHT", Boolean.TRUE);
                          tMap.addColumn("delivery_order.CANCEL_BY", "");
                          tMap.addColumn("delivery_order.CANCEL_DATE", new Date());
          }
}
