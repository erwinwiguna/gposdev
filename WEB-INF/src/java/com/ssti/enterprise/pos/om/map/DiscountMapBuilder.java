package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DiscountMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DiscountMapBuilder";

    /**
     * Item
     * @deprecated use DiscountPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "discount";
    }

  
    /**
     * discount.DISCOUNT_ID
     * @return the column name for the DISCOUNT_ID field
     * @deprecated use DiscountPeer.discount.DISCOUNT_ID constant
     */
    public static String getDiscount_DiscountId()
    {
        return "discount.DISCOUNT_ID";
    }
  
    /**
     * discount.PROMO_CODE
     * @return the column name for the PROMO_CODE field
     * @deprecated use DiscountPeer.discount.PROMO_CODE constant
     */
    public static String getDiscount_PromoCode()
    {
        return "discount.PROMO_CODE";
    }
  
    /**
     * discount.DISCOUNT_CODE
     * @return the column name for the DISCOUNT_CODE field
     * @deprecated use DiscountPeer.discount.DISCOUNT_CODE constant
     */
    public static String getDiscount_DiscountCode()
    {
        return "discount.DISCOUNT_CODE";
    }
  
    /**
     * discount.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use DiscountPeer.discount.LOCATION_ID constant
     */
    public static String getDiscount_LocationId()
    {
        return "discount.LOCATION_ID";
    }
  
    /**
     * discount.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use DiscountPeer.discount.ITEM_ID constant
     */
    public static String getDiscount_ItemId()
    {
        return "discount.ITEM_ID";
    }
  
    /**
     * discount.ITEM_SKU
     * @return the column name for the ITEM_SKU field
     * @deprecated use DiscountPeer.discount.ITEM_SKU constant
     */
    public static String getDiscount_ItemSku()
    {
        return "discount.ITEM_SKU";
    }
  
    /**
     * discount.KATEGORI_ID
     * @return the column name for the KATEGORI_ID field
     * @deprecated use DiscountPeer.discount.KATEGORI_ID constant
     */
    public static String getDiscount_KategoriId()
    {
        return "discount.KATEGORI_ID";
    }
  
    /**
     * discount.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use DiscountPeer.discount.CUSTOMER_TYPE_ID constant
     */
    public static String getDiscount_CustomerTypeId()
    {
        return "discount.CUSTOMER_TYPE_ID";
    }
  
    /**
     * discount.CUSTOMERS
     * @return the column name for the CUSTOMERS field
     * @deprecated use DiscountPeer.discount.CUSTOMERS constant
     */
    public static String getDiscount_Customers()
    {
        return "discount.CUSTOMERS";
    }
  
    /**
     * discount.ITEMS
     * @return the column name for the ITEMS field
     * @deprecated use DiscountPeer.discount.ITEMS constant
     */
    public static String getDiscount_Items()
    {
        return "discount.ITEMS";
    }
  
    /**
     * discount.BRANDS
     * @return the column name for the BRANDS field
     * @deprecated use DiscountPeer.discount.BRANDS constant
     */
    public static String getDiscount_Brands()
    {
        return "discount.BRANDS";
    }
  
    /**
     * discount.MANUFACTURERS
     * @return the column name for the MANUFACTURERS field
     * @deprecated use DiscountPeer.discount.MANUFACTURERS constant
     */
    public static String getDiscount_Manufacturers()
    {
        return "discount.MANUFACTURERS";
    }
  
    /**
     * discount.TAGS
     * @return the column name for the TAGS field
     * @deprecated use DiscountPeer.discount.TAGS constant
     */
    public static String getDiscount_Tags()
    {
        return "discount.TAGS";
    }
  
    /**
     * discount.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use DiscountPeer.discount.PAYMENT_TYPE_ID constant
     */
    public static String getDiscount_PaymentTypeId()
    {
        return "discount.PAYMENT_TYPE_ID";
    }
  
    /**
     * discount.CARD_PREFIX
     * @return the column name for the CARD_PREFIX field
     * @deprecated use DiscountPeer.discount.CARD_PREFIX constant
     */
    public static String getDiscount_CardPrefix()
    {
        return "discount.CARD_PREFIX";
    }
  
    /**
     * discount.DISCOUNT_DAYS
     * @return the column name for the DISCOUNT_DAYS field
     * @deprecated use DiscountPeer.discount.DISCOUNT_DAYS constant
     */
    public static String getDiscount_DiscountDays()
    {
        return "discount.DISCOUNT_DAYS";
    }
  
    /**
     * discount.TOTAL_DISC_SCRIPT
     * @return the column name for the TOTAL_DISC_SCRIPT field
     * @deprecated use DiscountPeer.discount.TOTAL_DISC_SCRIPT constant
     */
    public static String getDiscount_TotalDiscScript()
    {
        return "discount.TOTAL_DISC_SCRIPT";
    }
  
    /**
     * discount.IS_TOTAL_DISC
     * @return the column name for the IS_TOTAL_DISC field
     * @deprecated use DiscountPeer.discount.IS_TOTAL_DISC constant
     */
    public static String getDiscount_IsTotalDisc()
    {
        return "discount.IS_TOTAL_DISC";
    }
  
    /**
     * discount.HAPPY_HOUR
     * @return the column name for the HAPPY_HOUR field
     * @deprecated use DiscountPeer.discount.HAPPY_HOUR constant
     */
    public static String getDiscount_HappyHour()
    {
        return "discount.HAPPY_HOUR";
    }
  
    /**
     * discount.START_HOUR
     * @return the column name for the START_HOUR field
     * @deprecated use DiscountPeer.discount.START_HOUR constant
     */
    public static String getDiscount_StartHour()
    {
        return "discount.START_HOUR";
    }
  
    /**
     * discount.END_HOUR
     * @return the column name for the END_HOUR field
     * @deprecated use DiscountPeer.discount.END_HOUR constant
     */
    public static String getDiscount_EndHour()
    {
        return "discount.END_HOUR";
    }
  
    /**
     * discount.DISCOUNT_TYPE
     * @return the column name for the DISCOUNT_TYPE field
     * @deprecated use DiscountPeer.discount.DISCOUNT_TYPE constant
     */
    public static String getDiscount_DiscountType()
    {
        return "discount.DISCOUNT_TYPE";
    }
  
    /**
     * discount.QTY_AMOUNT_1
     * @return the column name for the QTY_AMOUNT_1 field
     * @deprecated use DiscountPeer.discount.QTY_AMOUNT_1 constant
     */
    public static String getDiscount_QtyAmount1()
    {
        return "discount.QTY_AMOUNT_1";
    }
  
    /**
     * discount.QTY_AMOUNT_2
     * @return the column name for the QTY_AMOUNT_2 field
     * @deprecated use DiscountPeer.discount.QTY_AMOUNT_2 constant
     */
    public static String getDiscount_QtyAmount2()
    {
        return "discount.QTY_AMOUNT_2";
    }
  
    /**
     * discount.QTY_AMOUNT_3
     * @return the column name for the QTY_AMOUNT_3 field
     * @deprecated use DiscountPeer.discount.QTY_AMOUNT_3 constant
     */
    public static String getDiscount_QtyAmount3()
    {
        return "discount.QTY_AMOUNT_3";
    }
  
    /**
     * discount.QTY_AMOUNT_4
     * @return the column name for the QTY_AMOUNT_4 field
     * @deprecated use DiscountPeer.discount.QTY_AMOUNT_4 constant
     */
    public static String getDiscount_QtyAmount4()
    {
        return "discount.QTY_AMOUNT_4";
    }
  
    /**
     * discount.QTY_AMOUNT_5
     * @return the column name for the QTY_AMOUNT_5 field
     * @deprecated use DiscountPeer.discount.QTY_AMOUNT_5 constant
     */
    public static String getDiscount_QtyAmount5()
    {
        return "discount.QTY_AMOUNT_5";
    }
  
    /**
     * discount.DISCOUNT_VALUE_1
     * @return the column name for the DISCOUNT_VALUE_1 field
     * @deprecated use DiscountPeer.discount.DISCOUNT_VALUE_1 constant
     */
    public static String getDiscount_DiscountValue1()
    {
        return "discount.DISCOUNT_VALUE_1";
    }
  
    /**
     * discount.DISCOUNT_VALUE_2
     * @return the column name for the DISCOUNT_VALUE_2 field
     * @deprecated use DiscountPeer.discount.DISCOUNT_VALUE_2 constant
     */
    public static String getDiscount_DiscountValue2()
    {
        return "discount.DISCOUNT_VALUE_2";
    }
  
    /**
     * discount.DISCOUNT_VALUE_3
     * @return the column name for the DISCOUNT_VALUE_3 field
     * @deprecated use DiscountPeer.discount.DISCOUNT_VALUE_3 constant
     */
    public static String getDiscount_DiscountValue3()
    {
        return "discount.DISCOUNT_VALUE_3";
    }
  
    /**
     * discount.DISCOUNT_VALUE_4
     * @return the column name for the DISCOUNT_VALUE_4 field
     * @deprecated use DiscountPeer.discount.DISCOUNT_VALUE_4 constant
     */
    public static String getDiscount_DiscountValue4()
    {
        return "discount.DISCOUNT_VALUE_4";
    }
  
    /**
     * discount.DISCOUNT_VALUE_5
     * @return the column name for the DISCOUNT_VALUE_5 field
     * @deprecated use DiscountPeer.discount.DISCOUNT_VALUE_5 constant
     */
    public static String getDiscount_DiscountValue5()
    {
        return "discount.DISCOUNT_VALUE_5";
    }
  
    /**
     * discount.EVENT_BEGIN_DATE
     * @return the column name for the EVENT_BEGIN_DATE field
     * @deprecated use DiscountPeer.discount.EVENT_BEGIN_DATE constant
     */
    public static String getDiscount_EventBeginDate()
    {
        return "discount.EVENT_BEGIN_DATE";
    }
  
    /**
     * discount.EVENT_END_DATE
     * @return the column name for the EVENT_END_DATE field
     * @deprecated use DiscountPeer.discount.EVENT_END_DATE constant
     */
    public static String getDiscount_EventEndDate()
    {
        return "discount.EVENT_END_DATE";
    }
  
    /**
     * discount.EVENT_DISCOUNT
     * @return the column name for the EVENT_DISCOUNT field
     * @deprecated use DiscountPeer.discount.EVENT_DISCOUNT constant
     */
    public static String getDiscount_EventDiscount()
    {
        return "discount.EVENT_DISCOUNT";
    }
  
    /**
     * discount.PER_ITEM
     * @return the column name for the PER_ITEM field
     * @deprecated use DiscountPeer.discount.PER_ITEM constant
     */
    public static String getDiscount_PerItem()
    {
        return "discount.PER_ITEM";
    }
  
    /**
     * discount.MULTIPLY
     * @return the column name for the MULTIPLY field
     * @deprecated use DiscountPeer.discount.MULTIPLY constant
     */
    public static String getDiscount_Multiply()
    {
        return "discount.MULTIPLY";
    }
  
    /**
     * discount.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use DiscountPeer.discount.DESCRIPTION constant
     */
    public static String getDiscount_Description()
    {
        return "discount.DESCRIPTION";
    }
  
    /**
     * discount.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use DiscountPeer.discount.UPDATE_DATE constant
     */
    public static String getDiscount_UpdateDate()
    {
        return "discount.UPDATE_DATE";
    }
  
    /**
     * discount.LAST_UPDATE_BY
     * @return the column name for the LAST_UPDATE_BY field
     * @deprecated use DiscountPeer.discount.LAST_UPDATE_BY constant
     */
    public static String getDiscount_LastUpdateBy()
    {
        return "discount.LAST_UPDATE_BY";
    }
  
    /**
     * discount.BUY_QTY
     * @return the column name for the BUY_QTY field
     * @deprecated use DiscountPeer.discount.BUY_QTY constant
     */
    public static String getDiscount_BuyQty()
    {
        return "discount.BUY_QTY";
    }
  
    /**
     * discount.GET_QTY
     * @return the column name for the GET_QTY field
     * @deprecated use DiscountPeer.discount.GET_QTY constant
     */
    public static String getDiscount_GetQty()
    {
        return "discount.GET_QTY";
    }
  
    /**
     * discount.IS_CC_DISC
     * @return the column name for the IS_CC_DISC field
     * @deprecated use DiscountPeer.discount.IS_CC_DISC constant
     */
    public static String getDiscount_IsCcDisc()
    {
        return "discount.IS_CC_DISC";
    }
  
    /**
     * discount.CC_DISC_COMP
     * @return the column name for the CC_DISC_COMP field
     * @deprecated use DiscountPeer.discount.CC_DISC_COMP constant
     */
    public static String getDiscount_CcDiscComp()
    {
        return "discount.CC_DISC_COMP";
    }
  
    /**
     * discount.CC_DISC_BANK
     * @return the column name for the CC_DISC_BANK field
     * @deprecated use DiscountPeer.discount.CC_DISC_BANK constant
     */
    public static String getDiscount_CcDiscBank()
    {
        return "discount.CC_DISC_BANK";
    }
  
    /**
     * discount.CC_MIN_PURCH
     * @return the column name for the CC_MIN_PURCH field
     * @deprecated use DiscountPeer.discount.CC_MIN_PURCH constant
     */
    public static String getDiscount_CcMinPurch()
    {
        return "discount.CC_MIN_PURCH";
    }
  
    /**
     * discount.COUPON_TYPE
     * @return the column name for the COUPON_TYPE field
     * @deprecated use DiscountPeer.discount.COUPON_TYPE constant
     */
    public static String getDiscount_CouponType()
    {
        return "discount.COUPON_TYPE";
    }
  
    /**
     * discount.COUPON_DESC
     * @return the column name for the COUPON_DESC field
     * @deprecated use DiscountPeer.discount.COUPON_DESC constant
     */
    public static String getDiscount_CouponDesc()
    {
        return "discount.COUPON_DESC";
    }
  
    /**
     * discount.COUPON_PURCH_AMT
     * @return the column name for the COUPON_PURCH_AMT field
     * @deprecated use DiscountPeer.discount.COUPON_PURCH_AMT constant
     */
    public static String getDiscount_CouponPurchAmt()
    {
        return "discount.COUPON_PURCH_AMT";
    }
  
    /**
     * discount.COUPON_VALUE
     * @return the column name for the COUPON_VALUE field
     * @deprecated use DiscountPeer.discount.COUPON_VALUE constant
     */
    public static String getDiscount_CouponValue()
    {
        return "discount.COUPON_VALUE";
    }
  
    /**
     * discount.COUPON_QTY
     * @return the column name for the COUPON_QTY field
     * @deprecated use DiscountPeer.discount.COUPON_QTY constant
     */
    public static String getDiscount_CouponQty()
    {
        return "discount.COUPON_QTY";
    }
  
    /**
     * discount.COUPON_MESSAGE
     * @return the column name for the COUPON_MESSAGE field
     * @deprecated use DiscountPeer.discount.COUPON_MESSAGE constant
     */
    public static String getDiscount_CouponMessage()
    {
        return "discount.COUPON_MESSAGE";
    }
  
    /**
     * discount.COUPON_VALID_DAYS
     * @return the column name for the COUPON_VALID_DAYS field
     * @deprecated use DiscountPeer.discount.COUPON_VALID_DAYS constant
     */
    public static String getDiscount_CouponValidDays()
    {
        return "discount.COUPON_VALID_DAYS";
    }
  
    /**
     * discount.COUPON_VALID_FROM
     * @return the column name for the COUPON_VALID_FROM field
     * @deprecated use DiscountPeer.discount.COUPON_VALID_FROM constant
     */
    public static String getDiscount_CouponValidFrom()
    {
        return "discount.COUPON_VALID_FROM";
    }
  
    /**
     * discount.COUPON_VALID_TO
     * @return the column name for the COUPON_VALID_TO field
     * @deprecated use DiscountPeer.discount.COUPON_VALID_TO constant
     */
    public static String getDiscount_CouponValidTo()
    {
        return "discount.COUPON_VALID_TO";
    }
  
    /**
     * discount.DISC_SCRIPT
     * @return the column name for the DISC_SCRIPT field
     * @deprecated use DiscountPeer.discount.DISC_SCRIPT constant
     */
    public static String getDiscount_DiscScript()
    {
        return "discount.DISC_SCRIPT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("discount");
        TableMap tMap = dbMap.getTable("discount");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("discount.DISCOUNT_ID", "");
                          tMap.addColumn("discount.PROMO_CODE", "");
                          tMap.addColumn("discount.DISCOUNT_CODE", "");
                          tMap.addColumn("discount.LOCATION_ID", "");
                          tMap.addColumn("discount.ITEM_ID", "");
                          tMap.addColumn("discount.ITEM_SKU", "");
                          tMap.addColumn("discount.KATEGORI_ID", "");
                          tMap.addColumn("discount.CUSTOMER_TYPE_ID", "");
                          tMap.addColumn("discount.CUSTOMERS", "");
                          tMap.addColumn("discount.ITEMS", "");
                          tMap.addColumn("discount.BRANDS", "");
                          tMap.addColumn("discount.MANUFACTURERS", "");
                          tMap.addColumn("discount.TAGS", "");
                          tMap.addColumn("discount.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("discount.CARD_PREFIX", "");
                          tMap.addColumn("discount.DISCOUNT_DAYS", "");
                          tMap.addColumn("discount.TOTAL_DISC_SCRIPT", "");
                          tMap.addColumn("discount.IS_TOTAL_DISC", Boolean.TRUE);
                          tMap.addColumn("discount.HAPPY_HOUR", Boolean.TRUE);
                          tMap.addColumn("discount.START_HOUR", "");
                          tMap.addColumn("discount.END_HOUR", "");
                            tMap.addColumn("discount.DISCOUNT_TYPE", Integer.valueOf(0));
                            tMap.addColumn("discount.QTY_AMOUNT_1", bd_ZERO);
                            tMap.addColumn("discount.QTY_AMOUNT_2", bd_ZERO);
                            tMap.addColumn("discount.QTY_AMOUNT_3", bd_ZERO);
                            tMap.addColumn("discount.QTY_AMOUNT_4", bd_ZERO);
                            tMap.addColumn("discount.QTY_AMOUNT_5", bd_ZERO);
                          tMap.addColumn("discount.DISCOUNT_VALUE_1", "");
                          tMap.addColumn("discount.DISCOUNT_VALUE_2", "");
                          tMap.addColumn("discount.DISCOUNT_VALUE_3", "");
                          tMap.addColumn("discount.DISCOUNT_VALUE_4", "");
                          tMap.addColumn("discount.DISCOUNT_VALUE_5", "");
                          tMap.addColumn("discount.EVENT_BEGIN_DATE", new Date());
                          tMap.addColumn("discount.EVENT_END_DATE", new Date());
                          tMap.addColumn("discount.EVENT_DISCOUNT", "");
                          tMap.addColumn("discount.PER_ITEM", Boolean.TRUE);
                          tMap.addColumn("discount.MULTIPLY", Boolean.TRUE);
                          tMap.addColumn("discount.DESCRIPTION", "");
                          tMap.addColumn("discount.UPDATE_DATE", new Date());
                          tMap.addColumn("discount.LAST_UPDATE_BY", "");
                            tMap.addColumn("discount.BUY_QTY", bd_ZERO);
                            tMap.addColumn("discount.GET_QTY", bd_ZERO);
                          tMap.addColumn("discount.IS_CC_DISC", Boolean.TRUE);
                            tMap.addColumn("discount.CC_DISC_COMP", bd_ZERO);
                            tMap.addColumn("discount.CC_DISC_BANK", bd_ZERO);
                            tMap.addColumn("discount.CC_MIN_PURCH", bd_ZERO);
                            tMap.addColumn("discount.COUPON_TYPE", Integer.valueOf(0));
                          tMap.addColumn("discount.COUPON_DESC", "");
                            tMap.addColumn("discount.COUPON_PURCH_AMT", bd_ZERO);
                            tMap.addColumn("discount.COUPON_VALUE", bd_ZERO);
                            tMap.addColumn("discount.COUPON_QTY", Integer.valueOf(0));
                          tMap.addColumn("discount.COUPON_MESSAGE", "");
                            tMap.addColumn("discount.COUPON_VALID_DAYS", Integer.valueOf(0));
                          tMap.addColumn("discount.COUPON_VALID_FROM", new Date());
                          tMap.addColumn("discount.COUPON_VALID_TO", new Date());
                          tMap.addColumn("discount.DISC_SCRIPT", "");
          }
}
