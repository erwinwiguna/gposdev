package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class FifoInMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.FifoInMapBuilder";

    /**
     * Item
     * @deprecated use FifoInPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "fifo_in";
    }

  
    /**
     * fifo_in.FIFO_IN_ID
     * @return the column name for the FIFO_IN_ID field
     * @deprecated use FifoInPeer.fifo_in.FIFO_IN_ID constant
     */
    public static String getFifoIn_FifoInId()
    {
        return "fifo_in.FIFO_IN_ID";
    }
  
    /**
     * fifo_in.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use FifoInPeer.fifo_in.LOCATION_ID constant
     */
    public static String getFifoIn_LocationId()
    {
        return "fifo_in.LOCATION_ID";
    }
  
    /**
     * fifo_in.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use FifoInPeer.fifo_in.LOCATION_NAME constant
     */
    public static String getFifoIn_LocationName()
    {
        return "fifo_in.LOCATION_NAME";
    }
  
    /**
     * fifo_in.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use FifoInPeer.fifo_in.ITEM_ID constant
     */
    public static String getFifoIn_ItemId()
    {
        return "fifo_in.ITEM_ID";
    }
  
    /**
     * fifo_in.QTY
     * @return the column name for the QTY field
     * @deprecated use FifoInPeer.fifo_in.QTY constant
     */
    public static String getFifoIn_Qty()
    {
        return "fifo_in.QTY";
    }
  
    /**
     * fifo_in.OUT_QTY
     * @return the column name for the OUT_QTY field
     * @deprecated use FifoInPeer.fifo_in.OUT_QTY constant
     */
    public static String getFifoIn_OutQty()
    {
        return "fifo_in.OUT_QTY";
    }
  
    /**
     * fifo_in.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use FifoInPeer.fifo_in.ITEM_COST constant
     */
    public static String getFifoIn_ItemCost()
    {
        return "fifo_in.ITEM_COST";
    }
  
    /**
     * fifo_in.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use FifoInPeer.fifo_in.TRANSACTION_DATE constant
     */
    public static String getFifoIn_TransactionDate()
    {
        return "fifo_in.TRANSACTION_DATE";
    }
  
    /**
     * fifo_in.LAST_UPDATE
     * @return the column name for the LAST_UPDATE field
     * @deprecated use FifoInPeer.fifo_in.LAST_UPDATE constant
     */
    public static String getFifoIn_LastUpdate()
    {
        return "fifo_in.LAST_UPDATE";
    }
  
    /**
     * fifo_in.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use FifoInPeer.fifo_in.TRANSACTION_ID constant
     */
    public static String getFifoIn_TransactionId()
    {
        return "fifo_in.TRANSACTION_ID";
    }
  
    /**
     * fifo_in.INVENTORY_TRANSACTION_ID
     * @return the column name for the INVENTORY_TRANSACTION_ID field
     * @deprecated use FifoInPeer.fifo_in.INVENTORY_TRANSACTION_ID constant
     */
    public static String getFifoIn_InventoryTransactionId()
    {
        return "fifo_in.INVENTORY_TRANSACTION_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("fifo_in");
        TableMap tMap = dbMap.getTable("fifo_in");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("fifo_in.FIFO_IN_ID", "");
                          tMap.addColumn("fifo_in.LOCATION_ID", "");
                          tMap.addColumn("fifo_in.LOCATION_NAME", "");
                          tMap.addColumn("fifo_in.ITEM_ID", "");
                            tMap.addColumn("fifo_in.QTY", bd_ZERO);
                            tMap.addColumn("fifo_in.OUT_QTY", bd_ZERO);
                            tMap.addColumn("fifo_in.ITEM_COST", bd_ZERO);
                          tMap.addColumn("fifo_in.TRANSACTION_DATE", new Date());
                          tMap.addColumn("fifo_in.LAST_UPDATE", new Date());
                          tMap.addColumn("fifo_in.TRANSACTION_ID", "");
                          tMap.addForeignKey(
                "fifo_in.INVENTORY_TRANSACTION_ID", "" , "inventory_transaction" ,
                "inventory_transaction_id");
          }
}
