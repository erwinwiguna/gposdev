package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class FobMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.FobMapBuilder";

    /**
     * Item
     * @deprecated use FobPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "fob";
    }

  
    /**
     * fob.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use FobPeer.fob.FOB_ID constant
     */
    public static String getFob_FobId()
    {
        return "fob.FOB_ID";
    }
  
    /**
     * fob.FOB_CODE
     * @return the column name for the FOB_CODE field
     * @deprecated use FobPeer.fob.FOB_CODE constant
     */
    public static String getFob_FobCode()
    {
        return "fob.FOB_CODE";
    }
  
    /**
     * fob.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use FobPeer.fob.DESCRIPTION constant
     */
    public static String getFob_Description()
    {
        return "fob.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("fob");
        TableMap tMap = dbMap.getTable("fob");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("fob.FOB_ID", "");
                          tMap.addColumn("fob.FOB_CODE", "");
                          tMap.addColumn("fob.DESCRIPTION", "");
          }
}
