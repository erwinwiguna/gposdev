package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class GeneralLedgerMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.GeneralLedgerMapBuilder";

    /**
     * Item
     * @deprecated use GeneralLedgerPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "general_ledger";
    }

  
    /**
     * general_ledger.GENERAL_LEDGER_ID
     * @return the column name for the GENERAL_LEDGER_ID field
     * @deprecated use GeneralLedgerPeer.general_ledger.GENERAL_LEDGER_ID constant
     */
    public static String getGeneralLedger_GeneralLedgerId()
    {
        return "general_ledger.GENERAL_LEDGER_ID";
    }
  
    /**
     * general_ledger.PERIOD_ID
     * @return the column name for the PERIOD_ID field
     * @deprecated use GeneralLedgerPeer.general_ledger.PERIOD_ID constant
     */
    public static String getGeneralLedger_PeriodId()
    {
        return "general_ledger.PERIOD_ID";
    }
  
    /**
     * general_ledger.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use GeneralLedgerPeer.general_ledger.ACCOUNT_ID constant
     */
    public static String getGeneralLedger_AccountId()
    {
        return "general_ledger.ACCOUNT_ID";
    }
  
    /**
     * general_ledger.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use GeneralLedgerPeer.general_ledger.LOCATION_ID constant
     */
    public static String getGeneralLedger_LocationId()
    {
        return "general_ledger.LOCATION_ID";
    }
  
    /**
     * general_ledger.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use GeneralLedgerPeer.general_ledger.DEPARTMENT_ID constant
     */
    public static String getGeneralLedger_DepartmentId()
    {
        return "general_ledger.DEPARTMENT_ID";
    }
  
    /**
     * general_ledger.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use GeneralLedgerPeer.general_ledger.PROJECT_ID constant
     */
    public static String getGeneralLedger_ProjectId()
    {
        return "general_ledger.PROJECT_ID";
    }
  
    /**
     * general_ledger.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use GeneralLedgerPeer.general_ledger.OPENING_BALANCE constant
     */
    public static String getGeneralLedger_OpeningBalance()
    {
        return "general_ledger.OPENING_BALANCE";
    }
  
    /**
     * general_ledger.TOTAL_DEBIT_AMOUNT
     * @return the column name for the TOTAL_DEBIT_AMOUNT field
     * @deprecated use GeneralLedgerPeer.general_ledger.TOTAL_DEBIT_AMOUNT constant
     */
    public static String getGeneralLedger_TotalDebitAmount()
    {
        return "general_ledger.TOTAL_DEBIT_AMOUNT";
    }
  
    /**
     * general_ledger.TOTAL_CREDIT_AMOUNT
     * @return the column name for the TOTAL_CREDIT_AMOUNT field
     * @deprecated use GeneralLedgerPeer.general_ledger.TOTAL_CREDIT_AMOUNT constant
     */
    public static String getGeneralLedger_TotalCreditAmount()
    {
        return "general_ledger.TOTAL_CREDIT_AMOUNT";
    }
  
    /**
     * general_ledger.ENDING_BALANCE
     * @return the column name for the ENDING_BALANCE field
     * @deprecated use GeneralLedgerPeer.general_ledger.ENDING_BALANCE constant
     */
    public static String getGeneralLedger_EndingBalance()
    {
        return "general_ledger.ENDING_BALANCE";
    }
  
    /**
     * general_ledger.BUDGET_AMOUNT
     * @return the column name for the BUDGET_AMOUNT field
     * @deprecated use GeneralLedgerPeer.general_ledger.BUDGET_AMOUNT constant
     */
    public static String getGeneralLedger_BudgetAmount()
    {
        return "general_ledger.BUDGET_AMOUNT";
    }
  
    /**
     * general_ledger.VARIANCE
     * @return the column name for the VARIANCE field
     * @deprecated use GeneralLedgerPeer.general_ledger.VARIANCE constant
     */
    public static String getGeneralLedger_Variance()
    {
        return "general_ledger.VARIANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("general_ledger");
        TableMap tMap = dbMap.getTable("general_ledger");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("general_ledger.GENERAL_LEDGER_ID", "");
                          tMap.addColumn("general_ledger.PERIOD_ID", "");
                          tMap.addColumn("general_ledger.ACCOUNT_ID", "");
                          tMap.addColumn("general_ledger.LOCATION_ID", "");
                          tMap.addColumn("general_ledger.DEPARTMENT_ID", "");
                          tMap.addColumn("general_ledger.PROJECT_ID", "");
                            tMap.addColumn("general_ledger.OPENING_BALANCE", bd_ZERO);
                            tMap.addColumn("general_ledger.TOTAL_DEBIT_AMOUNT", bd_ZERO);
                            tMap.addColumn("general_ledger.TOTAL_CREDIT_AMOUNT", bd_ZERO);
                            tMap.addColumn("general_ledger.ENDING_BALANCE", bd_ZERO);
                            tMap.addColumn("general_ledger.BUDGET_AMOUNT", bd_ZERO);
                            tMap.addColumn("general_ledger.VARIANCE", bd_ZERO);
          }
}
