package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class GlTransactionCancelMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.GlTransactionCancelMapBuilder";

    /**
     * Item
     * @deprecated use GlTransactionCancelPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "gl_transaction_cancel";
    }

  
    /**
     * gl_transaction_cancel.GL_TRANSACTION_ID
     * @return the column name for the GL_TRANSACTION_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.GL_TRANSACTION_ID constant
     */
    public static String getGlTransactionCancel_GlTransactionId()
    {
        return "gl_transaction_cancel.GL_TRANSACTION_ID";
    }
  
    /**
     * gl_transaction_cancel.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.TRANSACTION_TYPE constant
     */
    public static String getGlTransactionCancel_TransactionType()
    {
        return "gl_transaction_cancel.TRANSACTION_TYPE";
    }
  
    /**
     * gl_transaction_cancel.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.TRANSACTION_ID constant
     */
    public static String getGlTransactionCancel_TransactionId()
    {
        return "gl_transaction_cancel.TRANSACTION_ID";
    }
  
    /**
     * gl_transaction_cancel.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.TRANSACTION_NO constant
     */
    public static String getGlTransactionCancel_TransactionNo()
    {
        return "gl_transaction_cancel.TRANSACTION_NO";
    }
  
    /**
     * gl_transaction_cancel.GL_TRANSACTION_NO
     * @return the column name for the GL_TRANSACTION_NO field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.GL_TRANSACTION_NO constant
     */
    public static String getGlTransactionCancel_GlTransactionNo()
    {
        return "gl_transaction_cancel.GL_TRANSACTION_NO";
    }
  
    /**
     * gl_transaction_cancel.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.TRANSACTION_DATE constant
     */
    public static String getGlTransactionCancel_TransactionDate()
    {
        return "gl_transaction_cancel.TRANSACTION_DATE";
    }
  
    /**
     * gl_transaction_cancel.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.PROJECT_ID constant
     */
    public static String getGlTransactionCancel_ProjectId()
    {
        return "gl_transaction_cancel.PROJECT_ID";
    }
  
    /**
     * gl_transaction_cancel.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.DEPARTMENT_ID constant
     */
    public static String getGlTransactionCancel_DepartmentId()
    {
        return "gl_transaction_cancel.DEPARTMENT_ID";
    }
  
    /**
     * gl_transaction_cancel.PERIOD_ID
     * @return the column name for the PERIOD_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.PERIOD_ID constant
     */
    public static String getGlTransactionCancel_PeriodId()
    {
        return "gl_transaction_cancel.PERIOD_ID";
    }
  
    /**
     * gl_transaction_cancel.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.ACCOUNT_ID constant
     */
    public static String getGlTransactionCancel_AccountId()
    {
        return "gl_transaction_cancel.ACCOUNT_ID";
    }
  
    /**
     * gl_transaction_cancel.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.CURRENCY_ID constant
     */
    public static String getGlTransactionCancel_CurrencyId()
    {
        return "gl_transaction_cancel.CURRENCY_ID";
    }
  
    /**
     * gl_transaction_cancel.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.CURRENCY_RATE constant
     */
    public static String getGlTransactionCancel_CurrencyRate()
    {
        return "gl_transaction_cancel.CURRENCY_RATE";
    }
  
    /**
     * gl_transaction_cancel.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.AMOUNT constant
     */
    public static String getGlTransactionCancel_Amount()
    {
        return "gl_transaction_cancel.AMOUNT";
    }
  
    /**
     * gl_transaction_cancel.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.AMOUNT_BASE constant
     */
    public static String getGlTransactionCancel_AmountBase()
    {
        return "gl_transaction_cancel.AMOUNT_BASE";
    }
  
    /**
     * gl_transaction_cancel.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.DESCRIPTION constant
     */
    public static String getGlTransactionCancel_Description()
    {
        return "gl_transaction_cancel.DESCRIPTION";
    }
  
    /**
     * gl_transaction_cancel.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.USER_NAME constant
     */
    public static String getGlTransactionCancel_UserName()
    {
        return "gl_transaction_cancel.USER_NAME";
    }
  
    /**
     * gl_transaction_cancel.DEBIT_CREDIT
     * @return the column name for the DEBIT_CREDIT field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.DEBIT_CREDIT constant
     */
    public static String getGlTransactionCancel_DebitCredit()
    {
        return "gl_transaction_cancel.DEBIT_CREDIT";
    }
  
    /**
     * gl_transaction_cancel.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.CREATE_DATE constant
     */
    public static String getGlTransactionCancel_CreateDate()
    {
        return "gl_transaction_cancel.CREATE_DATE";
    }
  
    /**
     * gl_transaction_cancel.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.LOCATION_ID constant
     */
    public static String getGlTransactionCancel_LocationId()
    {
        return "gl_transaction_cancel.LOCATION_ID";
    }
  
    /**
     * gl_transaction_cancel.SUB_LEDGER_TYPE
     * @return the column name for the SUB_LEDGER_TYPE field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.SUB_LEDGER_TYPE constant
     */
    public static String getGlTransactionCancel_SubLedgerType()
    {
        return "gl_transaction_cancel.SUB_LEDGER_TYPE";
    }
  
    /**
     * gl_transaction_cancel.SUB_LEDGER_ID
     * @return the column name for the SUB_LEDGER_ID field
     * @deprecated use GlTransactionCancelPeer.gl_transaction_cancel.SUB_LEDGER_ID constant
     */
    public static String getGlTransactionCancel_SubLedgerId()
    {
        return "gl_transaction_cancel.SUB_LEDGER_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("gl_transaction_cancel");
        TableMap tMap = dbMap.getTable("gl_transaction_cancel");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("gl_transaction_cancel.GL_TRANSACTION_ID", "");
                            tMap.addColumn("gl_transaction_cancel.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction_cancel.TRANSACTION_ID", "");
                          tMap.addColumn("gl_transaction_cancel.TRANSACTION_NO", "");
                          tMap.addColumn("gl_transaction_cancel.GL_TRANSACTION_NO", "");
                          tMap.addColumn("gl_transaction_cancel.TRANSACTION_DATE", new Date());
                          tMap.addColumn("gl_transaction_cancel.PROJECT_ID", "");
                          tMap.addColumn("gl_transaction_cancel.DEPARTMENT_ID", "");
                          tMap.addColumn("gl_transaction_cancel.PERIOD_ID", "");
                          tMap.addColumn("gl_transaction_cancel.ACCOUNT_ID", "");
                          tMap.addColumn("gl_transaction_cancel.CURRENCY_ID", "");
                            tMap.addColumn("gl_transaction_cancel.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("gl_transaction_cancel.AMOUNT", bd_ZERO);
                            tMap.addColumn("gl_transaction_cancel.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("gl_transaction_cancel.DESCRIPTION", "");
                          tMap.addColumn("gl_transaction_cancel.USER_NAME", "");
                            tMap.addColumn("gl_transaction_cancel.DEBIT_CREDIT", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction_cancel.CREATE_DATE", new Date());
                          tMap.addColumn("gl_transaction_cancel.LOCATION_ID", "");
                            tMap.addColumn("gl_transaction_cancel.SUB_LEDGER_TYPE", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction_cancel.SUB_LEDGER_ID", "");
          }
}
