package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class InventoryLocationMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.InventoryLocationMapBuilder";

    /**
     * Item
     * @deprecated use InventoryLocationPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "inventory_location";
    }

  
    /**
     * inventory_location.INVENTORY_LOCATION_ID
     * @return the column name for the INVENTORY_LOCATION_ID field
     * @deprecated use InventoryLocationPeer.inventory_location.INVENTORY_LOCATION_ID constant
     */
    public static String getInventoryLocation_InventoryLocationId()
    {
        return "inventory_location.INVENTORY_LOCATION_ID";
    }
  
    /**
     * inventory_location.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use InventoryLocationPeer.inventory_location.LOCATION_ID constant
     */
    public static String getInventoryLocation_LocationId()
    {
        return "inventory_location.LOCATION_ID";
    }
  
    /**
     * inventory_location.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use InventoryLocationPeer.inventory_location.LOCATION_NAME constant
     */
    public static String getInventoryLocation_LocationName()
    {
        return "inventory_location.LOCATION_NAME";
    }
  
    /**
     * inventory_location.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use InventoryLocationPeer.inventory_location.ITEM_ID constant
     */
    public static String getInventoryLocation_ItemId()
    {
        return "inventory_location.ITEM_ID";
    }
  
    /**
     * inventory_location.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use InventoryLocationPeer.inventory_location.ITEM_CODE constant
     */
    public static String getInventoryLocation_ItemCode()
    {
        return "inventory_location.ITEM_CODE";
    }
  
    /**
     * inventory_location.CURRENT_QTY
     * @return the column name for the CURRENT_QTY field
     * @deprecated use InventoryLocationPeer.inventory_location.CURRENT_QTY constant
     */
    public static String getInventoryLocation_CurrentQty()
    {
        return "inventory_location.CURRENT_QTY";
    }
  
    /**
     * inventory_location.START_STOCK
     * @return the column name for the START_STOCK field
     * @deprecated use InventoryLocationPeer.inventory_location.START_STOCK constant
     */
    public static String getInventoryLocation_StartStock()
    {
        return "inventory_location.START_STOCK";
    }
  
    /**
     * inventory_location.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use InventoryLocationPeer.inventory_location.ITEM_COST constant
     */
    public static String getInventoryLocation_ItemCost()
    {
        return "inventory_location.ITEM_COST";
    }
  
    /**
     * inventory_location.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use InventoryLocationPeer.inventory_location.UPDATE_DATE constant
     */
    public static String getInventoryLocation_UpdateDate()
    {
        return "inventory_location.UPDATE_DATE";
    }
  
    /**
     * inventory_location.LAST_IN
     * @return the column name for the LAST_IN field
     * @deprecated use InventoryLocationPeer.inventory_location.LAST_IN constant
     */
    public static String getInventoryLocation_LastIn()
    {
        return "inventory_location.LAST_IN";
    }
  
    /**
     * inventory_location.LAST_OUT
     * @return the column name for the LAST_OUT field
     * @deprecated use InventoryLocationPeer.inventory_location.LAST_OUT constant
     */
    public static String getInventoryLocation_LastOut()
    {
        return "inventory_location.LAST_OUT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("inventory_location");
        TableMap tMap = dbMap.getTable("inventory_location");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("inventory_location.INVENTORY_LOCATION_ID", "");
                          tMap.addColumn("inventory_location.LOCATION_ID", "");
                          tMap.addColumn("inventory_location.LOCATION_NAME", "");
                          tMap.addColumn("inventory_location.ITEM_ID", "");
                          tMap.addColumn("inventory_location.ITEM_CODE", "");
                            tMap.addColumn("inventory_location.CURRENT_QTY", bd_ZERO);
                            tMap.addColumn("inventory_location.START_STOCK", bd_ZERO);
                            tMap.addColumn("inventory_location.ITEM_COST", bd_ZERO);
                          tMap.addColumn("inventory_location.UPDATE_DATE", new Date());
                          tMap.addColumn("inventory_location.LAST_IN", new Date());
                          tMap.addColumn("inventory_location.LAST_OUT", new Date());
          }
}
