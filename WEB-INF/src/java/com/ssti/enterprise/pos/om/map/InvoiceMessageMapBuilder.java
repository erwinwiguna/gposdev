package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class InvoiceMessageMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.InvoiceMessageMapBuilder";

    /**
     * Item
     * @deprecated use InvoiceMessagePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "invoice_message";
    }

  
    /**
     * invoice_message.INVOICE_MESSAGE_ID
     * @return the column name for the INVOICE_MESSAGE_ID field
     * @deprecated use InvoiceMessagePeer.invoice_message.INVOICE_MESSAGE_ID constant
     */
    public static String getInvoiceMessage_InvoiceMessageId()
    {
        return "invoice_message.INVOICE_MESSAGE_ID";
    }
  
    /**
     * invoice_message.START_DATE
     * @return the column name for the START_DATE field
     * @deprecated use InvoiceMessagePeer.invoice_message.START_DATE constant
     */
    public static String getInvoiceMessage_StartDate()
    {
        return "invoice_message.START_DATE";
    }
  
    /**
     * invoice_message.END_DATE
     * @return the column name for the END_DATE field
     * @deprecated use InvoiceMessagePeer.invoice_message.END_DATE constant
     */
    public static String getInvoiceMessage_EndDate()
    {
        return "invoice_message.END_DATE";
    }
  
    /**
     * invoice_message.TITLE
     * @return the column name for the TITLE field
     * @deprecated use InvoiceMessagePeer.invoice_message.TITLE constant
     */
    public static String getInvoiceMessage_Title()
    {
        return "invoice_message.TITLE";
    }
  
    /**
     * invoice_message.CONTENTS
     * @return the column name for the CONTENTS field
     * @deprecated use InvoiceMessagePeer.invoice_message.CONTENTS constant
     */
    public static String getInvoiceMessage_Contents()
    {
        return "invoice_message.CONTENTS";
    }
  
    /**
     * invoice_message.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use InvoiceMessagePeer.invoice_message.CUSTOMER_TYPE_ID constant
     */
    public static String getInvoiceMessage_CustomerTypeId()
    {
        return "invoice_message.CUSTOMER_TYPE_ID";
    }
  
    /**
     * invoice_message.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use InvoiceMessagePeer.invoice_message.LOCATION_ID constant
     */
    public static String getInvoiceMessage_LocationId()
    {
        return "invoice_message.LOCATION_ID";
    }
  
    /**
     * invoice_message.LAST_UPDATE_BY
     * @return the column name for the LAST_UPDATE_BY field
     * @deprecated use InvoiceMessagePeer.invoice_message.LAST_UPDATE_BY constant
     */
    public static String getInvoiceMessage_LastUpdateBy()
    {
        return "invoice_message.LAST_UPDATE_BY";
    }
  
    /**
     * invoice_message.LAST_UPDATE
     * @return the column name for the LAST_UPDATE field
     * @deprecated use InvoiceMessagePeer.invoice_message.LAST_UPDATE constant
     */
    public static String getInvoiceMessage_LastUpdate()
    {
        return "invoice_message.LAST_UPDATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("invoice_message");
        TableMap tMap = dbMap.getTable("invoice_message");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("invoice_message.INVOICE_MESSAGE_ID", "");
                          tMap.addColumn("invoice_message.START_DATE", new Date());
                          tMap.addColumn("invoice_message.END_DATE", new Date());
                          tMap.addColumn("invoice_message.TITLE", "");
                          tMap.addColumn("invoice_message.CONTENTS", "");
                          tMap.addColumn("invoice_message.CUSTOMER_TYPE_ID", "");
                          tMap.addColumn("invoice_message.LOCATION_ID", "");
                          tMap.addColumn("invoice_message.LAST_UPDATE_BY", "");
                          tMap.addColumn("invoice_message.LAST_UPDATE", new Date());
          }
}
