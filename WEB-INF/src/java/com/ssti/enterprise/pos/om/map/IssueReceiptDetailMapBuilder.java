package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class IssueReceiptDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.IssueReceiptDetailMapBuilder";

    /**
     * Item
     * @deprecated use IssueReceiptDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "issue_receipt_detail";
    }

  
    /**
     * issue_receipt_detail.ISSUE_RECEIPT_DETAIL_ID
     * @return the column name for the ISSUE_RECEIPT_DETAIL_ID field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.ISSUE_RECEIPT_DETAIL_ID constant
     */
    public static String getIssueReceiptDetail_IssueReceiptDetailId()
    {
        return "issue_receipt_detail.ISSUE_RECEIPT_DETAIL_ID";
    }
  
    /**
     * issue_receipt_detail.ISSUE_RECEIPT_ID
     * @return the column name for the ISSUE_RECEIPT_ID field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.ISSUE_RECEIPT_ID constant
     */
    public static String getIssueReceiptDetail_IssueReceiptId()
    {
        return "issue_receipt_detail.ISSUE_RECEIPT_ID";
    }
  
    /**
     * issue_receipt_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.INDEX_NO constant
     */
    public static String getIssueReceiptDetail_IndexNo()
    {
        return "issue_receipt_detail.INDEX_NO";
    }
  
    /**
     * issue_receipt_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.ITEM_ID constant
     */
    public static String getIssueReceiptDetail_ItemId()
    {
        return "issue_receipt_detail.ITEM_ID";
    }
  
    /**
     * issue_receipt_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.ITEM_CODE constant
     */
    public static String getIssueReceiptDetail_ItemCode()
    {
        return "issue_receipt_detail.ITEM_CODE";
    }
  
    /**
     * issue_receipt_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.UNIT_ID constant
     */
    public static String getIssueReceiptDetail_UnitId()
    {
        return "issue_receipt_detail.UNIT_ID";
    }
  
    /**
     * issue_receipt_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.UNIT_CODE constant
     */
    public static String getIssueReceiptDetail_UnitCode()
    {
        return "issue_receipt_detail.UNIT_CODE";
    }
  
    /**
     * issue_receipt_detail.QTY_CHANGES
     * @return the column name for the QTY_CHANGES field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.QTY_CHANGES constant
     */
    public static String getIssueReceiptDetail_QtyChanges()
    {
        return "issue_receipt_detail.QTY_CHANGES";
    }
  
    /**
     * issue_receipt_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.QTY_BASE constant
     */
    public static String getIssueReceiptDetail_QtyBase()
    {
        return "issue_receipt_detail.QTY_BASE";
    }
  
    /**
     * issue_receipt_detail.COST
     * @return the column name for the COST field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.COST constant
     */
    public static String getIssueReceiptDetail_Cost()
    {
        return "issue_receipt_detail.COST";
    }
  
    /**
     * issue_receipt_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.DEPARTMENT_ID constant
     */
    public static String getIssueReceiptDetail_DepartmentId()
    {
        return "issue_receipt_detail.DEPARTMENT_ID";
    }
  
    /**
     * issue_receipt_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use IssueReceiptDetailPeer.issue_receipt_detail.PROJECT_ID constant
     */
    public static String getIssueReceiptDetail_ProjectId()
    {
        return "issue_receipt_detail.PROJECT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("issue_receipt_detail");
        TableMap tMap = dbMap.getTable("issue_receipt_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("issue_receipt_detail.ISSUE_RECEIPT_DETAIL_ID", "");
                          tMap.addForeignKey(
                "issue_receipt_detail.ISSUE_RECEIPT_ID", "" , "issue_receipt" ,
                "issue_receipt_id");
                            tMap.addColumn("issue_receipt_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("issue_receipt_detail.ITEM_ID", "");
                          tMap.addColumn("issue_receipt_detail.ITEM_CODE", "");
                          tMap.addColumn("issue_receipt_detail.UNIT_ID", "");
                          tMap.addColumn("issue_receipt_detail.UNIT_CODE", "");
                            tMap.addColumn("issue_receipt_detail.QTY_CHANGES", bd_ZERO);
                            tMap.addColumn("issue_receipt_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("issue_receipt_detail.COST", bd_ZERO);
                          tMap.addColumn("issue_receipt_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("issue_receipt_detail.PROJECT_ID", "");
          }
}
