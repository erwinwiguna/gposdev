package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemGroupMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemGroupMapBuilder";

    /**
     * Item
     * @deprecated use ItemGroupPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_group";
    }

  
    /**
     * item_group.ITEM_GROUP_ID
     * @return the column name for the ITEM_GROUP_ID field
     * @deprecated use ItemGroupPeer.item_group.ITEM_GROUP_ID constant
     */
    public static String getItemGroup_ItemGroupId()
    {
        return "item_group.ITEM_GROUP_ID";
    }
  
    /**
     * item_group.GROUP_ID
     * @return the column name for the GROUP_ID field
     * @deprecated use ItemGroupPeer.item_group.GROUP_ID constant
     */
    public static String getItemGroup_GroupId()
    {
        return "item_group.GROUP_ID";
    }
  
    /**
     * item_group.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemGroupPeer.item_group.ITEM_ID constant
     */
    public static String getItemGroup_ItemId()
    {
        return "item_group.ITEM_ID";
    }
  
    /**
     * item_group.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use ItemGroupPeer.item_group.ITEM_CODE constant
     */
    public static String getItemGroup_ItemCode()
    {
        return "item_group.ITEM_CODE";
    }
  
    /**
     * item_group.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use ItemGroupPeer.item_group.ITEM_NAME constant
     */
    public static String getItemGroup_ItemName()
    {
        return "item_group.ITEM_NAME";
    }
  
    /**
     * item_group.QTY
     * @return the column name for the QTY field
     * @deprecated use ItemGroupPeer.item_group.QTY constant
     */
    public static String getItemGroup_Qty()
    {
        return "item_group.QTY";
    }
  
    /**
     * item_group.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use ItemGroupPeer.item_group.UNIT_ID constant
     */
    public static String getItemGroup_UnitId()
    {
        return "item_group.UNIT_ID";
    }
  
    /**
     * item_group.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use ItemGroupPeer.item_group.UNIT_CODE constant
     */
    public static String getItemGroup_UnitCode()
    {
        return "item_group.UNIT_CODE";
    }
  
    /**
     * item_group.COST
     * @return the column name for the COST field
     * @deprecated use ItemGroupPeer.item_group.COST constant
     */
    public static String getItemGroup_Cost()
    {
        return "item_group.COST";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_group");
        TableMap tMap = dbMap.getTable("item_group");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_group.ITEM_GROUP_ID", "");
                          tMap.addForeignKey(
                "item_group.GROUP_ID", "" , "item" ,
                "item_id");
                          tMap.addForeignKey(
                "item_group.ITEM_ID", "" , "item" ,
                "item_id");
                          tMap.addColumn("item_group.ITEM_CODE", "");
                          tMap.addColumn("item_group.ITEM_NAME", "");
                            tMap.addColumn("item_group.QTY", bd_ZERO);
                          tMap.addColumn("item_group.UNIT_ID", "");
                          tMap.addColumn("item_group.UNIT_CODE", "");
                            tMap.addColumn("item_group.COST", bd_ZERO);
          }
}
