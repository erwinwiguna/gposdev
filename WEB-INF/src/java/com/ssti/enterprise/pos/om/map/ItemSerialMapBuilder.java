package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemSerialMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemSerialMapBuilder";

    /**
     * Item
     * @deprecated use ItemSerialPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_serial";
    }

  
    /**
     * item_serial.ITEM_SERIAL_ID
     * @return the column name for the ITEM_SERIAL_ID field
     * @deprecated use ItemSerialPeer.item_serial.ITEM_SERIAL_ID constant
     */
    public static String getItemSerial_ItemSerialId()
    {
        return "item_serial.ITEM_SERIAL_ID";
    }
  
    /**
     * item_serial.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemSerialPeer.item_serial.ITEM_ID constant
     */
    public static String getItemSerial_ItemId()
    {
        return "item_serial.ITEM_ID";
    }
  
    /**
     * item_serial.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use ItemSerialPeer.item_serial.ITEM_CODE constant
     */
    public static String getItemSerial_ItemCode()
    {
        return "item_serial.ITEM_CODE";
    }
  
    /**
     * item_serial.SERIAL_NO
     * @return the column name for the SERIAL_NO field
     * @deprecated use ItemSerialPeer.item_serial.SERIAL_NO constant
     */
    public static String getItemSerial_SerialNo()
    {
        return "item_serial.SERIAL_NO";
    }
  
    /**
     * item_serial.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use ItemSerialPeer.item_serial.LOCATION_ID constant
     */
    public static String getItemSerial_LocationId()
    {
        return "item_serial.LOCATION_ID";
    }
  
    /**
     * item_serial.STATUS
     * @return the column name for the STATUS field
     * @deprecated use ItemSerialPeer.item_serial.STATUS constant
     */
    public static String getItemSerial_Status()
    {
        return "item_serial.STATUS";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_serial");
        TableMap tMap = dbMap.getTable("item_serial");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_serial.ITEM_SERIAL_ID", "");
                          tMap.addColumn("item_serial.ITEM_ID", "");
                          tMap.addColumn("item_serial.ITEM_CODE", "");
                          tMap.addColumn("item_serial.SERIAL_NO", "");
                          tMap.addColumn("item_serial.LOCATION_ID", "");
                          tMap.addColumn("item_serial.STATUS", Boolean.TRUE);
          }
}
