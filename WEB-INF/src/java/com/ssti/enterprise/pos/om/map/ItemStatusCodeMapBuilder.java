package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemStatusCodeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemStatusCodeMapBuilder";

    /**
     * Item
     * @deprecated use ItemStatusCodePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_status_code";
    }

  
    /**
     * item_status_code.ITEM_STATUS_CODE_ID
     * @return the column name for the ITEM_STATUS_CODE_ID field
     * @deprecated use ItemStatusCodePeer.item_status_code.ITEM_STATUS_CODE_ID constant
     */
    public static String getItemStatusCode_ItemStatusCodeId()
    {
        return "item_status_code.ITEM_STATUS_CODE_ID";
    }
  
    /**
     * item_status_code.STATUS_CODE
     * @return the column name for the STATUS_CODE field
     * @deprecated use ItemStatusCodePeer.item_status_code.STATUS_CODE constant
     */
    public static String getItemStatusCode_StatusCode()
    {
        return "item_status_code.STATUS_CODE";
    }
  
    /**
     * item_status_code.VARIABLE
     * @return the column name for the VARIABLE field
     * @deprecated use ItemStatusCodePeer.item_status_code.VARIABLE constant
     */
    public static String getItemStatusCode_Variable()
    {
        return "item_status_code.VARIABLE";
    }
  
    /**
     * item_status_code.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use ItemStatusCodePeer.item_status_code.DESCRIPTION constant
     */
    public static String getItemStatusCode_Description()
    {
        return "item_status_code.DESCRIPTION";
    }
  
    /**
     * item_status_code.IS_DEFAULT
     * @return the column name for the IS_DEFAULT field
     * @deprecated use ItemStatusCodePeer.item_status_code.IS_DEFAULT constant
     */
    public static String getItemStatusCode_IsDefault()
    {
        return "item_status_code.IS_DEFAULT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_status_code");
        TableMap tMap = dbMap.getTable("item_status_code");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_status_code.ITEM_STATUS_CODE_ID", "");
                          tMap.addColumn("item_status_code.STATUS_CODE", "");
                            tMap.addColumn("item_status_code.VARIABLE", Integer.valueOf(0));
                          tMap.addColumn("item_status_code.DESCRIPTION", "");
                          tMap.addColumn("item_status_code.IS_DEFAULT", Boolean.TRUE);
          }
}
