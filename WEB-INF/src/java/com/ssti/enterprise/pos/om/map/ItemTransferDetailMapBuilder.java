package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemTransferDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemTransferDetailMapBuilder";

    /**
     * Item
     * @deprecated use ItemTransferDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_transfer_detail";
    }

  
    /**
     * item_transfer_detail.ITEM_TRANSFER_DETAIL_ID
     * @return the column name for the ITEM_TRANSFER_DETAIL_ID field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.ITEM_TRANSFER_DETAIL_ID constant
     */
    public static String getItemTransferDetail_ItemTransferDetailId()
    {
        return "item_transfer_detail.ITEM_TRANSFER_DETAIL_ID";
    }
  
    /**
     * item_transfer_detail.ITEM_TRANSFER_ID
     * @return the column name for the ITEM_TRANSFER_ID field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.ITEM_TRANSFER_ID constant
     */
    public static String getItemTransferDetail_ItemTransferId()
    {
        return "item_transfer_detail.ITEM_TRANSFER_ID";
    }
  
    /**
     * item_transfer_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.INDEX_NO constant
     */
    public static String getItemTransferDetail_IndexNo()
    {
        return "item_transfer_detail.INDEX_NO";
    }
  
    /**
     * item_transfer_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.ITEM_ID constant
     */
    public static String getItemTransferDetail_ItemId()
    {
        return "item_transfer_detail.ITEM_ID";
    }
  
    /**
     * item_transfer_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.ITEM_CODE constant
     */
    public static String getItemTransferDetail_ItemCode()
    {
        return "item_transfer_detail.ITEM_CODE";
    }
  
    /**
     * item_transfer_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.UNIT_ID constant
     */
    public static String getItemTransferDetail_UnitId()
    {
        return "item_transfer_detail.UNIT_ID";
    }
  
    /**
     * item_transfer_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.UNIT_CODE constant
     */
    public static String getItemTransferDetail_UnitCode()
    {
        return "item_transfer_detail.UNIT_CODE";
    }
  
    /**
     * item_transfer_detail.QTY_CHANGES
     * @return the column name for the QTY_CHANGES field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.QTY_CHANGES constant
     */
    public static String getItemTransferDetail_QtyChanges()
    {
        return "item_transfer_detail.QTY_CHANGES";
    }
  
    /**
     * item_transfer_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.QTY_BASE constant
     */
    public static String getItemTransferDetail_QtyBase()
    {
        return "item_transfer_detail.QTY_BASE";
    }
  
    /**
     * item_transfer_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use ItemTransferDetailPeer.item_transfer_detail.COST_PER_UNIT constant
     */
    public static String getItemTransferDetail_CostPerUnit()
    {
        return "item_transfer_detail.COST_PER_UNIT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_transfer_detail");
        TableMap tMap = dbMap.getTable("item_transfer_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_transfer_detail.ITEM_TRANSFER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "item_transfer_detail.ITEM_TRANSFER_ID", "" , "item_transfer" ,
                "item_transfer_id");
                            tMap.addColumn("item_transfer_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("item_transfer_detail.ITEM_ID", "");
                          tMap.addColumn("item_transfer_detail.ITEM_CODE", "");
                          tMap.addColumn("item_transfer_detail.UNIT_ID", "");
                          tMap.addColumn("item_transfer_detail.UNIT_CODE", "");
                            tMap.addColumn("item_transfer_detail.QTY_CHANGES", bd_ZERO);
                            tMap.addColumn("item_transfer_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("item_transfer_detail.COST_PER_UNIT", bd_ZERO);
          }
}
