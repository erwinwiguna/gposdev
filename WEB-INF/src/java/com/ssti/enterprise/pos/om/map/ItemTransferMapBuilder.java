package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemTransferMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemTransferMapBuilder";

    /**
     * Item
     * @deprecated use ItemTransferPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_transfer";
    }

  
    /**
     * item_transfer.ITEM_TRANSFER_ID
     * @return the column name for the ITEM_TRANSFER_ID field
     * @deprecated use ItemTransferPeer.item_transfer.ITEM_TRANSFER_ID constant
     */
    public static String getItemTransfer_ItemTransferId()
    {
        return "item_transfer.ITEM_TRANSFER_ID";
    }
  
    /**
     * item_transfer.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use ItemTransferPeer.item_transfer.TRANSACTION_NO constant
     */
    public static String getItemTransfer_TransactionNo()
    {
        return "item_transfer.TRANSACTION_NO";
    }
  
    /**
     * item_transfer.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use ItemTransferPeer.item_transfer.TRANSACTION_DATE constant
     */
    public static String getItemTransfer_TransactionDate()
    {
        return "item_transfer.TRANSACTION_DATE";
    }
  
    /**
     * item_transfer.ADJUSTMENT_TYPE_ID
     * @return the column name for the ADJUSTMENT_TYPE_ID field
     * @deprecated use ItemTransferPeer.item_transfer.ADJUSTMENT_TYPE_ID constant
     */
    public static String getItemTransfer_AdjustmentTypeId()
    {
        return "item_transfer.ADJUSTMENT_TYPE_ID";
    }
  
    /**
     * item_transfer.FROM_LOCATION_ID
     * @return the column name for the FROM_LOCATION_ID field
     * @deprecated use ItemTransferPeer.item_transfer.FROM_LOCATION_ID constant
     */
    public static String getItemTransfer_FromLocationId()
    {
        return "item_transfer.FROM_LOCATION_ID";
    }
  
    /**
     * item_transfer.FROM_LOCATION_NAME
     * @return the column name for the FROM_LOCATION_NAME field
     * @deprecated use ItemTransferPeer.item_transfer.FROM_LOCATION_NAME constant
     */
    public static String getItemTransfer_FromLocationName()
    {
        return "item_transfer.FROM_LOCATION_NAME";
    }
  
    /**
     * item_transfer.TO_LOCATION_ID
     * @return the column name for the TO_LOCATION_ID field
     * @deprecated use ItemTransferPeer.item_transfer.TO_LOCATION_ID constant
     */
    public static String getItemTransfer_ToLocationId()
    {
        return "item_transfer.TO_LOCATION_ID";
    }
  
    /**
     * item_transfer.TO_LOCATION_NAME
     * @return the column name for the TO_LOCATION_NAME field
     * @deprecated use ItemTransferPeer.item_transfer.TO_LOCATION_NAME constant
     */
    public static String getItemTransfer_ToLocationName()
    {
        return "item_transfer.TO_LOCATION_NAME";
    }
  
    /**
     * item_transfer.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use ItemTransferPeer.item_transfer.USER_NAME constant
     */
    public static String getItemTransfer_UserName()
    {
        return "item_transfer.USER_NAME";
    }
  
    /**
     * item_transfer.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use ItemTransferPeer.item_transfer.DESCRIPTION constant
     */
    public static String getItemTransfer_Description()
    {
        return "item_transfer.DESCRIPTION";
    }
  
    /**
     * item_transfer.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use ItemTransferPeer.item_transfer.COURIER_ID constant
     */
    public static String getItemTransfer_CourierId()
    {
        return "item_transfer.COURIER_ID";
    }
  
    /**
     * item_transfer.STATUS
     * @return the column name for the STATUS field
     * @deprecated use ItemTransferPeer.item_transfer.STATUS constant
     */
    public static String getItemTransfer_Status()
    {
        return "item_transfer.STATUS";
    }
  
    /**
     * item_transfer.REQUEST_ID
     * @return the column name for the REQUEST_ID field
     * @deprecated use ItemTransferPeer.item_transfer.REQUEST_ID constant
     */
    public static String getItemTransfer_RequestId()
    {
        return "item_transfer.REQUEST_ID";
    }
  
    /**
     * item_transfer.REQUEST_NO
     * @return the column name for the REQUEST_NO field
     * @deprecated use ItemTransferPeer.item_transfer.REQUEST_NO constant
     */
    public static String getItemTransfer_RequestNo()
    {
        return "item_transfer.REQUEST_NO";
    }
  
    /**
     * item_transfer.CONFIRM_BY
     * @return the column name for the CONFIRM_BY field
     * @deprecated use ItemTransferPeer.item_transfer.CONFIRM_BY constant
     */
    public static String getItemTransfer_ConfirmBy()
    {
        return "item_transfer.CONFIRM_BY";
    }
  
    /**
     * item_transfer.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use ItemTransferPeer.item_transfer.CONFIRM_DATE constant
     */
    public static String getItemTransfer_ConfirmDate()
    {
        return "item_transfer.CONFIRM_DATE";
    }
  
    /**
     * item_transfer.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use ItemTransferPeer.item_transfer.CANCEL_BY constant
     */
    public static String getItemTransfer_CancelBy()
    {
        return "item_transfer.CANCEL_BY";
    }
  
    /**
     * item_transfer.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use ItemTransferPeer.item_transfer.CANCEL_DATE constant
     */
    public static String getItemTransfer_CancelDate()
    {
        return "item_transfer.CANCEL_DATE";
    }
  
    /**
     * item_transfer.RECEIVE_BY
     * @return the column name for the RECEIVE_BY field
     * @deprecated use ItemTransferPeer.item_transfer.RECEIVE_BY constant
     */
    public static String getItemTransfer_ReceiveBy()
    {
        return "item_transfer.RECEIVE_BY";
    }
  
    /**
     * item_transfer.RECEIVE_DATE
     * @return the column name for the RECEIVE_DATE field
     * @deprecated use ItemTransferPeer.item_transfer.RECEIVE_DATE constant
     */
    public static String getItemTransfer_ReceiveDate()
    {
        return "item_transfer.RECEIVE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_transfer");
        TableMap tMap = dbMap.getTable("item_transfer");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_transfer.ITEM_TRANSFER_ID", "");
                          tMap.addColumn("item_transfer.TRANSACTION_NO", "");
                          tMap.addColumn("item_transfer.TRANSACTION_DATE", new Date());
                          tMap.addColumn("item_transfer.ADJUSTMENT_TYPE_ID", "");
                          tMap.addColumn("item_transfer.FROM_LOCATION_ID", "");
                          tMap.addColumn("item_transfer.FROM_LOCATION_NAME", "");
                          tMap.addColumn("item_transfer.TO_LOCATION_ID", "");
                          tMap.addColumn("item_transfer.TO_LOCATION_NAME", "");
                          tMap.addColumn("item_transfer.USER_NAME", "");
                          tMap.addColumn("item_transfer.DESCRIPTION", "");
                          tMap.addColumn("item_transfer.COURIER_ID", "");
                            tMap.addColumn("item_transfer.STATUS", Integer.valueOf(0));
                          tMap.addColumn("item_transfer.REQUEST_ID", "");
                          tMap.addColumn("item_transfer.REQUEST_NO", "");
                          tMap.addColumn("item_transfer.CONFIRM_BY", "");
                          tMap.addColumn("item_transfer.CONFIRM_DATE", new Date());
                          tMap.addColumn("item_transfer.CANCEL_BY", "");
                          tMap.addColumn("item_transfer.CANCEL_DATE", new Date());
                          tMap.addColumn("item_transfer.RECEIVE_BY", "");
                          tMap.addColumn("item_transfer.RECEIVE_DATE", new Date());
          }
}
