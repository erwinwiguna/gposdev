package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemVendorMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemVendorMapBuilder";

    /**
     * Item
     * @deprecated use ItemVendorPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_vendor";
    }

  
    /**
     * item_vendor.ITEM_VENDOR_ID
     * @return the column name for the ITEM_VENDOR_ID field
     * @deprecated use ItemVendorPeer.item_vendor.ITEM_VENDOR_ID constant
     */
    public static String getItemVendor_ItemVendorId()
    {
        return "item_vendor.ITEM_VENDOR_ID";
    }
  
    /**
     * item_vendor.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemVendorPeer.item_vendor.ITEM_ID constant
     */
    public static String getItemVendor_ItemId()
    {
        return "item_vendor.ITEM_ID";
    }
  
    /**
     * item_vendor.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use ItemVendorPeer.item_vendor.VENDOR_ID constant
     */
    public static String getItemVendor_VendorId()
    {
        return "item_vendor.VENDOR_ID";
    }
  
    /**
     * item_vendor.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use ItemVendorPeer.item_vendor.UPDATE_DATE constant
     */
    public static String getItemVendor_UpdateDate()
    {
        return "item_vendor.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_vendor");
        TableMap tMap = dbMap.getTable("item_vendor");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_vendor.ITEM_VENDOR_ID", "");
                          tMap.addColumn("item_vendor.ITEM_ID", "");
                          tMap.addColumn("item_vendor.VENDOR_ID", "");
                          tMap.addColumn("item_vendor.UPDATE_DATE", new Date());
          }
}
