package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class JournalVoucherMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.JournalVoucherMapBuilder";

    /**
     * Item
     * @deprecated use JournalVoucherPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "journal_voucher";
    }

  
    /**
     * journal_voucher.JOURNAL_VOUCHER_ID
     * @return the column name for the JOURNAL_VOUCHER_ID field
     * @deprecated use JournalVoucherPeer.journal_voucher.JOURNAL_VOUCHER_ID constant
     */
    public static String getJournalVoucher_JournalVoucherId()
    {
        return "journal_voucher.JOURNAL_VOUCHER_ID";
    }
  
    /**
     * journal_voucher.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use JournalVoucherPeer.journal_voucher.TRANSACTION_NO constant
     */
    public static String getJournalVoucher_TransactionNo()
    {
        return "journal_voucher.TRANSACTION_NO";
    }
  
    /**
     * journal_voucher.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use JournalVoucherPeer.journal_voucher.TRANSACTION_DATE constant
     */
    public static String getJournalVoucher_TransactionDate()
    {
        return "journal_voucher.TRANSACTION_DATE";
    }
  
    /**
     * journal_voucher.STATUS
     * @return the column name for the STATUS field
     * @deprecated use JournalVoucherPeer.journal_voucher.STATUS constant
     */
    public static String getJournalVoucher_Status()
    {
        return "journal_voucher.STATUS";
    }
  
    /**
     * journal_voucher.TOTAL_DEBIT
     * @return the column name for the TOTAL_DEBIT field
     * @deprecated use JournalVoucherPeer.journal_voucher.TOTAL_DEBIT constant
     */
    public static String getJournalVoucher_TotalDebit()
    {
        return "journal_voucher.TOTAL_DEBIT";
    }
  
    /**
     * journal_voucher.TOTAL_CREDIT
     * @return the column name for the TOTAL_CREDIT field
     * @deprecated use JournalVoucherPeer.journal_voucher.TOTAL_CREDIT constant
     */
    public static String getJournalVoucher_TotalCredit()
    {
        return "journal_voucher.TOTAL_CREDIT";
    }
  
    /**
     * journal_voucher.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use JournalVoucherPeer.journal_voucher.USER_NAME constant
     */
    public static String getJournalVoucher_UserName()
    {
        return "journal_voucher.USER_NAME";
    }
  
    /**
     * journal_voucher.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use JournalVoucherPeer.journal_voucher.DESCRIPTION constant
     */
    public static String getJournalVoucher_Description()
    {
        return "journal_voucher.DESCRIPTION";
    }
  
    /**
     * journal_voucher.RECURRING
     * @return the column name for the RECURRING field
     * @deprecated use JournalVoucherPeer.journal_voucher.RECURRING constant
     */
    public static String getJournalVoucher_Recurring()
    {
        return "journal_voucher.RECURRING";
    }
  
    /**
     * journal_voucher.NEXT_RECURRING_DATE
     * @return the column name for the NEXT_RECURRING_DATE field
     * @deprecated use JournalVoucherPeer.journal_voucher.NEXT_RECURRING_DATE constant
     */
    public static String getJournalVoucher_NextRecurringDate()
    {
        return "journal_voucher.NEXT_RECURRING_DATE";
    }
  
    /**
     * journal_voucher.TRANS_TYPE
     * @return the column name for the TRANS_TYPE field
     * @deprecated use JournalVoucherPeer.journal_voucher.TRANS_TYPE constant
     */
    public static String getJournalVoucher_TransType()
    {
        return "journal_voucher.TRANS_TYPE";
    }
  
    /**
     * journal_voucher.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use JournalVoucherPeer.journal_voucher.CANCEL_BY constant
     */
    public static String getJournalVoucher_CancelBy()
    {
        return "journal_voucher.CANCEL_BY";
    }
  
    /**
     * journal_voucher.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use JournalVoucherPeer.journal_voucher.CANCEL_DATE constant
     */
    public static String getJournalVoucher_CancelDate()
    {
        return "journal_voucher.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("journal_voucher");
        TableMap tMap = dbMap.getTable("journal_voucher");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("journal_voucher.JOURNAL_VOUCHER_ID", "");
                          tMap.addColumn("journal_voucher.TRANSACTION_NO", "");
                          tMap.addColumn("journal_voucher.TRANSACTION_DATE", new Date());
                            tMap.addColumn("journal_voucher.STATUS", Integer.valueOf(0));
                            tMap.addColumn("journal_voucher.TOTAL_DEBIT", bd_ZERO);
                            tMap.addColumn("journal_voucher.TOTAL_CREDIT", bd_ZERO);
                          tMap.addColumn("journal_voucher.USER_NAME", "");
                          tMap.addColumn("journal_voucher.DESCRIPTION", "");
                          tMap.addColumn("journal_voucher.RECURRING", Boolean.TRUE);
                          tMap.addColumn("journal_voucher.NEXT_RECURRING_DATE", new Date());
                            tMap.addColumn("journal_voucher.TRANS_TYPE", Integer.valueOf(0));
                          tMap.addColumn("journal_voucher.CANCEL_BY", "");
                          tMap.addColumn("journal_voucher.CANCEL_DATE", new Date());
          }
}
