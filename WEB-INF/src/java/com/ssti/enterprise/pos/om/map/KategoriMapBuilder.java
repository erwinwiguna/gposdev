package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class KategoriMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.KategoriMapBuilder";

    /**
     * Item
     * @deprecated use KategoriPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "kategori";
    }

  
    /**
     * kategori.KATEGORI_ID
     * @return the column name for the KATEGORI_ID field
     * @deprecated use KategoriPeer.kategori.KATEGORI_ID constant
     */
    public static String getKategori_KategoriId()
    {
        return "kategori.KATEGORI_ID";
    }
  
    /**
     * kategori.KATEGORI_CODE
     * @return the column name for the KATEGORI_CODE field
     * @deprecated use KategoriPeer.kategori.KATEGORI_CODE constant
     */
    public static String getKategori_KategoriCode()
    {
        return "kategori.KATEGORI_CODE";
    }
  
    /**
     * kategori.KATEGORI_LEVEL
     * @return the column name for the KATEGORI_LEVEL field
     * @deprecated use KategoriPeer.kategori.KATEGORI_LEVEL constant
     */
    public static String getKategori_KategoriLevel()
    {
        return "kategori.KATEGORI_LEVEL";
    }
  
    /**
     * kategori.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use KategoriPeer.kategori.DESCRIPTION constant
     */
    public static String getKategori_Description()
    {
        return "kategori.DESCRIPTION";
    }
  
    /**
     * kategori.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use KategoriPeer.kategori.PARENT_ID constant
     */
    public static String getKategori_ParentId()
    {
        return "kategori.PARENT_ID";
    }
  
    /**
     * kategori.HAS_CHILD
     * @return the column name for the HAS_CHILD field
     * @deprecated use KategoriPeer.kategori.HAS_CHILD constant
     */
    public static String getKategori_HasChild()
    {
        return "kategori.HAS_CHILD";
    }
  
    /**
     * kategori.ADD_DATE
     * @return the column name for the ADD_DATE field
     * @deprecated use KategoriPeer.kategori.ADD_DATE constant
     */
    public static String getKategori_AddDate()
    {
        return "kategori.ADD_DATE";
    }
  
    /**
     * kategori.INTERNAL_CODE
     * @return the column name for the INTERNAL_CODE field
     * @deprecated use KategoriPeer.kategori.INTERNAL_CODE constant
     */
    public static String getKategori_InternalCode()
    {
        return "kategori.INTERNAL_CODE";
    }
  
    /**
     * kategori.DISPLAY_STORE
     * @return the column name for the DISPLAY_STORE field
     * @deprecated use KategoriPeer.kategori.DISPLAY_STORE constant
     */
    public static String getKategori_DisplayStore()
    {
        return "kategori.DISPLAY_STORE";
    }
  
    /**
     * kategori.DISPLAY_DESC
     * @return the column name for the DISPLAY_DESC field
     * @deprecated use KategoriPeer.kategori.DISPLAY_DESC constant
     */
    public static String getKategori_DisplayDesc()
    {
        return "kategori.DISPLAY_DESC";
    }
  
    /**
     * kategori.PICTURE
     * @return the column name for the PICTURE field
     * @deprecated use KategoriPeer.kategori.PICTURE constant
     */
    public static String getKategori_Picture()
    {
        return "kategori.PICTURE";
    }
  
    /**
     * kategori.THUMBNAIL
     * @return the column name for the THUMBNAIL field
     * @deprecated use KategoriPeer.kategori.THUMBNAIL constant
     */
    public static String getKategori_Thumbnail()
    {
        return "kategori.THUMBNAIL";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("kategori");
        TableMap tMap = dbMap.getTable("kategori");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("kategori.KATEGORI_ID", "");
                          tMap.addColumn("kategori.KATEGORI_CODE", "");
                            tMap.addColumn("kategori.KATEGORI_LEVEL", Integer.valueOf(0));
                          tMap.addColumn("kategori.DESCRIPTION", "");
                          tMap.addColumn("kategori.PARENT_ID", "");
                          tMap.addColumn("kategori.HAS_CHILD", Boolean.TRUE);
                          tMap.addColumn("kategori.ADD_DATE", new Date());
                          tMap.addColumn("kategori.INTERNAL_CODE", "");
                          tMap.addColumn("kategori.DISPLAY_STORE", Boolean.TRUE);
                          tMap.addColumn("kategori.DISPLAY_DESC", "");
                          tMap.addColumn("kategori.PICTURE", "");
                          tMap.addColumn("kategori.THUMBNAIL", "");
          }
}
