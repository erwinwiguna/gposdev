package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class LocationMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.LocationMapBuilder";

    /**
     * Item
     * @deprecated use LocationPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "location";
    }

  
    /**
     * location.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use LocationPeer.location.LOCATION_ID constant
     */
    public static String getLocation_LocationId()
    {
        return "location.LOCATION_ID";
    }
  
    /**
     * location.LOCATION_CODE
     * @return the column name for the LOCATION_CODE field
     * @deprecated use LocationPeer.location.LOCATION_CODE constant
     */
    public static String getLocation_LocationCode()
    {
        return "location.LOCATION_CODE";
    }
  
    /**
     * location.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use LocationPeer.location.LOCATION_NAME constant
     */
    public static String getLocation_LocationName()
    {
        return "location.LOCATION_NAME";
    }
  
    /**
     * location.LOCATION_ADDRESS
     * @return the column name for the LOCATION_ADDRESS field
     * @deprecated use LocationPeer.location.LOCATION_ADDRESS constant
     */
    public static String getLocation_LocationAddress()
    {
        return "location.LOCATION_ADDRESS";
    }
  
    /**
     * location.SITE_ID
     * @return the column name for the SITE_ID field
     * @deprecated use LocationPeer.location.SITE_ID constant
     */
    public static String getLocation_SiteId()
    {
        return "location.SITE_ID";
    }
  
    /**
     * location.INVENTORY_TYPE
     * @return the column name for the INVENTORY_TYPE field
     * @deprecated use LocationPeer.location.INVENTORY_TYPE constant
     */
    public static String getLocation_InventoryType()
    {
        return "location.INVENTORY_TYPE";
    }
  
    /**
     * location.HO_STORE_LEADTIME
     * @return the column name for the HO_STORE_LEADTIME field
     * @deprecated use LocationPeer.location.HO_STORE_LEADTIME constant
     */
    public static String getLocation_HoStoreLeadtime()
    {
        return "location.HO_STORE_LEADTIME";
    }
  
    /**
     * location.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use LocationPeer.location.DESCRIPTION constant
     */
    public static String getLocation_Description()
    {
        return "location.DESCRIPTION";
    }
  
    /**
     * location.LOCATION_TYPE
     * @return the column name for the LOCATION_TYPE field
     * @deprecated use LocationPeer.location.LOCATION_TYPE constant
     */
    public static String getLocation_LocationType()
    {
        return "location.LOCATION_TYPE";
    }
  
    /**
     * location.OB_TRANS_ID
     * @return the column name for the OB_TRANS_ID field
     * @deprecated use LocationPeer.location.OB_TRANS_ID constant
     */
    public static String getLocation_ObTransId()
    {
        return "location.OB_TRANS_ID";
    }
  
    /**
     * location.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use LocationPeer.location.UPDATE_DATE constant
     */
    public static String getLocation_UpdateDate()
    {
        return "location.UPDATE_DATE";
    }
  
    /**
     * location.LOCATION_PHONE
     * @return the column name for the LOCATION_PHONE field
     * @deprecated use LocationPeer.location.LOCATION_PHONE constant
     */
    public static String getLocation_LocationPhone()
    {
        return "location.LOCATION_PHONE";
    }
  
    /**
     * location.LOCATION_EMAIL
     * @return the column name for the LOCATION_EMAIL field
     * @deprecated use LocationPeer.location.LOCATION_EMAIL constant
     */
    public static String getLocation_LocationEmail()
    {
        return "location.LOCATION_EMAIL";
    }
  
    /**
     * location.LOCATION_URL
     * @return the column name for the LOCATION_URL field
     * @deprecated use LocationPeer.location.LOCATION_URL constant
     */
    public static String getLocation_LocationUrl()
    {
        return "location.LOCATION_URL";
    }
  
    /**
     * location.PARENT_LOCATION_ID
     * @return the column name for the PARENT_LOCATION_ID field
     * @deprecated use LocationPeer.location.PARENT_LOCATION_ID constant
     */
    public static String getLocation_ParentLocationId()
    {
        return "location.PARENT_LOCATION_ID";
    }
  
    /**
     * location.TRANSFER_ACCOUNT
     * @return the column name for the TRANSFER_ACCOUNT field
     * @deprecated use LocationPeer.location.TRANSFER_ACCOUNT constant
     */
    public static String getLocation_TransferAccount()
    {
        return "location.TRANSFER_ACCOUNT";
    }
  
    /**
     * location.SALES_AREA_ID
     * @return the column name for the SALES_AREA_ID field
     * @deprecated use LocationPeer.location.SALES_AREA_ID constant
     */
    public static String getLocation_SalesAreaId()
    {
        return "location.SALES_AREA_ID";
    }
  
    /**
     * location.TERRITORY_ID
     * @return the column name for the TERRITORY_ID field
     * @deprecated use LocationPeer.location.TERRITORY_ID constant
     */
    public static String getLocation_TerritoryId()
    {
        return "location.TERRITORY_ID";
    }
  
    /**
     * location.LONGITUDES
     * @return the column name for the LONGITUDES field
     * @deprecated use LocationPeer.location.LONGITUDES constant
     */
    public static String getLocation_Longitudes()
    {
        return "location.LONGITUDES";
    }
  
    /**
     * location.LATITUDES
     * @return the column name for the LATITUDES field
     * @deprecated use LocationPeer.location.LATITUDES constant
     */
    public static String getLocation_Latitudes()
    {
        return "location.LATITUDES";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("location");
        TableMap tMap = dbMap.getTable("location");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("location.LOCATION_ID", "");
                          tMap.addColumn("location.LOCATION_CODE", "");
                          tMap.addColumn("location.LOCATION_NAME", "");
                          tMap.addColumn("location.LOCATION_ADDRESS", "");
                          tMap.addColumn("location.SITE_ID", "");
                            tMap.addColumn("location.INVENTORY_TYPE", Integer.valueOf(0));
                            tMap.addColumn("location.HO_STORE_LEADTIME", Integer.valueOf(0));
                          tMap.addColumn("location.DESCRIPTION", "");
                            tMap.addColumn("location.LOCATION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("location.OB_TRANS_ID", "");
                          tMap.addColumn("location.UPDATE_DATE", new Date());
                          tMap.addColumn("location.LOCATION_PHONE", "");
                          tMap.addColumn("location.LOCATION_EMAIL", "");
                          tMap.addColumn("location.LOCATION_URL", "");
                          tMap.addColumn("location.PARENT_LOCATION_ID", "");
                          tMap.addColumn("location.TRANSFER_ACCOUNT", "");
                          tMap.addColumn("location.SALES_AREA_ID", "");
                          tMap.addColumn("location.TERRITORY_ID", "");
                            tMap.addColumn("location.LONGITUDES", bd_ZERO);
                            tMap.addColumn("location.LATITUDES", bd_ZERO);
          }
}
