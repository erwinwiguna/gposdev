package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class OfflineTransMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.OfflineTransMapBuilder";

    /**
     * Item
     * @deprecated use OfflineTransPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "offline_trans";
    }

  
    /**
     * offline_trans.DOC_ID
     * @return the column name for the DOC_ID field
     * @deprecated use OfflineTransPeer.offline_trans.DOC_ID constant
     */
    public static String getOfflineTrans_DocId()
    {
        return "offline_trans.DOC_ID";
    }
  
    /**
     * offline_trans.DOC_TYPE
     * @return the column name for the DOC_TYPE field
     * @deprecated use OfflineTransPeer.offline_trans.DOC_TYPE constant
     */
    public static String getOfflineTrans_DocType()
    {
        return "offline_trans.DOC_TYPE";
    }
  
    /**
     * offline_trans.IP_ADDRESS
     * @return the column name for the IP_ADDRESS field
     * @deprecated use OfflineTransPeer.offline_trans.IP_ADDRESS constant
     */
    public static String getOfflineTrans_IpAddress()
    {
        return "offline_trans.IP_ADDRESS";
    }
  
    /**
     * offline_trans.JSON_DOC
     * @return the column name for the JSON_DOC field
     * @deprecated use OfflineTransPeer.offline_trans.JSON_DOC constant
     */
    public static String getOfflineTrans_JsonDoc()
    {
        return "offline_trans.JSON_DOC";
    }
  
    /**
     * offline_trans.RECEIVE_DATE
     * @return the column name for the RECEIVE_DATE field
     * @deprecated use OfflineTransPeer.offline_trans.RECEIVE_DATE constant
     */
    public static String getOfflineTrans_ReceiveDate()
    {
        return "offline_trans.RECEIVE_DATE";
    }
  
    /**
     * offline_trans.PROCESS_DATE
     * @return the column name for the PROCESS_DATE field
     * @deprecated use OfflineTransPeer.offline_trans.PROCESS_DATE constant
     */
    public static String getOfflineTrans_ProcessDate()
    {
        return "offline_trans.PROCESS_DATE";
    }
  
    /**
     * offline_trans.PROCESS_BY
     * @return the column name for the PROCESS_BY field
     * @deprecated use OfflineTransPeer.offline_trans.PROCESS_BY constant
     */
    public static String getOfflineTrans_ProcessBy()
    {
        return "offline_trans.PROCESS_BY";
    }
  
    /**
     * offline_trans.PROCESS_LOG
     * @return the column name for the PROCESS_LOG field
     * @deprecated use OfflineTransPeer.offline_trans.PROCESS_LOG constant
     */
    public static String getOfflineTrans_ProcessLog()
    {
        return "offline_trans.PROCESS_LOG";
    }
  
    /**
     * offline_trans.STATUS
     * @return the column name for the STATUS field
     * @deprecated use OfflineTransPeer.offline_trans.STATUS constant
     */
    public static String getOfflineTrans_Status()
    {
        return "offline_trans.STATUS";
    }
  
    /**
     * offline_trans.TRANS_ID
     * @return the column name for the TRANS_ID field
     * @deprecated use OfflineTransPeer.offline_trans.TRANS_ID constant
     */
    public static String getOfflineTrans_TransId()
    {
        return "offline_trans.TRANS_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("offline_trans");
        TableMap tMap = dbMap.getTable("offline_trans");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("offline_trans.DOC_ID", "");
                            tMap.addColumn("offline_trans.DOC_TYPE", Integer.valueOf(0));
                          tMap.addColumn("offline_trans.IP_ADDRESS", "");
                          tMap.addColumn("offline_trans.JSON_DOC", "");
                          tMap.addColumn("offline_trans.RECEIVE_DATE", new Date());
                          tMap.addColumn("offline_trans.PROCESS_DATE", new Date());
                          tMap.addColumn("offline_trans.PROCESS_BY", "");
                          tMap.addColumn("offline_trans.PROCESS_LOG", "");
                            tMap.addColumn("offline_trans.STATUS", Integer.valueOf(0));
                          tMap.addColumn("offline_trans.TRANS_ID", "");
          }
}
