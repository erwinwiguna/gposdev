package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PaymentBillsMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PaymentBillsMapBuilder";

    /**
     * Item
     * @deprecated use PaymentBillsPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "payment_bills";
    }

  
    /**
     * payment_bills.PAYMENT_BILLS_ID
     * @return the column name for the PAYMENT_BILLS_ID field
     * @deprecated use PaymentBillsPeer.payment_bills.PAYMENT_BILLS_ID constant
     */
    public static String getPaymentBills_PaymentBillsId()
    {
        return "payment_bills.PAYMENT_BILLS_ID";
    }
  
    /**
     * payment_bills.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PaymentBillsPeer.payment_bills.DESCRIPTION constant
     */
    public static String getPaymentBills_Description()
    {
        return "payment_bills.DESCRIPTION";
    }
  
    /**
     * payment_bills.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use PaymentBillsPeer.payment_bills.AMOUNT constant
     */
    public static String getPaymentBills_Amount()
    {
        return "payment_bills.AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("payment_bills");
        TableMap tMap = dbMap.getTable("payment_bills");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("payment_bills.PAYMENT_BILLS_ID", "");
                          tMap.addColumn("payment_bills.DESCRIPTION", "");
                            tMap.addColumn("payment_bills.AMOUNT", Integer.valueOf(0));
          }
}
