package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PaymentTermMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PaymentTermMapBuilder";

    /**
     * Item
     * @deprecated use PaymentTermPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "payment_term";
    }

  
    /**
     * payment_term.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use PaymentTermPeer.payment_term.PAYMENT_TERM_ID constant
     */
    public static String getPaymentTerm_PaymentTermId()
    {
        return "payment_term.PAYMENT_TERM_ID";
    }
  
    /**
     * payment_term.PAYMENT_TERM_CODE
     * @return the column name for the PAYMENT_TERM_CODE field
     * @deprecated use PaymentTermPeer.payment_term.PAYMENT_TERM_CODE constant
     */
    public static String getPaymentTerm_PaymentTermCode()
    {
        return "payment_term.PAYMENT_TERM_CODE";
    }
  
    /**
     * payment_term.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use PaymentTermPeer.payment_term.DISCOUNT constant
     */
    public static String getPaymentTerm_Discount()
    {
        return "payment_term.DISCOUNT";
    }
  
    /**
     * payment_term.PAYMENT_DAY
     * @return the column name for the PAYMENT_DAY field
     * @deprecated use PaymentTermPeer.payment_term.PAYMENT_DAY constant
     */
    public static String getPaymentTerm_PaymentDay()
    {
        return "payment_term.PAYMENT_DAY";
    }
  
    /**
     * payment_term.NET_DAY
     * @return the column name for the NET_DAY field
     * @deprecated use PaymentTermPeer.payment_term.NET_DAY constant
     */
    public static String getPaymentTerm_NetDay()
    {
        return "payment_term.NET_DAY";
    }
  
    /**
     * payment_term.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PaymentTermPeer.payment_term.DESCRIPTION constant
     */
    public static String getPaymentTerm_Description()
    {
        return "payment_term.DESCRIPTION";
    }
  
    /**
     * payment_term.CASH_PAYMENT
     * @return the column name for the CASH_PAYMENT field
     * @deprecated use PaymentTermPeer.payment_term.CASH_PAYMENT constant
     */
    public static String getPaymentTerm_CashPayment()
    {
        return "payment_term.CASH_PAYMENT";
    }
  
    /**
     * payment_term.IS_DEFAULT
     * @return the column name for the IS_DEFAULT field
     * @deprecated use PaymentTermPeer.payment_term.IS_DEFAULT constant
     */
    public static String getPaymentTerm_IsDefault()
    {
        return "payment_term.IS_DEFAULT";
    }
  
    /**
     * payment_term.IS_MULTIPLE_PAYMENT
     * @return the column name for the IS_MULTIPLE_PAYMENT field
     * @deprecated use PaymentTermPeer.payment_term.IS_MULTIPLE_PAYMENT constant
     */
    public static String getPaymentTerm_IsMultiplePayment()
    {
        return "payment_term.IS_MULTIPLE_PAYMENT";
    }
  
    /**
     * payment_term.IS_EDC
     * @return the column name for the IS_EDC field
     * @deprecated use PaymentTermPeer.payment_term.IS_EDC constant
     */
    public static String getPaymentTerm_IsEdc()
    {
        return "payment_term.IS_EDC";
    }
  
    /**
     * payment_term.PAYMENT_TYPE_BANK_ID
     * @return the column name for the PAYMENT_TYPE_BANK_ID field
     * @deprecated use PaymentTermPeer.payment_term.PAYMENT_TYPE_BANK_ID constant
     */
    public static String getPaymentTerm_PaymentTypeBankId()
    {
        return "payment_term.PAYMENT_TYPE_BANK_ID";
    }
  
    /**
     * payment_term.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use PaymentTermPeer.payment_term.BANK_ID constant
     */
    public static String getPaymentTerm_BankId()
    {
        return "payment_term.BANK_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("payment_term");
        TableMap tMap = dbMap.getTable("payment_term");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("payment_term.PAYMENT_TERM_ID", "");
                          tMap.addColumn("payment_term.PAYMENT_TERM_CODE", "");
                          tMap.addColumn("payment_term.DISCOUNT", "");
                            tMap.addColumn("payment_term.PAYMENT_DAY", Integer.valueOf(0));
                            tMap.addColumn("payment_term.NET_DAY", Integer.valueOf(0));
                          tMap.addColumn("payment_term.DESCRIPTION", "");
                          tMap.addColumn("payment_term.CASH_PAYMENT", Boolean.TRUE);
                          tMap.addColumn("payment_term.IS_DEFAULT", Boolean.TRUE);
                          tMap.addColumn("payment_term.IS_MULTIPLE_PAYMENT", Boolean.TRUE);
                          tMap.addColumn("payment_term.IS_EDC", Boolean.TRUE);
                          tMap.addColumn("payment_term.PAYMENT_TYPE_BANK_ID", "");
                          tMap.addColumn("payment_term.BANK_ID", "");
          }
}
