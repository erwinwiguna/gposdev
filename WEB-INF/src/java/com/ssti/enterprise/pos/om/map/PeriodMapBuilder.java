package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PeriodMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PeriodMapBuilder";

    /**
     * Item
     * @deprecated use PeriodPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "period";
    }

  
    /**
     * period.PERIOD_ID
     * @return the column name for the PERIOD_ID field
     * @deprecated use PeriodPeer.period.PERIOD_ID constant
     */
    public static String getPeriod_PeriodId()
    {
        return "period.PERIOD_ID";
    }
  
    /**
     * period.BEGIN_DATE
     * @return the column name for the BEGIN_DATE field
     * @deprecated use PeriodPeer.period.BEGIN_DATE constant
     */
    public static String getPeriod_BeginDate()
    {
        return "period.BEGIN_DATE";
    }
  
    /**
     * period.END_DATE
     * @return the column name for the END_DATE field
     * @deprecated use PeriodPeer.period.END_DATE constant
     */
    public static String getPeriod_EndDate()
    {
        return "period.END_DATE";
    }
  
    /**
     * period.MONTH
     * @return the column name for the MONTH field
     * @deprecated use PeriodPeer.period.MONTH constant
     */
    public static String getPeriod_Month()
    {
        return "period.MONTH";
    }
  
    /**
     * period.YEAR
     * @return the column name for the YEAR field
     * @deprecated use PeriodPeer.period.YEAR constant
     */
    public static String getPeriod_Year()
    {
        return "period.YEAR";
    }
  
    /**
     * period.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PeriodPeer.period.DESCRIPTION constant
     */
    public static String getPeriod_Description()
    {
        return "period.DESCRIPTION";
    }
  
    /**
     * period.IS_CLOSED
     * @return the column name for the IS_CLOSED field
     * @deprecated use PeriodPeer.period.IS_CLOSED constant
     */
    public static String getPeriod_IsClosed()
    {
        return "period.IS_CLOSED";
    }
  
    /**
     * period.PERIOD_CODE
     * @return the column name for the PERIOD_CODE field
     * @deprecated use PeriodPeer.period.PERIOD_CODE constant
     */
    public static String getPeriod_PeriodCode()
    {
        return "period.PERIOD_CODE";
    }
  
    /**
     * period.CLOSED_BY
     * @return the column name for the CLOSED_BY field
     * @deprecated use PeriodPeer.period.CLOSED_BY constant
     */
    public static String getPeriod_ClosedBy()
    {
        return "period.CLOSED_BY";
    }
  
    /**
     * period.CLOSED_DATE
     * @return the column name for the CLOSED_DATE field
     * @deprecated use PeriodPeer.period.CLOSED_DATE constant
     */
    public static String getPeriod_ClosedDate()
    {
        return "period.CLOSED_DATE";
    }
  
    /**
     * period.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PeriodPeer.period.REMARK constant
     */
    public static String getPeriod_Remark()
    {
        return "period.REMARK";
    }
  
    /**
     * period.CLOSE_TRANS_ID
     * @return the column name for the CLOSE_TRANS_ID field
     * @deprecated use PeriodPeer.period.CLOSE_TRANS_ID constant
     */
    public static String getPeriod_CloseTransId()
    {
        return "period.CLOSE_TRANS_ID";
    }
  
    /**
     * period.DEPR_TRANS_ID
     * @return the column name for the DEPR_TRANS_ID field
     * @deprecated use PeriodPeer.period.DEPR_TRANS_ID constant
     */
    public static String getPeriod_DeprTransId()
    {
        return "period.DEPR_TRANS_ID";
    }
  
    /**
     * period.IS_FA_CLOSED
     * @return the column name for the IS_FA_CLOSED field
     * @deprecated use PeriodPeer.period.IS_FA_CLOSED constant
     */
    public static String getPeriod_IsFaClosed()
    {
        return "period.IS_FA_CLOSED";
    }
  
    /**
     * period.FA_CLOSED_BY
     * @return the column name for the FA_CLOSED_BY field
     * @deprecated use PeriodPeer.period.FA_CLOSED_BY constant
     */
    public static String getPeriod_FaClosedBy()
    {
        return "period.FA_CLOSED_BY";
    }
  
    /**
     * period.FA_CLOSED_DATE
     * @return the column name for the FA_CLOSED_DATE field
     * @deprecated use PeriodPeer.period.FA_CLOSED_DATE constant
     */
    public static String getPeriod_FaClosedDate()
    {
        return "period.FA_CLOSED_DATE";
    }
  
    /**
     * period.FA_CLOSING_REMARK
     * @return the column name for the FA_CLOSING_REMARK field
     * @deprecated use PeriodPeer.period.FA_CLOSING_REMARK constant
     */
    public static String getPeriod_FaClosingRemark()
    {
        return "period.FA_CLOSING_REMARK";
    }
  
    /**
     * period.IS_YEAR_CLOSED
     * @return the column name for the IS_YEAR_CLOSED field
     * @deprecated use PeriodPeer.period.IS_YEAR_CLOSED constant
     */
    public static String getPeriod_IsYearClosed()
    {
        return "period.IS_YEAR_CLOSED";
    }
  
    /**
     * period.CLOSE_YEAR_TRANS_ID
     * @return the column name for the CLOSE_YEAR_TRANS_ID field
     * @deprecated use PeriodPeer.period.CLOSE_YEAR_TRANS_ID constant
     */
    public static String getPeriod_CloseYearTransId()
    {
        return "period.CLOSE_YEAR_TRANS_ID";
    }
  
    /**
     * period.CLOSE_YEAR_REMARK
     * @return the column name for the CLOSE_YEAR_REMARK field
     * @deprecated use PeriodPeer.period.CLOSE_YEAR_REMARK constant
     */
    public static String getPeriod_CloseYearRemark()
    {
        return "period.CLOSE_YEAR_REMARK";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("period");
        TableMap tMap = dbMap.getTable("period");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("period.PERIOD_ID", "");
                          tMap.addColumn("period.BEGIN_DATE", new Date());
                          tMap.addColumn("period.END_DATE", new Date());
                            tMap.addColumn("period.MONTH", Integer.valueOf(0));
                            tMap.addColumn("period.YEAR", Integer.valueOf(0));
                          tMap.addColumn("period.DESCRIPTION", "");
                          tMap.addColumn("period.IS_CLOSED", Boolean.TRUE);
                            tMap.addColumn("period.PERIOD_CODE", Integer.valueOf(0));
                          tMap.addColumn("period.CLOSED_BY", "");
                          tMap.addColumn("period.CLOSED_DATE", new Date());
                          tMap.addColumn("period.REMARK", "");
                          tMap.addColumn("period.CLOSE_TRANS_ID", "");
                          tMap.addColumn("period.DEPR_TRANS_ID", "");
                          tMap.addColumn("period.IS_FA_CLOSED", Boolean.TRUE);
                          tMap.addColumn("period.FA_CLOSED_BY", "");
                          tMap.addColumn("period.FA_CLOSED_DATE", new Date());
                          tMap.addColumn("period.FA_CLOSING_REMARK", "");
                          tMap.addColumn("period.IS_YEAR_CLOSED", Boolean.TRUE);
                          tMap.addColumn("period.CLOSE_YEAR_TRANS_ID", "");
                          tMap.addColumn("period.CLOSE_YEAR_REMARK", "");
          }
}
