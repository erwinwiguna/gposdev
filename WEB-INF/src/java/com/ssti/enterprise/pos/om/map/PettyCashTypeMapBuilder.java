package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PettyCashTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PettyCashTypeMapBuilder";

    /**
     * Item
     * @deprecated use PettyCashTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "petty_cash_type";
    }

  
    /**
     * petty_cash_type.PETTY_CASH_TYPE_ID
     * @return the column name for the PETTY_CASH_TYPE_ID field
     * @deprecated use PettyCashTypePeer.petty_cash_type.PETTY_CASH_TYPE_ID constant
     */
    public static String getPettyCashType_PettyCashTypeId()
    {
        return "petty_cash_type.PETTY_CASH_TYPE_ID";
    }
  
    /**
     * petty_cash_type.INCOMING_OUTGOING
     * @return the column name for the INCOMING_OUTGOING field
     * @deprecated use PettyCashTypePeer.petty_cash_type.INCOMING_OUTGOING constant
     */
    public static String getPettyCashType_IncomingOutgoing()
    {
        return "petty_cash_type.INCOMING_OUTGOING";
    }
  
    /**
     * petty_cash_type.PETTY_CASH_CODE
     * @return the column name for the PETTY_CASH_CODE field
     * @deprecated use PettyCashTypePeer.petty_cash_type.PETTY_CASH_CODE constant
     */
    public static String getPettyCashType_PettyCashCode()
    {
        return "petty_cash_type.PETTY_CASH_CODE";
    }
  
    /**
     * petty_cash_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PettyCashTypePeer.petty_cash_type.DESCRIPTION constant
     */
    public static String getPettyCashType_Description()
    {
        return "petty_cash_type.DESCRIPTION";
    }
  
    /**
     * petty_cash_type.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use PettyCashTypePeer.petty_cash_type.ACCOUNT_ID constant
     */
    public static String getPettyCashType_AccountId()
    {
        return "petty_cash_type.ACCOUNT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("petty_cash_type");
        TableMap tMap = dbMap.getTable("petty_cash_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("petty_cash_type.PETTY_CASH_TYPE_ID", "");
                            tMap.addColumn("petty_cash_type.INCOMING_OUTGOING", Integer.valueOf(0));
                          tMap.addColumn("petty_cash_type.PETTY_CASH_CODE", "");
                          tMap.addColumn("petty_cash_type.DESCRIPTION", "");
                          tMap.addColumn("petty_cash_type.ACCOUNT_ID", "");
          }
}
