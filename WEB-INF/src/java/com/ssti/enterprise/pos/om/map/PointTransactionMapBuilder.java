package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PointTransactionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PointTransactionMapBuilder";

    /**
     * Item
     * @deprecated use PointTransactionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "point_transaction";
    }

  
    /**
     * point_transaction.POINT_TRANSACTION_ID
     * @return the column name for the POINT_TRANSACTION_ID field
     * @deprecated use PointTransactionPeer.point_transaction.POINT_TRANSACTION_ID constant
     */
    public static String getPointTransaction_PointTransactionId()
    {
        return "point_transaction.POINT_TRANSACTION_ID";
    }
  
    /**
     * point_transaction.POINT_TYPE_ID
     * @return the column name for the POINT_TYPE_ID field
     * @deprecated use PointTransactionPeer.point_transaction.POINT_TYPE_ID constant
     */
    public static String getPointTransaction_PointTypeId()
    {
        return "point_transaction.POINT_TYPE_ID";
    }
  
    /**
     * point_transaction.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use PointTransactionPeer.point_transaction.TRANSACTION_TYPE constant
     */
    public static String getPointTransaction_TransactionType()
    {
        return "point_transaction.TRANSACTION_TYPE";
    }
  
    /**
     * point_transaction.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use PointTransactionPeer.point_transaction.CUSTOMER_ID constant
     */
    public static String getPointTransaction_CustomerId()
    {
        return "point_transaction.CUSTOMER_ID";
    }
  
    /**
     * point_transaction.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use PointTransactionPeer.point_transaction.TRANSACTION_ID constant
     */
    public static String getPointTransaction_TransactionId()
    {
        return "point_transaction.TRANSACTION_ID";
    }
  
    /**
     * point_transaction.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use PointTransactionPeer.point_transaction.TRANSACTION_NO constant
     */
    public static String getPointTransaction_TransactionNo()
    {
        return "point_transaction.TRANSACTION_NO";
    }
  
    /**
     * point_transaction.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use PointTransactionPeer.point_transaction.TRANSACTION_DATE constant
     */
    public static String getPointTransaction_TransactionDate()
    {
        return "point_transaction.TRANSACTION_DATE";
    }
  
    /**
     * point_transaction.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PointTransactionPeer.point_transaction.LOCATION_ID constant
     */
    public static String getPointTransaction_LocationId()
    {
        return "point_transaction.LOCATION_ID";
    }
  
    /**
     * point_transaction.POINT_BEFORE
     * @return the column name for the POINT_BEFORE field
     * @deprecated use PointTransactionPeer.point_transaction.POINT_BEFORE constant
     */
    public static String getPointTransaction_PointBefore()
    {
        return "point_transaction.POINT_BEFORE";
    }
  
    /**
     * point_transaction.POINT_CHANGES
     * @return the column name for the POINT_CHANGES field
     * @deprecated use PointTransactionPeer.point_transaction.POINT_CHANGES constant
     */
    public static String getPointTransaction_PointChanges()
    {
        return "point_transaction.POINT_CHANGES";
    }
  
    /**
     * point_transaction.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PointTransactionPeer.point_transaction.DESCRIPTION constant
     */
    public static String getPointTransaction_Description()
    {
        return "point_transaction.DESCRIPTION";
    }
  
    /**
     * point_transaction.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use PointTransactionPeer.point_transaction.CREATE_DATE constant
     */
    public static String getPointTransaction_CreateDate()
    {
        return "point_transaction.CREATE_DATE";
    }
  
    /**
     * point_transaction.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use PointTransactionPeer.point_transaction.USER_NAME constant
     */
    public static String getPointTransaction_UserName()
    {
        return "point_transaction.USER_NAME";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("point_transaction");
        TableMap tMap = dbMap.getTable("point_transaction");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("point_transaction.POINT_TRANSACTION_ID", "");
                          tMap.addColumn("point_transaction.POINT_TYPE_ID", "");
                            tMap.addColumn("point_transaction.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("point_transaction.CUSTOMER_ID", "");
                          tMap.addColumn("point_transaction.TRANSACTION_ID", "");
                          tMap.addColumn("point_transaction.TRANSACTION_NO", "");
                          tMap.addColumn("point_transaction.TRANSACTION_DATE", new Date());
                          tMap.addColumn("point_transaction.LOCATION_ID", "");
                            tMap.addColumn("point_transaction.POINT_BEFORE", bd_ZERO);
                            tMap.addColumn("point_transaction.POINT_CHANGES", bd_ZERO);
                          tMap.addColumn("point_transaction.DESCRIPTION", "");
                          tMap.addColumn("point_transaction.CREATE_DATE", new Date());
                          tMap.addColumn("point_transaction.USER_NAME", "");
          }
}
