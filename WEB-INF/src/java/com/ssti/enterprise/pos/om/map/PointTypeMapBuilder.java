package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PointTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PointTypeMapBuilder";

    /**
     * Item
     * @deprecated use PointTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "point_type";
    }

  
    /**
     * point_type.POINT_TYPE_ID
     * @return the column name for the POINT_TYPE_ID field
     * @deprecated use PointTypePeer.point_type.POINT_TYPE_ID constant
     */
    public static String getPointType_PointTypeId()
    {
        return "point_type.POINT_TYPE_ID";
    }
  
    /**
     * point_type.CODE
     * @return the column name for the CODE field
     * @deprecated use PointTypePeer.point_type.CODE constant
     */
    public static String getPointType_Code()
    {
        return "point_type.CODE";
    }
  
    /**
     * point_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PointTypePeer.point_type.DESCRIPTION constant
     */
    public static String getPointType_Description()
    {
        return "point_type.DESCRIPTION";
    }
  
    /**
     * point_type.FOR_SI
     * @return the column name for the FOR_SI field
     * @deprecated use PointTypePeer.point_type.FOR_SI constant
     */
    public static String getPointType_ForSi()
    {
        return "point_type.FOR_SI";
    }
  
    /**
     * point_type.FOR_SR
     * @return the column name for the FOR_SR field
     * @deprecated use PointTypePeer.point_type.FOR_SR constant
     */
    public static String getPointType_ForSr()
    {
        return "point_type.FOR_SR";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("point_type");
        TableMap tMap = dbMap.getTable("point_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("point_type.POINT_TYPE_ID", "");
                          tMap.addColumn("point_type.CODE", "");
                          tMap.addColumn("point_type.DESCRIPTION", "");
                          tMap.addColumn("point_type.FOR_SI", Boolean.TRUE);
                          tMap.addColumn("point_type.FOR_SR", Boolean.TRUE);
          }
}
