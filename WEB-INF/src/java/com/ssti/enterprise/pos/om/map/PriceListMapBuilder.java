package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PriceListMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PriceListMapBuilder";

    /**
     * Item
     * @deprecated use PriceListPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "price_list";
    }

  
    /**
     * price_list.PRICE_LIST_ID
     * @return the column name for the PRICE_LIST_ID field
     * @deprecated use PriceListPeer.price_list.PRICE_LIST_ID constant
     */
    public static String getPriceList_PriceListId()
    {
        return "price_list.PRICE_LIST_ID";
    }
  
    /**
     * price_list.PRICE_LIST_CODE
     * @return the column name for the PRICE_LIST_CODE field
     * @deprecated use PriceListPeer.price_list.PRICE_LIST_CODE constant
     */
    public static String getPriceList_PriceListCode()
    {
        return "price_list.PRICE_LIST_CODE";
    }
  
    /**
     * price_list.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use PriceListPeer.price_list.CUSTOMER_ID constant
     */
    public static String getPriceList_CustomerId()
    {
        return "price_list.CUSTOMER_ID";
    }
  
    /**
     * price_list.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use PriceListPeer.price_list.CUSTOMER_TYPE_ID constant
     */
    public static String getPriceList_CustomerTypeId()
    {
        return "price_list.CUSTOMER_TYPE_ID";
    }
  
    /**
     * price_list.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PriceListPeer.price_list.LOCATION_ID constant
     */
    public static String getPriceList_LocationId()
    {
        return "price_list.LOCATION_ID";
    }
  
    /**
     * price_list.PRICE_LIST_TYPE
     * @return the column name for the PRICE_LIST_TYPE field
     * @deprecated use PriceListPeer.price_list.PRICE_LIST_TYPE constant
     */
    public static String getPriceList_PriceListType()
    {
        return "price_list.PRICE_LIST_TYPE";
    }
  
    /**
     * price_list.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use PriceListPeer.price_list.CREATE_DATE constant
     */
    public static String getPriceList_CreateDate()
    {
        return "price_list.CREATE_DATE";
    }
  
    /**
     * price_list.EXPIRED_DATE
     * @return the column name for the EXPIRED_DATE field
     * @deprecated use PriceListPeer.price_list.EXPIRED_DATE constant
     */
    public static String getPriceList_ExpiredDate()
    {
        return "price_list.EXPIRED_DATE";
    }
  
    /**
     * price_list.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PriceListPeer.price_list.REMARK constant
     */
    public static String getPriceList_Remark()
    {
        return "price_list.REMARK";
    }
  
    /**
     * price_list.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use PriceListPeer.price_list.UPDATE_DATE constant
     */
    public static String getPriceList_UpdateDate()
    {
        return "price_list.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("price_list");
        TableMap tMap = dbMap.getTable("price_list");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("price_list.PRICE_LIST_ID", "");
                          tMap.addColumn("price_list.PRICE_LIST_CODE", "");
                          tMap.addColumn("price_list.CUSTOMER_ID", "");
                          tMap.addColumn("price_list.CUSTOMER_TYPE_ID", "");
                          tMap.addColumn("price_list.LOCATION_ID", "");
                            tMap.addColumn("price_list.PRICE_LIST_TYPE", Integer.valueOf(0));
                          tMap.addColumn("price_list.CREATE_DATE", new Date());
                          tMap.addColumn("price_list.EXPIRED_DATE", new Date());
                          tMap.addColumn("price_list.REMARK", "");
                          tMap.addColumn("price_list.UPDATE_DATE", new Date());
          }
}
