package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseOrderDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseOrderDetailMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseOrderDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_order_detail";
    }

  
    /**
     * purchase_order_detail.PURCHASE_ORDER_DETAIL_ID
     * @return the column name for the PURCHASE_ORDER_DETAIL_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.PURCHASE_ORDER_DETAIL_ID constant
     */
    public static String getPurchaseOrderDetail_PurchaseOrderDetailId()
    {
        return "purchase_order_detail.PURCHASE_ORDER_DETAIL_ID";
    }
  
    /**
     * purchase_order_detail.PURCHASE_ORDER_ID
     * @return the column name for the PURCHASE_ORDER_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.PURCHASE_ORDER_ID constant
     */
    public static String getPurchaseOrderDetail_PurchaseOrderId()
    {
        return "purchase_order_detail.PURCHASE_ORDER_ID";
    }
  
    /**
     * purchase_order_detail.REQUEST_TYPE
     * @return the column name for the REQUEST_TYPE field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.REQUEST_TYPE constant
     */
    public static String getPurchaseOrderDetail_RequestType()
    {
        return "purchase_order_detail.REQUEST_TYPE";
    }
  
    /**
     * purchase_order_detail.REQUEST_DETAIL_ID
     * @return the column name for the REQUEST_DETAIL_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.REQUEST_DETAIL_ID constant
     */
    public static String getPurchaseOrderDetail_RequestDetailId()
    {
        return "purchase_order_detail.REQUEST_DETAIL_ID";
    }
  
    /**
     * purchase_order_detail.REQUEST_ID
     * @return the column name for the REQUEST_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.REQUEST_ID constant
     */
    public static String getPurchaseOrderDetail_RequestId()
    {
        return "purchase_order_detail.REQUEST_ID";
    }
  
    /**
     * purchase_order_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.INDEX_NO constant
     */
    public static String getPurchaseOrderDetail_IndexNo()
    {
        return "purchase_order_detail.INDEX_NO";
    }
  
    /**
     * purchase_order_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.ITEM_ID constant
     */
    public static String getPurchaseOrderDetail_ItemId()
    {
        return "purchase_order_detail.ITEM_ID";
    }
  
    /**
     * purchase_order_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.ITEM_CODE constant
     */
    public static String getPurchaseOrderDetail_ItemCode()
    {
        return "purchase_order_detail.ITEM_CODE";
    }
  
    /**
     * purchase_order_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.ITEM_NAME constant
     */
    public static String getPurchaseOrderDetail_ItemName()
    {
        return "purchase_order_detail.ITEM_NAME";
    }
  
    /**
     * purchase_order_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.DESCRIPTION constant
     */
    public static String getPurchaseOrderDetail_Description()
    {
        return "purchase_order_detail.DESCRIPTION";
    }
  
    /**
     * purchase_order_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.UNIT_ID constant
     */
    public static String getPurchaseOrderDetail_UnitId()
    {
        return "purchase_order_detail.UNIT_ID";
    }
  
    /**
     * purchase_order_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.UNIT_CODE constant
     */
    public static String getPurchaseOrderDetail_UnitCode()
    {
        return "purchase_order_detail.UNIT_CODE";
    }
  
    /**
     * purchase_order_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.TAX_ID constant
     */
    public static String getPurchaseOrderDetail_TaxId()
    {
        return "purchase_order_detail.TAX_ID";
    }
  
    /**
     * purchase_order_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.TAX_AMOUNT constant
     */
    public static String getPurchaseOrderDetail_TaxAmount()
    {
        return "purchase_order_detail.TAX_AMOUNT";
    }
  
    /**
     * purchase_order_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.SUB_TOTAL_TAX constant
     */
    public static String getPurchaseOrderDetail_SubTotalTax()
    {
        return "purchase_order_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * purchase_order_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.DISCOUNT constant
     */
    public static String getPurchaseOrderDetail_Discount()
    {
        return "purchase_order_detail.DISCOUNT";
    }
  
    /**
     * purchase_order_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.SUB_TOTAL_DISC constant
     */
    public static String getPurchaseOrderDetail_SubTotalDisc()
    {
        return "purchase_order_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * purchase_order_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.QTY constant
     */
    public static String getPurchaseOrderDetail_Qty()
    {
        return "purchase_order_detail.QTY";
    }
  
    /**
     * purchase_order_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.QTY_BASE constant
     */
    public static String getPurchaseOrderDetail_QtyBase()
    {
        return "purchase_order_detail.QTY_BASE";
    }
  
    /**
     * purchase_order_detail.QTY_AUTO
     * @return the column name for the QTY_AUTO field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.QTY_AUTO constant
     */
    public static String getPurchaseOrderDetail_QtyAuto()
    {
        return "purchase_order_detail.QTY_AUTO";
    }
  
    /**
     * purchase_order_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.ITEM_PRICE constant
     */
    public static String getPurchaseOrderDetail_ItemPrice()
    {
        return "purchase_order_detail.ITEM_PRICE";
    }
  
    /**
     * purchase_order_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.SUB_TOTAL constant
     */
    public static String getPurchaseOrderDetail_SubTotal()
    {
        return "purchase_order_detail.SUB_TOTAL";
    }
  
    /**
     * purchase_order_detail.LAST_PURCHASE
     * @return the column name for the LAST_PURCHASE field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.LAST_PURCHASE constant
     */
    public static String getPurchaseOrderDetail_LastPurchase()
    {
        return "purchase_order_detail.LAST_PURCHASE";
    }
  
    /**
     * purchase_order_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.COST_PER_UNIT constant
     */
    public static String getPurchaseOrderDetail_CostPerUnit()
    {
        return "purchase_order_detail.COST_PER_UNIT";
    }
  
    /**
     * purchase_order_detail.RECEIVED_QTY
     * @return the column name for the RECEIVED_QTY field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.RECEIVED_QTY constant
     */
    public static String getPurchaseOrderDetail_ReceivedQty()
    {
        return "purchase_order_detail.RECEIVED_QTY";
    }
  
    /**
     * purchase_order_detail.CLOSED
     * @return the column name for the CLOSED field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.CLOSED constant
     */
    public static String getPurchaseOrderDetail_Closed()
    {
        return "purchase_order_detail.CLOSED";
    }
  
    /**
     * purchase_order_detail.CHANGED_MANUAL
     * @return the column name for the CHANGED_MANUAL field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.CHANGED_MANUAL constant
     */
    public static String getPurchaseOrderDetail_ChangedManual()
    {
        return "purchase_order_detail.CHANGED_MANUAL";
    }
  
    /**
     * purchase_order_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.PROJECT_ID constant
     */
    public static String getPurchaseOrderDetail_ProjectId()
    {
        return "purchase_order_detail.PROJECT_ID";
    }
  
    /**
     * purchase_order_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use PurchaseOrderDetailPeer.purchase_order_detail.DEPARTMENT_ID constant
     */
    public static String getPurchaseOrderDetail_DepartmentId()
    {
        return "purchase_order_detail.DEPARTMENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_order_detail");
        TableMap tMap = dbMap.getTable("purchase_order_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_order_detail.PURCHASE_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "purchase_order_detail.PURCHASE_ORDER_ID", "" , "purchase_order" ,
                "purchase_order_id");
                            tMap.addColumn("purchase_order_detail.REQUEST_TYPE", Integer.valueOf(0));
                          tMap.addColumn("purchase_order_detail.REQUEST_DETAIL_ID", "");
                          tMap.addColumn("purchase_order_detail.REQUEST_ID", "");
                            tMap.addColumn("purchase_order_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("purchase_order_detail.ITEM_ID", "");
                          tMap.addColumn("purchase_order_detail.ITEM_CODE", "");
                          tMap.addColumn("purchase_order_detail.ITEM_NAME", "");
                          tMap.addColumn("purchase_order_detail.DESCRIPTION", "");
                          tMap.addColumn("purchase_order_detail.UNIT_ID", "");
                          tMap.addColumn("purchase_order_detail.UNIT_CODE", "");
                          tMap.addColumn("purchase_order_detail.TAX_ID", "");
                            tMap.addColumn("purchase_order_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("purchase_order_detail.DISCOUNT", "");
                            tMap.addColumn("purchase_order_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.QTY", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.QTY_AUTO", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.ITEM_PRICE", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.SUB_TOTAL", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.LAST_PURCHASE", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.COST_PER_UNIT", bd_ZERO);
                            tMap.addColumn("purchase_order_detail.RECEIVED_QTY", bd_ZERO);
                          tMap.addColumn("purchase_order_detail.CLOSED", Boolean.TRUE);
                          tMap.addColumn("purchase_order_detail.CHANGED_MANUAL", Boolean.TRUE);
                          tMap.addColumn("purchase_order_detail.PROJECT_ID", "");
                          tMap.addColumn("purchase_order_detail.DEPARTMENT_ID", "");
          }
}
