package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseRequestDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseRequestDetailMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseRequestDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_request_detail";
    }

  
    /**
     * purchase_request_detail.PURCHASE_REQUEST_DETAIL_ID
     * @return the column name for the PURCHASE_REQUEST_DETAIL_ID field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.PURCHASE_REQUEST_DETAIL_ID constant
     */
    public static String getPurchaseRequestDetail_PurchaseRequestDetailId()
    {
        return "purchase_request_detail.PURCHASE_REQUEST_DETAIL_ID";
    }
  
    /**
     * purchase_request_detail.PURCHASE_REQUEST_ID
     * @return the column name for the PURCHASE_REQUEST_ID field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.PURCHASE_REQUEST_ID constant
     */
    public static String getPurchaseRequestDetail_PurchaseRequestId()
    {
        return "purchase_request_detail.PURCHASE_REQUEST_ID";
    }
  
    /**
     * purchase_request_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.INDEX_NO constant
     */
    public static String getPurchaseRequestDetail_IndexNo()
    {
        return "purchase_request_detail.INDEX_NO";
    }
  
    /**
     * purchase_request_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.ITEM_ID constant
     */
    public static String getPurchaseRequestDetail_ItemId()
    {
        return "purchase_request_detail.ITEM_ID";
    }
  
    /**
     * purchase_request_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.ITEM_CODE constant
     */
    public static String getPurchaseRequestDetail_ItemCode()
    {
        return "purchase_request_detail.ITEM_CODE";
    }
  
    /**
     * purchase_request_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.ITEM_NAME constant
     */
    public static String getPurchaseRequestDetail_ItemName()
    {
        return "purchase_request_detail.ITEM_NAME";
    }
  
    /**
     * purchase_request_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.DESCRIPTION constant
     */
    public static String getPurchaseRequestDetail_Description()
    {
        return "purchase_request_detail.DESCRIPTION";
    }
  
    /**
     * purchase_request_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.UNIT_ID constant
     */
    public static String getPurchaseRequestDetail_UnitId()
    {
        return "purchase_request_detail.UNIT_ID";
    }
  
    /**
     * purchase_request_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.UNIT_CODE constant
     */
    public static String getPurchaseRequestDetail_UnitCode()
    {
        return "purchase_request_detail.UNIT_CODE";
    }
  
    /**
     * purchase_request_detail.MINIMUM_STOCK
     * @return the column name for the MINIMUM_STOCK field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.MINIMUM_STOCK constant
     */
    public static String getPurchaseRequestDetail_MinimumStock()
    {
        return "purchase_request_detail.MINIMUM_STOCK";
    }
  
    /**
     * purchase_request_detail.MAXIMUM_STOCK
     * @return the column name for the MAXIMUM_STOCK field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.MAXIMUM_STOCK constant
     */
    public static String getPurchaseRequestDetail_MaximumStock()
    {
        return "purchase_request_detail.MAXIMUM_STOCK";
    }
  
    /**
     * purchase_request_detail.REORDER_POINT
     * @return the column name for the REORDER_POINT field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.REORDER_POINT constant
     */
    public static String getPurchaseRequestDetail_ReorderPoint()
    {
        return "purchase_request_detail.REORDER_POINT";
    }
  
    /**
     * purchase_request_detail.CURRENT_QTY
     * @return the column name for the CURRENT_QTY field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.CURRENT_QTY constant
     */
    public static String getPurchaseRequestDetail_CurrentQty()
    {
        return "purchase_request_detail.CURRENT_QTY";
    }
  
    /**
     * purchase_request_detail.RECOMMENDED_QTY
     * @return the column name for the RECOMMENDED_QTY field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.RECOMMENDED_QTY constant
     */
    public static String getPurchaseRequestDetail_RecommendedQty()
    {
        return "purchase_request_detail.RECOMMENDED_QTY";
    }
  
    /**
     * purchase_request_detail.REQUEST_QTY
     * @return the column name for the REQUEST_QTY field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.REQUEST_QTY constant
     */
    public static String getPurchaseRequestDetail_RequestQty()
    {
        return "purchase_request_detail.REQUEST_QTY";
    }
  
    /**
     * purchase_request_detail.SENT_QTY
     * @return the column name for the SENT_QTY field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.SENT_QTY constant
     */
    public static String getPurchaseRequestDetail_SentQty()
    {
        return "purchase_request_detail.SENT_QTY";
    }
  
    /**
     * purchase_request_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use PurchaseRequestDetailPeer.purchase_request_detail.COST_PER_UNIT constant
     */
    public static String getPurchaseRequestDetail_CostPerUnit()
    {
        return "purchase_request_detail.COST_PER_UNIT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_request_detail");
        TableMap tMap = dbMap.getTable("purchase_request_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_request_detail.PURCHASE_REQUEST_DETAIL_ID", "");
                          tMap.addForeignKey(
                "purchase_request_detail.PURCHASE_REQUEST_ID", "" , "purchase_request" ,
                "purchase_request_id");
                            tMap.addColumn("purchase_request_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("purchase_request_detail.ITEM_ID", "");
                          tMap.addColumn("purchase_request_detail.ITEM_CODE", "");
                          tMap.addColumn("purchase_request_detail.ITEM_NAME", "");
                          tMap.addColumn("purchase_request_detail.DESCRIPTION", "");
                          tMap.addColumn("purchase_request_detail.UNIT_ID", "");
                          tMap.addColumn("purchase_request_detail.UNIT_CODE", "");
                            tMap.addColumn("purchase_request_detail.MINIMUM_STOCK", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.MAXIMUM_STOCK", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.REORDER_POINT", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.CURRENT_QTY", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.RECOMMENDED_QTY", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.REQUEST_QTY", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.SENT_QTY", bd_ZERO);
                            tMap.addColumn("purchase_request_detail.COST_PER_UNIT", bd_ZERO);
          }
}
