package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseRequestMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseRequestMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseRequestPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_request";
    }

  
    /**
     * purchase_request.PURCHASE_REQUEST_ID
     * @return the column name for the PURCHASE_REQUEST_ID field
     * @deprecated use PurchaseRequestPeer.purchase_request.PURCHASE_REQUEST_ID constant
     */
    public static String getPurchaseRequest_PurchaseRequestId()
    {
        return "purchase_request.PURCHASE_REQUEST_ID";
    }
  
    /**
     * purchase_request.TRANSACTION_STATUS
     * @return the column name for the TRANSACTION_STATUS field
     * @deprecated use PurchaseRequestPeer.purchase_request.TRANSACTION_STATUS constant
     */
    public static String getPurchaseRequest_TransactionStatus()
    {
        return "purchase_request.TRANSACTION_STATUS";
    }
  
    /**
     * purchase_request.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use PurchaseRequestPeer.purchase_request.TRANSACTION_NO constant
     */
    public static String getPurchaseRequest_TransactionNo()
    {
        return "purchase_request.TRANSACTION_NO";
    }
  
    /**
     * purchase_request.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use PurchaseRequestPeer.purchase_request.TRANSACTION_DATE constant
     */
    public static String getPurchaseRequest_TransactionDate()
    {
        return "purchase_request.TRANSACTION_DATE";
    }
  
    /**
     * purchase_request.LAST_UPDATE_DATE
     * @return the column name for the LAST_UPDATE_DATE field
     * @deprecated use PurchaseRequestPeer.purchase_request.LAST_UPDATE_DATE constant
     */
    public static String getPurchaseRequest_LastUpdateDate()
    {
        return "purchase_request.LAST_UPDATE_DATE";
    }
  
    /**
     * purchase_request.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use PurchaseRequestPeer.purchase_request.TOTAL_QTY constant
     */
    public static String getPurchaseRequest_TotalQty()
    {
        return "purchase_request.TOTAL_QTY";
    }
  
    /**
     * purchase_request.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use PurchaseRequestPeer.purchase_request.VENDOR_ID constant
     */
    public static String getPurchaseRequest_VendorId()
    {
        return "purchase_request.VENDOR_ID";
    }
  
    /**
     * purchase_request.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use PurchaseRequestPeer.purchase_request.VENDOR_NAME constant
     */
    public static String getPurchaseRequest_VendorName()
    {
        return "purchase_request.VENDOR_NAME";
    }
  
    /**
     * purchase_request.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PurchaseRequestPeer.purchase_request.LOCATION_ID constant
     */
    public static String getPurchaseRequest_LocationId()
    {
        return "purchase_request.LOCATION_ID";
    }
  
    /**
     * purchase_request.REQUESTED_BY
     * @return the column name for the REQUESTED_BY field
     * @deprecated use PurchaseRequestPeer.purchase_request.REQUESTED_BY constant
     */
    public static String getPurchaseRequest_RequestedBy()
    {
        return "purchase_request.REQUESTED_BY";
    }
  
    /**
     * purchase_request.APPROVED_BY
     * @return the column name for the APPROVED_BY field
     * @deprecated use PurchaseRequestPeer.purchase_request.APPROVED_BY constant
     */
    public static String getPurchaseRequest_ApprovedBy()
    {
        return "purchase_request.APPROVED_BY";
    }
  
    /**
     * purchase_request.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PurchaseRequestPeer.purchase_request.REMARK constant
     */
    public static String getPurchaseRequest_Remark()
    {
        return "purchase_request.REMARK";
    }
  
    /**
     * purchase_request.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PurchaseRequestPeer.purchase_request.CANCEL_BY constant
     */
    public static String getPurchaseRequest_CancelBy()
    {
        return "purchase_request.CANCEL_BY";
    }
  
    /**
     * purchase_request.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PurchaseRequestPeer.purchase_request.CANCEL_DATE constant
     */
    public static String getPurchaseRequest_CancelDate()
    {
        return "purchase_request.CANCEL_DATE";
    }
  
    /**
     * purchase_request.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use PurchaseRequestPeer.purchase_request.CONFIRM_DATE constant
     */
    public static String getPurchaseRequest_ConfirmDate()
    {
        return "purchase_request.CONFIRM_DATE";
    }
  
    /**
     * purchase_request.TRANSFER_TRANS_ID
     * @return the column name for the TRANSFER_TRANS_ID field
     * @deprecated use PurchaseRequestPeer.purchase_request.TRANSFER_TRANS_ID constant
     */
    public static String getPurchaseRequest_TransferTransId()
    {
        return "purchase_request.TRANSFER_TRANS_ID";
    }
  
    /**
     * purchase_request.TRANSFER_BY
     * @return the column name for the TRANSFER_BY field
     * @deprecated use PurchaseRequestPeer.purchase_request.TRANSFER_BY constant
     */
    public static String getPurchaseRequest_TransferBy()
    {
        return "purchase_request.TRANSFER_BY";
    }
  
    /**
     * purchase_request.TRANSFER_DATE
     * @return the column name for the TRANSFER_DATE field
     * @deprecated use PurchaseRequestPeer.purchase_request.TRANSFER_DATE constant
     */
    public static String getPurchaseRequest_TransferDate()
    {
        return "purchase_request.TRANSFER_DATE";
    }
  
    /**
     * purchase_request.IS_PURCHASE
     * @return the column name for the IS_PURCHASE field
     * @deprecated use PurchaseRequestPeer.purchase_request.IS_PURCHASE constant
     */
    public static String getPurchaseRequest_IsPurchase()
    {
        return "purchase_request.IS_PURCHASE";
    }
  
    /**
     * purchase_request.IS_REIMBURSE
     * @return the column name for the IS_REIMBURSE field
     * @deprecated use PurchaseRequestPeer.purchase_request.IS_REIMBURSE constant
     */
    public static String getPurchaseRequest_IsReimburse()
    {
        return "purchase_request.IS_REIMBURSE";
    }
  
    /**
     * purchase_request.IS_EXTERNAL
     * @return the column name for the IS_EXTERNAL field
     * @deprecated use PurchaseRequestPeer.purchase_request.IS_EXTERNAL constant
     */
    public static String getPurchaseRequest_IsExternal()
    {
        return "purchase_request.IS_EXTERNAL";
    }
  
    /**
     * purchase_request.CROSS_ENTITY_ID
     * @return the column name for the CROSS_ENTITY_ID field
     * @deprecated use PurchaseRequestPeer.purchase_request.CROSS_ENTITY_ID constant
     */
    public static String getPurchaseRequest_CrossEntityId()
    {
        return "purchase_request.CROSS_ENTITY_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_request");
        TableMap tMap = dbMap.getTable("purchase_request");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_request.PURCHASE_REQUEST_ID", "");
                            tMap.addColumn("purchase_request.TRANSACTION_STATUS", Integer.valueOf(0));
                          tMap.addColumn("purchase_request.TRANSACTION_NO", "");
                          tMap.addColumn("purchase_request.TRANSACTION_DATE", new Date());
                          tMap.addColumn("purchase_request.LAST_UPDATE_DATE", new Date());
                            tMap.addColumn("purchase_request.TOTAL_QTY", bd_ZERO);
                          tMap.addColumn("purchase_request.VENDOR_ID", "");
                          tMap.addColumn("purchase_request.VENDOR_NAME", "");
                          tMap.addColumn("purchase_request.LOCATION_ID", "");
                          tMap.addColumn("purchase_request.REQUESTED_BY", "");
                          tMap.addColumn("purchase_request.APPROVED_BY", "");
                          tMap.addColumn("purchase_request.REMARK", "");
                          tMap.addColumn("purchase_request.CANCEL_BY", "");
                          tMap.addColumn("purchase_request.CANCEL_DATE", new Date());
                          tMap.addColumn("purchase_request.CONFIRM_DATE", new Date());
                          tMap.addColumn("purchase_request.TRANSFER_TRANS_ID", "");
                          tMap.addColumn("purchase_request.TRANSFER_BY", "");
                          tMap.addColumn("purchase_request.TRANSFER_DATE", new Date());
                          tMap.addColumn("purchase_request.IS_PURCHASE", Boolean.TRUE);
                          tMap.addColumn("purchase_request.IS_REIMBURSE", Boolean.TRUE);
                          tMap.addColumn("purchase_request.IS_EXTERNAL", Boolean.TRUE);
                          tMap.addColumn("purchase_request.CROSS_ENTITY_ID", "");
          }
}
