package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseReturnMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseReturnMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseReturnPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_return";
    }

  
    /**
     * purchase_return.PURCHASE_RETURN_ID
     * @return the column name for the PURCHASE_RETURN_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.PURCHASE_RETURN_ID constant
     */
    public static String getPurchaseReturn_PurchaseReturnId()
    {
        return "purchase_return.PURCHASE_RETURN_ID";
    }
  
    /**
     * purchase_return.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use PurchaseReturnPeer.purchase_return.TRANSACTION_TYPE constant
     */
    public static String getPurchaseReturn_TransactionType()
    {
        return "purchase_return.TRANSACTION_TYPE";
    }
  
    /**
     * purchase_return.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.TRANSACTION_ID constant
     */
    public static String getPurchaseReturn_TransactionId()
    {
        return "purchase_return.TRANSACTION_ID";
    }
  
    /**
     * purchase_return.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.LOCATION_ID constant
     */
    public static String getPurchaseReturn_LocationId()
    {
        return "purchase_return.LOCATION_ID";
    }
  
    /**
     * purchase_return.STATUS
     * @return the column name for the STATUS field
     * @deprecated use PurchaseReturnPeer.purchase_return.STATUS constant
     */
    public static String getPurchaseReturn_Status()
    {
        return "purchase_return.STATUS";
    }
  
    /**
     * purchase_return.VENDOR_TRANSACTION_NO
     * @return the column name for the VENDOR_TRANSACTION_NO field
     * @deprecated use PurchaseReturnPeer.purchase_return.VENDOR_TRANSACTION_NO constant
     */
    public static String getPurchaseReturn_VendorTransactionNo()
    {
        return "purchase_return.VENDOR_TRANSACTION_NO";
    }
  
    /**
     * purchase_return.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use PurchaseReturnPeer.purchase_return.TRANSACTION_NO constant
     */
    public static String getPurchaseReturn_TransactionNo()
    {
        return "purchase_return.TRANSACTION_NO";
    }
  
    /**
     * purchase_return.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use PurchaseReturnPeer.purchase_return.TRANSACTION_DATE constant
     */
    public static String getPurchaseReturn_TransactionDate()
    {
        return "purchase_return.TRANSACTION_DATE";
    }
  
    /**
     * purchase_return.RETURN_NO
     * @return the column name for the RETURN_NO field
     * @deprecated use PurchaseReturnPeer.purchase_return.RETURN_NO constant
     */
    public static String getPurchaseReturn_ReturnNo()
    {
        return "purchase_return.RETURN_NO";
    }
  
    /**
     * purchase_return.RETURN_DATE
     * @return the column name for the RETURN_DATE field
     * @deprecated use PurchaseReturnPeer.purchase_return.RETURN_DATE constant
     */
    public static String getPurchaseReturn_ReturnDate()
    {
        return "purchase_return.RETURN_DATE";
    }
  
    /**
     * purchase_return.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.VENDOR_ID constant
     */
    public static String getPurchaseReturn_VendorId()
    {
        return "purchase_return.VENDOR_ID";
    }
  
    /**
     * purchase_return.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use PurchaseReturnPeer.purchase_return.VENDOR_NAME constant
     */
    public static String getPurchaseReturn_VendorName()
    {
        return "purchase_return.VENDOR_NAME";
    }
  
    /**
     * purchase_return.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use PurchaseReturnPeer.purchase_return.TOTAL_QTY constant
     */
    public static String getPurchaseReturn_TotalQty()
    {
        return "purchase_return.TOTAL_QTY";
    }
  
    /**
     * purchase_return.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use PurchaseReturnPeer.purchase_return.TOTAL_TAX constant
     */
    public static String getPurchaseReturn_TotalTax()
    {
        return "purchase_return.TOTAL_TAX";
    }
  
    /**
     * purchase_return.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use PurchaseReturnPeer.purchase_return.TOTAL_AMOUNT constant
     */
    public static String getPurchaseReturn_TotalAmount()
    {
        return "purchase_return.TOTAL_AMOUNT";
    }
  
    /**
     * purchase_return.PURCHASE_DISCOUNT
     * @return the column name for the PURCHASE_DISCOUNT field
     * @deprecated use PurchaseReturnPeer.purchase_return.PURCHASE_DISCOUNT constant
     */
    public static String getPurchaseReturn_PurchaseDiscount()
    {
        return "purchase_return.PURCHASE_DISCOUNT";
    }
  
    /**
     * purchase_return.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use PurchaseReturnPeer.purchase_return.TOTAL_DISCOUNT constant
     */
    public static String getPurchaseReturn_TotalDiscount()
    {
        return "purchase_return.TOTAL_DISCOUNT";
    }
  
    /**
     * purchase_return.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use PurchaseReturnPeer.purchase_return.USER_NAME constant
     */
    public static String getPurchaseReturn_UserName()
    {
        return "purchase_return.USER_NAME";
    }
  
    /**
     * purchase_return.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PurchaseReturnPeer.purchase_return.REMARK constant
     */
    public static String getPurchaseReturn_Remark()
    {
        return "purchase_return.REMARK";
    }
  
    /**
     * purchase_return.CREATE_DEBIT_MEMO
     * @return the column name for the CREATE_DEBIT_MEMO field
     * @deprecated use PurchaseReturnPeer.purchase_return.CREATE_DEBIT_MEMO constant
     */
    public static String getPurchaseReturn_CreateDebitMemo()
    {
        return "purchase_return.CREATE_DEBIT_MEMO";
    }
  
    /**
     * purchase_return.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.CURRENCY_ID constant
     */
    public static String getPurchaseReturn_CurrencyId()
    {
        return "purchase_return.CURRENCY_ID";
    }
  
    /**
     * purchase_return.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use PurchaseReturnPeer.purchase_return.CURRENCY_RATE constant
     */
    public static String getPurchaseReturn_CurrencyRate()
    {
        return "purchase_return.CURRENCY_RATE";
    }
  
    /**
     * purchase_return.CASH_RETURN
     * @return the column name for the CASH_RETURN field
     * @deprecated use PurchaseReturnPeer.purchase_return.CASH_RETURN constant
     */
    public static String getPurchaseReturn_CashReturn()
    {
        return "purchase_return.CASH_RETURN";
    }
  
    /**
     * purchase_return.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.BANK_ID constant
     */
    public static String getPurchaseReturn_BankId()
    {
        return "purchase_return.BANK_ID";
    }
  
    /**
     * purchase_return.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use PurchaseReturnPeer.purchase_return.BANK_ISSUER constant
     */
    public static String getPurchaseReturn_BankIssuer()
    {
        return "purchase_return.BANK_ISSUER";
    }
  
    /**
     * purchase_return.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use PurchaseReturnPeer.purchase_return.DUE_DATE constant
     */
    public static String getPurchaseReturn_DueDate()
    {
        return "purchase_return.DUE_DATE";
    }
  
    /**
     * purchase_return.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use PurchaseReturnPeer.purchase_return.REFERENCE_NO constant
     */
    public static String getPurchaseReturn_ReferenceNo()
    {
        return "purchase_return.REFERENCE_NO";
    }
  
    /**
     * purchase_return.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.CASH_FLOW_TYPE_ID constant
     */
    public static String getPurchaseReturn_CashFlowTypeId()
    {
        return "purchase_return.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * purchase_return.RETURN_REASON_ID
     * @return the column name for the RETURN_REASON_ID field
     * @deprecated use PurchaseReturnPeer.purchase_return.RETURN_REASON_ID constant
     */
    public static String getPurchaseReturn_ReturnReasonId()
    {
        return "purchase_return.RETURN_REASON_ID";
    }
  
    /**
     * purchase_return.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use PurchaseReturnPeer.purchase_return.FISCAL_RATE constant
     */
    public static String getPurchaseReturn_FiscalRate()
    {
        return "purchase_return.FISCAL_RATE";
    }
  
    /**
     * purchase_return.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use PurchaseReturnPeer.purchase_return.IS_TAXABLE constant
     */
    public static String getPurchaseReturn_IsTaxable()
    {
        return "purchase_return.IS_TAXABLE";
    }
  
    /**
     * purchase_return.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use PurchaseReturnPeer.purchase_return.IS_INCLUSIVE_TAX constant
     */
    public static String getPurchaseReturn_IsInclusiveTax()
    {
        return "purchase_return.IS_INCLUSIVE_TAX";
    }
  
    /**
     * purchase_return.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PurchaseReturnPeer.purchase_return.CANCEL_BY constant
     */
    public static String getPurchaseReturn_CancelBy()
    {
        return "purchase_return.CANCEL_BY";
    }
  
    /**
     * purchase_return.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PurchaseReturnPeer.purchase_return.CANCEL_DATE constant
     */
    public static String getPurchaseReturn_CancelDate()
    {
        return "purchase_return.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_return");
        TableMap tMap = dbMap.getTable("purchase_return");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_return.PURCHASE_RETURN_ID", "");
                            tMap.addColumn("purchase_return.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("purchase_return.TRANSACTION_ID", "");
                          tMap.addColumn("purchase_return.LOCATION_ID", "");
                            tMap.addColumn("purchase_return.STATUS", Integer.valueOf(0));
                          tMap.addColumn("purchase_return.VENDOR_TRANSACTION_NO", "");
                          tMap.addColumn("purchase_return.TRANSACTION_NO", "");
                          tMap.addColumn("purchase_return.TRANSACTION_DATE", new Date());
                          tMap.addColumn("purchase_return.RETURN_NO", "");
                          tMap.addColumn("purchase_return.RETURN_DATE", new Date());
                          tMap.addColumn("purchase_return.VENDOR_ID", "");
                          tMap.addColumn("purchase_return.VENDOR_NAME", "");
                            tMap.addColumn("purchase_return.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("purchase_return.TOTAL_TAX", bd_ZERO);
                            tMap.addColumn("purchase_return.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("purchase_return.PURCHASE_DISCOUNT", "");
                            tMap.addColumn("purchase_return.TOTAL_DISCOUNT", bd_ZERO);
                          tMap.addColumn("purchase_return.USER_NAME", "");
                          tMap.addColumn("purchase_return.REMARK", "");
                          tMap.addColumn("purchase_return.CREATE_DEBIT_MEMO", Boolean.TRUE);
                          tMap.addColumn("purchase_return.CURRENCY_ID", "");
                            tMap.addColumn("purchase_return.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("purchase_return.CASH_RETURN", Boolean.TRUE);
                          tMap.addColumn("purchase_return.BANK_ID", "");
                          tMap.addColumn("purchase_return.BANK_ISSUER", "");
                          tMap.addColumn("purchase_return.DUE_DATE", new Date());
                          tMap.addColumn("purchase_return.REFERENCE_NO", "");
                          tMap.addColumn("purchase_return.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("purchase_return.RETURN_REASON_ID", "");
                            tMap.addColumn("purchase_return.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("purchase_return.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("purchase_return.IS_INCLUSIVE_TAX", Boolean.TRUE);
                          tMap.addColumn("purchase_return.CANCEL_BY", "");
                          tMap.addColumn("purchase_return.CANCEL_DATE", new Date());
          }
}
