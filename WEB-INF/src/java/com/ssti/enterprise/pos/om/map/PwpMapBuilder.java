package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PwpMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PwpMapBuilder";

    /**
     * Item
     * @deprecated use PwpPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "pwp";
    }

  
    /**
     * pwp.PWP_ID
     * @return the column name for the PWP_ID field
     * @deprecated use PwpPeer.pwp.PWP_ID constant
     */
    public static String getPwp_PwpId()
    {
        return "pwp.PWP_ID";
    }
  
    /**
     * pwp.PWP_CODE
     * @return the column name for the PWP_CODE field
     * @deprecated use PwpPeer.pwp.PWP_CODE constant
     */
    public static String getPwp_PwpCode()
    {
        return "pwp.PWP_CODE";
    }
  
    /**
     * pwp.PWP_TYPE
     * @return the column name for the PWP_TYPE field
     * @deprecated use PwpPeer.pwp.PWP_TYPE constant
     */
    public static String getPwp_PwpType()
    {
        return "pwp.PWP_TYPE";
    }
  
    /**
     * pwp.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PwpPeer.pwp.DESCRIPTION constant
     */
    public static String getPwp_Description()
    {
        return "pwp.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("pwp");
        TableMap tMap = dbMap.getTable("pwp");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("pwp.PWP_ID", "");
                          tMap.addColumn("pwp.PWP_CODE", "");
                            tMap.addColumn("pwp.PWP_TYPE", Integer.valueOf(0));
                          tMap.addColumn("pwp.DESCRIPTION", "");
          }
}
