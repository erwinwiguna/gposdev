package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class RemoteStoreDataMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.RemoteStoreDataMapBuilder";

    /**
     * Item
     * @deprecated use RemoteStoreDataPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "remote_store_data";
    }

  
    /**
     * remote_store_data.REMOTE_STORE_DATA_ID
     * @return the column name for the REMOTE_STORE_DATA_ID field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.REMOTE_STORE_DATA_ID constant
     */
    public static String getRemoteStoreData_RemoteStoreDataId()
    {
        return "remote_store_data.REMOTE_STORE_DATA_ID";
    }
  
    /**
     * remote_store_data.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.LOCATION_ID constant
     */
    public static String getRemoteStoreData_LocationId()
    {
        return "remote_store_data.LOCATION_ID";
    }
  
    /**
     * remote_store_data.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.LOCATION_NAME constant
     */
    public static String getRemoteStoreData_LocationName()
    {
        return "remote_store_data.LOCATION_NAME";
    }
  
    /**
     * remote_store_data.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.USER_NAME constant
     */
    public static String getRemoteStoreData_UserName()
    {
        return "remote_store_data.USER_NAME";
    }
  
    /**
     * remote_store_data.FILE_PATH
     * @return the column name for the FILE_PATH field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.FILE_PATH constant
     */
    public static String getRemoteStoreData_FilePath()
    {
        return "remote_store_data.FILE_PATH";
    }
  
    /**
     * remote_store_data.SETUP_DATE
     * @return the column name for the SETUP_DATE field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.SETUP_DATE constant
     */
    public static String getRemoteStoreData_SetupDate()
    {
        return "remote_store_data.SETUP_DATE";
    }
  
    /**
     * remote_store_data.LOG_FILE
     * @return the column name for the LOG_FILE field
     * @deprecated use RemoteStoreDataPeer.remote_store_data.LOG_FILE constant
     */
    public static String getRemoteStoreData_LogFile()
    {
        return "remote_store_data.LOG_FILE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("remote_store_data");
        TableMap tMap = dbMap.getTable("remote_store_data");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("remote_store_data.REMOTE_STORE_DATA_ID", "");
                          tMap.addColumn("remote_store_data.LOCATION_ID", "");
                          tMap.addColumn("remote_store_data.LOCATION_NAME", "");
                          tMap.addColumn("remote_store_data.USER_NAME", "");
                          tMap.addColumn("remote_store_data.FILE_PATH", "");
                          tMap.addColumn("remote_store_data.SETUP_DATE", new Date());
                          tMap.addColumn("remote_store_data.LOG_FILE", "");
          }
}
