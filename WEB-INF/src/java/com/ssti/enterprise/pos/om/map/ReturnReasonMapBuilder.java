package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ReturnReasonMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ReturnReasonMapBuilder";

    /**
     * Item
     * @deprecated use ReturnReasonPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "return_reason";
    }

  
    /**
     * return_reason.RETURN_REASON_ID
     * @return the column name for the RETURN_REASON_ID field
     * @deprecated use ReturnReasonPeer.return_reason.RETURN_REASON_ID constant
     */
    public static String getReturnReason_ReturnReasonId()
    {
        return "return_reason.RETURN_REASON_ID";
    }
  
    /**
     * return_reason.CODE
     * @return the column name for the CODE field
     * @deprecated use ReturnReasonPeer.return_reason.CODE constant
     */
    public static String getReturnReason_Code()
    {
        return "return_reason.CODE";
    }
  
    /**
     * return_reason.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use ReturnReasonPeer.return_reason.DESCRIPTION constant
     */
    public static String getReturnReason_Description()
    {
        return "return_reason.DESCRIPTION";
    }
  
    /**
     * return_reason.TRANS_TYPE
     * @return the column name for the TRANS_TYPE field
     * @deprecated use ReturnReasonPeer.return_reason.TRANS_TYPE constant
     */
    public static String getReturnReason_TransType()
    {
        return "return_reason.TRANS_TYPE";
    }
  
    /**
     * return_reason.LAST_UPDATE_BY
     * @return the column name for the LAST_UPDATE_BY field
     * @deprecated use ReturnReasonPeer.return_reason.LAST_UPDATE_BY constant
     */
    public static String getReturnReason_LastUpdateBy()
    {
        return "return_reason.LAST_UPDATE_BY";
    }
  
    /**
     * return_reason.LAST_UPDATE
     * @return the column name for the LAST_UPDATE field
     * @deprecated use ReturnReasonPeer.return_reason.LAST_UPDATE constant
     */
    public static String getReturnReason_LastUpdate()
    {
        return "return_reason.LAST_UPDATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("return_reason");
        TableMap tMap = dbMap.getTable("return_reason");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("return_reason.RETURN_REASON_ID", "");
                          tMap.addColumn("return_reason.CODE", "");
                          tMap.addColumn("return_reason.DESCRIPTION", "");
                            tMap.addColumn("return_reason.TRANS_TYPE", Integer.valueOf(0));
                          tMap.addColumn("return_reason.LAST_UPDATE_BY", "");
                          tMap.addColumn("return_reason.LAST_UPDATE", new Date());
          }
}
