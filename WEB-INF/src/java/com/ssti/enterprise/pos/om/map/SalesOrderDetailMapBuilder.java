package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SalesOrderDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SalesOrderDetailMapBuilder";

    /**
     * Item
     * @deprecated use SalesOrderDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sales_order_detail";
    }

  
    /**
     * sales_order_detail.SALES_ORDER_DETAIL_ID
     * @return the column name for the SALES_ORDER_DETAIL_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.SALES_ORDER_DETAIL_ID constant
     */
    public static String getSalesOrderDetail_SalesOrderDetailId()
    {
        return "sales_order_detail.SALES_ORDER_DETAIL_ID";
    }
  
    /**
     * sales_order_detail.SALES_ORDER_ID
     * @return the column name for the SALES_ORDER_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.SALES_ORDER_ID constant
     */
    public static String getSalesOrderDetail_SalesOrderId()
    {
        return "sales_order_detail.SALES_ORDER_ID";
    }
  
    /**
     * sales_order_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.INDEX_NO constant
     */
    public static String getSalesOrderDetail_IndexNo()
    {
        return "sales_order_detail.INDEX_NO";
    }
  
    /**
     * sales_order_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.ITEM_ID constant
     */
    public static String getSalesOrderDetail_ItemId()
    {
        return "sales_order_detail.ITEM_ID";
    }
  
    /**
     * sales_order_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.ITEM_CODE constant
     */
    public static String getSalesOrderDetail_ItemCode()
    {
        return "sales_order_detail.ITEM_CODE";
    }
  
    /**
     * sales_order_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.ITEM_NAME constant
     */
    public static String getSalesOrderDetail_ItemName()
    {
        return "sales_order_detail.ITEM_NAME";
    }
  
    /**
     * sales_order_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.DESCRIPTION constant
     */
    public static String getSalesOrderDetail_Description()
    {
        return "sales_order_detail.DESCRIPTION";
    }
  
    /**
     * sales_order_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.UNIT_ID constant
     */
    public static String getSalesOrderDetail_UnitId()
    {
        return "sales_order_detail.UNIT_ID";
    }
  
    /**
     * sales_order_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.UNIT_CODE constant
     */
    public static String getSalesOrderDetail_UnitCode()
    {
        return "sales_order_detail.UNIT_CODE";
    }
  
    /**
     * sales_order_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.TAX_ID constant
     */
    public static String getSalesOrderDetail_TaxId()
    {
        return "sales_order_detail.TAX_ID";
    }
  
    /**
     * sales_order_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.TAX_AMOUNT constant
     */
    public static String getSalesOrderDetail_TaxAmount()
    {
        return "sales_order_detail.TAX_AMOUNT";
    }
  
    /**
     * sales_order_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.SUB_TOTAL_TAX constant
     */
    public static String getSalesOrderDetail_SubTotalTax()
    {
        return "sales_order_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * sales_order_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.DISCOUNT constant
     */
    public static String getSalesOrderDetail_Discount()
    {
        return "sales_order_detail.DISCOUNT";
    }
  
    /**
     * sales_order_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.SUB_TOTAL_DISC constant
     */
    public static String getSalesOrderDetail_SubTotalDisc()
    {
        return "sales_order_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * sales_order_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.QTY constant
     */
    public static String getSalesOrderDetail_Qty()
    {
        return "sales_order_detail.QTY";
    }
  
    /**
     * sales_order_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.QTY_BASE constant
     */
    public static String getSalesOrderDetail_QtyBase()
    {
        return "sales_order_detail.QTY_BASE";
    }
  
    /**
     * sales_order_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.ITEM_PRICE constant
     */
    public static String getSalesOrderDetail_ItemPrice()
    {
        return "sales_order_detail.ITEM_PRICE";
    }
  
    /**
     * sales_order_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.SUB_TOTAL constant
     */
    public static String getSalesOrderDetail_SubTotal()
    {
        return "sales_order_detail.SUB_TOTAL";
    }
  
    /**
     * sales_order_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.COST_PER_UNIT constant
     */
    public static String getSalesOrderDetail_CostPerUnit()
    {
        return "sales_order_detail.COST_PER_UNIT";
    }
  
    /**
     * sales_order_detail.DELIVERED_QTY
     * @return the column name for the DELIVERED_QTY field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.DELIVERED_QTY constant
     */
    public static String getSalesOrderDetail_DeliveredQty()
    {
        return "sales_order_detail.DELIVERED_QTY";
    }
  
    /**
     * sales_order_detail.CLOSED
     * @return the column name for the CLOSED field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.CLOSED constant
     */
    public static String getSalesOrderDetail_Closed()
    {
        return "sales_order_detail.CLOSED";
    }
  
    /**
     * sales_order_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.PROJECT_ID constant
     */
    public static String getSalesOrderDetail_ProjectId()
    {
        return "sales_order_detail.PROJECT_ID";
    }
  
    /**
     * sales_order_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.DEPARTMENT_ID constant
     */
    public static String getSalesOrderDetail_DepartmentId()
    {
        return "sales_order_detail.DEPARTMENT_ID";
    }
  
    /**
     * sales_order_detail.EMPLOYEE_ID
     * @return the column name for the EMPLOYEE_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.EMPLOYEE_ID constant
     */
    public static String getSalesOrderDetail_EmployeeId()
    {
        return "sales_order_detail.EMPLOYEE_ID";
    }
  
    /**
     * sales_order_detail.QUOTATION_ID
     * @return the column name for the QUOTATION_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.QUOTATION_ID constant
     */
    public static String getSalesOrderDetail_QuotationId()
    {
        return "sales_order_detail.QUOTATION_ID";
    }
  
    /**
     * sales_order_detail.QUOTATION_DETAIL_ID
     * @return the column name for the QUOTATION_DETAIL_ID field
     * @deprecated use SalesOrderDetailPeer.sales_order_detail.QUOTATION_DETAIL_ID constant
     */
    public static String getSalesOrderDetail_QuotationDetailId()
    {
        return "sales_order_detail.QUOTATION_DETAIL_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sales_order_detail");
        TableMap tMap = dbMap.getTable("sales_order_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sales_order_detail.SALES_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "sales_order_detail.SALES_ORDER_ID", "" , "sales_order" ,
                "sales_order_id");
                            tMap.addColumn("sales_order_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("sales_order_detail.ITEM_ID", "");
                          tMap.addColumn("sales_order_detail.ITEM_CODE", "");
                          tMap.addColumn("sales_order_detail.ITEM_NAME", "");
                          tMap.addColumn("sales_order_detail.DESCRIPTION", "");
                          tMap.addColumn("sales_order_detail.UNIT_ID", "");
                          tMap.addColumn("sales_order_detail.UNIT_CODE", "");
                          tMap.addColumn("sales_order_detail.TAX_ID", "");
                            tMap.addColumn("sales_order_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_order_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("sales_order_detail.DISCOUNT", "");
                            tMap.addColumn("sales_order_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("sales_order_detail.QTY", bd_ZERO);
                            tMap.addColumn("sales_order_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("sales_order_detail.ITEM_PRICE", bd_ZERO);
                            tMap.addColumn("sales_order_detail.SUB_TOTAL", bd_ZERO);
                            tMap.addColumn("sales_order_detail.COST_PER_UNIT", bd_ZERO);
                            tMap.addColumn("sales_order_detail.DELIVERED_QTY", bd_ZERO);
                          tMap.addColumn("sales_order_detail.CLOSED", Boolean.TRUE);
                          tMap.addColumn("sales_order_detail.PROJECT_ID", "");
                          tMap.addColumn("sales_order_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("sales_order_detail.EMPLOYEE_ID", "");
                          tMap.addColumn("sales_order_detail.QUOTATION_ID", "");
                          tMap.addColumn("sales_order_detail.QUOTATION_DETAIL_ID", "");
          }
}
