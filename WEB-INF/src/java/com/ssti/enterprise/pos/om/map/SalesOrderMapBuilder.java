package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SalesOrderMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SalesOrderMapBuilder";

    /**
     * Item
     * @deprecated use SalesOrderPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sales_order";
    }

  
    /**
     * sales_order.SALES_ORDER_ID
     * @return the column name for the SALES_ORDER_ID field
     * @deprecated use SalesOrderPeer.sales_order.SALES_ORDER_ID constant
     */
    public static String getSalesOrder_SalesOrderId()
    {
        return "sales_order.SALES_ORDER_ID";
    }
  
    /**
     * sales_order.TRANSACTION_STATUS
     * @return the column name for the TRANSACTION_STATUS field
     * @deprecated use SalesOrderPeer.sales_order.TRANSACTION_STATUS constant
     */
    public static String getSalesOrder_TransactionStatus()
    {
        return "sales_order.TRANSACTION_STATUS";
    }
  
    /**
     * sales_order.SALES_ORDER_NO
     * @return the column name for the SALES_ORDER_NO field
     * @deprecated use SalesOrderPeer.sales_order.SALES_ORDER_NO constant
     */
    public static String getSalesOrder_SalesOrderNo()
    {
        return "sales_order.SALES_ORDER_NO";
    }
  
    /**
     * sales_order.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use SalesOrderPeer.sales_order.TRANSACTION_DATE constant
     */
    public static String getSalesOrder_TransactionDate()
    {
        return "sales_order.TRANSACTION_DATE";
    }
  
    /**
     * sales_order.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use SalesOrderPeer.sales_order.DUE_DATE constant
     */
    public static String getSalesOrder_DueDate()
    {
        return "sales_order.DUE_DATE";
    }
  
    /**
     * sales_order.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use SalesOrderPeer.sales_order.CUSTOMER_ID constant
     */
    public static String getSalesOrder_CustomerId()
    {
        return "sales_order.CUSTOMER_ID";
    }
  
    /**
     * sales_order.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use SalesOrderPeer.sales_order.CUSTOMER_NAME constant
     */
    public static String getSalesOrder_CustomerName()
    {
        return "sales_order.CUSTOMER_NAME";
    }
  
    /**
     * sales_order.CUSTOMER_PO_NO
     * @return the column name for the CUSTOMER_PO_NO field
     * @deprecated use SalesOrderPeer.sales_order.CUSTOMER_PO_NO constant
     */
    public static String getSalesOrder_CustomerPoNo()
    {
        return "sales_order.CUSTOMER_PO_NO";
    }
  
    /**
     * sales_order.CUSTOMER_PO_DATE
     * @return the column name for the CUSTOMER_PO_DATE field
     * @deprecated use SalesOrderPeer.sales_order.CUSTOMER_PO_DATE constant
     */
    public static String getSalesOrder_CustomerPoDate()
    {
        return "sales_order.CUSTOMER_PO_DATE";
    }
  
    /**
     * sales_order.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use SalesOrderPeer.sales_order.TOTAL_QTY constant
     */
    public static String getSalesOrder_TotalQty()
    {
        return "sales_order.TOTAL_QTY";
    }
  
    /**
     * sales_order.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use SalesOrderPeer.sales_order.TOTAL_AMOUNT constant
     */
    public static String getSalesOrder_TotalAmount()
    {
        return "sales_order.TOTAL_AMOUNT";
    }
  
    /**
     * sales_order.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use SalesOrderPeer.sales_order.TOTAL_DISCOUNT_PCT constant
     */
    public static String getSalesOrder_TotalDiscountPct()
    {
        return "sales_order.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * sales_order.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use SalesOrderPeer.sales_order.TOTAL_DISCOUNT constant
     */
    public static String getSalesOrder_TotalDiscount()
    {
        return "sales_order.TOTAL_DISCOUNT";
    }
  
    /**
     * sales_order.TOTAL_FEE
     * @return the column name for the TOTAL_FEE field
     * @deprecated use SalesOrderPeer.sales_order.TOTAL_FEE constant
     */
    public static String getSalesOrder_TotalFee()
    {
        return "sales_order.TOTAL_FEE";
    }
  
    /**
     * sales_order.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use SalesOrderPeer.sales_order.TOTAL_TAX constant
     */
    public static String getSalesOrder_TotalTax()
    {
        return "sales_order.TOTAL_TAX";
    }
  
    /**
     * sales_order.REMARK
     * @return the column name for the REMARK field
     * @deprecated use SalesOrderPeer.sales_order.REMARK constant
     */
    public static String getSalesOrder_Remark()
    {
        return "sales_order.REMARK";
    }
  
    /**
     * sales_order.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use SalesOrderPeer.sales_order.PAYMENT_TYPE_ID constant
     */
    public static String getSalesOrder_PaymentTypeId()
    {
        return "sales_order.PAYMENT_TYPE_ID";
    }
  
    /**
     * sales_order.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use SalesOrderPeer.sales_order.PAYMENT_TERM_ID constant
     */
    public static String getSalesOrder_PaymentTermId()
    {
        return "sales_order.PAYMENT_TERM_ID";
    }
  
    /**
     * sales_order.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use SalesOrderPeer.sales_order.CURRENCY_ID constant
     */
    public static String getSalesOrder_CurrencyId()
    {
        return "sales_order.CURRENCY_ID";
    }
  
    /**
     * sales_order.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use SalesOrderPeer.sales_order.CURRENCY_RATE constant
     */
    public static String getSalesOrder_CurrencyRate()
    {
        return "sales_order.CURRENCY_RATE";
    }
  
    /**
     * sales_order.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use SalesOrderPeer.sales_order.LOCATION_ID constant
     */
    public static String getSalesOrder_LocationId()
    {
        return "sales_order.LOCATION_ID";
    }
  
    /**
     * sales_order.PRINT_TIMES
     * @return the column name for the PRINT_TIMES field
     * @deprecated use SalesOrderPeer.sales_order.PRINT_TIMES constant
     */
    public static String getSalesOrder_PrintTimes()
    {
        return "sales_order.PRINT_TIMES";
    }
  
    /**
     * sales_order.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use SalesOrderPeer.sales_order.USER_NAME constant
     */
    public static String getSalesOrder_UserName()
    {
        return "sales_order.USER_NAME";
    }
  
    /**
     * sales_order.CONFIRM_BY
     * @return the column name for the CONFIRM_BY field
     * @deprecated use SalesOrderPeer.sales_order.CONFIRM_BY constant
     */
    public static String getSalesOrder_ConfirmBy()
    {
        return "sales_order.CONFIRM_BY";
    }
  
    /**
     * sales_order.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use SalesOrderPeer.sales_order.CONFIRM_DATE constant
     */
    public static String getSalesOrder_ConfirmDate()
    {
        return "sales_order.CONFIRM_DATE";
    }
  
    /**
     * sales_order.DOWN_PAYMENT
     * @return the column name for the DOWN_PAYMENT field
     * @deprecated use SalesOrderPeer.sales_order.DOWN_PAYMENT constant
     */
    public static String getSalesOrder_DownPayment()
    {
        return "sales_order.DOWN_PAYMENT";
    }
  
    /**
     * sales_order.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use SalesOrderPeer.sales_order.SHIP_TO constant
     */
    public static String getSalesOrder_ShipTo()
    {
        return "sales_order.SHIP_TO";
    }
  
    /**
     * sales_order.SHIP_DATE
     * @return the column name for the SHIP_DATE field
     * @deprecated use SalesOrderPeer.sales_order.SHIP_DATE constant
     */
    public static String getSalesOrder_ShipDate()
    {
        return "sales_order.SHIP_DATE";
    }
  
    /**
     * sales_order.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use SalesOrderPeer.sales_order.BANK_ID constant
     */
    public static String getSalesOrder_BankId()
    {
        return "sales_order.BANK_ID";
    }
  
    /**
     * sales_order.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use SalesOrderPeer.sales_order.BANK_ISSUER constant
     */
    public static String getSalesOrder_BankIssuer()
    {
        return "sales_order.BANK_ISSUER";
    }
  
    /**
     * sales_order.DP_ACCOUNT_ID
     * @return the column name for the DP_ACCOUNT_ID field
     * @deprecated use SalesOrderPeer.sales_order.DP_ACCOUNT_ID constant
     */
    public static String getSalesOrder_DpAccountId()
    {
        return "sales_order.DP_ACCOUNT_ID";
    }
  
    /**
     * sales_order.DP_DUE_DATE
     * @return the column name for the DP_DUE_DATE field
     * @deprecated use SalesOrderPeer.sales_order.DP_DUE_DATE constant
     */
    public static String getSalesOrder_DpDueDate()
    {
        return "sales_order.DP_DUE_DATE";
    }
  
    /**
     * sales_order.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use SalesOrderPeer.sales_order.REFERENCE_NO constant
     */
    public static String getSalesOrder_ReferenceNo()
    {
        return "sales_order.REFERENCE_NO";
    }
  
    /**
     * sales_order.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use SalesOrderPeer.sales_order.CASH_FLOW_TYPE_ID constant
     */
    public static String getSalesOrder_CashFlowTypeId()
    {
        return "sales_order.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * sales_order.SALES_ID
     * @return the column name for the SALES_ID field
     * @deprecated use SalesOrderPeer.sales_order.SALES_ID constant
     */
    public static String getSalesOrder_SalesId()
    {
        return "sales_order.SALES_ID";
    }
  
    /**
     * sales_order.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use SalesOrderPeer.sales_order.COURIER_ID constant
     */
    public static String getSalesOrder_CourierId()
    {
        return "sales_order.COURIER_ID";
    }
  
    /**
     * sales_order.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use SalesOrderPeer.sales_order.FOB_ID constant
     */
    public static String getSalesOrder_FobId()
    {
        return "sales_order.FOB_ID";
    }
  
    /**
     * sales_order.ESTIMATED_FREIGHT
     * @return the column name for the ESTIMATED_FREIGHT field
     * @deprecated use SalesOrderPeer.sales_order.ESTIMATED_FREIGHT constant
     */
    public static String getSalesOrder_EstimatedFreight()
    {
        return "sales_order.ESTIMATED_FREIGHT";
    }
  
    /**
     * sales_order.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use SalesOrderPeer.sales_order.IS_TAXABLE constant
     */
    public static String getSalesOrder_IsTaxable()
    {
        return "sales_order.IS_TAXABLE";
    }
  
    /**
     * sales_order.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use SalesOrderPeer.sales_order.IS_INCLUSIVE_TAX constant
     */
    public static String getSalesOrder_IsInclusiveTax()
    {
        return "sales_order.IS_INCLUSIVE_TAX";
    }
  
    /**
     * sales_order.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use SalesOrderPeer.sales_order.FISCAL_RATE constant
     */
    public static String getSalesOrder_FiscalRate()
    {
        return "sales_order.FISCAL_RATE";
    }
  
    /**
     * sales_order.FREIGHT_ACCOUNT_ID
     * @return the column name for the FREIGHT_ACCOUNT_ID field
     * @deprecated use SalesOrderPeer.sales_order.FREIGHT_ACCOUNT_ID constant
     */
    public static String getSalesOrder_FreightAccountId()
    {
        return "sales_order.FREIGHT_ACCOUNT_ID";
    }
  
    /**
     * sales_order.IS_INCLUSIVE_FREIGHT
     * @return the column name for the IS_INCLUSIVE_FREIGHT field
     * @deprecated use SalesOrderPeer.sales_order.IS_INCLUSIVE_FREIGHT constant
     */
    public static String getSalesOrder_IsInclusiveFreight()
    {
        return "sales_order.IS_INCLUSIVE_FREIGHT";
    }
  
    /**
     * sales_order.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use SalesOrderPeer.sales_order.CANCEL_BY constant
     */
    public static String getSalesOrder_CancelBy()
    {
        return "sales_order.CANCEL_BY";
    }
  
    /**
     * sales_order.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use SalesOrderPeer.sales_order.CANCEL_DATE constant
     */
    public static String getSalesOrder_CancelDate()
    {
        return "sales_order.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sales_order");
        TableMap tMap = dbMap.getTable("sales_order");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sales_order.SALES_ORDER_ID", "");
                            tMap.addColumn("sales_order.TRANSACTION_STATUS", Integer.valueOf(0));
                          tMap.addColumn("sales_order.SALES_ORDER_NO", "");
                          tMap.addColumn("sales_order.TRANSACTION_DATE", new Date());
                          tMap.addColumn("sales_order.DUE_DATE", new Date());
                          tMap.addColumn("sales_order.CUSTOMER_ID", "");
                          tMap.addColumn("sales_order.CUSTOMER_NAME", "");
                          tMap.addColumn("sales_order.CUSTOMER_PO_NO", "");
                          tMap.addColumn("sales_order.CUSTOMER_PO_DATE", new Date());
                            tMap.addColumn("sales_order.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("sales_order.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("sales_order.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("sales_order.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("sales_order.TOTAL_FEE", bd_ZERO);
                            tMap.addColumn("sales_order.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("sales_order.REMARK", "");
                          tMap.addColumn("sales_order.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("sales_order.PAYMENT_TERM_ID", "");
                          tMap.addColumn("sales_order.CURRENCY_ID", "");
                            tMap.addColumn("sales_order.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("sales_order.LOCATION_ID", "");
                            tMap.addColumn("sales_order.PRINT_TIMES", Integer.valueOf(0));
                          tMap.addColumn("sales_order.USER_NAME", "");
                          tMap.addColumn("sales_order.CONFIRM_BY", "");
                          tMap.addColumn("sales_order.CONFIRM_DATE", new Date());
                            tMap.addColumn("sales_order.DOWN_PAYMENT", bd_ZERO);
                          tMap.addColumn("sales_order.SHIP_TO", "");
                          tMap.addColumn("sales_order.SHIP_DATE", new Date());
                          tMap.addColumn("sales_order.BANK_ID", "");
                          tMap.addColumn("sales_order.BANK_ISSUER", "");
                          tMap.addColumn("sales_order.DP_ACCOUNT_ID", "");
                          tMap.addColumn("sales_order.DP_DUE_DATE", new Date());
                          tMap.addColumn("sales_order.REFERENCE_NO", "");
                          tMap.addColumn("sales_order.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("sales_order.SALES_ID", "");
                          tMap.addColumn("sales_order.COURIER_ID", "");
                          tMap.addColumn("sales_order.FOB_ID", "");
                            tMap.addColumn("sales_order.ESTIMATED_FREIGHT", bd_ZERO);
                          tMap.addColumn("sales_order.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("sales_order.IS_INCLUSIVE_TAX", Boolean.TRUE);
                            tMap.addColumn("sales_order.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("sales_order.FREIGHT_ACCOUNT_ID", "");
                          tMap.addColumn("sales_order.IS_INCLUSIVE_FREIGHT", Boolean.TRUE);
                          tMap.addColumn("sales_order.CANCEL_BY", "");
                          tMap.addColumn("sales_order.CANCEL_DATE", new Date());
          }
}
