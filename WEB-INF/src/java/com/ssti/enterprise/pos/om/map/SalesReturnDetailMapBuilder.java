package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SalesReturnDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SalesReturnDetailMapBuilder";

    /**
     * Item
     * @deprecated use SalesReturnDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sales_return_detail";
    }

  
    /**
     * sales_return_detail.SALES_RETURN_DETAIL_ID
     * @return the column name for the SALES_RETURN_DETAIL_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.SALES_RETURN_DETAIL_ID constant
     */
    public static String getSalesReturnDetail_SalesReturnDetailId()
    {
        return "sales_return_detail.SALES_RETURN_DETAIL_ID";
    }
  
    /**
     * sales_return_detail.SALES_RETURN_ID
     * @return the column name for the SALES_RETURN_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.SALES_RETURN_ID constant
     */
    public static String getSalesReturnDetail_SalesReturnId()
    {
        return "sales_return_detail.SALES_RETURN_ID";
    }
  
    /**
     * sales_return_detail.TRANSACTION_DETAIL_ID
     * @return the column name for the TRANSACTION_DETAIL_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.TRANSACTION_DETAIL_ID constant
     */
    public static String getSalesReturnDetail_TransactionDetailId()
    {
        return "sales_return_detail.TRANSACTION_DETAIL_ID";
    }
  
    /**
     * sales_return_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.INDEX_NO constant
     */
    public static String getSalesReturnDetail_IndexNo()
    {
        return "sales_return_detail.INDEX_NO";
    }
  
    /**
     * sales_return_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.ITEM_ID constant
     */
    public static String getSalesReturnDetail_ItemId()
    {
        return "sales_return_detail.ITEM_ID";
    }
  
    /**
     * sales_return_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.ITEM_CODE constant
     */
    public static String getSalesReturnDetail_ItemCode()
    {
        return "sales_return_detail.ITEM_CODE";
    }
  
    /**
     * sales_return_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.ITEM_NAME constant
     */
    public static String getSalesReturnDetail_ItemName()
    {
        return "sales_return_detail.ITEM_NAME";
    }
  
    /**
     * sales_return_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.DESCRIPTION constant
     */
    public static String getSalesReturnDetail_Description()
    {
        return "sales_return_detail.DESCRIPTION";
    }
  
    /**
     * sales_return_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.UNIT_ID constant
     */
    public static String getSalesReturnDetail_UnitId()
    {
        return "sales_return_detail.UNIT_ID";
    }
  
    /**
     * sales_return_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.UNIT_CODE constant
     */
    public static String getSalesReturnDetail_UnitCode()
    {
        return "sales_return_detail.UNIT_CODE";
    }
  
    /**
     * sales_return_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.QTY constant
     */
    public static String getSalesReturnDetail_Qty()
    {
        return "sales_return_detail.QTY";
    }
  
    /**
     * sales_return_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.QTY_BASE constant
     */
    public static String getSalesReturnDetail_QtyBase()
    {
        return "sales_return_detail.QTY_BASE";
    }
  
    /**
     * sales_return_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.ITEM_PRICE constant
     */
    public static String getSalesReturnDetail_ItemPrice()
    {
        return "sales_return_detail.ITEM_PRICE";
    }
  
    /**
     * sales_return_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.TAX_ID constant
     */
    public static String getSalesReturnDetail_TaxId()
    {
        return "sales_return_detail.TAX_ID";
    }
  
    /**
     * sales_return_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.TAX_AMOUNT constant
     */
    public static String getSalesReturnDetail_TaxAmount()
    {
        return "sales_return_detail.TAX_AMOUNT";
    }
  
    /**
     * sales_return_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.SUB_TOTAL_TAX constant
     */
    public static String getSalesReturnDetail_SubTotalTax()
    {
        return "sales_return_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * sales_return_detail.DISCOUNT_ID
     * @return the column name for the DISCOUNT_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.DISCOUNT_ID constant
     */
    public static String getSalesReturnDetail_DiscountId()
    {
        return "sales_return_detail.DISCOUNT_ID";
    }
  
    /**
     * sales_return_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.DISCOUNT constant
     */
    public static String getSalesReturnDetail_Discount()
    {
        return "sales_return_detail.DISCOUNT";
    }
  
    /**
     * sales_return_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.SUB_TOTAL_DISC constant
     */
    public static String getSalesReturnDetail_SubTotalDisc()
    {
        return "sales_return_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * sales_return_detail.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.ITEM_COST constant
     */
    public static String getSalesReturnDetail_ItemCost()
    {
        return "sales_return_detail.ITEM_COST";
    }
  
    /**
     * sales_return_detail.SUB_TOTAL_COST
     * @return the column name for the SUB_TOTAL_COST field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.SUB_TOTAL_COST constant
     */
    public static String getSalesReturnDetail_SubTotalCost()
    {
        return "sales_return_detail.SUB_TOTAL_COST";
    }
  
    /**
     * sales_return_detail.RETURN_AMOUNT
     * @return the column name for the RETURN_AMOUNT field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.RETURN_AMOUNT constant
     */
    public static String getSalesReturnDetail_ReturnAmount()
    {
        return "sales_return_detail.RETURN_AMOUNT";
    }
  
    /**
     * sales_return_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.PROJECT_ID constant
     */
    public static String getSalesReturnDetail_ProjectId()
    {
        return "sales_return_detail.PROJECT_ID";
    }
  
    /**
     * sales_return_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use SalesReturnDetailPeer.sales_return_detail.DEPARTMENT_ID constant
     */
    public static String getSalesReturnDetail_DepartmentId()
    {
        return "sales_return_detail.DEPARTMENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sales_return_detail");
        TableMap tMap = dbMap.getTable("sales_return_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sales_return_detail.SALES_RETURN_DETAIL_ID", "");
                          tMap.addForeignKey(
                "sales_return_detail.SALES_RETURN_ID", "" , "sales_return" ,
                "sales_return_id");
                          tMap.addColumn("sales_return_detail.TRANSACTION_DETAIL_ID", "");
                            tMap.addColumn("sales_return_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("sales_return_detail.ITEM_ID", "");
                          tMap.addColumn("sales_return_detail.ITEM_CODE", "");
                          tMap.addColumn("sales_return_detail.ITEM_NAME", "");
                          tMap.addColumn("sales_return_detail.DESCRIPTION", "");
                          tMap.addColumn("sales_return_detail.UNIT_ID", "");
                          tMap.addColumn("sales_return_detail.UNIT_CODE", "");
                            tMap.addColumn("sales_return_detail.QTY", bd_ZERO);
                            tMap.addColumn("sales_return_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("sales_return_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("sales_return_detail.TAX_ID", "");
                            tMap.addColumn("sales_return_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_return_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("sales_return_detail.DISCOUNT_ID", "");
                          tMap.addColumn("sales_return_detail.DISCOUNT", "");
                            tMap.addColumn("sales_return_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("sales_return_detail.ITEM_COST", bd_ZERO);
                            tMap.addColumn("sales_return_detail.SUB_TOTAL_COST", bd_ZERO);
                            tMap.addColumn("sales_return_detail.RETURN_AMOUNT", bd_ZERO);
                          tMap.addColumn("sales_return_detail.PROJECT_ID", "");
                          tMap.addColumn("sales_return_detail.DEPARTMENT_ID", "");
          }
}
