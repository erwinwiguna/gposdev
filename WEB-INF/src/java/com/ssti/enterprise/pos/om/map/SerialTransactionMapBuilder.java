package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SerialTransactionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SerialTransactionMapBuilder";

    /**
     * Item
     * @deprecated use SerialTransactionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "serial_transaction";
    }

  
    /**
     * serial_transaction.SERIAL_TRANSACTION_ID
     * @return the column name for the SERIAL_TRANSACTION_ID field
     * @deprecated use SerialTransactionPeer.serial_transaction.SERIAL_TRANSACTION_ID constant
     */
    public static String getSerialTransaction_SerialTransactionId()
    {
        return "serial_transaction.SERIAL_TRANSACTION_ID";
    }
  
    /**
     * serial_transaction.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use SerialTransactionPeer.serial_transaction.ITEM_ID constant
     */
    public static String getSerialTransaction_ItemId()
    {
        return "serial_transaction.ITEM_ID";
    }
  
    /**
     * serial_transaction.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use SerialTransactionPeer.serial_transaction.LOCATION_ID constant
     */
    public static String getSerialTransaction_LocationId()
    {
        return "serial_transaction.LOCATION_ID";
    }
  
    /**
     * serial_transaction.SERIAL_NO
     * @return the column name for the SERIAL_NO field
     * @deprecated use SerialTransactionPeer.serial_transaction.SERIAL_NO constant
     */
    public static String getSerialTransaction_SerialNo()
    {
        return "serial_transaction.SERIAL_NO";
    }
  
    /**
     * serial_transaction.QTY
     * @return the column name for the QTY field
     * @deprecated use SerialTransactionPeer.serial_transaction.QTY constant
     */
    public static String getSerialTransaction_Qty()
    {
        return "serial_transaction.QTY";
    }
  
    /**
     * serial_transaction.INVENTORY_TRANSACTION_ID
     * @return the column name for the INVENTORY_TRANSACTION_ID field
     * @deprecated use SerialTransactionPeer.serial_transaction.INVENTORY_TRANSACTION_ID constant
     */
    public static String getSerialTransaction_InventoryTransactionId()
    {
        return "serial_transaction.INVENTORY_TRANSACTION_ID";
    }
  
    /**
     * serial_transaction.TRANSACTION_DETAIL_ID
     * @return the column name for the TRANSACTION_DETAIL_ID field
     * @deprecated use SerialTransactionPeer.serial_transaction.TRANSACTION_DETAIL_ID constant
     */
    public static String getSerialTransaction_TransactionDetailId()
    {
        return "serial_transaction.TRANSACTION_DETAIL_ID";
    }
  
    /**
     * serial_transaction.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use SerialTransactionPeer.serial_transaction.TRANSACTION_ID constant
     */
    public static String getSerialTransaction_TransactionId()
    {
        return "serial_transaction.TRANSACTION_ID";
    }
  
    /**
     * serial_transaction.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use SerialTransactionPeer.serial_transaction.TRANSACTION_NO constant
     */
    public static String getSerialTransaction_TransactionNo()
    {
        return "serial_transaction.TRANSACTION_NO";
    }
  
    /**
     * serial_transaction.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use SerialTransactionPeer.serial_transaction.TRANSACTION_DATE constant
     */
    public static String getSerialTransaction_TransactionDate()
    {
        return "serial_transaction.TRANSACTION_DATE";
    }
  
    /**
     * serial_transaction.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use SerialTransactionPeer.serial_transaction.TRANSACTION_TYPE constant
     */
    public static String getSerialTransaction_TransactionType()
    {
        return "serial_transaction.TRANSACTION_TYPE";
    }
  
    /**
     * serial_transaction.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use SerialTransactionPeer.serial_transaction.DESCRIPTION constant
     */
    public static String getSerialTransaction_Description()
    {
        return "serial_transaction.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("serial_transaction");
        TableMap tMap = dbMap.getTable("serial_transaction");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("serial_transaction.SERIAL_TRANSACTION_ID", "");
                          tMap.addColumn("serial_transaction.ITEM_ID", "");
                          tMap.addColumn("serial_transaction.LOCATION_ID", "");
                          tMap.addColumn("serial_transaction.SERIAL_NO", "");
                            tMap.addColumn("serial_transaction.QTY", Integer.valueOf(0));
                          tMap.addColumn("serial_transaction.INVENTORY_TRANSACTION_ID", "");
                          tMap.addColumn("serial_transaction.TRANSACTION_DETAIL_ID", "");
                          tMap.addColumn("serial_transaction.TRANSACTION_ID", "");
                          tMap.addColumn("serial_transaction.TRANSACTION_NO", "");
                          tMap.addColumn("serial_transaction.TRANSACTION_DATE", new Date());
                            tMap.addColumn("serial_transaction.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("serial_transaction.DESCRIPTION", "");
          }
}
