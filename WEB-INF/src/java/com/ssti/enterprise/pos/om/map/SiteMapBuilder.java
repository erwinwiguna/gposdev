package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SiteMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SiteMapBuilder";

    /**
     * Item
     * @deprecated use SitePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "site";
    }

  
    /**
     * site.SITE_ID
     * @return the column name for the SITE_ID field
     * @deprecated use SitePeer.site.SITE_ID constant
     */
    public static String getSite_SiteId()
    {
        return "site.SITE_ID";
    }
  
    /**
     * site.SITE_NAME
     * @return the column name for the SITE_NAME field
     * @deprecated use SitePeer.site.SITE_NAME constant
     */
    public static String getSite_SiteName()
    {
        return "site.SITE_NAME";
    }
  
    /**
     * site.COSTING_METHOD
     * @return the column name for the COSTING_METHOD field
     * @deprecated use SitePeer.site.COSTING_METHOD constant
     */
    public static String getSite_CostingMethod()
    {
        return "site.COSTING_METHOD";
    }
  
    /**
     * site.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use SitePeer.site.DESCRIPTION constant
     */
    public static String getSite_Description()
    {
        return "site.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("site");
        TableMap tMap = dbMap.getTable("site");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("site.SITE_ID", "");
                          tMap.addColumn("site.SITE_NAME", "");
                            tMap.addColumn("site.COSTING_METHOD", Integer.valueOf(0));
                          tMap.addColumn("site.DESCRIPTION", "");
          }
}
