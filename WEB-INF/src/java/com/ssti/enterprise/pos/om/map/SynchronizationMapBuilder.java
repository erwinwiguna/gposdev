package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SynchronizationMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SynchronizationMapBuilder";

    /**
     * Item
     * @deprecated use SynchronizationPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "synchronization";
    }

  
    /**
     * synchronization.SYNCHRONIZATION_ID
     * @return the column name for the SYNCHRONIZATION_ID field
     * @deprecated use SynchronizationPeer.synchronization.SYNCHRONIZATION_ID constant
     */
    public static String getSynchronization_SynchronizationId()
    {
        return "synchronization.SYNCHRONIZATION_ID";
    }
  
    /**
     * synchronization.SYNC_TYPE
     * @return the column name for the SYNC_TYPE field
     * @deprecated use SynchronizationPeer.synchronization.SYNC_TYPE constant
     */
    public static String getSynchronization_SyncType()
    {
        return "synchronization.SYNC_TYPE";
    }
  
    /**
     * synchronization.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use SynchronizationPeer.synchronization.LOCATION_ID constant
     */
    public static String getSynchronization_LocationId()
    {
        return "synchronization.LOCATION_ID";
    }
  
    /**
     * synchronization.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use SynchronizationPeer.synchronization.LOCATION_NAME constant
     */
    public static String getSynchronization_LocationName()
    {
        return "synchronization.LOCATION_NAME";
    }
  
    /**
     * synchronization.SYNC_DATE
     * @return the column name for the SYNC_DATE field
     * @deprecated use SynchronizationPeer.synchronization.SYNC_DATE constant
     */
    public static String getSynchronization_SyncDate()
    {
        return "synchronization.SYNC_DATE";
    }
  
    /**
     * synchronization.FILE_NAME
     * @return the column name for the FILE_NAME field
     * @deprecated use SynchronizationPeer.synchronization.FILE_NAME constant
     */
    public static String getSynchronization_FileName()
    {
        return "synchronization.FILE_NAME";
    }
  
    /**
     * synchronization.IS_PROCESSED
     * @return the column name for the IS_PROCESSED field
     * @deprecated use SynchronizationPeer.synchronization.IS_PROCESSED constant
     */
    public static String getSynchronization_IsProcessed()
    {
        return "synchronization.IS_PROCESSED";
    }
  
    /**
     * synchronization.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use SynchronizationPeer.synchronization.USER_NAME constant
     */
    public static String getSynchronization_UserName()
    {
        return "synchronization.USER_NAME";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("synchronization");
        TableMap tMap = dbMap.getTable("synchronization");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("synchronization.SYNCHRONIZATION_ID", "");
                            tMap.addColumn("synchronization.SYNC_TYPE", Integer.valueOf(0));
                          tMap.addColumn("synchronization.LOCATION_ID", "");
                          tMap.addColumn("synchronization.LOCATION_NAME", "");
                          tMap.addColumn("synchronization.SYNC_DATE", new Date());
                          tMap.addColumn("synchronization.FILE_NAME", "");
                          tMap.addColumn("synchronization.IS_PROCESSED", Boolean.TRUE);
                          tMap.addColumn("synchronization.USER_NAME", "");
          }
}
