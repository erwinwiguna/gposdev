package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SysConfigMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SysConfigMapBuilder";

    /**
     * Item
     * @deprecated use SysConfigPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sys_config";
    }

  
    /**
     * sys_config.SYS_CONFIG_ID
     * @return the column name for the SYS_CONFIG_ID field
     * @deprecated use SysConfigPeer.sys_config.SYS_CONFIG_ID constant
     */
    public static String getSysConfig_SysConfigId()
    {
        return "sys_config.SYS_CONFIG_ID";
    }
  
    /**
     * sys_config.POS_DEFAULT_SCREEN
     * @return the column name for the POS_DEFAULT_SCREEN field
     * @deprecated use SysConfigPeer.sys_config.POS_DEFAULT_SCREEN constant
     */
    public static String getSysConfig_PosDefaultScreen()
    {
        return "sys_config.POS_DEFAULT_SCREEN";
    }
  
    /**
     * sys_config.POS_DEFAULT_SCREEN_FOCUS
     * @return the column name for the POS_DEFAULT_SCREEN_FOCUS field
     * @deprecated use SysConfigPeer.sys_config.POS_DEFAULT_SCREEN_FOCUS constant
     */
    public static String getSysConfig_PosDefaultScreenFocus()
    {
        return "sys_config.POS_DEFAULT_SCREEN_FOCUS";
    }
  
    /**
     * sys_config.POS_DEFAULT_OPEN_CASHIER_AMT
     * @return the column name for the POS_DEFAULT_OPEN_CASHIER_AMT field
     * @deprecated use SysConfigPeer.sys_config.POS_DEFAULT_OPEN_CASHIER_AMT constant
     */
    public static String getSysConfig_PosDefaultOpenCashierAmt()
    {
        return "sys_config.POS_DEFAULT_OPEN_CASHIER_AMT";
    }
  
    /**
     * sys_config.POS_DEFAULT_ITEM_DISPLAY
     * @return the column name for the POS_DEFAULT_ITEM_DISPLAY field
     * @deprecated use SysConfigPeer.sys_config.POS_DEFAULT_ITEM_DISPLAY constant
     */
    public static String getSysConfig_PosDefaultItemDisplay()
    {
        return "sys_config.POS_DEFAULT_ITEM_DISPLAY";
    }
  
    /**
     * sys_config.POS_DEFAULT_ITEM_SORT
     * @return the column name for the POS_DEFAULT_ITEM_SORT field
     * @deprecated use SysConfigPeer.sys_config.POS_DEFAULT_ITEM_SORT constant
     */
    public static String getSysConfig_PosDefaultItemSort()
    {
        return "sys_config.POS_DEFAULT_ITEM_SORT";
    }
  
    /**
     * sys_config.POS_DEFAULT_PRINT
     * @return the column name for the POS_DEFAULT_PRINT field
     * @deprecated use SysConfigPeer.sys_config.POS_DEFAULT_PRINT constant
     */
    public static String getSysConfig_PosDefaultPrint()
    {
        return "sys_config.POS_DEFAULT_PRINT";
    }
  
    /**
     * sys_config.POS_CONFIRM_QTY
     * @return the column name for the POS_CONFIRM_QTY field
     * @deprecated use SysConfigPeer.sys_config.POS_CONFIRM_QTY constant
     */
    public static String getSysConfig_PosConfirmQty()
    {
        return "sys_config.POS_CONFIRM_QTY";
    }
  
    /**
     * sys_config.POS_CONFIRM_COST
     * @return the column name for the POS_CONFIRM_COST field
     * @deprecated use SysConfigPeer.sys_config.POS_CONFIRM_COST constant
     */
    public static String getSysConfig_PosConfirmCost()
    {
        return "sys_config.POS_CONFIRM_COST";
    }
  
    /**
     * sys_config.POS_CHANGE_VALIDATION
     * @return the column name for the POS_CHANGE_VALIDATION field
     * @deprecated use SysConfigPeer.sys_config.POS_CHANGE_VALIDATION constant
     */
    public static String getSysConfig_PosChangeValidation()
    {
        return "sys_config.POS_CHANGE_VALIDATION";
    }
  
    /**
     * sys_config.POS_DELETE_VALIDATION
     * @return the column name for the POS_DELETE_VALIDATION field
     * @deprecated use SysConfigPeer.sys_config.POS_DELETE_VALIDATION constant
     */
    public static String getSysConfig_PosDeleteValidation()
    {
        return "sys_config.POS_DELETE_VALIDATION";
    }
  
    /**
     * sys_config.POS_OPEN_CASHIER_VALIDATION
     * @return the column name for the POS_OPEN_CASHIER_VALIDATION field
     * @deprecated use SysConfigPeer.sys_config.POS_OPEN_CASHIER_VALIDATION constant
     */
    public static String getSysConfig_PosOpenCashierValidation()
    {
        return "sys_config.POS_OPEN_CASHIER_VALIDATION";
    }
  
    /**
     * sys_config.POS_SAVE_DIRECT_PRINT
     * @return the column name for the POS_SAVE_DIRECT_PRINT field
     * @deprecated use SysConfigPeer.sys_config.POS_SAVE_DIRECT_PRINT constant
     */
    public static String getSysConfig_PosSaveDirectPrint()
    {
        return "sys_config.POS_SAVE_DIRECT_PRINT";
    }
  
    /**
     * sys_config.POS_PRINTER_DETAIL_LINE
     * @return the column name for the POS_PRINTER_DETAIL_LINE field
     * @deprecated use SysConfigPeer.sys_config.POS_PRINTER_DETAIL_LINE constant
     */
    public static String getSysConfig_PosPrinterDetailLine()
    {
        return "sys_config.POS_PRINTER_DETAIL_LINE";
    }
  
    /**
     * sys_config.POS_DIRECT_ADD
     * @return the column name for the POS_DIRECT_ADD field
     * @deprecated use SysConfigPeer.sys_config.POS_DIRECT_ADD constant
     */
    public static String getSysConfig_PosDirectAdd()
    {
        return "sys_config.POS_DIRECT_ADD";
    }
  
    /**
     * sys_config.POS_ROUNDING_TO
     * @return the column name for the POS_ROUNDING_TO field
     * @deprecated use SysConfigPeer.sys_config.POS_ROUNDING_TO constant
     */
    public static String getSysConfig_PosRoundingTo()
    {
        return "sys_config.POS_ROUNDING_TO";
    }
  
    /**
     * sys_config.POS_ROUNDING_MODE
     * @return the column name for the POS_ROUNDING_MODE field
     * @deprecated use SysConfigPeer.sys_config.POS_ROUNDING_MODE constant
     */
    public static String getSysConfig_PosRoundingMode()
    {
        return "sys_config.POS_ROUNDING_MODE";
    }
  
    /**
     * sys_config.POS_SPC_DISC_CODE
     * @return the column name for the POS_SPC_DISC_CODE field
     * @deprecated use SysConfigPeer.sys_config.POS_SPC_DISC_CODE constant
     */
    public static String getSysConfig_PosSpcDiscCode()
    {
        return "sys_config.POS_SPC_DISC_CODE";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_USE
     * @return the column name for the COMPLEX_BARCODE_USE field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_USE constant
     */
    public static String getSysConfig_ComplexBarcodeUse()
    {
        return "sys_config.COMPLEX_BARCODE_USE";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_PREFIX
     * @return the column name for the COMPLEX_BARCODE_PREFIX field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_PREFIX constant
     */
    public static String getSysConfig_ComplexBarcodePrefix()
    {
        return "sys_config.COMPLEX_BARCODE_PREFIX";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_PLU_START
     * @return the column name for the COMPLEX_BARCODE_PLU_START field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_PLU_START constant
     */
    public static String getSysConfig_ComplexBarcodePluStart()
    {
        return "sys_config.COMPLEX_BARCODE_PLU_START";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_PLU_LENGTH
     * @return the column name for the COMPLEX_BARCODE_PLU_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_PLU_LENGTH constant
     */
    public static String getSysConfig_ComplexBarcodePluLength()
    {
        return "sys_config.COMPLEX_BARCODE_PLU_LENGTH";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_QTY_START
     * @return the column name for the COMPLEX_BARCODE_QTY_START field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_QTY_START constant
     */
    public static String getSysConfig_ComplexBarcodeQtyStart()
    {
        return "sys_config.COMPLEX_BARCODE_QTY_START";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_QTY_LENGTH
     * @return the column name for the COMPLEX_BARCODE_QTY_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_QTY_LENGTH constant
     */
    public static String getSysConfig_ComplexBarcodeQtyLength()
    {
        return "sys_config.COMPLEX_BARCODE_QTY_LENGTH";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_QTY_DEC_POS
     * @return the column name for the COMPLEX_BARCODE_QTY_DEC_POS field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_QTY_DEC_POS constant
     */
    public static String getSysConfig_ComplexBarcodeQtyDecPos()
    {
        return "sys_config.COMPLEX_BARCODE_QTY_DEC_POS";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_OTHER_START
     * @return the column name for the COMPLEX_BARCODE_OTHER_START field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_OTHER_START constant
     */
    public static String getSysConfig_ComplexBarcodeOtherStart()
    {
        return "sys_config.COMPLEX_BARCODE_OTHER_START";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_OTHER_LENGTH
     * @return the column name for the COMPLEX_BARCODE_OTHER_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_OTHER_LENGTH constant
     */
    public static String getSysConfig_ComplexBarcodeOtherLength()
    {
        return "sys_config.COMPLEX_BARCODE_OTHER_LENGTH";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_OTHER_DEC_POS
     * @return the column name for the COMPLEX_BARCODE_OTHER_DEC_POS field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_OTHER_DEC_POS constant
     */
    public static String getSysConfig_ComplexBarcodeOtherDecPos()
    {
        return "sys_config.COMPLEX_BARCODE_OTHER_DEC_POS";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_OTHER_TYPE
     * @return the column name for the COMPLEX_BARCODE_OTHER_TYPE field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_OTHER_TYPE constant
     */
    public static String getSysConfig_ComplexBarcodeOtherType()
    {
        return "sys_config.COMPLEX_BARCODE_OTHER_TYPE";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_SUFFIX
     * @return the column name for the COMPLEX_BARCODE_SUFFIX field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_SUFFIX constant
     */
    public static String getSysConfig_ComplexBarcodeSuffix()
    {
        return "sys_config.COMPLEX_BARCODE_SUFFIX";
    }
  
    /**
     * sys_config.COMPLEX_BARCODE_TOTAL_LENGTH
     * @return the column name for the COMPLEX_BARCODE_TOTAL_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.COMPLEX_BARCODE_TOTAL_LENGTH constant
     */
    public static String getSysConfig_ComplexBarcodeTotalLength()
    {
        return "sys_config.COMPLEX_BARCODE_TOTAL_LENGTH";
    }
  
    /**
     * sys_config.BACKUP_OUTPUT_DIR
     * @return the column name for the BACKUP_OUTPUT_DIR field
     * @deprecated use SysConfigPeer.sys_config.BACKUP_OUTPUT_DIR constant
     */
    public static String getSysConfig_BackupOutputDir()
    {
        return "sys_config.BACKUP_OUTPUT_DIR";
    }
  
    /**
     * sys_config.BACKUP_FILE_PREF
     * @return the column name for the BACKUP_FILE_PREF field
     * @deprecated use SysConfigPeer.sys_config.BACKUP_FILE_PREF constant
     */
    public static String getSysConfig_BackupFilePref()
    {
        return "sys_config.BACKUP_FILE_PREF";
    }
  
    /**
     * sys_config.PICTURE_PATH
     * @return the column name for the PICTURE_PATH field
     * @deprecated use SysConfigPeer.sys_config.PICTURE_PATH constant
     */
    public static String getSysConfig_PicturePath()
    {
        return "sys_config.PICTURE_PATH";
    }
  
    /**
     * sys_config.B2B_PO_PATH
     * @return the column name for the B2B_PO_PATH field
     * @deprecated use SysConfigPeer.sys_config.B2B_PO_PATH constant
     */
    public static String getSysConfig_B2bPoPath()
    {
        return "sys_config.B2B_PO_PATH";
    }
  
    /**
     * sys_config.IREPORT_PATH
     * @return the column name for the IREPORT_PATH field
     * @deprecated use SysConfigPeer.sys_config.IREPORT_PATH constant
     */
    public static String getSysConfig_IreportPath()
    {
        return "sys_config.IREPORT_PATH";
    }
  
    /**
     * sys_config.ONLINE_HELP_URL
     * @return the column name for the ONLINE_HELP_URL field
     * @deprecated use SysConfigPeer.sys_config.ONLINE_HELP_URL constant
     */
    public static String getSysConfig_OnlineHelpUrl()
    {
        return "sys_config.ONLINE_HELP_URL";
    }
  
    /**
     * sys_config.SMS_GATEWAY_URL
     * @return the column name for the SMS_GATEWAY_URL field
     * @deprecated use SysConfigPeer.sys_config.SMS_GATEWAY_URL constant
     */
    public static String getSysConfig_SmsGatewayUrl()
    {
        return "sys_config.SMS_GATEWAY_URL";
    }
  
    /**
     * sys_config.EMAIL_SERVER
     * @return the column name for the EMAIL_SERVER field
     * @deprecated use SysConfigPeer.sys_config.EMAIL_SERVER constant
     */
    public static String getSysConfig_EmailServer()
    {
        return "sys_config.EMAIL_SERVER";
    }
  
    /**
     * sys_config.EMAIL_USER
     * @return the column name for the EMAIL_USER field
     * @deprecated use SysConfigPeer.sys_config.EMAIL_USER constant
     */
    public static String getSysConfig_EmailUser()
    {
        return "sys_config.EMAIL_USER";
    }
  
    /**
     * sys_config.EMAIL_PWD
     * @return the column name for the EMAIL_PWD field
     * @deprecated use SysConfigPeer.sys_config.EMAIL_PWD constant
     */
    public static String getSysConfig_EmailPwd()
    {
        return "sys_config.EMAIL_PWD";
    }
  
    /**
     * sys_config.SYNC_ALLOW_PENDING_TRANS
     * @return the column name for the SYNC_ALLOW_PENDING_TRANS field
     * @deprecated use SysConfigPeer.sys_config.SYNC_ALLOW_PENDING_TRANS constant
     */
    public static String getSysConfig_SyncAllowPendingTrans()
    {
        return "sys_config.SYNC_ALLOW_PENDING_TRANS";
    }
  
    /**
     * sys_config.SYNC_FULL_INVENTORY
     * @return the column name for the SYNC_FULL_INVENTORY field
     * @deprecated use SysConfigPeer.sys_config.SYNC_FULL_INVENTORY constant
     */
    public static String getSysConfig_SyncFullInventory()
    {
        return "sys_config.SYNC_FULL_INVENTORY";
    }
  
    /**
     * sys_config.SYNC_ALLOW_STORE_ADJ
     * @return the column name for the SYNC_ALLOW_STORE_ADJ field
     * @deprecated use SysConfigPeer.sys_config.SYNC_ALLOW_STORE_ADJ constant
     */
    public static String getSysConfig_SyncAllowStoreAdj()
    {
        return "sys_config.SYNC_ALLOW_STORE_ADJ";
    }
  
    /**
     * sys_config.SYNC_ALLOW_STORE_RECEIPT
     * @return the column name for the SYNC_ALLOW_STORE_RECEIPT field
     * @deprecated use SysConfigPeer.sys_config.SYNC_ALLOW_STORE_RECEIPT constant
     */
    public static String getSysConfig_SyncAllowStoreReceipt()
    {
        return "sys_config.SYNC_ALLOW_STORE_RECEIPT";
    }
  
    /**
     * sys_config.SYNC_ALLOW_FREE_TRANSFER
     * @return the column name for the SYNC_ALLOW_FREE_TRANSFER field
     * @deprecated use SysConfigPeer.sys_config.SYNC_ALLOW_FREE_TRANSFER constant
     */
    public static String getSysConfig_SyncAllowFreeTransfer()
    {
        return "sys_config.SYNC_ALLOW_FREE_TRANSFER";
    }
  
    /**
     * sys_config.SYNC_HODATA_PATH
     * @return the column name for the SYNC_HODATA_PATH field
     * @deprecated use SysConfigPeer.sys_config.SYNC_HODATA_PATH constant
     */
    public static String getSysConfig_SyncHodataPath()
    {
        return "sys_config.SYNC_HODATA_PATH";
    }
  
    /**
     * sys_config.SYNC_STOREDATA_PATH
     * @return the column name for the SYNC_STOREDATA_PATH field
     * @deprecated use SysConfigPeer.sys_config.SYNC_STOREDATA_PATH constant
     */
    public static String getSysConfig_SyncStoredataPath()
    {
        return "sys_config.SYNC_STOREDATA_PATH";
    }
  
    /**
     * sys_config.SYNC_STORESETUP_PATH
     * @return the column name for the SYNC_STORESETUP_PATH field
     * @deprecated use SysConfigPeer.sys_config.SYNC_STORESETUP_PATH constant
     */
    public static String getSysConfig_SyncStoresetupPath()
    {
        return "sys_config.SYNC_STORESETUP_PATH";
    }
  
    /**
     * sys_config.SYNC_STORETRF_PATH
     * @return the column name for the SYNC_STORETRF_PATH field
     * @deprecated use SysConfigPeer.sys_config.SYNC_STORETRF_PATH constant
     */
    public static String getSysConfig_SyncStoretrfPath()
    {
        return "sys_config.SYNC_STORETRF_PATH";
    }
  
    /**
     * sys_config.SYNC_DESTINATION
     * @return the column name for the SYNC_DESTINATION field
     * @deprecated use SysConfigPeer.sys_config.SYNC_DESTINATION constant
     */
    public static String getSysConfig_SyncDestination()
    {
        return "sys_config.SYNC_DESTINATION";
    }
  
    /**
     * sys_config.SYNC_DISK_DESTINATION
     * @return the column name for the SYNC_DISK_DESTINATION field
     * @deprecated use SysConfigPeer.sys_config.SYNC_DISK_DESTINATION constant
     */
    public static String getSysConfig_SyncDiskDestination()
    {
        return "sys_config.SYNC_DISK_DESTINATION";
    }
  
    /**
     * sys_config.SYNC_HO_UPLOAD_URL
     * @return the column name for the SYNC_HO_UPLOAD_URL field
     * @deprecated use SysConfigPeer.sys_config.SYNC_HO_UPLOAD_URL constant
     */
    public static String getSysConfig_SyncHoUploadUrl()
    {
        return "sys_config.SYNC_HO_UPLOAD_URL";
    }
  
    /**
     * sys_config.SYNC_HO_EMAIL
     * @return the column name for the SYNC_HO_EMAIL field
     * @deprecated use SysConfigPeer.sys_config.SYNC_HO_EMAIL constant
     */
    public static String getSysConfig_SyncHoEmail()
    {
        return "sys_config.SYNC_HO_EMAIL";
    }
  
    /**
     * sys_config.SYNC_STORE_EMAIL_SERVER
     * @return the column name for the SYNC_STORE_EMAIL_SERVER field
     * @deprecated use SysConfigPeer.sys_config.SYNC_STORE_EMAIL_SERVER constant
     */
    public static String getSysConfig_SyncStoreEmailServer()
    {
        return "sys_config.SYNC_STORE_EMAIL_SERVER";
    }
  
    /**
     * sys_config.SYNC_STORE_EMAIL_USER
     * @return the column name for the SYNC_STORE_EMAIL_USER field
     * @deprecated use SysConfigPeer.sys_config.SYNC_STORE_EMAIL_USER constant
     */
    public static String getSysConfig_SyncStoreEmailUser()
    {
        return "sys_config.SYNC_STORE_EMAIL_USER";
    }
  
    /**
     * sys_config.SYNC_STORE_EMAIL_PWD
     * @return the column name for the SYNC_STORE_EMAIL_PWD field
     * @deprecated use SysConfigPeer.sys_config.SYNC_STORE_EMAIL_PWD constant
     */
    public static String getSysConfig_SyncStoreEmailPwd()
    {
        return "sys_config.SYNC_STORE_EMAIL_PWD";
    }
  
    /**
     * sys_config.SYNC_DAILY_CLOSING
     * @return the column name for the SYNC_DAILY_CLOSING field
     * @deprecated use SysConfigPeer.sys_config.SYNC_DAILY_CLOSING constant
     */
    public static String getSysConfig_SyncDailyClosing()
    {
        return "sys_config.SYNC_DAILY_CLOSING";
    }
  
    /**
     * sys_config.USE_LOCATION_CTLTABLE
     * @return the column name for the USE_LOCATION_CTLTABLE field
     * @deprecated use SysConfigPeer.sys_config.USE_LOCATION_CTLTABLE constant
     */
    public static String getSysConfig_UseLocationCtltable()
    {
        return "sys_config.USE_LOCATION_CTLTABLE";
    }
  
    /**
     * sys_config.USE_CUSTOMER_TR_PREFIX
     * @return the column name for the USE_CUSTOMER_TR_PREFIX field
     * @deprecated use SysConfigPeer.sys_config.USE_CUSTOMER_TR_PREFIX constant
     */
    public static String getSysConfig_UseCustomerTrPrefix()
    {
        return "sys_config.USE_CUSTOMER_TR_PREFIX";
    }
  
    /**
     * sys_config.USE_VENDOR_TR_PREFIX
     * @return the column name for the USE_VENDOR_TR_PREFIX field
     * @deprecated use SysConfigPeer.sys_config.USE_VENDOR_TR_PREFIX constant
     */
    public static String getSysConfig_UseVendorTrPrefix()
    {
        return "sys_config.USE_VENDOR_TR_PREFIX";
    }
  
    /**
     * sys_config.USE_VENDOR_TAX
     * @return the column name for the USE_VENDOR_TAX field
     * @deprecated use SysConfigPeer.sys_config.USE_VENDOR_TAX constant
     */
    public static String getSysConfig_UseVendorTax()
    {
        return "sys_config.USE_VENDOR_TAX";
    }
  
    /**
     * sys_config.USE_CUSTOMER_TAX
     * @return the column name for the USE_CUSTOMER_TAX field
     * @deprecated use SysConfigPeer.sys_config.USE_CUSTOMER_TAX constant
     */
    public static String getSysConfig_UseCustomerTax()
    {
        return "sys_config.USE_CUSTOMER_TAX";
    }
  
    /**
     * sys_config.USE_CUSTOMER_LOCATION
     * @return the column name for the USE_CUSTOMER_LOCATION field
     * @deprecated use SysConfigPeer.sys_config.USE_CUSTOMER_LOCATION constant
     */
    public static String getSysConfig_UseCustomerLocation()
    {
        return "sys_config.USE_CUSTOMER_LOCATION";
    }
  
    /**
     * sys_config.USE_CF_IN_OUT
     * @return the column name for the USE_CF_IN_OUT field
     * @deprecated use SysConfigPeer.sys_config.USE_CF_IN_OUT constant
     */
    public static String getSysConfig_UseCfInOut()
    {
        return "sys_config.USE_CF_IN_OUT";
    }
  
    /**
     * sys_config.USE_DIST_MODULES
     * @return the column name for the USE_DIST_MODULES field
     * @deprecated use SysConfigPeer.sys_config.USE_DIST_MODULES constant
     */
    public static String getSysConfig_UseDistModules()
    {
        return "sys_config.USE_DIST_MODULES";
    }
  
    /**
     * sys_config.USE_WST_MODULES
     * @return the column name for the USE_WST_MODULES field
     * @deprecated use SysConfigPeer.sys_config.USE_WST_MODULES constant
     */
    public static String getSysConfig_UseWstModules()
    {
        return "sys_config.USE_WST_MODULES";
    }
  
    /**
     * sys_config.USE_EDI_MODULES
     * @return the column name for the USE_EDI_MODULES field
     * @deprecated use SysConfigPeer.sys_config.USE_EDI_MODULES constant
     */
    public static String getSysConfig_UseEdiModules()
    {
        return "sys_config.USE_EDI_MODULES";
    }
  
    /**
     * sys_config.USE_BOM
     * @return the column name for the USE_BOM field
     * @deprecated use SysConfigPeer.sys_config.USE_BOM constant
     */
    public static String getSysConfig_UseBom()
    {
        return "sys_config.USE_BOM";
    }
  
    /**
     * sys_config.USE_PDT
     * @return the column name for the USE_PDT field
     * @deprecated use SysConfigPeer.sys_config.USE_PDT constant
     */
    public static String getSysConfig_UsePdt()
    {
        return "sys_config.USE_PDT";
    }
  
    /**
     * sys_config.CONFIRM_TRF_AT_TOLOC
     * @return the column name for the CONFIRM_TRF_AT_TOLOC field
     * @deprecated use SysConfigPeer.sys_config.CONFIRM_TRF_AT_TOLOC constant
     */
    public static String getSysConfig_ConfirmTrfAtToloc()
    {
        return "sys_config.CONFIRM_TRF_AT_TOLOC";
    }
  
    /**
     * sys_config.SI_ALLOW_IMPORT_SO
     * @return the column name for the SI_ALLOW_IMPORT_SO field
     * @deprecated use SysConfigPeer.sys_config.SI_ALLOW_IMPORT_SO constant
     */
    public static String getSysConfig_SiAllowImportSo()
    {
        return "sys_config.SI_ALLOW_IMPORT_SO";
    }
  
    /**
     * sys_config.SI_DEFAULT_SCREEN
     * @return the column name for the SI_DEFAULT_SCREEN field
     * @deprecated use SysConfigPeer.sys_config.SI_DEFAULT_SCREEN constant
     */
    public static String getSysConfig_SiDefaultScreen()
    {
        return "sys_config.SI_DEFAULT_SCREEN";
    }
  
    /**
     * sys_config.ITEM_COLOR_LT_MIN
     * @return the column name for the ITEM_COLOR_LT_MIN field
     * @deprecated use SysConfigPeer.sys_config.ITEM_COLOR_LT_MIN constant
     */
    public static String getSysConfig_ItemColorLtMin()
    {
        return "sys_config.ITEM_COLOR_LT_MIN";
    }
  
    /**
     * sys_config.ITEM_COLOR_LT_ROP
     * @return the column name for the ITEM_COLOR_LT_ROP field
     * @deprecated use SysConfigPeer.sys_config.ITEM_COLOR_LT_ROP constant
     */
    public static String getSysConfig_ItemColorLtRop()
    {
        return "sys_config.ITEM_COLOR_LT_ROP";
    }
  
    /**
     * sys_config.ITEM_COLOR_GT_MAX
     * @return the column name for the ITEM_COLOR_GT_MAX field
     * @deprecated use SysConfigPeer.sys_config.ITEM_COLOR_GT_MAX constant
     */
    public static String getSysConfig_ItemColorGtMax()
    {
        return "sys_config.ITEM_COLOR_GT_MAX";
    }
  
    /**
     * sys_config.ITEM_CODE_LENGTH
     * @return the column name for the ITEM_CODE_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.ITEM_CODE_LENGTH constant
     */
    public static String getSysConfig_ItemCodeLength()
    {
        return "sys_config.ITEM_CODE_LENGTH";
    }
  
    /**
     * sys_config.CUST_CODE_LENGTH
     * @return the column name for the CUST_CODE_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.CUST_CODE_LENGTH constant
     */
    public static String getSysConfig_CustCodeLength()
    {
        return "sys_config.CUST_CODE_LENGTH";
    }
  
    /**
     * sys_config.VEND_CODE_LENGTH
     * @return the column name for the VEND_CODE_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.VEND_CODE_LENGTH constant
     */
    public static String getSysConfig_VendCodeLength()
    {
        return "sys_config.VEND_CODE_LENGTH";
    }
  
    /**
     * sys_config.OTHR_CODE_LENGTH
     * @return the column name for the OTHR_CODE_LENGTH field
     * @deprecated use SysConfigPeer.sys_config.OTHR_CODE_LENGTH constant
     */
    public static String getSysConfig_OthrCodeLength()
    {
        return "sys_config.OTHR_CODE_LENGTH";
    }
  
    /**
     * sys_config.SALES_INCLUSIVE_TAX
     * @return the column name for the SALES_INCLUSIVE_TAX field
     * @deprecated use SysConfigPeer.sys_config.SALES_INCLUSIVE_TAX constant
     */
    public static String getSysConfig_SalesInclusiveTax()
    {
        return "sys_config.SALES_INCLUSIVE_TAX";
    }
  
    /**
     * sys_config.PURCH_INCLUSIVE_TAX
     * @return the column name for the PURCH_INCLUSIVE_TAX field
     * @deprecated use SysConfigPeer.sys_config.PURCH_INCLUSIVE_TAX constant
     */
    public static String getSysConfig_PurchInclusiveTax()
    {
        return "sys_config.PURCH_INCLUSIVE_TAX";
    }
  
    /**
     * sys_config.SALES_MULTI_CURRENCY
     * @return the column name for the SALES_MULTI_CURRENCY field
     * @deprecated use SysConfigPeer.sys_config.SALES_MULTI_CURRENCY constant
     */
    public static String getSysConfig_SalesMultiCurrency()
    {
        return "sys_config.SALES_MULTI_CURRENCY";
    }
  
    /**
     * sys_config.PURCH_MULTI_CURRENCY
     * @return the column name for the PURCH_MULTI_CURRENCY field
     * @deprecated use SysConfigPeer.sys_config.PURCH_MULTI_CURRENCY constant
     */
    public static String getSysConfig_PurchMultiCurrency()
    {
        return "sys_config.PURCH_MULTI_CURRENCY";
    }
  
    /**
     * sys_config.SALES_MERGE_SAME_ITEM
     * @return the column name for the SALES_MERGE_SAME_ITEM field
     * @deprecated use SysConfigPeer.sys_config.SALES_MERGE_SAME_ITEM constant
     */
    public static String getSysConfig_SalesMergeSameItem()
    {
        return "sys_config.SALES_MERGE_SAME_ITEM";
    }
  
    /**
     * sys_config.PURCH_MERGE_SAME_ITEM
     * @return the column name for the PURCH_MERGE_SAME_ITEM field
     * @deprecated use SysConfigPeer.sys_config.PURCH_MERGE_SAME_ITEM constant
     */
    public static String getSysConfig_PurchMergeSameItem()
    {
        return "sys_config.PURCH_MERGE_SAME_ITEM";
    }
  
    /**
     * sys_config.SALES_SORT_BY
     * @return the column name for the SALES_SORT_BY field
     * @deprecated use SysConfigPeer.sys_config.SALES_SORT_BY constant
     */
    public static String getSysConfig_SalesSortBy()
    {
        return "sys_config.SALES_SORT_BY";
    }
  
    /**
     * sys_config.PURCH_SORT_BY
     * @return the column name for the PURCH_SORT_BY field
     * @deprecated use SysConfigPeer.sys_config.PURCH_SORT_BY constant
     */
    public static String getSysConfig_PurchSortBy()
    {
        return "sys_config.PURCH_SORT_BY";
    }
  
    /**
     * sys_config.SALES_FIRSTLINE_INSERT
     * @return the column name for the SALES_FIRSTLINE_INSERT field
     * @deprecated use SysConfigPeer.sys_config.SALES_FIRSTLINE_INSERT constant
     */
    public static String getSysConfig_SalesFirstlineInsert()
    {
        return "sys_config.SALES_FIRSTLINE_INSERT";
    }
  
    /**
     * sys_config.PURCH_FIRSTLINE_INSERT
     * @return the column name for the PURCH_FIRSTLINE_INSERT field
     * @deprecated use SysConfigPeer.sys_config.PURCH_FIRSTLINE_INSERT constant
     */
    public static String getSysConfig_PurchFirstlineInsert()
    {
        return "sys_config.PURCH_FIRSTLINE_INSERT";
    }
  
    /**
     * sys_config.INVEN_FIRSTLINE_INSERT
     * @return the column name for the INVEN_FIRSTLINE_INSERT field
     * @deprecated use SysConfigPeer.sys_config.INVEN_FIRSTLINE_INSERT constant
     */
    public static String getSysConfig_InvenFirstlineInsert()
    {
        return "sys_config.INVEN_FIRSTLINE_INSERT";
    }
  
    /**
     * sys_config.SALES_FOB_COURIER
     * @return the column name for the SALES_FOB_COURIER field
     * @deprecated use SysConfigPeer.sys_config.SALES_FOB_COURIER constant
     */
    public static String getSysConfig_SalesFobCourier()
    {
        return "sys_config.SALES_FOB_COURIER";
    }
  
    /**
     * sys_config.PURCH_FOB_COURIER
     * @return the column name for the PURCH_FOB_COURIER field
     * @deprecated use SysConfigPeer.sys_config.PURCH_FOB_COURIER constant
     */
    public static String getSysConfig_PurchFobCourier()
    {
        return "sys_config.PURCH_FOB_COURIER";
    }
  
    /**
     * sys_config.SALES_COMMA_SCALE
     * @return the column name for the SALES_COMMA_SCALE field
     * @deprecated use SysConfigPeer.sys_config.SALES_COMMA_SCALE constant
     */
    public static String getSysConfig_SalesCommaScale()
    {
        return "sys_config.SALES_COMMA_SCALE";
    }
  
    /**
     * sys_config.PURCH_COMMA_SCALE
     * @return the column name for the PURCH_COMMA_SCALE field
     * @deprecated use SysConfigPeer.sys_config.PURCH_COMMA_SCALE constant
     */
    public static String getSysConfig_PurchCommaScale()
    {
        return "sys_config.PURCH_COMMA_SCALE";
    }
  
    /**
     * sys_config.INVEN_COMMA_SCALE
     * @return the column name for the INVEN_COMMA_SCALE field
     * @deprecated use SysConfigPeer.sys_config.INVEN_COMMA_SCALE constant
     */
    public static String getSysConfig_InvenCommaScale()
    {
        return "sys_config.INVEN_COMMA_SCALE";
    }
  
    /**
     * sys_config.CUST_MAX_SB_MODE
     * @return the column name for the CUST_MAX_SB_MODE field
     * @deprecated use SysConfigPeer.sys_config.CUST_MAX_SB_MODE constant
     */
    public static String getSysConfig_CustMaxSbMode()
    {
        return "sys_config.CUST_MAX_SB_MODE";
    }
  
    /**
     * sys_config.VEND_MAX_SB_MODE
     * @return the column name for the VEND_MAX_SB_MODE field
     * @deprecated use SysConfigPeer.sys_config.VEND_MAX_SB_MODE constant
     */
    public static String getSysConfig_VendMaxSbMode()
    {
        return "sys_config.VEND_MAX_SB_MODE";
    }
  
    /**
     * sys_config.PURCH_PR_QTY_GT_PO
     * @return the column name for the PURCH_PR_QTY_GT_PO field
     * @deprecated use SysConfigPeer.sys_config.PURCH_PR_QTY_GT_PO constant
     */
    public static String getSysConfig_PurchPrQtyGtPo()
    {
        return "sys_config.PURCH_PR_QTY_GT_PO";
    }
  
    /**
     * sys_config.PURCH_PR_QTY_EQUAL_PO
     * @return the column name for the PURCH_PR_QTY_EQUAL_PO field
     * @deprecated use SysConfigPeer.sys_config.PURCH_PR_QTY_EQUAL_PO constant
     */
    public static String getSysConfig_PurchPrQtyEqualPo()
    {
        return "sys_config.PURCH_PR_QTY_EQUAL_PO";
    }
  
    /**
     * sys_config.PURCH_LIMIT_BY_EMP
     * @return the column name for the PURCH_LIMIT_BY_EMP field
     * @deprecated use SysConfigPeer.sys_config.PURCH_LIMIT_BY_EMP constant
     */
    public static String getSysConfig_PurchLimitByEmp()
    {
        return "sys_config.PURCH_LIMIT_BY_EMP";
    }
  
    /**
     * sys_config.PI_ALLOW_IMPORT_PO
     * @return the column name for the PI_ALLOW_IMPORT_PO field
     * @deprecated use SysConfigPeer.sys_config.PI_ALLOW_IMPORT_PO constant
     */
    public static String getSysConfig_PiAllowImportPo()
    {
        return "sys_config.PI_ALLOW_IMPORT_PO";
    }
  
    /**
     * sys_config.PI_LAST_PURCH_METHOD
     * @return the column name for the PI_LAST_PURCH_METHOD field
     * @deprecated use SysConfigPeer.sys_config.PI_LAST_PURCH_METHOD constant
     */
    public static String getSysConfig_PiLastPurchMethod()
    {
        return "sys_config.PI_LAST_PURCH_METHOD";
    }
  
    /**
     * sys_config.PI_UPDATE_LAST_PURCHASE
     * @return the column name for the PI_UPDATE_LAST_PURCHASE field
     * @deprecated use SysConfigPeer.sys_config.PI_UPDATE_LAST_PURCHASE constant
     */
    public static String getSysConfig_PiUpdateLastPurchase()
    {
        return "sys_config.PI_UPDATE_LAST_PURCHASE";
    }
  
    /**
     * sys_config.PI_UPDATE_SALES_PRICE
     * @return the column name for the PI_UPDATE_SALES_PRICE field
     * @deprecated use SysConfigPeer.sys_config.PI_UPDATE_SALES_PRICE constant
     */
    public static String getSysConfig_PiUpdateSalesPrice()
    {
        return "sys_config.PI_UPDATE_SALES_PRICE";
    }
  
    /**
     * sys_config.PI_UPDATE_PR_COST
     * @return the column name for the PI_UPDATE_PR_COST field
     * @deprecated use SysConfigPeer.sys_config.PI_UPDATE_PR_COST constant
     */
    public static String getSysConfig_PiUpdatePrCost()
    {
        return "sys_config.PI_UPDATE_PR_COST";
    }
  
    /**
     * sys_config.SALES_USE_SALESMAN
     * @return the column name for the SALES_USE_SALESMAN field
     * @deprecated use SysConfigPeer.sys_config.SALES_USE_SALESMAN constant
     */
    public static String getSysConfig_SalesUseSalesman()
    {
        return "sys_config.SALES_USE_SALESMAN";
    }
  
    /**
     * sys_config.SALES_USE_MIN_PRICE
     * @return the column name for the SALES_USE_MIN_PRICE field
     * @deprecated use SysConfigPeer.sys_config.SALES_USE_MIN_PRICE constant
     */
    public static String getSysConfig_SalesUseMinPrice()
    {
        return "sys_config.SALES_USE_MIN_PRICE";
    }
  
    /**
     * sys_config.SALES_SALESMAN_PERITEM
     * @return the column name for the SALES_SALESMAN_PERITEM field
     * @deprecated use SysConfigPeer.sys_config.SALES_SALESMAN_PERITEM constant
     */
    public static String getSysConfig_SalesSalesmanPeritem()
    {
        return "sys_config.SALES_SALESMAN_PERITEM";
    }
  
    /**
     * sys_config.SALES_ALLOW_MULTIPMT_DISC
     * @return the column name for the SALES_ALLOW_MULTIPMT_DISC field
     * @deprecated use SysConfigPeer.sys_config.SALES_ALLOW_MULTIPMT_DISC constant
     */
    public static String getSysConfig_SalesAllowMultipmtDisc()
    {
        return "sys_config.SALES_ALLOW_MULTIPMT_DISC";
    }
  
    /**
     * sys_config.SALES_EMPLEVEL_DISC
     * @return the column name for the SALES_EMPLEVEL_DISC field
     * @deprecated use SysConfigPeer.sys_config.SALES_EMPLEVEL_DISC constant
     */
    public static String getSysConfig_SalesEmplevelDisc()
    {
        return "sys_config.SALES_EMPLEVEL_DISC";
    }
  
    /**
     * sys_config.SALES_CUSTOM_PRICING_USE
     * @return the column name for the SALES_CUSTOM_PRICING_USE field
     * @deprecated use SysConfigPeer.sys_config.SALES_CUSTOM_PRICING_USE constant
     */
    public static String getSysConfig_SalesCustomPricingUse()
    {
        return "sys_config.SALES_CUSTOM_PRICING_USE";
    }
  
    /**
     * sys_config.SALES_CUSTOM_PRICING_TPL
     * @return the column name for the SALES_CUSTOM_PRICING_TPL field
     * @deprecated use SysConfigPeer.sys_config.SALES_CUSTOM_PRICING_TPL constant
     */
    public static String getSysConfig_SalesCustomPricingTpl()
    {
        return "sys_config.SALES_CUSTOM_PRICING_TPL";
    }
  
    /**
     * sys_config.POINTREWARD_USE
     * @return the column name for the POINTREWARD_USE field
     * @deprecated use SysConfigPeer.sys_config.POINTREWARD_USE constant
     */
    public static String getSysConfig_PointrewardUse()
    {
        return "sys_config.POINTREWARD_USE";
    }
  
    /**
     * sys_config.POINTREWARD_BYITEM
     * @return the column name for the POINTREWARD_BYITEM field
     * @deprecated use SysConfigPeer.sys_config.POINTREWARD_BYITEM constant
     */
    public static String getSysConfig_PointrewardByitem()
    {
        return "sys_config.POINTREWARD_BYITEM";
    }
  
    /**
     * sys_config.POINTREWARD_FIELD
     * @return the column name for the POINTREWARD_FIELD field
     * @deprecated use SysConfigPeer.sys_config.POINTREWARD_FIELD constant
     */
    public static String getSysConfig_PointrewardField()
    {
        return "sys_config.POINTREWARD_FIELD";
    }
  
    /**
     * sys_config.POINTREWARD_PVALUE
     * @return the column name for the POINTREWARD_PVALUE field
     * @deprecated use SysConfigPeer.sys_config.POINTREWARD_PVALUE constant
     */
    public static String getSysConfig_PointrewardPvalue()
    {
        return "sys_config.POINTREWARD_PVALUE";
    }
  
    /**
     * sys_config.DEFAULT_SEARCH_COND
     * @return the column name for the DEFAULT_SEARCH_COND field
     * @deprecated use SysConfigPeer.sys_config.DEFAULT_SEARCH_COND constant
     */
    public static String getSysConfig_DefaultSearchCond()
    {
        return "sys_config.DEFAULT_SEARCH_COND";
    }
  
    /**
     * sys_config.KEY_NEW_TRANS
     * @return the column name for the KEY_NEW_TRANS field
     * @deprecated use SysConfigPeer.sys_config.KEY_NEW_TRANS constant
     */
    public static String getSysConfig_KeyNewTrans()
    {
        return "sys_config.KEY_NEW_TRANS";
    }
  
    /**
     * sys_config.KEY_OPEN_LOOKUP
     * @return the column name for the KEY_OPEN_LOOKUP field
     * @deprecated use SysConfigPeer.sys_config.KEY_OPEN_LOOKUP constant
     */
    public static String getSysConfig_KeyOpenLookup()
    {
        return "sys_config.KEY_OPEN_LOOKUP";
    }
  
    /**
     * sys_config.KEY_PAYMENT_FOCUS
     * @return the column name for the KEY_PAYMENT_FOCUS field
     * @deprecated use SysConfigPeer.sys_config.KEY_PAYMENT_FOCUS constant
     */
    public static String getSysConfig_KeyPaymentFocus()
    {
        return "sys_config.KEY_PAYMENT_FOCUS";
    }
  
    /**
     * sys_config.KEY_SAVE_TRANS
     * @return the column name for the KEY_SAVE_TRANS field
     * @deprecated use SysConfigPeer.sys_config.KEY_SAVE_TRANS constant
     */
    public static String getSysConfig_KeySaveTrans()
    {
        return "sys_config.KEY_SAVE_TRANS";
    }
  
    /**
     * sys_config.KEY_PRINT_TRANS
     * @return the column name for the KEY_PRINT_TRANS field
     * @deprecated use SysConfigPeer.sys_config.KEY_PRINT_TRANS constant
     */
    public static String getSysConfig_KeyPrintTrans()
    {
        return "sys_config.KEY_PRINT_TRANS";
    }
  
    /**
     * sys_config.KEY_FIND_TRANS
     * @return the column name for the KEY_FIND_TRANS field
     * @deprecated use SysConfigPeer.sys_config.KEY_FIND_TRANS constant
     */
    public static String getSysConfig_KeyFindTrans()
    {
        return "sys_config.KEY_FIND_TRANS";
    }
  
    /**
     * sys_config.KEY_SELECT_PAYMENT
     * @return the column name for the KEY_SELECT_PAYMENT field
     * @deprecated use SysConfigPeer.sys_config.KEY_SELECT_PAYMENT constant
     */
    public static String getSysConfig_KeySelectPayment()
    {
        return "sys_config.KEY_SELECT_PAYMENT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sys_config");
        TableMap tMap = dbMap.getTable("sys_config");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sys_config.SYS_CONFIG_ID", "");
                          tMap.addColumn("sys_config.POS_DEFAULT_SCREEN", "");
                          tMap.addColumn("sys_config.POS_DEFAULT_SCREEN_FOCUS", "");
                          tMap.addColumn("sys_config.POS_DEFAULT_OPEN_CASHIER_AMT", "");
                          tMap.addColumn("sys_config.POS_DEFAULT_ITEM_DISPLAY", "");
                          tMap.addColumn("sys_config.POS_DEFAULT_ITEM_SORT", "");
                          tMap.addColumn("sys_config.POS_DEFAULT_PRINT", "");
                          tMap.addColumn("sys_config.POS_CONFIRM_QTY", "");
                          tMap.addColumn("sys_config.POS_CONFIRM_COST", "");
                          tMap.addColumn("sys_config.POS_CHANGE_VALIDATION", "");
                          tMap.addColumn("sys_config.POS_DELETE_VALIDATION", "");
                          tMap.addColumn("sys_config.POS_OPEN_CASHIER_VALIDATION", "");
                          tMap.addColumn("sys_config.POS_SAVE_DIRECT_PRINT", "");
                          tMap.addColumn("sys_config.POS_PRINTER_DETAIL_LINE", "");
                          tMap.addColumn("sys_config.POS_DIRECT_ADD", "");
                          tMap.addColumn("sys_config.POS_ROUNDING_TO", "");
                          tMap.addColumn("sys_config.POS_ROUNDING_MODE", "");
                          tMap.addColumn("sys_config.POS_SPC_DISC_CODE", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_USE", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_PREFIX", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_PLU_START", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_PLU_LENGTH", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_QTY_START", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_QTY_LENGTH", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_QTY_DEC_POS", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_OTHER_START", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_OTHER_LENGTH", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_OTHER_DEC_POS", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_OTHER_TYPE", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_SUFFIX", "");
                          tMap.addColumn("sys_config.COMPLEX_BARCODE_TOTAL_LENGTH", "");
                          tMap.addColumn("sys_config.BACKUP_OUTPUT_DIR", "");
                          tMap.addColumn("sys_config.BACKUP_FILE_PREF", "");
                          tMap.addColumn("sys_config.PICTURE_PATH", "");
                          tMap.addColumn("sys_config.B2B_PO_PATH", "");
                          tMap.addColumn("sys_config.IREPORT_PATH", "");
                          tMap.addColumn("sys_config.ONLINE_HELP_URL", "");
                          tMap.addColumn("sys_config.SMS_GATEWAY_URL", "");
                          tMap.addColumn("sys_config.EMAIL_SERVER", "");
                          tMap.addColumn("sys_config.EMAIL_USER", "");
                          tMap.addColumn("sys_config.EMAIL_PWD", "");
                          tMap.addColumn("sys_config.SYNC_ALLOW_PENDING_TRANS", "");
                          tMap.addColumn("sys_config.SYNC_FULL_INVENTORY", "");
                          tMap.addColumn("sys_config.SYNC_ALLOW_STORE_ADJ", "");
                          tMap.addColumn("sys_config.SYNC_ALLOW_STORE_RECEIPT", "");
                          tMap.addColumn("sys_config.SYNC_ALLOW_FREE_TRANSFER", "");
                          tMap.addColumn("sys_config.SYNC_HODATA_PATH", "");
                          tMap.addColumn("sys_config.SYNC_STOREDATA_PATH", "");
                          tMap.addColumn("sys_config.SYNC_STORESETUP_PATH", "");
                          tMap.addColumn("sys_config.SYNC_STORETRF_PATH", "");
                          tMap.addColumn("sys_config.SYNC_DESTINATION", "");
                          tMap.addColumn("sys_config.SYNC_DISK_DESTINATION", "");
                          tMap.addColumn("sys_config.SYNC_HO_UPLOAD_URL", "");
                          tMap.addColumn("sys_config.SYNC_HO_EMAIL", "");
                          tMap.addColumn("sys_config.SYNC_STORE_EMAIL_SERVER", "");
                          tMap.addColumn("sys_config.SYNC_STORE_EMAIL_USER", "");
                          tMap.addColumn("sys_config.SYNC_STORE_EMAIL_PWD", "");
                          tMap.addColumn("sys_config.SYNC_DAILY_CLOSING", "");
                          tMap.addColumn("sys_config.USE_LOCATION_CTLTABLE", "");
                          tMap.addColumn("sys_config.USE_CUSTOMER_TR_PREFIX", "");
                          tMap.addColumn("sys_config.USE_VENDOR_TR_PREFIX", "");
                          tMap.addColumn("sys_config.USE_VENDOR_TAX", "");
                          tMap.addColumn("sys_config.USE_CUSTOMER_TAX", "");
                          tMap.addColumn("sys_config.USE_CUSTOMER_LOCATION", "");
                          tMap.addColumn("sys_config.USE_CF_IN_OUT", "");
                          tMap.addColumn("sys_config.USE_DIST_MODULES", "");
                          tMap.addColumn("sys_config.USE_WST_MODULES", "");
                          tMap.addColumn("sys_config.USE_EDI_MODULES", "");
                          tMap.addColumn("sys_config.USE_BOM", "");
                          tMap.addColumn("sys_config.USE_PDT", "");
                          tMap.addColumn("sys_config.CONFIRM_TRF_AT_TOLOC", "");
                          tMap.addColumn("sys_config.SI_ALLOW_IMPORT_SO", "");
                          tMap.addColumn("sys_config.SI_DEFAULT_SCREEN", "");
                          tMap.addColumn("sys_config.ITEM_COLOR_LT_MIN", "");
                          tMap.addColumn("sys_config.ITEM_COLOR_LT_ROP", "");
                          tMap.addColumn("sys_config.ITEM_COLOR_GT_MAX", "");
                          tMap.addColumn("sys_config.ITEM_CODE_LENGTH", "");
                          tMap.addColumn("sys_config.CUST_CODE_LENGTH", "");
                          tMap.addColumn("sys_config.VEND_CODE_LENGTH", "");
                          tMap.addColumn("sys_config.OTHR_CODE_LENGTH", "");
                          tMap.addColumn("sys_config.SALES_INCLUSIVE_TAX", "");
                          tMap.addColumn("sys_config.PURCH_INCLUSIVE_TAX", "");
                          tMap.addColumn("sys_config.SALES_MULTI_CURRENCY", "");
                          tMap.addColumn("sys_config.PURCH_MULTI_CURRENCY", "");
                          tMap.addColumn("sys_config.SALES_MERGE_SAME_ITEM", "");
                          tMap.addColumn("sys_config.PURCH_MERGE_SAME_ITEM", "");
                          tMap.addColumn("sys_config.SALES_SORT_BY", "");
                          tMap.addColumn("sys_config.PURCH_SORT_BY", "");
                          tMap.addColumn("sys_config.SALES_FIRSTLINE_INSERT", "");
                          tMap.addColumn("sys_config.PURCH_FIRSTLINE_INSERT", "");
                          tMap.addColumn("sys_config.INVEN_FIRSTLINE_INSERT", "");
                          tMap.addColumn("sys_config.SALES_FOB_COURIER", "");
                          tMap.addColumn("sys_config.PURCH_FOB_COURIER", "");
                          tMap.addColumn("sys_config.SALES_COMMA_SCALE", "");
                          tMap.addColumn("sys_config.PURCH_COMMA_SCALE", "");
                          tMap.addColumn("sys_config.INVEN_COMMA_SCALE", "");
                          tMap.addColumn("sys_config.CUST_MAX_SB_MODE", "");
                          tMap.addColumn("sys_config.VEND_MAX_SB_MODE", "");
                          tMap.addColumn("sys_config.PURCH_PR_QTY_GT_PO", "");
                          tMap.addColumn("sys_config.PURCH_PR_QTY_EQUAL_PO", "");
                          tMap.addColumn("sys_config.PURCH_LIMIT_BY_EMP", "");
                          tMap.addColumn("sys_config.PI_ALLOW_IMPORT_PO", "");
                          tMap.addColumn("sys_config.PI_LAST_PURCH_METHOD", "");
                          tMap.addColumn("sys_config.PI_UPDATE_LAST_PURCHASE", "");
                          tMap.addColumn("sys_config.PI_UPDATE_SALES_PRICE", "");
                          tMap.addColumn("sys_config.PI_UPDATE_PR_COST", "");
                          tMap.addColumn("sys_config.SALES_USE_SALESMAN", "");
                          tMap.addColumn("sys_config.SALES_USE_MIN_PRICE", "");
                          tMap.addColumn("sys_config.SALES_SALESMAN_PERITEM", "");
                          tMap.addColumn("sys_config.SALES_ALLOW_MULTIPMT_DISC", "");
                          tMap.addColumn("sys_config.SALES_EMPLEVEL_DISC", "");
                          tMap.addColumn("sys_config.SALES_CUSTOM_PRICING_USE", "");
                          tMap.addColumn("sys_config.SALES_CUSTOM_PRICING_TPL", "");
                          tMap.addColumn("sys_config.POINTREWARD_USE", "");
                          tMap.addColumn("sys_config.POINTREWARD_BYITEM", "");
                          tMap.addColumn("sys_config.POINTREWARD_FIELD", "");
                          tMap.addColumn("sys_config.POINTREWARD_PVALUE", "");
                          tMap.addColumn("sys_config.DEFAULT_SEARCH_COND", "");
                          tMap.addColumn("sys_config.KEY_NEW_TRANS", "");
                          tMap.addColumn("sys_config.KEY_OPEN_LOOKUP", "");
                          tMap.addColumn("sys_config.KEY_PAYMENT_FOCUS", "");
                          tMap.addColumn("sys_config.KEY_SAVE_TRANS", "");
                          tMap.addColumn("sys_config.KEY_PRINT_TRANS", "");
                          tMap.addColumn("sys_config.KEY_FIND_TRANS", "");
                          tMap.addColumn("sys_config.KEY_SELECT_PAYMENT", "");
          }
}
