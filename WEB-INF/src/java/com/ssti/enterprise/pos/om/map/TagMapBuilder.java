package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TagMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TagMapBuilder";

    /**
     * Item
     * @deprecated use TagPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "tag";
    }

  
    /**
     * tag.TAG_ID
     * @return the column name for the TAG_ID field
     * @deprecated use TagPeer.tag.TAG_ID constant
     */
    public static String getTag_TagId()
    {
        return "tag.TAG_ID";
    }
  
    /**
     * tag.TAG_NAME
     * @return the column name for the TAG_NAME field
     * @deprecated use TagPeer.tag.TAG_NAME constant
     */
    public static String getTag_TagName()
    {
        return "tag.TAG_NAME";
    }
  
    /**
     * tag.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use TagPeer.tag.DESCRIPTION constant
     */
    public static String getTag_Description()
    {
        return "tag.DESCRIPTION";
    }
  
    /**
     * tag.TAG_ICON
     * @return the column name for the TAG_ICON field
     * @deprecated use TagPeer.tag.TAG_ICON constant
     */
    public static String getTag_TagIcon()
    {
        return "tag.TAG_ICON";
    }
  
    /**
     * tag.TAG_LOGO
     * @return the column name for the TAG_LOGO field
     * @deprecated use TagPeer.tag.TAG_LOGO constant
     */
    public static String getTag_TagLogo()
    {
        return "tag.TAG_LOGO";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("tag");
        TableMap tMap = dbMap.getTable("tag");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("tag.TAG_ID", "");
                          tMap.addColumn("tag.TAG_NAME", "");
                          tMap.addColumn("tag.DESCRIPTION", "");
                          tMap.addColumn("tag.TAG_ICON", "");
                          tMap.addColumn("tag.TAG_LOGO", "");
          }
}
