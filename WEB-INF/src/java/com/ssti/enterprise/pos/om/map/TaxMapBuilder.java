package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TaxMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TaxMapBuilder";

    /**
     * Item
     * @deprecated use TaxPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "tax";
    }

  
    /**
     * tax.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use TaxPeer.tax.TAX_ID constant
     */
    public static String getTax_TaxId()
    {
        return "tax.TAX_ID";
    }
  
    /**
     * tax.TAX_CODE
     * @return the column name for the TAX_CODE field
     * @deprecated use TaxPeer.tax.TAX_CODE constant
     */
    public static String getTax_TaxCode()
    {
        return "tax.TAX_CODE";
    }
  
    /**
     * tax.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use TaxPeer.tax.AMOUNT constant
     */
    public static String getTax_Amount()
    {
        return "tax.AMOUNT";
    }
  
    /**
     * tax.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use TaxPeer.tax.DESCRIPTION constant
     */
    public static String getTax_Description()
    {
        return "tax.DESCRIPTION";
    }
  
    /**
     * tax.DEFAULT_SALES_TAX
     * @return the column name for the DEFAULT_SALES_TAX field
     * @deprecated use TaxPeer.tax.DEFAULT_SALES_TAX constant
     */
    public static String getTax_DefaultSalesTax()
    {
        return "tax.DEFAULT_SALES_TAX";
    }
  
    /**
     * tax.DEFAULT_PURCHASE_TAX
     * @return the column name for the DEFAULT_PURCHASE_TAX field
     * @deprecated use TaxPeer.tax.DEFAULT_PURCHASE_TAX constant
     */
    public static String getTax_DefaultPurchaseTax()
    {
        return "tax.DEFAULT_PURCHASE_TAX";
    }
  
    /**
     * tax.SALES_TAX_ACCOUNT
     * @return the column name for the SALES_TAX_ACCOUNT field
     * @deprecated use TaxPeer.tax.SALES_TAX_ACCOUNT constant
     */
    public static String getTax_SalesTaxAccount()
    {
        return "tax.SALES_TAX_ACCOUNT";
    }
  
    /**
     * tax.PURCHASE_TAX_ACCOUNT
     * @return the column name for the PURCHASE_TAX_ACCOUNT field
     * @deprecated use TaxPeer.tax.PURCHASE_TAX_ACCOUNT constant
     */
    public static String getTax_PurchaseTaxAccount()
    {
        return "tax.PURCHASE_TAX_ACCOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("tax");
        TableMap tMap = dbMap.getTable("tax");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("tax.TAX_ID", "");
                          tMap.addColumn("tax.TAX_CODE", "");
                            tMap.addColumn("tax.AMOUNT", bd_ZERO);
                          tMap.addColumn("tax.DESCRIPTION", "");
                          tMap.addColumn("tax.DEFAULT_SALES_TAX", Boolean.TRUE);
                          tMap.addColumn("tax.DEFAULT_PURCHASE_TAX", Boolean.TRUE);
                          tMap.addColumn("tax.SALES_TAX_ACCOUNT", "");
                          tMap.addColumn("tax.PURCHASE_TAX_ACCOUNT", "");
          }
}
