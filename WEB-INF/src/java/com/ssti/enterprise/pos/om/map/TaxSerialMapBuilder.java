package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TaxSerialMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TaxSerialMapBuilder";

    /**
     * Item
     * @deprecated use TaxSerialPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "tax_serial";
    }

  
    /**
     * tax_serial.TRANS_ID
     * @return the column name for the TRANS_ID field
     * @deprecated use TaxSerialPeer.tax_serial.TRANS_ID constant
     */
    public static String getTaxSerial_TransId()
    {
        return "tax_serial.TRANS_ID";
    }
  
    /**
     * tax_serial.TAX_SERIAL
     * @return the column name for the TAX_SERIAL field
     * @deprecated use TaxSerialPeer.tax_serial.TAX_SERIAL constant
     */
    public static String getTaxSerial_TaxSerial()
    {
        return "tax_serial.TAX_SERIAL";
    }
  
    /**
     * tax_serial.TAX_DATE
     * @return the column name for the TAX_DATE field
     * @deprecated use TaxSerialPeer.tax_serial.TAX_DATE constant
     */
    public static String getTaxSerial_TaxDate()
    {
        return "tax_serial.TAX_DATE";
    }
  
    /**
     * tax_serial.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use TaxSerialPeer.tax_serial.AMOUNT constant
     */
    public static String getTaxSerial_Amount()
    {
        return "tax_serial.AMOUNT";
    }
  
    /**
     * tax_serial.AMOUNT_TAX
     * @return the column name for the AMOUNT_TAX field
     * @deprecated use TaxSerialPeer.tax_serial.AMOUNT_TAX constant
     */
    public static String getTaxSerial_AmountTax()
    {
        return "tax_serial.AMOUNT_TAX";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("tax_serial");
        TableMap tMap = dbMap.getTable("tax_serial");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("tax_serial.TRANS_ID", "");
                          tMap.addColumn("tax_serial.TAX_SERIAL", "");
                          tMap.addColumn("tax_serial.TAX_DATE", new Date());
                            tMap.addColumn("tax_serial.AMOUNT", bd_ZERO);
                            tMap.addColumn("tax_serial.AMOUNT_TAX", bd_ZERO);
          }
}
