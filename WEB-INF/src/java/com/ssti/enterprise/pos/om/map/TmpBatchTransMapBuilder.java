package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TmpBatchTransMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TmpBatchTransMapBuilder";

    /**
     * Item
     * @deprecated use TmpBatchTransPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "tmp_batch_trans";
    }

  
    /**
     * tmp_batch_trans.TMP_BATCH_TRANS_ID
     * @return the column name for the TMP_BATCH_TRANS_ID field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.TMP_BATCH_TRANS_ID constant
     */
    public static String getTmpBatchTrans_TmpBatchTransId()
    {
        return "tmp_batch_trans.TMP_BATCH_TRANS_ID";
    }
  
    /**
     * tmp_batch_trans.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.ITEM_ID constant
     */
    public static String getTmpBatchTrans_ItemId()
    {
        return "tmp_batch_trans.ITEM_ID";
    }
  
    /**
     * tmp_batch_trans.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.BATCH_NO constant
     */
    public static String getTmpBatchTrans_BatchNo()
    {
        return "tmp_batch_trans.BATCH_NO";
    }
  
    /**
     * tmp_batch_trans.EXPIRED_DATE
     * @return the column name for the EXPIRED_DATE field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.EXPIRED_DATE constant
     */
    public static String getTmpBatchTrans_ExpiredDate()
    {
        return "tmp_batch_trans.EXPIRED_DATE";
    }
  
    /**
     * tmp_batch_trans.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.DESCRIPTION constant
     */
    public static String getTmpBatchTrans_Description()
    {
        return "tmp_batch_trans.DESCRIPTION";
    }
  
    /**
     * tmp_batch_trans.TRANSACTION_DETAIL_ID
     * @return the column name for the TRANSACTION_DETAIL_ID field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.TRANSACTION_DETAIL_ID constant
     */
    public static String getTmpBatchTrans_TransactionDetailId()
    {
        return "tmp_batch_trans.TRANSACTION_DETAIL_ID";
    }
  
    /**
     * tmp_batch_trans.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.TRANSACTION_ID constant
     */
    public static String getTmpBatchTrans_TransactionId()
    {
        return "tmp_batch_trans.TRANSACTION_ID";
    }
  
    /**
     * tmp_batch_trans.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use TmpBatchTransPeer.tmp_batch_trans.TRANSACTION_TYPE constant
     */
    public static String getTmpBatchTrans_TransactionType()
    {
        return "tmp_batch_trans.TRANSACTION_TYPE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("tmp_batch_trans");
        TableMap tMap = dbMap.getTable("tmp_batch_trans");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("tmp_batch_trans.TMP_BATCH_TRANS_ID", "");
                          tMap.addColumn("tmp_batch_trans.ITEM_ID", "");
                          tMap.addColumn("tmp_batch_trans.BATCH_NO", "");
                          tMap.addColumn("tmp_batch_trans.EXPIRED_DATE", new Date());
                          tMap.addColumn("tmp_batch_trans.DESCRIPTION", "");
                          tMap.addColumn("tmp_batch_trans.TRANSACTION_DETAIL_ID", "");
                          tMap.addColumn("tmp_batch_trans.TRANSACTION_ID", "");
                            tMap.addColumn("tmp_batch_trans.TRANSACTION_TYPE", Integer.valueOf(0));
          }
}
