package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TurbineGroupMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TurbineGroupMapBuilder";

    /**
     * Item
     * @deprecated use TurbineGroupPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "TURBINE_GROUP";
    }

  
    /**
     * TURBINE_GROUP.GROUP_ID
     * @return the column name for the GROUP_ID field
     * @deprecated use TurbineGroupPeer.TURBINE_GROUP.GROUP_ID constant
     */
    public static String getTurbineGroup_GroupId()
    {
        return "TURBINE_GROUP.GROUP_ID";
    }
  
    /**
     * TURBINE_GROUP.GROUP_NAME
     * @return the column name for the GROUP_NAME field
     * @deprecated use TurbineGroupPeer.TURBINE_GROUP.GROUP_NAME constant
     */
    public static String getTurbineGroup_GroupName()
    {
        return "TURBINE_GROUP.GROUP_NAME";
    }
  
    /**
     * TURBINE_GROUP.OBJECTDATA
     * @return the column name for the OBJECTDATA field
     * @deprecated use TurbineGroupPeer.TURBINE_GROUP.OBJECTDATA constant
     */
    public static String getTurbineGroup_Objectdata()
    {
        return "TURBINE_GROUP.OBJECTDATA";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("TURBINE_GROUP");
        TableMap tMap = dbMap.getTable("TURBINE_GROUP");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("TURBINE_GROUP_SEQ");

                      tMap.addPrimaryKey("TURBINE_GROUP.GROUP_ID", Integer.valueOf(0));
                          tMap.addColumn("TURBINE_GROUP.GROUP_NAME", "");
                          tMap.addColumn("TURBINE_GROUP.OBJECTDATA", new Object());
          }
}
