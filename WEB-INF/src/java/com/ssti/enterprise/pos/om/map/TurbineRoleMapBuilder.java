package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TurbineRoleMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TurbineRoleMapBuilder";

    /**
     * Item
     * @deprecated use TurbineRolePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "TURBINE_ROLE";
    }

  
    /**
     * TURBINE_ROLE.ROLE_ID
     * @return the column name for the ROLE_ID field
     * @deprecated use TurbineRolePeer.TURBINE_ROLE.ROLE_ID constant
     */
    public static String getTurbineRole_RoleId()
    {
        return "TURBINE_ROLE.ROLE_ID";
    }
  
    /**
     * TURBINE_ROLE.ROLE_NAME
     * @return the column name for the ROLE_NAME field
     * @deprecated use TurbineRolePeer.TURBINE_ROLE.ROLE_NAME constant
     */
    public static String getTurbineRole_RoleName()
    {
        return "TURBINE_ROLE.ROLE_NAME";
    }
  
    /**
     * TURBINE_ROLE.OBJECTDATA
     * @return the column name for the OBJECTDATA field
     * @deprecated use TurbineRolePeer.TURBINE_ROLE.OBJECTDATA constant
     */
    public static String getTurbineRole_Objectdata()
    {
        return "TURBINE_ROLE.OBJECTDATA";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("TURBINE_ROLE");
        TableMap tMap = dbMap.getTable("TURBINE_ROLE");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("TURBINE_ROLE_SEQ");

                      tMap.addPrimaryKey("TURBINE_ROLE.ROLE_ID", Integer.valueOf(0));
                          tMap.addColumn("TURBINE_ROLE.ROLE_NAME", "");
                          tMap.addColumn("TURBINE_ROLE.OBJECTDATA", new Object());
          }
}
