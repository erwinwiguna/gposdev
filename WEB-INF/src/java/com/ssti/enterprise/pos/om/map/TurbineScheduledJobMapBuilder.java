package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TurbineScheduledJobMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TurbineScheduledJobMapBuilder";

    /**
     * Item
     * @deprecated use TurbineScheduledJobPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "TURBINE_SCHEDULED_JOB";
    }

  
    /**
     * TURBINE_SCHEDULED_JOB.JOB_ID
     * @return the column name for the JOB_ID field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.JOB_ID constant
     */
    public static String getTurbineScheduledJob_JobId()
    {
        return "TURBINE_SCHEDULED_JOB.JOB_ID";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.SECOND
     * @return the column name for the SECOND field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.SECOND constant
     */
    public static String getTurbineScheduledJob_Second()
    {
        return "TURBINE_SCHEDULED_JOB.SECOND";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.MINUTE
     * @return the column name for the MINUTE field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.MINUTE constant
     */
    public static String getTurbineScheduledJob_Minute()
    {
        return "TURBINE_SCHEDULED_JOB.MINUTE";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.HOUR
     * @return the column name for the HOUR field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.HOUR constant
     */
    public static String getTurbineScheduledJob_Hour()
    {
        return "TURBINE_SCHEDULED_JOB.HOUR";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.WEEK_DAY
     * @return the column name for the WEEK_DAY field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.WEEK_DAY constant
     */
    public static String getTurbineScheduledJob_WeekDay()
    {
        return "TURBINE_SCHEDULED_JOB.WEEK_DAY";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.DAY_OF_MONTH
     * @return the column name for the DAY_OF_MONTH field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.DAY_OF_MONTH constant
     */
    public static String getTurbineScheduledJob_DayOfMonth()
    {
        return "TURBINE_SCHEDULED_JOB.DAY_OF_MONTH";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.TASK
     * @return the column name for the TASK field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.TASK constant
     */
    public static String getTurbineScheduledJob_Task()
    {
        return "TURBINE_SCHEDULED_JOB.TASK";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.EMAIL constant
     */
    public static String getTurbineScheduledJob_Email()
    {
        return "TURBINE_SCHEDULED_JOB.EMAIL";
    }
  
    /**
     * TURBINE_SCHEDULED_JOB.PROPERTY
     * @return the column name for the PROPERTY field
     * @deprecated use TurbineScheduledJobPeer.TURBINE_SCHEDULED_JOB.PROPERTY constant
     */
    public static String getTurbineScheduledJob_Property()
    {
        return "TURBINE_SCHEDULED_JOB.PROPERTY";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("TURBINE_SCHEDULED_JOB");
        TableMap tMap = dbMap.getTable("TURBINE_SCHEDULED_JOB");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("TURBINE_SCHEDULED_JOB_SEQ");

                      tMap.addPrimaryKey("TURBINE_SCHEDULED_JOB.JOB_ID", Integer.valueOf(0));
                            tMap.addColumn("TURBINE_SCHEDULED_JOB.SECOND", Integer.valueOf(0));
                            tMap.addColumn("TURBINE_SCHEDULED_JOB.MINUTE", Integer.valueOf(0));
                            tMap.addColumn("TURBINE_SCHEDULED_JOB.HOUR", Integer.valueOf(0));
                            tMap.addColumn("TURBINE_SCHEDULED_JOB.WEEK_DAY", Integer.valueOf(0));
                            tMap.addColumn("TURBINE_SCHEDULED_JOB.DAY_OF_MONTH", Integer.valueOf(0));
                          tMap.addColumn("TURBINE_SCHEDULED_JOB.TASK", "");
                          tMap.addColumn("TURBINE_SCHEDULED_JOB.EMAIL", "");
                          tMap.addColumn("TURBINE_SCHEDULED_JOB.PROPERTY", new Object());
          }
}
