package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TurbineUserMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TurbineUserMapBuilder";

    /**
     * Item
     * @deprecated use TurbineUserPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "TURBINE_USER";
    }

  
    /**
     * TURBINE_USER.USER_ID
     * @return the column name for the USER_ID field
     * @deprecated use TurbineUserPeer.TURBINE_USER.USER_ID constant
     */
    public static String getTurbineUser_UserId()
    {
        return "TURBINE_USER.USER_ID";
    }
  
    /**
     * TURBINE_USER.LOGIN_NAME
     * @return the column name for the LOGIN_NAME field
     * @deprecated use TurbineUserPeer.TURBINE_USER.LOGIN_NAME constant
     */
    public static String getTurbineUser_LoginName()
    {
        return "TURBINE_USER.LOGIN_NAME";
    }
  
    /**
     * TURBINE_USER.PASSWORD_VALUE
     * @return the column name for the PASSWORD_VALUE field
     * @deprecated use TurbineUserPeer.TURBINE_USER.PASSWORD_VALUE constant
     */
    public static String getTurbineUser_PasswordValue()
    {
        return "TURBINE_USER.PASSWORD_VALUE";
    }
  
    /**
     * TURBINE_USER.FIRST_NAME
     * @return the column name for the FIRST_NAME field
     * @deprecated use TurbineUserPeer.TURBINE_USER.FIRST_NAME constant
     */
    public static String getTurbineUser_FirstName()
    {
        return "TURBINE_USER.FIRST_NAME";
    }
  
    /**
     * TURBINE_USER.LAST_NAME
     * @return the column name for the LAST_NAME field
     * @deprecated use TurbineUserPeer.TURBINE_USER.LAST_NAME constant
     */
    public static String getTurbineUser_LastName()
    {
        return "TURBINE_USER.LAST_NAME";
    }
  
    /**
     * TURBINE_USER.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use TurbineUserPeer.TURBINE_USER.EMAIL constant
     */
    public static String getTurbineUser_Email()
    {
        return "TURBINE_USER.EMAIL";
    }
  
    /**
     * TURBINE_USER.CONFIRM_VALUE
     * @return the column name for the CONFIRM_VALUE field
     * @deprecated use TurbineUserPeer.TURBINE_USER.CONFIRM_VALUE constant
     */
    public static String getTurbineUser_ConfirmValue()
    {
        return "TURBINE_USER.CONFIRM_VALUE";
    }
  
    /**
     * TURBINE_USER.MODIFIED
     * @return the column name for the MODIFIED field
     * @deprecated use TurbineUserPeer.TURBINE_USER.MODIFIED constant
     */
    public static String getTurbineUser_Modified()
    {
        return "TURBINE_USER.MODIFIED";
    }
  
    /**
     * TURBINE_USER.CREATED
     * @return the column name for the CREATED field
     * @deprecated use TurbineUserPeer.TURBINE_USER.CREATED constant
     */
    public static String getTurbineUser_Created()
    {
        return "TURBINE_USER.CREATED";
    }
  
    /**
     * TURBINE_USER.LAST_LOGIN
     * @return the column name for the LAST_LOGIN field
     * @deprecated use TurbineUserPeer.TURBINE_USER.LAST_LOGIN constant
     */
    public static String getTurbineUser_LastLogin()
    {
        return "TURBINE_USER.LAST_LOGIN";
    }
  
    /**
     * TURBINE_USER.OBJECTDATA
     * @return the column name for the OBJECTDATA field
     * @deprecated use TurbineUserPeer.TURBINE_USER.OBJECTDATA constant
     */
    public static String getTurbineUser_Objectdata()
    {
        return "TURBINE_USER.OBJECTDATA";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("TURBINE_USER");
        TableMap tMap = dbMap.getTable("TURBINE_USER");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("TURBINE_USER_SEQ");

                      tMap.addPrimaryKey("TURBINE_USER.USER_ID", Integer.valueOf(0));
                          tMap.addColumn("TURBINE_USER.LOGIN_NAME", "");
                          tMap.addColumn("TURBINE_USER.PASSWORD_VALUE", "");
                          tMap.addColumn("TURBINE_USER.FIRST_NAME", "");
                          tMap.addColumn("TURBINE_USER.LAST_NAME", "");
                          tMap.addColumn("TURBINE_USER.EMAIL", "");
                          tMap.addColumn("TURBINE_USER.CONFIRM_VALUE", "");
                          tMap.addColumn("TURBINE_USER.MODIFIED", new Date());
                          tMap.addColumn("TURBINE_USER.CREATED", new Date());
                          tMap.addColumn("TURBINE_USER.LAST_LOGIN", new Date());
                          tMap.addColumn("TURBINE_USER.OBJECTDATA", new Object());
          }
}
