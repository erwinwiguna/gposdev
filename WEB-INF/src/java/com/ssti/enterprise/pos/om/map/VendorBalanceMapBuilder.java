package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VendorBalanceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VendorBalanceMapBuilder";

    /**
     * Item
     * @deprecated use VendorBalancePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "vendor_balance";
    }

  
    /**
     * vendor_balance.VENDOR_BALANCE_ID
     * @return the column name for the VENDOR_BALANCE_ID field
     * @deprecated use VendorBalancePeer.vendor_balance.VENDOR_BALANCE_ID constant
     */
    public static String getVendorBalance_VendorBalanceId()
    {
        return "vendor_balance.VENDOR_BALANCE_ID";
    }
  
    /**
     * vendor_balance.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use VendorBalancePeer.vendor_balance.VENDOR_ID constant
     */
    public static String getVendorBalance_VendorId()
    {
        return "vendor_balance.VENDOR_ID";
    }
  
    /**
     * vendor_balance.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use VendorBalancePeer.vendor_balance.VENDOR_NAME constant
     */
    public static String getVendorBalance_VendorName()
    {
        return "vendor_balance.VENDOR_NAME";
    }
  
    /**
     * vendor_balance.AP_BALANCE
     * @return the column name for the AP_BALANCE field
     * @deprecated use VendorBalancePeer.vendor_balance.AP_BALANCE constant
     */
    public static String getVendorBalance_ApBalance()
    {
        return "vendor_balance.AP_BALANCE";
    }
  
    /**
     * vendor_balance.PRIME_BALANCE
     * @return the column name for the PRIME_BALANCE field
     * @deprecated use VendorBalancePeer.vendor_balance.PRIME_BALANCE constant
     */
    public static String getVendorBalance_PrimeBalance()
    {
        return "vendor_balance.PRIME_BALANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("vendor_balance");
        TableMap tMap = dbMap.getTable("vendor_balance");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("vendor_balance.VENDOR_BALANCE_ID", "");
                          tMap.addColumn("vendor_balance.VENDOR_ID", "");
                          tMap.addColumn("vendor_balance.VENDOR_NAME", "");
                            tMap.addColumn("vendor_balance.AP_BALANCE", bd_ZERO);
                            tMap.addColumn("vendor_balance.PRIME_BALANCE", bd_ZERO);
          }
}
