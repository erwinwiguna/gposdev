package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VendorMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VendorMapBuilder";

    /**
     * Item
     * @deprecated use VendorPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "vendor";
    }

  
    /**
     * vendor.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use VendorPeer.vendor.VENDOR_ID constant
     */
    public static String getVendor_VendorId()
    {
        return "vendor.VENDOR_ID";
    }
  
    /**
     * vendor.VENDOR_TYPE_ID
     * @return the column name for the VENDOR_TYPE_ID field
     * @deprecated use VendorPeer.vendor.VENDOR_TYPE_ID constant
     */
    public static String getVendor_VendorTypeId()
    {
        return "vendor.VENDOR_TYPE_ID";
    }
  
    /**
     * vendor.VENDOR_CODE
     * @return the column name for the VENDOR_CODE field
     * @deprecated use VendorPeer.vendor.VENDOR_CODE constant
     */
    public static String getVendor_VendorCode()
    {
        return "vendor.VENDOR_CODE";
    }
  
    /**
     * vendor.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use VendorPeer.vendor.VENDOR_NAME constant
     */
    public static String getVendor_VendorName()
    {
        return "vendor.VENDOR_NAME";
    }
  
    /**
     * vendor.TAX_NO
     * @return the column name for the TAX_NO field
     * @deprecated use VendorPeer.vendor.TAX_NO constant
     */
    public static String getVendor_TaxNo()
    {
        return "vendor.TAX_NO";
    }
  
    /**
     * vendor.VENDOR_STATUS
     * @return the column name for the VENDOR_STATUS field
     * @deprecated use VendorPeer.vendor.VENDOR_STATUS constant
     */
    public static String getVendor_VendorStatus()
    {
        return "vendor.VENDOR_STATUS";
    }
  
    /**
     * vendor.CONTACT_NAME1
     * @return the column name for the CONTACT_NAME1 field
     * @deprecated use VendorPeer.vendor.CONTACT_NAME1 constant
     */
    public static String getVendor_ContactName1()
    {
        return "vendor.CONTACT_NAME1";
    }
  
    /**
     * vendor.CONTACT_PHONE1
     * @return the column name for the CONTACT_PHONE1 field
     * @deprecated use VendorPeer.vendor.CONTACT_PHONE1 constant
     */
    public static String getVendor_ContactPhone1()
    {
        return "vendor.CONTACT_PHONE1";
    }
  
    /**
     * vendor.JOB_TITLE1
     * @return the column name for the JOB_TITLE1 field
     * @deprecated use VendorPeer.vendor.JOB_TITLE1 constant
     */
    public static String getVendor_JobTitle1()
    {
        return "vendor.JOB_TITLE1";
    }
  
    /**
     * vendor.CONTACT_NAME2
     * @return the column name for the CONTACT_NAME2 field
     * @deprecated use VendorPeer.vendor.CONTACT_NAME2 constant
     */
    public static String getVendor_ContactName2()
    {
        return "vendor.CONTACT_NAME2";
    }
  
    /**
     * vendor.CONTACT_PHONE2
     * @return the column name for the CONTACT_PHONE2 field
     * @deprecated use VendorPeer.vendor.CONTACT_PHONE2 constant
     */
    public static String getVendor_ContactPhone2()
    {
        return "vendor.CONTACT_PHONE2";
    }
  
    /**
     * vendor.JOB_TITLE2
     * @return the column name for the JOB_TITLE2 field
     * @deprecated use VendorPeer.vendor.JOB_TITLE2 constant
     */
    public static String getVendor_JobTitle2()
    {
        return "vendor.JOB_TITLE2";
    }
  
    /**
     * vendor.CONTACT_NAME3
     * @return the column name for the CONTACT_NAME3 field
     * @deprecated use VendorPeer.vendor.CONTACT_NAME3 constant
     */
    public static String getVendor_ContactName3()
    {
        return "vendor.CONTACT_NAME3";
    }
  
    /**
     * vendor.CONTACT_PHONE3
     * @return the column name for the CONTACT_PHONE3 field
     * @deprecated use VendorPeer.vendor.CONTACT_PHONE3 constant
     */
    public static String getVendor_ContactPhone3()
    {
        return "vendor.CONTACT_PHONE3";
    }
  
    /**
     * vendor.JOB_TITLE3
     * @return the column name for the JOB_TITLE3 field
     * @deprecated use VendorPeer.vendor.JOB_TITLE3 constant
     */
    public static String getVendor_JobTitle3()
    {
        return "vendor.JOB_TITLE3";
    }
  
    /**
     * vendor.ADDRESS
     * @return the column name for the ADDRESS field
     * @deprecated use VendorPeer.vendor.ADDRESS constant
     */
    public static String getVendor_Address()
    {
        return "vendor.ADDRESS";
    }
  
    /**
     * vendor.ZIP
     * @return the column name for the ZIP field
     * @deprecated use VendorPeer.vendor.ZIP constant
     */
    public static String getVendor_Zip()
    {
        return "vendor.ZIP";
    }
  
    /**
     * vendor.COUNTRY
     * @return the column name for the COUNTRY field
     * @deprecated use VendorPeer.vendor.COUNTRY constant
     */
    public static String getVendor_Country()
    {
        return "vendor.COUNTRY";
    }
  
    /**
     * vendor.PROVINCE
     * @return the column name for the PROVINCE field
     * @deprecated use VendorPeer.vendor.PROVINCE constant
     */
    public static String getVendor_Province()
    {
        return "vendor.PROVINCE";
    }
  
    /**
     * vendor.CITY
     * @return the column name for the CITY field
     * @deprecated use VendorPeer.vendor.CITY constant
     */
    public static String getVendor_City()
    {
        return "vendor.CITY";
    }
  
    /**
     * vendor.DISTRICT
     * @return the column name for the DISTRICT field
     * @deprecated use VendorPeer.vendor.DISTRICT constant
     */
    public static String getVendor_District()
    {
        return "vendor.DISTRICT";
    }
  
    /**
     * vendor.VILLAGE
     * @return the column name for the VILLAGE field
     * @deprecated use VendorPeer.vendor.VILLAGE constant
     */
    public static String getVendor_Village()
    {
        return "vendor.VILLAGE";
    }
  
    /**
     * vendor.PHONE1
     * @return the column name for the PHONE1 field
     * @deprecated use VendorPeer.vendor.PHONE1 constant
     */
    public static String getVendor_Phone1()
    {
        return "vendor.PHONE1";
    }
  
    /**
     * vendor.PHONE2
     * @return the column name for the PHONE2 field
     * @deprecated use VendorPeer.vendor.PHONE2 constant
     */
    public static String getVendor_Phone2()
    {
        return "vendor.PHONE2";
    }
  
    /**
     * vendor.FAX
     * @return the column name for the FAX field
     * @deprecated use VendorPeer.vendor.FAX constant
     */
    public static String getVendor_Fax()
    {
        return "vendor.FAX";
    }
  
    /**
     * vendor.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use VendorPeer.vendor.EMAIL constant
     */
    public static String getVendor_Email()
    {
        return "vendor.EMAIL";
    }
  
    /**
     * vendor.WEB_SITE
     * @return the column name for the WEB_SITE field
     * @deprecated use VendorPeer.vendor.WEB_SITE constant
     */
    public static String getVendor_WebSite()
    {
        return "vendor.WEB_SITE";
    }
  
    /**
     * vendor.DEFAULT_TAX_ID
     * @return the column name for the DEFAULT_TAX_ID field
     * @deprecated use VendorPeer.vendor.DEFAULT_TAX_ID constant
     */
    public static String getVendor_DefaultTaxId()
    {
        return "vendor.DEFAULT_TAX_ID";
    }
  
    /**
     * vendor.DEFAULT_TYPE_ID
     * @return the column name for the DEFAULT_TYPE_ID field
     * @deprecated use VendorPeer.vendor.DEFAULT_TYPE_ID constant
     */
    public static String getVendor_DefaultTypeId()
    {
        return "vendor.DEFAULT_TYPE_ID";
    }
  
    /**
     * vendor.DEFAULT_TERM_ID
     * @return the column name for the DEFAULT_TERM_ID field
     * @deprecated use VendorPeer.vendor.DEFAULT_TERM_ID constant
     */
    public static String getVendor_DefaultTermId()
    {
        return "vendor.DEFAULT_TERM_ID";
    }
  
    /**
     * vendor.DEFAULT_EMPLOYEE_ID
     * @return the column name for the DEFAULT_EMPLOYEE_ID field
     * @deprecated use VendorPeer.vendor.DEFAULT_EMPLOYEE_ID constant
     */
    public static String getVendor_DefaultEmployeeId()
    {
        return "vendor.DEFAULT_EMPLOYEE_ID";
    }
  
    /**
     * vendor.DEFAULT_CURRENCY_ID
     * @return the column name for the DEFAULT_CURRENCY_ID field
     * @deprecated use VendorPeer.vendor.DEFAULT_CURRENCY_ID constant
     */
    public static String getVendor_DefaultCurrencyId()
    {
        return "vendor.DEFAULT_CURRENCY_ID";
    }
  
    /**
     * vendor.DEFAULT_LOCATION_ID
     * @return the column name for the DEFAULT_LOCATION_ID field
     * @deprecated use VendorPeer.vendor.DEFAULT_LOCATION_ID constant
     */
    public static String getVendor_DefaultLocationId()
    {
        return "vendor.DEFAULT_LOCATION_ID";
    }
  
    /**
     * vendor.DEFAULT_ITEM_DISC
     * @return the column name for the DEFAULT_ITEM_DISC field
     * @deprecated use VendorPeer.vendor.DEFAULT_ITEM_DISC constant
     */
    public static String getVendor_DefaultItemDisc()
    {
        return "vendor.DEFAULT_ITEM_DISC";
    }
  
    /**
     * vendor.DEFAULT_DISCOUNT_AMOUNT
     * @return the column name for the DEFAULT_DISCOUNT_AMOUNT field
     * @deprecated use VendorPeer.vendor.DEFAULT_DISCOUNT_AMOUNT constant
     */
    public static String getVendor_DefaultDiscountAmount()
    {
        return "vendor.DEFAULT_DISCOUNT_AMOUNT";
    }
  
    /**
     * vendor.CREDIT_LIMIT
     * @return the column name for the CREDIT_LIMIT field
     * @deprecated use VendorPeer.vendor.CREDIT_LIMIT constant
     */
    public static String getVendor_CreditLimit()
    {
        return "vendor.CREDIT_LIMIT";
    }
  
    /**
     * vendor.MEMO
     * @return the column name for the MEMO field
     * @deprecated use VendorPeer.vendor.MEMO constant
     */
    public static String getVendor_Memo()
    {
        return "vendor.MEMO";
    }
  
    /**
     * vendor.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use VendorPeer.vendor.UPDATE_DATE constant
     */
    public static String getVendor_UpdateDate()
    {
        return "vendor.UPDATE_DATE";
    }
  
    /**
     * vendor.LAST_UPDATE_LOCATION_ID
     * @return the column name for the LAST_UPDATE_LOCATION_ID field
     * @deprecated use VendorPeer.vendor.LAST_UPDATE_LOCATION_ID constant
     */
    public static String getVendor_LastUpdateLocationId()
    {
        return "vendor.LAST_UPDATE_LOCATION_ID";
    }
  
    /**
     * vendor.FIELD1
     * @return the column name for the FIELD1 field
     * @deprecated use VendorPeer.vendor.FIELD1 constant
     */
    public static String getVendor_Field1()
    {
        return "vendor.FIELD1";
    }
  
    /**
     * vendor.FIELD2
     * @return the column name for the FIELD2 field
     * @deprecated use VendorPeer.vendor.FIELD2 constant
     */
    public static String getVendor_Field2()
    {
        return "vendor.FIELD2";
    }
  
    /**
     * vendor.VALUE1
     * @return the column name for the VALUE1 field
     * @deprecated use VendorPeer.vendor.VALUE1 constant
     */
    public static String getVendor_Value1()
    {
        return "vendor.VALUE1";
    }
  
    /**
     * vendor.VALUE2
     * @return the column name for the VALUE2 field
     * @deprecated use VendorPeer.vendor.VALUE2 constant
     */
    public static String getVendor_Value2()
    {
        return "vendor.VALUE2";
    }
  
    /**
     * vendor.AP_ACCOUNT
     * @return the column name for the AP_ACCOUNT field
     * @deprecated use VendorPeer.vendor.AP_ACCOUNT constant
     */
    public static String getVendor_ApAccount()
    {
        return "vendor.AP_ACCOUNT";
    }
  
    /**
     * vendor.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use VendorPeer.vendor.OPENING_BALANCE constant
     */
    public static String getVendor_OpeningBalance()
    {
        return "vendor.OPENING_BALANCE";
    }
  
    /**
     * vendor.AS_DATE
     * @return the column name for the AS_DATE field
     * @deprecated use VendorPeer.vendor.AS_DATE constant
     */
    public static String getVendor_AsDate()
    {
        return "vendor.AS_DATE";
    }
  
    /**
     * vendor.OB_TRANS_ID
     * @return the column name for the OB_TRANS_ID field
     * @deprecated use VendorPeer.vendor.OB_TRANS_ID constant
     */
    public static String getVendor_ObTransId()
    {
        return "vendor.OB_TRANS_ID";
    }
  
    /**
     * vendor.OB_RATE
     * @return the column name for the OB_RATE field
     * @deprecated use VendorPeer.vendor.OB_RATE constant
     */
    public static String getVendor_ObRate()
    {
        return "vendor.OB_RATE";
    }
  
    /**
     * vendor.SALES_AREA_ID
     * @return the column name for the SALES_AREA_ID field
     * @deprecated use VendorPeer.vendor.SALES_AREA_ID constant
     */
    public static String getVendor_SalesAreaId()
    {
        return "vendor.SALES_AREA_ID";
    }
  
    /**
     * vendor.TERRITORY_ID
     * @return the column name for the TERRITORY_ID field
     * @deprecated use VendorPeer.vendor.TERRITORY_ID constant
     */
    public static String getVendor_TerritoryId()
    {
        return "vendor.TERRITORY_ID";
    }
  
    /**
     * vendor.LONGITUDES
     * @return the column name for the LONGITUDES field
     * @deprecated use VendorPeer.vendor.LONGITUDES constant
     */
    public static String getVendor_Longitudes()
    {
        return "vendor.LONGITUDES";
    }
  
    /**
     * vendor.LATITUDES
     * @return the column name for the LATITUDES field
     * @deprecated use VendorPeer.vendor.LATITUDES constant
     */
    public static String getVendor_Latitudes()
    {
        return "vendor.LATITUDES";
    }
  
    /**
     * vendor.TRANS_PREFIX
     * @return the column name for the TRANS_PREFIX field
     * @deprecated use VendorPeer.vendor.TRANS_PREFIX constant
     */
    public static String getVendor_TransPrefix()
    {
        return "vendor.TRANS_PREFIX";
    }
  
    /**
     * vendor.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use VendorPeer.vendor.CREATE_BY constant
     */
    public static String getVendor_CreateBy()
    {
        return "vendor.CREATE_BY";
    }
  
    /**
     * vendor.LAST_UPDATE_BY
     * @return the column name for the LAST_UPDATE_BY field
     * @deprecated use VendorPeer.vendor.LAST_UPDATE_BY constant
     */
    public static String getVendor_LastUpdateBy()
    {
        return "vendor.LAST_UPDATE_BY";
    }
  
    /**
     * vendor.ALLOW_RETURN
     * @return the column name for the ALLOW_RETURN field
     * @deprecated use VendorPeer.vendor.ALLOW_RETURN constant
     */
    public static String getVendor_AllowReturn()
    {
        return "vendor.ALLOW_RETURN";
    }
  
    /**
     * vendor.ITEM_TYPE
     * @return the column name for the ITEM_TYPE field
     * @deprecated use VendorPeer.vendor.ITEM_TYPE constant
     */
    public static String getVendor_ItemType()
    {
        return "vendor.ITEM_TYPE";
    }
  
    /**
     * vendor.CONSIGN_DISC
     * @return the column name for the CONSIGN_DISC field
     * @deprecated use VendorPeer.vendor.CONSIGN_DISC constant
     */
    public static String getVendor_ConsignDisc()
    {
        return "vendor.CONSIGN_DISC";
    }
  
    /**
     * vendor.CONSIGN_FIELD
     * @return the column name for the CONSIGN_FIELD field
     * @deprecated use VendorPeer.vendor.CONSIGN_FIELD constant
     */
    public static String getVendor_ConsignField()
    {
        return "vendor.CONSIGN_FIELD";
    }
  
    /**
     * vendor.PAYMENT_ACC_BANK
     * @return the column name for the PAYMENT_ACC_BANK field
     * @deprecated use VendorPeer.vendor.PAYMENT_ACC_BANK constant
     */
    public static String getVendor_PaymentAccBank()
    {
        return "vendor.PAYMENT_ACC_BANK";
    }
  
    /**
     * vendor.PAYMENT_ACC_NO
     * @return the column name for the PAYMENT_ACC_NO field
     * @deprecated use VendorPeer.vendor.PAYMENT_ACC_NO constant
     */
    public static String getVendor_PaymentAccNo()
    {
        return "vendor.PAYMENT_ACC_NO";
    }
  
    /**
     * vendor.PAYMENT_ACC_NAME
     * @return the column name for the PAYMENT_ACC_NAME field
     * @deprecated use VendorPeer.vendor.PAYMENT_ACC_NAME constant
     */
    public static String getVendor_PaymentAccName()
    {
        return "vendor.PAYMENT_ACC_NAME";
    }
  
    /**
     * vendor.PAYMENT_EMAIL_TO
     * @return the column name for the PAYMENT_EMAIL_TO field
     * @deprecated use VendorPeer.vendor.PAYMENT_EMAIL_TO constant
     */
    public static String getVendor_PaymentEmailTo()
    {
        return "vendor.PAYMENT_EMAIL_TO";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("vendor");
        TableMap tMap = dbMap.getTable("vendor");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("vendor.VENDOR_ID", "");
                          tMap.addColumn("vendor.VENDOR_TYPE_ID", "");
                          tMap.addColumn("vendor.VENDOR_CODE", "");
                          tMap.addColumn("vendor.VENDOR_NAME", "");
                          tMap.addColumn("vendor.TAX_NO", "");
                            tMap.addColumn("vendor.VENDOR_STATUS", Integer.valueOf(0));
                          tMap.addColumn("vendor.CONTACT_NAME1", "");
                          tMap.addColumn("vendor.CONTACT_PHONE1", "");
                          tMap.addColumn("vendor.JOB_TITLE1", "");
                          tMap.addColumn("vendor.CONTACT_NAME2", "");
                          tMap.addColumn("vendor.CONTACT_PHONE2", "");
                          tMap.addColumn("vendor.JOB_TITLE2", "");
                          tMap.addColumn("vendor.CONTACT_NAME3", "");
                          tMap.addColumn("vendor.CONTACT_PHONE3", "");
                          tMap.addColumn("vendor.JOB_TITLE3", "");
                          tMap.addColumn("vendor.ADDRESS", "");
                          tMap.addColumn("vendor.ZIP", "");
                          tMap.addColumn("vendor.COUNTRY", "");
                          tMap.addColumn("vendor.PROVINCE", "");
                          tMap.addColumn("vendor.CITY", "");
                          tMap.addColumn("vendor.DISTRICT", "");
                          tMap.addColumn("vendor.VILLAGE", "");
                          tMap.addColumn("vendor.PHONE1", "");
                          tMap.addColumn("vendor.PHONE2", "");
                          tMap.addColumn("vendor.FAX", "");
                          tMap.addColumn("vendor.EMAIL", "");
                          tMap.addColumn("vendor.WEB_SITE", "");
                          tMap.addColumn("vendor.DEFAULT_TAX_ID", "");
                          tMap.addColumn("vendor.DEFAULT_TYPE_ID", "");
                          tMap.addColumn("vendor.DEFAULT_TERM_ID", "");
                          tMap.addColumn("vendor.DEFAULT_EMPLOYEE_ID", "");
                          tMap.addColumn("vendor.DEFAULT_CURRENCY_ID", "");
                          tMap.addColumn("vendor.DEFAULT_LOCATION_ID", "");
                          tMap.addColumn("vendor.DEFAULT_ITEM_DISC", "");
                          tMap.addColumn("vendor.DEFAULT_DISCOUNT_AMOUNT", "");
                            tMap.addColumn("vendor.CREDIT_LIMIT", bd_ZERO);
                          tMap.addColumn("vendor.MEMO", "");
                          tMap.addColumn("vendor.UPDATE_DATE", new Date());
                          tMap.addColumn("vendor.LAST_UPDATE_LOCATION_ID", "");
                          tMap.addColumn("vendor.FIELD1", "");
                          tMap.addColumn("vendor.FIELD2", "");
                          tMap.addColumn("vendor.VALUE1", "");
                          tMap.addColumn("vendor.VALUE2", "");
                          tMap.addColumn("vendor.AP_ACCOUNT", "");
                            tMap.addColumn("vendor.OPENING_BALANCE", bd_ZERO);
                          tMap.addColumn("vendor.AS_DATE", new Date());
                          tMap.addColumn("vendor.OB_TRANS_ID", "");
                            tMap.addColumn("vendor.OB_RATE", bd_ZERO);
                          tMap.addColumn("vendor.SALES_AREA_ID", "");
                          tMap.addColumn("vendor.TERRITORY_ID", "");
                            tMap.addColumn("vendor.LONGITUDES", bd_ZERO);
                            tMap.addColumn("vendor.LATITUDES", bd_ZERO);
                          tMap.addColumn("vendor.TRANS_PREFIX", "");
                          tMap.addColumn("vendor.CREATE_BY", "");
                          tMap.addColumn("vendor.LAST_UPDATE_BY", "");
                            tMap.addColumn("vendor.ALLOW_RETURN", Integer.valueOf(0));
                            tMap.addColumn("vendor.ITEM_TYPE", Integer.valueOf(0));
                          tMap.addColumn("vendor.CONSIGN_DISC", "");
                          tMap.addColumn("vendor.CONSIGN_FIELD", "");
                          tMap.addColumn("vendor.PAYMENT_ACC_BANK", "");
                          tMap.addColumn("vendor.PAYMENT_ACC_NO", "");
                          tMap.addColumn("vendor.PAYMENT_ACC_NAME", "");
                          tMap.addColumn("vendor.PAYMENT_EMAIL_TO", "");
          }
}
