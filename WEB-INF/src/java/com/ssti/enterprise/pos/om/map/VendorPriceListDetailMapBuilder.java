package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VendorPriceListDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VendorPriceListDetailMapBuilder";

    /**
     * Item
     * @deprecated use VendorPriceListDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "vendor_price_list_detail";
    }

  
    /**
     * vendor_price_list_detail.VENDOR_PRICE_LIST_DETAIL_ID
     * @return the column name for the VENDOR_PRICE_LIST_DETAIL_ID field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.VENDOR_PRICE_LIST_DETAIL_ID constant
     */
    public static String getVendorPriceListDetail_VendorPriceListDetailId()
    {
        return "vendor_price_list_detail.VENDOR_PRICE_LIST_DETAIL_ID";
    }
  
    /**
     * vendor_price_list_detail.VENDOR_PRICE_LIST_ID
     * @return the column name for the VENDOR_PRICE_LIST_ID field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.VENDOR_PRICE_LIST_ID constant
     */
    public static String getVendorPriceListDetail_VendorPriceListId()
    {
        return "vendor_price_list_detail.VENDOR_PRICE_LIST_ID";
    }
  
    /**
     * vendor_price_list_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.ITEM_ID constant
     */
    public static String getVendorPriceListDetail_ItemId()
    {
        return "vendor_price_list_detail.ITEM_ID";
    }
  
    /**
     * vendor_price_list_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.ITEM_CODE constant
     */
    public static String getVendorPriceListDetail_ItemCode()
    {
        return "vendor_price_list_detail.ITEM_CODE";
    }
  
    /**
     * vendor_price_list_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.ITEM_NAME constant
     */
    public static String getVendorPriceListDetail_ItemName()
    {
        return "vendor_price_list_detail.ITEM_NAME";
    }
  
    /**
     * vendor_price_list_detail.PURCHASE_PRICE
     * @return the column name for the PURCHASE_PRICE field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.PURCHASE_PRICE constant
     */
    public static String getVendorPriceListDetail_PurchasePrice()
    {
        return "vendor_price_list_detail.PURCHASE_PRICE";
    }
  
    /**
     * vendor_price_list_detail.DISCOUNT_AMOUNT
     * @return the column name for the DISCOUNT_AMOUNT field
     * @deprecated use VendorPriceListDetailPeer.vendor_price_list_detail.DISCOUNT_AMOUNT constant
     */
    public static String getVendorPriceListDetail_DiscountAmount()
    {
        return "vendor_price_list_detail.DISCOUNT_AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("vendor_price_list_detail");
        TableMap tMap = dbMap.getTable("vendor_price_list_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("vendor_price_list_detail.VENDOR_PRICE_LIST_DETAIL_ID", "");
                          tMap.addForeignKey(
                "vendor_price_list_detail.VENDOR_PRICE_LIST_ID", "" , "vendor_price_list" ,
                "vendor_price_list_id");
                          tMap.addColumn("vendor_price_list_detail.ITEM_ID", "");
                          tMap.addColumn("vendor_price_list_detail.ITEM_CODE", "");
                          tMap.addColumn("vendor_price_list_detail.ITEM_NAME", "");
                            tMap.addColumn("vendor_price_list_detail.PURCHASE_PRICE", bd_ZERO);
                          tMap.addColumn("vendor_price_list_detail.DISCOUNT_AMOUNT", "");
          }
}
