package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VendorTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VendorTypeMapBuilder";

    /**
     * Item
     * @deprecated use VendorTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "vendor_type";
    }

  
    /**
     * vendor_type.VENDOR_TYPE_ID
     * @return the column name for the VENDOR_TYPE_ID field
     * @deprecated use VendorTypePeer.vendor_type.VENDOR_TYPE_ID constant
     */
    public static String getVendorType_VendorTypeId()
    {
        return "vendor_type.VENDOR_TYPE_ID";
    }
  
    /**
     * vendor_type.VENDOR_TYPE_CODE
     * @return the column name for the VENDOR_TYPE_CODE field
     * @deprecated use VendorTypePeer.vendor_type.VENDOR_TYPE_CODE constant
     */
    public static String getVendorType_VendorTypeCode()
    {
        return "vendor_type.VENDOR_TYPE_CODE";
    }
  
    /**
     * vendor_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use VendorTypePeer.vendor_type.DESCRIPTION constant
     */
    public static String getVendorType_Description()
    {
        return "vendor_type.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("vendor_type");
        TableMap tMap = dbMap.getTable("vendor_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("vendor_type.VENDOR_TYPE_ID", "");
                          tMap.addColumn("vendor_type.VENDOR_TYPE_CODE", "");
                          tMap.addColumn("vendor_type.DESCRIPTION", "");
          }
}
