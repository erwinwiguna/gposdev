package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VoucherMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VoucherMapBuilder";

    /**
     * Item
     * @deprecated use VoucherPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "voucher";
    }

  
    /**
     * voucher.VOUCHER_ID
     * @return the column name for the VOUCHER_ID field
     * @deprecated use VoucherPeer.voucher.VOUCHER_ID constant
     */
    public static String getVoucher_VoucherId()
    {
        return "voucher.VOUCHER_ID";
    }
  
    /**
     * voucher.VOUCHER_CODE
     * @return the column name for the VOUCHER_CODE field
     * @deprecated use VoucherPeer.voucher.VOUCHER_CODE constant
     */
    public static String getVoucher_VoucherCode()
    {
        return "voucher.VOUCHER_CODE";
    }
  
    /**
     * voucher.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use VoucherPeer.voucher.DESCRIPTION constant
     */
    public static String getVoucher_Description()
    {
        return "voucher.DESCRIPTION";
    }
  
    /**
     * voucher.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use VoucherPeer.voucher.AMOUNT constant
     */
    public static String getVoucher_Amount()
    {
        return "voucher.AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("voucher");
        TableMap tMap = dbMap.getTable("voucher");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("voucher.VOUCHER_ID", "");
                          tMap.addColumn("voucher.VOUCHER_CODE", "");
                          tMap.addColumn("voucher.DESCRIPTION", "");
                            tMap.addColumn("voucher.AMOUNT", Integer.valueOf(0));
          }
}
