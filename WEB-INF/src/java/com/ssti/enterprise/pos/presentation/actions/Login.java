package com.ssti.enterprise.pos.presentation.actions;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.om.security.User;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.TurbineSecurityException;

import com.ssti.enterprise.pos.om.CompanyData;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.license.License;
import com.ssti.framework.license.LicenseInvalidException;
import com.ssti.framework.mvc.session.SessionHandler;
import com.ssti.framework.om.ActiveUser;
import com.ssti.framework.om.ActiveUserPeer;
import com.ssti.framework.presentation.BaseLogin;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Handle customer login
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 *  * 2017-05-27
 * - Change doPerform add checking when compName / locName is empty
 *   probably license fraud. see {@BaseLogin} {@BaseAction}

 * </pre><br>
 */
public class Login extends BaseLogin
{
	private static Log log = LogFactory.getLog(Login.class);
	protected boolean loginValid = false;
	public static final String SESSION_KEY = "License";
	static final String PRODUCT_NAME = "Retailsoft Platinum";
	
	Configuration conf = Attributes.CONFIG;
	protected String username = "";
	protected String password = "";
	
	protected String comName = "";
	protected String locName = "";
	protected List locations = null;
	
	boolean licValid = false;
	int iLoginUser = 0;
	int iAllowed = 3;
		
	protected void doLogin(RunData data) throws Exception, TurbineSecurityException
	{
		Logout.logout(data);		
		
		try
		{
			iLoginUser = SessionHandler.countOtherUser(username);			

			if (StringUtil.isNotEmpty(comName) || (locations != null && locations.size() > 0))
			{
				log.debug(comName + " , " + locName);

				//validate license here 
				licValid = true; // check from hub is payment valid 
				iAllowed = 3; 	 // check number of allowed user in hub

				if(!licValid)
				{
					String sMsg = "License is Not Valid / Expired";
					throw new LicenseInvalidException(sMsg);							
					
				}
				if (iAllowed <= iLoginUser)
				{
					String sMsg = "Maximum Login " + iAllowed + " Users, Please Obtain Additional User License / Logout First";
					throw new LicenseInvalidException(sMsg);							
				}
				
				License oLicense = new License(PRODUCT_NAME, comName, locName);
				log.debug("license valid");
				data.getSession().setAttribute("License", oLicense);
			}
			
			licValid = true;
		}
		catch (Exception _oEx)
		{
			if (StringUtil.contains(_oEx.getMessage(), "serialVersionUID"))
			{
				data.setMessage("Incompatible License File");
			}
			else
			{
				data.setMessage(_oEx.getMessage());
			}
			data.setUser(TurbineSecurity.getAnonymousUser());
			return;
		}

		if (licValid)
		{
			try
			{
				User user = TurbineSecurity.getAuthenticatedUser(username, password);

				//validate if this user is already logged in
				if (user != null) validateLoginUser(data, user);
	            				
				data.setUser(user);
				user.setHasLoggedIn(Boolean.TRUE);
				user.updateLastLogin();
				data.save();

				loginValid = true;
			}
			catch (Exception _oEx)
			{
				log.error(_oEx);

				data.setMessage(conf.getString("login.error", ""));
				data.setUser(TurbineSecurity.getAnonymousUser());
				String loginTemplate = conf.getString("template.login");

				if (StringUtils.isNotEmpty(loginTemplate))
				{
					data.setScreenTemplate(loginTemplate);
				}
				else
				{
					data.setScreen(conf.getString("screen.login"));
				}
			}	
		}
	}

	private void validateLoginUser(RunData data, User user)  throws Exception
	{
		try 
		{
			String sUser = user.getName();	
			Criteria oCrit = new Criteria();
			oCrit.add(ActiveUserPeer.USER_NAME, sUser);
			List vUser = ActiveUserPeer.doSelect(oCrit);

			//there is active user login
			if (vUser.size() > 0)
			{

				ActiveUser oUser = (ActiveUser) vUser.get(0);
				log.info("Old ActiverUser Login Found: " + oUser);
				SessionHandler.deleteActiveUser(user.getName(), "");
				SessionHandler.invalidateSession(oUser.getSessionId());
			}

			ActiveUser oUser = new ActiveUser();
			oUser.setUserName(sUser);
			oUser.setSessionId(data.getSession().getId());
			oUser.setIpAddress(data.getRemoteAddr());
			oUser.save();

			log.debug("New ActiverUser: " + oUser);			
		} 
		catch (Exception e) 
		{
			log.error(e.getMessage(), e);
			e.printStackTrace();	
		}	
	}	
	
	public void doPerform(RunData data) throws Exception, TurbineSecurityException
	{
		CompanyData company = PreferenceTool.getCompany();		
		if(company != null)
		{
			comName = company.getCompanyName();
			locName = PreferenceTool.getLocationName();
			locations = LocationTool.getAllLocation();			
		}
		
		username = data.getParameters().getString("username", "");
		password = data.getParameters().getString("password", "");		
		if(StringUtil.isNotEmpty(username))
		{
			doLogin(data);			
			if (loginValid)
			{
				data.getSession().setAttribute("conf", Attributes.CONFIG);
				data.getSession().setAttribute("userPref", PreferenceTool.getUserPreference(data));
			}
		}
	}
}
