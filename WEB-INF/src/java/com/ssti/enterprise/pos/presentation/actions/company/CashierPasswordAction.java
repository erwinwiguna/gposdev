package com.ssti.enterprise.pos.presentation.actions.company;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Preference;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

public class CashierPasswordAction extends CompanySecureAction
{    
	private static Log log = LogFactory.getLog(CashierPasswordAction.class);
	
	private static final String[] a_PERM = {"View Master Data"};	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }	
	
	public void doSave(RunData data, Context context)
        throws Exception
    {
		try 
		{
			String sPwd = data.getParameters().getString("CashierPassword");
			Preference oPref = PreferenceTool.getPreference();
			if (oPref != null && StringUtil.isNotEmpty(sPwd))
			{
				oPref.setCashierPassword(sPwd);
		        oPref.save();
			}
	        PreferenceTool.refreshPref();
        	data.setMessage (LocaleTool.getString("pref_saved"));
        }
        catch (Exception _oEx) 
        {
        	String 	sError = LocaleTool.getString("pref_save_failed") + _oEx.getMessage();
			log.error ( sError, _oEx);			        	
        	data.setMessage (sError);
        }
    }
}