package com.ssti.enterprise.pos.presentation.actions.periodic;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.DatabaseBackupTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/periodic/DatabaseBackupAction.java,v $
 * Purpose: handle backup / restore db action
 *
 * @author  $Author: albert $
 * @version $Id: DatabaseBackupAction.java,v 1.6 2006/02/04 12:19:21 albert Exp $
 *
 * $Log: DatabaseBackupAction.java,v $
 * Revision 1.6  2006/02/04 12:19:21  albert
 * *** empty log message ***
 *
 * Revision 1.5  2005/08/21 14:26:35  albert
 * *** empty log message ***
 *
 * Revision 1.4  2005/03/28 09:18:21  Albert
 * *** empty log message ***
 *
 * Revision 1.3  2004/11/18 07:18:29  albert
 * *** empty log message ***
 *
 */

public class DatabaseBackupAction extends PeriodicSecureAction
{    
	public void doBackup (RunData data, Context context)
        throws Exception
    {
		try 
		{
			String[] aResult = DatabaseBackupTool.backupDatabase(data);
        	data.setMessage (aResult[0]);
			context.put ("file", aResult[1]);	
		}
        catch (Exception _oEx) 
        {
        	data.setMessage("Backup Data Failed " + _oEx.getMessage());
        }
    }
}