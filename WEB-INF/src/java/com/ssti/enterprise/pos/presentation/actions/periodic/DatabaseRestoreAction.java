package com.ssti.enterprise.pos.presentation.actions.periodic;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.DatabaseBackupTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/periodic/DatabaseRestoreAction.java,v $
 * Purpose: handle backup / restore db action
 *
 * @author  $Author: albert $
 * @version $Id: DatabaseRestoreAction.java,v 1.4 2005/08/21 14:26:35 albert Exp $
 *
 * $Log: DatabaseRestoreAction.java,v $
 * Revision 1.4  2005/08/21 14:26:35  albert
 * *** empty log message ***
 *
 * Revision 1.3  2005/03/28 09:18:21  Albert
 * *** empty log message ***
 *
 * Revision 1.2  2004/11/29 02:25:04  albert
 * *** empty log message ***
 *
 * Revision 1.1  2004/11/27 09:04:21  albert
 * *** empty log message ***
 */

public class DatabaseRestoreAction extends PeriodicSecureAction
{    
	public void doRestore (RunData data, Context context)
        throws Exception
    {
		try 
		{
			String sResult = DatabaseBackupTool.restoreDatabase(data);
        	data.setMessage (sResult);
		}
        catch (Exception _oEx) 
        {
        	data.setMessage("Data Restore Failed " + _oEx.getMessage());
        	_oEx.printStackTrace();
        }
    }
}