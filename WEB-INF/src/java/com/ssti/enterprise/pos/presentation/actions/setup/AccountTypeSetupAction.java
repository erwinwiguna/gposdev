package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.om.AccountTypePeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.gl.AccountTypeTool;
import com.ssti.framework.tools.SqlUtil;

public class AccountTypeSetupAction extends GLSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			AccountType oAccountType = new AccountType();
	    	data.getParameters().setProperties(oAccountType);			
			oAccountType.setAccountTypeId (IDGenerator.generateSysID());
	        oAccountType.save();
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch(Exception _oEx)
        {
	       	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			AccountType oAccountType = AccountTypeTool.getAccountTypeByID(data.getParameters().getString("ID"));
	    	data.getParameters().setProperties(oAccountType);
	        oAccountType.save();
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("account","account_type_id",sID))
    		{
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(AccountTypePeer.ACCOUNT_TYPE_ID, data.getParameters().getString("ID"));
	        	AccountTypePeer.doDelete(oCrit);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}   
        	else
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }
}