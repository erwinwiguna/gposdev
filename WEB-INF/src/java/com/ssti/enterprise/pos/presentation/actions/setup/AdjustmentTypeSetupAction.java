package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.AdjustmentTypeLoader;
import com.ssti.enterprise.pos.manager.AdjustmentTypeManager;
import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.AdjustmentTypePeer;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AdjustmentTypeSetupAction.java,v 1.11 2008/06/29 07:03:22 albert Exp $ <br>
 *
 * <pre>
 * $Log: AdjustmentTypeSetupAction.java,v $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class AdjustmentTypeSetupAction extends POSSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			AdjustmentType oADJ = new AdjustmentType();
			setProperties (oADJ, data);
			oADJ.setAdjustmentTypeId (IDGenerator.generateSysID());
	        oADJ.save();
            AdjustmentTypeManager.getInstance().refreshCache(oADJ);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch(Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			AdjustmentType oADJ = AdjustmentTypeTool.getAdjustmentTypeByID(data.getParameters().getString("ID"));
			AdjustmentType oOld = oADJ.copy();
			setProperties (oADJ, data);
	        oADJ.save();
            AdjustmentTypeManager.getInstance().refreshCache(oADJ);
            UpdateHistoryTool.createHistory(oOld, oADJ, data.getUser().getName(), null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("issue_receipt","adjustment_type_id",sID))
    		{
    			AdjustmentType oADJ = AdjustmentTypeTool.getAdjustmentTypeByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(AdjustmentTypePeer.ADJUSTMENT_TYPE_ID, sID);
	        	AdjustmentTypePeer.doDelete(oCrit);
                AdjustmentTypeManager.getInstance().refreshCache(sID);	        	
                UpdateHistoryTool.createDelHistory(oADJ, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}   
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    private void setProperties (AdjustmentType _oADJ, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oADJ);
			_oADJ.setDefaultCostAdj(data.getParameters().getBoolean("DefaultCostAdj"));
            _oADJ.setIsTransfer(data.getParameters().getBoolean("IsTransfer"));            
		}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
    
    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
        try 
        {
            FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
            InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            AdjustmentTypeLoader oLoader = new AdjustmentTypeLoader(data.getUser().getName());
            oLoader.loadData (oBufferStream);
            data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));            
            context.put ("LoadResult", oLoader.getResult());
        }
        catch (Exception _oEx) 
        {
            log.error(_oEx);
            _oEx.printStackTrace();
            data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
        }
    }
}