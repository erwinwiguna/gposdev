package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.BankLoader;
import com.ssti.enterprise.pos.manager.BankManager;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankBookPeer;
import com.ssti.enterprise.pos.om.BankPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/setup/BankSetupAction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: BankSetupAction.java,v 1.19 2008/10/22 05:30:31 albert Exp $
 *
 * $Log: BankSetupAction.java,v $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 *
 */
public class BankSetupAction extends SetupSecureAction
{
	private static final Log log = LogFactory.getLog(BankSetupAction.class);
	
	protected static final String[] a_PERM = {"View Bank Setup"};	
	protected static final String[] a_UPDATE_PERM = {"View Bank Setup", "Update Bank Setup"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doFind",null) != null)
        {
            return isAuthorized(data, a_PERM);
        }
        else if(data.getParameters().getString("eventSubmit_doInsert",null)!= null ||
                data.getParameters().getString("eventSubmit_doUpdate",null)!= null ||
                data.getParameters().getString("eventSubmit_doDelete",null)!= null)
        {
        	return isAuthorized(data, a_UPDATE_PERM);
        }	
        return isAuthorized(data, a_PERM);
    }	
	
	public void doInsert(RunData data, Context context)
		throws Exception
    {
		try
		{
		    double dBalance = data.getParameters().getDouble("OpeningBalance", 0);
			Bank oBank = new Bank();
			setProperties (oBank, data);
			oBank.setBankId (IDGenerator.generateSysID());		
			oBank.setOpeningBalance(bd_ZERO); //prevent update OB failure
	        
			//validate currency and account currency
			CurrencyTool.validateCurrency(oBank.getCurrencyId(), oBank.getAccountId());
			oBank.save();
			
	        if (dBalance > 0)
	        {
	        	CashFlowTool.validateDate(oBank.getAsDate(), null);
	        	CashFlowTool.updateOpeningBalance (oBank, data.getUser().getName(),dBalance, false);
	        }
			oBank.setOpeningBalance(new BigDecimal(dBalance));
	        oBank.save();
	        BankManager.getInstance().refreshCache(oBank);
	        data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
		}
        catch (Exception _oEx)
        {
            log.error(_oEx);
        	if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + 
	       	    	LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }	

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Bank oBank = BankTool.getBankByID(data.getParameters().getString("ID"));
			Bank oOld = oBank.copy();
			String sOldCurrID = oBank.getCurrencyId();
			BigDecimal bdOld = oBank.getOpeningBalance();

			//set changed properties
			setProperties (oBank, data);
			oBank.setOpeningBalance(bdOld);
			
			String sNewCurrID = oBank.getCurrencyId();
			if (!sOldCurrID.equals(sNewCurrID)) 
			{   
				//if currency change then check if ob / cf exists
				if (CashFlowTool.isBankUsed(oBank.getBankId()))
				{
					data.setMessage(LocaleTool.getString("curr_maynot_changed"));
					return;
				}
			}						
			//validate currency and account currency
			CurrencyTool.validateCurrency(oBank.getCurrencyId(), oBank.getAccountId());

		    double dBalance = data.getParameters().getDouble("OpeningBalance", 0);
	        double dChange = dBalance - bdOld.doubleValue();
	        if (dChange != 0)
	        {
	        	CashFlowTool.validateDate(oBank.getAsDate(), null);
    	        CashFlowTool.updateOpeningBalance (oBank, data.getUser().getName(), dChange, true);   
    	        oBank.setOpeningBalance(new BigDecimal(dBalance));
	        }	        
	        
	        oBank.save(); //saved only if OB updated successfully
	        BankManager.getInstance().refreshCache(oBank);
	        UpdateHistoryTool.createHistory(oOld, oBank, data.getUser().getName(), null);
	        data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sColumn = "bank_id";
    		String sID = data.getParameters().getString("ID");
    		String sCurrencyID = data.getParameters().getString("CurrencyId");
    			
    		if(SqlUtil.validateFKRef("ar_payment",sColumn,sID)
    			&& SqlUtil.validateFKRef("ap_payment",sColumn,sID)
    			&& SqlUtil.validateFKRef("cash_flow",sColumn,sID)
    			&& SqlUtil.validateFKRef("sales_order",sColumn,sID)
    			&& SqlUtil.validateFKRef("purchase_order",sColumn,sID)
    			&& SqlUtil.validateFKRef("invoice_payment",sColumn,sID)
    		  )
    		{
    			Bank oBank = BankTool.getBankByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(BankPeer.BANK_ID, sID);
        		BankPeer.doDelete(oCrit);
	    		
        		oCrit = new Criteria();
	        	oCrit.add(BankBookPeer.BANK_ID, sID);
        		BankBookPeer.doDelete(oCrit);
        		BankManager.getInstance().refreshCache(sID, sCurrencyID);
    	        UpdateHistoryTool.createDelHistory(oBank, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
            {	 
                data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
            }
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }
 	
    private void setProperties (Bank _oBank, RunData data)
    	throws Exception
    {
		try
		{
		    Date dAsDate = CustomParser.parseDate(data.getParameters().getString("AsDate"));
	    	if (dAsDate == null) dAsDate = new Date();
	    	data.getParameters().setProperties(_oBank);
	        _oBank.setAsDate(dAsDate);
    	}
    	catch (Exception _oEx)
    	{
    		log.error(_oEx);
        	throw new Exception (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage(), _oEx);
    	}
    }    
    
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			BankLoader oLoader = new BankLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}
