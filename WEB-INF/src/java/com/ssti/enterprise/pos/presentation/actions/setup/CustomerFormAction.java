package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.CustomerLoader;
import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.AccountReceivablePeer;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerBalancePeer;
import com.ssti.enterprise.pos.om.CustomerCfield;
import com.ssti.enterprise.pos.om.CustomerField;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.financial.AccountReceivableTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * 
 * 2016-03-01
 * - change doFind add parameter employeeId and statusId criteria due to change in CustomerTool.find
 * 
 * 2015-10-01
 * - change method doInsert, return Customer Data back to form in case of error happened
 * 
 * 2015-09-17
 * -add method saveFields 
 * 
 * </pre><br>
 */
public class CustomerFormAction extends CustomerSecureAction
{ 
	private Log log = LogFactory.getLog(CustomerFormAction.class);

	public void doFind(RunData data, Context context)
	    throws Exception
	{
		try 
		{
			int iCondition = data.getParameters().getInt("Condition");
			int iBalance = data.getParameters().getInt("CurrentBalance", -1);
			String sKeyword = data.getParameters().getString("Keywords", "");
			String sTypeID = data.getParameters().getString("CustomerTypeId", "");			
			String sCurrID = data.getParameters().getString("CurrencyId");
            String sLocID = data.getParameters().getString("LocationId"); 
    		String sAreaID = data.getParameters().getString ("SalesAreaId", "");
    		String sTerrID = data.getParameters().getString ("TerritoryId", ""); 
    		String sEmpID = data.getParameters().getString ("EmployeeId", "");
    		String sStatID = data.getParameters().getString ("StatusId", "");    		    		
            
			int iLimit = data.getParameters().getInt("ViewLimit", 25);
			
			LargeSelect vCust = CustomerTool.find(iCondition, iBalance, -1, sKeyword, sTypeID, sCurrID, sLocID, sAreaID, sTerrID, sEmpID, sStatID, iLimit);

			data.getUser().setTemp("findCustomerResult", vCust);	
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError = LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage();
	    	data.setMessage (sError);
	    }
	}
	
	public void doInsert(RunData data, Context context)
    {
		Customer oCustomer = null;
		try
		{
			double dOpeningBalance = data.getParameters().getDouble("OpeningBalance");			
			oCustomer = new Customer();
			setProperties (oCustomer, data);
			oCustomer.setCustomerId (IDGenerator.generateSysID());
			oCustomer.setIsDefault (data.getParameters().getBoolean("IsDefault"));
			CustomerTool.validateCustomer (oCustomer);
			oCustomer.setOpeningBalance(bd_ZERO);
			oCustomer.setLastUpdateLocationId(PreferenceTool.getLocationID());
			oCustomer.setAddDate(new Date());			
			oCustomer.setUpdateDate(new Date());
			oCustomer.setCreateBy(data.getUser().getName());
			oCustomer.save();
			
			saveFields(data, oCustomer);
			
        	CustomerManager.getInstance().refreshCache(oCustomer);
			
	        if (dOpeningBalance != 0)
	        {
	        	oCustomer.setOpeningBalance(new BigDecimal(dOpeningBalance)); //will be saved by AR below
	        	AccountReceivableTool.validateDate(oCustomer.getAsDate(), null);
	        	AccountReceivableTool.updateOpeningBalance (oCustomer, false, data.getUser().getName());	        	
            	CustomerManager.getInstance().refreshCache(oCustomer);
	        }
            data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
		}
        catch (Exception _oEx)
        {
        	//prevent customer save and empty form
        	context.put("Customer", oCustomer);
        	
        	log.error(_oEx);
        	_oEx.printStackTrace();

        	if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doUpdate(RunData data, Context context) throws Exception
    {
 		String sID = data.getParameters().getString("ID");
 		String sCode = data.getParameters().getString("CustomerCode");
 		boolean bUpdateOB = data.getParameters().getBoolean("UpdateOB");
 		Customer oCustomer = null;
 		try
    	{    		
	    	oCustomer = CustomerTool.getCustomerByID(sID);	    	
 	    	if (oCustomer != null)
	    	{
 	    		Customer oOld = oCustomer.copy();
	    		String sOldName = oCustomer.getCustomerName();
	    		String sOldCurrID = oCustomer.getDefaultCurrencyId();
	    	    double dOld = oCustomer.getOpeningBalance().doubleValue();        	    
                Date dOldAsDate = oCustomer.getAsDate();
                                
        	    setProperties (oCustomer, data);
        	    oCustomer.setIsDefault (data.getParameters().getBoolean("IsDefault"));
        	    CustomerTool.validateCustomer (oCustomer);
        	    
    			String sNewCurrID = oCustomer.getDefaultCurrencyId();
    			if (!sOldCurrID.equals(sNewCurrID)) 
    			{   
    				//if currency change then check if ob AR trans exists
    				if (AccountReceivableTool.isTransExist(oCustomer.getCustomerId()))
    				{
    					//remove from cache, because object in cache has been altered by set properties
    					CustomerManager.getInstance().refreshCache(oCustomer.getCustomerId(), oCustomer.getCustomerCode());
    					data.setMessage(LocaleTool.getString("curr_maynot_changed"));
    					return;
    				}
    			}	        	    
        	    
        	    //opening balance
                Date dNewAsDate = oCustomer.getAsDate();                
                double dNew = oCustomer.getOpeningBalance().doubleValue();
                double dChange = dNew - dOld;
                
                if (bUpdateOB && (dChange != 0 || !StringUtil.isEqual (dOldAsDate, dNewAsDate)))
                {
                	AccountReceivableTool.validateDate (oCustomer.getAsDate(), null);
                	AccountReceivableTool.updateOpeningBalance (oCustomer, true, data.getUser().getName());
        	    }
                
                //set registration date and location for pre-registered customer
                if(StringUtil.isEmpty(sOldName) && StringUtil.isNotEmpty(oCustomer.getCustomerName()))                
                {
                	oCustomer.setAddDate(new Date());
                	oCustomer.setDefaultLocationId(PreferenceTool.getLocationID());
                }
                
                oCustomer.setLastUpdateLocationId(PreferenceTool.getLocationID());
                oCustomer.setUpdateDate(new Date());
                oCustomer.setLastUpdateBy(data.getUser().getName());
                oCustomer.save();
                
                UpdateHistoryTool.createHistory(oOld, oCustomer, data.getUser().getName(), null);
                
                saveFields(data, oCustomer);
                
	            CustomerManager.getInstance().refreshCache(oCustomer);
                data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
            }
        }
        catch (Exception _oEx)
        {
        	//prevent customer save and empty form
        	context.put("Customer", oCustomer);

        	log.error(_oEx);
        	CustomerManager.getInstance().refreshCache(sID, sCode);
        	if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		String sCode = data.getParameters().getString("CustomerCode");
    		if(SqlUtil.validateFKRef("price_list","customer_id",sID)
    			&& SqlUtil.validateFKRef("sales_transaction","customer_id",sID)
    			&& SqlUtil.validateFKRef("sales_return","customer_id",sID)
    			&& SqlUtil.validateFKRef("ar_payment","customer_id",sID)
    			&& SqlUtil.validateFKRef("credit_memo","customer_id",sID))
    		{
    			Customer oCust = CustomerTool.getCustomerByID(sID);
    			
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(CustomerBalancePeer.CUSTOMER_ID, sID);
	        	CustomerBalancePeer.doDelete(oCrit);
                
                oCrit = new Criteria();
	        	oCrit.add(AccountReceivablePeer.CUSTOMER_ID, sID);
	        	AccountReceivablePeer.doDelete(oCrit);
                
                oCrit = new Criteria();
	        	oCrit.add(CustomerPeer.CUSTOMER_ID, sID);
	        	CustomerPeer.doDelete(oCrit);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
	        	CustomerManager.getInstance().refreshCache(sID, sCode);
	        	
	        	UpdateHistoryTool.createDelHistory(oCust, data.getUser().getName(), null);
        	}
        	else 
        	{
				data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }


	public void setProperties ( Customer _oCustomer, RunData data)
    {
    	try
    	{
	    	data.getParameters().setProperties(_oCustomer);
	    	_oCustomer.setAsDate(CustomParser.parseDate(data.getParameters().getString("AsDate")));
    	}
    	catch (Exception _oEx)
    	{
    		log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }

    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			CustomerLoader oLoader = new CustomerLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
    
    private void saveFields(RunData data, Customer oCustomer)    
    	throws Exception
    {
    	if(PreferenceTool.medicalInstalled())
		{
			List vCF = CustomerTool.getCFields(oCustomer.getCustomerTypeId());
			for(int i = 0; i < vCF.size(); i++)
			{
				CustomerCfield oCF = (CustomerCfield) vCF.get(i);				
				CustomerField oCFD = CustomerTool.getField(oCustomer.getCustomerId(), oCF.getId());
				CustomerField oOLD = null;
				if(oCFD == null)
				{
					oCFD = new CustomerField();				
					oCFD.setCustomerFieldId(IDGenerator.generateSysID());
				}	
				else
				{
					oOLD = oCFD.copy();
				}
				oCFD.setCustomerId(oCustomer.getId());
				oCFD.setCfieldId(oCF.getId());				
				oCFD.setFieldName(oCF.getFieldName());
				oCFD.setFieldValue(data.getParameters().getString(oCF.getId(),""));
				oCFD.setUpdateDate(new Date());
				oCFD.save();
				
				if(oOLD != null)
				{
					UpdateHistoryTool.createHistory(oOLD, oCFD, data.getUser().getName(), null);
				}
			}
		}		
    }
}