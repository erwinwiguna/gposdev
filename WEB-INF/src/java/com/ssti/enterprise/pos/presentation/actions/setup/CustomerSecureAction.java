package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureAction;

public class CustomerSecureAction extends SecureAction
{	
   	private static final String s_VIEW_PERM    = "View Customer Data";
	private static final String s_UPDATE_PERM  = "Update Customer Data";
	
    private static final String[] a_UPDATE_PERM  = {s_VIEW_PERM, s_UPDATE_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
	private Log log = LogFactory.getLog(VendorFormAction.class);
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {

        if(!data.getParameters().getString("eventSubmit_doFind","false").equals("false"))
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
        else if(!data.getParameters().getString("eventSubmit_doInsert","").equals("") ||
                !data.getParameters().getString("eventSubmit_doUpdate","").equals("") ||
                !data.getParameters().getString("eventSubmit_doDelete","").equals("") ||
				!data.getParameters().getString("eventSubmit_doLoadFile","").equals("")
			)
        {
        	return isAuthorized (data, a_UPDATE_PERM);
    	}
        return false;
    }
}
