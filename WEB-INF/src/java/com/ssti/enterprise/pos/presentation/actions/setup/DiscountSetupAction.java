package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.DiscountLoader;
import com.ssti.enterprise.pos.manager.DiscountManager;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.DiscountPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class DiscountSetupAction extends SetupSecureAction
{
	private static Log log = LogFactory.getLog(DiscountSetupAction.class);
	
	private static final String[] a_PERM = {"View Discount Setup", "Update Discount Setup"};
    private static final String[] a_VIEW = {"View Discount Setup"};
	
    static final int i_ADD    = 1;
    static final int i_UPDATE = 2;
    static final int i_DELETE = 3;
    static final int i_FIND   = 4;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iOp = data.getParameters().getInt("op");
        if (iOp == i_FIND)
        {
            return isAuthorized(data, a_VIEW);
        }
        return isAuthorized(data, a_PERM);
    }	
    
    public void doPerform(RunData data, Context context)
    {
    	int iOp = data.getParameters().getInt("op");
    	if (iOp == i_ADD) doInsert(data, context);
    	else if (iOp == i_UPDATE) doSave(data, context);
    	else if (iOp == i_DELETE) doDelete(data, context);
    	else if (iOp == i_FIND) doFind(data, context);
    }

	public void doInsert(RunData data, Context context)
    {
		
		doSave(data, context);
    }

    public void doSave(RunData data, Context context)
    {
        String sID = data.getParameters().getString("id");
        Discount oDisc = null;
        Discount oOld = null;
		try
		{
			if (StringUtil.isNotEmpty(sID))
			{
				oDisc = DiscountTool.getDiscountByID(sID);			
			}
			if (oDisc == null)
			{
				oDisc = new Discount();
				oDisc.setDiscountId (IDGenerator.generateSysID());
			}
			else
			{
				oOld = oDisc.copy();
			}
		    setProperties (oDisc, data);
            oDisc.setUpdateDate(new Date());
            oDisc.setLastUpdateBy(data.getUser().getName());
            oDisc.setIsTotalDiscount();

		    validate(oDisc);
		    oDisc.save();
            DiscountManager.getInstance().refreshCache(sID);
        	UpdateHistoryTool.createHistory(oOld, oDisc, data.getUser().getName(), null);
            data.setMessage (LocaleTool.getString(s_SAVE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
        	log.error(_oEx);
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_SAVE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_SAVE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
        setContext(data,context,oDisc);
    }
    
	private void validate(Discount _oDisc)
	    throws Exception
    {
        //validate customer code
	    if (StringUtil.isNotEmpty(_oDisc.getCustomers()))
        {
	        List vCode = StringUtil.toList(_oDisc.getCustomers(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                if (CustomerTool.getCustomerByCode(sCode) == null) throw new Exception("Customer " + sCode + " Not Found");
            }
        }
        //validate item code
        if (StringUtil.isNotEmpty(_oDisc.getItems()))
        {
            List vCode = StringUtil.toList(_oDisc.getItems(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                if (ItemTool.getItemByCode(sCode) == null) throw new Exception("Item " + sCode + " Not Found");
            }
        }
        //validate SKU
        
        //validate type && other rule   
        
        //validate exists
        Discount oExist = DiscountTool.getExists(_oDisc);
        if(oExist != null)
        {
        	throw new Exception("Discount Rule Conflict With : " + oExist.getDiscountCode());
        }
    }  
    
    private void setContext(RunData data, Context context, Discount _oDisc)
    {
        data.getParameters().setString("mode","update");
        context.put("Discount", _oDisc);
    }
    
    public void doDelete(RunData data, Context context)
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		Discount oDisc = DiscountTool.getDiscountByID(sID);
    		Criteria oCrit = new Criteria();
        	oCrit.add(DiscountPeer.DISCOUNT_ID, sID);
        	DiscountPeer.doDelete(oCrit);
        	DiscountManager.getInstance().refreshCache(sID);
       		UpdateHistoryTool.createDelHistory(oDisc, data.getUser().getName(), null);
        	data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

	public void doFind(RunData data, Context context)
    {
		ParameterParser formData = data.getParameters();
		try
		{	
			int iCond = formData.getInt("Condition");     
			String sKey = formData.getString("Keywords");   
			String sStart = formData.getString("StartDate");  
			String sEnd = formData.getString("EndDate");    
			String sKatID = formData.getString("KategoriId"); 
            String sLocID = formData.getString("LocationId");             
			int iDisc = formData.getInt("DiscountType");
            int iStatus = formData.getInt("Status");
			
			context.put("Discounts", DiscountTool.findData(iCond, sKey, sStart, sEnd, sKatID, sLocID, iDisc, iStatus));			
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	data.setMessage (_oEx.getMessage());
        }
    }    
    
    private void setProperties(Discount  _oDiscount,  RunData data)
    	throws Exception
    {
		try
		{
			String sBegin = data.getParameters().getString("EventBeginDate");
			String sEnd = data.getParameters().getString("EventEndDate");
			data.getParameters().setProperties(_oDiscount);
			_oDiscount.setEventBeginDate(DateUtil.getStartOfDayDate(CustomParser.parseDate(sBegin)));
			_oDiscount.setEventEndDate(DateUtil.getEndOfDayIncSec(CustomParser.parseDate(sEnd)));
			_oDiscount.setPerItem(data.getParameters().getBoolean("PerItem"));
			_oDiscount.setMultiply(data.getParameters().getBoolean("Multiply"));	
			_oDiscount.setIsCcDisc(data.getParameters().getBoolean("IsCcDisc"));	
			_oDiscount.setHappyHour(data.getParameters().getBoolean("HappyHour"));				
			
			if (_oDiscount.getKategoriId() == null) _oDiscount.setKategoriId("");
            
            StringBuilder oLoc = new StringBuilder();
            String[] aLoc = data.getParameters().getStrings("locationId");
            for (int i = 0; i < aLoc.length; i++)
            {
                String sLoc = aLoc[i];
                if (sLoc.equals(""))
                {
                    oLoc.append("");
                    break;
                }
                else 
                {
                    oLoc.append(sLoc);
                    if (i != (aLoc.length - 1))
                    {
                        oLoc.append(",");
                    }
                }
            }
            _oDiscount.setLocationId(oLoc.toString());
    	}
    	catch (Exception _oEx)
    	{
    		log.error (_oEx);
        	throw new Exception (LocaleTool.getString(s_SET_PROP_FAILED) + _oEx.getMessage());
    	}
    }
    
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	log.debug (oFileItem);
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			DiscountLoader oLoader = new DiscountLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			
			if (oLoader.getTotalError() > 0)
			{
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_FAILED));			
			}
			else
			{
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
			}
			context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
			data.setMessage (sError);
			_oEx.printStackTrace();
		}
	}       
}
