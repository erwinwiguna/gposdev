package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.FOBCourierManager;
import com.ssti.enterprise.pos.om.Fob;
import com.ssti.enterprise.pos.om.FobPeer;
import com.ssti.enterprise.pos.tools.FobTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class FOBSetupAction extends SetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Fob oFob = new Fob();
			setProperties (oFob, data);
			oFob.setFobId (IDGenerator.generateSysID());
	        oFob.save();
	        FOBCourierManager.getInstance().refreshCache(oFob);
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Fob oFob = FobTool.getFobByID(data.getParameters().getString("ID"));
			Fob oOld = oFob.copy();
			setProperties (oFob, data);
	        oFob.save();
	        FOBCourierManager.getInstance().refreshCache(oFob);
	        UpdateHistoryTool.createHistory(oOld, oFob, data.getUser().getName(), null);
           	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("purchase_order","fob_id",sID)   &&
    		   SqlUtil.validateFKRef("purchase_receipt","fob_id",sID) &&
    		   SqlUtil.validateFKRef("purchase_invoice","fob_id",sID) &&
    		   SqlUtil.validateFKRef("purchase_order","fob_id",sID)   &&
    		   SqlUtil.validateFKRef("purchase_receipt","fob_id",sID) &&
    		   SqlUtil.validateFKRef("purchase_invoice","fob_id",sID) 
    		  )
    		{
    			Fob oFob = FobTool.getFobByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(FobPeer.FOB_ID, sID);
	        	FobPeer.doDelete(oCrit);
	        	FOBCourierManager.getInstance().refreshCache(sID);
	        	UpdateHistoryTool.createDelHistory(oFob, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));	
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }

    private void setProperties (Fob _oFob, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oFob);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
