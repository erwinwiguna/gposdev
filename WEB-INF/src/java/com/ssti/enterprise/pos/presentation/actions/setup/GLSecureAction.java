package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureAction;

public class GLSecureAction extends SecureAction
{
	private static final String[] a_PERM = {"GL Setup", "View GL Setup"};
	private static final String[] a_UPDATE_PERM = {"GL Setup", "View GL Setup", "Update GL Setup"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doFind",null) != null)
        {
        	return isAuthorized(data, a_PERM);
    	}
        else if(data.getParameters().getString("eventSubmit_doInsert",null)!= null ||
                data.getParameters().getString("eventSubmit_doUpdate",null)!= null ||
                data.getParameters().getString("eventSubmit_doDelete",null)!= null ||
                data.getParameters().getString("eventSubmit_doClose", null)!= null ||
                data.getParameters().getString("eventSubmit_doReopen",null)!= null ||
                data.getParameters().getString("eventSubmit_doFaclose", null)!= null ||
                data.getParameters().getString("eventSubmit_doFareopen",null)!= null ||
                data.getParameters().getString("eventSubmit_doClosepl",null)!= null)
        {
        	return isAuthorized(data, a_UPDATE_PERM);
    	}
        return false;
    }		
}
