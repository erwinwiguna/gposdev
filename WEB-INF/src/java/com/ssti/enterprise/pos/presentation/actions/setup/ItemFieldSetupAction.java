package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CustomFieldManager;
import com.ssti.enterprise.pos.om.CustomField;
import com.ssti.enterprise.pos.om.CustomFieldPeer;
import com.ssti.enterprise.pos.om.ItemFieldPeer;
import com.ssti.enterprise.pos.tools.CustomFieldTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.SqlUtil;

public class ItemFieldSetupAction extends ItemSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			CustomField oCustomField = new CustomField();
			setProperties (oCustomField, data);
			oCustomField.setCustomFieldId (IDGenerator.generateSysID());
	        oCustomField.save();
	        CustomFieldManager.getInstance().refreshCache(oCustomField);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			CustomField oCustomField = CustomFieldTool.getCustomFieldByID(data.getParameters().getString("ID"));
			setProperties (oCustomField, data);
	        oCustomField.save();
	        CustomFieldManager.getInstance().refreshCache(oCustomField);
	        
	        String sOldName = data.getParameters().getString("OldFieldName","");
			String sNewName = data.getParameters().getString("FieldName","");
			if (!sOldName.equals(sNewName))
			{

				Criteria oCrit = new Criteria();
				oCrit.add (ItemFieldPeer.FIELD_NAME, sOldName);
				Criteria oNewCrit = new Criteria();
				oNewCrit.add (ItemFieldPeer.FIELD_NAME, sNewName);	
				ItemFieldPeer.doUpdate(oCrit, oNewCrit);
			}
	        data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) +_oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{	
    		String sID = data.getParameters().getString("ID");
    		String sName = data.getParameters().getString("FieldName");
    		
    		if(SqlUtil.validateFKRef("item_field","field_name",sName))
    		{
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(CustomFieldPeer.CUSTOM_FIELD_ID, sID);
	        	CustomFieldPeer.doDelete(oCrit);
	        	CustomFieldManager.getInstance().refreshCache(sID);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	 data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED)+_oEx.getMessage());
        }
    }

    private void setProperties (CustomField _oCustomField, RunData data) 
    	throws Exception
    {
		try
		{
	    	data.getParameters().setProperties(_oCustomField);
    	}
    	catch (Exception _oEx)
    	{
    		
        	throw new Exception (LocaleTool.getString(s_SET_PROP_FAILED)+_oEx.getMessage());
        }
    }
}
