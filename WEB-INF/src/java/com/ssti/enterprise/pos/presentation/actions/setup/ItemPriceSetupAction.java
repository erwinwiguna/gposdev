package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.ItemPriceLoader;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.excel.helper.ExcelLoader;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemGroupSetupAction.java,v 1.1 2009/05/04 01:58:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemGroupSetupAction.java,v $
 * Revision 1.1  2009/05/04 01:58:15  albert
 * *** empty log message ***
 *
 * Revision 1.53  2008/06/29 07:03:21  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ItemPriceSetupAction extends ItemSetupSecureAction
{
	private static Log log = LogFactory.getLog ( ItemPriceSetupAction.class );
	
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    }

    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
         	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			ExcelLoader oLoader = new ItemPriceLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
        	context.put ("LoadResult", oLoader.getResult());
        }
        catch (Exception _oEx) 
        {
        	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
        	log.error ( sError, _oEx );
        	data.setMessage (sError);
        }
    }	
}