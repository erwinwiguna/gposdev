package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.ItemLoader;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.presentation.screens.setup.ItemSetup;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemHistoryTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemSetupAction.java,v 1.54 2009/05/04 01:58:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemSetupAction.java,v $
 * Revision 1.54  2009/05/04 01:58:15  albert
 * *** empty log message ***
 *
 * Revision 1.53  2008/06/29 07:03:21  albert
 * *** empty log message ***
 *
 * Revision 1.52  2008/03/03 01:56:09  albert
 * *** empty log message ***
 *
 * Revision 1.51  2008/02/26 05:11:58  albert
 * *** empty log message ***
 *
 * Revision 1.50  2007/06/21 03:43:39  albert
 * *** empty log message ***
 *
 * Revision 1.49  2007/05/01 03:09:06  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ItemSetupAction extends ItemSetupSecureAction
{
	private static Log log = LogFactory.getLog ( ItemSetupAction.class );
	
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
        
    	String sPerform = data.getParameters().getString("perform", "");
    	//System.out.println("perform " + sPerform + " params " + data.getParameters().toString());
    	
    	if (sPerform.equals("insert")) 
    	{
    		doInsert(data, context);
    	}
    	else if (sPerform.equals("update")) 
    	{
    		doUpdate(data, context);
    	}
    	else if (sPerform.equals("delete")) 
    	{
    		doDelete(data, context);
    	}
    }

    /**
     * 
     * @param data
     * @param context
     * @throws Exception
     */
	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Item oItem = new Item();
			setProperties(oItem, data);
			oItem.setUpdateDate (new Date());	
			oItem.setItemId (IDGenerator.generateSysID());
			
			boolean bSaveNew = data.getParameters().getBoolean("saveNew");
			
			//generate auto number
	    	if(data.getParameters().getBoolean("AutoNumberCode") == true) 
	    	{
				String sKategoriID = data.getParameters().getString("KategoriId");
				String sSuffix = data.getParameters().getString("CodeManual");
				String sGeneratedCode = ItemTool.generateItemCode (sKategoriID, sSuffix);
				oItem.setItemCode(sGeneratedCode);
			}
			if(data.getParameters().getString("Barcode", "").equals(""))
			{
				oItem.setBarcode(oItem.getItemCode());
			}
			processPictureUpload (data,  oItem);
			oItem.setAddDate(new Date());
			oItem.setCreateBy(data.getUser().getName());
			oItem.save();
			
			saveFields(data, oItem);
			
	        ItemManager.getInstance().refreshCache(oItem);	        			
			if (oItem.getItemType() == i_INVENTORY_PART) 
			{
				createReceiptUnplanned (data, oItem);
			}
        	else if (oItem.getItemType() == i_GROUPING) 
        	{
        		ItemSetup.setGroupDataContext(data, context, oItem.getItemId());
        		context.put ("Item", oItem );
        		context.put ("is_grouping", "1");
        	}
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        	
        	if(!bSaveNew)
        	{
        		context.put("Item", oItem);
        	}
        }
        catch (Exception _oEx)
       	{
        	_oEx.printStackTrace();
       		log.error(_oEx);
       		if(_oEx.getMessage() == null || _oEx.getMessage().indexOf(s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else if(_oEx.getMessage().indexOf(s_DUPLICATE_MSG) == -1)
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
    			Item oItem = new Item();
    			setProperties(oItem, data);
    			oItem.setItemId("");
          		context.put ("Item", oItem);
	        }
        }
    }

	/**
	 * 
	 * @param data
	 * @param context
	 * @throws Exception
	 */
 	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
 		try
    	{
			Item oOldItemRef = oItem.copy();
			setProperties(oItem, data);
			oItem.setUpdateDate (new Date());	

			if(data.getParameters().getString("Barcode", "").equals(""))
			{
				oItem.setBarcode(oItem.getItemCode());
			}
			processPictureUpload ( data, oItem );
	        oItem.save();
	        
	        if (oItem.getItemType() == i_GROUPING)
	        {
	        	ItemSetup.setGroupDataContext(data, context, oItem.getItemId());
	        }
	        
			//set the item update history
			ItemHistoryTool.createItemHistory (oOldItemRef, oItem, data.getUser().getName(), null);

        	data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
        	context.put("Item", oItem);
        	saveFields(data, oItem);
        	
	        //refresh data in cache
	        ItemManager.getInstance().refreshCache(oItem);
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
        	log.error(_oEx);
       		if(_oEx.getMessage() == null || _oEx.getMessage().indexOf(s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else if(_oEx.getMessage().indexOf(s_DUPLICATE_MSG) > 0)
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
       		if (oItem != null)
       		{
       			ItemManager.getInstance().refreshCache (
       			    oItem.getItemId(), oItem.getItemCode(), oItem.getKategoriId()
				);
       		}
       	    context.put ("Item", oItem);
       	}
    }

 	/**
 	 * 
 	 * @param data
 	 * @param _oItem
 	 * @throws Exception
 	 */
	private void createReceiptUnplanned (RunData data, Item _oItem)
		throws Exception
	{
		int iNumOfLoc = data.getParameters().getInt("numOfLoc");
		for (int i = 0; i <= iNumOfLoc; i++) 
		{
			double dQty = data.getParameters().getDouble("Qty" + i);
			if (dQty > 0) 
			{
			    Account oEquity = AccountTool.getOpeningBalanceEquity(null);
			    
				double dCost = data.getParameters().getDouble("Cost" + i);
				IssueReceipt oIRU = new IssueReceipt ();
				oIRU.setTransactionDate (new Date());
				oIRU.setTransactionType (i_INV_TRANS_RECEIPT_UNPLANNED);
				oIRU.setAdjustmentTypeId(data.getParameters().getString ("AdjustmentTypeId"));
				oIRU.setLocationId      (data.getParameters().getString ("LocationId" + i));
				oIRU.setLocationName    (data.getParameters().getString ("LocationName" + i));
				oIRU.setUserName        (data.getUser().getName());
				oIRU.setDescription     ("Start Stock from Item Creation");
				oIRU.setAccountId       (oEquity.getAccountId());

				List vDetail = new ArrayList(1);
				IssueReceiptDetail oDetail = new IssueReceiptDetail ();
				oDetail.setItemId       (_oItem.getItemId());
				oDetail.setItemCode     (_oItem.getItemCode());
				oDetail.setUnitId       (_oItem.getUnitId());
				oDetail.setUnitCode     (UnitTool.getCodeByID(_oItem.getUnitId()));
				oDetail.setQtyChanges   (new BigDecimal(dQty));
				oDetail.setCost         (new BigDecimal(dCost));
				oDetail.setProjectId    ("");
				oDetail.setDepartmentId ("");
				vDetail.add (oDetail);

				IssueReceiptTool.saveData (oIRU, vDetail, null);				
			}
		}
	}

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ItemId");
    		if ( ItemTool.delete(sID) )
    		{
        		data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else 
        	{
				data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }
   
	// Private Methods 
	private void setProperties(Item _oItem, RunData data)
    	throws Exception
    {
	    data.getParameters().setProperties(_oItem);
	    _oItem.setIsFixedPrice(data.getParameters().getBoolean("IsFixedPrice"));
	    
	    String[] tags = data.getParameters().getStrings("tags");
	    if(tags != null)
	    {
	    	StringBuilder tag = new StringBuilder("");
		    for(int i = 0;i < tags.length; i++)
		    {
		    	tag.append(tags[i]);
		    	if(i < tags.length - 1) tag.append(",");
		    }
		    _oItem.setTags(tag.toString());
	    }
	    else
	    {
	    	_oItem.setTags("");
	    }
    }
	
	private void processPictureUpload ( RunData data, Item _oItem )
    	throws Exception
    {
    	try
    	{
			//get file item
			FileItem oFileItem = data.getParameters().getFileItem("ItemPicture");
			if (oFileItem != null)
			{
				String sFileName   = oFileItem.getName();					
				if (StringUtil.isNotEmpty(sFileName)) 
				{
					String sSeparator  = System.getProperty("file.separator");
					String sPathDest   = PreferenceTool.getPicturePath();
					String sFilePath   = IOTool.getRealPath(data, sPathDest);		
					int iSeparatorIndex = sFileName.lastIndexOf (sSeparator.charAt(0));
					if (iSeparatorIndex > 0)
					{
						sFileName = sFileName.substring (iSeparatorIndex);
					}
					sFilePath = sFilePath + sSeparator + sFileName;					
					log.debug("File Path:" + sFilePath);
					
					oFileItem.write (new File (sFilePath)); //save picture file
					StringBuilder oSB = new StringBuilder();
					oSB.append(data.getContextPath());
					oSB.append(sPathDest);
					oSB.append(sFileName);        	
					String sFilePathToSafe = (oSB.toString()).replace('\\','/');
        			
					log.debug("File Path:" + sFilePathToSafe);
					_oItem.setItemPicture ( sFilePathToSafe );
        		}
        	}
		}
		catch (Exception _oEx)
    	{
        	throw new Exception (LocaleTool.getString(s_UPLOAD_PIC_FAILED) +  _oEx.getMessage());
        }
	}

	private void saveFields ( RunData data, Item _oItem )
		throws Exception
	{
		try
		{
			int iTotalFields = data.getParameters().getInt("TotalField", 0);
			for (int i = 0; i < iTotalFields; i++)
			{
				String sID = data.getParameters().getString("ItemFieldId" + i, "");
				String sName = data.getParameters().getString("FieldName" + i, "");
				String sValue = data.getParameters().getString("FieldValue" + i, "");
				ItemFieldTool.saveField(_oItem.getItemId(), sName, sValue);				
			}
		}
		catch (Exception _oEx)
		{
	    	throw new Exception (LocaleTool.getString(s_SAVE_FIELDS_FAILED) +  _oEx.getMessage());
	    }
	}
	
    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
         	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			ItemLoader oLoader = new ItemLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
        	context.put ("LoadResult", oLoader.getResult());
        }
        catch (Exception _oEx) 
        {
        	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
        	log.error ( sError, _oEx );
        	data.setMessage (sError);
        }
    }	
}