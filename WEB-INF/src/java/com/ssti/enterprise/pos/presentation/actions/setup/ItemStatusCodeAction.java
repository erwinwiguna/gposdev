package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ItemStatusCodeManager;
import com.ssti.enterprise.pos.om.ItemStatusCode;
import com.ssti.enterprise.pos.om.ItemStatusCodePeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemStatusCodeTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.SqlUtil;

public class ItemStatusCodeAction extends ItemSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			ItemStatusCode oData = new ItemStatusCode();
			setProperties (oData, data);
			oData.setItemStatusCodeId (IDGenerator.generateSysID());
	        oData.save();
	        ItemStatusCodeManager.getInstance().refreshCache(oData);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			ItemStatusCode oData = ItemStatusCodeTool.getItemStatusCodeByID(data.getParameters().getString("ID"));
			setProperties (oData, data);
			oData.setIsDefault(data.getParameters().getBoolean("IsDefault"));
	        oData.save();
	        ItemStatusCodeManager.getInstance().refreshCache(oData);   	
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED)+_oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{	
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("item","item_status_code_id",sID))
    		{
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(ItemStatusCodePeer.ITEM_STATUS_CODE_ID, sID);
	        	ItemStatusCodePeer.doDelete(oCrit);
	        	ItemStatusCodeManager.getInstance().refreshCache(sID);	        	
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	 data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED)+_oEx.getMessage());
        }
    }

    private void setProperties (ItemStatusCode _oItemStatusCode, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oItemStatusCode);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
