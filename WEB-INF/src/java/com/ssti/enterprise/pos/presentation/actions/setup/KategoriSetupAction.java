package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.Torque;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.KategoriLoader;
import com.ssti.enterprise.pos.manager.KategoriManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.KategoriAccount;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-07-11
 * - add webstore specific data save
 * 
 * </pre><br>
 */
public class KategoriSetupAction extends SecureAction
{

   	private static final String s_VIEW_PERM    = "View Kategori Data";
	private static final String s_UPDATE_PERM  = "Update Kategori Data";
	private static final String s_DELETE_PERM  = "Delete Kategori Data";
	
    private static final String[] a_UPDATE_PERM  = {s_VIEW_PERM, s_UPDATE_PERM};
    private static final String[] a_DELETE_PERM  = {s_VIEW_PERM, s_DELETE_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {        
        if(data.getParameters().getString("eventSubmit_doInsert",null) != null ||
           data.getParameters().getString("eventSubmit_doLoadfile",null) != null ||
           data.getParameters().getInt("deleteKat") > 0)
        {
        	return isAuthorized (data, a_UPDATE_PERM);
    	}
        return isAuthorized (data, a_VIEW_PERM);
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	int iOp = data.getParameters().getInt("op");
    	if(iOp == 1)
    	{
    		doDelete(data, context);
    	}
    	else if(iOp == 2)
    	{
    		doUpdate(data, context);
    	}
    	else if(iOp == 3)
    	{
    		setAccount(data, context);
    	}    	
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Kategori oKategori = new Kategori();
			setProperties(oKategori, data);
			oKategori.setKategoriId (IDGenerator.generateSysID(false));
			oKategori.setInternalCode(KategoriTool.getInternalCode(oKategori));
			oKategori.save();
			
			KategoriManager.getInstance().refreshCache(oKategori);
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

 	public void doUpdate(RunData data, Context context)
        throws Exception
    {
    	try
    	{
			Kategori oKategori = KategoriTool.getKategoriByID (data.getParameters().getString("parentid"));
			if (oKategori != null)
			{
				oKategori.setKategoriCode(data.getParameters().getString("parentcode"));		
				oKategori.setDescription(data.getParameters().getString("parentdesc"));
				oKategori.setInternalCode(KategoriTool.getInternalCode(oKategori));								
				
				//if from WstKategoriForm				
				oKategori.save();

				KategoriManager.getInstance().refreshCache(oKategori);
				data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
			}
		}
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("parentid");
    		if(SqlUtil.validateFKRef("item","kategori_id",sID))
    		{
				KategoriTool.deleteKategoriByID(data.getParameters().getString("parentid"));
        		data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	{	
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}
        }
        catch (Exception _oEx)
        {                                                                   
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }   
    }

    public void setProperties (Kategori _oKategori, RunData data)
    	throws Exception
    {
	    data.getParameters().setProperties(_oKategori);
	    _oKategori.setAddDate (new Date());
	    _oKategori.setKategoriLevel(KategoriTool.setChildLevel(_oKategori.getParentId()));
    }
    
    public void doLoadfile (RunData data, Context context)
    	throws Exception
    {
    	try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	log.debug (oFileItem);
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			KategoriLoader oLoader = new KategoriLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			
			if (oLoader.getTotalError() > 0)
			{
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_FAILED));			
			}
			else
			{
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
			}
			context.put ("LoadResult", oLoader.getResult());
		}
    	catch (Exception _oEx) 
		{
    		String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
    		data.setMessage (sError);
    		_oEx.printStackTrace();
		}
    }    
    
	public void setAccount(RunData data, Context context) 
	{
		String sKategoriID = data.getParameters().getString("KategoriId");
		if (StringUtil.isNotEmpty(sKategoriID))
		{
			try 
			{
				Connection oConn = Torque.getConnection();
				
				KategoriAccount oKat = KategoriTool.getKategoriAccount(sKategoriID);
				if (oKat == null) oKat = new KategoriAccount();
				data.getParameters().setProperties(oKat);
				oKat.setCreateBy(data.getUser().getName());
				oKat.setUpdateDate(new Date());
				oKat.save(oConn);				
				KategoriManager.getInstance().refreshCache(oKat.getKategoriId());
				
				//update items
				if(data.getParameters().getBoolean("UpdateItems"))
				{
					List vKatID = KategoriTool.getChildIDList(sKategoriID);
					vKatID.add(sKategoriID);
					List vItem = ItemTool.getItemByKategoriID(vKatID);
					for (int i = 0; i < vItem.size(); i++)
					{						
						Item oItem = (Item)vItem.get(i);
						Item oOld = oItem.copy();
						oItem.setInventoryAccount(oKat.getInventoryAccount());
						oItem.setSalesAccount(oKat.getSalesAccount());
						oItem.setSalesReturnAccount(oKat.getSalesReturnAccount());
						oItem.setItemDiscountAccount(oKat.getItemDiscountAccount());
						oItem.setCogsAccount(oKat.getCogsAccount());
						oItem.setExpenseAccount(oKat.getExpenseAccount());						
						oItem.setPurchaseReturnAccount(oKat.getPurchaseReturnAccount());
						oItem.setUnbilledGoodsAccount(oKat.getUnbilledGoodsAccount());
						oItem.save(oConn);
						//ItemHistoryTool.createItemHistory (oOld, oItem, data.getUser().getName(), oConn);						
					}
				}
				
	        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));				
			} 
			catch (Exception _oEx) 
			{
	        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
			}
		}
	}  
	
	private void processPicUpload ( RunData data, Kategori _oKat )
		throws Exception
	{
		try
		{
			String sSeparator  = System.getProperty("file.separator");
			String sPathDest   = PreferenceTool.getPicturePath();
			if(StringUtil.isEmpty(sPathDest)) sPathDest = PreferenceTool.getPicturePath();
			String sFilePath   = IOTool.getRealPath(data, sPathDest);		

			//get file item
			FileItem oPic = data.getParameters().getFileItem("prnpicture");
			if (oPic != null && StringUtil.isNotEmpty(oPic.getName()) && _oKat != null)
			{
				String sFileExt = IOTool.getFileExtension(oPic.getName());	
				String sFileName =_oKat.getInternalCode() + "_pic" + sFileExt;
				String sFSPath = sFilePath + sSeparator + sFileName; 					
				log.info("PIC FS Path:" + sFilePath);

				oPic.write (new File (sFSPath)); //save picture file
				StringBuilder oSB = new StringBuilder();
				oSB.append(data.getContextPath());
				oSB.append(sPathDest);
				oSB.append(sFileName);        	
				String sDBPath = (oSB.toString()).replace('\\','/');

				log.info("PIC DB Path:" + sDBPath);
				_oKat.setPicture ( sDBPath );
			}

			//get file item
			FileItem oThumb = data.getParameters().getFileItem("prnthumbnail");
			if (oThumb != null && StringUtil.isNotEmpty(oThumb.getName()))
			{
				String sFileExt = IOTool.getFileExtension(oThumb.getName());	
				String sFileName =_oKat.getInternalCode() + "_thumb" + sFileExt;
				String sFSPath = sFilePath + sSeparator + sFileName; 					
				log.info("THUMB FS Path:" + sFilePath);

				oThumb.write (new File (sFSPath)); //save picture file
				StringBuilder oSB = new StringBuilder();
				oSB.append(data.getContextPath());
				oSB.append(sPathDest);
				oSB.append(sFileName);        	
				String sDBPath = (oSB.toString()).replace('\\','/');

				log.info("THUMB DB Path:" + sDBPath);
				_oKat.setThumbnail( sDBPath );
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new Exception (LocaleTool.getString(s_UPLOAD_PIC_FAILED) +  _oEx.getMessage());
		}
	}
}