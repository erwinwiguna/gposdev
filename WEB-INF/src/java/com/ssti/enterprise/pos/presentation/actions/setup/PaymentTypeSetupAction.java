package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PaymentTypeManager;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.PaymentTypePeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class PaymentTypeSetupAction extends PaymentSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			PaymentType oPT = new PaymentType();
			setProperties (oPT, data);
			oPT.setPaymentTypeId (IDGenerator.generateSysID());
	        oPT.save();
	        PaymentTypeManager.getInstance().refreshCache(oPT);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(data.getParameters().getString("ID"));
			PaymentType oOld = oPT.copy();
			setProperties (oPT, data);
			oPT.setIsDefault(data.getParameters().getBoolean("IsDefault"));
			oPT.setIsCredit(data.getParameters().getBoolean("IsCredit"));
			oPT.setIsVoucher(data.getParameters().getBoolean("IsVoucher"));
			oPT.setIsCoupon(data.getParameters().getBoolean("IsCoupon"));			
			oPT.setIsCreditCard(data.getParameters().getBoolean("IsCreditCard"));						
			oPT.setIsPointReward(data.getParameters().getBoolean("IsPointReward"));			
			oPT.setIsMultiplePayment(data.getParameters().getBoolean("IsMultiplePayment"));
			oPT.setShowInPos(data.getParameters().getBoolean("ShowInPos"));
			
	        oPT.save();
	        PaymentTypeManager.getInstance().refreshCache(oPT);
	        UpdateHistoryTool.createHistory(oOld, oPT, data.getUser().getName(), null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
 		StringBuilder oMsg = new StringBuilder();
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("sales_transaction","payment_type_id",sID,oMsg)
    		    && SqlUtil.validateFKRef("sales_order","payment_type_id",sID,oMsg)	
    			&& SqlUtil.validateFKRef("purchase_order","payment_type_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_invoice","payment_type_id",sID,oMsg))
    		{
    			PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(sID);
    			
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(PaymentTypePeer.PAYMENT_TYPE_ID, data.getParameters().getString("ID"));
	        	PaymentTypePeer.doDelete(oCrit);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        		UpdateHistoryTool.createDelHistory(oPT, data.getUser().getName(), null);
        		PaymentTypeManager.getInstance().refreshCache(sID);
        	}
        	else
        	{
				data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE) + " \n" + oMsg);
        	}
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED)+_oEx.getMessage());
        }
    }

    private void setProperties (PaymentType _oPT, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oPT);
	    	if (_oPT.getDirectSalesAccount() == null)
	    	{
	    		_oPT.setDirectSalesAccount("");
	    	}
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
