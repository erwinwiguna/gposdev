package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ShiftManager;
import com.ssti.enterprise.pos.om.Shift;
import com.ssti.enterprise.pos.om.ShiftPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.ShiftTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class ShiftSetupAction extends ItemSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Shift oShift = new Shift();
			setProperties (oShift, data);
			oShift.setShiftId (IDGenerator.generateSysID());
	        oShift.save();
	        ShiftManager.getInstance().refreshCache(oShift);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Shift oShift = ShiftTool.getShiftByID(data.getParameters().getString("ID"));
			Shift oOld = oShift.copy();
			setProperties (oShift, data);
	        oShift.save();
	        ShiftManager.getInstance().refreshCache(oShift);
	        UpdateHistoryTool.createHistory(oOld, oShift, data.getUser().getName(), null);
	        data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) +_oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{	
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("cashier_balance","shift",sID))
    		{
    			Shift oShift = ShiftTool.getShiftByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(ShiftPeer.SHIFT_ID, sID);
	        	ShiftPeer.doDelete(oCrit);
	        	ShiftManager.getInstance().refreshCache(sID);
	        	UpdateHistoryTool.createDelHistory(oShift, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	 data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED)+_oEx.getMessage());
        }
    }

    private void setProperties (Shift _oShift, RunData data) 
    	throws Exception
    {
		try
		{
	    	data.getParameters().setProperties(_oShift);
    	}
    	catch (Exception _oEx)
    	{
    		
        	throw new Exception (LocaleTool.getString(s_SET_PROP_FAILED)+_oEx.getMessage());
        }
    }
}
