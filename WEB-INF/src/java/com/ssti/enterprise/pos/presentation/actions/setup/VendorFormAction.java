package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.VendorLoader;
import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.om.AccountPayablePeer;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorBalancePeer;
import com.ssti.enterprise.pos.om.VendorPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.AccountPayableTool;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class VendorFormAction extends SecureAction
{
	
   	private static final String s_VIEW_PERM    = "View Vendor Data";
	private static final String s_UPDATE_PERM  = "Update Vendor Data";
	
    private static final String[] a_UPDATE_PERM  = {s_VIEW_PERM, s_UPDATE_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
	private Log log = LogFactory.getLog(VendorFormAction.class);
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(!data.getParameters().getString("eventSubmit_doFind","").equals(""))
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
        else if(!data.getParameters().getString("eventSubmit_doInsert","").equals("") ||
                !data.getParameters().getString("eventSubmit_doUpdate","").equals("") ||
                !data.getParameters().getString("eventSubmit_doDelete","").equals("") ||
				!data.getParameters().getString("eventSubmit_doLoadFile","").equals("")
			)
        {
        	return isAuthorized (data, a_UPDATE_PERM);
    	}
        return false;
    }
    
	public void doFind(RunData data, Context context)
	    throws Exception
	{
		try 
		{
			int iCondition = data.getParameters().getInt ("Condition");
			int iStatus = data.getParameters().getInt ("Status");
			int iBalance = data.getParameters().getInt ("CurrentBalance");
			String sTypeID = data.getParameters().getString ("VendorTypeId");
			String sKeyword = data.getParameters().getString ("Keywords");
			String sCurrID = data.getParameters().getString("CurrencyId");
			int iItemType = data.getParameters().getInt("ItemType", 0);
			int iLimit = data.getParameters().getInt("ViewLimit", 25);
			
			String sUserName = data.getUser().getName();
			String sEmpID = "";
			if (!data.getACL().hasPermission("Access All Vendor"))
			{
				Employee oEmp = EmployeeTool.getEmployeeByUsername(sUserName);
				if (oEmp != null) sEmpID = oEmp.getEmployeeId();
			}
			
			LargeSelect vVendor = VendorTool.find(iCondition, iStatus, iBalance, sKeyword, sTypeID, sCurrID, sEmpID, iItemType, iLimit);
			data.getUser().setTemp("findVendorResult", vVendor);	
	    }
	    catch (Exception _oEx) 
	    {
	    	_oEx.printStackTrace();
	    	String sError = LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage();
	    	data.setMessage (sError);
	    }
	}    
    
	public void doInsert(RunData data, Context context)
        throws Exception
    {
		Vendor oVendor = null;
		try
		{
			double dOpeningBalance = data.getParameters().getDouble("OpeningBalance");			
			oVendor = new Vendor();
			setProperties(oVendor, data);
			oVendor.setVendorId(IDGenerator.generateSysID());
			oVendor.setOpeningBalance(bd_ZERO); //to prevent update OB error	
			oVendor.setUpdateDate(new Date());
			oVendor.setLastUpdateLocationId(PreferenceTool.getLocationID());
			oVendor.setCreateBy(data.getUser().getName());					
			oVendor.save();
			
			VendorManager.getInstance().refreshCache(oVendor);
			
			if (dOpeningBalance != 0)
	        {
            	AccountPayableTool.validateDate(oVendor.getAsDate(), null);
				oVendor.setOpeningBalance(new BigDecimal(dOpeningBalance)); //will be saved AP below
				AccountPayableTool.updateOpeningBalance (oVendor, false, data.getUser().getName());
				VendorManager.getInstance().refreshCache(oVendor);
	        }			
	        data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	context.put("Vendor",oVendor);
        	
        	log.error(_oEx);
        	_oEx.printStackTrace();
        	
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doUpdate(RunData data, Context context)
        throws Exception
    {
 		String sID = data.getParameters().getString("ID");
 		String sCode = data.getParameters().getString("VendorCode");
 		boolean bUpdateOB = data.getParameters().getBoolean("UpdateOB");
    	try
    	{
	    	Vendor oVendor = VendorTool.getVendorByID(sID);
            if (oVendor != null)
            {
            	Vendor oOld = oVendor.copy();
            	
            	String sOldCurrID = oVendor.getDefaultCurrencyId();
                double dOld = oVendor.getOpeningBalance().doubleValue();        	                    
                Date dOldAsDate = oVendor.getAsDate();
        	    setProperties (oVendor, data); //set properties including ob and date
        	    
    			String sNewCurrID = oVendor.getDefaultCurrencyId();
    			if (!sOldCurrID.equals(sNewCurrID)) 
    			{   
    				//if currency change then check if ob AP trans exists
    				if (AccountPayableTool.isTransExist(oVendor.getVendorId()))
    				{
    					//remove from cache, because object in cache has been altered by set properties
    					VendorManager.getInstance().refreshCache(oVendor.getVendorId(), oVendor.getVendorCode());
    					data.setMessage(LocaleTool.getString("curr_maynot_changed"));
    					return;
    				}
    			}						  	    
        	    
                Date dNewAsDate = oVendor.getAsDate();                
                double dNew = oVendor.getOpeningBalance().doubleValue();
                double dChange = dNew - dOld;
                if (bUpdateOB && (dChange != 0 || !StringUtil.isEqual (dOldAsDate, dNewAsDate)))
                {
                	AccountPayableTool.validateDate(oVendor.getAsDate(), null);
        	        AccountPayableTool.updateOpeningBalance (oVendor, true, data.getUser().getName());
                }
                oVendor.setUpdateDate(new Date());
    			oVendor.setLastUpdateLocationId(PreferenceTool.getLocationID());
    			oVendor.setLastUpdateBy(data.getUser().getName());
        	    oVendor.save();
        	    
        	    UpdateHistoryTool.createHistory(oOld, oVendor, data.getUser().getName(), null);
                
	            VendorManager.getInstance().refreshCache(oVendor);
                data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
            }
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	_oEx.printStackTrace();
        	VendorManager.getInstance().refreshCache(sID,sCode);
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }


 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("vendor_price_list","vendor_id",sID)
    			&& SqlUtil.validateFKRef("purchase_order","vendor_id",sID)
    			&& SqlUtil.validateFKRef("purchase_receipt","vendor_id",sID)
    			&& SqlUtil.validateFKRef("purchase_invoice","vendor_id",sID)
    			&& SqlUtil.validateFKRef("purchase_return","vendor_id",sID)
    			&& SqlUtil.validateFKRef("ap_payment","vendor_id",sID)
    			&& SqlUtil.validateFKRef("debit_memo","vendor_id",sID))
    		{
    		        	    
    			Vendor oVend = VendorTool.getVendorByID(sID);    			
    			
	    	    Criteria oCrit = new Criteria();
                oCrit.add(VendorBalancePeer.VENDOR_ID, sID);
	        	VendorBalancePeer.doDelete(oCrit);
                
                oCrit = new Criteria();
	        	oCrit.add(AccountPayablePeer.VENDOR_ID, sID);
	        	AccountPayablePeer.doDelete(oCrit);
                
	        	String sCode = VendorTool.getCodeByID(sID);
                oCrit = new Criteria();
	            oCrit.add(VendorPeer.VENDOR_ID, sID);
	            VendorPeer.doDelete(oCrit);
	            
	            UpdateHistoryTool.createDelHistory(oVend, data.getUser().getName(), null);
	            
	        	VendorManager.getInstance().refreshCache(sID, sCode);
        	    data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else 
        	{
				data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}	        
        }
        catch (Exception _oEx)
        {
            log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

	public void setProperties ( Vendor _oVendor, RunData data)
    {
    	try
    	{
	    	data.getParameters().setProperties(_oVendor);
	    	_oVendor.setAsDate(CustomParser.parseDate(data.getParameters().getString("AsDate")));
    	}
    	catch (Exception _oEx)
    	{
    		log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }

    public void doLoadfile (RunData data, Context context)
		throws Exception
    {
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			VendorLoader oLoader = new VendorLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			data.setMessage ( LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
			log.error(_oEx);
			_oEx.printStackTrace();
		}
	}
}