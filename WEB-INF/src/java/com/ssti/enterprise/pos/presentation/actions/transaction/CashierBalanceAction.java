package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CashierBalance;
import com.ssti.enterprise.pos.om.CashierBalancePeer;
import com.ssti.enterprise.pos.om.CashierBills;
import com.ssti.enterprise.pos.om.CashierBillsPeer;
import com.ssti.enterprise.pos.om.CashierCoupon;
import com.ssti.enterprise.pos.om.CashierCouponPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.StringUtil;

public class CashierBalanceAction extends SecureAction
{	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	AccessControlList oACL = data.getACL();
    	int iOp = data.getParameters().getInt("op",1);
    	if (iOp == 1)
    	{
	    	if (oACL != null && oACL.getRoles().containsName("Cashier"))
	    	{
	    		return true;
	    	}	    	
    	}
    	else if (iOp == 4)
    	{
    		return true;
    	}    
    	return false;
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	int iOp = data.getParameters().getInt("op",1);
    	if (iOp == 1)
    	{
    		doSave(data, context);
    	}
    	else
    	{
    		if (iOp == 4 && data.getParameters().getBoolean("del"))
    		{
    			doDelete(data, context);
    		}
    	}
    }
    
	public void doSave(RunData data, Context context)
        throws Exception
    {
		int iType = data.getParameters().getInt("TransactionType");
		String sType = "opening_balance";
		if (iType == CashierBalanceTool.i_CLOSE) sType = "ending_balance";

		String sID = data.getParameters().getString("id");
		CashierBalance oBalance = null;

		try
		{
			if (StringUtil.isNotEmpty(sID))
			{
				oBalance = CashierBalanceTool.getByID(sID);
			}
			if (oBalance == null) oBalance = new CashierBalance();
			
			data.getParameters().setProperties(oBalance);
			//Generate Sys ID
			System.out.println("TtlCash" + data.getParameters().getBigDecimal("TtlCash"));
			oBalance.setCashierBalanceId (IDGenerator.generateSysID());
			oBalance.setTransAmount(new BigDecimal(data.getParameters().getDouble("TtlCash")));
			System.out.println("TransAmount" + oBalance.getTransAmount());
			
			oBalance.setTransactionDate  (new Date());
			oBalance.setHostName(data.getRemoteHost());
			oBalance.setIpAddress(data.getRemoteAddr());
			oBalance.setAuditLog("");
			oBalance.save();
			
			if(iType == CashierBalanceTool.i_CLOSE)
			{
				int iTotalCB = data.getParameters().getInt("TotalCB");
				for (int i = 0; i < iTotalCB; i++)
				{
					CashierBills oBills = new CashierBills();
					oBills.setCashierBillsId(IDGenerator.generateSysID());
					oBills.setCashierBalanceId(oBalance.getCashierBalanceId());
					oBills.setPaymentBillsId(data.getParameters().getString("BillId" + i));
					oBills.setBillsAmount(data.getParameters().getInt("BillAmt" + i));
					oBills.setQty(data.getParameters().getInt("Qty" + i));
					oBills.setAmount(data.getParameters().getBigDecimal("Amount" + i));
					oBills.save();
				}

				int iTotalVch = data.getParameters().getInt("TotalVch");
				for (int i = 0; i < iTotalVch; i++)
				{
					CashierCoupon oCpn = new CashierCoupon();					
					oCpn.setCashierCouponId(IDGenerator.generateSysID());
					oCpn.setCashierBalanceId(oBalance.getCashierBalanceId());
					oCpn.setCouponType(1);
					oCpn.setCouponValue(data.getParameters().getInt("VchVal" + i));
					oCpn.setTrQty(data.getParameters().getInt("VchQty" + i));
					oCpn.setTrAmount(data.getParameters().getBigDecimal("VchAmt" + i));
					oCpn.setInQty(data.getParameters().getInt("VinQty" + i));
					oCpn.setInAmount(data.getParameters().getBigDecimal("VinAmt" + i));					
					oCpn.save();
				}			
				
				int iTotalCpn = data.getParameters().getInt("TotalCpn");
				for (int i = 0; i < iTotalVch; i++)
				{
					CashierCoupon oCpn = new CashierCoupon();					
					oCpn.setCashierCouponId(IDGenerator.generateSysID());
					oCpn.setCashierBalanceId(oBalance.getCashierBalanceId());
					oCpn.setCouponType(2);
					oCpn.setCouponValue(data.getParameters().getInt("CpnVal" + i));
					oCpn.setTrQty(data.getParameters().getInt("CpnQty" + i));
					oCpn.setTrAmount(data.getParameters().getBigDecimal("CpnAmt" + i));
					oCpn.setInQty(data.getParameters().getInt("CinQty" + i));
					oCpn.setInAmount(data.getParameters().getBigDecimal("CinAmt" + i));					
					oCpn.save();
				}
				context.put("sID", oBalance.getCashierBalanceId());
				context.put("oCloseTrans", oBalance);
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append (LocaleTool.getString(sType))
			  .append (" ")
			  .append (LocaleTool.getString(s_SAVE_SUCCESS));
						
			data.setMessage(sb.toString());
        }
        catch(Exception _oEx)
        {
			StringBuilder sb = new StringBuilder();
			sb.append (LocaleTool.getString(sType))
			  .append (" ")
			  .append (LocaleTool.getString(s_SAVE_FAILED));

        	_oEx.printStackTrace();
	       	data.setMessage(sb.toString() + _oEx.getMessage());
        }
    }
	
	public void doDelete(RunData data, Context context)
        throws Exception
    {
		String sID = data.getParameters().getString("id");
		System.out.println("sID " + sID);
		CashierBalance oClose = CashierBalanceTool.getByID(sID);
		System.out.println(oClose);
		if (StringUtil.isNotEmpty(sID) && oClose != null)
		{
			try 
			{
				if(!oClose.isNextOpenExists())
				{
					Criteria oCrit = new Criteria();
					oCrit.add(CashierBillsPeer.CASHIER_BALANCE_ID, sID);
					CashierBillsPeer.doDelete(oCrit);

					oCrit = new Criteria();
					oCrit.add(CashierCouponPeer.CASHIER_BALANCE_ID, sID);
					CashierCouponPeer.doDelete(oCrit);

					oCrit = new Criteria();
					oCrit.add(CashierBalancePeer.CASHIER_BALANCE_ID, sID);
					CashierBalancePeer.doDelete(oCrit);
					
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
				}
				else
				{
					data.setMessage("ERROR: Can not delete Close Cashier, There are Open Transaction afterward ");
				}				
			} 
			catch (Exception e) 
			{
				data.setMessage("ERROR: " + e.getMessage());
				log.error(e);				
			}
		}
    }
}