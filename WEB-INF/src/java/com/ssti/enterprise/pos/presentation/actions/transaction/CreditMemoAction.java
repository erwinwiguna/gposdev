package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.CreditMemoLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class CreditMemoAction extends TransactionSecureAction
{
    private static final String s_VIEW_PERM    = "View Credit Memo";
	private static final String s_CREATE_PERM  = "Create Credit Memo";
	private static final String s_CANCEL_PERM  = "Cancel Credit Memo";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
        	return isAuthorized (data, a_CREATE_PERM);
    	}
    	else if (iSave == 2) 
    	{
        	return isAuthorized (data, a_CANCEL_PERM);
    	}
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {	
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
    		doSave(data,context);
    	}
    	else if (iSave == 2) 
    	{
    		doCancel(data,context);
    	}
    	else
    	{
    		doFind(data, context);
    	}
    }
    
    public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);			
			int iStatus = data.getParameters().getInt("Status");
			String sCustID = data.getParameters().getString ("CustomerId");
			LargeSelect vTR = CreditMemoTool.findData (
				iCond, sKeywords, sCustID, sLocationID,
				dStart, dEnd, iLimit, iStatus, sCurrencyID 
			);
			data.getUser().setTemp("findCreditMemoResult", vTR);
		}
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }

	public void doSave(RunData data, Context context)
        throws Exception
    {
		try
		{
			CreditMemo oCM = null;
			String sID = data.getParameters().getString("ID");
			if (StringUtil.isNotEmpty(sID))
			{
				oCM = CreditMemoTool.getCreditMemoByID(sID);				
			}
			else
			{
				oCM = new CreditMemo();
			}
			setProperties (oCM, data);
			double dAmountBase = oCM.getAmount().doubleValue() * oCM.getCurrencyRate().doubleValue();
			oCM.setAmountBase(new BigDecimal(dAmountBase));			
	        CreditMemoTool.saveCM (oCM, false, null);
	        context.put ("CreditMemo", oCM);
        	data.setMessage(LocaleTool.getString("cm_save_success"));
        }
        catch(Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("cm_save_failed"), _oEx);
        }
    }

	public void doCancel(RunData data, Context context)
	    throws Exception
	{
		try
		{
			String sCMID = data.getParameters().getString("CreditMemoId");
			String sCancelRemark = data.getParameters().getString("Remark");
			CreditMemo oCM = CreditMemoTool.getCreditMemoByID(sCMID);
			if (oCM != null)
			{
		        oCM.setRemark(oCM.getRemark() + "\n" + sCancelRemark);
				CreditMemoTool.cancelCM(oCM, data.getUser().getName(), null);
		    	data.setMessage(LocaleTool.getString("cm_cancel_success"));
			}
	    }
	    catch(Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("cm_cancel_failed"), _oEx);
	    }
	}

    private void setProperties (CreditMemo _oCM, RunData data)
    {
		try
		{
			data.getParameters().setProperties(_oCM);
	    	_oCM.setTransactionDate(CustomParser.parseDate(data.getParameters().getString("TransactionDate")));
	    	_oCM.setDueDate(CustomParser.parseDate(data.getParameters().getString("DueDate")));
	    	_oCM.setCustomerName(CustomerTool.getCustomerNameByID(data.getParameters().getString("CustomerId")));
		}
    	catch (Exception _oEx)
    	{
    		log.error(_oEx);
    		_oEx.printStackTrace();
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
	     	
	     	String sLoader = data.getParameters().getString("loader");
	     	TransactionLoader oLoader = null;
	     	if (StringUtil.isNotEmpty(sLoader))
	     	{
	     		oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
	     	}
	     	else
	     	{
				oLoader = new CreditMemoLoader(data.getUser().getName());
	     	}
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());
	    	context.put ("LoadResult", oLoader.getResult());
		}
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}       
}