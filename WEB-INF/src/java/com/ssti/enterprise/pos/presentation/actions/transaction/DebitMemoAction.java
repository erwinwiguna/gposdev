package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.DebitMemoLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class DebitMemoAction extends TransactionSecureAction
{
    private static final String s_VIEW_PERM    = "View Debit Memo";
	private static final String s_CREATE_PERM  = "Create Debit Memo";
	private static final String s_CANCEL_PERM  = "Cancel Debit Memo";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
        	return isAuthorized (data, a_CREATE_PERM);
    	}
    	if (iSave == 2) 
    	{
        	return isAuthorized (data, a_CANCEL_PERM);
    	}    	
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
    		doSave(data,context);
    	}
    	else if (iSave == 2) 
    	{
    		doCancel(data,context);
    	}
    	else
    	{
    		doFind(data,context);
    	}
    }
    
    public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);
			int iStatus = data.getParameters().getInt("Status");
			String sVendorID = data.getParameters().getString ("VendorId");
			LargeSelect vTR = DebitMemoTool.findData (
				iCond, sKeywords, sVendorID, sLocationID,
				dStart, dEnd, iLimit, iStatus, sCurrencyID 
			);			
			data.getUser().setTemp("findDebitMemoResult", vTR);
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }

	public void doSave(RunData data, Context context)
        throws Exception
    {
		try
		{
			DebitMemo oDM = new DebitMemo();
			String sID = data.getParameters().getString("ID");
			if (StringUtil.isNotEmpty(sID))
			{
				oDM = DebitMemoTool.getDebitMemoByID(sID);				
			}
			else
			{
				oDM = new DebitMemo();
			}			
			setProperties (oDM, data);
			double dAmountBase = oDM.getAmount().doubleValue() * oDM.getCurrencyRate().doubleValue();
			oDM.setAmountBase(new BigDecimal(dAmountBase));
	        DebitMemoTool.saveDM (oDM, null);
	        context.put ("DebitMemo", oDM);
        	data.setMessage(LocaleTool.getString("dm_save_success"));
        }
        catch(Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("dm_save_failed"), _oEx);
        }
    }

	public void doCancel(RunData data, Context context)
	    throws Exception
	{
		try
		{			
			String sDMID = data.getParameters().getString("DebitMemoId");
			String sCancelRemark = data.getParameters().getString("Remark");
			DebitMemo oDM = DebitMemoTool.getDebitMemoByID(sDMID);
			if (oDM != null)
			{
		        oDM.setRemark(oDM.getRemark() + "\n" + sCancelRemark);
				DebitMemoTool.cancelDM(oDM, data.getUser().getName(), null);
	    		data.setMessage(LocaleTool.getString("dm_cancel_success"));
			}
	    }
	    catch(Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("dm_cancel_failed"), _oEx);
	    }
	}

    private void setProperties (DebitMemo _oDM, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oDM);
	    	_oDM.setTransactionDate(CustomParser.parseDate(data.getParameters().getString("TransactionDate")));
	    	_oDM.setVendorName(VendorTool.getVendorNameByID(data.getParameters().getString("VendorId")));
	    	_oDM.setDueDate(CustomParser.parseDate(data.getParameters().getString("DueDate")));

		}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
	     	
	     	String sLoader = data.getParameters().getString("loader");
	     	TransactionLoader oLoader = null;
	     	if (StringUtil.isNotEmpty(sLoader))
	     	{
	     		oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
	     	}
	     	else
	     	{
				oLoader = new DebitMemoLoader(data.getUser().getName());
	     	}
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());
	    	context.put ("LoadResult", oLoader.getResult());
		}
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}    
}