package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-10-28
 * -Fix isAuthorized Permission Bug
 * 
 * </pre><br>
 */
public class DeliveryOrderAction extends TransactionSecureAction
{
	private static final String s_VIEW_PERM   = "View Delivery Order";
	private static final String s_CREATE_PERM = "Create Delivery Order";
	private static final String s_CONFIRM_PERM = "Confirm Delivery Order";
	private static final String s_CANCEL_PERM = "Cancel Delivery Order";
	
    private static final String[] a_CREATE_PERM  = {s_SALES_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CONFIRM_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CONFIRM_PERM};
    private static final String[] a_CANCEL_PERM  = {s_SALES_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_SALES_PERM, s_VIEW_PERM};

	private static Log log = LogFactory.getLog ( DeliveryOrderAction.class );
	
    private HttpSession oDOSes = null;
    private DeliveryOrder oDO;
    private List vDOD;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iStatus = data.getParameters().getInt("Status");
    	int iSave = data.getParameters().getInt("save"); 
        if (iSave == 1 &&  iStatus == i_DO_NEW) 
	   	{
        	return isAuthorized (data, a_CREATE_PERM);
	   	}
	    else if (iSave == 1 && iStatus == i_DO_DELIVERED)
	   	{
	    	return isAuthorized (data, a_CONFIRM_PERM);
	   	}
	    else if	(iSave == 2 && iStatus == i_DO_CANCELLED)
	    {
	    	return isAuthorized (data, a_CANCEL_PERM);
	   	}
	   	else
	   	{
	   		return isAuthorized (data, a_VIEW_PERM);
	   	}
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {    	
    	int iStatus = data.getParameters().getInt("Status");
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1 &&  iStatus == i_DO_NEW) 
	   	{
	   		doSave (data,context);
	   	}
	    else if (iSave == 1 && iStatus == i_DO_DELIVERED)
	   	{
	   		doConfirm (data,context);
	   	}
	    else if	(iSave == 2 && iStatus == i_DO_CANCELLED)
	    {
	   		doCancel (data,context);
	   	}
	   	else
	   	{
	   		doFind (data,context);
	   	}
    }

    public void doFind ( RunData data, Context context )
        throws Exception
    {
    	super.doFind(data);
    	String sCustomerID = data.getParameters().getString("CustomerId", "");
    	String sLocationID = data.getParameters().getString("LocationId", "");
    	String sSalesID = data.getParameters().getString("SalesId", "");
		int iStatus = data.getParameters().getInt("Status");
		LargeSelect vTR = DeliveryOrderTool.findData (
			iCond, sKeywords, dStart, dEnd, sCustomerID, sLocationID,  sSalesID, iStatus, iLimit, iGroupBy,  sCurrencyID);
		
		data.getUser().setTemp("findDeliveryOrderResult", vTR);
	}

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			initSession ( data );
			DeliveryOrderTool.updateDetail(vDOD,data);
			DeliveryOrderTool.setHeaderProperties (oDO, vDOD, data);
			prepareTransData ( data );
			DeliveryOrderTool.saveData (oDO, vDOD, null, false);
			data.setMessage(LocaleTool.getString("do_save_success"));
	       	resetSession ( data );
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("do_save_failed"), _oEx);
        }
    }

    public void doConfirm ( RunData data, Context context )
        throws Exception
    {
		try
		{
			initSession ( data );
			int iStatBeforeUpdate = oDO.getStatus(); //used if delivered then cancelled
			if(iStatBeforeUpdate != i_DO_DELIVERED) // if cancel after delivered, no need to update detail
			{
			    DeliveryOrderTool.updateDetail(vDOD,data);
			}
			DeliveryOrderTool.setHeaderProperties (oDO, vDOD, data);
			prepareTransData ( data );
			boolean bCloseSO = data.getParameters().getBoolean("CloseSO");			
			if(data.getParameters().getInt("Status") == i_DO_DELIVERED)
			{
				oDO.setConfirmBy(data.getUser().getName());
				oDO.setConfirmDate(new Date());
				DeliveryOrderTool.saveData(oDO, vDOD, null, bCloseSO);
	        	data.setMessage(LocaleTool.getString("do_confirm_success"));
			}
	       	resetSession ( data );
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("do_confirm_failed"), _oEx);
        }
    }

    public void doCancel ( RunData data, Context context )
	    throws Exception
	{
		try
		{
			initSession ( data );
			DeliveryOrderTool.cancelDO(oDO, vDOD, data.getUser().getName(), null);	
	        data.setMessage(LocaleTool.getString("do_cancel_success"));
	    }
	    catch (Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("do_cancel_failed"), _oEx);
	    }
	}    
    
    private void initSession ( RunData data )
    	throws Exception
    {
    	synchronized (this)
    	{
    		oDOSes = data.getSession();
    		if ( oDOSes.getAttribute (s_DO) == null ||
    				oDOSes.getAttribute (s_DOD) == null )
    		{
    			throw new Exception ("Delivery Order Data Invalid");
    		}
    		oDO = (DeliveryOrder) oDOSes.getAttribute (s_DO);
    		vDOD = (List) oDOSes.getAttribute (s_DOD);
    	}
    }

    private void prepareTransData ( RunData data )
    	throws Exception
    {
    	//check transaction data
    	if ( oDO.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen
		if ( vDOD.size () < 1) {throw new Exception ("Total Item Sold < 1"); } //-- should never happen
    }

    private void resetSession ( RunData data )
    	throws Exception
    {
     	synchronized (this)
    	{
			oDOSes.setAttribute (s_DO, oDO );
			oDOSes.setAttribute (s_DOD, vDOD );
    	}
    }
}