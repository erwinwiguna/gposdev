package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.IssueReceiptLoader;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.framework.tools.StringUtil;

public class IssueReceiptAction extends TransactionSecureAction
{   
    private HttpSession oSes;  

    private static final String s_VIEW_PERM    = "View Issue Receipt";
	private static final String s_CREATE_PERM  = "Create Issue Receipt";
	private static final String s_CANCEL_PERM  = "Cancel Issue Receipt";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iSave = data.getParameters().getInt("save");
    	if(iSave == 1)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
        else if(iSave == 2)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
        int iSave = data.getParameters().getInt("save");
        if (iSave == 1) 
        {
    		doSave (data,context);
    	}
    	if (iSave == 2) 
        {
    		doCancel (data,context);
    	}
    }

	public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);
			
			String sLocationID = data.getParameters().getString("LocationId","");
			String sEntityID = data.getParameters().getString("CrossEntityId","");
			int iTransType = data.getParameters().getInt("TransactionType");
			int iStatus = data.getParameters().getInt("Status");
			boolean bIsTransfer = data.getParameters().getBoolean("InternalTransfer");
			
			String sAdjTypeID = data.getParameters().getString("AdjustmentTypeId","");			
						
			LargeSelect vData = IssueReceiptTool.findData (iCond, sKeywords, dStart ,dEnd, sLocationID, 
														   sAdjTypeID, iTransType, iStatus, iLimit, bIsTransfer);
			
			data.getUser().setTemp("findIssueReceiptResult", vData);
        }
        catch(Exception _oEx)
        {
	       	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }

	public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{   
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			String sLocationID = data.getParameters().getString("LocationId");
            boolean bForceZero = data.getParameters().getBoolean("ZeroCost");            
			
	     	InputStream oBufferStream  = new BufferedInputStream(oFileItem.getInputStream());
			IssueReceiptLoader oLoader = new IssueReceiptLoader(sLocationID,bForceZero);
			oLoader.loadData (oBufferStream);
			
			oSes = data.getSession();
			
			IssueReceipt oIssueReceipt = new IssueReceipt();
			data.getParameters().setProperties (oIssueReceipt);
			//oIssueReceipt.setLocationId(sLocationId);
			oSes.setAttribute (s_IR,oIssueReceipt);
			oSes.setAttribute (s_IRD, oLoader.getListResult());
			
			if (oLoader.getTotalError() > 0)
			{
				data.setMessage(oLoader.getErrorMessage());
			}
			else 
			{
				context.put("bClose", Boolean.valueOf(true));
			}
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}
    
    public void doSave(RunData data, Context context)
        throws Exception
    {
        try
        {
            IssueReceipt oIR = (IssueReceipt) data.getSession().getAttribute(s_IR);
            List vIRD = (List) data.getSession().getAttribute(s_IRD);           
            IssueReceiptTool.updateDetail(data, vIRD);
            IssueReceiptTool.setHeader(data, oIR);
            
            if(!oIR.getInternalTransfer()) //if not internal transfer
            {
                if (oIR.getStatus() == i_PENDING)
                {
                    oIR.setUserName(data.getUser().getName());
                }
                if (oIR.getStatus() == i_PROCESSED)
                {
                    oIR.setConfirmBy (data.getUser().getName());
                    oIR.setConfirmDate(new Date());
                }
                if (StringUtil.isEmpty(oIR.getAccountId()))
                {
                    data.setMessage("ERROR: Adjustment Account is NOT SET, Please Check Adjustment Type Setup");                
                }
                else
                {
                    IssueReceiptTool.saveData (oIR, vIRD, null);
                    data.setMessage(LocaleTool.getString("iru_save_success"));              
                }
            }
            else
            {
                data.setMessage("ERROR: Invalid Transaction Type (Interbranch Transfer), Please Create New Transaction ");
            }
        }
        catch(Exception _oEx)
        {
            handleError (data, LocaleTool.getString("iru_save_failed"), _oEx);
        }
    }
    
	public void doCancel(RunData data, Context context)
	    throws Exception
	{
		try
		{
			IssueReceipt oIR = (IssueReceipt) data.getSession().getAttribute(s_IR);
	    	List vIRD = (List) data.getSession().getAttribute(s_IRD);
	    	
	    	if(vIRD.size() > 1000)
	    	{
	    		IssueReceiptTool.cancelPartial(oIR.getIssueReceiptId(), data.getUser().getName(), 500);	
	    		oIR = IssueReceiptTool.getHeaderByID(oIR.getIssueReceiptId());
	    		vIRD = IssueReceiptTool.getDetailsByID(oIR.getIssueReceiptId()); 	    		
	    	}
	    	else
	    	{
	    		IssueReceiptTool.cancelTrans(oIR, vIRD, data.getUser().getName(), null);
	    	}
			data.setMessage(LocaleTool.getString("iru_cancel_success"));
	    }
	    catch(Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("iru_cancel_failed"), _oEx);
	    }
	}	
	
}