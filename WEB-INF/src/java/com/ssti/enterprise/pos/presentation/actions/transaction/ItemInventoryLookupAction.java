package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.ItemInventoryLoader;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.framework.tools.StringUtil;

public class ItemInventoryLookupAction extends ItemLookupAction
{
	ValueParser formData = null; 
    public void doPerform(RunData data, Context context)
        throws Exception
    {    	
    	if(data.getParameters().getInt("save") == 1)
        {
            doSave(data, context);
        }
    	else
    	{
            doFind(data, context);
    	}
    }
    
	public void doFind(RunData data, Context context)
        throws Exception
    {
		formData = data.getParameters();
		try 
		{
			int iCondition 	      = formData.getInt  	("Condition");
			String sKeyword       = formData.getString  ("Keywords");
			String sKatID 	      = formData.getString  ("KategoriId");		
			int iSortBy 	      = formData.getInt  	("SortBy", 2);
			int iViewLimit 	      = formData.getInt  	("ViewLimit", 25);
			String sGrpID         = formData.getString  ("groupid");
			String sVendorID      = formData.getString  ("VendorId","");
			String sLocationID    = formData.getString  ("LocationId","");
			int iStockStatus	  = formData.getInt     ("StockStatus",-1);
			int iItemStatus 	  = formData.getInt     ("ItemStatus",1);
			int iStatusInLocation = formData.getInt     ("StatusInLocation",-1);
            
			LargeSelect vItem     = ItemTool.findData ( iCondition, 
														sKeyword, sKatID, 
														iSortBy, 
														iViewLimit, 
														sGrpID, 
													    sVendorID, 
														sLocationID, 
														iStockStatus, 
														iItemStatus, 
														iStatusInLocation,
														i_INVENTORY_TRANSACTION);
            
			data.getUser().setTemp("findItemResult", vItem);	
        }
        catch (Exception _oEx) 
        {
        	String sError = LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage();
        	data.setMessage (sError);
        }
    }    
    public void doSave(RunData data, Context context)
        throws Exception
    {
        updateInventoryLocation(data);
    	super.doFind(data,context);
    }
    
    private void updateInventoryLocation(RunData data)
    	throws Exception
    {
    	formData = data.getParameters();
    	try
    	{
    		int iSize = data.getParameters().getInt("DataSize");
    		String sLocationID = formData.getString("LocationId");
    		String sLocationName = formData.getString("LocationName","");
    		if(StringUtil.isEmpty(sLocationName)) sLocationName = LocationTool.getLocationNameByID(sLocationID);

    		for ( int i = 0; i < iSize; i++ )
    		{
    		    String sItemInvID 	= formData.getString("ItemInventoryId"+(i+1),"");
    		    String sItemID 		= formData.getString("ItemId"+(i+1));
    		    String sItemCode 	= formData.getString("ItemCode"+(i+1));
    		    BigDecimal dMaxQty 	= formData.getBigDecimal("max"+(i+1));
    		    BigDecimal dMinQty 	= formData.getBigDecimal("min"+(i+1));
    		    BigDecimal dRPQty 	= formData.getBigDecimal("reorder"+(i+1));
    		    String sRack 		= formData.getString("rack"+(i+1));
    		    boolean bActive 	= formData.getBoolean("statusitem"+(i+1));
    		        		    
    		    if(sItemInvID.equals(""))
    		    {
                    if (dMinQty.doubleValue() > 0 || dRPQty.doubleValue() > 0 || dMaxQty.doubleValue() > 0 || 
                        StringUtil.isNotEmpty(sRack) || !bActive);
                    {
                        ItemInventoryTool.saveNew(sLocationID,
    		        						      sLocationName,
    		        						      sItemID,
    		        						      sItemCode,
    		        						      dMaxQty,
    		        						      dMinQty,
    		        						      dRPQty,
    		        						      sRack,
    		        						      bActive);
                    }
    		    }
    		    else
    		    {
    		        ItemInventoryTool.updateItemInventoryByID(sItemInvID,
    		        										  dMaxQty,
															  dMinQty,
															  dRPQty,
															  sRack,
															  bActive);
    		    }
    		    
    		    if( PreferenceTool.getLocationFunction() == i_HEAD_OFFICE && 
    		    		PreferenceTool.getLocationID().equals(sLocationID) )
    		    {
    		        Item oItem = ItemTool.getItemByID(sItemID);
    		        oItem.setMinimumStock(dMinQty);
    		        oItem.setMaximumStock(dMaxQty);
    		        oItem.setReorderPoint(dRPQty);
    		        oItem.save();
    		    }
			}
    		data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
    	}
    	catch(Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		log.error (_oEx);
    		throw new Exception(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
    	}
	}
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			ItemInventoryLoader oLoader = new ItemInventoryLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}
    
}