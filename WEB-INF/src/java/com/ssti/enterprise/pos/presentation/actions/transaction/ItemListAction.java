package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.ConvertExcel;
import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/transaction/ItemListAction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ItemListAction.java,v 1.5 2007/03/05 16:04:24 albert Exp $
 *
 * $Log: ItemListAction.java,v $
 * 
 * 2017-03-12
 * -add function to import item list excel to merge with current list
 *
 */
public class ItemListAction extends TransactionSecureAction
{
	private static Log log = LogFactory.getLog ( ItemListAction.class );
	
    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
         	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			
         	String sFileDest = data.getParameters().getString("FileDestination","");
         	int iFileType = data.getParameters().getInt("ExcelType",1);
         	
         	if (iFileType == 1) //FROM MATRIX
         	{
	         	ConvertExcel oLoader = new ConvertExcel(iFileType, sFileDest);
				oLoader.loadData (oBufferStream);
				if (!sFileDest.equals(""))
				{
					oLoader.saveWB();
				}
				else
				{
					data.getSession().setAttribute (s_ITMD, oLoader.getItemList()); 
				}
				if (log.isDebugEnabled()) oLoader.writeResult();
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
				context.put("result", oLoader.getResult());						
         	}         	         	         
		}
        catch (Exception _oEx) 
        {
        	String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
        	log.error ( sError, _oEx );
        	data.setMessage (sError);
        }
    }
    
    protected void processResult(RunData data, List _vResult)
    {
		if(_vResult != null && _vResult.size() > 0)
		{
			List vExist = (List)data.getSession().getAttribute(s_ITMD);
			if(vExist == null) vExist = new ArrayList();
			for(int i = 0; i < _vResult.size(); i++ )
			{
				ItemList oIL = (ItemList) _vResult.get(i);
				if(!isExist(oIL, vExist))
				{
					vExist.add(0, oIL);
				}
			}
			data.getSession().setAttribute(s_ITMD, vExist);			
		}
    }
    
    protected boolean isExist (ItemList oIL, List _vExist)
    {
    	for (int j = 0; j < _vExist.size(); j++)
    	{
    		ItemList oExist = (ItemList) _vExist.get(j);
    		if ( (oExist.getItemId().equals(oIL.getItemId())))
    		{
    			double dNewQty = oExist.getQty().doubleValue() + oIL.getQty().doubleValue();
    			oExist.setQty(new BigDecimal(dNewQty));
    			return true;
    		}
    	}
    	return false;
    }	
}