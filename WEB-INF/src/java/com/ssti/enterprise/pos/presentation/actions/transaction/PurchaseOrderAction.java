package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.PurchaseOrderItemLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.PurchaseOrderLoader;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/transaction/PurchaseOrderAction.java,v $
 * Purpose: PO Action handler
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseOrderAction.java,v 1.14 2009/05/04 01:58:24 albert Exp $
 *
 * $Log: PurchaseOrderAction.java,v $
 * Revision 1.14  2009/05/04 01:58:24  albert
 * *** empty log message ***
 *
 * Revision 1.13  2008/04/14 09:14:34  albert
 * *** empty log message ***
 *
 * Revision 1.12  2008/01/29 23:28:01  albert
 * *** empty log message ***
 *
 */
public class PurchaseOrderAction extends TransactionSecureAction
{
	private static final String s_VIEW_PERM    = "View Purchase Order";
	private static final String s_CREATE_PERM  = "Create Purchase Order";
	private static final String s_CONFIRM_PERM = "Confirm Purchase Order";
	private static final String s_CANCEL_PERM  = "Cancel Purchase Order";
	
    private static final String[] a_CONFIRM_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_CONFIRM_PERM};
    private static final String[] a_CANCEL_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_CREATE_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_VIEW_PERM   = {s_PURCHASE_PERM, s_VIEW_PERM};
    	
    private static final Log log = LogFactory.getLog(PurchaseOrderAction.class);
    
    private HttpSession oPOSes = null;
    private PurchaseOrder oPO;
    private List vPOD;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {        
    	int iStatus = data.getParameters().getInt("TransactionStatus");
    	int iSave = data.getParameters().getInt("save");
    	if (iSave >= 1)
    	{
	    	if (iSave == 1 && iStatus < 1)
	    	{
	    	    return isAuthorized(data, a_CREATE_PERM);
	    	}
	    	else if (iSave == 1  && iStatus == 1) 
	    	{
	    	    return isAuthorized(data, a_CONFIRM_PERM);
	        }
	    	else if (iSave == 2) 
	    	{
	    	    return isAuthorized(data, a_CANCEL_PERM);
	        }
    	}
    	return isAuthorized(data, a_VIEW_PERM);
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	int iStatus = data.getParameters().getInt("TransactionStatus");
    	int iSave = data.getParameters().getInt("save");
    	
    	if (iSave == 1 && 
    			(iStatus == i_PO_NEW || iStatus == i_PO_ORDERED)) 
    	{
    		doSave (data,context);
    	}
    	else if (iSave == 1 && iStatus == i_PO_CLOSED) 
    	{
    		doClose (data,context);
    	}
    	else if (iSave == 2 && iStatus == i_PO_CANCELLED) 
    	{
    		doCancel (data,context);
    	}
    	else if (iSave == 2 && iStatus == 6) //unconfirm 
    	{
    		doUnconfirm (data,context);
    	}    	    	
    	else 
    	{
    		doFind (data,context);
    	}
    }

    public void doFind ( RunData data, Context context )
        throws Exception
    {
    	super.doFind(data);
    	
    	String sVendorID = data.getParameters().getString("VendorId", "");
    	boolean bAccessAllVendor = data.getParameters().getBoolean("AccessAllVendor");
		int iStatus = data.getParameters().getInt("Status");
		
		LargeSelect vTR = null;
		String sUserName = "";
		if(b_PURCHASE_RELATED_EMPLOYEE && !bAccessAllVendor)
		{
			sUserName = data.getUser().getName();
	    }
	    vTR = PurchaseOrderTool.findData (
	    	iCond, sKeywords, dStart, dEnd, sVendorID, sLocationID, 
	    	iStatus, iLimit, sUserName, sCurrencyID
		);
		data.getUser().setTemp("findPurchaseOrderResult", vTR);	
    }

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{	
			initSession ( data );	
			PurchaseOrderTool.updateDetail(vPOD,data);
			PurchaseOrderTool.setHeaderProperties (oPO, vPOD, data);
			prepareTransData ( data );
			PurchaseOrderTool.saveData (oPO, vPOD, null);
        	data.setMessage(LocaleTool.getString("po_save_success"));
	       	resetSession ( data );
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("po_save_failed"), _oEx);
        }
    }
    
    public void doClose ( RunData data, Context context )
        throws Exception
    {
    	try
    	{
    		initSession(data);
    		PurchaseOrderTool.closePO(oPO, data.getUser().getName());
    		data.setMessage(LocaleTool.getString("po_close_success"));
    	}
    	catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("po_close_failed"), _oEx);
        }
	}
    
    public void doUnconfirm ( RunData data, Context context )
	    throws Exception
	{
		try
		{
			initSession(data);
    		if (!PurchaseReceiptTool.isPOReceived(oPO.getPurchaseOrderId(), null))
			{
    			PurchaseOrderTool.unconfirmPO(oPO, data.getUser().getName());
    			data.setMessage(LocaleTool.getString("po_save_success"));
			}
			else
			{
				data.setMessage(LocaleTool.getString("po_cancel_received"));
			}
		}
		catch (Exception _oEx)
	    {
	    	handleError (data, LocaleTool.getString("po_save_failed"), _oEx);
	    }
	}

    public void doCancel ( RunData data, Context context )
    	throws Exception
	{
		try
		{
    		initSession(data);			
    		if (!PurchaseReceiptTool.isPOReceived(oPO.getPurchaseOrderId(), null))
			{
    			PurchaseOrderTool.cancelPO(oPO, data.getUser().getName());
	    		data.setMessage(LocaleTool.getString("po_cancel_success"));
			}
			else
			{
				data.setMessage(LocaleTool.getString("po_cancel_received"));
			}
		}
		catch (Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("po_cancel_failed"), _oEx);
	    }
	}

    private void initSession ( RunData data )
    	throws Exception
    {
    	synchronized (this) 
    	{
    		oPOSes = data.getSession();
    		if (oPOSes.getAttribute (s_PO) == null ||
    			oPOSes.getAttribute (s_POD) == null )
    		{
    			throw new Exception ("Purchase Order Data Invalid");
    		}
    		oPO = (PurchaseOrder) oPOSes.getAttribute (s_PO);
    		vPOD = (List) oPOSes.getAttribute (s_POD);
    	}
    }

    private void prepareTransData ( RunData data )
    	throws Exception
    {
    	//check transaction data
    	if ( oPO.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen
		if ( vPOD.size () < 1) {throw new Exception ("Total Item Purchased < 1"); } //-- should never happen
    }

    private void resetSession ( RunData data )
    	throws Exception
    {
    	synchronized (this) 
    	{
			oPOSes.setAttribute (s_PO, oPO );
			oPOSes.setAttribute (s_POD, vPOD );
    	}
    }
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			int iType = data.getParameters().getInt("ExcelType");			
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			if (iType == 1) //load PO item only
			{
				PurchaseOrder oPO = new PurchaseOrder();				
				data.getParameters().setProperties(oPO);

				PurchaseOrderItemLoader oLoader = new PurchaseOrderItemLoader(oPO);
				oLoader.loadData (oBufferStream);
								
				oPOSes = data.getSession();
				oPOSes.setAttribute (s_PO, oPO);
				oPOSes.setAttribute(s_POD, oLoader.getListResult());
												
				if (oLoader.getTotalError() > 0)
				{
					data.setMessage(oLoader.getErrorMessage());
				}
				else 
				{
					context.put("bClose", Boolean.valueOf(true));
				}				
			}
			else //load whole PO
			{
		     	PurchaseOrderLoader oLoader = new PurchaseOrderLoader(data.getUser().getName());
				oLoader.loadData (oBufferStream);
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
		    	context.put ("LoadResult", oLoader.getResult());		    		
			}
		}
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	} 
}