package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.SalesOrderItemLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.SalesOrderLoader;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.StringUtil;

public class SalesOrderAction extends TransactionSecureAction
{
    private HttpSession oSOSes = null;
    private SalesOrder oSO;
    private List vSOD;
    
   	private static final String s_VIEW_PERM    = "View Sales Order";
	private static final String s_CREATE_PERM  = "Create Sales Order";
	private static final String s_CONFIRM_PERM = "Confirm Sales Order";
	private static final String s_CANCEL_PERM  = "Cancel Sales Order";
	
	private static final String[] a_CONFIRM_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CONFIRM_PERM};
    private static final String[] a_CREATE_PERM  = {s_SALES_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_SALES_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_SALES_PERM, s_VIEW_PERM};

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	int iSave = data.getParameters().getInt("save"); 
    	int iStatus = data.getParameters().getInt("TransactionStatus");
        System.out.println("SO: iSave:" + iSave + " iStatus:" + iStatus);
    	if(iSave > 0)
        {        	
        	if(iStatus == i_SO_ORDERED)
        	{
        		return isAuthorized(data, a_CONFIRM_PERM);
        	}
        	if(iStatus == i_SO_NEW)
        	{
        		return isAuthorized(data, a_CREATE_PERM);
        	}
        }
        if(data.getParameters().getInt("closeSO") > 0)
        {
            return isAuthorized (data, a_CONFIRM_PERM);
        }
        if(data.getParameters().getInt("cancelSO") > 0)
        {
            return isAuthorized (data, a_CANCEL_PERM);
        }
        return isAuthorized (data, a_VIEW_PERM);    	
    }
   
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	if (data.getParameters().getInt("save") > 0 ) 
    	{
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("closeSO",0) > 0) 
    	{
    		doClose (data,context);
    	}
    	else if (data.getParameters().getInt("cancelSO",0) > 0) 
    	{
    		doCancel (data,context);
    	}
    	else 
    	{
    		doFind (data,context);
    	}
    }
    
    public void doFind ( RunData data, Context context )
        throws Exception
    {
    	super.doFind(data);
    	String sCustomerID = data.getParameters().getString("CustomerId", "");
    	String sSalesID = data.getParameters().getString("SalesId");
		int iStatus = data.getParameters().getInt("Status");
		LargeSelect vTR = SalesOrderTool.findData (
			iCond, sKeywords, dStart, dEnd, sCustomerID, sLocationID, sSalesID, iStatus, iLimit, iGroupBy, sCurrencyID);
		data.getUser().setTemp("findSalesOrderResult", vTR);	
    }

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			initSession(data);
			
			SalesOrderTool.updateDetail(vSOD,data);			
			SalesOrderTool.setHeaderProperties (oSO, vSOD, data);
			prepareTransData (data);

			SalesOrderTool.saveData (oSO, vSOD);
			data.setMessage(LocaleTool.getString("so_save_success"));
	       	resetSession ( data );
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("so_save_failed"), _oEx);
        }
    }

    public void doClose ( RunData data, Context context )
        throws Exception
    {
    	try
    	{
    		initSession(data);    		
    		String sSOID = data.getParameters().getString("SalesOrderId");    		    	
    		if(oSO != null && StringUtil.isEqual(oSO.getSalesOrderId(), sSOID))
    		{
    			oSO.setTransactionStatus(i_SO_CLOSED);
    			SalesOrderTool.closeSO(oSO, null);
    			data.setMessage(LocaleTool.getString("so_close_success"));
    		}
    	}
    	catch (Exception _oEx)
        {
    		handleError (data, LocaleTool.getString("so_close_failed"), _oEx);     
        }
	}

    public void doCancel ( RunData data, Context context )
    	throws Exception
	{
		try
		{
			initSession ( data );
			if (oSO != null)
			{
				String sSOID = data.getParameters().getString("SalesOrderId");
				int iDO = DeliveryOrderTool.getDOBySOID(sSOID, null).size();			
	    		if(iDO == 0)
	    		{
					SalesOrderTool.cancelSO(oSO, vSOD, data.getUser().getName());
					data.setMessage(LocaleTool.getString("so_cancel_success"));
	    		}
	    		else
	    		{
	    			data.setMessage(LocaleTool.getString("so_cancel_delivered"));
	    		}
			}
		}
		catch (Exception _oEx)
	    {
			handleError (data, LocaleTool.getString("so_cancel_failed"), _oEx);
	    }
	}

    private void initSession ( RunData data )
    	throws Exception
    {
    	synchronized (this)
    	{
    		oSOSes = data.getSession();
    		if ( oSOSes.getAttribute (s_SO) == null ||
    				oSOSes.getAttribute (s_SOD) == null )
    		{
    			throw new Exception ("Sales Order Data Invalid");
    		}
    		oSO = (SalesOrder) oSOSes.getAttribute (s_SO);
    		vSOD = (List) oSOSes.getAttribute (s_SOD);
    	}
    }

    private void prepareTransData ( RunData data )
    	throws Exception
    {
    	//set confirm by and confirm date
    	if (oSO.getTransactionStatus() == i_SO_ORDERED)
    	{
    		oSO.setConfirmBy (data.getUser().getName());
    		oSO.setConfirmDate (new Date());
    	}
    	//check transaction data
    	if (oSO.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen
		if (vSOD.size () < 1) {throw new Exception ("Total Item Ordered < 1"); } //-- should never happen
    }

    private void resetSession ( RunData data )
    	throws Exception
    {
       	synchronized (this)
    	{
			oSOSes.setAttribute (s_SO, oSO );
			oSOSes.setAttribute (s_SOD, vSOD );
    	}
    }

    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			int iType = data.getParameters().getInt("ExcelType");			
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			if (iType == 1) //load PO item only
			{
				SalesOrder oPO = new SalesOrder();				
				data.getParameters().setProperties(oPO);

				SalesOrderItemLoader oLoader = new SalesOrderItemLoader(oSO);
				oLoader.loadData (oBufferStream);
								
				oSOSes = data.getSession();
				oSOSes.setAttribute (s_SO, oSO);
				oSOSes.setAttribute(s_SOD, oLoader.getListResult());
												
				if (oLoader.getTotalError() > 0)
				{
					data.setMessage(oLoader.getErrorMessage());
				}
				else 
				{
					context.put("bClose", Boolean.valueOf(true));
				}				
			}
			else //load whole PO
			{
		     	SalesOrderLoader oLoader = new SalesOrderLoader(data.getUser().getName());
				oLoader.loadData (oBufferStream);
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
		    	context.put ("LoadResult", oLoader.getResult());		    		
			}
			
			SalesOrderLoader oLoader = new SalesOrderLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}    
}