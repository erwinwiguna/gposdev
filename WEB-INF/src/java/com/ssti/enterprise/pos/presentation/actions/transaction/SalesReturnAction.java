package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.SalesReturnLoader;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.framework.tools.CustomParser;

public class SalesReturnAction extends TransactionSecureAction
{    
	private static final String s_VIEW_PERM   = "View Sales Return";
	private static final String s_CREATE_PERM = "Create Sales Return";
	private static final String s_CANCEL_PERM = "Cancel Sales Return";

    private static final String[] a_CREATE_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM   = {s_SALES_PERM, s_VIEW_PERM};
	
    protected HttpSession oSes;
    protected SalesReturn oSR = null;    
    protected List vSRD = null;
    
    private void initSession(RunData data) 
        throws Exception
    {
        synchronized (this)
        {
            oSes = data.getSession();
            if (oSes.getAttribute (s_SR) == null || oSes.getAttribute (s_SRD) == null) 
            {
                throw new Exception ("Invalid Transaction Data ");
            }    
            oSR = (SalesReturn) oSes.getAttribute (s_SR);
            vSRD = (List) oSes.getAttribute (s_SRD);      
        }
    }
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if (data.getParameters().getInt("save") == 1 ||
            data.getParameters().getString("eventSubmit_doUpdate") != null)
        {
            return isAuthorized (data, a_CREATE_PERM);
    	}
        else if (data.getParameters().getInt("save") == 2 || 
                 data.getParameters().getInt("save") == 3)        
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
    	else
    	{
            return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform (RunData data, Context context)
        throws Exception
    {
    	if (data.getParameters().getInt("save") == 1) 
    	{
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == 2) 
    	{
    		doCancel (data,context);
    	}
        else if (data.getParameters().getInt("save") == 3) 
        {
            doDelTrans (data,context);
        }
    	else 
    	{
    		doFind (data,context);
    	}
    }
    
    public void doFind (RunData data, Context context)
        throws Exception
    {
    	super.doFind(data);

    	Date dStart = CustomParser.parseDate (data.getParameters().getString("StartReturnDate") );
    	Date dEnd = CustomParser.parseDate (data.getParameters().getString("EndReturnDate") );

    	if (data.getParameters().getBoolean("fromReport"))
    	{
        	dStart = CustomParser.parseDate (data.getParameters().getString("StartDate"));
        	dEnd = CustomParser.parseDate (data.getParameters().getString("EndDate"));
    	}
    	String sReasonID = data.getParameters().getString("ReturnReasonId","");
    	String sCustomerID = data.getParameters().getString("CustomerId");
    	int iStatus = data.getParameters().getInt("Status",-1);
    	
		LargeSelect vTR = SalesReturnTool.findData (
			iCond, sKeywords, dStart, dEnd, sCustomerID, sLocationID,
			iStatus, iLimit, iGroupBy, sCurrencyID, sReasonID);
		data.getUser().setTemp("findSalesReturnResult", vTR);			
    }
    
	public void doUpdate (RunData data, Context context)
        throws Exception
    {	
		doSave (data, context);
    }    
    
	public void doSave (RunData data, Context context)
        throws Exception
    {
		try 
		{
		    initSession(data);
            
			SalesReturnTool.updateDetail(vSRD, data);
			SalesReturnTool.setHeaderProperties(oSR, vSRD, data);			
			SalesReturnTool.saveData(oSR, vSRD, null);	

            resetSession (data);
            
			context.put (s_RET_DET, vSRD);		
			context.put (s_RET, oSR);
			
        	data.setMessage(LocaleTool.getString("sret_save_success"));
        }
        catch (Exception _oEx) 
		{
        	handleError(data, LocaleTool.getString("sret_save_failed"), _oEx);
        }
    }

	public void doCancel (RunData data, Context context)
	    throws Exception
	{
		try 
		{
            initSession(data);

			SalesReturnTool.cancelReturn(oSR, vSRD, data.getUser().getName());
	    	data.setMessage(LocaleTool.getString("sret_cancel_success"));

            resetSession (data);
        }
	    catch (Exception _oEx) 
	    {
        	handleError(data, LocaleTool.getString("sret_cancel_failed"), _oEx);
	    }
	}	
    
    public void doDelTrans (RunData data, Context context)
        throws Exception
    {
        try 
        {
            initSession(data);
            SalesReturnTool.deleteTrans(oSR.getSalesReturnId());
            
            oSes.setAttribute(s_SR, null);           
            oSes.setAttribute(s_SRD, null);
        }
        catch (Exception _oEx) 
        {
            handleError (data, LocaleTool.getString("si_cancel_failed"), _oEx);
        }
    }
    
    private void resetSession (RunData data) 
        throws Exception
    {
        synchronized (this)
        {
            oSes.setAttribute(s_SR, oSR);           
            oSes.setAttribute(s_SRD, vSRD);
        }
    }
    
    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
        try 
        {
            FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
            InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            SalesReturnLoader oLoader = new SalesReturnLoader(data.getUser().getName());
            oLoader.loadData (oBufferStream);
            data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());         
            context.put ("LoadResult", oLoader.getResult());
        }
        catch (Exception _oEx) 
        {
            String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
            log.error ( sError, _oEx );
            data.setMessage (sError);
        }
    }    
}