package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.util.Date;

import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Transaction Secure Action, super class for all transaction Action class
 * implements TransactionAttributes to hold various transaction related constants
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: TransactionSecureAction.java,v 1.13 2009/05/04 01:58:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: TransactionSecureAction.java,v $
 * Revision 1.13  2009/05/04 01:58:24  albert
 * *** empty log message ***
 *
 * Revision 1.12  2008/06/29 07:04:44  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/04/10 07:56:21  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class TransactionSecureAction extends SecureAction implements TransactionAttributes, AppAttributes
{
	protected static final String s_SALES_PERM = "Sales Transaction";	
	protected static final String s_PURCHASE_PERM = "Purchase Transaction";
	protected static final String s_INVENTORY_PERM = "Inventory Transaction";
	
	//FINDER 
	protected int iCond;
	protected String sKeywords;
	protected Date dStart;
	protected Date dEnd;
	protected int iLimit;
	protected int iGroupBy;
	protected String sCurrencyID;
	protected String sLocationID;
	
	
	/**
	 * 
	 * @param data
	 * @throws Exception
	 */
    public void doFind (RunData data)
    	throws Exception
	{    	
    	iCond = data.getParameters().getInt(s_COND);
    	sKeywords = data.getParameters().getString(s_KEY);
    	dStart = DateUtil.parseStartDate (data.getParameters().getString(s_START_DATE) );
    	dEnd = DateUtil.parseEndDate (data.getParameters().getString(s_END_DATE) );
		iLimit = data.getParameters().getInt(s_VIEW_LIMIT, 50);		
		iGroupBy = data.getParameters().getInt(s_GROUP_BY, -1);		
    	sCurrencyID = data.getParameters().getString(s_CURRENCY_ID);	
    	sLocationID = data.getParameters().getString(s_LOCATION_ID);
	}	

    /**
     * change to display stacktrace on nullPointer
     * 
     * @param data
     * @param _sDesc
     * @param t
     */
	protected void handleError(RunData data, 
							   String _sDesc, 
							   Throwable t)
	{
    	String sError = _sDesc + t.getMessage(); 
    	String sStack = BaseTool.displayStackTrace(t,null);
    	if (StringUtil.isNotEmpty(sStack))
    	{
    		StringBuilder sb = new StringBuilder();    		
    		sError += BaseTool.displayStackTpl(data,  sStack, null);      		
    	}
    	data.getParameters().add(s_POST_ERROR, Boolean.valueOf(true).toString());        	
    	log.error (sError, t); 
    	data.setMessage (sError);
	}
	
}
