package com.ssti.enterprise.pos.presentation.actions.transaction.intransfer;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.presentation.actions.transaction.IssueReceiptAction;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.framework.tools.StringUtil;

public class InternalTransferAction extends IssueReceiptAction
{   
    private HttpSession oSes;  

    private static final String s_VIEW_PERM    = "View Item Transfer";
    private static final String s_CREATE_PERM  = "Create Item Transfer";
    private static final String s_CANCEL_PERM  = "Cancel Item Transfer";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    @Override
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	if(data.getParameters().getInt("save") == 1)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
    	if(data.getParameters().getInt("save") == 2)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    

    
    IssueReceipt oIR;
    List vIRD;
    
    public void doSave(RunData data, Context context)
        throws Exception
    {
        try
        {
            oIR = (IssueReceipt) data.getSession().getAttribute(s_IR);
            vIRD = (List) data.getSession().getAttribute(s_IRD);           
            IssueReceiptTool.updateDetail(data, vIRD);
            IssueReceiptTool.setHeader(data, oIR);
            
            if (oIR.getInternalTransfer()) //only save internal Transfer trans
            {
                if (oIR.getTransactionType() == i_INV_TRANS_ISSUE_UNPLANNED)
                {
                    oIR.setCrossEntityId(data.getParameters().getString("CustomerId"));
                    oIR.setCrossEntityCode(CustomerTool.getCodeByID(oIR.getCrossEntityId()));
                }
                else if (oIR.getTransactionType() == i_INV_TRANS_RECEIPT_UNPLANNED)
                {
                    oIR.setCrossEntityId(data.getParameters().getString("VendorId"));
                    oIR.setCrossEntityCode(VendorTool.getCodeByID(oIR.getCrossEntityId()));
                }
                if (StringUtil.isNotEmpty(oIR.getCrossEntityId())) //validate cross entity
                {
                    if (oIR.getStatus() == i_PENDING)
                    {
                        oIR.setUserName(data.getUser().getName());
                    }
                    if (oIR.getStatus() == i_PROCESSED)
                    {
                        oIR.setConfirmBy (data.getUser().getName());
                        oIR.setConfirmDate(new Date());
                    }
                    if (StringUtil.isEmpty(oIR.getAccountId()))
                    {
                        data.setMessage("ERROR: Adjustment Account is NOT SET, Please Check Adjustment Type Setup");                
                    }
                    else
                    {
                        IssueReceiptTool.saveData (oIR, vIRD, null);
                        data.setMessage(LocaleTool.getString("iru_save_success"));              
                    }
                }
                else
                {
                    oIR.setStatus(i_PENDING);
                    data.setMessage("ERROR: Please Fill in External Entity ");                                    
                }
            }
            else
            {
                data.setMessage("ERROR: Invalid Transaction Type (Issue Receipt), Please Create New Transaction ");
            }
        }
        catch(Exception _oEx)
        {
            handleError (data, LocaleTool.getString("iru_save_failed"), _oEx);
        }
    }
}