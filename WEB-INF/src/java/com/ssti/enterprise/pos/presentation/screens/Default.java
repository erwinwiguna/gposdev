package com.ssti.enterprise.pos.presentation.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.presentation.SecureScreen;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Default Screen Class
 * <br>
 *
 * $@author  Author: LionZ $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class Default extends SecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	if (data.getSession().getAttribute("License") != null)
    	{
    		data.setAction("Logout");
    	}
    }
}
