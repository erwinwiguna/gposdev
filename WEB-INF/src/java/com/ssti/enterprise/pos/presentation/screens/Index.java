package com.ssti.enterprise.pos.presentation.screens;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.TemplateUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Index.vm View Class
 * <br>
 *
 * $@author  Author: LionZ $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class Index extends SecureScreen
{
	private static Log log = LogFactory.getLog(Index.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	if (data.getSession().getAttribute("License") != null)
    	{
    		data.setAction("Logout");
    	}    	
    	try
		{    		
			TemplateUtil.generateMenu(data);
			TemplateUtil.generateTimeJS(data);
		}
		catch (Exception _oEx)
		{
			log.error(_oEx);
			_oEx.printStackTrace();
		}    	
    }
}
