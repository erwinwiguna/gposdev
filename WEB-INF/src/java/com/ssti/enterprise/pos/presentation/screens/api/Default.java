package com.ssti.enterprise.pos.presentation.screens.api;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.google.gson.Gson;
import com.ssti.framework.presentation.NonSecureScreen;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class Default extends NonSecureScreen
{
	static Gson gson = new Gson();
    
	@Override
    public void doBuildTemplate(RunData data, Context context) 
    {
		context.put("gson", gson);		
    }
}
