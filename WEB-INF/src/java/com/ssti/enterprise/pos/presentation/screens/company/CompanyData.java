package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/company/CompanyData.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: CompanyData.java,v 1.7 2007/08/10 01:54:30 albert Exp $
 *
 * $Log: CompanyData.java,v $
 * Revision 1.7  2007/08/10 01:54:30  albert
 * *** empty log message ***
 *
 * Revision 1.6  2006/03/22 15:48:52  albert
 * *** empty log message ***
 *
 */
public class CompanyData extends CompanySecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {
        	com.ssti.enterprise.pos.om.CompanyData oCompany = PreferenceTool.getCompany();
        	if (oCompany != null)
        	{
		        context.put("Company", oCompany);        		        		
        	}
        	else 
        	{
        		if (StringUtil.isEmpty(data.getMessage()))
				{
        			data.setMessage(LocaleTool.getString("company_not_setup"));    	
				}
        	}
        }
        catch (Exception e)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + e.getMessage());
        }        
    }    
}
