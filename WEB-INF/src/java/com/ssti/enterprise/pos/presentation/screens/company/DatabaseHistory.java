package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.db.DBHistoryTool;
import com.ssti.framework.tools.StringUtil;

public class DatabaseHistory extends CompanySecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {
        	int op = data.getParameters().getInt("op");
        	if (op == 1)
        	{
        		String sSQL = data.getParameters().getString("UpdateScript");
        		if (StringUtil.isNotEmpty(sSQL))
        		{
        			context.put("sResult",DBHistoryTool.executeBatch(sSQL));
        		}
        	}
        	context.put("vData",DBHistoryTool.getHistory());
        }
        catch (Exception _oEx)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }        
    }    
}
