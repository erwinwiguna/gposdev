package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.GlConfig;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/company/GLConfig.java,v $
 *
 * Purpose: gl config screen class
 *
 * @author  $Author: albert $
 * @version $Id: GLConfig.java,v 1.8 2008/03/03 01:59:25 albert Exp $
 *
 * $Log: GLConfig.java,v $
 * Revision 1.8  2008/03/03 01:59:25  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/08/10 01:54:30  albert
 * *** empty log message ***
 *
 * Revision 1.6  2005/08/21 14:24:01  albert
 * *** empty log message ***
 *
 * Revision 1.5  2005/04/13 02:49:15  Albert
 * *** empty log message ***
 *
 * Revision 1.4  2005/03/28 09:18:19  Albert
 * *** empty log message ***
 *
 * Revision 1.3  2005/01/20 07:36:49  yudi
 * no message
 *
 * Revision 1.2  2004/11/08 02:20:35  Albert
 * organize import by eclipse
 *
 * Revision 1.1  2004/10/27 06:34:11  Albert
 * initial commit
 *
 */

public class GLConfig extends CompanySecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {
            GlConfig oGL = GLConfigTool.getGLConfig(null);
            if (oGL != null)
        	{
		        context.put("GlConfig", oGL);        		
        	}
        	else 
        	{
				data.setMessage(LocaleTool.getString("glconf_not_setup"));    	
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }        
    }    
}
