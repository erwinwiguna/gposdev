package com.ssti.enterprise.pos.presentation.screens.company;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.RemoteStoreData;
import com.ssti.enterprise.pos.om.RemoteStoreDataPeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.IOTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/company/LoadRemoteStoreData.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: LoadRemoteStoreData.java,v 1.3 2008/02/26 05:12:18 albert Exp $
 *
 * $Log: LoadRemoteStoreData.java,v $
 * Revision 1.3  2008/02/26 05:12:18  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/10 04:47:40  albert
 * *** empty log message ***
 *
 * Revision 1.1  2006/01/21 12:06:23  albert
 * *** empty log message ***
 *
 */
public class LoadRemoteStoreData extends SecureScreen
{
	private static final String[] a_PERM = {"Setup Master Data", "View Remote Store Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }		
	
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {        	        
        	String sPath = PreferenceTool.getSysConfig().getSyncStoresetupPath();
            sPath = IOTool.checkPath(sPath);
            
        	File oFile = new File (sPath);
        	if (oFile != null && oFile.isDirectory())
        	{
            	Criteria oCrit = new Criteria();
        		List vData = RemoteStoreDataPeer.doSelect(oCrit);
        		if (vData.size() > 0)
        		{
        			RemoteStoreData oRSData = (RemoteStoreData) vData.get(0);
        			context.put("oRSData", oRSData);
        		}
        		else
        		{
	        		File[] aFiles = oFile.listFiles();
	        		if (data.getParameters().getInt("op") != 1)
	        		{
		        		if (aFiles == null || aFiles.length == 0)
		        		{
		            		data.setMessage("Store Setup Directory Is Empty, Copy Store Setup File First To " + sPath);
		        		}
		        		else if (aFiles.length > 1)
		        		{
		            		data.setMessage("WARNING : Store Setup Directory " + sPath + 
		            		    " Contains " + aFiles.length + " Files, Make Sure Correct File Loaded ");        			
		        		}
		        		else if (aFiles.length == 1)
		        		{
		            		data.setMessage("Store Setup Directory " + sPath  );        			        		
		        		}
	        		}
	        		context.put("files", Arrays.asList(aFiles));
        		}
        		context.put("path", sPath);
        	}
        	else
        	{
        		data.setMessage("Store Setup Directory " + sPath + " Not Exist ");
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }        
    }    
}
