package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Preference;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

public class Preferences extends CompanySecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {
        	Preference oPref = PreferenceTool.getPreference();
        	if (oPref != null)
        	{
		        context.put("Preferences", oPref);        		
        	}
        	else 
        	{   
        		if (StringUtil.isEmpty(data.getMessage()))
				{
					data.setMessage(LocaleTool.getString("pref_not_setup"));
				}
        	}        	
        }
        catch (Exception _oEx)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }        
    }    
}
