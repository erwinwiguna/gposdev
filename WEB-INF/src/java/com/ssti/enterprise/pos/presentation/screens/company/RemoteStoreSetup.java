package com.ssti.enterprise.pos.presentation.screens.company;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.RemoteStoreDataPeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/company/RemoteStoreSetup.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: RemoteStoreSetup.java,v 1.8 2007/07/10 04:47:40 albert Exp $
 *
 * $Log: RemoteStoreSetup.java,v $
 * Revision 1.8  2007/07/10 04:47:40  albert
 * *** empty log message ***
 *
 * Revision 1.7  2006/03/22 15:48:52  albert
 * *** empty log message ***
 *
 */
public class RemoteStoreSetup extends SecureScreen
{
	private static final String[] a_PERM = {"Setup Master Data", "View Remote Store Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {
        	Criteria oCrit = new Criteria();
        	oCrit.addDescendingOrderByColumn(RemoteStoreDataPeer.SETUP_DATE);
            List vRemoteStore = RemoteStoreDataPeer.doSelect(oCrit);
        	if (vRemoteStore.size() > 0) 
        	{
		        context.put("RemoteStores", vRemoteStore);        		
        	}
        	else 
        	{
        		if (StringUtil.isEmpty(data.getMessage()))
				{
        			data.setMessage(LocaleTool.getString("remote_not_setup"));    	
				}
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }        
    }   
}
