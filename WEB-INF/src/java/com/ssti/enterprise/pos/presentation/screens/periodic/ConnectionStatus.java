package com.ssti.enterprise.pos.presentation.screens.periodic;

import org.apache.turbine.modules.screens.VelocityScreen;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.networking.SocketTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/periodic/LoadHOData.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: LoadHOData.java,v 1.11 2008/08/17 02:18:47 albert Exp $
 *
 * $Log: LoadHOData.java,v $
 * Revision 1.11  2008/08/17 02:18:47  albert
 * *** empty log message ***
 *
 */

public class ConnectionStatus extends VelocityScreen

{    
    public void doBuildTemplate(RunData data, Context context)
    {   
       context.put("socket", SocketTool.getInstance());
	}
}


