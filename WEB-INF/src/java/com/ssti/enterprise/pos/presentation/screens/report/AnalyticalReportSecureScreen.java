package com.ssti.enterprise.pos.presentation.screens.report;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureScreen;

public class AnalyticalReportSecureScreen extends SecureScreen
{
	private static final String[] a_PERM = {"View Analytical Reports"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }		
}
