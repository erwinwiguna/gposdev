package com.ssti.enterprise.pos.presentation.screens.report;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.report.ReportTool;
import com.ssti.enterprise.pos.tools.report.SalesReportTool;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.DateUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-07-31
 * - Remove link to medConfigTool
 * </pre><br>
 */
public class ReportSecureScreen extends SecureScreen implements AppAttributes, TransactionAttributes
{
	public static final SalesReportTool o_REPORT_TOOL = SalesReportTool.getInstance();

	protected String sStart;
	protected String sEnd;
	protected Date dStart;
	protected Date dEnd;
    protected String sLocationID;
    protected String sDepartmentID;
    protected String sProjectID;
    protected int iGroupBy;
    protected boolean bPrintable; 
    protected boolean bExcel;
    protected boolean bIncTax;    
	
	static protected Date dNow;
	String[] a_PERM = {"View Operational Reports"};
	
	protected boolean isAuthorized (RunData data)
        throws Exception
    {
        return isAuthorized (data, a_PERM);
    }
	
    public void setParams(RunData data)
    {
    	sLocationID = data.getParameters().getString("LocationId");
    	sDepartmentID = data.getParameters().getString("DepartmentId");
    	sProjectID = data.getParameters().getString("ProjectId");    	
    	iGroupBy = data.getParameters().getInt ("GroupBy", 1);
    	bIncTax = data.getParameters().getBoolean("IncludeTax");
	
    	sStart = data.getParameters().getString("StartDate");
    	sEnd = data.getParameters().getString("EndDate");	
	    dStart = DateUtil.parseStartDate(sStart);
	    dEnd = DateUtil.parseEndDate(sEnd);
	    dNow = new Date();
	    
	    if (dStart == null || dEnd == null) 
	    {	
	    	dStart = DateUtil.getStartOfDayDate(dNow);
	    	dEnd = DateUtil.getEndOfDayDate(dNow);
	    }
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	bPrintable = data.getParameters().getBoolean("printable");
    	bExcel = data.getParameters().getBoolean("excel");
    	if (bPrintable) 
    	{
    		data.setLayoutTemplate("/Printable.vm");
     	    context.put("printable", Boolean.valueOf(bPrintable));	
     	    context.put("excel", Boolean.valueOf(bExcel));	
    	}
    	else 
    	{
    		data.setLayoutTemplate("/Report.vm");    	
    	}
 	    context.put("salesMultiCurrency", Boolean.valueOf(b_SALES_MULTI_CURRENCY));
 	    context.put("purchaseMultiCurrency", Boolean.valueOf(b_PURCHASE_MULTI_CURRENCY));	
 	    context.put("formData", data.getParameters());	
 	    context.put("rpt", ReportTool.getInstance());	

 	    //already in global tool
 	    //context.put("sqlutil", SqlUtil.getInstance());
 	    
 	    PreferenceTool.setLocationInContext(data, context);
 	     	    
 	    if (PreferenceTool.useSerialNo())
 	    {
 	    	context.put("itemserial", ItemSerialTool.getInstance());
 	    }
 	    
 	    if (PreferenceTool.useBatchNo())
	    {
	    	context.put("batchtool", BatchTransactionTool.getInstance());
	    }
    }   
}
