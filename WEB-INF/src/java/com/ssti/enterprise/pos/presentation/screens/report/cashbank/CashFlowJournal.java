package com.ssti.enterprise.pos.presentation.screens.report.cashbank;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/cashbank/CashFlowJournal.java,v $
 * Purpose: this class is BankBook Report Screen
 *
 * @author  $Author: albert $
 * @version $Id: CashFlowJournal.java,v 1.1 2008/06/29 07:07:48 albert Exp $
 * @see CashFlowJournal
 *
 * $Log: CashFlowJournal.java,v $
 * Revision 1.1  2008/06/29 07:07:48  albert
 * *** empty log message ***
 *
 */

public class CashFlowJournal extends CashBankReport
{
    public void doBuildTemplate(RunData data, Context context)
    {    	
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		setParams(data);
	    	
			String sTypeID = data.getParameters().getString("CashFlowTypeId");
			String sCurrID = data.getParameters().getString("CurrencyId");
			Integer iType = Integer.valueOf(data.getParameters().getInt("CashFlowType"));
			
			if (StringUtil.isEmpty(sTypeID))
			{
				sTypeID = null;
			}
			
			context.put ("CashFlows", CashFlowTool.getCashFlowJournal(sTypeID, sCurrID, sBankID, iType, dStart, dEnd));                        
	    }   
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    	}    
    }    
}
