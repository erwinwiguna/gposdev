package com.ssti.enterprise.pos.presentation.screens.report.cashbank;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class CashManagementReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findCashFlowResult", "vCF");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}
