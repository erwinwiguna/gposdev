package com.ssti.enterprise.pos.presentation.screens.report.cashbank;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/cashbank/Default.java,v $
 * Purpose: this class is BankBook Report Screen
 *
 * @author  $Author: albert $
 * @version $Id: Default.java,v 1.1 2009/05/04 02:00:48 albert Exp $
 * @see Default
 *
 * $Log: Default.java,v $
 * Revision 1.1  2009/05/04 02:00:48  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/06/29 07:07:48  albert
 * *** empty log message ***
 *
 */

public class Default extends CashBankReport
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	    }
    	catch (Exception _oEx) 
    	{	
    		log.error(_oEx);
    	}    
    }    
}
