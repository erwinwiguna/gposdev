package com.ssti.enterprise.pos.presentation.screens.report.custom;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.mvc.session.SessionHandler;
import com.ssti.framework.om.ActiveUser;
import com.ssti.framework.om.ActiveUserPeer;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/custom/ReportKategoriSetup.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ReportKategoriSetup.java,v 1.3 2008/06/29 07:08:24 albert Exp $
 *
 * $Log: ReportKategoriSetup.java,v $
 * Revision 1.3  2008/06/29 07:08:24  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/20 14:33:52  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/10/10 03:53:02  albert
 * *** empty log message ***
 *
 */

public class ActiveLoginUser extends Default
{	
	public void doBuildTemplate (RunData _data, Context context)
	{
		super.doBuildTemplate(_data, context);

		int iOp = data.getParameters().getInt("op");
		if (iOp == 1)
		{
			String sUser = data.getParameters().getString("userName");
			if (StringUtil.isNotEmpty(sUser))
			{
				try {
					Criteria oCrit = new Criteria();
					oCrit.add(ActiveUserPeer.USER_NAME, sUser);
					List v = ActiveUserPeer.doSelect(oCrit);
					
					if (v.size() > 0)
					{
						ActiveUser oUser = (ActiveUser) v.get(0);
						SessionHandler.invalidateSession(oUser.getSessionId());
						SessionHandler.deleteActiveUser(sUser,null);
						data.setMessage("User " + sUser + " deleted from Active User Login");
						
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		}
		
		List v = SessionHandler.getActiveUser();
		context.put("activeUsers", v);
	}
}
