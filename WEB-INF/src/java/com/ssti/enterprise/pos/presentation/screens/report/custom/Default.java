package com.ssti.enterprise.pos.presentation.screens.report.custom;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.framework.jasper.JasperAttributes;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/custom/Default.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: Default.java,v 1.4 2009/05/04 02:01:17 albert Exp $
 *
 * $Log: Default.java,v $
 * Revision 1.4  2009/05/04 02:01:17  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/10 10:22:50  seph
 * *** empty log message ***
 *
 * Revision 1.2  2007/02/23 13:32:09  seph
 * *** empty log message ***
 *
 * Revision 1.1  2005/10/10 03:53:02  albert
 * *** empty log message ***
 *
 */

public class Default extends ReportSecureScreen implements JasperAttributes
{
	protected Log log = LogFactory.getLog(getClass());
	
	public static final String s_ID = "id";
	
	public static final String s_VIEW   = "view";
	public static final String s_ADD    = "add";
	public static final String s_UPDATE = "update";
	public static final String s_DELETE = "delete";

	public static final String s_DEFAULT_REPORT_DIR = "/report";

	public ValueParser formData;
	
	public RunData data;
	
	String sOP;
		
	public void doBuildTemplate (RunData _data, Context context)
	{				
		this.data = _data;
		formData = data.getParameters();
		
		super.doBuildTemplate(data, context);
		
		sOP = formData.getString("op", "");		
	}
}
