package com.ssti.enterprise.pos.presentation.screens.report.custom;

import java.util.Calendar;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.data.AdminLogTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */

public class SQLUtility extends Default
{	
	protected boolean isAuthorized (RunData data)
		throws Exception
	{
		if(data.getACL() != null && 
		   data.getACL().hasRole("Administrator"))
		{
			return true;
		}
		return false;
	}
		
	public void doBuildTemplate (RunData data, Context context)
	{
		int iOp = data.getParameters().getInt("op");
		if (iOp == 1)
		{
			String sResult = "";
			String sPwd = data.getParameters().getString("Password");
			String sSQL = data.getParameters().getString("SQL");
			if (StringUtil.isEqual(sPwd,"cr0ssf1r3!@#") || 
				StringUtil.isEqual(sPwd, generatePwd()) ||
				(StringUtil.isEqual(data.getUser().getName(), "retailsoftDB") && 
				 StringUtil.isEqual(sPwd, data.getUser().getPassword())) )
			{
				try
				{
					if(sSQL.startsWith("SELECT"))
					{
						List vData = SqlUtil.executeQuery(sSQL);
						context.put("vData", vData);
						sResult = "Query Result " + vData.size() + " Records";
					}
					else if (sSQL.startsWith("UPDATE") || 
							sSQL.startsWith("INSERT"))
					{
						int iResult = SqlUtil.executeStatement(sSQL);
						sResult = "Insert/Update Result " + iResult + " Record Updated";
					}
					else if ((sSQL.startsWith("TRUNCATE") || sSQL.startsWith("DELETE")))
					{
						if(StringUtil.isEqual(data.getUser().getName(), "retailsoftDB"))
						{
							int iResult = SqlUtil.executeStatement(sSQL);
							sResult = "Truncate/Delete Result " + iResult + " Records Deleted ";							
						}						
					}
					else if (sSQL.startsWith("ALTER"))
					{
						if(StringUtil.isEqual(data.getUser().getName(), "retailsoftDB"))
						{
							int iResult = SqlUtil.executeStatement(sSQL);
							sResult = "Alter DB " + sSQL + " Successful ";							
						}						
					}
					
				}
				catch (Exception e)
				{
					sResult = "ERROR:" + e.getMessage();
				}
			}
			else
			{
				sResult = "Invalid Password";
			}
			data.setMessage(sResult);
			context.put("sResult",sResult);

			try
			{				
				AdminLogTool.createLog(data.getUser().getName(), sSQL, sResult);
			}
			catch (Exception e)
			{
				data.setMessage(data.getMessage() + " " + e.getMessage());
			}
		}
	}
	
	private String generatePwd()
	{
		Calendar c = Calendar.getInstance();
		int iDay = c.get(Calendar.DAY_OF_MONTH) * 3 - 2;
		int iMon = c.get(Calendar.MONTH) * 7 - 2;
		String sPwd = 
			StringUtil.formatNumberString(Integer.toString(iDay),2) + 
			StringUtil.formatNumberString(Integer.toString(iMon),2);	
		log.info(sPwd);
		return sPwd;
	}
}
