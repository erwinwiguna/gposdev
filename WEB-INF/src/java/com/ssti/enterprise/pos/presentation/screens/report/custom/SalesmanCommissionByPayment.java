package com.ssti.enterprise.pos.presentation.screens.report.custom;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.commission.SalesCommissionTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/custom/SalesmanCommissionReport.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesmanCommissionReport.java,v 1.2 2005/08/21 14:23:17 albert Exp $
 *
 * $Log: SalesmanCommissionReport.java,v $
 * Revision 1.2  2005/08/21 14:23:17  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/03/28 09:18:22  Albert
 * *** empty log message ***
 *
 */

public class SalesmanCommissionByPayment extends ReportSecureScreen
{	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		context.put ("salescommission", SalesCommissionTool.getInstance());
	    }   
    	catch (Exception _oEx) 
    	{
    	    _oEx.printStackTrace();
    		data.setMessage (_oEx.getMessage());
    	}    
    }    
}