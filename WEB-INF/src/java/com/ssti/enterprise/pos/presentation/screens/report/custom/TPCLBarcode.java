package com.ssti.enterprise.pos.presentation.screens.report.custom;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.networking.SocketTool;
import com.ssti.framework.print.helper.RemotePrintHelper;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/custom/ReportKategoriSetup.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ReportKategoriSetup.java,v 1.3 2008/06/29 07:08:24 albert Exp $
 *
 * $Log: ReportKategoriSetup.java,v $
 * Revision 1.3  2008/06/29 07:08:24  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/20 14:33:52  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/10/10 03:53:02  albert
 * *** empty log message ***
 *
 */

public class TPCLBarcode extends Default
{	
	static Log log = LogFactory.getLog(TPCLBarcode.class);
	
	int iLabelType = 0;
	String sModelFlag = "0";
	boolean bPrintBarcode = false;
	int iPrintAmount = 0;
	String sPrintAmount = "0";
	
	String sItemCode = "";
	String sVendorCode = "";
	String sPODate = "";
	String sVenItemCode = "";
	String sSalesPrice = "";
	String sBarcodeParam = ",9,1,02,1,0090";
	
	String sPrinterIP = "";
	int iListenPort = 8089;
	int iPrintTo = 1;
	StringBuilder printMsg = null;
	String[] charArr = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	String sPONo = "";
	
	public void doBuildTemplate (RunData _data, Context context)
	{
		super.doBuildTemplate(_data, context);
		
		int op = data.getParameters().getInt("op");		
		if (op == 1)
		{
			try 
			{
				String sParam = data.getParameters().getString("barcodeParam");
				if (StringUtil.isNotEmpty(sParam)) sBarcodeParam = sParam;
				
				iLabelType = data.getParameters().getInt("labelType");
				bPrintBarcode = data.getParameters().getBoolean("printBarcode");
				iPrintAmount = data.getParameters().getInt("printAmount");
				sPrinterIP = data.getParameters().getString("printerIp");
				iListenPort = data.getParameters().getInt("listenPort");
				iPrintTo = data.getParameters().getInt("printTo");

				printMsg = new StringBuilder();
				
				Calendar oCal = Calendar.getInstance();
				int iMonth = oCal.get(Calendar.MONTH);
				int iYear = oCal.get(Calendar.YEAR) - 2011;
				sPODate = charArr[iMonth] + charArr[iYear];

				sPrintAmount = StringUtil.formatNumberString(Integer.toString(iPrintAmount),4);
				
				int src = data.getParameters().getInt("src");	
				if (src == 1) //single item
				{
					sItemCode = data.getParameters().getString("ItemCode");				
					Item oItem = ItemTool.getItemByCode(sItemCode);
					setFromItem(oItem, data);
					printLabel();
					data.setMessage("Item " + sItemCode + " Printed Successfully ");
				}
				else if (src == 2) //kategori
				{
					String sKatID = data.getParameters().getString("KategoriId");
					List vKatID = KategoriTool.getChildIDList(sKatID);
					vKatID.add(sKatID);
					List vItem = ItemTool.getItemByKategoriID(vKatID);
					for (int i = 0; i < vItem.size(); i++)
					{
						Item oItem = (Item) vItem.get(i);
						setFromItem(oItem, data);
						printLabel();
					}				
					data.setMessage("Kategori " + KategoriTool.getLongDescription(sKatID) + " Printed Successfully ");
				}
				else if (src == 3) //PO
				{
					sPONo = data.getParameters().getString("PoNo");
					PurchaseOrder oPO = getByNoAndStatus(sPONo, i_PO_ORDERED);
                    if (oPO == null)
                    {
                        oPO = getByNoAndStatus(sPONo, i_PO_RECEIVED);
                    }
					if (oPO != null)
					{
						oCal.setTime(oPO.getTransactionDate());
						iMonth = oCal.get(Calendar.MONTH);
						iYear = oCal.get(Calendar.YEAR) - 2011;
						sPODate = charArr[iMonth] + charArr[iYear];
						
						List vDet = PurchaseOrderTool.getDetailsByID(oPO.getPurchaseOrderId());
						for (int i = 0; i < vDet.size(); i++)
						{
							PurchaseOrderDetail oPOD = (PurchaseOrderDetail) vDet.get(i);
							Item oItem = ItemTool.getItemByID(oPOD.getItemId());
							setFromItem(oItem, data);
                            if (iPrintAmount == 0)
                            {
    							iPrintAmount = oPOD.getQty().intValue();
    							sPrintAmount = StringUtil.formatNumberString(Integer.toString(iPrintAmount),4);
                            }
							printLabel();
						}							
						data.setMessage("PO " + sPONo + " Printed Successfully ");
					}
					else
					{
						data.setMessage("PO " + sPONo + " Not Found!");
					}
				}
				else if (src == 4) //PR
				{
					
				}
				else if (src == 5) //PI
				{
					
				}
				else if (src == 6) //IT
				{
					
				}				
			} 
			catch (Exception e) 
			{
				data.setMessage("ERROR: " + e.getMessage());
				log.error("ERROR: " + e.getMessage(), e);
			}			
		}
	}
	
	private void setFromItem(Item oItem, RunData data)
		throws Exception
	{
		if (oItem != null)
		{
			sItemCode = oItem.getItemCode();
			sVendorCode = VendorTool.getCodeByID(oItem.getPreferedVendorId());
			sVenItemCode = StringUtil.center(oItem.getVendorItemCode(),12);
			sSalesPrice = format(oItem.getItemPrice(),"Rp#,##0");	
		}
		else
		{
			data.setMessage("Item " + sItemCode + " Not Found ");
		}
	}
	
	private void printLabel()
	{
		String pitchLength = "0280"; // (in 0.1 mm units)
		String printWidth  = "0370";
		String printLength = "0260";
		String feedFormat  = "20C62";
		
		if (iLabelType == 0) //TEMPEL
		{
			//log.debug("Label Tempel");
			pitchLength = "0280";
			printWidth  = "0370";
			printLength = "0260";
			sModelFlag  = "0";
		}
		else if (iLabelType == 1) //GANTUNG
		{
			//log.debug("Label Gantung");
			pitchLength = "0260";
			printWidth  = "0350";
			printLength = "0255";
			sModelFlag  = "1";
		}
		
		// TPCL Commands.
		//String cmdLabelSize = "";
		//String cmdFeed = "";
		//String cmdImgBufClear = "";
		//String cmdBMFFormat = "";
		//String cmdBMFData = "";
		//String cmdIssue = "";
		String[] cmd = new String[14];
		String[] x = new String[5];
		String[] y = new String[5];
		//log.debug(brcdFlag.ToString());
		
		if (bPrintBarcode == false)
		{
			if (StringUtil.equals(sModelFlag,"0")) //TEMPEL
			{
				x[0] = "0170"; y[0] = "0015";
				x[1] = "0135"; y[1] = "0015";
				x[2] = "0135"; y[2] = "0190";
				x[3] = "0100"; y[3] = "0015";
				x[4] = "0010"; y[4] = "0015";
			}
			if (StringUtil.equals(sModelFlag,"1")) //GANTUNG
			{
				x[0] = "0170"; y[0] = "0015"; //165
				x[1] = "0135"; y[1] = "0015"; //130
				x[2] = "0135"; y[2] = "0170"; //130
				x[3] = "0100"; y[3] = "0015";
				x[4] = "0010"; y[4] = "0015";
			}
			// Issue command in TPCL format.
			cmd[0] = "D" + pitchLength + "," + printWidth + "," + printLength;
			cmd[1] = "T" + feedFormat;
			cmd[2] = "C";
			cmd[3] = "PC21;" + x[0] + "," + y[0] + ",10,10,N,-02,11,B";     // Inventory Code
			cmd[4] = "PC22;" + x[1] + "," + y[1] + ",10,05,N,-03,11,B";     // Supplier Code
			cmd[5] = "PC23;" + x[2] + "," + y[2] + ",10,05,N,-03,11,B";     // String BlnThn
			cmd[6] = "PC24;" + x[3] + "," + y[3] + ",10,05,N,-02,11,W";     // Article Supplier
			cmd[7] = "PC25;" + x[4] + "," + y[4] + ",10,10,N,-03,11,B";     // Price
			cmd[8] = "RC21;" + sItemCode;
			cmd[9] = "RC22;" + sVendorCode;
			cmd[10] = "RC23;" + sPODate;
			cmd[11] = "RC24;" + sVenItemCode;                  // Character in the middle (max 12 char);
			cmd[12] = "RC25;" + sSalesPrice;
			cmd[13] = "XS;I," + sPrintAmount + ",0002C6200";     // Qty printed <= 5000
		}
		else
		{
			if (StringUtil.equals(sModelFlag,"0")) //TEMPEL
			{
				x[0] = "0170"; y[0] = "0010";
				x[1] = "0170"; y[1] = "0010";
				x[2] = "0010"; y[2] = "0010";
			}
			else if (StringUtil.equals(sModelFlag,"1")) //GANTUNG
			{
				x[0] = "0170"; y[0] = "0010";
				x[1] = "0170"; y[1] = "0010";
				x[2] = "0010"; y[2] = "0010";
			}
			
			if (StringUtil.isNotEmpty(sPONo))
			{
				pitchLength = "0300";
				printWidth  = "0370";
				printLength = "0290";
				
				String sFmtNo = sPONo;
				//sFmtNo = sFmtNo.substring(sFmtNo.indexOf("/") + 1);
				//sFmtNo = sFmtNo.replaceAll("/","");
				
				//XBaa; bbbb, cccc, d, e, ff, g, h (, Mi) (, Kj) (, Jkkllmm)
			
				cmd[0] = "D" + pitchLength + "," + printWidth + "," + printLength;
				cmd[1] = "T" + feedFormat;
				cmd[2] = "C";
				//cmd[3] = "PC21;" + "0240" + "," + "0025" + ",10,05,N,-02,11,B";     // PO No		
				cmd[3] = "XB01;" + "0230" + "," + "0100" + ",T,L,03,A,1";        	// QR Code
				cmd[4] = "XB01;" + "0230" + "," + "0100" + ",T,L,03,A,1";        	// QR Code
				cmd[5] = "PC22;" + "0120" + "," + "0025" + ",10,05,N,-02,11,B";     // Inventory Code
				cmd[6] = "XB02;" + "0100" + "," + "0025" + sBarcodeParam;        	// Barcode
				cmd[7] = "PC23;" + "0010" + "," + "0025" + ",10,05,N,-03,11,B";     // Price
				//cmd[8] = "RC21;" + sFmtNo;
				cmd[8] = "RC22;" + sItemCode;
				cmd[9] = "RB01;" + sFmtNo;
				cmd[10] = "RB02;" + sItemCode;				
				cmd[11] = "RC23;" + sSalesPrice;
				cmd[12] = "XS;I," + sPrintAmount + ",0002C6200";     // Qty printed <= 5000
				//cmd[13] = "";
				//cmd[13] = ""; 
				
				/*
				// Issue command in TPCL format.
				cmd[0] = "D" + pitchLength + "," + printWidth + "," + printLength;
				cmd[1] = "T" + feedFormat;
				cmd[2] = "C";
				//cmd[3] = "PC21;" + "0240" + "," + "0025" + ",10,05,N,-02,11,B";     // PO No		
				cmd[3] = "XB01;" + "0240" + "," + "0025" + ",Q,00,01,01,0";        	// Barcode
				cmd[4] = "XB01;" + "0240" + "," + "0025" + ",Q,00,01,01,0";        	// Barcode
				cmd[5] = "PC22;" + "0120" + "," + "0025" + ",10,05,N,-02,11,B";     // Inventory Code
				cmd[6] = "XB02;" + "0100" + "," + "0025" + sBarcodeParam;        	// Barcode
				cmd[7] = "PC23;" + "0010" + "," + "0025" + ",10,05,N,-03,11,B";     // Price
				//cmd[8] = "RC21;" + sFmtNo;
				cmd[8] = "RC22;" + sItemCode;
				cmd[9] = "RB01;" + sFmtNo;
				cmd[10] = "RB02;" + sItemCode;				
				cmd[11] = "RC23;" + sSalesPrice;
				cmd[12] = "XS;I," + sPrintAmount + ",0002C6200";     // Qty printed <= 5000
				//cmd[13] = "";
				//cmd[13] = ""; 
				*/
				/*
				 * 				
				 * 
				 * 
				 * 
				// LAST OK
				cmd[0] = "D" + pitchLength + "," + printWidth + "," + printLength;
				cmd[1] = "T" + feedFormat;
				cmd[2] = "C";
				cmd[3] = "PC21;" + "0240" + "," + "0025" + ",10,05,N,-02,11,B";     // PO No		
				cmd[4] = "XB01;" + "0220" + "," + "0025" + sBarcodeParam;        	// Barcode
				cmd[5] = "PC22;" + "0120" + "," + "0025" + ",10,05,N,-02,11,B";     // Inventory Code
				cmd[6] = "XB02;" + "0100" + "," + "0025" + sBarcodeParam;        	// Barcode
				cmd[7] = "PC23;" + "0010" + "," + "0025" + ",10,05,N,-03,11,B";     // Price
				cmd[8] = "RC21;" + sFmtNo;
				cmd[9] = "RC22;" + sItemCode;
				cmd[10] = "RB01;" + sFmtNo;
				cmd[11] = "RB02;" + sItemCode;				
				cmd[12] = "RC23;" + sSalesPrice;
				cmd[13] = "XS;I," + sPrintAmount + ",0002C6200";     // Qty printed <= 5000
				//cmd[13] = "";
				//cmd[13] = ""; 

				 
				 // Issue command in TPCL format
				cmd[0] = "D" + pitchLength + "," + printWidth + "," + printLength;
				cmd[1] = "T" + feedFormat;
				cmd[2] = "C";
				cmd[3] = "PC21;" + "0140" + "," + "0010" + ",10,05,N,-02,11,B";     // PO No		
				cmd[4] = "PC22;" + "0110" + "," + "0010" + ",10,05,N,-02,11,B";     // Inventory Code
				cmd[5] = "XB01;" + "0090" + "," + "0010" + sBarcodeParam;        	// Barcode
				cmd[6] = "PC23;" + "0010" + "," + "0010" + ",10,05,N,-03,11,B";     // Price
				cmd[7] = "RC21;" + sPONo.substring(sPONo.indexOf("/") + 1);
				cmd[8] = "RC22;" + sItemCode;
				cmd[9] = "RB01;" + sItemCode;
				cmd[10] = "RC23;" + sSalesPrice;
				cmd[11] = "XS;I," + sPrintAmount + ",0002C6200";     // Qty printed <= 5000
				cmd[12] = "";
				cmd[13] = ""; 
				 */
			}
			else
			{
				// Issue command in TPCL format.
				cmd[0] = "D" + pitchLength + "," + printWidth + "," + printLength;
				cmd[1] = "T" + feedFormat;
				cmd[2] = "C";
				cmd[3] = "PC21;" + x[0] + "," + y[0] + ",10,10,N,-02,11,B";     // Inventory Code
				cmd[4] = "XB01;" + x[1] + "," + y[1] + sBarcodeParam;        	// Barcode
				cmd[5] = "PC22;" + x[2] + "," + y[2] + ",10,10,N,-03,11,B";     // Price
				cmd[6] = "RC21;" + sItemCode;
				cmd[7] = "RB01;" + sItemCode;
				cmd[8] = "RC22;" + sSalesPrice;
				cmd[9] = "XS;I," + sPrintAmount + ",0002C6200";     // Qty printed <= 5000
				cmd[10] = "";
				cmd[11] = ""; cmd[12] = ""; cmd[13] = "";
			}
		}
		
		// Convert TPCL Strings into ASCII Code and Convert ASCII into HEX.
		StringBuilder sResult = new StringBuilder();
		StringBuilder sHexResult = new StringBuilder();

		for (int i = 0; i < cmd.length; i++)
		{
			if (StringUtil.isNotEmpty(cmd[i]))
			{
				sResult.append((char)27).append(cmd[i]).append((char)10).append((char)0);
				sHexResult.append("1B").append(asciiToHex(cmd[i])).append("0A00");
				//res += "1B" + DataAscHex(cmd[i]) + "0A00"; ;
			}
		}
		System.out.println("TIPE : " + sModelFlag + " BARCODE: " + bPrintBarcode + " PO: " + sPONo);
		System.out.println("ASCII Result: \n" + sResult);
		System.out.println("HEX Result: \n" + sHexResult);

		byte[] asc = sResult.toString().getBytes();
		byte[] hex = hexToByte(sHexResult.toString());

		System.out.println("BYTE ASCII: " + asc.length + " HEX:" + hex.length);
		int len = hex.length;
		if (asc.length > len) len = asc.length;
		
		for (int i = 0; i < len; i++)
		{			
			byte bhex = -1;
			byte basc = -1;
			if (i < hex.length) bhex = hex[i];
			if (i < asc.length) basc = asc[i];			
			//System.out.println(i + " " + bhex + " " + basc);			
		}
		
		String sLabel = "Label "; 
		if (sModelFlag == "0") sLabel += "Tempel ";
		else sLabel += "Gantung ";
		if (bPrintBarcode) sLabel += "Barcode ";

		if (iPrintTo == 1)
		{
			SocketTool.writeToSocket(sPrinterIP, iListenPort, sHexResult.toString());
		}
		else
		{
			String sURL = "http://" + sPrinterIP + ":8080";
			System.out.println(sURL);
			RemotePrintHelper oHelper = new RemotePrintHelper(sURL, sResult.toString());
			Thread oPrintThread = new Thread(oHelper);
			oPrintThread.start();
		}
		printMsg.append("Printing ").append(sLabel).append(" Qty: ").append(iPrintAmount).append("\n");
	}
	
    private String asciiToHex(String s)
    {
        String sValue;
        String sHex = "";
        char[] car = s.toCharArray();
        for (int i = 0; i < car.length; i++) 
        {
            char c = car[i];            
        	sValue = String.format("%x", (int)c);
            sHex = sHex + sValue;
        }
        return sHex;
    }

    private byte[] hexToByte(String s)
    {
        s = s.replace(" ", "");
        byte[] buffer = new byte[s.length() / 2];
        for (int i = 0; i < s.length(); i += 2)
        {
            buffer[i / 2] = (byte)Byte.valueOf(s.substring(i, i + 2), 16);
        }
        return buffer;
    }
    

	public static String format(Number _oValue, String _sFormat)
	{
		if (_oValue != null && StringUtil.isNotEmpty(_sFormat)) 
		{
			try 
			{
				DecimalFormatSymbols oSymbol = new DecimalFormatSymbols();	
				oSymbol.setDecimalSeparator(',');
				oSymbol.setGroupingSeparator('.');
				
				NumberFormat oFormatter = new DecimalFormat(_sFormat, oSymbol);
				return oFormatter.format(_oValue);
			} 
			catch (Exception _oEx) 
			{
				log.error("Error Parsing Number " + _oValue + " Class " + _oValue.getClass() + " Format " + _sFormat, _oEx);
			}
    	}
    	return null;
	}
	
    static PurchaseOrder getByNoAndStatus(String _sNo, int _iStatus)
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_NO, _sNo);
	    oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, _iStatus);        
	    List vData = PurchaseOrderPeer.doSelect(oCrit);
	    if (vData != null && vData.size() > 0) 
	    {
	        return (PurchaseOrder) vData.get(0);
	    }
	    return null;
	}
}
