package com.ssti.enterprise.pos.presentation.screens.report.custom;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.VATLoader;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class VATImport extends Default
{		
	public void doBuildTemplate (RunData data, Context context)
	{
		int iOp = data.getParameters().getInt("Save");
		if (iOp == 1)
		{
			try
			{
				FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
				InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
				VATLoader oLoader = new VATLoader(data.getUser().getName(), data.getParameters().getInt("type"));
				
				oLoader.loadData (oBufferStream);
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
		    	context.put ("LoadResult", oLoader.getResult());				
			}
			catch (Exception e)
			{
				e.printStackTrace();				
				context.put ("LoadResult", e.getMessage() + "\n" + StringUtil.getStackTrace(e));								
			}
		}
	}	
}
