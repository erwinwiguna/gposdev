package com.ssti.enterprise.pos.presentation.screens.report.financial;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.gl.GLReportTool;
import com.ssti.enterprise.pos.tools.gl.GeneralLedgerTool;

public class Default extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	context.put ("generalledger", GeneralLedgerTool.getInstance());
    	context.put ("glreport", GLReportTool.getInstance());    	
    }    
}
