package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;

public class AdjustmentStockReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {
	    	    //String sKategoriID = data.getParameters().getString("KategoriId","");
                //List vKategoriId = null;
                
	    	    context.put ("vResult", 
	    	    	IssueReceiptTool.findData(dStart, dEnd, sLocationID, sAdjTypeID, i_PROCESSED));
            }
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
    }    
}
