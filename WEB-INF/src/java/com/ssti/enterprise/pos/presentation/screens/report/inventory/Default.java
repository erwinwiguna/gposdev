package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.inventory.InventoryAnalysisTool;
import com.ssti.enterprise.pos.tools.report.InventoryReportTool;

public class Default extends ReportSecureScreen
{
	String sAdjTypeID = "";
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		setParams(data);
    		super.doBuildTemplate(data, context);
    		sAdjTypeID = data.getParameters().getString("AdjustmentTypeId");

			context.put("inventoryreport", InventoryReportTool.getInstance());		
			context.put("inventoryanalysis", InventoryAnalysisTool.getInstance());	
	    }   
    	catch (Exception _oEx) {
			_oEx.printStackTrace();
    	}    
    }    
}
