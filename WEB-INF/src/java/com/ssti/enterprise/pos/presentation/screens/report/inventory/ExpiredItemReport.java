package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class ExpiredItemReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		context.put("batchtool", BatchTransactionTool.getInstance());   
    		super.doBuildTemplate(data, context);
    		LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");    		
    	}   
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		data.setMessage("Item LookUp Failed : " + _oEx.getMessage());
    	}    
   }    
}
