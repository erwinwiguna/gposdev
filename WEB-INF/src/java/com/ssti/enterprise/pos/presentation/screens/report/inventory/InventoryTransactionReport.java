package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;

public class InventoryTransactionReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	    	String sKeywords = data.getParameters().getString("Keywords");
	    	int iSearchBy = data.getParameters().getInt("Condition");
	    	int iGroupBy = data.getParameters().getInt("GroupBy");
	    	int iType = data.getParameters().getInt("TransType");
	    	String sKategoriID= data.getParameters().getString("KategoriId");
	        
    		context.put ("InventoryTransactions", 
    			InventoryTransactionTool.findData (dStart, dEnd, 
    				iSearchBy, iType, sKeywords, sKategoriID, sLocationID));
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Data LookUp Failed !");
    	}    
    }    
}
