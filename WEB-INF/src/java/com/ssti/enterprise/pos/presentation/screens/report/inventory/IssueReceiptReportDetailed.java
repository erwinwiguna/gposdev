package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;

public class IssueReceiptReportDetailed extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
	    	super.doBuildTemplate(data, context);
	        context.put ("IssueReceipts", 
	        	IssueReceiptTool.findData(dStart, dEnd, sLocationID,sAdjTypeID,i_PROCESSED));
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Data LookUp Failed !");
    	}    
    }    
}
