package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;


public class ItemRawImporter extends Default
{    
	InputStream oItemStream = null;
	InputStream oRawStream = null;
	StringBuilder oResult = null;
	boolean bToIR = false;
	String sFileDest = "";
	private static Log log = LogFactory.getLog(ItemRawImporter.class);
	
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	int iOp = data.getParameters().getInt("op");
    	if (iOp == 1)
    	{
    		bToIR = data.getParameters().getBoolean("ToIR");
    		try 
    		{
    			FileItem oItemFile = data.getParameters().getFileItem("ItemFile");
             	oItemStream = new BufferedInputStream(oItemFile.getInputStream());    			

    			FileItem oRecipeFile = data.getParameters().getFileItem("RecipeFile");
             	oRawStream = new BufferedInputStream(oRecipeFile.getInputStream());    			
             	
             	sFileDest = data.getParameters().getString("FileDestination","");

             	if (oItemFile != null && oRecipeFile != null)
             	{
	             	loadData();
	             	exportData();
	    			data.setMessage(LocaleTool.getString("excel_load_success"));				    			
	    			context.put("result", oResult);						
             	}
             	else
             	{
	    			data.setMessage("Invalid Item / Recipe File ");				    			             		
             	}
    		}
            catch (Exception _oEx) 
            {
            	String sError = LocaleTool.getString("excel_load_failed") + _oEx.getMessage();
            	log.error ( sError, _oEx );
            	data.setMessage (sError);
            }
    	}
    }
    
    Map mRawResult = null;
    
    private void loadData()	
    	throws Exception
    {	
    	mRawResult = new HashMap();  
    	oResult = new StringBuilder();
    	
    	POIFSFileSystem oRawFS = new POIFSFileSystem(oRawStream);
    	HSSFWorkbook oRawWB = new HSSFWorkbook(oRawFS);
    	   	
    	POIFSFileSystem oItmFS  = new POIFSFileSystem(oItemStream);
    	HSSFWorkbook oItmWB = new HSSFWorkbook(oItmFS);
    	HSSFSheet oItmSHEET = oItmWB.getSheetAt(0);    	
		int totalRows = oItmSHEET.getPhysicalNumberOfRows();
		
		oResult.append("IMPORTING " + totalRows + " ROWS IN ITEM FILE ");
		
		for (int iRow = 1; iRow < totalRows; iRow++)
		{
			HSSFRow oRow = oItmSHEET.getRow(iRow);
			String sItemCode = getString(oRow, (short)0);
			BigDecimal bdQty = getBigDecimal(oRow, (short)2);
			double dQty = bdQty.doubleValue();
			oResult.append("\nItem: ").append(sItemCode).append(" Qty: ")
				   .append(StringUtil.left(String.valueOf(dQty),10)).append(" Status: ");
			
			HSSFSheet oRawSHEET = oRawWB.getSheet(sItemCode);
			if (oRawSHEET != null)
			{
				int rawRows = oRawSHEET.getPhysicalNumberOfRows();
				for (int rawRow = 0; rawRow < rawRows; rawRow++)
				{		
					HSSFRow oRawRow = oRawSHEET.getRow(rawRow);
					String sRawCode = getString(oRawRow, (short)0);
					BigDecimal oRawQty = getBigDecimal(oRawRow, (short)2);
					double dRawQty = oRawQty.doubleValue();
					
					Double dExists = (Double) mRawResult.get(sRawCode);
					if (dExists != null)
					{
						dExists += (dQty * dRawQty);
					}
					else
					{
						dExists = (dQty * dRawQty);
					}		
					mRawResult.put(sRawCode, dExists);
				}
				oResult.append(" OK: ").append(rawRows).append(" Raw Material Used");
			}
			else
			{
				oResult.append(" ERROR: RawMaterial Sheet not found in Recipe File");
			}
    	} 	
    }
    
    private void exportData()	
    	throws Exception
    {	
    	int resultRow = 0;
    	
		HSSFWorkbook resultWB = new HSSFWorkbook();	
		HSSFSheet resultSheet = resultWB.createSheet("Stock Opname Worksheet");
        HSSFRow oRow = resultSheet.createRow(resultRow);

        int iCol = 0;
		createCell(resultWB, oRow, (short)iCol,  "Item Code");    iCol++;
		createCell(resultWB, oRow, (short)iCol,  "Item Name");    iCol++;
		createCell(resultWB, oRow, (short)iCol,  "Full Name");    iCol++;
		createCell(resultWB, oRow, (short)iCol,  "Qty"); 		  iCol++;
		createCell(resultWB, oRow, (short)iCol,  "New Qty"); 	  iCol++;
		createCell(resultWB, oRow, (short)iCol,  "+/-"); 		  iCol++;
		createCell(resultWB, oRow, (short)iCol,  "Unit"); 	      iCol++;
		createCell(resultWB, oRow, (short)iCol,  "Description");  iCol++;
		createCell(resultWB, oRow, (short)iCol,  "Cost"); 	      iCol++;
		resultRow++;
		
		oResult.append("\n\nStart Exporting Raw Item");
		oResult.append("\n-------------------------");
		
		Iterator iter = mRawResult.keySet().iterator();
		while(iter.hasNext())
		{
			iCol = 0;
			String sItemCode = (String)iter.next();
			double dQty = (Double)mRawResult.get(sItemCode);
			Item oItem = ItemTool.getItemByCode(sItemCode);
			if (oItem != null)
			{
				String sUnit = UnitTool.getCodeByID(oItem.getUnitId());
				BigDecimal dCost = InventoryLocationTool.getItemCost(oItem.getItemId(),PreferenceTool.getLocationID());
				
				oRow = resultSheet.createRow(resultRow);
				createCell(resultWB, oRow, (short)iCol,  sItemCode);    		  iCol++;
				createCell(resultWB, oRow, (short)iCol,  oItem.getItemName());    iCol++;
				createCell(resultWB, oRow, (short)iCol,  oItem.getDescription()); iCol++;
				createCell(resultWB, oRow, (short)iCol,  bd_ZERO); 				  iCol++;
				createCell(resultWB, oRow, (short)iCol,  bd_ZERO); 			  	  iCol++;
				createCell(resultWB, oRow, (short)iCol,  new BigDecimal(dQty));   iCol++;
				createCell(resultWB, oRow, (short)iCol,  sUnit); 	    		  iCol++;
				createCell(resultWB, oRow, (short)iCol,  "");  		  			  iCol++;
				createCell(resultWB, oRow, (short)iCol,  dCost);  				  iCol++;
				resultRow++;
			}
			else
			{
				oResult.append("\nERROR: Raw Item " + sItemCode + " NOT FOUND!");
			}
		}
		oResult.append("\nTOTAL ").append(resultRow - 1).append(" Raw Item Exported");		
		resultWB.write(new FileOutputStream(sFileDest));
    }
    
    /**
     * create cell 
     * 
     * @param _oWB
     * @param _oRow
     * @param _iColumn
     * @param _iAlign
     * @param _sValue
     */
    protected void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short iCol, Object _oValue)
    {                                                                                      
    	if (_oValue != null)
    	{
        	
        	if (_oValue instanceof Number)
        	{
                HSSFCell oCell = _oRow.createCell(iCol);                                            
                oCell.setCellValue(((Number)_oValue).doubleValue());  
                HSSFCellStyle oStyle = _oWB.createCellStyle();                                    
                oStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);                                           
                oStyle.setWrapText(false);
                oCell.setCellStyle(oStyle);     
        	}
        	else
        	{
                HSSFCell oCell = _oRow.createCell(iCol);                                            
                oCell.setCellValue(_oValue.toString());       	
        	}
    	}
    	else
    	{
            HSSFCell oCell = _oRow.createCell(iCol);                                            
            oCell.setCellValue("");       	
    	}                  
    }
    
    protected String getString(HSSFRow _oRow, short _iColumn)
    {                                                
    	if (_oRow != null)
    	{
    	    HSSFCell oCell = _oRow.getCell(_iColumn);

    	    if (oCell != null)
    	    {    	    
    	        if (oCell.getCellType() == HSSFCell.CELL_TYPE_STRING)
    	        {
    	        	return StringUtil.trim(oCell.getStringCellValue());
    	        }
    	        else if (oCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
    	        {    
    	            StringBuilder oSB = new StringBuilder();
    	        	return StringUtil.trim(oSB.append((int)oCell.getNumericCellValue()).toString());
    	        }
            }
        }
        return "";
    }     
    
    protected BigDecimal getBigDecimal(HSSFRow _oRow, short _iColumn)
    {                                
        if (_oRow != null)
        {                
    	    HSSFCell oCell = _oRow.getCell(_iColumn);
    	    if (oCell != null)
    	    {
    	        if (oCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
    	        {
    	        	return new BigDecimal (oCell.getNumericCellValue());
    	        }
    	        else if (oCell.getCellType() == HSSFCell.CELL_TYPE_STRING)
    	        {
    	        	try 
					{
    	        		return new BigDecimal (StringUtil.trim(oCell.getStringCellValue()));
    	        	}
    	        	catch(NumberFormatException _oNFEx){
    	        		return Attributes.bd_ZERO;                                    
    	        	}
    	        }
            }
        }
    	return Attributes.bd_ZERO;                                    
    }   
    
    protected void createIR()
    {
    	
    }
}
