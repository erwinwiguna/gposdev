package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.SerialNoLoader;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemSerial;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;


public class ItemSerialOpname extends Default
{
    //start screen methods
	protected static final int i_FIND_ITEM  = 1;
	protected static final int i_SCAN_SN    = 2;
	protected static final int i_LOAD_EXCEL = 3;

    protected Map mSN = null;
    protected List vIS = null;
    protected HttpSession oSes;
    
    String sCode = "";
    String sName = "";
    String sLocID = "";
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	super.doBuildTemplate(data, context);
		int iOp = data.getParameters().getInt("op");
		int iCond = data.getParameters().getInt("Condition");
		int iStat = data.getParameters().getInt("Status");
		String sKey = data.getParameters().getString("Keywords");
		sLocID = data.getParameters().getString("LocationId");
		
		initSession(data);
		try 
		{
			if (iOp == i_FIND_ITEM && StringUtil.isNotEmpty(sKey)) 
			{
				if(iCond == 1) sCode = sKey;
				if(iCond == 2) sName = sKey;
				vIS = ItemSerialTool.findData("",sCode,sName,"",sLocationID,"",iStat,null);
				data.getSession().setAttribute("vIS",vIS);
			}		
			if (iOp == i_SCAN_SN && vIS != null && vIS.size() > 0) 
			{
				String sSN = data.getParameters().getString("SerialNo");
				List vSN = BeanUtil.filterListByFieldValue(vIS, "serialNo", sSN);
				if (vSN.size() > 0)
				{
					//exists
					ItemSerial oIS = (ItemSerial)vSN.get(0);
					mSN.put(oIS.getItemId() + oIS.getSerialNo(),oIS);
				}
				else
				{
					//not exists
					String sItemID = data.getParameters().getString("ItemId","");
					List vNE = (List) mSN.get("NotExists" + sItemID);
					if (vNE == null) vNE = new ArrayList();
					vNE.add(sSN);
					mSN.put("NotExists" + sItemID, vNE);
				}
				data.getSession().setAttribute("mSN",mSN);
			}				
			if (iOp == i_LOAD_EXCEL)
			{
				loadExcel(data,context);
			}
		}
		catch (Exception _oEx) 
		{
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put("mSN", mSN);
		context.put("vIS", vIS);		
    }
    
    void initSession(RunData data)
    {
		oSes = data.getSession();
		if (data.getSession().getAttribute("mSN") != null)
		{
			mSN = (Map)data.getSession().getAttribute("mSN");
		}
		else
		{
			mSN = new HashMap();
		}
		if (data.getSession().getAttribute("vIS") != null)
		{
			vIS = (List)data.getSession().getAttribute("vIS");
		}
		else
		{
			vIS = new ArrayList();
		}
    }
    
	private void loadExcel(RunData data, Context context)
	{
		try 
		{
			StringBuilder sEX = new StringBuilder();
			StringBuilder sNEX = new StringBuilder();
			StringBuilder sNF = new StringBuilder();
			
			//reset session var
			vIS = new ArrayList(); 
			mSN = new HashMap();
			
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			SerialNoLoader oLoader = new SerialNoLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			Map mESN = oLoader.getMapResult();
			Set vITM = mESN.keySet();
			Iterator iter = vITM.iterator();
			while(iter.hasNext())
			{
				String sItemCode = (String)iter.next();
				Item oItem = ItemTool.getItemByCode(sItemCode);
				if (oItem != null)
				{
					String sItemID = oItem.getItemId();
					List vSN = (List)mESN.get(sItemCode);
					for (int i = 0; i < vSN.size(); i++)
					{
						String sSN = (String)vSN.get(i);
						System.out.println("Serial No " + sSN);

						List vEIS = ItemSerialTool.findData(sItemID,"","","",sLocID,sSN,2,null);
						System.out.println(" vEIS " + vEIS.size());

						if (vEIS.size() > 0)
						{
							ItemSerial oIS = (ItemSerial) vEIS.get(0);
							vIS.add(oIS);
							mSN.put(sItemID + sSN, oIS);
							sEX.append(StringUtil.left(sItemCode,15)).append(StringUtil.left(sSN,25)).append("FOUND").append("\n");
						}
						else
						{
							System.out.println(" NOT EXISTS " + sSN);
							
							List vNE = (List) mSN.get("NotExists" + sItemID);
							if (vNE == null) vNE = new ArrayList();
							vNE.add(sSN);
							mSN.put("NotExists" + sItemID, vNE);							
							sNEX.append(StringUtil.left(sItemCode,15)).append(StringUtil.left(sSN,25)).append("NOT FOUND").append("\n");
						}
					}
				}
				else
				{
					sNF.append(StringUtil.left(sItemCode,15)).append(" IS INVALID ITEM ").append("\n");					
				}
				context.put("ExcelEX", sEX);
				context.put("ExcelNEX", sNEX);
				context.put("ExcelNF", sNF);
				
				data.getSession().setAttribute("vIS",vIS);
				data.getSession().setAttribute("mSN",mSN);
			}			
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}
}
