package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;

public class ItemTransferDetailReport extends Default
{
	private static final Log log = LogFactory.getLog(ItemTransferDetailReport.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {	        		    	    
	        	String sLocationID = data.getParameters().getString("LocationId");
	    	    String sToLocationID = data.getParameters().getString("ToLocationId");
	    	    int iStatus = data.getParameters().getInt("Status", i_MULTISTAT); //i_PROCESSED -> 9 PROCESSED / RECEIVED
	    	    //String sKategoriID = data.getParameters().getString("KategoriId","");
                //List vKategoriId = null;
	    	    	    	    
	    	    List vResult = 
	    	    	ItemTransferTool.findData(dStart, dEnd, sLocationID, sToLocationID, sAdjTypeID, iStatus);

	    	    context.put ("vResult", vResult);
    	    }
	    }   
    	catch (Exception _oEx) 
		{
    		log.error (_oEx);
    		_oEx.printStackTrace();
    		data.setMessage("Report LookUp Failed : " + _oEx.getMessage());
    	}    
    }    
}
