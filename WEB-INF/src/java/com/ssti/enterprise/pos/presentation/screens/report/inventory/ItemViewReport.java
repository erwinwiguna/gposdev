package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class ItemViewReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");    		
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}