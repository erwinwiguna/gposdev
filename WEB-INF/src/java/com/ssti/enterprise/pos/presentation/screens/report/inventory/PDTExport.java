package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;


public class PDTExport extends TransactionSecureScreen
{
    //start screen methods
	protected static final int i_EXPORT  = 1;
	
	public void doBuildTemplate ( RunData data, Context context )
    {	
    	super.doBuildTemplate(data, context);
		int iOp = data.getParameters().getInt("op");
		try 
		{
			if ( iOp == i_EXPORT ) 
			{
				exportData (data);
				displayFile(data, context);
				context.put("mResult", mResult);
			}				
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
    }
	
	private void displayFile(RunData data, Context context) 
	{
		File oFile = new File(IOTool.getRealPath(data, "tmp"));
		context.put("files", oFile.listFiles());		
	}

	static final String s_LOC  = "Location";
	static final String s_VEND = "Vendor";
	static final String s_ITEM = "Item";
	static final String s_ILVL = "InvLevel";
	static final String s_PORD = "PO";	
	static final String s_DATA = "vData";	
		
	Map mResult = null;
	String sKatID = "";
	String sLocID = "";
	String sLCode = "";
	Date dTrans = null;
	public void exportData(RunData data)
		throws Exception
	{
		//delete all file in dir
		File oFile = new File(IOTool.getRealPath(data, "tmp"));
		FileUtils.cleanDirectory(oFile);
		
		mResult = new HashMap();
		ParameterParser f = data.getParameters();
		sKatID = f.getString("KategoriId");
		sLocID = f.getString("LocationId");
		sLCode = LocationTool.getCodeByID(sLocID);
		dTrans = f.getDate("TransDate");
		
		if(f.getBoolean(s_LOC))
		{
			List vLocs = LocationTool.getAllLocation();			
			mResult.put(s_LOC, vLocs.size());

			VelocityContext oCtx = new VelocityContext();			
			oCtx.put(s_DATA, vLocs);
			genFile(data, oCtx, "print/pdt/Location.vm", "tmp/DN_str");			
		}
		if(f.getBoolean(s_VEND))
		{
			List vVends = VendorTool.getAllVendor();			
			mResult.put(s_VEND, vVends.size());

			VelocityContext oCtx = new VelocityContext();			
			oCtx.put(s_DATA, vVends);
			genFile(data, oCtx, "print/pdt/Vendor.vm", "tmp/DN_vendor");			
		}
		if(f.getBoolean(s_ITEM))
		{
			List vKatID = KategoriTool.getChildIDList(sKatID);
			vKatID.add(sKatID);			
			List vItems = ItemTool.getItemByKategoriID(vKatID);
			mResult.put(s_ITEM, vItems.size());
			
			VelocityContext oCtx = new VelocityContext();	
			oCtx.put(s_DATA,vItems);
			genFile(data, oCtx, "print/pdt/Item.vm", "tmp/DN_item");			
		}
		if(f.getBoolean(s_ILVL))
		{
			List vInvLvl = ItemInventoryTool.findItemInv(sKatID,sLocID,"");
			mResult.put(s_ILVL, vInvLvl.size());
			
			VelocityContext oCtx = new VelocityContext();
			oCtx.put(s_DATA,vInvLvl);
			genFile(data, oCtx, "print/pdt/InvLevel.vm", "tmp/DN_invlevel");
		}
		if(f.getBoolean(s_PORD))
		{
			List vPO = PurchaseOrderTool.findData(-1, "", DateUtil.getStartOfDayDate(dTrans), DateUtil.getEndOfDayDate(dTrans), "", sLocID, i_PO_ORDERED);
			mResult.put(s_PORD, vPO.size());
			
			VelocityContext oCtx = new VelocityContext();
			oCtx.put(s_DATA, vPO);
			genFile(data, oCtx, "print/pdt/PO.vm", "tmp/DN_purorder");
		}		
	}
	
	protected void genFile(RunData data, VelocityContext oCtx, String _sTPL, String _sFName)
		throws Exception
	{    	
    	oCtx.put("data", data);      
		oCtx.put("katid", sKatID);
    	oCtx.put("locid", sLocID);
		oCtx.put("lcode", sLCode);
		oCtx.put("discount", new DiscountTool());
    	oCtx.put("fd",data.getParameters());
    	oCtx.put("s",StringUtil.getInstance());
    	Context oGlobal = TurbinePull.getGlobalContext();		
    	for (int i = 0; i < oGlobal.getKeys().length; i++)
    	{
    		String sKey = (String) oGlobal.getKeys()[i];
    		oCtx.put(sKey, oGlobal.get(sKey));
    	}		
    	TurbinePull.populateContext(oCtx, data);
    	
    	StringWriter oWriter = new StringWriter ();
    	Velocity.mergeTemplate(_sTPL, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
    	
		String sDate = CustomFormatter.formatCustomDate(DateUtil.getTodayDate(), "yyMMdd");    	
		String sPath = IOTool.getRealPath(data, _sFName + ".txt");

		System.out.println("Path: " + sPath);
		
		FileWriter oFW = new FileWriter(sPath);
		BufferedWriter oBW = new BufferedWriter(oFW);
		oBW.write(oWriter.toString());
		oBW.close();
	}	
}
