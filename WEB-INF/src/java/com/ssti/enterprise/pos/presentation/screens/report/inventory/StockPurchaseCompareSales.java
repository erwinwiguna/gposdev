package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class StockPurchaseCompareSales extends StockMovementSummaryReport
{
	static final Log log = LogFactory.getLog (StockPurchaseCompareSales.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);    
	}   
}