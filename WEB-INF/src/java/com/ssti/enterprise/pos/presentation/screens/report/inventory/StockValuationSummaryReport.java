package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.report.InventoryReportTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.ssti.framework.turbine.LargeSelectHandler;

public class StockValuationSummaryReport extends Default
{
	static final Log log = LogFactory.getLog (StockValuationSummaryReport.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		ParameterParser formData = data.getParameters();    		
    		if ( StringUtil.isEmpty(formData.getString("op")) && formData.getInt("ViewLimit") > 0)
    		{	    		
    			int iLimit = formData.getInt("ViewLimit",50);
	    		String sKatID = formData.getString("KategoriId");
	    		String sKeyword = formData.getString("Keywords");
	    		int iCond = formData.getInt("Condition");
	    		int iSortBy = formData.getInt("SortBy");
	    		int iStockStat = formData.getInt("StockStatus",-1);
	    		boolean bInTrans = formData.getBoolean("InTrans");
	    		Date dStart = DateUtil.getStartOfDayDate(CustomParser.parseDate(formData.getString("StartDate")));
	    		Date dEnd = DateUtil.getEndOfDayDate(CustomParser.parseDate(formData.getString("EndDate")));
	    		
	    		LargeSelect vTR = null;
	    		if (!bInTrans)
	    		{
		    		vTR = ItemTool.findData(iCond,
		    								sKeyword,
											sKatID,
											iSortBy,
											iLimit,
											"","",sLocationID,iStockStat,-1,-1,i_INVENTORY_TRANSACTION);
		    	}
	    		else
	    		{
		    		vTR = InventoryReportTool.getActiveItem(iCond,
							sKeyword,
							sKatID,
							iSortBy,
							iLimit,
							sLocationID, "", dStart, dEnd);
	    			
	    		}
	    		data.getUser().setTemp("findItemResult",vTR);	    		
	    		formData.setString( "op","next");
    		}
	    	LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}   
}