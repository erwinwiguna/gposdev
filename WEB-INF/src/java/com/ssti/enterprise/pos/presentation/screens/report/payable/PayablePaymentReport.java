package com.ssti.enterprise.pos.presentation.screens.report.payable;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;

public class PayablePaymentReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);    	
    	try 
    	{
    		int iCFStatus = data.getParameters().getInt("CfStatus");
    		context.put ("Payments", PayablePaymentTool.findTrans(
	    		sVendorID, sBankID, sLocationID, sCFTID, dStart, dEnd, iStatus, iCFStatus));    	
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Payment Report Failed : " + _oEx.getMessage());
    	}    
    }    
}
