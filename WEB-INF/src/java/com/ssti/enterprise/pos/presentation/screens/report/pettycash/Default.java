package com.ssti.enterprise.pos.presentation.screens.report.pettycash;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PettyCash;
import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.financial.PettyCashTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/pettycash/Default.java,v $
 * Purpose: this class is PettyCash Report Screen
 *
 * @author  $Author: albert $
 * @version $Id: Default.java,v 1.1 2008/06/29 07:09:31 albert Exp $
 * @see PettyCash
 *
 * $Log: Default.java,v $
 * Revision 1.1  2008/06/29 07:09:31  albert
 * *** empty log message ***
 *
 *
 */
 
public class Default extends ReportSecureScreen
{	
	@Override
	public void setParams(RunData data) 
	{
		super.setParams(data);
	}
	
	public void doBuildTemplate(RunData data, Context context) 
	{
    	bPrintable = data.getParameters().getBoolean("printable");
    	bExcel = data.getParameters().getBoolean("excel");
    	if (bPrintable) 
    	{
    		data.setLayoutTemplate("/Printable.vm");
     	    context.put("printable", Boolean.valueOf(bPrintable));	
     	    context.put("excel", Boolean.valueOf(bExcel));	
    	}
		context.put ("pettycash", PettyCashTool.getInstance());
	}
}
