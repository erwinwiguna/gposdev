package com.ssti.enterprise.pos.presentation.screens.report.purchase;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source$
 * Purpose: 
 *
 * @author  $Author$
 * @version $Id$
 *
 * $Log$
 * Revision 1.1  2006/03/22 15:49:17  albert
 * *** empty log message ***
 *
 */

public class PurchaseReportScreen extends ReportSecureScreen 
{    
	
	int iStatus = 0;
	String sVendorID = null;	    	
	String sKategoriID = null;
	String sKeywords = null;
	int iGroupBy = 0;
	int iCondition = 0;
	
	public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
		
    	iStatus = data.getParameters().getInt("Status");
    	sVendorID = data.getParameters().getString("VendorID");	    	
    	sKategoriID = data.getParameters().getString("KategoriID");
    	String sKeywords = data.getParameters().getString("Keywords");
    	iGroupBy = data.getParameters().getInt("GroupBy");
    	iCondition = data.getParameters().getInt("Condition");
		
    	String sDisplayPRID = data.getParameters().getString ("DisplayPRId", "");
    	String sDisplayPIID = data.getParameters().getString ("DisplayPIId", "");
    	String sDisplayPOID = data.getParameters().getString ("DisplayPOId", "");
    	
    	try
		{
    		if (!sDisplayPOID.equals(""))
    		{
    			context.put ("PODetails", PurchaseOrderTool.getDetailsByID (sDisplayPOID));
    		}
    		
    		if (!sDisplayPRID.equals(""))
    		{
    			context.put ("PRDetails", PurchaseReceiptTool.getDetailsByID (sDisplayPRID));
    		}
    		
    		if (!sDisplayPIID.equals(""))
    		{
    			context.put ("PIDetails", PurchaseInvoiceTool.getDetailsByID (sDisplayPIID));
    		}
		}
    	catch (Exception ex)
		{
    		log.error (ex);
		}
    }  
}
