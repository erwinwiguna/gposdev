package com.ssti.enterprise.pos.presentation.screens.report.purchase;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class UncompleteOrderList extends PurchaseReportScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
		{
	    	super.doBuildTemplate(data, context);
	    	super.setParams(data);

	    }   
    	catch (Exception _oEx) {
    		log.error(_oEx);
    	}    
    }    
}
