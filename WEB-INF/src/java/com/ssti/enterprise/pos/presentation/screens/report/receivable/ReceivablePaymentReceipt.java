package com.ssti.enterprise.pos.presentation.screens.report.receivable;

import java.util.ArrayList;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.framework.tools.SqlUtil;

public class ReceivablePaymentReceipt extends Default
{
    private static final String sGroup = "Grop Receivable Payment Receipt";
    private static final String sView = "View Payment";
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
        try 
    	{
    	    int iOp = data.getParameters().getInt("Op", 0);
    	    if (iOp == 1 ) //after submit group
    	    {
    	    	int iNo = data.getParameters().getInt("No");
    	    	List vID = new ArrayList (iNo);
    	    	for (int i = 0; i < iNo; i++)
    	    	{
    	    		boolean bChecked = data.getParameters().getBoolean("cb" + i);
    	    		if (bChecked)
    	    		{
    	    			String sID = data.getParameters().getString("id" + i);
    	    			vID.add (sID);
    	    		}
    	    	}
    	    	context.put ("bGrouped", Boolean.valueOf(true));                      
    	    	context.put ("sSelectedID", SqlUtil.convertToINMode(vID));                      
    	    	context.put ("Payments", ReceivablePaymentTool.getArPayments(vID));
    	    }
    	    else if (iOp == 2)//for printing the page
    	    {
    	    	int iNo = data.getParameters().getInt("No");
    	    	List vID = new ArrayList (iNo);
    	    	for (int i = 0; i < iNo; i++)
    	    	{
    	    		String sID = data.getParameters().getString("id" + i);
    	    		vID.add (sID);
    	    	}    	        
    	    	context.put ("bGrouped", Boolean.valueOf(true));                        
    	    	context.put ("Payments", ReceivablePaymentTool.getArPayments(vID));    	    
    	    }
    	    else
    	    {    	    
    	    	context.put ("Payments", ReceivablePaymentTool.findTrans(
    	    		sCustomerID, sBankID, sLocationID, sCFTID, dStart, dEnd, iStatus, 2));
    	    }
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Payment Report Failed : " + _oEx.getMessage());
    	}
    }    
}
