package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.report.SalesReportTool;

public class ItemSalesReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {
	    	    setParams(data);
	        	
	    	    String sKategoriID = data.getParameters().getString("KategoriId");
	    	    String sVendorID = data.getParameters().getString("VendorId","");
	    	    String sEmployeeID = data.getParameters().getString("EmployeeId","");
	    	    String sEmpDetID = data.getParameters().getString("EmplDetId","");	    	    
	    	    String sCashier = data.getParameters().getString("Cashier","");
	    	    String sColor = data.getParameters().getString("Color","");
	    	    boolean bIncTax = data.getParameters().getBoolean("IncludeTax",false);	    	    
	    	    boolean bIncRet = data.getParameters().getBoolean("IncludeReturn",false);
	    	    boolean bDiscount = data.getParameters().getBoolean("IncludeDiscount",false);
	    	    int iSortBy = data.getParameters().getInt("SortBy",1);
	    	    
	    	    List vResult = SalesReportTool.getTotalSalesPerItem(
	    	    	dStart, dEnd, sLocationID, sDepartmentID, sProjectID, sKategoriID, "", 
	    	    	sVendorID, sEmployeeID, sEmpDetID, sCashier, sColor, bIncTax, bDiscount, i_TRANS_PROCESSED, iSortBy);
	    	    context.put ("vResult", vResult);
	    	    
	    	    if (bIncRet)
	    	    {
	    	    	List vReturn = SalesReportTool.getTotalReturnPerItem(
	    	    			dStart, dEnd, sLocationID, sDepartmentID, sProjectID, sKategoriID, "", 
	    	    	    	sVendorID, sEmployeeID, sCashier, sColor, bIncTax, bDiscount, i_TRANS_PROCESSED, iSortBy);

	    	    	context.put ("vReturn", vReturn);		
	    	    }
            }
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    }    
}
