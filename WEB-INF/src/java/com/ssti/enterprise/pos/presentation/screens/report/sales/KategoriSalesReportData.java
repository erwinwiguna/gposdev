package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.math.BigDecimal;

public class KategoriSalesReportData 
{
	private String     KategoriId;    
	private String     KategoriCode;
    private String     Description;
    private int        KategoriLevel;
    private String     ParentId;
    private BigDecimal TotalQty;
    private BigDecimal TotalAmount;
    private BigDecimal TotalCost;
    private BigDecimal GrossProfit;
    private BigDecimal Percentage;
    private int		   TotalTrans;

	public String     getKategoriId     () { return KategoriId      ;  } 
	public String     getKategoriCode   () { return KategoriCode    ;  }
    public String     getDescription    () { return Description     ;  }
    public int        getKategoriLevel  () { return KategoriLevel   ;  }
    public String     getParentId       () { return ParentId        ;  }
    public BigDecimal getTotalQty       () { return TotalQty        ;  }
    public BigDecimal getTotalAmount    () { return TotalAmount     ;  }
    public BigDecimal getTotalCost      () { return TotalCost       ;  }
    public BigDecimal getGrossProfit    () { return GrossProfit     ;  }
    public BigDecimal getPercentage     () { return Percentage      ;  }
    public int        getTotalTrans  	() { return TotalTrans   	;  }

	public void setKategoriId    (String     _oValue) { this.KategoriId     = _oValue;  } 
	public void setKategoriCode  (String     _oValue) { this.KategoriCode   = _oValue;  }
    public void setDescription   (String     _oValue) { this.Description    = _oValue;  }
    public void setKategoriLevel (int        _oValue) { this.KategoriLevel  = _oValue;  }
    public void setParentId      (String     _oValue) { this.ParentId       = _oValue;  }    
    public void setTotalQty      (BigDecimal _oValue) { this.TotalQty       = _oValue;  }
    public void setTotalAmount   (BigDecimal _oValue) { this.TotalAmount    = _oValue;  }
    public void setTotalCost     (BigDecimal _oValue) { this.TotalCost      = _oValue;  }
    public void setGrossProfit   (BigDecimal _oValue) { this.GrossProfit    = _oValue;  }    
    public void setPercentage    (BigDecimal _oValue) { this.Percentage     = _oValue;  }
    public void setTotalTrans 	 (int        _oValue) { this.TotalTrans  	= _oValue;  }
    
}
