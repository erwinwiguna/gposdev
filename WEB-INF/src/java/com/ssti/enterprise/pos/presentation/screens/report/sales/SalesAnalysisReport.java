package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;

public class SalesAnalysisReport extends ReportSecureScreen
{
	Date dStart;
	Date dEnd;
	
	int iLimit; 
	int iRankBy;
	int iRankType;
	String sLocationID;
	String sKategoriID;
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
		{
	    	dStart = CustomParser.parseDate(data.getParameters().getString("StartDate"));
	    	dEnd = CustomParser.parseDate(data.getParameters().getString("EndDate"));

	        dStart  = DateUtil.getStartOfDayDate(dStart);
    	    dEnd = DateUtil.getEndOfDayDate(dEnd);
    	    
	    	context.put ("dStart", dStart);
    		context.put ("dEnd", dEnd);    		
		}   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Sales Summary Data Lookup Failed : " + _oEx.getMessage());
    	}    
    }    
}
