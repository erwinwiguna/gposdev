
package com.ssti.enterprise.pos.presentation.screens.report.sales;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.framework.turbine.LargeSelectHandler;

public class SalesOrderReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	try 
		{ 	    	 	    	 	    	
    		LargeSelectHandler.handleLargeSelectParameter(data, context, "findSalesOrderResult", "SalesOrders"); 	    	
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
    	}    
    }    
}
