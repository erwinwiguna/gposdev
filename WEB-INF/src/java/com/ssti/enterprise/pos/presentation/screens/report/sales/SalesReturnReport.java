package com.ssti.enterprise.pos.presentation.screens.report.sales;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/sales/SalesReturnReport.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesReturnReport.java,v 1.7 2006/03/22 15:49:25 albert Exp $
 *
 * $Log: SalesReturnReport.java,v $
 * Revision 1.7  2006/03/22 15:49:25  albert
 * *** empty log message ***
 *
 */
public class SalesReturnReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);    	
    	try 
		{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findSalesReturnResult", "Returns");    
	    }   
		catch (Exception _oEx) 
		{
			log.error(_oEx);
		}    	
    }    
}
