package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;

public class AccountSetup extends SetupSecureScreen
{
	private static final String[] a_PERM = {"View Chart Of Account"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
}
