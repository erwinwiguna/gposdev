package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.BarcodePaper;
import com.ssti.enterprise.pos.om.BarcodePaperPeer;
import com.ssti.enterprise.pos.tools.BarcodePaperTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/setup/BarcodePaperSetup.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: BarcodePaperSetup.java,v 1.1 2008/08/20 15:58:34 albert Exp $
 *
 * $Log: BarcodePaperSetup.java,v $
 * Revision 1.1  2008/08/20 15:58:34  albert
 * *** empty log message ***
 *
 * Revision 1.3  2008/06/29 07:10:05  albert
 * *** empty log message ***
 *
 * Revision 1.2  2006/03/29 08:14:31  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/10/10 03:52:26  albert
 * *** empty log message ***
 *
 */

public class BarcodePaperSetup extends SetupSecureScreen
{
	public void doBuildTemplate(RunData data, Context context)
    {
		ValueParser formData = data.getParameters();
		int iOp = formData.getInt("op");
		
		try
		{		
			String sPaperID = formData.getString("BarcodePaperId");
			if (iOp == 1)
			{
				boolean bIsDefault = formData.getBoolean("IsDefault");
				int iPaperType =formData.getInt("paperType");
				if(bIsDefault)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(BarcodePaperPeer.PAPER_TYPE, iPaperType);
					oCrit.add(BarcodePaperPeer.IS_DEFAULT, bIsDefault);
					List vData = BarcodePaperPeer.doSelect(oCrit);
					if (vData.size() > 0)
					{
						BarcodePaper oDefault = (BarcodePaper) vData.get(0);
						oDefault.setIsDefault (false);
						oDefault.save();
					}
				}
				BarcodePaper oPaper = null;
				boolean bNew = true;
				if (StringUtil.isNotEmpty(sPaperID))
				{
					oPaper = BarcodePaperTool.getBarcodePaperByID(sPaperID);
					bNew = false;
				}
				if (oPaper == null) oPaper = new BarcodePaper();	      	    		    
				formData.setProperties(oPaper);
				if (bNew) oPaper.setBarcodePaperId( IDGenerator.generateSysID() );
				oPaper.setIsDefault(bIsDefault);
				oPaper.save();
				data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
			}
			else if (iOp == 2)
			{
				if (StringUtil.isNotEmpty(sPaperID))
				{
					Criteria oCrit = new Criteria();
					oCrit.add(BarcodePaperPeer.BARCODE_PAPER_ID, sPaperID);
					BarcodePaperPeer.doDelete(oCrit);
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + ex.getMessage());
		}
    }
}
