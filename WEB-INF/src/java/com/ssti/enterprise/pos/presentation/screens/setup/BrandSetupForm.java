package com.ssti.enterprise.pos.presentation.screens.setup;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.imgscalr.Scalr;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Brand;
import com.ssti.enterprise.pos.om.BrandPeer;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.IDGenerator;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2012 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2015-09-07
 * - Initial Version
 * 
 * </pre><br>
 */
public class BrandSetupForm extends Default
{	
	public static final int i_OP_VIEW = 1;
	public static final int i_OP_SAVE = 2;
	public static final int i_OP_DEL = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", ItemFieldTool.getBrandByID(sID));
    		}
			if (iOp == i_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				Brand oData = ItemFieldTool.getBrandByID(sID);
				if (oData == null)
				{
					oData = new Brand();
					oData.setBrandId(IDGenerator.generateSysID());					
				}
				data.getParameters().setProperties(oData);
				processUpload(data, oData);
				oData.save();				
				ItemManager.getInstance().refreshBrand(oData.getId());
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				Brand oData = ItemFieldTool.getBrandByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(BrandPeer.BRAND_ID, oData.getBrandId());
					BrandPeer.doDelete(oCrit);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
					ItemManager.getInstance().refreshBrand(oData.getId());
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
    
    public void processUpload(RunData data, Brand _oBrand)
    	throws Exception
    {
		FileItem oFileItem = data.getParameters().getFileItem("picFile");	
		if (oFileItem != null && _oBrand != null)
		{
			String sFileName   = oFileItem.getName();					
			if (StringUtil.isNotEmpty(sFileName)) 
			{
				String sSeparator  = System.getProperty("file.separator");
				String sPathDest = PreferenceTool.getPicturePath();
				String sFilePath = IOTool.getRealPath(data, sPathDest);		
				String sFileExt = IOTool.getFileExtension(sFileName); 
				String sDate = CustomFormatter.formatCustomDate(DateUtil.getTodayDate(), "yyyyMMdd");
				sFileName = "brandLogo"+_oBrand.getBrandCode()+ sDate + sFileExt; 
				sFilePath = sFilePath + sSeparator + sFileName;					
				log.debug("File Path:" + sFilePath);

				File oImgFile = new File(sFilePath);
				oFileItem.write (oImgFile); //save picture file
				
				BufferedImage oImg = ImageIO.read(oImgFile);
				System.out.println("width:" + oImg.getWidth() + " heigth:" +oImg.getHeight());
				if(oImg.getHeight() > 300)
				{
					BufferedImage oRes = Scalr.resize(oImg, 300);							
					ImageIO.write(oRes, sFileExt.substring(1), oImgFile);
				}
				
				StringBuilder oSB = new StringBuilder();
				oSB.append(data.getContextPath());
				oSB.append(sPathDest);
				oSB.append(sFileName);        	
				String sFilePathToSafe = (oSB.toString()).replace('\\','/');				
				_oBrand.setBrandLogo(sFilePathToSafe);
				log.debug("File Path:" + sFilePathToSafe);
				data.setMessage("Pic Uploaded Successfully to " + sFilePathToSafe);
			}
		}
    }
}