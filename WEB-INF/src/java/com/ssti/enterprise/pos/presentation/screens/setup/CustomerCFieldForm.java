package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.Date;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.CustomerCfield;
import com.ssti.enterprise.pos.om.CustomerCfieldPeer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.IDGenerator;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2012 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2015-09-07
 * - Initial Version
 * 
 * </pre><br>
 */
public class CustomerCFieldForm extends Default
{	
	public static final int i_OP_VIEW = 1;
	public static final int i_OP_SAVE = 2;
	public static final int i_OP_DEL = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", CustomerTool.getCFieldByID(sID));
    		}
			if (iOp == i_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				CustomerCfield oData = CustomerTool.getCFieldByID(sID);
				if (oData == null)
				{
					oData = new CustomerCfield();
					oData.setCfieldId(IDGenerator.generateSysID());					
				}
				data.getParameters().setProperties(oData);
				oData.setLastUpdateBy(data.getUser().getName());
				oData.setUpdateDate(new Date());				
				oData.save();				
				CustomerManager.getInstance().refreshCField(oData);
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				CustomerCfield oData = CustomerTool.getCFieldByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(CustomerCfieldPeer.CFIELD_ID, oData.getCfieldId());
					CustomerCfieldPeer.doDelete(oCrit);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
					CustomerManager.getInstance().refreshCField(oData);
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}