package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.presentation.SecureScreen;


public class DiscountSetup extends SecureScreen
{
	ParameterParser formData;
	
	private static final String[] a_PERM = {"View Discount Setup"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	formData = data.getParameters();
    	try
    	{    		
    		context.put("formData", formData);			
		}
		catch(Exception _oEx)
		{
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
		}
    }
}
