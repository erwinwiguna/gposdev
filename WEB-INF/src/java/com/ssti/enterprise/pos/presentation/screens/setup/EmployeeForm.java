package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.presentation.SecureScreen;

public class EmployeeForm extends SecureScreen
{
	private static final String[] a_PERM = {"View Employee Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (sMode.equals("delete") || (iOp == 4 || sMode.equals("update")) || sMode.equals("view") )
			{
				sID = data.getParameters().getString("id");
				context.put("Employee", EmployeeTool.getEmployeeByID(sID));
    		}
    		context.put("Roles", TurbineSecurity.getAllRoles().getRolesArray() );
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx);
    	}
    }
}
