package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.framework.tools.StringUtil;

public class EmployeeView extends EmployeeForm
{	
	public void doBuildTemplate(RunData data, Context context)
    {
        try 
        {
        	int iOp = data.getParameters().getInt ("op", -1);
            int iCondition = data.getParameters().getInt ("Condition", -1);
    		String sKeyword = data.getParameters().getString ("Keywords", "");
    		String sLocID = data.getParameters().getString ("LocationId", "");
    		String sAreaID = data.getParameters().getString ("SalesAreaId", "");
    		String sTerrID = data.getParameters().getString ("TerritoryId", "");    		
    		String sBossID = data.getParameters().getString ("BossId", "");
    		boolean bNonAct = data.getParameters().getBoolean("NonActive");
    		
    		List vEmployee = null;
    		if (StringUtil.isNotEmpty(sKeyword) || StringUtil.isNotEmpty(sAreaID) || StringUtil.isNotEmpty(sLocID) ||
    			StringUtil.isNotEmpty(sTerrID)  || StringUtil.isNotEmpty(sBossID) || bNonAct)
    		{
    			vEmployee = EmployeeTool.findData (iCondition, sKeyword, sLocID, sAreaID, sTerrID, sBossID, bNonAct);
    		}
    		else
    		{
    			if (iOp == 1)
    			{
    				vEmployee = EmployeeTool.getAllEmployee();
    			}
    		}
    		context.put ("Employees", vEmployee);
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();
        }
    }
}