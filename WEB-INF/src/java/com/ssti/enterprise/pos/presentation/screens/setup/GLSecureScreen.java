package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureScreen;

public class GLSecureScreen extends SecureScreen
{
	private static final String[] a_PERM = {"GL Setup", "View GL Setup"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
}
