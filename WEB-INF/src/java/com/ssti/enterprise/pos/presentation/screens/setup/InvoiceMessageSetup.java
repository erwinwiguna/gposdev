package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.Date;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.InvoiceMessage;
import com.ssti.enterprise.pos.om.InvoiceMessagePeer;
import com.ssti.enterprise.pos.tools.InvoiceMessageTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.IDGenerator;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change update & del, call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class InvoiceMessageSetup extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		context.put("invoicemessage", InvoiceMessageTool.getInstance());
		try
		{
			if (iOp == i_SETUP_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", InvoiceMessageTool.getByID(sID));
    		}
			if (iOp == i_SETUP_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				InvoiceMessage oData = InvoiceMessageTool.getByID(sID);
				InvoiceMessage oOld = null;
				if (oData == null)
				{
					oData = new InvoiceMessage();
					oData.setInvoiceMessageId(IDGenerator.generateSysID());					
				}		
				else
				{
					oOld = oData.copy();
				}
				data.getParameters().setProperties(oData);
				
				StringBuilder sContent = new StringBuilder();
				String[] aPref = data.getParameters().getStrings("Prefix");
				String[] aRows = data.getParameters().getStrings("Row");				
				for(int i = 0; i < aRows.length; i++)
				{
					sContent.append(aPref[i]);
					sContent.append(aRows[i]);
					if(i != aRows.length - 1)
					{
						sContent.append(s_LINE_SEPARATOR);
					}
				}
				oData.setContents(sContent.toString());
				System.out.println(oData.getContents());
				
				if(!isExists(oData))
				{
					oData.setLastUpdate(new Date());
					oData.setLastUpdateBy(data.getUser().getName());				
					oData.save();				
					
					if(oOld != null) UpdateHistoryTool.createHistory(oOld, oData, data.getUser().getName(), null);
					data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
				}
				else
				{
					data.setMessage("ERROR: Data Exists In Same Period");
				}				
			}
			if (iOp == i_SETUP_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				InvoiceMessage oData = InvoiceMessageTool.getByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(InvoiceMessagePeer.INVOICE_MESSAGE_ID, oData.getInvoiceMessageId());
					InvoiceMessagePeer.doDelete(oCrit);
					UpdateHistoryTool.createDelHistory(oData, data.getUser().getName(), null);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }

	private boolean isExists(InvoiceMessage _oData)
		throws Exception
	{
		if (_oData != null && _oData.getStartDate() != null && _oData.getEndDate() != null)
		{
			if (InvoiceMessageTool.getExists(_oData) != null) return true;
		}
		return false;
	}
}