package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.ItemMinPriceTool;

public class ItemMinPriceSetup extends ItemSecureScreen
{
	private static Log log = LogFactory.getLog(ItemMinPriceSetup.class);
    public void doBuildTemplate(RunData data, Context context)
    {
		context.put("itemminprice", ItemMinPriceTool.getInstance());
    }
}
