package com.ssti.enterprise.pos.presentation.screens.setup;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.torque.Torque;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemHistoryTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class ItemPriceUpdate extends ItemSecureScreen
{
	private static final String[] a_PERM = {"View Item Data", "Update Item Data", "View Item Cost"};

	protected boolean isAuthorized(RunData data) throws Exception 
	{
		return isAuthorized(data, a_PERM);
	}

	int ops = -1;	
	int iRound = 0;			
	double dMargin = 0;
	boolean bMinPriceOnly = false;
	String sUser = "";
	String op = "";

    public void doBuildTemplate(RunData data, Context context)
    {    	
		super.doBuildTemplate(data, context);
		sUser = data.getUser().getName();
		op = data.getParameters().getString("op");	
		
		if (StringUtil.isNotEmpty(data.getAction()) || StringUtil.isNotEmpty(op))
		{
			try 
	    	{
				LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");
	    	}
			catch(Exception e)
			{
				
			}
	    }
		else
		{
			ops = data.getParameters().getInt("ops");	
			iRound = data.getParameters().getInt("RoundTo",0);			
			dMargin = data.getParameters().getDouble("NewMargin");
			bMinPriceOnly = data.getParameters().getBoolean("MinPriceOnly");
						
			System.out.println("ops"+ ops);
			
			if (ops == 1) //update page
			{
				updatePage(data, context);
			}
			if (ops == 2) //update all page
			{
				
			}
			if (ops == 3) //update by rate
			{
				updateRate(data, context);
			}
			if (ops == 4) //update by changed last purchase
			{				
				updateLastPur(data, context);
			}						
		}		 	   
    }
    
	public void updatePage(RunData data, Context context)
	{
		LargeSelect oLargeData = (LargeSelect) data.getUser().getTemp("findItemResult");			
    	int iPage = data.getParameters().getInt("page", 1);
    	try
		{
			List vData = oLargeData.getPage(iPage);
			if (vData != null && vData.size() > 0)
			{
				for (int i = 0; i < vData.size(); i++)
				{
					Item oItem = (Item)vData.get(i);
					Item oOld = oItem.copy();
					
					double dLastPur = oItem.getLastPurchasePrice().doubleValue();
					double dRate = oItem.getLastPurchaseRate().doubleValue();
					if(dRate == 0) dRate = 1;
					dLastPur = dLastPur * dRate;
					double dNewPrice = calcPrice(dLastPur, oItem.getProfitMargin());												
					
					if(!bMinPriceOnly)
					{
						oItem.setItemPrice(new BigDecimal(dNewPrice));
					}
					if(bMinPriceOnly || PreferenceTool.getSalesUseMinPrice())
					{
						oItem.setMinPrice(new BigDecimal(dNewPrice));	
					}
					oItem.save();
		    		ItemManager.getInstance().refreshCache(oItem);
		    		ItemHistoryTool.createItemHistory (oOld, oItem, sUser, null);							
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			data.setMessage("ERROR: " + e.getMessage());
		}
	}
	
	public void updateRate(RunData data, Context context)
	{
		String sCurrID = data.getParameters().getString("CurrencyId");
		String sKatID = data.getParameters().getString("KategoriId");    				
		double dRate = data.getParameters().getDouble("NewRate");
		
		Connection oConn = null;
		ResultSet rs = null;
		try
		{
			Currency oCurr = CurrencyTool.getCurrencyByID(sCurrID);
			if (oCurr != null && dRate > 0 && dMargin > 0)
			{
				List vKatID = KategoriTool.getChildIDList(sKatID);
				vKatID.add(sKatID);
				String sKatIDs = SqlUtil.convertToINMode(vKatID);
				
				oConn = Torque.getConnection();
				String sCode = oCurr.getCode();
				String sSQL = "SELECT * FROM item WHERE last_purchase_curr = '" + sCode + "' AND kategori_id IN " + sKatIDs + " ";
				String sUpdate = "UPDATE item SET item_price = ?, min_price = ?, last_purchase_rate = ? WHERE item_id = ?";
				PreparedStatement oPstmt = oConn.prepareStatement(sUpdate);
				Statement oStmt = oConn.createStatement();
				rs = oStmt.executeQuery(sSQL);
				System.out.println(sSQL);
				while (rs.next())
				{
					String sID = rs.getString("item_id");
					String sItemCode = rs.getString("item_code");
					String sKategoriID = rs.getString("kategori_id");

					double dLastPur = rs.getDouble("last_purchase_price") * dRate;
					double dNewPrice = calcPrice(dLastPur, rs.getString("profit_margin"));	
									
					System.out.println("Item " + sItemCode + " NEW PRICE " + dNewPrice);
												
					oPstmt.setBigDecimal(1, new BigDecimal(dNewPrice));
					oPstmt.setBigDecimal(2, new BigDecimal(dNewPrice));
					oPstmt.setBigDecimal(3, new BigDecimal(dRate));
					oPstmt.setString(3, sID);    						
					oPstmt.executeUpdate();
					
					ItemManager.getInstance().refreshCache(sID, sItemCode, sKategoriID);    	    				
				}
			}
		}				
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				rs.close();
				oConn.close();						
			}
			catch (Exception e2)
			{
			}
		} 
	}
	
	public void updateLastPur(RunData data, Context context)
	{
		Connection oConn = null;
		ResultSet rs = null;
		//boolean bMinPriceOnly = data.getParameters().getBoolean("MinPriceOnly");
		String sD1 = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(data.getParameters().getDate("StartDate")));
		String sD2 = CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(data.getParameters().getDate("EndDate")));
		try
		{
			oConn = Torque.getConnection();
			String sSQL = "SELECT * FROM item WHERE item_id IN (SELECT item_id FROM item_update_history " 
					    + "WHERE update_part = 'LastPurchasePrice' "
					    + "AND update_date >= '" + sD1  +"' AND update_date <= '" + sD2 + "') ";
			
			System.out.println(sSQL);
			Statement oStmt = oConn.createStatement();
			rs = oStmt.executeQuery(sSQL);
			List vItem = new ArrayList();
			while (rs.next())
			{
				String sID = rs.getString("item_id");
				String sItemCode = rs.getString("item_code");
				String sKategoriID = rs.getString("kategori_id");
				double dLastPur = rs.getDouble("last_purchase_price");
				double dRate = rs.getDouble("last_purchase_rate");
				if(dRate == 0) dRate = 1;
				String sMargin = rs.getString("profit_margin");
				System.out.println("ItemCode: " + sItemCode + " Margin: " + sMargin + " Global Margin: " + dMargin);
				
				if(dMargin > 0 || (StringUtil.isNotEmpty(sMargin) && !StringUtil.equals(sMargin, "0")))
				{
					double dNewPrice = calcPrice((dLastPur * dRate), sMargin);					
					System.out.println("Item " + sItemCode + " NEW PRICE: " + dNewPrice);
												
					Item oItem = ItemTool.getItemByID(sID);
					Item oOld = oItem.copy();							
					if(!bMinPriceOnly)
					{
						oItem.setItemPrice(new BigDecimal(dNewPrice));
					}
					if(bMinPriceOnly || PreferenceTool.getSalesUseMinPrice())
					{
						oItem.setMinPrice(new BigDecimal(dNewPrice));
					}
					oItem.save(oConn);
												
					vItem.add(oItem);
					ItemHistoryTool.createItemHistory (oOld, oItem, data.getUser().getName(), null);

					ItemManager.getInstance().refreshCache(oItem); 
				}
			}
			context.put("Items", vItem);					
		}			
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				rs.close();
				oConn.close();						
			}
			catch (Exception e2)
			{
			}
		} 
	}
	
	private double calcPrice(double _dBasePrice, String sMargin)
	{
		double dItemMargin = 0;
		double dNewPrice = 0;
		if(StringUtil.isNotEmpty(sMargin) && !StringUtil.equals(sMargin, "0"))
		{						
			if(StringUtil.containsIgnoreCase(sMargin, "%"))
			{
				sMargin = StringUtil.replace(sMargin, "%", "");
				dItemMargin = Double.parseDouble(sMargin) / 100;
				dItemMargin = (dItemMargin / (1 - dItemMargin)) * 100;
				dNewPrice = _dBasePrice + (_dBasePrice * (dItemMargin/100));
			}
			else
			{
				dNewPrice = _dBasePrice + Double.parseDouble(sMargin);					
			}															
		}
		else
		{
			dItemMargin = dMargin / 100;
			dItemMargin = (dItemMargin / (1 - dItemMargin)) * 100;
			dNewPrice = _dBasePrice + (_dBasePrice * (dItemMargin/100));
		}
		dNewPrice = Calculator.roundNumber(dNewPrice, iRound, BigDecimal.ROUND_UP);
		return dNewPrice;
	}
}
