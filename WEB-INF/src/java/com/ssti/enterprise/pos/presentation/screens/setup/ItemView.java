package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CustomField;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.pref.UserPreference;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * ItemView screen class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemView.java,v 1.9 2007/03/05 16:04:54 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemView.java,v $
 * 
 * 2016-03-01
 * - refactor add method setUserPref so set preference function will be usable by other method
 * 
 * 2016-02-01
 * - change doBuildTemplate correct oUserPref
 * </pre><br>
 */
public class ItemView extends ItemSecureScreen
{
	static final Log log = LogFactory.getLog (ItemView.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{        	
    		setUserPref(data, context);
        	    					       
    		LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");    		    		
    	}
    	catch (Exception _oEx) 
    	{
    		log.error (_oEx);
    		_oEx.printStackTrace();	
    	}
    	
        context.put ("minHighlight", PreferenceTool.getSysConfig().getItemColorLtMin());
        context.put ("maxHighlight", PreferenceTool.getSysConfig().getItemColorGtMax());
        context.put ("rpHighlight",  PreferenceTool.getSysConfig().getItemColorLtRop());    	
	}
    
    public static void setUserPref(RunData data, Context context)
    	throws Exception
    {
    	
    	UserPreference oUserPref = PreferenceTool.getUserPreference(data);
    	List vFields  = ItemFieldTool.getAllCustomField();	
    	
    	if (data.getParameters().getBoolean("savePrev"))
        {
            if (oUserPref == null)
            {
            	oUserPref = new UserPreference();
            	oUserPref.setUserName(data.getUser().getName());
            }
        	
            for (int i = 1; i <= 50; i++)
            {
            	String fld = "f" + i;
            	try 
            	{
            		BeanUtil.setProperty(oUserPref, fld, data.getParameters().getBoolean(fld));	
				} 
            	catch (Exception e) 
            	{
            		e.printStackTrace();
            		log.error(e);
            	}
            }
                                            
        	String[] aFields = new String[vFields.size()];
        	int j = 0;
            for (int i = 0; i < vFields.size(); i++)
            {
            	CustomField oField = (CustomField) vFields.get(i);
            	if (data.getParameters().getBoolean(oField.getFieldName()))
            	{
            		aFields[j] = oField.getFieldName();
            		j++;
            	}
            }
            oUserPref.setCustomField(aFields);
            PreferenceTool.saveUserPref(oUserPref, data);
        }
    	
        context.put ("oUserPref", oUserPref);
        context.put ("vFields", vFields);
    }
}

//oUserPref.setF1 (data.getParameters().getBoolean("f1")       );  
//oUserPref.setF2 (data.getParameters().getBoolean("f2")       );  
//oUserPref.setF3 (data.getParameters().getBoolean("f3")       );  
//oUserPref.setF4 (data.getParameters().getBoolean("f4")       );  
//oUserPref.setF5 (data.getParameters().getBoolean("f5")       );  
//oUserPref.setF6 (data.getParameters().getBoolean("f6",false) );  
//oUserPref.setF7 (data.getParameters().getBoolean("f7",false) );  
//oUserPref.setF8 (data.getParameters().getBoolean("f8")       );  
//oUserPref.setF9 (data.getParameters().getBoolean("f9",false) );  
//oUserPref.setF10(data.getParameters().getBoolean("f10",false)); 
//oUserPref.setF11(data.getParameters().getBoolean("f11",false)); 
//oUserPref.setF12(data.getParameters().getBoolean("f12",false)); 
//oUserPref.setF13(data.getParameters().getBoolean("f13",false)); 
//oUserPref.setF14(data.getParameters().getBoolean("f14",false)); 
//oUserPref.setF15(data.getParameters().getBoolean("f15",false)); 
//oUserPref.setF16(data.getParameters().getBoolean("f16",false)); 
//oUserPref.setF17(data.getParameters().getBoolean("f17",false)); 
//oUserPref.setF18(data.getParameters().getBoolean("f18",false)); 
//oUserPref.setF19(data.getParameters().getBoolean("f19",false)); 
//oUserPref.setF20(data.getParameters().getBoolean("f20",false)); 
//oUserPref.setF21(data.getParameters().getBoolean("f21",false)); 
//oUserPref.setF22(data.getParameters().getBoolean("f22",false)); 
//oUserPref.setF23(data.getParameters().getBoolean("f23",false)); 
//oUserPref.setF24(data.getParameters().getBoolean("f24",false));
//oUserPref.setF25(data.getParameters().getBoolean("f25",false)); 
//oUserPref.setF26(data.getParameters().getBoolean("f26",false)); 
//oUserPref.setF27(data.getParameters().getBoolean("f27",false)); 
//oUserPref.setF28(data.getParameters().getBoolean("f28",false));
//oUserPref.setF29(data.getParameters().getBoolean("f29",false));
//oUserPref.setF30(data.getParameters().getBoolean("f30",false));
//oUserPref.setF31(data.getParameters().getBoolean("f31",false));
//oUserPref.setF32(data.getParameters().getBoolean("f32",false));
//oUserPref.setF33(data.getParameters().getBoolean("f33",false));
//oUserPref.setF34(data.getParameters().getBoolean("f34",false));
//oUserPref.setF35(data.getParameters().getBoolean("f35",false));
