package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.company.CompanySecureScreen;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

public class LocationSetup extends CompanySecureScreen
{
	static final int i_MAX_RECORD = PreferenceTool.getCustMaxSbMode();
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
    		int iTotalLoc = SqlUtil.countRecords("location");	
    		int iCond = data.getParameters().getInt("condition");
    		String sKeywords = data.getParameters().getString("keywords");
    		String sSiteID = data.getParameters().getString("SiteId");
    		int iLocType = data.getParameters().getInt("LocationType");
    		    		
			if (iTotalLoc <= i_MAX_RECORD || 
				StringUtil.isNotEmpty(data.getParameters().getString("eventSubmit_doFind")) || 
				StringUtil.isNotEmpty(data.getParameters().get("sort")))
			{	
				List vLoc = null;
				if(StringUtil.isNotEmpty(sKeywords) || StringUtil.isNotEmpty(sSiteID) || iLocType > 0)
				{
					vLoc = LocationTool.find(iCond, sKeywords, sSiteID, iLocType);
				}
				else //from cache
				{
					vLoc = LocationTool.getAllLocation();
				}
				context.put("Locations", vLoc);
			}
        }
        catch(Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}
