package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class MultiCurrencyDetails extends GLSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		String sID = data.getParameters().getString("id");
		try
		{
			if (sMode.equals ("delete") || sMode.equals ("update"))
			{
				String sDetailID = data.getParameters().getString("CurrencyDetailId");
				context.put("CurrencyDetail", CurrencyTool.getDetailByID (sDetailID, null));
			}

			//GetMasterData
			context.put("Currency", CurrencyTool.getCurrencyByID (sID));			
		}
		catch (Exception _oEx)
		{
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
		}
    }
}
