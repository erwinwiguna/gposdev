package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocaleTool;


public class MultiCurrencyForm extends GLSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		try
		{
			if (sMode.equals("delete") || sMode.equals("update"))
			{
				sID = data.getParameters().getString("id");
				context.put("Currency", CurrencyTool.getCurrencyByID(sID));
	    	}
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}
