package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PaymentEdc;
import com.ssti.enterprise.pos.om.PaymentEdcPeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.framework.tools.IDGenerator;

public class PaymentTermEDC extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_SETUP_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", PaymentTermTool.getPaymentEDCByID(sID));
    		}
			if (iOp == i_SETUP_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				PaymentEdc oData = PaymentTermTool.getPaymentEDCByID(sID);
				if (oData == null)
				{
					oData = new PaymentEdc();
					oData.setPaymentEdcId(IDGenerator.generateSysID());					
				}				
				data.getParameters().setProperties(oData);		
				oData.setPayHoliday(data.getParameters().getBoolean("PayHoliday"));
				
				oData.save();				
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_SETUP_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				PaymentEdc oData = PaymentTermTool.getPaymentEDCByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(PaymentEdcPeer.PAYMENT_EDC_ID, oData.getPaymentEdcId());
					PaymentEdcPeer.doDelete(oCrit);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
				}
			}
			if (iOp == 4) //find Data
			{
				Date dStart = data.getParameters().getDate("StartDate");
				Date dEnd = data.getParameters().getDate("EndDate");
				String sEDC = data.getParameters().getString("PaymentTermId");
				int iStatus = data.getParameters().getInt("Status",1);
				List v = PaymentTermTool.findPaymentEDC(sEDC, dStart, dEnd, iStatus);
				context.put("vData", v);
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}
