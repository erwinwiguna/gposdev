package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PettyCashTypeTool;

public class PettyCashTypeSetup extends BankSetup
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
        	context.put("PettyCashTypes", PettyCashTypeTool.getAllPettyCashType());
        }
        catch(Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}
