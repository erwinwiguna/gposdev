package com.ssti.enterprise.pos.presentation.screens.setup;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemHistoryTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * ItemView screen class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PriceCalculator.java,v 1.1 2009/05/04 02:02:19 albert Exp $ <br>
 *
 * <pre>
 * $Log: PriceCalculator.java,v $
 * Revision 1.1  2009/05/04 02:02:19  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/03/05 16:04:54  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PriceCalculator extends ItemSecureScreen
{
	static final Log log = LogFactory.getLog (PriceCalculator.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{   		            
            if (data.getParameters().getInt("save") == 1)
            {
            	String sVendorID = data.getParameters().getString("VendorId");            	
            	String sKatID = data.getParameters().getString("KategoriId");
            	double dMargin = data.getParameters().getDouble("MarginSetup");
            	int iFrom = data.getParameters().getInt("CalculateFrom");  
            	if (StringUtil.isNotEmpty(sKatID))
            	{
            		String sMsg = updatePrice(sKatID, sVendorID, data.getUser().getName(), dMargin, iFrom);
            		data.setMessage(sMsg);
            	}
            }
    	}
    	catch (Exception _oEx) 
    	{
    		log.error (_oEx);
    		_oEx.printStackTrace();	
    	}
	}
    
    public static String updatePrice(String _sKatID, 
    							     String _sVendorID, 
    							     String _sUserName,
    							     double _dMargin, 
    							     int _iFrom)
    	throws Exception
    {
    	List vKat = KategoriTool.getChildIDList(_sKatID);
    	vKat.add(_sKatID);
    	StringBuilder oSQL = new StringBuilder();
    	oSQL.append("SELECT item_id FROM item WHERE kategori_id IN ");
    	oSQL.append(SqlUtil.convertToINMode(vKat));
    	if (StringUtil.isNotEmpty(_sVendorID))
    	{
    		oSQL.append(" AND prefered_vendor_id = '").append(_sVendorID).append("'");
    	}
    	Connection oConn = null;
    	_sUserName = _sUserName + " from (Price Calculator)";
    	try 
    	{
    		oConn = BaseTool.beginTrans();
        	Statement oStmt = oConn.createStatement();
        	ResultSet rs = oStmt.executeQuery(oSQL.toString());
        	int iTotal = 0;
        	while (rs.next())
        	{
        		String sItemID = rs.getString("item_id");
        		Item oItem = ItemTool.getItemByID(sItemID, oConn);
        		if (oItem != null)
        		{
        			Item oOld = oItem.copy();
        			double dFrom = oItem.getLastPurchasePrice().doubleValue();
        			if (_iFrom == 1) //if from cost
        			{
        				dFrom = InventoryLocationTool.getItemCost(oItem.getItemId(), 
        							PreferenceTool.getLocationID(), oConn).doubleValue();
        			}
        			double dPrice = dFrom + ((_dMargin / 100) * dFrom);
        			oItem.setItemPrice(new BigDecimal(dPrice).setScale(2, 4));
        			oItem.save(oConn);
        			ItemHistoryTool.createItemHistory (oOld, oItem, _sUserName, oConn);
        			ItemManager.getInstance().refreshCache(oItem);
        			iTotal++;
        		}
        	}	
        	BaseTool.commit(oConn);
        	return ("Total " + iTotal + " Item Updated ");
		} 
    	catch (Exception e) 
    	{
    		e.printStackTrace();
    		BaseTool.rollback(oConn);
        	return ("ERROR: " + e.getMessage());
    	}    	
    }
}