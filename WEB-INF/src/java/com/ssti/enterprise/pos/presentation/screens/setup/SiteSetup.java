package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.company.CompanySecureScreen;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.SiteTool;

public class SiteSetup extends CompanySecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
        	context.put("Sites", SiteTool.getAllSite());
        }
        catch(Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}
