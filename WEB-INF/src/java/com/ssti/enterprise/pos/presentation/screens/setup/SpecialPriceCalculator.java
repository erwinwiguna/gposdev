package com.ssti.enterprise.pos.presentation.screens.setup;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CurrencyManager;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class SpecialPriceCalculator extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	if (data.getParameters().getString("mode","").equals("updateRate"))
    	{
    		Connection oConn = null;
    		String sCurrID = data.getParameters().getString("currid");
    		double dRate = data.getParameters().getDouble("rate");
    		if (StringUtil.isNotEmpty(sCurrID) && dRate > 0)
    		{
	    		try
	    		{
	    			Currency oCurr = CurrencyTool.getCurrencyByID(sCurrID);
	    			CurrencyDetail oDetail = CurrencyTool.getLatestDetailByID(sCurrID);
		    		Date dEnd = DateUtil.getEndOfDayDate(DateUtil.addDays(new Date(), 60));
		    		if (oDetail != null)
		    		{
		    			oDetail.setEndDate(dEnd);
		    			oDetail.setExchangeRate(new BigDecimal(dRate));
		    			oDetail.setUpdateDate(new Date());
		    			oDetail.save();
		    		}
		    		else
		    		{
		    			oDetail = new CurrencyDetail();
		    			oDetail.setCurrencyId(sCurrID);
		    			oDetail.setBeginDate(DateUtil.getStartOfDayDate(new Date()));
		    			oDetail.setEndDate(dEnd);
		    			oDetail.setExchangeRate(new BigDecimal(dRate));
		    			oDetail.setUpdateDate(new Date());
		    			oDetail.save();
		    		}
		    		CurrencyManager.getInstance().refreshCache(oCurr);
	    			log.debug("Currency Detail : " + oDetail);
		    		
		    		String sSQL = "SELECT item_id from item where last_purchase_curr = '" + sCurrID + "'";
		    		oConn = BaseTool.beginTrans();
		    		Statement oST = oConn.createStatement();
		    		ResultSet oRS = oST.executeQuery(sSQL);
		    		while (oRS.next())
		    		{
		    			Item oItem = ItemTool.getItemByID(oRS.getString(1));
		    			double dLP = oItem.getLastPurchasePrice().doubleValue();
		    			double dFreight = 0;
		    			String sFreight = ItemFieldTool.getValue(oItem.getItemId(), "Freight Cost");
		    			if (StringUtil.isNotEmpty(sFreight))
		    			{
		    				dFreight = Double.parseDouble(sFreight);
		    			}
		    			double dAltCost = (dLP * dRate) + dFreight;
		    			ItemFieldTool.saveField(oItem.getItemId(), "Alt Cost", 
		    					new BigDecimal(dAltCost).setScale(2,4).toString());
		    			
		    			double dMargin = 0;
		    			if (StringUtil.isNotEmpty(oItem.getProfitMargin()))
		    			{
		    				dMargin = Double.parseDouble(oItem.getProfitMargin());
		    			}		    			
		    			double dNewPrice = dAltCost + (dMargin / 100 * dAltCost);
		    			
		    			log.debug("Item  : " + oItem.getItemName() + " NEW PRICE : " + dNewPrice);
 
		    			oItem.setItemPrice(new BigDecimal(dNewPrice).setScale(2,4));
		    			oItem.save(oConn);
		    			ItemManager.getInstance().refreshCache(oItem);
		    		}		    		
		    		BaseTool.commit(oConn);
	    			data.setMessage("Currency Rate and Item Price Updated Successfully "); 
	    		}
	    		catch (Exception _oEx)
	    		{
	    			try 
	    			{
						BaseTool.rollback(oConn);
					} 
	    			catch (Exception e) 
	    			{
	    				data.setMessage("ERROR ROLLBACK UPDATING PRICE : " + _oEx.getMessage());
						e.printStackTrace();
					}
	    			data.setMessage("ERROR UPDATING PRICE : " + _oEx.getMessage());
	    			_oEx.printStackTrace();
	    		}
    		}
    	}
		else if (data.getParameters().getString("mode","").equals("updateAllPrice"))
		{			
			double dMargin = data.getParameters().getDouble("profitMargin");
			if (dMargin > 0)
			{
				String sSQL = "UPDATE item SET item_price = (last_purchase_price * last_purchase_rate) + " +
							  "((last_purchase_price * last_purchase_rate)  * " + dMargin + "/100)";
				try 
				{
    				int i = BasePeer.executeStatement(sSQL);				
    				ItemManager.getInstance().refreshCache(null);
    				data.setMessage(i + " Item Updated Successfully "); 
				} 
				catch (Exception _oEx)
				{
					data.setMessage("ERROR UPDATING PRICE : " + _oEx.getMessage());
				}
			}
		}
	}
}
