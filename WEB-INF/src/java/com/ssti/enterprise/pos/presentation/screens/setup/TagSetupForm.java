package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Tag;
import com.ssti.enterprise.pos.om.TagPeer;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.IDGenerator;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2012 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2015-09-07
 * - Initial Version
 * 
 * </pre><br>
 */
public class TagSetupForm extends Default
{	
	public static final int i_OP_VIEW = 1;
	public static final int i_OP_SAVE = 2;
	public static final int i_OP_DEL = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", ItemFieldTool.getTagByID(sID));
    		}
			if (iOp == i_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				Tag oData = ItemFieldTool.getTagByID(sID);
				if (oData == null)
				{
					oData = new Tag();
					oData.setTagId(IDGenerator.generateSysID());					
				}
				data.getParameters().setProperties(oData);
				oData.save();				
				ItemManager.getInstance().refreshTag(oData.getId(), oData.getTagName());
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				Tag oData = ItemFieldTool.getTagByID(sID);
				if (oData != null)
				{
					ItemManager.getInstance().refreshTag(oData.getId(), oData.getTagName());
					
					Criteria oCrit = new Criteria();
					oCrit.add(TagPeer.TAG_ID, oData.getTagId());
					TagPeer.doDelete(oCrit);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
					ItemManager.getInstance().refreshTag(oData.getId(),"");
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}