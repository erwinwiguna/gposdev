package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.BankReconcile;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.BankReconcileTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankReconcileTrans.java,v 1.2 2008/08/17 02:19:50 albert Exp $ <br>
 *
 * <pre>
 * $Log: BankReconcileTrans.java,v $
 * Revision 1.2  2008/08/17 02:19:50  albert
 * *** empty log message ***
 *
 * Revision 1.1  2008/06/29 07:10:31  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class BankReconcileTrans extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Cash Management"};
	public static final String s_BR = "br";
	public static final String s_BRD = "brDet";
	public static final String s_LASTREC = "LastReconciled";
	
    protected static final int i_OP_SET_BANK   = 1;
    protected static final int i_OP_NEW_BR     = 2;
    protected static final int i_OP_SAVE_BR    = 3;
    protected static final int i_OP_CANCEL_BR  = 4;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    protected HttpSession oSes;
    protected BankReconcile oBR;
    protected List vCF;

    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op");
    	initSession (data);
    	
    	try
    	{
	    	if (iOp == i_OP_SET_BANK)
	    	{
	        	data.getParameters().setProperties(oBR);
	        	
	    		String sBankID = data.getParameters().getString("BankId");
	        	Date dRecDate = CustomParser.parseDate(data.getParameters().getString("ReconcileDate"));
	    		if (dRecDate != null)
	    		{
	    			dRecDate = DateUtil.getEndOfDayDate(dRecDate);
	    		}
	        	oBR.setBankId(sBankID);
	        	oBR.setReconcileDate(dRecDate);
	        	oBR.setUserName(data.getUser().getName());
	        	
	        	Date dLastRec = null;
	        	BankReconcile oLastBR = BankReconcileTool.getLastReconcile(sBankID, null);
	        	if (oLastBR != null)  
	        	{
	        		dLastRec = oLastBR.getReconcileDate();
		        	oBR.setBankAmount(oLastBR.getCalculatedAmount());
	        	}	        	
	        	if (DateUtil.isDateEqual(dRecDate, dLastRec) || DateUtil.isBefore(dRecDate, dLastRec))
	        	{
	        		BankReconcile oOldBR = BankReconcileTool.getReconcileAtDate(sBankID, dRecDate);
	        		if (oOldBR != null)
	        		{
		        		oBR = oOldBR;
		        		dLastRec = BankReconcileTool.getLastReconDate(sBankID, dRecDate);
		        		oSes.setAttribute(s_BR, oBR);
	        		}
	        	}	        	
	        	vCF = CashFlowTool.getForReconcile(sBankID, dLastRec, dRecDate, null);
	        	oSes.setAttribute(s_BRD, vCF);	        	
	        	oSes.setAttribute(s_LASTREC, dLastRec);
	    	}
	    	else if (iOp == i_OP_NEW_BR)
	    	{
				oSes.setAttribute(s_BR, new BankReconcile());
				oSes.setAttribute(s_BRD, new ArrayList());
				oSes.setAttribute(s_LASTREC, null);				
	    	}
	    	else if (iOp == i_OP_SAVE_BR)
	    	{
	    		data.getParameters().setProperties(oBR);
	    		saveBR(data);	    		
	    	}	    
	    	else if (iOp == i_OP_CANCEL_BR)
	    	{
	    		data.getParameters().setProperties(oBR);
	    		cancelBR(data);	    		
	    	}		    	
	    	context.put("bankreconcile", BankReconcileTool.getinstance());
	    	context.put(s_BR, oSes.getAttribute(s_BR));
	    	context.put(s_BRD, oSes.getAttribute(s_BRD));	    	
	    	context.put(s_LASTREC, oSes.getAttribute(s_LASTREC));	    	
    	}
    	catch (Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
    
    private void initSession ( RunData data )
	{	
    	synchronized(this)
    	{
			oSes = data.getSession();
			if (oSes.getAttribute (s_BR) == null) 
			{
				oSes.setAttribute(s_BR, new BankReconcile());
				oSes.setAttribute(s_BRD, new ArrayList());
				oSes.setAttribute(s_LASTREC, null);
			}
			oBR = (BankReconcile) oSes.getAttribute (s_BR);
			vCF = (List) oSes.getAttribute(s_BRD);
    	}
	}
    
    private void saveBR(RunData data)
    	throws Exception
    {
    	int iNo = 1;
    	double dLast = data.getParameters().getDouble("LastAmount", 0);
    	double dCalculated = 0;
    	for (int i = 0; i < vCF.size(); i++)
    	{
    		CashFlow oCF = (CashFlow) vCF.get(i);
    		String sFormCFID = data.getParameters().getString("CashFlowId" + iNo, "");
    		boolean bRecon = data.getParameters().getBoolean("Reconciled" + iNo);
    		if (oCF.getCashFlowId().equals(sFormCFID))
    		{
	    		if (bRecon)
	    		{
	    			if (oCF.getCashFlowType() == i_DEPOSIT)
	    			{
	    				dCalculated = dCalculated + Calculator.precise(oCF.getCashFlowAmount(), 4);
	    			}
	    			if (oCF.getCashFlowType() == i_WITHDRAWAL)
	    			{
	    				dCalculated = dCalculated - Calculator.precise(oCF.getCashFlowAmount(), 4);
	    			}	    			
	    			oCF.setReconciled(true);
	    			oCF.setReconcileDate(oBR.getReconcileDate());
	    		}
	    		else
	    		{
	    			oCF.setReconciled(false);
	    			oCF.setReconcileDate(null);    			
	    		}
	    		iNo++;
    		}
    		else
    		{
    			throw new Exception ("Invalid Data ");
    		}
    	}
    	dCalculated = dCalculated + dLast;
    	oBR.setCalculatedAmount(new BigDecimal(dCalculated));
    	BankReconcileTool.saveData(oBR, vCF);
    	data.setMessage(LocaleTool.getString("save_success"));
    }
    
    
    private void cancelBR(RunData data)
    	throws Exception
    {
    	BankReconcileTool.cancelBR(oBR, vCF);
    	data.setMessage(LocaleTool.getString("delete_success"));
		
    	oSes.setAttribute(s_BR, new BankReconcile());
		oSes.setAttribute(s_BRD, new ArrayList());
		oSes.setAttribute(s_LASTREC, null);		    	
    }    
}
