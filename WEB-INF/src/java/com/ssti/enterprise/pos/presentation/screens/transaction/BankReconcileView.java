package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.BankReconcileTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankReconcileView.java,v 1.1 2008/06/29 07:10:31 albert Exp $ <br>
 *
 * <pre>
 * $Log: BankReconcileView.java,v $
 * Revision 1.1  2008/06/29 07:10:31  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class BankReconcileView extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Cash Management"};

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);    	
	    context.put("bankreconcile", BankReconcileTool.getinstance());
    }
}
