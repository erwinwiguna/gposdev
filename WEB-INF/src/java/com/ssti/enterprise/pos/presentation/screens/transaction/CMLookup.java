package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;

public class CMLookup extends TransactionSecureScreen
{
	protected List vARDP = null;      
    protected List vARDPTemp = null;  
	protected HttpSession oSes;

    public void doBuildTemplate(RunData data, Context context)
    {    	
    	try 
    	{
    		oSes = data.getSession();
			oSes.setAttribute (s_ARDPTMP, new ArrayList(5));
			
    		int iOp = data.getParameters().getInt("op");
    		log.debug("op " + iOp);
    		String sCurrencyID = data.getParameters().getString("CurrencyId");
    		Currency oCurr = CurrencyTool.getCurrencyByID(sCurrencyID);
    		if(iOp == 1)
    		{
				vARDPTemp = new ArrayList();
				
				int iTotal = data.getParameters().getInt("TotalNo");
				log.debug("Total Memo " + iTotal);
				StringBuilder oSelMemo = new StringBuilder();
				
				for (int i = 1; i <= iTotal; i++)
				{
					int iSel = data.getParameters().getInt("No" + i);
					String sMemoID = data.getParameters().getString("MemoId" + i); 
					String sMemoNo = data.getParameters().getString("MemoNo" + i); 
					double dClosingRate = data.getParameters().getDouble("ClosingRate" + i); 
					if (iSel > 0 && iSel == i)
					{
						log.debug("Selected Memo " + i + " " + sMemoNo);						
						insertToSession(sMemoID);
						if (oCurr != null && !oCurr.getIsDefault())
						{
							log.debug("Closing Rate " + i + " " + dClosingRate);
							//update closing rate first
							CreditMemoTool.updateClosingRate(sMemoID, dClosingRate);
						}
					}
				}
    		}
    		else
    		{
    			String sCustomerID = data.getParameters().getString("CustomerId");
        		
        		//get DP in session object
    			vARDP = (List) oSes.getAttribute (s_ARDP);
    			
        		//get unused credit memo by vendor, status, currency
        		List vCreditMemoTemp = CreditMemoTool.getCMByCustomerAndStatus(sCustomerID, 
                                        i_MEMO_OPEN,sCurrencyID);
                                
        		List vCreditMemo = new ArrayList();
                
                //filter unused memo, make sure memo used by invoice not displayed
        		for(int i = 0; i < vCreditMemoTemp.size(); i++)
        		{
        			CreditMemo oCreditMemo = (CreditMemo)vCreditMemoTemp.get(i);
        			if(!isMemoExist(oCreditMemo.getCreditMemoId()))
        			{
        				vCreditMemo.add(oCreditMemo);
        			}
        		}
        		context.put ("vMemo", vCreditMemo);
    		}
    	}
    	catch (Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		data.setMessage("CM Lookup Failed : " + _oEx.getMessage());
    	}
    }
    
    protected void insertToSession(String _sMemoID)
    	throws Exception
    {
		vARDPTemp.add (0, _sMemoID);
		oSes.setAttribute (s_ARDPTMP, vARDPTemp);    
	}
    
    protected boolean isMemoExist (String _sMemoID) 
    	throws Exception
    {

    	for (int i = 0; i < vARDP.size(); i++) 
    	{
    		List vExist = (List) vARDP.get(i);
    		for (int j = 0; j < vExist.size(); j++) 
    		{
    			String sExistID = (String) vExist.get(j);
    			if ( sExistID.equals(_sMemoID)) 
    			{
					return true;    			
    			}
    		}
    	}
    	return false;
    } 
}