package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

public class CalculateTotalAfterDisc extends TransactionSecureScreen
{
    private SalesTransaction oTR = null;
    private List vTD = null;
    private HttpSession oSes;
    
    int i_TR_SCALE = -1;

    public void doBuildTemplate(RunData data, Context context)
    {
		String sTotalDiscountPct = data.getParameters().getString("TotalDiscountPct", "");
    	i_TR_SCALE = data.getParameters().getInt("trScale", i_SALES_COMMA_SCALE);    	
    	if (StringUtil.isNotEmpty(sTotalDiscountPct))
    	{
    		try 
    		{
    			initSession ( data );
    			
    			double dInvoiceDiscount = 0;
    			oTR.setTotalAmount     (TransactionTool.countSubTotal (vTD));
    			oTR.setTotalDiscount   (TransactionTool.countDiscount (vTD));
    			oTR.setTotalTax        (TransactionTool.countTax(sTotalDiscountPct, vTD, oTR));
    			
    			double dTotalAmount = oTR.getTotalAmount().doubleValue();
    			if (StringUtil.isNotEmpty(sTotalDiscountPct)) 
    			{
    				//get discount for all 
    				//count total discount from total amount
    				double dTotalDiscountAmount = Calculator.calculateDiscount(sTotalDiscountPct,dTotalAmount);
    				double dSubTotalDiscount = oTR.getTotalDiscount().doubleValue();
    				oTR.setTotalDiscount (new BigDecimal(dTotalDiscountAmount + dSubTotalDiscount));
    				oTR.setTotalAmount (new BigDecimal(dTotalAmount - dTotalDiscountAmount));							
    			}
    			
    			//count Total Amount with tax
    			if(!oTR.getIsInclusiveTax())
    			{
    				dTotalAmount = oTR.getTotalAmount().doubleValue() + oTR.getTotalTax().doubleValue();
    				oTR.setTotalAmount (new BigDecimal(dTotalAmount));
    			}
    			
    			oTR.setTotalAmount(oTR.getTotalAmount().setScale(i_TR_SCALE, BigDecimal.ROUND_HALF_EVEN));
    			oTR.setTotalDiscount(oTR.getTotalDiscount().setScale(i_TR_SCALE, BigDecimal.ROUND_HALF_EVEN));
    			oTR.setTotalTax(oTR.getTotalTax().setScale(i_TR_SCALE, BigDecimal.ROUND_HALF_EVEN));
    			
    			context.put(s_TR, oTR);
    			InvoicePaymentTool.setPayment(oTR, data);
    		}
    		catch (Exception _oEx) 
    		{
    			_oEx.printStackTrace();
    			log.error(_oEx);
    		}
    	}
    }
    
    private void initSession ( RunData data )
	{
		oSes = data.getSession();
		if ( oSes.getAttribute (s_TR) == null )
		{
			oSes.setAttribute (s_TR, new SalesTransaction());
			if ( oSes.getAttribute (s_TD) == null )
			{
				oSes.setAttribute (s_TD, new ArrayList());
			}
		}
		oTR = (SalesTransaction) oSes.getAttribute (s_TR);
		vTD = (List) oSes.getAttribute (s_TD);
	}
}