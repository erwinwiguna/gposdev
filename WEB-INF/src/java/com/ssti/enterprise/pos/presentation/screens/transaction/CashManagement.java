package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashManagement.java,v 1.17 2008/06/29 07:10:31 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashManagement.java,v $
 * Revision 1.17  2008/06/29 07:10:31  albert
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/03 02:01:40  albert
 * *** empty log message ***
 *
 * Revision 1.15  2007/11/09 01:13:28  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/07/20 14:34:21  albert
 * *** empty log message ***
 *
 * Revision 1.13  2007/05/08 11:55:11  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CashManagement extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Cash Management"};
	
    protected static final int i_OP_ADD_DETAIL = 1;
	protected static final int i_OP_DEL_DETAIL = 2;
    protected static final int i_OP_NEW  	   = 3;
	protected static final int i_OP_VIEW       = 4;
	protected static final int i_OP_COPY       = 6;

    static final String s_MSG_EQUAL = LocaleTool.getString("cf_msg_equal");
    static final String s_MSG_BANK  = LocaleTool.getString("cf_msg_bank");
    
    protected CashFlow oCF = null;    
    protected List vCFD = null;
    protected HttpSession oSes;
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }

    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op"); 
		initSession (data);
		try
		{
			if (iOp == i_OP_NEW)
			{
				newTransaction (data);
			}
			else if (iOp == i_OP_ADD_DETAIL)
			{
				addDetailData (data);
			}
			else if (iOp == i_OP_DEL_DETAIL)
			{
				delDetailData (data);
			}
			else if (iOp == i_OP_VIEW)
			{
				getData (data, context);
			}
			else if (iOp == i_OP_COPY)
			{
				copyTrans (data);
			}
			
		}
		catch (Exception _oEx)
		{
		    _oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_CF, oSes.getAttribute (s_CF));
    	context.put(s_CFD, oSes.getAttribute (s_CFD));
    }
    
    private void initSession ( RunData data )
	{	
    	synchronized(this)
    	{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_CF) == null ) 
			{
				oSes.setAttribute (s_CF, new CashFlow());
				oSes.setAttribute (s_CFD, new ArrayList());
			}
			oCF = (CashFlow) oSes.getAttribute (s_CF);
			vCFD = (List) oSes.getAttribute (s_CFD);
    	}
	}

    private void newTransaction ( RunData data ) 
		throws Exception
	{
    	synchronized(this)
    	{
			oCF = new CashFlow();
			vCFD = new ArrayList();
			oSes.setAttribute (s_CF, oCF);
			oSes.setAttribute (s_CFD, vCFD);
    	}
	}
    
     private void addDetailData ( RunData data ) 
    	throws Exception
    {
        CashFlowTool.setHeaderProperties (oCF, vCFD, data, false);			    	 
		CashFlowJournal oCFD = new CashFlowJournal();
		data.getParameters().setProperties (oCFD);
        
	    boolean bSetMemo = data.getParameters().getBoolean("SetMemo");
	    if (bSetMemo)
	    {
	    	oCFD.setMemo(data.getParameters().getString("TxtAccountName"));
	    }
		
	    String sValid = isValid(oCFD.getAccountId(), data);
		if (sValid.equals("")) 
		{
			vCFD.add (oCFD);		
		}		
		else 
		{
		    data.setMessage (sValid); //send error message to screen
		}
        CashFlowTool.setHeaderProperties (oCF, vCFD, data, false);			
		oSes.setAttribute (s_CF, oCF); 
		oSes.setAttribute (s_CFD, vCFD);
    }
    
    private void delDetailData ( RunData data )
    	throws Exception
    {
        int iNo = data.getParameters().getInt("No");
        int iIdx = iNo - 1;
		if (vCFD.size() > iIdx) 
		{
		    log.debug ("** delete cash flow detail IDX : " + iIdx);
			vCFD.set (iIdx, new CashFlowJournal());	
			CashFlowTool.setHeaderProperties (oCF, vCFD, data, true);
			vCFD.remove (iIdx);	

			oSes.setAttribute (s_CF, oCF); 
			oSes.setAttribute (s_CFD, vCFD);
		}    	
    }

    private void getData (RunData data, Context context)
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try
    		{
    			synchronized (this)
    			{
	    			oCF = CashFlowTool.getHeaderByID (sID);
	    			vCFD = CashFlowTool.getDetailsByID (sID);
	    			oSes.setAttribute (s_CF, oCF);
					oSes.setAttribute (s_CFD, vCFD);
    			}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
    
    private String isValid (String _sAccountID, RunData data) 
    	throws Exception
    {
        Bank oBank = BankTool.getBankByID(data.getParameters().getString("BankId"));

        if (oBank == null) return s_MSG_BANK;

        String sBankAccountID = oBank.getAccountId();
        
        Account oAccount = AccountTool.getAccountByID(_sAccountID);
        
        if (sBankAccountID.equals(_sAccountID)) return s_MSG_EQUAL;
        
        if (oAccount != null)
        {
        	if (oAccount.getAccountType() == 1) //if CASH && used by other CASH/BANK must set subsidiary
        	{
                if (AccountTool.isAccountUsedByCashBank(_sAccountID))
                {
                    return LocaleTool.getString("gl_msg_cash");
                }
        	}
        	if (oAccount.getAccountType() == 2) //if AR
        	{
        		String sAR = AccountTool.getAccountReceivable(oCF.getCurrencyId(), "", null).getAccountId(); 
        		if (oAccount.getAccountType() == 2 && oAccount.getAccountId().equals(sAR))
        		{
        			return s_GL_MSG_AR_AP;
        		}
        	}
        	else if (oAccount.getAccountType() == 7) //if AP
        	{
        		String sAP = AccountTool.getAccountPayable(oCF.getCurrencyId(), "", null).getAccountId(); 
        		if (oAccount.getAccountType() == 2 && oAccount.getAccountId().equals(sAP))
        		{
        			return s_GL_MSG_AR_AP;
        		}
        	}
        	else if (oAccount.getAccountType() == 3) //if Inventory
        	{
        		if (AccountTool.isAccountUsedByInventory(oAccount.getAccountId()))
        		{
        			return s_GL_MSG_INV;
        		}
        	}        
        	/*
        	for (int i = 0; i < vCFD.size(); i++) 
        	{
        		CashFlowJournal oExistCFD = (CashFlowJournal) vCFD.get (i);
        		if ( oExistCFD.getAccountId().equals(_sAccountID)) 
        		{
        			return s_GL_MSG_EXIST;    			
        		}		
        	}
        	*/
        }
    	return "";
    } 
 
    private void copyTrans (RunData data)
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	CashFlow oCFC = CashFlowTool.getHeaderByID(sID);
    	oCF.setBankId(oCFC.getBankId());
    	oCF.setBankIssuer(oCFC.getBankIssuer());
    	oCF.setCashFlowAmount(oCFC.getCashFlowAmount());
    	oCF.setCashFlowType(oCFC.getCashFlowType());
    	oCF.setCurrencyId(oCFC.getCurrencyId());
    	oCF.setCurrencyRate(oCFC.getCurrencyRate());
    	oCF.setDescription(oCFC.getDescription());
    	oCF.setDueDate(oCFC.getDueDate());
    	oCF.setExpectedDate(oCFC.getExpectedDate());
    	oCF.setJournalAmount(oCFC.getJournalAmount());
    	oCF.setTransactionType(oCFC.getTransactionType());
    	oCF.setReferenceNo(oCFC.getReferenceNo());
    	
    	if (oCFC != null && StringUtil.isNotEmpty (sID))
    	{			
    		List vTrans = CashFlowTool.getDetailsByID(sID);
    		vCFD = new ArrayList(vTrans.size());
    		for (int i = 0; i < vTrans.size(); i++)
    		{
    			CashFlowJournal oDet = (CashFlowJournal) vTrans.get(i);
    			CashFlowJournal oNewDet = oDet.copy();
    			vCFD.add(oNewDet);
    		}
    	}
    	oSes.setAttribute (s_CFD, vCFD);
    	oSes.setAttribute (s_CF, oCF);
    }
}
