package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;

public class CreateVendorPriceList extends TransactionSecureScreen
{

    public void doBuildTemplate(RunData data, Context context)
    {
    	String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		try
    	{
			if (sMode.equals("update") || sMode.equals("delete"))
			{
				sID = data.getParameters().getString ("id");
				context.put("VendorPriceList", VendorPriceListTool.getVendorPriceListByID(sID));
    		}
    		
    		context.put("VendorPriceLists", VendorPriceListTool.getAllVendorPriceList());
    	}
    	catch(Exception _oEx)
    	{
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}
