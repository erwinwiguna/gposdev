package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.sales.CreditLimitValidator;

public class CustomerDataLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		String sCustomerID = data.getParameters().getString("CustomerId", null);
    		Customer oCustomer= CustomerManager.getInstance().getCustomerByID ( sCustomerID , null );	
    		context.put("oCustomer", oCustomer);
    		context.put("creditlimit", CreditLimitValidator.getInstance());
    	}
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		data.setMessage("Customer Data Lookup Failed : " + _oEx.getMessage());
    	}
    }
}