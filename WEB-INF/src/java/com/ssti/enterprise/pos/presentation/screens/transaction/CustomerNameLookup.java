package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.sales.CreditLimitValidator;
import com.ssti.framework.tools.StringUtil;

public class CustomerNameLookup extends TransactionSecureScreen
{
	protected List vCustomers = null;
    public void doBuildTemplate(RunData data, Context context)
    {
    	try     	
    	{
    		super.doBuildTemplate(data, context);
    		String sName = data.getParameters().getString("CustomerName");
    		String sCode = data.getParameters().getString("CustomerCode");
    		String sUser = data.getUser().getName();
    		if (StringUtil.isNotEmpty(sName))
    		{	
    			vCustomers = CustomerTool.getCustomerByName(sName,true,true);            
    		}
    		if (StringUtil.isNotEmpty(sCode))
    		{	
    			vCustomers = CustomerTool.getCustomerByCode(sCode,true,true);
            }
		    context.put ("vCustomers", vCustomers);
		    context.put("creditlimit", CreditLimitValidator.getInstance());
    	}
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		data.setMessage("Customer Lookup Failed : " + _oEx.getMessage());
    	}
    }
}