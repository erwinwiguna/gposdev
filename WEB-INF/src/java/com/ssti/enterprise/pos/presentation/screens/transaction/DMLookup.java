package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;

public class DMLookup extends TransactionSecureScreen
{
	protected List vAPDP = null;      
    protected List vAPDPTemp = null;  
	protected HttpSession oSes;

    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		oSes = data.getSession();
			oSes.setAttribute (s_APDPTMP, new ArrayList(5));
			
    		int iOp = data.getParameters().getInt("op");
    		log.debug("op " + iOp);
    		String sCurrencyID = data.getParameters().getString("CurrencyId");
    		Currency oCurr = CurrencyTool.getCurrencyByID(sCurrencyID);
    		if(iOp == 1)
    		{
				vAPDPTemp = new ArrayList();
				
				int iTotal = data.getParameters().getInt("TotalNo");
				log.debug("Total Memo " + iTotal);
				StringBuilder oSelMemo = new StringBuilder();
				
				for (int i = 1; i <= iTotal; i++)
				{
					log.debug("No" + i  + ":"  + data.getParameters().getInt("No" + i));
					
					int iSel = data.getParameters().getInt("No" + i);
					String sMemoID = data.getParameters().getString("MemoId" + i); 
					String sMemoNo = data.getParameters().getString("MemoNo" + i); 
					double dClosingRate = data.getParameters().getDouble("ClosingRate" + i); 
					if (iSel > 0 && iSel == i)
					{
						log.debug("Selected Memo " + i + " " + sMemoNo);						
						insertToSession(sMemoID);
						if (oCurr != null && !oCurr.getIsDefault())
						{
							log.debug("Closing Rate " + i + " " + dClosingRate);
							//update closing rate first
							DebitMemoTool.updateClosingRate(sMemoID, dClosingRate);
						}
					}
				}
    		}
    		else
    		{
    			String sVendorID = data.getParameters().getString("VendorId");
        		
        		//get DP in session object
    			vAPDP = (List) oSes.getAttribute (s_APDP);
    			
        		//get unused debit memo by vendor, status, currency
        		List vDebitMemoTemp = DebitMemoTool.getDMByVendorAndStatus(sVendorID, 
                                        i_MEMO_OPEN,sCurrencyID);
                                
        		List vDebitMemo = new ArrayList();
                
                //filter unused memo, make sure memo used by invoice not displayed
        		for(int i = 0; i < vDebitMemoTemp.size(); i++)
        		{
        			DebitMemo oDebitMemo = (DebitMemo)vDebitMemoTemp.get(i);
        			if(!isMemoExist(oDebitMemo.getDebitMemoId()))
        			{
        				vDebitMemo.add(oDebitMemo);
        			}
        		}
        		context.put ("vMemo", vDebitMemo);
    		}
    	}
    	catch (Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		data.setMessage("DM Lookup Failed : " + _oEx.getMessage());
    	}
    }
    
    protected void insertToSession(String _sMemoID)
    	throws Exception
    {
		vAPDPTemp.add (0, _sMemoID);
		oSes.setAttribute (s_APDPTMP, vAPDPTemp);    
	}
    
    protected boolean isMemoExist (String _sMemoID) 
    	throws Exception
    {
    	for (int i = 0; i < vAPDP.size(); i++) 
    	{
    		List vExist = (List) vAPDP.get(i);
    		for (int j = 0; j < vExist.size(); j++) 
    		{
    			String sExistID = (String) vExist.get(j);
    			if ( sExistID.equals(_sMemoID)) 
    			{
					return true;    			
    			}
    		}
    	}
    	return false;
    } 
}