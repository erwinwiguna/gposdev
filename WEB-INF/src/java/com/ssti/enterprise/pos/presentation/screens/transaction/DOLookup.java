package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;

public class DOLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		
    		String sCustomerID = data.getParameters().getString ("CustomerId");
    		String sCurrencyID = data.getParameters().getString ("CurrencyId");
    		String sDisplaySOID = data.getParameters().getString ("DisplaySOId", "");
    		String sDisplayDOID = data.getParameters().getString ("DisplayDOId", "");
   			context.put ("OpenDO", DeliveryOrderTool.getDOByStatusAndCustomerID(sCustomerID, AppAttributes.i_DO_DELIVERED, false,sCurrencyID));
    		
    		if (!sDisplaySOID.equals(""))
    		{
    			context.put ("SODetails", SalesOrderTool.getDetailsByID (sDisplaySOID));
    		}
    		
    		if (!sDisplayDOID.equals(""))
    		{
    			context.put ("DODetails", DeliveryOrderTool.getDetailsByID (sDisplayDOID));
    		}
            context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_SALES_MULTI_CURRENCY));

    			
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("PR Lookup Failed : " + _oEx.getMessage());
    	}
    }
}