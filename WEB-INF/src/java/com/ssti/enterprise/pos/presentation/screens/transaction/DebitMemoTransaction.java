package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomFormatter;

public class DebitMemoTransaction extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Debit Memo"};
	
    protected static final int i_OP_SET  = 1;
	protected static final int i_OP_VIEW  = 4; 
	protected static final int i_OP_NEW_FROM_PMT = 5; 
	protected static final int i_OP_NEW_FROM_ORD = 6;	
	
	protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		//return isAuthorized (data, a_PERM);
		return true;
	}
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString ("id");
				context.put("DebitMemo", DebitMemoTool.getDebitMemoByID(sID));
    		}
			else if (iOp == i_OP_SET)
			{
				DebitMemo oDM = new DebitMemo();
				data.getParameters().setProperties(oDM);
				context.put("DebitMemo", oDM);
			}
			else if (iOp == i_OP_NEW_FROM_PMT)
			{
				String sPmtID = data.getParameters().getString("paymentid");
				ApPayment oPMT = PayablePaymentTool.getHeaderByID(sPmtID);
				if (oPMT != null)
				{
					double dAmt = oPMT.getTotalAmount().doubleValue();
					double dRate = oPMT.getCurrencyRate().doubleValue();
					if (dAmt < 0) dAmt = dAmt * -1;
					
					List vMemo = DebitMemoTool.getByPaymentTransID(oPMT.getTransactionId());
					String sMemoNo = "";
					StringBuilder oRemark = new StringBuilder();
					oRemark.append("Automatic Memo from Payment ").append(oPMT.getTransactionNo());
				    if (vMemo.size() > 0) oRemark.append(" Used Memo: \n");
					for (int i = 0; i < vMemo.size(); i++)
				    {
				    	DebitMemo oMemo = (DebitMemo)vMemo.get(i);
				    	oRemark.append(oMemo.getMemoNo());
				    	oRemark.append("(").append(CustomFormatter.formatNumber(oMemo.getAmount())).append(") ");
				    	if (i != (vMemo.size() - 1)) oRemark.append("\n");
				    }
					
					DebitMemo oMemo = new DebitMemo();
					BeanUtil.setBean2BeanProperties(oPMT,oMemo);
					oMemo.setDebitMemoId("");
					oMemo.setNew(true);
					oMemo.setModified(true);
					oMemo.setPaymentTransId("");
					oMemo.setPaymentTransNo("");
					oMemo.setTransactionId("");
					oMemo.setTransactionNo("");					
					oMemo.setTransactionDate(new Date());					
					oMemo.setAmount(new BigDecimal(dAmt));
					oMemo.setAmountBase(new BigDecimal(dAmt * dRate));
					oMemo.setRemark(oRemark.toString());
					oMemo.setFromPaymentId(oPMT.getTransactionId());
					context.put("DebitMemo", oMemo);	
				}
			}
			else if (iOp == i_OP_NEW_FROM_ORD)
			{
				String sOrderID = data.getParameters().getString("orderid");
				PurchaseOrder oPO = PurchaseOrderTool.getHeaderByID(sOrderID);
				if (oPO != null)
				{
					DebitMemo oMemo = DebitMemoTool.getBySourceTransID(oPO.getPurchaseOrderId());					
					if( oMemo == null)
					{
						double dAmountBase = oPO.getCurrencyRate().doubleValue() * oPO.getTotalAmount().doubleValue();
						Date dToday = new Date();
						Date dDueDate = new Date();
	
						oMemo = new DebitMemo();
						oMemo.setTransactionDate  (oPO.getTransactionDate());
						oMemo.setTransactionId    (oPO.getPurchaseOrderId());
						oMemo.setTransactionNo    (oPO.getPurchaseOrderNo());
						oMemo.setTransactionType  (i_AR_TRANS_SO_DOWNPAYMENT);			
						oMemo.setVendorId         (oPO.getVendorId());
						oMemo.setVendorName       (oPO.getVendorName());
						oMemo.setUserName         (oPO.getCreateBy());
						oMemo.setCurrencyId       (oPO.getCurrencyId());
						oMemo.setCurrencyRate     (oPO.getCurrencyRate());
						oMemo.setAmount           (oPO.getDownPayment());
						oMemo.setAmountBase       (new BigDecimal(dAmountBase));						
						oMemo.setBankId           (oPO.getBankId());
						oMemo.setBankIssuer       (oPO.getBankIssuer());
						oMemo.setReferenceNo      (oPO.getReferenceNo());
						oMemo.setDueDate          (oPO.getDpDueDate());
						oMemo.setAccountId        (oPO.getDpAccountId());
						oMemo.setCashFlowTypeId	  (oPO.getCashFlowTypeId());
						oMemo.setLocationId		  (oPO.getLocationId());
						
						StringBuilder oRemark = new StringBuilder();
						oRemark.append(oPO.getRemark());
						oRemark.append("\n DP ").append(LocaleTool.getString(" for ")).append(" ");
						oRemark.append(oPO.getPurchaseOrderNo());
						oRemark.append("\n").append(oPO.getRemark());
						oMemo.setRemark (oRemark.toString());
						oMemo.setUpdateOrder(true);
					}
					context.put("DebitMemo", oMemo);						
				}
			}
    	}
    	catch(Exception _oEx)
    	{
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    	context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_PURCHASE_MULTI_CURRENCY));
    }
}
