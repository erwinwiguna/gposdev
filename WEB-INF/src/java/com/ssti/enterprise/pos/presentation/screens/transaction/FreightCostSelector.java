package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;

import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;

public class FreightCostSelector extends TransactionSecureScreen
{
	private SalesOrder oSO = null;
	private SalesTransaction oSI = null;
	private PurchaseInvoice oPI = null;

	boolean bSO = false;
	boolean bSI = false;
	boolean bPI = false;
	
	public void doBuildTemplate(RunData data, Context context)
    {
    	ParameterParser formData = data.getParameters();

    	int iOp = formData.getInt("op");
    	String sFrom = formData.getString("from", "");
    	
    	try
    	{
    		if (sFrom.equals("PI"))
    		{
    			oPI = (PurchaseInvoice) data.getSession().getAttribute(s_PI);
    			bPI = true;
    		}
    		if (sFrom.equals("SI"))
    		{
    			oSI = (SalesTransaction) data.getSession().getAttribute(s_TR);
    			bSI = true;
    		}
    		if (sFrom.equals("SO"))
    		{
    			oSO = (SalesOrder) data.getSession().getAttribute(s_SO);
    			bSO = true;
    		}
    		
    		
    		BigDecimal dFreight = formData.getBigDecimal("TotalExpense");
    		
    		if (bPI && oPI != null)
    		{
    			if (iOp == 1) //set vendor
    			{
    				setVendor(formData);
    			}
    			if (iOp == 2) //set freight
    			{
    				oPI.setFreightVendorId(formData.getString("FreightVendorId"));
    				oPI.setFreightCurrencyId(formData.getString("FreightCurrencyId"));
    				oPI.setFreightAccountId(formData.getString("FreightAccountId"));
    				oPI.setFreightAsCost(formData.getBoolean("FreightAsCost"));
    			}
				oPI.setTotalExpense(dFreight);
				
    			context.put("oTrans", oPI);
    			context.put("bPI", Boolean.valueOf(bPI));
    		}
    		else if (bSI && oSI != null)
    		{
    			if (iOp == 2) //set freight
    			{
        			oSI.setFreightAccountId(formData.getString("FreightAccountId"));
        			oSI.setIsInclusiveFreight(formData.getBoolean("IsInclusiveFreight"));
    			}
    			context.put("oTrans", oSI);
    			context.put("bSI", Boolean.valueOf(bSI));
    		}
    		else if (bSO && oSO != null)
    		{
    			if (iOp == 2) //set freight
    			{
    				oSO.setFreightAccountId(formData.getString("FreightAccountId"));
    				oSO.setIsInclusiveFreight(formData.getBoolean("IsInclusiveFreight"));
    			}
    			context.put("oTrans", oSO);
    			context.put("bSO", Boolean.valueOf(bSO));
    		}
    		
    		context.put("sFrom", sFrom);
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("ERROR: " + _oEx.getMessage());
    		log.error(_oEx);
    	}  	
    }
	
	private void setVendor(ParameterParser formData)
		throws Exception
	{
		if (oPI != null)
		{
			oPI.setFreightVendorId(formData.getString("FreightVendorId"));
			Vendor oVendor = VendorTool.getVendorByID(oPI.getFreightVendorId());
			if (oVendor != null)
			{
				if (!oVendor.getVendorId().equals(oPI.getVendorId())) 
				{
					Currency oVendorCurr = CurrencyTool.getCurrencyByID(oVendor.getDefaultCurrencyId());
					if (!oVendorCurr.getIsDefault()) //if vendor is not PI vendor then only use base currency
					{
						throw new Exception ("Only Vendor with base currency allowed ");
					}
				}
				
				oPI.setFreightCurrencyId(oVendor.getDefaultCurrencyId());
				Account oAP = AccountTool.getAccountPayable(
					oVendor.getDefaultCurrencyId(), oVendor.getVendorId(), null);
				
				oPI.setFreightAccountId(oAP.getAccountId());
				oPI.setFreightAsCost(true); //when vendor is not the same with PI vendor then set default 
			}
		}
	}
}