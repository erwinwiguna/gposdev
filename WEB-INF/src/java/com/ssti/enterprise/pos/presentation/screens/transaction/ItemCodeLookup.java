package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.framework.parser.BarcodeData;
import com.ssti.framework.parser.BarcodeParser;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Item Code Lookup Screen Handler
 * <br>
 *
 * $@author  Author: Albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: ItemCodeLookup.java,v $
 * 
 * 2017-08-25
 * - changes in @see ItemTool will set discount, and item type checking in findPurchaseItemByCode 
 * 
 * </pre><br>
 */
public class ItemCodeLookup extends TransactionSecureScreen
{   
	private static final Log log = LogFactory.getLog(ItemCodeLookup.class);
	protected Item m_oItem = null;
	protected boolean b_SESSION_VALID = true;
	
    protected boolean isAuthorized (RunData data)  
		throws Exception 
	{
	    AccessControlList acl = data.getACL();
	    if (acl == null) b_SESSION_VALID = false;
	    return true;
	}
	
    public void doBuildTemplate(RunData data, Context context)
    {
		context.put("bSessionValid", Boolean.valueOf(b_SESSION_VALID));
    	if(!b_SESSION_VALID) return; //return if session is invalid 

    	ValueParser formData = data.getParameters();
    	
		String sCustomerID = formData.getString("CustomerId", "");
		String sVendorID = formData.getString("VendorId", "");
		String sLocationID = formData.getString("LocationId", "");
		String sCode = formData.getString ("ItemCode");	
		String sFrom = formData.getString("FromTransaction", "");
		
		String sDate = formData.getString("TransDate");
		Date dTransDate = CustomParser.parseDate(sDate);
		if (StringUtil.isNotEmpty(sDate) && sDate.contains(":")) //contains hour
		{
			dTransDate = CustomParser.parseDateTime(sDate);
		}
		if (dTransDate == null) dTransDate = new Date();
		
		//TransGroup Value: @see AppAttributes
		//Purchase = 1, Sales = 2, Inventory = 3
		int iTransGroup = formData.getInt("TransactionGroup",-1);
		
		if (iTransGroup < 1) log.warn ("TRANSACTION GROUP is not set properly");
		
		try 
    	{
			if (StringUtil.isEmpty(sLocationID)) sLocationID = PreferenceTool.getLocationID();
    		
    		BarcodeData oBarcode = BarcodeParser.parseBarcode(sCode);
    		sCode = oBarcode.getItemCode();
    		    		
    		if(iTransGroup != i_PURCHASE_TRANSACTION)
    		{
    		    m_oItem = ItemTool.findItemByCode(sCode, sCustomerID, sLocationID, iTransGroup, dTransDate);	
    			if (iTransGroup == i_SALES_TRANSACTION)
    			{   //check back date to correct inventory qty validation
    				dTransDate = BaseTool.checkBackDate(dTransDate,i_INV_OUT);
    			}
    		}
    		
    		if(iTransGroup == i_PURCHASE_TRANSACTION)
    		{
    			m_oItem = ItemTool.findPurchaseItemByCode(sCode, sVendorID, dTransDate);     			
    			if(m_oItem != null) 
    			{
	    			//check price & discount in vendor price list
	    		    VendorPriceListDetail oVPLD = 
	    		    	VendorPriceListTool.getPriceListDetailByVendorID(sVendorID, m_oItem.getItemId(), dTransDate);
	    		    if(oVPLD != null)
	    		    {    		    
	    		    	if(oVPLD.getPurchasePrice() != null && oVPLD.getPurchasePrice().doubleValue() >= 0)
	    		    	{
	    		    		m_oItem.setItemPrice(oVPLD.getPurchasePrice());    		    		    		    		
	    		    	}
	    		    	if(StringUtil.isNotEmpty(oVPLD.getDiscountAmount()))
	    		    	{
	    		    		m_oItem.setDiscountAmount(oVPLD.getDiscountAmount());
	    		    	}
	    		    	m_oItem.setFromPriceList(true);
	    		    	context.put("oVendorPL", oVPLD);
	    		    }
    			}
    			else
    			{
    				log.debug("findPurchaseItemByCode: Item " + sCode + " Not Found");
    			}
    		}
    		context.put("oItem", m_oItem);
    		context.put("dQty", oBarcode.getQty());
    		
    		if (m_oItem != null) 
    		{	
    			if (oBarcode.getPrice() != null && oBarcode.getPrice().intValue() > 0)
    			{
    				m_oItem.setItemPrice(oBarcode.getPrice());
    				m_oItem.setFromPriceList(true); //for now identified this is from external price not from master
    			}
        		Location oLoc = LocationTool.getLocationByID(sLocationID);
                context.put("oLoc", oLoc);				    

    			String sItemID = m_oItem.getItemId();
    			
    			double dDetQty = 0;
    			if (iTransGroup != i_PURCHASE_TRANSACTION && oLoc != null)
    			{		
    				dDetQty = getExistingQty(sFrom, data);
    			}
    			context.put("dDetQty", new Double(dDetQty));

    			setUnitAndTax(iTransGroup, sCustomerID, sVendorID, context);
    			setDirectAdd (data, sFrom, context);
    			
    			if (m_oItem.getItemType() == i_INVENTORY_PART)
    			{
    	    		setInvTrans(sItemID, sLocationID, dTransDate, context);	
    			}
    		}
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();
    		log.error(_oEx);
    	}
    }
    
    /**
     * 
     * @param sFrom
     * @param data
     * @return
     * @throws Exception
     */
    private double getExistingQty (String _sFrom, RunData data)
    	throws Exception
    {
	    double dDetQty = 0;
        String sDet = "";
        String sFrom = _sFrom;
        if (_sFrom.equals("pos")) sFrom = s_TR;
                
        if (sFrom.equals(s_SO))      {sDet = s_SOD;}
        else if (sFrom.equals(s_DO)) {sDet = s_DOD;}
        else if (sFrom.equals(s_TR)) {sDet = s_TD ;}
        else if (sFrom.equals(s_JC)) {sDet = s_JCD;}
        else if (sFrom.equals(s_IR)) {sDet = s_IRD;}
        else if (sFrom.equals(s_IT)) {sDet = s_ITD;}
            
	    List vDetail = (List) data.getSession().getAttribute(sDet); 
	    
	    if (sFrom.equals(s_SO))      {dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId()); }
	    else if (sFrom.equals(s_DO)) {dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId()); }
	    else if (sFrom.equals(s_TR)) {dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId()); }
	    else if (sFrom.equals(s_JC)) {dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId()); }
	    
	    else if (sFrom.equals(s_IR)) {dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId(), "qtyChanges"); }
	    else if (sFrom.equals(s_IT)) {dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId(), "qtyChanges"); }
		
		return dDetQty;
    }

    /**
     * 
     * @param iTransGroup
     * @param oItem
     * @param oUnit
     * @param oTax
     * @throws Exception
     */
    private void setUnitAndTax (int iTransGroup, String _sCustomerID, String _sVendorID, Context context)
    	throws Exception
    {
		Unit oUnit = null;
		Tax oTax = null;
				
		if (iTransGroup == i_PURCHASE_TRANSACTION)
		{
			if (PreferenceTool.useVendorTax()) //use tax from vendor default tax
			{
				Vendor oVendor = VendorTool.getVendorByID(_sVendorID);
				if (oVendor != null && StringUtil.isNotEmpty(oVendor.getDefaultTaxId()))
				{
					oTax = TaxTool.getTaxByID(oVendor.getDefaultTaxId());    				
				}
			}
			if (oTax == null) oTax = TaxTool.getTaxByID(m_oItem.getPurchaseTaxId());
			if (oTax == null) oTax  = TaxTool.getDefaultPurchaseTax();
			oUnit = UnitTool.getUnitByID(m_oItem.getPurchaseUnitId());
		}
		else if (iTransGroup == i_SALES_TRANSACTION)
		{
			if (PreferenceTool.useCustomerTax()) //use tax from customer default tax
			{
				Customer oCustomer = CustomerTool.getCustomerByID(_sCustomerID);
				if (oCustomer != null && StringUtil.isNotEmpty(oCustomer.getDefaultTaxId()))
				{
					oTax = TaxTool.getTaxByID(oCustomer.getDefaultTaxId());    				
				}
			}
			if (oTax == null) oTax = TaxTool.getTaxByID(m_oItem.getTaxId()); //by item			
			if (oTax == null) oTax  = TaxTool.getDefaultSalesTax(); //default 
			oUnit = UnitTool.getUnitByID(m_oItem.getUnitId());
		}
		else if (iTransGroup == i_INVENTORY_TRANSACTION)
		{
			oUnit = UnitTool.getAlternateUnitByID(m_oItem.getUnitId());
		}    
		
		context.put("oUnit", oUnit);
		context.put("oTax", oTax);
    }
    
    /**
     * 
     * @param iTransGroup
     * @param oItem
     * @param oUnit
     * @param oTax
     * @throws Exception
     */
    private void setInvTrans (String _sItemID, String _sLocationID, Date _dTransDate, Context context)
    	throws Exception
    {
		InventoryTransaction oInv = 
			InventoryTransactionTool.getLastTrans (_sItemID, _sLocationID, _dTransDate, null);

        if (oInv != null)
        {
        	double dQtyBalance = oInv.getQtyBalance().doubleValue();
        	double dItemCost = oInv.getCost().doubleValue();
        	
        	//query again QtyBalance because qty balance in global costing contains qty balance for all location
        	//while oInv.QtyBalance is needed to validate whether Qty is enough
        	if (b_GLOBAL_COSTING)
			{
        		oInv.setQtyBalance(InventoryTransactionTool.getQtyAsOf(_sItemID, _sLocationID, _dTransDate));			
			}			
        	else //only re-count item cost from value balance /qty balance if costing is not GLOBAL
        	{
        		if (dQtyBalance > 0)
        		{
        			dItemCost = oInv.getValueBalance().doubleValue() / dQtyBalance;
        		}
        	}
        	BigDecimal bdItemCost = new BigDecimal(dItemCost);
        	bdItemCost = bdItemCost.setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_EVEN);
        	context.put("oInv", oInv);
        	context.put("dItemCost", bdItemCost);
	    }
        else
        {
			//double dBaseUnit = UnitTool.getBaseValue(m_oItem.getPurchaseUnitId());
			//double dLastPurchase = m_oItem.getLastPurchasePrice().doubleValue() /  dBaseUnit;
			context.put("dItemCost", bd_ZERO);
        }
    }    
    
    /**
     * 
     * @param sFrom
     * @param context
     * @throws Exception
     */
    private void setDirectAdd (RunData data, String sFrom, Context context)
	{	    
        context.put(s_DA, Boolean.valueOf(BaseTool.getDirectAdd(sFrom)));
	}    
}
