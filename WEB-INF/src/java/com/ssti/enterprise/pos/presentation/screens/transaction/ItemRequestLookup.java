package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;

public class ItemRequestLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		String sLocID = data.getParameters().getString("ToLocationId");
    		String sDisplayID = data.getParameters().getString("DisplayId", "");
    		
   			context.put ("ItemRequests", 
   				PurchaseRequestTool.findForLookup(sLocID, "", "", -1, false));	
    			
    		if (!sDisplayID.equals(""))
    		{
    			context.put ("Details", PurchaseRequestTool.getDetailsByID(sDisplayID));
    		}
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("Item Request Lookup Failed : " + _oEx.getMessage());
    	}
    }
}