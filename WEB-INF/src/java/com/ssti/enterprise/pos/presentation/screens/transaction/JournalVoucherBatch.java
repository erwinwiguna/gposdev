package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.StringUtil;

public class JournalVoucherBatch extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Journal Voucher"};
	private static final int i_BATCH_PROC = 2;
	
	@Override
	protected boolean isAuthorized(RunData data)
		throws Exception 
	{
		return super.isAuthorized(data, a_PERM);
	}
	
	int op = 0;
    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
        	op = data.getParameters().getInt("op");
        	int iTotalNo = data.getParameters().getInt("totalNo");
        	if(op == i_BATCH_PROC)
        	{
        		StringBuilder sbMsg = new StringBuilder();
        		for (int i = 1; i <= iTotalNo; i++)
        		{
        			boolean bSel = data.getParameters().getBoolean("cb" + i);
        			if(bSel)
        			{
        				String sCFID = data.getParameters().getString("id" + i);	
        				if(StringUtil.isNotEmpty(sCFID))
        				{
        					JournalVoucher oJV = JournalVoucherTool.getHeaderByID(sCFID);
        					if(oJV != null && oJV.getStatus() == i_PENDING)
        					{
        						try 
        						{
            						List vCFD = JournalVoucherTool.getDetailsByID(sCFID);
            						oJV.setStatus(i_PROCESSED);
            						JournalVoucherTool.saveData(oJV, vCFD); 
            						sbMsg.append(LocaleTool.getString("journal_voucher")).append(": ")
            							 .append(oJV.getTransactionNo()).append(" ")
            							 .append(LocaleTool.getString(s_SAVE_SUCCESS))
            							 .append(s_LINE_SEPARATOR);
								} 
        						catch (Exception e) 
        						{
            						sbMsg.append(LocaleTool.getString("journal_voucher")).append(": ")
		       							 .append(oJV.getDescription()).append(" ")
		       							 .append(LocaleTool.getString(s_SAVE_FAILED))
		       							 .append(e.getMessage())
		       							 .append(s_LINE_SEPARATOR);
        							
        							e.printStackTrace();
        							log.error(e);
        						}
        					}
        				}
        			}
        		}
        		context.put("processMessage", sbMsg.toString());
        	}
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}
