package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.enterprise.pos.tools.gl.SubLedgerTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: JournalVoucherTransaction.java,v 1.13 2008/06/29 07:10:31 albert Exp $ <br>
 *
 * <pre>
 * $Log: JournalVoucherTransaction.java,v $
 * Revision 1.13  2008/06/29 07:10:31  albert
 * *** empty log message ***
 *
 * Revision 1.12  2007/11/09 01:13:28  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/04/17 13:04:24  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class JournalVoucherTransaction extends TransactionSecureScreen
{
    protected static final String[] a_PERM = {"View Journal Voucher"};

    protected static final int i_OP_ADD_DETAIL  = 1;
    protected static final int i_OP_DEL_DETAIL  = 2;    
    protected static final int i_OP_NEW  		= 3;
    protected static final int i_OP_VIEW        = 4;
	protected static final int i_OP_COPY_TRANS  = 6; 
  	
    protected JournalVoucher oJV = null;    
    protected List vJVD = null;
    protected HttpSession oSes;                         
                         
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try
		{
			if ( iOp == i_OP_NEW )
			{
				newTransaction (data);
			}
			else if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}
			else if (iOp == i_OP_VIEW )
			{
				getData (data, context);
			}
			else if (iOp == i_OP_COPY_TRANS && !b_REFRESH) 
			{
				copyTrans (data);
			}			
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_JV, oSes.getAttribute (s_JV));
    	context.put(s_JVD, oSes.getAttribute (s_JVD));
    }
    
    protected void initSession ( RunData data )
	{	
    	synchronized(this)
    	{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_JV) == null ) 
			{
				oSes.setAttribute (s_JV, new JournalVoucher());
				oSes.setAttribute (s_JVD, new ArrayList());
			}
			oJV = (JournalVoucher) oSes.getAttribute (s_JV);
			vJVD = (List) oSes.getAttribute (s_JVD);
    	}
	}

    protected void newTransaction ( RunData data ) 
		throws Exception
	{
		synchronized(this)
		{
			oJV = new JournalVoucher();
			vJVD = new ArrayList();
			oSes.setAttribute (s_JV, oJV);
			oSes.setAttribute (s_JVD, vJVD);
		}
	}
    
    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
		JournalVoucherDetail oJVD = new JournalVoucherDetail();
		data.getParameters().setProperties (oJVD);
				
		boolean bSetMemo = data.getParameters().getBoolean("SetMemo");
	    if (bSetMemo)
	    {
	    	oJVD.setMemo(data.getParameters().getString("TxtAccountName"));
	    }
		
		if (oJVD.getSubLedgerType() > 0 && 
			StringUtil.isNotEmpty(oJVD.getSubLedgerId()) && 
			StringUtil.isEmpty(oJVD.getSubLedgerDesc()))
		{
			oJVD.setSubLedgerDesc(SubLedgerTool.getSLCode(oJVD.getSubLedgerType(), oJVD.getSubLedgerId()));
		}
	    
		vJVD.add (oJVD);		
		JournalVoucherTool.setHeaderProperties (oJV, vJVD,data, false);			
		
		oSes.setAttribute (s_JV, oJV); 
		oSes.setAttribute (s_JVD, vJVD);
    }
    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
        int iNo = data.getParameters().getInt("No");
        int iIdx = iNo - 1;
		if (vJVD.size() > iIdx) 
		{
			vJVD.set (iIdx, new JournalVoucherDetail());	
			JournalVoucherTool.setHeaderProperties (oJV, vJVD, data, true);
			vJVD.remove (iIdx);	

			oSes.setAttribute (s_JV, oJV); 
			oSes.setAttribute (s_JVD, vJVD);
		}    	
    }

    protected void getData ( RunData data, Context context  )
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try
    		{
    			synchronized (this)
    			{
	    			oJV = JournalVoucherTool.getHeaderByID (sID);
	    			vJVD = JournalVoucherTool.getDetailsByID (sID);
	    			oSes.setAttribute (s_JV, oJV);
					oSes.setAttribute (s_JVD, vJVD);
    			}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
    
    protected boolean isExist (String _sAccountID) 
    	throws Exception
    {
    	for (int i = 0; i < vJVD.size(); i++) 
    	{
    		JournalVoucherDetail oExistJVD = (JournalVoucherDetail) vJVD.get (i);
    		if ( oExistJVD.getAccountId().equals(_sAccountID) ) 
    		{
				return true;    			
    		}		
    	}
    	return false;
    } 

	/**
	 * @param data
	 */
	protected void copyTrans(RunData data) 
		throws Exception
	{
		try
		{
			String sTransID = data.getParameters().getString("TransId");
			
			oJV = new JournalVoucher();
			data.getParameters().setProperties (oJV);		

			if (StringUtil.isNotEmpty (sTransID))
			{			
                JournalVoucher oOldJV = JournalVoucherTool.getHeaderByID(sTransID);
                if(oOldJV != null)
                {
                    oJV.setDescription(oOldJV.getDescription());
                }
                
				List vTrans = JournalVoucherTool.getDetailsByID(sTransID);
				
				double dTotalDebit  = 0;
				double dTotalCredit = 0;
				
				vJVD = new ArrayList(vTrans.size());
				for (int i = 0; i < vTrans.size(); i++)
				{
					JournalVoucherDetail oDet = (JournalVoucherDetail) vTrans.get(i);
					JournalVoucherDetail oNewDet = oDet.copy();
					vJVD.add(oNewDet);
				    
					double dAmountBase = oDet.getAmountBase().doubleValue();
					
					if (oDet.getDebitCredit() == GlAttributes.i_DEBIT) dTotalDebit  += dAmountBase;
				    if (oDet.getDebitCredit() == GlAttributes.i_CREDIT) dTotalCredit += dAmountBase;
		            		
		         }
				oJV.setTotalDebit  (new BigDecimal (dTotalDebit));
		        oJV.setTotalCredit (new BigDecimal (dTotalCredit));
				
				oSes.setAttribute (s_JV, oJV);
				oSes.setAttribute (s_JVD, vJVD);
			}
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
		}
	}    
}
