package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.om.PriceListDetailPeer;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - changes in updateDetailByID, add username parameter to record history
 * 
 * </pre><br>
 */
public class ManagePriceList extends CreatePriceList
{
	private static Log log = LogFactory.getLog(ManagePriceList.class);
	ValueParser formData;
	String sID = "";
	int iOp  = -1;
	int iPriceFrom = 1;
	boolean bRefresh = true;
	
    public void doBuildTemplate(RunData data, Context context)
    {
		formData = data.getParameters();
		sID = formData.getString("id");
		iOp = formData.getInt("op", -1);
		iPriceFrom = formData.getInt("PriceFrom");
		String sDetID = formData.getString("DetId");
		
		try
    	{
			if (StringUtil.isNotEmpty(sID))
			{
				if(iOp == 1)
				{
					updatePriceListDetail(data);
				}
				else if(iOp == 2)
				{
					updateNewPrice(sDetID, data);
				}
				else if(iOp == 3)
				{
					deleteItem(sDetID, data);
				}
				else if(iOp == 4)
				{
					addPriceListDetail(data);
				}
				else if(iOp <= 0)
				{
					bRefresh = false;
				}
				displayPriceList(data, context);
    		}
    	}
    	catch (Exception _oEx) 
    	{
    		log.error ( _oEx );
    		data.setMessage (_oEx.getMessage());
    	}
    }

	private void displayPriceList(RunData data, Context context)
		throws Exception
	{		
		context.put ("PriceList", PriceListTool.getPriceListByID (sID));
		
		int iCond = formData.getInt("Condition");
		int iSortBy = formData.getInt("SortBy",2);
		int iLimit = formData.getInt("ViewLimit", 50);
		String sKey = formData.getString("Keywords","");	
		String sKatID = formData.getString("KategoriID","");	
		int iPage = formData.getInt("page",1);				
		
		LargeSelect oDetails = null;
		
		Object oTemp = data.getUser().getTemp ("priceListResult") ;
		
		if (bRefresh) //refresh data filter
		{
			oDetails = PriceListTool.findData (sID, iCond, iSortBy, iLimit, sKatID, sKey);
			data.getUser().setTemp ("priceListResult",oDetails);
		}
		else 
		{
			if (oTemp != null)
			{
				oDetails = (LargeSelect) oTemp;
			}
			else 
			{
				oDetails = PriceListTool.findData (sID, iCond, iSortBy, iLimit, sKatID, sKey);
				data.getUser().setTemp ("priceListResult",oDetails);		
			}
		}
		context.put ("PriceListDetail", oDetails.getPage(iPage));
		context.put ("LargeData", oDetails);
	}

    private void addPriceListDetail(RunData data)
    	throws Exception
    {
		String sItemID = formData.getString ("itemid");		
		if (StringUtil.isNotEmpty(sItemID))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddDetail(sItemID, data);
				}
			}
			else 
			{
				processAddDetail(sItemID, data);
			}
		}
	}

    private void processAddDetail(String _sItemID, RunData data)
    	throws Exception
    {
		if (!PriceListTool.isItemExist(_sItemID, sID))
		{
			if (!PriceListTool.isItemExistInActivePL(_sItemID, sID))
			{
				PriceListTool.addItemToPriceList (_sItemID, sID, iPriceFrom);
			}
			else
			{
				data.setMessage("Item Exists in Active Price Lists");
			}
		}
		else
		{
			data.setMessage("Item Already Exists in Current Price List");
		}    
    }	

    private void updatePriceListDetail(RunData data)
    	throws Exception
    {
    	try
    	{
    		int iSize = formData.getInt("DataSize");
    		for ( int i = 1; i <= iSize; i++ )
    		{
				String sDiscAmt  = formData.getString("DiscountAmount" + i);
				double dNewPrice = formData.getDouble("NewPrice" + i);
				String sDetailID = formData.getString("PriceListDetailId" + i);
				PriceListTool.updateDetailByID(sID, sDetailID, dNewPrice, sDiscAmt, data.getUser().getName());
			}
    		data.setMessage("Price List Detail Updated Successfully");
    	}
    	catch(Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		throw new NestableException("Price List Detail Update Failed : " + _oEx.getMessage(), _oEx);
    	}
	}

	private void updateNewPrice(String _sDetID, RunData data)
		throws Exception
	{
		try
    	{
			PriceListTool.updateDetailByID(sID, _sDetID, 
				formData.getDouble("NewPrice"), formData.getString("DiscountAmount"), data.getUser().getName());
    		
			data.setMessage("New Price Updated Successfully");
    	}
    	catch(Exception _oEx)
    	{
    		log.error (_oEx);
    		throw new NestableException("New Price Update Failed : " + _oEx.getMessage(), _oEx);
    	}
	}

	private void deleteItem(String _sDetailID, RunData data)
		throws Exception
	{
		try
    	{
			PriceListDetail oPLD = PriceListTool.getDetailByDetID(_sDetailID);			
			Criteria oCrit = new Criteria();
	        oCrit.add(PriceListDetailPeer.PRICE_LIST_DETAIL_ID, _sDetailID);
	        PriceListDetailPeer.doDelete(oCrit);
    		UpdateHistoryTool.createDelHistory(oPLD, data.getUser().getName(), null);
	        data.setMessage("Item Deleted Successfully");
    	}
    	catch(Exception _oEx)
    	{
    		log.error (_oEx);
    		throw new NestableException("Item Deletion Failed : " + _oEx.getMessage(), _oEx);
    	}
	}
}
