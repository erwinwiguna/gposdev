package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.DrawerLog;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.pos.drawer.DrawerTool;
import com.ssti.framework.tools.DeviceConfigTool;


public class OpenDrawer extends TransactionSecureScreen
{
    protected static boolean m_bUseDrawer = DeviceConfigTool.drawerUse();
    
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	boolean bReason = data.getParameters().getBoolean("setreason");
    	int iOp = data.getParameters().getInt("op", -1);
    	
    	if (!bReason)
    	{
	        if (iOp == 1) //log
	        {
	        	DrawerLog oLog = new DrawerLog();
	        	try
	        	{
	        		data.getParameters().setProperties(oLog);
	        		oLog.setDrawerLogId(IDGenerator.generateSysID());
	        		oLog.setOpenDate(new Date());
	        		oLog.save();
	        	}
	        	catch (Exception _oEx)
	        	{
	        		log.error (_oEx);
	        	}
	        }
	        	
    		if (m_bUseDrawer)
	        {
	            DrawerTool oTool = new DrawerTool (data.getRemoteAddr());
	            oTool.openDrawer();
	        }
    	}
    }
}
