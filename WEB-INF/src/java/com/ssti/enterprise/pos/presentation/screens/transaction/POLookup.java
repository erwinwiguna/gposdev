package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.StringUtil;

public class POLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		
    		String sMode = data.getParameters().getString("mode","");
    		if (sMode.equals("findByNo"))
    		{
    			String sNo = data.getParameters().getString("PoNo");
    			if (StringUtil.isNotEmpty(sNo))
    			{
    				PurchaseOrder oPO = PurchaseOrderTool.getByNoAndStatus(sNo,i_PO_ORDERED);
    				context.put("po",oPO);
    			}
    		}
    		else
    		{
	    		String sTrans = data.getParameters().getString("Trans");
	    		String sVendorID = data.getParameters().getString ("VendorId");
	    		String sDisplayID = data.getParameters().getString ("DisplayId", "");
	    		String sLocID = data.getParameters().getString("LocationId");
	    		if (data.getACL().getPermissions().containsName("Access All Location"))sLocID = "";
	
	    		if(StringUtil.equals(sTrans, "Receipt") || StringUtil.equals(sTrans, "Invoice"))
	    		{
	    			context.put ("OpenPO", PurchaseOrderTool.getPOByStatusAndVendorID(sVendorID, sLocID, i_PO_ORDERED));	
	    		}	
	    		if (!sDisplayID.equals(""))
	    		{
	    			context.put ("Details", PurchaseOrderTool.getDetailsByID(sDisplayID, i_PURCHASE_SORT_BY, null));
	    		}
    		}
    	}
    	catch (Exception _oEx)
    	{
    		log.error (_oEx);
    		data.setMessage("PO Lookup Failed : " + _oEx.getMessage());
    	}
    }
}