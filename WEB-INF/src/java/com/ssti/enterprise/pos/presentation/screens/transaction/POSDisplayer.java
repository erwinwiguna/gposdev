package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.pos.displayer.Displayer;
import com.ssti.framework.pos.displayer.DisplayerTool;
import com.ssti.framework.tools.DeviceConfigTool;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Screens to handle AJAX request to POS Displayer
 *
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class POSDisplayer extends DefaultSalesTransaction
{
	@Override
	protected boolean isAuthorized(RunData data) throws Exception 
	{
		return true;
	}
	
	public void doBuildTemplate(RunData data, Context context) 
	{
		if (DeviceConfigTool.displayerUse())
		{
			Displayer disp = DisplayerTool.getInstance(data.getRemoteAddr());			
			int iOp = data.getParameters().getInt("op", 1);
			if (iOp == 1)
			{
				context.put("cmd", data.getParameters().getInt("cmd",1));
			}
			else if (iOp == 2)
			{
				context.put(s_TR, data.getSession().getAttribute(s_TR));
				context.put(s_TD, data.getSession().getAttribute(s_TD));
			}			
			context.put("op", iOp);
			context.put("displayer", disp);			
		}		
	}
}
