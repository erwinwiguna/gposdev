package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;

public class PRLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		String sVendorID = data.getParameters().getString ("VendorId");
    		String sCurrencyID = data.getParameters().getString ("CurrencyId");
    		String sDisplayPOID = data.getParameters().getString ("DisplayPOId", "");
    		String sDisplayPRID = data.getParameters().getString ("DisplayPRId", "");
    		if((data.getParameters().getString("Trans")).equals("Invoice"))
    		{
    			context.put ("OpenPR", PurchaseReceiptTool.getPRByStatusAndVendorID(sVendorID, AppAttributes.i_PR_APPROVED, false,sCurrencyID));
    		}
    		
    		if (!sDisplayPOID.equals(""))
    		{
    			context.put ("PODetails", PurchaseOrderTool.getDetailsByID (sDisplayPOID));
    		}
    		
    		if (!sDisplayPRID.equals(""))
    		{
    			context.put ("PRDetails", PurchaseReceiptTool.getDetailsByID (sDisplayPRID));
    		}
            context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_PURCHASE_MULTI_CURRENCY));		
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("PR Lookup Failed : " + _oEx.getMessage());
    	}
    }
}