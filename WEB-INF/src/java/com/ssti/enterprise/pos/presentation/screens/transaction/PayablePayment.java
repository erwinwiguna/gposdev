package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ApPaymentDetail;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-07-31
 * - Allow multiple empty invoice with other amount / memo added
 * 
 * </pre><br>
 */
public class PayablePayment extends TransactionSecureScreen
{
	protected static final Log log = LogFactory.getLog(PayablePayment.class);
	protected static final String[] a_PERM = {"View Payable Payment"};
	
    protected static final int i_OP_ADD_DETAIL			= 1;
	protected static final int i_OP_DEL_DETAIL			= 2;    
    protected static final int i_OP_NEW					= 3;	
	protected static final int i_OP_VIEW_AP_PAYMENT		= 4;
	
	protected static final int i_OP_ADD_DP				= 5;
	protected static final int i_OP_DEL_DP				= 6;

    protected ApPayment oAP = null;    
    protected List vAPD = null;    
    protected List vAPDP = null;		 //List<List<String MemoID>>
    protected List vAPDPTemp = null;     //List<String MemoID>
    protected HttpSession oSes;

    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return isAuthorized (data, a_PERM);
	}
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try
		{
			if ( iOp == i_OP_NEW )
			{
				newTransaction (data);
			}
			else if ( iOp == i_OP_ADD_DETAIL )
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL )
			{
				delDetailData (data);
			}
			else if (iOp == i_OP_VIEW_AP_PAYMENT )
			{
				getData (data, context);
			}
			else if (iOp == i_OP_ADD_DP)
			{
				addNewDP(data);
			}
			else if (iOp == i_OP_DEL_DP)
			{
				delDP(data);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			data.setMessage(_oEx.getMessage());
		}
		context.put(s_AP, oSes.getAttribute (s_AP));
    	context.put(s_APD, oSes.getAttribute (s_APD));
        context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_PURCHASE_MULTI_CURRENCY));
        context.put("appayment", PayablePaymentTool.getInstance());
    }
    
    protected void initSession (RunData data)
	{	
    	synchronized (this) 
    	{
    		oSes = data.getSession();
    		if ( oSes.getAttribute (s_AP) == null ) 
    		{
    			oSes.setAttribute (s_AP,  new ApPayment());
    			oSes.setAttribute (s_APD, new ArrayList());
    			oSes.setAttribute (s_APDP,new ArrayList());
    			oSes.setAttribute (s_APDPTMP,new ArrayList());
    		}
    		oAP = (ApPayment) oSes.getAttribute (s_AP);
    		vAPD = (List) oSes.getAttribute (s_APD);
    		vAPDP = (List) oSes.getAttribute (s_APDP);
    		vAPDPTemp = (List) oSes.getAttribute (s_APDPTMP);
    	}
	}

    protected void newTransaction ( RunData data ) 
		throws Exception
	{
		synchronized(this)
		{
			oAP = new ApPayment();
			vAPD = new ArrayList();
			vAPDP = new ArrayList();
			vAPDPTemp = new ArrayList();
			
			oSes.setAttribute (s_AP, oAP);
			oSes.setAttribute (s_APD, vAPD);
			oSes.setAttribute (s_APDP, vAPDP);
			oSes.setAttribute (s_APDPTMP, vAPDPTemp);
		}
	}
    
     protected void addDetailData ( RunData data ) 
    	throws Exception
    {
		ApPaymentDetail oAPD = new ApPaymentDetail();
		data.getParameters().setProperties (oAPD);
		if(!data.getParameters().getString("DPPaidAmount").equals(""))
		{
			oAPD.setDownPayment(new BigDecimal(data.getParameters().getDouble("DPPaidAmount")));
		}
		else
		{
			oAPD.setDownPayment(bd_ZERO);
		}
		
		if (oAPD.getInvoiceRate().doubleValue() <= 0) 
		{
			oAPD.setInvoiceRate(data.getParameters().getBigDecimal("CurrencyRate", bd_ONE));
		}
		
		if (!isExist (oAPD.getPurchaseInvoiceNo())) 
		{
			vAPD.add (0, oAPD);
			vAPDP.add (0, vAPDPTemp);
			
			PayablePaymentTool.setHeaderProperties (oAP, vAPD,data);
			
			//set TotalDP
			double dTotalDownPayment;
			if(oAP.getTotalDownPayment() != null)
			{
				dTotalDownPayment = oAP.getTotalDownPayment().doubleValue();
			}
			else
			{
				dTotalDownPayment = 0;
			}
			oAP.setTotalDownPayment(new BigDecimal(dTotalDownPayment + oAPD.getDownPayment().doubleValue()));
		}
		oSes.setAttribute (s_AP, oAP); 
		oSes.setAttribute (s_APD, vAPD);
		oSes.setAttribute (s_APDP, vAPDP);
		oSes.setAttribute (s_APDPTMP, new ArrayList());
    }
    
     protected void addNewDP ( RunData data ) 
    	throws Exception
    {
		String sMemoID = data.getParameters().getString("DebitMemoId");
		//double dDebitMemoAmount = data.getParameters().getDouble("DebitMemoAmount");

		if (!isMemoExist (sMemoID)) 
		{
			vAPDPTemp.add (0, sMemoID);
		}
		oSes.setAttribute (s_APDPTMP, vAPDPTemp);    
    }
    
    protected void delDP(RunData data) 
    	throws Exception
    {
    	oSes.setAttribute (s_APDPTMP, new ArrayList());
    }

    protected void delDetailData (RunData data)
    	throws Exception
    {
    	int iDelNo = data.getParameters().getInt("delno") - 1;
		if (vAPD.size() > iDelNo) 
		{
			ApPaymentDetail oAPPD = (ApPaymentDetail)vAPD.get(iDelNo);
			double dDPTemp = oAPPD.getDownPayment().doubleValue();
						
			vAPD.remove  (iDelNo);			
			if (vAPDP.size() > iDelNo)
			{
				removeAPDP(iDelNo);
			}
			oSes.setAttribute (s_APD, vAPD);
			
			PayablePaymentTool.setHeaderProperties (oAP, vAPD, data);
			double dTotalDP = oAP.getTotalDownPayment().doubleValue();
						
			dTotalDP = dTotalDP - dDPTemp;
			oAP.setTotalDownPayment(new BigDecimal(dTotalDP));
			oSes.setAttribute(s_AP, oAP); 
		}    	
    }
    
    /**
     * Remove List of MemoIDs linked with PaymentDetail
     * 
     * @param _iDelNo
     * @throws Exception
     */
    private void removeAPDP(int _iDelNo)
     	throws Exception
	{
    	List vID = (List)vAPDP.get(_iDelNo);
    	//if transaction is saved pending
    	if (vID != null && vID.size() > 0 && StringUtil.isNotEmpty(oAP.getApPaymentId()))
    	{
    		try
			{
    			//clear DM link to PP		
    			for (int i = 0; i < vID.size(); i++)
    	    	{
    	    		String sDMID = (String)vID.get(i);
    	    		DebitMemo oDM = DebitMemoTool.getDebitMemoByID(sDMID);
    	    		if(oDM != null && StringUtil.isNotEmpty(oDM.getPaymentTransId()))
    	    		{
	    	    		oDM.setPaymentTransId("");
	    	    		oDM.setPaymentInvId("");
	    	    		oDM.setPaymentTransNo("");
	    	    		oDM.save();
    	    		}    	    		
    	    	}
			}
			catch (Exception _oEx)
			{
				throw new Exception("Error Clearing Memo Payment Info: " + _oEx.getMessage(), _oEx);
			}			
    	}	
		vAPDP.remove (_iDelNo);	 
	}

	protected void getData (RunData data, Context context)
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try
    		{
    			newTransaction(data);
    			oAP = PayablePaymentTool.getHeaderByID (sID);
    			vAPD = PayablePaymentTool.getDetailsByID (sID);
    			oSes.setAttribute (s_AP, oAP);
				oSes.setAttribute (s_APD, vAPD);
				
				List vDM = new ArrayList();				
				for (int i = 0; i < vAPD.size(); i++)
				{
					ApPaymentDetail oAPD = (ApPaymentDetail)vAPD.get(i);
					if (oAPD.getDownPayment().doubleValue() != 0)
					{
						List vID = DebitMemoTool.getIDByPaymentTransID(sID, oAPD.getApPaymentDetailId());						
						log.debug("APD " + oAPD.getApPaymentDetailId() + " vMemoID " + vID);
						vDM.add(vID);
					}
				}				
				oSes.setAttribute (s_APDP, vDM);
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage (), _oEx);
    		}
    	}
    }
    
    protected boolean isExist (String _sInvoiceNo) 
    	throws Exception
    {
    	for (int i = 0; i < vAPD.size(); i++) 
    	{
    		ApPaymentDetail oExistAPD = (ApPaymentDetail) vAPD.get (i);
    		if ( oExistAPD.getPurchaseInvoiceNo().equals(_sInvoiceNo) && StringUtil.isNotEmpty(_sInvoiceNo) ) 
    		{
				return true;    			
    		}		
    	}
    	return false;
    } 
    
    protected boolean isMemoExist (String _sMemoID) 
    	throws Exception
    {
    	for (int i = 0; i < vAPDP.size(); i++) 
    	{
    		String sExistAPD = (String) vAPDP.get (i);
    		if ( sExistAPD.equals(_sMemoID)) 
    		{
				return true;    			
    		}		
    	}
    	return false;
    } 
}
