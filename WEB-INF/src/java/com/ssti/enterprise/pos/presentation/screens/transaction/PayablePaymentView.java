package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class PayablePaymentView extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Payable Payment"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
        	context.put("appayment", PayablePaymentTool.getInstance());
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findPayablePaymentResult", "ApPayments");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}
