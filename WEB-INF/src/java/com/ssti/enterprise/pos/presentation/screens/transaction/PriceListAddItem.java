package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.Torque;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PriceList;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

public class PriceListAddItem extends SecureScreen
{    
	private static final String[] a_PERM = {"Price List Setup", "View Customer Price List"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }	
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	int iOp = data.getParameters().getInt("op");
    	String sItemID = data.getParameters().getString("ItemId");
    	if (iOp == 1 && StringUtil.isNotEmpty(sItemID))
    	{
    		Connection oConn = null;
    		try 
    		{
    			oConn = Torque.getConnection();
    			Item oItem = ItemTool.getItemByID(sItemID,oConn);
    			if (oItem != null)
    			{
	        		List vPL = PriceListTool.getAllActivePriceList();
	        		for (int i = 0; i < vPL.size(); i++)
	        		{
	        			PriceList oPL = (PriceList)vPL.get(i);
	        			String sPLID = oPL.getPriceListId();
	        			PriceListDetail oPLD = PriceListTool.getDetailByPriceListAndItemID(sPLID,sItemID,oConn);
	        			if (oPLD == null)
	        			{
	        				oPLD = new PriceListDetail();
	        				oPLD.setPriceListId(sPLID);
	        				oPLD.setPriceListDetailId(IDGenerator.generateSysID());
	        				oPLD.setItemId  (oItem.getItemId());	
	        				oPLD.setItemCode(oItem.getItemCode());
	        				oPLD.setItemName(oItem.getItemName());
	        		        oPLD.setDiscountAmount("0");
	        		        if (PreferenceTool.medicalInstalled())
	        		        {
	        		        	oPLD.setOldPrice(oItem.getLastPurchasePrice());        	
	        		        }
	        		        else
	        		        {
	        		        	oPLD.setOldPrice(oItem.getItemPrice());
	        		        }        				
	        			}
	        			String sDisc = data.getParameters().getString("DiscountAmount" + sPLID);
	        			double dPrice = data.getParameters().getDouble("NewPrice" + sPLID);
	        			if (StringUtil.isNotEmpty(sDisc)) oPLD.setDiscountAmount(sDisc);
	        			if (dPrice > 0) oPLD.setNewPrice(new BigDecimal(dPrice));
	        			oPLD.save(oConn);
	        			
	        			oPL.setUpdateDate(new Date());
	        			oPL.save(oConn);
	        		}
	        		data.setMessage("Add Item to Price List Successful");
    			}
			} 
    		catch (Exception e) 
    		{
    			log.error(e);    			
			}
    	}
    }
}
