package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.framework.tools.CustomParser;

public class PurchaseInvoicePaymentLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
			String sInvNo = data.getParameters().getString("PurchaseInvoiceNo","");
    		String sPaymentTypeID = data.getParameters().getString("PaymentTypeId","");
    		String sVendorID = data.getParameters().getString("VendorId","");
    		String sLocID = data.getParameters().getString("LocationId","");
    		boolean bTax = data.getParameters().getBoolean("ViewTax");
    		
    		Vendor oVendor = VendorTool.getVendorByID(sVendorID);    		
   		    String sCurrencyID = oVendor.getDefaultCurrencyId();    		
    		Date dStart = CustomParser.parseDate (data.getParameters().getString("StartDate",""));
    		Date dEnd = CustomParser.parseDate (data.getParameters().getString("EndDate",""));
    		Date dStartDue = CustomParser.parseDate (data.getParameters().getString("StartDue",""));
    		Date dEndDue = CustomParser.parseDate (data.getParameters().getString("EndDue",""));

	    	List vTR = PurchaseInvoiceTool.findCreditTrans (sInvNo, 
	    													dStart, 
	    													dEnd,
															dStartDue,
															dEndDue,
															sVendorID, 
															sLocID,
				                                            sPaymentTypeID, 
															sCurrencyID,
															bTax);
	    	
    		List vAPD = (List) data.getSession().getAttribute(s_APD);
	    	context.put("sTransDetID", PayablePaymentTool.getDetailTransIDString(vAPD));	
	    	context.put("vInvoices", vTR);
	    	context.put("tool", PayablePaymentTool.getInstance());    		
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("Purchase Invoice Lookup Failed : " + _oEx.getMessage());
    	}
    }
}