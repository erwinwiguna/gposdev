package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.io.IOException;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.mail.MailSender;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2017-01-15
 * - Now we use 2 method for PO Send
 *   1. Direct to Supplier who is using same Retailsoft with same data Article, PO will be send as SO
 *   2. Send PO to EDI system, where supplier can login, then they can view / process PO
 * 
 * </pre><br>
 */
public class PurchaseOrderSend extends PurchaseOrderView
{
    private static final Log log = LogFactory.getLog(PurchaseOrderSend.class);
    
	String sPOID = "";
	PurchaseOrder oPO = null;
	List vPOD = null;	
	Vendor oVendor = null;	
	String sURL = "";
	String sUser = "";
	String sEmailMsg = "";
	String sResult = "";
	
    public void doBuildTemplate(RunData data, Context context)
    {    	    	
    	super.doBuildTemplate(data,context);

		sPOID = data.getParameters().getString("PoId");
		if (StringUtil.isNotEmpty(sPOID))
		{			
			try 
			{
				//upload PO via XML to Vendor
				oPO = PurchaseOrderTool.getHeaderByID(sPOID);
				vPOD = PurchaseOrderTool.getDetailsByID(sPOID);	
				
				if (oPO != null && vPOD != null)
				{
					oVendor = VendorTool.getVendorByID(oPO.getVendorId());					
					if(oVendor != null)
					{						
						processToSO(data,context);
					}
					else
					{
						data.setMessage("PO Vendor Data Not Found!");
					}
				}
				else
				{
					data.setMessage("PO Not Found");
				}				
			} 
			catch (Exception _oEx) 
			{
				data.setMessage("ERROR:" + _oEx.getMessage());
				log.error("ERROR:" + _oEx.getMessage(), _oEx);
			}
		}//if op == 1		
	}
   
    //-------------------------------------------------------------------------
    // TO SO Methods
    //-------------------------------------------------------------------------    
    void processToSO(RunData data, Context context)
    	throws Exception
    {
    	if (validateVendor(data))
		{
			sURL = oVendor.getWebSite();
			sUser = data.getUser().getName();
			sResult = createSO(); 
			context.put("result",sResult);
			
			//update Sent Status Times in PO (as SENT Times)
			oPO.setSentStatus(oPO.getSentStatus() + 1);
			oPO.save();
			data.setMessage("PO Sent to " + sURL + ", Please Check The Result Below from Vendor Server");
		}
    }
    
    boolean validateVendor(RunData data)
    {
    	if (oVendor != null)
    	{
			if (StringUtil.isEmpty(oVendor.getWebSite()))
			{
				data.setMessage("Vendor URL (Website Field) is Empty");
				return false;
			}    	
			if (StringUtil.isEmpty(oVendor.getValue1()))
			{
				data.setMessage("Vendor Customer Code (Value 1) is Empty");
				return false;
			}    	
			if (StringUtil.isEmpty(oVendor.getValue2()))
			{
				data.setMessage("Vendor Location Code (Value 2) is Empty");
				return false;
			}
			if (StringUtil.isEmpty(oVendor.getEmail()))
			{
				//data.setMessage("Vendor Email (Email Field) is Empty");
				//return false;
			}
    	}
    	else
    	{
			data.setMessage("Vendor Not Found");
			return false;    		
    	}
    	return true;
    }
        
	String createSO() 
		throws Exception
	{		
		Document doc = DocumentHelper.createDocument();
		Element root = doc.addElement("trans");
		Element so = root.addElement("sales_order");
		so.addAttribute("trans_no",oPO.getPurchaseOrderNo());
		so.addAttribute("location_code",oVendor.getValue2()); 
		so.addAttribute("customer_code",oVendor.getValue1()); 
		so.addAttribute("trans_date",CustomFormatter.formatDate(oPO.getTransactionDate())); 
		so.addAttribute("salesman", ""); 
		so.addAttribute("create_by",oPO.getCreateBy()); 
		so.addAttribute("payment_type",PaymentTypeTool.getCodeByID(oPO.getPaymentTypeId())); 
		so.addAttribute("payment_term",PaymentTermTool.getCodeByID(oPO.getPaymentTermId())); 
		so.addAttribute("remark",oPO.getRemark()); 
		so.addAttribute("total_disc",oPO.getTotalDiscountPct());  //total disc
		so.addAttribute("inclusive_tax",Boolean.toString(oPO.getIsInclusiveTax()));  //inc 
		
		so.addAttribute("est_freight","0");  //est freight
		so.addAttribute("freight_acc","");   //acc 
		so.addAttribute("down_payment","0");  //dp
		so.addAttribute("bank_code","");   //bank
		so.addAttribute("dp_account","");   //dp acc
		so.addAttribute("cash_flow_type","");   //dp cf		
		so.addAttribute("issuer_dest","");   //bank issuer
		so.addAttribute("reference_no","");   //ref no
		so.addAttribute("due_date","");  //dp due date
		so.addAttribute("po_no",oPO.getPurchaseOrderNo());  //po no
		so.addAttribute("po_date",CustomFormatter.formatDate(oPO.getTransactionDate()));  //po date
		
		for (int i = 0; i < vPOD.size(); i++)
		{
			PurchaseOrderDetail oTD = (PurchaseOrderDetail)vPOD.get(i);
			
			Element item = so.addElement("item");
	    	item.addAttribute("item_code", oTD.getItemCode()); 
			item.addAttribute("qty", oTD.getQty().toString()); 
			item.addAttribute("unit", oTD.getUnitCode()); 
			item.addAttribute("item_price", oTD.getItemPrice().toString()); 
			item.addAttribute("tax", TaxTool.getCodeByID(oTD.getTaxId())); 
			item.addAttribute("discount", oTD.getDiscount()); 
		}
		
		String sDoc = doc.asXML();
		log.debug("DOC:" + sDoc);
		
		PostMethod oPost =  new PostMethod(sURL);
		oPost.addParameter("user", sUser);    	
		oPost.addParameter("doc", sDoc);
		oPost.addParameter("helper", "com.ssti.enterprise.pos.xml.helper.SalesOrderXML");
	
		log.debug("URI:" + oPost.getURI());
		log.debug("PARAMS:" + oPost.getParameter("user") + " " + oPost.getParameter("doc") + " " + oPost.getParameter("helper"));
		
		String sResult = "";
		HttpClient oHTTPClient = new HttpClient();    	
		try 
		{
			int iResult = oHTTPClient.executeMethod(oPost);
			sResult = oPost.getResponseBodyAsString();
			if (iResult != -1)
			{
				oPost.releaseConnection();
			}
			log.debug("RESULT: " + iResult + " RESPONSE: " + sResult);
		} 
		catch (HttpException _oEx) 
		{
			sResult  += "HTTP Error: " + _oEx.getMessage();
			_oEx.printStackTrace();
			log.error(_oEx.getMessage(), _oEx);
		} 
		catch (IOException _oEx) 
		{
			sResult  += "I/O Error: " + _oEx.getMessage();
			_oEx.printStackTrace();
			log.error(_oEx.getMessage(), _oEx);
		}
		try
		{
			sendToVendor();
			sResult += "\n\nEMAIL SENT TO: " + oVendor.getEmail();
		}
		catch (Exception _oEx) 
		{
			sResult += "\n\nMAIL ERROR: " + _oEx.getMessage();
			sResult += "\n You can resend or manually send the Message: \n" +  sEmailMsg;
			_oEx.printStackTrace();
			log.error(_oEx.getMessage(), _oEx);
		}			
		return sResult;		
	}
	
	void sendToVendor()
		throws Exception
	{		
		if (StringUtil.isNotEmpty(oVendor.getEmail()) && oPO != null)
		{
		    String sEmailSvr = PreferenceTool.getSysConfig().getEmailServer();
		    String sEmailUsr = PreferenceTool.getSysConfig().getEmailUser();
		    String sEmailPwd = PreferenceTool.getSysConfig().getEmailPwd();	
		    
		    String subject = "PO From Customer: " + PreferenceTool.getCompany().getCompanyName();
		    StringBuilder sbBody = new StringBuilder();
		    sbBody.append("Dear Partner,")
		    	  .append(s_LINE_SEPARATOR)
		    	  .append("Please find our PO that is already submitted and generated as a Pending Sales Order in your system. ")
		    	  .append(s_LINE_SEPARATOR)
		    	  .append("PO Number is: ").append(oPO.getPurchaseOrderNo())	    	  
		    	  .append(s_LINE_SEPARATOR)	    	  
		    	  .append("PO Items are: ");
		    
		    for (int i = 0; i < vPOD.size(); i++)
			{
				PurchaseOrderDetail oTD = (PurchaseOrderDetail)vPOD.get(i);
				
				sbBody.append(StringUtil.left(oTD.getItemCode(),15))
					  .append(" ").append(StringUtil.left(oTD.getItemName(),30))
					  .append(" ").append(StringUtil.left(oTD.getQty().toString(),10))
					  .append(" ").append(StringUtil.left(oTD.getUnitCode(),10))	    	  
		    	  	  .append(" ").append(CustomFormatter.fmt(oTD.getSubTotal()))	    	  	    	  	  
					  .append(s_LINE_SEPARATOR);	    	  	    	  	   	    
			}
		    sbBody.append("Thanks & Regards")		          
		    	  .append(s_LINE_SEPARATOR)
		          .append(s_LINE_SEPARATOR)
		      	  .append(StringUtil.center(CustomFormatter.fmt(DateUtil.getTodayDate()), 20)).append(",")		      	  		      	  
		      	  .append(s_LINE_SEPARATOR)		    	  
		          .append(StringUtil.center(sUser, 20))
		    	  .append(s_LINE_SEPARATOR)
		      	  .append(s_LINE_SEPARATOR)
		      	  .append(s_LINE_SEPARATOR)
		    	  .append("___________________")
		    	  .append(s_LINE_SEPARATOR);
		    sbBody.append(PreferenceTool.getCompany().getCompanyName());	    	   	    	    
		    sEmailMsg = sbBody.toString();
			String[] aTo = {oVendor.getEmail()};
			MailSender oMS = new MailSender(sEmailSvr, sEmailUsr, sEmailPwd, sEmailUsr, aTo, subject, sEmailMsg, "");
			oMS.send();
		}
	}
}