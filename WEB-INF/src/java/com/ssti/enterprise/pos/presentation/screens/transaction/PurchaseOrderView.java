package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class PurchaseOrderView extends PurchaseOrderTransaction
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	setContext(data, context);
    	try 
    	{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findPurchaseOrderResult", "PurchaseOrders");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}