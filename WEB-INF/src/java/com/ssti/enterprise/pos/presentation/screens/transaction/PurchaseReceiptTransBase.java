package com.ssti.enterprise.pos.presentation.screens.transaction;

import com.ssti.enterprise.pos.om.PurchaseReceipt;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/PurchaseReceiptTransaction.java,v $
 * Purpose: this class used as purchase receipt screen handler 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseReceiptTransaction.java,v 1.38 2009/05/04 02:02:36 albert Exp $
 * @see PurchaseReceipt
 *
 * $Log: PurchaseReceiptTransaction.java,v $
 * Revision 1.38  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 * Revision 1.37  2008/03/13 03:07:08  albert
 * *** empty log message ***
 */

public class PurchaseReceiptTransBase extends PurchaseReceiptTransaction
{
}
