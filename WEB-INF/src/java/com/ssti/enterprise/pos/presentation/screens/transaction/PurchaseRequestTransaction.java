package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.PurchaseRequest;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/PurchaseRequestTransaction.java,v $
 * Purpose: this class used as purchase request screen handler 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseRequestTransaction.java,v 1.27 2007/11/09 01:13:28 albert Exp $
 * @see PurchaseRequest
 *
 * $Log: PurchaseRequestTransaction.java,v $
 * 
 * 2017-05-26
 * - Add function in PurchaseOrderTool && ItemTransferTool, change processAddItem calculate outstanding TI & outstanding PO Qty 
 *   before generating recommended Qty.
 * 
 * 2015-07-31
 * - Change processAddItem allow to add Non Inventory / Asset Item so this can be used as Purchase Request
 * 
 */

public class PurchaseRequestTransaction extends PurchaseSecureScreen
{
	protected static final String[] a_PERM = {"Inventory Transaction", "View Item Request"};
	
	protected static final Log log = LogFactory.getLog(PurchaseRequestTransaction.class);
	
	protected static final int i_OP_ADD_DETAIL  = 1;
	protected static final int i_OP_DEL_DETAIL  = 2;
	protected static final int i_OP_NEW_TRANS   = 3;
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details
	protected static final int i_OP_ADD_ITEMS   = 5;
	protected static final int i_OP_COPY_TRANS  = 6;	
	protected static final int i_OP_SEND_ALL    = 7;
	protected static final int i_OP_IMPORT_FROM_EXCEL  = 8;

	protected PurchaseRequest oRQ = null;
	protected List vRQD = null;
	protected HttpSession oSes;
	
	protected boolean isAuthorized(RunData data)
	throws Exception
	{
		return isAuthorized (data, a_PERM);
	}
	
	protected int iOp = 0;
	
	public void doBuildTemplate ( RunData data, Context context )
	{
		super.doBuildTemplate(data, context);
		
		iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}
			else if ( iOp == i_OP_NEW_TRANS )
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS )
			{
				getData (data);
			}
			else if (iOp == i_OP_ADD_ITEMS && !b_REFRESH)
			{
				addItems (data);
			}			
			else if (iOp == i_OP_COPY_TRANS && !b_REFRESH) {
				copyTrans (data);
			}			
			else if (iOp == i_OP_SEND_ALL && !b_REFRESH) {
				sendAll (data,context);
			}			
			
			//put rq in context
			context.put(s_RQD, oSes.getAttribute (s_RQD));
			context.put(s_RQ, oSes.getAttribute (s_RQ));
			
			//put config in context
			context.put(s_RELATED_EMPLOYEE, Boolean.valueOf(b_PURCHASE_RELATED_EMPLOYEE));
			if(hasPermission(data, "Access All Vendor"))
			{
				context.put(s_RELATED_EMPLOYEE, Boolean.valueOf(false));
			}
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error (_oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
	}
	
	protected void initSession ( RunData data )
	{
		synchronized (this)
		{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_RQ) == null )
			{
				oSes.setAttribute (s_RQ, new PurchaseRequest());
				oSes.setAttribute (s_RQD, new ArrayList());
			}
			oRQ = (PurchaseRequest) oSes.getAttribute (s_RQ);
			vRQD = (List) oSes.getAttribute (s_RQD);
		}
	}
	
	/**
	 * Add items from query string, we sent item id in 00123,00124 
	 * 
	 * @param data
	 * @throws Exception
	 */
	protected void addItems(RunData data)
		throws Exception
	{
		//save current updated qty first
		PurchaseRequestTool.updateDetailQty (vRQD,data);
		
		String sItemID = data.getParameters().getString ("items");		
		log.debug (sItemID);
		if (sItemID != null && !sItemID.equals(""))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem (sItemID, data);
				}
			}
			else 
			{
				processAddItem (sItemID, data);
			}
		}
	}
	
	protected void processAddItem ( String _sItemID, RunData data)
	throws Exception
	{
		processAddItem (_sItemID, data, -1);
	}
	
	/**
	 * Item to add to request detail
	 * 
	 * @param _sItemID
	 * @param data
	 * @param _iFromAddDetail this is the flag that differentiate request from 
	 * single addDetail method, or from addItems method
	 * 					 
	 * @throws Exception
	 */
	protected void processAddItem(String _sItemID, 
								  RunData data, 
								  int _iFromAddDetail)
		throws Exception
	{
		Item oItem = ItemTool.getItemByID(_sItemID);		
		String sLocID = data.getParameters().getString("LocationId");
		int iSetRecQty = data.getParameters().getInt("SetRecQty",1);
		
		//only process add if ITEM TYPE is INVENTORY TYPE
		if (oItem != null && (oItem.getItemType() == i_INVENTORY_PART || 
			oItem.getItemType() == i_NON_INVENTORY_PART) || 
			oItem.getItemType() == i_ASSET)
		{
			oRQ.setLocationId(sLocID);
			System.out.println("oRQ:"+oRQ);
			
			PurchaseRequestDetail oRQD = new PurchaseRequestDetail();
			oRQD.setHeader(oRQ);
			Unit oUnit = UnitTool.getUnitByID(oItem.getUnitId());			
			String sDesc = oItem.getDescription();
			if(_iFromAddDetail == 1)
			{
				sDesc = data.getParameters().getString("Description",sDesc);
			}
			oRQD.setLocationId (sLocID);
			oRQD.setItemId     (_sItemID);
			oRQD.setItemCode   (oItem.getItemCode());
			oRQD.setItemName   (oItem.getItemName());
			oRQD.setDescription(sDesc);
			oRQD.setUnitId     (oUnit.getUnitId());
			oRQD.setUnitCode   (oUnit.getUnitCode());			
			oRQD.setCurrentQty (bd_ZERO);
			oRQD.setCostPerUnit(bd_ZERO);
			oRQD.setMinimumStock(bd_ZERO);
			oRQD.setMaximumStock(bd_ZERO);
			oRQD.setReorderPoint(bd_ZERO);			
			
			InventoryLocation oInvLoc = InventoryLocationTool.getDataByItemAndLocationID (_sItemID, sLocID);						
			ItemInventory oItemInv = ItemInventoryTool.getItemInventory(oRQD.getItemId(), sLocID);
			if(oItemInv != null)
			{
				oRQD.setItemInv(oItemInv);
				oRQD.setMinimumStock(oItemInv.getMinimumQty());
				oRQD.setMaximumStock(oItemInv.getMaximumQty());
				oRQD.setReorderPoint(oItemInv.getReorderPoint());
			}
			
			double dQtyOH = 0;
			if(oInvLoc != null)
			{				
				dQtyOH = oInvLoc.getCurrentQty().doubleValue();
				oRQD.setInvLoc(oInvLoc);
				oRQD.setCurrentQty (oInvLoc.getCurrentQty());
				oRQD.setCostPerUnit(oInvLoc.getItemCost());
			}

			double dRequestQty = 0;
			if(iSetRecQty == 1) //set automatically if not == 1 then set manually via logic in VM
			{
				//TODO : check validity. the default request qty is max - qty oh)				
				double dMaxStock = oRQD.getMaximumStock().doubleValue();
				if (dMaxStock != 0 && dMaxStock > dQtyOH) 
				{
					double dOutTI = 0;
					double dOutPO = 0;
					//if location is Store then check outstanding TI && outstanding PO
					if(oRQ != null && oRQ.getLocation() != null && oRQ.getLocation().getLocationType() == i_STORE)
					{
						List vOutTI = ItemTransferTool.getOutstandingTIQty(sLocID, _sItemID, null);
						if(vOutTI.size() > 0)
						{
							dOutTI = ItemTransferTool.filterProcessedQty(vOutTI, _sItemID, sLocID);
						}
					}				
					dOutPO = PurchaseOrderTool.getOnOrderQty(_sItemID, oRQ.getLocationId());
					dRequestQty = dMaxStock - dQtyOH - dOutTI - dOutPO;
					if(dRequestQty < 0) dRequestQty = 0;
				}
			} //end if automatically set req qty
			
			oRQD.setRecommendedQty (new BigDecimal(dRequestQty));
			oRQD.setRequestQty (bd_ONE);
			oRQD.setSentQty (bd_ZERO);
			//if current qty < reorder point 
			if( oRQD.getCurrentQty().doubleValue() <= oRQD.getReorderPoint().doubleValue() && _iFromAddDetail == -1)
			{				
				if(dRequestQty > 0)
				{
					oRQD.setRequestQty (new BigDecimal(dRequestQty));
				}
			}
			else
			{
				if(_iFromAddDetail == 1)
				{
					oRQD.setRequestQty (data.getParameters().getBigDecimal("Qty"));
				}				
			}			
			
			if (!isExist (oRQD))
			{
				if (b_PURCHASE_FIRST_LAST)
				{
					vRQD.add (0, oRQD);
				}
				else
				{
					vRQD.add (oRQD);
				}
			}
			PurchaseRequestTool.setHeaderProperties (oRQ, vRQD,data);
			oSes.setAttribute (s_RQD, vRQD);
			oSes.setAttribute (s_RQ, oRQ);
		}
		else //if item is null or item not inventory type
		{
			data.setMessage("Request available only for Inventory Part Item");
		}
	}
	
	/**
	 * add item to request detail
	 * 
	 * @param data
	 * @throws Exception
	 */
	protected void addDetailData ( RunData data )
	throws Exception
	{
		//save current updated qty first
		PurchaseRequestTool.updateDetailQty (vRQD,data);
		String sItemID = data.getParameters().getString("ItemId");
		processAddItem(sItemID, data, 1);
	}
	
	protected void delDetailData ( RunData data )
	throws Exception
	{
		//save current updated qty first
		PurchaseRequestTool.updateDetailQty (vRQD,data);
		
		if (vRQD.size() > (data.getParameters().getInt("No") - 1))
		{
			vRQD.remove ( data.getParameters().getInt("No") - 1 );
			oSes.setAttribute  ( s_RQD, vRQD );
			PurchaseRequestTool.setHeaderProperties ( oRQ, vRQD, data );
			oSes.setAttribute ( s_RQ, oRQ );
		}
	}
	
	protected void createNewTrans ( RunData data )
	{
		synchronized (this)
		{
			oRQ = new PurchaseRequest();
			vRQD = new ArrayList();
			oSes.setAttribute (s_RQ,  oRQ);
			oSes.setAttribute (s_RQD, vRQD);
		}
	}
	
	protected boolean isExist (PurchaseRequestDetail _oReqDetail)
	{
		for (int i = 0; i < vRQD.size(); i++)
		{
			PurchaseRequestDetail oExist = (PurchaseRequestDetail) vRQD.get (i);
			if ( oExist.getItemCode().equals(_oReqDetail.getItemCode()) && 
					(oExist.getItemId().equals(_oReqDetail.getItemId())) )
			{
				if(PreferenceTool.getPurchMergeSameItem())
				{
					mergeData (oExist, _oReqDetail);					
				}
				else
				{
					return false;
				}	
				return true;
			}
		}
		return false;
	}
	
	protected void mergeData (PurchaseRequestDetail _oOldTD, PurchaseRequestDetail _oNewTD)
	{
		double dQty = _oOldTD.getRequestQty().doubleValue() + 
		_oNewTD.getRequestQty().doubleValue();    
		_oOldTD.setRequestQty (new BigDecimal(dQty));    	
		_oNewTD = null;
	}
	
	protected void getData ( RunData data )
	throws Exception
	{
		String sID = data.getParameters().getString("id");
		if (StringUtil.isNotEmpty(sID))
		{
			try
			{
				oRQ = PurchaseRequestTool.getHeaderByID (sID);
				vRQD = PurchaseRequestTool.getDetailsByID (sID);
				oSes.setAttribute (s_RQ, oRQ);
				oSes.setAttribute (s_RQD, vRQD);
			}
			catch (Exception _oEx)
			{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
			}
		}
	}
	
	/**
	 * @param data
	 */
	protected void copyTrans(RunData data) 
	    throws Exception
	{
		try
		{
			String sTransID = data.getParameters().getString("TransId");
			PurchaseRequestTool.updateDetailQty(vRQD, data);
			
			if (StringUtil.isNotEmpty (sTransID))
			{			
				List vTrans = PurchaseRequestTool.getDetailsByID(sTransID);
				vRQD = new ArrayList(vTrans.size());
				for (int i = 0; i < vTrans.size(); i++)
				{
					PurchaseRequestDetail oDet = (PurchaseRequestDetail) vTrans.get(i);
					PurchaseRequestDetail oNewDet = oDet.copy();
					oNewDet.setSentQty(bd_ZERO);
					vRQD.add(oNewDet);
				}
				oSes.setAttribute (s_RQD, vRQD);
				oSes.setAttribute (s_RQ, oRQ);
			}
			PurchaseRequestTool.setHeaderProperties(oRQ, vRQD, data);		
		}
		catch (Exception _oEx)
		{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
		}
	}    
	
	/**
	 * 
	 */
    /**
     * 
     * @param data
     * @param context
     * @throws Exception
     */
	public void sendAll ( RunData data, Context context )
        throws Exception
    {
		try
		{
            for (int i = 0; i < vRQD.size(); i++)
			{
            	PurchaseRequestDetail oDet = new PurchaseRequestDetail();
            	oDet = (PurchaseRequestDetail) vRQD.get(i);
            	log.debug (oDet);				    
            	oDet.setSentQty(oDet.getRequestQty());
    			oSes.setAttribute (s_RQD, vRQD);
    			oSes.setAttribute (s_RQ, oRQ);    			
			}
			PurchaseRequestTool.setHeaderProperties (oRQ, vRQD, data);
        }
        catch (Exception _oEx)
        {
        	throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
        }
    }
}
