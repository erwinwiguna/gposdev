package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.ArPaymentDetail;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-07-31
 * - Allow multiple empty invoice with other amount / memo added
 * 
 * </pre><br>
 */
public class ReceivablePayment extends TransactionSecureScreen
{
	protected static final Log log = LogFactory.getLog(ReceivablePayment.class);
    protected static final String[] a_PERM = {"View Receivable Payment"};
	
    protected static final int i_OP_ADD_DETAIL    	  = 1;
	protected static final int i_OP_DEL_DETAIL    	  = 2;
    protected static final int i_OP_NEW  		  	  = 3;
    protected static final int i_OP_VIEW_AR_PAYMENT   = 4;
	
	protected static final int i_OP_ADD_DP			  = 5;
	protected static final int i_OP_DEL_DP			  = 6;
	protected static final int i_OP_LOAD_ALL_INV	  = 7;
    protected static final int i_OP_ADD_SEL_INV       = 8;
    
	protected static final int i_OP_REFRESH	  		  = 99;

    protected ArPayment oAR = null;    
    protected List vARD = null;
    protected List vARDP = null;		//List<List<String MemoID>>
    protected List vARDPTemp = null;	//List<String MemoID>
    protected HttpSession oSes;

    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return isAuthorized (data, a_PERM);
	}
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try
		{
			if ( iOp == i_OP_NEW )
			{
				newTransaction (data);
			}
			else if ( iOp == i_OP_ADD_DETAIL )
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL )
			{
				delDetailData (data);
			}
			else if (iOp == i_OP_VIEW_AR_PAYMENT )
			{
				getData (data, context);
			}
			else if (iOp == i_OP_ADD_DP)
			{
				addNewDP(data);
			}
			else if (iOp == i_OP_DEL_DP)
			{
				delDP(data);
			}			
			else if (iOp == i_OP_LOAD_ALL_INV)
			{
				loadAll(data);
			}	
            else if (iOp == i_OP_ADD_SEL_INV)
            {
                addInvoices(data);
            }               
			else if (iOp == i_OP_REFRESH)
			{
				//update detail & header
				ReceivablePaymentTool.updateDetail(oAR, vARD, data);
				ReceivablePaymentTool.setHeaderProperties(oAR, vARD, data);				
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			data.setMessage(_oEx.getMessage());
		}
		context.put(s_AR, oSes.getAttribute (s_AR));
    	context.put(s_ARD, oSes.getAttribute (s_ARD));
        context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_SALES_MULTI_CURRENCY));
        context.put("arpayment", ReceivablePaymentTool.getInstance());
    }
    
    protected void initSession ( RunData data )
	{	
    	synchronized (this)
    	{
    		oSes = data.getSession();
    		if (oSes.getAttribute (s_AR) == null) 
    		{
    			oSes.setAttribute (s_AR, new ArPayment());
    			oSes.setAttribute (s_ARD, new ArrayList());
    			oSes.setAttribute (s_ARDP, new ArrayList());
    			oSes.setAttribute (s_ARDPTMP,new ArrayList());
    		}
    		oAR = (ArPayment) oSes.getAttribute (s_AR);
    		vARD = (List) oSes.getAttribute (s_ARD);
    		vARDP = (List) oSes.getAttribute (s_ARDP);
    		vARDPTemp = (List) oSes.getAttribute (s_ARDPTMP);
    	}
	}
    
    protected void newTransaction ( RunData data ) 
		throws Exception
	{
		synchronized(this)
		{
			oAR = new ArPayment();
			vARD = new ArrayList();
			vARDP = new ArrayList();
			vARDPTemp = new ArrayList();
			
			oSes.setAttribute (s_AR, oAR);
			oSes.setAttribute (s_ARD, vARD);
			oSes.setAttribute (s_ARDP, vARDP);
			oSes.setAttribute (s_ARDPTMP, vARDPTemp);
		}
	}

    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
		ArPaymentDetail oARD = new ArPaymentDetail();
		data.getParameters().setProperties (oARD);
		if(!data.getParameters().getString("DPPaidAmount","").equals(""))
		{
			oARD.setDownPayment(new BigDecimal(data.getParameters().getDouble("DPPaidAmount")));
		}
		else
		{
			oARD.setDownPayment(bd_ZERO);
		}			
		if (!isExist (oARD.getInvoiceNo())) 
		{
			vARD.add (0, oARD);
			vARDP.add (0, vARDPTemp);			
			ReceivablePaymentTool.setHeaderProperties (oAR, vARD, data);
			
			//set TotalDP
			double dTotalDownPayment;
			if(oAR.getTotalDownPayment() != null)
			{
			    dTotalDownPayment = oAR.getTotalDownPayment().doubleValue();
			}
			else
			{
			    dTotalDownPayment = 0;
 			}
 			oAR.setTotalDownPayment ( new BigDecimal ( dTotalDownPayment + oARD.getDownPayment().doubleValue()));            
		}		
		oSes.setAttribute (s_AR, oAR); 
		oSes.setAttribute (s_ARD, vARD);
		oSes.setAttribute (s_ARDP, vARDP);
		oSes.setAttribute (s_ARDPTMP, new ArrayList());
    }

    protected void addNewDP ( RunData data ) 
    	throws Exception
    {
		String sMemoID = data.getParameters().getString("CreditMemoId");
		//double dMemoAmount = data.getParameters().getDouble("CreditMemoAmount");

		if (!isMemoExist (sMemoID)) 
		{
			vARDPTemp.add (0, sMemoID);
		}
		oSes.setAttribute (s_ARDPTMP, vARDPTemp);    
    }
    
    protected void delDP ( RunData data ) 
    	throws Exception
    {
    	oSes.setAttribute (s_ARDPTMP, new ArrayList());
    }
    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	int iDelNo = data.getParameters().getInt("delno") - 1;
		if (vARD.size() > iDelNo) 
		{
   			ArPaymentDetail oArPaymentDetail = (ArPaymentDetail)vARD.get(iDelNo);
			double dDPTemp = oArPaymentDetail.getDownPayment().doubleValue();

			vARD.remove (iDelNo);
			if (vARDP.size() > iDelNo)
			{
				removeARDP(iDelNo);
			}
			oSes.setAttribute (s_ARD, vARD);
	
			ReceivablePaymentTool.setHeaderProperties (oAR, vARD, data);
			double dTotalDP = oAR.getTotalDownPayment().doubleValue();
			
			dTotalDP = dTotalDP - dDPTemp;
			oAR.setTotalDownPayment(new BigDecimal(dTotalDP));

			oSes.setAttribute (s_AR, oAR); 
		}    	
    }

    /**
     * Remove List of MemoIDs linked with PaymentDetail
     * 
     * @param _iDelNo
     * @throws Exception
     */
    private void removeARDP(int _iDelNo)
     	throws Exception
	{
    	List vID = (List)vARDP.get(_iDelNo);
    	//if transaction is saved pending
    	if (vID != null && vID.size() > 0 && StringUtil.isNotEmpty(oAR.getArPaymentId()))
    	{
    		try
			{
    			//clear CM link to PP		
    			for (int i = 0; i < vID.size(); i++)
    	    	{
    	    		String sCMID = (String)vID.get(i);
    	    		CreditMemo oCM = CreditMemoTool.getCreditMemoByID(sCMID);
    	    		if(oCM != null && StringUtil.isNotEmpty(oCM.getPaymentTransId()))
    	    		{
	    	    		oCM.setPaymentTransId("");
	    	    		oCM.setPaymentInvId("");
	    	    		oCM.setPaymentTransNo("");
	    	    		oCM.save();
    	    		}    	    		
    	    	}
			}
			catch (Exception _oEx)
			{
				throw new Exception("Error Clearing Memo Payment Info: " + _oEx.getMessage(), _oEx);
			}			
    	}	
		vARDP.remove (_iDelNo);	 
	}
    
    protected void getData ( RunData data, Context context  )
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try
    		{
    			newTransaction(data);
    			oAR = ReceivablePaymentTool.getHeaderByID (sID);
    			vARD = ReceivablePaymentTool.getDetailsByID (sID);
    			oSes.setAttribute (s_AR, oAR);
				oSes.setAttribute (s_ARD, vARD);
				
				List vCM = new ArrayList();				
				for (int i = 0; i < vARD.size(); i++)
				{
					ArPaymentDetail oARD = (ArPaymentDetail) vARD.get(i);
					if(oARD.getDownPayment().doubleValue() != 0)
					{
						List vID = CreditMemoTool.getIDByPaymentTransID(sID, oARD.getArPaymentDetailId());	
						log.debug("ARD " + oARD.getArPaymentDetailId() + " vMemoID " + vID);
						vCM.add(vID);
					}
				}				
				oSes.setAttribute (s_ARDP, vCM);				
    		}
    		catch (Exception _oEx)
    		{
    			_oEx.printStackTrace();
    			log.error(_oEx);
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage (), _oEx);
    		}
    	}
    }
    
    protected boolean isExist (String _sInvoiceNo) 
    	throws Exception
    {
    	for (int i = 0; i < vARD.size(); i++) 
    	{
    		ArPaymentDetail oExistARD = (ArPaymentDetail) vARD.get (i);
    		if(oExistARD.getInvoiceNo().equals(_sInvoiceNo) && StringUtil.isNotEmpty(_sInvoiceNo)) 
    		{
				return true;    			
    		}		
    	}
    	return false;
    } 

    protected boolean isMemoExist (String _sMemoID) 
    	throws Exception
    {
    	for (int i = 0; i < vARDP.size(); i++) 
    	{
    		String sExistARD = (String) vARDP.get (i);
    		if ( sExistARD.equals(_sMemoID)) 
    		{
				return true;    			
    		}		
    	}
    	return false;
    } 
    
    protected void loadAll ( RunData data ) 
		throws Exception
	{
    	String sCustID = data.getParameters().getString("CustomerId");
    	String sLocationID = data.getParameters().getString("LocationId", "");
    	if (StringUtil.isNotEmpty(sCustID))
		{		
			String sInvNo = "";
    		String sPaymentTypeID = "";
    		boolean bTax = data.getParameters().getBoolean("ViewTax");
    		Customer oCust = CustomerTool.getCustomerByID(sCustID);    		
   		    String sCurrencyID = oCust.getDefaultCurrencyId();
    		Date dInvDate = null;
    		Date dDueDate = null;
        	
    		List vTR = TransactionTool.findCreditTrans(sInvNo, 
													   dInvDate, 
													   dInvDate, 													   
													   dDueDate, 
													   dDueDate, 													   
													   sCustID, 
													   sLocationID,
			                                           sPaymentTypeID, 
													   sCurrencyID,
													   bTax);
    		
    		vARD = (List) data.getSession().getAttribute(s_ARD);			    		
    		for (int i = 0; i < vTR.size(); i++)
    		{
    			SalesTransaction oTR = (SalesTransaction)vTR.get(i);    			
    			addInvoice(oTR);
    		}
		}
		ReceivablePaymentTool.setHeaderProperties (oAR, vARD, data);

		oSes.setAttribute (s_AR, oAR); 
		oSes.setAttribute (s_ARD, vARD);
		oSes.setAttribute (s_ARDP, vARDP);
		oSes.setAttribute (s_ARDPTMP, new ArrayList());
	}
    
    /**
     * add multiple invoices as once. item id saved in query string
     * parse the id one by one then process it
     * 
     * @param data
     * @throws Exception
     */
    protected void addInvoices(RunData data)
        throws Exception
    {        
        String sInvID = data.getParameters().getString ("invoices");      
        log.debug (sInvID);
        if (StringUtil.isNotEmpty(sInvID))
        {
            if (sInvID.indexOf(',') > 0 )
            {
                StringBuilder oTemp = new StringBuilder(sInvID);
                while ( oTemp.indexOf(",") > 0 ) 
                {   
                    int iLastCommaPosition = oTemp.indexOf(",");
                    sInvID = oTemp.substring (0, iLastCommaPosition);
                    oTemp.delete (0,iLastCommaPosition + 1);
                    
                    SalesTransaction oTR = TransactionTool.getHeaderByID(sInvID);
                    addInvoice (oTR);
                }
            }
            else 
            {
                SalesTransaction oTR = TransactionTool.getHeaderByID(sInvID);
                addInvoice (oTR);
            }
        }
        ReceivablePaymentTool.setHeaderProperties (oAR, vARD, data);

        oSes.setAttribute (s_AR, oAR); 
        oSes.setAttribute (s_ARD, vARD);
        oSes.setAttribute (s_ARDP, vARDP);
        oSes.setAttribute (s_ARDPTMP, new ArrayList());
    }
    
    protected void addInvoice(SalesTransaction oTR)
    {
        ArPaymentDetail oARD = new ArPaymentDetail();
        oARD.setDepartmentId("");
        oARD.setDiscountAccountId("");
        oARD.setDiscountAmount(bd_ZERO);
        oARD.setDownPayment(bd_ZERO);
        oARD.setInvoiceNo(oTR.getInvoiceNo());
        oARD.setInvoiceRate(oTR.getCurrencyRate());
        oARD.setMemoId("");
        oARD.setPaidAmount(oTR.getPaidAmount());
        oARD.setPaymentAmount(bd_ZERO);
        oARD.setProjectId("");
        oARD.setSalesTransactionAmount(oTR.getTotalAmount());
        oARD.setSalesTransactionId(oTR.getSalesTransactionId());
        oARD.setTaxPayment(false);
        
        vARD.add(oARD);
        vARDP.add(new ArrayList());        
    }
}
