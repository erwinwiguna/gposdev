package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.enterprise.pos.tools.sales.CreditLimitValidator;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2016-09-21
 * - change PWP logic using discount amount 
 *   1. Change method applyPWPDiscount to call PWPTool.updateDiscAmount
 * 
 * 2016-03-10
 * - change copy trans set SODetail Qty as left qty if copy from ORDERED / CLOSED
 * 
 * 2016-03-01
 * - add refreshDetail method to refreshPage properly after scan barcode
 * 
 * 24-02-2015
 * - Change addDetail - setDepartmentID from Salesman
 * </pre><br>
 */
public class SalesOrderTransaction extends SalesSecureScreen
{
	protected static final Log log = LogFactory.getLog(SalesOrder.class);
	
    protected static final String[] a_PERM = {"Sales Transaction", "View Sales Order"};

    protected static final int i_OP_ADD_DETAIL	= 1;
    protected static final int i_OP_DEL_DETAIL	= 2;
	protected static final int i_OP_NEW_TRANS	= 3;
	protected static final int i_OP_VIEW_TRANS	= 4; //after saved or view details
    protected static final int i_OP_ADD_ITEMS	= 5;
    protected static final int i_OP_COPY_TRANS	= 6;
    protected static final int i_OP_IMPORT_QT	= 7;
    protected static final int i_OP_REFRESH		= 11;    
    
    protected SalesOrder oSO = null;
    protected List vSOD = null;
    protected HttpSession oSOSes;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}  
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}
			else if ( iOp == i_OP_NEW_TRANS )
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS )
			{
				getData (data);
			}
			else if (iOp == i_OP_ADD_ITEMS && !b_REFRESH)
			{
				addItems (data);
			}
			else if (iOp == i_OP_COPY_TRANS ) 
			{
				copyTrans (data);
			}		
			else if (iOp == i_OP_REFRESH) 
			{
				refreshData(data);
			}
			
		}
		catch (Exception _oEx) 
		{
		    _oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put("limitvalidator", CreditLimitValidator.getInstance());
		context.put(s_SOD, oSOSes.getAttribute (s_SOD));
    	context.put(s_SO, oSOSes.getAttribute (s_SO));
    }

	private void refreshData(RunData data) 
		throws Exception
	{
        SalesOrderTool.updateDetail (vSOD, data);
        data.getParameters().setProperties (oSO);	
						
		SalesOrderTool.setHeaderProperties (oSO, vSOD,data);
		oSOSes.setAttribute (s_SOD, vSOD); 
		oSOSes.setAttribute (s_SO, oSO);     					
	}

	protected void initSession ( RunData data )
	{
		synchronized (this)
		{
			oSOSes = data.getSession();
			if ( oSOSes.getAttribute (s_SO) == null )
			{
				createNewTrans(data);
			}
			oSO = (SalesOrder) oSOSes.getAttribute (s_SO);
			vSOD = (List) oSOSes.getAttribute (s_SOD);
		}
	}

    protected void addDetailData ( RunData data )
    	throws Exception
    {
    	data.getParameters().setProperties (oSO);	
    	
    	//save current updated qty first
		SalesOrderTool.updateDetail (vSOD,data);
		    	
		SalesOrderDetail oSOD = new SalesOrderDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oSOD);	
			oSOD.setItemCode (oItem.getItemCode());
			oSOD.setItemName(oItem.getItemName());
			
			//set department id from salesman
			if(PreferenceTool.getMultiDept())
			{
				Employee oSales = oSO.getSalesman();
				if(oSales != null && StringUtil.isNotEmpty(oSales.getDepartmentId()))
				{
					oSOD.setDepartmentId(oSales.getDepartmentId());
				}
			}
		}
		else
		{
			return;
		}				
		
		double dPrice = oSOD.getItemPrice().doubleValue() * oSO.getCurrencyRate().doubleValue();
		
		//validate price first before add into detail
		TransactionTool.validatePrice(oSOD.getItemId(), dPrice, 
			oSOD.getDiscount(), oSO.getLocationId(), oSO.getCustomerId());

		if (!isExist (oSOD))
		{
		    if (b_SALES_FIRST_LAST)
		    {
		        vSOD.add (0 ,oSOD);
		    }
		    else{
   				vSOD.add (oSOD);
			}
		}
		SalesOrderTool.setHeaderProperties (oSO, vSOD,data);
		oSOSes.setAttribute (s_SOD, vSOD);
		oSOSes.setAttribute (s_SO, oSO);
    }

    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	//save current updated qty first
		SalesOrderTool.updateDetail (vSOD,data);

		if (vSOD.size() > (data.getParameters().getInt("No") - 1))
		{
			vSOD.remove ( data.getParameters().getInt("No") - 1 );
			oSOSes.setAttribute  ( s_SOD, vSOD );
			SalesOrderTool.setHeaderProperties ( oSO, vSOD, data );
			oSOSes.setAttribute ( s_SO, oSO );
		}
    }
    protected void createNewTrans ( RunData data )
    {
    	synchronized (this)
    	{
    		oSO = new SalesOrder();
    		vSOD = new ArrayList();
    		
    		//initialize Inclusive Tax
    		oSO.setIsInclusiveTax(b_SALES_TAX_INCLUSIVE);
    		
    		oSOSes.setAttribute (s_SO,  oSO);
    		oSOSes.setAttribute (s_SOD, vSOD);
    	}
    }

    protected boolean isExist (SalesOrderDetail _oSOD)
        throws Exception
    {
        boolean bIsExist = true;         
    	for (int i = 0; i < vSOD.size(); i++)
    	{
    		SalesOrderDetail oExistSOD = (SalesOrderDetail) vSOD.get (i);
    		
//    		if ( oExistSOD.getItemCode().equals(_oSOD.getItemCode()) && 
//    		     oExistSOD.getItemId().equals(_oSOD.getItemId()) ) 
//    		{
//    			throw new Exception ("Item " + _oSOD.getItemCode() + " Already exists ");
//    		}
    		
    		if ( oExistSOD.getItemId().equals(_oSOD.getItemId()) && 
    			 oExistSOD.getUnitId().equals(_oSOD.getUnitId()) &&
			     oExistSOD.getTaxId().equals(_oSOD.getTaxId()) &&
			     oExistSOD.getDescription().equals(_oSOD.getDescription()) &&
			    (oExistSOD.getDiscount().equals(_oSOD.getDiscount()) || oExistSOD.isQtyDiscount() || oExistSOD.isPwpDiscount() || oExistSOD.isMultiUnit()) &&    		     
				(oExistSOD.getItemPrice().intValue() == _oSOD.getItemPrice().intValue()) &&
				 StringUtil.isEqual(oExistSOD.getEmployeeId(), _oSOD.getEmployeeId()) )
    		{
       		    if(b_SALES_MERGE_DETAIL || oExistSOD.isPwpDiscount() || oExistSOD.isMultiUnit())
    		    {
				    mergeData (oExistSOD, _oSOD);
				    //apply discount setup to the existing detail data
				    //if discount on sales order was 0 then apply Disc from discount setup
				    if(_oSOD.getDiscount().equals("0"))
				    {
    			        applyDiscount (oSO, oExistSOD, false);	
    			    }
				}
				else
				{
				    bIsExist = false;
				}
				//apply discount to the new detail data
    	        //if discount on sales trans was 0 then apply Disc from discount setup
    	        if(_oSOD.getDiscount().equals("0"))
    	        {
    	            applyDiscount (oSO, _oSOD, false);
    	        }				
				return bIsExist;
    		}
    	}
    	//if there is no same item on list then
    	bIsExist = false;
    	//apply discount to the new detail data
    	//if discount on sales order was 0 then apply Disc from discount setup
    	if(_oSOD.getDiscount().equals("0"))
    	{
    	    applyDiscount (oSO, _oSOD, false);
    	}
    	return bIsExist;
    }
 
    protected void mergeData (SalesOrderDetail _oOldTD, SalesOrderDetail _oNewTD)
    {
    	double dQty 		= _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	double dTotalPurc   = dQty * _oNewTD.getItemPrice().doubleValue();
    	String sDiscount	= _oNewTD.getDiscount();
    	double dTotalDisc   = Calculator.calculateDiscount(sDiscount,dTotalPurc);
    	dTotalPurc 			= dTotalPurc - dTotalDisc;
    
    	double dTaxAmount   = _oNewTD.getTaxAmount().doubleValue()/100;
    	double dTotalTax 	= dTotalPurc * dTaxAmount;
    	double dSubTotal 	= dTotalPurc; //+ dTotalTax;

    	_oOldTD.setQty 			( new BigDecimal (dQty));
		_oOldTD.setDiscount 	( sDiscount );    	
		_oOldTD.setTaxAmount	( new BigDecimal (dTaxAmount * 100) );    
    	_oOldTD.setSubTotalDisc ( new BigDecimal (dTotalDisc) );
    	_oOldTD.setSubTotalTax 	( new BigDecimal (dTotalTax)  );
    	_oOldTD.setSubTotal     ( new BigDecimal (dSubTotal)  );
    	_oNewTD = null;
   	}

    //-------------------------------------------------------------------------
    // apply discount
    //-------------------------------------------------------------------------
    
    /**
     * apply discount to sales transaction
     * 
     * @param _oTR
     * @param _oTD
     * @throws Exception
     */
	protected void applyDiscount(SalesOrder _oTR, SalesOrderDetail _oTD, boolean _bMerge)
    	throws Exception
    {
		String sLocID = _oTR.getLocationId();
		String sItemID = _oTD.getItemId();
        String sCustID = _oTR.getCustomerId();
        String sPTypeID = _oTR.getPaymentTypeId();
		Date dDate = _oTR.getTransactionDate();		
        
        log.debug("****** FIND DISCOUNT **********");
        log.debug("Loc   : " + LocationTool.getLocationCodeByID(sLocID));
        log.debug("Cust  : " + CustomerTool.getCodeByID(sCustID));
        log.debug("Item  : " + ItemTool.getItemCodeByID(sItemID));
        
        Discount oDisc =  DiscountTool.findDiscount(sLocID, sItemID, sCustID, sPTypeID, dDate);
        boolean bPWP = false;
		boolean bSKU = false;
		boolean bMUL = false;		
		
		if (oDisc != null)
		{
            if (StringUtil.isNotEmpty(oDisc.getItemSku()))
            {
                bSKU = true;
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_PWP)
            {
                bPWP = true; 
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_MULTI)
            {
                bMUL = true; 
            }            
            if (bSKU)
			{
				applySKUDiscount(_oTD,oDisc,_bMerge);	
			}
            else if(bPWP)
            {
                applyPWPDiscount(_oTD,oDisc);
            }
            else if(bMUL)
            {
                applyMULDiscount(_oTD,oDisc);
            }            
			else
			{
			    applySTDDiscount(_oTD,oDisc);
			}
            _oTD.setDiscountId(oDisc.getDiscountId());
		}
	}    
    
	private void applySKUDiscount(SalesOrderDetail _oTD,
								  Discount _oDisc,
								  boolean _bMerge)
		throws Exception
	{
        log.debug ("****** SKU DISCOUNT **********");
		List vSKU = new ArrayList(vSOD.size());
		double dTotalSKUQty = 0;
		for (int i = 0; i < vSOD.size(); i++) 
    	{
    		SalesOrderDetail oExistTD = (SalesOrderDetail) vSOD.get (i);
    		Item oItem = ItemTool.getItemByID(oExistTD.getItemId());
    		if (oItem.getItemSku().equals(_oDisc.getItemSku()))
    		{
    			vSKU.add(oExistTD);
    			dTotalSKUQty += oExistTD.getQtyBase().doubleValue();
    		}
    	}
		if (!_bMerge)
		{
			vSKU.add(_oTD);
			dTotalSKUQty += _oTD.getQty().doubleValue();
		}
		for (int i = 0; i < vSKU.size(); i++) 
    	{
    		SalesOrderDetail oExistTD = (SalesOrderDetail) vSKU.get (i);
    		oExistTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, dTotalSKUQty));
            recalculateTD(oExistTD);
    	}
	}
    
	private void applyPWPDiscount(SalesOrderDetail _oTD,Discount _oDisc)
	    throws Exception
	{
        log.debug ("****** PWP DISCOUNT **********");
        PWPTool.updateGetDiscAmount(_oDisc, vSOD, _oTD);
        recalculateTD(_oTD);
	}

    private void applySTDDiscount(SalesOrderDetail _oTD,Discount _oDisc)
        throws Exception
    {
        log.debug ("****** NORMAL DISCOUNT **********");        
        _oTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, _oTD.getQty().doubleValue()));
        recalculateTD(_oTD);
    }

    /**
     * apply multi unit discount validate with existing buy qty
     * 
     * @param _oTD
     * @throws Exception
     */
    private void applyMULDiscount(SalesOrderDetail _oTD,Discount _oDisc)
    	throws Exception
    {    	    	
    	log.debug ("****** MULTI UNIT DISCOUNT **********");
		if(_oDisc != null)		
		{			
			int iBuy = _oDisc.getBuyQty().intValue();
			int iGet = _oDisc.getGetQty().intValue();
			int iPack = iBuy + iGet;
			int iQty = _oTD.getQty().intValue(); 
			
			int iAvail = (int) (iQty / iPack);
			int iDisc = iAvail * iGet;
			double dAmt = Calculator.mul(_oTD.getItemPrice(),iDisc);			
			
			_oTD.setDiscount(new BigDecimal(dAmt).toString());
			_oTD.setDiscountId(_oDisc.getDiscountId());	
			recalculateTD(_oTD);
		}
	}
    
    private void recalculateTD(SalesOrderDetail _oTD)
        throws Exception
    {        
        double dSubTotal =  _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();            
        double dSubTotalDisc = 0;
        dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), dSubTotal, _oTD.getQty());            
        dSubTotal = dSubTotal - dSubTotalDisc;
        double dSubTotalTax = (_oTD.getTaxAmount().doubleValue() / 100) * dSubTotal;
        
        //dSubTotal = dSubTotal;//+ dSubTotalTax;
        _oTD.setSubTotalDisc (new BigDecimal (dSubTotalDisc));
        _oTD.setSubTotalTax  (new BigDecimal (dSubTotalTax));
        _oTD.setSubTotal     (new BigDecimal (dSubTotal));
        
        log.debug("Discount : " + _oTD.getDiscount());
        log.debug("SubTotal : " + _oTD.getSubTotal());
    }  
    
    protected void getData (RunData data)
    	throws Exception
    {
        String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID))
    	{
    		try
    		{
    			synchronized (this) 
    	    	{
    				oSO = SalesOrderTool.getHeaderByID (sID);
    				vSOD = SalesOrderTool.getDetailsByID (sID);
	    			
    				oSOSes.setAttribute (s_SO, oSO);
					oSOSes.setAttribute (s_SOD, vSOD);
    	    	}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    	else
    	{
    		//createNewTrans(data);
    	}
    }
    
    /**
     * copy Trans
     * 
     * @param data
     * @throws Exception
     */
    protected void copyTrans (RunData data)
    	throws Exception
    {
    	synchronized (this) 
    	{		
	    	String sID = data.getParameters().getString("SoId");
	    	SalesOrder oTRC = SalesOrderTool.getHeaderByID(sID);
	    	SalesOrderTool.setHeaderProperties(oSO, vSOD, data);
	    	
			if (oTRC != null && StringUtil.isNotEmpty (sID))
			{			
				BeanUtil.setBean2BeanProperties(oTRC, oSO);
				oSO.setSalesOrderId("");
				oSO.setSalesOrderNo("");
				oSO.setNew(true);
				oSO.setModified(true);
				oSO.setTransactionStatus(i_SO_NEW);
				
				List vTrans = SalesOrderTool.getDetailsByID(sID);
				vSOD = new ArrayList(vTrans.size());
				for (int i = 0; i < vTrans.size(); i++)
				{
					SalesOrderDetail oDet = (SalesOrderDetail) vTrans.get(i);
					SalesOrderDetail oNewDet = oDet.copy();

					if(oTRC.getStatus() == i_SO_ORDERED || oTRC.getStatus() == i_SO_CLOSED)
					{
						double dLeft = Calculator.sub(oDet.getQty(), oDet.getDeliveredQty());
						
						System.out.println(oNewDet.getItemCode() + " dLeft : " + dLeft);
						
						oNewDet.setQty(new BigDecimal(dLeft));
						oNewDet.setQtyBase(UnitTool.getBaseQty(oNewDet.getItemId(), oNewDet.getUnitId(), oNewDet.getQty()));
					}
					oNewDet.setDeliveredQty(bd_ZERO);											
					vSOD.add(oNewDet);
				}
			}
			oSOSes.setAttribute (s_SOD, vSOD);
			oSOSes.setAttribute (s_SO, oSO);
    	}
    }      
        
	
	/**
	 * add multiple items as once. item id saved in query string
	 * parse the id one by one then process it
	 * 
	 * @param data
	 * @throws Exception
	 */
    private void addItems(RunData data)
    	throws Exception
    {
    	//save current updated qty first
		SalesOrderTool.updateDetail (vSOD,data);

		String sItemID = data.getParameters().getString ("items");		
		log.debug ("Sales Order Add Items:" + sItemID);
		if (StringUtil.isNotEmpty(sItemID))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem (sItemID,data);
				}
			}
			else {
				processAddItem (sItemID,data);
			}
		}
	}
    
    /**
     * process add single item from multiple item
     * 
     * @param _sItemID
     * @param data
     * @param _dQty if -1 then count using max - min 
     * if Qty -1 also mean call from addItems
     * @throws Exception
     */
    private void processAddItem (String _sItemID, RunData data)
    	throws Exception
    {
		
		double dUnitBaseValue = 1;
		SalesOrderDetail oSOD = new SalesOrderDetail ();
						
		Item oItem = ItemTool.getItemByID (_sItemID);
		if (oItem != null)
		{
		    oSOD.setItemId(oItem.getItemId());
		    oSOD.setItemCode(oItem.getItemCode());
	    	oSOD.setItemName(oItem.getItemName());
			oSOD.setQty(bd_ONE);
			oSOD.setQtyBase(bd_ONE);
			oSOD.setDiscount("0");		   
			oSOD.setUnitId(oItem.getUnitId());
			oSOD.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));
			oSOD.setItemPrice(oItem.getItemPrice());
            oSOD.setTaxId(oItem.getTaxId());
		    oSOD.setTaxAmount(TaxTool.getAmountByID(oItem.getPurchaseTaxId()));
			oSOD.setCostPerUnit(bd_ZERO);
			
			//calculate sub total, discount			
    		double dSubTotal = oSOD.getItemPrice().doubleValue();
    		double dDiscount = Calculator.calculateDiscount(oSOD.getDiscount(), dSubTotal);
    		dSubTotal = dSubTotal - dDiscount;
    		double dTax = dSubTotal * oSOD.getTaxAmount().doubleValue() / 100;
    		
    		oSOD.setSubTotalDisc (new BigDecimal(dDiscount));    	
			oSOD.setSubTotal (new BigDecimal(dSubTotal));			
    		oSOD.setSubTotalTax (new BigDecimal(dTax));
    		
			if (!isExist(oSOD))
			{
   			    if (b_SALES_FIRST_LAST)
   			    {
			        vSOD.add (0, oSOD);
			    }
			    else
			    {
    				vSOD.add (oSOD);
				}
			}
			SalesOrderTool.setHeaderProperties (oSO, vSOD,data);
			oSOSes.setAttribute (s_SOD, vSOD);
			oSOSes.setAttribute (s_SO, oSO);
    	}
    }
}
