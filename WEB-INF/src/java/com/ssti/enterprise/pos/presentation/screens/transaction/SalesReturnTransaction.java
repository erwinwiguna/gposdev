package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.StringUtil;
/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/SalesReturnTransaction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesReturnTransaction.java,v 1.17 2007/11/09 01:13:28 albert Exp $
 *
 * $Log: SalesReturnTransaction.java,v $
 * 
 * 2017-05-08
 * -  update doBuildTemplate, add redirect to open cashier variable to screen
 *  
 */
public class SalesReturnTransaction extends SalesSecureScreen
{
	private static final String[] a_PERM = {"View Sales Return"};
	//start screen methods
    
    private static final int i_OP_NEW  		  	  = 1;
    private static final int i_OP_GET_TRANS   	  = 2;
	private static final int i_OP_VIEW_RETURN 	  = 3;
	private static final int i_OP_VIEW_RETURN_ALT = 4;
	private static final int i_OP_ADD_NEW_ITEM    = 5;
	private static final int i_OP_DEL_ITEM  	  = 6;
	
    protected HttpSession oSes;
    protected SalesReturn oSR = null;    
    protected List vSRD = null;
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {	
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op",1);
		initSession (data);
		try 
		{
			//validate open cashier
	    	CashierBalanceTool.validateOpenCashier(data, context);

			if (iOp == i_OP_NEW) 
			{
		    	newTransaction (data);				
			}	
			else if ( iOp == i_OP_GET_TRANS && !b_REFRESH) 
			{
				getTransData (data, context);
			}	
			else if (iOp == i_OP_VIEW_RETURN || (iOp == i_OP_VIEW_RETURN_ALT)) 
			{
				getData (data, context);
			}
			else if (iOp == i_OP_ADD_NEW_ITEM && !b_REFRESH) 
			{
				addDetailData (data);
			}	
			else if (iOp == i_OP_DEL_ITEM && !b_REFRESH) 
			{
				delDetailData (data);
			}
			
     		context.put(s_RET_DET, oSes.getAttribute (s_SRD));
     		context.put(s_RET, oSes.getAttribute (s_SR));   
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
    }

    protected void initSession ( RunData data )
	{	
    	synchronized (this)
    	{
    		oSes = data.getSession();
    		if ( oSes.getAttribute (s_SR) == null ) 
    		{
    			oSes.setAttribute (s_SR, new SalesReturn());
    			oSes.setAttribute (s_SRD, new ArrayList());
    		}
    		oSR = (SalesReturn) oSes.getAttribute (s_SR);
    		vSRD = (List) oSes.getAttribute (s_SRD);
    	}
	}    

    private void newTransaction ( RunData data ) 
    {
    	synchronized (this)
    	{
    		oSR = new SalesReturn();
    		vSRD = new ArrayList();
    		
    		//initialize inclusive tax
    		oSR.setIsInclusiveTax(b_SALES_TAX_INCLUSIVE);
    		
    		oSes.setAttribute (s_SR,  oSR);
    		oSes.setAttribute (s_SRD, vSRD);
    	}
    }
    
    private void getTransData ( RunData data, Context context  ) 
		throws Exception
	{
		String sTransID = data.getParameters().getString("TransID");    	
		try 
		{	
			if(data.getParameters().getInt("ReturnFrom") == i_RET_FROM_PR_DO) //FROM DO
			{
	    		DeliveryOrder oDO = DeliveryOrderTool.getHeaderByID(sTransID);
	    		if (oSR != null)
	    		{
		    		List vTD = DeliveryOrderTool.getDetailsByID(oDO.getDeliveryOrderId());
			    	SalesReturnTool.mapDOToSR(oDO,oSR);
			    	vSRD = new ArrayList (vTD.size());
	
			    	for (int i = 0; i < vTD.size(); i++)
			    	{
			    		DeliveryOrderDetail oDOD = (DeliveryOrderDetail) vTD.get(i);
			    		SalesReturnDetail oSRD = new SalesReturnDetail();
			    		SalesReturnTool.mapDODetailToSRDetail (oDOD, oSRD);
			    		vSRD.add (oSRD);
			    	}
	    		}
			}
	
			if(data.getParameters().getInt("ReturnFrom") == i_RET_FROM_PI_SI) //FROM SI
			{
	    		SalesTransaction oSI = TransactionTool.getHeaderByID(sTransID);
	    		if (oSI != null)
	    		{
		    		List vTD = TransactionTool.getDetailsByID(oSI.getSalesTransactionId());
			    	SalesReturnTool.mapSIToSR(oSI,oSR);
			    	vSRD = new ArrayList (vTD.size());
	
			    	for (int i = 0; i < vTD.size(); i++)
			    	{
			    		SalesTransactionDetail oTD = (SalesTransactionDetail) vTD.get(i);
			    		SalesReturnDetail oSRD = new SalesReturnDetail();
			    		SalesReturnTool.mapSIDetailToSRDetail (oTD, oSRD);
			    		vSRD.add (oSRD);
			    	}
	    		}
			}    		
			oSes.setAttribute (s_SRD, vSRD);
			oSes.setAttribute (s_SR, oSR); 			
	  	}
		catch (Exception _oEx) 
		{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
		}
	}    
    
    private void getData (RunData data, Context context) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    		    oSes.setAttribute (s_SR, SalesReturnTool.getHeaderByID (sID));
		        oSes.setAttribute (s_SRD, SalesReturnTool.getDetailsByID (sID));
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }

    protected boolean isExist (SalesReturnDetail _oSRD, RunData data) 
		throws Exception
	{
    	if (_oSRD != null)
    	{
			for (int i = 0; i < vSRD.size(); i++) 
			{
				SalesReturnDetail oExistTD = (SalesReturnDetail) vSRD.get (i);
				if (StringUtil.isEqual(oExistTD.getItemCode(), _oSRD.getItemCode()) && 
					StringUtil.isEqual(oExistTD.getItemId(), _oSRD.getItemId()) && 
					StringUtil.isEqual(oExistTD.getItemName(), _oSRD.getItemName()) && 					
					StringUtil.isEqual(oExistTD.getDescription(), _oSRD.getDescription()) && 
					StringUtil.isEqual(oExistTD.getUnitId(), _oSRD.getUnitId()) &&
					oExistTD.getItemPrice().doubleValue() == _oSRD.getItemPrice().doubleValue())
				{
					mergeData (oExistTD, _oSRD);
					return true;    			
				}		
			}
    	}
		return false;
	}
	
	protected void mergeData (SalesReturnDetail _oOldTD, SalesReturnDetail _oNewTD) 
		throws Exception
	{		
		double dQty  = _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
		double dBase = UnitTool.getBaseValue(_oOldTD.getItemId(), _oOldTD.getUnitId());		
		
		double dQtyBase  = _oOldTD.getQty().doubleValue() * dBase + _oNewTD.getQty().doubleValue() * dBase;
		double dSubTotal = dQty * _oNewTD.getItemPrice().doubleValue();
	
		_oOldTD.setQty 	        (new BigDecimal(dQty));    	
		_oOldTD.setQtyBase 	    (new BigDecimal(dQtyBase));    	
		_oOldTD.setReturnAmount (new BigDecimal(dSubTotal));
		
		_oNewTD = null;
	}
	
	private void addDetailData ( RunData data ) 
		throws Exception
	{
	    SalesReturnTool.updateDetail(vSRD,data);
		
	    SalesReturnDetail oSRD = new SalesReturnDetail();
		data.getParameters().setProperties (oSRD);	
		oSRD.setTransactionDetailId("");
		if (!isExist(oSRD, data)) 
		{
		    if (b_SALES_FIRST_LAST)
		    {
			    vSRD.add(0, oSRD);
			}
			else{
			    vSRD.add (oSRD);
			}
		}
		SalesReturnTool.setHeaderProperties(oSR, vSRD, data);
		oSes.setAttribute (s_SRD, vSRD); 
		oSes.setAttribute (s_SR, oSR);     
	}
	
	private void delDetailData ( RunData data )
		throws Exception
	{
		int iIDX = data.getParameters().getInt("no") - 1;

		SalesReturnTool.updateDetail(vSRD,data);
		if (vSRD.size() > iIDX) 
		{
			vSRD.remove(iIDX);
		}    	
	    SalesReturnTool.setHeaderProperties(oSR, vSRD, data);
		oSes.setAttribute (s_SRD, vSRD);
		oSes.setAttribute (s_SR, oSR); 
	}    
}