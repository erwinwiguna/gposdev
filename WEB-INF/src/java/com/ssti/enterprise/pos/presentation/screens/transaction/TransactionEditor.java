package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.TransEdit;
import com.ssti.enterprise.pos.tools.data.TransEditTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 * 
 * 
 * @author  $Author: albert $
 * @version $Id: TransactionEditor,v 1.9 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: TransactionEditor,v $
 * 
 * 2015-02-22 change is Authorized, by pass Admin Role if fromTrans Called
 */
public class TransactionEditor extends TransactionSecureScreen
{ 	    
	boolean bFromTrans = false;
    protected boolean isAuthorized(RunData data) throws Exception    
    {
    	bFromTrans = data.getParameters().getBoolean("fromTrans");
    	if(!bFromTrans)
    	{
	        if (data.getACL() != null && data.getACL().getRoles() != null)
	        {
	            return data.getACL().getRoles().containsName("Administrator");
	        }
	        return false;
	    }
    	else
    	{
    		return true;
    	}        
    }
    
    TransEdit oTR = null; 
    protected HttpSession oSes;
    static final String s_TE = "trEdit";
    
    private void initSession(RunData data)
    {   
        synchronized(this)
        {
            oSes = data.getSession();
            if(oSes.getAttribute (s_TE) == null)
            {
                oSes.setAttribute (s_TE, new TransEdit());
            }
            oTR = (TransEdit) oSes.getAttribute (s_TE);
        }
    }
    
	public void doBuildTemplate(RunData data, Context context)
	{        
	    super.doBuildTemplate(data, context);
        
        int iOp = data.getParameters().getInt("op"); 
        initSession(data);
        
        try
        {
            if(iOp == 1)
            {
                oTR = new TransEdit();
                data.getParameters().setProperties(oTR);
                oTR.setEditDate(new Date());
                oTR.setUserName(data.getUser().getName());
                oSes.setAttribute(s_TE, oTR);
            }
            else if(iOp == 2)
            {
                oTR = new TransEdit();
                String sTRNo = data.getParameters().getString("TransNo");
                if (StringUtil.isNotEmpty(sTRNo))
                {
                    data.getParameters().setProperties(oTR);
                    oTR.setEditDate(new Date());
                    oTR.setUserName(data.getUser().getName());
                    Object o = TransEditTool.getByNo(oTR.getTransType(), sTRNo);
                    if (o != null)
                    {
                        oTR.setTrans(o);
                    }
                    else
                    {
                        data.setMessage("Transation " + sTRNo + " Not Found");
                    }
                }
                oSes.setAttribute(s_TE, oTR);
            }              
            else if(iOp == 3)
            {
                oTR = new TransEdit();
                oSes.setAttribute(s_TE, oTR);                
            }
            else if(iOp == 4)
            {
                String sID = data.getParameters().getString("id");
                if(StringUtil.isNotEmpty(sID))
                {
                    oTR = TransEditTool.getByID(sID);
                    if (oTR != null)
                    {
                        oTR.loadTrans();
                    }
                }
                oSes.setAttribute(s_TE, oTR);                
            }            
            else if(iOp == 5)
            {
                if(oTR != null)
                {
                    data.getParameters().setProperties(oTR);
                    TransEditTool.updateTrans(oTR);
                    data.setMessage("Transaction Edited Successfully");
                }
            }                          
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
            data.setMessage("ERROR: " + e.getMessage());
        }      
        context.put(s_TE, oSes.getAttribute(s_TE));
        context.put("tedit", TransEditTool.getInstance());
    }
}
