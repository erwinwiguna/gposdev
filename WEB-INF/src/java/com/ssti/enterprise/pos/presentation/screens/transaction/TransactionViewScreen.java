package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.tools.DateUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/TransactionSecureScreen.java,v $
 * Purpose: Transaction Secure screen, super class for all transaction screen
 * implements TransactionAttributes to hold various transaction related constants
 *
 * @author  $Author: albert $
 * @version $Id: TransactionSecureScreen.java,v 1.9 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: TransactionSecureScreen.java,v $
 * Revision 1.9  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 */
public class TransactionViewScreen extends TransactionSecureScreen 
{ 	

    protected int iCond;
    protected String sKeywords;
    protected Date dStart;
    protected Date dEnd;
    protected int iLimit;
    protected int iGroupBy;
    protected String sCurrencyID;
    protected String sLocationID;
    
	public void doBuildTemplate(RunData data, Context context)
	{
	    super.doBuildTemplate(data,context);
	}
    
    /**
     * 
     * @param data
     * @throws Exception
     */
    public void doFind (RunData data)
        throws Exception
    {       
        iCond = data.getParameters().getInt(s_COND);
        sKeywords = data.getParameters().getString(s_KEY);
        dStart = DateUtil.parseStartDate (data.getParameters().getString(s_START_DATE) );
        dEnd = DateUtil.parseEndDate (data.getParameters().getString(s_END_DATE) );
        iLimit = data.getParameters().getInt(s_VIEW_LIMIT, 50);     
        iGroupBy = data.getParameters().getInt(s_GROUP_BY, -1);     
        sCurrencyID = data.getParameters().getString(s_CURRENCY_ID);    
        sLocationID = data.getParameters().getString(s_LOCATION_ID);
    }   
}
