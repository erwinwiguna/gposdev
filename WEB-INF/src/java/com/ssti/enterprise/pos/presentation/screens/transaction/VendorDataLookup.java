package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.framework.tools.StringUtil;

public class VendorDataLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		String sVendorID = data.getParameters().getString("VendorId", "");
    		String sVendorName = data.getParameters().getString("VendorName", "");
    		String sVendorCode = data.getParameters().getString("VendorCode", "");

    		List vVendors = new ArrayList();
    		if (StringUtil.isNotEmpty(sVendorID))
    		{
    			vVendors.add (VendorTool.getVendorByID(sVendorID));
    		}
    		else if (StringUtil.isNotEmpty(sVendorName))
    		{
    			vVendors = VendorTool.getVendorByName(sVendorName, true, true);
    		}
    		else if (StringUtil.isNotEmpty(sVendorCode))
    		{
    			vVendors = VendorTool.getVendorByCode(sVendorCode, true, true);    			
    		}
			context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_PURCHASE_MULTI_CURRENCY));
    		context.put("vVendors", vVendors);
    	}
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Vendor Data Lookup Failed : " + _oEx.getMessage());
    	}
    }
}