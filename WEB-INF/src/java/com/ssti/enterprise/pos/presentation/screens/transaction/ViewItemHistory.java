package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class ViewItemHistory extends TransactionSecureScreen 
{
	@Override
	protected boolean isAuthorized(RunData data) throws Exception {
		return true;
	}

	@Override
	protected boolean isAuthorized(RunData data, String[] _aPerms) throws Exception {
		return true;
	}

	public void doBuildTemplate(RunData data, Context context)
	{
		super.doBuildTemplate(data, context);
	}	
}
