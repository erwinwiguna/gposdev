package com.ssti.enterprise.pos.presentation.screens.transaction.notif;

import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;

public class NotifCenter extends TransactionSecureScreen
{	
	@Override
	protected boolean isAuthorized(RunData data) throws Exception {		
		if(data.getACL() != null)
		{
			return true;
		}
		return false;
	}
	
	
}
