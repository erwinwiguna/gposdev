package com.ssti.enterprise.pos.presentation.screens.transaction.pointreward;

import java.util.Date;

import org.apache.turbine.services.localization.Localization;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PointTransaction;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.CustomParser;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/pointreward/PointRewardTransaction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PointRewardTransaction.java,v 1.6 2009/05/04 02:03:06 albert Exp $
 *
 * $Log: PointRewardTransaction.java,v $
 * Revision 1.6  2009/05/04 02:03:06  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/08/17 02:19:44  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/07/10 04:49:31  albert
 * *** empty log message ***
 *
 * Revision 1.3  2006/03/29 08:15:12  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/08/21 14:35:35  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/03/28 09:18:23  Albert
 * *** empty log message ***
 *
 */

public class PointRewardTransaction extends TransactionSecureScreen
{   
	protected static int i_GET_CUSTOMER_POINT = 1;
	protected static int i_SAVE_TRANSACTION   = 2;
	protected static int i_VIEW_TRANSACTION   = 4;
	
	private static final String s_SAVE_SUCCESS = "point_save_success";
	private static final String s_SAVE_FAILED = "point_save_failed";	
	private static final String s_CONTEXT = "ptr";
	
    //start screen methods
	private static final String[] a_PERM = {"View Sales Invoice"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("Op");
    	if (iOp == i_SAVE_TRANSACTION)
    	{
    		PointTransaction oPoint = new PointTransaction();
    		try
			{
    			data.getParameters().setProperties(oPoint);
    			oPoint.setTransactionDate(CustomParser.parseDate(data.getParameters().getString("TransactionDate")));
    			oPoint.setCreateDate(new Date());
    			PointRewardTool.saveData(oPoint, null);
    			data.setMessage(Localization.getString(Localization.getDefaultBundleName(), 
    							PreferenceTool.getLocale(data), s_SAVE_SUCCESS));
			}
    		catch (Exception _oEx)
			{
    			data.setMessage(Localization.getString(Localization.getDefaultBundleName(), 
				   	   			PreferenceTool.getLocale(data), s_SAVE_FAILED) + _oEx.getMessage());
    			_oEx.printStackTrace();
    			log.error (_oEx);
			}
    		context.put (s_CONTEXT, oPoint);
    	}
    	else if (iOp == i_VIEW_TRANSACTION)
    	{
    		String sTransID = data.getParameters().getString("id");
    		try
			{
    			PointTransaction oPoint = PointRewardTool.getPointRewardTransByID(sTransID, null);
        		context.put (s_CONTEXT, oPoint);
			}
    		catch (Exception _oEx)
			{
    			_oEx.printStackTrace();
	    		log.error (_oEx);
			}
    	}
    }
}
