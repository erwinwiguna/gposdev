package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.4  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/12/20 13:48:42  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ArPaymentPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
    String s_PDF_TPL_NAME = "/print/ArPaymentPDF.vm";
    String s_TXT_TPL_NAME = "/print/ArPaymentTXT.vm";
    String s_HTML_TPL_NAME = "/print/ArPaymentHTML.vm";
   
	private ArPayment oTR = null;
	List vTD = null;
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		
		if (_oSession == null) 
		{
			oTR = ReceivablePaymentTool.getHeaderByID (sID);
			vTD = ReceivablePaymentTool.getDetailsByID (sID);
		}
		else
		{
			oTR = (ArPayment) _oSession.getAttribute(AppAttributes.s_AR);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_ARD);
		}
		
        oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        checkTemplate (oParam);
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }                
        return oWriter.toString();
    }

    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);

		//get transaction data by id		
		if (_oSession == null) 
		{
			oTR = ReceivablePaymentTool.getHeaderByID (sID);
			vTD = ReceivablePaymentTool.getDetailsByID (sID);
		}
		else
		{
			oTR = (ArPayment) _oSession.getAttribute(AppAttributes.s_AR);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_ARD);
		}
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }    
}
