package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.BankReconcile;
import com.ssti.enterprise.pos.presentation.screens.transaction.BankReconcileTrans;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankReconcilePrintHelper.java,v 1.1 2008/08/17 02:20:01 albert Exp $ <br>
 *
 * <pre>
 * 
 * </pre><br>
 */
public class BankReconcilePrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
    String s_PDF_TPL_NAME = "/print/BankReconcilePDF.vm";
    String s_TXT_TPL_NAME = "/print/BankReconcileTXT.vm";
    String s_HTML_TPL_NAME = "/print/BankReconcileHTML.vm";
    
	private BankReconcile oTR = null;
	List vTD = null;
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);

    	oTR = (BankReconcile) _oSession.getAttribute(BankReconcileTrans.s_BR);
		vTD = (List) _oSession.getAttribute(BankReconcileTrans.s_BRD);
        
		oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        checkTemplate (oParam);

        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }        
        return oWriter.toString();
    }

    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);

    	oTR = (BankReconcile) _oSession.getAttribute(BankReconcileTrans.s_BR);
		vTD = (List) _oSession.getAttribute(BankReconcileTrans.s_BRD);
        checkTemplate (oParam);

		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }    
}
