package com.ssti.enterprise.pos.print.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.CashierBalance;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashierTransPrintHelper.java,v 1.4 2007/12/20 13:48:42 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashierTransPrintHelper.java,v $
 * Revision 1.4  2007/12/20 13:48:42  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CashierTransPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper, TransactionAttributes 
{           
	private CashierBalance oTR = null;
	
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	s_TXT_TPL_NAME = "/print/CashierTransTXT.vm";
    	
    	String sID = getString(oParam, s_PARAM_ID);;
		
		oTR = CashierBalanceTool.getByID(sID);
        oCtx.put(s_CTX_TRANS, oTR);

		if (oTR != null && oTR.getTransactionType() == CashierBalanceTool.i_CLOSE)
		{
			CashierBalance oOpening = CashierBalanceTool.getByID(oTR.getOpenCashierId());
	        oCtx.put("opening", oOpening);
		}	
		
		checkTemplate (oParam);
				
		System.out.println(s_TXT_TPL_NAME);
		
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        
        return oWriter.toString();
    }

    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
        return getPrintedText (oParam, _iPrintType, _oSession);
    }    
}
