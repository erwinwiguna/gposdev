package com.ssti.enterprise.pos.print.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InvoicePrintHelper.java,v 1.21 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: InvoicePrintHelper.java,v $
 * 
 * </pre><br>
 */
public class GeneralPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper, AppAttributes
{    
	private static Log log = LogFactory.getLog(GeneralPrintHelper.class);
		   	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
		String sID = getString(oParam, s_PARAM_ID);
		String sUserName = getString(oParam, s_PARAM_USER);
				
        prepareContext();
        oCtx.put("session",_oSession);
        
        checkTemplate (oParam);        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
        {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF)
        {
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML)
        {
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        
        return oWriter.toString();
    }
 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
    	return getPrintedText(oParam, _iPrintType, _oSession);
    }    
    
    /**
     * 
     * @param _oTR
     * @param _sUserName
     * @throws Exception
     */
    private void logPrintingProcess (SalesTransaction _oTR, String _sUserName)	
    	throws Exception
    {

    }
    
    /**
     * 
     * @throws Exception
     */
    private void prepareContext()
    	throws Exception
    {		                
    	Context oGlobal = TurbinePull.getGlobalContext();		
    	for (int i = 0; i < oGlobal.getKeys().length; i++)
    	{
    		String sKey = (String) oGlobal.getKeys()[i];
    		oCtx.put(sKey, oGlobal.get(sKey));
    	}
    }
}
