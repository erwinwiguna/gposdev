package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.InvoiceMessageTool;
import com.ssti.enterprise.pos.tools.PrintingLogTool;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InvoicePrintHelper.java,v 1.21 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: InvoicePrintHelper.java,v $
 * Revision 1.21  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.20  2008/08/17 02:20:01  albert
 * *** empty log message ***
 *
 * Revision 1.19  2008/03/09 15:04:00  albert
 * *** empty log message ***
 *
 * Revision 1.18  2007/12/20 13:48:42  albert
 * *** empty log message ***
 *
 * Revision 1.17  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.16  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class InvoicePrintHelper 
	extends BasePrintHelper 
	implements PrintHelper, AppAttributes
{    
	private static Log log = LogFactory.getLog(InvoicePrintHelper.class);
		   
	private SalesTransaction oTR = null;
	private List vTD = null;
	private List vPMT = null;
	private String s_CTX_PMT = "vPmt";
		
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
		String sID = getString(oParam, s_PARAM_ID);
		String sUserName = getString(oParam, s_PARAM_USER);
		
		if (_oSession != null && _oSession.getAttribute(s_TR) != null) 
		{
			oTR = (SalesTransaction) _oSession.getAttribute(s_TR);
			vTD = (List) _oSession.getAttribute(s_TD);
			vPMT = (List) _oSession.getAttribute(s_PMT);
		}
		else
		{
			oTR = TransactionTool.getHeaderByID (sID);
			vTD = TransactionTool.getDetailsByID (sID);
			vPMT = InvoicePaymentTool.getTransPayment(sID);
		}
        logPrintingProcess (oTR,  sUserName);
        
        prepareContext();
		oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        oCtx.put(s_CTX_PMT, vPMT);
        oCtx.put("invoicepayment", InvoicePaymentTool.getInstance());
        oCtx.put("userName",sUserName);
        
        checkTemplate (oParam);        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
        {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF)
        {
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML)
        {
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        
        return oWriter.toString();
    }
 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
		String sID = getString(oParam, s_PARAM_ID);
		String sUserName = getString(oParam, s_PARAM_USER);
		
		if (_oSession != null)
		{
			oTR = (SalesTransaction) _oSession.getAttribute(s_TR);
			vTD = (List) _oSession.getAttribute(s_TD);
			vPMT = (List) _oSession.getAttribute(s_PMT);
		}
		//get transaction data by id		
		else if (_oSession == null || oTR == null) 
		{
			oTR = TransactionTool.getHeaderByID (sID);
			vTD = TransactionTool.getDetailsByID (sID);
			vPMT = InvoicePaymentTool.getTransPayment(sID);
		}
		logPrintingProcess (oTR,  sUserName);

		prepareContext();
		checkTemplate (oParam);
		oCtx.put(s_CTX_PMT, vPMT);
		oCtx.put("userName",sUserName);
		
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }    
    
    /**
     * 
     * @param _oTR
     * @param _sUserName
     * @throws Exception
     */
    private void logPrintingProcess (SalesTransaction _oTR, String _sUserName)	
    	throws Exception
    {
    	if (_oTR != null && StringUtil.isNotEmpty(_oTR.getSalesTransactionId()))
		{
			TransactionTool.updatePrintTimes (_oTR);
			PrintingLogTool.createPrintingLogFromSalesTransaction (_oTR, _sUserName);
		}
    }
    
    /**
     * 
     * @throws Exception
     */
    private void prepareContext()
    	throws Exception
    {		        
        s_PDF_TPL_NAME  = "/print/InvoicePDF.vm";
        s_TXT_TPL_NAME  = "/print/InvoiceTXT.vm";
        s_HTML_TPL_NAME = "/print/InvoiceHTML.vm";
        
    	oCtx.put("invoicemessage", InvoiceMessageTool.getInstance());    	
        if (oTR != null)
        {
        	oCtx.put("invoice_msg",  CustomerTool.getInvoiceMessageByID (oTR.getCustomerId()));
        }
        
    	Context oGlobal = TurbinePull.getGlobalContext();		
    	for (int i = 0; i < oGlobal.getKeys().length; i++)
    	{
    		String sKey = (String) oGlobal.getKeys()[i];
    		oCtx.put(sKey, oGlobal.get(sKey));
    	}
    }
}
