package com.ssti.enterprise.pos.print.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.4  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/12/20 13:48:42  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/07/29 05:11:05  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class MemoPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
    String s_PDF_TPL_NAME = "/print/MemoPDF.vm";
    String s_TXT_TPL_NAME = "/print/MemoTXT.vm";
    String s_HTML_TPL_NAME = "/print/MemoHTML.vm";
   
    String s_CM = "1";
    String s_DM = "2";
    
	private Object oTR = null;

	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
    	int iType = 1;    			
		oTR = CreditMemoTool.getCreditMemoByID(sID);
		if(oTR == null)
		{
			iType = 2;
			oTR = DebitMemoTool.getDebitMemoByID(sID);			
		}		
		
        oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put("type", iType);
        checkTemplate (oParam);

        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }           
        return oWriter.toString();
    }

    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
    	return getPrintedText (oParam, _iPrintType, _oSession);
    }    
}
