package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseReceiptPrintHelper.java,v 1.15 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseReceiptPrintHelper.java,v $
 * Revision 1.15  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/12/20 13:48:42  albert
 * *** empty log message ***
 *
 * Revision 1.13  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.12  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PurchaseReceiptPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{       
	private PurchaseReceipt oTR = null;
	private List vTD = null;
	
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		
		oTR = PurchaseReceiptTool.getHeaderByID (sID);
		vTD = PurchaseReceiptTool.getDetailsByID (sID);
				
        oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        
        prepareContext();
        checkTemplate (oParam);
        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        return oWriter.toString();
    }
   	
   	/**
	 * get Printed Text (use detail per page)
	 */
	 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);

		if (_oSession != null)
		{
			oTR = (PurchaseReceipt) _oSession.getAttribute(AppAttributes.s_PR);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_PRD);
		}
		//get transaction data by id		
		else if (_oSession == null || oTR == null) 
		{
			oTR = PurchaseReceiptTool.getHeaderByID (sID);
			vTD = PurchaseReceiptTool.getDetailsByID (sID);
		}
        prepareContext();
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }
    
    private void prepareContext()
    {
        s_PDF_TPL_NAME = "/print/PurchaseReceiptPDF.vm";
        s_TXT_TPL_NAME = "/print/PurchaseReceiptTXT.vm";
        s_HTML_TPL_NAME = "/print/PurchaseReceiptHTML.vm";    
    }
   
}
