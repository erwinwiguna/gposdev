package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseReturnPrintHelper.java,v 1.13 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseReturnPrintHelper.java,v $
 * Revision 1.13  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.12  2007/12/20 13:48:43  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PurchaseReturnPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{        
    private static final String s_PARAM_ID = "id";
    private static final String s_PARAM_USER = "user_name";
   
	private PurchaseReturn oTR = null;
	private List vTD = null;
	
	/**
	 * get Printed Text (normal)
	 */	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		
		oTR = PurchaseReturnTool.getHeaderByID (sID);
		vTD = PurchaseReturnTool.getDetailsByID (sID);
		
		oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        
        prepareContext();
        checkTemplate (oParam);
        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
                
        return oWriter.toString();
    }
   	
   	/**
	 * get Printed Text (use detail per page)
	 */
	 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);

		if (_oSession != null)
		{
			oTR = (PurchaseReturn) _oSession.getAttribute(AppAttributes.s_PRT);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_PRTD);
		}
		//get transaction data by id		
		else if (_oSession == null || oTR == null) 
		{
			oTR = PurchaseReturnTool.getHeaderByID (sID);
			vTD = PurchaseReturnTool.getDetailsByID (sID);
		}
        prepareContext();
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }
    
    private void prepareContext()
    {
        s_PDF_TPL_NAME = "/print/PurchaseReturnPDF.vm";
        s_TXT_TPL_NAME = "/print/PurchaseReturnTXT.vm";
        s_HTML_TPL_NAME = "/print/PurchaseReturnHTML.vm";
    }
}
