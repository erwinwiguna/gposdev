package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SyncPrintHelper.java,v 1.3 2007/09/17 07:36:10 albert Exp $ <br>
 *
 * <pre>
 * $Log: SyncPrintHelper.java,v $
 * Revision 1.3  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SyncPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
	private static Log log = LogFactory.getLog(SyncPrintHelper.class);
		   
	private SalesTransaction oTR = null;
	private List vTD = null;
    private static final String s_TXT_TPL_NAME = "/print/Empty.vm";
		
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {		        
        Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        return oWriter.toString();
    }
 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
        oCtx.put("content", _oSession.getAttribute("synccontent"));
    	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        return oWriter.toString();
    }    
}
