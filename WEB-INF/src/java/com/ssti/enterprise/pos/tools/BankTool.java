package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.security.PermissionSet;

import com.ssti.enterprise.pos.manager.BankManager;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankTool.java,v 1.16 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: BankTool.java,v $
 *    
 * </pre><br>
 */
public class BankTool extends BaseTool
{
	//static methods
	public static List getAllBank()
		throws Exception
	{
		return BankManager.getInstance().getAllBank();
	}

	public static Bank getBankByID(String _sID)
    	throws Exception
    {
		return BankManager.getInstance().getBankByID(_sID, null);
	}

	public static Bank getBankByID(String _sID, Connection _oConn)
		throws Exception
	{
		return BankManager.getInstance().getBankByID(_sID, _oConn);
	}

	public static List getBankByLocationID(String _sLocID)
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sLocID))
		{
			return BankManager.getInstance().getBankByLocationID(_sLocID);
		}
		return BankManager.getInstance().getAllBank();
	}
	
	public static Bank getBankByCode(String _sCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(BankPeer.BANK_CODE, _sCode);
	    List vData = BankPeer.doSelect(oCrit);
	    if (vData.size() > 0) {
			return (Bank) vData.get(0);
		}
		return null;
	}
	
	public static String getBankCurrencyID(String _sID)
    	throws Exception
    {
		Bank oBank = getBankByID (_sID);
		if (oBank != null)
		{
			return oBank.getCurrencyId();
		}
		return "";
	}

	public static String getCodeByID(String _sID)
		throws Exception
	{
		Bank oBank = getBankByID (_sID);
		if (oBank != null)
		{
			return oBank.getBankCode();
		}
		return "";
	}
	
	public static String getBankNameByID(String _sID)
    	throws Exception
    {
		Bank oBank = getBankByID (_sID);
		if (oBank != null)
		{
			return oBank.getDescription();
		}
		return "";
	}

	public static List getBankByCurrencyID(String _sCurrencyID)
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sCurrencyID))
		{
			return BankManager.getInstance().getBankByCurrencyID(_sCurrencyID);
		}
		return getAllBank();
	}
    
	public static String createJSArray () 
		throws Exception
	{	
	 	List vData = getAllBank();

	 	StringBuilder oSB = new StringBuilder ();
		oSB.append ("var aCurrId = new Array (");
		oSB.append (vData.size());
		oSB.append (");\n");
		oSB.append ("var aCurrRate = new Array (");
		oSB.append (vData.size());
		oSB.append (");\n");

	
	 	for (int i = 0; i < vData.size(); i++)
	 	{
		 	Bank oField = (Bank) vData.get(i);
			oSB.append ("aCurrId[");
			oSB.append (i);
			oSB.append ("] = new Array ('");
			oSB.append (oField.getCurrencyId());
			oSB.append ("');\n");
			
			oSB.append ("aCurrRate[");
			oSB.append (i);
			oSB.append ("] = new Array ('");
			oSB.append (CurrencyTool.getCurrentRateByID(oField.getCurrencyId(),null));
			oSB.append ("');\n");

	 	}
	 	return oSB.toString();
	}
	
	public static List getByUserName(String _sUserName, PermissionSet _vPerms)
		throws Exception
	{
		List vBank = new ArrayList();
		if (_vPerms != null && _vPerms.containsName("Access All Site"))
		{
			vBank = getAllBank();
		}
		else
		{
			List vLoc = LocationTool.getByUserName(_sUserName, _vPerms);
			if (vLoc != null && vLoc.size() > 0)
			{
				for (int i = 0; i < vLoc.size(); i++)
				{
					Location oLoc = (Location) vLoc.get(i);
					List vBankLoc = getBankByLocationID(oLoc.getLocationId());
					vBank.addAll(vBankLoc);
				}
			}
		}
		return vBank;
	}
}