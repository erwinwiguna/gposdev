package com.ssti.enterprise.pos.tools;

import java.io.StringWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.turbine.util.RunData;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.BarcodePaper;
import com.ssti.enterprise.pos.om.BarcodePaperPeer;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.CustomFormatter;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BarcodePaperTool.java,v 1.12 2007/12/20 13:47:44 albert Exp $ <br>
 *
 * <pre>
 * $Log: BarcodePaperTool.java,v $
 * Revision 1.12  2007/12/20 13:47:44  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class BarcodePaperTool extends BaseTool 
{
    private static final String s_TPL_NAME = "/util/BarcodeTemplate.vm";
    private static final CustomFormatter FORMATTER = new CustomFormatter();

    public static List getAllPaper(int _iType)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if (_iType > 0)
		{
			oCrit.add(BarcodePaperPeer.PAPER_TYPE,_iType);
		}
		return BarcodePaperPeer.doSelect(oCrit);
    }
    
	public static List getAllBarcodePaper()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BarcodePaperPeer.PAPER_TYPE,BarcodePaper.i_BARCODE);
		return BarcodePaperPeer.doSelect(oCrit);
    }
	public static BarcodePaper getBarcodePaperByID(String _sID) 
    	throws Exception
    {
		return getBarcodePaperByID(_sID, null);
    }
	
	public static BarcodePaper getBarcodePaperByID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BarcodePaperPeer.BARCODE_PAPER_ID, _sID);
        List vData = BarcodePaperPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (BarcodePaper) vData.get(0);
		}
		return null;
	}

	public static BarcodePaper getDefaultBarcode() 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BarcodePaperPeer.PAPER_TYPE, BarcodePaper.i_BARCODE);        
		oCrit.add(BarcodePaperPeer.IS_DEFAULT, true);
        List vData = BarcodePaperPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (BarcodePaper) vData.get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param data
	 * @param vItem
	 * @param _oPaper
	 * @param _iTransType
	 * @return parsed barcode template string
	 * @throws Exception
	 */
	
	public static String displayBarcode(RunData data, 
			                          	List vItem,
										BarcodePaper _oPaper,
										int _iTransType)
		throws Exception
	{	    	    
	    StringBuilder oSB = new StringBuilder();

		List vBarcode = new ArrayList();
		
	    int iRow = _oPaper.getPaperRow();
	    int iCol = _oPaper.getPaperColumn();
	    int iTotal = iRow * iCol; 
	
	    Item oItem = null;
	    int iQty = 0;
	    int iTotalQty = 0;
	    
        VelocityContext oCtx = new VelocityContext();
        oCtx.put("bcd", _oPaper);
        oCtx.put("data", data);          
        oCtx.put("cformat", FORMATTER);          
        
	    Context oGlobal = TurbinePull.getGlobalContext();	
		for (int i = 0; i < oGlobal.getKeys().length; i++)
		{
			String sKey = (String) oGlobal.getKeys()[i];
			oCtx.put(sKey, oGlobal.get(sKey));
		}		
        TurbinePull.populateContext(oCtx, data);
	    
	    for(int i = 0; i < vItem.size(); i++)
	    {
	        if(_iTransType == 1)  //if from purchase receipt
	        {
	            oItem = ItemTool.getItemByID( ((PurchaseReceiptDetail)vItem.get(i)).getItemId() );
	            iQty = ((PurchaseReceiptDetail)vItem.get(i)).getQty().intValue();
	        }
	        else if(_iTransType == 2) //if from purchase invoice
	        {
	            oItem = ItemTool.getItemByID( ((PurchaseInvoiceDetail)vItem.get(i)).getItemId() );
	            iQty = ((PurchaseInvoiceDetail)vItem.get(i)).getQty().intValue();
	        }	        
   	        else if(_iTransType == 3) //if from item list report
	        {
	            oItem = ItemTool.getItemByID( ((ItemList)vItem.get(i)).getItemId() );
	            iQty = ((ItemList)vItem.get(i)).getQty().intValue();
	        }
	        else if(_iTransType == 4) //if from issue receipt
	        {
	            oItem = ItemTool.getItemByID( ((IssueReceiptDetail)vItem.get(i)).getItemId() );
	            iQty = ((IssueReceiptDetail)vItem.get(i)).getQtyChanges().intValue();
	        }
	        else if(_iTransType == 5) //if from item transfer
	        {
	            oItem = ItemTool.getItemByID( ((ItemTransferDetail)vItem.get(i)).getItemId() );
	            iQty = ((ItemTransferDetail)vItem.get(i)).getQtyChanges().intValue();
	        }
	        else if(_iTransType == 6) //if from item setup
	        {
	            oItem = (Item) vItem.get(i);
	            if (data.getParameters().getInt("BarcodePerItem") < 1)
	            {
	            	iQty = InventoryLocationTool.getInventoryOnHand
						(oItem.getItemId(), PreferenceTool.getLocationID()).intValue();
	            }
	            else
	            {
	            	iQty = data.getParameters().getInt("BarcodePerItem");
	            }
	        }
	        for (int j = 1; j <= iQty; j++) {vBarcode.add(oItem);}
	        iTotalQty += iQty;
	    }
	    
	    int iTotalPage = iTotalQty / iTotal;
	    if (iTotalQty % iTotal > 0)  iTotalPage++;
	    int iTotalBarcode = vBarcode.size();

	    log.debug(" iTotalQty = " + iTotalQty);
	    log.debug(" iTotalPage = " + iTotalPage);
	    log.debug(" iTotalBarcode = " + iTotalBarcode);
	    
	    int iFrom = 0;
	    int iTo = 0;
	    for (int iPage = 1; iPage <= iTotalPage; iPage++)
	    {
	    	if (iPage > 1) iFrom = (iPage - 1) * iTotal;
	    	iTo = iPage * iTotal;
	    	if (iTo > iTotalBarcode) iTo = iTotalBarcode;

	    	log.debug(" page = " + iPage + ", from = " + iFrom + " to = " + iTo);
		    
	    	List vPageBarcode = vBarcode.subList(iFrom, iTo);

	        oCtx.put("result", vPageBarcode);            
				        		        
		    StringWriter oWriter = new StringWriter ();
		    Velocity.mergeTemplate(s_TPL_NAME, s_DEFAULT_ENCODING, oCtx, oWriter );
	        oSB.append(oWriter.toString());		  	    	
	    }	    
		return oSB.toString();
	}
	
	//-------------------------------------------------------------------------
	public static BarcodePaper getDefaultPaper(int _iType) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BarcodePaperPeer.PAPER_TYPE, _iType);        
		oCrit.add(BarcodePaperPeer.IS_DEFAULT, true);
        List vData = BarcodePaperPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (BarcodePaper) vData.get(0);
		}
		return null;
	}

	public static List getAllShelvingPaper()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BarcodePaperPeer.PAPER_TYPE,BarcodePaper.i_SHELVING);
		return BarcodePaperPeer.doSelect(oCrit);
    }
	
	public static BarcodePaper getDefaultShelving() 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BarcodePaperPeer.PAPER_TYPE, BarcodePaper.i_SHELVING);        
		oCrit.add(BarcodePaperPeer.IS_DEFAULT, true);
        List vData = BarcodePaperPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (BarcodePaper) vData.get(0);
		}
		return null;
	}
	
	//--------------------------------------------------------------------------
	public static String display(int _iType, RunData data,  List vItem, BarcodePaper _oPaper, int _iTransType, int _iFromList)
		throws Exception
	{
		if(_iType == BarcodePaper.i_BARCODE)
		{
			return displayBarcode(data, vItem, _oPaper, _iTransType);
		}
		if (_iType == BarcodePaper.i_SHELVING)
		{			
			return displayShelving(data, vItem, _oPaper, _iFromList);			
		}
		return "";
	}
	
	protected static final String s_SHELVING_TPL = "/util/ItemShelvingTemplate.vm";
	
    /**
     * 
     * @param _iRow
     * @param _iCol
     * @param _vItem
     * @param _oPaper
     * @param _iFromList
     * @return parsed template string 
     * @throws Exception
     */
    public static String displayShelving(RunData data,
										 List _vItem,
										 BarcodePaper _oPaper, 
										 int _iFromList)
	    throws Exception
	{
	    StringBuilder oSB = new StringBuilder();
	    int itemQty = 0;
	    int iRow = _oPaper.getPaperRow();
	    int iCol = _oPaper.getPaperColumn();
	    int iColCounter = 1;
	    int iRowCounter = 0;	    
	    
	    Context oGlobal = TurbinePull.getGlobalContext();
	    VelocityContext oCtx = new VelocityContext();        
        oCtx.put("bcd", _oPaper);
        oCtx.put("data", data);          
        oCtx.put("cformat", FORMATTER);	
		for (int j = 0; j < oGlobal.getKeys().length; j++)
		{
			String sKey = (String) oGlobal.getKeys()[j];
			oCtx.put(sKey, oGlobal.get(sKey));
		}		
        TurbinePull.populateContext(oCtx, data);
	    
        List vResult = new ArrayList(_vItem.size());
        if(_iFromList == 0)
        {
        	vResult = _vItem;
        }
        else if(_iFromList == 1)
        {        	
    	    for(int i = 0; i < _vItem.size(); i++)
    	    {
    	    	ItemList oIL = (ItemList) _vItem.get(i);
    	    	Item oItem = oIL.getItem();
    	    	int iQty = oIL.getQty().intValue();
    	    	for (int j = 0; j < iQty; j++)
    	    	{
    	    		vResult.add(oItem);
    	    	}
    	    }
        }
        
	    for(int i = 0; i < vResult.size(); i++)
	    {
	    	Item oItem = (Item) vResult.get(i);
                        
            if(iRowCounter == 0)
            {
                oSB.append("<table class='mainTable'>").append(s_LINE_SEPARATOR);
                iRowCounter = 1;
            }
            if(iColCounter == 1 || iColCounter>iCol)
            {
                oSB.append("<tr>").append(s_LINE_SEPARATOR);
                iColCounter = 1;
            }               
            oCtx.put("itm", oItem);                         	        	       	
            
            StringWriter oWriter = new StringWriter ();
            Velocity.mergeTemplate(s_SHELVING_TPL, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
            
            oSB.append (oWriter.toString());
            oSB.append(s_LINE_SEPARATOR);
                        
            if(i == vResult.size() - 1)
            {
                if(iColCounter < iCol)
                {
                    for(int j = 0; j < iCol-iColCounter; j++)
                    {
                        oSB.append("<td class='mainCol'>&nbsp;</td>");                        
                        oSB.append(s_LINE_SEPARATOR);
                    }
                }
            }
            iColCounter = iColCounter+1;
            if(iColCounter > iCol)
            {
                oSB.append("</tr>").append(s_LINE_SEPARATOR);
                iRowCounter = iRowCounter+1;
            }
            if(iRowCounter > iRow)
            {
                oSB.append("</table>").append(s_LINE_SEPARATOR);
                oSB.append("<p class=\"pagebreakhere\"/><br>\n");
                iRowCounter = 0;
            }
	   }
	   return oSB.toString();
    }
}
