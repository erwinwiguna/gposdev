package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerAddress;
import com.ssti.enterprise.pos.om.CustomerAddressPeer;
import com.ssti.enterprise.pos.om.CustomerBalancePeer;
import com.ssti.enterprise.pos.om.CustomerCfield;
import com.ssti.enterprise.pos.om.CustomerContact;
import com.ssti.enterprise.pos.om.CustomerContactPeer;
import com.ssti.enterprise.pos.om.CustomerField;
import com.ssti.enterprise.pos.om.CustomerFieldPeer;
import com.ssti.enterprise.pos.om.CustomerFollowup;
import com.ssti.enterprise.pos.om.CustomerFollowupPeer;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.CustomerTypePeer;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomerTool.java,v 1.30 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: CustomerTool.java,v $
 * 
 * 2018-02-28
 * - Activate IS_ACTIVE column in queries
 * - change buildFindCriteria add param int status (< 0 or 1 Active, 2 Non Active) 
 * - add Prospect Related Method
 * 
 * 2017-08-25
 * - add method getFieldByName to get CustomerField by CustomerId & FieldName
 * 
 * 2016-10-13
 * - Change query method getNewStoreCustomer, include update date 
 * - Old method oCrit.add(CustomerPeer.ADD_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);    
 * - New method
 * - Criteria.Criterion oNew  = oCrit.getNewCriterion(CustomerPeer.ADD_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
 * - Criteria.Criterion oUpdated  = oCrit.getNewCriterion(CustomerPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
 * - oCrit.add(oNew.or(oUpdated));		
 * 
 * 2016-05-28
 * - add findByEmail
 * 
 * 2016-01-03
 * - change buildFindCriteria add param statusId
 * - change List findData add param statusId
 * - change LargeSelect find add param empId and statusId
 * 
 * 2015-12-08
 * - add CustomerStatus table
 * - addField isActive in Customer
 * - change method to filter active only customer
 * 
 * 2015-10-17
 * - change buildFindCriteria add condition 20 (birthday), add method in SqlUtil.setBirthDayCriteria
 * - add condition 99 (custom field) keywords FieldName,%value%
 * - birthday format supported: dd-dd/mm 10-20/01 
 * - changed templates: CustomerView.vm, CustomerDataReport.vm 
 * 
 * 2015-09-17
 * - add CustomerCfield Table and CustomerField Table methods
 * 
 * 2015-09-15
 * - change buildFindCriteria filter by defaultEmployeeID
 * 
 * </pre><br>
 */
public class CustomerTool extends BaseTool 
{
	public static List getAllCustomer()
    	throws Exception
    {
		return CustomerManager.getInstance().getAllCustomer();
    }

	public static Customer getCustomerByID(String _sID)
    	throws Exception
    {
		return CustomerManager.getInstance().getCustomerByID(_sID, null);
	}

	public static Customer getCustomerByID(String _sID, Connection _oConn)
		throws Exception
	{
		return CustomerManager.getInstance().getCustomerByID(_sID, _oConn);
	}

	public static Customer getCustomerByCode(String _sCode) 
		throws Exception
	{
		return getCustomerByCode(_sCode, null);
	}

	public static Customer getCustomerByCode(String _sCode, Connection _oConn) 
		throws Exception
	{
		return CustomerManager.getInstance().getCustomerByCode(_sCode, _oConn);	}
	
	public static List getCustomerByCode(String _sCode, boolean _bPreffix, boolean _bSuffix) 
		throws Exception
	{
		Criteria oCrit = new Criteria();		
        oCrit.add(CustomerPeer.CUSTOMER_CODE, 
          (Object) SqlUtil.like (CustomerPeer.CUSTOMER_CODE, _sCode, _bSuffix, _bPreffix), Criteria.CUSTOM );
        oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_CODE);
        return CustomerPeer.doSelect(oCrit);
	}	
	
	public static Customer getCustomerByName(String _sCustomerName)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (CustomerPeer.CUSTOMER_NAME, _sCustomerName);
		List vData = CustomerPeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return (Customer) vData.get(0);
		}
		return null;
	}
	
	public static List getCustomerByName(String _sCustomerName, boolean _bPreffix, boolean _bSuffix)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(CustomerPeer.CUSTOMER_NAME, 
            (Object) SqlUtil.like (CustomerPeer.CUSTOMER_NAME, _sCustomerName, _bSuffix, _bPreffix), Criteria.CUSTOM );
        oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_NAME);
        return CustomerPeer.doSelect(oCrit);
	}

	public static Customer getCustomerByEmail(String _sEmail) 
		throws Exception
	{
		Criteria oCrit = new Criteria();		
        oCrit.add(CustomerPeer.EMAIL, _sEmail);
        List vData = CustomerPeer.doSelect(oCrit);
        if(vData.size() > 0)
        {
        	return (Customer) vData.get(0);
        }
        return null;
	}	
	
	public static String getCustomerIDByName (String _sCustomerName)
    	throws Exception
    {
		Customer oCust = getCustomerByName (_sCustomerName);
		if (oCust != null)
		{
			return oCust.getCustomerId();
		}
		return "";
	}

	public static String getCodeByID(String _sID)
		throws Exception
	{
		return getCustomerCodeByID(_sID);
	}
	
	public static String getCustomerCodeByID(String _sID)
		throws Exception
	{
		Customer oCust = getCustomerByID (_sID);
		if (oCust != null)
		{
			return oCust.getCustomerCode();
		}
		return "";
	}
	
	public static String getCustomerNameByID(String _sID)
    	throws Exception
    {
		Customer oCust = getCustomerByID (_sID);
		if (oCust != null)
		{
			return oCust.getCustomerName();
		}
		return "";
	}

	public static String getAddressByID(String _sID)
    	throws Exception
    {
		Customer oCust = getCustomerByID (_sID);
		if (oCust != null)
		{
			return oCust.getAddress();
		}
		return "";
	}
		
	public static String getInvoiceMessageByID(String _sID)
    	throws Exception
    {
		Customer oCust = getCustomerByID (_sID);
		if (oCust != null)
		{
			return oCust.getInvoiceMessage();
		}
		return "";
	}
	
	public static String getCustomerTypeByID(String _sID)
		throws Exception
	{
		Customer oCust = getCustomerByID (_sID);
		if (oCust != null)
		{
			return oCust.getCustomerTypeId();
		}
		return "";
	}

	public static String getDefaultTermByID(String _sID, Connection _oConn)
		throws Exception
	{
		Customer oCust = getCustomerByID (_sID, _oConn);
		if (oCust != null)
		{
			return oCust.getDefaultTermId();
		}
		return "";
	}
	public static BigDecimal getCreditLimitByID(String _sID)
    	throws Exception
    {
		Customer oCustomer = getCustomerByID(_sID);
		if (oCustomer != null) {
			return oCustomer.getCreditLimit();
		}
		return bd_ZERO;
	}
	
	public static Customer getDefaultCustomer()
    	throws Exception
    {
		return CustomerManager.getInstance().getDefaultCustomer();
	}

	public static List getCustomerByType(String _sTypeID, String _sTypeCode, String _sTypeDesc)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sTypeID)) oCrit.add(CustomerPeer.CUSTOMER_TYPE_ID, _sTypeID);

		if (StringUtil.isNotEmpty(_sTypeCode)) 
		{
			oCrit.addJoin(CustomerPeer.CUSTOMER_TYPE_ID, CustomerTypePeer.CUSTOMER_TYPE_ID);
			oCrit.add(CustomerTypePeer.CUSTOMER_TYPE_CODE, _sTypeCode);
		}
		if (StringUtil.isNotEmpty(_sTypeDesc)) 
		{
			oCrit.addJoin(CustomerPeer.CUSTOMER_TYPE_ID, CustomerTypePeer.CUSTOMER_TYPE_ID);
			oCrit.add(CustomerTypePeer.DESCRIPTION, 
					(Object) SqlUtil.like (CustomerTypePeer.DESCRIPTION, _sTypeDesc), Criteria.CUSTOM);
		}
		oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_NAME);
		return CustomerPeer.doSelect(oCrit);
	}
	
	private static Criteria buildFindCriteria(int _iCondition, 
											  int _iBalance, 
											  int _iSortBy, 
											  String _sKeywords, 
											  String _sTypeID,                                              
											  String _sCurrencyID,
                                              String _sLocID,
                                              String _sSalesAreaID,
                                              String _sTerritoryID,
                                              String _sEmpID,
                                              String _sStatusID,
                                              int _iStatus)
        throws Exception                                              
	{		
		Criteria oCrit = new Criteria();
		if (!StringUtil.empty(_sKeywords))
		{
			if (_iCondition == 1)
			{
				oCrit.add(CustomerPeer.CUSTOMER_NAME,
					(Object) SqlUtil.like (CustomerPeer.CUSTOMER_NAME, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 2)
			{
				oCrit.add(CustomerPeer.CUSTOMER_CODE,
					(Object) SqlUtil.like (CustomerPeer.CUSTOMER_CODE, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 3)
			{
				oCrit.addJoin(CustomerPeer.CUSTOMER_TYPE_ID, CustomerTypePeer.CUSTOMER_TYPE_ID);
				oCrit.add(CustomerTypePeer.DESCRIPTION,
					(Object) SqlUtil.like (CustomerTypePeer.DESCRIPTION, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 4)
			{
				oCrit.add(CustomerPeer.ADDRESS,(Object) _sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 5)
			{
				oCrit.addJoin(CustomerPeer.DEFAULT_EMPLOYEE_ID, EmployeePeer.EMPLOYEE_ID);
				oCrit.add(EmployeePeer.EMPLOYEE_NAME, (Object) _sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 6)
			{
				oCrit.add(CustomerPeer.CITY, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 7)
			{
				oCrit.add(CustomerPeer.PROVINCE, (Object)_sKeywords, Criteria.ILIKE);
			}			
			else if (_iCondition == 8)
			{
				oCrit.add(CustomerPeer.PHONE1, (Object)_sKeywords, Criteria.ILIKE);
			}			
			else if (_iCondition == 9)
			{
				oCrit.add(CustomerPeer.PHONE2, (Object)_sKeywords, Criteria.ILIKE);
			}
            else if (_iCondition == 10)
            {
                oCrit.addJoin(CustomerPeer.DEFAULT_LOCATION_ID, LocationPeer.LOCATION_ID);                
                Criteria.Criterion oLoc1  = oCrit.getNewCriterion(LocationPeer.LOCATION_CODE, (Object)_sKeywords, Criteria.ILIKE);
                Criteria.Criterion oLoc2  = oCrit.getNewCriterion(LocationPeer.LOCATION_NAME, (Object)_sKeywords, Criteria.ILIKE);
                oCrit.add(oLoc1.or(oLoc2));
            }
            else if (_iCondition == 20) //birthday
            {
            	SqlUtil.setBirthDayCriteria(oCrit,CustomerPeer.BIRTH_DATE,_sKeywords);
            }	
            else if (_iCondition == 21) //regdate
            {
            	String s[] = StringUtil.split(_sKeywords,",");
            	if(s != null && s.length > 0)
            	{
            		oCrit.add(CustomerPeer.ADD_DATE, DateUtil.parseStartDate(s[0]), Criteria.GREATER_EQUAL);
            	}
            	if(s != null && s.length > 1)
            	{
            		oCrit.and(CustomerPeer.ADD_DATE, DateUtil.parseEndDate(s[1]), Criteria.LESS_EQUAL);
            	}            	
            }	
            else if (_iCondition == 22) //update
            {
            	String s[] = StringUtil.split(_sKeywords,",");
            	if(s != null && s.length > 0)
            	{
            		oCrit.add(CustomerPeer.UPDATE_DATE, DateUtil.parseStartDate(s[0]), Criteria.GREATER_EQUAL);
            	}
            	if(s != null && s.length > 1)
            	{
            		oCrit.and(CustomerPeer.UPDATE_DATE, DateUtil.parseEndDate(s[1]), Criteria.LESS_EQUAL);
            	}            	
            }	
			
            else if (_iCondition == 99) //custom field
            {
				List vData = StringUtil.toList(_sKeywords, ",");
				log.debug("vData "  + vData  + vData.size());
				String sField = "";
				String sValue = "";
				if (vData.size() == 2) 
				{
					sField = (String) vData.get(0);
					sValue = (String) vData.get(1);
				}
				oCrit.addJoin(CustomerPeer.CUSTOMER_ID, CustomerFieldPeer.CUSTOMER_ID);
				oCrit.add(CustomerFieldPeer.FIELD_NAME, (Object)sField, Criteria.ILIKE);
				oCrit.add(CustomerFieldPeer.FIELD_VALUE, (Object)sValue, Criteria.ILIKE);
            }				
        }
		
		if (_iBalance > 0)
		{
			if (_iBalance == 1)
			{
				oCrit.addJoin(CustomerPeer.CUSTOMER_ID, CustomerBalancePeer.CUSTOMER_ID);
				oCrit.add(CustomerBalancePeer.AR_BALANCE, 0, Criteria.GREATER_THAN);				
			}
			if (_iBalance == 2)
			{
				oCrit.addJoin(CustomerPeer.CUSTOMER_ID, CustomerBalancePeer.CUSTOMER_ID);
				oCrit.add(CustomerBalancePeer.AR_BALANCE, 0, Criteria.LESS_THAN);				
			}
			if (_iBalance == 3)
			{
				oCrit.addJoin(CustomerPeer.CUSTOMER_ID, CustomerBalancePeer.CUSTOMER_ID);
				oCrit.add(CustomerBalancePeer.AR_BALANCE, 0);				
			}
			if (_iBalance == 4)
			{
				oCrit.addJoin(CustomerPeer.CUSTOMER_ID, CustomerBalancePeer.CUSTOMER_ID);
				oCrit.add(CustomerBalancePeer.AR_BALANCE, 0, Criteria.NOT_EQUAL);				
			}
			if (_iBalance == 5)
			{
				oCrit.addJoin(CustomerPeer.CUSTOMER_ID, CustomerBalancePeer.CUSTOMER_ID);
				oCrit.add(CustomerBalancePeer.AR_BALANCE, 
					(Object)"customer_balance.ar_balance > customer.credit_limit", Criteria.CUSTOM);				
			}
		}
		
		if (_iSortBy > 0)
		{
			if (_iSortBy == 1)
			{
				oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_NAME);
			}
			if (_iSortBy == 2)
			{
				oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_CODE);
			}			
		}
		else
		{
			//default sort
			oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_NAME);			
		}
		if (StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.add(CustomerPeer.CUSTOMER_TYPE_ID, _sTypeID);
		}
		if (StringUtil.isNotEmpty(_sCurrencyID))
		{
			oCrit.add(CustomerPeer.DEFAULT_CURRENCY_ID, _sCurrencyID);
		}
        if (StringUtil.isNotEmpty(_sLocID))
        {
            oCrit.add(CustomerPeer.DEFAULT_LOCATION_ID, _sLocID);
        }
		if (StringUtil.isNotEmpty(_sEmpID))  
		{
			List vEmp = EmployeeTool.getChildIDList(new ArrayList(), _sEmpID);
			vEmp.add(_sEmpID);
			oCrit.addIn(CustomerPeer.DEFAULT_EMPLOYEE_ID, vEmp);	
			oCrit.or(CustomerPeer.DEFAULT_EMPLOYEE_ID, "");				
		}
		if(_iStatus <= 0 || _iStatus == 1)
		{
			oCrit.add(CustomerPeer.IS_ACTIVE, true);
		}
		if(_iStatus == 2)
		{
			oCrit.add(CustomerPeer.IS_ACTIVE, false);
		}
		
		if (StringUtil.isNotEmpty(_sStatusID))  
		{
			oCrit.or(CustomerPeer.STATUS_ID, _sStatusID);				
		}		
		return oCrit;
	}
	
	public static List findData(int _iCondition, int _iBalance, String _sKeywords)
		throws Exception
	{
		return findData(_iCondition,_iBalance,_sKeywords,"");
	}

	public static List findData(int _iCondition, int _iBalance, String _sKeywords, String _sCurrencyID) 
		throws Exception
	{
		return findData(_iCondition,_iBalance, -1, _sKeywords, "", _sCurrencyID);		
	}

    public static List findData(int _iCondition, 
                                int _iBalance, 
                                int _iSortBy, 
                                String _sKeywords, 
                                String _sTypeID,
                                String _sCurrencyID)
        throws Exception
    {
        return findData(_iCondition,_iBalance,_iSortBy,_sKeywords,_sTypeID,_sCurrencyID,"");
    }
    
	public static List findData(int _iCondition, 
								int _iBalance, 
								int _iSortBy, 
								String _sKeywords, 
								String _sTypeID,
								String _sCurrencyID,
                                String _sLocID)
    	throws Exception
    {	
		return findData(_iCondition, _iBalance, _iSortBy, _sKeywords, _sTypeID, _sCurrencyID, _sLocID, "", "", "", "");
	}

	public static List findData(int _iCondition, 
								int _iBalance, 
								int _iSortBy, 
								String _sKeywords, 
								String _sTypeID,
								String _sCurrencyID,
								String _sLocID,
								String _sSalesAreaID,
								String _sTerritoryID,
								String _sEmployeeID,
								String _sStatusID)
        throws Exception
	{
		Criteria oCrit = buildFindCriteria(_iCondition, _iBalance, _iSortBy, 
				_sKeywords, _sTypeID, _sCurrencyID, _sLocID, _sSalesAreaID, _sTerritoryID, _sEmployeeID, _sStatusID, -1);
		if (StringUtil.isNotEmpty(_sKeywords) ||
			StringUtil.isNotEmpty(_sLocID) ||
			StringUtil.isNotEmpty(_sSalesAreaID) ||
			StringUtil.isNotEmpty(_sTerritoryID) ||			
			StringUtil.isNotEmpty(_sCurrencyID) || 
			StringUtil.isNotEmpty(_sTypeID) || 
			StringUtil.isNotEmpty(_sEmployeeID) ||
			StringUtil.isNotEmpty(_sStatusID) ||
			_iBalance > 0)
		{		
			log.debug(oCrit);
			return CustomerPeer.doSelect(oCrit);
		}
		return getAllCustomer();
	}
	
	public static LargeSelect find(int _iCondition, 
								   int _iBalance, 
								   int _iSortBy, 
								   String _sKeywords, 
								   String _sTypeID,
								   String _sCurrencyID,
                                   String _sLocID,
								   int _iViewLimit)
		throws Exception
	{
		return find(_iCondition, _iBalance, _iSortBy, _sKeywords, _sTypeID, _sCurrencyID, _sLocID, "", "", "", "", _iViewLimit);
	}

	public static LargeSelect find(int _iCondition, 
			   					   int _iBalance, 
			   					   int _iSortBy, 
			   					   String _sKeywords, 
			   					   String _sTypeID,
			   					   String _sCurrencyID,
			   					   String _sLocID,
			   					   String _sSalesAreaID,
			   					   String _sTerritoryID,
			   					   String _sEmpID,
			   					   String _sStatusID,
			   					   int _iViewLimit)
	throws Exception
	{
		Criteria oCrit = buildFindCriteria(_iCondition, _iBalance, _iSortBy, _sKeywords, _sTypeID, _sCurrencyID, _sLocID,  _sSalesAreaID, _sTerritoryID,  _sEmpID,  _sStatusID, -1);
		return new LargeSelect(oCrit, _iViewLimit, "com.ssti.enterprise.pos.om.CustomerPeer");
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 * @return updated customers since last ho 2 store
	 * @throws Exception
	 */
	public static List getUpdatedCustomers ()
		throws Exception
	{
		return getUpdatedCustomers(null);
	}
	
	public static List getUpdatedCustomers (Date _dLast)
		throws Exception
	{
		if (_dLast == null)
		{
			_dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //oneway		
		}
		Criteria oCrit = new Criteria();				
		if (_dLast != null) 
		{      
	    	oCrit.add(CustomerPeer.UPDATE_DATE, _dLast, Criteria.GREATER_EQUAL);        
		}
		return CustomerPeer.doSelect(oCrit);
	}
	
	/**
	 * get newly added customer in store used for sync
	 * 
	 * @return newly added customer in store 
	 * @throws Exception
	 */
	public static List getNewStoreCustomers()
		throws Exception
	{
		List vData = new ArrayList(1);
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);		
		//this will only happen on the first time of a store 2 ho sync process
		if (dLastSyncDate  == null) 
		{	      			
		}		
		if (dLastSyncDate != null) //only get newly added customer
		{
			Criteria oCrit = new Criteria();		
			oCrit.add(CustomerPeer.LAST_UPDATE_LOCATION_ID, PreferenceTool.getLocationID());  
			
			Criteria.Criterion oNew  = oCrit.getNewCriterion(CustomerPeer.ADD_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
			Criteria.Criterion oUpdated  = oCrit.getNewCriterion(CustomerPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
			oCrit.add(oNew.or(oUpdated));
			
			vData = CustomerPeer.doSelect(oCrit);						
		}
		return vData;
	}

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(CustomerPeer.class, CustomerPeer.CUSTOMER_ID, _sID, _bIsNext);
	}	  	

	public static String getCompleteAddress(Customer _oCust) 
		throws Exception
	{
		StringBuilder oSB = new StringBuilder("");
		if (_oCust != null)
		{
			if (StringUtil.isNotEmpty(_oCust.getAddress())) {oSB.append(_oCust.getAddress()); oSB.append("\n");}
			if (StringUtil.isNotEmpty(_oCust.getCity())) {oSB.append(_oCust.getCity()); oSB.append("\n");}
			if (StringUtil.isNotEmpty(_oCust.getProvince())) {oSB.append(_oCust.getProvince()); oSB.append(" ");}
			if (StringUtil.isNotEmpty(_oCust.getZip())) {oSB.append(_oCust.getZip()); } oSB.append("\n");
			if (StringUtil.isNotEmpty(_oCust.getCountry())) {oSB.append(_oCust.getCountry()); }
		}
		return oSB.toString();
	}	
	
	public static void validateCustomer (Customer _oCust)
		throws Exception 
	{
		if (_oCust != null)
		{
			Customer oExist = getCustomerByCode (_oCust.getCustomerCode());
			if (oExist != null && !oExist.getCustomerId().equals(_oCust.getCustomerId())) 
			{
				CustomerManager.getInstance().refreshCache(_oCust.getCustomerId(), _oCust.getCustomerCode());
				throw new Exception (" Customer with Code : " + _oCust.getCustomerCode() + " already Exist ");
			}
			
			/*
			oExist = getCustomerByName (_oCust.getCustomerName());
			if (oExist != null && !oExist.getCustomerId().equals(_oCust.getCustomerId())) 
			{
				CustomerManager.getInstance().refreshCache(_oCust.getCustomerId());
				throw new Exception (" Customer with Name : " + _oCust.getCustomerName() + " already Exist ");
			}
			*/
		}
	}    
	
	public static int getGender(String _sGender)
	{
		
		if (StringUtil.isNotEmpty(_sGender))
		{
			if(_sGender.equalsIgnoreCase(LocaleTool.getString("female")) || _sGender.equalsIgnoreCase("Female")) return 2;
			if(_sGender.equalsIgnoreCase(LocaleTool.getString("male")) || _sGender.equalsIgnoreCase("Male")) return 1;			
		}
		return 1;
	}

	public static String getGender(int _iGender)
	{
		if (_iGender == 2) return LocaleTool.getString("female");
		return LocaleTool.getString("male");
	}
	
	//-------------------------------------------------------------------------
	// Customer Contacts, Address & Followup Details
	//-------------------------------------------------------------------------
	
	public static List getContacts(String _sCustomerID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerContactPeer.CUSTOMER_ID, _sCustomerID);
		return CustomerContactPeer.doSelect(oCrit);
	}

	public static CustomerContact getContactByID(String _sContactID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerContactPeer.CUSTOMER_CONTACT_ID, _sContactID);
		List vData = CustomerContactPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (CustomerContact) vData.get(0);
		}
		return null;
	}
	
	public static List getAddresses(String _sCustomerID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerAddressPeer.CUSTOMER_ID, _sCustomerID);
		return CustomerAddressPeer.doSelect(oCrit);
	}
		
	public static CustomerAddress getCustomerAddressByID(String _sAddressID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerAddressPeer.CUSTOMER_ADDRESS_ID, _sAddressID);
		List vData = CustomerAddressPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (CustomerAddress) vData.get(0);
		}
		return null;
	}
	
	public static List getFollowups(String _sCustomerID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerFollowupPeer.CUSTOMER_ID, _sCustomerID);
		return CustomerFollowupPeer.doSelect(oCrit);
	}
		
	public static CustomerFollowup getFollowupByID(String _sFollowupID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerFollowupPeer.CUSTOMER_FOLLOWUP_ID, _sFollowupID);
		List vData = CustomerFollowupPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (CustomerFollowup) vData.get(0);
		}
		return null;
	}	
	
	public static String generateCode(String _sPrefix, int _iLength)
	{
		if (_iLength <= 0) _iLength = PreferenceTool.getCustCodeLength();
		return generateCode(_sPrefix, CustomerPeer.CUSTOMER_CODE, "customerCode", CustomerPeer.class, _iLength);
	}
    
    public static int getTotalCustomer()
        throws Exception
    {
        return SqlUtil.countRecords(CustomerPeer.TABLE_NAME);
    }
    
	//-------------------------------------------------------------------------
	// Customer Custom Field 
	//-------------------------------------------------------------------------
	public static CustomerCfield getCFieldByID(String _sID)
		throws Exception
	{
		return CustomerManager.getInstance().getCFieldByID(_sID, null);
	}

	public static List getCFields(String _sTypeID)
		throws Exception
	{
		return CustomerManager.getInstance().getCFields(_sTypeID);
	}
	
	//-------------------------------------------------------------------------
	// Customer Fields 
	//-------------------------------------------------------------------------
	public static List getFields(String _sCustomerID) throws Exception
	{
		return CustomerManager.getInstance().getFields(_sCustomerID);
	}

	public static CustomerField getField(String _sCustomerID, String _sCFieldID) throws Exception
	{
		List vData = getFields(_sCustomerID);
		if(vData.size() > 0)
		{
			for (int i = 0; i < vData.size(); i++)
			{
				CustomerField oField = (CustomerField) vData.get(i);
				if(StringUtil.isEqual(oField.getCfieldId(),_sCFieldID) )
				{
					return oField;
				}
			}
		}
		return null;	
	}	

	public static CustomerField getFieldByName(String _sCustomerID, String _sFieldName) throws Exception
	{
		List vData = getFields(_sCustomerID);
		if(vData.size() > 0)
		{
			for (int i = 0; i < vData.size(); i++)
			{
				CustomerField oField = (CustomerField) vData.get(i);
				if(StringUtil.equalsIgnoreCase(oField.getFieldName(),_sFieldName))
				{
					return oField;
				}
			}
		}
		return null;	
	}		
	
	public static Map getFieldMap(String _sCustomerID) throws Exception
	{
		List vData = getFields(_sCustomerID);
		if(vData.size() > 0)
		{
			Map mFields = new HashMap(vData.size());
			for (int i = 0; i < vData.size(); i++)
			{
				CustomerField oField = (CustomerField) vData.get(i);
				mFields.put(oField.getCfieldId(), oField.getFieldValue());		
			}
			return mFields;
		}
		return null;
	}	
	
	//-------------------------------------------------------------------------
	// Customer Fields 
	//-------------------------------------------------------------------------
	public static List getByParentID(String _sParentID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerPeer.PARENT_ID, _sParentID);
		return CustomerPeer.doSelect(oCrit);
	}		
}
