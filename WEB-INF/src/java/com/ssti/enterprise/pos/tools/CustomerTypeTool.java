package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.CustomerType;
import com.ssti.enterprise.pos.om.CustomerTypePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomerTypeTool.java,v 1.12 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: CustomerTypeTool.java,v $
 * Revision 1.12  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/08/22 05:34:36  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class CustomerTypeTool extends BaseTool 
{
	public static List getAllCustomerType()
		throws Exception
	{
		return CustomerManager.getInstance().getAllCustomerType();
	}
	
	public static CustomerType getCustomerTypeByID(String _sID)
		throws Exception
	{
		return CustomerManager.getInstance().getCustomerTypeByID(_sID, null);
	}	
	
	public static CustomerType getCustomerTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
		return CustomerManager.getInstance().getCustomerTypeByID(_sID, _oConn);
	}		

	public static String getCodeByID(String _sID)
		throws Exception
	{
		CustomerType oType = getCustomerTypeByID (_sID);
		if (oType != null)
	    {
			return oType.getCustomerTypeCode();
		}
	    return "";
	}	
	
	public static String getCustomerTypeCodeByID(String _sID)
		throws Exception
	{
		CustomerType oType = getCustomerTypeByID (_sID);
		if (oType != null)
	    {
			return oType.getCustomerTypeCode();
		}
	    return "";
	}	

	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		CustomerType oType = getCustomerTypeByID (_sID);
		if (oType != null)
	    {
			return oType.getDescription();
		}
	    return "";
	}	

	/**
	 * @param Code
	 * @return
	 */
	public static CustomerType getCustomerTypeByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (CustomerTypePeer.CUSTOMER_TYPE_CODE, _sCode);
		List vData = CustomerTypePeer.doSelect(oCrit);
		if (vData.size()>0)
		{
			return (CustomerType)vData.get(0);
		}
		return null;
	}
	
	/**
	 * @param Code
	 * @return
	 */
	public static String getIDByCode(String _sCode) 
		throws Exception
	{
		CustomerType oType = getCustomerTypeByCode(_sCode);
		if (oType != null)
		{
			return oType.getCustomerTypeId();
		}
		return "";
	}	
}
