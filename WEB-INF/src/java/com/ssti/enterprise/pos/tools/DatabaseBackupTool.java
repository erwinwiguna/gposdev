package com.ssti.enterprise.pos.tools;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.om.DatabaseBackup;
import com.ssti.enterprise.pos.om.DatabaseBackupPeer;
import com.ssti.framework.db.DBManager;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DatabaseBackupTool.java,v 1.12 2008/06/29 07:11:57 albert Exp $ <br>
 *
 * <pre>
 * $Log: DatabaseBackupTool.java,v $
 * 
 * 2015-09-15
 * - change backupDatabase, set year month properties from sysconfig BackupFilePref
 * - zip backup file add method in IOTool
 * 
 * </pre><br>
 */
public class DatabaseBackupTool implements AppAttributes
{
	private static Log log = LogFactory.getLog(DatabaseBackup.class); 

	static DatabaseBackupTool instance = null;
	
	public static synchronized DatabaseBackupTool getInstance() 
	{
		if (instance == null) instance = new DatabaseBackupTool();
		return instance;
	}	
	
	public static String[] backupDatabase(RunData data)
    	throws Exception
    {
		String sDBName = PreferenceTool.getCurrentDatabase().toLowerCase();
		Date dNow = new Date();
		String sYr = CustomFormatter.formatCustomDate(dNow, "yy");		
		String sMo = CustomFormatter.formatCustomDate(dNow, "MM");
		String sDy = CustomFormatter.formatCustomDate(dNow, "dd");
		
		String sBakFile = PreferenceTool.getSysConfig().getBackupFilePref();
		if(StringUtil.containsIgnoreCase(sBakFile, "yy")) sBakFile = StringUtil.replace(sBakFile, "yy", sYr);
		if(StringUtil.containsIgnoreCase(sBakFile, "mm")) sBakFile = StringUtil.replace(sBakFile, "mm", sMo);
		if(StringUtil.containsIgnoreCase(sBakFile, "dd")) sBakFile = StringUtil.replace(sBakFile, "dd", sDy);
				
		StringBuilder oOutFile = new StringBuilder ();
		oOutFile.append(PreferenceTool.getSysConfig().getBackupOutputDir());
		oOutFile.append(sBakFile);		
		oOutFile.append(".dat");
		
		String sOutFile = oOutFile.toString();
		String sZipFile = sOutFile.replace(".dat", ".zip");
		DBManager oDBM = new DBManager (DBManager.i_BACKUP, s_DB_TYPE, sDBName, sOutFile, "");    	    	
    	String[] aResult =  { oDBM.getResult(), sZipFile };
    	
    	if(StringUtil.containsIgnoreCase(oDBM.getResult(), "successful"))
    	{
    		//create zip
    		IOTool.zipFile(sOutFile, sZipFile, true);
    	}
    	createDBBackupEntry (data, sZipFile);
    	return aResult;
    }

	public static String restoreDatabase(RunData data)
    	throws Exception
    {
		String sFileName = data.getParameters().getString("DataFileLocation");		
     	String sDBName = PreferenceTool.getCurrentDatabase();						
    	DBManager oDBManager = new DBManager (DBManager.i_RESTORE, s_DB_TYPE, sDBName, sFileName, "");
    	return oDBManager.getResult();
    }
	
	public static Date getLastBackupDate()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.addDescendingOrderByColumn (DatabaseBackupPeer.BACKUP_DATE);
		oCrit.setLimit (1);
		List vData = DatabaseBackupPeer.doSelect(oCrit);
    	if (vData.size() > 0){
    		return ((DatabaseBackup)vData.get(0)).getBackupDate();
    	}
    	return null;
    }
	
	public static void createDBBackupEntry (RunData data, String _sFileName)
    	throws Exception
    {
		DatabaseBackup oDB = new DatabaseBackup ();
		oDB.setDatabaseBackupId   (IDGenerator.generateSysID());	
		oDB.setBackupDate         (new Date());
		oDB.setLocationId     	  (PreferenceTool.getLocationID());
		oDB.setLocationName   	  (PreferenceTool.getLocationName());
		oDB.setFileName 		  (_sFileName);		
		oDB.setUserName			  (data.getUser().getName());
		oDB.setDescription		  (data.getParameters().getString("Description",""));		
		oDB.save();		
	}
	
	public static List getBackupList (int _iLimit)
			throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addDescendingOrderByColumn(DatabaseBackupPeer.BACKUP_DATE);
		if(_iLimit > 0)
		{
			oCrit.setLimit(_iLimit);
		}
		return DatabaseBackupPeer.doSelect(oCrit);
	}
}