package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.DiscountManager;
import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerTypePeer;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.DiscountCoupon;
import com.ssti.enterprise.pos.om.DiscountCouponPeer;
import com.ssti.enterprise.pos.om.DiscountPeer;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.PaymentTypePeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DiscountTool.java,v 1.24 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: DiscountTool.java,v $
 * 
 * 2018-07-27
 * - change method getDiscountAmount for By Qty Discount.
 *   now support multiply and perItem settings for value discount  
 * 
 * 2017-01-07
 * - change method query discount. Now PLU/SKU rule is not %PLU% but PLU || PLU,% || %,PLU || %,PLU,%
 *   to fix problem with discount mix when there are discount for PLU 001 and 01001 
 * 
 * 2016-12-27
 * - add method validate (move from discount setup action)
 *  
 * 2016-09-21
 * - @see {@link DefaultSalesTransaction#}
 * - deprecate method getPWPPrice 
 * - change PWP method now PWP will use DiscountAmount
 * - change MultiUnit method, multi unit will use DiscountAmount
 * 
 * 2016-03-08
 * - add new discount type i_BY_CUSTOM
 * - add new method queryCustomDiscount to query custom discount
 * - add new method findCustomDiscount to get custom discount from manager
 * 
 * 2015-03-03
 * Change in getExists add coupon type criteria if discount is by coupon
 * 
 * </pre><br>
 */
public class DiscountTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( DiscountTool.class );

	public static final int i_BY_EVENT  = 1;
	public static final int i_BY_QTY    = 2;
	public static final int i_BY_EVTQTY = 3;
    public static final int i_BY_TOTAL  = 4;
    public static final int i_BY_PWP    = 5;
    public static final int i_BY_MULTI  = 6;
    public static final int i_BY_COUPON = 7;
    public static final int i_BY_PRICE  = 8;    
    public static final int i_BY_CUSTOM = 99;    
    
    public static List v_DET_DISC = null;    
    static
    {
    	v_DET_DISC = new ArrayList(4);
    	v_DET_DISC.add(i_BY_EVENT);
    	v_DET_DISC.add(i_BY_EVTQTY);
    	v_DET_DISC.add(i_BY_PWP);
    	v_DET_DISC.add(i_BY_MULTI);    	    
    }
    
	public static List getAllDiscount()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		return DiscountPeer.doSelect(oCrit);
    }
    
	public static Discount getDiscountByID(String _sID)
    	throws Exception
    {
		return getDiscountByID(_sID, null);
	}

	public static Discount getDiscountByID(String _sID, Connection _oConn)
		throws Exception
	{		
		return DiscountManager.getInstance().getDiscountByID(_sID, _oConn);
	}
	
	public static Discount getDiscountByCode(String _sCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(DiscountPeer.DISCOUNT_CODE, _sCode);
	    List vData = DiscountPeer.doSelect(oCrit);
		if (vData.size() > 0) {
			return (Discount) vData.get(0);
		}
		return null;
	}
	
	public static String getDiscountCodeByID(String _sID)
    	throws Exception
    {
        Discount oDisc = getDiscountByID (_sID);
        if (oDisc != null)
		{
			return oDisc.getDiscountCode();
		}
		else
		{
			return null;
		}
	}
	
	public static String getTypeStr(int _iType)
		throws Exception
	{
		if (_iType == i_BY_QTY || _iType == i_BY_EVTQTY) return LocaleTool.getString("by_qty");
        if (_iType == i_BY_TOTAL) return LocaleTool.getString("by_total");
        if (_iType == i_BY_PWP) return LocaleTool.getString("by_pwp");        
        if (_iType == i_BY_MULTI) return LocaleTool.getString("by_multi_unit");        
        if (_iType == i_BY_COUPON) return LocaleTool.getString("coupon"); 
        if (_iType == i_BY_PRICE) return LocaleTool.getString("sales_price");
        if (_iType == i_BY_CUSTOM) return "Custom Logic";         
        return LocaleTool.getString("by_event");
	}	

	public static int getTypeByStr(String _sType)
		throws Exception
	{
		if (LocaleTool.getString("by_qty").equals(_sType)) return i_BY_QTY;
		if (LocaleTool.getString("by_event_qty").equals(_sType)) return i_BY_EVTQTY;
		if (LocaleTool.getString("by_event").equals(_sType)) return i_BY_EVENT;
        if (LocaleTool.getString("by_total").equals(_sType)) return i_BY_TOTAL;
        if (LocaleTool.getString("by_pwp").equals(_sType)) return i_BY_PWP;
        if (LocaleTool.getString("by_multi_unit").equals(_sType)) return i_BY_MULTI;
        if (LocaleTool.getString("coupon").equals(_sType)) return i_BY_COUPON;
        if (LocaleTool.getString("sales_price").equals(_sType)) return i_BY_PRICE;
        if ("Custom Logic".equals(_sType)) return i_BY_CUSTOM;        
        return -1;
	}	
	
	/**
     * @@deprecated
     * 
	 * @param _sLocationID
	 * @param _sItemID
	 * @param _sCustTypeID
	 * @return Discount List
	 * @throws Exception
	 */
	public static List findDiscount (String _sLocationID, String _sItemID, String _sCustTypeID, Date _dTransDate)
		throws Exception
	{
		Item oItem = ItemTool.getItemByID(_sItemID);

		String sKatID = oItem.getKategoriId();
		Kategori oKat = KategoriTool.getKategoriByID(sKatID);
		List vKat = KategoriTool.getParentIDList(sKatID, null);
		
		if (_dTransDate == null) _dTransDate = new Date();
		
		Criteria oCrit = new Criteria ();
		
		Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, _sLocationID, Criteria.EQUAL);
		Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);
		
		Criteria.Criterion oItem1 = oCrit.getNewCriterion(DiscountPeer.ITEM_ID, _sItemID, Criteria.EQUAL);
		Criteria.Criterion oItem2 = oCrit.getNewCriterion(DiscountPeer.ITEM_ID, "", Criteria.EQUAL);

		Criteria.Criterion oKat1 = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, vKat, Criteria.IN);
		Criteria.Criterion oKat2 = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, "", Criteria.EQUAL);

		Criteria.Criterion oSKU1   = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, oItem.getItemSku(), Criteria.EQUAL);
		Criteria.Criterion oSKU2   = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "", Criteria.EQUAL);
		
		Criteria.Criterion oStart1 = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, _dTransDate, Criteria.LESS_EQUAL);
		Criteria.Criterion oStart2 = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, null ,Criteria.ISNULL);

		Criteria.Criterion oEnd1  = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(_dTransDate), Criteria.GREATER_EQUAL);
		Criteria.Criterion oEnd2  = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, null ,Criteria.ISNULL);
		
		Criteria.Criterion oCustType1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, _sCustTypeID ,Criteria.EQUAL);
		Criteria.Criterion oCustType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
		
		oCrit.add(oLoc1.or(oLoc2)
				  .and(oItem1.or(oItem2))
				  .and(oSKU1.or(oSKU2))
				  .and(oKat1.or(oKat2))
				  .and(oStart1.or(oStart2))
				  .and(oEnd1.or(oEnd2))
				  .and(oCustType1.or(oCustType2)));
		
		//log.debug ( oCrit.toString());
		
		return DiscountPeer.doSelect (oCrit);
	}

	/**
     * Find Discount for Sales Transaction
     * Use Discount Manager to handle cache
     * 
     * @param _sLocationID
     * @param _sItemID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public static Discount findDiscount (String _sLocationID,                                      
                                         String _sItemID, 
                                         String _sCustomerID,
                                         String _sPTypeID,
                                         Date _dTransDate)
        throws Exception
    {
    	return findDiscount(_sLocationID, _sItemID, _sCustomerID, _sPTypeID, null, _dTransDate);
    }
	
    /**
     * 
     * @param _sLocationID
     * @param _sItemID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _vPTypeIDs
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public static Discount findDiscount (String _sLocationID,                                      
                                         String _sItemID, 
                                         String _sCustomerID,
                                         String _sPTypeID,
                                         List _vPTypeIDs,
                                         Date _dTransDate)
        throws Exception
    {
        List vDisc = DiscountManager.getInstance().findDiscount(_sLocationID,_sItemID,_sCustomerID,_sPTypeID,_vPTypeIDs,_dTransDate);
        if (vDisc != null && vDisc.size() > 0)
        {
            return (Discount)vDisc.get(0);
        }
        return null;
    }
    
    /**
     * query discount from database by following parameters
     * 
     * @param _sLocationID 
     * @param _sItemID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _vPTypeIDs  => Support Multi Payment IDs  from SalesTransaction oTR.getPaymentIDs()
     * @param _dTransDate
     * @param _iDiscType
     * @return
     * @throws Exception
     */
    public static List queryDiscount (String _sLocationID,                                      
                                      String _sItemID, 
                                      String _sCustomerID,
                                      String _sPTypeID,
                                      List _vPTypeIDs,
                                      Date _dTransDate,
                                      int _iDiscType)
        throws Exception
    {
        Item oItem = ItemTool.getItemByID(_sItemID);        
        Customer oCust = CustomerTool.getCustomerByID(_sCustomerID);
        if (oItem != null && oCust != null)
        {
            String sItemCode = oItem.getItemCode();
            String sSKU = oItem.getItemSku();
            String sBrand = oItem.getBrand();
            String sManuf = oItem.getManufacturer();
            String sTags = oItem.getTags();
            
            String sCustCode = oCust.getCustomerCode();
            String sCustTypeID = oCust.getCustomerTypeId();
            
            String sKatID = oItem.getKategoriId();
            Kategori oKat = KategoriTool.getKategoriByID(sKatID);
            List vKat = KategoriTool.getParentIDList(sKatID, null);        
                        
            if (_dTransDate == null) _dTransDate = new Date();
            String sDay = DateUtil.getDayCode(_dTransDate);
            String sHour = CustomFormatter.formatCustomDate(_dTransDate,"HH:mm");
            
            Criteria oCrit = new Criteria ();
            
            Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "%"+_sLocationID+"%", Criteria.ILIKE);
            Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);
            
            Criteria.Criterion oItem1 = oCrit.getNewCriterion(DiscountPeer.ITEMS, sItemCode, Criteria.EQUAL);        //002 i.e we want disc for PLU 002
            Criteria.Criterion oItem2 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + sItemCode, Criteria.ILIKE); //001,002
            Criteria.Criterion oItem3 = oCrit.getNewCriterion(DiscountPeer.ITEMS, sItemCode + ",%", Criteria.ILIKE); //002,003
            Criteria.Criterion oItem4 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + sItemCode + ",%", Criteria.ILIKE); //001,002,003            
            Criteria.Criterion oItem5 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "", Criteria.EQUAL);
            
            Criteria.Criterion oKat1 = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, vKat, Criteria.IN);
            Criteria.Criterion oKat2 = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, "", Criteria.EQUAL);
    
            Criteria.Criterion oSKU1 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, sSKU, Criteria.EQUAL);        //002
            Criteria.Criterion oSKU2 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "%," + sSKU, Criteria.ILIKE); //001,002
            Criteria.Criterion oSKU3 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, sSKU + ",%", Criteria.ILIKE); //002,003
            Criteria.Criterion oSKU4 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "%," + sSKU + ",%", Criteria.ILIKE); //001,002,003            
            Criteria.Criterion oSKU5 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "", Criteria.EQUAL);
            
            Criteria.Criterion oBRN1 = oCrit.getNewCriterion(DiscountPeer.BRANDS, sBrand, Criteria.EQUAL);        //002
            Criteria.Criterion oBRN2 = oCrit.getNewCriterion(DiscountPeer.BRANDS, "%," + sBrand, Criteria.ILIKE); //001,002
            Criteria.Criterion oBRN3 = oCrit.getNewCriterion(DiscountPeer.BRANDS, sBrand + ",%", Criteria.ILIKE); //002,003
            Criteria.Criterion oBRN4 = oCrit.getNewCriterion(DiscountPeer.BRANDS, "%," + sBrand + ",%", Criteria.ILIKE); //001,002,003            
            Criteria.Criterion oBRN5 = oCrit.getNewCriterion(DiscountPeer.BRANDS, "", Criteria.EQUAL);

            Criteria.Criterion oMAN1 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, sManuf, Criteria.EQUAL);        //002
            Criteria.Criterion oMAN2 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, "%," + sManuf, Criteria.ILIKE); //001,002
            Criteria.Criterion oMAN3 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, sManuf + ",%", Criteria.ILIKE); //002,003
            Criteria.Criterion oMAN4 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, "%," + sManuf + ",%", Criteria.ILIKE); //001,002,003            
            Criteria.Criterion oMAN5 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, "", Criteria.EQUAL);

            Criteria.Criterion oTAG1 = oCrit.getNewCriterion(DiscountPeer.TAGS, sTags, Criteria.EQUAL);        //002
            Criteria.Criterion oTAG2 = oCrit.getNewCriterion(DiscountPeer.TAGS, "%," + sTags, Criteria.ILIKE); //001,002
            Criteria.Criterion oTAG3 = oCrit.getNewCriterion(DiscountPeer.TAGS, sTags + ",%", Criteria.ILIKE); //002,003
            Criteria.Criterion oTAG4 = oCrit.getNewCriterion(DiscountPeer.TAGS, "%," + sTags + ",%", Criteria.ILIKE); //001,002,003            
            Criteria.Criterion oTAG5 = oCrit.getNewCriterion(DiscountPeer.TAGS, "", Criteria.EQUAL);
            
            Criteria.Criterion oStart = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, _dTransDate, Criteria.LESS_EQUAL);    
            Criteria.Criterion oEnd   = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(_dTransDate), Criteria.GREATER_EQUAL);

            Criteria.Criterion oDay1 = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "%"+sDay+"%", Criteria.ILIKE);
            Criteria.Criterion oDay2 = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "", Criteria.EQUAL);

            Criteria.Criterion oHour1 = oCrit.getNewCriterion(DiscountPeer.HAPPY_HOUR, false, Criteria.EQUAL);
            Criteria.Criterion oHour2 = oCrit.getNewCriterion(DiscountPeer.HAPPY_HOUR, true,  Criteria.EQUAL);
            Criteria.Criterion oHour3 = oCrit.getNewCriterion(DiscountPeer.START_HOUR, sHour, Criteria.LESS_EQUAL);
            Criteria.Criterion oHour4 = oCrit.getNewCriterion(DiscountPeer.END_HOUR, sHour, Criteria.GREATER_EQUAL);
            
            Criteria.Criterion oCust1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "%"+sCustCode+"%", Criteria.ILIKE);
            Criteria.Criterion oCust2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "", Criteria.EQUAL);

            Criteria.Criterion oCustType1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, sCustTypeID ,Criteria.EQUAL);
            Criteria.Criterion oCustType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);

         	Criteria.Criterion oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _sPTypeID,Criteria.EQUAL);
        	Criteria.Criterion oPType2 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, "" ,Criteria.EQUAL);           

            if (_vPTypeIDs != null && _vPTypeIDs.size() > 1 && PreferenceTool.getSalesAllowMultipmtDisc()) 
            {
            	oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _vPTypeIDs,Criteria.IN);
            }
            
            oCrit.add(oLoc1.or(oLoc2)
                    .and(oItem1.or(oItem2).or(oItem3).or(oItem4).or(oItem5))
                    .and(oSKU1.or(oSKU2).or(oSKU3).or(oSKU4).or(oSKU5))
                    .and(oBRN1.or(oBRN2).or(oBRN3).or(oBRN4).or(oBRN5))
                    .and(oMAN1.or(oMAN2).or(oMAN3).or(oMAN4).or(oMAN5))
                    .and(oTAG1.or(oTAG2).or(oTAG3).or(oTAG4).or(oTAG5))
                    .and(oKat1.or(oKat2))
                    .and(oStart.and(oEnd))
                    .and(oDay1.or(oDay2))
                    .and(oHour1.or(oHour2.and(oHour3).and(oHour4)))
                    .and(oCust1.or(oCust2))
                    .and(oCustType1.or(oCustType2))
                    .and(oPType1.or(oPType2)));
             
                      
            oCrit.add(DiscountPeer.IS_TOTAL_DISC, false);           
            if(_iDiscType > 0)
            {
                oCrit.add(DiscountPeer.DISCOUNT_TYPE, _iDiscType);            	
            }
            else if(_iDiscType == -1) //exclude multi unit discount, because multi unit discount should be called before other disc
            {
            	//2016-09-20 changed multi unit discount logic so can include multi unit discount in default
                //oCrit.add(DiscountPeer.DISCOUNT_TYPE, i_BY_MULTI, Criteria.NOT_EQUAL);
            }   
            System.out.println(oCrit.toString());
            //log.debug ( oCrit.toString());
            return DiscountPeer.doSelect (oCrit);
        }
        return null;
    }
    
	public static boolean isPct (String _sDisc)
	{
		if (StringUtil.isNotEmpty(_sDisc))
		{
			if (_sDisc.indexOf("%") > 0)
			{
				return true;
			}
		}
		return false;
	}

	public static String getDiscountAmount(Discount _oDisc, double _dQty)
        throws Exception
    {
		return getDiscountAmount(_oDisc, _dQty, 0);
    }
    /**
     * 
     * @param _oDisc
     * @param _dQty
     * @return
     * @throws Exception
     */
    public static String getDiscountAmount(Discount _oDisc, double _dQty, double _dPrice)
        throws Exception
    {   
    	String sDisc = "0";
        if (_oDisc != null)
        {
            int iType = _oDisc.getDiscountType();
            //by event
            if (iType == i_BY_EVENT ) 
            {
                log.debug ( "Discount By Event : " + _oDisc.getDiscountCode() ); 
                sDisc = _oDisc.getEventDiscount();
            }
            if (iType == i_BY_PRICE ) 
            {
                log.debug ( "Discount By Price : " + _oDisc.getDiscountCode() ); 
                sDisc = "0";
                double dDisc = Double.valueOf(_oDisc.getEventDiscount());
                if(_dPrice >= dDisc)
                {
                	dDisc = _dPrice - dDisc;
                	sDisc = new BigDecimal(dDisc).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
                }
            }
            
            //by qty
            else if (iType == i_BY_QTY  || iType == i_BY_EVTQTY )
            {
                String sQtyDisc = "0";           
                log.debug ( "Discount By Qty : " + _oDisc.getDiscountCode() );
                
                double dDiscQty = 0;
                if (_dQty >= _oDisc.getQtyAmount5().doubleValue() && _oDisc.getQtyAmount5().doubleValue() != 0) 
                {
                    sQtyDisc = _oDisc.getDiscountValue5();
                    dDiscQty = _oDisc.getQtyAmount5().doubleValue();
                }
                else if (_dQty >= _oDisc.getQtyAmount4().doubleValue() && _oDisc.getQtyAmount4().doubleValue() != 0) 
                {
                    sQtyDisc = _oDisc.getDiscountValue4();
                    dDiscQty = _oDisc.getQtyAmount4().doubleValue();
                }
                else if (_dQty >= _oDisc.getQtyAmount3().doubleValue() && _oDisc.getQtyAmount3().doubleValue() != 0) 
                {
                    sQtyDisc = _oDisc.getDiscountValue3();
                    dDiscQty = _oDisc.getQtyAmount3().doubleValue();
                }
                else if (_dQty >= _oDisc.getQtyAmount2().doubleValue() && _oDisc.getQtyAmount2().doubleValue() != 0) 
                {
                    sQtyDisc = _oDisc.getDiscountValue2();
                    dDiscQty = _oDisc.getQtyAmount2().doubleValue();
                }
                else if (_dQty >= _oDisc.getQtyAmount1().doubleValue()) 
                {
                    sQtyDisc = _oDisc.getDiscountValue1();
                    dDiscQty = _oDisc.getQtyAmount1().doubleValue();                
                }
                if (iType == i_BY_EVTQTY) 
                {
                    String sEventDisc = _oDisc.getEventDiscount();
                    if (isPct(sEventDisc) && isPct(sQtyDisc) && 
                        StringUtil.isNotEmpty(sEventDisc) && StringUtil.isNotEmpty(sQtyDisc))
                    {
                        sEventDisc = StringUtil.replace(sEventDisc,"%","");
                        sDisc = new StringBuilder(sEventDisc).append("+").append(sQtyDisc).toString();
                    }
                    else if (!isPct(sEventDisc) && !isPct(sQtyDisc) && 
                             StringUtil.isNotEmpty(sEventDisc) && StringUtil.isNotEmpty(sQtyDisc))
                    {
                        try
                        {
                            double dEvent = Double.parseDouble(sEventDisc);
                            double dQty = Double.parseDouble(sQtyDisc);
                            Double dTotal = new Double(dEvent + dQty);
                            sDisc = dTotal.toString();
                        }
                        catch (Exception _oEx)
                        {
                            log.error(_oEx);
                        }
                    }
                    else if (StringUtil.isNotEmpty(sEventDisc) && 
                            (StringUtil.isEmpty(sQtyDisc) || sQtyDisc.equals("0")))
                    {
                    	sDisc = sEventDisc;
                    }
                }
                sDisc = sQtyDisc;
            	//if qty discount multiply ticked, get multiplier
	            if (_oDisc.getMultiply() && !isPct(sDisc))
	            {
	            	if (dDiscQty > 0)
	            	{
	            		BigDecimal bdQty = new BigDecimal(_dQty);
	            		BigDecimal bdMul = new BigDecimal(_dQty / dDiscQty).setScale(0, BigDecimal.ROUND_DOWN);
	            		BigDecimal bdDisc = new BigDecimal(sDisc).multiply(bdMul);
	            		if(_oDisc.getPerItem()) { //if per item ticked then multiply with discount qty
	            			bdDisc = bdDisc.multiply(new BigDecimal(dDiscQty));	
	            		}
	            		bdDisc = bdDisc.setScale(2, BigDecimal.ROUND_HALF_UP);
	            		sDisc = bdDisc.toString();
	            	}
	            	return sDisc;
	            }                
            }
            if (!isPct(sDisc))
            {
	            //if per item then times with qty
	            if (_oDisc.getPerItem())
	            {
	            	BigDecimal bdDisc = new BigDecimal(Double.valueOf(sDisc) * _dQty);
	            	bdDisc = bdDisc.setScale(2, BigDecimal.ROUND_HALF_UP);
	            	sDisc = bdDisc.toString();
	            }
            }
        }
        return sDisc;
    }
    
    /**
     * @@deprecated
     * 
     * @param _vDisc
     * @param _dQty
     * @return
     * @throws Exception
     */
	public static String getDiscountAmount(List _vDisc, double _dQty)
    	throws Exception
    {
		for (int i = 0; i < _vDisc.size(); i++) 
		{
			Discount oDisc = (Discount) _vDisc.get(i);
            getDiscountAmount(oDisc,_dQty);
		}
		return "0";
	}

	/**
	 * @param _iCond
	 * @param _sKey
	 * @param _sStart
	 * @param _sEnd
	 * @param _sKatID
	 * @param _iDisc
	 * @return Discount List
	 */
	public static List findData(int _iCond, 
								String _sKey, 
								String _sStart, 
								String _sEnd, 
								String _sKatID, 
                                String _sLocID,
								int _iDisc,
                                int _iStatus) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sKey))
		{
            String sWCKey = _sKey;
            if (!StringUtil.contains(_sKey,"%")) sWCKey = "%"+_sKey+"%";
            
			if (_iCond == 1)
			{
				oCrit.add(DiscountPeer.DISCOUNT_CODE, (Object)sWCKey, Criteria.ILIKE);
			}
            if (_iCond == 2)
            {
                oCrit.add(DiscountPeer.DESCRIPTION, (Object)sWCKey, Criteria.ILIKE);
            }            
            else if (_iCond == 3)
            {
                oCrit.add(DiscountPeer.ITEM_SKU, (Object)sWCKey, Criteria.ILIKE);
            }
            else if (_iCond == 4)
            {
                oCrit.add(DiscountPeer.ITEMS, (Object)sWCKey, Criteria.ILIKE);
            }
            else if (_iCond == 5)
            {
                oCrit.addJoin(CustomerTypePeer.CUSTOMER_TYPE_ID, DiscountPeer.CUSTOMER_TYPE_ID);
                oCrit.add(CustomerTypePeer.DESCRIPTION, (Object)sWCKey, Criteria.ILIKE);
            }
            else if (_iCond == 6)
            {
                oCrit.add(DiscountPeer.CUSTOMERS, (Object)sWCKey, Criteria.ILIKE);
            }            
            else if (_iCond == 7)
            {
                oCrit.addJoin(PaymentTypePeer.PAYMENT_TYPE_ID, DiscountPeer.PAYMENT_TYPE_ID);
                oCrit.add(PaymentTypePeer.DESCRIPTION, (Object)sWCKey, Criteria.ILIKE);
            }
            else if (_iCond == 8)
            {
                oCrit.add(DiscountPeer.DISCOUNT_DAYS, (Object)sWCKey, Criteria.ILIKE);                
            }            
            else if (_iCond == 9)
            {
                oCrit.add(DiscountPeer.PROMO_CODE, (Object)sWCKey, Criteria.ILIKE);                
            }           
		}
		if (StringUtil.isNotEmpty(_sKatID))
		{
			List vKat = KategoriTool.getChildIDList(_sKatID);
			vKat.add(_sKatID);
			oCrit.add(DiscountPeer.KATEGORI_ID, vKat, Criteria.IN);
		}	
        if (StringUtil.isNotEmpty(_sLocID))
        {
            oCrit.add(DiscountPeer.LOCATION_ID, (Object)("%"+_sLocID+"%"), Criteria.ILIKE);
            oCrit.or(DiscountPeer.LOCATION_ID, "");
        }   
        if (StringUtil.isNotEmpty(_sStart))
		{
			oCrit.add(DiscountPeer.EVENT_BEGIN_DATE, 
				DateUtil.getStartOfDayDate(CustomParser.parseDate(_sStart)), Criteria.GREATER_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sEnd))
		{
			oCrit.add(DiscountPeer.EVENT_END_DATE, 
				DateUtil.getEndOfDayDate(CustomParser.parseDate(_sEnd)), Criteria.LESS_EQUAL);
		}
		if (_iDisc > 0)
		{
			oCrit.add(DiscountPeer.DISCOUNT_TYPE, _iDisc );		
		}
        if (_iStatus != 3 && StringUtil.isEmpty(_sStart) && StringUtil.isEmpty(_sEnd))
        {
            Date dToday = new Date();
            if (_iStatus == 1) //active
            {
                oCrit.add(DiscountPeer.EVENT_BEGIN_DATE, DateUtil.getStartOfDayDate(dToday), Criteria.LESS_EQUAL);
                oCrit.add(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(dToday), Criteria.GREATER_EQUAL);
            }
            else
            {
                Criteria.Criterion oStart = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, dToday, Criteria.GREATER_EQUAL);
                Criteria.Criterion oEnd  = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, dToday, Criteria.LESS_EQUAL);
                oCrit.add(oStart.or(oEnd));
            }
        }
        oCrit.addAscendingOrderByColumn(DiscountPeer.DISCOUNT_CODE);
		log.debug("Discount Criteria : " + oCrit);
		return DiscountPeer.doSelect(oCrit);
	}

    //-------------------------------------------------------------------------
    // PAYMENT TYPE DISCOUNT
    //-------------------------------------------------------------------------
	
	public static List findActivePaymentTypeDisc(Date _dTrans, String _sCustID, String _sLocID)
		throws Exception
	{
		return DiscountManager.getInstance().findActivePaymentTypeDisc(_dTrans, _sCustID, _sLocID);
	}
	/**
	 * 
	 * @param _dTrans
	 * @param _sCustID
	 * @param _sLocID
	 * @return
	 * @throws Exception
	 */
	public static boolean isActivePaymentTypeDiscExists(Date _dTrans, String _sCustID, String _sLocID) 
		throws Exception
	{
		List vPmt = findActivePaymentTypeDisc(_dTrans, _sCustID, _sLocID);
		if(vPmt.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	public static boolean isPaymentTypeValidForDisc(Date _dTrans, String _sCustID, String _sLocID, List _vIDs)
		throws Exception
	{
		List vPT = findActivePaymentTypeDisc(_dTrans, _sCustID, _sLocID);
		if(vPT.size() > 0 && _vIDs != null && _vIDs.size() > 0)
		{
			for (int i = 0; i < _vIDs.size(); i++)
			{
				String sPTypeID = (String)_vIDs.get(i);				
				for (int j = 0; j < vPT.size(); j++)
				{
					Discount oDisc = (Discount) vPT.get(j);
					if (StringUtil.isEqual(oDisc.getPaymentTypeId(),sPTypeID))
					{
						return true;
					}							
				}
			}
		}
		return false;
	}
    
    //-------------------------------------------------------------------------
    // TOTAL / COUPON DISC
    //-------------------------------------------------------------------------

    /**
     * find total discount by salesTransaction
     * 
     * @param _oTR
     * @param _bCoupon
     * @return
     * @throws Exception
     */
    public static Discount findTotalDiscount (SalesTransaction _oTR)
        throws Exception
    {        
    	if (_oTR != null)
    	{
	        return findTotalDiscount(_oTR.getLocationId(), 
	        						 _oTR.getCustomerId(), 
	        						 _oTR.getPaymentTypeId(), 
	        						 _oTR.getPaymentTypeIDs(),
	        						 _oTR.getTransactionDate(), false);
    	}
    	return null;
    }
        
    /**
     * Find Total/Coupon Discount for Sales Transaction
     * Use Discount Manager to handle cache
     * 
     * @param _sLocationID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public static Discount findTotalDiscount (String _sLocationID,                                      
                                              String _sCustomerID,
                                              String _sPTypeID,
                                              List _vPTypeIDs,
                                              Date _dTransDate,
                                              boolean _bCoupon)
        throws Exception
    {
        List vDisc = DiscountManager.getInstance().findTotalDiscount(_sLocationID,_sCustomerID,_sPTypeID,_vPTypeIDs,_dTransDate,_bCoupon);
        if (vDisc != null && vDisc.size() > 0)
        {
            return (Discount)vDisc.get(0);
        }
        return null;
    }

    /**
     * find total discount by salesTransaction
     * 
     * @param _oTR
     * @param _bCoupon
     * @return
     * @throws Exception
     */
    public static List findCouponDiscount (SalesTransaction _oTR)
        throws Exception
    {        
    	List vDisc = null;
    	if (_oTR != null)
    	{
            vDisc = DiscountManager.getInstance().findTotalDiscount(_oTR.getLocationId(),
            														_oTR.getCustomerId(),
            														_oTR.getPaymentTypeId(),
            														_oTR.getPaymentTypeIDs(),
            														_oTR.getTransactionDate(),true);             
    	}
    	if(vDisc == null) vDisc = new ArrayList(1); //prevent nullpointer
    	return vDisc;
    }
    
    /**
     * Query Total Discount from DB from Sales Transaction
     *  
     * @param _sLocationID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _vPTypeIDs  => Support Multi Payments
     * @param _dTransDate
     * @param _bCoupon
     * @return
     * @throws Exception
     */
    public static List queryTotalDiscount (String _sLocationID,                                      
                                      	   String _sCustomerID,
                                      	   String _sPTypeID,
                                      	   List _vPTypeIDs,
                                      	   Date _dTransDate,
                                      	   boolean _bCoupon)
        throws Exception
    {        
        Customer oCust = CustomerTool.getCustomerByID(_sCustomerID);
        if(oCust != null)
        {            
            String sCustCode = oCust.getCustomerCode();
            String sCustTypeID = oCust.getCustomerTypeId();
                        
            if (_dTransDate == null) _dTransDate = new Date();
            String sDay = DateUtil.getDayCode(_dTransDate);
            String sHour = CustomFormatter.formatCustomDate(_dTransDate,"HH:mm");
            
            Criteria oCrit = new Criteria ();
            
            Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "%"+_sLocationID+"%", Criteria.ILIKE);
            Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);
                                
            Criteria.Criterion oStart = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, _dTransDate, Criteria.LESS_EQUAL);    
            Criteria.Criterion oEnd   = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(_dTransDate), Criteria.GREATER_EQUAL);

            Criteria.Criterion oDay1 = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "%"+sDay+"%", Criteria.ILIKE);
            Criteria.Criterion oDay2 = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "", Criteria.EQUAL);

            Criteria.Criterion oHour1 = oCrit.getNewCriterion(DiscountPeer.HAPPY_HOUR, false, Criteria.EQUAL);
            Criteria.Criterion oHour2 = oCrit.getNewCriterion(DiscountPeer.HAPPY_HOUR, true,  Criteria.EQUAL);
            Criteria.Criterion oHour3 = oCrit.getNewCriterion(DiscountPeer.START_HOUR, sHour, Criteria.LESS_EQUAL);
            Criteria.Criterion oHour4 = oCrit.getNewCriterion(DiscountPeer.END_HOUR, sHour, Criteria.GREATER_EQUAL);
            
            Criteria.Criterion oCust1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "%"+sCustCode+"%", Criteria.ILIKE);
            Criteria.Criterion oCust2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "", Criteria.EQUAL);

            Criteria.Criterion oCustType1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, sCustTypeID ,Criteria.EQUAL);
            Criteria.Criterion oCustType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
 
            Criteria.Criterion oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _sPTypeID,Criteria.EQUAL);
            Criteria.Criterion oPType2 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, "" ,Criteria.EQUAL);           
            
            if(_vPTypeIDs != null && _vPTypeIDs.size() > 1 && PreferenceTool.getSalesAllowMultipmtDisc())
            {
                oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _vPTypeIDs,Criteria.IN);            	
            }            
            
            oCrit.add(oLoc1.or(oLoc2)
                      .and(oStart.and(oEnd))
                      .and(oDay1.or(oDay2))
                      .and(oHour1.or(oHour2.and(oHour3).and(oHour4)))
                      .and(oCust1.or(oCust2))
                      .and(oCustType1.or(oCustType2))
                      .and(oPType1.or(oPType2)));

            oCrit.add(DiscountPeer.IS_TOTAL_DISC, true);
            if(_bCoupon)
            {
            	oCrit.add(DiscountPeer.DISCOUNT_TYPE, i_BY_COUPON);
            }
            else
            {
            	oCrit.add(DiscountPeer.DISCOUNT_TYPE, i_BY_TOTAL);
            }
            return DiscountPeer.doSelect (oCrit);
        }
        return null;
    }
    
    /**
     * 
     * @param _oDisc
     * @param _dTotalAmt
     * @return
     * @throws Exception
     */
    public static String getTotalDiscAmount(Discount _oDisc, double _dTotalAmt)
        throws Exception
    {   
    	String sDisc = "0";
        if (_oDisc != null)
        {
            int iType = _oDisc.getDiscountType();
            if (iType == i_BY_TOTAL)
            {
                String sTRDisc = "0";           
                log.debug ( "Total Discount By Amount : " + _oDisc.getDiscountCode() );
                
                double dDiscAmt = 0;
                if (_dTotalAmt >= _oDisc.getQtyAmount5().doubleValue() && _oDisc.getQtyAmount5().doubleValue() != 0) 
                {
                    sTRDisc = _oDisc.getDiscountValue5();
                    dDiscAmt = _oDisc.getQtyAmount5().doubleValue();
                }
                else if (_dTotalAmt >= _oDisc.getQtyAmount4().doubleValue() && _oDisc.getQtyAmount4().doubleValue() != 0) 
                {
                    sTRDisc = _oDisc.getDiscountValue4();
                    dDiscAmt = _oDisc.getQtyAmount4().doubleValue();
                }
                else if (_dTotalAmt >= _oDisc.getQtyAmount3().doubleValue() && _oDisc.getQtyAmount3().doubleValue() != 0) 
                {
                    sTRDisc = _oDisc.getDiscountValue3();
                    dDiscAmt = _oDisc.getQtyAmount3().doubleValue();
                }
                else if (_dTotalAmt >= _oDisc.getQtyAmount2().doubleValue() && _oDisc.getQtyAmount2().doubleValue() != 0) 
                {
                    sTRDisc = _oDisc.getDiscountValue2();
                    dDiscAmt = _oDisc.getQtyAmount2().doubleValue();
                }
                else if (_dTotalAmt >= _oDisc.getQtyAmount1().doubleValue()) 
                {
                    sTRDisc = _oDisc.getDiscountValue1();
                    dDiscAmt = _oDisc.getQtyAmount1().doubleValue();                
                }                
                sDisc = sTRDisc;
            	//if per qty discount multiply applied then times with qty
	            if (_oDisc.getMultiply() && !isPct(sDisc))
	            {
	            	if (dDiscAmt > 0)
	            	{
		            	BigDecimal bdQty = new BigDecimal(_dTotalAmt / dDiscAmt).setScale(0, BigDecimal.ROUND_DOWN);
		            	BigDecimal bdDisc = new BigDecimal(Double.valueOf(sDisc) * bdQty.intValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		            	sDisc = bdDisc.toString();
	            	}	            	
	            }    
	            return sDisc;
            }
        }
        return sDisc;
    }
    //-------------------------------------------------------------------------
    // COUPON DISC
    //-------------------------------------------------------------------------
    public static void createCoupon(SalesTransaction _oTR, Connection _oConn)
    	throws Exception
    {
    	List vCoupon = findCouponDiscount(_oTR);
    	if(vCoupon != null)
    	{
	    	for (int i = 0; i < vCoupon.size(); i++)
	    	{
	    		Discount oCoupon = (Discount) vCoupon.get(i);
	    		if(_oTR.getTotalAmount().doubleValue() >= oCoupon.getCouponPurchAmt().doubleValue())
	    		{
	    			int iQty = oCoupon.getCouponQty();
	    			if(oCoupon.getCouponType() == 1 && iQty <= 0) iQty = 1;
	    			for (int j = 0; j < iQty; j++)
	    			{
	    				String sLocCode = _oTR.getLocation().getCode();
	    				DiscountCoupon oDC = new DiscountCoupon();
	    				oDC.setDiscountCouponId(
	    					_oTR.getSalesTransactionId().replace(sLocCode, "") + 
	    						StringUtil.formatNumberString(Integer.toString(i),1) + 
	    							StringUtil.formatNumberString(Integer.toString(j), 2));
	    				
	    				oDC.setDiscountId(oCoupon.getDiscountId());
	    				oDC.setCouponNo(oDC.getDiscountCouponId());
	    				oDC.setCouponValue(oCoupon.getCouponValue());
	    				oDC.setCouponType(oCoupon.getCouponType());
	    				oDC.setCreateDate(_oTR.getTransactionDate());
	    				oDC.setCreateTransId(_oTR.getSalesTransactionId());
	    				oDC.setValidFrom(DateUtil.getStartOfDayDate(oCoupon.getCouponValidFrom()));
	    				oDC.setValidTo(DateUtil.getEndOfDayDate(oCoupon.getCouponValidTo()));    		
	    				oDC.save(_oConn);
	    			}
	    		}
	    	}
    	}
    }

    public static void deleteCoupon(SalesTransaction _oTR, Connection _oConn) 
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(DiscountCouponPeer.CREATE_TRANS_ID, _oTR.getSalesTransactionId());
    	DiscountCouponPeer.doDelete(oCrit, _oConn);
    }
    
    public static List getBySrcTransID(String _sTransID, Connection _oConn) 
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(DiscountCouponPeer.CREATE_TRANS_ID, _sTransID);
    	return DiscountCouponPeer.doSelect(oCrit, _oConn);    	
    }
    
    public static DiscountCoupon getByCouponNo(String _sCouponNo, Connection _oConn) 
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(DiscountCouponPeer.COUPON_NO, _sCouponNo);
    	List v = DiscountCouponPeer.doSelect(oCrit, _oConn);
    	if (v.size() > 0)
    	{
    		return (DiscountCoupon)v.get(0);
    	}
    	return null;
    }
    
    public static List getByUseCashier(String _sCashierName, Date _dStart, Connection _oConn) 
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(DiscountCouponPeer.USE_CASHIER,_sCashierName);
    	oCrit.add(DiscountCouponPeer.USE_DATE, _dStart, Criteria.GREATER_EQUAL);
    	oCrit.and(DiscountCouponPeer.USE_DATE, new Date(), Criteria.LESS_EQUAL);
    	return DiscountCouponPeer.doSelect(oCrit, _oConn);
    }    
    
    public static void useCoupon(SalesTransaction _oTR, InvoicePayment _oPmt, String _sCouponNo, Connection _oConn)
    	throws Exception
    {
    	boolean bSync = false;
    	if(!StringUtil.equals(PreferenceTool.getLocationID(), _oTR.getLocationId())) 
		{
    		bSync = true;
		}
    	DiscountCoupon oDC = getByCouponNo(_sCouponNo, _oConn);
    	if(oDC != null && _oTR != null)
    	{
    		if (StringUtil.isEmpty(oDC.getUseTransId()) || 
    			StringUtil.equals(oDC.getUseTransId(), _oTR.getSalesTransactionId()))
    		{
	    		oDC.setUseDate(_oTR.getTransactionDate());
	    		oDC.setUseTransId(_oTR.getSalesTransactionId());  	    		
	    		oDC.setUseCashier(_oTR.getCashierName());
	    		oDC.setPaymentTypeId(_oPmt.getPaymentTypeId());
	    		oDC.save(_oConn);
    		}
    		else
    		{
    			String sMsg = "Coupon " + oDC.getCouponNo() + " Already Used!";
    			//prevent sync error when using coupon 
        		if(!bSync) 
        		{
        			throw new Exception(sMsg);
        		}
        		else
        		{
        			log.error(sMsg);
        		}
    		}
    	}
    	else
    	{
    		String sMsg = "Invalid Coupon No: " + _sCouponNo + "!";
    		//prevent sync error when using coupon 
    		if(!bSync) 
    		{
    			throw new Exception (sMsg);
    		}
    		else
    		{
    			log.error(sMsg);
    		}
    	}
    }

    //-------------------------------------------------------------------------
    // CUSTOM DISC
    //-------------------------------------------------------------------------
    /**
     * find Custom discount by SalesTransaction
     * 
     * @param _oTR
     * @return
     * @throws Exception
     */
    public static Discount findCustomDiscount (SalesTransaction _oTR)
        throws Exception
    {        
    	if (_oTR != null)
    	{
	        return findCustomDiscount(_oTR.getLocationId(), 
	        						  _oTR.getCustomerId(), 
	        						  _oTR.getPaymentTypeId(), 
	        						  _oTR.getPaymentTypeIDs(),
	        						  _oTR.getTransactionDate());
    	}
    	return null;
    }
    
    /**
     * Find Custom Discount for SalesTransaction
     * Use Discount Manager to handle cache
     * 
     * @param _sLocationID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public static Discount findCustomDiscount (String _sLocationID,                                      
                                              String _sCustomerID,
                                              String _sPTypeID,
                                              List _vPTypeIDs,
                                              Date _dTransDate)
        throws Exception
    {
        List vDisc = DiscountManager.getInstance().findCustomDiscount(_sLocationID,_sCustomerID,_sPTypeID,_vPTypeIDs,_dTransDate);
        if (vDisc != null && vDisc.size() > 0)
        {
            return (Discount)vDisc.get(0);
        }
        return null;
    }
    
    /**
     * Query Existing Custom Discount from DB from Sales Transaction
     *  
     * @param _sLocationID
     * @param _sCustomerID
     * @param _sPTypeID
     * @param _vPTypeIDs  => Support Multi Payments
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public static List queryCustomDiscount (String _sLocationID,                                      
                                      	    String _sCustomerID,
                                      	    String _sPTypeID,
                                      	    List _vPTypeIDs,
                                      	    Date _dTransDate)
        throws Exception
    {        
        Customer oCust = CustomerTool.getCustomerByID(_sCustomerID);
        if(oCust != null)
        {            
            String sCustCode = oCust.getCustomerCode();
            String sCustTypeID = oCust.getCustomerTypeId();
                        
            if (_dTransDate == null) _dTransDate = new Date();
            String sDay = DateUtil.getDayCode(_dTransDate);
            String sHour = CustomFormatter.formatCustomDate(_dTransDate,"HH:mm");
            
            Criteria oCrit = new Criteria ();
            
            Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "%"+_sLocationID+"%", Criteria.ILIKE);
            Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);
                                
            Criteria.Criterion oStart = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, _dTransDate, Criteria.LESS_EQUAL);    
            Criteria.Criterion oEnd   = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(_dTransDate), Criteria.GREATER_EQUAL);

            Criteria.Criterion oDay1 = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "%"+sDay+"%", Criteria.ILIKE);
            Criteria.Criterion oDay2 = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "", Criteria.EQUAL);

            Criteria.Criterion oHour1 = oCrit.getNewCriterion(DiscountPeer.HAPPY_HOUR, false, Criteria.EQUAL);
            Criteria.Criterion oHour2 = oCrit.getNewCriterion(DiscountPeer.HAPPY_HOUR, true,  Criteria.EQUAL);
            Criteria.Criterion oHour3 = oCrit.getNewCriterion(DiscountPeer.START_HOUR, sHour, Criteria.LESS_EQUAL);
            Criteria.Criterion oHour4 = oCrit.getNewCriterion(DiscountPeer.END_HOUR, sHour, Criteria.GREATER_EQUAL);
            
            Criteria.Criterion oCust1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "%"+sCustCode+"%", Criteria.ILIKE);
            Criteria.Criterion oCust2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "", Criteria.EQUAL);

            Criteria.Criterion oCustType1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, sCustTypeID ,Criteria.EQUAL);
            Criteria.Criterion oCustType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
 
            Criteria.Criterion oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _sPTypeID,Criteria.EQUAL);
            Criteria.Criterion oPType2 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, "" ,Criteria.EQUAL);           
            
            if(_vPTypeIDs != null && _vPTypeIDs.size() > 1 && PreferenceTool.getSalesAllowMultipmtDisc())
            {
                oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _vPTypeIDs,Criteria.IN);            	
            }            
            
            oCrit.add(oLoc1.or(oLoc2)
                      .and(oStart.and(oEnd))
                      .and(oDay1.or(oDay2))
                      .and(oHour1.or(oHour2.and(oHour3).and(oHour4)))
                      .and(oCust1.or(oCust2))
                      .and(oCustType1.or(oCustType2))
                      .and(oPType1.or(oPType2)));
            
            oCrit.add(DiscountPeer.DISCOUNT_TYPE, i_BY_CUSTOM);
            return DiscountPeer.doSelect (oCrit);
        }
        return null;
    }
    
    //-------------------------------------------------------------------------
    // VALIDATE EXISTS DISCOUNT
    //-------------------------------------------------------------------------

    public static Discount getExists(Discount _oData)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		Criteria.Criterion oDT1  = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, _oData.getEventBeginDate(), Criteria.LESS_EQUAL);
		Criteria.Criterion oDT2  = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE,   _oData.getEventBeginDate(), Criteria.GREATER_EQUAL);
		Criteria.Criterion oDT3  = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, _oData.getEventEndDate(), Criteria.LESS_EQUAL);
		Criteria.Criterion oDT4  = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE,   _oData.getEventEndDate(), Criteria.GREATER_EQUAL);		
		oCrit.add((oDT1.and(oDT2)).or(oDT3.and(oDT4)));
		
		if (StringUtil.isNotEmpty(_oData.getLocationId()))
		{
			Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "%"+_oData.getLocationId()+"%", Criteria.ILIKE);
			Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);
			oCrit.add(oLoc1.or(oLoc2));
		}
		if (StringUtil.isNotEmpty(_oData.getCustomerTypeId()))
		{
			Criteria.Criterion oCustType1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, _oData.getCustomerTypeId() ,Criteria.EQUAL);
			Criteria.Criterion oCustType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
			oCrit.add(oCustType1.or(oCustType2));
		}
		if (StringUtil.isNotEmpty(_oData.getCustomers()))
		{
			Criteria.Criterion oCustomers1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "%"+_oData.getCustomers()+"%" ,Criteria.ILIKE);
			Criteria.Criterion oCustomers2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "" ,Criteria.EQUAL);
			oCrit.add(oCustomers1.or(oCustomers2));			
		}
		if (StringUtil.isNotEmpty(_oData.getItemSku()))
		{
			Criteria.Criterion oSku1 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "%"+_oData.getItemSku()+"%" ,Criteria.ILIKE);
			//Criteria.Criterion oSku2 = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "" ,Criteria.EQUAL);
			oCrit.add(oSku1); //.or(oSku2));			
		}
		if (StringUtil.isNotEmpty(_oData.getBrands()))
		{
			Criteria.Criterion oBrn1 = oCrit.getNewCriterion(DiscountPeer.BRANDS, "%"+_oData.getBrands()+"%" ,Criteria.ILIKE);
			Criteria.Criterion oBrn2 = oCrit.getNewCriterion(DiscountPeer.BRANDS, "" ,Criteria.EQUAL);
			oCrit.add(oBrn1).or(oBrn2);			
		}
		if (StringUtil.isNotEmpty(_oData.getManufacturers()))
		{
			Criteria.Criterion oMan1 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, "%"+_oData.getManufacturers()+"%" ,Criteria.ILIKE);
			Criteria.Criterion oMan2 = oCrit.getNewCriterion(DiscountPeer.MANUFACTURERS, "" ,Criteria.EQUAL);
			oCrit.add(oMan1).or(oMan2);			
		}
		if (StringUtil.isNotEmpty(_oData.getTags()))
		{
			Criteria.Criterion oTag1 = oCrit.getNewCriterion(DiscountPeer.TAGS, "%"+_oData.getTags()+"%" ,Criteria.ILIKE);
			Criteria.Criterion oTag2 = oCrit.getNewCriterion(DiscountPeer.TAGS, "" ,Criteria.EQUAL);
			oCrit.add(oTag1).or(oTag2);			
		}
		
		
		if (StringUtil.isNotEmpty(_oData.getItems()))
		{
			List vCode = StringUtil.toList(_oData.getItems(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                Criteria.Criterion oItem1 = oCrit.getNewCriterion(DiscountPeer.ITEMS, sCode, Criteria.EQUAL);        //002 i.e we want disc for PLU 002
                Criteria.Criterion oItem2 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + sCode, Criteria.ILIKE); //001,002
                Criteria.Criterion oItem3 = oCrit.getNewCriterion(DiscountPeer.ITEMS, sCode + ",%", Criteria.ILIKE); //002,003
                Criteria.Criterion oItem4 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + sCode + ",%", Criteria.ILIKE); //001,002,003            
                Criteria.Criterion oItem5 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "", Criteria.EQUAL);
    			    		
    			oCrit.add(oItem1.or(oItem2).or(oItem3).or(oItem4).or(oItem5));
            }
		}
		if (StringUtil.isNotEmpty(_oData.getPaymentTypeId()))
		{
			Criteria.Criterion oPType1 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, _oData.getPaymentTypeId() ,Criteria.EQUAL);
			Criteria.Criterion oPType2 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, "" ,Criteria.EQUAL);
			oCrit.add(oPType1.or(oPType2));
		}	
		if (StringUtil.isNotEmpty(_oData.getKategoriId()))
		{
			//TODO: validate parent also
			Criteria.Criterion oKat1 = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, _oData.getKategoriId() ,Criteria.EQUAL);
			Criteria.Criterion oKat2 = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, "" ,Criteria.EQUAL);
			oCrit.add(oKat1.or(oKat2));
			
		}
		if (_oData.getDiscountType() == i_BY_COUPON || 
			_oData.getDiscountType() == i_BY_TOTAL || 
			_oData.getDiscountType() == i_BY_PWP)
		{
			oCrit.add(DiscountPeer.DISCOUNT_TYPE, _oData.getDiscountType());
			
			if(_oData.getDiscountType() == i_BY_COUPON)
			{
				oCrit.add(DiscountPeer.COUPON_TYPE,_oData.getCouponType());
			}
		}
		else
		{
			oCrit.add(DiscountPeer.DISCOUNT_TYPE, i_BY_COUPON, Criteria.NOT_EQUAL);
			oCrit.and(DiscountPeer.DISCOUNT_TYPE, i_BY_TOTAL, Criteria.NOT_EQUAL);
			oCrit.and(DiscountPeer.DISCOUNT_TYPE, i_BY_PWP, Criteria.NOT_EQUAL);			
		}		
		if (StringUtil.isNotEmpty(_oData.getId()))
		{
			oCrit.add(DiscountPeer.DISCOUNT_ID, (Object)_oData.getId(), Criteria.NOT_EQUAL);
		}
		List vData = DiscountPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (Discount)vData.get(0);
		}
		return null;		
    }	
    //-------------------------------------------------------------------------
    // DISCOUNT FOR SPECIFIC ITEM & EVENT (USED IN PRICE TAG / LABEL)
    //-------------------------------------------------------------------------

    public static List getItemEventDisc(String _sItemID, String _sLocID)
    	throws Exception
    {
        Item oItem = ItemTool.getItemByID(_sItemID);        
        if (oItem != null)
        {
            String sItemCode = oItem.getItemCode();
            String sSKU = oItem.getItemSku();
                        
            String sKatID = oItem.getKategoriId();
            Kategori oKat = KategoriTool.getKategoriByID(sKatID);
            List vKat = KategoriTool.getParentIDList(sKatID, null);        
                        
            Date dNow = new Date();
            String sDay = DateUtil.getDayCode(dNow);
            String sHour = CustomFormatter.formatCustomDate(dNow,"HH:mm");
            
            Criteria oCrit = new Criteria ();
            
            Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, _sLocID, Criteria.EQUAL);            
            Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);            
            
            Criteria.Criterion oItem1 = oCrit.getNewCriterion(DiscountPeer.ITEMS, sItemCode, Criteria.EQUAL);        //002 i.e we want disc for PLU 002
            Criteria.Criterion oItem2 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + sItemCode, Criteria.ILIKE); //001,002
            Criteria.Criterion oItem3 = oCrit.getNewCriterion(DiscountPeer.ITEMS, sItemCode + ",%", Criteria.ILIKE); //002,003
            Criteria.Criterion oItem4 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + sItemCode + ",%", Criteria.ILIKE); //001,002,003            
            
            Criteria.Criterion oKat1  = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, vKat, Criteria.IN);
            Criteria.Criterion oKat2  = oCrit.getNewCriterion(DiscountPeer.KATEGORI_ID, "", Criteria.EQUAL);
    
            Criteria.Criterion oSKU2  = oCrit.getNewCriterion(DiscountPeer.ITEM_SKU, "", Criteria.EQUAL);
            
            Criteria.Criterion oStart = oCrit.getNewCriterion(DiscountPeer.EVENT_BEGIN_DATE, dNow, Criteria.LESS_EQUAL);    
            Criteria.Criterion oEnd   = oCrit.getNewCriterion(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(dNow), Criteria.GREATER_EQUAL);

            Criteria.Criterion oDay2  = oCrit.getNewCriterion(DiscountPeer.DISCOUNT_DAYS, "", Criteria.EQUAL);
            
            Criteria.Criterion oCust2  = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "", Criteria.EQUAL);
            Criteria.Criterion oCType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
        	Criteria.Criterion oPType2 = oCrit.getNewCriterion(DiscountPeer.PAYMENT_TYPE_ID, "" ,Criteria.EQUAL);           
            
            oCrit.add(oLoc1.or(oLoc2)
                    .and(oItem1.or(oItem2).or(oItem3).or(oItem4))
                    .and(oSKU2)
                    .and(oKat1.or(oKat2))
                    .and(oStart.and(oEnd))
                    .and(oDay2)                    
                    .and(oCust2)
                    .and(oCType2)
                    .and(oPType2));
                                   
            oCrit.add(DiscountPeer.IS_TOTAL_DISC, false);           
            oCrit.add(DiscountPeer.DISCOUNT_TYPE, i_BY_EVENT);
            oCrit.or(DiscountPeer.DISCOUNT_TYPE, i_BY_PRICE);            
            log.debug ("getItemEventDisc" + oCrit.toString());
            return DiscountPeer.doSelect (oCrit);
        }
        return null;
    }
    
    //-------------------------------------------------------------------------
    // SYNC
    //-------------------------------------------------------------------------
	public static List getLocationActiveDisc(String _sLocationID)
		throws Exception
	{		
		Date dToday = new Date();
		Criteria oCrit = new Criteria();
		//oCrit.add(DiscountPeer.EVENT_BEGIN_DATE, dToday, Criteria.LESS_EQUAL);
		oCrit.add(DiscountPeer.LOCATION_ID, (Object)("%"+_sLocationID+"%"), Criteria.ILIKE);
		oCrit.or(DiscountPeer.LOCATION_ID, "");		
		oCrit.add(DiscountPeer.EVENT_END_DATE, dToday, Criteria.GREATER_EQUAL);		
		return DiscountPeer.doSelect(oCrit);
	}

    public static List getUpdatedDiscount()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE);
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(DiscountPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		return DiscountPeer.doSelect(oCrit);
	}
    
    //-------------------------------------------------------------------------
    // PWP DISCOUNT - DEPRECATED
    //-------------------------------------------------------------------------
	/**
     * Get PWP Price For Get Item
     * 
     * @param _oPWP
     * @param _oTD //can be merged existing buy OR merged existing GET
     * @param _vTD
     * @return
     * @throws Exception
     * @deprecated
	 */
    public static BigDecimal getPWPPrice(Discount _oPWP, InventoryDetailOM _oTD, List _vTD)
        throws Exception
    {        
        if (_oPWP != null)
        {
            int iValidQty = PWPTool.validBuy(_oPWP.getDiscountId(), _vTD);
            System.out.println("iValidQty From Exist vTD: " + iValidQty);
        	if (iValidQty == 0) //check valid buy qty from current TD
        	{
        		List vTMP = new ArrayList(1);
        		vTMP.add(_oTD);
        		iValidQty = PWPTool.validBuy(_oPWP.getDiscountId(), vTMP);        		
        		System.out.println("iValidQty From Current TD: " + iValidQty);
        	}        	
            //if (iValidQty > 0)
            //{
                //check if current TD is GET
                return PWPTool.getPrice(_oPWP.getDiscountId(),_vTD,_oTD,iValidQty);
            //}            
        }
        return null;
    }
    
    //-------------------------------------------------------------------------
    // MULTI UNIT DISCOUNT - DEPRECATED
    //-------------------------------------------------------------------------    

    /**
     * @deprecated
     * @param _sLocID
     * @param _sCustID
     * @param _sPTypeID
     * @param _sItemID
     * @param _dTrans
     * @return
     * @throws Exception
     */
    public static Discount findMultiDiscount(String _sLocID, 
											 String _sCustID, 
											 String _sPTypeID,
										     String _sItemID,
										     Date _dTrans)
    	throws Exception
    {
    	List vDisc = DiscountManager.getInstance().findMultiDiscount(_sLocID, _sItemID, _sCustID, _sPTypeID, _dTrans);	    																	
        if (vDisc != null && vDisc.size() > 0)
        {
            Discount oMulti = (Discount)vDisc.get(0);
            return oMulti;
        }	
        return null;
    }   
    
    /**
     * check if current discount is valid
     * 
     * @param _oDisc
     * @throws Exception
     */
    public static void validate(Discount _oDisc)
	    throws Exception
    {
        //validate customer code
	    if (StringUtil.isNotEmpty(_oDisc.getCustomers()))
        {
	        List vCode = StringUtil.toList(_oDisc.getCustomers(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                if (CustomerTool.getCustomerByCode(sCode) == null) throw new Exception("Customer " + sCode + " Not Found");
            }
        }
        //validate item code
        if (StringUtil.isNotEmpty(_oDisc.getItems()))
        {
            List vCode = StringUtil.toList(_oDisc.getItems(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                if (ItemTool.getItemByCode(sCode) == null) throw new Exception("Item " + sCode + " Not Found");
            }
        }
        //validate SKU
        
        //validate type && other rule   
        
        //validate exists
        Discount oExist = getExists(_oDisc);
        if(oExist != null)
        {
        	throw new Exception("Discount Rule Conflict With : " + oExist.getDiscountCode());
        }
    } 
}
