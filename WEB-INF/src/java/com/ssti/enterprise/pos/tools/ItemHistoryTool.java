package com.ssti.enterprise.pos.tools;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemStatusCode;
import com.ssti.enterprise.pos.om.ItemUpdateHistory;
import com.ssti.enterprise.pos.om.ItemUpdateHistoryPeer;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemHistoryTool.java,v 1.22 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemHistoryTool.java,v $
 * 
 * 2018-02-28
 * - extends UpdateHistoryTool
 * - move set*Id methods to UpdateHistoryTool
 * -
 * </pre><br>
 */
public class ItemHistoryTool extends UpdateHistoryTool
{
	private static Log log = LogFactory.getLog(ItemHistoryTool.class);
	
	static ItemHistoryTool instance = null;
	
	public static synchronized ItemHistoryTool getInstance() 
	{
		if (instance == null) instance = new ItemHistoryTool();
		return instance;
	}		
	
	private static void createItemUpdateHistory(String _sItemID, 
												String _sItemCode, 
												String _sField, 
											    Object _oOldValue, 
											    Object _oNewValue, 
											    String _sUserName, Connection _oConn)
    	throws Exception
    {
    	String sOldValue = "";
    	String sNewValue = "";
		
		if (_oOldValue != null) 
        {
            sOldValue = _oOldValue.toString();
            if (_oOldValue instanceof Number) sOldValue = CustomFormatter.fmt(sOldValue);
        }
		if (_oNewValue != null) 
        {
            sNewValue = _oNewValue.toString();
            if (_oNewValue instanceof Number) sNewValue = CustomFormatter.fmt(sNewValue);
        }		
        
        if (_sField.equals("UnitId")           ) { String[] aData = setUnitId            (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("PurchaseUnitId")   ) { String[] aData = setUnitId            (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("KategoriId")       ) { String[] aData = setKategoriId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("PreferedVendorId") ) { String[] aData = setVendorId  		  (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("TaxId")            ) { String[] aData = setTaxId             (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("PurchaseTaxId")    ) { String[] aData = setTaxId     		  (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("LastPurchaseCurr") ) { String[] aData = setCurrencyId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("WarehouseId")      ) { String[] aData = setLocationId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        
        if (_sField.equals("ItemStatusCodeId") ) { String[] aData = setItemStatusCodeId  (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("ItemStatus")       ) { String[] aData = setItemStatus        (sOldValue, sNewValue); sOldValue = aData[0]; sNewValue = aData[1];}

        if (_sField.endsWith("Account")        ) { String[] aData = setAccount(sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        
    	ItemUpdateHistory oHistory = new ItemUpdateHistory ();
    	oHistory.setItemId     (_sItemID);
    	oHistory.setItemCode   (_sItemCode);
    	oHistory.setUpdatePart (StringUtil.cut(_sField, 50, ""));
    	oHistory.setOldValue   (StringUtil.cut(sOldValue, 100, ""));
    	oHistory.setNewValue   (StringUtil.cut(sNewValue, 100, ""));
    	oHistory.setUpdateDate (new Date());
    	oHistory.setUserName   (_sUserName);
    	
    	StringBuilder oDescription = new StringBuilder ("Change ");
    	oDescription.append (_sField);
    	oDescription.append (" From : " );
    	oDescription.append (sOldValue);
    	oDescription.append (" To : " );
    	oDescription.append (sNewValue);
    	oDescription.append (" At : ");
    	oDescription.append (CustomFormatter.formatDateTime(oHistory.getUpdateDate()));    	
    	oDescription.append (" By : ");
    	oDescription.append (_sUserName);
		
		log.debug("History : " + oDescription);

    	oHistory.setUpdateDescription(StringUtil.cut(oDescription.toString(), 255, ""));
    	oHistory.setItemUpdateHistoryId(IDGenerator.generateSysID());
		oHistory.save(_oConn);    	
    }
    
	public static void createItemHistory(Item _oOldItem, 
										 Item _oNewItem, 
										 String  _sUserName, 
										 Connection _oConn)
    	throws Exception
    {
		
		List vFields = Item.getFieldNames();
		Class aOldItemClass = _oOldItem.getClass();
		Class aNewItemClass = _oNewItem.getClass();
		
		for (int i = 0; i < vFields.size(); i++)
		{
			String sFieldName = (String) vFields.get(i);
						
			if (!sFieldName.equals("ItemId") && !sFieldName.equals("UpdateDate"))
			{ 
				Method oOldMethod = aOldItemClass.getMethod ("get" + sFieldName, (Class[])null);
				Method oNewMethod = aNewItemClass.getMethod ("get" + sFieldName, (Class[])null);
				
				//log.debug("Field : "  + sFieldName + "   -  Method : " + oNewMethod);
				
				Object oOldValue = oOldMethod.invoke(_oOldItem, (Object[])null);
				Object oNewValue = oNewMethod.invoke(_oNewItem, (Object[])null);
            	
				//log.debug("Old Value : " + oOldValue + " - New Value : " + oNewValue);

				if (oNewValue != null)
				{
					if (oNewValue instanceof Number)
					{
						//log.debug("Field is Number");

						Number nNewValue = (Number) oNewValue;
						Number nOldValue = (Number) oOldValue;
						
						if (nOldValue != null && (nNewValue.doubleValue() != nOldValue.doubleValue()))
						{
							nNewValue = Calculator.precise(nNewValue, 2);
							nOldValue = Calculator.precise(nOldValue, 2);							
							createItemUpdateHistory(_oNewItem.getItemId(), _oNewItem.getItemCode(), 
							                        sFieldName, nOldValue, nNewValue, _sUserName, _oConn);
						}
					}
					else 
					{						
						if (!oNewValue.equals(oOldValue))
						{
							createItemUpdateHistory(_oNewItem.getItemId(), _oNewItem.getItemCode(), 
							                        sFieldName, oOldValue, oNewValue, _sUserName, _oConn);
						}
					}
				}
			}
		}	
	}
	
	public static String[] setItemStatus (String _sOldValue, String _sNewValue) throws Exception
    { 
        if (_sOldValue.equals("false")) _sOldValue = "Non Active";
        if (_sNewValue.equals("false")) _sNewValue = "Non Active";
        if (_sOldValue.equals("true")) _sOldValue = "Active";
        if (_sNewValue.equals("true")) _sNewValue = "Active";        
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;       
    }    

	public static String[] setItemStatusCodeId (String _sOldValue, String _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	ItemStatusCode oOld = ItemStatusCodeTool.getItemStatusCodeByID(_sOldValue, _oConn);
    	ItemStatusCode oNew = ItemStatusCodeTool.getItemStatusCodeByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getStatusCode();
        if (oNew != null) _sNewValue = oNew.getStatusCode();     	    
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    } 
	
	//finder
	public static List findData (String _sItemID, 
	  							 String _sStartDate, 
	  							 String _sEndDate, 
								 String _sUpdatedPart, 
								 String _sKeywords,
								 int _iCondition,
								 int _iSortBy)
		throws Exception
	{
		Date dStart = CustomParser.parseDate (_sStartDate);
		Date dEnd   = CustomParser.parseDate (_sEndDate);
		dStart = DateUtil.getStartOfDayDate(dStart);
		dEnd = DateUtil.getEndOfDayDate(dEnd);
		Criteria oCrit = new Criteria();

		if (dStart != null) {
        	oCrit.add(ItemUpdateHistoryPeer.UPDATE_DATE, dStart, Criteria.GREATER_EQUAL);   
		}
		if (dEnd != null) {
        	oCrit.and(ItemUpdateHistoryPeer.UPDATE_DATE, dEnd, Criteria.LESS_EQUAL);   
		}
		if (_sItemID != null && !_sItemID.equals("")) {
        	oCrit.add(ItemUpdateHistoryPeer.ITEM_ID, _sItemID);   
		}
		if (_sUpdatedPart != null && !_sUpdatedPart.equals("") && !_sUpdatedPart.equals(" ")) {
        	oCrit.add(ItemUpdateHistoryPeer.UPDATE_PART, _sUpdatedPart);   
		}
	    //define sorting criteria
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(ItemUpdateHistoryPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
			oCrit.addJoin (ItemPeer.ITEM_ID, ItemUpdateHistoryPeer.ITEM_ID);	
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
		else if (_iSortBy == 4) {
			oCrit.addDescendingOrderByColumn(ItemUpdateHistoryPeer.UPDATE_DATE);
		}
		
		if (!_sKeywords.equals("")) {
			if (_iCondition == 1) {
				oCrit.add(ItemUpdateHistoryPeer.ITEM_CODE,
					(Object) SqlUtil.like (ItemPeer.ITEM_CODE, _sKeywords), Criteria.CUSTOM);
			} 
			else if (_iCondition == 2) {
				oCrit.addJoin (ItemPeer.ITEM_ID, ItemUpdateHistoryPeer.ITEM_ID);	
				oCrit.add(ItemPeer.ITEM_NAME,
					(Object) SqlUtil.like (ItemPeer.ITEM_NAME, _sKeywords, false, true), Criteria.CUSTOM);
			}
			else if (_iCondition == 3) {
				oCrit.addJoin (ItemPeer.ITEM_ID, ItemUpdateHistoryPeer.ITEM_ID);	
				oCrit.add(ItemPeer.DESCRIPTION,
					(Object) SqlUtil.like (ItemPeer.DESCRIPTION, _sKeywords, false, true), Criteria.CUSTOM);
			}
			else if (_iCondition == 4) {
				oCrit.add(ItemUpdateHistoryPeer.UPDATE_DESCRIPTION,
					(Object) SqlUtil.like (ItemUpdateHistoryPeer.UPDATE_DESCRIPTION, _sKeywords, false, true), Criteria.CUSTOM);
			}
			else if (_iCondition == 5) {
				oCrit.add(ItemUpdateHistoryPeer.USER_NAME, _sKeywords);
			}	
		}
		log.debug(oCrit);
		return ItemUpdateHistoryPeer.doSelect(oCrit);
	}			
	
	//Report filter methods
	//by item
	public static List getItemList (List _vData) 
    	throws Exception
    {		
		List vHist = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			ItemUpdateHistory oHist = (ItemUpdateHistory) _vData.get(i);			
			if (!isItemInList (vHist, oHist)) {
				vHist.add (oHist);
			}
		}
		return vHist;
	}
	
	private static boolean isItemInList (List _vHist, ItemUpdateHistory _oHist) 
	{
		for (int i = 0; i < _vHist.size(); i++) {
			ItemUpdateHistory oHist = (ItemUpdateHistory) _vHist.get(i);			
			if (oHist.getItemId().equals(_oHist.getItemId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByItemID (List _vHist, String _sItemID) 
    	throws Exception
    { 
		List vHist = new ArrayList(_vHist); 
		ItemUpdateHistory oHist = null;
		Iterator oIter = vHist.iterator();
		while (oIter.hasNext()) {
			oHist = (ItemUpdateHistory ) oIter.next();
			if (!oHist.getItemId().equals(_sItemID)) {
				oIter.remove();
			}
		}
		return vHist;
	}		
	
	//by part
	public static List getUpdatePartList (List _vData) 
    	throws Exception
    {		
		List vHist = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			ItemUpdateHistory oHist = (ItemUpdateHistory) _vData.get(i);			
			if (!isUpdatePartInList (vHist, oHist)) {
				vHist.add (oHist);
			}
		}
		return vHist;
	}
	
	private static boolean isUpdatePartInList (List _vHist, ItemUpdateHistory _oHist) 
	{
		for (int i = 0; i < _vHist.size(); i++) {
			ItemUpdateHistory oHist = (ItemUpdateHistory) _vHist.get(i);			
			if (oHist.getUpdatePart().equals(_oHist.getUpdatePart())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByUpdatePart (List _vHist, String _sUpdatePart) 
    	throws Exception
    { 
		List vHist = new ArrayList(_vHist); 
		ItemUpdateHistory oHist = null;
		Iterator oIter = vHist.iterator();
		while (oIter.hasNext()) {
			oHist = (ItemUpdateHistory ) oIter.next();
			if (!oHist.getUpdatePart().equals(_sUpdatePart)) {
				oIter.remove();
			}
		}
		return vHist;
	}			
            
    public static ItemUpdateHistory getItemUpdateHistoryByID (String _sHistoryID, Connection _oConn)
        throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemUpdateHistoryPeer.ITEM_UPDATE_HISTORY_ID, _sHistoryID);
     	oCrit.setLimit(1);				
		
		List vData = ItemUpdateHistoryPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (ItemUpdateHistory) vData.get(0);
		}
		return null;        
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // sync methods
    ///////////////////////////////////////////////////////////////////////////    
    
    /**
     * 
     * @return List of Updated Item History
     * @throws Exception
     */
    public static List getUpdatedItemHistory(Date _dLast)
		throws Exception
	{
    	if (_dLast == null)
    	{
    		_dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
    	}
		Criteria oCrit = new Criteria();
		if (_dLast  != null) 
		{
        	oCrit.add(ItemUpdateHistoryPeer.UPDATE_DATE, _dLast, Criteria.GREATER_EQUAL);   
		}
		return ItemUpdateHistoryPeer.doSelect(oCrit);
	}	
}