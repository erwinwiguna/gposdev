package com.ssti.enterprise.pos.tools;

import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import org.apache.torque.util.Criteria;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerType;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemMinPrice;
import com.ssti.enterprise.pos.om.ItemMinPricePeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemStatusCodeTool.java,v 1.12 2004/12/31 22:06:55 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemStatusCodeTool.java,v $
 * 
 * 2016-10-25
 * - add applyCustomPricing method
 * 
 * </pre><br>
 */
public class ItemMinPriceTool extends BaseTool 
{
	static ItemMinPriceTool instance = null;
	
	public static synchronized ItemMinPriceTool getInstance() 
	{
		if (instance == null) instance = new ItemMinPriceTool();
		return instance;
	}	
	
    public static ItemMinPrice getItemMinPrice (String _sItemID, String _sLocID, String _sCustTypeID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(ItemMinPricePeer.ITEM_ID, _sItemID);
        if (StringUtil.isNotEmpty(_sLocID))
        {
            oCrit.add(ItemMinPricePeer.LOCATION_ID, _sLocID);        	
        }
        else
        {        	
            oCrit.add(ItemMinPricePeer.LOCATION_ID, "");        	
        }
        if (StringUtil.isNotEmpty(_sCustTypeID))
        {
            oCrit.add(ItemMinPricePeer.CUSTOMER_TYPE_ID, _sCustTypeID);        	
        }
        else
        {        	
            oCrit.add(ItemMinPricePeer.CUSTOMER_TYPE_ID, "");        	
        }
        List vData = ItemMinPricePeer.doSelect(oCrit);
        if (vData.size() > 0)
        {
        	return (ItemMinPrice)vData.get(0);
        }
		return null;
    }
    
    public static double getMinPrice (String _sItemID, String _sLocID, String _sCustID)
	    throws Exception
	{
	    Customer oCust = CustomerTool.getCustomerByID(_sCustID);
	    String sTypeID = "";
	    if (oCust != null)
	    {
	    	sTypeID = oCust.getCustomerTypeId();
	    }
	    
        Criteria oCrit = new Criteria();
        oCrit.add(ItemMinPricePeer.ITEM_ID, _sItemID);
        oCrit.add(ItemMinPricePeer.LOCATION_ID, _sLocID);        	
        oCrit.or(ItemMinPricePeer.LOCATION_ID, "");  
        oCrit.add(ItemMinPricePeer.CUSTOMER_TYPE_ID, sTypeID);        	
        oCrit.or(ItemMinPricePeer.CUSTOMER_TYPE_ID, "");        	 
        List vData = ItemMinPricePeer.doSelect(oCrit);
	    if (vData.size() > 0)
	    {
	    	return ((ItemMinPrice)vData.get(0)).getMinPrice().doubleValue();
	    }
		return 0;
	}
    
    public static List findData (String _sItemCode, String _sLocCode, String _sCustCode)
	    throws Exception
	{
    	String sItemID = "";
    	String sLocID = "";
	    String sTypeID = "";
    	
    	Item oItem = ItemTool.getItemByCode(_sItemCode);
    	if (oItem != null)
    	{
    		sItemID = oItem.getItemId();
    	}
    	Location oLoc = LocationTool.getLocationByCode(_sLocCode);
    	if (oLoc != null)
    	{
    		sLocID = oLoc.getLocationId();
    	}
	    CustomerType oCust = CustomerTypeTool.getCustomerTypeByCode(_sCustCode);
	    if (oCust != null)
	    {
	    	sTypeID = oCust.getCustomerTypeId();
	    }
	    
	    Criteria oCrit = new Criteria();
	    if (StringUtil.isNotEmpty(sItemID))
	    {
	    	oCrit.add(ItemMinPricePeer.ITEM_ID, sItemID);
	    }
	    if (StringUtil.isNotEmpty(sLocID))
	    {
		    oCrit.add(ItemMinPricePeer.LOCATION_ID, sLocID);        	
		    oCrit.or(ItemMinPricePeer.LOCATION_ID, "");  
	    }
	    if (StringUtil.isNotEmpty(sTypeID))
	    {
		    oCrit.add(ItemMinPricePeer.CUSTOMER_TYPE_ID, sTypeID);        	
		    oCrit.or(ItemMinPricePeer.CUSTOMER_TYPE_ID, "");        	 
	    }
		return ItemMinPricePeer.doSelect(oCrit);
	}
    
    public static void delete(String _sID)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(ItemMinPricePeer.ITEM_MIN_PRICE_ID, _sID);
    	ItemMinPricePeer.doDelete(oCrit);
    }
        
    /**
     * method to use custom pricing
     * 
     * @param _oTR
     * @param _vTD
     * @param _oTD
     */
    public static void applyCustomPricing(SalesTransaction _oTR, List _vTD, SalesTransactionDetail _oTD)
    {
    	String sTPL = PreferenceTool.getSalesCustomPricingTpl();
    	if(PreferenceTool.getSalesCustomPricingUse() && 
    		StringUtil.isNotEmpty(sTPL) && _oTR != null && _oTD != null)
    	{
    		try
    		{
    			String s_TPL_PATH = "/print/modules/";
    	    	String s_VEL_PROP = "file.resource.loader.path";
    	    	
    			Properties oProp = new Properties();
    		    oProp.setProperty(s_VEL_PROP, s_TPL_PATH); 	
    			Velocity.init(oProp);		
    			
    			StringWriter oWriter = new StringWriter ();
    			VelocityContext oCtx = new VelocityContext();

    	    	Context oGlobal = TurbinePull.getGlobalContext();		
    	    	for (int i = 0; i < oGlobal.getKeys().length; i++)
    	    	{
    	    		String sKey = (String) oGlobal.getKeys()[i];
    	    		oCtx.put(sKey, oGlobal.get(sKey));
    	    	}
    			
    	        oCtx.put ("oTR", _oTR);
    	        oCtx.put ("oTD", _oTD);
    	        oCtx.put ("vTD", _vTD);    	        
    	        oCtx.put ("out", System.out);
    	        Velocity.mergeTemplate(s_TPL_PATH + sTPL, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );    						
    	        
    	        System.out.println(oWriter.toString());
    		} 
    		catch (Exception e) 
    		{
    			log.error(e);
    			e.printStackTrace();
			}
    	}
    }    
}
