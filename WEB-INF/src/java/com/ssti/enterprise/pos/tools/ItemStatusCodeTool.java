package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.ItemStatusCodeManager;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemStatusCode;
import com.ssti.enterprise.pos.om.ItemStatusCodePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemStatusCodeTool.java,v 1.12 2004/12/31 22:06:55 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemStatusCodeTool.java,v $
 * Revision 1.12  2004/12/31 22:06:55  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/05/30 05:12:53  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/05/09 06:50:27  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/02/23 14:44:43  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class ItemStatusCodeTool extends BaseTool 
{
	public static List getAllItemStatusCode()
		throws Exception
	{
		return ItemStatusCodeManager.getInstance().getAllItemStatusCode();
    }

	public static ItemStatusCode getItemStatusCodeByID(String _sID)
		throws Exception
	{
		return ItemStatusCodeManager.getInstance().getItemStatusCodeByID(_sID, null);
	}
	
	public static ItemStatusCode getItemStatusCodeByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return ItemStatusCodeManager.getInstance().getItemStatusCodeByID(_sID, _oConn);
    }

	public static String getCodeByID(String _sID)
    	throws Exception
    {
        ItemStatusCode oCode = getItemStatusCodeByID(_sID);
        if (oCode != null)
        {
            return oCode.getStatusCode();    
        }
		return "";    
    }    
    
    public static int getItemVariableFromID (String _sID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_ID, _sID);
        oCrit.addJoin(ItemPeer.ITEM_STATUS_CODE_ID,ItemStatusCodePeer.ITEM_STATUS_CODE_ID);
		ItemStatusCodePeer.doSelect(oCrit);
		List vData = ItemStatusCodePeer.doSelect(oCrit);
        if (vData.size() > 0) 
        {
			ItemStatusCode oItemStatus = (ItemStatusCode) vData.get(0);
			return oItemStatus.getVariable();
		}
		return 0;
    }
    
    public static String getIDByCode(String _sCode)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemStatusCodePeer.STATUS_CODE, _sCode);
        List vData = ItemStatusCodePeer.doSelect (oCrit);
        if (vData.size() > 0) {
        	return ((ItemStatusCode)vData.get(0)).getItemStatusCodeId();
		}
		return "";
	}
}
