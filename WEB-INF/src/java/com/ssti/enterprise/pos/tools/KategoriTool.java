package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.KategoriManager;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.KategoriAccount;
import com.ssti.enterprise.pos.om.KategoriPeer;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: KategoriTool.java,v 1.36 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: KategoriTool.java,v $
 * Revision 1.36  2009/05/04 02:03:51  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class KategoriTool extends BaseTool 
{
	private static Log log = LogFactory.getLog(KategoriTool.class);
	
	private static final String s_EXCEL_DELIMITER  = ",";
	private static final String s_REPORT_DELIMITER = " -> ";	
	
	public static List getAllKategori()
    	throws Exception
    {
    	return KategoriManager.getInstance().getAllKategori();
	}

	public static List getAllKategoriSortByLevel()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addAscendingOrderByColumn(KategoriPeer.KATEGORI_LEVEL);
		oCrit.addAscendingOrderByColumn(KategoriPeer.KATEGORI_CODE);		
		return KategoriPeer.doSelect(oCrit);
	}
	
	public static Kategori getKategoriByID(String _sID)
    	throws Exception
    {
		return KategoriManager.getInstance().getKategoriByID(_sID, null);
	}

	public static Kategori getKategoriByID(String _sID, Connection _oConn)
		throws Exception
	{
		return KategoriManager.getInstance().getKategoriByID(_sID, _oConn);
	}
		
	public static String getCodeByID(String _sID)
		throws Exception
	{
		Kategori oKategori = getKategoriByID(_sID);
		if (oKategori != null) return oKategori.getKategoriCode();
		return "";
	}	
	
	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		Kategori oKategori = getKategoriByID (_sID);
		if (oKategori != null) return oKategori.getDescription();
		return "";
	}

	public static String getIDByDescription(String _sDesc)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.setIgnoreCase(true);
		oCrit.add(KategoriPeer.DESCRIPTION, (Object) _sDesc, Criteria.LIKE);
		log.debug(oCrit);
		List vData = KategoriPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((Kategori) vData.get(0)).getKategoriId();
		}
		return "";
	}

	public static List findByInternalCode(String _sIntCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(KategoriPeer.INTERNAL_CODE, (Object) _sIntCode, Criteria.LIKE);
		log.debug(oCrit);
		return KategoriPeer.doSelect(oCrit);
	}
	
	/**
	 * get parent by current kategori id
	 * 
	 * @param _sKategoriID
	 * @return parent kategori
	 * @throws Exception
	 */
	public static Kategori getParent (String _sKategoriID)
		throws Exception
	{
		Kategori oKategori = getKategoriByID (_sKategoriID);
		if (oKategori != null)
		{
			if (StringUtil.isNotEmpty(oKategori.getParentId()))
			{
				return getKategoriByID (oKategori.getParentId());
			}
		}
		return null;
	}
	
	/**
	 * get root parent of a certain kategori
	 * 
	 * @param _sKategoriID
	 * @return root parent kategori
	 * @throws Exception
	 */
	public static Kategori getRootParent (String _sKategoriID)
		throws Exception
	{
		Kategori oKategori = getKategoriByID (_sKategoriID);
		if (oKategori != null)
		{
			if (StringUtil.isNotEmpty(oKategori.getParentId()))
			{
				return getRootParent(oKategori.getParentId());
			}
		}
		return oKategori;
	}
	
	/**
	 * get all kategori at certain level 
	 * 
	 * @param _iLevel
	 * @return list of kategori at level
	 * @throws Exception
	 */
	public static List getKategoriAtLevel(int _iLevel)
	    throws Exception
	{
        return KategoriManager.getInstance().getKategoriAtLevel(_iLevel);
	}	
	
	/**
	 * get kategori by code and parent id
	 * this method must be the only accessible method to get a kategori by code
	 * because kategori code maybe the same among several categories
	 * 
	 * @param _sCode
	 * @param _sParentID
	 * @return kategori
	 * @throws Exception
	 */
	public static Kategori getKategoriByCodeAndParentID(String _sCode, String _sParentID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(KategoriPeer.KATEGORI_CODE, _sCode);
	    oCrit.add(KategoriPeer.PARENT_ID, _sParentID);
	    List vData = KategoriPeer.doSelect (oCrit);
	    if (vData.size() > 0) 
	    {
	    	return ((Kategori)vData.get(0));
		}
	    return null;
	}
	
	/**
	 * getChildren from ehcache
	 * 
	 * @param _sParentID
	 * @return children list
	 * @throws Exception
	 */
	
	public static List getChildren(String _sParentID)
		throws Exception
	{
		return KategoriManager.getInstance().getChildren(_sParentID, null);
	}

	public static boolean hasChild (String _sParentID)
		throws Exception
	{
	    List vData = getChildren (_sParentID);
	    if (vData.size() > 0) return true;
	    return false;
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _vKat
	 * @return parent id list
	 * @throws Exception
	 */
	public static List getParentIDList (String _sID, List _vKat)
		throws Exception
	{
		if (_vKat == null)
		{
			_vKat = new ArrayList(4);
			_vKat.add(_sID);
		}
		Kategori oKat = getKategoriByID(_sID);
		if (oKat != null && StringUtil.isNotEmpty(oKat.getParentId()))
		{
			_vKat.add(oKat.getParentId());
			getParentIDList(oKat.getParentId(), _vKat);
		}
		return _vKat;
	}
	
	/**
	 * get list of children id of a kategori
	 * 
	 * @param _sParentID
	 * @return child id list
	 * @throws Exception
	 */
    public static List getChildIDList (String _sParentID)
		throws Exception
	{
	    return getChildIDList(new ArrayList(), _sParentID);
	}
	
    /**
     * get list of children id of a kategori
     * 
     * @param _vID
     * @param _sParentID
     * @return child id list 
     * @throws Exception
     */
	public static List getChildIDList (List _vID, String _sParentID)
		throws Exception
	{
		List vData = getChildren(_sParentID);
		for (int i = 0; i < vData.size(); i++)
		{
			Kategori oKategori = (Kategori) vData.get(i);
			getChildIDList (_vID, oKategori.getKategoriId());
			_vID.add( oKategori.getKategoriId() );
		}
		return _vID;
	}  	
	
	/**
	 * display kategori description from the first parent
	 * i.e car -> accessories -> perfume used in reports
	 * 
	 * @param _sID
	 * @return long description
	 * @throws Exception
	 */
	public static String getLongDescription(String _sID)
    	throws Exception
    {
		Kategori oKategori = getKategoriByID(_sID);		
		if (oKategori != null) 
		{
			StringBuilder oSB = new StringBuilder(oKategori.getDescription());
			while (oKategori != null && oKategori.getParentId() != null && !oKategori.getParentId().equals(""))
			{
				oKategori = getKategoriByID (oKategori.getParentId());
				if (oKategori != null)
				{	
					oSB.insert (0, s_REPORT_DELIMITER);
					oSB.insert (0, oKategori.getDescription());
				}	
			}
			return oSB.toString();
		}
		return "";
	}

	/**
	 * display kategori code from the first parent
	 * i.e 01,02,05
	 * 
	 * @param _sID
	 * @return string
	 * @throws Exception
	 */	
	public static String getInternalCode (Kategori _oKategori)
		throws Exception
	{
		if (_oKategori != null) 
		{
			StringBuilder oSB = new StringBuilder(_oKategori.getKategoriCode());
			Kategori oParent = new Kategori();
			oParent.setParentId(_oKategori.getParentId());
			while (oParent != null && StringUtil.isNotEmpty(oParent.getParentId()))
			{
				oParent = getKategoriByID (oParent.getParentId());
				if (oParent != null)
				{	
					oSB.insert (0, oParent.getKategoriCode());
				}	
			}
			log.debug("INTERNAL CODE : " + oSB.toString());
			return oSB.toString();
		}
		return "";
	}	
	
	/**
	 * display kategori code from the first parent
	 * i.e 01,02,05
	 * 
	 * @param _sID
	 * @return string
	 * @throws Exception
	 */	
	public static String getParentToChildCodeByID (String _sID)
		throws Exception
	{
		Kategori oKategori = getKategoriByID(_sID);		
		if (oKategori != null) 
		{
			StringBuilder oSB = new StringBuilder(oKategori.getKategoriCode());
			while (oKategori != null && oKategori.getParentId() != null && !oKategori.getParentId().equals(""))
			{
				oKategori = getKategoriByID (oKategori.getParentId());
				if (oKategori != null)
				{	
					oSB.insert (0, s_EXCEL_DELIMITER);
					oSB.insert (0, oKategori.getKategoriCode());
				}	
			}
			return oSB.toString();
		}
		return "";
	}
	
	/**
	 * get kategori id from a string such "01,03,05" 
	 * will parse the token then get each object by code and parent id
	 * 
	 * @param _sKategori
	 * @return kategori id
	 * @throws Exception
	 */
	public static String getKategoriIDFromCodeList (String _sKategori)
		throws Exception
	{		
		String sPrevParentID = "";
					
		if (_sKategori != null)
		{
			StringTokenizer oTokenizer = new StringTokenizer(_sKategori, s_EXCEL_DELIMITER);
			int i = 0;
			while (oTokenizer.hasMoreTokens())
			{
				Kategori oKategori = getKategoriByCodeAndParentID (oTokenizer.nextToken(), sPrevParentID);
				if (oKategori != null) sPrevParentID = oKategori.getKategoriId();
				i++;
			}
		}
		return sPrevParentID;
	}

	/**
	 * get category code from parent to last selected child
	 * used for generating automatic item code
	 * 
	 * @param _oKategori
	 * @return code
	 * @throws Exception
	 */
	public static String getItemCodeByCategory(Kategori _oKategori)
		throws Exception
	{
		String sCodeTemp = _oKategori.getKategoriCode();
		Kategori oKategori = getKategoriByID(_oKategori.getParentId());
		if(oKategori != null)
		{
			sCodeTemp = getItemCodeByCategory(oKategori) + "" + sCodeTemp;
		}
		return sCodeTemp;
	}

	public static int setChildLevel(String _sParentID)
		throws Exception
	{
		int iLevel = 1;
		if (StringUtil.isNotEmpty (_sParentID))
		{
			Kategori oParent = getKategoriByID(_sParentID);
			iLevel = oParent.getKategoriLevel() + 1;
		}
		return iLevel;
	}
	
	/**
	 * recursively delete a kategori 
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteKategoriByID(String _sID)
		throws Exception
	{	
		List vChild = getChildren (_sID);
		for (int i = 0; i< vChild.size(); i++ )
		{
			Kategori oKategori = (Kategori) vChild.get(i);
			deleteKategoriByID (oKategori.getKategoriId());
		}
		Criteria oCrit = new Criteria();
		oCrit.add(KategoriPeer.KATEGORI_ID, _sID);
		KategoriManager.getInstance().refreshCache(_sID);
		KategoriPeer.doDelete(oCrit);
	}
	
	///////////////////////////////////////////////////////////////////////////
	//generate javascript tree
	///////////////////////////////////////////////////////////////////////////	
	
	private static List filterByLevel(List _vData, int _iLevel)
	{
		List vCategoryAtLevel = new ArrayList();
		for (int i = 0; i < _vData.size(); i++)
		{
			Kategori oKategori = (Kategori) _vData.get(i);
			if (oKategori.getKategoriLevel() == _iLevel) vCategoryAtLevel.add(oKategori);
		}
		return vCategoryAtLevel;
	}

	private static List filterByParentID(List _aData, String _sParentID)
	{
		List vChildren = new ArrayList ();
		for (int i = 0; i < _aData.size(); i++)
		{
			Kategori oKategori = (Kategori) _aData.get(i);
			if (oKategori.getParentId().equals(_sParentID)) vChildren.add(oKategori);
		}
		return vChildren;
	}

	private static String createVarStr(Kategori _oKategori, String _sParentNode, String _sNode)
	{
		StringBuilder oSpace = new StringBuilder("");
		if (log.isDebugEnabled()) for (int i = 1; i < _oKategori.getKategoriLevel(); i++) oSpace.append("    ");
		
		StringBuilder oStr = new StringBuilder();
		oStr.append (oSpace);
		oStr.append ("var node");
		oStr.append (_sNode);
		oStr.append (" = new TreeNode (");
		oStr.append (_sNode);
		oStr.append (", '");
		oStr.append (_oKategori.getKategoriCode());
		oStr.append (" ");		
		oStr.append (_oKategori.getDescription());
		oStr.append ("', treeImg, '");
		oStr.append (_oKategori.getKategoriId());
		oStr.append ("');\n");
		
		if (StringUtil.isNotEmpty(_sParentNode))
		{
			oStr.append (oSpace);
			oStr.append ("node");
			oStr.append (_sParentNode);
			oStr.append (".addChild(node");
			oStr.append (_sNode);
			oStr.append (");\n");
		}
		else 
		{
			oStr.append ("rootNode.addChild(node");
			oStr.append (_sNode);
			oStr.append (");\n");
		}
		return oStr.toString();
	}

	private static void buildTree(List _vAll,  List _vLevel, String _sParentNode, int _iLevel, StringBuilder _oJS)
	{
		for (int i = 0; i < _vLevel.size(); i++)
		{
			int iDigit = 3; if (_iLevel >= 2) iDigit = 2;			
			String sNode = _sParentNode + Integer.valueOf(_iLevel).toString() + 
				StringUtil.formatNumberString(Integer.valueOf(i).toString(), iDigit);
			
			Kategori oKategori = (Kategori) _vLevel.get(i);
			_oJS.append (createVarStr (oKategori, _sParentNode, sNode));
			List vChildren = filterByParentID (_vAll, oKategori.getKategoriId());
			if (vChildren.size() > 0)
			{
				int iChildLevel = _iLevel + 1;
				buildTree(_vAll, vChildren, sNode, iChildLevel, _oJS);
			}
		}
	}
	
	/**
	 * create Javascript tree
	 * @return js tree
	 * @throws Exception
	 */
	//TODO: current JS tree may not exceed certain number so we need to change the code 
	public static String createJSTree()
		throws Exception
	{
		List aData = getAllKategori();
		StringBuilder oJS = new StringBuilder();
		oJS.append("rootNode = new TreeNode(-1,'Kategori', userIcon,'-1');\n\n");
		List aKategoriLevel1 = filterByLevel(aData, 1);
		buildTree(aData, aKategoriLevel1, "", 1, oJS);
		return oJS.toString();
	}
	
	public static String getKategoriJSTree()
		throws Exception
	{
		return KategoriManager.getInstance().getKategoriJSTree();
	}

	/**
	 * category tree list
	 * 
	 * @param _sID
	 * @param _bFromCache
	 * @return kategori tree list
	 * @throws Exception
	 */
	
	public static List getKategoriTreeList(String _sID, boolean _bFromCache)
		throws Exception
	{
		if (_bFromCache)
		{
			return KategoriManager.getInstance().getKategoriTreeList(null, _sID);
		}
		return getKategoriTreeList(null, _sID);
	}
    
	/**
	 * recursive method to get the category object graph
	 * 
	 * @param _vData
	 * @param _sID
	 * @return kategori tree list
	 * @throws Exception
	 */
	public static List getKategoriTreeList(List _vData, String _sID)
		throws Exception
	{	    
	    if (_vData == null || _vData.size() < 1)
	    {
	        _vData = new ArrayList();
	        
	        if (StringUtil.isNotEmpty(_sID)) 
	        {
	            _vData.add(getKategoriByID(_sID));
	        }
	    }
		List vChildren = getChildren(_sID);
		for (int i = 0; i < vChildren.size(); i++)
		{
			Kategori oKategori = (Kategori) vChildren.get(i);
			_vData.add(oKategori);
            if (hasChild(oKategori.getKategoriId())) 
            {
			    getKategoriTreeList (_vData, oKategori.getKategoriId());
		    }
		}
		return _vData;
	}		

	public static void updateHasChild()
		throws Exception
	{
		try 
		{
			String sSQL = "UPDATE kategori SET has_child = false WHERE kategori_id NOT IN (SELECT parent_id FROM kategori)";
			SqlUtil.executeStatement(sSQL);
			
			sSQL = "UPDATE kategori SET has_child = true WHERE kategori_id IN (SELECT parent_id FROM kategori)";
			SqlUtil.executeStatement(sSQL);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
	
	//-------------------------------------------------------------------------
	// Kategori Account
	//-------------------------------------------------------------------------
	public static KategoriAccount getKategoriAccount(String _sKategoriID)
		throws Exception
	{
		return KategoriManager.getInstance().getKategoriAccount(_sKategoriID, null);
	}
	
	public static KategoriAccount findKategoriAccount(String _sKategoriID)
		throws Exception
	{
		KategoriAccount oKatAcc = getKategoriAccount(_sKategoriID);
		if (oKatAcc != null)
		{
			return oKatAcc;
		}
		else
		{
			Kategori oParent = getParent(_sKategoriID);
			if (oParent != null)
			{
				return findKategoriAccount(oParent.getKategoriId());
			}
		}
		return null;
	}
}