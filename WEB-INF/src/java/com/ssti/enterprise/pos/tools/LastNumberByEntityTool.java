package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.BasePeer;

import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: LastNumberTool.java,v 1.26 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: LastNumberTool.java,v $
 * Revision 1.26  2009/05/04 02:03:51  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class LastNumberByEntityTool extends LastNumberTool
{
	private final Log log = LogFactory.getLog (LastNumberByEntityTool.class);
	
	protected String s_TABLE = "ctl_customer_trans";
	
	protected int iType = 0;
	protected String sPrefix = "";
	protected String sFormat = "";
	protected String sLocCode = "";
	protected Connection m_oConn = null;
	
	/*
	public EntityTransNoTool(int _iType, String _sPrefix, String _sFormat, String _sLocCode, Connection _oConn)
		throws Exception
	{
		this.iType = _iType;
		this.sPrefix = _sPrefix;
		this.sFormat = _sFormat;
		this.sLocCode = _sLocCode;
		this.m_oConn = _oConn;
		validate(_oConn);
	}*/
	
    public synchronized String get()
		throws Exception
	{
		
		String sTransNo = "";
		try
		{
			sTransNo = generateNo();
			updateLastNo(sTransNo);
		}
		catch (Exception _oEx)
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			throw new Exception ("Last number generation failed, ERROR: " + _oEx.getMessage(), _oEx);
		}    	
		return sTransNo;
	}
    
    private String getLastNo()
		throws Exception
	{             
    	StringBuilder sSQL = new StringBuilder();
    	sSQL.append("SELECT last_number FROM ").append(s_TABLE)
    		.append(" WHERE trans_type = ").append(iType)
    		.append(" AND prefix = '").append(sPrefix).append("'")
    		.append(" FOR UPDATE");

    	List vData = BasePeer.executeQuery(sSQL.toString(), 0, -1, false, m_oConn);
		if (vData.size() > 0)
		{
			Record oRecord = (Record) vData.get(0);
			return oRecord.getValue(1).asString();		        
		}
	    return null;
	}
    
    /**
     * Generate trans no
     * 
     * @param _sFormat
     * @param iType
     * @param m_oConn
     * @return generated no
     * @throws Exception
     */
    private String generateNo()
		throws Exception
	{
    	//get number format string first char i.e "x"
    	String sNumberChar = s_FORMAT_NO.substring(0,1);    	
		String sLastNo = getLastNo();
		String sTransNo = sFormat;

		if (log.isWarnEnabled())
		{
	    	//log.debug ("sNoStart : " + sNoStart);
			//log.debug ("sTransNo : " + sTransNo);
			log.info ("sFormat  : " + sFormat);
			log.info ("sLastNo1 : " + sLastNo);
			log.info ("separator : " + s_SEPARATOR + "  length : " + s_SEPARATOR.length());
		}
		
		int iLastNo = 0;
		if (StringUtil.isNotEmpty(sLastNo))
		{
            String sSeparator = s_SEPARATOR;
            if (!StringUtil.contains(sFormat, sSeparator)) //find another separator possibliites
            {
                if(StringUtil.contains(sFormat, s_STRIP)) sSeparator = s_STRIP;
                if(StringUtil.contains(sFormat, s_UNDER)) sSeparator = s_UNDER;
                if(StringUtil.contains(sFormat, s_SLASH)) sSeparator = s_SLASH;   
                if(StringUtil.contains(sFormat, s_DOT))   sSeparator = s_DOT;                   
                log.info ("separator2 : " + sSeparator + "  length : " + sSeparator.length());
            }       

			//parse the last number i.e 10203
			iLastNo = Integer.parseInt(sLastNo.substring(sLastNo.length() - s_FORMAT_NO.length()));
			log.info ("iLastNo " + iLastNo);
			
			//construct max no String -> if s_FORMAT_NO = "xxxxx" then sMaxNo = "99999"
			String sMaxNo = sFormat.substring(sFormat.indexOf(sNumberChar));
			sMaxNo = sMaxNo.replaceAll(sNumberChar, "9");

            if(StringUtil.containsIgnoreCase(sFormat, s_FORMAT_DD)) //if format contains day i.e PRS/yymmdd/xxxxx then apply daily reset
            {
            	int iDD = sFormat.indexOf(s_FORMAT_DD);
            	if(sLocCode.length() > 2) iDD = iDD + (sLocCode.length() - 2);
            	if(sLocCode.length() == 1) iDD = iDD - 1;            	
            	
				String sTDD = StringUtil.formatNumberString(Integer.toString (Calendar.getInstance().get(Calendar.DAY_OF_MONTH)), 2);
            	String sLDD = sLastNo.substring(iDD, iDD + s_FORMAT_DD.length());
            	
    			log.info ("Today Date sTDD : " + sTDD);
    			log.info ("Last  Date sLDD : " + sLDD);
    			
            	if(!StringUtil.equalsIgnoreCase(sTDD, sLDD)) iLastNo = 0; //if today DD != Last No DD reset No            	
            }
            else //reset monthly
            {            
	            if(StringUtil.containsIgnoreCase(sFormat, s_FORMAT_MO)) //if format contains month i.e PRS/yymm/xxxxx then apply monthly reset
	            {            	
	            	int iMO = sFormat.indexOf(s_FORMAT_MO);
	            	if(sLocCode.length() > 2) iMO = iMO + (sLocCode.length() - 2);
	            	if(sLocCode.length() == 1) iMO = iMO - 1;
	            	
					String sTMO = StringUtil.formatNumberString(Integer.toString (Calendar.getInstance().get(Calendar.MONTH) + 1), 2);
	            	String sLMO = sLastNo.substring(iMO, iMO + s_FORMAT_MO.length());
	            	
	    			log.info ("Today Date sTMO : " + sTMO);
	    			log.info ("Last  Date sLMO : " + sLMO);

	            	if(!StringUtil.equalsIgnoreCase(sTMO, sLMO)) iLastNo = 0; //if today DD != Last No DD reset No            	
	            }
            }
			       
			if (log.isInfoEnabled())
			{
				log.info ("sSepIDX : " + sLastNo.lastIndexOf(s_SEPARATOR));
				log.info ("sLastNo2 : " + sLastNo);
				log.info ("sMaxNo : " + sMaxNo);
			}

			//if iLastNo equals to MaxNo then start from zero
			if (iLastNo == Integer.parseInt(sMaxNo)) iLastNo = 0;
		}
		iLastNo++;
		log.info ("iLastNo : " + iLastNo);
		
		Calendar oCal = Calendar.getInstance();

		String sNewNo = StringUtil.formatNumberString (Integer.toString (iLastNo), s_FORMAT_NO.length());
		String sDays  = StringUtil.formatNumberString(Integer.toString (oCal.get(Calendar.DAY_OF_MONTH)), 2);
		String sMonth = StringUtil.formatNumberString (Integer.toString (oCal.get(Calendar.MONTH) + 1), 2);
		String sYear  = (Integer.toString(oCal.get(Calendar.YEAR))).substring(2) ;
		
		if (StringUtil.isEmpty(sLocCode))
		{
			sLocCode = PreferenceTool.getLocationCode();
		}
		
		if (!StringUtil.isEqual(s_TABLE, "ctl_location_trans"))
		{
			String sHeader = sTransNo.substring(0, sFormat.indexOf(s_SEPARATOR));
			sTransNo = sTransNo.replace(sHeader, sPrefix);		
		}
		
		sTransNo = sTransNo.replaceAll (s_FORMAT_LC, sLocCode);
		sTransNo = sTransNo.replaceAll (s_FORMAT_DD, sDays  );
		sTransNo = sTransNo.replaceAll (s_FORMAT_MO, sMonth );
		sTransNo = sTransNo.replaceAll (s_FORMAT_YR, sYear  ); 
		sTransNo = sTransNo.replaceAll (s_FORMAT_NO, sNewNo );
		
		log.info ("sTransNo : " + sTransNo);
		return sTransNo;
	} 

	/**
     * Update last number 
     * 
     * @param iType
     * @param _sTransNo
     * @param m_oConn
     * @throws Exception
     */
    private void updateLastNo(String _sTransNo)
    	throws Exception
    {
        String sLastNo = getLastNo();
        StringBuilder oSQL = new StringBuilder();
        if (sLastNo != null)
        {	         
	        oSQL.append("UPDATE ").append(s_TABLE)
	        	.append(" SET last_number = '").append(_sTransNo).append("'")
	        	.append(" WHERE trans_type = ").append(iType)
	        	.append(" AND prefix = '").append(sPrefix).append("'");
	        
	        BasePeer.executeStatement(oSQL.toString(), m_oConn);
        }
        else
        {
	        oSQL.append("INSERT INTO ").append(s_TABLE)
	        	.append(" (trans_type, prefix, last_number) ")
	        	.append(" VALUES (").append(iType).append(",")
	        	.append(" '").append(sPrefix).append("',")	        	
	        	.append(" '").append(_sTransNo).append("')");
	        
	        BasePeer.executeStatement(oSQL.toString(), m_oConn);        	
        }
    } 
}       
        