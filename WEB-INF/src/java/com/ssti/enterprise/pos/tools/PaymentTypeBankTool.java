package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PaymentTypeBank;
import com.ssti.enterprise.pos.om.PaymentTypeBankPeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PaymentTypeBankTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: PaymentTypeBankTool.java,v $
 * Revision 1.8  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 07:44:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PaymentTypeBankTool extends BaseTool 
{
	private static PaymentTypeBankTool instance = null;
	
	public static synchronized PaymentTypeBankTool getInstance()
	{
		if (instance == null) instance = new PaymentTypeBankTool();
		return instance;
	}
	
	public static List getAll()
		throws Exception
	{
		Criteria oCrit = new Criteria();	
		oCrit.addAscendingOrderByColumn(PaymentTypeBankPeer.BANK_CODE);
		return PaymentTypeBankPeer.doSelect(oCrit);
	}
	
	public static PaymentTypeBank getByID(String _sID)
		throws Exception
	{
		return getByID(_sID, null);
	}
	
	public static PaymentTypeBank getByID(String _sID,Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(PaymentTypeBankPeer.PAYMENT_TYPE_BANK_ID, _sID);
		List vData = PaymentTypeBankPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (PaymentTypeBank)vData.get(0);
		}
		return null;
	}

}
