package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.Voucher;
import com.ssti.enterprise.pos.om.VoucherNo;
import com.ssti.enterprise.pos.om.VoucherNoPeer;
import com.ssti.enterprise.pos.om.VoucherPeer;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: VoucherTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: VoucherTool.java,v $
 * Revision 1.8  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 07:44:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PaymentVoucherTool extends BaseTool 
{
	private static PaymentVoucherTool instance = null;
	
	public static synchronized PaymentVoucherTool getInstance()
	{
		if (instance == null) instance = new PaymentVoucherTool();
		return instance;
	}
	
	public static List getAll()
		throws Exception
	{
		Criteria oCrit = new Criteria();	
		oCrit.addDescendingOrderByColumn(VoucherPeer.AMOUNT);
		return VoucherPeer.doSelect(oCrit);
	}
	
	public static Voucher getByID(String _sID)
		throws Exception
	{		
		return getByID(_sID,null);
	}	

	public static Voucher getByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(VoucherPeer.VOUCHER_ID, _sID);
		List vData = VoucherPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (Voucher)vData.get(0);
		}
		return null;
	}	
	
	public static List getActiveNo()
		throws Exception
	{
		Date dToday = DateUtil.getTodayDate();
		Criteria oCrit = new Criteria();		
		oCrit.add(VoucherNoPeer.VALID_FROM, dToday, Criteria.LESS_EQUAL );		
		oCrit.add(VoucherNoPeer.VALID_TO, dToday, Criteria.GREATER_EQUAL );
		List vData = VoucherNoPeer.doSelect(oCrit);
		return vData;
	}
	
	public static List getNoList()
		throws Exception
	{
		
		String sSQL = "select create_id, voucher_id, min(valid_from) as valid_from, min(valid_to) as valid_to, " +
				      " min(create_by) as create_by, min(create_date) as create_date, count(*) as qty, " +
				      " min(voucher_no) as from, max(voucher_no) as to " +				
				      " FROM voucher_no GROUP BY create_id, voucher_id ORDER BY description, create_date DESC";
		return SqlUtil.executeQuery(sSQL);		
	}	

	public static List getNoByCID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(VoucherNoPeer.CREATE_ID, _sID);
		List vData = VoucherNoPeer.doSelect(oCrit);
		return vData;
	}

	public static void createNo(RunData data)
		throws Exception
	{
		String sVID = data.getParameters().getString("VoucherId");
		int iQty = data.getParameters().getInt("qty",0);
		if (iQty > 0)
		{
			Voucher oVCH = getByID(sVID);
			if (oVCH != null)
			{
				Connection oConn = BaseTool.beginTrans();
				try
				{
					Date dFrom = DateUtil.getStartOfDayDate(data.getParameters().getDate("ValidFrom"));
					Date dTo =DateUtil.getEndOfDayDate( data.getParameters().getDate("ValidTo"));
					Date dToday = DateUtil.getTodayDate();
					String sDate = CustomFormatter.formatCustomDateTime(dTo, "yyMMdd");
					String sPref = oVCH.getCode() + sDate;
					BigDecimal bdAmt = new BigDecimal(oVCH.getAmount());
					String sCreateID = IDGenerator.generateSysID();
					
					for (int i = 1; i <= iQty; i++)
					{
						String sNo = sPref + StringUtil.formatNumberString(Integer.toString(i), 5);
						VoucherNo oVN = new VoucherNo();
						oVN.setVoucherNoId(IDGenerator.generateSysID());
						oVN.setVoucherId(oVCH.getId());
						oVN.setVoucherNo(sNo);
						oVN.setValidFrom(dFrom);
						oVN.setValidTo(dTo);
						oVN.setAmount(bdAmt);										
						oVN.setCreateDate(dToday);
						oVN.setCreateBy(data.getUser().getName());
						oVN.setCreateId(sCreateID);
						oVN.save(oConn);
					}
					BaseTool.commit(oConn);
				}
				catch (Exception _oEx)
				{
					BaseTool.rollback(oConn);
					log.error(_oEx);		
					throw new Exception ("ERROR Creating Voucher No : " + _oEx.getMessage(), _oEx);
				}								
			}			
		}
	}
	
	public static void deleteNo(RunData data)
		throws Exception
	{
		String sVID = data.getParameters().getString("VoucherId");
		String sCID = data.getParameters().getString("CreateId");
		
		Criteria oCrit = new Criteria();
		oCrit.add(VoucherNoPeer.CREATE_ID,sCID);
		oCrit.add(VoucherNoPeer.USE_TRANS_ID, (Object)"", Criteria.NOT_EQUAL);
		List vUsed = VoucherNoPeer.doSelect(oCrit);
		if (vUsed.size() == 0)
		{
			oCrit = new Criteria();
			oCrit.add(VoucherNoPeer.CREATE_ID,sCID);
			VoucherNoPeer.doDelete(oCrit);	
		}
		else
		{
			throw new Exception ("Can not delete voucher some are already used!");
		}
	}
	
	public static VoucherNo getByVoucherNo(String _sNo, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(VoucherNoPeer.VOUCHER_NO, _sNo);
		List vData = VoucherNoPeer.doSelect(oCrit,_oConn);
		if (vData.size() > 0)
		{
			return (VoucherNo)vData.get(0);
		}
		return null;
	}	
	
    public static void useVoucher(SalesTransaction _oTR, InvoicePayment _oPmt, String _sVoucherNo, Connection _oConn)
    	throws Exception
    {
    	VoucherNo oVCH = getByVoucherNo(_sVoucherNo, _oConn);
    	if(oVCH != null && _oTR != null)
    	{
    		if (StringUtil.isEmpty(oVCH.getUseTransId()))
    		{
	    		oVCH.setUseDate(_oTR.getTransactionDate());
	    		oVCH.setUseTransId(_oTR.getSalesTransactionId());  	    		
	    		oVCH.setUseCashier(_oTR.getCashierName());
	    		oVCH.setUsePtypeId(_oPmt.getPaymentTypeId());
	    		oVCH.save(_oConn);
    		}
    		else
    		{
    			throw new Exception("Voucher " + oVCH.getVoucherNo() + " Already Used");
    		}
    	}
    	else
    	{
    		throw new Exception ("Invalid Voucher No: " + _sVoucherNo + "!");
    	}
    }
    
    public static List getByUseCashier(String _sCashierName, Date _dStart, Connection _oConn) 
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(VoucherNoPeer.USE_CASHIER,_sCashierName);
    	oCrit.add(VoucherNoPeer.USE_DATE, _dStart, Criteria.GREATER_EQUAL);
    	oCrit.and(VoucherNoPeer.USE_DATE, new Date(), Criteria.LESS_EQUAL);
    	return VoucherNoPeer.doSelect(oCrit, _oConn);
    }    
}
