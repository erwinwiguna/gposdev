package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.PeriodManager;
import com.ssti.enterprise.pos.om.EndPeriodCurrency;
import com.ssti.enterprise.pos.om.EndPeriodCurrencyPeer;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.om.PeriodPeer;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for Period OM
 * the OM is presenting a range of date that we called period
 * the period has function as a simpler grouping for transaction happened in 
 * certain date range
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PeriodTool.java,v 1.28 2008/08/28 12:23:58 albert Exp $ <br>
 *
 * <pre>
 * $Log: PeriodTool.java,v $
 * 
 * 2016-03-28
 * - change method getToCloseYear to allow close year any unclosed year, previously 
 *   system only allow 1 previous year to close		   
 * 
 * 2016-01-08
 * - change method getPeriodByDate use data from PeriodManager getPeriodByMonthYear.
 * 
 * 2015-12-09
 * - add closeInventory method to crate monthly inventory balance table
 * - call closeInventory from closePeriod and reopenPeriod
 * 
 * 2015-02-15
 * - add getPeriods(start, end) 
 * </pre><br>
 */
public class PeriodTool extends BaseTool 
{    
	private static Log log = LogFactory.getLog (PeriodTool.class);

	public static final int i_LAST_PERIOD = 12;
	
	public static Period getPeriodByID(String _sID)
		throws Exception
	{
		return PeriodManager.getInstance().getPeriodByID(_sID, null);
	}
	
	public static Period getPeriodByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return PeriodManager.getInstance().getPeriodByID(_sID, _oConn);
	}

	public static List getAllPeriod()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.addDescendingOrderByColumn(PeriodPeer.YEAR);
        oCrit.addAscendingOrderByColumn(PeriodPeer.MONTH); 
		return PeriodPeer.doSelect(oCrit);
    }

	public static int getCodeByID(String _sID)
    	throws Exception
    {

        Period oPeriod = getPeriodByID(_sID);
        if (oPeriod != null)
		{
		    return oPeriod.getPeriodCode();
	    }
	    return -1;
	}		
	
	public static String getDescriptionByID(String _sID)
    	throws Exception
    {

        Period oPeriod = getPeriodByID(_sID);
        if (oPeriod != null)
		{
		    return oPeriod.getDescription();
	    }
	    return "";
	}

	public static List getPeriodByYear(String _sYear)
    	throws Exception
    {
        int iYear = 0;
        try
        {
            iYear = Integer.parseInt (_sYear);
        }
        catch (Exception _oEx)
        {
        	log.error("Ivalid Year : " + _sYear);
            iYear = DateUtil.getCurrentYear();
        }
        return getPeriodByYear (iYear, null);
	}    
	
	/**
	 * get period data for this date
	 * 
	 * @param _dDate
	 * @return Period
	 * @throws Exception
	 */
	public static Period getPeriodByDate(Date _dDate, Connection _oConn)
    	throws Exception
    {
		if(_dDate != null)
		{
			int iYr = Integer.parseInt(CustomFormatter.formatCustomDate(_dDate, "yyyy"));
			int iMo = Integer.parseInt(CustomFormatter.formatCustomDate(_dDate, "MM"));
			Period oPeriod = PeriodManager.getInstance().getPeriodByMonthYear(iYr, iMo, _oConn);
	
	        if (oPeriod != null)
	        {
				return oPeriod;
			}
			else 
			{
			    //try to generate the period
			    log.warn ("Period does not exist, try to generate");
			    String sYear = CustomFormatter.formatCustomDate (_dDate, "yyyy");
			    int iYear = Integer.parseInt(sYear);
			    List vPeriods = getPeriodByYear(iYear, _oConn);
			    if (vPeriods.size() < 1)
			    {
			        generatePeriod(iYear, _oConn);
	                return getPeriodByDate(_dDate, _oConn);
			    }
			    else 
			    {
			        //if the period is not auto generate, then the user should add period manually
			        throw new Exception ("Invalid Period Data, Correct Period Setup First");
			    }
			}
		}
		return null;
	}

	/**
	 * get periods in this year
	 * 
	 * @param _iYear
	 * @return List of Period
	 * @throws Exception
	 */
	public static List getPeriodByYear(int _iYear, Connection _oConn)
    	throws Exception
    {
		return PeriodManager.getInstance().getPeriodByYear(_iYear, _oConn);
	}   
	
	/**
	 * get list of periods within specified date range
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @return
	 * @throws Exception
	 */
	public static List getPeriods(Date _dStart, Date _dEnd)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		
		int iSYear = DateUtil.getCurrentYear();
		int iEYear = DateUtil.getCurrentYear();
		
		int iStartMo = DateUtil.getCurrentMonth();
		int iEndMo = DateUtil.getCurrentMonth();
		if (_dStart != null && _dEnd != null)
		{
			Calendar oCal = Calendar.getInstance();
			oCal.setTime(_dStart);
			iStartMo = oCal.get(Calendar.MONTH) + 1;
			iSYear = oCal.get(Calendar.YEAR);
			
			oCal.setTime(_dEnd);
			iEndMo = oCal.get(Calendar.MONTH) + 1;
			iEYear = oCal.get(Calendar.YEAR);
		}
		
		oCrit.add(PeriodPeer.MONTH, iStartMo, Criteria.GREATER_EQUAL);
		oCrit.and(PeriodPeer.MONTH, iEndMo, Criteria.LESS_EQUAL);
		oCrit.add(PeriodPeer.YEAR, iSYear, Criteria.GREATER_EQUAL);
		oCrit.and(PeriodPeer.YEAR, iEYear, Criteria.LESS_EQUAL);
		
		oCrit.addDescendingOrderByColumn(PeriodPeer.YEAR);
		oCrit.addAscendingOrderByColumn(PeriodPeer.MONTH);		
		
		return PeriodPeer.doSelect(oCrit);
	}
	
	/**
	 * auto generate period data for certain year
	 * 
	 * @param _iYear
	 * @throws Exception
	 */
	public static void generatePeriod(int _iYear, Connection _oConn)
    	throws Exception
    {
        List vYear = getPeriodByYear (_iYear, _oConn);
        if (!(vYear.size() > 0))
        {
        	String sPeriod = LocaleTool.getString("period") + " ";
            String sYear = " " + Integer.toString(_iYear);
            
            if (PreferenceTool.getStartDate() == null)
            {
            	throw new Exception ("Start Date must be set up in Preferences first !");
            }
            
        	for (int i = 0; i <= 11; i++)
            {
                int iMonth = i + 1;
                Date dBeginDate = DateUtil.getStartOfMonthDate(i, _iYear);
                Date dEndDate = DateUtil.getEndOfMonthDate(i, _iYear);
                int iCode = Integer.parseInt(CustomFormatter.formatCustomDate(dBeginDate, "yyyyMM"));
                
                boolean bClosed = false;
                if (dEndDate.before(PreferenceTool.getStartDate()))
                {
                	bClosed = true;
                }
                
                Period oPeriod = new Period();
                oPeriod.setBeginDate(dBeginDate);
                oPeriod.setEndDate(dEndDate);
                oPeriod.setMonth(iMonth);
                oPeriod.setYear(_iYear);
                oPeriod.setDescription(sPeriod + (i + 1) + sYear);
                oPeriod.setIsClosed(bClosed);
                oPeriod.setIsFaClosed(bClosed);
                oPeriod.setPeriodCode(iCode);
                oPeriod.setPeriodId(IDGenerator.generateSysID());
                oPeriod.save(_oConn);
            }
        	PeriodManager.getInstance().refreshCache("");
	    }
	    else 
	    {
	        throw new Exception ("Period in this year already exist");
	    }
	}  

	public static Period getCurrentPeriod()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PeriodPeer.BEGIN_DATE, new Date(), Criteria.LESS_EQUAL);
        oCrit.add(PeriodPeer.END_DATE, DateUtil.getEndOfDayDate(new Date()), Criteria.GREATER_EQUAL);
        //oCrit.add(PeriodPeer.IS_CLOSED, 0);
        
        List vPeriod = PeriodPeer.doSelect(oCrit);
		if (vPeriod.size() > 0)
		{
			return (Period) vPeriod.get(0);
		}
		return null;
	}

	public static int getCurrentCode()
    	throws Exception
    {

        Period oPeriod = getCurrentPeriod();
        if (oPeriod != null)
		{
		    return oPeriod.getPeriodCode();
	    }
	    return -1;
	}		
	
	public static boolean isCurrentPeriod(String _sPeriodID)
    	throws Exception
    {
        Period oPeriod = getPeriodByID (_sPeriodID);
        Period oCurrentPeriod = getCurrentPeriod ();
        if (oPeriod != null && oCurrentPeriod != null)
        {
            if (oPeriod.getPeriodId().equals(oCurrentPeriod.getPeriodId()))
            {
                return true;
            }
        }
        return false;  
	}

	public static List getPeriodIDList (String _sFromID)
    	throws Exception
    {
        int iCode = getCodeByID(_sFromID);
        int iCurrentCode = getCurrentCode();
        if (iCurrentCode > -1)
        {
            if (iCode < iCurrentCode)
            {
		        Criteria oCrit = new Criteria();
                oCrit.add(PeriodPeer.PERIOD_CODE, iCode, Criteria.GREATER_EQUAL);		
                oCrit.and(PeriodPeer.PERIOD_CODE, iCurrentCode, Criteria.LESS_EQUAL);
                oCrit.addAscendingOrderByColumn(PeriodPeer.PERIOD_CODE);
                
                log.debug ("getPeriodIDList1 : " + oCrit);
                List vData = PeriodPeer.doSelect(oCrit);   
                List vID = new ArrayList (vData.size());
                for (int i = 0; i < vData.size(); i++)
                {
                    Period oPeriod = (Period) vData.get(i);
                    vID.add (oPeriod.getPeriodId());
                }
                return vID;
            }
            else if (iCode > iCurrentCode)
            {
		        Criteria oCrit = new Criteria();
                oCrit.add(PeriodPeer.PERIOD_CODE, iCurrentCode, Criteria.GREATER_EQUAL);		
                oCrit.and(PeriodPeer.PERIOD_CODE, iCode, Criteria.LESS_EQUAL);
                oCrit.addAscendingOrderByColumn(PeriodPeer.PERIOD_CODE);
                
                log.debug ("getPeriodIDList2 : " + oCrit);
                List vData = PeriodPeer.doSelect(oCrit);   
                List vID = new ArrayList (vData.size());
                for (int i = 0; i < vData.size(); i++)
                {
                    Period oPeriod = (Period) vData.get(i);
                    vID.add (oPeriod.getPeriodId());
                }
                return vID;                
            }
        }
        else 
        {
            throw new Exception ("Current Period not Exist, Invalid Period Setup");
        }
        return new ArrayList(1);
	}
	
	//-------------------------------------------------------------------------
	// FA CLOSING
	//-------------------------------------------------------------------------
	/**
	 * close FA period & create FA depreciation journal
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void closeFAPeriod(String _sID, String _sUserName)
    	throws Exception
    {
		Connection oConn = null;	
		try
		{
			oConn = beginTrans();
			Period oPeriod = getPeriodByID(_sID, oConn);
			if (!oPeriod.getIsFaClosed())
			{
				oPeriod.setFaClosedBy(_sUserName);
				oPeriod.setFaClosedDate(new Date());
				oPeriod.setIsFaClosed (true);
				
				StringBuilder oRemark = new StringBuilder();
				if (StringUtil.isNotEmpty(oPeriod.getFaClosingRemark()))
				{
					oRemark.append(oPeriod.getFaClosingRemark()).append("\n");
				}
				oRemark.append("FA Closed by: ").append(_sUserName)
					   .append(" at ").append(CustomFormatter.formatDateTime(oPeriod.getFaClosedDate()));
				
				oPeriod.setFaClosingRemark(oRemark.toString());							
				oPeriod.save(oConn);
			}
			commit(oConn);			
		}
		catch (Exception _oEx)
		{
			rollback(oConn);
			log.error(_oEx);
			throw new NestableException (_oEx);
		}
    }	
	
	/**
	 * Reopen FA, cancel FA Depreciation Journal
	 * 
	 * @param _sID
	 * @param _sUserName
	 * @throws Exception
	 */
	public static void reopenFAPeriod(String _sID, String _sUserName)
		throws Exception
	{
		Connection oConn = null;	
		try
		{
			oConn = beginTrans();
			Period oPeriod = getPeriodByID(_sID, oConn);
			if (oPeriod.getIsFaClosed())
			{
				oPeriod.setIsFaClosed (false);
				oPeriod.setFaClosedBy("");
				oPeriod.setFaClosedDate(null);
				
				StringBuilder oRemark = new StringBuilder();
				if (StringUtil.isNotEmpty(oPeriod.getFaClosingRemark()))
				{
					oRemark.append(oPeriod.getFaClosingRemark()).append("\n");
				}
				oRemark.append("FA Re-opened by: ").append(_sUserName)
				.append(" at ").append(CustomFormatter.formatDateTime(new Date()));
				
				oPeriod.setFaClosingRemark(oRemark.toString());
				
				//cancel depreciation journal voucher
				Period oLastPeriod = getPrevPeriod (oPeriod.getPeriodId(), oConn);
				Date dLastDate = PreferenceTool.getStartDate();
				if (oLastPeriod != null) dLastDate = oLastPeriod.getEndDate();
				
				oPeriod.save(oConn);
			}
			commit(oConn);
		}
		catch (Exception _oEx)
		{
			rollback(oConn);
			log.error(_oEx);
			throw new NestableException (_oEx);
		}
	}	
	
	//-------------------------------------------------------------------------
	// PERIOD CLOSING
	//-------------------------------------------------------------------------
	/**
	 * close period, create forex gain loss journal
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void closePeriod(String _sID, String _sUserName, Map _mRates)
    	throws Exception
    {
		Connection oConn = null;	
		try
		{
			oConn = beginTrans();
			Period oPeriod = getPeriodByID(_sID, oConn);
			oPeriod.setClosedBy(_sUserName);
			oPeriod.setClosedDate(new Date());
			oPeriod.setIsClosed (true);
			
			StringBuilder oRemark = new StringBuilder();
			if (StringUtil.isNotEmpty(oPeriod.getRemark()))
			{
				oRemark.append(oPeriod.getRemark()).append("\n");
			}
			oRemark.append("Closed by: ").append(_sUserName)
				   .append(" at ").append(CustomFormatter.formatDateTime(oPeriod.getClosedDate()));
			
			oPeriod.setRemark(oRemark.toString());
			
			//create journal voucher 
			//multi currency realize & unrealize gain / loss
			String sCloseID = 
				JournalVoucherTool.createForexAdj(_mRates, oPeriod.getEndDate(), oPeriod, _sUserName, oConn);
			oPeriod.setCloseTransId(sCloseID);

			//create end period currency entry
			Iterator iter =_mRates.keySet().iterator();
			while (iter.hasNext())
			{
				String sCurrencyID = (String)iter.next();
				double dRates = (Double)_mRates.get(sCurrencyID);
				if (dRates > 0 && dRates != 1)
				{
					EndPeriodCurrency oData = new EndPeriodCurrency();
					oData.setEndPeriodCurrencyId(IDGenerator.generateSysID());
					oData.setPeriodId(oPeriod.getPeriodId());
					oData.setCurrencyId(sCurrencyID);
					oData.setClosingRate(new BigDecimal(dRates));
					oData.save(oConn);
				}
			}

			oPeriod.save(oConn);
			commit(oConn);			
			
			//close inventory
			closeInventory(oPeriod, false);
		}
		catch (Exception _oEx)
		{
			rollback(oConn);
			log.error(_oEx);
			throw new NestableException (_oEx);
		}
    }		
	
	/**
	 * reopen closed period & cancel gain loss journal
	 * 
	 * @param _sID
	 * @param _sUserName
	 * @throws Exception
	 */
	public static void reopenPeriod(String _sID, String _sUserName)
		throws Exception
	{
		Connection oConn = null;	
		try
		{
			oConn = beginTrans();
			Period oPeriod = getPeriodByID(_sID, oConn);
			oPeriod.setIsClosed (false);
			oPeriod.setClosedBy("");
			oPeriod.setClosedDate(null);
			
			StringBuilder oRemark = new StringBuilder();
			if (StringUtil.isNotEmpty(oPeriod.getRemark()))
			{
				oRemark.append(oPeriod.getRemark()).append("\n");
			}
			oRemark.append("Re-opened by: ").append(_sUserName)
				   .append(" at ").append(CustomFormatter.formatDateTime(new Date()));

			oPeriod.setRemark(oRemark.toString());
			
			deleteEndPeriodCurrency(oPeriod.getPeriodId(), oConn);
			
			//cancel multi currency realize & unrealize gaion / loss journal voucher
			JournalVoucherTool.cancelForexAdj(oPeriod.getCloseTransId(), _sUserName, oConn);
			
			//save period
			oPeriod.save(oConn);
			commit(oConn);
			
			//drop close inventory table
			closeInventory(oPeriod, true);
		}
		catch (Exception _oEx)
		{
			rollback(oConn);
			log.error(_oEx);
			throw new NestableException (_oEx);
		}
	}		
	//-------------------------------------------------------------------------
	// END PERIOD CURRENCY
	//-------------------------------------------------------------------------
	public static void deleteEndPeriodCurrency(String _sPeriodID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EndPeriodCurrencyPeer.PERIOD_ID, _sPeriodID);
		EndPeriodCurrencyPeer.doDelete(oCrit, _oConn);
	}
	
	public static BigDecimal getEndPeriodRate(String _sPeriodID, String _sCurrencyID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EndPeriodCurrencyPeer.PERIOD_ID, _sPeriodID);
		oCrit.add(EndPeriodCurrencyPeer.CURRENCY_ID, _sCurrencyID);
		List vData = EndPeriodCurrencyPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			EndPeriodCurrency oCurr = (EndPeriodCurrency) vData.get(0);
			return oCurr.getClosingRate();
		}
		return bd_ONE;
	}
	
	//-------------------------------------------------------------------------
	// CLOSING VALIDATION
	//-------------------------------------------------------------------------
	public static boolean isClosed(Date _dDate, Connection _oConn)
		throws Exception
	{
		Period oPeriod = getPeriodByDate(_dDate, _oConn);
		if (oPeriod != null && oPeriod.getIsClosed())
		{
			return true;
		}
		return false;
	}	
	
	/**
	 * check whether date period submitted is already passed 
	 * if we want to close period 1 2006 then system will warn if 
	 * closing done before 31/1/2006
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static boolean isEndOfPeriod(String _sID)
    	throws Exception
    {
		Period oCurrentPeriod = getPeriodByID(_sID);
		Date dToday = new Date ();
		if (dToday.before(oCurrentPeriod.getEndDate()))
		{
			return false;
		}
		else 
		{
			return true;
        }
	}

	/**
	 * get last closed period
	 * 
	 * @return Last closed Period
	 * @throws Exception
	 */
	public static Period getLastClosedPeriod()
    	throws Exception
    {
		return PeriodManager.getInstance().getLastClosedPeriod();
	}

	/**
	 * get last closed period
	 * 
	 * @return Last closed Period
	 * @throws Exception
	 */
	public static Period getLastFAClosedPeriod()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PeriodPeer.IS_FA_CLOSED, true);
		oCrit.addDescendingOrderByColumn(PeriodPeer.BEGIN_DATE);
		oCrit.setLimit(1);
		List vPeriod = PeriodPeer.doSelect(oCrit);
		if (vPeriod.size() > 0)
		{
			return (Period) vPeriod.get(0);
		}
		return null;
	}	
	 
	public static Period getLastOpenPeriod(Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PeriodPeer.IS_CLOSED, false);
		oCrit.addAscendingOrderByColumn(PeriodPeer.BEGIN_DATE);
		oCrit.setLimit(1);
		List vData = PeriodPeer.doSelect(oCrit,_oConn);
		if (vData.size() > 0) 
		{
			return (Period)vData.get(0);
		}	 
		return null;
	}
	
	public static Period getPrevPeriod(String _sID)
		throws Exception
	{
		return getPrevPeriod(_sID, null);
	}	
	
	/**
	 * get previous period 
	 * 
	 * @param _sID
	 * @return previous Period
	 * @throws Exception
	 */
	public static Period getPrevPeriod(String _sID, Connection _oConn)
		throws Exception
	{
		Period oPeriod = getPeriodByID(_sID, _oConn);
		if (oPeriod != null)
		{
			int iMonth = oPeriod.getMonth();
			int iYear = oPeriod.getYear();
			
			if (iMonth != 1)
			{
				iMonth = iMonth - 1;
			}
			else
			{
				iMonth = i_LAST_PERIOD;
				iYear = iYear - 1;
			}
			
			Criteria oCrit = new Criteria();
		    oCrit.add(PeriodPeer.MONTH, iMonth);
		    oCrit.add(PeriodPeer.YEAR, iYear);
			oCrit.setLimit(1);			
			List vPeriod = PeriodPeer.doSelect(oCrit, _oConn);
			if (vPeriod.size() > 0)
			{
				return (Period) vPeriod.get(0);
			}
		}
		return null;
	}
	
	/**
	 * get next period
	 * 
	 * @param _sID
	 * @return Next Period
	 * @throws Exception
	 */
	public static Period getNextPeriod(String _sID)
		throws Exception
	{
		Period oPeriod = getPeriodByID(_sID);
		if (oPeriod != null)
		{
			int iMonth = oPeriod.getMonth();
			int iYear = oPeriod.getYear();
			
			if (iMonth != i_LAST_PERIOD)
			{
				iMonth = iMonth + 1;
			}
			else
			{
				iMonth = 1;
				iYear = iYear + 1;
			}
			
			Criteria oCrit = new Criteria();
		    oCrit.add(PeriodPeer.MONTH, iMonth);
		    oCrit.add(PeriodPeer.YEAR, iYear);
			oCrit.setLimit(1);			
			List vPeriod = PeriodPeer.doSelect(oCrit);
			if (vPeriod.size() > 0)
			{
				return (Period) vPeriod.get(0);
			}
		}
		return null;
	}	
	
	/**
	 * check whether period is valid for closing by checking 
	 * whether previous period if exists already closed
	 * 
	 * @param _sPeriodID
	 * @return is Period Valid for CLose
	 * @throws Exception
	 */
	public static boolean isValidForClose (String _sPeriodID)
		throws Exception
	{
		Period oPrev = getPrevPeriod(_sPeriodID);
		if (oPrev != null)
		{
			if (oPrev.getIsClosed())
			{
				return true;
			}
			return false;
		}
		else //no previous period
		{
			return true;
		}
	}
	
	/**
	 * check whether period is valid for closing by checking 
	 * whether next period if exists already closed
	 * 
	 * @param _sPeriodID
	 * @return is Period valid for reopen
	 * @throws Exception
	 */
	public static boolean isValidForReopen (String _sPeriodID)
		throws Exception
	{
		Period oNext = getNextPeriod(_sPeriodID);
		if (oNext != null)
		{
			if (oNext.getIsClosed())
			{
				return false;
			}
			return true;
		}
		else //no next period
		{
			return true;
		}
	}

	/**
	 * check whether period is valid for closing FA by checking 
	 * whether previous period FA if exists already closed
	 * 
	 * @param _sPeriodID
	 * @return is Period Valid for CLose
	 * @throws Exception
	 */
	public static boolean isValidForFAClose (String _sPeriodID)
		throws Exception
	{
		Period oPrev = getPrevPeriod(_sPeriodID);
		if (oPrev != null)
		{
			if (oPrev.getIsFaClosed())
			{
				return true;
			}
			return false;
		}
		else //no previous period
		{
			return true;
		}
	}	

	/**
	 * check whether period is valid for closing FA by checking 
	 * whether next period if exists already closed
	 * 
	 * @param _sPeriodID
	 * @return is Period valid for reopen FA
	 * @throws Exception
	 */
	public static boolean isValidForFAReopen (String _sPeriodID)
		throws Exception
	{
		Period oNext = getNextPeriod(_sPeriodID);
		if (oNext != null)
		{
			if (oNext.getIsFaClosed())
			{
				return false;
			}
			return true;
		}
		else //no next period
		{
			return true;
		}
	}
	
	/**
	 * get previous year periods
	 * 
	 * @param _oPeriod
	 * @return List of previous year Period
	 */
	public static List getYearPrevPeriod(Period _oPeriod) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PeriodPeer.YEAR,_oPeriod.getYear());
		oCrit.add(PeriodPeer.MONTH,_oPeriod.getMonth(), Criteria.LESS_THAN);
		return PeriodPeer.doSelect(oCrit);
	}
	
	//-------------------------------------------------------------------------
	// CLOSING YEAR 
	//-------------------------------------------------------------------------
	 
	public static Period getToCloseYear ()
		throws Exception
	{
		String sSQL = "select distinct date_part('year', transaction_date) AS yr from gl_transaction ORDER BY yr";
		List vClosed = getClosedYear();
		List vYear = SqlUtil.executeQuery(sSQL);
		for (int i = 0; i < vYear.size(); i++)
		{			
			Record r = (Record) vYear.get(i);
			int iYear = r.getValue(1).asInt();
			
			Criteria oCrit = new Criteria();
			oCrit.add(PeriodPeer.YEAR, iYear);
			oCrit.add(PeriodPeer.MONTH, 12);
			List vLastPeriodYear = PeriodPeer.doSelect(oCrit);
			if(vLastPeriodYear.size() > 0)
			{
				Period oLast = (Period) vLastPeriodYear.get(0);
				if(oLast != null && !oLast.getIsYearClosed())
				{
					//may not be closed yet 
					if (!DateUtil.getTodayDate().after(oLast.getEndDate()))
					{
						return null;
					}
					if (!oLast.getIsFaClosed() || !oLast.getIsClosed()) //not month closed yet
					{
						return null;
					}
					return oLast;
				}
			}
		}
		return null;				
	}
	
	public static Period getLastPeriodYear (int _iYear, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PeriodPeer.YEAR, _iYear);
		oCrit.add(PeriodPeer.MONTH, 12);
		List vData = PeriodPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (Period) vData.get(0);
		}
		return null;
	}
	
	public static boolean isEndOfYear(int _iYear)
    	throws Exception
    {
		Period oLastPeriod = getLastPeriodYear(_iYear, null);
		if (oLastPeriod != null)
		{
			Date dToday = new Date ();
			if (dToday.before(oLastPeriod.getEndDate()))
			{
				return false;
			}
			else 
			{
				return true;
			}
		}
		return false;
	}


	public static boolean isValidForCloseYear (int _iYear)
		throws Exception
	{
		int iPrevYear = _iYear - 1;
		Period oPrev = getLastPeriodYear(iPrevYear, null);
		if (oPrev != null)
		{
			if (oPrev.getIsYearClosed())
			{
				return true;
			}
			return false;
		}
		else //no previous period
		{
			return true;
		}
	}
	
	public static boolean isValidForReopenYear (int _iYear)
		throws Exception
	{
		int iNextYear = _iYear + 1;
		Period oNext = getLastPeriodYear(iNextYear, null);		
		if (oNext != null)
		{
			if (oNext.getIsYearClosed())
			{
				return false;
			}
			return true;
		}
		else //no next period
		{
			return true;
		}
	}

	public static List getClosedYear ()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PeriodPeer.IS_YEAR_CLOSED, true);
		oCrit.addDescendingOrderByColumn(PeriodPeer.YEAR);
		return PeriodPeer.doSelect(oCrit);
	}	

	/**
	 * 
	 * @param _iYear
	 * @param _sUserName
	 * @throws Exception
	 */
	public static void closeYear(int _iYear, String _sUserName)
    	throws Exception
    {
		closeYear(_iYear,_sUserName,false);
    }
	
	public static void closeYear(int _iYear, String _sUserName, boolean _bLocation)
			throws Exception
	{
		closeYear(_iYear,_sUserName,_bLocation,false);
	}

	/**
	 * 
	 * @param _iYear
	 * @param _sUserName
	 * @throws Exception
	 */
	public static void closeYear(int _iYear, String _sUserName, boolean _bLocation, boolean _bFirstJan)
    	throws Exception
    {
		Connection oConn = null;	
		try
		{
			oConn = beginTrans();
						
			Period oPeriod = getLastPeriodYear(_iYear, oConn);
			oPeriod.setIsYearClosed (true);
			
			String sID = "";
			//create closing journal voucher 
			if (_bLocation)
			{
				sID = JournalVoucherTool.createClosingYearLocation(oPeriod, _sUserName, _iYear, _bFirstJan, oConn);
			}
			else
			{
				sID = JournalVoucherTool.createClosingYearJournal(oPeriod, _sUserName, _iYear, _bFirstJan, oConn);
			}
			oPeriod.setCloseYearTransId(sID);
			oPeriod.save(oConn);
			commit(oConn);
		}
		catch (Exception _oEx)
		{
			rollback(oConn);
			log.error(_oEx);
			throw new NestableException (_oEx);
		}
    }
	
	/**
	 * Reopen year
	 * 
	 * @param _iYear
	 * @param _sUserName
	 * @throws Exception
	 */
	public static void reopenYear(int _iYear, String _sUserName)
		throws Exception
	{
		Connection oConn = null;	
		try
		{
			oConn = beginTrans();
						
			Period oPeriod = getLastPeriodYear(_iYear, oConn);
			oPeriod.setIsYearClosed (false);
			
			//cancel closing journal voucher 
			JournalVoucherTool.cancelClosingYearJournal(oPeriod.getCloseYearTransId(), _sUserName, oConn);
	
			oPeriod.save(oConn);
			commit(oConn);
		}
		catch (Exception _oEx)
		{
			rollback(oConn);
			log.error(_oEx);
			throw new NestableException (_oEx);
		}
	}	    

    public static boolean closeInvExists(Period _oPeriod)
    	throws Exception
    {
    	String sTbl = "inv_trans_" + _oPeriod.getPeriodCode();
    	String sSQL = "select * from pg_catalog.pg_tables WHERE tablename = '" + sTbl + "'";    	
    	List v = SqlUtil.executeQuery(sSQL);
    	if(v.size() > 0)
    	{
    		return true;
    	}
    	return false;
    }
    
    /**
     * VM Link: PeriodCloseInventory.vm
     * 
     * @param Period
     * @throws Exception
     */
    public static int closeInventory(Period _oPeriod, boolean _bDropOnly)
        throws Exception
    {
        int iResult = 0; 
        try
        {
        	if(_oPeriod != null)
        	{
	        	String sTbl = "inv_trans_" + _oPeriod.getPeriodCode();
	        	String sEnd = CustomFormatter.formatSqlDate(_oPeriod.getEndDate());
	        	
	        	StringBuilder oSQL = new StringBuilder();
	        	oSQL.append(" DROP TABLE IF EXISTS ").append(sTbl);
	        	SqlUtil.executeStatement(oSQL.toString());
	        	
	        	if(!_bDropOnly)
		        {
		        	oSQL = new StringBuilder();
					oSQL.append("SELECT DISTINCT ON (item_id, location_id) ")
						.append(" inventory_transaction_id, transaction_id, transaction_detail_id, item_id, location_id, transaction_no, ")
						.append(" transaction_date, item_code, location_name, qty_changes, cost, qty_balance, value_balance ")
						.append(" INTO ").append(sTbl).append(" FROM inventory_transaction ")
						.append(" WHERE transaction_date <= ' ").append(sEnd).append("'")
						.append(" ORDER  BY item_id, location_id, (transaction_date, inventory_transaction_id) DESC ");
		        	iResult = SqlUtil.executeStatement(oSQL.toString());
	        	}
        	}
        }
        catch (Exception _oEx)
        {
            log.error(_oEx);
            throw new NestableException (_oEx);
        }
        return iResult;
    }    
}
