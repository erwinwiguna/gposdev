package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.manager.PriceListDetailManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PriceList;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.om.PriceListDetailPeer;
import com.ssti.enterprise.pos.om.PriceListPeer;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PriceListTool.java,v 1.29 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: PriceListTool.java,v $
 * 
 * 2018-03-02
 * - change updateDetailByID, now query PLD, and keep history if Price / Discount Changed
 * - add method getDetailByDetID
 * 
 * 2017-03-01
 * - add PriceListDetailManager
 * - move getPriceListByID to PriceListDetailManager
 * - add method getDetail for public method
 * - add method getPLDetail for method to be called by PriceListDetailManager or non-cache method call
 * - add method isByCustomerExist - to make cache query more efficient in PriceListDetailManager
 *   if isByCustomer not exist then no need to put customerID in cache key
 * 
 * </pre><br>
 */
public class PriceListTool extends BaseTool 
{
    private static Log log = LogFactory.getLog(PriceListTool.class);

	public static List getAllPriceList()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		return PriceListPeer.doSelect(oCrit);
    }

	public static List getAllActivePriceList()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add (PriceListPeer.CREATE_DATE, new Date(), Criteria.LESS_EQUAL);
		oCrit.add (PriceListPeer.EXPIRED_DATE, DateUtil.getEndOfDayDate(new Date()), Criteria.GREATER_EQUAL);
		return PriceListPeer.doSelect(oCrit);
    }    
    
	public static List getLocationActivePriceList(String _sLocationID)
    	throws Exception
    {
		String sLocationID = "%" + _sLocationID + "%"; //to like mode
		
		Criteria oCrit = new Criteria();
		oCrit.add (PriceListPeer.CREATE_DATE, new Date(), Criteria.LESS_EQUAL);
		oCrit.add (PriceListPeer.EXPIRED_DATE, DateUtil.getEndOfDayDate(new Date()), Criteria.GREATER_EQUAL);
		oCrit.add (PriceListPeer.LOCATION_ID, (Object)sLocationID, Criteria.LIKE);
		oCrit.or (PriceListPeer.LOCATION_ID, "");
		return PriceListPeer.doSelect(oCrit);
    }    

	public static boolean isByCustomerExist(String _sLocationID)
    	throws Exception
    {
		List vPL = getLocationActivePriceList(_sLocationID);
		if(vPL.size() > 0)
		{
			Iterator iter = vPL.iterator();
			while(iter.hasNext())
			{
				PriceList oPL = (PriceList) iter.next();
				if(StringUtil.isNotEmpty(oPL.getCustomerId()))
				{
					return true;
				}
			}
		}
		return false;
    }    

	public static PriceList getPriceListByID(String _sID)
    	throws Exception
    {
		return getPriceListByID(_sID, null);
	}

	public static PriceList getPriceListByID(String _sID, Connection _oConn)
		throws Exception
	{
		return PriceListDetailManager.getInstance().getPLByID(_sID, _oConn);
	}	
	
	public static PriceList getPriceListByCode(String _sCode, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListPeer.PRICE_LIST_CODE, _sCode);
	    List vData = PriceListPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0)
	    {
			return (PriceList) vData.get(0);
		}
		return null;
	}	

	public static void importAllItem (String _sID)
    	throws Exception
    {
		LargeSelect oLS = ItemTool.getAllItemLS();
		while(oLS.getNextResultsAvailable())
		{
			List vItem = oLS.getNextResults();
			for (int i = 0; i < vItem.size(); i++) 
			{
				PriceListDetail oDet = new PriceListDetail ();
				//Generate Sys ID
				oDet.setPriceListDetailId ( IDGenerator.generateSysID() );
				oDet.setPriceListId (_sID);
				Item oItem = (Item) vItem.get(i);
				mapItemToDetail(oItem, oDet,-1);
				oDet.save();
			}
		}
	}

	public static PriceListDetail getDetailByDetID(String _sDetID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListDetailPeer.PRICE_LIST_DETAIL_ID, _sDetID);
	    List v = PriceListDetailPeer.doSelect(oCrit);	    
	    if(v.size() > 0)
	    {
	    	return (PriceListDetail) v.get(0);
	    }
	    return null;
	}
	
	public static PriceListDetail getDetailByPriceListAndItemID(String _sPLID, 
																String _sItemID,
																Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListDetailPeer.PRICE_LIST_ID, _sPLID);
	    oCrit.add(PriceListDetailPeer.ITEM_ID, _sItemID);
	    List vDetail = PriceListDetailPeer.doSelect(oCrit, _oConn);
	    if (vDetail.size() > 0)
	    {
	    	return (PriceListDetail) vDetail.get(0);
	    }
	    return null;
	}

	public static List getDetails(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListDetailPeer.PRICE_LIST_ID, _sID);
	    oCrit.addAscendingOrderByColumn(PriceListDetailPeer.PRICE_LIST_DETAIL_ID);
	    return PriceListDetailPeer.doSelect(oCrit);
	}
	
	public static List getItemInActivePL(String _sItemID, Connection _oConn)
		throws Exception
	{	
		Date dNow = new Date();
		Criteria oCrit = new Criteria();
		oCrit.add(PriceListDetailPeer.ITEM_ID, _sItemID);
	    oCrit.addJoin(PriceListDetailPeer.PRICE_LIST_ID, PriceListPeer.PRICE_LIST_ID);
    	oCrit.add(PriceListPeer.EXPIRED_DATE, dNow, Criteria.GREATER_EQUAL);   		    	    	
    	return PriceListDetailPeer.doSelect(oCrit,_oConn);
	}

	//search
	public static LargeSelect findData( String _sID,
										int _iCondition, 
									    int _iSortBy, 
									    int _iViewLimit,
									    String _sKatID,
									    String _sKeywords )
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		
		oCrit.add (PriceListDetailPeer.PRICE_LIST_ID, _sID);
		
		//search by keywords		
		if (_sKeywords != null && !_sKeywords.equals("")) 
		{
			if (_iCondition == 1) {
				oCrit.add(PriceListDetailPeer.ITEM_CODE,
					(Object) SqlUtil.like (PriceListDetailPeer.ITEM_CODE, _sKeywords), Criteria.CUSTOM);
			} 
			else if (_iCondition == 2) {
				oCrit.add(PriceListDetailPeer.ITEM_NAME,
					(Object) SqlUtil.like (PriceListDetailPeer.ITEM_NAME, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 3) {
				oCrit.add(PriceListDetailPeer.NEW_PRICE, Double.valueOf(_sKeywords).doubleValue(), Criteria.EQUAL);
			}
			else if (_iCondition == 4) {
				oCrit.add(PriceListDetailPeer.OLD_PRICE, Double.valueOf(_sKeywords).doubleValue(), Criteria.EQUAL);
			}	
		}
		//define kategori criteria
		if (_sKatID != null && !_sKatID.equals(""))
		{
			List vID = KategoriTool.getChildIDList(_sKatID);
	        if (vID.size()  > 0) {
	        	oCrit.addJoin(PriceListDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		        vID.add(_sKatID);
		        oCrit.addIn(ItemPeer.KATEGORI_ID, vID);
	        }
	        else {
	        	oCrit.addJoin(PriceListDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		        oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
	        }
	    }		
	    //define sorting criteria
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(PriceListDetailPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
        	oCrit.addAscendingOrderByColumn(PriceListDetailPeer.ITEM_NAME);
		}
		else if (_iSortBy == 4) {
			oCrit.addAscendingOrderByColumn(PriceListDetailPeer.NEW_PRICE);
		}
		else if (_iSortBy == 4) {
			oCrit.addAscendingOrderByColumn(PriceListDetailPeer.OLD_PRICE);
		}
		return new LargeSelect(oCrit, _iViewLimit, "com.ssti.enterprise.pos.om.PriceListDetailPeer");
	}

	public static void updateDetailByID(String _sID, String _sDetailID, double _dPrice, String _sDiscount)
		throws Exception
	{
		updateDetailByID(_sID, _sDetailID, _dPrice, _sDiscount, "");
	}
	
	public static void updateDetailByID(String _sID, String _sDetailID, double _dPrice, String _sDiscount, String _sUser)
		throws Exception
	{
		
		Criteria oSelectCrit = new Criteria();
	    oSelectCrit.add(PriceListDetailPeer.PRICE_LIST_DETAIL_ID, _sDetailID);
	    List v = PriceListDetailPeer.doSelect(oSelectCrit);
	    PriceListDetail oPLD = null;
	    if(v.size() > 0)
	    {
	    	oPLD = (PriceListDetail) v.get(0);
	    	PriceListDetail oOld = oPLD.copy();
	    	
	    	if(!Calculator.isInTolerance(_dPrice, oOld.getNewPrice(), 0.5) || 
	    	   !StringUtil.equals(_sDiscount, oOld.getDiscountAmount()) )
	    	{
		    	oPLD.setNewPrice(new BigDecimal(_dPrice));
				oPLD.setDiscountAmount(_sDiscount);
				oPLD.save();
				
				UpdateHistoryTool.createHistory(oOld, oPLD, _sUser, null);
			    updatePriceListDateTag (_sID);
	    	}
	    }	    
	    
	    //refresh find cache
	    ItemManager.getInstance().refreshCache(null);
	}

	public static PriceList addItemToPriceList (String _sItemID, String _sPriceListID, int _iPriceFrom)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		Item oItem = ItemTool.getItemByID (_sItemID);
		if (oItem != null) 
		{
			PriceListDetail oDet = new PriceListDetail ();
			oDet.setPriceListId (_sPriceListID);
			mapItemToDetail (oItem, oDet, _iPriceFrom);
			//Generate Sys ID                                   			
			oDet.setPriceListDetailId ( IDGenerator.generateSysID() );			
			oDet.save();
		}
		updatePriceListDateTag(_sPriceListID);
        oCrit.add(PriceListPeer.PRICE_LIST_ID, _sPriceListID);
		return (PriceList) PriceListPeer.doSelect(oCrit).get(0);
	}

	private static void mapItemToDetail (Item _oItem, PriceListDetail _oDet, int _iPriceFrom)
		throws Exception
	{
		_oDet.setItemId  		(_oItem.getItemId	 ());	
        _oDet.setItemCode		(_oItem.getItemCode	 ());
        _oDet.setItemName       (_oItem.getItemName  ());
        _oDet.setNewPrice		(_oItem.getItemPrice ());
        _oDet.setDiscountAmount	("0");
        
        if (PreferenceTool.medicalInstalled())
        {
        	_oDet.setOldPrice(_oItem.getLastPurchasePrice());        	
        }
        else
        {
        	if (_iPriceFrom == 1)
        	{
            	_oDet.setOldPrice(_oItem.getLastPurchasePrice());        	        		
        	}
        	else
        	{
        		_oDet.setOldPrice(_oItem.getItemPrice());
        	}
        }
	}

	public static boolean isItemExist(String _sItemID, String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListDetailPeer.ITEM_ID, _sItemID);
	    oCrit.add(PriceListDetailPeer.PRICE_LIST_ID, _sID);
	    List vPL = PriceListDetailPeer.doSelect(oCrit);
		if (vPL.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * validation limitation does not comply with multi location
	 * means if PL A Loc A,B,C and PL B Loc C,D,E, validation 
	 * didnot work
	 * 
	 * @param _sItemID
	 * @param _sCurrentID
	 * @return 
	 * @throws Exception
	 */
	public static boolean isItemExistInActivePL (String _sItemID, String _sCurrentID)
		throws Exception
	{
		PriceList oPL = getPriceListByID(_sCurrentID);
		
		Date dNow = new Date();
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListPeer.PRICE_LIST_ID, (Object)_sCurrentID, Criteria.NOT_EQUAL);
		oCrit.add(PriceListDetailPeer.ITEM_ID, _sItemID);
	    oCrit.addJoin(PriceListDetailPeer.PRICE_LIST_ID, PriceListPeer.PRICE_LIST_ID);
    	oCrit.add(PriceListPeer.EXPIRED_DATE, dNow, Criteria.GREATER_EQUAL);   		
    	oCrit.and(PriceListPeer.EXPIRED_DATE, oPL.getCreateDate(),Criteria.GREATER_EQUAL);
    	oCrit.add(PriceListPeer.CUSTOMER_ID, oPL.getCustomerId());
    	oCrit.add(PriceListPeer.CUSTOMER_TYPE_ID, oPL.getCustomerTypeId());
    	oCrit.add(PriceListPeer.LOCATION_ID, (Object) ("%"+oPL.getLocationId()+"%"),Criteria.ILIKE);
    	
    	List vData = PriceListDetailPeer.doSelect(oCrit);
    	if (vData.size() > 0)
    	{
    		return true;
    	}
    	return false;
	}

	/**
	 * 
	 * @param _sCustID
	 * @param _sCustTypeID
	 * @param _sLocID
	 * @param _sItemID
	 * @param _dTransDate
	 * @return
	 * @throws Exception
	 */
	public static PriceListDetail getDetail (String _sCustID, 
										  	 String _sCustTypeID, 
											 String _sLocID, 
											 String _sItemID,
											 Date _dTransDate) 
		throws Exception
	{
		return PriceListDetailManager.getInstance().getDetail(_sCustID, _sCustTypeID, _sLocID, _sItemID, _dTransDate);
	}
	/**
	 * 
	 * @param _sCustID
	 * @param _sCustTypeID
	 * @param _sLocID
	 * @param _sItemID
	 * @return Price List Detail
	 * @throws Exception
	 */
	public static PriceListDetail getPLDetail (String _sCustID, 
										  	   String _sCustTypeID, 
											   String _sLocID, 
											   String _sItemID,
											   Date _dTransDate) 
		throws Exception
	{		
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sCustID)) 
		{
			String sCustomerID = "%" + _sCustID + "%";
			oCrit.add(PriceListPeer.CUSTOMER_ID, (Object)sCustomerID, Criteria.ILIKE);
			oCrit.or(PriceListPeer.CUSTOMER_ID, "");
		}
		if (StringUtil.isNotEmpty(_sCustTypeID)) 
		{
			oCrit.add(PriceListPeer.CUSTOMER_TYPE_ID, _sCustTypeID);
			oCrit.or(PriceListPeer.CUSTOMER_TYPE_ID, "");
		}
		if (StringUtil.isNotEmpty(_sLocID)) 
		{
			String sLocationID = "%" + _sLocID + "%";
			oCrit.add(PriceListPeer.LOCATION_ID, (Object)sLocationID, Criteria.LIKE);
			oCrit.or(PriceListPeer.LOCATION_ID, "");
		}
		oCrit.add(PriceListPeer.CREATE_DATE, _dTransDate, Criteria.LESS_EQUAL);
		oCrit.add(PriceListPeer.EXPIRED_DATE, DateUtil.getEndOfDayDate(_dTransDate), Criteria.GREATER_EQUAL);
		oCrit.addJoin(PriceListPeer.PRICE_LIST_ID, PriceListDetailPeer.PRICE_LIST_ID);
		oCrit.add(PriceListDetailPeer.ITEM_ID, _sItemID);
		
		log.info("get price list detail : "  + oCrit);
		
		List vDetail = PriceListDetailPeer.doSelect (oCrit);
		if (vDetail.size() > 0) 
		{
			return (PriceListDetail) vDetail.get(0);
		}	
		return null;
	}	
	
	public static String displayLocation(String _sLocationID)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			StringTokenizer oToken = new StringTokenizer(_sLocationID, ",");
			while (oToken.hasMoreTokens())
			{
				String sID = oToken.nextToken();
				String sLocName = LocationTool.getLocationNameByID(sID);
				oSB.append(sLocName).append("<br/>");
			}
		}	
		else
		{
			oSB.append(LocaleTool.getString("all"));
		}
		return oSB.toString();
	}
	
	//synchronization method
	public static void updatePriceListDateTag (String _sPriceListID)
		throws Exception 
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PriceListPeer.UPDATE_DATE, new Date());
	    Criteria oSelectCrit = new Criteria();
	    oSelectCrit.add(PriceListPeer.PRICE_LIST_ID, _sPriceListID);
	    PriceListPeer.doUpdate(oSelectCrit,oCrit);
	} 
	
	public static List getAllUnsynchronizedPriceList (Date _dLastSyncDate)
		throws Exception 
	{
		Criteria oCrit = new Criteria();
		if (_dLastSyncDate  != null) {
        	oCrit.add(PriceListPeer.CREATE_DATE, _dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		return PriceListPeer.doSelect(oCrit);
	} 
	
	/////////////////////////////////////////////////////////////////////////////////////
	// price adjustment
	/////////////////////////////////////////////////////////////////////////////////////
	public static void adjustPrice (PriceList _oPL, int _iAdjType)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		
		Criteria oCrit = new Criteria();
		oCrit.add(PriceListDetailPeer.PRICE_LIST_ID, _oPL.getPriceListId());
		
		oCrit.addJoin(PriceListDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		oCrit.add(PriceListDetailPeer.OLD_PRICE, (Object) SqlUtil.buildCompareQuery 
				(PriceListDetailPeer.OLD_PRICE, ItemPeer.ITEM_PRICE, "<>"), Criteria.CUSTOM);
		
		LargeSelect oLS = new LargeSelect(oCrit, 100, "com.ssti.enterprise.pos.om.PriceListDetailPeer");
		
		while (oLS.getNextResultsAvailable())
		{
			log.debug("next result  : " + true);
			try
			{
				List vPLD = oLS.getNextResults();
				log.debug("vPLD : " + vPLD);

				if (vPLD != null && vPLD.size() > 0) 
				{
					adjustPrice (vPLD, _iAdjType);
				}
				else break;
			}
			catch (Exception _oEx)
			{
				_oEx.printStackTrace();
				break;
			}
		}
	}
	
	private static void adjustPrice (List _vPLD, int _iAdjType)
		throws Exception
	{
		for (int i = 0; i < _vPLD.size(); i++)
		{
			PriceListDetail oPLD = (PriceListDetail) _vPLD.get(i);
			Item oItem = ItemTool.getItemByID(oPLD.getItemId());
			
			log.debug("oPLD : " + oPLD);
			log.debug("Item Price : " + oItem.getItemPrice());
			
			double dNewPrice = oPLD.getNewPrice().doubleValue();
			double dOldPrice = oPLD.getOldPrice().doubleValue();
			double dItemPrice = oItem.getItemPrice().doubleValue();

			if (_iAdjType == 1) //by margin pct
			{
				double dPct = (dNewPrice - dOldPrice) / dOldPrice * 100;
				dNewPrice = dItemPrice + (dPct/100 * dItemPrice);

				log.debug("dPct : " + dPct);
				log.debug("New Price : " + dNewPrice);

			}
			else if(_iAdjType == 2) //by margin value
			{
				double dDelta = dNewPrice - dOldPrice;
				dNewPrice = dItemPrice + dDelta;					

				log.debug("dPct : " + dDelta);
				log.debug("New Price : " + dNewPrice);

			}
			oPLD.setOldPrice(new BigDecimal(dItemPrice));
			oPLD.setNewPrice(new BigDecimal(dNewPrice));
			oPLD.save();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static List getAllUpdatedActivePriceList (int _iSyncType)
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(_iSyncType);
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(PriceListPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
        	oCrit.add(PriceListPeer.EXPIRED_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		return PriceListPeer.doSelect(oCrit);
	}		
}