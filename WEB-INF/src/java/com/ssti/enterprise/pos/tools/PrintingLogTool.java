package com.ssti.enterprise.pos.tools;

import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.TransactionMasterOM;
import com.ssti.enterprise.pos.om.PrintingLog;
import com.ssti.enterprise.pos.om.PrintingLogPeer;
import com.ssti.enterprise.pos.om.SalesTransaction;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PrintingLogTool.java,v 1.5 2008/06/29 07:11:57 albert Exp $ <br>
 *
 * <pre>
 * $Log: PrintingLogTool.java,v $
 * Revision 1.5  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/02/23 14:44:43  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PrintingLogTool extends BaseTool 
{	
	public static List getAllPrintingLog()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return PrintingLogPeer.doSelect(oCrit);
	}

	public static PrintingLog getPrintingLogByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PrintingLogPeer.PRINTING_LOG_ID, _sID);
        List vData = PrintingLogPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (PrintingLog) vData.get(0);
		}
		return null;
	}
	
	public static void createPrintingLogFromSalesTransaction (SalesTransaction _oTR, String _sUserName) 
		throws Exception
	{
		PrintingLog oPLog = new PrintingLog	();
		oPLog.setPrintingLogId (IDGenerator.generateSysID());
		oPLog.setTransactionId (_oTR.getSalesTransactionId());
		oPLog.setTransactionNo (_oTR.getInvoiceNo());
		oPLog.setPrintingDate (new Date());
		oPLog.setUserName (_sUserName);
		oPLog.save();
	}
	
	public static void logTrans (TransactionMasterOM _oTR, String _sUserName) 
		throws Exception
	{
		PrintingLog oPLog = new PrintingLog	();
		oPLog.setPrintingLogId (IDGenerator.generateSysID());
		oPLog.setTransactionId (_oTR.getTransactionId());
		oPLog.setTransactionNo (_oTR.getTransactionNo());
		oPLog.setPrintingDate (new Date());
		oPLog.setUserName (_sUserName);
		oPLog.save();
	}	
}