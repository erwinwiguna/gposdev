package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.ReturnReason;
import com.ssti.enterprise.pos.om.ReturnReasonPeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ReturnReasonTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ReturnReasonTool.java,v $
 * Revision 1.8  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 07:44:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class ReturnReasonTool extends BaseTool 
{
	private static ReturnReasonTool instance = null;
	
	public static synchronized ReturnReasonTool getInstance()
	{
		if (instance == null) instance = new ReturnReasonTool();
		return instance;
	}
	
	public static List getAll()
		throws Exception
	{
		Criteria oCrit = new Criteria();	
		oCrit.addAscendingOrderByColumn(ReturnReasonPeer.CODE);
		return ReturnReasonPeer.doSelect(oCrit);
	}
	
	public static ReturnReason getByID(String _sID)
		throws Exception
	{
		return getByID(_sID,null);
	}
	
	public static ReturnReason getByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(ReturnReasonPeer.RETURN_REASON_ID, _sID);
		List vData = ReturnReasonPeer.doSelect(oCrit,_oConn);
		if (vData.size() > 0)
		{
			return (ReturnReason)vData.get(0);
		}
		return null;
	}
	
	public static List getReason(boolean _bIsSales)
		throws Exception
	{
		Criteria oCrit = new Criteria();	
		if (_bIsSales)
		{
			oCrit.add(ReturnReasonPeer.TRANS_TYPE,ReturnReason.i_SALES);			
			oCrit.or(ReturnReasonPeer.TRANS_TYPE,ReturnReason.i_BOTH);
		}
		else
		{
			oCrit.add(ReturnReasonPeer.TRANS_TYPE,ReturnReason.i_PURCHASE);						
			oCrit.or(ReturnReasonPeer.TRANS_TYPE,ReturnReason.i_BOTH);
		}				
		oCrit.addAscendingOrderByColumn(ReturnReasonPeer.CODE);
		return ReturnReasonPeer.doSelect(oCrit);
	}	
}
