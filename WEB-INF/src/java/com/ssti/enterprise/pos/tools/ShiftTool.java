package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ssti.enterprise.pos.manager.ShiftManager;
import com.ssti.enterprise.pos.om.Shift;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ShiftTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ShiftTool.java,v $
 * 
 * 2015-09-01
 * - change method getByTime to consider closest period with time supplied as params
 * 
 * </pre><br>
 */
public class ShiftTool extends BaseTool 
{
	private static ShiftTool instance = null;
	
	public static synchronized ShiftTool getInstance()
	{
		if (instance == null) instance = new ShiftTool();
		return instance;
	}
	
	public static List getAllShift()
		throws Exception
	{
		return ShiftManager.getInstance().getAllShift();
	}
	
	public static Shift getShiftByID(String _sID)
		throws Exception
	{
		return ShiftManager.getInstance().getShiftByID (_sID, null);
	}
	
	public static Shift getShiftByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return ShiftManager.getInstance().getShiftByID(_sID, _oConn);
	}
	
	public static String getName(String _sID)
		throws Exception
	{
		Shift o = ShiftManager.getInstance().getShiftByID (_sID, null);
		if (o != null) return o.getShiftName();
		return "";
	}
	
	public static Shift getByTime(Date _dDate)	
		throws Exception
	{
		if (_dDate == null) _dDate = new Date();
		Calendar oCal = Calendar.getInstance();
		oCal.setTime(_dDate);
		
		List v = getAllShift();
		for (int i = 0; i < v.size(); i++)
		{
			Shift s = (Shift) v.get(i);
			String sStart = s.getStartHour();
			if(StringUtil.isNotEmpty(sStart) && sStart.length() >= 2)
			{
				int iStartHr = Integer.valueOf(sStart.substring(0, 2));
				int iDiff = iStartHr - oCal.get(Calendar.HOUR_OF_DAY);
				if(iDiff < 0) iDiff = iDiff * -1;
				if(iDiff <= 1) return s;			
			}
 		}
		return null;		
	}
}
