package com.ssti.enterprise.pos.tools;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Synchronization;
import com.ssti.enterprise.pos.om.SynchronizationPeer;
import com.ssti.framework.io.FileDateComparator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SynchronizationTool.java,v 1.17 2008/08/17 02:20:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: SynchronizationTool.java,v $
 * Revision 1.17  2008/08/17 02:20:24  albert
 * *** empty log message ***
 *
 * Revision 1.16  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/26 05:14:41  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/05/09 06:50:27  albert
 * *** empty log message ***
 *
 * Revision 1.13  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class SynchronizationTool extends BaseTool 
{
	static SynchronizationTool instance = null;
	
	public static synchronized SynchronizationTool getInstance() 
	{
		if (instance == null) instance = new SynchronizationTool();
		return instance;
	}
	
	public static List getAllSynchronization()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return SynchronizationPeer.doSelect(oCrit);
	}

	public static Synchronization getSynchronizationByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SynchronizationPeer.SYNCHRONIZATION_ID, _sID);
        List vData = SynchronizationPeer.doSelect(oCrit);
		if (vData.size () > 0) {
			return (Synchronization) vData.get(0);
		}
		return null;
	}

	public static Synchronization getByFileName(String _sFileName)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		_sFileName = "%" + _sFileName;
        oCrit.add(SynchronizationPeer.FILE_NAME, (Object) _sFileName, Criteria.LIKE);
        List vData = SynchronizationPeer.doSelect(oCrit);
		if (vData.size () > 0) 
		{
			return (Synchronization) vData.get(0);
		}
		return null;
	}

	public static boolean isFileEverBeenSyncBefore(String _sFileName)
    	throws Exception
    {
		if (getByFileName(_sFileName) != null)
		{
			return true;
		}
		return false;
	}

	public static Synchronization getLastSyncByType (int _iSyncType)
		throws Exception
	{
		return getLastSyncByType (_iSyncType, "");
	}

	public static Synchronization getLastSyncByType (int _iSyncType, String _sLocationID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(SynchronizationPeer.SYNC_TYPE, _iSyncType);
	    if (StringUtil.isNotEmpty(_sLocationID))
	    {
	    	oCrit.add(SynchronizationPeer.LOCATION_ID, _sLocationID);
	    }
	    oCrit.addDescendingOrderByColumn(SynchronizationPeer.SYNC_DATE);
		oCrit.setLimit (1);
		List vData = SynchronizationPeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return (Synchronization) vData.get(0);
		}
		return null;
	}
    
	public static Date getLastSyncDateByType (int _iSyncType)
    	throws Exception
    {
		Synchronization oData = getLastSyncByType (_iSyncType);
		if (oData != null) 
		{
			return oData.getSyncDate();
		}		
		return null;
	}

    public static String getLastSyncInfo (int _iSyncType)
    	throws Exception
    {   
		return CustomFormatter.formatDateTime(SynchronizationTool.getLastSyncDateByType(_iSyncType));
    }    
    
	public static String getLastSyncFileByType (int _iSyncType)
    	throws Exception
    {
		Synchronization oData = getLastSyncByType (_iSyncType);
		if (oData != null) 
		{
			return oData.getFileName();
		}
		return null;
	}
	
	public static Synchronization createSyncTransaction (int _iSyncType, 
														 String _sFileName,
														 String _sLocationID,
														 String _sUserName,
														 Connection _oConn)
    	throws Exception
    {
		//in HO2Store
		//generate, location is target store or empty when for all store
		//sync, location is target store

		//in Store2HO
		//generate, location value is store 
		//sync, location value is store
		
		if (_sLocationID == null) _sLocationID = "";
		if (_sUserName == null) _sUserName = "";
		
		Synchronization oSync = new Synchronization ();
		oSync.setSynchronizationId  (IDGenerator.generateSysID());	
		oSync.setSyncType			(_iSyncType);	
		oSync.setSyncDate       	(new Date());
		oSync.setLocationId     	(_sLocationID);
		oSync.setLocationName   	(LocationTool.getLocationNameByID(_sLocationID));
		oSync.setFileName 			(_sFileName);		
		oSync.setUserName			(_sUserName);
		oSync.save					(_oConn);
		return oSync;
	}


    /**
     * 
     * @param _iSyncType
     * @return List of file in data directory
     * @throws Exception
     */
	public static List displayDataDirContent (int _iSyncType)
    	throws Exception
    {
		String sDataDir = "";		
		if (_iSyncType == i_HO_TO_STORE) 
		{
			sDataDir = getHODataPath();
		}
		else 
		{
			sDataDir = getStoreDataPath();
		}
		File oDataDir = new File (sDataDir);
		File[] aFile = oDataDir.listFiles();
		List vData = new ArrayList();
		if (aFile != null)
		{
			Arrays.sort (aFile, new FileDateComparator());
			for (int i = (aFile.length - 1); i >= 0; i--)
			{
				File oDataFile = aFile[i];			
				Synchronization oSyncData = getByFileName (oDataFile.getName());			
				log.debug("File : " + oDataFile.getName() + " sycndata : " + oSyncData);								
			}
		}
		return vData;
	}
	
	public static String cancelLastGenerate (int _iType, String _sLocationID)
	    throws Exception
	{
		Synchronization oSync = SynchronizationTool.getLastSyncByType(_iType, _sLocationID);
		if (oSync != null)
		{
			String sFile = oSync.getFileName();
			StringBuilder sMsg = new StringBuilder();
			try 
			{
				SynchronizationPeer.doDelete(oSync.getPrimaryKey());
				sMsg.append("Data Generation (FILE) : ").append(sFile).append(" CANCELLED !");
	
			    try
				{
					File oFile = new File(sFile);
					boolean bResult = oFile.delete();
					if (bResult) {
						sMsg.append("<br><br>PHYSICAL FILE : ").append(sFile).append(" DELETED !");
					}
					else
					{
						sMsg.append("<br>PHYSICAL FILE : ").append(sFile)
							.append(" NOT DELETED, MUST BE DELETED MANUALLY!");					
					}
				}
			    catch (Exception _oEx) 
			    {
			    	sMsg.append("<br><br>SYNC CANCELLED, DELETING FILE : ").append(sFile) 
			    		.append(" ERROR: ").append(_oEx.getMessage());
			    }					
			}
		    catch (Exception _oEx) 
		    {
		    	sMsg.append("ERROR : " + _oEx.getMessage());
		    }
		    return sMsg.toString();
		}
		return "LAST SYNC DATA NOT FOUND!";
	}	
	
	public static String getHODataPath()
	{
		return getPath(true);
	}

	public static String getStoreDataPath()
	{
		return getPath(false);
	}

	private static String getPath(boolean _bHO)
	{
		try
		{
			String sDataFolder = PreferenceTool.getSysConfig().getSyncHodataPath();
			if (!_bHO)
			{
				sDataFolder = PreferenceTool.getSysConfig().getSyncStoredataPath();
			}
			sDataFolder = IOTool.checkPath(sDataFolder);	    	
			return sDataFolder;
		}
		catch (Exception e)
		{
			log.error("ERROR " + e.getMessage());
			e.printStackTrace();
		}		
		return "";
	}
	
	public static Connection getHOConn()
		throws Exception 
	{
		try 
		{
			if(PreferenceTool.getConfiguration().containsKey("modules.sync.ho.db.url"))
			{
				String sURL = PreferenceTool.getConfiguration().getString("modules.sync.ho.db.url");
				if(StringUtil.isNotEmpty(sURL))
				{    		
					Properties oProp = new Properties();
					oProp.put("user", "root");
					oProp.put("password", "crossfire");
					Connection conn = DriverManager.getConnection(sURL, oProp);
					return conn;						
				}
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return null;		
	}
	
	public static void closeHOConn(Connection conn)                                                                                 
		throws Exception                                                                                                
	{                                                                                                                   
		if(conn != null)
		{
			try {
				conn.close();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
		}
	} 
	
}