package com.ssti.enterprise.pos.tools;

import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: TransactionAttributes.java,v 1.25 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: TransactionAttributes.java,v $
 * Revision 1.25  2009/05/04 02:03:51  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */

public interface TransactionAttributes extends Attributes
{	
    //TRANSACTION CODES
    public static int i_PO = 1;
    public static int i_PR = 2;
    public static int i_PI = 3;
    public static int i_PT = 4;
    public static int i_SO = 5;
    public static int i_DO = 6;
    public static int i_SI = 7;
    public static int i_SR = 8;
    public static int i_RQ = 9;
    public static int i_IT = 10;
    public static int i_IR = 11;
    public static int i_JC = 12;
    public static int i_PL = 13;        
    public static int i_CM = 14;
    public static int i_DM = 15;
    public static int i_RP = 16;
    public static int i_PP = 17;
    public static int i_CF = 18;
    public static int i_JV = 19;
    public static int i_FA = 20;
    
	//velocity context name
	public static final String s_MULTI_CURRENCY    = "useMultiCurrency";
    public static final String s_RELATED_EMPLOYEE  = "allowRelatedEmployee";
    public static final String s_B2B_ENABLED       = "b2bEnabled";
    public static final String s_ALLOW_COPY_TRANS  = "allowCopyTrans";    
    public static final String s_USE_FOB_COURIER   = "useFobCourier";    
    
    public static final String s_RET = "return";
    public static final String s_RET_DET = "returnDetail";
    
    //global app related config
    public static final String b_ALLOW_COPY_TRANS = CONFIG.getString("transaction.allow.copytrans");   
	public static final boolean b_STRICT_MINUS = CONFIG.getBoolean("inventory.strict.minus");	
	public static final boolean b_GLOBAL_COSTING = CONFIG.getBoolean("inventory.global.costing");	

	//COST ADJUSTMENT w/ separate ISSUE RECEIPT No
    public static final boolean b_INVENTORY_SEPARATE_COST_ADJ = CONFIG.getBoolean("costadjustment.separate.no");
    public static final boolean b_SALES_SEPARATE_TAX_INV = CONFIG.getBoolean("sales.separate.tax.invoice");

	public static final boolean b_USE_GL = CONFIG.getBoolean("gl.installed");
	public static final boolean b_TRANSFER_JOURNAL = CONFIG.getBoolean("gl.itemtransfer.journal");
	public static final int i_GL_GROUPING_COST = CONFIG.getInt("gl.grouping.cogs.type");	    
    public static final int i_LAST_PURCHASE_METHOD = CONFIG.getInt("purchase.lastpurchase.countmethod");
    //public static final String s_MIN_SALES_PRICE_FIELD = CONFIG.getString("min.sell.price.field");
		
    //purchase related configs
   
    //public static final String s_SEPARATE_COST_ADJ_TYPE_CODE = CONFIG.getString("costadjustment.separate.type.code");

	//public static final boolean b_PURCHASE_SORT_DETAIL = 
    	//CONFIG.getBoolean("application.purchase.sortdetail");
	//public static final boolean b_PURCHASE_USE_FOB_COURIER = 
		//CONFIG.getBoolean("application.purchase.usefobcourier");
    //public static final boolean b_PURCHASE_FIRST_LAST = 
		//CONFIG.getBoolean("application.purchase.firstlastinsert");
	
    public static final int i_PURCHASE_SORT_BY = PreferenceTool.getPurchSortBy();
    	//CONFIG.getInt("application.purchase.sortby");

    public static final int i_PURCHASE_COMMA_SCALE = PreferenceTool.getPurchCommaScale();
		//CONFIG.getInt("application.purchase.scaleaftercomma");
    
    public static final boolean b_PURCHASE_MULTI_CURRENCY = PreferenceTool.getPurchMultiCurrency();
    	//CONFIG.getBoolean("application.purchase.multicurrency");
    
	public static final boolean b_PURCHASE_TAX_INCLUSIVE = PreferenceTool.getPurchInclusiveTax();
		//CONFIG.getBoolean("application.purchase.tax.inclusive");
	
    public static final boolean b_PURCHASE_RELATED_EMPLOYEE = PreferenceTool.getPurchLimitByEmp(); 
    	//CONFIG.getBoolean("application.purchase.allowrelatedemployeeonly");

    public static final boolean b_ALLOW_GREATER_PR_QTY = PreferenceTool.getPurchPrQtyGtPo();

    //sales related configs
    //public static final boolean b_SALES_SORT_DETAIL = 
		//CONFIG.getBoolean("application.sales.sortdetail");
    //public static final boolean b_SALES_FIRST_LAST = 
		//CONFIG.getBoolean("application.sales.firstlastinsert");
    //public static final boolean b_SALES_USE_FOB_COURIER = 
		//CONFIG.getBoolean("application.sales.usefobcourier");

    public static final int i_SALES_SORT_BY = PreferenceTool.getSalesSortBy();
    	//CONFIG.getInt("application.sales.sortby");

	public static final boolean b_MIN_SALES_PRICE_USE = PreferenceTool.getSalesUseMinPrice();
		//CONFIG.getBoolean("min.sell.price.use");
	
    public static final int i_SALES_COMMA_SCALE = PreferenceTool.getSalesCommaScale();
		//CONFIG.getInt("application.sales.scaleaftercomma");
    
    public static final boolean b_SALES_MULTI_CURRENCY = PreferenceTool.getSalesMultiCurrency();
		//CONFIG.getBoolean("application.sales.multicurrency");    

	public static final boolean b_SALES_TAX_INCLUSIVE = PreferenceTool.getSalesInclusiveTax();
		//CONFIG.getBoolean("application.sales.tax.inclusive");

	public static final boolean b_USE_SALES = PreferenceTool.getSalesUseSalesman();
		//CONFIG.getBoolean("application.sales.usesales");

    public static final boolean b_INVENTORY_FIRST_LAST = PreferenceTool.getInvenFirstlineInsert();

    public static final int i_INVENTORY_COMMA_SCALE = PreferenceTool.getInvenCommaScale();
		//CONFIG.getInt("application.inventory.scaleaftercomma");

    /*
	/* MOVED TO SysConfig -- 14 Jul 2010
    public static final boolean b_USE_DISPLAYER = CONFIG.getBoolean("displayer.use");
	public static final boolean b_USE_DRAWER = CONFIG.getBoolean("drawer.use");
	public static final boolean b_CONFIRM_QTY = CONFIG.getBoolean("pos.confirm.qty");	
	public static final boolean b_CONFIRM_COST = CONFIG.getBoolean("pos.confirm.cost");	
	public static final boolean b_VALIDATE_CHANGE = CONFIG.getBoolean("pos.manualchange.validation");
	public static final boolean b_VALIDATE_DELETE = CONFIG.getBoolean("pos.deleteitem.validation");		
	public static final boolean b_POS_SAVE_DIRECTPRINT = CONFIG.getBoolean("printer.pos.save.directprint");
	*/
	            
    //DIRECT ADD ITEM
	public static final String s_DA = "directadd";
		
	public static final boolean b_POS_DIRECT_ADD = PreferenceTool.posDirectAdd();
		//CONFIG.getBoolean("pos.direct.add.item");

	public static final boolean b_SO_DIRECT_ADD = CONFIG.getBoolean("so.direct.add.item");
	public static final boolean b_DO_DIRECT_ADD = CONFIG.getBoolean("do.direct.add.item");
	public static final boolean b_SI_DIRECT_ADD = CONFIG.getBoolean("si.direct.add.item");	
	public static final boolean b_PO_DIRECT_ADD = CONFIG.getBoolean("po.direct.add.item");
	public static final boolean b_PR_DIRECT_ADD = CONFIG.getBoolean("pr.direct.add.item");
	public static final boolean b_PI_DIRECT_ADD = CONFIG.getBoolean("pi.direct.add.item");
	public static final boolean b_RQ_DIRECT_ADD = CONFIG.getBoolean("rq.direct.add.item");
	public static final boolean b_IR_DIRECT_ADD = CONFIG.getBoolean("ir.direct.add.item");
	public static final boolean b_IT_DIRECT_ADD = CONFIG.getBoolean("it.direct.add.item");
	
	//var in RunData's ParameterParser and context that shown
	//a post result contains error
	public static final String s_POST_ERROR = "POST_ERROR";
	public static final String s_PRINT_READY = "PRINT_READY";
			
    //SEARCH BY FOR TRANSACTION
    //ALL 
    public static final int i_TRANS_NO 	  = 1;
    public static final int i_DESCRIPTION = 2;
    public static final int i_CREATE_BY   = 3;
    public static final int i_CONFIRM_BY  = 4;    
    public static final int i_REF_NO   	  = 5;    
    public static final int i_LOCATION    = 6;
    
    //INV
    public static final int i_ITEM_CODE = 7;    
    public static final int i_ITEM_NAME = 8;
    public static final int i_ITEM_DESC = 9;
    public static final int i_KATEGORI  = 10;    
    public static final int i_PREF_VEND = 11;    
                
    //SALES & PURCHASE
    public static final int i_PMT_TYPE = 12;
    public static final int i_PMT_TERM = 13;
    public static final int i_COURIER  = 14;
    public static final int i_FOB 	   = 15;
    public static final int i_CURRENCY = 16;
    public static final int i_BANK 	   = 17;
    public static final int i_VENDOR   = 18;
    public static final int i_CUSTOMER = 19;
        
    //SALES
    public static final int i_SALESMAN = 20;
     
    //PMT, JV, JC
    public static final int i_ISSUER  = 21;
    public static final int i_ACCOUNT = 22;    
    
    //ALL
    public static final int i_STATUS  = 23;    
    
    //PR, PI
    public static final int i_VEND_DO = 24;
    public static final int i_VEND_SI = 25;
    
    //DEPT PROJECT
    public static final int i_DEPARTMENT = 26;
    public static final int i_PROJECT = 27;
    
    //CFT
    public static final int i_CFT = 28;
    
    //AR / AP PMT
    public static final int i_INV_NO = 29;    
    
    //CUST TYPE
    public static final int i_CUST_TYPE = 30;    

    //CM / DM
    public static final int i_PMT_NO = 31;    

    //LINK TRANS  
    public static final int i_PO_NO = 32;        
    public static final int i_PR_NO = 33;    
    public static final int i_PI_NO = 34;    
    
    public static final int i_SO_NO = 35;    
    public static final int i_DO_NO = 36;    
    public static final int i_SI_NO = 37;    
    
    //GL MESSAGE
    public static final String s_GL_MSG_AR_AP = LocaleTool.getString("gl_msg_ar_ap");
    public static final String s_GL_MSG_INV   = LocaleTool.getString("gl_msg_inv");
    public static final String s_GL_MSG_EXIST = LocaleTool.getString("gl_msg_acc_exist");
    
    //GROUP BY FOR REPORT
    //ALL 
    public static final int i_ORDER_BY_DATE = -1;
    
    public static final int i_GB_NONE 	    = 1;
    public static final int i_GB_CUST_VEND  = 2;
    public static final int i_GB_STATUS  	= 3;        
    public static final int i_GB_LOCATION   = 4;
    public static final int i_GB_CREATE_BY 	= 5;    
    public static final int i_GB_CONFIRM_BY = 6;    
    public static final int i_GB_PMT_TYPE   = 7;
    public static final int i_GB_CURRENCY   = 8;
    public static final int i_GB_SALESMAN   = 9;
    public static final int i_GB_TAX   		= 10;
    
    public static final int i_GB_CATEGORY   = 11;
    public static final int i_GB_ITEM    	= 12;
    public static final int i_GB_CV_ITEM 	= 13;
    public static final int i_GB_LOC_ITEM 	= 14;
    public static final int i_GB_SALES_ITEM = 15;  
    public static final int i_GB_CUST_TYPE  = 16;  

    public static final int i_GB_FOB 		= 17;  
    public static final int i_GB_COURIER  	= 18;  

    
    //SALES TRANSACTION CONTEXT VARS
    public static final String m_CTX_USE_DISPLAYER   = "UseDisplayer";
    public static final String m_CTX_USE_DRAWER      = "UseDrawer";
    public static final String m_CTX_DIRECTPRINT     = "DirectPrint";	
    public static final String m_CTX_CONFIRM_QTY     = "ConfirmQty";
    public static final String m_CTX_CONFIRM_COST    = "ConfirmCost";	
    public static final String m_CTX_VALIDATE_CHANGE = "ValidateChange";
    public static final String m_CTX_VALIDATE_DELETE = "ValidateDelete";	
}
