package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorBalancePeer;
import com.ssti.enterprise.pos.om.VendorPeer;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: VendorTool.java,v 1.30 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: VendorTool.java,v $
 * 
 * 2017-08-25
 * - add ItemType, change find method add parameter _iItemType
 * - change buildFindCriteria, add Condition 10 item_type
 * 
 * 2016-09-10
 * - add vendor type
 * 
 * </pre><br>
 */
public class VendorTool extends BaseTool
{
	public static VendorTypeTool getTypeTool()
	{
		return VendorTypeTool.getInstance();
	}
	
	public static Vendor getVendorByID(String _sID)
    	throws Exception
    {
		return getVendorByID(_sID, null);
    }

	public static Vendor getVendorByID(String _sID, Connection _oConn)
		throws Exception
	{
		return VendorManager.getInstance().getVendorByID(_sID, _oConn);
	}
	
	public static List getAllVendor()
    	throws Exception
    {
    	return VendorManager.getInstance().getAllVendor();
    }

	public static Vendor getVendorByCode(String _sCode) 
		throws Exception
	{
		return VendorManager.getInstance().getVendorByCode(_sCode, null);
	}

	public static Vendor getVendorByCode(String _sCode, Connection _oConn) 
	throws Exception
	{
		return VendorManager.getInstance().getVendorByCode(_sCode, _oConn);
	}
	/**
	 * get vendor by name
	 * 
	 * @param vendorName
	 * @param _bPreffix use preffix
	 * @param _bSuffix use suffix
	 * @return List of vendor with similar name pattern
	 * @throws Exception
	 */
	public static List getVendorByName(String _sVendorName, boolean _bPreffix, boolean _bSuffix) 
		throws Exception 
	{
		Criteria oCrit = new Criteria();
        oCrit.add(VendorPeer.VENDOR_NAME, 
        	(Object) SqlUtil.like (VendorPeer.VENDOR_NAME, _sVendorName, _bSuffix, _bPreffix), 
				Criteria.CUSTOM );	
        oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		return VendorPeer.doSelect(oCrit);
	}	

	public static List getVendorByCode(String _sVendorCode, boolean _bPreffix, boolean _bSuffix) 
		throws Exception 
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(VendorPeer.VENDOR_CODE, 
	    	(Object) SqlUtil.like (VendorPeer.VENDOR_CODE, _sVendorCode, _bSuffix, _bPreffix), 
				Criteria.CUSTOM );
	    oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_CODE);
		return VendorPeer.doSelect(oCrit);
	}	
	
	public static List getVendorByStatus(int _iStatus)
    	throws Exception
    {
		if (_iStatus == 1)//active
		{
			return VendorManager.getInstance().getActiveVendor();
		}
		else
		{	
			Criteria oCrit = new Criteria();
			oCrit.add(VendorPeer.VENDOR_STATUS,_iStatus);
			oCrit.addAscendingOrderByColumn (VendorPeer.VENDOR_NAME);
			return VendorPeer.doSelect(oCrit);
		}
    }
    
	public static List getVendorByRelEmployee(String _sUserName, int _iStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.USER_NAME,_sUserName);
		oCrit.addJoin(VendorPeer.DEFAULT_EMPLOYEE_ID,EmployeePeer.EMPLOYEE_ID);
		oCrit.add(VendorPeer.VENDOR_STATUS,_iStatus);
		oCrit.addAscendingOrderByColumn (VendorPeer.VENDOR_NAME);
		return VendorPeer.doSelect(oCrit);
    }

	public static List getVendorByType(String _sTypeID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if(StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.add(VendorPeer.VENDOR_TYPE_ID,_sTypeID);
		}
		oCrit.addAscendingOrderByColumn (VendorPeer.VENDOR_NAME);
		return VendorPeer.doSelect(oCrit);
    }

	public static String getDefaultTermByID(String _sID)
		throws Exception
	{
		Vendor oVendor = getVendorByID (_sID);
		if (oVendor != null)
		{
			return oVendor.getDefaultTermId();
		}
		return "";
	}
	
	public static String getVendorNameByID(String _sID)
    	throws Exception
    {
		Vendor oVendor = getVendorByID (_sID);
		if (oVendor != null)
		{
			return oVendor.getVendorName();
		}
		return "";
	}

	public static String getIDByCode(String _sCode)
    	throws Exception
    {
		Vendor oVendor = getVendorByCode(_sCode);
        if (oVendor != null) {
        	return oVendor.getVendorId();
		}
		return "";
	}

	public static String getCodeByID(String _sID)
    	throws Exception
    {
		Vendor oVendor = getVendorByID(_sID);
		if (oVendor != null) {
			return oVendor.getVendorCode();
		}
		return "";
	}
	
	public static BigDecimal getCreditLimitByID(String _sID)
    	throws Exception
    {
		Vendor oVendor = getVendorByID(_sID);
		if (oVendor != null) {
			return oVendor.getCreditLimit();
		}
		return bd_ZERO;
	}
    
	private static Criteria buildFindCriteria(int _iCondition, 
											  int _iStatus, 
											  int _iBalance, 
											  String _sKeywords, 
											  String _sTypeID,
											  String _sCurrencyID)
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sKeywords))
		{
			if (_iCondition == 1)
			{
				oCrit.add(VendorPeer.VENDOR_NAME,
					(Object) SqlUtil.like (VendorPeer.VENDOR_NAME, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 2)
			{
				oCrit.add(VendorPeer.VENDOR_CODE,
					(Object) SqlUtil.like (VendorPeer.VENDOR_CODE, _sKeywords,false,true), Criteria.CUSTOM);
			}
			else if (_iCondition == 3)
			{
				oCrit.add(VendorPeer.ADDRESS, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 5)
			{
				oCrit.addJoin(VendorPeer.DEFAULT_EMPLOYEE_ID, EmployeePeer.EMPLOYEE_ID);
				oCrit.add(EmployeePeer.EMPLOYEE_NAME, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 6)
			{
				oCrit.add(VendorPeer.CITY, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 7)
			{
				oCrit.add(VendorPeer.PROVINCE, (Object)_sKeywords, Criteria.ILIKE);
			}			
			else if (_iCondition == 8)
			{
				oCrit.add(VendorPeer.PHONE1, (Object)_sKeywords, Criteria.ILIKE);
			}			
			else if (_iCondition == 9)
			{
				oCrit.add(VendorPeer.PHONE2, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 10)
			{
				if(StringUtil.containsIgnoreCase(_sKeywords, "non"))
				{											
					oCrit.add(VendorPeer.ITEM_TYPE, i_NON_INVENTORY_PART);
				}
				else if(StringUtil.containsIgnoreCase(_sKeywords, "inv"))
				{
					oCrit.add(VendorPeer.ITEM_TYPE, i_INVENTORY_PART);					
				}
			}			
		}
		if(_iStatus > 0)
		{
			oCrit.add(VendorPeer.VENDOR_STATUS,_iStatus);
	    }

		if (_iBalance > 0)
		{
			if (_iBalance == 1)
			{
				oCrit.addJoin(VendorPeer.VENDOR_ID, VendorBalancePeer.VENDOR_ID);
				oCrit.add(VendorBalancePeer.AP_BALANCE, 0, Criteria.GREATER_THAN);				
			}
			if (_iBalance == 2)
			{
				oCrit.addJoin(VendorPeer.VENDOR_ID, VendorBalancePeer.VENDOR_ID);
				oCrit.add(VendorBalancePeer.AP_BALANCE, 0, Criteria.LESS_THAN);				
			}			
			if (_iBalance == 3)
			{
				oCrit.addJoin(VendorPeer.VENDOR_ID, VendorBalancePeer.VENDOR_ID);
				oCrit.add(VendorBalancePeer.AP_BALANCE, 0);				
			}
			if (_iBalance == 4)
			{
				oCrit.addJoin(VendorPeer.VENDOR_ID, VendorBalancePeer.VENDOR_ID);
				oCrit.add(VendorBalancePeer.AP_BALANCE, 0, Criteria.NOT_EQUAL);				
			}			
			if (_iBalance == 5)
			{
				oCrit.addJoin(VendorPeer.VENDOR_ID, VendorBalancePeer.VENDOR_ID);
				oCrit.add(VendorBalancePeer.AP_BALANCE, 
						(Object)"vendor_balance.ap_balance > vendor.credit_limit", Criteria.CUSTOM);				
			}
		}
		if (StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.add(VendorPeer.VENDOR_TYPE_ID, _sTypeID);
		}
		if (StringUtil.isNotEmpty(_sCurrencyID))
		{
			oCrit.add(VendorPeer.DEFAULT_CURRENCY_ID, _sCurrencyID);
		}
		if (_iCondition == 1)
		{
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_CODE);
		}
		else if (_iCondition == 6)
		{
			oCrit.addAscendingOrderByColumn(VendorPeer.CITY);
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		}
		else if (_iCondition == 7)
		{
			oCrit.addAscendingOrderByColumn(VendorPeer.PROVINCE);
			oCrit.addAscendingOrderByColumn(VendorPeer.CITY);
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		}		
		else
		{
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_CODE);
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		}
		return oCrit;
	}
	
	/**
	 * 
	 * @param _iCondition
	 * @param _iStatus
	 * @param _iBalance
	 * @param _sKeywords
	 * @param _sCurrID
	 * @return list of vendor
	 * @throws Exception
	 */
	public static List findData(int _iCondition, 
								int _iStatus, 
								int _iBalance, 
								String _sKeywords, 
								String _sTypeID,
								String _sCurrID)
    	throws Exception
    {		
		return VendorPeer.doSelect(buildFindCriteria(_iCondition, _iStatus, _iBalance, _sKeywords, _sTypeID, _sCurrID));
    }

	public static LargeSelect find(int _iCondition, 
								   int _iStatus, 
								   int _iBalance, 
								   String _sKeywords, 
								   String _sTypeID,
								   String _sCurrID,								   
								   String _sEmpID,
								   int _iItemType,
								   int _iViewLimit)
		throws Exception
	{
		
		Criteria oCrit = buildFindCriteria(_iCondition, _iStatus, _iBalance, _sKeywords, _sTypeID, _sCurrID);
		if (StringUtil.isNotEmpty(_sEmpID))
		{
			oCrit.add(VendorPeer.DEFAULT_EMPLOYEE_ID, _sEmpID);
		}
		if (_iItemType > 0)
		{
			oCrit.add(VendorPeer.ITEM_TYPE, _iItemType);
		}
		return new LargeSelect(oCrit, _iViewLimit, "com.ssti.enterprise.pos.om.VendorPeer");
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////
		
	/**
	 * 
	 * @return updated vendors since last ho 2 store
	 * @throws Exception
	 */
	public static List getUpdatedVendors ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way		
		Criteria oCrit = new Criteria();				
		if (dLastSyncDate  != null) 
		{      
        	oCrit.add(VendorPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
		}
		return VendorPeer.doSelect(oCrit);
	}
	
	public static String generateCode(String _sPrefix, int _iLength)
	{
		if (_iLength <= 0) _iLength = PreferenceTool.getVendCodeLength();
		return generateCode(_sPrefix, VendorPeer.VENDOR_CODE, "vendorCode", VendorPeer.class, _iLength);
	}
	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(VendorPeer.class, VendorPeer.VENDOR_ID, _sID, _bIsNext);
	}	    
}
