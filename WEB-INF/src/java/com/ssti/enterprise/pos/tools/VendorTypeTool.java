package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.om.VendorType;
import com.ssti.enterprise.pos.om.VendorTypePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: VendorTypeTool.java,v 1.12 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: VendorTypeTool.java,v $
 *
 * </pre><br>
 */
public class VendorTypeTool extends BaseTool 
{
	static VendorTypeTool instance = null;
	
	public static synchronized VendorTypeTool getInstance() 
	{
		if (instance == null) instance = new VendorTypeTool();
		return instance;
	}	

	public static List getAllVendorType()
		throws Exception
	{
		return VendorManager.getInstance().getAllVendorType();
	}
	
	public static VendorType getVendorTypeByID(String _sID)
		throws Exception
	{
		return VendorManager.getInstance().getVendorTypeByID(_sID, null);
	}	
	
	public static VendorType getVendorTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
		return VendorManager.getInstance().getVendorTypeByID(_sID, _oConn);
	}		

	public static String getCodeByID(String _sID)
		throws Exception
	{
		VendorType oType = getVendorTypeByID (_sID);
		if (oType != null)
	    {
			return oType.getVendorTypeCode();
		}
	    return "";
	}	
	
	public static String getVendorTypeCodeByID(String _sID)
		throws Exception
	{
		VendorType oType = getVendorTypeByID (_sID);
		if (oType != null)
	    {
			return oType.getVendorTypeCode();
		}
	    return "";
	}	

	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		VendorType oType = getVendorTypeByID (_sID);
		if (oType != null)
	    {
			return oType.getDescription();
		}
	    return "";
	}	

	/**
	 * @param Code
	 * @return
	 */
	public static VendorType getVendorTypeByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (VendorTypePeer.VENDOR_TYPE_CODE, _sCode);
		List vData = VendorTypePeer.doSelect(oCrit);
		if (vData.size()>0)
		{
			return (VendorType)vData.get(0);
		}
		return null;
	}
	
	/**
	 * @param Code
	 * @return
	 */
	public static String getIDByCode(String _sCode) 
		throws Exception
	{
		VendorType oType = getVendorTypeByCode(_sCode);
		if (oType != null)
		{
			return oType.getVendorTypeId();
		}
		return "";
	}	
}
