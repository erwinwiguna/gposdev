package com.ssti.enterprise.pos.tools.data;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.manager.AccountManager;
import com.ssti.enterprise.pos.manager.AccountTypeManager;
import com.ssti.enterprise.pos.manager.AdjustmentTypeManager;
import com.ssti.enterprise.pos.manager.BankManager;
import com.ssti.enterprise.pos.manager.CashFlowTypeManager;
import com.ssti.enterprise.pos.manager.CurrencyManager;
import com.ssti.enterprise.pos.manager.CustomFieldManager;
import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.manager.DiscountManager;
import com.ssti.enterprise.pos.manager.EmployeeManager;
import com.ssti.enterprise.pos.manager.FOBCourierManager;
import com.ssti.enterprise.pos.manager.GLConfigManager;
import com.ssti.enterprise.pos.manager.InventoryLocationManager;
import com.ssti.enterprise.pos.manager.ItemInventoryManager;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.manager.ItemStatusCodeManager;
import com.ssti.enterprise.pos.manager.KategoriManager;
import com.ssti.enterprise.pos.manager.LocationManager;
import com.ssti.enterprise.pos.manager.PaymentTermManager;
import com.ssti.enterprise.pos.manager.PaymentTypeManager;
import com.ssti.enterprise.pos.manager.PeriodManager;
import com.ssti.enterprise.pos.manager.PettyCashManager;
import com.ssti.enterprise.pos.manager.PreferenceManager;
import com.ssti.enterprise.pos.manager.ShiftManager;
import com.ssti.enterprise.pos.manager.SiteManager;
import com.ssti.enterprise.pos.manager.TaxManager;
import com.ssti.enterprise.pos.manager.UnitManager;
import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.tools.AppAttributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Clear Cache
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * 
 * </pre><br>
 */
public class CacheControl implements AppAttributes
{    
    private static CacheControl m_oInstance;
    
    public static CacheControl getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CacheControl.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CacheControl();
                }
            }
        }
        return m_oInstance;
    }    
    
    String msg;       
	public String getMsg() {
		return msg;
	}

	Log log = LogFactory.getLog(getClass());
	public void clearAllCache()
	{
        try
        {
            AccountManager.getInstance().refreshCache(null);
            AccountTypeManager.getInstance().refreshCache(null);
            AdjustmentTypeManager.getInstance().refreshCache("");
            BankManager.getInstance().refreshCache(null);
            CashFlowTypeManager.getInstance().refreshCache("");
            CurrencyManager.getInstance().refreshCache("");
            CustomerManager.getInstance().refreshCache("","");
            CustomFieldManager.getInstance().refreshCache("");
            DiscountManager.getInstance().refreshCache("");
            EmployeeManager.getInstance().refreshCache("");
            FOBCourierManager.getInstance().refreshCache("");
            GLConfigManager.getInstance().refreshCache();
            InventoryLocationManager.getInstance().clearCache();
            ItemInventoryManager.getInstance().refreshCache(null);
            ItemManager.getInstance().refreshCache(null);
            ItemStatusCodeManager.getInstance().refreshCache("");
            KategoriManager.getInstance().refreshCache("");
            LocationManager.getInstance().refreshCache(null);
            PaymentTermManager.getInstance().refreshCache("");
            PaymentTypeManager.getInstance().refreshCache("");
            PeriodManager.getInstance().refreshCache("");
            PettyCashManager.getInstance().refreshCache("");
            PettyCashManager.getInstance().refreshBalance("");
            PreferenceManager.getInstance().refreshCache();
            //PriceListDetailManager
            //ReportManager.getInstance().refreshCache("");            
            ShiftManager.getInstance().refreshCache("");
            SiteManager.getInstance().refreshCache("");
            TaxManager.getInstance().refreshCache("","");
            UnitManager.getInstance().refreshCache("","");
            VendorManager.getInstance().refreshCache("","");
            
            msg = "Cache Cleared Successfully";            
        }
        catch (Exception e)
        {
        	msg = "ERROR Clearing Cache:" + e.getMessage();
        	e.printStackTrace();
            log.error(e);
        }
	}
}
