package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankBook;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.financial.BankBookTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashBankValidator.java,v 1.3 2007/04/17 13:05:40 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashBankValidator.java,v $
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CashBankValidator extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Bank Code", "Description", "Deposit", "Withdrawal", "Activity Balance", "Current Balance"};
	static final int[] l1 = {5, 12, 25, 20, 20, 20, 20};
	static final int[] a1 = {0, 0, 0, 1, 1, 1, 1};

	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void validateBalance()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Cash/Bank Result : ", s1, l1, a1);			
		
		try
		{							
			List vData = BankTool.getAllBank();
			
			for (int i = 0; i < vData.size(); i++)
			{
				Bank oData = (Bank) vData.get(i);
				String sID = oData.getBankId();
			
				Date dAsOf = oData.getAsDate();
				
				BankBook oBalance = BankBookTool.getBankBook(sID);
				if (oBalance != null)
				{
					double dBalance = oBalance.getAccountBalance().setScale(2,4).doubleValue();
					
					//get all cashflow transaction since beginning balance until now then recalculate 
				    List vTrans = CashFlowTool.getCashFlowByBankID (sID, dAsOf, dNow);
			        double dDeposit = CashFlowTool.filterTotalAmountByType (vTrans, 1);
				    double dWithdrawal = CashFlowTool.filterTotalAmountByType (vTrans, 2);
					double dTrans = dDeposit - dWithdrawal;
					dTrans = (new BigDecimal(dTrans).setScale(2,4)).doubleValue();
					
					if (dBalance != dTrans)
					{
						m_iInvalidResult ++;
						m_oResult.append("\n");
						
						append (null, l1[0]);
						append (oData.getBankCode(), l1[1]);
						append (StringUtil.cut(oData.getDescription(),22,".."), l1[2]);
						append (new Double(dDeposit), l1[3]);
						append (new Double(dWithdrawal), l1[4]);
						
						append (new Double(dTrans), l1[5]);
						append (new Double(dBalance), l1[6]);
					}
				}
			}
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
}
