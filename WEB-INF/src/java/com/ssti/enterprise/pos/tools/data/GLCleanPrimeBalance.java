package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountBalanceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class GLCleanPrimeBalance extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());

	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void cleanPrime(int iYear)
        throws Exception
	{		
        Period oPeriod = PeriodTool.getLastPeriodYear(iYear, null);
        
		if (oPeriod != null && oPeriod.getIsClosed())
		{
			Connection oConn = null;
			try
			{							
                List vPLAccount = new ArrayList();
                vPLAccount.addAll(AccountTool.getAccountByType(GlAttributes.i_REVENUE, false, false));
                vPLAccount.addAll(AccountTool.getAccountByType(GlAttributes.i_COGS, false, false));
                vPLAccount.addAll(AccountTool.getAccountByType(GlAttributes.i_EXPENSE, false, false));
                vPLAccount.addAll(AccountTool.getAccountByType(GlAttributes.i_OTHER_INCOME, false, false));
                vPLAccount.addAll(AccountTool.getAccountByType(GlAttributes.i_OTHER_EXPENSE, false, false));
                vPLAccount.addAll(AccountTool.getAccountByType(GlAttributes.i_INCOME_TAX, false, false));
                
                Date dAsOf = oPeriod.getEndDate();
                dAsOf = DateUtil.getStartOfDayDate(DateUtil.addDays(dAsOf, 1));
                m_oResult.append("Prime Balance Clean UP: ")
                         .append(CustomFormatter.formatDateTime(dAsOf)).append("\n");

                for (int i = 0; i < vPLAccount.size(); i++)
                {
                    Account oAcc = (Account) vPLAccount.get(i);
                    double[] dBal =  AccountBalanceTool.getBalance(oAcc.getAccountId(), dAsOf);
                    double dPrime = dBal[i_PRIME];
                    BigDecimal bdPrime = new BigDecimal(dPrime).setScale(4,BigDecimal.ROUND_HALF_UP);
                    
                    if (dPrime != 0)
                    {
                        String sSign = "-";
                        if (dPrime < 0) sSign = "+";
                        String sSQL = "UPDATE account_balance SET prime_balance  = prime_balance " + sSign + 
                                      " " + bdPrime + " WHERE account_id = '" + oAcc.getAccountId() + "'";
                        
                        m_oResult.append(oAcc.getAccountCode()).append(" ") 
                                 .append(StringUtil.left(oAcc.getAccountName(),30))
                                 .append(": ").append(sSQL).append("\n");
                        BasePeer.executeStatement(sSQL);
                    }                    
                }
			}
			catch (Exception _oEx)
			{
				handleError (oConn, _oEx);
			}
		}
	}
}
