package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;

import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate GL Transaction, Display unbalance transaction
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.5  2007/07/02 15:39:12  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/06/07 17:23:25  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class GLTransValidator extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	int iTotalChecked = 0;
	int iTotalUnbalanced = 0;
	long iElapsed = 0;
	StringBuilder sMessage = new StringBuilder();
	
	Connection oConn = null;

	public Map getUnbalanced(int _iType, String _sFrom, String _sTo)
	{
		return getUnbalanced(_iType, _sFrom, _sTo, "0.01");
	}
	
	public Map getUnbalanced(int _iType, String _sFrom, String _sTo, String _sTolerance)
	{		
		Map mUnbalanced = new HashMap();
		try
		{			
			oConn = Torque.getConnection(AppAttributes.s_DB_NAME);
			
			if (StringUtil.isEmpty(_sTolerance)) _sTolerance = "0";
			double _dTolerance = Double.parseDouble(_sTolerance);
			log.debug("UNBALANCE TOLERANCE : " + _dTolerance);
			if (_dTolerance == 0) _dTolerance = 0.01;
			if (_iType > 0)
			{
				mUnbalanced.put(Integer.valueOf(_iType), getUnbalancedByType(_iType,_sFrom, _sTo, _dTolerance));
			}
			else
			{
				for (int i = 1; i <= 20; i++)
				{
					List v = getUnbalancedByType(i, _sFrom, _sTo, _dTolerance);
					if (v.size() > 0) mUnbalanced.put(Integer.valueOf(i), v);					
				}
			}
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
		finally
		{
			Torque.closeConnection(oConn);
		}
		return mUnbalanced;
	}
	
	private List getUnbalancedByType(int _iType, String _sFrom, String _sTo, double _dTolerance)
	{		
		List vUnbalanced = new ArrayList();
		try
		{	
			StopWatch oSW = new StopWatch();
			oSW.start("all");

			oSW.start("distinct");
			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT DISTINCT transaction_id, transaction_no FROM  gl_transaction ")
				.append(" WHERE transaction_type = " ).append(_iType);
			if (StringUtil.isNotEmpty(_sFrom)) {oSQL.append(" AND transaction_date >= '").append(_sFrom).append("'");}
			if (StringUtil.isNotEmpty(_sTo)) {oSQL.append(" AND transaction_date <= '").append(_sTo).append("'");}			
			log.debug(oSQL);
			
			Statement oST = oConn.createStatement();
			ResultSet oRS = oST.executeQuery(oSQL.toString());
			oSW.stop("distinct");
			oSW.result("distinct");
			
			while (oRS.next())
			{
				iTotalChecked++;
				String sDK = "sum DB " + iTotalChecked;
				String sCK = "sum CR " + iTotalChecked;
				
				oSW.start(sDK);
				String sTransID = oRS.getString(1);
				String sTransNo = oRS.getString(2);
				
				String sDebit = "SELECT SUM(amount), SUM(amount_base) FROM gl_transaction " + 
								 " WHERE debit_credit = 1 AND transaction_id = ?";
				
				PreparedStatement oPSD = oConn.prepareStatement(sDebit);
				oPSD.setString(1, sTransID);
				ResultSet oRSD = oPSD.executeQuery();
				oRSD.next();
				double dDebit = oRSD.getDouble(1);
				double dDebBs = oRSD.getDouble(2);
				oSW.stop(sDK);
				
				oSW.start(sCK);
				String sCredit = "SELECT SUM(amount), SUM(amount_base) FROM gl_transaction " + 
								 " WHERE debit_credit = 2 AND transaction_id = ?";
				
				PreparedStatement oPSC = oConn.prepareStatement(sCredit);
				oPSC.setString(1, sTransID);
				ResultSet oRSC = oPSC.executeQuery();
				oRSC.next();
				double dCredit = oRSC.getDouble(1);
				double dCredBs = oRSC.getDouble(2);
				oSW.stop(sCK);
				
				if ((dDebit != dCredit) || (dDebBs != dCredBs))
				{
					double dDelta = 0;
					if (dCredBs > dDebBs) dDelta = dCredBs - dDebBs; else dDelta = dDebBs - dCredBs;
					if (dDelta > _dTolerance )
					{
						log.debug(dDelta);
						vUnbalanced.add(GlTransactionTool.getDataByTypeAndTransID(_iType, sTransID, oConn));
						iTotalUnbalanced++;
					}
					else
					{	
						sMessage.append("Unbalanced Trans : ")
							    .append(sTransNo)
							    .append(" Total Debit : ").append(dDebit)
							    .append(" Total Credit : ").append(dCredit)							    
							    .append(" Difference is : ").append(dDelta).append("\n");
					}
				}
			}
			oRS.close();
			oSW.stop("all");
			log.debug(oSW.result("all"));
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
		return vUnbalanced;
	}

	public long getElapsed() {
		return iElapsed;
	}
	public int getTotalChecked() {
		return iTotalChecked;
	}
	public int getTotalUnbalanced() {
		return iTotalUnbalanced;
	}
	public String getMessage() {
		return sMessage.toString();
	}
	
	/**
	 * Temprorary code for grouping update
	 * 
	 * @param _sFrom
	 * @param _sTo
	 */
	public void fixGroupingUnbalance(String _sFrom, String _sTo, String _sTolerance)
	{		
		int iTotalUpdated = 0;
		try
		{	
			double _dTolerance = Double.parseDouble(_sTolerance);
			log.debug("UNBALANCE TOLERANCE : " + _dTolerance);
			
			oConn = Torque.getConnection(AppAttributes.s_DB_NAME); 			

			StopWatch oSW = new StopWatch();
			oSW.start("all");

			oSW.start("distinct");
			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT DISTINCT transaction_id, transaction_no FROM  gl_transaction ")
				.append(" WHERE transaction_type = 9 " );
			if (StringUtil.isNotEmpty(_sFrom)) {oSQL.append(" AND transaction_date >= '").append(_sFrom).append("'");}
			if (StringUtil.isNotEmpty(_sTo)) {oSQL.append(" AND transaction_date <= '").append(_sTo).append("'");}			
			
			log.debug(oSQL);
			
			Statement oST = oConn.createStatement();
			ResultSet oRS = oST.executeQuery(oSQL.toString());
			oSW.stop("distinct");
			oSW.result("distinct");
			
			while (oRS.next())
			{
				iTotalChecked++;
				String sDK = "sum DB " + iTotalChecked;
				String sCK = "sum CR " + iTotalChecked;
				
				oSW.start(sDK);
				String sTransID = oRS.getString(1);
				String sTransNo = oRS.getString(2);
				
				String sDebit = "SELECT SUM(amount), SUM(amount_base) FROM gl_transaction " + 
								 " WHERE debit_credit = 1 AND transaction_id = ?";
				
				PreparedStatement oPSD = oConn.prepareStatement(sDebit);
				oPSD.setString(1, sTransID);
				ResultSet oRSD = oPSD.executeQuery();
				oRSD.next();
				double dDebit = oRSD.getDouble(1);
				double dDebBs = oRSD.getDouble(2);
				oSW.stop(sDK);
				
				oSW.start(sCK);
				String sCredit = "SELECT SUM(amount), SUM(amount_base) FROM gl_transaction " + 
								 " WHERE debit_credit = 2 AND transaction_id = ?";
				
				PreparedStatement oPSC = oConn.prepareStatement(sCredit);
				oPSC.setString(1, sTransID);
				ResultSet oRSC = oPSC.executeQuery();
				oRSC.next();
				double dCredit = oRSC.getDouble(1);
				double dCredBs = oRSC.getDouble(2);
				oSW.stop(sCK);
				
				if ((dDebit != dCredit) || (dDebBs != dCredBs))
				{
					double dFDelta = 0;
					if (dCredit > dDebit) dFDelta = dCredit - dDebit; else dFDelta = dDebit - dCredit;
					if (dFDelta > _dTolerance)
					{
						oSW.start("update");
						StringBuilder s = new StringBuilder();
						s.append("Unbalanced Trans : ").append(sTransNo)
						 .append(" Total Debit : ").append(dDebit)
						 .append(" Total Credit : ").append(dCredit)							    
						 .append(" Difference is : ").append(dCredit - dDebit).append("\n");
						
						log.debug(s);
						sMessage.append(s);
						
						String sAccSQL = 
							"SELECT g.gl_transaction_id, g.amount, g.amount_base, g.account_id " + 
							" FROM gl_transaction g, account a " + 
							" WHERE g.account_id = a.account_id AND a.account_type = 12 AND transaction_id = ?";
						
						PreparedStatement oPSA = oConn.prepareStatement(sAccSQL);
						oPSA.setString(1, sTransID);
						List vCOGS = new ArrayList(5);
						ResultSet oRSA = oPSA.executeQuery();
						int iTotal = 0;
						while (oRSA.next())
						{
							iTotal++;
							GlTransaction oGLTrans = new GlTransaction();
							oGLTrans.setGlTransactionId(oRSA.getString(1));
							oGLTrans.setAmount(oRSA.getBigDecimal(2));
							oGLTrans.setAmountBase(oRSA.getBigDecimal(3));
							oGLTrans.setAccountId(oRSA.getString(4));
							vCOGS.add(oGLTrans);
						}					
						double dDelta = (dCredit - dDebit) / iTotal;
						double dDelBs = (dCredBs - dDebBs) / iTotal;
						
						for (int i = 0; i < vCOGS.size(); i++)
						{
							String sCostSQL = 
								"UPDATE gl_transaction set amount = ?, amount_base = ? " + 
								" WHERE gl_transaction_id = ? " ;
							
							GlTransaction oGL = (GlTransaction)vCOGS.get(i);
							double dAmount = oGL.getAmount().doubleValue() + dDelta;
							double dAmntBs = oGL.getAmountBase().doubleValue() + dDelBs;
							
							PreparedStatement oPS = oConn.prepareStatement(sCostSQL);
							oPS.setDouble(1,dAmount);
							oPS.setDouble(2,dAmntBs);
							oPS.setString(3,oGL.getGlTransactionId());
							oPS.execute();
							
							String sBalanceSQL = 
								"UPDATE account_balance set balance = balance + " + dDelBs +
								" WHERE account_id = '" + oGL.getAccountId() + "'" ;
														
							Statement oPSBal = oConn.createStatement();
							oPSBal.execute(sBalanceSQL);							
						}
						iTotalUpdated++;
						oSW.stop("update");
						log.debug(oSW.result("update"));
					}
				}
			}
			oRS.close();
			oSW.stop("all");
			log.debug("Total Updated : " + iTotalUpdated);
			log.debug(oSW.result("all"));
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
		finally
		{
			Torque.closeConnection(oConn);
		}
	}		
}
