package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.gl.AccountBalanceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate InventoryLocation and Transaction
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InventoryTransactionValidator.java,v 1.5 2008/06/29 07:12:11 albert Exp $ <br>
 *
 * <pre>
 * $Log: InventoryTransactionValidator.java,v $
 * Revision 1.5  2008/06/29 07:12:11  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/04/18 06:51:46  albert
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/27 02:20:02  albert
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/15 12:41:30  albert
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/13 03:07:21  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/07/02 15:39:12  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class InventoryTransactionValidator extends BaseValidator implements GlAttributes
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Trans No", "Account Code", "Inv Trans Cost", "GL Cost"};
	static final int[] l1 = {5, 25, 25, 18, 18};

	static final String[] s2 = {"No", "Transfer No", "Item Code", "From Cost", "To Cost"};
	static final int[] l2 = {5, 25, 25, 18, 18};


	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void validateTrans(String _sTransNo)
	{
		prepareTitle ("Validate Inventory Transaction Result : ", s1, l1);			
		
		try
		{				
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			Statement oStmt = oConn.createStatement();
			
			String sSQL = "SELECT DISTINCT(transaction_no), transaction_id, transaction_type " + 
						  " FROM inventory_transaction WHERE transaction_type IN (1, 2, 3, 4, 6, 7, 10, 11, 12, 13) ";
			
			if (StringUtil.isNotEmpty(_sTransNo))
			{
				sSQL = "SELECT DISTINCT(transaction_no), transaction_id, transaction_type " + 
				  " FROM inventory_transaction WHERE transaction_no = '" + _sTransNo + "'";				
			}
			
			ResultSet oRS  = oStmt.executeQuery(sSQL);
			while (oRS.next())
			{
				String sTransNo = oRS.getString("transaction_no");
				String sTransID = oRS.getString("transaction_id");
				int iType = oRS.getInt("transaction_type");
				
				
				Map mJournal = new HashMap();
				List vInvTrans = InventoryTransactionTool.getByTransID(sTransID, iType, oConn);								
				for (int i = 0; i < vInvTrans.size(); i++)
				{
					InventoryTransaction oTrans = (InventoryTransaction) vInvTrans.get(i);
					Item oItem = ItemTool.getItemByID(oTrans.getItemId(), oConn);					
					System.out.println (oItem.getItemCode() + " " + AccountTool.getCodeByID(oItem.getInventoryAccount()));
					Double dTotalInv = (Double) mJournal.get(oItem.getInventoryAccount());
					if (dTotalInv != null) 
					{
						double dInv = dTotalInv.doubleValue() + oTrans.getTotalCost().doubleValue();						
						mJournal.put(oItem.getInventoryAccount(), Double.valueOf(dInv));
					}
					else
					{
						mJournal.put(oItem.getInventoryAccount(), Double.valueOf(oTrans.getTotalCost().doubleValue()));
					}
				}
				
				Iterator iter = mJournal.keySet().iterator();
				while (iter.hasNext())
				{
					String sInvAcc = (String) iter.next();
					Double oCost = (Double) mJournal.get(sInvAcc);
					double dCost = oCost.doubleValue();
					if ((iType == i_INV_TRANS_SALES_INVOICE || 
						 iType == i_INV_TRANS_ISSUE_UNPLANNED || 
						 iType == i_INV_TRANS_PURCHASE_RETURN) && dCost < 0) 
					{
						dCost = dCost * -1;
					}
					
					int iGLType = mapInvTypeToGL(iType);
					GlTransaction oInvGL = GlTransactionTool.getByTransAndAccountID(iGLType, sTransID, sInvAcc, "", oConn); 
										
					String sCost = CustomFormatter.formatAligned(Double.valueOf(dCost));					
					if (oInvGL != null)
					{
						String sGLCost = CustomFormatter.formatAligned(oInvGL.getAmountBase());
						if (!sCost.equals(sGLCost))
						{
							Account oAcc = AccountTool.getAccountByID(oInvGL.getAccountId(), oConn);
							m_iInvalidResult ++;
							m_oResult.append("\n");
							
							append (null, l1[0]);
							append (sTransNo, l1[1]);
							append (oAcc.getAccountCode(), l1[2]);							
							append (dCost, l1[3]);
							append (oInvGL.getAmountBase().doubleValue(), l1[4]);
						}
					}
					else
					{
						m_iInvalidResult ++;
						m_oResult.append("\n");
						
						append (null, l1[0]);
						append (sTransNo, l1[1]);
						append ("Invalid", l1[2]);							
						append (dCost, l1[3]);
						append (bd_ZERO, l1[4]);
					}
				}
			}	
			Transaction.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
	
	public void repairInvTransGL(String _sTransNo)
		throws Exception
	{
		oConn = Transaction.begin(AppAttributes.s_DB_NAME);
		Statement oStmt = oConn.createStatement();
		
		String sSQL = "SELECT transaction_no, transaction_id, transaction_type " + 
			   		  " FROM inventory_transaction WHERE transaction_no = '" + _sTransNo + "'";				
		
		ResultSet oRS  = oStmt.executeQuery(sSQL);
		while (oRS.next())
		{
			String sTransNo = oRS.getString("transaction_no");
			String sTransID = oRS.getString("transaction_id");
			int iType = oRS.getInt("transaction_type");

			Map mJournal = new HashMap();
			List vInvTrans = InventoryTransactionTool.getByTransID(sTransID, iType, oConn);								
			for (int i = 0; i < vInvTrans.size(); i++)
			{
				InventoryTransaction oTrans = (InventoryTransaction) vInvTrans.get(i);
				Item oItem = ItemTool.getItemByID(oTrans.getItemId(), oConn);					
				Double dTotalInv = (Double) mJournal.get(oItem.getInventoryAccount());
				if (dTotalInv != null) 
				{
					double dInv = dTotalInv.doubleValue() + oTrans.getTotalCost().doubleValue();						
					mJournal.put(oItem.getInventoryAccount(), Double.valueOf(dInv));
				}
				else
				{
					mJournal.put(oItem.getInventoryAccount(), Double.valueOf(oTrans.getTotalCost().doubleValue()));
				}
			}
			
			Iterator iter = mJournal.keySet().iterator();
			while (iter.hasNext())
			{
				String sInvAcc = (String) iter.next();
				Double oCost = (Double) mJournal.get(sInvAcc);
				double dCost = oCost.doubleValue();
				if ((iType == i_INV_TRANS_SALES_INVOICE || 
					 iType == i_INV_TRANS_ISSUE_UNPLANNED || 
					 iType == i_INV_TRANS_PURCHASE_RETURN) && dCost < 0) 
				{
					dCost = dCost * -1;
				}
				
				int iGLType = mapInvTypeToGL(iType);
				GlTransaction oInvGL = GlTransactionTool.getByTransAndAccountID(iGLType, sTransID, sInvAcc, "", oConn); 
									
				String sCost = CustomFormatter.formatAligned(Double.valueOf(dCost));					
				if (oInvGL != null)
				{
					String sGLCost = CustomFormatter.formatAligned(oInvGL.getAmountBase());
					if (!sCost.equals(sGLCost))
					{
						double dSTDelta = dCost - oInvGL.getAmountBase().doubleValue();
						System.out.println("Delta Cost & GL " + dSTDelta);
						List vTRGL = GlTransactionTool.getDataByTypeAndTransID(iGLType, sTransID, oConn);
						GlTransaction oOtherGL = null;
						for (int i = 0; i < vTRGL.size(); i++)
						{
							GlTransaction oTRGL = (GlTransaction) vTRGL.get(i);
							Account oTRAcc =AccountTool.getAccountByID(oTRGL.getAccountId(), oConn);
							if (iGLType == i_GL_TRANS_SALES_INVOICE)
							{
								if (!oTRAcc.getAccountId().equals(sInvAcc) && 
									 oTRAcc.getAccountType() == i_COGS &&
									 (Calculator.precise(oTRGL.getAmountBase().doubleValue()) == 
									  Calculator.precise(oInvGL.getAmountBase().doubleValue()))
								    )
								{
									oOtherGL = oTRGL;
								}
							}
						}
						if (oOtherGL != null)
						{
							GlTransactionTool.updateSalesGLTrans(
								iGLType, sTransID, sTransNo, oInvGL.getAccountId(), oOtherGL.getAccountId(), dSTDelta, oConn);
						}
					}
				}
			}
		}	
		Transaction.commit(oConn);
		footer();
		
		validateTrans(_sTransNo);
	}
	
	public void validateTransfer(boolean _bRepair)
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Item Transfer Result : ", s2, l2);			
		
		Map mInvalidItem = new HashMap();
		try
		{				
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			Statement oStmt = oConn.createStatement();
			
			String sq1 = "SELECT DISTINCT item_id FROM invalidatransfer ";			
			List v1 = SqlUtil.executeQuery(sq1);
			for (int i = 0; i < v1.size(); i++)
			{
				Record r1 = (Record) v1.get(i);
				String sItemID = r1.getValue(1).asString();
				
				String sq2 = "SELECT transaction_id FROM invalidatransfer WHERE item_id = '" + sItemID + "'";
				List v2 = SqlUtil.executeQuery(sq2);
				for (int j = 0; j < v2.size(); j++)
				{
					Record r2 = (Record) v2.get(j);
					String sTransID = r2.getValue(1).asString();
				}
			}
			
//			ResultSet oRS  = oStmt.executeQuery(sSQL);
//			while (oRS.next())
//			{
//				String sTransNo = oRS.getString("transaction_no");
//				String sTransID = oRS.getString("transaction_id");
//				int iType = oRS.getInt("transaction_type");
//				
//				Map mTransfer = new HashMap();
//				List vInvTrans = InventoryTransactionTool.getByTransID(sTransID, iType, oConn);								
//				for (int i = 0; i < vInvTrans.size(); i++)
//				{
//					InventoryTransaction oTrans = (InventoryTransaction) vInvTrans.get(i);
//					Item oItem = ItemTool.getItemByID(oTrans.getItemId(), oConn);					
//					List vTrf = (List) mTransfer.get(oItem.getItemId());												
//					if (vTrf == null) 
//					{
//						mTransfer.put(oItem.getItemId(), getInvTrans(sTransID, oItem.getItemId(), oConn));
//					}
//				}
//				
//				Iterator iter = mTransfer.keySet().iterator();
//				while (iter.hasNext())
//				{
//					String sItemID = (String) iter.next();
//					List vTrans = (List) mTransfer.get(sItemID);
//															
//					if (vTrans.size() == 2)
//					{
//						InventoryTransaction oTR1 = (InventoryTransaction) vTrans.get(0);
//						InventoryTransaction oTR2 = (InventoryTransaction) vTrans.get(1);
//						
//						String sTR1 = CustomFormatter.formatAligned(oTR1.getCost());						
//						String sTR2 = CustomFormatter.formatAligned(oTR2.getCost());
//						if (!sTR1.equals(sTR2))
//						{
//							m_iInvalidResult ++;
//							m_oResult.append("\n");
//							
//							append (null, l2[0]);
//							append (sTransNo, l2[1]);
//							append (oTR1.getItemCode(), l2[2]);							
//							append (oTR1.getCost(), l2[3]);
//							append (oTR2.getCost(), l2[4]);
//							
//							mInvalidItem.put(sItemID, vTrans);
//							if (_bRepair)
//							{
//								oTR2.setCost(oTR1.getCost());
//								//InventoryTransactionTool.recalculate(oTR2, false, oConn);
//							}
//							//InventoryTransactionTool.recalculateAll(oTR1.getItemId(), oTR1.getLocationId(), oConn);						
//						}
//					}
//					else
//					{
//						m_iInvalidResult ++;
//						m_oResult.append("\n");
//						
//						append (null, l2[0]);
//						append (sTransNo, l2[1]);
//						append ("Invalid Trans", l2[2]);	
//						append (bd_ZERO, l2[3]);
//						append (bd_ZERO, l2[4]);
//					}
//				}
//			}	
			Transaction.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
	
	public static void repair(Map _mItem, Connection _oConn)
		throws Exception
	{
		Iterator iter = _mItem.keySet().iterator();
		while (iter.hasNext())
		{
			String sItemID = (String) iter.next();
			InventoryTransactionTool.recalculateAll(sItemID, PreferenceTool.getLocationID(), _oConn);
		}
	}
	
	public static List getInvTrans(String _sTransID, String _sItemID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(InventoryTransactionPeer.TRANSACTION_ID, _sTransID);
		oCrit.add(InventoryTransactionPeer.ITEM_ID, _sItemID);
		return InventoryTransactionPeer.doSelect(oCrit,_oConn);
	}
	
	public static int mapInvTypeToGL(int iType)
	{
		if (iType == i_INV_TRANS_RECEIPT_UNPLANNED) return i_GL_TRANS_RECEIPT_UNPLANNED ;
		if (iType == i_INV_TRANS_ISSUE_UNPLANNED  ) return i_GL_TRANS_ISSUE_UNPLANNED   ;
		if (iType == i_INV_TRANS_PURCHASE_RECEIPT ) return i_GL_TRANS_PURCHASE_RECEIPT  ;
		if (iType == i_INV_TRANS_PURCHASE_RETURN  ) return i_GL_TRANS_PURCHASE_RETURN   ;
		if (iType == i_INV_TRANS_TRANSFER_OUT 	  ) return i_GL_TRANS_ITEM_TRANSFER 	;
		if (iType == i_INV_TRANS_TRANSFER_IN 	  ) return i_GL_TRANS_ITEM_TRANSFER 	;		
		if (iType == i_INV_TRANS_SALES_INVOICE 	  ) return i_GL_TRANS_SALES_INVOICE 	; 
		if (iType == i_INV_TRANS_SALES_RETURN 	  ) return i_GL_TRANS_SALES_RETURN 	    ; 
		if (iType == i_INV_TRANS_PURCHASE_INVOICE ) return i_GL_TRANS_PURCHASE_INVOICE  ;
		if (iType == i_INV_TRANS_DELIVERY_ORDER   ) return i_GL_TRANS_DELIVERY_ORDER    ;
		if (iType == i_INV_TRANS_JOB_COSTING      ) return i_GL_TRANS_JOB_COSTING       ;
		if (iType == i_INV_TRANS_JC_ROLLOVER 	  ) return i_GL_TRANS_JC_ROLLOVER 	    ;
		return -1;
	}
	
	public void fixSalesGL(int _iType)
	{
		if(_iType == i_INV_TRANS_DELIVERY_ORDER || _iType == i_INV_TRANS_SALES_INVOICE || _iType == i_INV_TRANS_SALES_RETURN || _iType == i_INV_TRANS_PURCHASE_RECEIPT)
		{
			Connection con = null;
			String sSQL = "SELECT *, (invcost - glcost) AS diff FROM invtrans_gltrans "; 
			sSQL += " WHERE ((invcost - glcost) > 0.1 OR  (invcost - glcost) < -0.1) AND txtp = " + _iType;
			try 
			{
				List vData = SqlUtil.executeQuery(sSQL);
				for(int i = 0; i < vData.size(); i++)
				{
					Record oRec = (Record) vData.get(i);
					String sID = oRec.getValue("txid").asString();
					String sNo = oRec.getValue("txno").asString();
					String sInv = oRec.getValue("invacc").asString();
					String sCOGS = GLConfigTool.getGLConfig().getCogsAccount();
					double dDiff = oRec.getValue("diff").asDouble();
					if(_iType == i_INV_TRANS_DELIVERY_ORDER)
					{
						if(StringUtil.isNotEmpty(GLConfigTool.getGLConfig().getDeliveryOrderAccount()))
						{
							sCOGS = GLConfigTool.getGLConfig().getDeliveryOrderAccount();
						}
					}
					if(_iType == i_INV_TRANS_PURCHASE_RECEIPT)
					{
						sCOGS = GLConfigTool.getGLConfig().getUnbilledGoodsAccount();
					}

					m_oResult.append(" \nTrans NO: ").append(sNo) 
						     .append(" INV:").append(CustomFormatter.fmt(oRec.getValue("invcost").asDouble()))
						     .append(" GL:").append(CustomFormatter.fmt(oRec.getValue("glcost").asDouble()))
						     .append(" DIFF:").append(CustomFormatter.fmt(dDiff));					
										
					StopWatch oSW = null;		
					String sSWKey = null;
	
					sSWKey = "Recalculate " + sNo ; 
					oSW = new StopWatch();
					oSW.start(sSWKey);				
	
					con = BaseTool.beginTrans();
					int iGLType = mapInvTypeToGL(_iType);
					GlTransactionTool.updateSalesGLTrans(iGLType, sID, sNo, sInv, sCOGS, dDiff, con);
					BaseTool.commit(con);
					
					m_oResult.append(" Time: ").append(oSW.stop(sSWKey)).append("ms ").append(s_LINE_SEPARATOR);
					
					oSW.stop(sSWKey);
				}			
			} 
			catch (Exception e) 
			{
				m_oResult.append("ERROR:").append(e.getMessage());
				e.printStackTrace();
				if(con != null)
				{
					try {
						BaseTool.rollback(con);	
					} 
					catch (Exception e2) 
					{
						e2.printStackTrace();
					}
				}
			}	
		}
	}
	
	/**
	 * update Issue RECEIPT GL TRANS
	 * Need special methods to get GL trans by including D/C parameter, 
	 * because Stock Opname / Item Exchanged Trans / Adjustment IR may 
	 * contains journal like this
	 * 
	 * INV 
	 *    ADJ 
	 * ADJ 
	 *    INV
	 *    
	 * @param _iGLType
	 * @param _iDC
	 * @param _sTransID
	 * @param _sTransNo
	 * @param _sAccountID
	 * @param _sAccPairID
	 * @param _sLocID
	 * @param _dDiff
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateGLTrans (int _iGLType, 
									  int _iDC,
									  String _sTransID, 
									  String _sTransNo,
									  String _sAccountID, 
								      String _sAccPairID,
									  String _sLocID,											  
									  double _dDiff, 
									 Connection _oConn)
	throws Exception
	{
		int iAdjDC = i_CREDIT;
		if (_iDC == i_CREDIT) iAdjDC = i_DEBIT;
		
		GlTransaction oGL1 = GlTransactionTool.getByTransAndAccountID(_iGLType,_iDC,_sTransID,_sAccountID,_sLocID,_oConn);
		GlTransaction oGL2 = GlTransactionTool.getByTransAndAccountID(_iGLType,iAdjDC,_sTransID,_sAccPairID,_sLocID,_oConn);

		//both pair may not be null
		if (oGL1 != null && oGL2 != null)
		{
			//if oGL1 && oGL2 is Pair (is debit && credit)
			if (oGL1.getDebitCredit() != oGL2.getDebitCredit())
			{
				updateGLTrans(oGL1, oGL2, _dDiff, _oConn);	
			}
			else
			{
				logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);
			}
		}
		else
		{
			logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);	
		}
	}	
	
	private static void updateGLTrans (GlTransaction oGL1, 
			GlTransaction oGL2,
			double _dDiff,
			Connection _oConn)
					throws Exception
	{
		if (oGL1 != null && oGL2 != null)
		{
			GlTransaction oOldGL1 = oGL1.copy();
			GlTransaction oOldGL2 = oGL2.copy();

			oGL1.setAmount(new BigDecimal(oGL1.getAmount().doubleValue() + _dDiff));
			oGL1.setAmountBase(
					new BigDecimal(oGL1.getAmount().doubleValue() * oGL1.getCurrencyRate().doubleValue())
					);

			oGL2.setAmount(new BigDecimal(oGL2.getAmount().doubleValue() + _dDiff));
			oGL2.setAmountBase(
					new BigDecimal(oGL2.getAmount().doubleValue() * oGL2.getCurrencyRate().doubleValue())
					);

			oGL1.save(_oConn);
			oGL2.save(_oConn);

			AccountBalanceTool.updateBalance(oOldGL1, oGL1, _oConn);		
			AccountBalanceTool.updateBalance(oOldGL2, oGL2, _oConn);
		}
	}

	private static void logUpdateError(String _sTransNo, 
			String _sAccID, 
			String _sPairID, 
			GlTransaction oGL1, 
			GlTransaction oGL2,
			boolean _bThrowEx,
			Connection _oConn)
					throws Exception
	{
		StringBuilder oErr = new StringBuilder();
		if (oGL1 == null || oGL2 == null)
		{
			Account oAcc1 = AccountTool.getAccountByID(_sAccID, _oConn);
			Account oAcc2 = AccountTool.getAccountByID(_sPairID, _oConn);
			oErr.append("Update GL Trans pair checking failed");		
			if (oGL1 == null)
			{
				oErr.append(" GL Trans 1 ").append(_sTransNo)
				.append(" Account ").append(oAcc1.getAccountCode()).append(" not found");
			}
			if (oGL2 == null)
			{
				oErr.append(" GL Trans 2 ").append(_sTransNo)
				.append(" Account ").append(oAcc2.getAccountCode()).append(" not found");			
			}
		}
		if (oGL1 != null && oGL2 != null)
		{
			if (oGL1.getDebitCredit() == oGL2.getDebitCredit())
			{
				Account oAcc1 = AccountTool.getAccountByID(oGL1.getAccountId(), _oConn);
				Account oAcc2 = AccountTool.getAccountByID(oGL2.getAccountId(), _oConn);

				oErr.append("Update GL Trans failed, Invalid Journal Debit/Credit")
				.append(" Trans : ").append(oGL1.getTransactionNo()).append("\n")
				.append(oAcc1.getAccountCode()).append(" ").append(oAcc1.getAccountName())
				.append(oGL1.getAmount()).append(" ").append(oGL1.getDebitCredit()).append("\n")
				.append(oAcc2.getAccountCode()).append(" ").append(oAcc2.getAccountName())
				.append(oGL2.getAmount()).append(" ").append(oGL2.getDebitCredit());				
			}
		}		
		if(StringUtil.isNotEmpty(oErr.toString()) && _bThrowEx) throw new NestableException(oErr.toString());
		//log.error(oErr);
	}
}
