package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

import com.ssti.enterprise.pos.manager.InventoryLocationManager;
import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;


/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate InventoryLocation and Transaction
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InventoryValidator.java,v 1.7 2008/08/28 12:23:39 albert Exp $ <br>
 *
 * <pre>
 * $Log: InventoryValidator.java,v $
 * Revision 1.7  2008/08/28 12:23:39  albert
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/23 09:47:36  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/07/02 15:39:12  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class InventoryValidator extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Item Code", "Item Name", "Location", "Last Trans", "Qty Bal", "Inv Loc"};
	static final int[] l1 = {5, 12, 30, 20, 20, 10, 10};

	static final String[] s2 = {"No", "Item Code", "Item Name", "Location", "Last Trans", "Cost ", "Inv Loc"};
	static final int[] l2 = {5, 12, 25, 20, 20, 15, 15};

	static final String[] s3 = {"No", "Location", "Item Code", "Item Name", "OB"};
	static final int[] l3 = {5, 30, 12, 30, 15};

	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void validateQtyBalance()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Qty Balance Result : ", s1, l1);			
		
		try
		{				
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			Statement oStmt = oConn.createStatement();
			
			ResultSet oRS  = oStmt.executeQuery(s_ALL_INV_LOC);
			while (oRS.next())
			{
				String sItemID = oRS.getString("item_id");
				String sLocationID = oRS.getString("location_id"); 
				String sLocationName = oRS.getString("location_name"); 
				double dCurrentQty = oRS.getDouble("current_qty");
				
				InventoryTransaction oLast = 
					InventoryTransactionTool.getLastTrans (sItemID, sLocationID, dNow, oConn );
				
				double dLastQtyBalance = oLast.getQtyBalance().doubleValue();
				String sTransNo = oLast.getTransactionNo();
				
				if (dCurrentQty != dLastQtyBalance)
				{
					Item oItem = ItemTool.getItemByID(sItemID, oConn);

					m_iInvalidResult ++;
					m_oResult.append("\n");
					
					append (null, l1[0]);
					append (oItem.getItemCode(), l1[1]);
					append (StringUtil.cut(oItem.getItemName(),22,".."), l1[2]);
					append (StringUtil.cut(sLocationName,16,".."), l1[3]);
					append (sTransNo, l1[4]);
					append (new Double(dLastQtyBalance), l1[5]);
					append (new Double(dCurrentQty), l1[6]);
				}			
			}	
			Transaction.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
	
	public void validateCost()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Cost Result : ", s2, l2);			
		
		try
		{				
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			Statement oStmt = oConn.createStatement();
			
			ResultSet oRS  = oStmt.executeQuery(s_ALL_INV_LOC);
			while (oRS.next())
			{
				String sItemID = oRS.getString("item_id");
				String sLocationID = oRS.getString("location_id"); 
				String sLocationName = oRS.getString("location_name"); 

				double dCurrentQty = oRS.getDouble("current_qty");				
				double dItemCost = oRS.getDouble("item_cost");
				
				InventoryTransaction oLast = 
					InventoryTransactionTool.getLastTrans (sItemID, sLocationID, dNow, oConn );
				
				double dLastQtyBalance = oLast.getQtyBalance().doubleValue();
				double dLastValueBalance = oLast.getValueBalance().doubleValue();
				double dTransCost = oLast.getCost().doubleValue();
				if (dLastQtyBalance > 0)
				{
					dTransCost = dLastValueBalance /dLastQtyBalance;
				}

				dTransCost = (new BigDecimal (dTransCost)).setScale(4, 4).doubleValue();
				dItemCost = (new BigDecimal (dItemCost)).setScale(4, 4).doubleValue();
								
				String sTransNo = oLast.getTransactionNo();
				
				if (dItemCost != dTransCost && dCurrentQty > 0)
				{
					Item oItem = ItemTool.getItemByID(sItemID, oConn);

					m_iInvalidResult ++;
					m_oResult.append("\n");
					
					append (null, l2[0]);
					append (oItem.getItemCode(), l2[1]);
					append (StringUtil.cut(oItem.getItemName(),26,".."), l2[2]);
					append (StringUtil.cut(sLocationName,16,".."), l2[3]);
					append (sTransNo, l2[4]);
					append (new Double(dTransCost), l2[5]);
					append (new Double(dItemCost), l2[6]);
				}			
			}	
			Transaction.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}	
	
	public void validateOB (boolean _bRepair, String _sItemCode, String _sLocationCode)
		throws Exception
	{
		Date dNow = new Date();		
		Date dStart = PreferenceTool.getStartDate();
		String sStart = CustomFormatter.formatDate(dStart);
		String sNow = CustomFormatter.formatDate(dNow);
		
		prepareTitle ("Validate Opening Balance Result : ", s3, l3);			
		
		try
		{
			//m_oResult.append("\nLocation : ").append(oLoc.getLocationName());
			
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			Statement oStmt = oConn.createStatement();

			List vLoc = new ArrayList();
			if (StringUtil.isNotEmpty(_sLocationCode))
			{
				Location oLoc = LocationTool.getLocationByCode(_sLocationCode);
				vLoc.add(oLoc);
			}
			else
			{
				vLoc = LocationTool.getAllLocation();
			}
			String sQuery = "select * from item ";
			if (StringUtil.isNotEmpty(_sItemCode))
			{
				sQuery = sQuery + "where item_code LIKE '" + _sItemCode + "'";
			}
			
			ResultSet oRS  = oStmt.executeQuery(sQuery);
			while (oRS.next())
			{
				String sItemID = oRS.getString("item_id");
				Item oItem = ItemTool.getItemByID(sItemID, oConn);
				
				for (int i = 0; i < vLoc.size(); i++)
				{
					Location oLoc = (Location) vLoc.get(i);
					
					BigDecimal bOB = 
						InventoryLocationTool.getBalanceAsOf(sItemID, "", oLoc.getLocationId(), dStart);
					
					if (bOB.doubleValue() != 0)
					{
						m_iInvalidResult ++;
						m_oResult.append("\n");
						
						append (null, l3[0]);
						append (oLoc.getLocationName(), l3[1]);						
						append (oItem.getItemCode(), l3[2]);
						append (StringUtil.cut(oItem.getItemName(),26,".."), l2[3]);
						append (bOB, l2[4]);
						
						m_oResult.append("\n     Trans history : \n");
						
						List vTR = InventoryTransactionTool.getItemHistory(
								oItem.getItemId(), sStart, sNow, oLoc.getLocationId()
						);
						
						for (int j = 0; j < vTR.size(); j++)
						{
							InventoryTransaction oTR = (InventoryTransaction) vTR.get(j);
							append ("", 5);
							append (oTR.getTransactionNo(), 20);						
							append (CustomFormatter.formatDateTime(oTR.getTransactionDate()), 20);						
							append (oTR.getQtyChanges(), 10);						
							append (oTR.getCost(), 15);						
							append (oTR.getQtyBalance(), 15);						
							append (oTR.getValueBalance(), 15);			
							m_oResult.append("\n");
						}
						if (_bRepair)
						{
							InventoryTransactionTool.recalculateAll(oItem.getItemId(), oLoc.getLocationId(),  oConn);
						}
					}
				}	
			}
			Transaction.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
	
	public void recalculate(String _sItemCode, String _sLocationCode)
	{
		Connection oConn = null;
		try 
		{
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			String sItemID = ItemTool.getItemByCode(_sItemCode, oConn).getItemId();
			String sLocationID = LocationTool.getIDByCode(_sLocationCode);
			System.out.println("recalculate all");
			
			Criteria oCrit = new Criteria();
			oCrit.add(InventoryLocationPeer.ITEM_ID, sItemID);
			InventoryLocationPeer.doDelete(oCrit, oConn);
	    	InventoryLocationManager.getInstance().clearCache();

			InventoryTransactionTool.recalculateAll(sItemID, sLocationID, oConn);
			Transaction.commit(oConn);
		} 
		catch (Exception e) 
		{
			Transaction.safeRollback(oConn);
		}
	}
	
	public void recalculate(String _sInvTransID)
	{
		Connection oConn = null;
		try 
		{
			oConn = Transaction.begin(AppAttributes.s_DB_NAME);
			
			InventoryTransaction oInvTrans = InventoryTransactionTool.getByID(_sInvTransID, oConn);
			//InventoryTransactionTool.recalculate(oInvTrans, false, oConn);
			Transaction.commit(oConn);
		} 
		catch (Exception e) 
		{
			Transaction.safeRollback(oConn);
		}
	}
}
