package com.ssti.enterprise.pos.tools.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Batch process pending invoice, only used in special case
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.4  2008/08/20 16:01:10  albert
 * *** empty log message ***
 *
 * Revision 1.3  2008/04/11 04:14:41  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class InvoiceBatchProcess extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Inv. No", "Date", "Customer", "Prc. Time", "Message"};
	static final int[] l1 = {8, 20, 15, 25, 15, 100};
	
	public void processInvoice(String _sFrom, String _sTo)
		throws Exception
	{
		prepareTitle ("Invoice Processed Result : ", s1, l1);			
		
		Date dFrom = null;
		Date dTo = null;
		
		if (StringUtil.isNotEmpty(_sFrom)) dFrom = CustomParser.parseDate(_sFrom);
		if (StringUtil.isNotEmpty(_sTo)) dTo = CustomParser.parseDate(_sTo);
		
		List vData = null;
		try
		{
			vData = TransactionTool.findTrans(dFrom, dTo, AppAttributes.i_TRANS_PENDING, "", "", "");
		}
		catch (Exception e)
		{
			log.error("error getting trans : " + e.getMessage());
			e.printStackTrace();
			return;
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			SalesTransaction oTR = (SalesTransaction) vData.get(i);
			List vTD = TransactionTool.getDetailsByID(oTR.getSalesTransactionId());
			List vPmt = new ArrayList (1);
			InvoicePayment oInv 	 = new InvoicePayment();
			oInv.setPaymentTypeId    (oTR.getPaymentTypeId());
			oInv.setPaymentTermId    (oTR.getPaymentTermId());
			oInv.setPaymentAmount    (oTR.getTotalAmount());
			oInv.setTransTotalAmount (oTR.getTotalAmount());
			oInv.setVoucherId        ("");
			oInv.setReferenceNo      ("");           
			vPmt.add (oInv);
			oTR.setStatus(AppAttributes.i_TRANS_PROCESSED);
			
			int no = i + 1; 
			append ("\n" + no, l1[0]);
			
			StopWatch sw = new StopWatch();					
			sw.start("save");				
			try
			{					
				TransactionTool.saveData(oTR, vTD, vPmt);
				
				append (oTR.getInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getCustomerName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("SUCCESS",l1[5]);
			}
			catch (Exception _oEx)
			{
				m_iInvalidResult ++;
				append (oTR.getInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getCustomerName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("ERROR: " + _oEx.getMessage(),l1[5]);
			}
		}	
		footer();
	}
	
	public void cancelInvoice(String _sFrom, String _sTo, String _sUser)
		throws Exception
	{
		prepareTitle ("Invoice Cancel Result : ", s1, l1);			
		
		Date dFrom = null;
		Date dTo = null;
		
		if (StringUtil.isNotEmpty(_sFrom)) dFrom = CustomParser.parseDate(_sFrom);
		if (StringUtil.isNotEmpty(_sTo)) dTo = CustomParser.parseDate(_sTo);
		
		List vData = null;
		try
		{
			vData = TransactionTool.findTrans(dFrom, dTo, AppAttributes.i_TRANS_PROCESSED, "", "", "");
		}
		catch (Exception e)
		{
			log.error("error getting trans : " + e.getMessage());
			e.printStackTrace();
			return;
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			SalesTransaction oTR = (SalesTransaction) vData.get(i);
			List vTD = TransactionTool.getDetailsByID(oTR.getSalesTransactionId());
			oTR.setStatus(AppAttributes.i_TRANS_PROCESSED);
			
			int no = i + 1; 
			append ("\n" + no, l1[0]);
			
			StopWatch sw = new StopWatch();					
			sw.start("save");				
			try
			{					
				TransactionTool.cancelSales(oTR, vTD, _sUser, null);
				
				append (oTR.getInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getCustomerName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("SUCCESS",l1[5]);
			}
			catch (Exception _oEx)
			{
				m_iInvalidResult ++;
				append (oTR.getInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getCustomerName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("ERROR: " + _oEx.getMessage(),l1[5]);
			}
		}	
		footer();
	}
	
	public void cancelPI(String _sFrom, String _sTo, String _sUser)
		throws Exception
	{
		prepareTitle ("Invoice Cancel Result : ", s1, l1);			
		
		Date dFrom = null;
		Date dTo = null;
		
		if (StringUtil.isNotEmpty(_sFrom)) dFrom = CustomParser.parseDate(_sFrom);
		if (StringUtil.isNotEmpty(_sTo)) dTo = CustomParser.parseDate(_sTo);
		
		List vData = null;
		try
		{
			vData = PurchaseInvoiceTool.findTrans(-1, "", dFrom, dTo, "", "", AppAttributes.i_TRANS_PROCESSED, "", "");
		}
		catch (Exception e)
		{
			log.error("error getting trans : " + e.getMessage());
			e.printStackTrace();
			return;
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			PurchaseInvoice oTR = (PurchaseInvoice) vData.get(i);
			List vTD = PurchaseInvoiceTool.getDetailsByID(oTR.getPurchaseInvoiceId());
			oTR.setStatus(AppAttributes.i_TRANS_PROCESSED);
			
			int no = i + 1; 
			append ("\n" + no, l1[0]);
			
			StopWatch sw = new StopWatch();					
			sw.start("save");				
			try
			{					
				PurchaseInvoiceTool.cancelPI(oTR, vTD, null, _sUser, null);
				
				append (oTR.getPurchaseInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getVendorName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("SUCCESS",l1[5]);
			}
			catch (Exception _oEx)
			{
				m_iInvalidResult ++;
				append (oTR.getPurchaseInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getVendorName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("ERROR: " + _oEx.getMessage(),l1[5]);
			}
		}	
		footer();
	}
	
	public void updateDOGLTransLoc(String _sFrom, String _sTo)
		throws Exception
	{
		prepareTitle ("Update DO JOURNAL Result : ", s1, l1);			
		
		Date dFrom = null;
		Date dTo = null;
		
		if (StringUtil.isNotEmpty(_sFrom)) dFrom = CustomParser.parseDate(_sFrom);
		if (StringUtil.isNotEmpty(_sTo)) dTo = CustomParser.parseDate(_sTo);
		
		List vData = null;
		try
		{
			vData = TransactionTool.findTrans(dFrom, dTo, AppAttributes.i_TRANS_PROCESSED, "", "", "");
		}
		catch (Exception e)
		{
			log.error("error getting trans : " + e.getMessage());
			e.printStackTrace();
			return;
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			SalesTransaction oTR = (SalesTransaction) vData.get(i);			
			int no = i + 1; 
			append ("\n" + no, l1[0]);
			
			StopWatch sw = new StopWatch();					
			sw.start("save");				

			try
			{					

				List vTD = TransactionTool.getDetailsByID(oTR.getSalesTransactionId());
				Set mDO = new HashSet();
				for (int j = 0; j < vTD.size(); j++)
				{
					SalesTransactionDetail oTD = (SalesTransactionDetail) vTD.get(j);
					String sDOID = oTD.getDeliveryOrderId();
					if(StringUtil.isNotEmpty(sDOID))
					{
						mDO.add(sDOID);
					}
				}			

				Iterator iter = mDO.iterator();
				String sDONo = "";
				String sLoc = "";
				while(iter.hasNext())
				{
					String sDOID = (String)iter.next();										
					sDONo = DeliveryOrderTool.getHeaderByID(sDOID).getTransactionNo();
					List vGL = GlTransactionTool.getDataByTypeAndTransID(GlAttributes.i_GL_TRANS_DELIVERY_ORDER, sDOID, null);					
					for (int j = 0; j < vGL.size(); j++)
					{
						GlTransaction oGL = (GlTransaction) vGL.get(j);						
						if(StringUtil.isEqual(oGL.getAccountId(), GLConfigTool.getGLConfig().getCogsAccount()) && 
						   !StringUtil.isEqual(oGL.getLocationId(), oTR.getLocationId()))
						{
							sLoc = LocationTool.getCodeByID(oGL.getLocationId()) + " TO " + LocationTool.getCodeByID(oTR.getLocationId());  							
							oGL.setLocationId(oTR.getLocationId());							
							oGL.save();
						}
					}
				}								
				
				append (oTR.getInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (sDONo, l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append (sLoc + " SUCCESS",l1[5]);
			}
			catch (Exception _oEx)
			{
				_oEx.printStackTrace();
				m_iInvalidResult ++;
				append (oTR.getInvoiceNo(), l1[1]);
				append (CustomFormatter.formatDate(oTR.getTransactionDate()), l1[2]);
				append (oTR.getCustomerName(), l1[3]);
				append (Long.valueOf(sw.stop("save")) + " ms", l1[4]);
				append ("ERROR: " + _oEx.getMessage(),l1[5]);
			}
		}	
		footer();
	}
}	

