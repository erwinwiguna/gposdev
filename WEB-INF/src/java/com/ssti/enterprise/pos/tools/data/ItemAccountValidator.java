package com.ssti.enterprise.pos.tools.data;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.KategoriAccount;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate Item Data
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemAccountValidator.java,v 1.1 2008/04/18 06:51:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemAccountValidator.java,v $
 * Revision 1.1  2008/04/18 06:51:46  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 02:04:56  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ItemAccountValidator extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Item Code", "Item Name", "Empty/Invalid fields"};
	static final int[] l1 = {5, 12, 40, 120};

	public void validateItem(int _iType, String _sKategoriCode, boolean _bRepair)
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Item Result : ", s1, l1);			
		
		try
		{				
			StringBuilder sSQL = new StringBuilder("select * from item ");			
			if (_iType > 0)
			{
				sSQL.append("where item_type = ").append(_iType);
			}
			if (StringUtil.isNotEmpty(_sKategoriCode))
			{
				String sID = KategoriTool.getKategoriIDFromCodeList(_sKategoriCode);
				if (StringUtil.isNotEmpty(sID))
				{
					List vID = KategoriTool.getChildIDList(sID);					
					vID.add(sID);
					
					sSQL.append(" and kategori_id in ").append(SqlUtil.convertToINMode(vID));					
				}
			}
			System.out.println(sSQL);
			
			oConn = BaseTool.beginTrans();
			Statement oStmt = oConn.createStatement();
			
			ResultSet oRS  = oStmt.executeQuery(sSQL.toString());
			
			while (oRS.next())
			{
				String sItemID = oRS.getString("item_id");
				
				Item oItem = ItemTool.getItemByID(sItemID, oConn);
				String sItemCode = oItem.getItemCode(); 
				String sItemName = oItem.getItemName(); 
				int iType = oItem.getItemType();
				StringBuilder sDesc = new StringBuilder();							

				String sInvAcc = oItem.getInventoryAccount();
				String sSalesAcc = oItem.getSalesAccount();
				String sRetAcc = oItem.getSalesReturnAccount();
				String sDiscAcc = oItem.getItemDiscountAccount();
				String sCOGSAcc = oItem.getCogsAccount();
				String sPRetAcc = oItem.getPurchaseReturnAccount();
				String sUbgAcc = oItem.getUnbilledGoodsAccount();				
				String sExpAcc = oItem.getExpenseAccount();
				
				KategoriAccount oKat = KategoriTool.findKategoriAccount(oItem.getKategoriId());
				//"inventoryAccount", "salesAccount", "salesReturnAccount", "itemDiscountAccount", 
				//"cogsAccount", "purchaseReturnAccount", "unbilledGoodsAccount"
				boolean bNeedRepair = false;
				
				if (iType == AppAttributes.i_INVENTORY_PART)
				{
					if (StringUtil.isEmpty(sInvAcc))
					{
						sDesc.append("Inventory Account Empty ");	
						bNeedRepair = true;
					}
					else
					{
						if (oKat != null)
						{
							if (!sInvAcc.equals(oKat.getInventoryAccount()))
							{
								Account oAcc = AccountTool.getAccountByID(sInvAcc, oConn);
								Account oKatAcc = AccountTool.getAccountByID(oKat.getInventoryAccount(), oConn);								
								sDesc.append("Inventory Account [").append(oAcc.getAccountCode())
									 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("] ");
							}
						}
					}
					if (StringUtil.isEmpty(sCOGSAcc))
					{
						sDesc.append("COGS Account Empty ");	
						bNeedRepair = true;
					}
					else
					{
						if (oKat != null)
						{
							if (!sCOGSAcc.equals(oKat.getCogsAccount()))
							{
								Account oAcc = AccountTool.getAccountByID(sCOGSAcc, oConn);
								Account oKatAcc = AccountTool.getAccountByID(oKat.getCogsAccount(), oConn);								
								sDesc.append("COGS Account [").append(oAcc.getAccountCode())
									 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("] ");
							}
						}
					}
				}			
				else if (iType == AppAttributes.i_NON_INVENTORY_PART)
				{
					if (StringUtil.isEmpty(sExpAcc))
					{
						sDesc.append("Expense Account Empty ");
						bNeedRepair = true;
					}
					else
					{
						if (oKat != null)
						{
							if (!sExpAcc.equals(oKat.getExpenseAccount()))
							{
								Account oAcc = AccountTool.getAccountByID(sExpAcc, oConn);
								Account oKatAcc = AccountTool.getAccountByID(oKat.getExpenseAccount(), oConn);								
								sDesc.append("Expense Account [").append(oAcc.getAccountCode())
									 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("] ");
							}
						}
					}
				}
				if (iType == AppAttributes.i_INVENTORY_PART || iType == AppAttributes.i_NON_INVENTORY_PART)
				{
					if (StringUtil.isEmpty(sPRetAcc))
					{
						sDesc.append("Purchase Return Account Empty ");
						bNeedRepair = true;
					}
					else
					{
						if (oKat != null)
						{
							if (!sPRetAcc.equals(oKat.getPurchaseReturnAccount()))
							{
								Account oAcc = AccountTool.getAccountByID(sPRetAcc, oConn);
								Account oKatAcc = AccountTool.getAccountByID(oKat.getPurchaseReturnAccount(), oConn);								
								sDesc.append("Purchase Return Account [").append(oAcc.getAccountCode())
									 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("]");
							}
						}
					}
				}
				
				if (StringUtil.isEmpty(sSalesAcc))
				{
					sDesc.append("Sales Account Empty ");
					bNeedRepair = true;
				}
				else
				{
					if (oKat != null)
					{
						if (!sSalesAcc.equals(oKat.getSalesAccount()))
						{
							Account oAcc = AccountTool.getAccountByID(sSalesAcc, oConn);
							Account oKatAcc = AccountTool.getAccountByID(oKat.getSalesAccount(), oConn);								
							sDesc.append("Sales Account [").append(oAcc.getAccountCode())
								 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("] ");
						}
					}
				}
				
				if (StringUtil.isEmpty(sRetAcc))
				{
					sDesc.append("Sales Return Account Empty ");
					bNeedRepair = true;
				}
				else
				{
					if (oKat != null)
					{
						if (!sRetAcc.equals(oKat.getSalesReturnAccount()))
						{
							Account oAcc = AccountTool.getAccountByID(sRetAcc, oConn);
							Account oKatAcc = AccountTool.getAccountByID(oKat.getSalesReturnAccount(), oConn);								
							sDesc.append("Sales Return Account [").append(oAcc.getAccountCode())
								 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("]");
						}
					}
				}

				if (StringUtil.isEmpty(sDiscAcc))
				{
					sDesc.append("Item Discount Account Empty ");		
					bNeedRepair = true;
				}
				else
				{
					if (oKat != null)
					{
						if (!sDiscAcc.equals(oKat.getItemDiscountAccount()))
						{
							Account oAcc = AccountTool.getAccountByID(sDiscAcc, oConn);
							Account oKatAcc = AccountTool.getAccountByID(oKat.getItemDiscountAccount(), oConn);								
							sDesc.append("Item Discount Account [").append(oAcc.getAccountCode())
								 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("]");
						}
					}
				}

				if (StringUtil.isEmpty(sUbgAcc))
				{
					sDesc.append("Unbilled Goods Account Empty ");
					bNeedRepair = true;
				}
				else
				{
					if (oKat != null)
					{
						if (!sUbgAcc.equals(oKat.getUnbilledGoodsAccount()))
						{
							Account oAcc = AccountTool.getAccountByID(sUbgAcc, oConn);
							Account oKatAcc = AccountTool.getAccountByID(oKat.getUnbilledGoodsAccount(), oConn);								
							sDesc.append("Unbilled Goods Account [").append(oAcc.getAccountCode())
								 .append("] != Kategori Account [" ).append(oKatAcc.getAccountCode()).append("]");
						}
					}
				}				
				
				if (StringUtil.isNotEmpty(sDesc.toString()))
				{
					m_iInvalidResult ++;
					m_oResult.append("\n");
					
					append (null, l1[0]);
					append (sItemCode, l1[1]);
					append (StringUtil.cut(sItemName,36,".."), l1[2]);
					append (sDesc.toString(), l1[3]);
				}			
				
				if (_bRepair && bNeedRepair && oKat != null)
				{
					oItem.setInventoryAccount(oKat.getInventoryAccount());
					oItem.setCogsAccount(oKat.getCogsAccount());
					oItem.setSalesAccount(oKat.getSalesAccount());
					oItem.setSalesReturnAccount(oKat.getSalesReturnAccount());
					oItem.setItemDiscountAccount(oKat.getItemDiscountAccount());
					oItem.setPurchaseReturnAccount(oKat.getPurchaseReturnAccount());
					oItem.setExpenseAccount(oKat.getExpenseAccount());
					oItem.setUnbilledGoodsAccount(oKat.getUnbilledGoodsAccount());
					oItem.save(oConn);
					ItemManager.getInstance().refreshCache(oItem);
				}
			}	
			BaseTool.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
}
