package com.ssti.enterprise.pos.tools.data;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate Item Data
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemValidator.java,v 1.5 2008/04/18 06:51:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemValidator.java,v $
 * Revision 1.5  2008/04/18 06:51:46  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 02:04:56  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ItemValidator extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Item Code", "Item Name", "Empty/Invalid fields"};
	static final int[] l1 = {5, 12, 40, 50};
	
	static final String[] a_ITEM_REQ = 
	{
		"barcode", "itemSku", "itemSkuName", "itemName", "description",
		"vendorItemCode", "vendorItemName", "unitId", "purchaseUnitId", 
		"taxId", "purchaseTaxId", "itemPrice"
	};

	static final String[] a_INVENTORY_ITEM_REQ = 
	{
		"barcode", "itemSku", "itemSkuName", "itemName", "description",
		"vendorItemCode", "vendorItemName", "unitId", "purchaseUnitId", 
		"taxId", "purchaseTaxId", "itemPrice", 
		"inventoryAccount", "salesAccount", "salesReturnAccount", "itemDiscountAccount", 
		"cogsAccount", "purchaseReturnAccount", "unbilledGoodsAccount"
	};

	static final String[] a_NON_INVENTORY_ITEM_REQ = 
	{
		"barcode", "itemSku", "itemSkuName", "itemName", "description",
		"vendorItemCode", "vendorItemName", "unitId", "purchaseUnitId", 
		"taxId", "purchaseTaxId", "itemPrice", 
		"expenseAccount", "salesAccount", "salesReturnAccount", "itemDiscountAccount", 
		"purchaseReturnAccount", "unbilledGoodsAccount"
	};
	
	static final String[] a_SERVICE_ITEM_REQ = 
	{
		"barcode", "itemSku", "itemSkuName", "itemName", "description",
		"vendorItemCode", "vendorItemName", "unitId", "purchaseUnitId", 
		"taxId", "purchaseTaxId", "itemPrice", 
		"salesAccount", "salesReturnAccount", "itemDiscountAccount", "unbilledGoodsAccount"
	};

	public void validateItem()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Item Result : ", s1, l1);			
		
		try
		{				
			oConn = BaseTool.beginTrans();
			Statement oStmt = oConn.createStatement();
			
			ResultSet oRS  = oStmt.executeQuery("select * from item ");
			while (oRS.next())
			{
				String sItemID = oRS.getString("item_id");
				
				Item oItem = ItemTool.getItemByID(sItemID, oConn);
				String sItemCode = oItem.getItemCode(); 
				String sItemName = oItem.getItemName(); 
				int iType = oItem.getItemType();
				StringBuilder sDesc = new StringBuilder();							
				
				if (iType == AppAttributes.i_INVENTORY_PART)
				{
					validateField (oItem, a_INVENTORY_ITEM_REQ, sDesc);
				}			
				else if (iType == AppAttributes.i_NON_INVENTORY_PART)
				{
					validateField (oItem, a_NON_INVENTORY_ITEM_REQ, sDesc);
				}
				else if(iType == AppAttributes.i_SERVICE)
				{
					validateField (oItem, a_SERVICE_ITEM_REQ, sDesc);	
				}
				else
				{
					validateField (oItem, a_ITEM_REQ, sDesc);						
				}
				
				if (StringUtil.isNotEmpty(sDesc.toString()))
				{
					m_iInvalidResult ++;
					m_oResult.append("\n");
					
					append (null, l1[0]);
					append (sItemCode, l1[1]);
					append (StringUtil.cut(sItemName,36,".."), l1[2]);
					append (sDesc.toString(), l1[3]);
				}			
			}	
			BaseTool.commit(oConn);
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
}
