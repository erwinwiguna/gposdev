package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetailPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetailPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetailPeer;
import com.ssti.enterprise.pos.om.ItemTransferPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetailPeer;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetailPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.financial.AccountPayableTool;
import com.ssti.enterprise.pos.tools.financial.AccountReceivableTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.enterprise.pos.tools.journal.BaseJournalTool;
import com.ssti.enterprise.pos.tools.journal.InventoryJournalTool;
import com.ssti.enterprise.pos.tools.journal.PurchaseJournalTool;
import com.ssti.enterprise.pos.tools.journal.SalesJournalTool;
import com.ssti.enterprise.pos.tools.journal.SalesReturnJournalTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ManualTransCancel extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());

	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void cancelTransaction(String _sTrans, String _sTransID)
	{		
		if (StringUtil.isNotEmpty(_sTrans) && StringUtil.isNotEmpty(_sTransID))
		{
			Connection oConn = null;
			try
			{							
				if (StringUtil.isEqual(_sTrans, "pr"))
				{
					oConn = BaseTool.beginTrans();
					PurchaseReceipt oTR = PurchaseReceiptTool.getHeaderByID(_sTransID, oConn);
					List vTD = PurchaseReceiptTool.getDetailsByID(_sTransID, oConn);

					//cancel inventory
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_PURCHASE_RECEIPT, oConn);
					
					//cancel gl
					PurchaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_PURCHASE_RECEIPT, _sTransID, oConn);    
					
					if (oTR != null)
					{
						oTR.setRemark(oTR.getRemark() + "\n MANUAL CANCEL");
						oTR.setStatus(i_PR_CANCELLED);
						oTR.save(oConn);
					}
					BaseTool.commit(oConn);
					m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
				}				
				else if (StringUtil.isEqual(_sTrans, "pi"))
				{
					oConn = BaseTool.beginTrans();
					PurchaseInvoice oTR = PurchaseInvoiceTool.getHeaderByID(_sTransID, oConn);
					List vTD = PurchaseInvoiceTool.getDetailsByID(_sTransID);
					
					//cancel inventory
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_PURCHASE_INVOICE, oConn);
					
					//cancel gl
					PurchaseJournalTool.deletePurchaseJournal(GlAttributes.i_GL_TRANS_PURCHASE_INVOICE, _sTransID, oConn);     
					
					//cancel AP
					AccountPayableTool.rollbackAP(_sTransID, oConn);
					
					if (oTR != null)
					{
						if (vTD != null)
						{
							PurchaseReceiptTool.updatePRFromPI(oTR, vTD, true, oConn);
						}
						oTR.setRemark(oTR.getRemark() + "\n MANUAL CANCEL");
						oTR.setStatus(i_PI_CANCELLED);
						oTR.save(oConn);
					}
					BaseTool.commit(oConn);
					m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
				}
				else if (StringUtil.isEqual(_sTrans, "do"))
				{
					oConn = BaseTool.beginTrans();
					DeliveryOrder oTR = DeliveryOrderTool.getHeaderByID(_sTransID, oConn);

					//cancel inventory
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_DELIVERY_ORDER, oConn);
					
					//cancel gl
					SalesJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_DELIVERY_ORDER, _sTransID, oConn);    
					
					if (oTR != null)
					{
						oTR.setRemark(oTR.getRemark() + "\n MANUAL CANCEL");
						oTR.setStatus(i_DO_CANCELLED);
						oTR.save(oConn);
					}
					BaseTool.commit(oConn);
					m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
				}				
				else if (StringUtil.isEqual(_sTrans, "si"))
				{
					oConn = BaseTool.beginTrans();
					SalesTransaction oTR = TransactionTool.getHeaderByID(_sTransID, oConn);
					List vTD = TransactionTool.getDetailsByID(_sTransID, oConn);
					
					//cancel inventory
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_SALES_INVOICE, oConn);
					
					//cancel gl
					SalesJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_SALES_INVOICE, _sTransID, oConn);     
					
					//cancel AP
					AccountReceivableTool.rollbackAR(_sTransID, oConn);
					
					if (oTR != null)
					{
						if (vTD != null)
						{
							DeliveryOrderTool.updateDOFromInvoice(vTD, true, oConn);
						}
						oTR.setRemark(oTR.getRemark() + "\n MANUAL CANCEL");
						oTR.setStatus(i_CANCELLED);
						oTR.save(oConn);
					}			
					BaseTool.commit(oConn);
					m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
				}
                else if (StringUtil.isEqual(_sTrans, "sr"))
                {
                    oConn = BaseTool.beginTrans();
                    SalesReturn oTR = SalesReturnTool.getHeaderByID(_sTransID, oConn);
                    List vTD = SalesReturnTool.getDetailsByID(_sTransID, oConn);

                    //update inventory
                    InventoryTransactionTool.delete (oTR.getSalesReturnId(), i_INV_TRANS_SALES_RETURN, oConn);

                    //update GL journal
                    SalesReturnJournalTool.deleteJournal 
                        (GlAttributes.i_GL_TRANS_SALES_RETURN, _sTransID, oConn);                

                    if (oTR != null)
                    {
                        //cancel credit memo
                        CreditMemoTool.cancelFromSalesReturn (oTR, oTR.getCreateBy(), oConn);

                        if (vTD != null)
                        {
                            //update do
                            if(oTR.getTransactionType() == i_RET_FROM_PR_DO) 
                            {
                                DeliveryOrderTool.updateReturnQty(vTD, true, oConn);
                            }                        
                            //update si
                            if(oTR.getTransactionType() == i_RET_FROM_PI_SI)
                            {   
                                TransactionTool.updateReturnQty(vTD, true, oConn);
                            }
                        }

                        oTR.setRemark(oTR.getRemark() + "\n MANUAL CANCEL");
                        oTR.setStatus(i_CANCELLED);
                        oTR.save(oConn);
                    }           
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }                
				else if (StringUtil.isEqual(_sTrans, "ir"))
				{
					oConn = BaseTool.beginTrans();
					IssueReceipt oTR = IssueReceiptTool.getHeaderByID(_sTransID, oConn);
					//cancel inventory
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_ISSUE_UNPLANNED, oConn);
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_RECEIPT_UNPLANNED, oConn);
					
					//cancel gl
					InventoryJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_ISSUE_UNPLANNED, _sTransID, oConn);     
					InventoryJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED, _sTransID, oConn);   
					
					if (oTR != null)
					{
						oTR.setDescription(oTR.getDescription() + "\n MANUAL CANCEL");
						oTR.setStatus(i_CANCELLED);
						oTR.save(oConn);
					}
					BaseTool.commit(oConn);
					m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
				}			
				else if (StringUtil.isEqual(_sTrans, "it"))
				{
					oConn = BaseTool.beginTrans();
					ItemTransfer oTR = ItemTransferTool.getHeaderByID(_sTransID, oConn);
					//cancel inventory
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_TRANSFER_OUT, oConn);
					InventoryTransactionTool.delete (_sTransID, i_INV_TRANS_TRANSFER_IN, oConn);
					
					//cancel gl
					InventoryJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_ITEM_TRANSFER, _sTransID, oConn);    
					
					if (oTR != null)
					{
						oTR.setDescription(oTR.getDescription() + "\n MANUAL CANCEL");
						oTR.setStatus(i_CANCELLED);
						oTR.save(oConn);
					}
					BaseTool.commit(oConn);
					m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
				}			
			}
			catch (Exception _oEx)
			{
				handleError (oConn, _oEx);
			}
		}
	}
    
    public void cancelByNo(String _sTrans, String _sTransID, String _sTransNo)
    {       
    	System.out.println("Cancel By No:" + _sTrans + " ID: " + _sTransID + " NO:" + _sTransNo);
        if(StringUtil.isNotEmpty(_sTrans) && StringUtil.isNotEmpty(_sTransID))
        {
            Connection oConn = null;
            try
            {                           
                if (StringUtil.isEqual(_sTrans, "pr"))
                {
                    oConn = BaseTool.beginTrans();
//                    PurchaseReceipt oTR = PurchaseReceiptTool.getHeaderByID(_sTransID, oConn);
//                    List vTD = PurchaseReceiptTool.getDetailsByID(_sTransID, oConn);

                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_PURCHASE_RECEIPT, oConn);
                    
                    //cancel gl
                    BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_PURCHASE_RECEIPT, _sTransID, _sTransNo, oConn);    
                    
//                    if (oTR != null)
//                    {
//                        oTR.setRemark(oTR.getRemark() + "\n MANUAL CANCEL");
//                        oTR.setStatus(i_PR_CANCELLED);
//                        oTR.save(oConn);
//                    }
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }               
                else if (StringUtil.isEqual(_sTrans, "pi"))
                {
                    oConn = BaseTool.beginTrans();
                    
                    System.out.println("Cancel PI ID: " + _sTransID + " NO:" + _sTransNo);
                    
                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_PURCHASE_INVOICE, oConn);
                    
                    //cancel gl
                    BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_PURCHASE_INVOICE, _sTransID, _sTransNo, oConn);     
                    
                    //cancel AP
                    AccountPayableTool.rollbackAP(_sTransID, _sTransNo, oConn);
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }
                else if (StringUtil.isEqual(_sTrans, "do"))
                {
                    oConn = BaseTool.beginTrans();
//                    DeliveryOrder oTR = DeliveryOrderTool.getHeaderByID(_sTransID, oConn);

                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_DELIVERY_ORDER, oConn);
                    
                    //cancel gl
                    SalesJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_DELIVERY_ORDER, _sTransID, _sTransNo, oConn);    
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }               
                else if (StringUtil.isEqual(_sTrans, "si"))
                {
                    oConn = BaseTool.beginTrans();
//                    SalesTransaction oTR = TransactionTool.getHeaderByID(_sTransID, oConn);
//                    List vTD = TransactionTool.getDetailsByID(_sTransID, oConn);
                    
                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_SALES_INVOICE, oConn);
                    
                    //cancel gl
                    BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_SALES_INVOICE, _sTransID, _sTransNo, oConn);     
                    
                    //cancel AP
                    AccountReceivableTool.rollbackAR(_sTransID, _sTransNo, oConn);

                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }
                else if (StringUtil.isEqual(_sTrans, "sr"))
                {
                    oConn = BaseTool.beginTrans();
//                    SalesReturn oTR = SalesReturnTool.getHeaderByID(_sTransID, oConn);
//                    List vTD = SalesReturnTool.getDetailsByID(_sTransID, oConn);
                    
                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_SALES_RETURN, oConn);
                    
                    //cancel gl
                    BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_SALES_RETURN, _sTransID, _sTransNo, oConn);     
                    
                    //cancel AP
                    AccountReceivableTool.rollbackAR(_sTransID, _sTransNo, oConn);

                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }
                
                else if (StringUtil.isEqual(_sTrans, "ir"))
                {
                    oConn = BaseTool.beginTrans();
//                    IssueReceipt oTR = IssueReceiptTool.getHeaderByID(_sTransID, oConn);
                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_ISSUE_UNPLANNED, oConn);
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_RECEIPT_UNPLANNED, oConn);
                    
                    //cancel gl
                    BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_ISSUE_UNPLANNED, _sTransID, _sTransNo, oConn);     
                    BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED, _sTransID, _sTransNo, oConn);   
                    
//                    if (oTR != null)
//                    {
//                        oTR.setDescription(oTR.getDescription() + "\n MANUAL CANCEL");
//                        oTR.setStatus(i_CANCELLED);
//                        oTR.save(oConn);
//                    }
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }
                else if (StringUtil.isEqual(_sTrans, "it"))
                {
                    oConn = BaseTool.beginTrans();
//                    ItemTransfer oTR = ItemTransferTool.getHeaderByID(_sTransID, oConn);
                    //cancel inventory
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_TRANSFER_OUT, oConn);
                    InventoryTransactionTool.delete (_sTransID, _sTransNo, i_INV_TRANS_TRANSFER_IN, oConn);
                    
                    //cancel gl
                    InventoryJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_ITEM_TRANSFER, _sTransID, _sTransNo, oConn);    
                    
//                    if (oTR != null)
//                    {
//                        oTR.setDescription(oTR.getDescription() + "\n MANUAL CANCEL");
//                        oTR.setStatus(i_CANCELLED);
//                        oTR.save(oConn);
//                    }
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }   
                else if (StringUtil.isEqual(_sTrans, "rp"))
                {
                    oConn = BaseTool.beginTrans();
                    ArPayment oTR = ReceivablePaymentTool.getHeaderByID(_sTransID, oConn);
                    
                    if (oTR != null)
                    {
                        //rollback ar entry
                        AccountReceivableTool.rollbackAR(oTR.getArPaymentId(), oConn);
                        
                        //rollback credit memo
                        CreditMemoTool.rollbackCMFromARPayment(oTR, oConn);
                    
                        //cancel cash flow entry                        
                        CashFlowTool.cancelCashFlowByTransID (oTR.getArPaymentId(), "MANUAL CANCEL", oConn);
                        
                        //rollback any GL Transaction
                        BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_AR_PAYMENT, oTR.getArPaymentId(), oConn);
                    }
                    //process save ar payment Data
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + _sTransNo + " Cancelled Successfully ");
                }                   
                
            }
            catch (Exception _oEx)
            {
                handleError (oConn, _oEx);
            }
        }
    }    

    public void delGLTrans(int _iGLTrans, String _sTransID, String _sTransNo)
    {       
        if (StringUtil.isNotEmpty(_sTransID))
        {
            try
            {
//              int i_GL_TRANS_OPENING_BALANCE    = 1;
//              int i_GL_TRANS_RECEIPT_UNPLANNED  = 2;
//              int i_GL_TRANS_ISSUE_UNPLANNED    = 3;
//              int i_GL_TRANS_PURCHASE_RECEIPT   = 4;
//              int i_GL_TRANS_PURCHASE_INVOICE   = 5;
//              int i_GL_TRANS_PURCHASE_RETURN    = 6;
//              int i_GL_TRANS_ITEM_TRANSFER      = 7;
//              int i_GL_TRANS_DELIVERY_ORDER     = 8;
//              int i_GL_TRANS_SALES_INVOICE      = 9;
//              int i_GL_TRANS_SALES_RETURN       = 11;
//              int i_GL_TRANS_FIXED_ASSET        = 12;
//              int i_GL_TRANS_JOURNAL_VOUCHER    = 13;
//              int i_GL_TRANS_AR_PAYMENT         = 14;
//              int i_GL_TRANS_AP_PAYMENT         = 15;
//              int i_GL_TRANS_CREDIT_MEMO        = 16;
//              int i_GL_TRANS_DEBIT_MEMO         = 17;
//              int i_GL_TRANS_CASH_MANAGEMENT    = 18;
//              int i_GL_TRANS_JOB_COSTING        = 19;
//              int i_GL_TRANS_JC_ROLLOVER        = 20;

                oConn = BaseTool.beginTrans();
                BaseJournalTool.deleteJournal(_iGLTrans, _sTransID, _sTransNo, oConn);     
                m_oResult.append(" GL Transaction " + _sTransNo + " Deleted Successfully ");
                BaseTool.commit(oConn);
            }
            catch (Exception _oEx)
            {
                handleError(oConn, _oEx);
                // TODO: handle exception
            }
            
        }
    }
    
    
    public void delTransaction(String _sTrans, String _sTransID, boolean _bPendingOnly)
    {       
        if (StringUtil.isNotEmpty(_sTrans) && StringUtil.isNotEmpty(_sTransID))
        {
            Connection oConn = null;
            try
            {  
                if (StringUtil.isEqual(_sTrans, "po"))
                {
                    oConn = BaseTool.beginTrans();
                    PurchaseOrder oTR = PurchaseOrderTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, _sTransID);
                            PurchaseOrderDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_ID, _sTransID);
                            PurchaseOrderPeer.doDelete(oCrit, oConn);
                        }
                    }          
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }                  
                else if (StringUtil.isEqual(_sTrans, "pr"))
                {
                    oConn = BaseTool.beginTrans();
                    PurchaseReceipt oTR = PurchaseReceiptTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, _sTransID);
                            PurchaseReceiptDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID, _sTransID);
                            PurchaseReceiptPeer.doDelete(oCrit, oConn);
                        }
                    }          
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }               
                else if (StringUtil.isEqual(_sTrans, "pi"))
                {
                    oConn = BaseTool.beginTrans();
                    PurchaseInvoice oTR = PurchaseInvoiceTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, _sTransID);
                            PurchaseInvoiceDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(PurchaseInvoicePeer.PURCHASE_INVOICE_ID, _sTransID);
                            PurchaseInvoicePeer.doDelete(oCrit, oConn);
                        }
                    }
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }
                else if (StringUtil.isEqual(_sTrans, "so"))
                {
                    oConn = BaseTool.beginTrans();
                    SalesOrder oTR = SalesOrderTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(SalesOrderDetailPeer.SALES_ORDER_ID, _sTransID);
                            SalesOrderDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(SalesOrderPeer.SALES_ORDER_ID, _sTransID);
                            SalesOrderPeer.doDelete(oCrit, oConn);
                        }
                    }          
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }                  
                else if (StringUtil.isEqual(_sTrans, "do"))
                {
                    oConn = BaseTool.beginTrans();
                    DeliveryOrder oTR = DeliveryOrderTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, _sTransID);
                            DeliveryOrderDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_ID, _sTransID);
                            DeliveryOrderPeer.doDelete(oCrit, oConn);
                        }
                    }

                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }               
                else if (StringUtil.isEqual(_sTrans, "si"))
                {
                    oConn = BaseTool.beginTrans();
                    SalesTransaction oTR = TransactionTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, _sTransID);
                            SalesTransactionDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(SalesTransactionPeer.SALES_TRANSACTION_ID, _sTransID);
                            SalesTransactionPeer.doDelete(oCrit, oConn);
                        }
                    }
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }
                else if (StringUtil.isEqual(_sTrans, "ir"))
                {
                    oConn = BaseTool.beginTrans();
                    IssueReceipt oTR = IssueReceiptTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, _sTransID);
                            IssueReceiptDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(IssueReceiptPeer.ISSUE_RECEIPT_ID, _sTransID);
                            IssueReceiptPeer.doDelete(oCrit, oConn);
                        }
                    }
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }           
                else if (StringUtil.isEqual(_sTrans, "it"))
                {
                    oConn = BaseTool.beginTrans();
                    ItemTransfer oTR = ItemTransferTool.getHeaderByID(_sTransID, oConn);

                    if (oTR != null)
                    {
                        if (oTR.getStatus() == i_PENDING && _bPendingOnly || !_bPendingOnly)
                        {
                            Criteria oCrit = new Criteria();
                            oCrit.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, _sTransID);
                            ItemTransferDetailPeer.doDelete(oCrit, oConn);

                            oCrit = new Criteria();
                            oCrit.add(ItemTransferPeer.ITEM_TRANSFER_ID, _sTransID);
                            ItemTransferPeer.doDelete(oCrit, oConn);
                        }
                    }
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Cancelled Successfully ");
                }           
            }
            catch (Exception _oEx)
            {
                handleError (oConn, _oEx);
            }
        }
    }
        
    public void rejournal(String _sTrans, String _sTransNo)
    {       
        if (StringUtil.isNotEmpty(_sTrans) && StringUtil.isNotEmpty(_sTransNo))
        {
            Connection oConn = null;
            try
            {  
                if (StringUtil.isEqual(_sTrans, "ir"))
                {
                    oConn = BaseTool.beginTrans();
                    IssueReceipt oTR = (IssueReceipt)IssueReceiptTool.getByTransNo(
                    	IssueReceiptPeer.TRANSACTION_NO,_sTransNo,IssueReceiptPeer.class, oConn);
                    if (oTR != null && oTR.getStatus() == i_TRANS_PROCESSED)
                    {
                    	List vTD = IssueReceiptTool.getDetailsByID(oTR.getTransactionId(), oConn);
                        BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED, oTR.getIssueReceiptId(), _sTransNo, oConn);   
                        
                        InventoryJournalTool oJT = new InventoryJournalTool(oTR, oConn);
                        oJT.createIssueReceiptJournal(oTR, vTD);                        
                    }
                    
                    BaseTool.commit(oConn);
                    m_oResult.append(_sTrans + " Transaction " + oTR + " Rejournal Successfully ");
                }           
            }
            catch (Exception _oEx)
            {
                handleError (oConn, _oEx);
            }
        }
    }
    
    
    public void findDuplicate(String _sTrans, boolean _bFix)
    {       
        if (StringUtil.isNotEmpty(_sTrans))
        {
        	StringBuilder oSQL =  new StringBuilder();
        	oSQL.append("SELECT DISTINCT g.transaction_id AS trans_id, g.transaction_no AS trans_no, g.description AS desc, g.transaction_date AS dt ")
        		.append(" FROM gl_transaction g WHERE transaction_no NOT IN ");
            try
            {  
                if (StringUtil.isEqual(_sTrans, "pr"))
                {
                	oSQL.append(" (SELECT receipt_no FROM purchase_receipt) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_PURCHASE_RECEIPT);                	
                }              
                else if (StringUtil.isEqual(_sTrans, "pi"))
                {
                	oSQL.append(" (SELECT purchase_invoice_no FROM purchase_invoice) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_PURCHASE_INVOICE);                	                    
                }
                else if (StringUtil.isEqual(_sTrans, "do"))
                {
                	oSQL.append(" (SELECT delivery_order_no FROM delivery_order) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_DELIVERY_ORDER);
                }               
                else if (StringUtil.isEqual(_sTrans, "si"))
                {
                	oSQL.append(" (SELECT invoice_no FROM sales_transaction) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_SALES_INVOICE);
                }
                else if (StringUtil.isEqual(_sTrans, "sr"))
                {
                	oSQL.append(" (SELECT return_no FROM sales_return) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_SALES_RETURN);
                }                
                else if (StringUtil.isEqual(_sTrans, "ir"))
                {
                	oSQL.append(" (SELECT transaction_no FROM issue_receipt) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED);
                }           
                else if (StringUtil.isEqual(_sTrans, "it"))
                {
                	oSQL.append(" (SELECT transaction_no FROM item_transfer) AND g.transaction_type = " + GlAttributes.i_GL_TRANS_ITEM_TRANSFER);

                }           
                oSQL.append(" ORDER BY dt DESC, trans_no DESC ");
                System.out.println(oSQL);
            	List v = SqlUtil.executeQuery(oSQL.toString());
            	for (int i = 0; i < v.size(); i++)
            	{
            		Record oRec = (Record)v.get(i);
            		String sID = oRec.getValue("trans_id").asString();
            		String sNo = oRec.getValue("trans_no").asString();
            		String sDT = oRec.getValue("dt").toString();
            		m_oResult.append(" Duplicate TransNo: ")
            				.append(sNo).append(" ID: ")
            				.append(sID).append(" DATE: ")
            				.append(sDT).append(" DESC: ")            				
            				.append(oRec.getValue("desc").asString()).append(s_LINE_SEPARATOR);
            		if(_bFix)
            		{
            			cancelByNo(_sTrans, sID, sNo);
                		m_oResult.append(" Fixed TransNo: ")
        				.append(sNo).append(" ID: ")
        				.append(sID).append(" DESC: ")
        				.append(oRec.getValue("desc").asString()).append(s_LINE_SEPARATOR);            			
            		}            	
            	}
            }
            catch (Exception _oEx)
            {
                handleError (oConn, _oEx);
            }
        }
    }
}
