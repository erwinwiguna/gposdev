package com.ssti.enterprise.pos.tools.data;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.GlConfig;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.Preference;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.AccountTypeTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate master data 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MasterValidator.java,v 1.5 2008/03/03 02:23:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: MasterValidator.java,v $
 * Revision 1.5  2008/03/03 02:23:17  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 02:04:56  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class MasterValidator extends BaseValidator
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Setup Part", "Status"};
	static final int[] l1 = {5, 50, 60};
	
	static final String[] a_LOCATION_REQ = 
	{
		"locationCode", "locationName", "siteId", "inventoryType", "locationType", 
		"hoStoreLeadtime", "locationAddress", "description"
	};
	
	static final String[] a_VENDOR_REQ = 
	{
		"vendorCode", "vendorName", "defaultTaxId", 
		"defaultTypeId", "defaultTermId", "defaultCurrencyId"
	};	
	
	static final String[] a_CUSTOMER_REQ = 
	{
		"customerCode", "customerName", "customerTypeId", "defaultTaxId", 
		"defaultTypeId", "defaultTermId", "defaultCurrencyId"
	};		
	
	static final String[] a_CATEGORY_REQ = 
	{
		"kategoriCode", "description"
	};		
	
	public void validateMaster()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate Master Result : ", s1, l1);			
		
		try
		{	
			//preference
			m_oResult.append ("\n");
			append ("1. ", l1[0]);
			append ("Preference", l1[1]);
			
			Preference oPref = PreferenceTool.getPreference();
			if (oPref != null)
			{
				append ("OK", l1[2]);
			}
			else
			{
				append ("Preference not setup yet !", l1[2]);
			}
			
			//preference current location
			m_oResult.append ("\n");
			append ("2. a", l1[0]);
			append ("Preference (Current Location)", l1[1]);
			if (oPref != null && StringUtil.isNotEmpty(oPref.getLocationId()))
			{
				append ("OK", l1[2]);
			}
			else
			{
				append ("Current Location in Preference not setup yet !", l1[2]);
			}

			//preference start Date
			m_oResult.append ("\n");
			append ("   b", l1[0]);
			append ("Preference (Start Date)", l1[1]);
			if (oPref != null && oPref.getStartDate() != null)
			{
				append ("OK", l1[2]);
			}
			else
			{
				append ("Start Date in Preference not setup yet !", l1[2]);
			}			
			
			//site
			m_oResult.append ("\n");
			append ("3. ", l1[0]);
			append ("Site Data ", l1[1]);
			List vData = SiteTool.getAllSite();
			if (vData.size() > 0)
			{				
				append (vData.size() + " Site, OK", l1[2]);
			}
			else
			{
				append ("Site Data not setup yet !", l1[2]);
			}
			
			//location
			m_oResult.append ("\n");
			append ("4. ", l1[0]);
			append ("Location Data ", l1[1]);			
			validateLocation();
			
			//employee
			m_oResult.append ("\n");
			append ("5. ", l1[0]);
			append ("Employee Data ", l1[1]);
			vData = EmployeeTool.getAllEmployee();
			if (vData.size() > 0)
			{				
				append (vData.size() + " Employee, OK", l1[2]);
			}
			else
			{
				append ("Employee Data not setup yet !", l1[2]);
			}
			
			//period
			String sYear = CustomFormatter.formatCustomDate(new Date(),"yyyy");
			m_oResult.append ("\n");
			append ("6. ", l1[0]);
			append ("Period Data, Year " + sYear, l1[1]);
			vData = PeriodTool.getPeriodByYear(sYear);
			if (vData.size() > 0)
			{				
				append (vData.size() + " Period, OK", l1[2]);
			}
			else
			{
				append ("Current year period not setup yet !", l1[2]);
			}
			
			//currency
			m_oResult.append ("\n");
			append ("7. ", l1[0]);
			append ("Currency ", l1[1]);
			validateCurrency();

			//account type
			m_oResult.append ("\n");
			append ("8. ", l1[0]);
			append ("Account Type", l1[1]);

			vData = AccountTypeTool.getAllAccountType();
			if (vData.size() == 15)
			{				
				append (vData.size() + " Account Type, OK", l1[2]);
			}
			else
			{
				append ("Invalid Number of Account Type (" + vData.size() + "), should be 15 !", l1[2]);
			}

			//account 
			m_oResult.append ("\n");
			append ("9. ", l1[0]);
			append ("Chart of Account ", l1[1]);
			validateAccount();

			//GL config
			m_oResult.append ("\n");
			append ("10. ", l1[0]);
			append ("GL Configuration ", l1[1]);
			validateGLConfig();

			//Unit
			m_oResult.append ("\n");
			append ("11. ", l1[0]);
			append ("Unit ", l1[1]);
			validateUnit();			
			
			//Tax
			m_oResult.append ("\n");
			append ("12. ", l1[0]);
			append ("Tax ", l1[1]);
			validateTax();					
			
			//Cash/Bank
			m_oResult.append ("\n");
			append ("13. ", l1[0]);
			append ("Cash/Bank ", l1[1]);
			validateCashBank();					
			
			//Adjustment Type
			m_oResult.append ("\n");
			append ("13. ", l1[0]);
			append ("Adjustment Type ", l1[1]);
			validateAdjType();				
			
			//Payment Type
			m_oResult.append ("\n");
			append ("14. ", l1[0]);
			append ("Payment Type ", l1[1]);
			validatePaymentType();					
			
			//Payment Term
			m_oResult.append ("\n");
			append ("15. ", l1[0]);
			append ("Payment Term ", l1[1]);
			validatePaymentTerm();	
			
			//Vendor
			m_oResult.append ("\n");
			append ("16. ", l1[0]);
			append ("Vendor ", l1[1]);
			validateVendor();	
			
			//account type
			m_oResult.append ("\n");
			append ("17. ", l1[0]);
			append ("Customer Type", l1[1]);

			vData = CustomerTypeTool.getAllCustomerType();
			if (vData.size() > 0)
			{				
				append (vData.size() + " Customer Type, OK", l1[2]);
			}
			else
			{
				append ("Customer Type not setup yet !", l1[2]);
			}
			
			//Customer
			m_oResult.append ("\n");
			append ("18. ", l1[0]);
			append ("Customer ", l1[1]);
			validateCustomer();	
			
			//Category
			m_oResult.append ("\n");
			append ("19. ", l1[0]);
			append ("Category ", l1[1]);
			validateCategory();				
			
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateLocation ()
		throws Exception
	{
		List vData = LocationTool.getAllLocation();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Location, OK", l1[2]);
			
			boolean bHOExist = false;
			for (int i = 0; i < vData.size(); i++)
			{
				Location oLoc = (Location) vData.get(i);
				
				if (oLoc.getLocationType() == AppAttributes.i_HEAD_OFFICE)
				{
					bHOExist = true;
				}
				
				StringBuilder sDesc = new StringBuilder();				
				validateField (oLoc, a_LOCATION_REQ, sDesc);
				
				if (StringUtil.isNotEmpty(sDesc.toString()))
				{
					m_iInvalidResult ++;
					m_oResult.append("\n");
					
					append ("", l1[0]);
					append (StringUtil.cut(oLoc.getLocationName(),36,".."), l1[1]);
					append (sDesc.toString(), l1[2]);
				}			
			}
			m_iInvalidResult = 0;
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("HO Location Exist ", l1[1]);

			if (bHOExist)
			{
				append ("OK ", l1[2]);
			}
			else
			{
				append ("HO Location Not Exist, At Least 1 Location should be as HO ", l1[2]);				
			}	

		}
		else
		{
			append ("Location Data not setup yet !", l1[2]);
		}	
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateCurrency()
		throws Exception
	{
		List vData = CurrencyTool.getAllCurrency();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Currency, OK", l1[2]);
		}
		else
		{
			append ("Currency not setup yet !", l1[2]);
			return;
		}	
		
		m_oResult.append("\n");
		append ("", l1[0]);
		append ("Default Currency ", l1[1]);
		Currency oDefault = CurrencyTool.getDefaultCurrency(null);
		if (oDefault != null)
		{
			double dRate = CurrencyTool.getCurrentRateByID(oDefault.getCurrencyId(),null);
			if (dRate != 1)
			{
				append ("Exist but invalid rate (" + dRate + "), should be 1 for default currency !", l1[2]);
			}
			else
			{
				if (oDefault.getIsActive())
				{
					append ("OK", l1[2]);			
				}
				else
				{
					append ("Default Currency is not Active, default must always be active", l1[2]);							
				}
			}
		}	
		
		List vDefault = CurrencyPeer.doSelect(new Criteria().add(CurrencyPeer.IS_DEFAULT, true));
		if (vDefault.size() > 1)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Currency " , l1[1]);
			append ("Default Currency (" + vDefault.size() + "), should be only 1 !", l1[2]);
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			Currency oCurr = (Currency) vData.get(i);
			if (oCurr != null && !oCurr.getIsDefault())
			{
				double dRate = CurrencyTool.getCurrentRateByID(oCurr.getCurrencyId(),null);
				if (dRate <= 0)
				{
					m_oResult.append("\n");
					append ("", l1[0]);
					append ("Currency " + oCurr.getCurrencyCode(), l1[1]);
					append ("Current Rate (" + dRate + ") is not setup !", l1[2]);
				}
				
				validateCurrencyAccount("AP", oCurr.getApAccount(), oCurr);
				validateCurrencyAccount("AR", oCurr.getArAccount(), oCurr);			
				validateCurrencyAccount("REALIZE GAIN", oCurr.getRgAccount(), oCurr);			
				validateCurrencyAccount("UNREALIZE GAIN", oCurr.getUgAccount(), oCurr);			
				validateCurrencyAccount("INV DISC", oCurr.getSdAccount(), oCurr);			
			}	
		}
	}
	
	public void validateCurrencyAccount(String _sType, String _sAccID, Currency _oCurr)
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sAccID))
		{
			Account oACC = AccountTool.getAccountByID(_sAccID);
			if (!_oCurr.getCurrencyId().equals(oACC.getCurrencyId()) && oACC != null &&
					(_sType.equals("AR") || _sType.equals("AP")))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Currency " + _oCurr.getCurrencyCode(), l1[1]);
				append (_sType + " Account is not setup properly (NOT MATCH) !", l1[2]);
			}
		}
		else
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Currency " + _oCurr.getCurrencyCode(), l1[1]);
			append (_sType + " Account is not setup yet (EMPTY) !", l1[2]);
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateAccount()
		throws Exception
	{
		List vData = AccountTool.getAllAccount();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Account, OK", l1[2]);
		}
		else
		{
			append ("Chart of Account is not setup yet !", l1[2]);
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void validateGLConfig()
		throws Exception
	{
		GlConfig oData = GLConfigTool.getGLConfig(null);
		if (oData != null)
		{				
			append ("GL Configuration, OK", l1[2]);
		}
		else
		{
			append ("GL Configuration is not setup yet !", l1[2]);
		}
		
		if (oData != null)
		{
			if (StringUtil.isEmpty(oData.getOpeningBalance()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Opening Balance ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}

			if (StringUtil.isEmpty(oData.getRetainedEarning()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Retained Earning ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}

			if (StringUtil.isEmpty(oData.getDebitMemoAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Debit Memo ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}

			if (StringUtil.isEmpty(oData.getCreditMemoAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Credit Memo ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}			
			
			if (StringUtil.isEmpty(oData.getDirectSalesAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Direct Sales ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}		
			
			if (StringUtil.isEmpty(oData.getInventoryAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Inventory Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}			
			
			if (StringUtil.isEmpty(oData.getSalesAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Sales Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}						

			if (StringUtil.isEmpty(oData.getSalesReturnAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Sales Return Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}			

			if (StringUtil.isEmpty(oData.getItemDiscountAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Item Discount Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}			
			
			if (StringUtil.isEmpty(oData.getCogsAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default COGS Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}						

			if (StringUtil.isEmpty(oData.getPurchaseReturnAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Purchase Return Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}						

			if (StringUtil.isEmpty(oData.getExpenseAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Expense Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}			
			
			if (StringUtil.isEmpty(oData.getUnbilledGoodsAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Default Unbilled Goods Account ", l1[1]);
				append ("Not setup yet !", l1[2]);
			}						
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateUnit()
		throws Exception
	{
		List vData = UnitTool.getAllUnit();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Unit, OK", l1[2]);
		}
		else
		{
			append ("Unit is not setup yet !", l1[2]);
		}
		
		boolean bBase = false;
		for (int i = 0; i < vData.size(); i++)
		{
			Unit oUnit = (Unit) vData.get(i);
			if (oUnit.getBaseUnit())
			{
				bBase = true;
			}
			if (!bBase)
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Base Unit ", l1[1]);
				append ("No Base Unit found !", l1[2]);			
			}
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			Unit oUnit = (Unit) vData.get(i);
			if (oUnit.getBaseUnit())
			{
				if (oUnit.getValueToBase().doubleValue() != 1)
				{
					m_oResult.append("\n");
					append ("", l1[0]);
					append ("Base Unit " + oUnit.getUnitCode(), l1[1]);
					append ("Value to base is invalid (" + oUnit.getValueToBase() + "), should be 1 !", l1[2]);		
				}
			}
			else
			{
				if (oUnit.getValueToBase().doubleValue() == 1)
				{
					m_oResult.append("\n");
					append ("", l1[0]);
					append ("Unit " + oUnit.getUnitCode(), l1[1]);
					append ("Value to base is 1, should be set as base unit ", l1[2]);		
				}
				else
				{
					if (StringUtil.isEmpty(oUnit.getAlternateBase()))
					{
						m_oResult.append("\n");
						append ("", l1[0]);
						append ("Unit " + oUnit.getUnitCode(), l1[1]);
						append ("Alternate base unit is not set ", l1[2]);							
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateTax()
		throws Exception
	{
		List vData = TaxTool.getAllTax();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Tax, OK", l1[2]);
		}
		else
		{
			append ("Tax is not setup yet !", l1[2]);
		}
		
		boolean bPurchase = false;
		boolean bSales = false;

		for (int i = 0; i < vData.size(); i++)
		{
			Tax oTax = (Tax) vData.get(i);
			if (oTax.getDefaultPurchaseTax())
			{
				bPurchase = true;
			}
			if (oTax.getDefaultSalesTax())
			{
				bSales = true;			
			}
				
			if (StringUtil.isEmpty(oTax.getPurchaseTaxAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Tax " + oTax.getTaxCode(), l1[1]);
				append ("Purchase Tax Account is not set ", l1[2]);							
			}
			
			if (StringUtil.isEmpty(oTax.getSalesTaxAccount()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Tax " + oTax.getTaxCode(), l1[1]);
				append ("Sales Tax Account is not set ", l1[2]);							
			}
		}
		if (!bPurchase)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Purchase Tax ", l1[1]);
			append ("Default Purchase Tax not setup !", l1[2]);			
		}
		if (!bSales)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Sales Tax ", l1[1]);
			append ("Default Sales Tax not setup !", l1[2]);			
		}				
	}	
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateCashBank()
		throws Exception
	{
		List vData = BankTool.getAllBank();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Cash/Bank, OK", l1[2]);
		}
		else
		{
			append ("Cash/Bank is not setup yet !", l1[2]);
		}

		for (int i = 0; i < vData.size(); i++)
		{
			Bank oBank = (Bank) vData.get(i);
				
			if (StringUtil.isEmpty(oBank.getAccountId()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Cash/Bank " + oBank.getBankCode(), l1[1]);
				append ("GL Account is not set ", l1[2]);							
			}
			
			if (oBank.getOpeningBalance() != null && oBank.getOpeningBalance().doubleValue() == 0)
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Cash/Bank " + oBank.getBankCode(), l1[1]);
				append ("Opening Balance is not set ", l1[2]);							
			}
		}
	}		
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateAdjType()
		throws Exception
	{
		List vData = AdjustmentTypeTool.getAllAdjustmentType();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Adjustment Type, OK", l1[2]);
		}
		else
		{
			append ("Adjustment Type is not setup yet !", l1[2]);
		}

		for (int i = 0; i < vData.size(); i++)
		{
			AdjustmentType oAdj = (AdjustmentType) vData.get(i);
				
			if (StringUtil.isEmpty(oAdj.getDefaultAccountId()))
			{
				m_oResult.append("\n");
				append ("", l1[0]);
				append ("Adjustment Type " + oAdj.getAdjustmentTypeCode(), l1[1]);
				append ("Default Account is not set ", l1[2]);							
			}
		}
	}	
	
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validatePaymentType()
		throws Exception
	{
		List vData = PaymentTypeTool.getAllPaymentType();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Payment Type, OK", l1[2]);
		}
		else
		{
			append ("Payment Type is not setup yet !", l1[2]);
		}

		int iDefault = 0;
		int iMulti = 0;
		int iCredit = 0;
		
		
		for (int i = 0; i < vData.size(); i++)
		{
			PaymentType oData = (PaymentType) vData.get(i);
			
			if (oData.getIsDefault()) iDefault++;		
			if (oData.getIsCredit()) iCredit++;		
			if (oData.getIsMultiplePayment()) iMulti++;
		}
		
		if (iDefault == 0)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Payment Type", l1[1]);
			append ("Default Not setup !", l1[2]);			
		}	
		
		if (iDefault > 1)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Payment Type", l1[1]);
			append ("Default Type (" + iDefault + "), should be only 1 ", l1[2]);			
		}	
		
		if (iMulti == 0)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Multi Payment Type", l1[1]);
			append ("Multiple Type Not setup, multiple payment may not be used ", l1[2]);			
		}	
		
		if (iMulti > 1)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Multi Payment Type", l1[1]);
			append ("Multi Type (" + iDefault + "), should be only 1 ", l1[2]);			
		}		
		
		
		if (iCredit == 0)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Credit Payment Type", l1[1]);
			append ("Credit Type Not setup, credit payment can't be used ", l1[2]);			
		}	
	}		
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validatePaymentTerm()
		throws Exception
	{
		List vData = PaymentTermTool.getAllPaymentTerm();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Payment Term, OK", l1[2]);
		}
		else
		{
			append ("Payment Term is not setup yet !", l1[2]);
		}
	
		int iDefault = 0;
		int iMulti = 0;
		int iCash = 0;
		
		
		for (int i = 0; i < vData.size(); i++)
		{
			PaymentTerm oData = (PaymentTerm) vData.get(i);
			
			if (oData.getIsDefault()) iDefault++;		
			if (oData.getCashPayment()) iCash++;		
			if (oData.getIsMultiplePayment()) iMulti++;
		}
		
		if (iDefault == 0)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Payment Term", l1[1]);
			append ("Default Not setup !", l1[2]);			
		}	
		
		if (iDefault > 1)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Default Payment Term", l1[1]);
			append ("Default Term (" + iDefault + "), should be only 1 ", l1[2]);			
		}	
		
		if (iMulti == 0)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Multi Payment Term", l1[1]);
			append ("Multiple Term Not setup, multiple payment may not be used ", l1[2]);			
		}	
		
		if (iMulti > 1)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Multi Payment Term", l1[1]);
			append ("Multi Term (" + iDefault + "), should be only 1 ", l1[2]);			
		}		
		
		if (iCash == 0)
		{
			m_oResult.append("\n");
			append ("", l1[0]);
			append ("Cash Payment Term", l1[1]);
			append ("Cash Term Not setup, cash payment type may not be used ", l1[2]);			
		}	
	}		
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateVendor()
		throws Exception
	{
		List vData = VendorTool.getAllVendor();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Vendor, OK", l1[2]);
		}
		else
		{
			append ("Vendor is not setup yet !", l1[2]);
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			Vendor oData = (Vendor) vData.get(i);
			StringBuilder sDesc = new StringBuilder();				
			validateField (oData, a_VENDOR_REQ, sDesc);
			
			if (StringUtil.isNotEmpty(sDesc.toString()))
			{
				m_iInvalidResult ++;
				m_oResult.append("\n");
				
				append ("", l1[0]);
				append (StringUtil.cut(oData.getVendorName(),36,".."), l1[1]);
				append (sDesc.toString(), l1[2]);
			}	
		}
	}	
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateCustomer()
		throws Exception
	{
		List vData = CustomerTool.getAllCustomer();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Customer, OK", l1[2]);
		}
		else
		{
			append ("Customer is not setup yet !", l1[2]);
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			Customer oData = (Customer) vData.get(i);
			StringBuilder sDesc = new StringBuilder();				
			validateField (oData, a_CUSTOMER_REQ, sDesc);
			
			if (StringUtil.isNotEmpty(sDesc.toString()))
			{
				m_iInvalidResult ++;
				m_oResult.append("\n");
				
				append ("", l1[0]);
				append (StringUtil.cut(oData.getCustomerName(),36,".."), l1[1]);
				append (sDesc.toString(), l1[2]);
			}	
			if (CustomerTypeTool.getCustomerTypeByID(oData.getCustomerTypeId()) == null)
			{
				m_oResult.append("\n");
				
				append ("", l1[0]);
				append (StringUtil.cut(oData.getCustomerName(),36,".."), l1[1]);
				append ("INVALID CUSTOMER TYPE ID !", l1[2]);
			}
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void validateCategory()
		throws Exception
	{
		List vData = KategoriTool.getAllKategori();
		if (vData.size() > 0)
		{				
			append (vData.size() + " Category, OK", l1[2]);
		}
		else
		{
			append ("Category is not setup yet !", l1[2]);
		}
		
		for (int i = 0; i < vData.size(); i++)
		{
			Kategori oData = (Kategori) vData.get(i);
			StringBuilder sDesc = new StringBuilder();				
			validateField (oData, a_CATEGORY_REQ, sDesc);
			
			if (StringUtil.isNotEmpty(sDesc.toString()))
			{
				m_iInvalidResult ++;
				m_oResult.append("\n");
				
				append ("", l1[0]);
				append (StringUtil.cut(oData.getDescription(),36,".."), l1[1]);
				append (sDesc.toString(), l1[2]);
			}	
			if (StringUtil.isNotEmpty(oData.getParentId()) &&
					KategoriTool.getKategoriByID(oData.getParentId()) == null)
			{
				m_oResult.append("\n");
				
				append ("", l1[0]);
				append (StringUtil.cut(oData.getDescription(),36,".."), l1[1]);
				append ("INVALID PARENT ID !", l1[2]);
			}
		}
	}

}
