package com.ssti.enterprise.pos.tools.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.json.JSONArray;
import org.json.JSONObject;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.OfflineTrans;
import com.ssti.enterprise.pos.om.OfflineTransPeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.IDGenerator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Offline Trans Processing
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: OfflineTransTool.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: OfflineTransTool.java,v $

 * </pre><br>
 */
public class OfflineTransTool extends BaseTool implements AppAttributes, TransactionAttributes
{
	static Log log = LogFactory.getLog(OfflineTransTool.class);

    static OfflineTransTool instance = null;    
    public static synchronized OfflineTransTool getInstance() 
    {
        if (instance == null) instance = new OfflineTransTool();
        return instance;
    }
    
    public static List getByStatus(int _iStatus)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(OfflineTransPeer.STATUS, _iStatus);
    	return OfflineTransPeer.doSelect(oCrit);
    }
    
    public static List getByTypeAndStatus(int _iType, int _iStatus)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	//oCrit.add(OfflineTransPeer.DOC_TYPE, _iType);
    	oCrit.add(OfflineTransPeer.STATUS, _iStatus);
    	return OfflineTransPeer.doSelect(oCrit);
    }
    
    public static List processInvoices()
    	throws Exception
    {
    	List vOF = getByTypeAndStatus(1, 1);
    	StringBuilder res = new StringBuilder();
    	for (Iterator iterator = vOF.iterator(); iterator.hasNext();) 
    	{
			OfflineTrans oOF = (OfflineTrans) iterator.next();				
			try 
			{
				JSONObject oJSON = new JSONObject(oOF.getJsonDoc());
				SalesTransaction oTR = processInvJSON(oJSON);
				
				oOF.setStatus(2);
				oOF.setProcessDate(new Date());
				oOF.setProcessLog("Successful");
				oOF.setTransId(oTR.getSalesTransactionId());
				oOF.save();				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
				
				oOF.setStatus(1);
				oOF.setProcessDate(new Date());
				oOF.setProcessLog("ERROR:" + e.getMessage());
				oOF.setTransId("");
				oOF.save();
			}
		}
    	return vOF;
    }

    /**
     * 
     * @param _oOF
     */
	private static SalesTransaction processInvJSON(JSONObject oJSON) 
		throws Exception
	{			
		String sTransNo = oJSON.getString("no");
		String sLocID = oJSON.optString("locId");  
		String sCustID = oJSON.optString("custId");					
		
		if(StringUtil.empty(sLocID)  || LocationTool.getLocationByID(sLocID) == null) throw new Exception("Invalid Location Data:" + sLocID);
		if(StringUtil.empty(sCustID) || CustomerTool.getCustomerByID(sCustID) == null) throw new Exception("Invalid Customer Data:" + sCustID);
		if(StringUtil.empty(sTransNo)) throw new Exception("Invalid/Empty Trans No: " + sTransNo);
		
		SalesTransaction oTR = new SalesTransaction();
		oTR.setSalesTransactionId(IDGenerator.generateSysID());
		oTR.setInvoiceNo(sTransNo);
		oTR.setLocationId(sLocID);
		oTR.setCustomerId(sCustID);
		oTR.setCustomerName(CustomerTool.getCustomerNameByID(sCustID));
		oTR.setTransactionDate(CustomParser.parseDateTime(oJSON.optString("dt")));
		oTR.setCurrencyId(CurrencyTool.getDefaultCurrency().getId());
		oTR.setCurrencyRate(bd_ONE);
		oTR.setFiscalRate(bd_ONE);
		oTR.setCashierName(oJSON.optString("cashier",""));
		oTR.setStatus(i_PROCESSED);

		String sTypeID = oJSON.optString("ptypeId",PaymentTypeTool.getDefaultPaymentType().getId());
		String sTermID = oJSON.optString("ptermId",PaymentTermTool.getDefaultPaymentTerm().getId());
		Date dDate = CustomParser.parseDateTime(oJSON.optString("dt"));
		if (dDate == null) dDate = new Date();
		oTR.setPaymentTypeId(sTypeID);
		oTR.setPaymentTermId(sTermID);
		oTR.setCashierName(oJSON.optString("cashier",""));
		oTR.setSalesId(EmployeeTool.getIDByCode(oJSON.optString("slsCode","")));
		oTR.setTransactionDate(dDate);
		oTR.setSourceTransId("");
		oTR.setRemark(oJSON.optString("remark","Offline " + oTR.getInvoiceNo()));
		oTR.setTotalQty(bd_ZERO);
		
		JSONArray aDet = oJSON.optJSONArray("items");
		List vTD = new ArrayList(aDet.length());
		for(int i = 0; i < aDet.length(); i++)
		{			
			JSONObject oJSDET = aDet.getJSONObject(i);
			String sItemID = oJSDET.optString("itemId");
			Item oItem = ItemTool.getItemByID(sItemID);
			if(oItem == null) throw new Exception ("Invalid Item ID: " + sItemID);
			
			SalesTransactionDetail oTD = new SalesTransactionDetail();
			oTD.setSalesTransactionDetailId(IDGenerator.generateSysID());
			oTD.setSalesTransactionId(IDGenerator.generateSysID());
			oTD.setDeliveryOrderDetailId("");
			oTD.setDeliveryOrderId("");
			oTD.setItemId(sItemID);
			oTD.setItemCode(oItem.getItemCode());
			oTD.setItemName(oItem.getItemName());
			oTD.setDescription(oJSDET.optString("description",oItem.getDescription()));
			oTD.setQty(oJSDET.optBigDecimal("qty", bd_ONE));
			oTD.setQtyBase(oTD.getQty());
			
			String sUnitID = UnitTool.getIDByCode(oJSDET.optString("unitCode"));
			if(StringUtil.empty(sUnitID)) sUnitID = oItem.getUnit().getId();
			oTD.setUnitId(sUnitID);
			oTD.setUnitCode(UnitTool.getCodeByID(oTD.getUnitId()));	
			oTD.setItemPrice(oJSDET.optBigDecimal("itemPrice", bd_ZERO));
			oTD.setItemCost(bd_ZERO);
			
			String sTaxID = TaxTool.getIDByCode(oJSDET.optString("taxCode"));
			if(StringUtil.empty(sTaxID)) sTaxID = oItem.getTax().getId();
			oTD.setTaxId(sTaxID);
			oTD.setTaxAmount(TaxTool.getTaxByID(oTD.getTaxId()).getAmount());
			oTD.setSubTotal(bd_ZERO);
			oTD.setReturnedQty(bd_ZERO);
			vTD.add(oTD);
		}						
		List vPMT = new ArrayList(1);
		TransactionTool.setHeaderProperties(oTR, vTD, null);
		TransactionTool.saveData(oTR, vTD, vPMT);	
		return oTR;
	}    
}
