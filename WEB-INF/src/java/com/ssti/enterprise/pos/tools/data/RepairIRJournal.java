package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetailPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.InventoryJournalTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class RepairIRJournal extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());

    public static IssueReceipt getHeaderByNo(String _sNO, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(IssueReceiptPeer.TRANSACTION_NO, _sNO);
        List vData = IssueReceiptPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
            return (IssueReceipt) vData.get(0);
        }
        return null;    
    }
    
    public static List getDetailsByID(String _sID, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, _sID);
        oCrit.addAscendingOrderByColumn(IssueReceiptDetailPeer.INDEX_NO);
        return IssueReceiptDetailPeer.doSelect(oCrit, _oConn);
    }
    
	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void repairJournal(String _sTransNo)
	{		
		if (StringUtil.isNotEmpty(_sTransNo))
		{
			Connection oConn = null;
			try
			{							
				oConn = BaseTool.beginTrans();
				IssueReceipt oIR = getHeaderByNo(_sTransNo, oConn);
				
                if (oIR != null && oIR.getStatus() == i_PROCESSED)
                {
                    String sID = oIR.getIssueReceiptId();
                    List vTD = getDetailsByID(sID,oConn);
                    InventoryJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED, sID, oConn);
                    
                    InventoryJournalTool oJournal = new InventoryJournalTool(oIR, oConn);
                    oJournal.createIssueReceiptJournal(oIR, vTD);
                }
                
				BaseTool.commit(oConn);
				m_oResult.append(" Issue Receipt Journal ").append(_sTransNo).append(" UPDATED ");				
			}
			catch (Exception _oEx)
			{
				try 
				{
					BaseTool.rollback(oConn);
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				handleError (oConn, _oEx);
			}
		}
	}
}
