package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.CreditMemoPeer;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.SalesReturnJournalTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SalesReturnDataUtil extends BaseValidator implements AppAttributes
{
	static Log log = LogFactory.getLog(SalesReturnDataUtil.class);

	StringBuilder oSB = new StringBuilder();

	/**
	 * cancel sales return
	 * 
	 * @param _oSR
	 * @param _vTD
	 * @throws Exception
	 */
	public void updateTax(String _sReturnNo) 
		throws Exception 
	{	
		Connection oConn = BaseTool.beginTrans ();
		try 
		{	
			Criteria oCrit = new Criteria();
			oCrit.add(SalesReturnPeer.RETURN_NO,_sReturnNo);
			oCrit.add(SalesReturnPeer.TRANSACTION_TYPE,SalesReturnTool.i_RET_FROM_NONE);
			oCrit.add(SalesReturnPeer.IS_INCLUSIVE_TAX,false);
			
			List vSR = SalesReturnPeer.doSelect(oCrit);
			if (vSR.size() > 0)
			{
				SalesReturn oSR = (SalesReturn) vSR.get(0);
				List vTD = SalesReturnTool.getDetailsByID(oSR.getSalesReturnId(),oConn);
			
				oSB.append("Return No: " + oSR.getReturnNo()).append("\n");
				oSB.append("BEFORE UPDATE Total Tax(" + oSR.getTotalTax() + ") + Total Amount (" + oSR.getTotalAmount() + ")" ).append("\n");				
				
				oCrit = new Criteria();
				oCrit.add(CreditMemoPeer.TRANSACTION_ID, oSR.getSalesReturnId());
				List vCM = CreditMemoPeer.doSelect(oCrit, oConn);
				if (vCM.size() > 0)
				{
					CreditMemo oCM = (CreditMemo) vCM.get(0);
					if (oCM.getStatus() == i_MEMO_CLOSED && !oSR.getCashReturn())
					{
						
						oSB.append("Invalid CM already used in payment " + oCM.getPaymentTransNo()).append("\n");
						throw new Exception("Credit Memo " + oCM.getCreditMemoNo() + 
							" already used in Payment No " + oCM.getPaymentTransNo());
					}
					else 
					{
						CreditMemoTool.cancelCM (oCM, oSR.getCreateBy(), oConn);
						oCrit = new Criteria();
						oCrit.add(CreditMemoPeer.CREDIT_MEMO_ID,oCM.getCreditMemoId());
						CreditMemoPeer.doDelete(oCrit, oConn);
						oSB.append("Credit Memo: " + oCM.getMemoNo() + " CANCELLED").append("\n");
					}
				}
				SalesReturnJournalTool.deleteJournal (GlAttributes.i_GL_TRANS_SALES_RETURN, oSR.getSalesReturnId(), oConn);                
				oSB.append("SalesReturn Journal Deleted").append("\n");
				
				//recalculate SR
				oSR.setIsInclusiveTax(true);
				SalesReturnTool.setHeaderProperties(oSR,vTD,null);

				oSB.append("AFTER UPDATE Total Tax(" + oSR.getTotalTax() + ") + Total Amount (" + oSR.getTotalAmount() + ")" ).append("\n");

				//create
				if (oSR.getTransactionType() == i_RET_FROM_NONE) 
				{
					CreditMemoTool.createFromSalesReturn (oSR, oConn);
				}
				oSB.append("Recreate Credit Memo").append("\n");
				
				SalesReturnJournalTool oReturnJournal = new SalesReturnJournalTool(oSR, oConn);
				oReturnJournal.createSalesReturnJournal(oSR, vTD, new HashMap());

				oSB.append("Recreate Journal").append("\n");
				
				//update GL journal
				oSR.save(oConn);
				BaseTool.commit (oConn);

				oSB.append("FINISH").append("\n");
			}
			else
			{
				oSB.append("Return " + _sReturnNo + " NOT FOUND ").append("\n");				
			}
		}
		catch (Exception _oEx)
		{			
			oSB.append("ERROR : " + _oEx.getMessage());
			oSB.append("ERROR : " + _oEx.getStackTrace());
			
			BaseTool.rollback ( oConn );
			_oEx.printStackTrace();
			throw new NestableException(_oEx.getMessage(), _oEx);
		}
	}	
	
	public String getResult()
	{
		if (oSB != null)
		{
			return oSB.toString();
		}
		return "";
	}
}
