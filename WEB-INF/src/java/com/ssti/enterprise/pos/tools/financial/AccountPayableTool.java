package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountPayable;
import com.ssti.enterprise.pos.om.AccountPayablePeer;
import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountPayableTool.java,v 1.14 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountPayableTool.java,v $
 * Revision 1.14  2009/05/04 02:04:13  albert
 * 
 * 2015-10-18
 * - override getTransactionHistory add parameter in main method to query by list of id
 * - override filterAmountByType add parameter _dBefore in main method to enable filter by date before
 *   so we only need to query all transaction history once  
 * </pre><br>
 */
public class AccountPayableTool extends BaseTool
{
    private static Log log = LogFactory.getLog(AccountPayableTool.class);

    private static int[] a_TRANS_TYPE = 
    {
        i_AP_TRANS_CASH_PAYMENT,
        i_AP_TRANS_DEBIT_MEMO,
        i_AP_TRANS_PURCHASE_INVOICE,
        i_AP_TRANS_OPENING_BALANCE 
    };

    private static AccountPayable getAccountPayableByID (String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AccountPayablePeer.ACCOUNT_PAYABLE_ID, _sID);
		List vData = AccountPayablePeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (AccountPayable) vData.get(0);
		}
		return null;	
	}
    
    /**
     * 
     * @param _sTransID
     * @param _oConn
     * @return List of AP Trans
     * @throws Exception
     */
    private static List getDataByTransID (String _sTransID, Connection _oConn)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(AccountPayablePeer.TRANSACTION_ID, _sTransID);
    	return AccountPayablePeer.doSelect (oCrit, _oConn);
    }

    /**
     * 
     * @param _sTransID
     * @param _sTransNo
     * @param _oConn
     * @return List of AP Trans
     * @throws Exception
     */
    private static List getDataByTransID (String _sTransID, String _sTransNo, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(AccountPayablePeer.TRANSACTION_ID, _sTransID);
        oCrit.add(AccountPayablePeer.TRANSACTION_NO, _sTransNo);
        return AccountPayablePeer.doSelect (oCrit, _oConn);
    }
    
    /**
     * check whether vendor has ap trans
     * @param _sVendorID
     * @return is vendor ever make an AP Trans
     */
	public static boolean isTransExist(String _sVendorID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AccountPayablePeer.VENDOR_ID, _sVendorID);
		oCrit.setLimit(1);
		List vData = AccountPayablePeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return true;
		}
		return false;
	}	    
    
	/**
	 * update vendor opening balance by creating AP transaction with type OB
	 * 
	 * @param _oVendor
	 * @param _bIsUpdate
	 * @param _sUserName
	 * @throws Exception
	 */
    public static void updateOpeningBalance (Vendor _oVendor, boolean _bIsUpdate, String _sUserName)
        throws Exception
    {       
	    Connection oConn = null;
	    try
	    {
	    	oConn = beginTrans();    	
	    	String sDesc = "AP Opening Balance ";        
	    	if (_bIsUpdate) sDesc = "Update AP Opening Balance ";
	    	sDesc += _oVendor.getVendorCode();
	    	
	    	AccountPayable oOldAP = null;
	    	AccountPayable oAP = null;
	    	if (StringUtil.isNotEmpty(_oVendor.getObTransId())) //if ob trans id exist then get old AP
	    	{
	    		oAP = getAccountPayableByID(_oVendor.getObTransId(), oConn);
	    		if (oAP != null)
	    		{
	    			oOldAP = oAP.copy();
	    			oAP.setModified(true);
	    		}
	    	}
	    	if (oAP == null)
	    	{
	    		oAP = new AccountPayable();
	    		oAP.setAccountPayableId (IDGenerator.generateSysID());	
		    	oAP.setTransactionId	("");
		    	oAP.setTransactionNo	("");   
		    	oAP.setVendorId		    (_oVendor.getVendorId());
		    	oAP.setVendorName	    (_oVendor.getVendorName());
	    	}
	    	
	    	double dAmountBase = _oVendor.getOpeningBalance().doubleValue() * _oVendor.getObRate().doubleValue();
	    	
	    	oAP.setTransactionType  (i_AP_TRANS_OPENING_BALANCE);
	    	oAP.setCurrencyId       (_oVendor.getDefaultCurrencyId());
	    	oAP.setCurrencyRate     (_oVendor.getObRate());
	    	oAP.setAmount           (_oVendor.getOpeningBalance());	    
	    	oAP.setAmountBase       (new BigDecimal(dAmountBase));
	    	oAP.setTransactionDate	(_oVendor.getAsDate());
	    	oAP.setDueDate			(_oVendor.getAsDate());
	    	oAP.setRemark			(sDesc);
	    	oAP.save(oConn);
	    	
	    	VendorBalanceTool.updateBalance (oOldAP, oAP, oConn);
	    	updateOpeningBalanceGLAccount (oOldAP, oAP, _sUserName, oConn);
	    	
	    	_oVendor.setObTransId(oAP.getAccountPayableId());
	    	_oVendor.save(oConn);

		    commit(oConn);    
	    }
	    catch (Exception _oEx)
	    {
	    	log.error (_oEx);	    	
	    	rollback (oConn);
	    	throw new NestableException (LocaleTool.getString("ob_failed")  + _oEx.getMessage(), _oEx);
	    }
    }

	/**
	 * create vendor openingbalance journal from AP
	 * 
	 * @param _oAP
	 * @param _sUserName
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updateOpeningBalanceGLAccount(AccountPayable _oOldAP, 
													  AccountPayable _oAP, 
													  String _sUserName, 
													  Connection _oConn) 
		throws Exception
	{

		Account oAPAccount = 
			AccountTool.getAccountPayable(_oAP.getCurrencyId(), _oAP.getVendorId(), _oConn);
		
		Account oEquity = AccountTool.getOpeningBalanceEquity(_oConn);
		double dAPAmount = oAPAccount.getOpeningBalance().doubleValue();
		
		if (_oOldAP != null)
		{
			//if old ap exists then substract ap amount from old value
			dAPAmount = dAPAmount - _oOldAP.getAmount().doubleValue();  
		}
		//sum all current AP amount to total account ob
		//total AP account opening balance value is not used in JV
		dAPAmount = dAPAmount + _oAP.getAmount().doubleValue();
		
		oAPAccount.setOpeningBalance(new BigDecimal(dAPAmount)); 
		oAPAccount.setAsDate(PreferenceTool.getStartDate()); //set default to start date		
		oAPAccount.setObTransId(""); //set ob trans id to empty because 1 AP/AR acc is multiple ob trans id
		oAPAccount.setObRate(_oAP.getCurrencyRate()); 
		oAPAccount.save(_oConn); //will not saved again in JV
		
		//create account ob jvm pass AP as sub ledger
        JournalVoucherTool.createAccountOB(oAPAccount, oEquity, _sUserName, _oAP, i_SUB_LEDGER_AP, _oConn);		
	}
    
	/**
	 * create AP Subledger journal from Invoice
	 * 
	 * @param _oPI
	 * @param _bMulti is PI in multi currency
	 * @param _oConn
	 * @throws Exception
	 */
	public static void  createAPEntryFromInvoice (PurchaseInvoice _oPI, boolean _bMulti, Connection _oConn)
    	throws Exception
    {
	    double dAmount = _oPI.getTotalAmount().doubleValue();		
		double dTotalTax = _oPI.getTotalTax().doubleValue();
	    double dFiscalRate = _oPI.getFiscalRate().doubleValue();
		if (_bMulti) 
		{
			dAmount = dAmount - dTotalTax;
			if (dFiscalRate == 0 || dFiscalRate == 1) 
			{
				dFiscalRate = CurrencyTool.getFiscalRate(_oPI.getCurrencyId(), _oConn);
			}
		}
		double dAmountBase = dAmount * _oPI.getCurrencyRate().doubleValue();

		AccountPayable oAccount = new AccountPayable();			
		oAccount.setAccountPayableId (IDGenerator.generateSysID());	
		oAccount.setTransactionId	 (_oPI.getPurchaseInvoiceId());	
	    oAccount.setTransactionNo	 (_oPI.getPurchaseInvoiceNo());
	    oAccount.setTransactionType  (i_AP_TRANS_PURCHASE_INVOICE);
		oAccount.setVendorId		 (_oPI.getVendorId());
		oAccount.setVendorName	 	 (_oPI.getVendorName());
		oAccount.setCurrencyId       (_oPI.getCurrencyId());
		oAccount.setCurrencyRate     (_oPI.getCurrencyRate());	    
		oAccount.setAmount           (new BigDecimal(dAmount));
		oAccount.setAmountBase       (new BigDecimal(dAmountBase));
	    oAccount.setTransactionDate	 (_oPI.getPurchaseInvoiceDate ());
	    oAccount.setDueDate			 (_oPI.getPaymentDueDate());
	    oAccount.setRemark			 (_oPI.getRemark());
	    oAccount.setLocationId		 (_oPI.getLocationId());
		oAccount.save(_oConn);			
		
		VendorBalanceTool.updateBalance (oAccount, _oConn);
		
		if (_bMulti && dTotalTax > 0) //create separate TAX AP Journal
		{
			AccountPayable oTaxAcc = new AccountPayable();			
			oTaxAcc.setAccountPayableId (IDGenerator.generateSysID());	
			oTaxAcc.setTransactionId	(_oPI.getPurchaseInvoiceId());	
		    oTaxAcc.setTransactionNo	(_oPI.getPurchaseInvoiceNo());
		    oTaxAcc.setTransactionType  (i_AP_TRANS_PURCHASE_INVOICE);
			oTaxAcc.setVendorId		 	(_oPI.getVendorId());
			oTaxAcc.setVendorName	 	(_oPI.getVendorName());
			oTaxAcc.setCurrencyId       (_oPI.getCurrencyId());
			oTaxAcc.setCurrencyRate     (new BigDecimal(dFiscalRate));	    
			oTaxAcc.setAmount           (new BigDecimal(dTotalTax));
			oTaxAcc.setAmountBase       (new BigDecimal(dTotalTax * dFiscalRate));
		    oTaxAcc.setTransactionDate	(_oPI.getPurchaseInvoiceDate ());
		    oTaxAcc.setDueDate			(_oPI.getPaymentDueDate());
		    oTaxAcc.setRemark			(_oPI.getRemark() + " [" + LocaleTool.getString("purch_tax") + "]");
		    oTaxAcc.setLocationId		(_oPI.getLocationId());
			oTaxAcc.save(_oConn);			
			
			VendorBalanceTool.updateBalance (oTaxAcc, _oConn);
		}
    }

	/**
	 * create AP Subledger journal from Debit memo
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createAPEntryFromDebitMemo(DebitMemo _oData, Connection _oConn)
		throws Exception
	{
		AccountPayable oAccount = new AccountPayable ();
		oAccount.setAccountPayableId (IDGenerator.generateSysID());	
		oAccount.setTransactionId	 (_oData.getDebitMemoId  	  ());	
        oAccount.setTransactionNo	 (_oData.getDebitMemoNo       ());
        oAccount.setTransactionType	 (i_AP_TRANS_DEBIT_MEMO);
        oAccount.setVendorId		 (_oData.getVendorId	      ());
	    oAccount.setVendorName	 	 (_oData.getVendorName		  ());
	    oAccount.setAmount           (_oData.getAmount		      ());
	    oAccount.setAmountBase       (_oData.getAmountBase		  ());
	    oAccount.setCurrencyId       (_oData.getCurrencyId        ());
	    oAccount.setCurrencyRate     (_oData.getCurrencyRate      ());
        oAccount.setTransactionDate	 (_oData.getTransactionDate	  ());
        oAccount.setDueDate			 (_oData.getDueDate			  ());
        oAccount.setRemark			 (_oData.getRemark 			  ());
        oAccount.setLocationId 		 (_oData.getLocationId		  ());
        
		oAccount.save(_oConn);
		VendorBalanceTool.updateBalance (oAccount, _oConn);
	}

	/**
	 * create AP Subledger journal from AP Payment
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createAPEntryFromApPayment(ApPayment _oData, Connection _oConn)
		throws Exception
	{
		AccountPayable oAccount = new AccountPayable ();
		oAccount.setAccountPayableId (IDGenerator.generateSysID   ());	
		oAccount.setTransactionId	 (_oData.getApPaymentId       ());	
        oAccount.setTransactionNo	 (_oData.getApPaymentNo  	  ());
        oAccount.setTransactionType	 (i_AP_TRANS_CASH_PAYMENT);
        oAccount.setVendorId		 (_oData.getVendorId	      ());
	    oAccount.setVendorName	 	 (_oData.getVendorName		  ());
	    oAccount.setCurrencyId       (_oData.getCurrencyId        ());
	    oAccount.setCurrencyRate     (_oData.getCurrencyRate      ());
	    
	    double dAmount =  _oData.getTotalAmount().doubleValue() + _oData.getTotalDiscount().doubleValue();
	    double dAmountBase =  dAmount * _oData.getCurrencyRate().doubleValue();
	    
	    oAccount.setAmount           (new BigDecimal(dAmount));
	    oAccount.setAmountBase       (new BigDecimal(dAmountBase));
        oAccount.setTransactionDate	 (_oData.getApPaymentDate	  ());
        oAccount.setDueDate			 (_oData.getApPaymentDueDate  ());
        oAccount.setRemark			 (_oData.getRemark 			  ());
        oAccount.setLocationId 		 (_oData.getLocationId		  ());
		oAccount.save(_oConn);
		VendorBalanceTool.updateBalance (oAccount, _oConn);		
	}

	/**
	 * rollback existing transaction journal
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void  rollbackAP (String _sTransID, Connection _oConn)
		throws Exception
	{
		List vAP = getDataByTransID (_sTransID, _oConn);
		
		for (int i = 0; i < vAP.size(); i++)
		{
			AccountPayable oAP = (AccountPayable) vAP.get(i);
			if (oAP != null)
			{
				double dAmount = oAP.getAmount().doubleValue();
				double dAmountBase = oAP.getAmountBase().doubleValue();
				oAP.setAmount(new BigDecimal (dAmount * -1));
				oAP.setAmountBase(new BigDecimal(dAmountBase * -1));
				
				VendorBalanceTool.updateBalance(oAP, _oConn);
				
				Criteria oCrit = new Criteria();
				oCrit.add(AccountPayablePeer.ACCOUNT_PAYABLE_ID, oAP.getAccountPayableId());
				AccountPayablePeer.doDelete(oCrit, _oConn);
			}		
		}
	}	
	
    /**
     * rollback existing transaction journal
     * 
     * @param _sTransID
     * @param _sTransNo
     * @param _oConn
     * @throws Exception
     */
    public static void  rollbackAP (String _sTransID, String _sTransNo, Connection _oConn)
        throws Exception
    {
        List vAP = getDataByTransID (_sTransID, _sTransNo,_oConn);
        
        for (int i = 0; i < vAP.size(); i++)
        {
            AccountPayable oAP = (AccountPayable) vAP.get(i);
            if (oAP != null)
            {
                double dAmount = oAP.getAmount().doubleValue();
                double dAmountBase = oAP.getAmountBase().doubleValue();
                oAP.setAmount(new BigDecimal (dAmount * -1));
                oAP.setAmountBase(new BigDecimal(dAmountBase * -1));
                
                VendorBalanceTool.updateBalance(oAP, _oConn);
                
                Criteria oCrit = new Criteria();
                oCrit.add(AccountPayablePeer.ACCOUNT_PAYABLE_ID, oAP.getAccountPayableId());
                AccountPayablePeer.doDelete(oCrit, _oConn);
            }       
        }
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static List getTransactionInDateRange (Date _dStart, Date _dEnd) 
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(AccountPayablePeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        oCrit.and(AccountPayablePeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);        
		return AccountPayablePeer.doSelect(oCrit);
	}

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _vIDs
	 * @param _sLocationID
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
	                                          Date _dEnd,
	                                          List _vIDs,
	                                          String _sLocationID) 
    	throws Exception
	{
		return getTransactionHistory(_dStart, _dEnd, _vIDs, "", _sLocationID, -1);
	}

	/**
	 * old method to get trans history
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sVendorTypeID
	 * @param _sVendorID
	 * @param _sLocationID
	 * @param _iType
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
	                                          Date _dEnd,
	                                          String _sVendorID,
	                                          String _sLocationID,
	                                          int _iType) 
    	throws Exception
	{
		return getTransactionHistory(_dStart, _dEnd, null, _sVendorID, _sLocationID, _iType);
	}

	
	/**
	 * get List of AP Journal Transaction
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sVendorID
	 * @param _iType
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
	                                          Date _dEnd,
	                                          List _vIDs,
	                                          String _sVendorID,
	                                          String _sLocationID,
	                                          int _iType) 
    	throws Exception
	{
		Criteria oCrit = new Criteria();
		if (_dStart != null)
        {
            oCrit.add(AccountPayablePeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        }
		if (_dEnd != null)
        {
            oCrit.and(AccountPayablePeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);        
		}
		if(_vIDs != null)
		{
			oCrit.addIn(AccountPayablePeer.VENDOR_ID, _vIDs);
		}		
		if (StringUtil.isNotEmpty(_sVendorID))
		{
            oCrit.add(AccountPayablePeer.VENDOR_ID, _sVendorID);        
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
            oCrit.add(AccountPayablePeer.LOCATION_ID, _sLocationID);        
		}		
		if (_iType > 0)
		{
            oCrit.add(AccountPayablePeer.TRANSACTION_TYPE, _iType);        
		} 
		oCrit.addAscendingOrderByColumn (AccountPayablePeer.TRANSACTION_DATE);
		return AccountPayablePeer.doSelect(oCrit);
	}

	public static int[] getTransTypeList () 
    	throws Exception
    {		
        return a_TRANS_TYPE;
	}	
	
	/**
	 * check whether this trans type add to AR or substract AP Balance
	 * 
	 * @param _iTransactionType
	 * @return
	 */
	public static final boolean isAdd (Integer _iTransactionType) 
	{
		if ((_iTransactionType.intValue() == i_AP_TRANS_PURCHASE_RECEIPT)  ||
		    (_iTransactionType.intValue() == i_AP_TRANS_PURCHASE_INVOICE)  ||
		    (_iTransactionType.intValue() == i_AP_TRANS_OPENING_BALANCE)) 
        {
            return true;
		}
		return false;
	}
	
	/**
	 * get trans type name 
	 * 
	 * @param _iTransType
	 * @return localized transaction name
	 */
	public static final String getTransTypeName (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);
		
		if (iType == i_AP_TRANS_CASH_PAYMENT    ) return LocaleTool.getString("payable_payment");
		if (iType == i_AP_TRANS_DEBIT_MEMO      ) return LocaleTool.getString("debit_memo");
		if (iType == i_AP_TRANS_PURCHASE_INVOICE) return LocaleTool.getString("purchase_invoice");
		if (iType == i_AP_TRANS_OPENING_BALANCE ) return LocaleTool.getString("opening_balance");
		if (iType == i_AP_TRANS_PI_FREIGHT) 	  return LocaleTool.getString("freight_cost");
		return "";
	}
	
	/**
	 * get trans screen
	 * 
	 * @param _iTransType
	 * @return
	 * @throws Exception transaction screen
	 */
	public static final String getTransactionScreen (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);
		
		if (iType == i_AP_TRANS_CASH_PAYMENT     ) return s_SCREEN_AP_PAYMENT;
		if (iType == i_AP_TRANS_DEBIT_MEMO       ) return s_SCREEN_DEBIT_MEMO;
		if (iType == i_AP_TRANS_PURCHASE_INVOICE ) return s_SCREEN_PURCHASE_INVOICE;
		if (iType == i_AP_TRANS_PI_FREIGHT       ) return s_SCREEN_PURCHASE_INVOICE;
		if (iType == i_AP_TRANS_OPENING_BALANCE  ) return s_SCREEN_JOURNAL_VOUCHER;
		return "#";
	}

	/**
	 * filter total amount
	 * 
	 * @param _vData
	 * @param _iType
	 * @return double [total amountbase, total amount]
	 * @throws Exception
	 */
	public static double[] filterTotalAmountByType(List _vData, int _iType)
    	throws Exception
    {
		return filterTotalAmountByType(_vData, null, _iType);
    }
	
	/**
	 * filter total amount
	 * 
	 * @param _vData
	 * @param _iType
	 * @return double [total amountbase, total amount]
	 * @throws Exception
	 */
	public static double[] filterTotalAmountByType(List _vData, Date _dBefore, int _iType)
    	throws Exception
    {
        double[] dTotal = new double[2];
        for (int i = 0; i < _vData.size(); i++)
        {
            AccountPayable oARP = (AccountPayable)_vData.get(i);
            if (oARP != null && (_dBefore == null || _dBefore != null && DateUtil.isBefore(oARP.getTransactionDate(), _dBefore)))
            {
                //AP transaction that add value to ar Balance
                if (_iType == 1)
                {
                    if (isAdd(Integer.valueOf(oARP.getTransactionType())))
                    {
                        dTotal[i_BASE] = dTotal[i_BASE] + oARP.getAmountBase().doubleValue();
                        dTotal[i_PRIME] = dTotal[i_PRIME] + oARP.getAmount().doubleValue();
                    }
                }
                //AP transaction that minus value to ar Balance
                else if (_iType == 2)
                {

                    if (!isAdd(Integer.valueOf(oARP.getTransactionType())))
                    {
                        dTotal[i_BASE] = dTotal[i_BASE] + oARP.getAmountBase().doubleValue();
                        dTotal[i_PRIME] = dTotal[i_PRIME] + oARP.getAmount().doubleValue();
                    }
                }
            }
        }
        return dTotal;
	}	

	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods
	/////////////////////////////////////////////////////////////////////////////////////

    /**
     * count total owing from list of transaction
     *  
     * @param _vTrans
     * @param _sCurrencyID
     * @param _dAsOf
     * @param _iMode
     * @param _iDayFrom
     * @param _iDayTo
     * @return
     * @throws Exception
     */
	public static double countTotalOwing(List _vTrans, String _sCurrencyID,Date _dAsOf, 
	                                     int _iMode, int _iDayFrom, int _iDayTo)
		throws Exception
	{
	    double dTotal = 0;
	    double dPaid  = 0;
	    double dOwe   = 0;
	    double dTotalOwe = 0;
	    Date dAgingDate  = null;
	    
	    for (int i = 0; i < _vTrans.size(); i++)
	    {
            PurchaseInvoice oTrans = (PurchaseInvoice) _vTrans.get(i);
            
            if (oTrans.getCurrencyId().equals(_sCurrencyID))
            {
                //aging start from transaction date 
                if (_iMode == 1)
                {
                    dAgingDate = oTrans.getPurchaseInvoiceDate();
                }
                //aging start from due date
                else 
                {
                    dAgingDate = oTrans.getPaymentDueDate();
                }
                
                dTotal = oTrans.getTotalAmount().doubleValue(); 
                dPaid  = oTrans.getPaidAmount().doubleValue();
                dOwe   = dTotal - dPaid;
                
                if (_iDayFrom > 0 && _iDayTo > 0)
                {
                    int iAging = DateUtil.countAging( _dAsOf, dAgingDate);
                    
                    if (iAging >= _iDayFrom && iAging <= _iDayTo)
                    {                
                        dTotalOwe = dTotalOwe + dOwe;
                    }
                }
                else 
                {
                    dTotalOwe = dTotalOwe + dOwe;
                }
            }
        }
	    return dTotalOwe;
	}
}
