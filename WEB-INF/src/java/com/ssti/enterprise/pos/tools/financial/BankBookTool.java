package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.BankManager;
import com.ssti.enterprise.pos.om.BankBook;
import com.ssti.enterprise.pos.om.BankBookPeer;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankBookTool.java,v 1.15 2008/06/29 07:12:27 albert Exp $ <br>
 *
 * <pre>
 * $Log: BankBookTool.java,v $
 * 
 * 2016-02-16
 * - Override getBalance add parameter base to have options to get base balance 
 * - add updateBankBookBalance to clear BankBook balance and transaction difference
 * </pre><br>
 */
public class BankBookTool extends BaseTool
{
    private static Log log = LogFactory.getLog(BankBookTool.class);

    public static BankBook getBankBook(String _sID)
		throws Exception    
    {
        return getBankBook(_sID, null);
    }
    
    /**
     * get bank book
     * 
     * @param _sID
     * @param _oConn
     * @return BankBook
     * @throws Exception
     */
	public static BankBook getBankBook(String _sID, Connection _oConn)
		throws Exception
	{
		return BankManager.getInstance().getBankBook(_sID, _oConn);
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return BankBook
	 * @throws Exception
	 */
	private static BankBook getForUpdate(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(BankBookPeer.BANK_ID, _sID);
		oCrit.setForUpdate(true);
		List vData = BankBookPeer.doSelect(oCrit, _oConn);
		if(vData.size() > 0)
		{
			return (BankBook) vData.get(0);
		}
		return null;
	}

	
	/**
	 * update bank balance and balance base
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateBankBook (CashFlow _oData, Connection _oConn)
    	throws Exception
    {
		validate(_oConn);
		
		double dPrimeBalance = 0;
		double dBaseBalance = 0;

		double dAmount = _oData.getCashFlowAmount().doubleValue();
		double dAmountBase = _oData.getAmountBase().doubleValue();

		BankBook oBook = getForUpdate(_oData.getBankId(), _oConn);

		if(oBook != null)
		{
			dPrimeBalance = oBook.getAccountBalance().doubleValue();
			dBaseBalance = oBook.getBaseBalance().doubleValue();	
		}
		else
		{
			oBook = new BankBook();
			oBook.setBankBookId     (IDGenerator.generateSysID());
			oBook.setBankId		    (_oData.getBankId());
        	oBook.setBankName  	    (BankTool.getBankNameByID(_oData.getBankId()));
	    	oBook.setCurrencyId	    (_oData.getCurrencyId());
	    	oBook.setOpeningBalance (bd_ZERO);
        }
		
		if(_oData.getCashFlowType() == i_DEPOSIT)
		{
			dPrimeBalance = dPrimeBalance + dAmount;
			dBaseBalance = dBaseBalance + dAmountBase;
		}
		else
		{
			dPrimeBalance = dPrimeBalance - dAmount;
			dBaseBalance = dBaseBalance - dAmountBase;
		}			
		oBook.setAccountBalance(new BigDecimal(dPrimeBalance ));
		oBook.setBaseBalance(new BigDecimal(dBaseBalance));			
    	oBook.save(_oConn);
    	
    	BankManager.getInstance().refreshCache(oBook.getBankId(), oBook.getCurrencyId());
    }
	
	/**
	 * get bank balance as of
	 * 
	 * @param _sBankID
	 * @param _dAsOf
	 * @return
	 * @throws Exception
	 */
	public static double getBalanceAsOf(String _sBankID, Date _dAsOf)
		throws Exception
	{
	    List vData = CashFlowTool.getCashFlowByBankID (_sBankID, _dAsOf, new Date());
	    double dBalance = 0;
	    BankBook oBook = getBankBook (_sBankID);
	    if (oBook != null)
	    {
	        dBalance = oBook.getAccountBalance().doubleValue();
	    }
        double dDeposit = CashFlowTool.filterTotalAmountByType (vData, i_DEPOSIT);
	    double dWithdrawal = CashFlowTool.filterTotalAmountByType (vData, i_WITHDRAWAL);
	    dBalance = dBalance - dDeposit + dWithdrawal;
		return dBalance;
	}

	public static double getBalance(String _sCFTypeID, 
									String _sCurrID, 
									String _sBankID, 
									Date _dFrom, 
									Date _dTo)
		throws Exception
	{
		return getBalance(_sCFTypeID, _sCurrID, _sBankID, _dFrom, _dTo, false);
	}

	public static double getBalance(String _sCFTypeID, 
									String _sCurrID, 
									String _sBankID, 
									Date _dFrom, 
									Date _dTo,
									boolean _bBase)
    	throws Exception
	{
		Integer iDEP = Integer.valueOf(i_DEPOSIT);
		Integer iWITH = Integer.valueOf(i_WITHDRAWAL);
		
		List vDeposit = CashFlowTool.getCashFlowJournal (_sCFTypeID, _sCurrID, _sBankID, iDEP, _dFrom, _dTo);
        List vWithdrawal = CashFlowTool.getCashFlowJournal (_sCFTypeID, _sCurrID, _sBankID, iWITH, _dFrom, _dTo);
        double dBalance = 0;
        double dDeposit = 0;
        double dWithdrawal = 0;
        
        Iterator it = vDeposit.iterator();
        while(it.hasNext())
        {
        	if(_bBase)
        	{
        		dDeposit += ((CashFlowJournal) it.next()).getAmountBase().doubleValue();
        	}
        	else        	
        	{
        		dDeposit += ((CashFlowJournal) it.next()).getAmount().doubleValue();
        	}
        }
        
        it = vWithdrawal.iterator();
        while(it.hasNext())
        {
        	if(_bBase)
        	{
                dWithdrawal += ((CashFlowJournal) it.next()).getAmountBase().doubleValue();
        	}
        	else        	
        	{
                dWithdrawal += ((CashFlowJournal) it.next()).getAmount().doubleValue();
        	}        	
        }
        dBalance = dDeposit - dWithdrawal;
        return dBalance;
	}
    
    public static double getBalanceByCashFlowType(String _sCFTypeID, String _sCurrencyID, Date _dFrom, Date _dTo)
        throws Exception
    {
    	return getBalance(_sCFTypeID,_sCurrencyID,null,_dFrom,_dTo);
    }
    
    public static double getBalanceByBank(String _sBankID, String _sCFTypeID, Date _dFrom, Date _dTo)
        throws Exception
    {
     	return getBalance(_sCFTypeID,null,_sBankID,_dFrom,_dTo);
    }
    
    /**
     * 
     * @param _sBankID
     */
    public static void updateBankBookBalance(String _sBankID)
    	throws Exception
    {
    	Connection oConn = null;
    	try {
        	oConn = BaseTool.beginTrans();
    	    List vData = CashFlowTool.getCashFlowByBankID (_sBankID, null, new Date());
    	    double dBalance = 0;
    	    BankBook oBook = getForUpdate(_sBankID, oConn);
    	    if(oBook != null)
    	    {
	            double dDeposit = CashFlowTool.filterTotalAmountByType (vData, i_DEPOSIT);
	    	    double dWithdrawal = CashFlowTool.filterTotalAmountByType (vData, i_WITHDRAWAL);
	    	    dBalance = dDeposit - dWithdrawal;
	    	    oBook.setAccountBalance(new BigDecimal(dBalance));
	    	    oBook.save(oConn);
	    	    BankManager.getInstance().refreshCache(oBook.getBankId(), oBook.getCurrencyId());
    	    }
    	    oConn.commit();    	    
		} 
    	catch (Exception e) 
    	{
    		BaseTool.rollback(oConn);
    		log.error(e);
		}    	
    }
}