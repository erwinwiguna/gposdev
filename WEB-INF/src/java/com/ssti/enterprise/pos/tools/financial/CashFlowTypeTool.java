package com.ssti.enterprise.pos.tools.financial;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.CashFlowTypeManager;
import com.ssti.enterprise.pos.om.AccountCashflow;
import com.ssti.enterprise.pos.om.AccountCashflowPeer;
import com.ssti.enterprise.pos.om.CashFlowJournalPeer;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.CashFlowTypePeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for Cash Flow and Cash Flow Journal OM 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashFlowTool.java,v 1.33 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashFlowTool.java,v $

 * </pre><br>
 */
public class CashFlowTypeTool extends BaseTool
{
    private static Log log = LogFactory.getLog(CashFlowTypeTool.class);
    
    static CashFlowTypeTool instance = null;
    
    public static synchronized CashFlowTypeTool getInstance() 
    {
        if (instance == null) instance = new CashFlowTypeTool();
        return instance;
    }
    
    public static List getAllType()
        throws Exception
    {
        return CashFlowTypeManager.getInstance().getAllType();
    }
    
    public static List getAllChild()
        throws Exception
    {
        return CashFlowTypeManager.getInstance().getAllChild();
    }
    
    public static CashFlowType getTypeByID(String _sID, Connection _oConn)
        throws Exception
    {
        return CashFlowTypeManager.getInstance().getTypeByID(_sID, _oConn);
    }
    
    public static CashFlowType getTypeByCode(String _sCode, Connection _oConn)
        throws Exception
    {
        return CashFlowTypeManager.getInstance().getTypeByCode(_sCode, _oConn);
    }
    
    public static String getCodeByID(String _sID)
        throws Exception
    {
        return getTypeCodeByID(_sID,null);
    }
    
    public static String getTypeCodeByID(String _sID, Connection _oConn)
        throws Exception
    {
        CashFlowType oCFT = getTypeByID(_sID, _oConn);
        if (oCFT != null)
        {
            return oCFT.getCashFlowTypeCode();
        }
        return "";
    }
    
    public static String getIDByCode(String _sCode, Connection _oConn)
        throws Exception
    {
        CashFlowType oCFT = getTypeByCode(_sCode, _oConn);
        if (oCFT != null)
        {
            return oCFT.getCashFlowTypeId();
        }
        return "";
    }
    
    public static int setChildLevel(String _sParentID)
        throws Exception
    {
        int iLevel = 1;
        if (StringUtil.isNotEmpty(_sParentID))
        {
            CashFlowType oParent = getTypeByID(_sParentID,null);
            iLevel = oParent.getCashFlowLevel() + 1;
        }
        return iLevel;
    }
    
    public static boolean hasChild (String _sID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add (CashFlowTypePeer.PARENT_ID, _sID);
        List vData = CashFlowTypePeer.doSelect(oCrit);
        if (vData.size() > 0) return true;
        return false;
    }
    
    public static List getChildIDList (String _sID)
        throws Exception
    {
        return getChildIDList(new ArrayList(), _sID);
    }
    
    public static List getChildIDList (List _vID, String _sID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(CashFlowTypePeer.PARENT_ID, _sID);
        List vData = CashFlowTypePeer.doSelect(oCrit);
        for (int i=0; i < vData.size(); i++)
        {
            CashFlowType oCFT = (CashFlowType)vData.get(i);
            getChildIDList(_vID, oCFT.getCashFlowTypeId());
            _vID.add( oCFT.getCashFlowTypeId());
        }
        return _vID;
    }    
       
    public static boolean isUsed(String _sCFTID)
    throws Exception
    {
        Criteria oCrit = new Criteria();        
        oCrit.addJoin (CashFlowJournalPeer.CASH_FLOW_ID, CashFlowPeer.CASH_FLOW_ID);
        oCrit.add (CashFlowJournalPeer.CASH_FLOW_TYPE_ID, _sCFTID);
        oCrit.add (CashFlowPeer.STATUS, i_PAYMENT_PROCESSED);
        List v = CashFlowJournalPeer.doSelect(oCrit);
        if (v.size() > 0)
        {
            return true;
        }
        return false;
    }   

    /**
     * update parent position when an account is currently having value and balance but 
     * another account is added into the account then we set the account into a new parent
     *
     * @param _oParent
     * @param _oConn
     * @throws Exception
     */
    public static void updateParent(CashFlowType _oParent, Connection _oConn)
        throws Exception
    {
        if (_oParent != null)
        {
            //validate parent process
            //if !parent haschild 
            if (!_oParent.getHasChild())
            {
                //if parent existing transaction exist 
                if (isUsed(_oParent.getCashFlowTypeId()))
                {
                    throw new Exception("Can not Update Type As Parent, CF Type " + _oParent.getCashFlowTypeCode() + 
                                        " Already used in Transaction");
                }
                _oParent.setHasChild(true);
                
                //SAVE Original Parent
                if (_oConn == null) _oParent.save(); else _oParent.save(_oConn);
                CashFlowTypeManager.getInstance().refreshCache(_oParent);                
            }
        }
    }

    //-------------------------------------------------------------------------
    // display
    //-------------------------------------------------------------------------
    public static String display(CashFlowType _oCFT, String _sField)
    {
        return display(_oCFT, _sField, "", true);
    }
    public static String display(CashFlowType _oCFT, String _sField, String _sSPC, boolean _bBold)
    {
        if (StringUtil.isEmpty(_sSPC)) _sSPC = "&nbsp;&nbsp;&nbsp;&nbsp;";
        StringBuilder oSB = new StringBuilder();
        if (_oCFT != null)
        {
            int iRepeat = _oCFT.getCashFlowLevel() - 1;            
            oSB.append(StringUtil.repeatString(_sSPC,iRepeat));
            if(_bBold && _oCFT.getHasChild())
            {
                oSB.append("<b>");
            }
            if (StringUtil.isEqual(_sField,"code"))
            {
                oSB.append(_oCFT.getCashFlowTypeCode());                
            }
            else
            {
                oSB.append(_oCFT.getDescription());
            }
        }
        return oSB.toString();
    }
        
    //-------------------------------------------------------------------------
    // Account Cash Flow
    //-------------------------------------------------------------------------
    
    public static List findAccCFT(String _sAccID, String _sCFTID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        if (StringUtil.isNotEmpty(_sAccID)) oCrit.add(AccountCashflowPeer.ACCOUNT_ID, _sAccID);
        if (StringUtil.isNotEmpty(_sCFTID)) oCrit.add(AccountCashflowPeer.CASH_FLOW_TYPE_ID, _sCFTID);
        return AccountCashflowPeer.doSelect(oCrit);
    }

    public static AccountCashflow getAccCFT(String _sAccID, String _sCFTID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(AccountCashflowPeer.ACCOUNT_ID, _sAccID);
        oCrit.add(AccountCashflowPeer.CASH_FLOW_TYPE_ID, _sCFTID);
        List vData = AccountCashflowPeer.doSelect(oCrit);
        if (vData.size() > 0)
        {
            return (AccountCashflow)vData.get(0);
        }
        return null;
    }

    //-------------------------------------------------------------------------
    // ALT Code
    //-------------------------------------------------------------------------    
    /**
     * 
     * @param _sAltCode
     * @return
     * @throws Exception
     */
    public static List getByAltCode(String _sAltCode)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(CashFlowTypePeer.ALT_CODE, _sAltCode);
        oCrit.add(CashFlowTypePeer.HAS_CHILD, false);
        return CashFlowTypePeer.doSelect(oCrit);        
    }
}