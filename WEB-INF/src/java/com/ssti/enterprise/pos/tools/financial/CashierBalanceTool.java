package com.ssti.enterprise.pos.tools.financial;

import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CashierBalance;
import com.ssti.enterprise.pos.om.CashierBalancePeer;
import com.ssti.enterprise.pos.om.CashierBillsPeer;
import com.ssti.enterprise.pos.om.CashierCouponPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashierBalanceTool.java,v 1.5 2008/03/03 03:04:26 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashierBalanceTool.java,v $
 * 
 * 
 * </pre><br>
 */
public class CashierBalanceTool extends BaseTool 
{
	public static final int i_OPEN  = 1;
	public static final int i_CLOSE = 2;

	static CashierBalanceTool instance = null;
	
	public static synchronized CashierBalanceTool getInstance() 
	{
		if (instance == null) instance = new CashierBalanceTool();
		return instance;
	}
	
    public static CashierBalance getByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(CashierBalancePeer.CASHIER_BALANCE_ID, _sID);
		List vData = CashierBalancePeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (CashierBalance) vData.get(0);
		}
		return null;
	}

	//static methods
	public static List getAllCashierBalance()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return CashierBalancePeer.doSelect(oCrit);
	}

	public static CashierBalance getBalance(String _sCashierName, 
											String _sLocationID,
											String _sStartDate, 
											String _sEndDate, 
											int _iType,
											int _iOrderBy)
        throws Exception
	{		
	    Date dStartDate = new Date();
	    Date dEndDate   = new Date();
	    
	    if(StringUtil.isNotEmpty(_sStartDate)) dStartDate = DateUtil.parseStartDate(_sStartDate);
	    if(StringUtil.isNotEmpty(_sEndDate))   dEndDate =  CustomParser.parseDate(_sEndDate);

	    dStartDate = DateUtil.parseStartDate(CustomFormatter.fmt(dStartDate));
	    //dEndDate = DateUtil.getEndOfDayDate(dEndDate);
	    
	    Criteria oCrit  = new Criteria();
	    oCrit.add(CashierBalancePeer.CASHIER_NAME,_sCashierName);
	    oCrit.add(CashierBalancePeer.LOCATION_ID,_sLocationID);
	    oCrit.add(CashierBalancePeer.TRANSACTION_DATE, dStartDate, Criteria.GREATER_EQUAL);
	    oCrit.and(CashierBalancePeer.TRANSACTION_DATE, dEndDate, Criteria.LESS_EQUAL);
	    oCrit.add(CashierBalancePeer.TRANSACTION_TYPE, _iType);
	    
	    if (_iOrderBy == i_DESC)
	    {
	    	oCrit.addDescendingOrderByColumn(CashierBalancePeer.TRANSACTION_DATE);
	    }
	    else
	    {
	    	oCrit.addAscendingOrderByColumn(CashierBalancePeer.TRANSACTION_DATE);	    
	    }
	    System.out.println("getBalance Criteria : " + oCrit);
	    log.debug("getBalance Criteria : " + oCrit);
	    
		List vCashierBalance = CashierBalancePeer.doSelect(oCrit);
		
		if(vCashierBalance.size() > 0)
		{
		    return (CashierBalance) vCashierBalance.get(0);
	    }
	    else
	    {
	        return null;
	    }
    }
    
    public static List findData(Date _dStart, Date _dEnd, String _sCashier, String _sLocID, int _iTransType)
    	throws Exception
    {
        Criteria oCrit = new Criteria();
	    oCrit.add(CashierBalancePeer.TRANSACTION_DATE, _dStart, Criteria.GREATER_EQUAL);
		oCrit.and(CashierBalancePeer.TRANSACTION_DATE, _dEnd, Criteria.LESS_EQUAL);
		oCrit.add(CashierBalancePeer.TRANSACTION_TYPE, _iTransType);
		if(StringUtil.isNotEmpty(_sLocID))
		{
		    oCrit.add(CashierBalancePeer.LOCATION_ID, _sLocID);			
		}
		if(StringUtil.isNotEmpty(_sCashier))
		{
		    oCrit.add(CashierBalancePeer.CASHIER_NAME, _sCashier);
	    } 
		return CashierBalancePeer.doSelect(oCrit);
    }

    public static boolean isOpened(String _sCashier, String _sLocID)
    	throws Exception
    {
    	CashierBalance oBalance = getBalance(_sCashier,_sLocID,"","", CashierBalanceTool.i_OPEN, i_DESC);
    	System.out.println(oBalance);
    	if (oBalance == null) //last balance not found
    	{
    		return false;
    	}
    	else //if last balance exist check if it's closed
    	{
    		if (isClosed(oBalance)) 
    		{	
    			return false; //last balance is already closed, means cashier is not opened
    		}
    		else 
    		{
    			return true; //last balance is not closed yet, means cashier already open trans
    		}
    	}
    }
    
	/**
	 * @param balance
	 * @return
	 */
	public static boolean isClosed(CashierBalance _oBalance) 
		throws Exception
	{
		if (_oBalance != null)
		{
			Criteria oCrit = new Criteria();
			oCrit.add(CashierBalancePeer.OPEN_CASHIER_ID, _oBalance.getCashierBalanceId());
			oCrit.add(CashierBalancePeer.TRANSACTION_TYPE, i_CLOSE);
			List vData = CashierBalancePeer.doSelect(oCrit);
			if (vData.size() > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	public static CashierBalance getClosingTrans(CashierBalance _oBalance) 
		throws Exception
	{
		if (_oBalance != null)
		{
			Criteria oCrit = new Criteria();
			oCrit.add(CashierBalancePeer.OPEN_CASHIER_ID, _oBalance.getCashierBalanceId());
			oCrit.add(CashierBalancePeer.TRANSACTION_TYPE, i_CLOSE);
			List vData = CashierBalancePeer.doSelect(oCrit);
			if (vData.size() > 0)
			{
				return (CashierBalance)vData.get(0);
			}
		}
		return null;
	}
	
	public static CashierBalance getCurrent(String _sCashier) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CashierBalancePeer.TRANSACTION_TYPE, i_OPEN);
		oCrit.add(CashierBalancePeer.CASHIER_NAME, _sCashier);
		oCrit.addDescendingOrderByColumn(CashierBalancePeer.TRANSACTION_DATE);
		oCrit.setLimit(1);
		List vData = CashierBalancePeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (CashierBalance)vData.get(0);
		}
		return null;
	}
	
	public static String getShift(String _sShiftCode)
	{
		if (StringUtil.isEqual(_sShiftCode,"1"))
		{
			return LocaleTool.getString("morning");
		}
		else if (StringUtil.isEqual(_sShiftCode,"2"))
		{
			return LocaleTool.getString("afternoon");	
		}
		else if (StringUtil.isEqual(_sShiftCode,"3"))
		{
			return LocaleTool.getString("night");
		}
		return "";
	}
	
	public static String getCurrentShift(String _sCashier) 
		throws Exception
	{
		CashierBalance oCurrent = getCurrent(_sCashier);
		if (oCurrent != null)
		{
			return getShift(oCurrent.getShift());
		}
		return "";
	}
	
    public static void validateOpenCashier(RunData data, Context context) 
		throws Exception
	{
    	if (data != null && (PreferenceTool.validateOpenCashier() || PreferenceTool.medicalInstalled()))
    	{
    		AccessControlList oACL = data.getACL();
    		if (oACL != null && oACL.getRoles().containsName(s_CASHIER_ROLE))
    		{
    			String sCashier = data.getUser().getName();
    			boolean bOpen = CashierBalanceTool.isOpened(sCashier, PreferenceTool.getLocationID(sCashier));
    			if (!bOpen)
    			{
    				log.debug("Cashier Not Open Yet");
    				context.put("RedirectToOpenCashier", Boolean.valueOf(true));
    			}    			
    		}
    	}
	}
    
    public static boolean isNextOpenExists(CashierBalance _oClose)
    	throws Exception
    {    	
    	Criteria oCrit = new Criteria();
    	oCrit.add(CashierBalancePeer.TRANSACTION_TYPE, i_OPEN);
    	oCrit.add(CashierBalancePeer.CASHIER_NAME, _oClose.getCashierName());
    	oCrit.add(CashierBalancePeer.TRANSACTION_DATE, _oClose.getTransactionDate(), Criteria.GREATER_EQUAL);    	
    	List vData = CashierBalancePeer.doSelect(oCrit);
    	if (vData.size() > 0)
    	{
    		return true;
    	}
    	return false;    	
    }

    public static List getStoreTrans()
    	throws Exception
    {
    	Date dLastSync = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
	    if (dLastSync != null)
	    {
	    	oCrit.add(CashierBalancePeer.TRANSACTION_DATE, dLastSync, Criteria.GREATER_EQUAL);
	    }
		return CashierBalancePeer.doSelect(oCrit);
    }
    
    //-------------------------------------------------------------------------
    // Bills
    //-------------------------------------------------------------------------
    
    public static List getBills(String _sID)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(CashierBillsPeer.CASHIER_BALANCE_ID,_sID);
    	return CashierBillsPeer.doSelect(oCrit);
    }
    
    //-------------------------------------------------------------------------
    // Coupons
    //-------------------------------------------------------------------------

    public static List getCoupons(String _sID)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(CashierCouponPeer.CASHIER_BALANCE_ID,_sID);
    	return CashierCouponPeer.doSelect(oCrit);
    }
}