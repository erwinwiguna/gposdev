package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.ArPaymentDetail;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.CreditMemoPeer;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.journal.CreditMemoJournalTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CreditMemoTool.java,v 1.28 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: CreditMemoTool.java,v $
 * 
 * 2016-02-01
 * -add method getBySourceTransID to get Memo By Source Transaction
 * </pre><br>
 */
public class CreditMemoTool extends BaseTool
{
    private static Log log = LogFactory.getLog(CreditMemoTool.class);
	
    protected static Map m_FIND_PEER = new HashMap();
	
    static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), CreditMemoPeer.CREDIT_MEMO_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), CreditMemoPeer.REMARK);          
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER	  ), CreditMemoPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), CreditMemoPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), CreditMemoPeer.REFERENCE_NO);             	
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), CreditMemoPeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), CreditMemoPeer.BANK_ID);	
        m_FIND_PEER.put (Integer.valueOf(i_CFT		  ), CreditMemoPeer.CASH_FLOW_TYPE_ID);
        m_FIND_PEER.put (Integer.valueOf(i_PMT_NO	  ), CreditMemoPeer.PAYMENT_TRANS_NO);
	}   
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
    
	//static methods
	public static CreditMemo getCreditMemoByID(String _sID)
		throws Exception
	{
		return getCreditMemoByID(_sID, null);
	}

	public static CreditMemo getCreditMemoByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(CreditMemoPeer.CREDIT_MEMO_ID, _sID);
        List vData = new ArrayList();
        if (_oConn != null) {
        	vData = CreditMemoPeer.doSelect(oCrit, _oConn);
        }
        else {
        	vData = CreditMemoPeer.doSelect(oCrit);
        }
        if (vData.size() > 0) {
			return (CreditMemo) vData.get(0);
		}
		return null;
	}
	
	public static List getAllCreditMemo()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return CreditMemoPeer.doSelect(oCrit);
	}
	
	/**
	 * create so down payment
	 * 
	 * @param _oSO
	 * @param _oConn
	 * @throws Exception
	 */
    public static void createFromSODownPayment (SalesOrder _oSO, Connection _oConn)
		throws Exception
	{
		try
		{		    
			double dAmountBase = _oSO.getCurrencyRate().doubleValue() * _oSO.getDownPayment().doubleValue();
			Date dToday = new Date();
			Date dDueDate = new Date();

			CreditMemo oCM = new CreditMemo();
			oCM.setTransactionDate  (_oSO.getTransactionDate());
			oCM.setTransactionId    (_oSO.getSalesOrderId());
			oCM.setTransactionNo    (_oSO.getSalesOrderNo());
			oCM.setTransactionType  (i_AR_TRANS_SO_DOWNPAYMENT);			
			oCM.setCustomerId       (_oSO.getCustomerId());
			oCM.setCustomerName     (_oSO.getCustomerName());
			oCM.setUserName         (_oSO.getUserName());
			oCM.setCurrencyId       (_oSO.getCurrencyId());
			oCM.setCurrencyRate     (_oSO.getCurrencyRate());
			oCM.setAmount           (_oSO.getDownPayment());
			oCM.setAmountBase       (new BigDecimal(dAmountBase));						
			oCM.setBankId           (_oSO.getBankId());
			oCM.setBankIssuer       (_oSO.getBankIssuer());
			oCM.setReferenceNo      (_oSO.getReferenceNo());
			oCM.setDueDate          (_oSO.getDpDueDate());
			oCM.setAccountId        (_oSO.getDpAccountId());
			oCM.setCashFlowTypeId	(_oSO.getCashFlowTypeId());
			oCM.setLocationId		(_oSO.getLocationId());
			
			StringBuilder oRemark = new StringBuilder();
			oRemark.append(_oSO.getRemark());
			oRemark.append("\n DP ").append(LocaleTool.getString("from")).append(" ");
			oRemark.append(_oSO.getSalesOrderNo());
			oRemark.append("\n").append(_oSO.getRemark());
			oCM.setRemark (oRemark.toString());
            
			saveCM (oCM, _oConn);
		}
		catch (Exception _oEx)
		{
		    log.error (_oEx);
		    _oEx.printStackTrace();
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}
    
	public static void cancelFromSalesOrderDP (SalesOrder _oSO, String _sCancelBy, Connection _oConn)
		throws Exception
	{
		try
		{
			Criteria oCrit = new Criteria();
			oCrit.add(CreditMemoPeer.TRANSACTION_TYPE, i_AR_TRANS_SO_DOWNPAYMENT);			
			oCrit.add(CreditMemoPeer.TRANSACTION_ID, _oSO.getSalesOrderId());
			List vCM = CreditMemoPeer.doSelect(oCrit, _oConn);
			if (vCM.size() > 0)
			{
				CreditMemo oCM = (CreditMemo) vCM.get(0);
				if (oCM.getStatus() == i_MEMO_CLOSED)
				{
					throw new Exception (
					  "Credit Memo " + oCM.getCreditMemoNo() + 
						" already used in Payment No " + oCM.getPaymentTransNo());
				}
				cancelCM (oCM, _sCancelBy, _oConn);
			}
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}    
    
    /**
     * create from sales return
     * 
     * @param _oSR
     * @param _oConn
     * @throws Exception
     */
	public static void createFromSalesReturn (SalesReturn _oSR, Connection _oConn)
		throws Exception
	{
		try
		{
			double dReturnAmount = _oSR.getReturnedAmount().doubleValue();
			double dAmountBase = _oSR.getCurrencyRate().doubleValue() * dReturnAmount;
			
			CreditMemo oCM = new CreditMemo();
			oCM.setTransactionDate (_oSR.getReturnDate());
			oCM.setTransactionId   (_oSR.getSalesReturnId());
			oCM.setTransactionNo   (_oSR.getReturnNo());
			oCM.setTransactionType (i_AR_TRANS_SALES_RETURN);			
			oCM.setCustomerId 	   (_oSR.getCustomerId());
			oCM.setCustomerName    (_oSR.getCustomerName());
			oCM.setUserName 	   (_oSR.getUserName());
			oCM.setCurrencyId 	   (_oSR.getCurrencyId());
			oCM.setCurrencyRate    (_oSR.getCurrencyRate());
			oCM.setAmount          (new BigDecimal(dReturnAmount));
			oCM.setAmountBase      (new BigDecimal(dAmountBase));
			oCM.setLocationId	   (_oSR.getLocationId());
			
			StringBuilder oRemark = new StringBuilder();
			oRemark.append(LocaleTool.getString("sales_return"))
				   .append(" ")
				   .append(_oSR.getReturnNo())
				   .append("\n")
				   .append(_oSR.getRemark());
			
			if (_oSR.getTransactionType() != i_RET_FROM_NONE)
			{
				oRemark.append(" ").append(LocaleTool.getString("from"))
					   .append(" ").append(_oSR.getTransactionNo());
			}
				
			if (_oSR.getCashReturn())
			{
				Location oReturnLoc = LocationTool.getLocationByID(_oSR.getLocationId(), _oConn);
				
				oCM.setBankId      	  (_oSR.getBankId());
				oCM.setBankIssuer     (_oSR.getBankIssuer());
				oCM.setReferenceNo 	  (_oSR.getReferenceNo());
				oCM.setDueDate 	      (_oSR.getDueDate());
				oCM.setCashFlowTypeId (_oSR.getCashFlowTypeId());
				
				if (StringUtil.isNotEmpty(oCM.getBankId()))
				{
					//close memo, if not it will still be usable in receivable payment
					oCM.setStatus(i_MEMO_CLOSED);
				}
			}
			else
			{
				oCM.setStatus	   (i_MEMO_OPEN);
				oCM.setReferenceNo ("Sales Return Credit Memo");
				oCM.setDueDate     (new Date());			
			}
			
			Account oAR = AccountTool.getAccountReceivable(_oSR.getCurrencyId(), _oSR.getCustomerId(), _oConn);			
			
			oCM.setAccountId (oAR.getAccountId());
			oCM.setRemark 	 (oRemark.toString());			
			
			saveCM (oCM, _oSR.getCashReturn(), _oConn);
		}
		catch (Exception _oEx)
		{
		    log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	public static void cancelFromSalesReturn (SalesReturn _oSR, String _sCancelBy, Connection _oConn)
		throws Exception
	{
		try
		{
			Criteria oCrit = new Criteria();
			oCrit.add(CreditMemoPeer.TRANSACTION_ID, _oSR.getSalesReturnId());
			List vCM = CreditMemoPeer.doSelect(oCrit, _oConn);
			if (vCM.size() > 0)
			{
				CreditMemo oCM = (CreditMemo) vCM.get(0);
				if (oCM.getStatus() == i_MEMO_CLOSED && !_oSR.getCashReturn())
				{
					throw new Exception (
					  "Credit Memo " + oCM.getCreditMemoNo() + 
						" already used in Payment No " + oCM.getPaymentTransNo());
				}
				cancelCM (oCM, _sCancelBy, _oConn);
			}
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

    private static void validateTrans (CreditMemo _oCM, Connection _oConn)
		throws Exception
	{
		//validate currency
		if (StringUtil.isNotEmpty(_oCM.getBankId()))
		{			
			Customer oCustomer = CustomerTool.getCustomerByID(_oCM.getCustomerId(), _oConn);
			Bank oBank = BankTool.getBankByID(_oCM.getBankId(), _oConn);			
			Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);			
			if (!oBankCurr.getIsDefault()) 
			{
				if (!StringUtil.isEqual(oBankCurr.getCurrencyId(),oCustomer.getDefaultCurrencyId()))
				{
					throw new NestableException ("Invalid Cash/Bank Selection");
				} 
			}
		}
	}
	
	public static void saveCM (CreditMemo _oCM, Connection _oConn)
		throws Exception
	{
		saveCM (_oCM, false, _oConn);
	}

	
	/**
	 * Save CM
	 * 
	 * @param _oCM
	 * @param _bCashReturn
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveCM (CreditMemo _oCM, boolean _bCashReturn, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null) 
	    {
		    _oConn = beginTrans();
		    bStartTrans = true;
		}
		try
		{			
			//validate date
			validateDate(_oCM.getTransactionDate(), _oConn);

			//validate trans
			validateTrans(_oCM, _oConn);
			
			//Generate Sys ID
			if (StringUtil.isEmpty(_oCM.getCreditMemoId()))
			{
				_oCM.setCreditMemoId (IDGenerator.generateSysID());			
				validateID(getCreditMemoByID(_oCM.getMemoId(),_oConn), "memoNo");
			}
			//SET Status
			if (_oCM.getStatus() == 0) _oCM.setStatus(i_MEMO_OPEN);
			
			if (_oCM.getStatus() == i_MEMO_OPEN || _oCM.getStatus() == i_MEMO_CLOSED)
			{
				String sLocCode = LocationTool.getLocationCodeByID(_oCM.getLocationId(), _oConn);
				_oCM.setCreditMemoNo (LastNumberTool.get(s_CM_FORMAT, LastNumberTool.i_CREDIT_MEMO, sLocCode, _oConn));
				validateNo(CreditMemoPeer.CREDIT_MEMO_NO,_oCM.getCreditMemoNo(),CreditMemoPeer.class, _oConn);
				_oCM.save ( _oConn );
				
				if (!_bCashReturn)
				{
				    AccountReceivableTool.createAREntryFromCreditMemo(_oCM, _oConn);
				}
				
				if (_oCM.getTransactionType() == i_AR_TRANS_SO_DOWNPAYMENT && StringUtil.isNotEmpty(_oCM.getTransactionId()))
				{
					//update SO DownPayment Amount
					SalesOrder oORD = SalesOrderTool.getHeaderByID(_oCM.getTransactionId(), _oConn);
					oORD.setDownPayment(_oCM.getAmount());
					oORD.save(_oConn);
				}
				
			    //if no bank ID (i.e from NON-CASH Sales Return means that it is just a crossable memo)
			    //if from SO DP or HO CASH Sales Return then Bank ID must not be empty
				if (StringUtil.isNotEmpty(_oCM.getBankId()))
			    {
			    	//createCashFlowEntryFromCreditMemo
			    	CashFlowTool.createFromCreditMemo (_oCM, _oConn);
			    }
			}
			else
			{
				_oCM.save ( _oConn );				
			}		

			if (bStartTrans)
            {
			    //commit transaction
			    commit (_oConn);
		    }
		}
		catch (Exception _oEx)
		{
		    log.error (_oEx);
            if (bStartTrans)
            {
			    rollback (_oConn);
			}
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * Cancel existing CM
	 * 
	 * @param _sCMID CreditMemoID
	 * @param _sCancelBy 
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelCM (CreditMemo _oCM, String _sCancelBy, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null) 
	    {
		    _oConn = beginTrans();
		    bStartTrans = true;
		}
		try
		{			
			if (_oCM != null)
			{
				//validate date
				validateDate(_oCM.getTransactionDate(), _oConn);
				
				//rollback AR
				AccountReceivableTool.rollbackAR(_oCM.getCreditMemoId(), _oConn);
				
				//if cash memo
				if (StringUtil.isNotEmpty(_oCM.getBankId()))
				{
				    //cancel CASH FLOW
				    CashFlowTool.cancelCashFlowByTransID(_oCM.getCreditMemoId(), _sCancelBy, _oConn);
				}
				
				_oCM.setRemark(cancelledBy(_oCM.getRemark(), _sCancelBy) );			
				_oCM.setStatus(i_MEMO_CANCELLED);
				_oCM.save(_oConn);
			}
            if (bStartTrans)
		    {
				//commit transaction
			    commit (_oConn);
            }            
		}
		catch (Exception _oEx)
		{
			if (bStartTrans)
	        {
			    rollback (_oConn);
			}
		    log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}
	
	public static List getCMByCustomerAndStatus(String _sCustomerID, int _iStatus, String _sCurrencyID)
		throws Exception
	{
		return getMemoByEntityStatus(_sCustomerID,_iStatus,_sCurrencyID,null,null);
	}
	
	public static List getMemoByEntityStatus(String _sCustomerID, int _iStatus, String _sCurrencyID, Date _dStart, Date _dEnd)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CreditMemoPeer.CUSTOMER_ID, _sCustomerID);
		oCrit.add(CreditMemoPeer.STATUS, _iStatus);
		oCrit.addAscendingOrderByColumn(CreditMemoPeer.TRANSACTION_DATE);
		if(StringUtil.isNotEmpty(_sCurrencyID))
		{
		    oCrit.add(CreditMemoPeer.CURRENCY_ID, _sCurrencyID);
	    }
		if(_dStart != null)
		{
		    oCrit.add(CreditMemoPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);		   
		}
		if(_dEnd != null)			
		{
		    oCrit.add(CreditMemoPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);		   			
		}
		log.debug("getCMByCustomerAndStatus : " + oCrit);
		return CreditMemoPeer.doSelect(oCrit);
	}


	/**
	 * update memo closing rate, done when memo is used in payable payment.
	 * Closing rate must be set so the closing journal will be correct
	 * 
	 * @param _sMemoID
	 * @param _dClosingRate
	 * @throws Exception
	 */
	public static void updateClosingRate(String _sMemoID, double _dClosingRate)
		throws Exception
	{
		CreditMemo oMemo = getCreditMemoByID(_sMemoID);
		if (oMemo != null)
		{
			oMemo.setClosingRate(new BigDecimal(_dClosingRate));
			oMemo.save();
		}
	}


	/**
	 * save Pending Payment temporarily
	 * 
	 * @param _vMemoIDs
	 * @param _oPP
	 * @param _vAPD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void savePendingPayment(List _vMemoIDs, ArPayment _oPP, List _vAPD, Connection _oConn)
		throws Exception
	{
		for(int i = 0; i < _vMemoIDs.size(); i++)
		{			
			List vData = (List)_vMemoIDs.get(i);
			ArPaymentDetail oAPD = (ArPaymentDetail)_vAPD.get(i);
			for(int j = 0; j < vData.size(); j++)
			{
				CreditMemo oMemo = getCreditMemoByID((String)vData.get(j));
				if (oMemo != null && oMemo.getStatus() == i_MEMO_OPEN)
				{
					if(StringUtil.isEmpty(oMemo.getPaymentTransId()) || 
					   StringUtil.isEqual(oMemo.getPaymentTransId(), _oPP.getArPaymentId()))
					{
						oMemo.setPaymentInvId(oAPD.getArPaymentDetailId());
						oMemo.setPaymentTransId(_oPP.getArPaymentId());
						oMemo.setPaymentTransNo(_oPP.getArPaymentNo());
						oMemo.save(_oConn);
					}					
					else
					{
						throw new NestableException("ERROR: Invalid Credit Memo " + oMemo.getTransactionNo() + 
													" Memo already used by Payment " + oMemo.getPaymentTransNo());
					}					
				}	
				else
				{
					throw new NestableException("ERROR: Invalid Credit Memo, Status is Not OPEN ");
				}
			}   
		}
	}
	
	public static void closeCM(List _vCreditMemoID, ArPayment _oARP, Connection _oConn)
		throws Exception
	{
		List vCMObj = new ArrayList (10);
		for(int i = 0; i < _vCreditMemoID.size(); i++)
		{
			List vData = (List)_vCreditMemoID.get(i);			
			for(int j = 0;j < vData.size(); j++)
			{      
				CreditMemo oCreditMemo = getCreditMemoByID ((String)vData.get(j), _oConn);
				oCreditMemo.setStatus(i_MEMO_CLOSED);
				oCreditMemo.setPaymentTransId (_oARP.getArPaymentId());
				oCreditMemo.setPaymentTransNo (_oARP.getArPaymentNo());
				oCreditMemo.setClosedDate(_oARP.getArPaymentDate());
				oCreditMemo.save(_oConn);
				vCMObj.add(oCreditMemo);
			}   
		}
		//create credit memo closing jornal if any CM used in this payment
		CreditMemoJournalTool.createClosingJournal(vCMObj, _oARP, _oConn);   		
	}
	
	public static void rollbackCMFromARPayment(ArPayment _oARP, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CreditMemoPeer.PAYMENT_TRANS_ID, _oARP.getArPaymentId());
		List vClosedCM = CreditMemoPeer.doSelect(oCrit, _oConn);
	
		//update CM CLOSED Status 
		for(int i = 0; i < vClosedCM.size(); i++)
		{      
			CreditMemo oCreditMemo = (CreditMemo) vClosedCM.get(i);
			oCreditMemo.setStatus(i_MEMO_OPEN);
			oCreditMemo.setPaymentTransId ("");
			oCreditMemo.setPaymentTransNo ("");
			oCreditMemo.setClosedDate(null);
			oCreditMemo.save(_oConn);
		}   
		//any closing journal created will be rolled back in 
		//ReceivablePaymentTool.cancelTransaction
	}
	

	public static LargeSelect findData(int _iCond, 
									   String _sKeywords, 
									   String _sCustomerID, 
									   String _sLocationID,
									   Date _dStart,
									   Date _dEnd,
									   int _iLimit,
									   int _iStatus,
									   String _sCurrencyID)
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			CreditMemoPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID 
		);

		if (StringUtil.isNotEmpty(_sCustomerID))
		{
			oCrit.add(CreditMemoPeer.CUSTOMER_ID, _sCustomerID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(CreditMemoPeer.LOCATION_ID, _sLocationID);
		}
		if (_iStatus > 0)
		{
			oCrit.add(CreditMemoPeer.STATUS, _iStatus);
		}
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.CreditMemoPeer");
	}

	public static List findTrans(String _sCustomerID, 
								 String _sBankID,
								 String _sLocationID,
								 String _sCashFlowTypeID,
								 Date _dFrom, 
								 Date _dTo, 
								 int _iStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if (_dFrom != null)
		{
			oCrit.add(CreditMemoPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		}
		if (_dTo != null)
		{
			oCrit.and(CreditMemoPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sCustomerID))
		{
			oCrit.add(CreditMemoPeer.CUSTOMER_ID, _sCustomerID);
		}
		if (StringUtil.isNotEmpty(_sBankID))
		{
			oCrit.add(CreditMemoPeer.BANK_ID, _sBankID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(CreditMemoPeer.LOCATION_ID, _sLocationID);
		}		
		if (StringUtil.isNotEmpty(_sCashFlowTypeID))
		{
			oCrit.add(CreditMemoPeer.CASH_FLOW_TYPE_ID, _sCashFlowTypeID);
		}				
		if (_iStatus > 0)
		{
			oCrit.add(CreditMemoPeer.STATUS, _iStatus);			
		}
		else
		{
			oCrit.add(CreditMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
		}
		return CreditMemoPeer.doSelect(oCrit);
	}

    //Report methods
	public static List getCustomerList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			CreditMemo oTrans = (CreditMemo) _vData.get(i);			
			if (!isCustomerInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isCustomerInList (List _vTrans, CreditMemo _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			CreditMemo oTrans = (CreditMemo) _vTrans.get(i);			
			if (oTrans.getCustomerId().equals(_oTrans.getCustomerId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByCustomerID (List _vTrans, String _sCustomerID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		CreditMemo oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (CreditMemo ) oIter.next();
			if (!oTrans.getCustomerId().equals(_sCustomerID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}	

	public static List filterTransByStatus (List _vTrans, int _iStatus) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		CreditMemo oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (CreditMemo ) oIter.next();
			if (oTrans.getStatus() == _iStatus) {
				oIter.remove();
			}
		}
		return vTrans;
	}		
		
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(CreditMemoPeer.class, CreditMemoPeer.CREDIT_MEMO_ID, _sID, _bIsNext);
	}	

	public static final String getTransactionTypeName (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);		
		if (iType == i_AR_TRANS_SO_DOWNPAYMENT ) return s_TRANS_SALES_ORDER;
		if (iType == i_AR_TRANS_SALES_RETURN   ) return s_TRANS_SALES_RETURN;
		return "";
	}
	
	public static final String getTransactionScreen (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);
		if (iType == i_AR_TRANS_SO_DOWNPAYMENT ) return s_SCREEN_SALES_ORDER;
		if (iType == i_AR_TRANS_SALES_RETURN   ) return s_SCREEN_SALES_RETURN;		
		return "";
	}	
	
	public static List getByPaymentTransID (String _sPaymentID)
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add (CreditMemoPeer.PAYMENT_TRANS_ID, _sPaymentID);	    
	    return CreditMemoPeer.doSelect (oCrit);
	} 

	public static List getIDByPaymentTransID (String _sPaymentID, String _sDetID)
	    throws Exception
	{
		List vCM = getByPaymentTransID(_sPaymentID);
		List vID = new ArrayList(vCM.size());
		for(int i = 0; i < vCM.size(); i++)
		{
			CreditMemo oCM = (CreditMemo)vCM.get(i);
			if (StringUtil.isEqual(oCM.getPaymentInvId(), _sDetID))
			{
				vID.add(oCM.getCreditMemoId());
			}
		}
		return vID;
	} 
	
	public static CreditMemo getByFromPaymentID (String _sPaymentID)
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add (CreditMemoPeer.FROM_PAYMENT_ID, _sPaymentID);	
	    oCrit.add (CreditMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
	    List vCM = CreditMemoPeer.doSelect (oCrit);
	    if (vCM.size() > 0)
	    {
	    	return (CreditMemo)vCM.get(0);
	    }
	    return null;
	} 
		
	public static double getTotalAllocatedInPayment (String _sArPaymentID)
	    throws Exception
	{
	    double dTotal = 0;
	    List vData = getByPaymentTransID(_sArPaymentID);
        for (int i = 0; i < vData.size(); i++)
        {
            CreditMemo oMemo = (CreditMemo) vData.get(i);
            dTotal = dTotal + oMemo.getAmount().doubleValue();
        }
        return dTotal;
	} 
	
	public static String getStatusString (Integer _iStatus)
	{
		if (_iStatus != null)
		{
			if (_iStatus.intValue() == i_MEMO_CLOSED) 	  return LocaleTool.getString ("closed");
			if (_iStatus.intValue() == i_MEMO_CANCELLED)  return LocaleTool.getString ("cancelled");
			if (_iStatus.intValue() == i_MEMO_NEW)  	  return LocaleTool.getString ("pending");
		}
		return LocaleTool.getString ("open");
	}	

	public static List getByOrderID (String _sSourceID, int _iStatus, Connection _oConn)
			throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (CreditMemoPeer.TRANSACTION_ID, _sSourceID);	
		if (_iStatus > 0)
		{
			oCrit.add (CreditMemoPeer.STATUS, _iStatus);
		}
		else
		{
			oCrit.add (CreditMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
		}
		List vCM = CreditMemoPeer.doSelect (oCrit, _oConn);
		return vCM;
	}
	
	public static CreditMemo getBySourceTransID (String _sSourceID)
			throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (CreditMemoPeer.TRANSACTION_ID, _sSourceID);	
		oCrit.add (CreditMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
		List vCM = CreditMemoPeer.doSelect (oCrit);
		if (vCM.size() > 0)
		{
			return (CreditMemo)vCM.get(0);
		}
		return null;
	} 			
}