package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.ArPaymentDetail;
import com.ssti.enterprise.pos.om.ArPaymentDetailPeer;
import com.ssti.enterprise.pos.om.ArPaymentPeer;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.BaseJournalTool;
import com.ssti.enterprise.pos.tools.journal.ReceivablePaymentJournalTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ReceivablePaymentTool.java,v 1.20 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: ReceivablePaymentTool.java,v $
 * Revision 1.20  2009/05/04 02:04:13  albert
 * *** empty log message ***
 *
 * Revision 1.19  2008/06/29 07:12:27  albert
 * *** empty log message ***
 *
 * Revision 1.18  2008/04/14 09:16:19  albert
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/03 03:04:26  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ReceivablePaymentTool extends BaseTool
{
	private static Log log = LogFactory.getLog(ReceivablePaymentTool.class);
    
	public static final String s_INV_NO = new StringBuilder(ArPaymentPeer.AR_PAYMENT_ID).append(",")
		.append(ArPaymentDetailPeer.AR_PAYMENT_ID).append(",")
		.append(ArPaymentDetailPeer.INVOICE_NO).toString();

	protected static Map m_FIND_PEER = new HashMap();
	
    static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), ArPaymentPeer.AR_PAYMENT_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), ArPaymentPeer.REMARK);          
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER	  ), ArPaymentPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CUST_TYPE  ), ArPaymentPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), ArPaymentPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), ArPaymentPeer.REFERENCE_NO);             	
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), ArPaymentPeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), ArPaymentPeer.BANK_ID);	
        m_FIND_PEER.put (Integer.valueOf(i_ISSUER	  ), ArPaymentPeer.BANK_ISSUER);	        
        m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), ArPaymentPeer.LOCATION_ID);	   
        m_FIND_PEER.put (Integer.valueOf(i_CFT 		  ), ArPaymentPeer.CASH_FLOW_TYPE_ID);	           
        m_FIND_PEER.put (Integer.valueOf(i_INV_NO     ), s_INV_NO);	   
	}   

	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);} 	
	
	static ReceivablePaymentTool instance = null;
	
	public static synchronized ReceivablePaymentTool getInstance() 
	{
		if (instance == null) instance = new ReceivablePaymentTool();
		return instance;
	}
	
	public static ArPayment getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID(_sID, null);
	}
	
	public static ArPayment getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ArPaymentPeer.AR_PAYMENT_ID, _sID);
        List vData = ArPaymentPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (ArPayment) vData.get(0);
		}
		return null;
	}

	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ArPaymentDetailPeer.AR_PAYMENT_ID, _sID);
		return ArPaymentDetailPeer.doSelect(oCrit);
	}

	public static List getArPayments(List _vID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.addIn(ArPaymentPeer.AR_PAYMENT_ID, _vID);
        return ArPaymentPeer.doSelect(oCrit);
	}
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	private static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ArPaymentDetailPeer.AR_PAYMENT_ID, _sID);
        ArPaymentDetailPeer.doDelete (oCrit, _oConn);
	}
	
	
	/**
	 * check whether a purchase invoice is exist in any OPEN or CLOSED ArPayment
	 * not included if ArPayment is CANCELLED 
	 * 
	 * @param _sSIID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static boolean isInvoicePaid(String _sSIID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ArPaymentPeer.STATUS, i_PAYMENT_CANCELLED, Criteria.NOT_EQUAL);
		oCrit.addJoin(ArPaymentPeer.AR_PAYMENT_ID,ArPaymentDetailPeer.AR_PAYMENT_ID);
	    oCrit.add(ArPaymentDetailPeer.SALES_TRANSACTION_ID, _sSIID);
		List vData = ArPaymentDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}	
	
	///////////////////////////////////////////////////////////////////////////
	//trans processing
	///////////////////////////////////////////////////////////////////////////

	public static void updateDetail (ArPayment _oAR, List _vARD, RunData data) 
		throws Exception
	{

		ValueParser formData = data.getParameters();
		try
		{
			for (int i = 0; i < _vARD.size(); i++)
			{
				ArPaymentDetail oTD = (ArPaymentDetail) _vARD.get(i);				
				//check null to prevent refresh from user that didn't submit param from data					
				int iNo = i + 1;
				if (formData.getString("PaymentAmount" + iNo) != null)
				{				
					BigDecimal bdPmt = formData.getBigDecimal("PaymentAmount" + iNo);
					if (bdPmt != null) 
					{
						double dPmt = bdPmt.doubleValue();
						double dToPay = oTD.getSalesTransactionAmount().doubleValue() - oTD.getPaidAmount().doubleValue();
						if (dPmt > dToPay) //validate payment input by user
						{
							oTD.setPaymentAmount(new BigDecimal(dToPay));
							data.setMessage(oTD.getInvoiceNo() + " Payment Amount > (Total - Paid)");
						}
						else 
						{
							oTD.setPaymentAmount (bdPmt);
						}
					}
				}
				//log.debug ("After update : "  + oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Update Detail Failed : " + _oEx.getMessage (), _oEx); 
		}
	}
	
	public static void setHeaderProperties (ArPayment _oAR, List _vARD, RunData data) 
    	throws Exception
    {
		try 
		{
			if (data != null)
			{
				data.getParameters().setProperties (_oAR);
				_oAR.setArPaymentDate   (CustomParser.parseDate(data.getParameters().getString("ArPaymentDate")));
				_oAR.setArPaymentDueDate(CustomParser.parseDate(data.getParameters().getString("ArPaymentDueDate")));
				_oAR.setTotalDownPayment(data.getParameters().getBigDecimal("TotalDownPayment"));
				_oAR.setCustomerName(CustomerTool.getCustomerNameByID(data.getParameters().getString("CustomerId")));
				_oAR.setPaymentAmount(data.getParameters().getBigDecimal("ReceiveAmount"));
			}
			double dTotalDiscount = countTotalDiscount(_vARD);
			double dTotalAmount = countTotalPaymentAmount(_vARD);

			_oAR.setTotalDiscount (new BigDecimal(dTotalDiscount));
			_oAR.setTotalAmount (new BigDecimal(dTotalAmount - dTotalDiscount));
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (),_oEx); 
		}
    }
	
    private static double countTotalPaymentAmount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			ArPaymentDetail oDet = (ArPaymentDetail) _vDetails.get(i);
			dAmt += oDet.getPaymentAmount().doubleValue();
		}
		return dAmt;
	}
    
    private static double countTotalDiscount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			ArPaymentDetail oDet = (ArPaymentDetail) _vDetails.get(i);
			dAmt += oDet.getDiscountAmount().doubleValue();
		}
		return dAmt;
	}    
    
    private static void validateTrans (ArPayment _oAR, List _vARD, List _vCM, Connection _oConn)
    	throws Exception
    {
    	//validate currency
    	Bank oBank = BankTool.getBankByID(_oAR.getBankId(), _oConn);
    	Customer oCustomer = CustomerTool.getCustomerByID(_oAR.getCustomerId(), _oConn);
    	Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
    	
    	if (!oBankCurr.getIsDefault())
    	{
    		if (!oBankCurr.getCurrencyId().equals(oCustomer.getDefaultCurrencyId()))
    		{
    			throw new NestableException ("Invalid Cash/Bank Selection");
    		} 
    	}   
    	
		for (int i = 0; i < _vARD.size(); i++) 
		{
			ArPaymentDetail oDet = (ArPaymentDetail) _vARD.get(i);
			if (StringUtil.isNotEmpty(oDet.getSalesTransactionId()))
			{
				SalesTransaction oTR = TransactionTool.getHeaderByID(oDet.getSalesTransactionId(), _oConn);
				//validate status
				if (oTR.getStatus() != i_PROCESSED)
				{
					throw new NestableException ("Sales Invoice " + oTR.getInvoiceNo() +
						" status is invalid (" + TransactionTool.getStatusString(oTR.getStatus()) + ")");
				}
			}			
		}
		
		//validate CM
		double dTotalMemo = 0;
		log.debug("vCM : " + _vCM);
		for(int i = 0; i < _vCM.size(); i++)
		{
			List vData = (List)_vCM.get(i);
			log.debug("vData : " + vData);
			for(int j = 0;j < vData.size(); j++)
			{      
				String sID = (String)vData.get(j);
				CreditMemo oMemo = CreditMemoTool.getCreditMemoByID(sID, _oConn);
				if (oMemo != null)
				{
					log.debug("Memo j : " + oMemo);
					if (oMemo.getStatus() != i_MEMO_OPEN)
					{
						throw new NestableException ("Credit Memo " + oMemo.getCreditMemoNo() +
								" status is invalid (" + CreditMemoTool.getStatusString(oMemo.getStatus()) + ")");
					}
					else
					{
						dTotalMemo += oMemo.getAmount().doubleValue();
					}
				}
				else
				{
					throw new NestableException ("Invalid / not found Credit Memo");
				}
			}   
		}

		log.debug("Total Memo " + dTotalMemo + " Total DP " + _oAR.getTotalDownPayment().doubleValue());
		if (dTotalMemo != _oAR.getTotalDownPayment().doubleValue())
		{
			double dDelta = dTotalMemo - _oAR.getTotalDownPayment().doubleValue();
			if (dDelta < 0) dDelta = dDelta * -1;
			String sMsg = "Total Memo Amount (" + CustomFormatter.fmt(dTotalMemo) + 
						") != Total Down Payment (" + CustomFormatter.fmt(_oAR.getTotalDownPayment()) + ")";
			log.info(sMsg);
			if (dDelta > bd_ONE.doubleValue())
			{
				throw new NestableException (sMsg);
			}
		}
    }
	
    public static boolean isTemproraryPayment(ArPayment _oAR)
    {
		if (DateUtil.isAfter(_oAR.getDueDate(), new Date()))
		{
			return true;
		}
		return false;
    }
    
	/**
	 * save ar payment
	 * 
	 * @param _oARP
	 * @param _vARD
	 * @param _vCreditMemo
	 * @param data
	 * @throws Exception
	 */
	public static void saveData (ArPayment _oARP, List _vARD, List _vCreditMemo)
		throws Exception
	{
		Connection oConn = beginTrans();
		boolean bNew = false;
		try 
		{
			if(StringUtil.isEmpty(_oARP.getArPaymentId())) bNew = true;		
			
			//validate date
			validateDate(_oARP.getArPaymentDate(), oConn);

			//validate ar payment
			validateTrans(_oARP, _vARD, _vCreditMemo, oConn);
			
			//process save ar payment Data
			processSaveARPaymentData (_oARP, _vARD, oConn);
			
			//update debit memo TEMP for pending
			CreditMemoTool.savePendingPayment(_vCreditMemo, _oARP, _vARD, oConn);			
			
			if (_oARP.getStatus() == i_PROCESSED)
			{
				//create ar entry
				AccountReceivableTool.createAREntryFromARPayment(_oARP,oConn);
				
				//update credit memo
				CreditMemoTool.closeCM(_vCreditMemo, _oARP, oConn);
				
				boolean bIsFuture = isFutureDueDate(_oARP);
				if (bIsFuture && StringUtil.isNotEmpty(GLConfigTool.getGLConfig().getArPaymentTemp()))
				{
					//create temprorary account journal (only if pending payment)
					ReceivablePaymentJournalTool oJournal = new ReceivablePaymentJournalTool(_oARP, _vARD, oConn);
					oJournal.createJournal();
				}

				//create cash flow entry						
				CashFlow oCF = CashFlowTool.createFromARPayment(_oARP, _vARD, bIsFuture, oConn);
				
				//update ARPayment
				_oARP.setCfStatus(oCF.getStatus());
				_oARP.setCashFlowId(oCF.getCashFlowId());
				_oARP.save(oConn);
			}
			commit (oConn);
		}
		catch (Exception _oEx) 
		{
			if (_oARP.getStatus() == i_PROCESSED)
			{
				_oARP.setStatus(i_PENDING);
				_oARP.setArPaymentNo("");
			}
			if (bNew)
			{
				_oARP.setArPaymentId("");				
			}
			rollback (oConn);
			_oEx.printStackTrace();
		    log.error(_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	//save data processor
	private static void processSaveARPaymentData (ArPayment _oARP, List _vARD, Connection _oConn)
		throws Exception
	{
		if (StringUtil.isEmpty(_oARP.getArPaymentId()))
		{
			_oARP.setArPaymentId (IDGenerator.generateSysID());
			_oARP.setNew (true);
			validateID(getHeaderByID(_oARP.getArPaymentId(), _oConn), "arPaymentNo");
		}
		if (_oARP.getStatus() == i_PROCESSED)
		{
			String sLocCode = LocationTool.getLocationCodeByID(_oARP.getLocationId(), _oConn);
			_oARP.setArPaymentNo (LastNumberTool.get(s_RP_FORMAT, LastNumberTool.i_AR_PAYMENT, sLocCode, _oConn));
			validateNo(ArPaymentPeer.AR_PAYMENT_NO,_oARP.getArPaymentNo(),ArPaymentPeer.class, _oConn);
		}		
		_oARP.save (_oConn);

		deleteDetailsByID (_oARP.getArPaymentId(), _oConn);
		
		for ( int i = 0; i < _vARD.size(); i++ )
		{
			ArPaymentDetail oRPD = (ArPaymentDetail) _vARD.get(i);
			
			oRPD.setArPaymentDetailId ( IDGenerator.generateSysID() );
			oRPD.setArPaymentId   	  ( _oARP.getArPaymentId());
			oRPD.setNew (true);
			oRPD.save(_oConn);
			
			if(_oARP.getStatus() == i_PROCESSED && 
				(oRPD.getPaymentAmount().doubleValue() > 0 || oRPD.getDownPayment().doubleValue() > 0))
			{	
				TransactionTool.updatePaidAmount(oRPD.getSalesTransactionId(), 
												 _oARP.getArPaymentDate(),
				                                 oRPD.getPaymentAmount().doubleValue(), 
				                                 oRPD.getDownPayment().doubleValue(), 
												 _oConn);
			}
		}
	}

	/**
	 * cancel receivable payment transaction
	 * 
	 * @param _oAR
	 * @param _vARD
	 * @param data
	 * @throws Exception
	 */
	public static void cancelTransaction (ArPayment _oAR, List _vARD, String _sCancelBy)
		throws Exception
	{
		Connection oConn = beginTrans();
		try 
		{
			//validate date
			validateDate(_oAR.getArPaymentDate(), oConn);
			
			//rollback ar entry
			AccountReceivableTool.rollbackAR(_oAR.getArPaymentId(), oConn);
			
			//rollback credit memo
			CreditMemoTool.rollbackCMFromARPayment(_oAR, oConn);
		
			//cancel cash flow entry						
			CashFlowTool.cancelCashFlowByTransID (_oAR.getArPaymentId(), _sCancelBy, oConn);
			
			//rollback any GL Transaction
			BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_AR_PAYMENT, _oAR.getArPaymentId(), oConn);
			
			//process save ar payment Data
			processCancel (_oAR, _vARD, _sCancelBy, oConn);
	
			commit (oConn );
		}
		catch (Exception _oEx) 
		{
			rollback (oConn);
			_oEx.printStackTrace();
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}
	
	//process cancel data 
	private static void processCancel (ArPayment _oRP, List _vARD, String _sCancelBy, Connection _oConn)
		throws Exception
	{			
		for ( int i = 0; i < _vARD.size(); i++ )
		{
			ArPaymentDetail oRPD = (ArPaymentDetail) _vARD.get(i);
			double dPmtAmt = oRPD.getPaymentAmount().doubleValue();
			double dDPAmt = oRPD.getDownPayment().doubleValue();
			double dInvAmt = oRPD.getSalesTransactionAmount().doubleValue();
			if(dPmtAmt > 0 || dDPAmt > 0)
			{									
				double dPmtCancel = dPmtAmt * -1;
				double dDPCancel = dDPAmt * -1;				
				if(dPmtAmt < 0 && dDPAmt > dInvAmt) //special case for DP > INV
				{
					dPmtCancel = dInvAmt * - 1; //rollback from invoice amount
					dDPCancel = 0;						
				}				
				TransactionTool.updatePaidAmount(oRPD.getSalesTransactionId(), 
												 _oRP.getArPaymentDate(),
												 dPmtCancel, 
				                                 dDPCancel, 
												 _oConn);
			}
		}
		//Generate Sys ID
		_oRP.setRemark(cancelledBy(_oRP.getRemark(), _sCancelBy));
		_oRP.setStatus(i_PAYMENT_CANCELLED);
		_oRP.setCfStatus(i_PAYMENT_CANCELLED);
		_oRP.save(_oConn);
	}
	
	/**
	 * check whether ArPayment due date is in the future 
	 * 
	 * @param _oARP
	 * @return
	 */
	private static boolean isFutureDueDate (ArPayment _oARP)
	{
		if (_oARP != null)
		{
			if (DateUtil.isAfter(_oARP.getDueDate(), _oARP.getTransactionDate()))
			{
				return true;			
			}
		}
		return false;
	}
	
	///////////////////////////////////////////////////////////////////////////
	//finder
	///////////////////////////////////////////////////////////////////////////
	
	private static Criteria buildCriteria(int _iCond, 
			   							  String _sKeywords, 
			   							  String _sEntityID, 
			   							  String _sLocationID,
			   							  Date _dStart, 
			   							  Date _dEnd, 
			   							  Date _dStartDue, 
			   							  Date _dEndDue,
			   							  int _iStatus,
			   							  int _iCFStatus,
			   							  String _sCurrencyID) 
		throws Exception
	{
		Criteria oCrit =  buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			ArPaymentPeer.AR_PAYMENT_DATE, _dStart, _dEnd, _sCurrencyID);
		
		if (_dStartDue != null && _dEndDue != null)
		{
			oCrit.add(ArPaymentPeer.AR_PAYMENT_DUE_DATE, DateUtil.getStartOfDayDate(_dStartDue), Criteria.GREATER_EQUAL);
			oCrit.and(ArPaymentPeer.AR_PAYMENT_DUE_DATE, DateUtil.getEndOfDayDate(_dEndDue), Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sEntityID))
		{
			oCrit.add(ArPaymentPeer.CUSTOMER_ID, _sEntityID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(ArPaymentPeer.LOCATION_ID, _sLocationID);
		}		
		if (_iStatus > 0)
		{
			oCrit.add(ArPaymentPeer.STATUS, _iStatus);
		}
		if (_iCFStatus > 0)
		{
			oCrit.add(ArPaymentPeer.CF_STATUS, _iCFStatus);
		}
		return oCrit;
	}
	
	public static LargeSelect findData(int _iCond, 
					   				   String _sKeywords, 
			                           String _sEntityID, 
			                           String _sLocationID,
			                           Date _dStart, 
			                           Date _dEnd, 
			                           Date _dStartDue, 
			                           Date _dEndDue,
			                           int _iLimit,
			                           int _iStatus,
			                           int _iCFStatus,
			                           String _sCurrencyID)
    	throws Exception
    {
		Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sEntityID, _sLocationID, _dStart, _dEnd, 
										_dStartDue, _dEndDue, _iStatus, _iCFStatus, _sCurrencyID);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.ArPaymentPeer");
	}
	
	public static List findTrans(int _iCond, 
				   			     String _sKeywords, 
				   			     String _sEntityID, 
				   			     String _sLocationID,
				   			     Date _dStart, 
				   			     Date _dEnd, 
				   			     Date _dStartDue, 
				   			     Date _dEndDue,
				   			     int _iStatus,
				   			     int _iCFStatus,
				   			     String _sCurrencyID)
	throws Exception
	{
		Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sEntityID, _sLocationID, _dStart, _dEnd, 
									   _dStartDue, _dEndDue, _iStatus, _iCFStatus, _sCurrencyID);
		return ArPaymentPeer.doSelect(oCrit);
	}

	public static List findTrans(String _sCustomerID, 
							     String _sBankID, 
								 String _sLocationID,
								 String _sCashFlowTypeID,
							     Date _dFrom, 
								 Date _dTo, 
								 int _iStatus,
								 int _iCFStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		
		if (_dFrom != null)
		{
			oCrit.add(ArPaymentPeer.AR_PAYMENT_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		}
		if (_dTo != null)
		{
			oCrit.and(ArPaymentPeer.AR_PAYMENT_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sCustomerID))
		{
			oCrit.add(ArPaymentPeer.CUSTOMER_ID, _sCustomerID);
		}
		if (StringUtil.isNotEmpty(_sBankID))
		{
			oCrit.add(ArPaymentPeer.BANK_ID, _sBankID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(ArPaymentPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sCashFlowTypeID))
		{
			oCrit.add(ArPaymentPeer.CASH_FLOW_TYPE_ID, _sCashFlowTypeID);
		}
		if (_iStatus > 0)
		{
			oCrit.add(ArPaymentPeer.STATUS, _iStatus);			
		}
		else
		{
			//oCrit.add(ArPaymentPeer.STATUS, i_PROCESSED);
		}
		if (_iCFStatus > 0)
		{
			oCrit.add(ArPaymentPeer.CF_STATUS, _iCFStatus);			
		}
		
		//oCrit.add(ArPaymentPeer.STATUS, i_PROCESSED);
		return ArPaymentPeer.doSelect(oCrit);
	}

    //Report methods
	public static List getCustomerList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			ArPayment oTrans = (ArPayment) _vData.get(i);			
			if (!isCustomerInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isCustomerInList (List _vTrans, ArPayment _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			ArPayment oTrans = (ArPayment) _vTrans.get(i);			
			if (oTrans.getCustomerId().equals(_oTrans.getCustomerId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByCustomerID (List _vTrans, String _sCustomerID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		ArPayment oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (ArPayment ) oIter.next();
			if (!oTrans.getCustomerId().equals(_sCustomerID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}	

	public static List getBankList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			ArPayment oTrans = (ArPayment) _vData.get(i);			
			if (!isBankInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isBankInList (List _vTrans, ArPayment _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			ArPayment oTrans = (ArPayment) _vTrans.get(i);			
			if (oTrans.getBankId().equals(_oTrans.getBankId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByBankID (List _vTrans, String _sBankID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		ArPayment oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (ArPayment ) oIter.next();
			if (!oTrans.getBankId().equals(_sBankID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}		

	public static String getStatusString (Integer _iStatus)
	{
		if (_iStatus != null)
		{
			if (_iStatus.intValue() == i_TRANS_PROCESSED) return LocaleTool.getString ("processed");
			if (_iStatus.intValue() == i_TRANS_CANCELLED) return LocaleTool.getString ("cancelled");
		}
		return LocaleTool.getString ("open");
	}	
	
	public static String getDetailTransIDString (List _vARD)
	{
		StringBuilder oSB = new StringBuilder();
		for (int i = 0; i < _vARD.size(); i++)
		{
			ArPaymentDetail oARD = (ArPaymentDetail) _vARD.get(i);
			oSB.append(oARD.getSalesTransactionId());
			if (i != _vARD.size() - 1) oSB.append(",");
		}
		return oSB.toString();
	}

    //find by SI NO
    public static List getBySIID(String _sSIID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.addJoin(ArPaymentPeer.AR_PAYMENT_ID, ArPaymentDetailPeer.AR_PAYMENT_ID);     
        oCrit.add(ArPaymentDetailPeer.SALES_TRANSACTION_ID, _sSIID);
        oCrit.add(ArPaymentPeer.STATUS, i_TRANS_PROCESSED);
        return ArPaymentPeer.doSelect(oCrit);  
    }

    //find by SI NO
    public static boolean isInvoiceInPending(String _sSIID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.addJoin(ArPaymentPeer.AR_PAYMENT_ID, ArPaymentDetailPeer.AR_PAYMENT_ID);     
        oCrit.add(ArPaymentDetailPeer.SALES_TRANSACTION_ID, _sSIID);
        oCrit.add(ArPaymentPeer.STATUS, i_TRANS_PENDING);
        List vPending = ArPaymentDetailPeer.doSelect(oCrit);  
        if (vPending.size() > 0)
        {
        	return true;
        }
        return false;
    }

    //next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(ArPaymentPeer.class, ArPaymentPeer.AR_PAYMENT_ID, _sID, _bIsNext);
	}			
	
    /**
     * get AR Payment created in store
     * 
     * @return AR Payment created in store since last store 2 ho
     * @throws Exception
     */
    public static List getStoreTrans()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
        if (dLastSyncDate  != null) {
            oCrit.add(ArPaymentPeer.AR_PAYMENT_DUE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
        }
        oCrit.add(ArPaymentPeer.LOCATION_ID, PreferenceTool.getLocationID());       
        oCrit.add(ArPaymentPeer.STATUS, i_PROCESSED);
        return ArPaymentPeer.doSelect(oCrit);
    }
}