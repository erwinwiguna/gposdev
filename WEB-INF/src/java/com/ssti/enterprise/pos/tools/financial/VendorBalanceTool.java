package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.om.AccountPayable;
import com.ssti.enterprise.pos.om.VendorBalance;
import com.ssti.enterprise.pos.om.VendorBalancePeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.BeanUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: VendorBalanceTool.java,v 1.13 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: VendorBalanceTool.java,v $
 * 
 * 2015-12-18
 * - add method getBalances with parameter List of id
 * - add getBalanceAsOf with passing List so prevent query foreach customer
 * </pre><br>
 */
public class VendorBalanceTool extends BaseTool 
{
	public static VendorBalance getVendorBalance(String _sID)
		throws Exception
	{
		return VendorManager.getInstance().getVendorBalance(_sID, null);
	}

	/**
	 * get balance
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static VendorBalance getVendorBalance(String _sID, Connection _oConn)
		throws Exception
	{
		if (_oConn != null) //if conn is not null then do not get from cache
		{
			Criteria oCrit = new Criteria();
			oCrit.add(VendorBalancePeer.VENDOR_ID, _sID);
			List vData = VendorBalancePeer.doSelect(oCrit, _oConn);
			if(vData.size() > 0)
			{
				VendorBalance oBalance = (VendorBalance) vData.get(0);
				return oBalance;
			}
			return null;
		}
		else
		{
			return VendorManager.getInstance().getVendorBalance(_sID, null);
		}
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getBalances(List _vID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addIn(VendorBalancePeer.VENDOR_ID,  _vID);
		oCrit.addAscendingOrderByColumn(VendorBalancePeer.VENDOR_NAME);
		List vData = VendorBalancePeer.doSelect(oCrit, _oConn);
		return vData;
	}	
	
	/**
	 * get balance for update
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return VendorBalance for update
	 * @throws Exception
	 */
	private static VendorBalance getForUpdate(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(VendorBalancePeer.VENDOR_ID, _sID);
		oCrit.setForUpdate(true);
		List vData = VendorBalancePeer.doSelect(oCrit, _oConn);
		if(vData.size() > 0)
		{
			VendorBalance oBalance = (VendorBalance) vData.get(0);
			return oBalance;
		}
		return null;		
	}	
	
	/**
	 * 
	 * @param _sVendorID
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getBalance(String _sVendorID)
		throws Exception
	{
		VendorBalance oVendorBalance = getVendorBalance (_sVendorID);
		if (oVendorBalance != null) {
			return oVendorBalance.getApBalance();
		}
		return bd_ZERO;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getOweToVendor()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(VendorBalancePeer.AP_BALANCE, 0, Criteria.GREATER_EQUAL);
		oCrit.addAscendingOrderByColumn(VendorBalancePeer.VENDOR_NAME);
		return VendorBalancePeer.doSelect(oCrit);
	}


	/**
	 * update vendor balance
	 * 
	 * @param _oAP
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateBalance (AccountPayable _oOldAP, AccountPayable _oAP, Connection _oConn)
    	throws Exception
    {
		if (_oOldAP != null) rollbackBalance (_oOldAP, _oConn);
		updateBalance (_oAP, _oConn);
	}	
	
	/**
	 * update Vendor balance
	 * 
	 * @param _oAP
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateBalance (AccountPayable _oAP, Connection _oConn)
    	throws Exception
    {		
		double dAmount = 0;
		double dAmountBase = 0;
		
		int iType = _oAP.getTransactionType();
		
		if (iType == i_AP_TRANS_OPENING_BALANCE || 
		     iType == i_AP_TRANS_PURCHASE_RECEIPT ||
		     iType == i_AP_TRANS_PURCHASE_INVOICE ||
		     iType == i_AP_TRANS_PI_FREIGHT)
        {
			dAmount = _oAP.getAmount().doubleValue();
            dAmountBase = _oAP.getAmountBase().doubleValue();
        }
        else 
        {
        	dAmount = _oAP.getAmount().doubleValue() * -1;
            dAmountBase = _oAP.getAmountBase().doubleValue() * -1;        
        }
		setBalance (_oAP, dAmount, dAmountBase, _oConn);
	}

	/**
	 * rollback Vendor balance
	 * 
	 * @param _oAP
	 * @param _oConn
	 * @throws Exception
	 */
	public static void rollbackBalance (AccountPayable _oAP, Connection _oConn)
    	throws Exception
    {		
		double dAmount = 0;
		double dAmountBase = 0;
		
		int iType = _oAP.getTransactionType();
		
		if (iType == i_AP_TRANS_OPENING_BALANCE || 
		    iType == i_AP_TRANS_PURCHASE_RECEIPT ||
		    iType == i_AP_TRANS_PURCHASE_INVOICE)
        {
			dAmount = _oAP.getAmount().doubleValue() * -1;
            dAmountBase = _oAP.getAmountBase().doubleValue() * -1;
        }
        else 
        {
        	dAmount = _oAP.getAmount().doubleValue();
            dAmountBase = _oAP.getAmountBase().doubleValue();        
        }
		setBalance (_oAP, dAmount, dAmountBase, _oConn);
	}	
	
	private static void setBalance (AccountPayable _oAP, 
									double _dAmount, 
									double _dAmountBase, 
									Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		
		double dPrimeBalance = 0;
		double dBalance = 0;
		
		VendorBalance oBalance = getForUpdate(_oAP.getVendorId(), _oConn);
		if(oBalance != null)
		{
			dPrimeBalance = oBalance.getPrimeBalance().doubleValue() + _dAmount;
			dBalance = oBalance.getApBalance().doubleValue() + _dAmountBase;
			oBalance.setPrimeBalance(new BigDecimal (dPrimeBalance));	
			oBalance.setApBalance(new BigDecimal (dBalance));
        	oBalance.save(_oConn);
		}
		else
		{
			oBalance = new VendorBalance();
			oBalance.setVendorBalanceId (IDGenerator.generateSysID());
			oBalance.setVendorId		(_oAP.getVendorId	());
	    	oBalance.setVendorName	    (_oAP.getVendorName	());
	    	oBalance.setPrimeBalance	(new BigDecimal (_dAmount));	
	    	oBalance.setApBalance		(new BigDecimal (_dAmountBase));
	    	oBalance.save(_oConn);
	    }
		VendorManager.getInstance().refreshCache(oBalance);
	}
	
	/**
	 * 
	 * @param _sVendorID
	 * @param _dAsOf
	 * @return
	 * @throws Exception
	 */
	public static double[] getBalanceAsOf(String _sVendorID, Date _dAsOf)
		throws Exception
	{
	    List vData = AccountPayableTool.getTransactionHistory (_dAsOf, null, _sVendorID, "", -1);
	    double[] dBalance = new double[2];
	    VendorBalance oBalance = getVendorBalance (_sVendorID);
	    if (oBalance != null)
	    {
	        dBalance[i_BASE]  = oBalance.getApBalance().doubleValue();
	        dBalance[i_PRIME] = oBalance.getPrimeBalance().doubleValue();
	    }
        double[] dDebit  = AccountPayableTool.filterTotalAmountByType (vData, 1);
	    double[] dCredit = AccountPayableTool.filterTotalAmountByType (vData, 2);
	    
	    dBalance[i_BASE]  = dBalance[i_BASE]  - dDebit[i_BASE]  + dCredit[i_BASE];
	    dBalance[i_PRIME] = dBalance[i_PRIME] - dDebit[i_PRIME] + dCredit[i_PRIME];
		
	    return dBalance;
	}
	
	/**
	 * get balance as of
	 * 
	 * @param _sVendorID
	 * @param _sCurrencyID
	 * 
	 * @return
	 * @throws Exception
	 */
	public static double[] getBalanceAsOf(List _vData, String _sVendorID)
		throws Exception
	{
		double[] dBalance = new double[2];
		if(_vData != null)
		{
		    List vData = BeanUtil.filterListByFieldValue(_vData, "customerId", _sVendorID);
		    VendorBalance oBalance = getVendorBalance (_sVendorID);
		    if (oBalance != null)
		    {
		        dBalance[i_BASE]  = oBalance.getApBalance().doubleValue();
		        dBalance[i_PRIME] = oBalance.getPrimeBalance().doubleValue();
		    
		    }
	        double[] dDebit  = AccountReceivableTool.filterTotalAmountByType (vData, 1);
		    double[] dCredit = AccountReceivableTool.filterTotalAmountByType (vData, 2);
		    
		    dBalance[i_BASE]  = dBalance[i_BASE]  - dDebit[i_BASE]  + dCredit[i_BASE];
		    dBalance[i_PRIME] = dBalance[i_PRIME] - dDebit[i_PRIME] + dCredit[i_PRIME];
		}
		return dBalance;
	}	
}
