package com.ssti.enterprise.pos.tools.gl;

import org.apache.commons.lang.exception.NestableException;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/gl/AccountInvalidException.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: AccountInvalidException.java,v 1.1 2005/06/17 10:26:23 albert Exp $
 *
 * $Log: AccountInvalidException.java,v $
 * Revision 1.1  2005/06/17 10:26:23  albert
 * *** empty log message ***
 *
 */

public class AccountInvalidException extends NestableException {

	public AccountInvalidException (String _sError)
	{
		super (_sError);
	}

	public AccountInvalidException (String _sError, Throwable cause)
	{
		super (_sError, cause);
	}
	
	public AccountInvalidException (Throwable cause)
	{
		super (cause);
	}
	
}
