package com.ssti.enterprise.pos.tools.gl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.AccountReport;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountPeer;
import com.ssti.enterprise.pos.om.GlTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for GlTransaction OM
 * the OM is presenting a transaction that add into debit or credit balance of an account
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GlTransactionTool.java,v 1.28 2009/05/04 02:04:20 albert Exp $ <br>
 *
 * <pre>
 * $Log: GlTransactionTool.java,v $
 * Revision 1.28  2009/05/04 02:04:20  albert
 * *** empty log message ***
 *
 * Revision 1.27  2008/08/17 02:20:47  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class GLReportTool extends BaseTool implements GlAttributes
{
	private static Log log = LogFactory.getLog(GLReportTool.class);
	
	static GLReportTool instance = null;
	
	public static GLReportTool getInstance() 
	{
		return new GLReportTool(); //not static instance
	}	
	
	//all account current balance
	private List vCurrent = null;
	private List vChanges = null;
	private Map mChanges = null;
		
	/**
	 * get current balance by sum all transaction from Preference Start Date / Beginning Balance Date until now
	 * 
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @param _sSiteID
	 * @return
	 * @throws Exception
	 */
	private AccountReport getCurrentBalance (String _sAccountID, String _sSubType, String _sSubID, String _sSiteID)
		throws Exception
	{
		Account oAccount = AccountTool.getAccountByID(_sAccountID);
		Date dNow = new Date();
		Date dBegin = PreferenceTool.getStartDate(); //use the preference, because account as of can be different between account
		if (vCurrent == null)
		{			
			//get all account current balance
			vCurrent = getChanges("",_sSubType,_sSubID,_sSiteID, dBegin, dNow);			
		}
		AccountReport oAcc = filterByAccountID(_sAccountID, vCurrent);
		return oAcc;
	}	

	public double getBalance(String _sAccountID, Date _dAsOf, boolean _bBase)
		throws Exception
	{
		double[] dBalance = getBalance(_sAccountID, "", "", "", _dAsOf);
		if (_bBase) return dBalance[i_BASE];
		return dBalance[i_PRIME];
	}
	
	public double getBalance(String _sAccountID, String _sSubType, String _sSubID, Date _dAsOf, boolean _bBase)
		throws Exception
	{
		double[] dBalance = getBalance(_sAccountID, _sSubType, _sSubID, "", _dAsOf);
		if (_bBase) return dBalance[i_BASE];
		return dBalance[i_PRIME];
	}
	
	public double getBalance(String _sAccountID, String _sSubType, String _sSubID, String _sSiteID, Date _dAsOf, boolean _bBase)
		throws Exception
	{
		double[] dBalance = getBalance(_sAccountID, _sSubType, _sSubID, _sSiteID, _dAsOf);
		if (_bBase) return dBalance[i_BASE];
		return dBalance[i_PRIME];
	}
	
	/**
	 * get account as of balance or total of as of balance for parent account
	 * 
	 * @param _sAccountID
	 * @param _sSubType (dept/prj/loc)
	 * @param _sSubID (dept_id/prj_id/loc_id)
	 * @param _sSiteID
	 * @param _dAsOf 
	 * @return array of base balance [1] & prime balance [2]
	 * @throws Exception
	 */
	public double[] getBalance (String _sAccountID, String _sSubType, String _sSubID, String _sSiteID, Date _dAsOf) 
		throws Exception
	{
		String sAsOf = CustomFormatter.formatDate(_dAsOf);
        Account oAccount = AccountTool.getAccountByID(_sAccountID);
        
        double[] dTotalBalance = new double[2];
        dTotalBalance[i_BASE] = 0;
        dTotalBalance[i_PRIME] = 0;
        
        if (mChanges == null)
        {
        	mChanges = new HashMap();
        }
    	vChanges = (List)mChanges.get(sAsOf);
		if (vChanges == null)
		{
			//get all account total changes from as of 
			vChanges = getChanges("",_sSubType,_sSubID,_sSiteID, _dAsOf, null);
			mChanges.put(sAsOf, vChanges);
		}        	        
		
		List vAccount = new ArrayList(1);
        if (oAccount != null)
        {
        	if (oAccount.getHasChild()) 
        	{
        		vAccount = AccountTool.getAccountTreeList (null, _sAccountID);
        	}
        	else 
        	{
        		vAccount.add(oAccount);
        	}             	
        	for (int i = 0; i < vAccount.size(); i++)
        	{
        		Account oAcc = (Account) vAccount.get(i);
        		String sAccountID = oAcc.getAccountId();
        		
        		//current balance
        		double dCurrentBase = 0;
        		double dCurrentPrime = 0;        		
        		AccountReport oCurrentBal = getCurrentBalance(sAccountID, _sSubType, _sSubID, _sSiteID);
    			if (oCurrentBal != null)
    			{
    				dCurrentBase = oCurrentBal.getBaseBalance();
    				dCurrentPrime = oCurrentBal.getPrimeBalance();
    			}
        		
        		//transaction changes
        		double dTransBase = 0;
        		double dTransPrime = 0;        	
        		AccountReport oTransBal = filterByAccountID(sAccountID, vChanges);
       			if (oTransBal != null)
    			{
            		dTransBase = oTransBal.getBaseChanges();
            		dTransPrime = oTransBal.getPrimeChanges();        	
    			}
        		if (log.isDebugEnabled() && !oAccount.getHasChild())
        		{
        			log.debug ("Account: " + oAcc.getAccountCode() + " - " + oAcc.getAccountName());
        			log.debug ("As Of  : " + CustomFormatter.formatDateTime(_dAsOf)); 
        			log.debug(logMsg("Current: ", dCurrentBase, dCurrentPrime));
        			log.debug(logMsg("Trans  : ", dTransBase, dTransPrime));
        			log.debug(logMsg("Balance: ", (dCurrentBase + dTransBase), (dCurrentPrime+ dTransPrime)));
        		}
        		dTotalBalance[i_BASE]  = dTotalBalance[i_BASE]  + dCurrentBase  + dTransBase;             
        		dTotalBalance[i_PRIME] = dTotalBalance[i_PRIME] + dCurrentPrime + dTransPrime;             
        	}
        }
	    return dTotalBalance;
	}	

	/**
	 * 
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @param _sSiteID
	 * @param _dFrom
	 * @param _dTo
	 * @param _bBase
	 * @return
	 * @throws Exception
	 */
	public double getRangeBalance(String _sAccountID, String _sSubType, String _sSubID, String _sSiteID, Date _dFrom, Date _dTo, boolean _bBase)
		throws Exception
	{
		double dFrom = getBalance(_sAccountID, _sSubType, _sSubID, _sSiteID, _dFrom, _bBase);
		double dTo = getBalance(_sAccountID, _sSubType, _sSubID, _sSiteID, _dTo, _bBase);
		return (dTo - dFrom);
	}
	
	/**
	 * get sum of transaction amount in gl transaction 
	 * 
	 * @param _sAccountID
	 * @param _sSubType ("", "dept", "prj", "loc")
	 * @param _sSubID 
	 * @param _sSiteID
	 * @param _dFrom
	 * @param _dTo
	 * @return List of AccountReport
	 * @throws Exception
	 */
	public static List getChanges (String _sAccountID,
			   					   String _sSubType,
			   					   String _sSubID, 
								   String _sSiteID,
								   Date _dFrom, 
								   Date _dTo) 
		throws Exception
	{         
		//count debit gl transaction 
		StringBuilder oSQL = new StringBuilder();
		oSQL.append (" SELECT g.account_id AS account_id, ");
		oSQL.append (" SUM(CASE WHEN debit_credit = 1 THEN amount_base ELSE 0 END) AS total_debit_base, ");		
		oSQL.append (" SUM(CASE WHEN debit_credit = 2 THEN amount_base ELSE 0 END) AS total_credit_base, ");		
		oSQL.append (" SUM(CASE WHEN debit_credit = 1 THEN amount ELSE 0 END) AS total_debit, ");
		oSQL.append (" SUM(CASE WHEN debit_credit = 2 THEN amount ELSE 0 END) AS total_credit ");
		oSQL.append (" FROM gl_transaction g ");
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append (" LEFT OUTER JOIN location l ON g.location_id = l.location_id ");
		}
		oSQL.append (" WHERE g.account_id != ''"); 
		
		if (StringUtil.isNotEmpty(_sAccountID))
		{
			oSQL.append(" g.account_id = '").append (_sAccountID).append ("'");
		}
		if (_dFrom != null)
		{
			oSQL.append (" AND g.transaction_date >= ");
			oSQL.append (oDB.getDateString(DateUtil.getStartOfDayDate(_dFrom)));
		}
		if (_dTo != null)
		{
			oSQL.append (" AND g.transaction_date <= ");
			oSQL.append (oDB.getDateString(DateUtil.getEndOfDayDate(_dTo)));
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append(" AND l.site_id = '").append(_sSiteID).append("' ");
		}
		if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID))
		{
			
			oSQL.append (" AND g.LOCATION_ID = '");
			oSQL.append (_sSubID);
			oSQL.append ("' ");
		}		
		oSQL.append(" GROUP BY g.account_id");        		
		log.debug (" Get Changes SQL: \n" + oSQL.toString());
		
	    List vData = GlTransactionPeer.executeQuery(oSQL.toString());	    
	    return recordToObj(vData,_sSiteID,_sSubType,_sSubID);
	}		  		 
	
	/**
	 * map query result Record to AccountReport Object
	 * 
	 * @param _vData
	 * @param _sSiteID
	 * @param _sSubType
	 * @param _sSubID
	 * @return
	 * @throws Exception
	 */
	private static List recordToObj(List _vData, String _sSiteID, String _sSubType, String _sSubID)
		throws Exception
	{
		if (_vData != null)
		{
			List vResult = new ArrayList(_vData.size());
			for (int i = 0; i < _vData.size(); i++)
			{
				Record oRec = (Record) _vData.get(i);
				String sAccID = oRec.getValue("account_id").asString();
				Account oAcc = AccountTool.getAccountByID(sAccID);
				if (oAcc != null)
				{
					AccountReport oData = new AccountReport(oAcc);
					oData.setTotalDebit(oRec.getValue("total_debit").asDouble());
					oData.setTotalDebitBase(oRec.getValue("total_debit_base").asDouble());
					oData.setTotalCredit(oRec.getValue("total_credit").asDouble());
					oData.setTotalCreditBase(oRec.getValue("total_credit_base").asDouble());
					oData.setSiteId(_sSiteID);
	
					if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID))
					{						
						oData.setLocationId(_sSubID);						
					}
					//total of transaction changes
					double dBaseChg = 0;
					double dPrimeChg = 0;
					
					//balance amount (only for CURRENT BALANCE)
					double dBaseBal = 0;
					double dPrimeBal = 0;					
				    if (oData.getNormalBalance() == i_DEBIT)
				    {
				    	dBaseChg = dBaseChg - oData.getTotalDebitBase() + oData.getTotalCreditBase();
				    	dPrimeChg = dPrimeChg - oData.getTotalDebit() + oData.getTotalCredit();
						dBaseBal = oData.getTotalDebitBase() - oData.getTotalCreditBase();
						dPrimeBal = oData.getTotalDebit() - oData.getTotalCredit();
				    }
				    else if (oData.getNormalBalance() == i_CREDIT)
				    {
				    	dBaseChg = dBaseChg - oData.getTotalCreditBase() + oData.getTotalDebitBase();
				    	dPrimeChg = dPrimeChg - oData.getTotalCredit() + oData.getTotalDebit();
						dBaseBal = oData.getTotalCreditBase() - oData.getTotalDebitBase();
						dPrimeBal = oData.getTotalCredit() - oData.getTotalDebit();
				    }
				    oData.setBaseChanges(dBaseChg);
				    oData.setPrimeChanges(dPrimeChg);
				    oData.setBaseBalance(dBaseBal);
				    oData.setPrimeBalance(dPrimeBal);
					vResult.add(oData);
				}
			}
			return vResult;
		}
		return null;
	}
	
	private AccountReport filterByAccountID(String _sAccID, List _vAcc)
		throws Exception
	{
		if (_vAcc != null)
		{
			for (int i = 0; i < _vAcc.size(); i++)
			{
				AccountReport oData = (AccountReport) _vAcc.get(i);
				if (StringUtil.isEqual(_sAccID,oData.getAccountId()))
				{
					return oData;
				}
			}
		}
		return null;
	}
	
	//-------------------------------------------------------------------------
	// TRIAL BALANCE REPORT
	//-------------------------------------------------------------------------
	
	List vTrialBalance = null;
	/**
	 * 
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @param _sSiteID
	 * @param _dFrom
	 * @param _dTo
	 * @param _iDC
	 * @return
	 * @throws Exception
	 */
	public double[] getChangesAmount(String _sAccountID, String _sSubType, String _sSubID, String _sSiteID, Date _dFrom, Date _dTo, int _iDC) 
		throws Exception
	{
	    Account oAccount = AccountTool.getAccountByID(_sAccountID);
	    
	    double[] dResult = new double[2];
	    dResult[i_BASE] = 0;
	    dResult[i_PRIME] = 0;
	    
		if (vTrialBalance == null)
		{
			vTrialBalance = getChanges("",_sSubType,_sSubID,_sSiteID, _dFrom, _dTo);
		}        	
	    
	    if (oAccount != null)
	    {
	    	List vAccount = new ArrayList(1);
	    	if (oAccount.getHasChild()) 
	    	{
	    		vAccount = AccountTool.getAccountTreeList (null, _sAccountID);
	    	}
	    	else 
	    	{
	    		vAccount.add(oAccount);
	    	}
	    	for (int i = 0; i < vAccount.size(); i++)
	    	{
	    		Account oAcc = (Account) vAccount.get(i);
	    		String sAccountID = oAcc.getAccountId();
	    		
	    		//transaction changes
	    		double dDebitPrime = 0;
	    		double dDebitBase = 0;	    		
	    		double dCreditPrime = 0;
	    		double dCreditBase = 0;
	    		
	    		AccountReport oTransBal = filterByAccountID(sAccountID, vTrialBalance);
	   			if (oTransBal != null)
				{
		    		dDebitPrime = oTransBal.getTotalDebit();
		    		dDebitBase = oTransBal.getTotalDebitBase();	    		
		    		dCreditPrime = oTransBal.getTotalCredit();
		    		dCreditBase = oTransBal.getTotalCreditBase();
				}
	    		if (log.isDebugEnabled() && !oAccount.getHasChild() && _iDC == i_DEBIT)
	    		{
	    			log.debug("Account: " + oAcc.getAccountCode() + " - " + oAcc.getAccountName());
	    			log.debug(logMsg("Debit :  ", dDebitBase,  dDebitPrime));
	    			log.debug(logMsg("Credit:  ", dCreditBase, dCreditPrime)); 
	    		}
	    		if (_iDC == i_DEBIT)
	    		{
	    		    dResult[i_BASE]  = dResult[i_BASE]  + dDebitBase;
	    		    dResult[i_PRIME] = dResult[i_PRIME] + dDebitPrime;
	    		}
	    		else if (_iDC == i_CREDIT)
	    		{
	    		    dResult[i_BASE]  = dResult[i_BASE]  + dCreditBase;
	    		    dResult[i_PRIME] = dResult[i_PRIME] + dCreditPrime;
	    		}
	    	}
	    }
	    return dResult;
	}	
	
	static String logMsg(String _sHeader, Number _dBase, Number _dPrime)
	{
		StringBuilder logMsg = new StringBuilder();
		logMsg.append(_sHeader)
			  .append("Base: ").append(StringUtil.right(CustomFormatter.formatAligned(_dBase),20))
			  .append("  Prime: ").append(StringUtil.right(CustomFormatter.formatAligned(_dPrime),20));
		return logMsg.toString();
	}
	
	public static double calculatePL(String _sSubType,
									 String _sSubID,
									 String _sSiteID,
									 Date _dFrom,
									 Date _dTo,
									 boolean _bFromBS)
	{    	
    	try
    	{
    		Criteria oCrit = new Criteria();
        	if (_bFromBS)
        	{
        		oCrit.add(AccountPeer.ACCOUNT_TYPE,10,Criteria.LESS_EQUAL);            		
        	}
        	else
        	{
        		oCrit.add(AccountPeer.ACCOUNT_TYPE,11,Criteria.GREATER_EQUAL);        
        	}
        	oCrit.add(AccountPeer.HAS_CHILD, false);
        	
    		double dTotalDbt = 0;
    		double dTotalCrd = 0;    		
			List vAcc = AccountPeer.doSelect(oCrit);
			GLReportTool oTool = new GLReportTool();
			for (int i = 0; i < vAcc.size(); i++) 
			{
				Account oAcc = (Account)vAcc.get(i);
				double[] dDbt = oTool.getChangesAmount(oAcc.getAccountId(),_sSubType,_sSubID,_sSiteID,_dFrom,_dTo,i_DEBIT);
				double[] dCrd = oTool.getChangesAmount(oAcc.getAccountId(),_sSubType,_sSubID,_sSiteID,_dFrom,_dTo,i_CREDIT);
				dTotalDbt += dDbt[i_BASE];
				dTotalCrd += dCrd[i_BASE];				
			}
			double dPL = dTotalCrd - dTotalDbt;
			if (_bFromBS) dPL = dTotalDbt - dTotalCrd;
			return dPL;
    	} 
    	catch (Exception e) 
		{
			e.printStackTrace();
		}
    	return 0;
	}
    
    /**
     * get Accumulated Account Balance by Alt CODE
     * 
     * @param _sAltCode
     * @param _dFrom
     * @param _dTo
     * @return balance amount
     * @throws Exception
     */
	public double getBalanceByAltCode (String _sAltCode, Date _dFrom, Date _dTo) 
        throws Exception
    {        
        double dTotal = 0;
        if (StringUtil.isNotEmpty(_sAltCode))
        {
            if(_dFrom == null) _dFrom = PreferenceTool.getStartDate();
            if (_dTo == null) _dTo = new Date();            
            List vAlt = AccountTool.getByAltCode(_sAltCode);
            for (int i = 0; i < vAlt.size(); i++)
            {                
                Account oAcc = (Account) vAlt.get(i);
                double dBal = getRangeBalance(oAcc.getAccountId(),"","","",_dFrom,_dTo,true);
                dTotal += dBal; 

                log.debug("CODE: " + _sAltCode + " ACC: " + oAcc.getAccountCode() + " BAL: " + CustomFormatter.fmt(dBal));
            }
        }
        return dTotal;
    }  
}