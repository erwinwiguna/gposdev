package com.ssti.enterprise.pos.tools.gl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountPeer;
import com.ssti.enterprise.pos.om.AccountTypePeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.GlTransactionPeer;
import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for GlTransaction OM
 * the OM is presenting a transaction that add into debit or credit balance of an account
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GlTransactionTool.java,v 1.28 2009/05/04 02:04:20 albert Exp $ <br>
 *
 * <pre>
 * $Log: GlTransactionTool.java,v $
 * 
 * 2016-03-01
 * - Add method addTransScreen (GlTransaction glt) to set correct transaction screen for PP/RP with memo
 * 
 * 2015-06-29
 * - Add method validateExists to check same ID posting with different no 
 *
 * </pre><br>
 */
public class GlTransactionTool extends BaseTool implements GlAttributes
{
	private static Log log = LogFactory.getLog(GlTransactionTool.class);
	
	static GlTransactionTool instance = null;
	
	public static synchronized GlTransactionTool getInstance() 
	{
		if (instance == null) instance = new GlTransactionTool();
		return instance;
	}

	public static void deleteDataByAccountID (String _sAccountID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GlTransactionPeer.ACCOUNT_ID, _sAccountID);
		GlTransactionPeer.doDelete (oCrit);
	}	  	

	public static List getDataByTypeAndTransID (int _iType, String _sTransID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType);
		oCrit.add (GlTransactionPeer.TRANSACTION_ID, _sTransID);
		return GlTransactionPeer.doSelect (oCrit, _oConn);
	}

    public static List getDataByTypeAndTransID (int _iType, String _sTransID, String _sTransNo, Connection _oConn) 
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType);
        oCrit.add (GlTransactionPeer.TRANSACTION_ID, _sTransID);
        oCrit.add (GlTransactionPeer.TRANSACTION_NO, _sTransNo);        
        return GlTransactionPeer.doSelect (oCrit, _oConn);
    }
    
	/**
	 * get GL Transaction by Type, TransID, AccountID
	 * 
	 * @param _iType
	 * @param _sTransID
	 * @param _sAccountID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static GlTransaction getByTransAndAccountID (int _iType, 
														String _sTransID, 
														String _sAccountID, 
														String _sLocationID,
														Connection _oConn) 
		throws Exception
	{
		return getByTransAndAccountID(_iType,-1,_sTransID,_sAccountID,_sLocationID,_oConn);
	}
	
	/**
	 * 
	 * @param _iType
	 * @param _iDebitCredit
	 * @param _sTransID
	 * @param _sAccountID
	 * @param _sLocationID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static GlTransaction getByTransAndAccountID (int _iType, 
														int _iDebitCredit,
														String _sTransID, 
														String _sAccountID, 
														String _sLocationID,														
														Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType);
		oCrit.add (GlTransactionPeer.TRANSACTION_ID, _sTransID);
		oCrit.add (GlTransactionPeer.ACCOUNT_ID, _sAccountID);
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add (GlTransactionPeer.LOCATION_ID, _sLocationID);			
		}
		if (_iDebitCredit == i_DEBIT || _iDebitCredit == i_CREDIT)
		{
			oCrit.add (GlTransactionPeer.DEBIT_CREDIT, _iDebitCredit);						
		}
		List vData = GlTransactionPeer.doSelect (oCrit, _oConn);
		
		//TODO: Warning this query not always return 1 record, validate usage again
		if (vData.size() > 0)
		{
			if (vData.size() > 1)
			{
				log.warn("getByTransAndAccountID probably return invalid data");
			}
			return (GlTransaction) vData.get(0);
		}
		return null;
	}	
	
	/**
	 * 
	 * @param _iType
	 * @param _iGLType
	 * @param _sAccountID
	 * @param _sProjectID
	 * @param _sDepartmentID
	 * @param _sTransNo
	 * @param _sAccNo
	 * @param _dFrom
	 * @param _dTo
	 * @return List of GlTransaction
	 * @throws Exception
	 */
	public static List findData (int _iType, 
	                             int _iGLType, 
	                             String _sAccountID,
	                             String _sSubType,	                             
								 String _sSubID,
								 String _sTransNo,
								 String _sAccNo,
	                             Date _dFrom, 
	                             Date _dTo)
		throws Exception
	{
		return findData (_iType,_iGLType,_sAccountID,_sSubType,_sSubID,_sTransNo,_sAccNo,_dFrom,_dTo,-1,"");
	}
	
	/**
	 * 
	 * @param _iType
	 * @param _iGLType
	 * @param _sAccountID
	 * @param _sProjectID
	 * @param _sDepartmentID
	 * @param _sTransNo
	 * @param _sAccNo
	 * @param _dFrom
	 * @param _dTo
	 * @return List of GlTransaction
	 * @throws Exception
	 */
	public static List findData (int _iType, 
	                             int _iGLType, 
	                             String _sAccountID,
	                             String _sSubType,	                             
								 String _sSubID,
								 String _sTransNo,
								 String _sAccNo,
	                             Date _dFrom, 
	                             Date _dTo,
								 int _iSubLedger,
								 String _sSubLedgerID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (_iType > 0)
		{
		    oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType);
		}
		if (_iGLType > 0)
		{
			oCrit.addJoin(GlTransactionPeer.ACCOUNT_ID, AccountPeer.ACCOUNT_ID);
		}
        if (_iGLType >= 1 && _iGLType <= 8)
        {
        	oCrit.addJoin(AccountPeer.ACCOUNT_TYPE, AccountTypePeer.ACCOUNT_TYPE);
        	oCrit.add(AccountTypePeer.GL_TYPE, _iGLType);
        }
        else if (_iGLType == 9) //BS ONLY
        {
        	oCrit.add(AccountPeer.ACCOUNT_TYPE, 10, Criteria.LESS_EQUAL );
        }
        else if (_iGLType == 10) //PL ONLY
        {
        	oCrit.add(AccountPeer.ACCOUNT_TYPE, 11, Criteria.GREATER_EQUAL );        
        }
		
		if (StringUtil.isNotEmpty(_sAccountID)) 
		{
		    oCrit.add (GlTransactionPeer.ACCOUNT_ID, _sAccountID);
	    }
		if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID)) 
		{
			oCrit.add (GlTransactionPeer.LOCATION_ID, _sSubID);
		}		
		if (StringUtil.isNotEmpty(_sTransNo)) 
		{
		    oCrit.add (GlTransactionPeer.TRANSACTION_NO, (Object)_sTransNo, Criteria.LIKE);
		}		
		if (StringUtil.isNotEmpty(_sAccNo)) 
		{
		    oCrit.addJoin(GlTransactionPeer.ACCOUNT_ID, AccountPeer.ACCOUNT_ID);
		    oCrit.add(AccountPeer.ACCOUNT_CODE, (Object)_sAccNo, Criteria.LIKE);
		}		
		
		if (_dFrom != null)  oCrit.add (GlTransactionPeer.TRANSACTION_DATE, _dFrom, Criteria.GREATER_EQUAL);
		if (_dTo != null)  oCrit.and (GlTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		
		oCrit.addAscendingOrderByColumn(GlTransactionPeer.TRANSACTION_DATE);
		oCrit.addAscendingOrderByColumn(GlTransactionPeer.TRANSACTION_NO);
		
		if (_iSubLedger > 0)
		{
			oCrit.add(GlTransactionPeer.SUB_LEDGER_TYPE, _iSubLedger);			
		}
		if (StringUtil.isNotEmpty(_sSubLedgerID))
		{
			oCrit.add(GlTransactionPeer.SUB_LEDGER_ID, _sSubLedgerID);			
		}		

		log.debug (oCrit);
		return GlTransactionPeer.doSelect (oCrit);
	}	   
    
    /**
     * Check whether the current account exist in gl transaction tables
     *
	 * @param _sAccountID
	 * @param _iType
	 * @param _bIsEqual
	 * @return whether account and trans type is exists
	 * @throws Exception
	 */
	public static boolean isExistInTransaction (String _sAccountID, int _iType, boolean _bIsEqual) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GlTransactionPeer.ACCOUNT_ID, _sAccountID);
		if (_iType > 0)
		{
		    if (_bIsEqual) 
		    {
		        oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType);
	        }
	        else 
	        {
		        oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType, Criteria.NOT_EQUAL);
	        }
	    }
	    oCrit.setLimit (1);
	    if (log.isDebugEnabled())
	    {
	    	log.debug("Is " + AccountTool.getCodeByID(_sAccountID) + " Exist In Transaction Query " + oCrit);
	    }
	    List vData = GlTransactionPeer.doSelect (oCrit);
		if (vData.size() > 0)
		{
	        return true;
	    }
	    return false;
	}	    	

	/**
	 * get accumulated changes of list of account ID
	 * used by Trial Balance Report
	 * 
	 * @param _vAccount
	 * @param _sSubType
	 * @param _sSubID
	 * @param _dFrom
	 * @param _dTo
	 * @param _iType
	 * @return array of base changes [1] & prime changes [2]
	 * @throws Exception
	 */
	public static double[] getChanges (List _vAccount, 
								       String _sSubType,
									   String _sSubID, 
									   Date _dFrom, 
									   Date _dTo, 
									   int _iType) 
		throws Exception
	{         
	    double[] dChanges = new double[2];
	    for (int i = 0; i < _vAccount.size(); i++)
	    {
	    	Account oAcc = (Account)_vAccount.get(i);
	    	if (!oAcc.getHasChild()) //skip parent account
	    	{
	    		double[] dTMP = getChanges(oAcc.getAccountId(), _sSubType, _sSubID, _dFrom, _dTo, _iType);
	    		dChanges[i_BASE]  += dTMP[i_BASE];
	    		dChanges[i_PRIME] += dTMP[i_PRIME];
	    	}
	    }
		return dChanges;
	}

	/**
	 * get account changes in gl transaction 
	 * 
	 * @param _sAccountID
	 * @param _sSubType ("", "dept", "prj", "loc")
	 * @param _sSubID 
	 * @param _dFrom
	 * @param _dTo
	 * @param _iType DEBIT OR CREDIT
	 * @return array of base changes [1] & prime changes [2]
	 * @throws Exception
	 */
	public static double[] getChanges (String _sAccountID,
									   String _sSubType,
									   String _sSubID,
									   Date _dFrom, 
									   Date _dTo, 
									   int _iType) 
		throws Exception
	{         
		//count debit gl transaction 
		StringBuilder oSQL = new StringBuilder();
		oSQL.append (" SELECT SUM(gtrans.amount_base), SUM(gtrans.amount) ");
		oSQL.append (" FROM gl_transaction gtrans ");
		oSQL.append (" WHERE gtrans.account_id = '").append (_sAccountID).append ("'");
		oSQL.append  ("AND gtrans.debit_credit =  ").append(_iType);		
		if (_dFrom != null)
		{
			oSQL.append (" AND gtrans.TRANSACTION_DATE >= ");
			oSQL.append (oDB.getDateString(DateUtil.getStartOfDayDate(_dFrom)));
		}
		if (_dTo != null)
		{
			oSQL.append (" AND gtrans.TRANSACTION_DATE <= ");
			oSQL.append (oDB.getDateString(DateUtil.getEndOfDayDate(_dTo)));
		}
		if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID))
		{			
			oSQL.append (" AND gtrans.LOCATION_ID = '");			
			oSQL.append (_sSubID);
			oSQL.append ("' ");
		}
		oSQL.append(" GROUP BY gtrans.account_id");        
	    log.debug (oSQL.toString());
	    
	    List vChanges = GlTransactionPeer.executeQuery(oSQL.toString());
	    double dChanges[] = new double[2];
	    if (vChanges.size() > 0)
	    {
	        Record oData = (Record)vChanges.get(0);
	        dChanges[i_BASE] =  oData.getValue(1).asDouble();            
	        dChanges[i_PRIME] =  oData.getValue(2).asDouble();            
	    }
	    return dChanges;
	}		  		 
	
	/**
	 * get Total amount changes since certain date
	 * 
	 * @param _sAccountID
	 * @param _sSubType ("", "dept", "prj", "loc")
	 * @param _sSubID
	 * @param _dFromDate
	 * @return array of base total changes [1] & prime total changes [2]
	 * @throws Exception
	 */
	public static double[] getTotalChanges (String _sAccountID, 
										    String _sSubType,
										    String _sSubID, 
										    Date _dFromDate) 
    	throws Exception
    {         			
        double[] dTotalDebit = getChanges (_sAccountID, _sSubType, _sSubID, _dFromDate, null, i_DEBIT);
        double[] dTotalCredit = getChanges (_sAccountID,_sSubType, _sSubID, _dFromDate, null, i_CREDIT);        
        double[] dTotal = new double[2];
        
        Account oAccount = AccountTool.getAccountByID (_sAccountID);
        if (oAccount.getNormalBalance() == i_DEBIT)
        {
            dTotal[i_BASE]  = dTotal[i_BASE]  - dTotalDebit[i_BASE]  + dTotalCredit[i_BASE];
            dTotal[i_PRIME] = dTotal[i_PRIME] - dTotalDebit[i_PRIME] + dTotalCredit[i_PRIME];
        }
        else if (oAccount.getNormalBalance() == i_CREDIT)
        {
            dTotal[i_BASE]  = dTotal[i_BASE]  + dTotalDebit[i_BASE]  - dTotalCredit[i_BASE];
            dTotal[i_PRIME] = dTotal[i_PRIME] + dTotalDebit[i_PRIME] - dTotalCredit[i_PRIME];
        }
        return dTotal;
	}		
	
    public static final String getTransType (int _iType)
    {
        if (_iType == i_GL_TRANS_OPENING_BALANCE   ) { return s_TRANS_OPENING_BALANCE   ; }
        if (_iType == i_GL_TRANS_RECEIPT_UNPLANNED ) { return s_TRANS_RECEIPT_UNPLANNED ; }
        if (_iType == i_GL_TRANS_ISSUE_UNPLANNED   ) { return s_TRANS_ISSUE_UNPLANNED   ; }
        if (_iType == i_GL_TRANS_PURCHASE_RECEIPT  ) { return s_TRANS_PURCHASE_RECEIPT  ; }
        if (_iType == i_GL_TRANS_PURCHASE_INVOICE  ) { return s_TRANS_PURCHASE_INVOICE  ; }
        if (_iType == i_GL_TRANS_PURCHASE_RETURN   ) { return s_TRANS_PURCHASE_RETURN   ; }
        if (_iType == i_GL_TRANS_ITEM_TRANSFER 	   ) { return s_TRANS_ITEM_TRANSFER 	; } 
        if (_iType == i_GL_TRANS_DELIVERY_ORDER    ) { return s_TRANS_DELIVERY_ORDER    ; }
        if (_iType == i_GL_TRANS_SALES_INVOICE 	   ) { return s_TRANS_SALES_INVOICE 	; }
        if (_iType == i_GL_TRANS_SALES_RETURN 	   ) { return s_TRANS_SALES_RETURN 	    ; }
        if (_iType == i_GL_TRANS_FIXED_ASSET 	   ) { return s_TRANS_FIXED_ASSET 	    ; }
        if (_iType == i_GL_TRANS_JOURNAL_VOUCHER   ) { return s_TRANS_JOURNAL_VOUCHER   ; }
        if (_iType == i_GL_TRANS_AR_PAYMENT        ) { return s_TRANS_AR_PAYMENT        ; }
        if (_iType == i_GL_TRANS_AP_PAYMENT        ) { return s_TRANS_AP_PAYMENT        ; }
        if (_iType == i_GL_TRANS_CREDIT_MEMO       ) { return s_TRANS_AR_MEMO           ; }
        if (_iType == i_GL_TRANS_DEBIT_MEMO        ) { return s_TRANS_AP_MEMO           ; }
        if (_iType == i_GL_TRANS_CASH_MANAGEMENT   ) { return s_TRANS_CASH_MANAGEMENT   ; }
        if (_iType == i_GL_TRANS_JOB_COSTING       ) { return s_TRANS_JOB_COSTING       ; }
        if (_iType == i_GL_TRANS_JC_ROLLOVER       ) { return s_TRANS_JC_ROLLOVER       ; }
        return "";
    }

    public static final String getTransScreen (int _iType)
    {
        if (_iType == i_GL_TRANS_OPENING_BALANCE   ) { return s_SCREEN_JOURNAL_VOUCHER   ; }
        if (_iType == i_GL_TRANS_RECEIPT_UNPLANNED ) { return s_SCREEN_ISSUE_RECEIPT 	 ; }
        if (_iType == i_GL_TRANS_ISSUE_UNPLANNED   ) { return s_SCREEN_ISSUE_RECEIPT   	 ; }
        if (_iType == i_GL_TRANS_PURCHASE_RECEIPT  ) { return s_SCREEN_PURCHASE_RECEIPT  ; }
        if (_iType == i_GL_TRANS_PURCHASE_INVOICE  ) { return s_SCREEN_PURCHASE_INVOICE  ; }
        if (_iType == i_GL_TRANS_PURCHASE_RETURN   ) { return s_SCREEN_PURCHASE_RETURN   ; }
        if (_iType == i_GL_TRANS_ITEM_TRANSFER 	   ) { return s_SCREEN_ITEM_TRANSFER 	 ; } 
        if (_iType == i_GL_TRANS_DELIVERY_ORDER    ) { return s_SCREEN_DELIVERY_ORDER    ; }
        if (_iType == i_GL_TRANS_SALES_INVOICE 	   ) { return s_SCREEN_SALES_INVOICE	 ; }
        if (_iType == i_GL_TRANS_SALES_RETURN 	   ) { return s_SCREEN_SALES_RETURN 	 ; }
        if (_iType == i_GL_TRANS_FIXED_ASSET 	   ) { return s_SCREEN_FIXED_ASSET 	     ; }
        if (_iType == i_GL_TRANS_JOURNAL_VOUCHER   ) { return s_SCREEN_JOURNAL_VOUCHER   ; }
        if (_iType == i_GL_TRANS_AR_PAYMENT        ) { return s_SCREEN_CASH_MANAGEMENT   ; }
        if (_iType == i_GL_TRANS_AP_PAYMENT        ) { return s_SCREEN_CASH_MANAGEMENT   ; }
        if (_iType == i_GL_TRANS_CREDIT_MEMO       ) { return s_SCREEN_CASH_MANAGEMENT   ; }
        if (_iType == i_GL_TRANS_DEBIT_MEMO        ) { return s_SCREEN_CASH_MANAGEMENT   ; }
        if (_iType == i_GL_TRANS_CASH_MANAGEMENT   ) { return s_SCREEN_CASH_MANAGEMENT   ; }
        if (_iType == i_GL_TRANS_JOB_COSTING       ) { return s_SCREEN_JOB_COSTING       ; }
        if (_iType == i_GL_TRANS_JC_ROLLOVER       ) { return s_SCREEN_JC_ROLLOVER       ; }
        return "";
    }    
    
    public static final String getTransScreen (GlTransaction _oGLT)   
    {
    	if(_oGLT.getTransactionType() == i_GL_TRANS_AP_PAYMENT && _oGLT.getTransactionNo().startsWith(s_PP_FORMAT.substring(0, 2)))
    	{
    		return s_SCREEN_AP_PAYMENT;
    	}
    	if(_oGLT.getTransactionType() == i_GL_TRANS_AR_PAYMENT && _oGLT.getTransactionNo().startsWith(s_RP_FORMAT.substring(0, 2)))
    	{
    		return s_SCREEN_AR_PAYMENT;
    	}
    	return getTransScreen(_oGLT.getTransactionType());
    }
    
    //---------------------------------------------------------
	// journal voucher
	//---------------------------------------------------------      

	/**
	 * Create Journal from JournalVoucher
	 * 
	 * @param _oJV JournalVoucher
	 * @param _vJVD List of JournalVoucherDetail
	 * @param _oConn Connection
	 * @throws Exception
	 */
	public static void createJournalVoucherTrans (JournalVoucher _oJV,
	                                              List _vJVD,
	                                              Connection _oConn) 
	    throws Exception
	{
		validate(_oConn);		
        Period oPeriod = PeriodTool.getPeriodByDate(_oJV.getTransactionDate(), _oConn);        
        List vTrans = new ArrayList(_vJVD.size());
                
        for (int i = 0; i < _vJVD.size(); i++)
        {
            JournalVoucherDetail oJVD = (JournalVoucherDetail) _vJVD.get(i);
            
            GlTransaction oTrans = new GlTransaction();
            oTrans.setGlTransactionId  (IDGenerator.generateSysID());
            oTrans.setTransactionType  (i_GL_TRANS_JOURNAL_VOUCHER);
            oTrans.setTransactionId    (_oJV.getJournalVoucherId());
            oTrans.setTransactionNo    (_oJV.getTransactionNo());
            oTrans.setGlTransactionNo  (_oJV.getTransactionNo());
            oTrans.setTransactionDate  (_oJV.getTransactionDate());
            oTrans.setLocationId	   (oJVD.getLocationId());
            oTrans.setProjectId        (oJVD.getProjectId());
            oTrans.setDepartmentId     (oJVD.getDepartmentId());
            oTrans.setPeriodId         (oPeriod.getPeriodId());
            oTrans.setAccountId        (oJVD.getAccountId());
            oTrans.setCurrencyId       (oJVD.getCurrencyId());
            oTrans.setCurrencyRate     (oJVD.getCurrencyRate());
            oTrans.setAmount           (oJVD.getAmount());
            oTrans.setAmountBase       (oJVD.getAmountBase());
            oTrans.setDescription      (oJVD.getMemo());
            oTrans.setUserName         (_oJV.getUserName());
	        oTrans.setDebitCredit      (oJVD.getDebitCredit());
            oTrans.setCreateDate       (new Date());
            oTrans.setSubLedgerType	   (oJVD.getSubLedgerType());
            oTrans.setSubLedgerId	   (oJVD.getSubLedgerId());
            oTrans.setSubLedgerDesc	   (oJVD.getSubLedgerDesc());
            
            if (StringUtil.isEmpty(oTrans.getDescription()))
            {
            	oTrans.setDescription(_oJV.getDescription());	
            }
	        vTrans.add (oTrans);
        }
        saveGLTransaction (vTrans, _oConn);
	}		
	
	/**
	 * Create reverse journal from an existing transaction. Used to rollback
	 * existing journal entries. This method usually used to update journal 
	 * entry created by linked previous transaction
	 * 
	 * @param _iType TransactionType, the transaction we'd like to rollback
	 * @param _sTransID Transaction ID
	 * @param _sUserName UserName 
	 * @param _oConn Connection
	 * @throws Exception
	 */
	public static void createReverseJournal (int _iType,
											 String _sTransID,
											 String _sUserName,
	                                         Connection _oConn) 
	    throws Exception
	{
        List vTrans = getDataByTypeAndTransID(_iType, _sTransID, _oConn);
        String sReverseTransID = IDGenerator.generateSysID();
        for (int i = 0; i < vTrans.size(); i++)
        {            
            GlTransaction oTrans = (GlTransaction) vTrans.get(i);
            int iType = oTrans.getDebitCredit();
            
            //reverse the debit credit
            if (iType == i_DEBIT) iType = i_CREDIT;
            else if (iType == i_CREDIT) iType = i_DEBIT;
            
            oTrans.setGlTransactionId  (IDGenerator.generateSysID());
            oTrans.setTransactionType  (_iType);
            oTrans.setTransactionId    (sReverseTransID);
            oTrans.setDescription      ("Reverse : " + oTrans.getDescription());
            oTrans.setUserName         (_sUserName);
	        oTrans.setDebitCredit      (iType);
            oTrans.setCreateDate       (new Date());
            oTrans.setNew			   (true);
            oTrans.setModified		   (true);
	        vTrans.add                 (oTrans);
        }
        saveGLTransaction (vTrans, _oConn);
	}		
	
	//---------------------------------------------------------
	// other journal 
	//---------------------------------------------------------      
	
	public static double[] getTotalDebitCredit (List _vTrans, Connection _oConn)
		throws Exception
	{
		double[] dResult = new double[2];
		double dTotalDebit = 0;
		double dTotalCredit = 0;
		
		log.debug("GET TOTAL DEBIT CREDIT ");
		GlTransaction oTrans = null;
		for (int i = 0; i < _vTrans.size(); i++)
		{
			oTrans = (GlTransaction) _vTrans.get(i); 
			
			//check for reverse DC
			checkReverseDC(oTrans);
			
			if (log.isDebugEnabled())
			{
				Account oAcc = AccountTool.getAccountByID(oTrans.getAccountId(), _oConn);
				log.debug("ACCOUNT " + i + " : "  + oAcc.getAccountCode() + " " + oAcc.getAccountName());
			}
			if (oTrans.getDebitCredit() == i_DEBIT) 
			{
				log.debug("DEBIT " + i + " += " + oTrans.getAmount().doubleValue());            	
				log.debug("DEBIT (BASE)" + i + " += " + oTrans.getAmountBase().doubleValue());            	
				dTotalDebit += oTrans.getAmountBase().doubleValue();
			}
			else if(oTrans.getDebitCredit() == i_CREDIT) 
			{
				log.debug("CREDIT " + i + " += " + oTrans.getAmount().doubleValue());            	
				log.debug("CREDIT (BASE)" + i + " += " + oTrans.getAmountBase().doubleValue());            	
				dTotalCredit += oTrans.getAmountBase().doubleValue();
			}
		}
		dResult[0] = dTotalDebit;
		dResult[1] = dTotalCredit;
		return dResult;
	}
	
	public static void validateBalance (List _vTrans, Connection _oConn)
		throws Exception
	{
        log.debug("VALIDATE BALANCE ");
        double[] dTotalDC   = getTotalDebitCredit(_vTrans, _oConn);
		double dTotalDebit  = dTotalDC[0];
		double dTotalCredit = dTotalDC[1];
		double dUnbalanced  = 0; 

		if (dTotalDebit > dTotalCredit) dUnbalanced = dTotalDebit - dTotalCredit;
		if (dTotalDebit < dTotalCredit) dUnbalanced = dTotalCredit - dTotalDebit;		
		
		if (dUnbalanced != 0)
		{
			log.debug("Unbalance Journal : " + dUnbalanced);
		}
		if ((dUnbalanced != 0) && (dUnbalanced > d_UNBALANCE_TOLERANCE))
		{		
			StringBuilder oSB = new StringBuilder(s_LINE_SEPARATOR);
			for(int i = 0; i < _vTrans.size(); i++)
			{
				GlTransaction oGL = (GlTransaction) _vTrans.get(i);
				oSB.append(AccountTool.getCodeByID(oGL.getAccountId())).append(":")
									  .append(oGL.getDebitCredit()).append(":")
								      .append(CustomFormatter.fmt(oGL.getAmountBase()))
								      .append(s_LINE_SEPARATOR);				
			}
			log.error(oSB.toString());
			throw new Exception (
				"Unbalanced Journal, Total Debit : " + CustomFormatter.formatNumber(dTotalDebit) + 
				" Total Credit : " + CustomFormatter.formatNumber(dTotalCredit));
		}
	}

	/**
	 * validate whether existing transaction no with same ID exists
	 * 
	 * @param _oGL
	 * @param _oConn
	 * @throws Exception
	 */
	private static void validateExists(GlTransaction _oGL, Connection _oConn)
		throws Exception
	{
		if(_oGL != null)
		{
			List vExists = getDataByTypeAndTransID(_oGL.getTransactionType(), _oGL.getTransactionId(), _oConn);
			for(int i = 0; i < vExists.size(); i++)
			{
				GlTransaction oExist = (GlTransaction) vExists.get(i);
				if(!StringUtil.isEqual(oExist.getTransactionNo(), _oGL.getTransactionNo()))
				{
					throw new Exception("Invalid GL Trans No " + _oGL.getTransactionNo() + " Same ID Exists with No " + oExist.getTransactionNo());
				}
			}
		}
	}

	
	/**
	 * Save GlTransaction created by automatic journal helper
	 * 
	 * @param _vTrans List of GlTransaction
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveGLTransaction (List _vTrans, Connection _oConn) 
	    throws Exception
	{		
		validateBalance (_vTrans, _oConn);
	    for (int i = 0; i < _vTrans.size(); i++)
	    {
	    	GlTransaction oTrans = (GlTransaction) _vTrans.get(i);    
	    	validateExists(oTrans, _oConn);
		    if (StringUtil.isEmpty(oTrans.getGlTransactionId()))
		    {
		    	oTrans.setGlTransactionId  (IDGenerator.generateSysID());
		    }
	    	oTrans.save (_oConn);
	    }
	    AccountBalanceTool.updateBalance(_vTrans, _oConn);      
	}
	
	//-------------------------------------------------------------------------
	//UPDATE GL TRANS FROM TRANSACTION
	//  TODO: CREATE UPDATE LOG?
	//  CHECK FOR POSSIBLE UNBALANCE PROBLEM
	//  CHECK FOR PERIOD CLOSED
	//-------------------------------------------------------------------------
	/**
	 * update Sales GL Trans
	 * 
	 * @param _iGLType
	 * @param _sTransID
	 * @param _sTransNo
	 * @param _sAccountID
	 * @param _sAccPairID
	 * @param _dDiff
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateSalesGLTrans(int _iGLType, 
									      String _sTransID, 
									      String _sTransNo,
									      String _sAccountID, 
 									      String _sAccPairID, 
									      double _dDiff, 
									      Connection _oConn)
		throws Exception
	{
		GlTransaction oGL1 = getByTransAndAccountID(_iGLType,_sTransID,_sAccountID,"",_oConn);
		GlTransaction oGL2 = getByTransAndAccountID(_iGLType,_sTransID,_sAccPairID,"",_oConn);

		//both pair may not be null
		if (oGL1 != null && oGL2 != null)
		{
			//if oGL1 && oGL2 is Pair (is debit && credit)
			if (oGL1.getDebitCredit() != oGL2.getDebitCredit())
			{
				updateGLTrans(oGL1, oGL2, _dDiff, _oConn);		
			}
			else
			{
				logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);
			}
		}
		else
		{
			logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);	
		}
	}	

	/**
	 * update Issue RECEIPT GL TRANS
	 * Need special methods to get GL trans by including D/C parameter, 
	 * because Stock Opname / Item Exchanged Trans / Adjustment IR may 
	 * contains journal like this
	 * 
	 * INV 
	 *    ADJ 
	 * ADJ 
	 *    INV
	 *    
	 * @param _iGLType
	 * @param _iDC
	 * @param _sTransID
	 * @param _sTransNo
	 * @param _sAccountID
	 * @param _sAccPairID
	 * @param _sLocID
	 * @param _dDiff
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateIRGLTrans (int _iGLType, 
										int _iDC,
										String _sTransID, 
										String _sTransNo,
										String _sAccountID, 
										String _sAccPairID,
										String _sLocID,											  
										double _dDiff, 
										Connection _oConn)
	throws Exception
	{
		int iAdjDC = i_CREDIT;
		if (_iDC == i_CREDIT) iAdjDC = i_DEBIT;
		
		GlTransaction oGL1 = getByTransAndAccountID(_iGLType,_iDC,_sTransID,_sAccountID,_sLocID,_oConn);
		GlTransaction oGL2 = getByTransAndAccountID(_iGLType,iAdjDC,_sTransID,_sAccPairID,_sLocID,_oConn);

		//both pair may not be null
		if (oGL1 != null && oGL2 != null)
		{
			//if oGL1 && oGL2 is Pair (is debit && credit)
			if (oGL1.getDebitCredit() != oGL2.getDebitCredit())
			{
				updateGLTrans(oGL1, oGL2, _dDiff, _oConn);	
			}
			else
			{
				logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);
			}
		}
		else
		{
			logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);	
		}
	}	
	
	/**
	 * update transfer GL Trans, need special method to get GL trans by From & To locationID
	 * 
	 * @param _iGLType
	 * @param _sTransID
	 * @param _sTransNo
	 * @param _sAccountID
	 * @param _sFromID
	 * @param _sToID
	 * @param _dDiff
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateTransferGLTrans (int _iGLType, 
											  String _sTransID, 
											  String _sTransNo,
											  String _sAccountID, 
											  String _sFromID,
											  String _sToID,											  
											  double _dDiff, 
											  Connection _oConn)
	throws Exception
	{
		GlTransaction oGL1 = getByTransAndAccountID(_iGLType,_sTransID,_sAccountID,_sFromID,_oConn);
		GlTransaction oGL2 = getByTransAndAccountID(_iGLType,_sTransID,_sAccountID,_sToID,_oConn);
		
		//both pair may not be null
		if (oGL1 != null && oGL2 != null && oGL1.getDebitCredit() != oGL2.getDebitCredit())
		{
			updateGLTrans(oGL1, oGL2, _dDiff, _oConn);			
			
			//update transfer cross journal
			Location oFr = LocationTool.getLocationByID(_sFromID,_oConn);
			Location oTo = LocationTool.getLocationByID(_sToID,_oConn);
			if (oFr != null && oTo != null && 
				StringUtil.isNotEmpty(oFr.getTransferAccount()) && 
				StringUtil.isNotEmpty(oTo.getTransferAccount()))
			{
				GlTransaction oGL3 = getByTransAndAccountID(_iGLType,_sTransID,oTo.getTransferAccount(),_sFromID,_oConn);
				GlTransaction oGL4 = getByTransAndAccountID(_iGLType,_sTransID,oFr.getTransferAccount(),_sToID,_oConn);		
				if (oGL3 != null && oGL4 != null && oGL3.getDebitCredit() != oGL4.getDebitCredit())
				{
					updateGLTrans(oGL3, oGL4, _dDiff, _oConn);									
				}
				else
				{
					logUpdateError(_sTransNo, oTo.getTransferAccount(), oFr.getTransferAccount(), oGL3, oGL4, false, _oConn);						
				}
			}					
		}
		else
		{			
			logUpdateError(_sTransNo, _sAccountID, _sAccountID, oGL1, oGL2, false, _oConn);
		}
	}	
	
	private static void updateGLTrans (GlTransaction oGL1, 
			   						   GlTransaction oGL2,
			   						   double _dDiff,
			   						   Connection _oConn)
		throws Exception
	{
		if (oGL1 != null && oGL2 != null)
		{
			GlTransaction oOldGL1 = oGL1.copy();
			GlTransaction oOldGL2 = oGL2.copy();
			
			oGL1.setAmount(new BigDecimal(oGL1.getAmount().doubleValue() + _dDiff));
			oGL1.setAmountBase(
					new BigDecimal(oGL1.getAmount().doubleValue() * oGL1.getCurrencyRate().doubleValue())
			);
			
			oGL2.setAmount(new BigDecimal(oGL2.getAmount().doubleValue() + _dDiff));
			oGL2.setAmountBase(
					new BigDecimal(oGL2.getAmount().doubleValue() * oGL2.getCurrencyRate().doubleValue())
			);
			
			oGL1.save(_oConn);
			oGL2.save(_oConn);
			
			AccountBalanceTool.updateBalance(oOldGL1, oGL1, _oConn);		
			AccountBalanceTool.updateBalance(oOldGL2, oGL2, _oConn);
		}
	}
	
	private static void logUpdateError(String _sTransNo, 
									   String _sAccID, 
									   String _sPairID, 
									   GlTransaction oGL1, 
									   GlTransaction oGL2,
									   boolean _bThrowEx,
									   Connection _oConn)
		throws Exception
	{
		StringBuilder oErr = new StringBuilder();
		if (oGL1 == null || oGL2 == null)
		{
			Account oAcc1 = AccountTool.getAccountByID(_sAccID, _oConn);
			Account oAcc2 = AccountTool.getAccountByID(_sPairID, _oConn);
			oErr.append("Update GL Trans pair checking failed");		
			if (oGL1 == null)
			{
				oErr.append(" GL Trans 1 ").append(_sTransNo)
				    .append(" Account ").append(oAcc1.getAccountCode()).append(" not found");
			}
			if (oGL2 == null)
			{
				oErr.append(" GL Trans 2 ").append(_sTransNo)
				    .append(" Account ").append(oAcc2.getAccountCode()).append(" not found");			
			}
		}
		if (oGL1 != null && oGL2 != null)
		{
			if (oGL1.getDebitCredit() == oGL2.getDebitCredit())
			{
				Account oAcc1 = AccountTool.getAccountByID(oGL1.getAccountId(), _oConn);
				Account oAcc2 = AccountTool.getAccountByID(oGL2.getAccountId(), _oConn);
				
				oErr.append("Update GL Trans failed, Invalid Journal Debit/Credit")
				    .append(" Trans : ").append(oGL1.getTransactionNo()).append("\n")
					.append(oAcc1.getAccountCode()).append(" ").append(oAcc1.getAccountName())
					.append(oGL1.getAmount()).append(" ").append(oGL1.getDebitCredit()).append("\n")
					.append(oAcc2.getAccountCode()).append(" ").append(oAcc2.getAccountName())
					.append(oGL2.getAmount()).append(" ").append(oGL2.getDebitCredit());				
			}
		}		
		if(StringUtil.isNotEmpty(oErr.toString()) && _bThrowEx) throw new NestableException(oErr.toString());
		log.error(oErr);
	}
	
	//-------------------------------------------------------------------------
	// Utilities
	//-------------------------------------------------------------------------
	
	/**
	 * reverse debit credit, if debit return credit if credit return debit
	 * @param _iDebitCredit
	 * @return reversed debit credit
	 */
	public static int reverseDC (int _iDebitCredit)
	{
		if (_iDebitCredit == i_DEBIT) 
		{
			return i_CREDIT;
		}
		return i_DEBIT;			
	}

	/**
	 * validate and update GLTrans amount if minus then reverse debit/credit amount
	 * 
	 * @param GlTransaction to check
	 */
	public static void checkReverseDC (GlTransaction _oGL)
	{
		if (_oGL != null)
		{
			if (_oGL.getAmount().doubleValue() < 0 && _oGL.getAmountBase().doubleValue() < 0)
			{
				_oGL.setDebitCredit(reverseDC(_oGL.getDebitCredit()));
				_oGL.setAmount(new BigDecimal(_oGL.getAmount().doubleValue() * -1));
				_oGL.setAmountBase(new BigDecimal(_oGL.getAmountBase().doubleValue() * -1));
				
			}
		}
	}
}