package com.ssti.enterprise.pos.tools.gl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.joda.time.DateTime;

import com.ssti.enterprise.pos.model.SubLedgerOM;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.om.JournalVoucherDetailPeer;
import com.ssti.enterprise.pos.om.JournalVoucherPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.journal.BaseJournalTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for JournalVoucher OM
 * a journal voucher is a transaction entered manually by user which will create 
 * Gltransaction that affected and updated general ledger and account balance
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: JournalVoucherTool.java,v 1.23 2009/05/04 02:04:20 albert Exp $ <br>
 *
 * <pre>
 * $Log: JournalVoucherTool.java,v $
 *
 * 2016-02-19
 * Add validate rate if rate > 0 && rate != 1 in createForexAdj method
 * 
 * 2015-03-22
 * Add saveData (JV, List, Connection) to support create JV from other trans
 * 
 * 2015-06-15
 * Add method createClosePL => move PL balance to RE Account similar to end year but not 
 * limited to december
 * </pre><br>
 */
public class JournalVoucherTool extends BaseTool implements GlAttributes
{
    private static Log log = LogFactory.getLog ( JournalVoucherTool.class );

    public static int i_JV_NORMAL = 1;
    public static int i_JV_ACC_OB = 2;
    public static int i_JV_LOC_OB = 3;
    public static int i_JV_DEP_OB = 4;
    public static int i_JV_PRJ_OB = 5;
    public static int i_JV_END_MO_CURR = 6;
    public static int i_JV_END_MO_DEPR = 7;
    public static int i_JV_END_YEAR = 8;
    public static int i_JV_FA_REVAL = 9;    
    
	public static final String s_ACCOUNT = new StringBuilder(JournalVoucherPeer.JOURNAL_VOUCHER_ID).append(",")
	.append(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID).append(",")
	.append(JournalVoucherDetailPeer.ACCOUNT_ID).toString();

	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), JournalVoucherPeer.TRANSACTION_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), JournalVoucherPeer.DESCRIPTION);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), JournalVoucherPeer.USER_NAME);
		m_FIND_PEER.put (Integer.valueOf(i_ACCOUNT	  ), s_ACCOUNT);		
	}   
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static JournalVoucher getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID (_sID, null);
	}
	
    /**
     * 
     * @param _sID
     * @return
     * @throws Exception
     */
	public static JournalVoucher getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(JournalVoucherPeer.JOURNAL_VOUCHER_ID, _sID);
        List vData = JournalVoucherPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (JournalVoucher) vData.get(0);
		}
		return null;
	}

    /**
     * 
     * @param _sID
     * @return
     * @throws Exception
     */
	public static JournalVoucher getHeaderByNo(String _sNo, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(JournalVoucherPeer.TRANSACTION_NO, _sNo);
        List vData = JournalVoucherPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (JournalVoucher) vData.get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID)
		throws Exception
	{
		return getDetailsByID (_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, _sID);
		return JournalVoucherDetailPeer.doSelect(oCrit, _oConn);
	}
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static JournalVoucherDetail getDetailByDetailID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_DETAIL_ID, _sID);
        JournalVoucherDetail oJVD = null;
        List vJVD = JournalVoucherDetailPeer.doSelect(oCrit);
        if (vJVD.size() > 0)
        {
        	oJVD = (JournalVoucherDetail) vJVD.get(0);
        }
        return oJVD;
	}
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static void deleteDetailsByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, _sID);
		JournalVoucherDetailPeer.doDelete(oCrit, _oConn);
	}	
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static void deleteHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(JournalVoucherPeer.JOURNAL_VOUCHER_ID, _sID);
		JournalVoucherPeer.doDelete(oCrit, _oConn);
	}	

	/**
	 * save journal voucher transaction
	 * @param _oJV
	 * @param _vJVD
	 * @throws Exception
	 */
	public static void saveData (JournalVoucher _oJV, List _vJVD)
		throws Exception
	{
		saveData(_oJV, _vJVD, null);
	}
	
	/**
	 * save journal voucher transaction
	 * 
	 * @param _oJV
	 * @param _vJVD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (JournalVoucher _oJV, List _vJVD, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		boolean bNew = false;
		if(_oConn == null)
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{
			if (_oJV.getTransType() != i_JV_END_MO_CURR) 
			{
				validateDate (_oJV.getTransactionDate(), _oConn);
			}
			
			if (validateJournal(_oJV))
		    {                
			    //process save jv
			    processSaveJournalVoucherData (_oJV, _vJVD, _oConn);
			    
			    if (_oJV.getStatus() == i_PROCESSED)
			    {  			    
                    GlTransactionTool.createJournalVoucherTrans (_oJV, _vJVD, _oConn);
			        //update gl account
                }
			    if(bStartTrans)
			    {
			    	commit (_oConn);
			    }
            }
		}
		catch (Exception _oEx) 
		{
		    if (_oJV.getStatus() == i_PROCESSED)
		    {  			    
		    	_oJV.setStatus(i_PENDING);
		    	_oJV.setTransactionNo("");
            }
		    if (bNew) //restore new state
		    {
		    	_oJV.setNew(true);
		    }
		    if(bStartTrans)
		    {
		    	rollback(_oConn );
		    }
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage());
		}
	}
	
	/**
	 * 
	 * @param _oJV
	 * @param _vJVD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveJournalVoucherData (JournalVoucher _oJV, 
													  List _vJVD, 
													  Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		//Generate Sys ID
		if (StringUtil.isEmpty(_oJV.getJournalVoucherId()))
		{
            _oJV.setJournalVoucherId (IDGenerator.generateSysID());   
            _oJV.setNew(true);
    		validateID(getHeaderByID(_oJV.getJournalVoucherId(), _oConn), "transactionNo");
		}
		
		if (StringUtil.isEmpty(_oJV.getTransactionNo())) 
        {         	         
        	if (_oJV.getStatus() == i_PROCESSED)
    	    {
    		    _oJV.setTransactionNo (LastNumberTool.get(s_JV_FORMAT, LastNumberTool.i_JOURNAL_VOUCHER, _oConn));
    		    validateNo(JournalVoucherPeer.TRANSACTION_NO, _oJV.getTransactionNo(), 
    		    		   JournalVoucherPeer.class, _oConn);
    		    
    		}
        	else
        	{
    		    _oJV.setTransactionNo ("");
        	}
        }  
				
		_oJV.save (_oConn);
		
		//delete any existing pending trans
		deleteDetailsByID (_oJV.getJournalVoucherId(), _oConn);

		for ( int i = 0; i < _vJVD.size(); i++ )
		{
			JournalVoucherDetail oJVD = (JournalVoucherDetail) _vJVD.get(i);
		    if (StringUtil.isEmpty(oJVD.getJournalVoucherDetailId()))
		    {			
		        oJVD.setJournalVoucherDetailId(IDGenerator.generateSysID());
			}
			oJVD.setNew(true);
			oJVD.setModified(true);
			oJVD.setJournalVoucherId(_oJV.getJournalVoucherId());			
			oJVD.save(_oConn);
		}
	}

	/**
	 * cancel JV 
	 * 
	 * @param _oJV
	 * @param _sCancelBy
	 * @throws Exception
	 */
	public static void cancelJV (JournalVoucher _oJV, String _sCancelBy)
		throws Exception
	{
		cancelJV(_oJV, _sCancelBy, null);
	}

	/**
	 * cancel JV 
	 * 
	 * @param _oJV
	 * @param _sCancelBy
	 * @throws Exception
	 */
	public static void cancelJV (JournalVoucher _oJV, String _sCancelBy, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		if (_oConn == null)
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{   
			if (_oJV.getTransType() != i_JV_END_MO_CURR && 
				_oJV.getTransType() != i_JV_END_MO_DEPR &&
				_oJV.getTransType() != i_JV_END_YEAR) 
			{
				validateDate (_oJV.getTransactionDate(), _oConn);
			}

			if (_oJV.getStatus() == i_PROCESSED)
			{  			    
			    BaseJournalTool.deleteJournal(
			    	GlAttributes.i_GL_TRANS_JOURNAL_VOUCHER, _oJV.getJournalVoucherId(), _oConn);
			}
			_oJV.setDescription(cancelledBy(_oJV.getDescription(), _sCancelBy));
	        _oJV.setStatus(i_CANCELLED);
			_oJV.save(_oConn);
			
			if (bStartTrans)
		    {
		    	commit (_oConn);            
		    }		
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new Exception (_oEx.getMessage());
		}
	}

	
	public static void deleteJV (JournalVoucher _oJV, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		if (_oConn == null)
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{   
			Criteria oCrit = new Criteria();
			oCrit.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, _oJV.getJournalVoucherId());
			JournalVoucherDetailPeer.doDelete(oCrit,_oConn);

			oCrit = new Criteria();
			oCrit.add(JournalVoucherPeer.JOURNAL_VOUCHER_ID, _oJV.getJournalVoucherId());
			JournalVoucherPeer.doDelete(oCrit,_oConn);
			if (bStartTrans)
		    {
		    	commit (_oConn);            
		    }
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new Exception (_oEx.getMessage(), _oEx);
		}
	}
	
	/**
	 * save / update journal voucher transaction
	 * roll back any journal created by existing jv first
	 * 
	 * @param _oJV
	 * @param _vJVD
	 * @throws Exception
	 */
	public static void saveJV (JournalVoucher _oJV, List _vJVD, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		if (_oConn == null)
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{
			if (_oJV.getTransType() != i_JV_END_MO_CURR && _oJV.getTransType() != i_JV_END_YEAR) 
			{
				validateDate (_oJV.getTransactionDate(), _oConn);
			}			
		    if (validateJournal(_oJV))
		    {   
		    	//delete old journal created by existing JV
			    BaseJournalTool.deleteJournal(
				    GlAttributes.i_GL_TRANS_JOURNAL_VOUCHER, _oJV.getJournalVoucherId(), _oConn);		    	
		    	
			    //process save jv
			    processSaveJournalVoucherData (_oJV, _vJVD, _oConn);
			    
			    if (_oJV.getStatus() == i_PROCESSED)
			    {  			    
                    GlTransactionTool.createJournalVoucherTrans (_oJV, _vJVD, _oConn);
			        //update gl account
                }
			    if (bStartTrans)
			    {
			    	commit (_oConn);            
			    }
            }
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans)
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new Exception (_oEx.getMessage(), _oEx);
		}
	}
	
	/**
	 * set header from screen
	 * 
	 * @param _oJV
	 * @param _vJVD
	 * @param data
	 * @param _bAll
	 * @throws Exception
	 */
	public static void setHeaderProperties (JournalVoucher _oJV, List _vJVD, RunData data, boolean _bAll) 
    	throws Exception
    {
		try 
		{
			if (data != null) 
			{
				data.getParameters().setProperties (_oJV);
				_oJV.setTransactionDate (CustomParser.parseDate(data.getParameters().getString("TransactionDate")));
			}
			setDetailProperties (_oJV, _vJVD, data, _bAll);
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new Exception ("Set JV Properties Failed : " + _oEx.getMessage ()); 
		}
    }

	/**
	 * set details
	 * 
	 * @param _oJV
	 * @param _vJVD
	 * @param data
	 * @param _bAll
	 * @throws Exception
	 */
	public static void setDetailProperties (JournalVoucher _oJV, List _vJVD, RunData data, boolean _bAll) 
    	throws Exception
    {
		double dTotalDebit  = 0;
		double dTotalCredit = 0;
		
		for (int i = 0; i < _vJVD.size(); i++)
		{		
		    int iNo = i + 1;    
		    String sNo = (Integer.valueOf(iNo)).toString();    
		    if (!_bAll && (i == (_vJVD.size() - 1))) sNo = "";
            
		    log.debug ("** i "   + i);
            log.debug ("** iNo " + iNo);
		    log.debug ("** sNo " + sNo);
            
		    JournalVoucherDetail oJVD = (JournalVoucherDetail) _vJVD.get(i);
		    
		    double dDebit  = 0; 
		    double dCredit = 0; 
		    double dAmount = 0;
		    double dRate   = 0;
		    
		    if (oJVD.getDebitAmount() != null) dDebit = oJVD.getDebitAmount().doubleValue();
		    if (oJVD.getCreditAmount() != null) dCredit =  oJVD.getCreditAmount().doubleValue();
		    if (oJVD.getAmount() != null) dAmount =  oJVD.getAmount().doubleValue();
		    if (oJVD.getCurrencyRate() != null) dRate = oJVD.getCurrencyRate().doubleValue();
		    
		    if (data != null)
		    {
		    	dDebit  = data.getParameters().getDouble("DebitAmount" + sNo);
		    	dCredit = data.getParameters().getDouble("CreditAmount" + sNo);
		    	dAmount = data.getParameters().getDouble("Amount" + sNo);
		    	dRate   = data.getParameters().getDouble("CurrencyRate" + sNo);
		    }
		    double dAmountBase = dAmount * dRate;
            
		    log.debug ("** dDebit " + dDebit);
		    log.debug ("** dCredit " + dCredit);
		    log.debug ("** dAmount " + dAmount);
		    log.debug ("** dAmountBase " + dAmountBase);
            
            //do some checking
            int iType = getDetailType (dDebit, dCredit);            
            if (dAmount == 0) 
            { 
                double dCorrectAmount = dDebit;
                if (iType == i_CREDIT) dCorrectAmount = dCredit;
                log.warn("Amount == 0 should be " + dCorrectAmount); 
                dAmount = dCorrectAmount;
                dAmountBase = dAmount * dRate;
            }

		    oJVD.setDebitAmount  (new BigDecimal (dDebit));
		    oJVD.setCreditAmount (new BigDecimal (dCredit));
		    oJVD.setCurrencyRate (new BigDecimal (dRate));
		    oJVD.setAmount       (new BigDecimal (dAmount));
		    oJVD.setAmountBase   (new BigDecimal (dAmountBase));
		    if (data != null)
		    {
			    oJVD.setLocationId	 (data.getParameters().getString("LocationId" + sNo,""));
			    oJVD.setProjectId    (data.getParameters().getString("ProjectId" + sNo,""));
			    oJVD.setDepartmentId (data.getParameters().getString("DepartmentId" + sNo,""));
			    oJVD.setReferenceNo  (data.getParameters().getString("ReferenceNo" + sNo,""));
			    oJVD.setSubLedgerDesc(data.getParameters().getString("SubLedgerDesc" + sNo,""));

		    }
		    oJVD.setDebitCredit  (iType); 
		    if (iType == i_DEBIT) dTotalDebit  += dAmountBase;
		    if (iType == i_CREDIT) dTotalCredit += dAmountBase;
            		
         }
		_oJV.setTotalDebit  (new BigDecimal (dTotalDebit));
        _oJV.setTotalCredit (new BigDecimal (dTotalCredit));
    }    

    //return debit / credit
    public static int getDetailType (double dDebit, double dCredit)
    {
    	//TODO: do Check 
        if (dDebit != 0 && dCredit == 0) return i_DEBIT;
        if (dCredit != 0 && dDebit == 0) return i_CREDIT;
        //return invalid type
        return -1; 
    }
    
    /**
     * validate journal make sure debit = credit
     * 
     * @param _oJV
     * @return
     * @throws Exception
     */
	public static boolean validateJournal (JournalVoucher _oJV) 
    	throws Exception
    {
        double dTotalDebit  = _oJV.getTotalDebit().doubleValue();
        double dTotalCredit = _oJV.getTotalCredit().doubleValue();
        
        if (dTotalDebit != dTotalCredit)
        {   
        	log.warn("Total Debit " + dTotalDebit + ", Total Credit " + dTotalCredit);
        	double dDelta = dTotalDebit - dTotalCredit;
        	if (dDelta < 0) dDelta = dDelta * -1;
        	if (dDelta > d_UNBALANCE_TOLERANCE)
        	{
            	log.error("Unbalance Journal : " + dDelta);
        		StringBuilder oError = new StringBuilder ("Unbalanced Journal ");
	            if (dTotalDebit < dTotalCredit) oError.append (" Total Debit < Total Credit ");
	            if (dTotalDebit > dTotalCredit) oError.append (" Total Debit > Total Credit ");
	            throw new Exception (oError.toString());
        	}
        }
        return true;
                
    }

	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _iStatus
	 * @param _iLimit
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData(int _iCond, 
									   String _sKeywords,
									   Date _dStart,
									   Date _dEnd,
									   int _iStatus,
									   int _iLimit)
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			JournalVoucherPeer.TRANSACTION_DATE, _dStart, _dEnd, null);
		
		if (_iStatus > 0)
		{
			oCrit.add(JournalVoucherPeer.STATUS, _iStatus);
		}
		
		return new LargeSelect(oCrit,_iLimit,"com.ssti.enterprise.pos.om.JournalVoucherPeer");
	}
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List findData(int _iCond, 
							    String _sKeywords,
							    Date _dStart,
							    Date _dEnd,
							    int _iStatus)	
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords,JournalVoucherPeer.TRANSACTION_DATE, _dStart, _dEnd, null);		
		if (_iStatus > 0)
		{
			oCrit.add(JournalVoucherPeer.STATUS, _iStatus);
		}
		return JournalVoucherPeer.doSelect(oCrit);
	}

	/**
	 * 
	 * @param _sID
	 * @param _bIsNext
	 * @return
	 * @throws Exception
	 */
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(JournalVoucherPeer.class, JournalVoucherPeer.JOURNAL_VOUCHER_ID, _sID, _bIsNext);
	}	
	
	public static String getStatusString (int _iStatus)
	{
		if (_iStatus == i_PENDING)   return LocaleTool.getString ("pending");
		if (_iStatus == i_PROCESSED) return LocaleTool.getString ("processed");
		if (_iStatus == i_CANCELLED) return LocaleTool.getString ("cancelled");
		return "";
	}

	//-------------------------------------------------------------------------
	// external JV (opening balance, closing period, closing year)
	//-------------------------------------------------------------------------
	public static void createAccountOB (Account _oAcc, Account _oEquity, String _sUserName)
		throws Exception
	{
		createAccountOB(_oAcc, _oEquity, _sUserName, null, -1, null);
	}
	
	/**
	 * create account opening balance JV
	 * will override and update existing JV and create new if not exist
	 * used by account opening balance and account with sub ledger (AR/AP)
	 * if updating JV for (AR/AP) then try to get Existing JV by TransId in AR/AP
	 * else try to get existing JV from Account ObTransId
	 * 
	 * @param _oAcc
	 * @param _oEquity
	 * @param _sUserName
	 * @param _oSubLedger AP / AR OM
	 * @param _sSubLedgerType if OB from AP / AR
	 * @throws Exception
	 */
	public static void createAccountOB (Account _oAcc, 
										Account _oEquity, 
										String _sUserName, 
										SubLedgerOM _oSubLedger,
										int _iSubLedgerType,
										Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		JournalVoucher oJV = null;
		try
		{
			if (_oConn == null) 
			{
				_oConn = beginTrans();				
				bStartTrans = true;
			}			
			
			//get account ob trans id to get existing JV
			String sOBTransID = _oAcc.getObTransId();
			if (_oSubLedger != null) 
			{
				//if from sub ledger then try get existing JV by TRANS ID in AR/AP
				sOBTransID = _oSubLedger.getTransactionId();
			}
			
			if (StringUtil.isNotEmpty(sOBTransID)) 
			{
				oJV = getHeaderByID(sOBTransID, _oConn);	
			}
			if (oJV == null) //creat new JV if no existing 
			{
				oJV = new JournalVoucher();	
				oJV.setJournalVoucherId(IDGenerator.generateSysID());
			}
			else
			{
				deleteJV(oJV,_oConn);
				oJV.setNew(true);
				oJV.setModified(true);
			}
			
	        double dRate = _oAcc.getObRate().doubleValue();
	        double dAmount = _oAcc.getOpeningBalance().doubleValue(); 		
	        Date dAsDate = _oAcc.getAsDate();
	        String sDesc = "Account Opening Balance " + _oAcc.getAccountCode();
			if (_oSubLedger != null) 
			{
				//if from sub ledger then get rate & amount from AR/AP
		        dRate = _oSubLedger.getCurrencyRate().doubleValue();
		        dAmount = _oSubLedger.getAmount().doubleValue(); 	
		        dAsDate = _oSubLedger.getTransactionDate();
		        sDesc = _oSubLedger.getRemark();
			}
	        
	        int iDebitCredit = _oAcc.getNormalBalance();
	        //if amount is minus then we need to switch the debit credit entry
	        if (dAmount < 0) 
	        {
	        	if (iDebitCredit == i_DEBIT) iDebitCredit = i_CREDIT;
	        	else if (iDebitCredit == i_CREDIT) iDebitCredit = i_DEBIT;
	        	dAmount = dAmount * -1;
	        }
	        double dAmountBase = dRate * dAmount;					
			
			oJV.setDescription(sDesc);
			oJV.setTransactionDate(dAsDate);
			oJV.setNextRecurringDate(null);
			oJV.setRecurring(false);
			oJV.setStatus(i_PROCESSED);
			oJV.setTotalCredit(new BigDecimal(dAmountBase));
			oJV.setTotalDebit(new BigDecimal(dAmountBase));
			oJV.setUserName(_sUserName);
			oJV.setTransType(i_JV_ACC_OB);
			
			//details
			List vJVD = new ArrayList(2);	
			JournalVoucherDetail oJVDAcc = new JournalVoucherDetail();
			oJVDAcc.setAccountId(_oAcc.getAccountId());
			oJVDAcc.setAccountCode(_oAcc.getAccountCode());
			oJVDAcc.setAccountName(_oAcc.getAccountName());
			oJVDAcc.setAmount(new BigDecimal(dAmount));
			oJVDAcc.setAmountBase(new BigDecimal(dAmountBase));			
			oJVDAcc.setCurrencyId(_oAcc.getCurrencyId());
			oJVDAcc.setCurrencyRate(new BigDecimal(dRate));	        
			oJVDAcc.setCreditAmount(bd_ZERO);
			oJVDAcc.setDebitAmount(bd_ZERO);
			
	        if (iDebitCredit == i_CREDIT)
	        {
	            oJVDAcc.setCreditAmount(oJVDAcc.getAmount());	        	
	        }
	        else if (iDebitCredit == i_DEBIT)
	        {
	        	oJVDAcc.setDebitAmount(oJVDAcc.getAmount());
	        }
			oJVDAcc.setDebitCredit(iDebitCredit);
			oJVDAcc.setLocationId("");
			oJVDAcc.setDepartmentId("");
			oJVDAcc.setMemo("");
			oJVDAcc.setProjectId("");			

			if (_oSubLedger != null)
			{
				oJVDAcc.setSubLedgerId(_oSubLedger.getEntityId());
				oJVDAcc.setSubLedgerType(_iSubLedgerType);				
			}			
			
			//opening balance equity 
			JournalVoucherDetail oJVDOB = new JournalVoucherDetail();
			oJVDOB.setAccountId(_oEquity.getAccountId());
			oJVDOB.setAccountCode(_oEquity.getAccountCode());
			oJVDOB.setAccountName(_oEquity.getAccountName());
			oJVDOB.setAmount(new BigDecimal(dAmountBase));
			oJVDOB.setAmountBase(new BigDecimal(dAmountBase));			
			oJVDOB.setCurrencyId(_oEquity.getCurrencyId());
			oJVDOB.setCurrencyRate(bd_ONE);
			oJVDOB.setCreditAmount(bd_ZERO);
			oJVDOB.setDebitAmount(bd_ZERO);

			int iEquityDebitCredit = i_CREDIT;	//opening balance equity default value        
	        if (iDebitCredit == i_CREDIT)
	        {
	            //if account normal balance is credit then we should set equity journal trans to debit
	            //that will minus the balance equity
	            iEquityDebitCredit = i_DEBIT;
	            oJVDOB.setDebitAmount(oJVDOB.getAmountBase());
	        }
	        else
	        {
	            oJVDOB.setCreditAmount(oJVDOB.getAmountBase());	        	
	        }
			oJVDOB.setDebitCredit(iEquityDebitCredit);
			oJVDOB.setLocationId("");
			oJVDOB.setDepartmentId("");
			oJVDOB.setMemo("");
			oJVDOB.setProjectId("");			

			vJVD.add(oJVDAcc);
			vJVD.add(oJVDOB);
			
			saveJV(oJV, vJVD, _oConn);			
			
			if (_oSubLedger != null) //set AR/AP link with JV
			{
				_oSubLedger.setTransactionId(oJV.getJournalVoucherId());
				_oSubLedger.setTransactionNo(oJV.getTransactionNo());
				_oSubLedger.save(_oConn);
			}
			else //if not AR/AP JV then set account ob trans id
			{
				_oAcc.setObTransId(oJV.getJournalVoucherId());
				_oAcc.save(_oConn);	
			}			
			if (bStartTrans)
			{
				commit(_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans)
			{
				rollback(_oConn);
			}
			log.error(_oEx);
			throw new NestableException (
				"Creating Opening Balance Voucher Failed, ERROR: " + _oEx.getMessage(), _oEx);
		}	
	}			

	//-------------------------------------------------------------------------
	// external JV (closing period)
	//-------------------------------------------------------------------------

	public static String createForexAdj(Map _mCurr, 
										Date _dEndDate, 
										Period _oPeriod, 
										String _sUserName,
										Connection _oConn)
		throws Exception
	{
		JournalVoucher oJV = null;
		try
		{
			String sCloseTransID = _oPeriod.getCloseTransId();
			if (StringUtil.isNotEmpty(sCloseTransID))
			{
				oJV = getHeaderByID(sCloseTransID, _oConn);	
			}
			if (oJV == null)
			{
				oJV = new JournalVoucher();	
				oJV.setJournalVoucherId(IDGenerator.generateSysID());
			}
		
			oJV.setDescription("Period Closing (" + _oPeriod.getDescription() + ")");
			oJV.setTransactionDate(_dEndDate);
			oJV.setNextRecurringDate(null);
			oJV.setRecurring(false);
			oJV.setStatus(i_PROCESSED);
			oJV.setUserName(_sUserName);
			oJV.setTransType(i_JV_END_MO_CURR);

			double dTotalAmount = 0;
			List vJVD = new ArrayList();

			Set vKeys = _mCurr.keySet();
			Iterator oIter = vKeys.iterator();
			while (oIter.hasNext())
			{
				String sCurrencyID = (String)oIter.next();
				double dRate = ((Double) _mCurr.get(sCurrencyID)).doubleValue();
				List vAcc = AccountTool.getMultiCurrencyAcc(sCurrencyID);
				
				if(dRate > 0 && dRate != 1) //validate whether rate correct
				{				
					for (int i = 0; i < vAcc.size(); i++)
					{
						Account oAcc = (Account) vAcc.get(i);
						log.debug("oACC\n" + oAcc);
						log.debug("dEndDate" + _dEndDate);
												
						Date dBalDate = DateUtil.getStartOfDayDate(DateUtil.addDays(_dEndDate, 1));
						
						double[] dBal = AccountBalanceTool.getBalance(oAcc.getAccountId(), dBalDate);
						double dBaseBal = dBal[i_BASE];
						double dPrimeBal = dBal[i_PRIME];
						double dAvgRate = dRate;
						if (dPrimeBal != 0) dAvgRate = dBaseBal /dPrimeBal;
						
						double dEndBaseBal = dPrimeBal * dRate;
						double dRealUnrealGL = dEndBaseBal - dBaseBal;
						
						if (dEndBaseBal > dBaseBal) dTotalAmount += dEndBaseBal;
						else dTotalAmount += dBaseBal;
							
						JournalVoucherDetail oJVDAcc = new JournalVoucherDetail();
						oJVDAcc.setAccountId(oAcc.getAccountId());
						oJVDAcc.setAccountCode(oAcc.getAccountCode());
						oJVDAcc.setAccountName(oAcc.getAccountName());
						oJVDAcc.setAmount(new BigDecimal(dPrimeBal));
						oJVDAcc.setAmountBase(new BigDecimal(dBaseBal));			
						oJVDAcc.setCurrencyId(oAcc.getCurrencyId());
						oJVDAcc.setCurrencyRate(new BigDecimal(dAvgRate));	        
						oJVDAcc.setCreditAmount(bd_ZERO);
						oJVDAcc.setDebitAmount(bd_ZERO);
						
						int iEndDebitCredit = oAcc.getNormalBalance();
						int iDebitCredit = i_CREDIT;
						if (iEndDebitCredit == i_CREDIT)
				        {
				            oJVDAcc.setDebitAmount(oJVDAcc.getAmount());	        	
				            iDebitCredit = i_DEBIT;
				        }
				        else if (iEndDebitCredit == i_DEBIT)
				        {
				        	oJVDAcc.setCreditAmount(oJVDAcc.getAmount());
				            iDebitCredit = i_CREDIT;
				        }
						oJVDAcc.setDebitCredit(iDebitCredit);
						oJVDAcc.setLocationId("");
						oJVDAcc.setDepartmentId("");
						oJVDAcc.setMemo("");
						oJVDAcc.setProjectId("");			
						
						JournalVoucherDetail oJVDEnd = oJVDAcc.copy();
						oJVDEnd.setAmount(new BigDecimal(dPrimeBal));
						oJVDEnd.setAmountBase(new BigDecimal(dEndBaseBal));			
						oJVDEnd.setCurrencyId(oAcc.getCurrencyId());
						oJVDEnd.setCurrencyRate(new BigDecimal(dRate));	        
						oJVDEnd.setCreditAmount(bd_ZERO);
						oJVDEnd.setDebitAmount(bd_ZERO);
	
						if (iEndDebitCredit == i_CREDIT)
				        {
				            oJVDEnd.setCreditAmount(oJVDEnd.getAmount());	        	
				        }
				        else if (iEndDebitCredit == i_DEBIT)
				        {
				        	oJVDEnd.setDebitAmount(oJVDEnd.getAmount());
				        }
						oJVDEnd.setDebitCredit(iEndDebitCredit);
						
						boolean bRealized = false;
						if (oAcc.getAccountType() == i_CASH_BANK) bRealized = true;
	
						Account oRUGAcc = AccountTool.getRealUnrealGainLoss(sCurrencyID, bRealized, _oConn);
						
						int iRUGDebitCredit = oRUGAcc.getNormalBalance();					
						if (dRealUnrealGL < 0) //end < base 
				        {
							//if real unreal < 0 then real unreal will put into the same 
							//position as default account debit credit
							dRealUnrealGL = dRealUnrealGL * -1;
							iRUGDebitCredit = iEndDebitCredit;						
				        }
						else
						{
							if (iEndDebitCredit == i_DEBIT)
							{
								iRUGDebitCredit = i_CREDIT;
							}
							else
							{
								iEndDebitCredit = i_DEBIT;							
							}
						}					
						
						JournalVoucherDetail oJVDRUG = new JournalVoucherDetail();
						oJVDRUG.setAccountId(oRUGAcc.getAccountId());
						oJVDRUG.setAccountCode(oRUGAcc.getAccountCode());
						oJVDRUG.setAccountName(oRUGAcc.getAccountName());
						oJVDRUG.setAmount(new BigDecimal(dRealUnrealGL));
						oJVDRUG.setAmountBase(new BigDecimal(dRealUnrealGL));			
						oJVDRUG.setCurrencyId(oRUGAcc.getCurrencyId());
						oJVDRUG.setCurrencyRate(bd_ONE);	        
						oJVDRUG.setCreditAmount(bd_ZERO);
						oJVDRUG.setDebitAmount(bd_ZERO);
						
						if (iRUGDebitCredit == i_DEBIT)
						{
				        	oJVDRUG.setDebitAmount(oJVDRUG.getAmount());
						}
				        else if (iRUGDebitCredit == i_CREDIT)
				        {
				        	oJVDRUG.setCreditAmount(oJVDRUG.getAmount());
				        }
						oJVDRUG.setDebitCredit(iRUGDebitCredit);
						oJVDRUG.setLocationId("");
						oJVDRUG.setDepartmentId("");
						oJVDRUG.setMemo(oAcc.getAccountCode() + " Forex Gain Loss");
						oJVDRUG.setProjectId("");			
						
						vJVD.add(oJVDAcc);
						vJVD.add(oJVDEnd); 
						vJVD.add(oJVDRUG);
					}
				}
			}
			oJV.setTotalCredit(new BigDecimal(dTotalAmount));
			oJV.setTotalDebit(new BigDecimal(dTotalAmount));			
			
			saveJV(oJV, vJVD, _oConn);	
			return oJV.getJournalVoucherId();
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (
				"Creating Closing Period Voucher (Rate Adj) Failed, ERROR: " + _oEx.getMessage(), _oEx);
		}	
	}	

	public static void cancelForexAdj (String _sJVID, String _sUserName, Connection _oConn)
		throws Exception
	{
		JournalVoucher oJV = getHeaderByID(_sJVID, _oConn);
		if (oJV != null && oJV.getStatus() == i_PROCESSED) {JournalVoucherTool.cancelJV(oJV, _sUserName);}
		Criteria oCrit = new Criteria();
		oCrit.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, _sJVID);
		JournalVoucherDetailPeer.doDelete(oCrit, _oConn);

		oCrit = new Criteria();
		oCrit.add(JournalVoucherPeer.JOURNAL_VOUCHER_ID, _sJVID);
		JournalVoucherPeer.doDelete(oCrit, _oConn);

	}

	/**
	 * create global closing year journal
	 * 
	 * @param _oPeriod
	 * @param _sUserName
	 * @param _iYear
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static String createClosingYearJournal(Period _oPeriod, 
												  String _sUserName, 
												  int _iYear, 
												  Connection _oConn) 
		throws Exception
	{
		return createClosingYearJournal(_oPeriod, _sUserName, _iYear, false, _oConn);
	}
	
	/**
	 * create global closing year journal
	 * 
	 * @param _oPeriod
	 * @param _sUserName
	 * @param _iYear
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static String createClosingYearJournal(Period _oPeriod, 
												  String _sUserName, 
												  int _iYear, 
												  boolean _bFirstJan,
												  Connection _oConn) 
		throws Exception
	{
		JournalVoucher oJV = null;
		try
		{
			oJV = new JournalVoucher();	
			oJV.setJournalVoucherId(IDGenerator.generateSysID());
			oJV.setDescription(LocaleTool.getString("close_year") + " " + _iYear);
			
			DateTime oDT = new DateTime(_oPeriod.getEndDate());
			oDT.withSecondOfMinute(59);
			oDT.withMillisOfSecond(59); //parse to 23:59:59`
			Date dTrans = oDT.toDate();
			if (_bFirstJan)
			{
				dTrans = DateUtil.getStartOfDayDate(DateUtil.addDays(dTrans, 1));
			}
			
			oJV.setTransactionDate(dTrans);
			oJV.setNextRecurringDate(null);
			oJV.setRecurring(false);
			oJV.setStatus(i_PROCESSED);
			oJV.setUserName(_sUserName);
			oJV.setTransType(i_JV_END_YEAR);

			double dTotalAmount = 0;
			List vJVD = new ArrayList();
			List vPL = new ArrayList();
			vPL.addAll(AccountTool.getAccountByType(i_REVENUE, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_COGS, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_EXPENSE, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_OTHER_INCOME, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_OTHER_EXPENSE, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_INCOME_TAX, false, false));
			
			Date dAsOf = _oPeriod.getEndDate();
			dAsOf = DateUtil.getStartOfDayDate(DateUtil.addDays(dAsOf, 1));
			
			double dTotalDebit = 0;
			double dTotalCredit = 0;					
			for (int i = 0; i < vPL.size(); i++)
			{
				Account oAcc = (Account) vPL.get(i);
				double[] dBal =  AccountBalanceTool.getBalance(oAcc.getAccountId(), dAsOf);
								
				log.debug(" Account " + oAcc.getAccountCode() + " - " + oAcc.getAccountName() + 
						  " Balance " + CustomFormatter.fmt(dBal[i_BASE]));
				if (dBal[i_BASE] != 0)
				{
					BigDecimal dAmt = new BigDecimal (dBal[i_BASE]);
					JournalVoucherDetail oJVD = new JournalVoucherDetail();
					oJVD.setAccountId(oAcc.getAccountId());
					oJVD.setAccountCode(oAcc.getAccountCode());
					oJVD.setAccountName(oAcc.getAccountName());
					oJVD.setAmount(dAmt);
					oJVD.setAmountBase(dAmt);
					oJVD.setCurrencyId(oAcc.getCurrencyId());
					oJVD.setCurrencyRate(bd_ONE);
					if (oAcc.getAccountType() == i_REVENUE || oAcc.getAccountType() == i_OTHER_INCOME)
					{
						if (dBal [i_BASE] >= 0)
						{
							oJVD.setCreditAmount(bd_ZERO);
							oJVD.setDebitAmount(dAmt);
							oJVD.setDebitCredit(i_DEBIT);
							dTotalDebit = dTotalDebit + dBal[i_BASE];
						}
						else
						{
							dAmt = new BigDecimal (dBal[i_BASE] * -1);
							oJVD.setCreditAmount(dAmt);
							oJVD.setDebitAmount(bd_ZERO);
							oJVD.setDebitCredit(i_CREDIT);
							oJVD.setAmount(dAmt);
							oJVD.setAmountBase(dAmt);
							dTotalCredit = dTotalCredit + dAmt.doubleValue();
						}
					}
					else
					{
						if (dBal [i_BASE] >= 0)
						{
							oJVD.setCreditAmount(dAmt);
							oJVD.setDebitAmount(bd_ZERO);
							oJVD.setDebitCredit(i_CREDIT);
							dTotalCredit = dTotalCredit + dBal[i_BASE];
						}
						else
						{
							dAmt = new BigDecimal (dBal[i_BASE] * -1);
							oJVD.setDebitAmount(dAmt);
							oJVD.setCreditAmount(bd_ZERO);
							oJVD.setDebitCredit(i_DEBIT);
							oJVD.setAmount(dAmt);
							oJVD.setAmountBase(dAmt);
							dTotalDebit = dTotalDebit + dAmt.doubleValue();
						}
					}
					oJVD.setDepartmentId("");
					oJVD.setProjectId("");
					oJVD.setLocationId("");
					oJVD.setJournalVoucherId(oJV.getJournalVoucherId());
					oJVD.setMemo("");
					
					vJVD.add(oJVD);
				}
			}
			double dRE = 0;
			int iREDC = i_DEBIT;
			if (dTotalDebit > dTotalCredit) {dRE = dTotalDebit - dTotalCredit; iREDC = i_CREDIT;}
			if (dTotalCredit > dTotalDebit) {dRE = dTotalCredit - dTotalDebit; iREDC = i_DEBIT;}
			BigDecimal bdRE = new BigDecimal(dRE);
			
			Account oREAcc = AccountTool.getRetainedEarning(_oConn);
			JournalVoucherDetail oJVD = new JournalVoucherDetail();
			oJVD.setAccountId(oREAcc.getAccountId());
			oJVD.setAccountCode(oREAcc.getAccountCode());
			oJVD.setAccountName(oREAcc.getAccountName());
			oJVD.setAmount(bdRE);
			oJVD.setAmountBase(bdRE);
			oJVD.setCurrencyId(oREAcc.getCurrencyId());
			oJVD.setCurrencyRate(bd_ONE);
	
			if (iREDC == i_DEBIT)
			{
				oJVD.setCreditAmount(bd_ZERO);
				oJVD.setDebitAmount(bdRE);
				oJVD.setDebitCredit(i_DEBIT);
				dTotalDebit = dTotalDebit + dRE;
			}
			else
			{
				oJVD.setCreditAmount(bdRE);
				oJVD.setDebitAmount(bd_ZERO);
				oJVD.setDebitCredit(i_CREDIT);
				dTotalCredit = dTotalCredit + dRE;
			}
			
			oJVD.setDepartmentId("");
			oJVD.setProjectId("");
			oJVD.setLocationId("");
			oJVD.setJournalVoucherId(oJV.getJournalVoucherId());
			oJVD.setMemo("");
			
			vJVD.add(oJVD);
			
			oJV.setTotalDebit (new BigDecimal (dTotalDebit));
			oJV.setTotalCredit (new BigDecimal (dTotalCredit));
			
			saveJV(oJV, vJVD, _oConn);	
			
			return oJV.getJournalVoucherId();
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (
				"Creating Closing Year Journal Voucher Failed, ERROR: " + _oEx.getMessage(), _oEx);
		}			
	}	
	/**
	 * create closing year journal by location
	 * 
	 * @param _oPeriod
	 * @param _sUserName
	 * @param _iYear
	 * @param _oConn
	 * @return journal voucher id
	 * @throws Exception
	 */
	public static String createClosingYearLocation(Period _oPeriod, 
			  									   String _sUserName, 
			  									   int _iYear, 
			  									   Connection _oConn)
		throws Exception
	{
		return createClosingYearLocation(_oPeriod,_sUserName,_iYear, false,_oConn);
	}
	
	/**
	 * create closing year journal by location
	 * 
	 * @param _oPeriod
	 * @param _sUserName
	 * @param _iYear
	 * @param _bFirstJan
	 * @param _oConn
	 * @return journal voucher id
	 * @throws Exception
	 */
	public static String createClosingYearLocation(Period _oPeriod, 
			  									   String _sUserName, 
			  									   int _iYear, 
			  									   boolean _bFirstJan,
			  									   Connection _oConn)
		throws Exception
	{
		JournalVoucher oJV = null;
		try
		{
			oJV = new JournalVoucher();	
			oJV.setJournalVoucherId(IDGenerator.generateSysID());
			oJV.setDescription(LocaleTool.getString("close_year") + " " + _iYear);
			
			DateTime oDT = new DateTime(_oPeriod.getEndDate());
			oDT.withSecondOfMinute(59);
			oDT.withMillisOfSecond(59); //parse to 23:59:59`
			Date dTrans = oDT.toDate();
			if (_bFirstJan)
			{
				dTrans = DateUtil.getStartOfDayDate(DateUtil.addDays(dTrans, 1));
			}
			
			oJV.setTransactionDate(dTrans);
			oJV.setNextRecurringDate(null);
			oJV.setRecurring(false);
			oJV.setStatus(i_PROCESSED);
			oJV.setUserName(_sUserName);
			oJV.setTransType(i_JV_END_YEAR);

			double dTotalAmount = 0;
			List vJVD = new ArrayList();
			List vPL = new ArrayList();
			vPL.addAll(AccountTool.getAccountByType(i_REVENUE, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_COGS, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_EXPENSE, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_OTHER_INCOME, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_OTHER_EXPENSE, false, false));
			vPL.addAll(AccountTool.getAccountByType(i_INCOME_TAX, false, false));
			
			Date dAsOf = _oPeriod.getEndDate();
			dAsOf = DateUtil.getStartOfDayDate(DateUtil.addDays(dAsOf, 1));
			List vLoc = LocationTool.getAllLocation();
			
			double dTotalDebit = 0;
			double dTotalCredit = 0;

			for (int j = 0; j < vLoc.size(); j++)
			{	
				double dTotalDebitLoc = 0;
				double dTotalCreditLoc = 0;

				Location oLoc = (Location) vLoc.get(j);
				for (int i = 0; i < vPL.size(); i++)
				{
					Account oAcc = (Account) vPL.get(i);
					double[] dBal =  AccountBalanceTool.getBalance(
						oAcc.getAccountId(),"loc", oLoc.getLocationId(), dAsOf);
					
					log.debug(" Location " + oLoc.getLocationName() +
							  " Account " + oAcc.getAccountCode() + " - " + oAcc.getAccountName() + 
							  " Balance " + CustomFormatter.fmt(dBal[i_BASE]));
					
					if (dBal[i_BASE] != 0)
					{
						BigDecimal dAmt = new BigDecimal (dBal[i_BASE]);
						JournalVoucherDetail oJVD = new JournalVoucherDetail();
						oJVD.setAccountId(oAcc.getAccountId());
						oJVD.setAccountCode(oAcc.getAccountCode());
						oJVD.setAccountName(oAcc.getAccountName());
						oJVD.setAmount(dAmt);
						oJVD.setAmountBase(dAmt);
						oJVD.setCurrencyId(oAcc.getCurrencyId());
						oJVD.setCurrencyRate(bd_ONE);
						if (oAcc.getAccountType() == i_REVENUE || 
							oAcc.getAccountType() == i_OTHER_INCOME)
						{
							if (dBal [i_BASE] >= 0)
							{
								oJVD.setCreditAmount(bd_ZERO);
								oJVD.setDebitAmount(dAmt);
								oJVD.setDebitCredit(i_DEBIT);
								dTotalDebit = dTotalDebit + dBal[i_BASE];
								dTotalDebitLoc = dTotalDebitLoc + dBal[i_BASE];
							}
							else
							{
								dAmt = new BigDecimal (dBal[i_BASE] * -1);
								oJVD.setCreditAmount(dAmt);
								oJVD.setDebitAmount(bd_ZERO);
								oJVD.setDebitCredit(i_CREDIT);
								oJVD.setAmount(dAmt);
								oJVD.setAmountBase(dAmt);
								dTotalCredit = dTotalCredit + dAmt.doubleValue();
								dTotalCreditLoc = dTotalCreditLoc + dAmt.doubleValue();							
							}
						}
						else
						{
							if (dBal [i_BASE] >= 0)
							{
								oJVD.setCreditAmount(dAmt);
								oJVD.setDebitAmount(bd_ZERO);
								oJVD.setDebitCredit(i_CREDIT);
								dTotalCredit = dTotalCredit + dBal[i_BASE];
								dTotalCreditLoc = dTotalCreditLoc + dBal[i_BASE];								
							}
							else
							{
								dAmt = new BigDecimal (dBal[i_BASE] * -1);
								oJVD.setDebitAmount(dAmt);
								oJVD.setCreditAmount(bd_ZERO);
								oJVD.setDebitCredit(i_DEBIT);
								oJVD.setAmount(dAmt);
								oJVD.setAmountBase(dAmt);
								dTotalDebit = dTotalDebit + dAmt.doubleValue();
								dTotalDebitLoc = dTotalDebitLoc + dAmt.doubleValue();								
							}
						}
						oJVD.setDepartmentId("");
						oJVD.setProjectId("");
						oJVD.setLocationId(oLoc.getLocationId());
						oJVD.setJournalVoucherId(oJV.getJournalVoucherId());
						oJVD.setMemo("");
						
						vJVD.add(oJVD);
					}//if bal > 0				
				}//for acc
				
				double dRE = 0;
				int iREDC = i_DEBIT;
				if (dTotalDebitLoc > dTotalCreditLoc) {dRE = dTotalDebitLoc - dTotalCreditLoc; iREDC = i_CREDIT;}
				if (dTotalCreditLoc > dTotalDebitLoc) {dRE = dTotalCreditLoc - dTotalDebitLoc; iREDC = i_DEBIT;}
				BigDecimal bdRE = new BigDecimal(dRE);
				
				Account oREAcc = AccountTool.getRetainedEarning(_oConn);
				JournalVoucherDetail oJVD = new JournalVoucherDetail();
				oJVD.setAccountId(oREAcc.getAccountId());
				oJVD.setAccountCode(oREAcc.getAccountCode());
				oJVD.setAccountName(oREAcc.getAccountName());
				oJVD.setAmount(bdRE);
				oJVD.setAmountBase(bdRE);
				oJVD.setCurrencyId(oREAcc.getCurrencyId());
				oJVD.setCurrencyRate(bd_ONE);
		
				if (iREDC == i_DEBIT)
				{
					oJVD.setCreditAmount(bd_ZERO);
					oJVD.setDebitAmount(bdRE);
					oJVD.setDebitCredit(i_DEBIT);
					dTotalDebit = dTotalDebit + dRE;
				}
				else
				{
					oJVD.setCreditAmount(bdRE);
					oJVD.setDebitAmount(bd_ZERO);
					oJVD.setDebitCredit(i_CREDIT);
					dTotalCredit = dTotalCredit + dRE;
				}
				
				oJVD.setDepartmentId("");
				oJVD.setProjectId("");
				oJVD.setLocationId(oLoc.getLocationId());
				oJVD.setJournalVoucherId(oJV.getJournalVoucherId());
				oJVD.setMemo("Retained Earning " + oLoc.getLocationCode() + " - " + oLoc.getLocationName());
				
				vJVD.add(oJVD);
			}//for loc
			
			oJV.setTotalDebit (new BigDecimal (dTotalDebit));
			oJV.setTotalCredit (new BigDecimal (dTotalCredit));
			
			saveJV(oJV, vJVD, _oConn);	
			
			return oJV.getJournalVoucherId();
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (
				"Creating Closing Year Journal Voucher Failed, ERROR: " + _oEx.getMessage(), _oEx);
		}		
	}
	
	public static void cancelClosingYearJournal (String _sJVID, String _sUserName, Connection _oConn)
		throws Exception
	{
		JournalVoucher oJV = getHeaderByID(_sJVID, _oConn);
		if (oJV != null && oJV.getStatus() == i_PROCESSED) 
		{
			JournalVoucherTool.cancelJV(oJV, _sUserName);
		}
		Criteria oCrit = new Criteria();
		oCrit.add(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, _sJVID);
		JournalVoucherDetailPeer.doDelete(oCrit, _oConn);
	
		oCrit = new Criteria();
		oCrit.add(JournalVoucherPeer.JOURNAL_VOUCHER_ID, _sJVID);
		JournalVoucherPeer.doDelete(oCrit, _oConn);
	}
	
	/**
	 * 
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static String displaySub(String _sID, String _sSub)
    	throws Exception
    {
		String sCol = "location_id";
			
        String sSQL = "SELECT DISTINCT " + sCol + " FROM journal_voucher_detail WHERE journal_voucher_id = '" + _sID + "'";
        String sResult = "";
        List v = BasePeer.executeQuery(sSQL);
        for (int i = 0; i < v.size(); i++)
        {
        	Record oRec = (Record)v.get(i);
        	String sID = oRec.getValue(1).asString();
        	String sCode = LocationTool.getCodeByID(sID);        	
        	sResult += sCode;
        	if (i != (v.size() - 1)) sResult += ",";
        }
        return sResult;
	}
    
    //-------------------------------------------------------------------------
    //SYNC
    //-------------------------------------------------------------------------
    
    /**
     * get JV created in store
     * 
     * @return transaction created in store since last store 2 ho
     * @throws Exception
     */
    public static List getStoreTrans()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
        if (dLastSyncDate  != null) 
        {
            oCrit.add(JournalVoucherPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
        }
        //only get IN transfer
        //oCrit.add(JournalVoucherPeer.LOCATION_ID, PreferenceTool.getLocationID());       
        oCrit.add(JournalVoucherPeer.STATUS, i_PROCESSED);
        return JournalVoucherPeer.doSelect(oCrit);
    }


    /**
     * move PL As Of Balance to RE, similar to end year closing but available to any period
     * 
     * @param _dAsOf
     * @param _sUserName
     * @return
     * @throws Exception
     */
    public static String createClosePL(Date _dAsOf, String _sUserName)
    	throws Exception
    {
    	JournalVoucher oJV = null;
    	try
    	{
    		oJV = new JournalVoucher();	
    		oJV.setJournalVoucherId(IDGenerator.generateSysID());
    		oJV.setDescription(" PL Close: " + CustomFormatter.fmt(_dAsOf));

    		DateTime oDT = new DateTime(_dAsOf);
    		oDT.withSecondOfMinute(59);
    		oDT.withMillisOfSecond(59); //parse to 23:59:59`
    		Date dTrans = oDT.toDate();

    		oJV.setTransactionDate(dTrans);
    		oJV.setNextRecurringDate(null);
    		oJV.setRecurring(false);
    		oJV.setStatus(i_PROCESSED);
    		oJV.setUserName(_sUserName);
    		oJV.setTransType(i_JV_NORMAL);

    		double dTotalAmount = 0;
    		List vJVD = new ArrayList();
    		List vPL = new ArrayList();
    		vPL.addAll(AccountTool.getAccountByType(i_REVENUE, false, false));
    		vPL.addAll(AccountTool.getAccountByType(i_COGS, false, false));
    		vPL.addAll(AccountTool.getAccountByType(i_EXPENSE, false, false));
    		vPL.addAll(AccountTool.getAccountByType(i_OTHER_INCOME, false, false));
    		vPL.addAll(AccountTool.getAccountByType(i_OTHER_EXPENSE, false, false));
    		vPL.addAll(AccountTool.getAccountByType(i_INCOME_TAX, false, false));

    		Date dAsOf = _dAsOf;
    		dAsOf = DateUtil.getStartOfDayDate(DateUtil.addDays(dAsOf, 1));
    		List vLoc = LocationTool.getAllLocation();

    		double dTotalDebit = 0;
    		double dTotalCredit = 0;

    		for (int j = 0; j < vLoc.size(); j++)
    		{	
    			double dTotalDebitLoc = 0;
    			double dTotalCreditLoc = 0;

    			Location oLoc = (Location) vLoc.get(j);
    			for (int i = 0; i < vPL.size(); i++)
    			{
    				Account oAcc = (Account) vPL.get(i);
    				double[] dBal =  AccountBalanceTool.getBalance(
    						oAcc.getAccountId(),"loc", oLoc.getLocationId(), dAsOf);

    				if (dBal[i_BASE] != 0)
    				{
    					BigDecimal dAmt = new BigDecimal (dBal[i_BASE]);
    					JournalVoucherDetail oJVD = new JournalVoucherDetail();
    					oJVD.setAccountId(oAcc.getAccountId());
    					oJVD.setAccountCode(oAcc.getAccountCode());
    					oJVD.setAccountName(oAcc.getAccountName());
    					oJVD.setAmount(dAmt);
    					oJVD.setAmountBase(dAmt);
    					oJVD.setCurrencyId(oAcc.getCurrencyId());
    					oJVD.setCurrencyRate(bd_ONE);
    					if (oAcc.getAccountType() == i_REVENUE || 
    							oAcc.getAccountType() == i_OTHER_INCOME)
    					{
    						if (dBal [i_BASE] >= 0)
    						{
    							oJVD.setCreditAmount(bd_ZERO);
    							oJVD.setDebitAmount(dAmt);
    							oJVD.setDebitCredit(i_DEBIT);
    							dTotalDebit = dTotalDebit + dBal[i_BASE];
    							dTotalDebitLoc = dTotalDebitLoc + dBal[i_BASE];
    						}
    						else
    						{
    							dAmt = new BigDecimal (dBal[i_BASE] * -1);
    							oJVD.setCreditAmount(dAmt);
    							oJVD.setDebitAmount(bd_ZERO);
    							oJVD.setDebitCredit(i_CREDIT);
    							oJVD.setAmount(dAmt);
    							oJVD.setAmountBase(dAmt);
    							dTotalCredit = dTotalCredit + dAmt.doubleValue();
    							dTotalCreditLoc = dTotalCreditLoc + dAmt.doubleValue();							
    						}
    					}
    					else
    					{
    						if (dBal [i_BASE] >= 0)
    						{
    							oJVD.setCreditAmount(dAmt);
    							oJVD.setDebitAmount(bd_ZERO);
    							oJVD.setDebitCredit(i_CREDIT);
    							dTotalCredit = dTotalCredit + dBal[i_BASE];
    							dTotalCreditLoc = dTotalCreditLoc + dBal[i_BASE];								
    						}
    						else
    						{
    							dAmt = new BigDecimal (dBal[i_BASE] * -1);
    							oJVD.setDebitAmount(dAmt);
    							oJVD.setCreditAmount(bd_ZERO);
    							oJVD.setDebitCredit(i_DEBIT);
    							oJVD.setAmount(dAmt);
    							oJVD.setAmountBase(dAmt);
    							dTotalDebit = dTotalDebit + dAmt.doubleValue();
    							dTotalDebitLoc = dTotalDebitLoc + dAmt.doubleValue();								
    						}
    					}
    					oJVD.setDepartmentId("");
    					oJVD.setProjectId("");
    					oJVD.setLocationId(oLoc.getLocationId());
    					oJVD.setJournalVoucherId(oJV.getJournalVoucherId());
    					oJVD.setMemo("");

    					vJVD.add(oJVD);
    				}//if bal > 0				
    			}//for acc

    			double dRE = 0;
    			int iREDC = i_DEBIT;
    			if (dTotalDebitLoc > dTotalCreditLoc) {dRE = dTotalDebitLoc - dTotalCreditLoc; iREDC = i_CREDIT;}
    			if (dTotalCreditLoc > dTotalDebitLoc) {dRE = dTotalCreditLoc - dTotalDebitLoc; iREDC = i_DEBIT;}
    			BigDecimal bdRE = new BigDecimal(dRE);

    			Account oREAcc = AccountTool.getRetainedEarning(null);
    			JournalVoucherDetail oJVD = new JournalVoucherDetail();
    			oJVD.setAccountId(oREAcc.getAccountId());
    			oJVD.setAccountCode(oREAcc.getAccountCode());
    			oJVD.setAccountName(oREAcc.getAccountName());
    			oJVD.setAmount(bdRE);
    			oJVD.setAmountBase(bdRE);
    			oJVD.setCurrencyId(oREAcc.getCurrencyId());
    			oJVD.setCurrencyRate(bd_ONE);

    			if (iREDC == i_DEBIT)
    			{
    				oJVD.setCreditAmount(bd_ZERO);
    				oJVD.setDebitAmount(bdRE);
    				oJVD.setDebitCredit(i_DEBIT);
    				dTotalDebit = dTotalDebit + dRE;
    			}
    			else
    			{
    				oJVD.setCreditAmount(bdRE);
    				oJVD.setDebitAmount(bd_ZERO);
    				oJVD.setDebitCredit(i_CREDIT);
    				dTotalCredit = dTotalCredit + dRE;
    			}

    			oJVD.setDepartmentId("");
    			oJVD.setProjectId("");
    			oJVD.setLocationId(oLoc.getLocationId());
    			oJVD.setJournalVoucherId(oJV.getJournalVoucherId());
    			oJVD.setMemo("Retained Earning " + oLoc.getLocationCode() + " - " + oLoc.getLocationName());

    			vJVD.add(oJVD);
    		}//for loc

    		oJV.setTotalDebit (new BigDecimal (dTotalDebit));
    		oJV.setTotalCredit (new BigDecimal (dTotalCredit));

    		saveJV(oJV, vJVD, null);	

    		return oJV.getJournalVoucherId();
    	}
    	catch (Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		log.error(_oEx);
    		throw new NestableException (
    				"Creating Closing PL Journal Voucher Failed, ERROR: " + _oEx.getMessage(), _oEx);
    	}		
    }
}