package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.framework.tools.DateUtil;
import com.workingdogs.village.Record;

public class InventoryAnalysisTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( InventoryAnalysisTool.class );

	static InventoryAnalysisTool instance = null;
	
	public static synchronized InventoryAnalysisTool getInstance() 
	{
		if (instance == null) instance = new InventoryAnalysisTool();
		return instance;
	}	
	
	public static double countTurnoverPercentage (double _dTotalOut, double _dTotalIn, double _dQty)
		throws Exception
	{
		if (_dTotalOut < 0) _dTotalOut = _dTotalOut / -1;
		double dTurnover =  (_dTotalOut / (_dQty + _dTotalIn)) * 100;
		return dTurnover;
	}
			
	public static double getTotalInventoryTurnover (Date _dStart, Date _dEnd)
		throws Exception
	{
		double dTotalOut  = getTotalTransactionQtyAmount  (_dStart, _dEnd, 2);
		double dTotalIn   = getTotalTransactionQtyAmount  (_dStart, _dEnd, 1);
		double dQty       = getStockPositionAtDate        (_dStart);
		
		log.debug ("Total Inventory Out Amount : " + dTotalOut);
		log.debug ("Total Inventory In  Amount : " + dTotalIn);
		log.debug ("Total Qty In  : " + _dStart + " : " + dQty);
		log.debug ("Turnover Perc  : " + countTurnoverPercentage (dTotalOut, dTotalIn, dQty));
		
		return countTurnoverPercentage (dTotalOut, dTotalIn, dQty);
	}
	
	public static StringBuilder appendDate (StringBuilder oSQL, Date _dStart, Date _dEnd)
	{
		oSQL.append (" tr.transaction_date >= ");
		oSQL.append (oDB.getDateString(DateUtil.getStartOfDayDate(_dStart))); 
		if (_dEnd != null)
		{
			oSQL.append (" AND tr.transaction_date <= ");
			oSQL.append (oDB.getDateString(DateUtil.getEndOfDayDate(_dEnd))); 			
		}
		oSQL.append (" "); 			
		return oSQL;
	}
		
	public static double getTotalTransactionQtyAmount (Date _dStart, Date _dEnd, int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append (" SELECT SUM(tr.qty_changes)");
		oSQL.append (" FROM inventory_transaction tr ");
		oSQL.append (" WHERE ");

		appendDate (oSQL, _dStart, _dEnd);
		
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //in
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); //out
						
		log.debug (oSQL.toString());

		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			Record oData = (Record) vData.get(0);
			return oData.getValue(1).asDouble();
		}
		return 0;	
	}	

	public static double getStockPositionAtDate (Date _dStart)
		throws Exception
	{		
		double dTotalMove = 0;
		double dCurrentQty = 0;
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append (" SELECT SUM(tr.qty_changes)");
		oSQL.append (" FROM inventory_transaction  tr");
		oSQL.append (" WHERE ");		
		appendDate (oSQL, _dStart, null);
		
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			Record oData = (Record) vData.get(0);
			dTotalMove = oData.getValue(1).asDouble();			

			log.debug (" -- Total Move : " + dTotalMove);

			oSQL = new StringBuilder ();
			oSQL.append (" SELECT SUM(current_qty)");
			oSQL.append (" FROM inventory_location  ");			
			
			vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
			if (vData.size() > 0)
			{
				oData = (Record) vData.get(0);
				dCurrentQty = oData.getValue(1).asDouble();
				
				log.debug (" -- Current Qty : " + dCurrentQty);

				return dCurrentQty - dTotalMove;	
			}
		}
		return 0;	
	}			

	/**
	 * method that calculate the turnover percentage
	 * 
	 * 
	 */
		
	public static List calculateTurnoverPercentage (String _sIDKey, Map _hTotalOut, Map _hTotalIn, Map _hQty)
		throws Exception
	{
		List vID = (List)_hQty.get(_sIDKey);
		List vResult = new ArrayList (vID.size());
		for (int i = 0; i < vID.size(); i++)
		{
			List vData = new ArrayList(3);
			String sID = (String) vID.get(i);
						
			log.debug (_sIDKey + " : " + sID);
			
			List vTotalOut  = (List)_hTotalOut.get(sID); 
			List vTotalIn   = (List)_hTotalIn.get(sID);
			List vQty       = (List)_hQty.get(sID);

			log.debug ("vTotalOut : " + vTotalOut);
			log.debug ("vTotalIn : " + vTotalIn);
			log.debug ("vQty : " + vQty);

			if (vTotalOut != null && vTotalIn != null) 
			{
				double dTotalOut = ((BigDecimal)vTotalOut.get(2)).doubleValue();			
				double dTotalIn = ((BigDecimal)vTotalIn.get(2)).doubleValue();
				double dQty = ((BigDecimal)vQty.get(2)).doubleValue();

				if (dTotalOut < 0) dTotalOut = dTotalOut / -1;
				double dTurnover =  (dTotalOut / (dQty + dTotalIn)) * 100;
			
				vData.add (sID);
				vData.add (vQty.get(1));
				vData.add (new BigDecimal (dTurnover));
				vResult.add (vData);
				
				log.debug (_sIDKey + " : " + vQty.get(1));
				log.debug ("Total Out : " + dTotalOut);
				log.debug ("Total In  : " + dTotalIn);
				log.debug ("Start Qty : " + dQty);
				log.debug ("Turnover  : " + dTurnover);
			}
		}
		return vResult;
	}

	//////////////////////////////////////////////////////////////////////////////
	// Site
	//////////////////////////////////////////////////////////////////////////////

	public static List getInventoryTurnoverPerSite (Date _dStart, Date _dEnd)
	{
		try 
		{
			Map hTotalOut  = getTotalTransactionQtyPerSite  (_dStart, _dEnd, 2);
			Map hTotalIn   = getTotalTransactionQtyPerSite  (_dStart, _dEnd, 1);
			Map hQty       = getStockPositionAtDatePerSite  (_dStart);
			return calculateTurnoverPercentage ("SiteIDs",hTotalOut, hTotalIn, hQty);
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);	
			return null;
		}
	}
	
	public static Map getTotalTransactionQtyPerSite (Date _dStart, Date _dEnd, int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.site_id, SUM(tr.qty_changes) FROM ");
		oSQL.append (" inventory_transaction tr, site s, location l WHERE ");

		appendDate (oSQL, _dStart, _dEnd);
		
		oSQL.append ("  AND tr.location_id = l.location_id ");
		oSQL.append ("  AND l.site_id = s.site_id ");
		
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //in
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); //out		
		
		oSQL.append(" GROUP BY s.site_id ");
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		
		Map hResult = new HashMap (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(SiteTool.getSiteNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);
		}
		return hResult;                                                                                                                                                                                 
	}	
	
	public static Map getStockPositionAtDatePerSite (Date _dStart)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.site_id,  SUM(tr.qty_changes) "); 
		oSQL.append (" FROM inventory_transaction tr, site s, location l WHERE ");

		appendDate (oSQL, _dStart, null);

		oSQL.append ("  AND tr.location_id = l.location_id ");
		oSQL.append ("  AND l.site_id = s.site_id GROUP BY s.site_id ");
			
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		//map to store pair of site id and list of values
		Map hResult = new HashMap (vData.size());
		List vSiteID = new ArrayList (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(SiteTool.getSiteNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);	 
			vSiteID.add (oData.getValue(1).asString());
		}

		oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.site_id, SUM(tr.current_qty) ");
		oSQL.append ("FROM inventory_location tr, site s, location l WHERE ");
		oSQL.append (" tr.location_id = l.location_id ");
		oSQL.append (" AND l.site_id = s.site_id GROUP BY s.site_id ");
		vData = InventoryTransactionPeer.executeQuery(oSQL.toString());					
		
		Map hQtyResult = new HashMap (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(SiteTool.getSiteNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hQtyResult.put (oData.getValue(1).asString(), vRecordData);	 
		}		
		
		for (int i = 0; i < vSiteID.size(); i++)
		{
			String sSiteID = (String) vSiteID.get(i);
			List vResult = (List) hResult.get(sSiteID); 
			List vQtyResult = (List) hQtyResult.get(sSiteID); 
			
			BigDecimal oCurrentQty = (BigDecimal) vQtyResult.get(2);
			double dCurrentQty = oCurrentQty.doubleValue();
			
			BigDecimal oTotalMove = (BigDecimal) vResult.get(2);
			double dTotalMove = oTotalMove.doubleValue();
			
			dCurrentQty = dCurrentQty - dTotalMove;
			vResult.set(2, new BigDecimal(dCurrentQty));
			hResult.put(sSiteID, vResult);
		}
		hResult.put ("SiteIDs", vSiteID);
		return hResult;
	}	
	
	//////////////////////////////////////////////////////////////////////////////
	// END Site
	//////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////
	// Location
	//////////////////////////////////////////////////////////////////////////////

	public static List getInventoryTurnoverPerLocation (Date _dStart, Date _dEnd, String _sSiteID)
	{
		try 
		{
			Map hTotalOut  = getTotalTransactionQtyPerLocation  (_dStart, _dEnd, _sSiteID,2);
			Map hTotalIn   = getTotalTransactionQtyPerLocation  (_dStart, _dEnd, _sSiteID, 1);
			Map hQty       = getStockPositionAtDatePerLocation  (_dStart, _sSiteID);
			return calculateTurnoverPercentage ("LocationIDs",hTotalOut, hTotalIn, hQty);
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);	
			return null;
		}
	}
	
	public static Map getTotalTransactionQtyPerLocation (Date _dStart, Date _dEnd, String _sSiteID,int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.location_id, SUM(tr.qty_changes) ");
		oSQL.append (" FROM inventory_transaction tr, location l WHERE ");

		appendDate (oSQL, _dStart, _dEnd);

		oSQL.append (" AND tr.location_id = l.location_id ");
		oSQL.append (" AND l.site_id = '");
		oSQL.append (_sSiteID);
		oSQL.append ("' ");
		
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //in
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); //out		
		
		oSQL.append(" GROUP BY tr.location_id ");
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		
		Map hResult = new HashMap (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(LocationTool.getLocationNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);
		}
		return hResult;                                                                                                                                                                                 
	}	
	
	public static Map getStockPositionAtDatePerLocation (Date _dStart, String _sSiteID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.location_id, SUM(tr.qty_changes) ");
		oSQL.append (" FROM inventory_transaction tr, location l WHERE ");

		appendDate (oSQL, _dStart, null);

		oSQL.append ("  AND tr.location_id = l.location_id ");
		oSQL.append ("  AND l.site_id = '");
		oSQL.append (_sSiteID);
		oSQL.append ("' GROUP BY tr.location_id ");
			
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		//map to store pair of site id and list of values
		Map hResult = new HashMap (vData.size());
		List vLocationID = new ArrayList (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(LocationTool.getLocationNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);	 
			vLocationID.add (oData.getValue(1).asString());
		}

		oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.location_id, SUM(tr.current_qty) "); 
		oSQL.append (" FROM inventory_location tr, location l ");
		oSQL.append (" WHERE tr.location_id = l.location_id ");
		oSQL.append (" AND l.site_id = '");
		oSQL.append (_sSiteID);
		oSQL.append ("' GROUP BY tr.location_id ");
		vData = InventoryLocationPeer.executeQuery(oSQL.toString());					
		
		Map hQtyResult = new HashMap (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(LocationTool.getLocationNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hQtyResult.put (oData.getValue(1).asString(), vRecordData);	 
		}		
		
		for (int i = 0; i < vLocationID.size(); i++)
		{
			String sLocationID = (String) vLocationID.get(i);
			List vResult = (List) hResult.get(sLocationID); 
			List vQtyResult = (List) hQtyResult.get(sLocationID); 
			
			BigDecimal oCurrentQty = (BigDecimal) vQtyResult.get(2);
			double dCurrentQty = oCurrentQty.doubleValue();
			
			BigDecimal oTotalMove = (BigDecimal) vResult.get(2);
			double dTotalMove = oTotalMove.doubleValue();
			
			dCurrentQty = dCurrentQty - dTotalMove;
			vResult.set(2, new BigDecimal(dCurrentQty));
			hResult.put(sLocationID, vResult);
		}
		hResult.put ("LocationIDs", vLocationID);
		return hResult;
	}	
	
	//////////////////////////////////////////////////////////////////////////////
	// END Location
	//////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////
	// Kategori
	//////////////////////////////////////////////////////////////////////////////

	public static List getInventoryTurnoverPerKategori (Date _dStart, Date _dEnd, String _sLocationID)
	{
		try 
		{
			Map hTotalOut  = getTotalTransactionQtyPerKategori  (_dStart, _dEnd, _sLocationID,2);
			Map hTotalIn   = getTotalTransactionQtyPerKategori  (_dStart, _dEnd, _sLocationID, 1);
			Map hQty       = getStockPositionAtDatePerKategori  (_dStart, _sLocationID);
			return calculateTurnoverPercentage ("KategoriIDs",hTotalOut, hTotalIn, hQty);
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);	
			return null;
		}
	}
	
	public static Map getTotalTransactionQtyPerKategori (Date _dStart, Date _dEnd, String _sLocationID,int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, SUM(tr.qty_changes) ");
		oSQL.append (" FROM inventory_transaction tr, kategori k, item i ");
		oSQL.append (" WHERE tr.item_id = i.item_id AND i.kategori_id = k.kategori_id AND ");

		appendDate (oSQL, _dStart, _dEnd);

		oSQL.append (" AND tr.location_id = '");
		oSQL.append (_sLocationID);
		oSQL.append ("' ");
		
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //in
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); //out		
		
		oSQL.append(" GROUP BY k.kategori_id ");
		
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		
		Map hResult = new HashMap (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(KategoriTool.getDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);
		}
		return hResult;                                                                                                                                                                                 
	}	
	
	public static Map getStockPositionAtDatePerKategori (Date _dStart, String _sLocationID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, SUM(tr.qty_changes) FROM inventory_transaction AS tr, kategori AS k, item as i ");
		oSQL.append (" WHERE tr.item_id = i.item_id AND i.kategori_id = k.kategori_id AND ");

		appendDate (oSQL, _dStart, null);

		oSQL.append (" AND tr.location_id = '");
		oSQL.append (_sLocationID);
		oSQL.append ("' GROUP BY k.kategori_id ");
			
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		//map to store pair of site id and list of values
		Map hResult = new HashMap (vData.size());
		List vKategoriID = new ArrayList (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(KategoriTool.getDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);	 
			vKategoriID.add (oData.getValue(1).asString());
		}

		oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, SUM(tr.current_qty) ");
		oSQL.append (" FROM inventory_location tr, item i, kategori k");
		oSQL.append (" WHERE i.item_id = tr.item_id AND i.kategori_id = k.kategori_id AND tr.location_id = '");
		oSQL.append (_sLocationID);
		oSQL.append ("' GROUP BY k.kategori_id ");
		vData = InventoryLocationPeer.executeQuery(oSQL.toString());					
		
		Map hQtyResult = new HashMap (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(KategoriTool.getDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hQtyResult.put (oData.getValue(1).asString(), vRecordData);	 
		}		
		
		for (int i = 0; i < vKategoriID.size(); i++)
		{
			String sKategoriID = (String) vKategoriID.get(i);
			List vResult = (List) hResult.get(sKategoriID); 
			List vQtyResult = (List) hQtyResult.get(sKategoriID); 
			
			BigDecimal oCurrentQty = (BigDecimal) vQtyResult.get(2);
			double dCurrentQty = oCurrentQty.doubleValue();
			
			BigDecimal oTotalMove = (BigDecimal) vResult.get(2);
			double dTotalMove = oTotalMove.doubleValue();
			
			dCurrentQty = dCurrentQty - dTotalMove;
			vResult.set(2, new BigDecimal(dCurrentQty));
			hResult.put(sKategoriID, vResult);
		}
		hResult.put ("KategoriIDs", vKategoriID);
		return hResult;
	}	
	
	//////////////////////////////////////////////////////////////////////////////
	// END Kategori
	//////////////////////////////////////////////////////////////////////////////
	

	//////////////////////////////////////////////////////////////////////////////
	// Item
	//////////////////////////////////////////////////////////////////////////////

	public static List getInventoryTurnoverPerItem (Date _dStart, Date _dEnd, 
													String _sLocationID, String _sKategoriID)
	{
		try 
		{
			Map hTotalOut  = getTotalTransactionQtyPerItem (_dStart, _dEnd, _sLocationID, _sKategoriID, 2);
			Map hTotalIn   = getTotalTransactionQtyPerItem (_dStart, _dEnd, _sLocationID, _sKategoriID, 1);
			Map hQty       = getStockPositionAtDatePerItem (_dStart, _sLocationID, _sKategoriID);
			return calculateTurnoverPercentage ("ItemIDs",hTotalOut, hTotalIn, hQty);
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);	
			return null;
		}
	}
	
	public static Map getTotalTransactionQtyPerItem (Date _dStart, Date _dEnd, 
													 String _sLocationID, String _sKategoriID, int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.item_id, SUM(tr.qty_changes) ");
		oSQL.append ("FROM inventory_transaction tr, item i ");
		oSQL.append (" WHERE tr.item_id = i.item_id AND ");
		
		appendDate (oSQL, _dStart, _dEnd);

		oSQL.append (" AND tr.location_id = '");
		oSQL.append (_sLocationID);
		oSQL.append ("' AND i.kategori_id = '");
		oSQL.append (_sKategoriID);
		oSQL.append ("' ");
		
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //in
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); //out		
		
		oSQL.append(" GROUP BY tr.item_id ");
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		
		Map hResult = new HashMap (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(ItemTool.getItemDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);
		}
		return hResult;                                                                                                                                                                                 
	}	
	
	public static Map getStockPositionAtDatePerItem (Date _dStart, String _sLocationID,String _sKategoriID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.item_id, SUM(tr.qty_changes) ");
		oSQL.append (" FROM inventory_transaction tr, item i ");
		oSQL.append (" WHERE tr.item_id = i.item_id AND ");
		
		appendDate (oSQL, _dStart, null);

		oSQL.append (" AND tr.location_id = '");
		oSQL.append (_sLocationID);
		oSQL.append ("' AND i.kategori_id = '");
		oSQL.append (_sKategoriID);
		oSQL.append ("' GROUP BY tr.item_id ");	
		
		log.debug (oSQL.toString());
		
		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		//map to store pair of site id and list of values
		Map hResult = new HashMap (vData.size());
		List vItemID = new ArrayList (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(ItemTool.getItemDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hResult.put (oData.getValue(1).asString(), vRecordData);	 
			vItemID.add (oData.getValue(1).asString());
		}

		oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.item_id, SUM(tr.current_qty) FROM inventory_location tr, item i ");
		oSQL.append (" WHERE i.item_id = tr.item_id AND tr.location_id = '");
		oSQL.append (_sLocationID);
		oSQL.append ("' AND i.kategori_id = '");
		oSQL.append (_sKategoriID);
		oSQL.append ("' GROUP BY tr.item_id ");		
		vData = InventoryLocationPeer.executeQuery(oSQL.toString());					
		
		Map hQtyResult = new HashMap (vData.size());
		
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(ItemTool.getItemDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			hQtyResult.put (oData.getValue(1).asString(), vRecordData);	 
		}		
		
		for (int i = 0; i < vItemID.size(); i++)
		{
			String sItemID = (String) vItemID.get(i);
			List vResult = (List) hResult.get(sItemID); 
			List vQtyResult = (List) hQtyResult.get(sItemID); 
			
			BigDecimal oCurrentQty = (BigDecimal) vQtyResult.get(2);
			double dCurrentQty = oCurrentQty.doubleValue();
			
			BigDecimal oTotalMove = (BigDecimal) vResult.get(2);
			double dTotalMove = oTotalMove.doubleValue();
			
			dCurrentQty = dCurrentQty - dTotalMove;
			vResult.set(2, new BigDecimal(dCurrentQty));
			hResult.put(sItemID, vResult);
		}
		hResult.put ("ItemIDs", vItemID);
		return hResult;
	}	
	
	//////////////////////////////////////////////////////////////////////////////
	// END Item
	//////////////////////////////////////////////////////////////////////////////	

}