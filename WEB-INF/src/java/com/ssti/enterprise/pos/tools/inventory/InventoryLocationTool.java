package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.InventoryLocationManager;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * InventoryLocation OM business logic
 * InventoryLocation keeps item location qty cost record
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InventoryLocationTool.java,v 1.21 2009/05/04 02:04:27 albert Exp $ <br>
 *
 * <pre>
 * $Log: InventoryLocationTool.java,v $
 * 
 * 2017-09-30
 * - Change method update, use BigDecimal calculation & comparison do not use double
 *   very small double amount like 0.00000x can cause messed up cost
 * 
 * 2015-07-15
 * - Move getDataByItemID to InventoryLocationManager to enable cache
 *
 * </pre><br>
 */
public class InventoryLocationTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( InventoryLocationTool.class );
	
	///////////////////////////////////////////////////////////////////////////
	//getter methods
	//////////////////////////////////////////////////////////////////////////
	/**
	 * get all inventory location, used by remote store setup
	 * @return List of InventoryLocation OM
	 * @throws Exception
	 */
	public static List getAllInventoryLocation (String _sLocationID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(InventoryLocationPeer.LOCATION_ID, _sLocationID);
		}
		return InventoryLocationPeer.doSelect (oCrit);
	}	
	
	public static List getDataByItemID (String _sItemID) 
		throws Exception
	{
		return getDataByItemID(_sItemID, null);
	}	
	
	/**
	 * get by item id
	 * @param _sItemID
	 * @return List of InventorLocation for certain Item
	 * @throws Exception
	 */
	public static List getDataByItemID (String _sItemID, Connection _oConn) 
		throws Exception
	{
		return InventoryLocationManager.getInstance().getByItemID(_sItemID, _oConn);
	}

	public static InventoryLocation getDataByItemAndLocationID (String _sItemID, String _sLocationID) 
		throws Exception
	{		
		return getDataByItemAndLocationID (_sItemID, _sLocationID, null);
	}
	
	/**
	 * get inventory location by item and location id
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _oConn
	 * @return InventoryLocation OM
	 * @throws Exception
	 */
	public static InventoryLocation getDataByItemAndLocationID (String _sItemID, 
																String _sLocationID, 
																Connection _oConn) 
		throws Exception
	{
		return InventoryLocationManager.getInstance().getInventoryLocation(_sItemID, _sLocationID, _oConn);
	}

	/**
	 * get inventory location by item and location id for update
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _oConn
	 * @return InventoryLocation OM for update
	 * @throws Exception
	 */
	private static InventoryLocation getForUpdate (String _sItemID, String _sLocationID, Connection _oConn) 
		throws Exception
	{		
		Criteria oCrit = new Criteria();
		oCrit.add (InventoryLocationPeer.ITEM_ID, _sItemID);
		oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
		oCrit.setForUpdate(true);
		List vData = InventoryLocationPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (InventoryLocation) vData.get(0);
		}
		return null;
	}


	/**
	 * get current qty of an item in a location only valid for inventory_type
	 * not valid for other item type as they are not kept in inv location
	 * @param _sItemID
	 * @param _sLocationID
	 * @return qty on hand
	 * @throws Exception
	 */
	public static BigDecimal getInventoryOnHand (String _sItemID, String _sLocationID) 
		throws Exception
	{
		return getInventoryOnHand(_sItemID, _sLocationID, null);
	}
	
	/**
	 * get current qty of an item in a location only valid for inventory_type
	 * not valid for other item type as they are not kept in inv location
	 * @param _sItemID
	 * @param _sLocationID
	 * @return qty on hand
	 * @throws Exception
	 */
	public static BigDecimal getInventoryOnHand (String _sItemID, String _sLocationID, Connection _oConn) 
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			InventoryLocation oInv = getDataByItemAndLocationID (_sItemID, _sLocationID, _oConn);
			if (oInv != null) 
			{
				return oInv.getCurrentQty();
			}
		}
		else
		{
			double dTotalQty = 0;
			List vInv = getDataByItemID (_sItemID, _oConn);
			for (int i = 0; i < vInv.size(); i++)
			{
				InventoryLocation oInv = (InventoryLocation) vInv.get(i);
				dTotalQty += oInv.getCurrentQty().doubleValue();
			}
			return new BigDecimal(dTotalQty);
		}
		return bd_ZERO;
	}
	
	public static BigDecimal getItemCost (String _sItemID, String _sLocationID) 
		throws Exception
	{
		return getItemCost (_sItemID, _sLocationID, null);
	}
	
	public static BigDecimal getItemCost (String _sItemID, String _sLocationID, Connection _oConn) 
		throws Exception
	{
		return getItemCost (_sItemID, _sLocationID, new Date(),_oConn);
	}	
	
	/**
	 * get current cost of an item in a location
	 * if item type is grouping then the cost is 
	 * @param _sItemID
	 * @param _sLocationID
	 * @return item cost
	 * @throws Exception
	 */
	public static BigDecimal getItemCost (String _sItemID, 
										  String _sLocationID, 
										  Date _dAsOf, 
										  Connection _oConn) 
		throws Exception
	{
	    double dCost = 0;
	    Item oItem = ItemTool.getItemByID (_sItemID, _oConn);
	    if (oItem != null)
	    {
		    if (oItem.getItemType() == i_INVENTORY_PART)
		    {
		        InventoryTransaction oInv = 
		        	InventoryTransactionTool.getLastTrans (_sItemID, _sLocationID, _dAsOf, _oConn);
						        
		        if (oInv != null) 
		        {
		        	double dQtyBalance = oInv.getQtyBalance().doubleValue();
		        	if (!b_GLOBAL_COSTING && dQtyBalance != 0)
		        	{
		        		dCost = oInv.getValueBalance().doubleValue() / oInv.getQtyBalance().doubleValue();
		        	}
		        	else
		        	{
		        		dCost = oInv.getCost().doubleValue();
		        	}
		        }
		    }
		    else if (oItem.getItemType() == i_GROUPING)
		    {
		        List vGroup = ItemGroupTool.getItemGroupByGroupID (_sItemID,_oConn);
		        for (int i = 0; i < vGroup.size(); i++)
		        {
		            ItemGroup oGroup = (ItemGroup) vGroup.get(i);
		            double dGroupPartCost = 
		            	getItemCost(oGroup.getItemId(), _sLocationID, _dAsOf, _oConn).doubleValue();
		            	
		            Unit oUnit = UnitTool.getUnitByID (oGroup.getUnitId(), _oConn);
                    dCost += dGroupPartCost * oUnit.getValueToBase().doubleValue() * oGroup.getQty().doubleValue();
		        }
		    }
		}
		return new BigDecimal (dCost);
	}

	/**
	 * get average cost and total qty of an item
	 * @param _sItemID
	 * @return List of BigDecimal [AvgCost, TotalQty]
	 * @throws Exception
	 */
	public static List getAvgCostAndTotalQtyByID (String _sItemID) 
		throws Exception
	{
		List vResult = new ArrayList(2);
	    if (StringUtil.isNotEmpty(_sItemID))
	    {
            double dTotalQty = 0;
            double dTotalCost = 0;
            double dAvgCost = 0;

		    List vData = getDataByItemID(_sItemID);	
            for (int i = 0; i < vData.size(); i++)
            {
                InventoryLocation oInvLoc = (InventoryLocation) vData.get(i);
                double dCurrentQty = oInvLoc.getCurrentQty().doubleValue();
                dTotalCost = dTotalCost + ( dCurrentQty * oInvLoc.getItemCost().doubleValue());
                dTotalQty = dTotalQty + oInvLoc.getCurrentQty().doubleValue();
            }        	        
            if (dTotalQty != 0) dAvgCost = dTotalCost / dTotalQty;
		    vResult.add(new BigDecimal(dAvgCost));
		    vResult.add(new BigDecimal(dTotalQty));
		}
		return vResult;      
	}

	public static BigDecimal getAvgCost(String _sItemID) 
		throws Exception
	{
		List vData = getAvgCostAndTotalQtyByID(_sItemID);
		if (vData != null && vData.size() > 0)
		{
			return (BigDecimal)vData.get(0);
		}
		return bd_ZERO;
	}

	public static BigDecimal getTotalQty(String _sItemID) 
		throws Exception
	{
		List vData = getAvgCostAndTotalQtyByID(_sItemID);
		if (vData != null && vData.size() > 0)
		{
			return (BigDecimal)vData.get(1);
		}
		return bd_ZERO;
	}
	
	public static Record getDataBySKUAndLocationID (String _sSKU, String _sLocationID) 
		throws Exception
	{
		return 	getDataBySKUAndLocationID (_sSKU, _sLocationID, null);
	}
	
	/**
	 * get inventory location by sku and location id
	 * return Record which have the value {"sku_name", "total_qty", "avg_cost"}
	 * 
	 * @param _sSKU
	 * @param _sLocationID
	 * @return Record [description, current_qty_, item_cost]
	 * @throws Exception
	 */
	public static Record getDataBySKUAndLocationID (String _sSKU, String _sLocationID, Connection _oConn) 
		throws Exception
	{		
	    StringBuilder oSQL = new StringBuilder();
	    oSQL.append (" SELECT it.item_sku, SUM(il.current_qty), AVG(il.item_cost)")
			.append (" FROM inventory_location il, item it ")
	        .append (" WHERE il.item_id = it.item_id ")
			.append (" AND it.item_sku = '").append (_sSKU).append ("' ");
	    
	    if (StringUtil.isNotEmpty(_sLocationID))
		{
		    oSQL.append (" AND il.location_id = '").append (_sLocationID).append ("' ");
		}			
        oSQL.append (" GROUP BY it.item_sku");	    
        
        //log.debug(oSQL);
        List vData = new ArrayList();
        if (_oConn != null)
        {
        	vData = InventoryLocationPeer.executeQuery(oSQL.toString(), false, _oConn);	
        }
        else
        {
        	vData = InventoryLocationPeer.executeQuery(oSQL.toString());	        
        }
		if (vData.size() > 0)
		{
            return (Record) vData.get(0);
		}
		return null;      
	}
	
	public static Record getDataByKategoriAndLocationID (String _sKategoriID, String _sLocationID, Connection _oConn) 
		throws Exception
	{		
		List vChild = KategoriTool.getChildIDList(_sKategoriID);
		vChild.add(_sKategoriID);
		String sKat = SqlUtil.convertToINMode(vChild);
		
	    StringBuilder oSQL = new StringBuilder();
	    oSQL.append (" SELECT SUM(il.current_qty), AVG(il.item_cost), SUM(il.current_qty * il.item_cost)")
			.append (" FROM inventory_location il, item it ")
	        .append (" WHERE il.item_id = it.item_id ");
	    
	    if (StringUtil.isNotEmpty(sKat) && !sKat.equals("()"))
		{
			oSQL.append (" AND it.kategori_id IN ").append (sKat).append (" ");
		}
	    if (StringUtil.isNotEmpty(_sLocationID))
		{
		    oSQL.append (" AND il.location_id = '").append (_sLocationID).append ("' ");
		}			
	    
	    log.debug(oSQL);
	    List vData = new ArrayList();
	    if (_oConn != null)
	    {
	    	vData = InventoryLocationPeer.executeQuery(oSQL.toString(), false, _oConn);	
	    }
	    else
	    {
	    	vData = InventoryLocationPeer.executeQuery(oSQL.toString());	        
	    }
		if (vData.size() > 0)
		{
	        return (Record) vData.get(0);
		}
		return null;      
	}

	public static BigDecimal getBalanceAsOf (String _sItemID, String _sItemSKU, String _sLocationID, Date _dAsOf) 
		throws Exception
	{
		return getBalanceAsOf (_sItemID, _sItemSKU, _sLocationID, _dAsOf, null);
	}
	
	/**
	 * get <b>QTY Balance</b> as of by sku and location id
	 * 
	 * @param _sItemID
	 * @param _sItemSKU
	 * @param _sLocationID
	 * @param _dAsOf
	 * @return qty balance as of
	 * @throws Exception
	 */
	public static BigDecimal getBalanceAsOf (String _sItemID, 
										     String _sItemSKU, 
											 String _sLocationID, 
											 Date _dAsOf, 
											 Connection _oConn) 
		throws Exception
	{
		BigDecimal oChanges = bd_ZERO;
	    double dBalance = 0;
		if (StringUtil.isNotEmpty(_sItemID))
	    {
			oChanges = InventoryTransactionTool.getTotalQtyChanges(_sItemID, "", "", _sLocationID, _dAsOf, _oConn );
			dBalance = getInventoryOnHand(_sItemID, _sLocationID, _oConn).doubleValue();
	    }
	    else if (StringUtil.isNotEmpty(_sItemSKU))
	    {
			oChanges = InventoryTransactionTool.getTotalQtyChanges("", _sItemSKU, "", _sLocationID, _dAsOf, _oConn );	    
		    Record oQty = getDataBySKUAndLocationID (_sItemSKU, _sLocationID, _oConn);
		    if (oQty != null)
		    {
		        dBalance = oQty.getValue(2).asDouble();
		    }
	    }	
        double dChanges = oChanges.doubleValue();	    
        dBalance = dBalance + (dChanges * -1);
        return new BigDecimal(dBalance);
	}

	/**
	 * get <b>VALUE Balance</b> as of by sku and location id
	 * 
	 * @param _sItemSKU
	 * @param _sLocationID
	 * @param _dAsOf
	 * @return value as of
	 * @throws Exception
	 */
	public static BigDecimal getValueAsOf (String _sItemID, 
										   String _sItemSKU, 
										   String _sKategoriID,
										   String _sLocationID, 
										   Date _dAsOf,
										   Connection _oConn) 
		throws Exception
	{
	    BigDecimal oChanges = InventoryTransactionTool
	    	.getTotalValueChanges(_sItemID, _sItemSKU, _sKategoriID, _sLocationID, _dAsOf, null);
	    
	    double dBalance = 0;
	    
		if (StringUtil.isNotEmpty(_sItemID))
	    {
			oChanges = InventoryTransactionTool.getTotalValueChanges(_sItemID, "", "", _sLocationID, _dAsOf, _oConn);	    			
			InventoryLocation oInvLoc = getDataByItemAndLocationID(_sItemID, _sLocationID, _oConn);
			if (oInvLoc != null)
			{
				dBalance = oInvLoc.getCurrentQty().doubleValue() * oInvLoc.getItemCost().doubleValue();
			}
	    }
	    else if (StringUtil.isNotEmpty(_sItemSKU))
	    {
			oChanges = InventoryTransactionTool.getTotalValueChanges("", _sItemSKU, "", _sLocationID, _dAsOf, _oConn);	    
		    Record oValue = getDataBySKUAndLocationID (_sItemSKU, _sLocationID, _oConn);
		    if (oValue != null)
		    {
		        dBalance = oValue.getValue(2).asDouble() * oValue.getValue(3).asDouble();
		    }
	    }
	    else if (StringUtil.isNotEmpty(_sKategoriID))
	    {
			oChanges = InventoryTransactionTool.getTotalValueChanges("", "", _sKategoriID, _sLocationID, _dAsOf, _oConn);	    
		    Record oValue = getDataByKategoriAndLocationID (_sKategoriID, _sLocationID, _oConn);
		    if (oValue != null)
		    {
		        dBalance = oValue.getValue(3).asDouble();
		    }
	    }
	    
	    double dChanges = oChanges.doubleValue();
	    dBalance = dBalance + (dChanges * -1);
	    return new BigDecimal(dBalance);
	}

	///////////////////////////////////////////////////////////////////////////
	//finder methods
	///////////////////////////////////////////////////////////////////////////

	/**
	 * find inventory location data
	 * called by some report templates
	 * 
	 * @param _sLocationID
	 * @param _iStockStatus
	 * @param _iSortBy
	 * @param _sKategoriID
	 * @param _vKategoriID
	 * @param _bGroupBySKU
	 * @return List of InventoryLocation
	 * @throws Exception
	 */
	public static List findData (String _sLocationID,
	                             int _iStockStatus, 
	                             int _iSortBy,
	                             String _sKategoriID,
	                             List _vKategoriID,
	                             boolean _bGroupBySKU) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
	    
	    //stock status
	    if (_iStockStatus == 1) {
			oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
			oCrit.add (InventoryLocationPeer.CURRENT_QTY, (Object)ItemPeer.MINIMUM_STOCK, Criteria.LESS_EQUAL);  			
	    }
	    if (_iStockStatus == 2) {
			oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
			oCrit.add (InventoryLocationPeer.CURRENT_QTY, (Object)ItemPeer.REORDER_POINT, Criteria.LESS_EQUAL);  			
	    }
	    if (_iStockStatus == 3) {
			oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.LESS_THAN);  			
	    }
	    if (_iStockStatus == 4) {
			oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.GREATER_THAN);  			
	    }
	    if (_iStockStatus == 5) {
			oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
			oCrit.add (InventoryLocationPeer.CURRENT_QTY, (Object)ItemPeer.MAXIMUM_STOCK, Criteria.GREATER_EQUAL);  			
	    }
	    //sort by
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(InventoryLocationPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
			oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
		else if (_iSortBy == 3) {
			oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
			oCrit.addAscendingOrderByColumn(ItemPeer.DESCRIPTION);
		}
		else if (_iSortBy == 4) {
			oCrit.addAscendingOrderByColumn(InventoryLocationPeer.CURRENT_QTY);
		}
		
		if(!_sKategoriID.equals(""))
		{
		    oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
		    oCrit.add(ItemPeer.KATEGORI_ID,_sKategoriID);   
	    }
	    if(_vKategoriID != null && _vKategoriID.size()>0)
	    {
   		    oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
            oCrit.addIn(ItemPeer.KATEGORI_ID, _vKategoriID);
	    }
	    if (_bGroupBySKU)
	    {
	        //oCrit.addGroupByColumn (ItemPeer.ITEM_SKU);
	    }
	    
		log.debug (oCrit);
		List vData = InventoryLocationPeer.doSelect (oCrit);
		return vData;
	}	

	///////////////////////////////////////////////////////////////////////////
	//update methods
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * Delete inventory transaction 
	 * 
	 * @param _oTrans
	 * @param _oConn
	 * @throws Exception
	 */
	public static void delete (InventoryTransaction _oTrans, Connection _oConn)
		throws Exception	
	{
    	InventoryLocation oInv = getForUpdate(_oTrans.getItemId(), _oTrans.getLocationId(), _oConn);
		
    	InventoryLocationManager.getInstance().refreshCache(_oTrans.getItemId(), _oTrans.getLocationId());

    	if (log.isDebugEnabled())
    	{
    		log.debug ( "\n\n** DELETE Inventory Location: " + oInv);
    	}
    	
    	if (oInv != null) 
    	{    		
    		Criteria oCrit = new Criteria();
    		oCrit.add(InventoryLocationPeer.INVENTORY_LOCATION_ID, oInv.getInventoryLocationId());
    		InventoryLocationPeer.doDelete(oCrit, _oConn);
    	}
	}
	
	/**
	 * Update inventory location from last inventory transaction of certain item 
	 * in certain location (ONLY USED BY NON GLOBAL COSTING)
	 * 
	 * @param _oTrans
	 * @param _oConn
	 * @throws Exception
	 */
	public static void update (InventoryTransaction _oTrans, Connection _oConn)
		throws Exception
	{ 	
		validate(_oConn);
	    try
	    {			    
	    	//get inventory location object
	    	InventoryLocation oInv = getForUpdate(_oTrans.getItemId(), _oTrans.getLocationId(), _oConn);
	    	
	    	if (log.isDebugEnabled())
	    	{
	    		log.debug ( "\n\n** START Update Inventory Location: " + oInv);
	    	}
	    	
	    	//if inventory location data exist
	    	if (oInv == null)
	    	{	    		
	    		//data not exist so we should create it
	    		oInv = new InventoryLocation();
	    		oInv.setLocationId 	  (_oTrans.getLocationId());
	    		oInv.setLocationName  (_oTrans.getLocationName());
	    		oInv.setItemId 		  (_oTrans.getItemId());
	    		oInv.setItemCode 	  (_oTrans.getItemCode());
	    		oInv.setStartStock 	  (_oTrans.getQtyBalance());

	    		oInv.setInventoryLocationId ( IDGenerator.generateSysID() );
	    	}	
	    	
	    	BigDecimal bdCost = bd_ZERO;	    	
	    	if (_oTrans.getQtyBalance().compareTo(bd_ZERO) != 0 && _oTrans.getValueBalance().compareTo(bd_ZERO) > 0)
	    	{
	    		bdCost = _oTrans.getValueBalance().divide(_oTrans.getQtyBalance(), i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
	    	}
	    	else
	    	{
	    		bdCost = _oTrans.getCost();
	    	}
	    	
		    //validate minus Qty	
			if (LocationTool.getInventoryType(_oTrans.getLocationId(), _oConn) != i_INV_MINUS)
	    	{
	    		if (_oTrans.getQtyBalance().doubleValue() < 0)
	    		{
	    			StringBuilder oSB = new StringBuilder();
	    			oSB.append("Item ").append(_oTrans.getItemCode())
	    			   .append(" qty on hand (").append(oInv.getCurrentQty())
	    			   .append(") not enough, trans qty changes (").append(_oTrans.getQtyChanges()).append(")")
	    			   .append(" Location (").append(_oTrans.getLocationName()).append(")");
	    			throw new Exception (oSB.toString());
	    		}
	    	}	    	
	    	
			//update last in / last out
			if (_oTrans.getTransactionType() != i_INV_TRANS_TRANSFER_OUT || 
				_oTrans.getTransactionType() != i_INV_TRANS_TRANSFER_IN)
			{
				if (_oTrans.getQtyChanges().doubleValue() > 0) oInv.setLastIn(_oTrans.getTransactionDate());
				if (_oTrans.getQtyChanges().doubleValue() < 0) oInv.setLastOut(_oTrans.getTransactionDate());				
			}
												
    		oInv.setCurrentQty (_oTrans.getQtyBalance());	    		
    		oInv.setItemCost (bdCost);    		
	    	oInv.setUpdateDate(new Date());
	    	oInv.save (_oConn);		
	    	
	    	InventoryLocationManager.getInstance().refreshCache(oInv);
	    	if (log.isDebugEnabled())
	    	{
	    		log.debug ( "\n\n** END Update Inventory Location RESULT : " + oInv);
	    	}

	    }
		catch (Exception _oEx) 
		{
			String sError = "Update Inventory Location Data Failed : " + _oEx.getMessage();
			log.error ( sError , _oEx );
			throw new NestableException (sError, _oEx);		
		}	
	}	
	
	///////////////////////////////////////////////////////////////////////////
	//GLOBAL COSTING methods
	///////////////////////////////////////////////////////////////////////////	
	
	public static boolean isGlobalCosting () 
	{
		return b_GLOBAL_COSTING;
	}
	
	/**
	 * get inventory location by item for update
	 * 
	 * @param _sItemID
	 * @param _oConn
	 * @return List of inventory location for an item
	 * @throws Exception
	 */
	private static List getForGlobalUpdate (String _sItemID, Connection _oConn) 
		throws Exception
	{		
		Criteria oCrit = new Criteria();
		oCrit.add (InventoryLocationPeer.ITEM_ID, _sItemID);
		oCrit.setForUpdate(true);
		return InventoryLocationPeer.doSelect (oCrit, _oConn);
	}
	
	private static InventoryLocation filterByLocationID (List _vInv, String _sLocationID, Connection _oConn) 
		throws Exception
	{		
		for (int i = 0; i < _vInv.size(); i++) 
		{
			InventoryLocation oInv = (InventoryLocation) _vInv.get(i);
			if (oInv.getLocationId().equals(_sLocationID))
			{
				return oInv;
			}
		}
		return null;
	}
	
		
	/**
	 * update inventory location from inv trans using global costing
	 * the main difference is the way we set current qty
	 * in global costing qty balance will contains global qty balance 
	 * so inventory location is not updated by last inv trans qty balance value
	 * 
	 * @param _oTrans
	 * @param _oLast
	 * @param _bCancel
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateGlobal (InventoryTransaction _oTrans, 
									 InventoryTransaction _oLast,
									 boolean _bIsNew,
									 boolean _bCancel, 
									 Connection _oConn)
		throws Exception
	{ 	
		validate(_oConn);
	    try
	    {			    
	    	List vInv = getForGlobalUpdate(_oTrans.getItemId(), _oConn);
	    	InventoryLocation oInv = filterByLocationID(vInv, _oTrans.getLocationId(), _oConn);
	    	
	    	if (log.isDebugEnabled())
	    	{
	    		log.debug ( "\n\n** START Update Inventory Location(GLOBAL): " + oInv);
	    	}
	    	
	    	//if inventory location data exist
	    	if (oInv == null)
	    	{	    		
	    		//data not exist so we should create it
	    		oInv = new InventoryLocation();
	    		oInv.setLocationId 	  (_oTrans.getLocationId());
	    		oInv.setLocationName  (_oTrans.getLocationName());
	    		oInv.setItemId 		  (_oTrans.getItemId());
	    		oInv.setItemCode 	  (_oTrans.getItemCode());
	    		oInv.setStartStock 	  (_oTrans.getQtyChanges());
	    		
	    		oInv.setInventoryLocationId ( IDGenerator.generateSysID() );
	    		
	    		//use qty changes as current qty
	    		oInv.setCurrentQty (_oTrans.getQtyChanges());
	    	}
	    	else //add or substract current qty with qty changes
	    	{ 
	    		//only update qty from new inv trans
	    		if ((_bIsNew && !_bCancel) || (_bCancel && !_bIsNew))
	    		{
		    		double dCurrentQty = oInv.getCurrentQty().doubleValue();
		    		if (!_bCancel)
		    		{
		    			dCurrentQty = dCurrentQty + _oTrans.getQtyChanges().doubleValue();
		    		}
		    		else
		    		{
		    			dCurrentQty = dCurrentQty - _oTrans.getQtyChanges().doubleValue();
		    		}
		    		oInv.setCurrentQty (new BigDecimal(dCurrentQty));
	    		}
	    	}	 
	    	
	    	double dCost = _oLast.getCost().doubleValue();
	    	if (_oLast.getQtyBalance().doubleValue() != 0) //if qty balance not 0 then calculate the cost
	    	{
	    		dCost = _oLast.getValueBalance().doubleValue() / _oLast.getQtyBalance().doubleValue();
	    	}
	    	//TODO: if qty balance == 0 || < 0 what cost should we use? 
	    	BigDecimal bdCost = new BigDecimal(dCost);
    		oInv.setItemCost(bdCost); //use last inv trans cost for both new and updated
	    	
	    	//validate Qty
	    	if (LocationTool.getInventoryType(_oTrans.getLocationId(), _oConn) != i_INV_MINUS)
	    	{
	    		if (oInv.getCurrentQty().doubleValue() < 0)
	    		{
	    			StringBuilder oSB = new StringBuilder();
	    			oSB.append("Item ").append(_oTrans.getItemCode())
	    			.append(" qty on hand (").append(oInv.getCurrentQty())
	    			.append(") not enough, trans qty changes (").append(_oTrans.getQtyChanges()).append(")")
	    			.append(" Location (").append(_oTrans.getLocationName()).append(")");
	    			throw new Exception (oSB.toString());
	    		}
	    	}	    	
	    	
	    	oInv.setUpdateDate(new Date());
	    	oInv.save (_oConn);		
	    		    	
	    	InventoryLocationManager.getInstance().refreshCache(oInv);
	    	
	    	//update all others inv loc cost 
	    	for (int i = 0; i < vInv.size(); i++)
	    	{
	    		InventoryLocation oOtherInv = (InventoryLocation) vInv.get(i);
	    		if (!oOtherInv.getLocationId().equals(_oTrans.getLocationId()))
	    		{
		    		oOtherInv.setItemCost (bdCost); 
			    	oOtherInv.setUpdateDate(new Date());
			    	oOtherInv.save (_oConn);
			    	InventoryLocationManager.getInstance().refreshCache(oOtherInv);
	    		}
	    	}
	    	
	    	if (log.isDebugEnabled())
	    	{
	    		log.debug ( "\n\n** END Update Inventory Location RESULT(GLOBAL) : " + oInv);
	    	}
	    }
		catch (Exception _oEx) 
		{
			String sError = "Update Inventory Location Data Failed : " + _oEx.getMessage();
			log.error ( sError , _oEx );
			throw new NestableException (sError, _oEx);		
		}	
	}	
	
	///////////////////////////////////////////////////////////////////////////
	// sync method
	///////////////////////////////////////////////////////////////////////////

	/**
	 * get inv_loc data in HO that haven't been sync'ed since last ho2store
	 * 
	 * @param _iSyncType
	 * @return Updated inventory transaction 
	 * @throws Exception
	 */
	public static List getUpdatedInventoryLocation (String _sLocationID)
		throws Exception
	{
		Date dLastSync = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //this data goes one way
		Criteria oCrit = new Criteria();
		if (dLastSync  != null) 
		{
	    	oCrit.add(InventoryLocationPeer.UPDATE_DATE, dLastSync, Criteria.GREATER_EQUAL);   	    	
	    	if (StringUtil.isNotEmpty(_sLocationID)) 
	    	{
		    	oCrit.add(InventoryLocationPeer.LOCATION_ID, _sLocationID);   	    		
	    	}
		}
		return InventoryLocationPeer.doSelect(oCrit);
	}	
}
