package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.InvTransUpdatePeer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Inventory (Sub Ledger) Journal related logic
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InventoryTransactionTool.java,v 1.34 2009/05/04 02:04:27 albert Exp $ <br>
 *
 * <pre>
 * $Log: InventoryTransactionTool.java,v $
 * 
 * 2018-09-08
 * - @see {@link InventoryCostingTool} for new method to check whether PR cost should be updated
 * - add method getNextTransferAfterPR to query all transfer transaction after PR Date
 * - add method updatePRCost param PRID to check whether this PR cost should be updated based on
 *   1. @see {@link PreferenceTool.getPiUpdatePrCost()}
 *   2. PR Period closed or not
 *   3. Is Transfer exists after PR  
 * 
 * 2015-06-29
 * - Add method validateExists to prevent double posting of transaction
 * 
 * </pre><br>
 */
public class InventoryTransactionTool extends BaseTool
{
	private static Log log = LogFactory.getLog(InventoryTransactionTool.class);
	
	static final String s_SAVE_FAILED = LocaleTool.getString("save_invtr_failed");
	static final String s_DEL_FAILED = LocaleTool.getString("del_invtr_failed");
	
	//-------------------------------------------------------------------------
	// getter
	//-------------------------------------------------------------------------
	
	/**
	 * get inventory transaction by id
	 * @param _sID
	 * @param _oConn
	 * @return InventoryTransaction
	 * @throws Exception
	 */
	public static InventoryTransaction getByID (String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria ();
		oCrit.add (InventoryTransactionPeer.INVENTORY_TRANSACTION_ID, _sID);
		List vData = InventoryTransactionPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
			InventoryTransaction oTrans = (InventoryTransaction) vData.get(0);
			return oTrans;
		}
		return null;
	}
	
	/**
	 * get inventory transaction by trans id
	 * @param _sTransID
	 * @param _oConn
	 * @return list of InventoryTransaction
	 * @throws Exception
	 */
	public static List getByTransID (String _sTransID, int _iTransType, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria ();
		oCrit.add (InventoryTransactionPeer.TRANSACTION_ID, _sTransID);
		if (_iTransType > 0)
		{
			oCrit.add (InventoryTransactionPeer.TRANSACTION_TYPE, _iTransType);
		}
		return InventoryTransactionPeer.doSelect (oCrit, _oConn);
	}	

    /**
     * get inventory transaction by trans id
     * @param _sTransID
     * @param _oConn
     * @return list of InventoryTransaction
     * @throws Exception
     */
    public static List getByTransID (String _sTransID, String _sTransNo, int _iTransType, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria ();
        oCrit.add (InventoryTransactionPeer.TRANSACTION_ID, _sTransID);
        oCrit.add (InventoryTransactionPeer.TRANSACTION_NO, _sTransNo);
        if (_iTransType > 0)
        {
            oCrit.add (InventoryTransactionPeer.TRANSACTION_TYPE, _iTransType);
        }
        return InventoryTransactionPeer.doSelect (oCrit, _oConn);
    }
    
	/**
	 * get inventory transaction by detail id
	 * @param _sDetailID
	 * @param _oConn
	 * @return InventoryTransaction
	 * @throws Exception
	 */
	public static InventoryTransaction getByTransDetailID (String _sDetailID, 
														   int _iTransType, 
														   Connection _oConn)
		throws Exception
	{
		return getByTransDetailID (_sDetailID, "", "", _iTransType, _oConn);
	}

	/**
	 * get inventory transaction by detail id
	 * @param _sDetailID
	 * @param _oConn
	 * @return InventoryTransaction
	 * @throws Exception
	 */
	public static InventoryTransaction getByTransDetailID (String _sDetailID, 
														   String _sTransID,
														   String _sLocationID,
														   int _iTransType,
														   Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria ();
		oCrit.add (InventoryTransactionPeer.TRANSACTION_DETAIL_ID, _sDetailID);
		if (_iTransType > 0)
		{
			oCrit.add (InventoryTransactionPeer.TRANSACTION_TYPE, _iTransType);
		}
		if (StringUtil.isNotEmpty(_sTransID))
		{
			oCrit.add (InventoryTransactionPeer.TRANSACTION_ID, _sTransID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add (InventoryTransactionPeer.LOCATION_ID, _sLocationID);
		}
		List vData = InventoryTransactionPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
			if (StringUtil.isNotEmpty(_sLocationID) && vData.size() > 1)
			{
				log.warn("getByTransDetailID return more than 1 result as below : " + vData);
			}
			InventoryTransaction oTrans = (InventoryTransaction) vData.get(0);
			return oTrans;
		}
		else
		{
			log.error("getByTransDetailID return 0 result");			
		}
		return null;
	}	
	
	/**
	 * get last transaction
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _dAsOf get the invtrans just before this date, be careful with time
	 * @param _oConn
	 * @return last InventoryTransaction
	 * @throws Exception
	 */
	public static InventoryTransaction getLastTrans(String _sItemID, 
													String _sLocationID, 
													Date _dAsOf,
													Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (InventoryTransactionPeer.ITEM_ID, _sItemID);
		oCrit.add (InventoryTransactionPeer.LOCATION_ID, _sLocationID);		
		oCrit.addDescendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
		oCrit.addDescendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
		oCrit.add (InventoryTransactionPeer.TRANSACTION_DATE, _dAsOf, Criteria.LESS_EQUAL);		
		oCrit.setLimit(1);
		
		if (log.isDebugEnabled())
		{
			log.debug("\nLast Trans Criteria : \n" + oCrit);
		}
		
		List vData = InventoryTransactionPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
			InventoryTransaction oInv = (InventoryTransaction) vData.get(0);
	        if (log.isDebugEnabled())
			{
	        	StringBuilder oSB = new StringBuilder();
	        	oSB.append("\nLast Trans Result : ")
	        	   .append(oInv.getTransactionNo()).append(",").append(oInv.getTransactionDate()).append(",")
	        	   .append(oInv.getQtyBalance()).append(",").append(oInv.getValueBalance());
				log.debug(oSB.toString());
			}
			return oInv;	
		}
		return null;
	}
	
	//-------------------------------------------------------------------------
	// history
	//-------------------------------------------------------------------------

	/**
	 * get inventory trans by SKU
	 * @param _sSKU
	 * @param _sStartDate
	 * @param _sEndDate
	 * @param _sLocationID
	 * @return List of InventoryTransaction
	 * @throws Exception
	 */
	public static List getItemHistoryBySKU (String _sSKU, 
											String _sStartDate, 
											String _sEndDate, 
											String _sLocationID) 
		throws Exception
	{		
		Date dStartDate = CustomParser.parseDate (_sStartDate);
		Date dEndDate = CustomParser.parseDate (_sEndDate);
		
		if (dStartDate == null) dStartDate = new Date();
		if (dEndDate == null) dEndDate = new Date();		
		
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID );		
		}
		oCrit.addJoin(InventoryTransactionPeer.ITEM_ID, ItemPeer.ITEM_ID);		         
		oCrit.add(ItemPeer.ITEM_SKU, _sSKU);
		
		oCrit.add(InventoryTransactionPeer.TRANSACTION_DATE, 
				DateUtil.getStartOfDayDate(dStartDate), Criteria.GREATER_EQUAL);
		
		oCrit.and(InventoryTransactionPeer.TRANSACTION_DATE, 
				DateUtil.getEndOfDayDate(dEndDate), Criteria.LESS_EQUAL);    
		return InventoryTransactionPeer.doSelect(oCrit);
	}
	
	/**
	 * get item history by ID
	 * @param _sItemID
	 * @param _sStartDate
	 * @param _sEndDate
	 * @param _sLocationID
	 * @return List of InventoryTransaction
	 * @throws Exception
	 */
	public static List getItemHistory (String _sItemID, 
									   String _sStartDate, 
									   String _sEndDate, 
									   String _sLocationID) 
		throws Exception
	{		
		Date dStartDate = CustomParser.parseDate (_sStartDate);
		Date dEndDate = CustomParser.parseDate (_sEndDate);
		
		if (dStartDate == null) dStartDate = new Date();
		if (dEndDate == null) dEndDate = new Date();		
		
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID );		
		}
		oCrit.add(InventoryTransactionPeer.ITEM_ID, _sItemID );		         
		oCrit.add(InventoryTransactionPeer.TRANSACTION_DATE, 
			DateUtil.getStartOfDayDate(dStartDate), Criteria.GREATER_EQUAL);
		
		oCrit.and(InventoryTransactionPeer.TRANSACTION_DATE, 
			DateUtil.getEndOfDayDate(dEndDate), Criteria.LESS_EQUAL);    
		
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
		return InventoryTransactionPeer.doSelect(oCrit);
	}
	
	//-------------------------------------------------------------------------
	// count total inventory changes 
	//-------------------------------------------------------------------------
	
	static final int i_TOTAL_ALL = 1;
	static final int i_TOTAL_IN  = 2;
	static final int i_TOTAL_OUT = 3;

	/**
	 * get an Item Qty in certain location as of certain date
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _dAsOf
	 * @return total sum of qty changes of an item in inventory transaction since start date to as of date
	 * @throws Exception
	 */
	public static BigDecimal getQtyAsOf (String _sItemID, String _sLocationID, Date _dAsOf)
		throws Exception
	{
		return getTotalChanges(_sItemID, "", "", _sLocationID, PreferenceTool.getStartDate(), 
							   _dAsOf, i_TOTAL_ALL, false, null);
	}
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sKategoriID
	 * @param _sLocationID
	 * @param _dFromDate
	 * @param _dToDate
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getTotalIn (String _sItemID, 
										 String _sSKU, 
										 String _sKategoriID,
										 String _sLocationID, 
										 Date _dFromDate, 
										 Date _dToDate)
		throws Exception
	{
		return getTotalQtyChanges (_sItemID, _sSKU, _sKategoriID, _sLocationID, 
									DateUtil.getStartOfDayDate(_dFromDate), 
									DateUtil.getEndOfDayDate(_dToDate), i_TOTAL_IN, null); 		
	}
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sKategoriID
	 * @param _sLocationID
	 * @param _dFromDate
	 * @param _dToDate
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getTotalOut (String _sItemID, 
										  String _sSKU, 
										  String _sKategoriID,
										  String _sLocationID, 
										  Date _dFromDate, 
										  Date _dToDate)
		throws Exception
	{
		return getTotalQtyChanges (_sItemID, _sSKU, _sKategoriID, _sLocationID, 
									DateUtil.getStartOfDayDate(_dFromDate), 
									DateUtil.getEndOfDayDate(_dToDate), i_TOTAL_OUT, null); 
		
	}
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sLocationID
	 * @param _dFromDate
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getTotalQtyChanges (String _sItemID, 
												 String _sSKU, 
												 String _sKategoriID,
												 String _sLocationID, 
												 Date _dFromDate, 												 
												 Connection _oConn) 
	throws Exception
	{
		return getTotalQtyChanges (_sItemID, _sSKU, _sKategoriID, _sLocationID, 
								   DateUtil.getStartOfDayDate(_dFromDate), 
								   null, i_TOTAL_ALL, _oConn); 
	}	
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sLocationID
	 * @param _dFromDate
	 * @param _dToDate
	 * @param _iMode
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getTotalQtyChanges (String _sItemID, 
												 String _sSKU, 
												 String _sKategoriID,
												 String _sLocationID, 
												 Date _dFromDate, 
												 Date _dToDate,
												 int _iMode, 
												 Connection _oConn) 
		throws Exception
	{
		return getTotalChanges (_sItemID, _sSKU, _sKategoriID,_sLocationID, 
								DateUtil.getStartOfDayDate(_dFromDate), 
								DateUtil.getEndOfDayDate(_dToDate), _iMode, false, _oConn); 
	}
	
	/**
	 * get total qty changes for an Item SKU in a location or 
	 * all location since certain date
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sKategoriID
	 * @param _sLocationID
	 * @param _dFromDate
	 * @param _dToDate
	 * @param _iMode
	 * @param _bValue
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getTotalChanges (String _sItemID, 
											  String _sSKU, 
											  String _sKategoriID,
											  String _sLocationID, 
											  Date _dFromDate, 
											  Date _dToDate,
											  int _iMode, 
											  boolean _bValue,
											  Connection _oConn) 
		throws Exception
	{ 
		StringBuilder oSB = new StringBuilder();
		oSB.append (" SELECT ");
		
		if (_bValue)
		{
			oSB.append(" SUM(itrans.qty_changes * itrans.cost) ");
		}
		else
		{
			oSB.append(" SUM(itrans.qty_changes) ");		
		}
		
		oSB.append(" FROM item itm, inventory_transaction itrans ");
		oSB.append(" WHERE itm.item_id = itrans.item_id ");

		if (_iMode == i_TOTAL_IN)  oSB.append(" AND itrans.qty_changes > 0 ");
		if (_iMode == i_TOTAL_OUT) oSB.append(" AND itrans.qty_changes < 0 ");
		
		if (StringUtil.isNotEmpty(_sItemID))
		{
			oSB.append(" AND itrans.item_id = '").append(_sItemID).append("' ");
		}
		else if (StringUtil.isNotEmpty(_sSKU))		
		{		
			oSB.append(" AND itm.item_sku = '").append (_sSKU).append("' ");
		}
		else if (StringUtil.isNotEmpty(_sKategoriID))		
		{		
			List vChild = KategoriTool.getChildIDList(_sKategoriID);
			vChild.add(_sKategoriID);
			String sKat = SqlUtil.convertToINMode(vChild);
			if (StringUtil.isNotEmpty(sKat) && !sKat.equals("()"))
			{
				oSB.append(" AND itm.kategori_id IN ").append(sKat).append(" ");
			}
		}
		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oSB.append(" AND itrans.location_id = '").append(_sLocationID).append("' ");
		}
		
		oSB.append(" AND itrans.transaction_date >= ");
		oSB.append(oDB.getDateString(_dFromDate));
		
		if (_dToDate != null)
		{
			oSB.append(" AND itrans.transaction_date <= ");
			oSB.append(oDB.getDateString(_dToDate));
		}
		if (StringUtil.isEmpty(_sItemID) && StringUtil.isNotEmpty(_sSKU))
		{
			oSB.append(" GROUP BY itm.item_sku ");
		}
		
		if (log.isDebugEnabled())
		{
			log.debug (oSB.toString());
		}
		
		double dTotalChanges = 0;		
		List vChanges = null;
		if (_oConn != null) 
		{
			vChanges = InventoryTransactionPeer.executeQuery(oSB.toString(), false, _oConn);
		}
		else 
		{
			vChanges = InventoryTransactionPeer.executeQuery(oSB.toString());		
		}
		for (int i = 0; i < vChanges.size(); i++)
		{
			Record oData = (Record)vChanges.get(i);
			dTotalChanges += oData.getValue(1).asDouble();
		}
		return new BigDecimal(dTotalChanges);        
	}	
	
	/**
	 * get total value changes for an Item SKU in a location or 
	 * all location since certain date
	 * 
	 * @param _sSKU         Item SKU
	 * @param _sLocationID  if not specified, then will be calculated on all location
	 * @param _dFromDate    Start Date
	 * @return total value changes
	 * @throws Exception
	 */
	public static BigDecimal getTotalValueChanges (String _sItemID,
												   String _sSKU, 
												   String _sKategoriID,
												   String _sLocationID,
												   Date _dFromDate, 
												   Connection _oConn) 
		throws Exception
	{ 
		return getTotalChanges (_sItemID, _sSKU, _sKategoriID, _sLocationID, 
								DateUtil.getStartOfDayDate(_dFromDate), null, i_TOTAL_ALL,  true,_oConn); 
	}	  
	
	public static BigDecimal getTotalValueIn (String _sItemID,
											  String _sSKU, 
											  String _sKategoriID,
											  String _sLocationID,
											  Date _dFromDate, 
											  Date _dToDate,
											  Connection _oConn) 
	throws Exception
	{ 
		return getTotalChanges (_sItemID, _sSKU, _sKategoriID, _sLocationID, 
								DateUtil.getStartOfDayDate(_dFromDate), 
								DateUtil.getEndOfDayDate(_dToDate), i_TOTAL_IN, true, _oConn); 
	}	
	
	public static BigDecimal getTotalValueOut (String _sItemID,
											  String _sSKU, 
											  String _sKategoriID,
											  String _sLocationID,
											  Date _dFromDate, 
											  Date _dToDate,
											  Connection _oConn) 
	throws Exception
	{ 
		return getTotalChanges (_sItemID, _sSKU, _sKategoriID, _sLocationID, 
								DateUtil.getStartOfDayDate(_dFromDate), 
								DateUtil.getEndOfDayDate(_dToDate), i_TOTAL_OUT, true, _oConn); 
	}

	//-------------------------------------------------------------------------
	// updated trans
	//-------------------------------------------------------------------------

	public static List findInvTransUpdate (String _sSrcNo, 
										   String _sTransNo,
										   String _sLocID,
										   String _sInvTransID,
										   String _sItemID,
										   Date _dStart, 
			   							   Date _dEnd) 
	     throws Exception
	{		
		Criteria oCrit = new Criteria();		
		if(StringUtil.isNotEmpty(_sSrcNo))
		{
			oCrit.add(InvTransUpdatePeer.UPDATE_TX, _sSrcNo);
		}
		if(StringUtil.isNotEmpty(_sTransNo))
		{
			oCrit.add(InvTransUpdatePeer.TRANS_NO, _sTransNo);
		}
		if (StringUtil.isNotEmpty(_sLocID)) 
		{
			oCrit.add(InvTransUpdatePeer.LOCATION_ID, _sLocID );		
		}
		if(StringUtil.isNotEmpty(_sInvTransID))
		{
			oCrit.add(InvTransUpdatePeer.INVTRANS_ID, _sInvTransID);
		}			
		if(StringUtil.isNotEmpty(_sItemID))
		{
			oCrit.add(InvTransUpdatePeer.ITEM_ID, _sItemID);
		}					
		if(_dStart != null) oCrit.add(InvTransUpdatePeer.UPDATE_DATE, 
				DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);

		if(_dEnd != null) oCrit.and(InvTransUpdatePeer.UPDATE_DATE, 
				DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);    

		oCrit.addAscendingOrderByColumn(InvTransUpdatePeer.UPDATE_DATE);
		oCrit.addAscendingOrderByColumn(InvTransUpdatePeer.TX_TYPE);
		oCrit.addAscendingOrderByColumn(InvTransUpdatePeer.TRANS_ID);
		return InvTransUpdatePeer.doSelect(oCrit);
	}
	
	/**
	 * Update GL trans per transId based on InvTransUpdate, 
	 * should be private only called from InvCostingTool 
	 * 
	 * @throws Exception
	 */
	public static void updateGLFromITU (String _sTXNo) 
		throws Exception
	{		
		Connection conn = beginTrans();
		try 
		{
			InventoryCostingTool oICT= new InventoryCostingTool(conn);
			oICT.setUpdatedTrans(findInvTransUpdate(_sTXNo, "", "", "", "", null, null));
			oICT.updateGL();
			commit(conn);			
		} 
		catch (Exception e) {			
			rollback(conn);
			log.error(e);
		}
	}
	
	//-------------------------------------------------------------------------
	// issue receipt
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from issue receipt unplanned
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param IssueReceipt _oIR
	 * @param List _vIRDet
	 * @param Connection _oConn, current transaction connection object
	 */    
	
	public static void createFromIssueReceipt (IssueReceipt _oIR, List _vIRDet, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try 
		{
			for (int i = 0; i < _vIRDet.size(); i++)
			{
				IssueReceiptDetail oIRDet = (IssueReceiptDetail) _vIRDet.get(i);
				mapIRDetailToInvTrans (_oIR, oIRDet, _oConn);
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapIRDetailToInvTrans (IssueReceipt _oIR, IssueReceiptDetail _oIRDet, Connection _oConn)
		throws Exception
	{
		int iItemType = ItemTool.getItemTypeByID (_oIRDet.getItemId(), _oConn); 
		
		if (iItemType == i_INVENTORY_PART) 
		{	
			StringBuilder oDesc = new StringBuilder ();
			if (_oIR.getInternalTransfer())
            {
                oDesc.append (AdjustmentTypeTool.getDescriptionByID(_oIR.getAdjustmentTypeId())).append(" (");
                oDesc.append (getTransactionTypeName(Integer.valueOf(_oIR.getTransactionType()))).append(") ");
                oDesc.append (_oIR.getCrossEntityCode()).append(_oIR.getDescription());                 
            }
            else
            {
                oDesc.append (AdjustmentTypeTool.getDescriptionByID(_oIR.getAdjustmentTypeId())).append(" (");
    			oDesc.append (getTransactionTypeName(Integer.valueOf(_oIR.getTransactionType())));
    			oDesc.append (") ").append(_oIR.getDescription()); 
            }
			InventoryTransaction oInv = new InventoryTransaction();
			oInv.setTransactionId	    (_oIR.getIssueReceiptId());	
			oInv.setTransactionNo	    (_oIR.getTransactionNo());	
			oInv.setTransactionDetailId (_oIRDet.getIssueReceiptDetailId());			    
			oInv.setTransactionType     (_oIR.getTransactionType()); 
			oInv.setTransactionDate     (_oIR.getTransactionDate());
			oInv.setLocationId          (_oIR.getLocationId());
			oInv.setLocationName        (_oIR.getLocationName());
			oInv.setItemId			    (_oIRDet.getItemId());
			oInv.setItemCode		    (_oIRDet.getItemCode());
			
			double dBaseValue = UnitTool.getBaseValue(_oIRDet.getItemId(), _oIRDet.getUnitId(), _oConn);
			double dQtyChange = _oIRDet.getQtyChanges().doubleValue();
			double dQtyBase   = dQtyChange * dBaseValue;
			double dCost      = _oIRDet.getCost().doubleValue() / dBaseValue;
			
			if(_oIR.getTransactionType() == i_INV_TRANS_ISSUE_UNPLANNED)
			{
				dQtyBase = 0 - dQtyBase;
			}
						
			oInv.setQtyChanges	 (new BigDecimal(dQtyBase));
			oInv.setCost		 (new BigDecimal(dCost));     
			oInv.setDescription	 (oDesc.toString());
			oInv.setItemPrice	 (ItemTool.getPriceExclTax(_oIRDet.getItemId(), _oConn));
			oInv.setPrice		 (oInv.getItemPrice());
			
			saveData (oInv, _oIRDet, _oConn);
		}
	}	
	
	//-------------------------------------------------------------------------
	// item transfer
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from item transfer
	 * 
	 * @deprecated move to InventoryCostingTool    
	 * @param ItemTransfer _oData
	 * @param Connection _oConn, current transaction connection object
	 */    
	
	public static void createFromItemTransfer (ItemTransfer _oIT, List _vITDet, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try 
		{
			for (int i = 0; i < _vITDet.size(); i++)
			{
				ItemTransferDetail oITDet = (ItemTransferDetail) _vITDet.get(i);
				mapITDetailToInvTrans (_oIT, oITDet, false, _oConn);
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapITDetailToInvTrans (ItemTransfer _oIT, ItemTransferDetail _oITDet, boolean _bInOnly, Connection _oConn)
		throws Exception
	{
		int iItemType = ItemTool.getItemTypeByID (_oITDet.getItemId(), _oConn); 
		
		if (iItemType == i_INVENTORY_PART) 
		{
			double dBaseValue = UnitTool.getBaseValue(_oITDet.getItemId(), _oITDet.getUnitId(), _oConn);
			double dQtyChange = _oITDet.getQtyChanges().doubleValue();
			double dQtyBase   = dQtyChange * dBaseValue;
			double dCost      = _oITDet.getCostPerUnit().doubleValue(); //Cost already in base unit
			
			if (dCost == 0)
			{
				//try to get last cost from inventory location 
				dCost = InventoryLocationTool
					.getItemCost(_oITDet.getItemId(), _oIT.getFromLocationId(), _oIT.getTransactionDate(), _oConn)
						.doubleValue();
			}
			
			//TO INV TRANS
			StringBuilder oDesc = new StringBuilder ();
			oDesc.append ("Transfer From : ");
			oDesc.append (_oIT.getFromLocationName()).append(" ");			       
			
			InventoryTransaction oInv = new InventoryTransaction ();
			
			oInv.setTransactionId	    (_oIT.getItemTransferId());	
			oInv.setTransactionNo	    (_oIT.getTransactionNo());			    
			oInv.setTransactionDetailId (_oITDet.getItemTransferDetailId());		       		    
			oInv.setTransactionType     (i_INV_TRANS_TRANSFER_IN); 
			oInv.setTransactionDate     (_oIT.getTransactionDate());
			oInv.setLocationId          (_oIT.getToLocationId());
			oInv.setLocationName        (_oIT.getToLocationName());
			oInv.setItemId			    (_oITDet.getItemId());
			oInv.setItemCode		    (_oITDet.getItemCode());
			oInv.setQtyChanges		    (new BigDecimal (dQtyBase));
			oInv.setCost 			    (new BigDecimal (dCost));		
			oInv.setDescription	     	(oDesc.toString () + _oIT.getDescription());
			oInv.setItemPrice	 		(ItemTool.getPriceExclTax(_oITDet.getItemId(), _oConn));
			oInv.setPrice               (oInv.getItemPrice());

			saveData(oInv, _oITDet, _oConn);
			
			//if this is STORE TRANSFER IN do not journal from inventory transaction
			if (!_bInOnly) 
			{
				//FROM INV TRANS
				oDesc = new StringBuilder ();
				oDesc.append ("Transfer To : ");
				oDesc.append (_oIT.getToLocationName()).append(" ");		
				
				oInv = new InventoryTransaction ();
				oInv.setTransactionId	    (_oIT.getItemTransferId()); 
				oInv.setTransactionNo	    (_oIT.getTransactionNo());	
				oInv.setTransactionDetailId (_oITDet.getItemTransferDetailId());		       
				oInv.setTransactionType     (i_INV_TRANS_TRANSFER_OUT); 
				oInv.setTransactionDate     (_oIT.getTransactionDate());
				oInv.setLocationId          (_oIT.getFromLocationId());
				oInv.setLocationName        (_oIT.getFromLocationName());
				oInv.setItemId              (_oITDet.getItemId());
				oInv.setItemCode            (_oITDet.getItemCode());       
				oInv.setQtyChanges          (new BigDecimal ((0 - dQtyBase)));
				oInv.setCost                (new BigDecimal (dCost));      
				oInv.setDescription         (oDesc.toString () + _oIT.getDescription());
				oInv.setItemPrice	 		(ItemTool.getPriceExclTax(_oITDet.getItemId(), _oConn));
				oInv.setPrice               (oInv.getItemPrice());

				saveData(oInv, _oITDet, _oConn);	
			}
		}
	}	
	
	/**
	 * create inventory transaction data from STORE TRANSFER IN 
	 *     
	 * @param ItemTransfer _oData
	 * @param Connection _oConn, current transaction connection object
	 */    
	
	public static void createFromStoreTransferIn (ItemTransfer _oIT, List _vITDet, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try 
		{
			for (int i = 0; i < _vITDet.size(); i++)
			{
				ItemTransferDetail oITDet = (ItemTransferDetail) _vITDet.get(i);
				mapITDetailToInvTrans (_oIT, oITDet, true, _oConn);
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	//-------------------------------------------------------------------------
	// delivery order
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from delivery order
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param Connection _oConn, current transaction connection object
	 */	
	public static void createFromDO (DeliveryOrder _oDO, List _vDOD, Map _mGroup, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try 
		{    
			String sLocationName = LocationTool.getLocationNameByID(_oDO.getLocationId(), _oConn);
			for (int i = 0; i < _vDOD.size(); i++)
			{
				DeliveryOrderDetail oDOD = (DeliveryOrderDetail)_vDOD.get(i);
                if (oDOD.getQty().doubleValue() != 0)
                {
                    mapDODetailsToInvTrans (_oDO, oDOD, sLocationName, _mGroup, _oConn);
                }
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapDODetailsToInvTrans (DeliveryOrder _oTR, 
												DeliveryOrderDetail _oTD, 
												String _sLocationName,
												Map _mGroup,
												Connection _oConn)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Delivery Order To ");
		oDesc.append (_oTR.getCustomerName());
		
		int iItemType = ItemTool.getItemTypeByID (_oTD.getItemId(), _oConn); 
		
		if (iItemType == i_INVENTORY_PART) 
		{	
			InventoryTransaction oInv = new InventoryTransaction ();
			oInv.setTransactionId		 (_oTR.getDeliveryOrderId());	
			oInv.setTransactionNo	     (_oTR.getDeliveryOrderNo());	
			oInv.setTransactionDetailId  (_oTD.getDeliveryOrderDetailId());		       			
			oInv.setTransactionType	 	 (i_INV_TRANS_DELIVERY_ORDER); 
			oInv.setTransactionDate      (_oTR.getDeliveryOrderDate());
			oInv.setLocationId       	 (_oTR.getLocationId());
			oInv.setLocationName     	 (_sLocationName);
			oInv.setItemId				 (_oTD.getItemId());
			oInv.setItemCode			 (_oTD.getItemCode());
			
			double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(),_oTD.getUnitId(), _oConn);
			double dQtyChange = _oTD.getQty().doubleValue();
			double dQtyBase   = dQtyChange * dBaseValue;
			double dCost      = _oTD.getCostPerUnit().doubleValue(); //Cost already in base unit
			
			oInv.setQtyChanges		(new BigDecimal((0 - dQtyBase)));
			oInv.setCost 			(new BigDecimal(dCost));
			oInv.setPrice			(_oTD.getItemPriceInSo());
			oInv.setDescription		(oDesc.toString());
			oInv.setItemPrice	 	(ItemTool.getPriceExclTax(_oTD.getItemId(), _oConn));

			saveData(oInv, _oTD, _oConn);
		}
		//if item in sales is grouping
		else if (iItemType == i_GROUPING) 
		{
			List vGroup = (List) _mGroup.get(_oTD.getItemId());
			for (int i = 0; i < vGroup.size(); i++) 
			{	
				oDesc = new StringBuilder ("Delivery Order To ");
				oDesc.append ( _oTR.getCustomerName() );
				oDesc.append ( " as Part of Group Item : " );
				oDesc.append ( _oTD.getItemCode() );
				
				InventoryTransaction oInv = new InventoryTransaction();
				ItemGroup oItemGroup = (ItemGroup) vGroup.get (i);	 
				
				oInv.setTransactionId	    (_oTR.getDeliveryOrderId());    
				oInv.setTransactionNo	    (_oTR.getDeliveryOrderNo());	                
				oInv.setTransactionDetailId (_oTD.getDeliveryOrderDetailId());
				oInv.setTransactionType     (i_INV_TRANS_DELIVERY_ORDER); 
				oInv.setTransactionDate     (_oTR.getDeliveryOrderDate());
				oInv.setLocationId          (_oTR.getLocationId());
				oInv.setLocationName        (_sLocationName);
				oInv.setItemId              (oItemGroup.getItemId());
				oInv.setItemCode            (oItemGroup.getItemCode());
				
				double dBaseValue = UnitTool.getBaseValue(oItemGroup.getItemId(), oItemGroup.getUnitId(), _oConn);
				double dQtyChange = oItemGroup.getQty().doubleValue();
				double dQtyBase   = _oTD.getQty().doubleValue()* dQtyChange * dBaseValue;				

				oInv.setQtyChanges  (new BigDecimal((0 - dQtyBase )));
				oInv.setPrice 	    (_oTD.getItemPriceInSo());
				oInv.setItemPrice   (ItemTool.getPriceExclTax(_oTD.getItemId(), _oConn));
				oInv.setDescription (oDesc.toString());
				oInv.setCost 		(oItemGroup.getCost());
				
				saveData(oInv, _oTD, _oConn);
			}
		}
	}
		
	//-------------------------------------------------------------------------
	// sales transaction
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from sales transaction 
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param SalesTransaction _oTR 
	 * @param List _vTD 
	 * @param Connection _oConn, current transaction connection object
	 */	
	public static void createFromSalesInvoice (SalesTransaction _oTR, List _vTD, Map _mGroup, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try
		{			
			String sLocationName = LocationTool.getLocationNameByID(_oTR.getLocationId(), _oConn);
			for (int i = 0; i < _vTD.size(); i++)
			{
				SalesTransactionDetail oTD = (SalesTransactionDetail) _vTD.get(i);
				if (StringUtil.isEmpty(oTD.getDeliveryOrderDetailId()) || //if not from DO || direct import from SO
                    (StringUtil.isNotEmpty(oTD.getDeliveryOrderDetailId()) && 
                     StringUtil.isEqual(oTD.getDeliveryOrderId(),oTD.getSalesOrderId()))    
                   ) 
				{
					mapSalesDetailsToInvTrans (_oTR, oTD, _mGroup, sLocationName, _oConn);
				}
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapSalesDetailsToInvTrans (SalesTransaction _oTR, 
												   SalesTransactionDetail _oTD, 
												   Map _mGroup,
												   String _sLocationName,
												   Connection _oConn)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Sold To ");
		oDesc.append ( _oTR.getCustomerName() );
		
		int iItemType = ItemTool.getItemTypeByID (_oTD.getItemId(), _oConn); 
		
		if (iItemType == i_INVENTORY_PART) 
		{	
			InventoryTransaction oInv = new InventoryTransaction ();
			
			oInv.setTransactionId		(_oTR.getSalesTransactionId());	
			oInv.setTransactionNo	    (_oTR.getInvoiceNo());				
			oInv.setTransactionDetailId (_oTD.getSalesTransactionDetailId());			
			oInv.setTransactionType		(i_INV_TRANS_SALES_INVOICE); 
			oInv.setTransactionDate     (_oTR.getTransactionDate());
			oInv.setLocationId       	(_oTR.getLocationId());
			oInv.setLocationName     	(_sLocationName);
			oInv.setItemId				(_oTD.getItemId());
			oInv.setItemCode			(_oTD.getItemCode());
			
			double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(), _oTD.getUnitId(), _oConn);
			double dQtyChange = _oTD.getQty().doubleValue();
			double dQtyBase   = dQtyChange * dBaseValue;
			double dCost      = _oTD.getItemCost().doubleValue(); //Cost already in base unit
			
			oInv.setQtyChanges	(new BigDecimal((0 - dQtyBase)));
			oInv.setCost 		(new BigDecimal(dCost));
			oInv.setPrice		(_oTD.getItemPrice());
			oInv.setItemPrice   (_oTD.getItemPrice());
			oInv.setDescription	(oDesc.toString());
			
			//if description is set manually
			if (!StringUtil.isEqual(_oTD.getItemName(), _oTD.getDescription()))
			{
				oInv.setDescription(oInv.getDescription() + "  " + _oTD.getDescription());
			}
			
			if(StringUtil.isEmpty(_oTD.getDeliveryOrderDetailId()))
			{
				saveData(oInv, _oTD, _oConn);
			}	
			else //from DO do not validate nor process batch / serial
			{
				saveData(oInv, null, _oConn);											
			}
		}
		//if item in sales is grouping
		else if (iItemType == i_GROUPING) 
		{
			List vGroup = (List) _mGroup.get(_oTD.getItemId()); 
			for (int i = 0; i < vGroup.size(); i++) 
			{	
				oDesc = new StringBuilder ("Sold To ");
				oDesc.append ( _oTR.getCustomerName() );
				oDesc.append ( " as Part of Group Item : " );
				oDesc.append ( _oTD.getItemCode() );

				ItemGroup oItemGroup = (ItemGroup) vGroup.get (i);	 
				
				InventoryTransaction oInv = new InventoryTransaction ();
				oInv.setTransactionId	    (_oTR.getSalesTransactionId());   
				oInv.setTransactionNo	    (_oTR.getInvoiceNo());				                
				oInv.setTransactionDetailId (_oTD.getSalesTransactionDetailId());                 
				oInv.setTransactionType     (i_INV_TRANS_SALES_INVOICE); 
				oInv.setTransactionDate     (_oTR.getTransactionDate());
				oInv.setLocationId          (_oTR.getLocationId());
				oInv.setLocationName        (_sLocationName);
				oInv.setItemId              (oItemGroup.getItemId());
				oInv.setItemCode            (oItemGroup.getItemCode());
				
				double dBaseValue = UnitTool.getBaseValue(oItemGroup.getItemId(),oItemGroup.getUnitId(), _oConn);
				double dQtyChange = oItemGroup.getQty().doubleValue();
				double dQtyBase   = _oTD.getQty().doubleValue()* dQtyChange * dBaseValue;
								
				oInv.setQtyChanges(new BigDecimal((0 - dQtyBase )));
				oInv.setPrice(_oTD.getItemPrice());
				oInv.setItemPrice(_oTD.getItemPrice());
				oInv.setDescription(oDesc.toString());
				oInv.setCost(oItemGroup.getCost());

				saveData(oInv, _oTD, _oConn);
			}
		}
	}
	
	//-------------------------------------------------------------------------
	// sales return
	//-------------------------------------------------------------------------
	/**
	 * create inventory transaction data from sales return
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param SalesReturn _oPR 
	 * @param List _vTD 
	 * @param Connection _oConn, current transaction connection object
	 */	
	public static void createFromSalesReturn (SalesReturn _oSR, List _vDetail, Map _mGroup, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try
		{
			String sLocationName = LocationTool.getLocationNameByID(_oSR.getLocationId(), _oConn);
			for (int i = 0; i < _vDetail.size(); i++)
			{
				SalesReturnDetail oSRD = (SalesReturnDetail) _vDetail.get(i);
				mapSalesReturnDetailsToInvTrans (_oSR, oSRD, sLocationName, _mGroup, _oConn);
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapSalesReturnDetailsToInvTrans (SalesReturn _oSR, 
														 SalesReturnDetail _oSRD, 
														 String _sLocationName,
														 Map _mGroup,
														 Connection _oConn)
		throws Exception
	{
		InventoryTransaction oInv = new InventoryTransaction();
		StringBuilder oDesc = new StringBuilder ("Returned from ");
		oDesc.append(_oSR.getCustomerName());
		oDesc.append(" ");
		if (StringUtil.isNotEmpty(_oSR.getTransactionNo()))
		{
			oDesc.append(" Trans No: ");
			oDesc.append(_oSR.getTransactionNo());			
		}

		int iItemType = ItemTool.getItemTypeByID (_oSRD.getItemId(), _oConn); 		
		if (iItemType == i_INVENTORY_PART) 
		{	
			oInv.setTransactionId		(_oSR.getSalesReturnId());	
			oInv.setTransactionNo	    (_oSR.getReturnNo());				
			oInv.setTransactionDetailId (_oSRD.getSalesReturnDetailId());			
			oInv.setTransactionType		(i_INV_TRANS_SALES_RETURN); 
			oInv.setTransactionDate     (_oSR.getReturnDate());
			oInv.setLocationId       	(_oSR.getLocationId());
			oInv.setLocationName     	(_sLocationName);
			oInv.setItemId				(_oSRD.getItemId());
			oInv.setItemCode			(_oSRD.getItemCode());
			
			double dBaseValue = UnitTool.getBaseValue(_oSRD.getItemId(),_oSRD.getUnitId(), _oConn);
			double dQtyChange = _oSRD.getQty().doubleValue();
			double dQtyBase   = dQtyChange * dBaseValue;
			double dCost      = _oSRD.getItemCost().doubleValue();
			
			oInv.setQtyChanges (new BigDecimal(dQtyBase));
			oInv.setCost (_oSRD.getItemCost());
			oInv.setPrice (_oSRD.getItemPrice());
			oInv.setItemPrice (_oSRD.getItemPrice());
			oInv.setDescription	(oDesc.toString());
			
			saveData(oInv, _oSRD, _oConn);			
		}
		//if item in sales is grouping
		else if (iItemType == i_GROUPING) 
		{
			List vGroup = (List) _mGroup.get(_oSRD.getItemId());
			for (int i = 0; i < vGroup.size(); i++) 
			{	
				oDesc = new StringBuilder ();
				oDesc.append ( " Returned as Part of Group Item : " );
				oDesc.append ( _oSRD.getItemCode() );
				
				oInv = new InventoryTransaction();
				ItemGroup oItemGroup = (ItemGroup) vGroup.get (i);	 
				
				oInv.setTransactionId		(_oSR.getSalesReturnId());	
				oInv.setTransactionNo	    (_oSR.getReturnNo());				
				oInv.setTransactionDetailId (_oSRD.getSalesReturnDetailId());			
				oInv.setTransactionType		(i_INV_TRANS_SALES_RETURN); 
				oInv.setTransactionDate     (_oSR.getReturnDate());
				oInv.setLocationId       	(_oSR.getLocationId());
				oInv.setLocationName     	(_sLocationName);
				oInv.setItemId				(oItemGroup.getItemId());
				oInv.setItemCode			(oItemGroup.getItemCode());
				
				double dBaseValue = UnitTool.getBaseValue(oItemGroup.getItemId(),oItemGroup.getUnitId(), _oConn);
				double dQtyChange = oItemGroup.getQty().doubleValue();
				double dQtyBase   = _oSRD.getQty().doubleValue()* dQtyChange * dBaseValue;			
								
				oInv.setQtyChanges (new BigDecimal(dQtyBase));
				oInv.setCost (oItemGroup.getCost());
				oInv.setPrice (_oSRD.getItemPrice());
				oInv.setItemPrice(_oSRD.getItemPrice());
				oInv.setDescription	(oDesc.toString());
				
				saveData(oInv, _oSRD, _oConn);
			}
		}		
	}	
	
	//-------------------------------------------------------------------------
	// purchase receipt
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from purchase receipt
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param int _iPRID, PurchaseRequestId 
	 * @param Connection _oConn, current transaction connection object
	 */
	
	public static void createFromPurchaseReceipt (PurchaseReceipt _oPR, List _vPRD, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try 
		{
			for (int i = 0; i < _vPRD.size(); i++)
			{
				PurchaseReceiptDetail oPRDet = (PurchaseReceiptDetail)_vPRD.get(i);
                if (oPRDet.getQty().doubleValue() != 0)
                {                    
                    mapPRDetailToInvTrans (_oPR, oPRDet, _oConn);
                }
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapPRDetailToInvTrans (PurchaseReceipt _oPR,
											   PurchaseReceiptDetail _oPRDet, 
											   Connection _oConn)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Receipt From ");
		oDesc.append ( _oPR.getVendorName() );
		
		int iItemType = ItemTool.getItemTypeByID (_oPRDet.getItemId(), _oConn); 
		if (iItemType == i_INVENTORY_PART)
		{	
			InventoryTransaction oInv = new InventoryTransaction();
			oInv.setTransactionId	   (_oPR.getPurchaseReceiptId());	
			oInv.setTransactionNo	   (_oPR.getReceiptNo());		
			oInv.setTransactionDetailId(_oPRDet.getPurchaseReceiptDetailId());		
			oInv.setTransactionType    (i_INV_TRANS_PURCHASE_RECEIPT); 
			oInv.setTransactionDate    (_oPR.getReceiptDate());
			oInv.setLocationId         (_oPR.getLocationId());
			oInv.setLocationName       (_oPR.getLocationName());			
			oInv.setItemId			   (_oPRDet.getItemId());
			oInv.setItemCode		   (_oPRDet.getItemCode());
			
			double dBaseValue = UnitTool.getBaseValue(_oPRDet.getItemId(),_oPRDet.getUnitId(), _oConn);
			double dQtyChange = _oPRDet.getQty().doubleValue();
			double dQtyBase   = dQtyChange * dBaseValue;
			double dCost      = _oPRDet.getCostPerUnit().doubleValue(); //check cost is already in base unit
			
			//if receipt without PO then cost will be 0, so we'll get the current 
			//cost so the cost data in inventory location not changed
			if (dCost == 0) 
			{
				//cost is already cost per unit
				dCost  = InventoryLocationTool
				.getItemCost(_oPRDet.getItemId(), _oPR.getLocationId(), _oPR.getReceiptDate(), _oConn)
				.doubleValue();		
				
			}			
			
			oInv.setPrice		(_oPRDet.getItemPriceInPo());
			oInv.setItemPrice	(ItemTool.getPriceExclTax(_oPRDet.getItemId(), _oConn));
			oInv.setQtyChanges	(new BigDecimal(dQtyBase));
			oInv.setCost		(new BigDecimal(dCost));
			oInv.setDescription	(oDesc.toString());
			
			saveData (oInv, _oPRDet, _oConn);
		}
	}
	
	//-------------------------------------------------------------------------
	// purchase invoice
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from purchase invoices
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param int _iPRID, PurchaseRequestId 
	 * @param Connection _oConn, current transaction connection object
	 */
	
	public static void createFromPurchaseInvoice (PurchaseInvoice _oPI, List _vPID, Connection _oConn)
		throws Exception
	{
		try 
		{
			for (int i = 0; i < _vPID.size(); i++)
			{
				PurchaseInvoiceDetail oPIDet = (PurchaseInvoiceDetail)_vPID.get(i);
				mapPIDetailToInvTrans (_oPI, oPIDet, _oConn);
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapPIDetailToInvTrans (PurchaseInvoice _oPI, 
											   PurchaseInvoiceDetail _oPIDet, 
											   Connection _oConn)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Purchase Invoice From ");
		oDesc.append ( _oPI.getVendorName() );
		
		int iItemType = ItemTool.getItemTypeByID (_oPIDet.getItemId(), _oConn); 
		if (iItemType == i_INVENTORY_PART)
		{
			InventoryTransaction oInv = new InventoryTransaction();
			oInv.setTransactionId	     (_oPI.getPurchaseInvoiceId());	
			oInv.setTransactionNo	     (_oPI.getPurchaseInvoiceNo());				
			oInv.setTransactionDetailId  (_oPIDet.getPurchaseInvoiceDetailId());		
			oInv.setTransactionType      (i_INV_TRANS_PURCHASE_INVOICE); 
			oInv.setTransactionDate      (_oPI.getPurchaseInvoiceDate());
			oInv.setLocationId           (_oPI.getLocationId());
			oInv.setLocationName         (_oPI.getLocationName());
			oInv.setItemId			     (_oPIDet.getItemId());
			oInv.setItemCode		     (_oPIDet.getItemCode());
			oInv.setCost		 		 (_oPIDet.getCostPerUnit());
			oInv.setQtyChanges	 		 (_oPIDet.getQtyBase());
			oInv.setPrice		 		 (_oPIDet.getItemPrice());	    
			oInv.setItemPrice	 		 (ItemTool.getPriceExclTax(_oPIDet.getItemId(), _oConn));
			oInv.setDescription 		 (oDesc.toString());			
			
			if (!StringUtil.isEqual(_oPIDet.getItemName(),_oPIDet.getDescription()))
			{
				oInv.setDescription(oInv.getDescription() + "  " + _oPIDet.getDescription());
			}
					
			//if direct purchase
			if (StringUtil.isEmpty(_oPIDet.getPurchaseReceiptDetailId()))
			{
				saveData (oInv, _oPIDet, _oConn);
			}
			else //if from receipt 
			{
				InventoryTransaction oRcpt = 
					getByTransDetailID (_oPIDet.getPurchaseReceiptDetailId(), 
						i_INV_TRANS_PURCHASE_RECEIPT, _oConn);
				
				//TODO: test & validate function
				log.debug("Receipt Inventory Transaction Before Update : " + oRcpt);
				if (oRcpt != null && updatePRCost(oRcpt.getTransactionId(), _oConn)) 
				{
					//if cost / price changed in PI
					if (!Calculator.isInTolerance(oRcpt.getCost(), _oPIDet.getCostPerUnit(), d_NUMBER_TOLERANCE))
					{
						oRcpt.setCost	(_oPIDet.getCostPerUnit());
						oRcpt.setPrice	(_oPIDet.getItemPrice());		
						saveData (oRcpt, _oConn);
					}
				}
				log.debug("Receipt Inventory Transaction After Update : " + oRcpt);
			}
		}
	}
	
	//-------------------------------------------------------------------------
	// purchase return
	//-------------------------------------------------------------------------
	
	/**
	 * create inventory transaction data from purchase return
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param PurchaseReturn _oPR 
	 * @param List _vTD 
	 * @param Connection _oConn, current transaction connection object
	 */	
	public static void createFromPurchaseReturn (PurchaseReturn _oPR, 
												 List _vDetail, 
												 Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		try 
		{
			for (int i = 0; i < _vDetail.size(); i++)
			{
				PurchaseReturnDetail oPRD = (PurchaseReturnDetail) _vDetail.get(i);
				mapPurchaseReturnDetailsToInvTrans (_oPR, oPRD, _oConn);
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private static void mapPurchaseReturnDetailsToInvTrans (PurchaseReturn _oPR, 
															PurchaseReturnDetail _oPRD, 
															Connection _oConn)
		throws Exception
	{
		
		StringBuilder oDesc = new StringBuilder ("Returned To ");
		oDesc.append (_oPR.getVendorName());
		oDesc.append ("  ");
		if (StringUtil.isNotEmpty(_oPR.getTransactionNo())) 
		{
			oDesc.append(" Trans No : ");
			oDesc.append(_oPR.getTransactionNo());
		}

		int iItemType = ItemTool.getItemTypeByID (_oPRD.getItemId(), _oConn); 
		if (iItemType == i_INVENTORY_PART)
		{
			InventoryTransaction oInv = new InventoryTransaction ();
			oInv.setTransactionId	    (_oPR.getPurchaseReturnId());	
			oInv.setTransactionNo	    (_oPR.getReturnNo());			
			oInv.setTransactionDetailId (_oPRD.getPurchaseReturnDetailId());	
			oInv.setTransactionType     (i_INV_TRANS_PURCHASE_RETURN); 
			oInv.setTransactionDate     (_oPR.getReturnDate());
			oInv.setLocationId          (_oPR.getLocationId());
			oInv.setLocationName        (LocationTool.getLocationNameByID(_oPR.getLocationId()));
			oInv.setItemId			    (_oPRD.getItemId());
			oInv.setItemCode		    (_oPRD.getItemCode());
			
			double dBaseValue = UnitTool.getBaseValue(_oPRD.getItemId(),_oPRD.getUnitId(), _oConn);
			double dQtyChange = _oPRD.getQty().doubleValue();
			double dQtyBase = dQtyChange * dBaseValue;
			
			double dCost = _oPRD.getItemCost().doubleValue(); //Cost in PR not in Base Unit		
			dCost = dCost * _oPR.getCurrencyRate().doubleValue(); 
			
			//TODO: check again, find effect from recalculate process
			//if (_oPR.getTransactionType() == i_RET_FROM_NONE || dCost == 0)
			//{
				dCost = InventoryLocationTool.getItemCost(_oPRD.getItemId(), _oPR.getLocationId(), 
						_oPR.getReturnDate(), _oConn).doubleValue();
				log.debug("dItemCost = Current COST " + dCost);
			//}
			
			oInv.setQtyChanges	  (new BigDecimal(dQtyBase * -1));
			oInv.setCost		  (new BigDecimal(dCost));
			oInv.setPrice		  (_oPRD.getItemPrice());
			oInv.setItemPrice	  (ItemTool.getPriceExclTax(_oPRD.getItemId(), _oConn));
			oInv.setDescription	  (oDesc.toString() + _oPR.getRemark());

			saveData (oInv, _oPRD, _oConn);
		}
	}

		
	/**
	 * validate whether existing transaction no with same ID exists
	 * 
	 * @param _oInv
	 * @param _oConn
	 * @throws Exception
	 */
	protected static void validateExists(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		if(_oInv != null)
		{
			List vExists = getByTransID(_oInv.getTransactionId(), _oInv.getTransactionType(), _oConn);
			for(int i = 0; i < vExists.size(); i++)
			{
				InventoryTransaction oExist = (InventoryTransaction) vExists.get(i);
				if(!StringUtil.isEqual(oExist.getTransactionNo(), _oInv.getTransactionNo()))
				{
					throw new Exception("Invalid Transaction No " + _oInv.getTransactionNo() + " Same ID Exists with No " + oExist.getTransactionNo());
				}
			}
		}
	}
	
	/**
	 * @deprecated move to InventoryCostingTool
	 * @param _oInv
	 * @param _oConn
	 * @throws Exception
	 */
	private static void saveData (InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		saveData (_oInv, null, _oConn);
	}
	
	/**
	 * save inventory transaction data
	 * 
	 * @deprecated move to InventoryCostingTool
	 * @param _oInv
	 * @param _oBT
	 * @param _oConn
	 * @throws Exception
	 */
	
	protected static void saveData (InventoryTransaction _oInv, InventoryDetailOM _oBT,  Connection _oConn)
		throws Exception
	{		
		Date dNow = new Date();
		//Generate Sys ID
		boolean bNewInvTrans = false; 
		if (StringUtil.isEmpty(_oInv.getInventoryTransactionId()))
		{
			_oInv.setInventoryTransactionId ( IDGenerator.generateSysID() );
			_oInv.setCreateDate(dNow);
			bNewInvTrans = true; 
		}
		//validate exists ID to prevent duplicate transaction
		validateExists(_oInv, _oConn);
		
		_oInv.setUpdateDate(dNow);
		_oInv.save(_oConn);
		
		StopWatch oSW = null;		
		String sSWKey = null;
		if (log.isInfoEnabled())
		{
			sSWKey = "Recalculate " + _oInv.getTransactionNo() + ", Item : " + _oInv.getItemCode(); 
			oSW = new StopWatch();
			oSW.start(sSWKey);
		}
		
		try
		{
			recalculate(_oInv, bNewInvTrans, _oConn);
		}
		catch (Exception _oEx) 
		{
			throw new Exception("ERROR recalculating inventory for Item : " + _oInv.getItemCode() + " " + _oEx.getMessage(), _oEx);
		}
		
		if (log.isInfoEnabled())
		{
			oSW.stop(sSWKey);
			log.debug(oSW.result(sSWKey));
		}
		
		//batch number process
		if (PreferenceTool.useBatchNo() && _oBT != null)
		{
			BatchTransactionTool.saveBatch(_oBT.getBatchTransaction(), _oInv, _oConn);
		}
		
		//serial number process
		if (PreferenceTool.useSerialNo() && _oBT != null)
		{
			ItemSerialTool.saveSerial(_oBT.getSerialTrans(), _oInv, _oConn);
		}		
	}
	
	//-------------------------------------------------------------------------
	// report methods
	//-------------------------------------------------------------------------

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _iGroupBy
	 * @param _sKeywords
	 * @param _sKategoriID
	 * @return List of InventoryTransaction
	 * @throws Exception
	 */
	public static List findData (Date _dStart, 
								 Date _dEnd,
								 int _iCondition,
								 int _iInOut,
								 String _sKeywords,
								 String _sKategoriID,
								 String _sLocationID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(InventoryTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
		oCrit.and(InventoryTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		
		if (StringUtil.isNotEmpty(_sKeywords))
		{
			if(_iCondition == 2)
			{
				oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_NAME,(Object) SqlUtil.like 
						 (ItemPeer.ITEM_NAME,_sKeywords,false,true), Criteria.CUSTOM);
			}
			else if(_iCondition == 1)
			{
				oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_CODE, _sKeywords);				
			}
			else if(_iCondition == 3)
			{
				oCrit.add(InventoryTransactionPeer.TRANSACTION_NO, (Object)_sKeywords, Criteria.ILIKE);
			}
		}
		if (_iInOut == 1) //IN
		{
			oCrit.add(InventoryTransactionPeer.QTY_CHANGES, 0, Criteria.GREATER_EQUAL);
		}
		else if (_iInOut == 2)
		{
			oCrit.add(InventoryTransactionPeer.QTY_CHANGES, 0, Criteria.LESS_THAN);			
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			
			oCrit.addJoin(InventoryTransactionPeer.ITEM_ID,ItemPeer.ITEM_ID);
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			if(vKategori.size() > 0)
			{
				vKategori.add(_sKategoriID);
				oCrit.addIn(ItemPeer.KATEGORI_ID,vKategori);
			}
			else{
				oCrit.add(ItemPeer.KATEGORI_ID,_sKategoriID);
			}
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID);
		}
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
		return InventoryTransactionPeer.doSelect(oCrit);
	}
	
	public static final String getTransactionTypeName (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);

		if (iType == i_INV_TRANS_RECEIPT_UNPLANNED) return s_TRANS_RECEIPT_UNPLANNED;
		if (iType == i_INV_TRANS_ISSUE_UNPLANNED  ) return s_TRANS_ISSUE_UNPLANNED;
		if (iType == i_INV_TRANS_PURCHASE_RECEIPT ) return s_TRANS_PURCHASE_RECEIPT;
		if (iType == i_INV_TRANS_PURCHASE_INVOICE ) return s_TRANS_PURCHASE_INVOICE;
		if (iType == i_INV_TRANS_PURCHASE_RETURN  ) return s_TRANS_PURCHASE_RETURN;
		if (iType == i_INV_TRANS_TRANSFER_OUT     ) return s_TRANS_ITEM_TRANSFER;
		if (iType == i_INV_TRANS_TRANSFER_IN      ) return s_TRANS_ITEM_TRANSFER;		
		if (iType == i_INV_TRANS_DELIVERY_ORDER   ) return s_TRANS_DELIVERY_ORDER;
		if (iType == i_INV_TRANS_SALES_INVOICE    ) return s_TRANS_SALES_INVOICE;
		if (iType == i_INV_TRANS_SALES_RETURN 	  ) return s_TRANS_SALES_RETURN;
		if (iType == i_INV_TRANS_JOB_COSTING      ) return s_TRANS_JOB_COSTING;
		if (iType == i_INV_TRANS_JC_ROLLOVER      ) return s_TRANS_JC_ROLLOVER;
		return "";
	}
	
	public static final String getTransactionScreen (Integer _iTransType) 
	{                            
		int iType = Calculator.toInt(_iTransType);
		
		if (iType == i_INV_TRANS_RECEIPT_UNPLANNED  ) return s_SCREEN_ISSUE_RECEIPT;
		if (iType == i_INV_TRANS_ISSUE_UNPLANNED    ) return s_SCREEN_ISSUE_RECEIPT;
		if (iType == i_INV_TRANS_PURCHASE_RECEIPT   ) return s_SCREEN_PURCHASE_RECEIPT;
		if (iType == i_INV_TRANS_PURCHASE_RETURN    ) return s_SCREEN_PURCHASE_RETURN;
		if (iType == i_INV_TRANS_PURCHASE_INVOICE   ) return s_SCREEN_PURCHASE_INVOICE;
		if (iType == i_INV_TRANS_TRANSFER_OUT 	    ) return s_SCREEN_ITEM_TRANSFER;
		if (iType == i_INV_TRANS_TRANSFER_IN 	    ) return s_SCREEN_ITEM_TRANSFER;		
		if (iType == i_INV_TRANS_SALES_INVOICE 	    ) return s_SCREEN_SALES_INVOICE;
		if (iType == i_INV_TRANS_SALES_RETURN 	 	) return s_SCREEN_SALES_RETURN;
		if (iType == i_INV_TRANS_DELIVERY_ORDER     ) return s_SCREEN_DELIVERY_ORDER;
		if (iType == i_INV_TRANS_JOB_COSTING        ) return s_SCREEN_JOB_COSTING; 
		return "#";
	}
	
	//-------------------------------------------------------------------------
	// sync method
	//-------------------------------------------------------------------------

	/**
	 * get all inventory trans for this location (for remote store setup)
	 * 
	 * @param _sLocationID
	 * @return all InventoryTransaction in Location
	 * @throws Exception
	 */
	public static List getAllInventoryTransaction (String _sLocationID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID);   	    		
	    return InventoryTransactionPeer.doSelect(oCrit);
	}			
	
	//-------------------------------------------------------------------------
	// Average Cost Recalculation
	//-------------------------------------------------------------------------

	/**
	 * recalculate all trans from the first trans
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void recalculateAll(String _sItemID, String _sLocationID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
		oCrit.add(InventoryTransactionPeer.ITEM_ID, _sItemID);
		if (!b_GLOBAL_COSTING)
		{
			oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID);
		}
		oCrit.setLimit(1);
		List vData = InventoryTransactionPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			InventoryTransaction oInv = (InventoryTransaction) vData.get(0);
			recalculate (oInv, false, _oConn);
		}
	}

    /**
     * recalculate all trans from the first trans
     * 
     * @param _sItemID
     * @param _sLocationID
     * @param _oConn
     * @throws Exception
     */
    public static void recalculateFrom (String _sItemID, String _sLocationID, String _sTransNo, double _dCost, Connection _oConn)
        throws Exception
    {
    	boolean bBegin = false;
    	if(_oConn == null)
    	{
    		bBegin = true;
    		_oConn = beginTrans();
    	}
        Criteria oCrit = new Criteria();
        oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
        oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
        oCrit.add(InventoryTransactionPeer.ITEM_ID, _sItemID);
        if(StringUtil.isNotEmpty(_sTransNo))
        {
            oCrit.add(InventoryTransactionPeer.TRANSACTION_NO, _sTransNo);
        }
        if (!b_GLOBAL_COSTING)
        {
            oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID);
        }
        oCrit.setLimit(1);
        List vData = InventoryTransactionPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0)
        {
            InventoryTransaction oInv = (InventoryTransaction) vData.get(0);
            oInv.setCost(new BigDecimal(_dCost));
            recalculate (oInv, false, _oConn);
        }
        if(bBegin)
        {
        	commit(_oConn);
        }
    }

    
	/**
	 * create new item, location, trans_date criteria
	 * 
	 * @param _oInv
	 * @param _iSort
	 * @return built criteria
	 */
	private static Criteria newItemLocation(InventoryTransaction _oInv, int _iSort)
	{
		Criteria oCrit = new Criteria();
		oCrit.add (InventoryTransactionPeer.ITEM_ID, _oInv.getItemId());
		if (!b_GLOBAL_COSTING)
		{
			oCrit.add (InventoryTransactionPeer.LOCATION_ID, _oInv.getLocationId());		
		}
		oCrit.add (InventoryTransactionPeer.INVENTORY_TRANSACTION_ID, 
				(Object)_oInv.getInventoryTransactionId(), Criteria.NOT_EQUAL);

		if (_iSort == i_ASC)
		{
			oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
			oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);			
		}
		else if (_iSort == i_DESC)
		{
			oCrit.addDescendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
			oCrit.addDescendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);
		}			
		return oCrit;
	}
	
	/**
	 * method to check invTrans cost * qty == total cost
	 * 
	 * @param _oLast
	 * @param m_oConn
	 */
	private static boolean isValid (InventoryTransaction _oLast)
	{
		double d1 = _oLast.getQtyChanges().doubleValue() * _oLast.getCost().doubleValue();
		d1 = Calculator.precise(d1);
		
		double d2 = _oLast.getTotalCost().doubleValue();
		d2 = Calculator.precise(d2);
		
		if (d1 == d2 || Calculator.isInTolerance(d1, d2, d_NUMBER_TOLERANCE))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * recalculate item cost from inventory transaction
	 * 
	 * @param _oInv current InventoryTransaction
	 * @param _bNewInvTrans is current InventoryTransaction is a new one
	 * @param _oConn
	 * @throws Exception
	 */
	public static void recalculate(InventoryTransaction _oInv, boolean _bNewInvTrans, Connection _oConn)
		throws Exception
	{
		if (log.isInfoEnabled()) 
		{
			log.info("** Processing Item " + _oInv.getItemCode() + " Trans : " + _oInv.getTransactionNo()); 
		}
		
		//get 1 previous inv trans which the date is just before this trans
		InventoryTransaction oLast = getLastTrans(_oInv, _oConn);

		if (oLast != null)
		{
			//check the prev trans whether the value balance if correct not = 0
			//if not then start from the beginning for this item_id & location_id
			//if (!isValid(oLast))
			//{
			//	updatePrevTrans (_oInv, oLast, _oConn);
			//}
			updateValue (_oInv, oLast, _oConn);
		}
		else
		{			
			//no last trans this should be the first trans
			updateValue (_oInv, null, _oConn);			
		}
		
		//get list of the next inv trans after this one sort by date -> becareful of out of mem
		//update each next invtrans total_cost qty_balance value_balance accordingly
		//use greater than if new, greater equal if update
		oLast = _oInv;
		List vNextTrans = getAllNextTrans(_oInv, _bNewInvTrans, _oConn); 

		if (log.isDebugEnabled()) 
		{
			log.debug("\n** Recalculate, next trans to be updated : \n" + vNextTrans);
		}

		for (int i = 0; i < vNextTrans.size(); i++)
		{
			InventoryTransaction oNext = (InventoryTransaction) vNextTrans.get(i);			
			updateValue (oNext, oLast, _oConn);
			oLast = oNext;
		}
		
		if (log.isDebugEnabled()) 
		{
			log.debug("\n** END Recalculate, Last trans update InvLOC : \n" + oLast);
		}
		
		if (b_GLOBAL_COSTING)
		{
			//before update inventory location, get last trans for this certain location
			InventoryLocationTool.updateGlobal(_oInv, oLast, _bNewInvTrans, false, _oConn);
		}
		else
		{			
			InventoryLocationTool.update(oLast, _oConn);
		}
	}

	/**
	 * get the last trans just before this invTrans param transactionDate
	 * 
	 * 1. try to get the invTrans where trans_date <= _oInv.transDate
	 *    AND createDate < invTrans.createDate
	 * 
	 * 2. if not found then try to find invTrans where trans_date < _oInv.transDate
	 *    
	 * @param _oInv
	 * @param _oConn
	 * @return last InventoryTransaction
	 * @throws Exception
	 */
	protected static InventoryTransaction getLastTrans(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = newItemLocation(_oInv, i_DESC);
		
		//try 1st method trans date equal/less equal trans date and create date less than inv 
		//this method needed to find the closest equal trans_date to inv.trans_date
		Criteria.Criterion oLETDate = 
			oCrit.getNewCriterion(InventoryTransactionPeer.TRANSACTION_DATE, 
					_oInv.getTransactionDate(), Criteria.LESS_EQUAL);

		Criteria.Criterion oLECDate = 
			oCrit.getNewCriterion(InventoryTransactionPeer.CREATE_DATE, 
					_oInv.getCreateDate(), Criteria.LESS_EQUAL);		

		Criteria.Criterion oLETORLEC = oLETDate.and(oLECDate);
		
		Criteria.Criterion oLTTDate = 
			oCrit.getNewCriterion(InventoryTransactionPeer.TRANSACTION_DATE, 
					_oInv.getTransactionDate(), Criteria.LESS_THAN);
		
		oCrit.add(oLETORLEC.or(oLTTDate));		
		
		oCrit.setLimit(1);
		
		if (log.isDebugEnabled()) 
		{
			log.debug("\n1. Last Trans Criteria : \n" + oCrit);
		}
		
		List vData = InventoryTransactionPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
			InventoryTransaction oLast = (InventoryTransaction) vData.get(0);
			
			if (log.isDebugEnabled()) 
			{
				log.debug("\nLast Trans is : " + oLast);
			}			
			return oLast;
		}
		else 
		{
			//if 1st method return no result try 2nd method where trans_date less than inv.trans_date
			//but create date is > inv.create_date
			oCrit = newItemLocation(_oInv, i_DESC);
			oCrit.add (InventoryTransactionPeer.TRANSACTION_DATE, _oInv.getTransactionDate(), Criteria.LESS_THAN);		
			oCrit.setLimit(1);
			
			if (log.isDebugEnabled()) 
			{
				log.debug("\n2. Last Trans Criteria (2nd try) : \n" + oCrit);
			}
			
			vData = InventoryTransactionPeer.doSelect (oCrit, _oConn);			
			if (vData.size() > 0) 
			{
				InventoryTransaction oLast = (InventoryTransaction) vData.get(0);
				
				if (log.isDebugEnabled()) 
				{
					log.debug("\nLast Trans is : " + oLast);
				}			
				return oLast;
			}
		}
		return null;
	}

	/**
	 * update each prev trans
	 * 
	 * @param _oInv
	 * @param _oLast
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updatePrevTrans (InventoryTransaction _oInv, 
										 InventoryTransaction _oLast, 
										 Connection _oConn)
		throws Exception
	{
		_oLast = null; 
		List vPrevTrans =  getAllPrevTrans(_oInv, _oConn);
		for (int i = 0; i < vPrevTrans.size(); i++)
		{
			InventoryTransaction oPrev = (InventoryTransaction) vPrevTrans.get(i);
			updateValue (oPrev, _oLast, _oConn);
			_oLast = oPrev;
		}
	}	
	
	/**
	 * get all prev trans before this inv_trans
	 * 
	 * @param _oInv
	 * @param _oConn
	 * @return All previous transaction 
	 * @throws Exception
	 */
	private static List getAllPrevTrans(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = newItemLocation(_oInv, i_ASC);
		oCrit.add (InventoryTransactionPeer.TRANSACTION_DATE, _oInv.getTransactionDate(), Criteria.LESS_EQUAL);
		List vResult = InventoryTransactionPeer.doSelect (oCrit, _oConn);
		
		if (log.isDebugEnabled()) 
		{
			log.debug("\nPrev Inv Trans Criteria : " + oCrit + "\n Result : " + vResult.size());
		}
		return vResult;
	}		

	/**
	 * Validate Qty Balance value that will be set to InvTrans
	 * if Strict minus then throw exception
	 * 
	 * @param dQtyBalance
	 * @param _oInv
	 * @param _oPrev
	 * @param _oConn
	 * @throws Exception
	 */
	protected static void validateQtyBalance(double dQtyBalance, 
                                           double dCost,
                                           double dValueBalance,
										   InventoryTransaction _oInv, 
										   InventoryTransaction _oPrev, 
										   Connection _oConn)
		throws Exception
	{		
		//validate temporary qty balance to not allow minus at all 
		if (dQtyBalance < 0)
		{
			log.warn("WARNING: Qty Balance is Minus \nInvTrans : " + _oInv + "\nPrev : " + _oPrev); 
			
			Location oLoc = LocationTool.getLocationByID(_oInv.getLocationId(), _oConn);
			//if stric minus is set via "inventory.strict.minus" in prop throw error
			if (oLoc.getInventoryType() == i_INV_NORMAL && b_STRICT_MINUS)
			{ 
				throw new NestableException("Transaction " + _oInv.getTransactionNo() + 
						" Qty Balance is minus, please check Item " + _oInv.getItemCode() + 
						" history in Location : " + _oInv.getLocationName());
			}
		}
        //TODO: validate & test process
		double dTolerance = -0.1;
        if ((dCost < 0 || dValueBalance < dTolerance) && b_STRICT_MINUS)
        {
            throw new NestableException("Transaction " + _oInv.getTransactionNo() + 
                    " COST (" + dCost + ") OR VALUE Balance (" + dValueBalance +   
            		") is minus, please check Item " + _oInv.getItemCode() + 
                    " history in Location : " + _oInv.getLocationName());                
        }
	}
	/**
	 * update current inv_trans value based on previous inv_trans value
	 * set current inv_trans QtyBalance & ValueBalance
	 * 
	 * @param _oInv
	 * @param _oPrev
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updateValue(InventoryTransaction _oInv, InventoryTransaction _oPrev, Connection _oConn)
		throws Exception
	{
		if (_oPrev == null) //if no previous value just set totalCost, qtyBalance, valueBalance using its own value
		{			
			double dQtyChanges = _oInv.getQtyChanges().doubleValue();
			double dCost = _oInv.getCost().doubleValue();
			double dValueBalance = _oInv.getTotalCost().doubleValue();
            
			//validate temporary qty balance to not allow minus at all 
			validateQtyBalance(dQtyChanges, dCost, dValueBalance, _oInv, _oPrev, _oConn);
			
			_oInv.setTotalCost(new BigDecimal(dQtyChanges * dCost));
			_oInv.setQtyBalance(_oInv.getQtyChanges());
			_oInv.setValueBalance(_oInv.getTotalCost());
			_oInv.setUpdateDate(new Date());
			_oInv.save(_oConn);					
			
			if (log.isDebugEnabled())
			{
				log.debug("\n No Prev, Inv Trans After UPDATE VALUE : " + _oInv);
			}
		}
		else
		{
			int iType = _oInv.getTransactionType();
			//if inv trans which need updated
			if (iType == i_INV_TRANS_SALES_INVOICE || 
				iType == i_INV_TRANS_DELIVERY_ORDER ||
				iType == i_INV_TRANS_SALES_RETURN)
			{
				//only update trans if current location is in HO else do not update
				//because source trans is might not available in other location else than HO
				if (PreferenceTool.getLocationFunction() == i_HEAD_OFFICE)
				{
					updateTrans (_oInv, _oPrev, iType, _oConn);
				}
			}//end if trans type is which cost is affected by prev trans 
			
			//boolean bTransferTo = false;
			//item transfer do update trans for FROM only
			if (iType == i_INV_TRANS_TRANSFER_OUT || iType == i_INV_TRANS_TRANSFER_IN)
			{
				if (PreferenceTool.getLocationFunction() == i_HEAD_OFFICE)
				{
					if (iType == i_INV_TRANS_TRANSFER_OUT) //update cost from prev
					{
						updateTrans(_oInv, _oPrev, iType, _oConn);
					}
					else if (iType == i_INV_TRANS_TRANSFER_IN)
					{
						//if global costing then set cost using prev value as done in update trans
						if (b_GLOBAL_COSTING)
						{
							double dCost = 0;
							//prevent NaN if prev qty balance 0 then get prev cost directly
							if (_oPrev.getQtyBalance().doubleValue() == 0) 
							{
								dCost = _oPrev.getCost().doubleValue();
							}
							else //on normal condition the cost will be calculated using prev valuebalance/qtybalance
							{
								dCost = _oPrev.getValueBalance().doubleValue() / _oPrev.getQtyBalance().doubleValue();
							}
							_oInv.setCost(new BigDecimal(dCost));
						}//if not global costing do not do this because value wil be set by Transfer FROM recalculate
						//bTransferTo = true;
					}
				}
			}
			
			boolean bIRCostAdj = false;
			//issue or receipt unplanned is sometimes cost adjustment so it's not affected
			//by previous trans
			if (iType == i_INV_TRANS_RECEIPT_UNPLANNED || 
				iType == i_INV_TRANS_ISSUE_UNPLANNED) 
			{
				if (PreferenceTool.getLocationFunction() == i_HEAD_OFFICE)
				{
					IssueReceipt oIR = IssueReceiptTool.getHeaderByID(_oInv.getTransactionId(), _oConn);					
					if (oIR != null & !oIR.getCostAdjustment())
					{
						updateTrans (_oInv, _oPrev, iType, _oConn);
					}
					else
					{
						bIRCostAdj = true;
					}
				}				
			}
			
			//update value process based on prev inv trans
			double dPrevQtyBalance = _oPrev.getQtyBalance().doubleValue();
			double dPrevValueBalance = _oPrev.getValueBalance().doubleValue();
			
			//for out transaction updated in trans above, current _oInv.getCost() value
			//has been altered by the prev value balance / qty balance value
			double dQtyChanges = _oInv.getQtyChanges().doubleValue();
			double dTotalCost = _oInv.getQtyChanges().doubleValue() * _oInv.getCost().doubleValue();
			double dQtyBalance = dPrevQtyBalance + dQtyChanges;
			double dValueBalance = dPrevValueBalance + dTotalCost;
			
			//if prev qty baalnce is minus make some adjustment to correct current value balance
			if ((iType == i_INV_TRANS_PURCHASE_RECEIPT || 
				 iType == i_INV_TRANS_PURCHASE_INVOICE ||
				 iType == i_INV_TRANS_PURCHASE_RETURN  || 
				 iType == i_INV_TRANS_TRANSFER_IN      || bIRCostAdj) && dPrevQtyBalance < 0)
			{
				if (DateUtil.isBackDate(_oInv.getTransactionDate()))
				{
					if (dQtyChanges > (dPrevQtyBalance * -1)) //only re-avg cost if qty changes > previous qty balance
					{
						//TODO: CHECK THIS METHOD, THIS METHOD ASSUME PREVIOUS COST IS CORRECT
						if (dPrevValueBalance < 0) dPrevValueBalance = dPrevValueBalance * -1; //set prev value bal to +
						if (dPrevQtyBalance < 0) dPrevQtyBalance = dPrevQtyBalance * -1; //set prev qty to +
						double dTmpQty = (dPrevQtyBalance + dQtyChanges) ;
						if (dTmpQty != 0)
						{						
							dValueBalance = ((dPrevValueBalance + dTotalCost) / dTmpQty) * dQtyBalance;
						}
					}
				}
			}
			
            double dCost = _oInv.getCost().doubleValue();
			//validate temporary qty balance to not allow minus at all 
			validateQtyBalance(dQtyBalance, dCost, dValueBalance, _oInv, _oPrev, _oConn);
			
			//validate against nan and infinity
			if (dValueBalance == Double.NaN || 
				dValueBalance == Double.NEGATIVE_INFINITY || 
				dValueBalance == Double.POSITIVE_INFINITY)
			{
				log.warn("WARNING: Value Balance is NaN / Infinity \nInvTrans : " + 
						 _oInv + "\nPrev : " + _oPrev);
				dValueBalance = 0;
			}
			
			_oInv.setTotalCost(new BigDecimal(dTotalCost));
			_oInv.setQtyBalance(new BigDecimal(dQtyBalance));
			_oInv.setValueBalance(new BigDecimal(dValueBalance));
			_oInv.setUpdateDate(new Date());
			_oInv.save(_oConn);
			
			if (log.isDebugEnabled())
			{
				log.debug("\n Prev Exist, Current Inv Trans now : " + _oInv);
			}
		}
	}		
	
	/**
	 * Update Cost in source transaction of an InventoryTransaction
	 * also will update GL value
	 * 
	 * @param _oInv current inv trans must not null
	 * @param _oPrev previous inv trans must not null
	 * @param iType
	 * @param _oConn
	 * @throws Exception
	 * 
	 */
	private static void updateTrans (InventoryTransaction _oInv, 
							 		 InventoryTransaction _oPrev, 
									 int iType, 
									 Connection _oConn)
		throws Exception
	{
		double dCost = 0;
		
		//prevent NaN if prev qty balance 0 then get prev cost directly
		if (_oPrev.getQtyBalance().doubleValue() == 0) 
		{
			dCost = _oPrev.getCost().doubleValue();
		}
		else //on normal condition the cost will be calculated using prev valuebalance/qtybalance
		{
			dCost = _oPrev.getValueBalance().doubleValue() / _oPrev.getQtyBalance().doubleValue();
		}
		
		double dOldCost = _oInv.getCost().doubleValue();
		double dQty = _oInv.getQtyChanges().doubleValue();
		if (dQty < 0) dQty = dQty * -1;
		
		double dSubTotal = dCost * dQty;
		double dOldSubTotal = dOldCost * dQty;
		
		double dDelta = dCost - dOldCost;
		double dSTDelta = dSubTotal - dOldSubTotal;
		
		//check if cost updating really needed
		boolean bUpdateCost = false;
		if (Calculator.precise(dCost) != Calculator.precise(dOldCost) &&
		    Calculator.precise(dSubTotal) != Calculator.precise(dOldSubTotal) &&
		    !PeriodTool.isClosed(_oPrev.getTransactionDate(), _oConn)) 
		{
			bUpdateCost = true;
		}
		
		if (bUpdateCost) 
		{
			Item oItem = ItemTool.getItemByID(_oInv.getItemId(), _oConn);
			String sInvAccount = oItem.getInventoryAccount();
			String sTransID  = _oInv.getTransactionId();
			String sDetailID = _oInv.getTransactionDetailId();
			String sTransNo  = _oInv.getTransactionNo();

			if (log.isDebugEnabled())
			{
				log.debug("\nTrans " + sTransNo + " Item " + oItem.getItemCode() +
						  " Old Cost " + dOldCost + " will be updated to " + dCost);
			}
			
			int iGLType = 0;
			boolean bUpdateGL = true;
			
			//update sales source transaction && gl journal
			if (iType == i_INV_TRANS_SALES_INVOICE || 
				iType == i_INV_TRANS_DELIVERY_ORDER || 
				iType == i_INV_TRANS_SALES_RETURN)
			{
				String sCOGSAccount = oItem.getCogsAccount();												
				
				//update trans detail cost, subTotalCost, trans master totalCost and gl
//				if (iType == i_INV_TRANS_SALES_INVOICE)
//				{
//					SalesTransactionDetail oDetail = TransactionTool.getDetailByDetailID(sDetailID, _oConn);
//					SalesTransaction oTrans = TransactionTool.getHeaderByID(sTransID, _oConn);
//					if (oDetail != null && oTrans != null)
//					{
//						Item oDetItem = ItemTool.getItemByID(oDetail.getItemId(), _oConn);						
//						//if grouping then check COGS account to item part or to group and get qty for cost delta
//						if (oDetItem.getItemType() == i_GROUPING)
//						{
//							if (i_GL_GROUPING_COST == 1) sCOGSAccount = oDetItem.getCogsAccount();	
//							dDelta = dDelta * ItemGroupTool.getQty(oDetItem.getItemId(),oItem.getItemId(),_oConn);
//						}
//	
//						oDetail.setItemCost(new BigDecimal(oDetail.getItemCost().doubleValue() + dDelta));
//						oDetail.setSubTotalCost(new BigDecimal(oDetail.getSubTotalCost().doubleValue() + dSTDelta));
//						oDetail.save(_oConn);
//						
//						oTrans.setTotalCost(new BigDecimal(oTrans.getTotalCost().doubleValue() + dSTDelta));
//						oTrans.save(_oConn);
//						iGLType = GlAttributes.i_GL_TRANS_SALES_INVOICE;
//
//						//update GL
//						GlTransactionTool.updateSalesGLTrans(iGLType, sTransID, sTransNo, sInvAccount, sCOGSAccount, dSTDelta, _oConn);
//						
//						if (log.isDebugEnabled())
//						{
//							log.debug("\nTotal Cost in sales " + sTransNo + " updated with amount : " + dSTDelta);
//						}
//					}
//					else
//					{
//						log.error("UpdateTrans: Sales Invoice Detail " + sTransNo + " NOT FOUND !");
//					}
				}
				
				//update trans detail costPerUnit, salesTransactionDetail && salesTransaction if exist
				if (iType == i_INV_TRANS_DELIVERY_ORDER)
				{					
//					String sDOAccount = "";
//					Account oDOAcc = AccountTool.getDeliveryOrderAccount(sCOGSAccount,_oConn);
//					if(oDOAcc != null) sDOAccount = oDOAcc.getAccountId();
//
//					DeliveryOrderDetail oDetail = DeliveryOrderTool.getDetailByDetailID(sDetailID, _oConn);
//					if (oDetail != null)
//					{
//						Item oDetItem = ItemTool.getItemByID(oDetail.getItemId(), _oConn);
//	
//						//if grouping then check COGS account to item part or to group and get qty for cost delta
//						if (oDetItem.getItemType() == i_GROUPING)
//						{
//							if (i_GL_GROUPING_COST == 1) sCOGSAccount = oDetItem.getCogsAccount();	
//							dDelta = dDelta * ItemGroupTool.getQty(oDetItem.getItemId(),oItem.getItemId(),_oConn);
//						}
//						
//						oDetail.setCostPerUnit(new BigDecimal(oDetail.getCostPerUnit().doubleValue() + dDelta));
//						oDetail.save(_oConn);
//						
//						iGLType = GlAttributes.i_GL_TRANS_DELIVERY_ORDER;
//						GlTransactionTool.updateSalesGLTrans(
//							iGLType, sTransID, sTransNo, sInvAccount, sDOAccount, dSTDelta, _oConn);
//
//						//update SalesTransactionDetail cost if exists
//						SalesTransactionDetail oTRD = TransactionTool.getDetailByDODetailID(sDetailID, _oConn);
//						if (oTRD != null)
//						{
//							oTRD.setItemCost(oDetail.getCostPerUnit());
//							oTRD.save(_oConn);
//							SalesTransaction oTrans = TransactionTool.getHeaderByID(oTRD.getSalesTransactionId(), _oConn);
//							if (oTrans != null)
//							{
//								oTrans.setTotalCost(new BigDecimal(oTrans.getTotalCost().doubleValue() + dSTDelta));
//								oTrans.save(_oConn);
//								
//								if (oTrans.getStatus() == i_TRANS_PROCESSED && !StringUtil.isEqual(sDOAccount,sCOGSAccount))
//								{
//									sTransID = oTrans.getSalesTransactionId();
//									sTransNo = oTrans.getInvoiceNo();
//									
//									iGLType = GlAttributes.i_GL_TRANS_SALES_INVOICE;
//									GlTransactionTool.updateSalesGLTrans(
//										iGLType, sTransID, sTransNo, sCOGSAccount, sDOAccount, dSTDelta, _oConn);
//								}
//							}
//							else
//							{
//								log.error("UpdateTrans: SalesTransaction NOT FOUND ! " + oTRD.getSalesTransactionId());
//							}
//						}		
//					}
//					else
//					{
//						log.error("UpdateTrans: DeliveryOrderDetail " + sTransNo + " NOT FOUND !");
//					}					
				}
				
				//sales return if not from none, need to check whether cost from source transaction (DO/SI)
				//is updated, if the source trans updated then we should update the return detail trans 
				//and gl value
				if (iType == i_INV_TRANS_SALES_RETURN)
				{					
//					SalesReturnDetail oDetail = SalesReturnTool.getDetailByDetailID(sDetailID, _oConn);
//					SalesReturn oSR = SalesReturnTool.getHeaderByID(sTransID, _oConn);
//					int iReturnFrom = oSR.getTransactionType();
//					if (oDetail != null && oSR != null)
//					{
//						Item oDetItem = ItemTool.getItemByID(oDetail.getItemId(), _oConn);
//						double dGroupQty = 1;
//						
//						//if grouping then check COGS account to item part or to group and get qty for cost delta
//						if (oDetItem.getItemType() == i_GROUPING)
//						{
//							if (i_GL_GROUPING_COST == 1) sCOGSAccount = oDetItem.getCogsAccount();	
//							dGroupQty = ItemGroupTool.getQty(oDetItem.getItemId(),oItem.getItemId(),_oConn);
//						}
//						
//						if (iReturnFrom != i_RET_FROM_NONE)
//						{
//							int iTransType = i_INV_TRANS_SALES_INVOICE;
//							if (iReturnFrom == i_RET_FROM_PR_DO) 
//							{
//								iTransType = i_INV_TRANS_DELIVERY_ORDER;
//								Account oAcc = AccountTool.getDeliveryOrderAccount(sCOGSAccount,_oConn);
//								if(oAcc != null) sCOGSAccount = oAcc.getAccountId();
//							}
//							
//							//get source trans detail -> maynot be null because if sales return exist
//							//then sales trans or do may not be deleted
//							InventoryTransaction oSRC = 
//								getByTransDetailID(oDetail.getTransactionDetailId(), iTransType, _oConn);
//
//							if (oSRC != null)
//							{
//								//TODO: check if global costing
//								double dQtyBalance = oSRC.getQtyBalance().doubleValue();
//								if (dQtyBalance != 0)
//								{
//									dCost = oSRC.getValueBalance().doubleValue() / dQtyBalance;
//								}
//								else
//								{
//									dCost = oSRC.getCost().doubleValue();
//								}
//							}
//							else
//							{
//								log.error("UpdateTrans: Return No " + sTransNo + " Sales Trans NOT FOUND !");
//							}
//							dSubTotal = dCost * dQty;					
//							dDelta = dCost - dOldCost;
//							dSTDelta = dSubTotal - dOldSubTotal;					
//						}
//						dDelta = dDelta * dGroupQty; //needed if item is grouping only, if inv then dGroupQty = 1
//						
//						oDetail.setItemCost(new BigDecimal(oDetail.getItemCost().doubleValue() + dDelta));
//						oDetail.setSubTotalCost(new BigDecimal(oDetail.getSubTotalCost().doubleValue() + dSTDelta));
//						oDetail.save(_oConn);
//												
//						//update GL
//						iGLType = GlAttributes.i_GL_TRANS_SALES_RETURN;
//						GlTransactionTool.updateSalesGLTrans(iGLType, sTransID, sTransNo, sInvAccount, sCOGSAccount, dSTDelta, _oConn);
//					}
//					else
//					{
//						log.error("UpdateTrans: Sales Return Master / Detail " + sTransNo + " NOT FOUND !");
//					}					
//				}
//				//update gl
//				if (bUpdateGL)
//				{
//					try
//					{
//						//GlTransactionTool.updateSalesGLTrans(
//								//iGLType, sTransID, sTransNo, sInvAccount, sCOGSAccount, dSTDelta, _oConn);							
//					}
//					catch(Exception _oEx)
//					{
//						StringBuilder oSB = new StringBuilder();
//						oSB.append("Update Sales GL Trans ").append(sTransNo)
//						   .append(" Item ").append(oItem.getItemCode())
//						   .append(" ERROR: ").append(_oEx.getMessage());
//						log.error(_oEx);
//						throw new Exception (oSB.toString());
//					}
//				}
			}
			
			//issue receipt neeed gl update to reflect inventory value updated
			if (iType == i_INV_TRANS_RECEIPT_UNPLANNED || iType == i_INV_TRANS_ISSUE_UNPLANNED)
			{
//				IssueReceipt oIR = IssueReceiptTool.getHeaderByID(sTransID, _oConn);				
//				if (oIR != null)
//				{
//					if (!oIR.getCostAdjustment()) //only update trans if it is not cost adjustment
//					{
//						IssueReceiptDetail oDetail = IssueReceiptTool.getDetailByDetailID(sDetailID, _oConn);
//						if (oDetail != null)
//						{
//							String sAdjAccount = oIR.getAccountId();
//							GlTransaction oGLTrans = null;
//							
//							oDetail.setCost(new BigDecimal(oDetail.getCost().doubleValue() + dDelta));
//							oDetail.save(_oConn);
//	
//							int iDC = GlAttributes.i_DEBIT; 
//							if (iType == i_INV_TRANS_RECEIPT_UNPLANNED) 
//							{
//								iGLType = GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED;
//								if (oDetail.getQtyChanges().doubleValue() < 0) iDC = GlAttributes.i_CREDIT;
//								
//							}
//							if (iType == i_INV_TRANS_ISSUE_UNPLANNED) 
//							{
//								iDC = GlAttributes.i_CREDIT;
//								iGLType = GlAttributes.i_GL_TRANS_ISSUE_UNPLANNED;
//								if (oDetail.getQtyChanges().doubleValue() < 0) iDC = GlAttributes.i_DEBIT;
//							}						
//							try
//							{
//								GlTransactionTool.updateIRGLTrans(
//									iGLType, iDC, sTransID, sTransNo, sInvAccount, sAdjAccount, oIR.getLocationId(), dSTDelta, _oConn);
//							}
//							catch(Exception _oEx)
//							{
//								StringBuilder oSB = new StringBuilder();
//								oSB.append("Update IR GL Trans ").append(sTransNo)
//								   .append(" Item ").append(oItem.getItemCode())
//								   .append(" ERROR: ").append(_oEx.getMessage());
//								log.error(_oEx);
//								throw new NestableException (oSB.toString());
//							}
//						}
//					}
//				}
//				else
//				{
//					log.error("UpdateTrans: Issue Receipt Master / Detail " + sTransNo + " NOT FOUND !");
//				}				
			}					
			if (iType == i_INV_TRANS_TRANSFER_OUT || iType == i_INV_TRANS_TRANSFER_IN)
			{
//				ItemTransfer oIT = ItemTransferTool.getHeaderByID(sTransID, _oConn);
//				if (oIT != null)
//				{
//					if (iType == i_INV_TRANS_TRANSFER_OUT) //if TRANSFER OUT
//					{	
//						iGLType = GlAttributes.i_GL_TRANS_ITEM_TRANSFER;
//						ItemTransferDetail oDetail = ItemTransferTool.getDetailByDetailID(sDetailID, _oConn);
//						if (oDetail != null)
//						{
//							oDetail.setCostPerUnit(new BigDecimal(dCost));
//							oDetail.save(_oConn);
//						}
//						if (b_TRANSFER_JOURNAL)
//						{
//							GlTransactionTool.updateTransferGLTrans(
//								iGLType, sTransID, sTransNo, sInvAccount, 
//									oIT.getFromLocationId(), oIT.getToLocationId(), dSTDelta, _oConn);
//						}
//						
//						if (!b_GLOBAL_COSTING) //Transfer TO only need update if costing is not GLOBAL (PER LOCATION)
//						{ 
//							//also update invTrans which locationId == itemTransfer.toLocationId
//							InventoryTransaction oToTrans = getByTransDetailID (
//								sDetailID, oIT.getItemTransferId(), oIT.getToLocationId(), 
//									i_INV_TRANS_TRANSFER_IN, _oConn);
//							
//							if (oToTrans != null)
//							{
//								log.debug("\nTransfer IN TRANS: " + oToTrans);	
//								oToTrans.setCost(new BigDecimal(dCost));			
//								//recalculate TO trans
//								recalculate(oToTrans, false, _oConn);
//							}
//							else
//							{
//								log.error("UpdateTrans: Item Transfer " + sTransNo + " To InvTrans NOT FOUND !");
//							}
//						}
//					}
//					else //do not update, because TI must only be updated by TO
//					{
//						return;
//					}
//				}
//				else
//				{
//					log.error("UpdateTrans: Item Transfer " + sTransNo + " NOT FOUND !");
//				}		
			}
			//update this inv trans cost
			_oInv.setCost(new BigDecimal(dCost));			
		}//end if cost updating needed		
	}

	/**
	 * get the next trans after this invTrans param transactionDate
	 * 1. try to get the invTrans where trans_date >= _oInv.transDate 
	 *    AND createDate > invTrans.createDate
	 * 2. try to find invTrans where trans_date > _oInv.transDate then 
	 *    merge with no 1 result
	 * 
	 * @param _oInv
	 * @param _bNewInvTrans 
	 * @param _oConn
	 * @return all next InventoryTransaction
	 * @throws Exception
	 */
	static List getAllNextTrans(InventoryTransaction _oInv, boolean _bNewInvTrans, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = newItemLocation(_oInv, i_ASC);
		List vResult = new ArrayList();
		if (!_bNewInvTrans)
		{ 
			//this method needed to find the NEXT transDate = _oInv.transDate 
			//which is created AFTER this _oInv.createDate
			oCrit.add (InventoryTransactionPeer.TRANSACTION_DATE, _oInv.getTransactionDate());
			oCrit.add (InventoryTransactionPeer.CREATE_DATE, _oInv.getCreateDate(), Criteria.GREATER_THAN);
			List vResultGE = InventoryTransactionPeer.doSelect (oCrit, _oConn);			
			
			if (log.isDebugEnabled())
			{
				log.debug("\n1. Next Inv Trans Criteria (DATE EQUAL) : \n" + oCrit + 
						  "\n Result : " + vResultGE.size());
			}
			
			//2nd method : try to find the NEXT trans which trans_date > _oInv.transDate 
			//and no need to consider _oInv.createDate
			oCrit = newItemLocation(_oInv, i_ASC);
			oCrit.add (InventoryTransactionPeer.TRANSACTION_DATE, 
					   _oInv.getTransactionDate(), Criteria.GREATER_THAN);
			
			vResult = InventoryTransactionPeer.doSelect (oCrit, _oConn);

			if (log.isDebugEnabled())
			{
				log.debug("\n2. Next Inv Trans Criteria (DATE EQUAL) : \n" + oCrit + 
						  "\n Result : " + vResult.size());
			}
			
			//append vResultGE to vResult
			for (int i = 0; i < vResultGE.size(); i++)
			{
				vResult.add(i, vResultGE.get(i));
			}			
		}
		else //if new inv trans then query for all inv trans which trans dat > current inv trans date
		{
			oCrit.add (InventoryTransactionPeer.TRANSACTION_DATE, 
					   _oInv.getTransactionDate(), Criteria.GREATER_THAN);
			vResult = InventoryTransactionPeer.doSelect (oCrit, _oConn);
			if (log.isDebugEnabled())
			{
				log.debug("\n Next Inv Trans Criteria (DATE GT) : \n" + oCrit + 
						  "\n Result : " + vResult.size());
			}
		}
		
		if (vResult.size() > 0 && log.isInfoEnabled())
		{
			log.info(_oInv.getItemCode() + " Total Trans to recalculate : " + vResult.size());
		}
		return vResult;
	}		
	
	/**
	 * delete all inv trans related to this trans
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void delete(String _sTransID, int _iTransType, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		List vInvTrans = getByTransID(_sTransID, _iTransType, _oConn);
		
		//recalculate all based on the last trans
		for (int i = 0; i < vInvTrans.size(); i++)
		{
			InventoryTransaction oInv = (InventoryTransaction) vInvTrans.get(i);
			delete (oInv, _oConn);
		
			//delet batch
			if (PreferenceTool.useBatchNo())
			{
				BatchTransactionTool.deleteBatch(oInv, _oConn);
			}
			
			if (PreferenceTool.useSerialNo())
			{
				ItemSerialTool.deleteSerial(oInv, _oConn);
			}
		}	
	}		
    
    /**
     * delete all inv trans related to this trans
     * 
     * @param _sTransID
     * @param _oConn
     * @throws Exception
     */
    public static void delete(String _sTransID, String _sTransNo, int _iTransType, Connection _oConn)
        throws Exception
    {
        validate(_oConn);
        List vInvTrans = getByTransID(_sTransID, _sTransNo, _iTransType, _oConn);
        
        //recalculate all based on the last trans
        for (int i = 0; i < vInvTrans.size(); i++)
        {
            InventoryTransaction oInv = (InventoryTransaction) vInvTrans.get(i);
            delete (oInv, _oConn);
        
            //delet batch
            if (PreferenceTool.useBatchNo())
            {
                BatchTransactionTool.deleteBatch(oInv, _oConn);
            }
            
            if (PreferenceTool.useSerialNo())
            {
                ItemSerialTool.deleteSerial(oInv, _oConn);
            }
        }   
    }       

	/**
	 * 
	 * @param _oInv invTrans to delete
	 * @param _oConn
	 * @throws Exception
	 */
	private static void delete(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		if (log.isDebugEnabled()) log.debug("\n** START Delete Trans " + _oInv);
				
		//get 1 previous inv trans which the date is just before this trans
		InventoryTransaction oLast = getLastTrans(_oInv, _oConn);
		
		//get next trans which greater equal because the trans deleted might be the first and
		//there are next trans which date is equal to the trans deleted
		List vNextTrans =  getAllNextTrans(_oInv, false, _oConn); 
		
		//if no previous trans, then set the first of the next trans to be the last trans
		if (oLast == null)
		{
			if (vNextTrans.size() > 0)
			{
				oLast = (InventoryTransaction) vNextTrans.get(0);
				vNextTrans.remove(0);

				if (log.isDebugEnabled()) 
				{
					log.debug("\nDelete, no last, first next trans is : " + oLast);
				}

				//update this first next trans also, because the cost might be differs from the prev
				updateValue (oLast, null, _oConn); 

				if (log.isDebugEnabled()) 
				{
					log.debug("\nDelete, no last, first next trans (after update value) : " + oLast);
				}
			}
		}
		else
		{
			//if prev trans exist, check if it's correct else update all prev trans
			//if (!isValid(oLast))
			//{
				//updatePrevTrans (_oInv, oLast, _oConn);
			//}		
		}
		log.debug("Delete, next trans to be updated : " + vNextTrans);
		//Thread.sleep(100000);
		//recalculate all based on the last trans
		for (int i = 0; i < vNextTrans.size(); i++)
		{
			InventoryTransaction oNext = (InventoryTransaction) vNextTrans.get(i);
			log.debug("Processing : " + oNext);
			//Thread.sleep(10000);
			updateValue (oNext, oLast, _oConn);
			oLast = oNext;
		}

		log.debug("** END Delete, Last trans that will update Inventory Location : \n : " + oLast);

		if (oLast != null)
		{
			if (b_GLOBAL_COSTING)
			{
				//before update inventory location, get last trans for this certain location
				InventoryLocationTool.updateGlobal(_oInv, oLast, false, true, _oConn);
			}
			else
			{
				InventoryLocationTool.update(oLast, _oConn);
			}		
		}
		else
		{
			//this is the first inv trans and no next trans so just delete the inv loc
			InventoryLocationTool.delete(_oInv, _oConn);			
		}
		
		//delete the intended inv trans
		InventoryTransactionPeer.doDelete(
			new Criteria().add(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID, 
				_oInv.getInventoryTransactionId()), _oConn);
	}	

	//-------------------------------------------------------------------------
	// END Average Cost Recalculation
	//-------------------------------------------------------------------------
    
	/**
	 * get last in / out date
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sLocationID
	 * @param _dToDate
	 * @param _bIn
	 * @return
	 * @throws TorqueException
	 */
    public static Date getLastDate(String _sItemID, 
						           String _sSKU, 
						           String _sLocationID, 
						           Date _dToDate,
						           boolean _bIn)
        throws TorqueException
    {
        Date dResult = null;
        Criteria oCrit = new Criteria();
        
        oCrit.add(InventoryTransactionPeer.ITEM_ID, _sItemID);
        if(_bIn)
        {
            oCrit.add(InventoryTransactionPeer.QTY_CHANGES, Double.valueOf(0),Criteria.GREATER_THAN);
        }
        else
        {
            oCrit.add(InventoryTransactionPeer.QTY_CHANGES, Double.valueOf(0),Criteria.LESS_THAN);
        }
            
        if(StringUtil.isNotEmpty(_sSKU))
        {
            oCrit.add(ItemPeer.ITEM_SKU, _sSKU);
        }
        if(StringUtil.isNotEmpty(_sLocationID))
        {
            oCrit.add(InventoryTransactionPeer.LOCATION_ID, _sLocationID);
        }
        if(_dToDate != null)
        {
            oCrit.add(InventoryTransactionPeer.TRANSACTION_DATE,_dToDate,Criteria.LESS_EQUAL);
        }
        
        oCrit.addJoin(ItemPeer.ITEM_ID, InventoryTransactionPeer.ITEM_ID);
        oCrit.addDescendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
        oCrit.setOffset(0);
        oCrit.setLimit(1);
        
        List vInvTrans = InventoryTransactionPeer.doSelect(oCrit);
        
        if(vInvTrans.size() > 0)
        {
            InventoryTransaction oInvTrans = (InventoryTransaction) vInvTrans.get(0);
            dResult = oInvTrans.getTransactionDate();
        }
        return dResult;
    }
    
    /**
     * 
     * @param _sPRID
     * @param _oConn
     * @return
     * @throws Exception
     */
    public static List getNextTransferAfterPR(PurchaseReceipt _oPR, Connection _oConn)
    	throws Exception
    {
    	List v = new ArrayList();
    	 	
    	if(_oPR != null)
    	{
    		List vPRD = PurchaseReceiptTool.getDetailsByID(_oPR.getPurchaseReceiptId(), _oConn);
    		List vItemID = new ArrayList(vPRD.size());
    		for(int i = 0; i < vPRD.size(); i++)
    		{
    			vItemID.add(((PurchaseReceiptDetail)vPRD.get(i)).getItemId());
    		}

    		Criteria oCrit = new Criteria();
    		oCrit.addIn(InventoryTransactionPeer.ITEM_ID, vItemID);
    		oCrit.add(InventoryTransactionPeer.LOCATION_ID, _oPR.getLocationId());
    		oCrit.add(InventoryTransactionPeer.TRANSACTION_DATE, _oPR.getReceiptDate(), Criteria.GREATER_EQUAL);
    		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.TRANSACTION_DATE);
    		oCrit.addAscendingOrderByColumn(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID);			
    		oCrit.add(InventoryTransactionPeer.TRANSACTION_TYPE, i_INV_TRANS_TRANSFER_OUT);
    		oCrit.or(InventoryTransactionPeer.TRANSACTION_TYPE, i_INV_TRANS_TRANSFER_IN);
    		v = InventoryTransactionPeer.doSelect(oCrit, _oConn);
    	}
    	return v;
    }
    
    /**
     * 
     * @param _sPRID
     * @param _oConn
     * @return
     */
    public static boolean updatePRCost(String _sPRID, Connection _oConn)
    {
    	try
    	{
    		PurchaseReceipt oPR = PurchaseReceiptTool.getHeaderByID(_sPRID, _oConn);   
			if(oPR != null && PreferenceTool.getPiUpdatePrCost() && 
			   !PeriodTool.isClosed(oPR.getReceiptDate(), _oConn))
			{
				List v = getNextTransferAfterPR(oPR, _oConn);
				
				log.info("InvTransTool - PR:" + oPR.getReceiptNo() + " sysconfig updatePrCost: "+ PreferenceTool.getPiUpdatePrCost() + 
						 " IsPeriodClosed:  " + PeriodTool.isClosed(oPR.getReceiptDate(), _oConn) + " NextTRFAfterPR: " + v.size());
				if(log.isDebugEnabled()) log.debug("List of next Trf: " + v);
				
				if(v.size() <= 0)
				{
					
					return true;
				}
			}    	
    	}
    	catch(Exception e)
    	{
    		log.error(e);
    		e.printStackTrace();    		    	
    	}
    	return false;    	
    }
}
