package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.BatchTransaction;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.IssueReceiptDetailPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.InventoryJournalTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for Issue Receipt & Issue Receipt Detail OM
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: IssueReceiptTool.java,v 1.23 2009/05/04 02:04:27 albert Exp $ <br>
 *
 * <pre>
 * $Log: IssueReceiptTool.java,v $
 * 
 * 2017-08-20
 * - change method find data add new parameters sUserName & sConfirmBy
 * 
 * 2015-12-10
 * - change method saveData, use new InventoryCostingTool class for creating inventory transaction journal 
 * - change method cancelTrans, use new InventoryCostingTool class for deleting inventory transaction journal
 * </pre><br>
 */
public class IssueReceiptTool extends BaseTool 
{
	private static Log log = LogFactory.getLog ( IssueReceiptTool.class );

	public static final String s_ITEM = new StringBuilder(IssueReceiptPeer.ISSUE_RECEIPT_ID).append(",")
	.append(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID).append(",")
	.append(IssueReceiptDetailPeer.ITEM_ID).toString();

	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), IssueReceiptPeer.TRANSACTION_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), IssueReceiptPeer.DESCRIPTION);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), IssueReceiptPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), IssueReceiptPeer.CONFIRM_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), IssueReceiptPeer.LOCATION_ID);             

		addInv(m_FIND_PEER, s_ITEM);
	}   
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	
	//static methods
	public static List getAllIssueReceipt()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return IssueReceiptPeer.doSelect(oCrit);
	}
	
	public static IssueReceipt getHeaderByID(String _sID)
    	throws Exception
    {
		return getHeaderByID(_sID, null);	
	}

	public static IssueReceipt getHeaderByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(IssueReceiptPeer.ISSUE_RECEIPT_ID, _sID);
	    List vData = IssueReceiptPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) {
			return (IssueReceipt) vData.get(0);
		}
		return null;	
	}
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
        return getDetailsByID(_sID, null);
	}	

	public static List getDetailsByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, _sID);
	    oCrit.addAscendingOrderByColumn(IssueReceiptDetailPeer.INDEX_NO);
	    return IssueReceiptDetailPeer.doSelect(oCrit, _oConn);
	}	
	
	public static IssueReceiptDetail getDetailByDetailID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_DETAIL_ID, _sID);
	    List vData = IssueReceiptDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0)
	    {
	    	return (IssueReceiptDetail) vData.get(0);
	    }
	    return null;
	}	
	
	private static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, _sID);
		IssueReceiptDetailPeer.doDelete (oCrit, _oConn);
	}	

	private static void deleteDetailByDetID (String _sDetID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_DETAIL_ID, _sDetID);
		IssueReceiptDetailPeer.doDelete (oCrit, _oConn);
	}	
	
	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_PENDING) return LocaleTool.getString("pending");
		if (_iStatusID == i_CANCELLED) return LocaleTool.getString("cancelled");
		return LocaleTool.getString("processed");
	}	
	
	public static double countQty (List _vDetails, String _sItemID)
	{
		return countQtyPerItem (_vDetails, _sItemID, "qtyChanges");
	}	

	/**
	 * 
	 * @param data
	 * @param _vTD
	 * @throws Exception
	 */
	public static void updateDetail( RunData data, List _vTD ) 
		throws Exception
	{		
		for (int i = 0; i < _vTD.size(); i++)
		{
			int iNo = i + 1;
			IssueReceiptDetail oTD = (IssueReceiptDetail) _vTD.get(i);
			String sQty = data.getParameters().getString("QtyChanges" + iNo);
			String sCost = data.getParameters().getString("Cost" + iNo);
			if (StringUtil.isNotEmpty(sQty))
			{
				oTD.setQtyChanges(new BigDecimal(sQty));				
			}
			if (StringUtil.isNotEmpty(sCost) && !StringUtil.isEqual(sCost, "0"))
			{
				oTD.setCost(new BigDecimal(sCost));
			}			
			_vTD.set(i, oTD);
		}
	}		
	
	/**
	 * 
	 * @param data
	 * @param _oTR
	 * @throws Exception
	 */
    public static void setHeader( RunData data, IssueReceipt _oTR ) 
		throws Exception
	{
		data.getParameters().setProperties (_oTR);
		if (StringUtil.isNotEmpty(_oTR.getAdjustmentTypeId()))
		{
			AdjustmentType oAdj = _oTR.getAdjType();
			if(oAdj != null)
			{
				if(StringUtil.isEmpty(_oTR.getAccountId())) _oTR.setAccountId(oAdj.getDefaultAccountId());
				if(_oTR.getTransactionType() <= 0) _oTR.setTransactionType(oAdj.getDefaultTransType());				
			}
		}
		_oTR.setDescription(data.getParameters().getString("TransDescription",""));
		_oTR.setTransactionDate(CustomParser.parseDate(data.getParameters().getString("TransactionDate")));
		_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), _oTR.getTransactionType()));
		if (StringUtil.isEmpty(_oTR.getLocationName()))
		{
			_oTR.setLocationName(LocationTool.getLocationNameByID(_oTR.getLocationId()));
		}			
		
	}		

	private static String generateTransactionNo (IssueReceipt _oTR, Connection _oConn) 
		throws Exception
	{
		String sTransNo = "";
		String sLocCode = LocationTool.getLocationCodeByID(_oTR.getLocationId(), _oConn);
		if (TransactionAttributes.b_INVENTORY_SEPARATE_COST_ADJ && _oTR.getCostAdjustment())
		{			
			sTransNo = LastNumberTool.get(s_CA_FORMAT, LastNumberTool.i_COST_ADJUSTMENT, sLocCode, _oConn);			
			return sTransNo;			
		}
		sTransNo = LastNumberTool.get(s_IR_FORMAT, LastNumberTool.i_ISSUE_RECEIPT, sLocCode, _oConn);	
		return sTransNo;
	}
	    
    /**
     * 
     * @param _oTR
     * @param _vTD
     * @param _oConn
     * @throws Exception
     */
	public static void saveData (IssueReceipt _oTR, List _vTD, Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		boolean bNew = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{
			//check for back date
			if(_oTR.getTransactionDate() != null)
			{
				String sDate = CustomFormatter.formatDateTime(_oTR.getTransactionDate());
				if (!sDate.contains("00:00") && !sDate.contains("23:59"))
				{
					_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), _oTR.getTransactionType()));
				}				
			}
			else
			{
				_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), _oTR.getTransactionType()));
			}
			
			//validate date
			validateDate(_oTR.getTransactionDate(), _oConn);

	        if(StringUtil.isEmpty(_oTR.getIssueReceiptId())) bNew = true;
			
			//process save sales Data
			processSaveIssueReceiptData (_oTR, _vTD, _oConn);
						
			//if transaction has been saved and invoice number has been generated
			if (_oTR.getStatus() == i_PROCESSED && StringUtil.isNotEmpty(_oTR.getTransactionNo())) 
			{
				//create inventory trans and update inventory location
				//InventoryTransactionTool.createFromIssueReceipt(_oTR, _vTD, _oConn);
				InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
				oCT.createFromIR(_oTR, _vTD);
				
				if (b_USE_GL)
				{
					//create inventory journal
					InventoryJournalTool oJournal = new InventoryJournalTool (_oTR, _oConn);
					oJournal.createIssueReceiptJournal(_oTR, _vTD);
				}
				//save
				_oTR.save (_oConn);
				
				//update Request
				if (StringUtil.isNotEmpty(_oTR.getRequestId()))
				{
					PurchaseRequestTool.updateRQ(_oTR,_vTD,_oTR.getRequestId(),false,_oConn);
				}
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			//restore object state to unsaved
			_oTR.setTransactionNo("");
			_oTR.setStatus(i_PENDING);
			
			if (bNew)
			{
				_oTR.setNew(true);
				_oTR.setIssueReceiptId("");
			}
			
			if (bStartTrans) 
			{
				rollback (_oConn);
			}				
			log.error (_oEx.getMessage(), _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveIssueReceiptData (IssueReceipt _oTR, List _vTD, Connection _oConn) 
		throws Exception
	{
		//check whether transaction is paid and invoice no is not generated yet
		//invoice no must be checked for synchronization process
		try 
		{
			if (StringUtil.isEmpty(_oTR.getIssueReceiptId())) 
			{
				_oTR.setIssueReceiptId (IDGenerator.generateSysID());
				_oTR.setNew (true);
			}
			
			validateID(getHeaderByID(_oTR.getIssueReceiptId(), _oConn), "transactionNo");
			
			if (_oTR.getTransactionNo() == null) _oTR.setTransactionNo ("");
			if ( _oTR.getStatus () == i_PROCESSED && _oTR.getTransactionNo().equals("")) 
			{
				//generate invoice no 
				_oTR.setTransactionNo (generateTransactionNo(_oTR, _oConn));
				validateNo(IssueReceiptPeer.TRANSACTION_NO,_oTR.getTransactionNo(),IssueReceiptPeer.class, _oConn);
			}
			
			//delete all existing detail from pending transaction			
			deleteDetailsByID (_oTR.getIssueReceiptId(), _oConn);

			_oTR.save (_oConn);
			
			//delete existing tmp batch
			BatchTransactionTool.clearTmpBatch(_oTR.getIssueReceiptId(), i_INV_TRANS_RECEIPT_UNPLANNED, _oConn);

			renumber(_vTD);
            Iterator oIter = _vTD.iterator();
            while (oIter.hasNext())
			{
				IssueReceiptDetail oTD = (IssueReceiptDetail) oIter.next();

                oTD.setIssueReceiptDetailId (IDGenerator.generateSysID());
                oTD.setIssueReceiptId (_oTR.getIssueReceiptId());
                oTD.setModified (true);
				oTD.setNew (true);

                double dQty = oTD.getQtyChanges().doubleValue();
				double dQtyBase = dQty * UnitTool.getBaseValue(oTD.getItemId(),oTD.getUnitId(),_oConn);
				oTD.setQtyBase (new BigDecimal(dQtyBase));

				//if not cost adjustment then reset cost to current cost, 
				//prevent recalculate and causing double journal
				if(!_oTR.getCostAdjustment())  
				{
					BigDecimal dCost = InventoryLocationTool.getItemCost(oTD.getItemId(), 
								_oTR.getLocationId(), _oTR.getTransactionDate(), _oConn);
					oTD.setCost(dCost);
				}
				
				oTD.save (_oConn);				
				
				if(_oTR.getStatus() == i_PENDING && PreferenceTool.useBatchNo() && oTD.getBatchTransaction() != null)
				{
		   			BatchTransactionTool.saveTmpBatch(oTD.getItemId(),
		   											  oTD.getIssueReceiptId(), 
		   											  oTD.getIssueReceiptDetailId(), 
		   											  i_INV_TRANS_RECEIPT_UNPLANNED, 
		   											  oTD.getBatchTransaction(), _oConn);
				}
				
				if(_oTR.getStatus() == i_PROCESSED)
				{
					//update item last adjustment
					Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
					if (oItem != null)
                    {
                        oItem.setLastAdjustment(new Date());
    					oItem.save(_oConn);
    					ItemManager.getInstance().refreshCache(oItem);
                    }
                    else
                    {
                        throw new Exception ("Item with ID " + oTD.getItemId() + " (" + oTD.getItemCode() + ") Not Found ");
                    }

					if (_oTR.getInternalTransfer() && oTD.getQty().doubleValue() == 0) //REMOVE ZERO QTY
                    {
					    IssueReceiptDetailPeer.doDelete(
					            new Criteria().add(IssueReceiptDetailPeer.ISSUE_RECEIPT_DETAIL_ID, 
					                    oTD.getIssueReceiptDetailId()), _oConn);
                        oIter.remove();
                    }
                    
                    //validate batch
					if (oItem != null && dQtyBase > 0  && 
						_oTR.getTransactionType() == i_INV_TRANS_RECEIPT_UNPLANNED)
					{
						BatchTransaction oBT = oTD.getBatchTransaction();
						BatchTransactionTool.validateBatch(oItem, oBT);
					}
					ItemSerialTool.validateSerial(oItem, dQtyBase, oTD.getSerialTrans());
				}
			}
		}
		catch (Exception _oEx)
		{
			String sError = "Process Issue/Receipt Failed : " + _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}

	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _sCancelBy
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelTrans (IssueReceipt _oTR, 
									List _vTD, 
									String _sCancelBy,
									Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		
		int iOldStatus = _oTR.getStatus();
		
		try 
		{		
			int iType = i_INV_TRANS_RECEIPT_UNPLANNED;
			int iGLType = GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED;
			if (_oTR.getTransactionType() == 2) //issue
			{
				iType = i_INV_TRANS_ISSUE_UNPLANNED; 
				iGLType = GlAttributes.i_GL_TRANS_ISSUE_UNPLANNED; 
			}
			//validate date
			validateDate(_oTR.getTransactionDate(), _oConn);

			//delete inv trans 
			InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
			oCT.delete(_oTR, _oTR.getIssueReceiptId(), iType);
			
			//delete gl
			if (b_USE_GL)
			{				
				//update GL journal
				InventoryJournalTool.deleteJournal(iGLType, _oTR.getIssueReceiptId(), _oConn);                
			}			
			_oTR.setDescription(cancelledBy(_oTR.getDescription(), _sCancelBy));			
			_oTR.setStatus(i_CANCELLED);
			_oTR.save (_oConn);
			
			//update Request
			if (StringUtil.isNotEmpty(_oTR.getRequestId()))
			{
				PurchaseRequestTool.updateRQ(_oTR,_vTD,_oTR.getRequestId(),true,_oConn);
			}
			
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			_oTR.setStatus(iOldStatus);
			if (bStartTrans) 
			{
				rollback (_oConn);
			}			
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}	
	
	public static void cancelPartial(String _sIRID, String _sCancelBy, int _iLimit)
		throws Exception
	{
		IssueReceipt oIR = getHeaderByID(_sIRID);		
		if(oIR != null)
		{
			List vTD = getDetailsByID(_sIRID);
			if(vTD.size() > 0)
			{
				if(vTD.size() < _iLimit)
				{
					_iLimit = vTD.size();
				}
				List vDTD = vTD.subList(0, _iLimit);
				String sKey = vDTD.size() + " TransDet, delete Partial TD FROM 0 to " + _iLimit + " of " + vTD.size();
				log.info(sKey);
				
				StopWatch oSW = new StopWatch();
				oSW.start(sKey);
				delPartial(oIR, vDTD);
				oSW.stop(sKey);
				log.info(oSW.result(sKey));
				
				cancelPartial(_sIRID, _sCancelBy, _iLimit);
			}
			else
			{
				log.info("All Details already deleted");
				oIR.setDescription(cancelledBy(oIR.getDescription(), _sCancelBy));			
				oIR.setStatus(i_CANCELLED);
				oIR.save();				
			}
		}
	}
	
	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _sCancelBy
	 * @param _oConn
	 * @throws Exception
	 */
	private static void delPartial (IssueReceipt _oTR, List _vTD) 
		throws Exception 
	{	
		Connection _oConn = beginTrans();		
		int iOldStatus = _oTR.getStatus();
		
		try 
		{		
			int iType = i_INV_TRANS_RECEIPT_UNPLANNED;
			int iGLType = GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED;
			if (_oTR.getTransactionType() == 2) //issue
			{
				iType = i_INV_TRANS_ISSUE_UNPLANNED; 
				iGLType = GlAttributes.i_GL_TRANS_ISSUE_UNPLANNED; 
			}
			//validate date
			validateDate(_oTR.getTransactionDate(), _oConn);
			
			Iterator iter = _vTD.iterator();
			while(iter.hasNext())
			{
				IssueReceiptDetail oTD = (IssueReceiptDetail)iter.next();
				InventoryTransaction oIT = InventoryTransactionTool.getByTransDetailID(
					oTD.getIssueReceiptDetailId(), _oTR.getIssueReceiptId(), _oTR.getLocationId(), iType, _oConn);
				InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
				oCT.setTrans(_oTR);
				oCT.delete(oIT);
				
				deleteDetailByDetID(oTD.getIssueReceiptDetailId(), _oConn);
			}			
			
			List vTD = getDetailsByID(_oTR.getIssueReceiptId(), _oConn); //left over TD
			//delete gl
			if (b_USE_GL)
			{				
				//delete GL journal
				InventoryJournalTool.deleteJournal(iGLType, _oTR.getIssueReceiptId(), _oConn);   

				if(vTD.size() > 0) //left over detail
				{
					//re-create GL Journal
					InventoryJournalTool oJournal = new InventoryJournalTool (_oTR, _oConn);
					oJournal.createIssueReceiptJournal(_oTR, vTD);
				}
			}						
			commit (_oConn);
		}
		catch (Exception _oEx) 
		{
			_oTR.setStatus(iOldStatus);
			rollback (_oConn);
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	/**
	 * 
	 * @param _sTransNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _iTransType
	 * @param _iLimit
	 * @return query result in LargeSelect
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond,
										String _sKeywords,
										Date _dStart,
										Date _dEnd, 
										String _sLocationID, 
										String _sAdjTypeID,
										int _iTransType,
										int _iStatus,
										int _iLimit,
										boolean _bIsTransfer) 
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			IssueReceiptPeer.TRANSACTION_DATE, _dStart, _dEnd, null
		);

		if (_iTransType > 0) 
		{
			oCrit.add(IssueReceiptPeer.TRANSACTION_TYPE, _iTransType);	
		}
		if (_iStatus > 0) 
		{
			oCrit.add(IssueReceiptPeer.STATUS, _iStatus);	
		}		
		if(StringUtil.isNotEmpty(_sLocationID))
		{
		    oCrit.add(IssueReceiptPeer.LOCATION_ID, _sLocationID);
		}
		if(StringUtil.isNotEmpty(_sAdjTypeID))
		{
		    oCrit.add(IssueReceiptPeer.ADJUSTMENT_TYPE_ID, _sAdjTypeID);
		}
		if(_bIsTransfer)
		{
		    oCrit.add(IssueReceiptPeer.INTERNAL_TRANSFER, _bIsTransfer);
		}
		
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.IssueReceiptPeer");
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// REPORT METHODS
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public static List findData (Date _dStart, 
                    	         Date _dEnd, 
                    	         String _sLocationID, 
                    	         String _sAdjTypeID,
                    	         int _iStatus)
	    throws Exception
	{
	    return findData(-1, "", _dStart,_dEnd,_sLocationID,_sAdjTypeID,"","","",_iStatus,false);
	}
    
	public static List findData (int _iCond,
								 String _sKey,
								 Date _dStart, 
								 Date _dEnd, 
								 String _sLocationID, 
								 String _sAdjTypeID,
                                 String _sEntityID,
                                 String _sUserName,
                                 String _sConfirmBy,
								 int _iStatus,
                                 boolean _bInTransfer)
    	throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKey, 
				IssueReceiptPeer.TRANSACTION_DATE, _dStart, _dEnd, null);

		if(StringUtil.isNotEmpty(_sLocationID))
        {
           oCrit.add(IssueReceiptPeer.LOCATION_ID, _sLocationID);
        }
        if(StringUtil.isNotEmpty(_sAdjTypeID))
        {
           oCrit.add(IssueReceiptPeer.ADJUSTMENT_TYPE_ID, _sAdjTypeID);
        }
        if(StringUtil.isNotEmpty(_sUserName))
        {
           oCrit.add(IssueReceiptPeer.USER_NAME, (Object)_sUserName, Criteria.ILIKE);
        }
        if(StringUtil.isNotEmpty(_sConfirmBy))
        {
           oCrit.add(IssueReceiptPeer.CONFIRM_BY, (Object)_sConfirmBy, Criteria.ILIKE);
        }        
        if (_iStatus > 0)
        {
        	oCrit.add(IssueReceiptPeer.STATUS, _iStatus);
        }
        else
        {
            oCrit.add(IssueReceiptPeer.STATUS, i_PROCESSED);
        }
        if (_bInTransfer)
        {
            oCrit.add(IssueReceiptPeer.INTERNAL_TRANSFER, _bInTransfer);            
            if (StringUtil.isNotEmpty(_sEntityID))
            {
                oCrit.add(IssueReceiptPeer.CROSS_ENTITY_ID, _sEntityID);
            }
        }
		return IssueReceiptPeer.doSelect(oCrit);
	}

	public static List getTransDetails (List _vTrans,List _vKategoriId)
    	throws Exception
    {
		List vTransID = new ArrayList (_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++)
		{
			IssueReceipt oTrans = (IssueReceipt) _vTrans.get(i);
			vTransID.add (oTrans.getIssueReceiptId());
		}
		Criteria oCrit = new Criteria();
		if(vTransID!=null && vTransID.size()>0)
    	{
    	    oCrit.addIn (IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, vTransID);
    	}
		if(_vKategoriId!= null && _vKategoriId.size()>0)
    	{
    	    oCrit.addJoin (IssueReceiptDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
    	    oCrit.addIn (ItemPeer.KATEGORI_ID, _vKategoriId);
    	}
		return IssueReceiptDetailPeer.doSelect(oCrit);
	}	
	
	public static double[] countTotalQtyCost (String _sIRID)
		throws Exception
	{
        double[] dQtyCost = new double[3];
		double dCost = 0;
		double dQty = 0;
        List vDetail = getDetailsByID(_sIRID);
		for (int i = 0; i < vDetail.size(); i++)
		{
			IssueReceiptDetail oIRD = (IssueReceiptDetail) vDetail.get(i);
			dCost += (oIRD.getCost().doubleValue() * oIRD.getQtyChanges().doubleValue());
            dQty += oIRD.getQtyChanges().doubleValue();
		}
        dQtyCost[0] = dQty;
        dQtyCost[1] = dCost;
        dQtyCost[2] = vDetail.size();        
		return dQtyCost;		
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// END REPORT METHODS
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
	
	/**
	 * get item transfer created in store
	 * 
	 * @return item transfer created in store since last store 2 ho
	 * @throws Exception
	 */
	public static List getStoreIssueReceipt ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) {
        	oCrit.add(IssueReceiptPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
		}
		oCrit.add(IssueReceiptPeer.STATUS, i_PROCESSED);
		return IssueReceiptPeer.doSelect(oCrit);
	}
	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(IssueReceiptPeer.class, IssueReceiptPeer.ISSUE_RECEIPT_ID, _sID, _bIsNext);
	}	  	
	//end next prev
}