package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.ItemTransferDetailPeer;
import com.ssti.enterprise.pos.om.ItemTransferPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.InventoryJournalTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/inventory/ItemTransferTool.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ItemTransferTool.java,v 1.24 2009/05/04 02:04:27 albert Exp $
 *
 * $Log: ItemTransferTool.java,v $
 * 
 * 2018-02-01
 * - change saveStoreTransferIn to prevent ERROR when loading IT data.
 * 
 * 2017-05-29
 * - add method getOutstandingTI 
 *  
 * 2016-12-27
 * - add i_TO = 1 transfer out variable, i_TI = 2 transfer in variable
 * - change method saveData, now there is configuration in PreferenceTool.getProcessTransferAtReceive
 *   if true then ItemTransfer inventory movement & journal will be done twice:
 *   1. Transfer OUT create Inventory Out and Inventory GL Journal Out at Confirm Trans (Status PROCESSED)
 *   2. Transfer IN create Inventory In and Inventory GL Journal In at Confirm Receive (Status RECEIVED)
 * - change method receiveTrans, if ProcessTransferAtReceive then create Transfer IN inventory movement & GL journal
 *  
 * 2015-12-10
 * - change method saveData, use new InventoryCostingTool class for creating inventory transaction journal
 * - change method cancelTrans, use new InventoryCostingTool class for deleting inventory transaction journal
 * 
 */
public class ItemTransferTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( ItemTransferTool.class );

	public static final int i_TO = 1;
	public static final int i_TI = 2;
	
	public static final String s_ITEM = new StringBuilder(ItemTransferPeer.ITEM_TRANSFER_ID).append(",")
	.append(ItemTransferDetailPeer.ITEM_TRANSFER_ID).append(",")
	.append(ItemTransferDetailPeer.ITEM_ID).toString();

	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), ItemTransferPeer.TRANSACTION_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), ItemTransferPeer.DESCRIPTION);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), ItemTransferPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), ItemTransferPeer.CONFIRM_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), ItemTransferPeer.TO_LOCATION_ID);             
		
		addInv(m_FIND_PEER, s_ITEM);
	}   
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	
	//static methods	
	public static List getAllItemTransfer()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return ItemTransferPeer.doSelect(oCrit);
	}
	
	public static ItemTransfer getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID(_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static ItemTransfer getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemTransferPeer.ITEM_TRANSFER_ID, _sID);
        List vData = ItemTransferPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (ItemTransfer) vData.get(0);
		}
		return null;	
	}
    
    public static ItemTransfer getByNo(String _sNo, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(ItemTransferPeer.TRANSACTION_NO, _sNo);
        List vData = ItemTransferPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
            return (ItemTransfer) vData.get(0);
        }
        return null;    
    }

	public static List getDetailsByID(String _sID)
		throws Exception
	{
		return getDetailsByID(_sID, "");
	}

	public static List getDetailsByID(String _sID, String _sItemID)
    	throws Exception
    {
        return getDetailsByID(_sID,_sItemID,null);
	}

    public static List getDetailsByID(String _sID, String _sItemID, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, _sID);
        if(StringUtil.isNotEmpty(_sItemID))
        {
            oCrit.add(ItemTransferDetailPeer.ITEM_ID, _sItemID);
        }
        oCrit.addAscendingOrderByColumn(ItemTransferDetailPeer.INDEX_NO);
        return ItemTransferDetailPeer.doSelect(oCrit, _oConn);
    }
    
	private static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, _sID);
		ItemTransferDetailPeer.doDelete (oCrit, _oConn);
	}	
	
	public static ItemTransferDetail getDetailByDetailID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(ItemTransferDetailPeer.ITEM_TRANSFER_DETAIL_ID, _sID);
	    List vData = ItemTransferDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0)
	    {
	    	return (ItemTransferDetail) vData.get(0);
	    }
	    return null;
	}	
		
	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_PENDING) return LocaleTool.getString("pending");
		if (_iStatusID == i_CANCELLED) return LocaleTool.getString("cancelled");
        if (_iStatusID == i_RECEIVED) return LocaleTool.getString("received");
        return LocaleTool.getString("processed");
	}		
	
	/**
	 * 
	 * @param data
	 * @param _vTD
	 * @throws Exception
	 */
	public static void updateDetail( RunData data, List _vTD ) 
		throws Exception
	{		
		for (int i = 0; i < _vTD.size(); i++)
		{
			int iNo = i + 1;
			ItemTransferDetail oTD = (ItemTransferDetail) _vTD.get(i);
			String sQty = data.getParameters().getString("QtyChanges" + iNo);
			if (StringUtil.isNotEmpty(sQty))
			{
				oTD.setQtyChanges(new BigDecimal(sQty));
				_vTD.set(i, oTD);
			}
		}
	}		
	
	/**
	 * 
	 * @param data
	 * @param _oTR
	 * @throws Exception
	 */
    public static void setHeader( RunData data, ItemTransfer _oTR ) 
		throws Exception
	{
		data.getParameters().setProperties (_oTR);
		_oTR.setDescription(data.getParameters().getString("TransDescription",""));
		_oTR.setTransactionDate(CustomParser.parseDate(data.getParameters().getString("TransactionDate")));
		_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), i_INV_TRF));
		
		_oTR.setFromLocationName(LocationTool.getLocationNameByID(_oTR.getFromLocationId()));
		_oTR.setToLocationName(LocationTool.getLocationNameByID(_oTR.getToLocationId()));
	}		
	
    /**
     * 
     * @param _oTR
     * @param _vTD
     * @param _oConn
     * @throws Exception
     */
	public static void saveData (ItemTransfer _oTR, List _vTD, Connection _oConn) 
		throws Exception 
	{	
		boolean bNew = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans ();
			bStartTrans = true;
		}
		validate(_oConn);
		try 
		{
			if(StringUtil.isEmpty(_oTR.getItemTransferId())) bNew = true;
			
			//check for back date
			_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), i_INV_TRF));
			
			//validate date
			validateDate(_oTR.getTransactionDate(), _oConn);
			
			//process save sales Data
			processSaveTransferData(_oTR, _vTD, _oConn);
			
			if(_oTR.getStatus() == i_PROCESSED && StringUtil.isNotEmpty(_oTR.getTransactionNo()))
			{
				//-1 transfer directly move stock from FR location to TO location
				//i_TO create out trans only, i_TI create in trans only
				int iTransType = -1; 
				if(PreferenceTool.getProcessTransferAtReceive())
				{
					iTransType = i_TO;
				}
				//create inventory transaction and update inventory location
				InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
				oCT.createFromIT(_oTR, _vTD, iTransType);
				
				//create GL transaction
				if (b_USE_GL && b_TRANSFER_JOURNAL && PreferenceTool.getLocationFunction() != i_STORE)
				{
					InventoryJournalTool oJournal = new InventoryJournalTool(_oTR, iTransType, _oConn);
					oJournal.createItemTransferJournal(_oTR, _vTD);
				}

				//update Request
				if (StringUtil.isNotEmpty(_oTR.getRequestId()))
				{
					PurchaseRequestTool.updateRQ(_oTR,_vTD,_oTR.getRequestId(),false,_oConn);
				}
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans) 
			{
				rollback ( _oConn );
			}						
			
			if (bNew)
			{
				_oTR.setNew(true);
				_oTR.setItemTransferId("");
			}
			
			//restore object state to unsaved
			_oTR.setTransactionNo("");
			_oTR.setStatus(i_PENDING);

			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveTransferData (ItemTransfer _oTR, List _vTD, Connection _oConn) 
		throws Exception
	{
		try 
		{
			if (StringUtil.isEmpty(_oTR.getItemTransferId())) 
			{
				_oTR.setItemTransferId (IDGenerator.generateSysID());
				_oTR.setNew (true);
			}
			
			validateID(getHeaderByID(_oTR.getItemTransferId(),_oConn), "transactionNo");
			
			if(_oTR.getTransactionNo() == null) _oTR.setTransactionNo ("");			
			if( _oTR.getStatus () == i_PROCESSED && _oTR.getTransactionNo().equals("")) 
			{
				String sLocCode = LocationTool.getLocationCodeByID(_oTR.getFromLocationId(), _oConn);
				
				//generate trans no 
				_oTR.setTransactionNo(LastNumberTool.get(s_IT_FORMAT, LastNumberTool.i_ITEM_TRANSFER, sLocCode, _oConn));	
				validateNo(ItemTransferPeer.TRANSACTION_NO,_oTR.getTransactionNo(),ItemTransferPeer.class, _oConn);
			}
			
			//delete all existing detail from pending transaction
			if (StringUtil.isNotEmpty(_oTR.getItemTransferId()))
			{
				deleteDetailsByID(_oTR.getItemTransferId(), _oConn);
			}			
			_oTR.save (_oConn);
			
			Date dDate = _oTR.getTransactionDate();			
			String sLocID = _oTR.getFromLocationId();
						
			renumber(_vTD);
			for ( int i = 0; i < _vTD.size(); i++ ) 
			{
				ItemTransferDetail oTD = (ItemTransferDetail) _vTD.get (i);
				oTD.setModified(true);
				oTD.setNew(true);
				oTD.setItemTransferId(_oTR.getItemTransferId());
				
				double dQty = oTD.getQtyChanges().doubleValue();
                double dQtyBase = dQty * UnitTool.getBaseValue(oTD.getItemId(),oTD.getUnitId(),_oConn);
				oTD.setQtyBase (new BigDecimal(dQtyBase));
				
			    //validate batch
				if(_oTR.getStatus () == i_PROCESSED) 
				{
					double dCurrentQty = 0;
					double dCost = 0;
					InventoryLocation oIL = InventoryLocationTool.getDataByItemAndLocationID(oTD.getItemId(), sLocID, _oConn);
 					if(oIL != null) dCurrentQty = oIL.getCurrentQty().doubleValue();  						 					
 					if(oIL != null) dCost = oIL.getItemCost().doubleValue();
 					
					oTD.setCostPerUnit(InventoryLocationTool.getItemCost(oTD.getItemId(), sLocID, dDate, _oConn));
					
					if(dCurrentQty < dQtyBase) //validate Qty
					{
						StringBuilder oMsg = new StringBuilder(oTD.getItemCode());
						oMsg.append(" Qty not enough, TO Qty:")
							.append(CustomFormatter.fmt(dQtyBase)).append(" QtyOH:")
						    .append(CustomFormatter.fmt(dCurrentQty));
						throw new Exception(oMsg.toString());
					}
					
					log.debug("Qty " + dQty + " Cost " + oTD.getCostPerUnit());
					if (oTD.getCostPerUnit().doubleValue() < 0 || dQty < 0)
				    {
				    	Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
				    	throw new Exception ("Invalid Cost / Qty (Minus) for Item " + oItem.getItemCode());
				    }
					
					//validate serial / batch no
					ItemSerialTool.validateSerial(oTD.getItemId(), dQtyBase, oTD.getSerialTrans(), _oConn);	
				}				
				if (StringUtil.isEmpty(oTD.getItemTransferDetailId())) 
				{
					oTD.setItemTransferDetailId (IDGenerator.generateSysID());					
				}
				oTD.save (_oConn);				
			}
		}
		catch (Exception _oEx)
		{
			String sError = "Process Save Failed : " + _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	/**
	 * Confirm Transfer Receive
	 * 
	 * @param _oIT
	 * @param _sUserName
	 * @throws Exception
	 */
    public static void receiveTrans(ItemTransfer _oIT, String _sUserName)
        throws Exception
    {        
		Connection oConn = beginTrans ();
        try
        {
        	if(_oIT != null)
        	{
	        	_oIT = ItemTransferTool.getHeaderByID(_oIT.getItemTransferId()); //get from DB to prevent double POST
	            if(_oIT.getStatus() == i_PROCESSED && StringUtil.isNotEmpty(_oIT.getTransactionNo()))
	            {
	                _oIT.setStatus(i_RECEIVED);
	                _oIT.setReceiveBy(_sUserName);
	                _oIT.setReceiveDate(new Date());
	                _oIT.save(oConn); 
	                
	                if(PreferenceTool.getProcessTransferAtReceive()) //if process transfer should be done at receive
	    			{                	
	                	//TODO: check if receive InvTrans Exists	                	
	                	List _vTD = getDetailsByID(_oIT.getItemTransferId(), "", oConn);
						//create inventory transaction and update inventory location
						InventoryCostingTool oCT = new InventoryCostingTool(oConn);
						oCT.createFromIT(_oIT, _vTD, i_TI);
						
						//create GL transaction
						if (b_USE_GL && b_TRANSFER_JOURNAL && PreferenceTool.getLocationFunction() != i_STORE)
						{
							InventoryJournalTool oJournal = new InventoryJournalTool(_oIT, i_TI, oConn);
							oJournal.createItemTransferJournal(_oIT, _vTD);
						}
	    			}                
	            }    
        	}
            commit(oConn);
        }
        catch (Exception _oEx)
        {        	
            //restore object state to unsaved
            _oIT.setReceiveBy("");
            _oIT.setReceiveDate(null);
        	_oIT.setStatus(i_PROCESSED);
            
            rollback(oConn);            
            String sError = _oEx.getMessage();
            log.error ( sError, _oEx);
            throw new NestableException (sError, _oEx);
        }
    }
	
	public static void cancelTrans (ItemTransfer _oTR, 
									List _vTD, 
									String _sCancelBy,
									Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{	
			//validate date
			validateDate(_oTR.getTransactionDate(), _oConn);

			String sID = _oTR.getItemTransferId();
			
			//delete inventory transaction and update inventory location
			
			InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
			oCT.delete(_oTR, sID, i_INV_TRANS_TRANSFER_OUT);
			oCT.delete(_oTR, sID, i_INV_TRANS_TRANSFER_IN);
			
			//delete gl
			if (b_USE_GL)
			{				
				//update GL journal
				InventoryJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_ITEM_TRANSFER, sID, _oConn);
			}	
			
			_oTR.setDescription(cancelledBy(_oTR.getDescription(), _sCancelBy));			
			_oTR.setStatus(i_CANCELLED);
			_oTR.save (_oConn);
			
			//update Request
			if (StringUtil.isNotEmpty(_oTR.getRequestId()))
			{
				PurchaseRequestTool.updateRQ(_oTR,_vTD,_oTR.getRequestId(),true,_oConn);
			}
			
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}			
			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}	
	    
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sFromID
	 * @param _sToID
	 * @param _iLimit
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond,
										String _sKeywords, 
								 		Date _dStart,
										Date _dEnd,
										String _sFromID,
										String _sToID,
										int _iStatus,
										int _iLimit) 
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			ItemTransferPeer.TRANSACTION_DATE, _dStart, _dEnd, null);
		
		if(StringUtil.isNotEmpty(_sFromID))
		{
		    oCrit.add(ItemTransferPeer.FROM_LOCATION_ID, _sFromID);
		}
		
		if(StringUtil.isNotEmpty(_sToID))
		{
		    oCrit.add(ItemTransferPeer.TO_LOCATION_ID, _sToID);
		}
		if (_iStatus > 0)
		{
			oCrit.add(ItemTransferPeer.STATUS, _iStatus);
		}
		log.debug(oCrit);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.ItemTransferPeer");
	
    }
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// Report Methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static List findByNo(String _sNo, int _iStatus)
        throws Exception
    {
        List vData = new ArrayList();
        Criteria oCrit = new Criteria();
        if (StringUtil.isNotEmpty(_sNo))
        {
            if (_iStatus <= 0) _iStatus = i_PROCESSED;
            oCrit.add(ItemTransferPeer.TRANSACTION_NO,(Object)_sNo,Criteria.ILIKE);            
            oCrit.add(ItemTransferPeer.STATUS, _iStatus);
            vData = ItemTransferPeer.doSelect(oCrit);
        }
        return vData;
    }
    
	public static List findData (Date _dStart, 
								 Date _dEnd, 
								 String _sLocationID, 
								 String _sToLocationID, 
								 String _sAdjTypeID,
								 int _iStatus)
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        if(_dStart != null)
        {
        	oCrit.add(ItemTransferPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        }
        if(_dEnd != null)
        {
        	oCrit.and(ItemTransferPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        }
        if(StringUtil.isNotEmpty(_sLocationID))
        {
            oCrit.add(ItemTransferPeer.FROM_LOCATION_ID, _sLocationID);
        }
        if(StringUtil.isNotEmpty(_sToLocationID))
        {
            oCrit.add(ItemTransferPeer.TO_LOCATION_ID, _sToLocationID);
        }
        if(StringUtil.isNotEmpty(_sAdjTypeID))
        {
            oCrit.add(ItemTransferPeer.ADJUSTMENT_TYPE_ID, _sAdjTypeID);
        }
        if(_iStatus > 0)
		{
        	if(_iStatus == i_MULTISTAT)
        	{
        		oCrit.add(ItemTransferPeer.STATUS, i_PROCESSED);
        		oCrit.or(ItemTransferPeer.STATUS, i_RECEIVED);
        	}
        	else
        	{
        		oCrit.add(ItemTransferPeer.STATUS, _iStatus);
        	}
//			if (!PreferenceTool.getProcessTransferAtReceive() && _iStatus == i_PROCESSED) //if processed, display also received status
//			{
//				if(_dStart != null && _dEnd != null) //reports, if null from getNotReceived IT
//				{
//					oCrit.or(ItemTransferPeer.STATUS, i_RECEIVED);
//				}
//			}
		}
        log.debug(oCrit);
		return ItemTransferPeer.doSelect(oCrit);
	}
	
	public static List getTransDetails (List _vTrans, List _vKategoriID)
    	throws Exception
    {
		List vTransID = new ArrayList (_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++)
		{
			ItemTransfer oTrans = (ItemTransfer) _vTrans.get(i);
			vTransID.add (oTrans.getItemTransferId());
		}
		Criteria oCrit = new Criteria();
		if(vTransID.size()>0)
    	{
    	    oCrit.addIn (ItemTransferDetailPeer.ITEM_TRANSFER_ID, vTransID);
    	}
		if(_vKategoriID != null && _vKategoriID.size()>0)
    	{
    	    oCrit.addJoin (ItemTransferDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
    	    oCrit.addIn (ItemPeer.KATEGORI_ID, _vKategoriID);
    	}
		return ItemTransferDetailPeer.doSelect(oCrit);
	}

    public static double getTotalQty(String _sID)
        throws Exception
    {
        double dTotal = 0;
        List vDet = getDetailsByID(_sID);
        for (int i = 0; i < vDet.size(); i++)
        {
            ItemTransferDetail oDet = (ItemTransferDetail) vDet.get(i);
            dTotal += oDet.getQtyChanges().doubleValue();
        }
        return dTotal;
    }
    
	public static double getTotalCost(String _sTransNo, String _sFromID)
		throws Exception
	{
		double dCost = 0;
		List vInvTrans = InventoryTransactionTool.findData(
			PreferenceTool.getStartDate(), new Date(), 3, -1, _sTransNo, "", _sFromID);
		for (int i = 0; i < vInvTrans.size(); i++)
		{
			InventoryTransaction oInv = (InventoryTransaction) vInvTrans.get(i);
			double dQty = oInv.getQtyChanges().doubleValue() * -1;
			dCost = dCost + (oInv.getCost().doubleValue() * dQty);
		}
		return dCost;
	}
	
	//-------------------------------------------------------------------------
	// ConfirmedAtToLoc / ProcessedTrfAtReceive methods  
	//-------------------------------------------------------------------------
	
	public static List getProcessedITQty(String _sLocID, String _sItemID, List _vItemID)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		oSB.append("SELECT * FROM it_confirmed_qty WHERE loc_id = '").append(_sLocID).append("'");
		if(StringUtil.isNotEmpty(_sItemID))
		{
			oSB.append(" AND item_id = '").append(_sItemID).append("'");
		}
		else
		{
			if(_vItemID != null && _vItemID.size() > 0)
			{
				List vID = SqlUtil.toListOfKeys(_vItemID, "itemId");
				oSB.append(" AND item_id IN ").append(SqlUtil.convertToINMode(vID));
			}
		}
		return SqlUtil.executeQuery(oSB.toString());		
	}
	
	public static double filterProcessedQty(List _vConfirmedQty, String _sItemID, String _sLocID)
		throws Exception
	{
		double dQty = 0;
		if(_vConfirmedQty != null && _vConfirmedQty.size() > 0)
		{			
			for (int i = 0; i < _vConfirmedQty.size(); i++)
			{
				Record r = (Record) _vConfirmedQty.get(i);
				String sItemID = r.getValue("item_id").asString();
				String sLocID = r.getValue("loc_id").asString();				
				if(StringUtil.isEqual(sItemID, _sItemID) && StringUtil.isEqual(sLocID, _sLocID))
				{
					dQty += r.getValue("tqty").asDouble();
				}
			}
		}
		return dQty;
	}

	public static List getNotReceivedIT(String _sFromID, String _sToLocID)
		throws Exception
	{		
		return findData(null, null, _sFromID, _sToLocID,"", i_PROCESSED);
	}
	
	public static List getOutstandingTIQty(String _sLocID, String _sItemID, List _vItemID)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		oSB.append("SELECT * FROM it_outstanding_ti_qty WHERE loc_id = '").append(_sLocID).append("'");
		if(StringUtil.isNotEmpty(_sItemID))
		{
			oSB.append(" AND item_id = '").append(_sItemID).append("'");
		}
		else
		{
			if(_vItemID != null && _vItemID.size() > 0)
			{
				List vID = SqlUtil.toListOfKeys(_vItemID, "itemId");
				oSB.append(" AND item_id IN ").append(SqlUtil.convertToINMode(vID));
			}
		}
		return SqlUtil.executeQuery(oSB.toString());		
	}	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// end Report Methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 

    /**
     * Special method for Store Transfer IN
     * 
     * @param _oTR
     * @param _vTD
     * @param _oConn
     * @throws Exception
     */
	public static void saveStoreTransferIn (ItemTransfer _oTR, List _vTD, Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans ();
			bStartTrans = true;
		}
		validate(_oConn);
		try 
		{
			//check for back date
			//_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), i_INV_TRF));
			//validate date
			//validateDate(_oTR.getTransactionDate(), _oConn);
			_oTR.setModified(true);
			_oTR.setNew(true);
			
			log.debug("STORE TRF IN: " + _oTR);
			
			//process save sales Data
			//processSaveTransferData (_oTR, _vTD, _oConn);
			
			validateID(getHeaderByID(_oTR.getItemTransferId(),_oConn), "transactionNo");
			validateNo(ItemTransferPeer.TRANSACTION_NO,_oTR.getTransactionNo(),ItemTransferPeer.class, _oConn);
			_oTR.save (_oConn);
								
			renumber(_vTD);
			for ( int i = 0; i < _vTD.size(); i++ ) 
			{
				ItemTransferDetail oTD = (ItemTransferDetail) _vTD.get (i);
				oTD.setModified(true);
				oTD.setNew(true);
				oTD.setItemTransferId(_oTR.getItemTransferId());
				oTD.save (_oConn);				
			}
						
			//if transaction has been saved and invoice number has been generated
			if (_oTR.getStatus() == i_PROCESSED && StringUtil.isNotEmpty(_oTR.getTransactionNo())) 
			{
				//create inventory transaction and update inventory location
				InventoryTransactionTool.createFromStoreTransferIn(_oTR, _vTD, _oConn);
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans) 
			{
				rollback ( _oConn );
			}						
			
			//restore object state to unsaved
			_oTR.setTransactionNo("");
			_oTR.setStatus(i_PENDING);

			String sError = _oEx.getMessage();
			log.error ( sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}
	
	/**
	 * get item transfer created in store
	 * 
	 * @return item transfer created in store since last store 2 ho
	 * @throws Exception
	 */
	public static List getStoreTransfer ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) {
        	oCrit.add(ItemTransferPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
		}
		//only get out transfer
		oCrit.add(ItemTransferPeer.FROM_LOCATION_ID, PreferenceTool.getLocationID());		
		oCrit.add(ItemTransferPeer.STATUS, i_PROCESSED);
		return ItemTransferPeer.doSelect(oCrit);
	}

    /**
     * get item transfer created in store
     * 
     * @return item transfer created in store since last store 2 ho
     * @throws Exception
     */
    public static List getReceivedTransfer()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
        if (dLastSyncDate  != null) {
            oCrit.add(ItemTransferPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
        }
        //only get IN transfer
        oCrit.add(ItemTransferPeer.TO_LOCATION_ID, PreferenceTool.getLocationID());       
        oCrit.add(ItemTransferPeer.STATUS, i_PROCESSED);
        return ItemTransferPeer.doSelect(oCrit);
    }

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(ItemTransferPeer.class, ItemTransferPeer.ITEM_TRANSFER_ID, _sID, _bIsNext);
	}	  	
	//end next prev
}