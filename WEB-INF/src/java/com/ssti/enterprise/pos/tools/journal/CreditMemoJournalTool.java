package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CreditMemoJournalTool.java,v 1.8 2009/05/04 02:04:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: CreditMemoJournalTool.java,v $
 * 
 * 2017-03-20
 * - Fix currency rate problem while closing Memo in other class
 * 
 * </pre><br>
 */
public class CreditMemoJournalTool extends BaseJournalTool
{
	private static final Log log = LogFactory.getLog ( CreditMemoJournalTool.class );	
	
	/**
	 * @param _vDebitMemo
	 * @param _oRP
	 * @param conn
	 */
	public static void createClosingJournal(List _vCM, ArPayment _oRP, Connection _oConn) 
		throws Exception
	{
		List vGLTrans = new ArrayList(_vCM.size());
		Iterator oIter = _vCM.iterator();
		while (oIter.hasNext())
		{
			CreditMemo oCM = (CreditMemo) oIter.next();
			
			//AR Journal
			Account oAR = 
				AccountTool.getAccountReceivable(oCM.getCurrencyId(), oCM.getCustomerId(), _oConn);			
	        
			GlTransaction oARTrans = new GlTransaction();
	        prepareGLTrans (oARTrans, oCM, _oRP, _oConn);	        
	        oARTrans.setAccountId   (oAR.getAccountId());
		    oARTrans.setDebitCredit (GlAttributes.i_CREDIT);

	        //CM Journal -> i.e Advance Sales		    
	        Account oCMAcc = AccountTool.getAccountByID(oCM.getAccountId(), _oConn);
	        GlTransaction oCMTrans = new GlTransaction();
	        prepareGLTrans (oCMTrans, oCM, _oRP, _oConn);
	        oCMTrans.setAccountId   (oCMAcc.getAccountId());
		    oCMTrans.setDebitCredit (GlAttributes.i_DEBIT);
		    
		    if (log.isDebugEnabled())
		    {
		        log.debug("AR TRANS : " + oARTrans);
		        log.debug("CM TRANS : " + oCMTrans);
		    }
		    
	        addJournal (oARTrans, oAR, vGLTrans);	
	        addJournal (oCMTrans, oCMAcc, vGLTrans);		

		}//end while
		GlTransactionTool.saveGLTransaction (vGLTrans, _oConn);
	}		
	
	/**
	 * 
	 * @param _oGLTrans
	 * @param _oCM
	 * @param _oRP
	 * @param _oConn
	 * @throws Exception
	 */
	private static void prepareGLTrans (GlTransaction _oGLTrans, 
										CreditMemo _oCM, 
										ArPayment _oRP, 
										Connection _oConn)
		throws Exception
	{
		Period oPeriod = PeriodTool.getPeriodByDate(_oCM.getClosedDate(), _oConn);
		Currency oCurrency = CurrencyTool.getCurrencyByID(_oCM.getCurrencyId(), _oConn);
		
		//TODO: closing rate / currency rate
		double dRate = _oCM.getCurrencyRate().doubleValue();
		if(!oCurrency.getIsDefault() && _oCM.getClosingRate() != null && _oCM.getClosingRate().doubleValue() != 1)
		{
			//dRate = _oCM.getClosingRate().doubleValue();
		}
		
		double dAmount = _oCM.getAmount().doubleValue();
		double dAmountBase = dAmount * dRate;
		
        _oGLTrans.setTransactionType  (GlAttributes.i_GL_TRANS_AR_PAYMENT);
        _oGLTrans.setTransactionId    (_oRP.getArPaymentId());
        _oGLTrans.setTransactionNo    (_oRP.getArPaymentNo());
        _oGLTrans.setGlTransactionNo  (_oRP.getArPaymentNo());
        _oGLTrans.setTransactionDate  (_oRP.getTransactionDate());
        _oGLTrans.setLocationId		  (_oRP.getLocationId());
        _oGLTrans.setProjectId        ("");
        _oGLTrans.setDepartmentId     ("");
        _oGLTrans.setPeriodId         (oPeriod.getPeriodId());
        _oGLTrans.setCurrencyId       (oCurrency.getCurrencyId());
        _oGLTrans.setCurrencyRate     (new BigDecimal(dRate));

        _oGLTrans.setAmount           (new BigDecimal(dAmount));
        _oGLTrans.setAmountBase       (new BigDecimal(dAmountBase));
        _oGLTrans.setUserName         (_oRP.getUserName());
        _oGLTrans.setCreateDate       (new Date());
        _oGLTrans.setSubLedgerType	  (i_SUB_LEDGER_AR);
        _oGLTrans.setSubLedgerId	  (_oRP.getCustomerId());
        
        StringBuilder oDesc = new StringBuilder();
        oDesc.append("Memo ").append(_oCM.getMemoNo())
        	 .append(" Closing in Payment ").append(_oRP.getTransactionNo())
        	 .append("\n").append(_oRP.getRemark());
        
        _oGLTrans.setDescription(oDesc.toString());           
	}	
}
