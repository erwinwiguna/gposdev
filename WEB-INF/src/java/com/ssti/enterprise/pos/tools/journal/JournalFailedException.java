package com.ssti.enterprise.pos.tools.journal;

import org.apache.commons.lang.exception.NestableException;

public class JournalFailedException extends NestableException
{
	public JournalFailedException (String _sMessage, Throwable cause)
	{
		super(_sMessage, cause);
	}

	public JournalFailedException (Throwable cause)
	{
		super(cause);
	}
}
