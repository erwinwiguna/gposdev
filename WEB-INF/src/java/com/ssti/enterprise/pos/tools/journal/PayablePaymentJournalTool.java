package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ApPaymentDetail;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.GlConfigPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CreditMemoJournalTool.java,v 1.8 2009/05/04 02:04:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: CreditMemoJournalTool.java,v $
 * Revision 1.8  2009/05/04 02:04:46  albert
 * *** empty log message ***
 *
 * Revision 1.7  2008/08/17 02:24:24  albert
 * *** empty log message ***
 *
 * Revision 1.6  2008/06/29 07:15:44  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/03 03:14:35  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/04/17 13:09:25  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PayablePaymentJournalTool extends BaseJournalTool
{
	private static final Log log = LogFactory.getLog ( PayablePaymentJournalTool.class );	
	
	private Currency m_oCurrency = null;
	private Currency m_oBaseCurrency = null;
	
	private ApPayment m_oRP;
	private List m_vRPD;	
	
	private double m_dRate = 1;
	private BigDecimal m_bdRate = bd_ONE;
	private Location m_oLoc = null;
	private boolean m_bConsignment = false;
	private Connection m_oConn = null;
	private Vendor m_oVendor = null;
	
	public PayablePaymentJournalTool(ApPayment _oRP, List _vRPD, Connection _oConn)
		throws Exception
	{
	    validate(_oConn);
	    m_oConn = _oConn;
	    
	    this.m_oRP = _oRP;
	    this.m_vRPD = _vRPD;
	    
		m_oPeriod = PeriodTool.getPeriodByDate(_oRP.getTransactionDate(), m_oConn);
	    m_oCurrency = CurrencyTool.getCurrencyByID(_oRP.getCurrencyId(), m_oConn);
	    m_oBaseCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
	    m_dRate = _oRP.getCurrencyRate().doubleValue();		
	    m_bdRate = new BigDecimal(m_dRate);
	    m_oLoc = LocationTool.getLocationByID(_oRP.getLocationId(), m_oConn);    	
	    m_sTransID = _oRP.getApPaymentId();
	    m_sTransNo = _oRP.getApPaymentNo();
		m_sUserName = _oRP.getUserName();
		m_sLocationID = _oRP.getLocationId();
		
	    StringBuilder oRemark = new StringBuilder (_oRP.getRemark());
		if (StringUtil.isEmpty(_oRP.getRemark()))
		{
			oRemark.append (LocaleTool.getString("payable_payment")).append(" ").append(m_sTransNo);
		}
		m_sRemark = oRemark.toString();
		m_dTransDate = _oRP.getTransactionDate();
		m_dCreate = new Date();
		m_oVendor = VendorTool.getVendorByID(_oRP.getVendorId(), m_oConn);    	
		m_iSubLedger = i_SUB_LEDGER_AP;
    	m_sSubLedgerID = m_oVendor.getVendorId();

	}
	
	/**
	 * @param _vDebitMemo
	 * @param m_oRP
	 * @param conn
	 */
	public void createJournal() 
		throws Exception
	{
		List vGLTrans = new ArrayList(2);
		Iterator oIter = m_vRPD.iterator();
		while (oIter.hasNext())
		{
			ApPaymentDetail oAPD = (ApPaymentDetail) oIter.next();

			double dInvRate = oAPD.getInvoiceRate().doubleValue();
			double dPayment = oAPD.getPaymentAmount().doubleValue();
			double dPaymentBase = dPayment * dInvRate;
			BigDecimal oPayment = new BigDecimal(dPayment);
			BigDecimal oPaymentBase = new BigDecimal(dPaymentBase);
			
			String sCurrID = m_oCurrency.getCurrencyId();
			String sVendorID = m_oVendor.getVendorId();
			Account oAP = AccountTool.getAccountPayable(sCurrID, sVendorID, m_oConn);
			
			GlTransaction oAPTrans = new GlTransaction();
	        oAPTrans.setAccountId      (oAP.getAccountId());
		    oAPTrans.setDebitCredit    (GlAttributes.i_DEBIT);
		    oAPTrans.setAmount		   (oPayment);
		    oAPTrans.setAmountBase	   (oPaymentBase);
		    oAPTrans.setCurrencyId     (m_oCurrency.getCurrencyId());
		    oAPTrans.setCurrencyRate   (new BigDecimal(dInvRate));
		    prepareGLTrans(oAPTrans);
		    
	        //AP TEMP JOURNAL
		    String sAPTemp = GLConfigTool.getGLConfig(m_oConn).getApPaymentTemp();
	        Account oAPTemp = AccountTool.getAccountByID(sAPTemp, m_oConn);
	        validate (oAPTemp, sAPTemp, GlConfigPeer.AP_PAYMENT_TEMP);

	        GlTransaction oAPTempTrans = new GlTransaction();
	        oAPTempTrans.setAccountId      (oAPTemp.getAccountId());
	        oAPTempTrans.setDebitCredit    (GlAttributes.i_CREDIT);
		    oAPTempTrans.setAmount		   (oPaymentBase);
		    oAPTempTrans.setAmountBase	   (oPaymentBase);
		    oAPTempTrans.setCurrencyId     (m_oBaseCurrency.getCurrencyId());
		    oAPTempTrans.setCurrencyRate   (bd_ONE);
		    prepareGLTrans(oAPTempTrans);
		    
	        addJournal (oAPTrans, oAP, vGLTrans);	
	        addJournal (oAPTempTrans, oAPTemp, vGLTrans);		
	        setGLTrans (vGLTrans);	        

		    if (log.isDebugEnabled())
		    {
		        log.debug("AP TRANS : " + oAPTrans);
		        log.debug("AP TEMP TRANS : " + oAPTempTrans);
		    }
		}//end while
		GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
	}		
	
	/**
	 * 
	 * @param _oGLTrans
	 * @param _oCM
	 * @param _oRP
	 * @param m_oConn
	 * @throws Exception
	 */
	private void prepareGLTrans (GlTransaction _oGLTrans)
		throws Exception
	{
		_oGLTrans.setCurrencyRate   (bd_ONE);
		_oGLTrans.setProjectId	    ("");
		_oGLTrans.setDepartmentId   ("");
		_oGLTrans.setSubLedgerType  (i_SUB_LEDGER_AP);
		_oGLTrans.setSubLedgerId    (m_oRP.getEntityId());
		_oGLTrans.setTransactionType(i_GL_TRANS_AP_PAYMENT);
	}	
}
