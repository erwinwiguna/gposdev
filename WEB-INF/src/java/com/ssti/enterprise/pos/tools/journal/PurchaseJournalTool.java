package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.om.GlConfigPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceExp;
import com.ssti.enterprise.pos.om.PurchaseInvoiceExpPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoiceFreight;
import com.ssti.enterprise.pos.om.PurchaseInvoiceFreightPeer;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Automatic Journal methods, used to create automatic GL
 * Transaction for PurchaseReceipt, Purchase Invoice
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseJournalTool.java,v 1.31 2008/10/22 05:51:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseJournalTool.java,v $
 * 
 * 2018-09-08
 * - add var m_bUpdatePRCost, to specify whether PI will update PR Cost based on Inventory Journal
 * - m_bUpdatePRCost value will be set Constructor from PurchaseInvoiceTool
 * - change updatePRJournal, check whether update PR Journal or not from m_bUpdatePRCost
 * 
 * 2016-03-25
 * - change createPIExpTrans set to always journal Expense in BaseCurrency if Account in BaseCurrency
 * </pre><br>
 */
public class PurchaseJournalTool extends BaseJournalTool
{
	private static final Log log = LogFactory.getLog (PurchaseJournalTool.class);

	private Currency m_oCurrency = null;
	private Currency m_oBaseCurrency = null;
	private double m_dRate = 1;
	private BigDecimal m_bdRate = bd_ONE;
	private Vendor m_oVendor = null;
	private boolean m_bUpdatePRCost = true;
		
	public PurchaseJournalTool(PurchaseInvoice _oTR, boolean _bUpdatePRCost, Connection _oConn)
		throws Exception
    {
		validate (_oConn);
		m_oConn = _oConn;
		
		m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getPurchaseInvoiceDate(), m_oConn);
		m_oCurrency = CurrencyTool.getCurrencyByID(_oTR.getCurrencyId(), m_oConn);
		m_oBaseCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
		m_dRate = _oTR.getCurrencyRate().doubleValue();		
		m_bdRate  = new BigDecimal(m_dRate);
		
        m_sTransID = _oTR.getPurchaseInvoiceId();
        m_sTransNo = _oTR.getPurchaseInvoiceNo();
    	m_sUserName = _oTR.getCreateBy();
    	m_sLocationID = _oTR.getLocationId();    	

        StringBuilder oRemark = new StringBuilder (_oTR.getRemark());
    	if (StringUtil.isEmpty(_oTR.getRemark()))
    	{
    		oRemark.append (LocaleTool.getString("purchase_invoice")).append(" ").append(m_sTransNo);
    	}
    	m_sRemark = oRemark.toString();

    	m_dTransDate = _oTR.getPurchaseInvoiceDate();
    	m_dCreate = new Date();	
    	
    	m_oVendor = VendorTool.getVendorByID(_oTR.getVendorId(), m_oConn);
    	m_iSubLedger = i_SUB_LEDGER_AP;
    	m_sSubLedgerID = m_oVendor.getVendorId();
    	m_bUpdatePRCost = _bUpdatePRCost;	
    }

	public PurchaseJournalTool(PurchaseReceipt _oTR, Connection _oConn)
		throws Exception
	{
		validate (_oConn);
		m_oConn = _oConn;

	    m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getReceiptDate(), m_oConn);

	    m_oCurrency = CurrencyTool.getCurrencyByID(_oTR.getCurrencyId(), m_oConn);
		m_oBaseCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
	    m_dRate = _oTR.getCurrencyRate().doubleValue();
	    
        m_sTransID = _oTR.getPurchaseReceiptId();
        m_sTransNo = _oTR.getReceiptNo();
    	m_sUserName = _oTR.getConfirmBy();
    	m_sLocationID = _oTR.getLocationId();    	

    	StringBuilder oRemark = new StringBuilder (_oTR.getRemark());
    	if (StringUtil.isEmpty(_oTR.getRemark()))
    	{
        	oRemark.append (LocaleTool.getString("purchase_receipt")).append(" ").append(m_sTransNo);
    	}    	
    	m_sRemark = oRemark.toString();

    	m_dTransDate = _oTR.getReceiptDate();
    	m_dCreate = new Date();
    	
    	m_oVendor = VendorTool.getVendorByID(_oTR.getVendorId(), m_oConn);
    	m_iSubLedger = i_SUB_LEDGER_AP;
    	m_sSubLedgerID = m_oVendor.getVendorId();
	}
	
	/**
	 * PR journal, unbilled -> credit and inventory -> debit
	 * add cogs, minus inventory
	 * 
	 * @param _oTR Purchase Receipt
	 * @param _vTD Purchase Receipt Detail 
	 * @param m_oConn The Connection object, null if not a join transaction
	 * @throws Exception
	 */
	public void createPRJournal(PurchaseReceipt _oTR, List _vTD) 
    	throws Exception
    {   
		validate(m_oConn);
        try
        {
            List vGLTrans = new ArrayList(_vTD.size());
            for (int i = 0; i < _vTD.size(); i++)
            {
                PurchaseReceiptDetail oTD = (PurchaseReceiptDetail) _vTD.get(i);
                String sItemID = oTD.getItemId();
                Item oItem = ItemTool.getItemByID (sItemID, m_oConn);
                createPRGLTrans (_oTR, oTD, oItem, vGLTrans);
            }
            
            setGLTrans(vGLTrans);
            //log.debug ("vGLTrans : " + vGLTrans );
            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);        
        }
        catch (Exception _oEx)
        {
			log.error(_oEx);
            throw new JournalFailedException ("GL Journal Failed : " + _oEx.getMessage(), _oEx);
        }
	}

	/**
	 * create journal for a purchase receipt detail entry
	 * journal entry (inventory & unbilled) always in base currency 
	 * 
	 * @param _oTR
	 * @param _oTD
	 * @param _oItem
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createPRGLTrans (PurchaseReceipt _oTR, 
	                              PurchaseReceiptDetail _oTD, 
	                              Item _oItem, 
	                              List _vGLTrans) 
    	throws Exception
    {
        double dCost = _oTD.getCostPerUnit().doubleValue(); //cost per unit is in base value
        double dQtyBase = _oTD.getQtyBase().doubleValue();
        double dAmount = dCost * dQtyBase / m_dRate;
        double dAmountBase = dCost * dQtyBase ;
        log.debug("dAmount " + dAmount);
        log.debug("dAmountBase " + dAmountBase);
        
        Account oAcc = null;
		if (_oItem.getItemType() == i_INVENTORY_PART)
		{
			//Inventory -> DEBIT
			String sInventory = _oItem.getInventoryAccount(); 
			oAcc = AccountTool.getAccountByID (sInventory, m_oConn);        
			validate (oAcc, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);	
		}			
		else if (_oItem.getItemType() == i_NON_INVENTORY_PART || _oItem.getItemType() == i_ASSET)
		{
			//Expense -> DEBIT
			String sExp = _oItem.getExpenseAccount(); 
			oAcc = AccountTool.getAccountByID (sExp, m_oConn);
			validate (oAcc, _oItem.getItemCode(), ItemPeer.EXPENSE_ACCOUNT);					
		}
		else
		{
			throw new Exception ("Item '" + _oItem.getItemCode() + 
				"' Item Type (Service/Grouping) is not allowed in purchase receipt");
		}    	
		
		if (oAcc != null)
		{
			BigDecimal bdAmount = new BigDecimal(dAmountBase);
			
			GlTransaction oInvTrans       = new GlTransaction();
			oInvTrans.setTransactionType  (i_GL_TRANS_PURCHASE_RECEIPT);
			oInvTrans.setProjectId        (_oTD.getProjectId());
			oInvTrans.setDepartmentId     (_oTD.getDepartmentId());
			oInvTrans.setAccountId        (oAcc.getAccountId());
			oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
			oInvTrans.setCurrencyRate     (bd_ONE);
			oInvTrans.setAmount           (bdAmount);
			oInvTrans.setAmountBase       (bdAmount);
			oInvTrans.setDebitCredit      (i_DEBIT);
			
			//Unbilled -> CREDIT
			String sUnbilled  = _oItem.getUnbilledGoodsAccount();
			Account oUnbilled = AccountTool.getAccountByID (sUnbilled, m_oConn);
			validate (oUnbilled, _oItem.getItemCode(), ItemPeer.UNBILLED_GOODS_ACCOUNT);
			
			GlTransaction oUnbilledTrans       = new GlTransaction();
			oUnbilledTrans.setTransactionType  (i_GL_TRANS_PURCHASE_RECEIPT);
			oUnbilledTrans.setProjectId        (_oTD.getProjectId());
			oUnbilledTrans.setDepartmentId     (_oTD.getDepartmentId());
			oUnbilledTrans.setAccountId        (oUnbilled.getAccountId());
			oUnbilledTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
			oUnbilledTrans.setCurrencyRate     (bd_ONE);
			oUnbilledTrans.setAmount           (bdAmount);
			oUnbilledTrans.setAmountBase       (bdAmount);
			oUnbilledTrans.setDebitCredit      (i_CREDIT);
			
			addJournal (oInvTrans, oAcc, _vGLTrans);	
			addJournal (oUnbilledTrans, oUnbilled, _vGLTrans);	
		}
        //log.debug ("PR Journal Inventory : "  + oInvTrans );
        //log.debug ("PR Journal Unbilled  : "  + oUnbilledTrans );
	}

	/**
	 * Purchase Invoice journal
	 * if from pr journal ap, unbilled & tax
	 * if not ap, inventory, tax
	 * inventory always in base currency 
	 * 
	 * @param _oTR Purchase Invoice
	 * @param _vTD Purchase Invoice Detail
	 * @param m_oConn The Connection object will be null if not a join transaction
	 * @throws Exception 
	 */
	public void createPIJournal(PurchaseInvoice _oTR, List _vTD, List _vFRT, Map _mPR) 
    	throws Exception
    {
		createPIJournal(_oTR, _vTD, _vFRT, new ArrayList(1), _mPR);
    }
	
	
	/**
	 * Purchase Invoice journal
	 * if from pr journal ap, unbilled & tax
	 * if not ap, inventory, tax
	 * inventory always in base currency 
	 * 
	 * @param _oTR Purchase Invoice
	 * @param _vTD Purchase Invoice Detail
	 * @param m_oConn The Connection object will be null if not a join transaction
	 * @throws Exception 
	 */
	public void createPIJournal(PurchaseInvoice _oTR, List _vTD, List _vFRT, List _vPIE, Map _mPR) 
    	throws Exception
    {
		validate (m_oConn);
        try
        {
            List vGLTrans = new ArrayList(_vTD.size());
            for (int i = 0; i < _vTD.size(); i++)
            {
                PurchaseInvoiceDetail oTD = (PurchaseInvoiceDetail) _vTD.get(i);
                String sItemID = oTD.getItemId();
                Item oItem = ItemTool.getItemByID (sItemID, m_oConn);
                createPIGLTrans (_oTR, oTD, _vTD, oItem, _mPR, vGLTrans);
            }                       
            createFreightTrans (_oTR, _vFRT, vGLTrans);           
            createPIExpTrans (_oTR, _vPIE, vGLTrans);
            //create variance because calculation of costPerUnit * QtyBase might be different with total AP            
            
            setGLTrans (vGLTrans);
            //log.debug ("vGLTrans : " + vGLTrans );
            
            if(m_bUpdatePRCost) //only update PR Journal if PI update PR is true in SysConfig
            {
            	updatePRJournal(_mPR);
            }
            
            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);       
        }
        catch (Exception _oEx)
        {            
			log.error(_oEx);
            throw new JournalFailedException ("Purchase Invoice Journal Failed : " + _oEx.getMessage(), _oEx);
        }        
	}

	/**
	 * create journal for single purchase invoice detail entry
	 * 
	 * @param _oTR
	 * @param _oTD
	 * @param _vTD
	 * @param _oItem
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createPIGLTrans (PurchaseInvoice _oTR, 
            					  PurchaseInvoiceDetail _oTD, 
								  List _vTD,
								  Item _oItem, 
								  Map _mPR,
								  List _vGLTrans) 
		throws Exception
	{
		//get sub total cost including total discount, amount will be journaled to AP
        double dCost = _oTD.getCostPerUnit().multiply(_oTD.getQtyBase())
        	.divide(_oTR.getCurrencyRate(),i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP).doubleValue();  //sub total already calculated from with Inventory Costing 
        double dCostBase = m_dRate * dCost;
        
//		//if total discount > 0 (invoice sales discount exist) then calculate the real cost here
//        if( StringUtil.isNotEmpty(_oTR.getTotalDiscountPct()) && !_oTR.getTotalDiscountPct().equals("0"))
//        {        	
//            String sDisc = _oTR.getTotalDiscountPct();
//            //calculate total amount before tax
//            double dSubTotal = PurchaseInvoiceTool.countSubTotal(_vTD).doubleValue();
//            double dDisc = Calculator.calculateDiscount(sDisc, dCost);
//		    // discount is not in percentage
//		    if(!sDisc.contains(Calculator.s_PCT))
//		    {
//		    	double dTotalDisc = (dDisc * (dCost / dSubTotal));
//		    	dCost = dCost - dTotalDisc;
//		    }
//	    	else
//		    {
//		   	    dCost = dCost - dDisc;
//		    }
//        }
//        if (_oTR.getIsInclusiveTax())
//        {
//        	//dCost = dCost / ((_oTD.getTaxAmount().doubleValue() + 100) / 100);
//        }
//        dCostBase = m_dRate * dCost;
	    
        //inventory journal use cost per unit value
        //double dQtyBase = _oTD.getQtyBase().doubleValue();
        //double dPICost = _oTD.getCostPerUnit().doubleValue(); //from 
        //double dTotalPICost = dPICost * dQtyBase;
        BigDecimal bdTotalPICost = _oTD.getCostPerUnit().multiply(_oTD.getQtyBase());
        
        System.out.println("PurchJournalTool: Item " + _oTD.getItemCode() + " dPICost: " + _oTD.getCostPerUnit() + " dTotalPICost: " + bdTotalPICost );
        
		//a direct purchase, do journal to inventory and account payable          
		if ((StringUtil.isEmpty(_oTD.getPurchaseReceiptId()) && StringUtil.isEmpty(_oTD.getPurchaseReceiptDetailId())) || 
			(StringUtil.isEqual(_oTD.getPurchaseOrderId(), _oTD.getPurchaseReceiptId()))) //FROM PO
		{
			Account oAcc = null;
			if (_oItem.getItemType() == i_INVENTORY_PART)
			{
				//Inventory -> DEBIT, 
				String sInventory = _oItem.getInventoryAccount(); 
				oAcc = AccountTool.getAccountByID (sInventory, m_oConn);        
				validate (oAcc, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);	
			}			
			else if (_oItem.getItemType() == i_NON_INVENTORY_PART)
			{
				//Expense -> DEBIT
				String sExp = _oItem.getExpenseAccount(); 
				oAcc = AccountTool.getAccountByID (sExp, m_oConn);
				validate (oAcc, _oItem.getItemCode(), ItemPeer.EXPENSE_ACCOUNT);					
			}
			else
			{
				throw new Exception ("Item '" + _oItem.getItemCode() + 
					"' Item Type (Service/Grouping) is not allowed in purchase invoice");
			}
			
			if (oAcc != null)
			{
				GlTransaction oInvTrans       = new GlTransaction();
				oInvTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
				oInvTrans.setProjectId        (_oTD.getProjectId());
				oInvTrans.setDepartmentId     (_oTD.getDepartmentId());
				oInvTrans.setAccountId        (oAcc.getAccountId());
				oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
				oInvTrans.setCurrencyRate     (bd_ONE);
				oInvTrans.setAmount           (bdTotalPICost);
				oInvTrans.setAmountBase       (bdTotalPICost);
				oInvTrans.setDebitCredit      (i_DEBIT);
				
				addJournal (oInvTrans, oAcc, _vGLTrans);	
				//log.debug ("PI Journal Inventory : "  + oInvTrans );
			}
		}
		else //from purchase receipt
		{			
			PurchaseReceipt oPR = (PurchaseReceipt) _mPR.get(_oTD.getPurchaseReceiptId());
			
			//if PI set to NOT Update PR alwas create the variance journal
			//if PR is in closed period then create variance journal
			System.out.println("PurchJournalTool: Update PR " + oPR.getTransactionNo() + "  UpdateCost:"+ m_bUpdatePRCost);
			if (!m_bUpdatePRCost)
			{
				//double dPRCost = dPICost;
				//double dTotalPRCost = dPRCost * dQtyBase;
				double dDelta = 0;
				
				PurchaseReceiptDetail oPRD = filterByPRDID(_oTD.getPurchaseReceiptDetailId(), oPR.getDetail());
				if (oPRD != null)
				{
					BigDecimal bdPRCost = oPRD.getCostPerUnit();
					BigDecimal bdTotalPRCost = oPRD.getCostPerUnit().multiply(_oTD.getQtyBase()); 
					BigDecimal bdDelta = bdTotalPICost.subtract(bdTotalPRCost);
					
					System.out.println("PurchJournalTool: Item " + _oTD.getItemCode() + " dPRCost: " + bdPRCost + " dTotalPRCost: " + bdTotalPRCost + " dDelta " + dDelta);					
					
					//dTotalPICost = dTotalPRCost;
					bdTotalPICost =  bdTotalPRCost; //new BigDecimal(dTotalPICost);					
					
					if (Calculator.precise(bdDelta, i_INVENTORY_COMMA_SCALE) != 0)
					{			
						//BigDecimal bdDelta = new BigDecimal(dDelta);
						
						//PR / PI Variance -> DEBIT
						String sVariance = GLConfigTool.getGLConfig(m_oConn).getPrPiVariance();
						Account oVariance = AccountTool.getAccountByID (sVariance, m_oConn);
				        validate (oVariance, sVariance, GlConfigPeer.PR_PI_VARIANCE);
	
						GlTransaction oVarianceTrans       = new GlTransaction();
						oVarianceTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
						oVarianceTrans.setProjectId        (_oTD.getProjectId());
						oVarianceTrans.setDepartmentId     (_oTD.getDepartmentId());
						oVarianceTrans.setAccountId        (oVariance.getAccountId());
						oVarianceTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
						oVarianceTrans.setCurrencyRate     (bd_ONE);
						oVarianceTrans.setAmount           (bdDelta);
						oVarianceTrans.setAmountBase       (bdDelta);
						oVarianceTrans.setDebitCredit      (i_DEBIT);
						
						addJournal (oVarianceTrans, oVariance, _vGLTrans);	
					}
				}
			}
			
			//Unbilled Goods -> DEBIT
			String sUnbilled  = _oItem.getUnbilledGoodsAccount();
			Account oUnbilled = AccountTool.getAccountByID (sUnbilled, m_oConn);
	        validate (oUnbilled, _oItem.getItemCode(), ItemPeer.UNBILLED_GOODS_ACCOUNT);

			GlTransaction oUnbilledTrans       = new GlTransaction();
			oUnbilledTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
			oUnbilledTrans.setProjectId        (_oTD.getProjectId());
			oUnbilledTrans.setDepartmentId     (_oTD.getDepartmentId());
			oUnbilledTrans.setAccountId        (oUnbilled.getAccountId());
			oUnbilledTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
			oUnbilledTrans.setCurrencyRate     (bd_ONE);
			oUnbilledTrans.setAmount           (bdTotalPICost);
			oUnbilledTrans.setAmountBase       (bdTotalPICost);
			oUnbilledTrans.setDebitCredit      (i_DEBIT);
			
			//log.debug ("PI Journal Unbilled : "  + oUnbilledTrans );
			addJournal (oUnbilledTrans, oUnbilled, _vGLTrans);		
		}
		
		//Account Payable -> CREDIT, Amount: Total cost w/o freight 
		Account oAP = AccountTool.getAccountPayable(
			m_oCurrency.getCurrencyId(), m_oVendor.getVendorId(), m_oConn);
        validate (oAP, m_oCurrency.getCurrencyCode(), CurrencyPeer.AP_ACCOUNT);

		GlTransaction oAPTrans = new GlTransaction();
		oAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
		oAPTrans.setProjectId        (_oTD.getProjectId());
		oAPTrans.setDepartmentId     (_oTD.getDepartmentId());
		oAPTrans.setAccountId        (oAP.getAccountId());
		oAPTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
		oAPTrans.setCurrencyRate     (m_bdRate);
		oAPTrans.setAmount           (new BigDecimal(dCost));
		oAPTrans.setAmountBase       (new BigDecimal(dCostBase));
		oAPTrans.setDebitCredit      (i_CREDIT);
		
		addJournal (oAPTrans, oAP, _vGLTrans);	        
		
		//Tax
		createTaxTrans (_oTR, _oTD, _vGLTrans);
	}

	private PurchaseReceiptDetail filterByPRDID(String _sPRDID, List _vPRD) 
	{
		if (_vPRD != null)
		{
			for (int i = 0; i < _vPRD.size(); i++)
			{
				PurchaseReceiptDetail oPRD = (PurchaseReceiptDetail) _vPRD.get(i);
				if (StringUtil.isEqual(oPRD.getPurchaseReceiptDetailId(), _sPRDID))
				{
					return oPRD;
				}
			}
		}
		return null;
	}

	/**
	 * create tax GL trans (Purchase TAX & Base Currency AP Account) 
	 * 
	 * @param _oTR
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createTaxTrans (PurchaseInvoice _oTR, 
								 PurchaseInvoiceDetail _oTD, 
								 List _vGLTrans)
		throws Exception
	{
		double dFiscalRate = _oTR.getFiscalRate().doubleValue();
		if (!m_oCurrency.getIsDefault()) 
		{
			if (dFiscalRate == 0 || dFiscalRate == 1)
			{
				dFiscalRate = CurrencyTool.getFiscalRate(m_oCurrency.getCurrencyId(), m_oConn);
			}
		}
		double dTax  = _oTD.getSubTotalTax().doubleValue(); 
        double dTaxBase = dFiscalRate * dTax;
        BigDecimal bdTax = new BigDecimal(dTaxBase);
		
        //TAX VAT IN -> DEBIT
		if (dTax > 0)
		{                   
			//PURCHASE TAX (VAT IN) -> DEBIT
			Tax oTax = TaxTool.getTaxByID (_oTD.getTaxId(), m_oConn);
			String sTax  = oTax.getPurchaseTaxAccount();
			Account oTaxAcc = AccountTool.getAccountByID (sTax, m_oConn);
	        validate (oTaxAcc, oTax.getTaxCode(), TaxPeer.PURCHASE_TAX_ACCOUNT);

			GlTransaction oTaxTrans = new GlTransaction();
			oTaxTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
			oTaxTrans.setProjectId        (_oTD.getProjectId());
			oTaxTrans.setDepartmentId     (_oTD.getDepartmentId());
			oTaxTrans.setAccountId        (oTaxAcc.getAccountId());
			oTaxTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
			oTaxTrans.setCurrencyRate     (bd_ONE); //journal in base currency
			oTaxTrans.setAmount           (bdTax);
			oTaxTrans.setAmountBase       (bdTax);
			oTaxTrans.setDebitCredit      (i_DEBIT);
			
			addJournal (oTaxTrans, oTaxAcc, _vGLTrans);	

			//AP -> CREDIT			
			//for default use AP account from vendor
			Account oTaxAP = AccountTool.getAccountPayable(
					m_oCurrency.getCurrencyId(), m_oVendor.getVendorId(), m_oConn);
			
			//if multi currency then get Tax AP account from base currency setting.
			if (!m_oCurrency.getCurrencyId().equals(m_oBaseCurrency.getCurrencyId()))
			{
				oTaxAP = AccountTool.getAccountByID(m_oBaseCurrency.getApAccount(), m_oConn);
			}			
			validate (oTaxAP, m_oCurrency.getCurrencyCode(), CurrencyPeer.AP_ACCOUNT);
			
			GlTransaction oAPTrans 		 = new GlTransaction();
			oAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
			oAPTrans.setProjectId        (_oTD.getProjectId());
			oAPTrans.setDepartmentId     (_oTD.getDepartmentId());
			oAPTrans.setAccountId        (oTaxAP.getAccountId()); 
			oAPTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
			oAPTrans.setCurrencyRate     (bd_ONE); //journal in base currency
			oAPTrans.setAmount           (bdTax);
			oAPTrans.setAmountBase       (bdTax);
			oAPTrans.setDebitCredit      (i_CREDIT);
			
			addJournal (oAPTrans, oTaxAP, _vGLTrans);
		}		
	}	
	
	/**
	 * create freight GL Trans 
	 * 
	 * @param _oTR
	 * @param _vGLTrans
	 * @throws Exception
	 */
	/*
	private void createFreightTrans (PurchaseInvoice _oTR, List _vFRT, List _vGLTrans, Connection _oConn)
		throws Exception
	{
        double dFreight = _oTR.getTotalExpense().doubleValue();	
        if (dFreight > 0) 
        {
        	Currency oFRCurrency = CurrencyTool.getCurrencyByID(_oTR.getFreightCurrencyId(), _oConn);
        	//TODO: add freight rate and freight expense account department & project selection
        	
        	double dFreightBase = m_dRate * dFreight;
    		double dRate = m_dRate;
    		if (oFRCurrency.getIsDefault()) 
    		{
    			dRate = 1;
    			dFreightBase = dFreight;
    		}
    		
        	if(!_oTR.getFreightAsCost()) 
        	{
        	    //create Freight Journal, 
        		//Freight Account > DEBIT 
        		String sFreight = _oTR.getFreightAccountId();
        		Account oFreight = AccountTool.getAccountByID ( sFreight, _oConn );
        		validate (oFreight, _oTR.getPurchaseInvoiceNo(), PurchaseInvoicePeer.FREIGHT_ACCOUNT_ID);
        		
        		GlTransaction oFreightTrans       = new GlTransaction();
        		oFreightTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
        		oFreightTrans.setProjectId        (""); //DEPT project should be set
        		oFreightTrans.setDepartmentId     ("");
        		oFreightTrans.setAccountId        (oFreight.getAccountId());
        		oFreightTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
        		oFreightTrans.setCurrencyRate     (new BigDecimal(dRate));
        		oFreightTrans.setAmount           (new BigDecimal(dFreight));
        		oFreightTrans.setAmountBase       (new BigDecimal(dFreightBase));
        		oFreightTrans.setDebitCredit      (i_DEBIT);
        		
        		addJournal (oFreightTrans, oFreight, _vGLTrans);	
        		
        		//Freight Currency AP Account > CREDIT
        		String sFAP = oFRCurrency.getApAccount();
        		Account oFAP = AccountTool.getAccountByID(sFAP, _oConn);
        		validate (oFAP, oFRCurrency.getCurrencyCode(), CurrencyPeer.AP_ACCOUNT);
        		
        		GlTransaction oFAPTrans 	  = new GlTransaction();
        		oFAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
        		oFAPTrans.setProjectId        ("");
        		oFAPTrans.setDepartmentId     ("");
        		oFAPTrans.setAccountId        (oFAP.getAccountId());
        		oFAPTrans.setCurrencyId       (oFRCurrency.getCurrencyId());
        		oFAPTrans.setCurrencyRate     (new BigDecimal(dRate));
        		oFAPTrans.setAmount           (new BigDecimal(dFreight));
        		oFAPTrans.setAmountBase       (new BigDecimal(dFreightBase));
        		oFAPTrans.setDebitCredit      (i_CREDIT);
        		
        		addJournal (oFAPTrans, oFAP, _vGLTrans);	        		
        	}
        	else //if freight is set as inventory
        	{
        		//inventory account gl trans already include freight cost
        		//so only create the freight currency AP journal
        		if (!_oTR.getCurrencyId().equals(_oTR.getFreightCurrencyId())) 
        		{	        	
            		dFreightBase = dFreight;
        			dFreight = dFreight / m_dRate;
        			
        			String sFAP = oFRCurrency.getApAccount();
	        		Account oFAP = AccountTool.getAccountByID ( sFAP, _oConn );
	        		validate (oFAP, oFRCurrency.getCurrencyCode(), CurrencyPeer.AP_ACCOUNT);
	        		
	        		//Freight Account Payable -> CREDIT
	        		GlTransaction oFAPTrans  = new GlTransaction();
	        		oFAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
	        		oFAPTrans.setProjectId        ("");
	        		oFAPTrans.setDepartmentId     ("");
	        		oFAPTrans.setAccountId        (oFAP.getAccountId());
	        		oFAPTrans.setCurrencyId       (oFRCurrency.getCurrencyId());
	        		oFAPTrans.setCurrencyRate     (new BigDecimal(dRate));
	        		oFAPTrans.setAmount           (new BigDecimal(dFreightBase));
	        		oFAPTrans.setAmountBase       (new BigDecimal(dFreightBase));
	        		oFAPTrans.setDebitCredit      (i_CREDIT);
	        		
	        		addJournal (oFAPTrans, oFAP, _vGLTrans);	        
        		}
        		else
        		{
        			//AP -> CREDIT
                	String sAP  = m_oCurrency.getApAccount();
                	Account oAP = AccountTool.getAccountByID (sAP, _oConn);
                	
                	GlTransaction oAPTrans 		 = new GlTransaction();
                	oAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
                	oAPTrans.setProjectId        ("");
                	oAPTrans.setDepartmentId     ("");
                	oAPTrans.setAccountId        (oAP.getAccountId()); 
                	oAPTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
                	oAPTrans.setCurrencyRate     (m_bdRate); 
                	oAPTrans.setAmount           (new BigDecimal(dFreight));
                	oAPTrans.setAmountBase       (new BigDecimal(dFreightBase));
                	oAPTrans.setDebitCredit      (i_CREDIT);
                	
                	addJournal (oAPTrans, oAP, _vGLTrans);	
        		}
        	}
        }
	}*/
	
	private void createFreightTrans (PurchaseInvoice _oTR, List _vFRT, List _vGLTrans)
		throws Exception
	{
	    double dFreight = _oTR.getTotalExpense().doubleValue();	
	    if (dFreight > 0) 
	    {
	    	//inventory account gl trans already include freight cost
	    	//create the freight currency EXP journal
	    	for (int i = 0; i < _vFRT.size(); i++)
	    	{
	    		PurchaseInvoiceFreight oPIF = (PurchaseInvoiceFreight) _vFRT.get(i);
	    		
	    		Account oExp = AccountTool.getAccountByID (oPIF.getAccountId(), m_oConn);
	    		validate (oExp, oPIF.getFreightPiNo(), PurchaseInvoiceFreightPeer.ACCOUNT_ID);
	    		
	    		//Freight Account Payable -> CREDIT
	    		GlTransaction oExpTrans  = new GlTransaction();
	    		oExpTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
	    		oExpTrans.setProjectId        (oPIF.getProjectId());
	    		oExpTrans.setDepartmentId     (oPIF.getDepartmentId());
	    		oExpTrans.setAccountId        (oExp.getAccountId());
	    		oExpTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	    		oExpTrans.setCurrencyRate     (bd_ONE);
	    		oExpTrans.setAmount           (oPIF.getFreightAmount());
	    		oExpTrans.setAmountBase       (oPIF.getFreightAmount());
	    		oExpTrans.setDebitCredit      (i_CREDIT);
	    		
	    		addJournal (oExpTrans, oExp, _vGLTrans);
	    	}
	    }
	}
	
	private void createPIExpTrans (PurchaseInvoice _oTR, List _vPIE, List _vGLTrans)
		throws Exception
	{
	    if (_vPIE != null && _vPIE.size() > 0) 
	    {
	    	//inventory account gl trans already include freight cost
	    	//create the freight currency EXP journal
	    	for (int i = 0; i < _vPIE.size(); i++)
	    	{
	    		PurchaseInvoiceExp oPIE = (PurchaseInvoiceExp) _vPIE.get(i);
	    		
	    		Account oExp = AccountTool.getAccountByID (oPIE.getAccountId(), m_oConn);
	    		validate (oExp, oPIE.getAccountCode(), PurchaseInvoiceExpPeer.ACCOUNT_ID);
	    		
	    		Account oAP = AccountTool.getAccountPayable(m_oCurrency.getCurrencyId(), m_oVendor.getVendorId(), m_oConn);
	    	    validate (oAP, m_oCurrency.getCurrencyCode(), CurrencyPeer.AP_ACCOUNT);

	    		double dBase = oPIE.getAmount().doubleValue() * m_bdRate.doubleValue();
	    		BigDecimal bdBase = new BigDecimal(dBase);
	    		
	    		//Exp Account Payable -> CREDIT
	    		GlTransaction oExpTrans  = new GlTransaction();
	    		oExpTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
	    		oExpTrans.setProjectId        (oPIE.getProjectId());
	    		oExpTrans.setDepartmentId     (oPIE.getDepartmentId());
	    		oExpTrans.setAccountId        (oExp.getAccountId());
	    		oExpTrans.setDebitCredit      (i_DEBIT);	  
	    		
	    		Currency oExpCurr = CurrencyTool.getCurrencyByID(oExp.getCurrencyId());	    			    		
	    		if(oExpCurr != null && oExpCurr.getIsDefault() || oExp.getAccountType() > i_EQUITY) //P&L
	    		{
		    		oExpTrans.setCurrencyId   (m_oBaseCurrency.getCurrencyId());
		    		oExpTrans.setCurrencyRate (bd_ONE);
		    		oExpTrans.setAmount       (bdBase);
		    		oExpTrans.setAmountBase   (bdBase);
	    		}
	    		else
	    		{
	    			oExpTrans.setCurrencyId   (m_oCurrency.getCurrencyId());
					oExpTrans.setCurrencyRate (m_bdRate);
					oExpTrans.setAmount       (oPIE.getAmount());
					oExpTrans.setAmountBase   (bdBase);						
	    		}
	    		addJournal (oExpTrans, oExp, _vGLTrans); 	    		

				GlTransaction oAPTrans = new GlTransaction();
				oAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_INVOICE);
				oAPTrans.setProjectId        (oPIE.getProjectId());
				oAPTrans.setDepartmentId     (oPIE.getDepartmentId());
				oAPTrans.setAccountId        (oAP.getAccountId());
				oAPTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
				oAPTrans.setCurrencyRate     (m_bdRate);
				oAPTrans.setAmount           (oPIE.getAmount());
				oAPTrans.setAmountBase       (bdBase);
				oAPTrans.setDebitCredit      (i_CREDIT);
				
				addJournal (oAPTrans, oAP, _vGLTrans);
	    	}
	    }
	}
	
	/**
	 * Update PR Journal, we must do this because it's possible that item price in PI 
	 * changed and not the same anymore with price in PO, so the cost and unbilled 
	 * amount in PR Journal needs to be updated 
	 * 
	 * @param m_oConn Connection
	 */
	private void updatePRJournal(Map _mPR) 
		throws Exception
	{
		Iterator oIter = _mPR.keySet().iterator();
		while (oIter.hasNext()) 
		{
			String sPRID = (String) oIter.next();
			PurchaseReceipt oPR = (PurchaseReceipt) _mPR.get(sPRID);
			log.debug("updatePRJournal  : " + oPR.getReceiptNo());

			if (m_bUpdatePRCost)
			{
				deleteJournal(i_GL_TRANS_PURCHASE_RECEIPT, sPRID, m_oConn);
				PurchaseJournalTool oPRJ = new PurchaseJournalTool(oPR, m_oConn);
				oPRJ.createPRJournal(oPR, oPR.getDetail());
			}
		}		
	}	
	
	public static void deletePurchaseJournal (int _iTransType, String _sID, Connection _oConn)
		throws Exception
	{
		deleteJournal (_iTransType, _sID, _oConn);
	}
}