package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseReturnJournalTool.java,v 1.19 2009/05/04 02:04:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseReturnJournalTool.java,v $
 * Revision 1.19  2009/05/04 02:04:46  albert
 * *** empty log message ***
 *
 * Revision 1.18  2008/06/29 07:15:44  albert
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/03 03:14:35  albert
 * *** empty log message ***
 *
 * Revision 1.16  2008/02/26 05:13:59  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PurchaseReturnJournalTool extends BaseJournalTool
{
	private static Log log = LogFactory.getLog ( PurchaseReturnJournalTool.class );

	private Currency m_oCurrency = null;
	private Currency m_oBaseCurrency = null;
	private double m_dRate = 1;
	
	private Vendor m_oVendor = null;
	
	//this map keep key -> PurchaseReturnID, value -> TotalAPAmount processed in invoice
	private Map m_hReturns = null;
	
	public PurchaseReturnJournalTool(PurchaseReturn _oTR, Connection _oConn)
		throws Exception
    {
		validate (_oConn);
		m_oConn = _oConn;
		
		m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getReturnDate(), m_oConn);
		m_oCurrency = CurrencyTool.getCurrencyByID(_oTR.getCurrencyId(), m_oConn);
		m_oBaseCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
		m_dRate = _oTR.getCurrencyRate().doubleValue();		
		m_hReturns = new HashMap();
		
        m_sTransID = _oTR.getPurchaseReturnId();
        m_sTransNo = _oTR.getReturnNo();
    	m_sUserName = _oTR.getUserName();
    	m_sLocationID = _oTR.getLocationId();    	

    	StringBuilder oRemark = new StringBuilder (_oTR.getRemark());
    	if (StringUtil.isEmpty(_oTR.getRemark()))
    	{
        	oRemark.append (LocaleTool.getString("purchase_return")).append(" ").append(_oTR.getReturnNo());
    	}            	
    	m_sRemark = oRemark.toString();

    	m_dTransDate = _oTR.getReturnDate();
    	m_dCreate = new Date();			
    	
    	m_oVendor = VendorTool.getVendorByID(_oTR.getVendorId(), m_oConn);
    	m_iSubLedger = i_SUB_LEDGER_AP;
    	m_sSubLedgerID = m_oVendor.getVendorId();
    }

	/**
	 * PR journal, unbilled -> credit and inventory -> debit
	 * add cogs, minus inventory
	 * 
	 * @param _oTR Purchase Return
	 * @param _vTD Purchase Return Detail 
	 * @param m_oConn The Connection object, null if not a join transaction
	 * @throws Exception
	 */
	public void createPRJournal(PurchaseReturn _oTR, List _vTD) 
    	throws Exception
    {   
		validate(m_oConn);
        try
        {
            List vGLTrans = new ArrayList(_vTD.size());
            for (int i = 0; i < _vTD.size(); i++)
            {
                PurchaseReturnDetail oTD = (PurchaseReturnDetail) _vTD.get(i);
                String sItemID = oTD.getItemId();
                Item oItem = ItemTool.getItemByID (sItemID);
                createReturnGLTrans (_oTR, oTD, oItem, vGLTrans);
            }
            setGLTrans (vGLTrans);
            //log.debug ("vGLTrans : " + vGLTrans );
            
            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
   
        }
        catch (Exception _oEx)
        {	
			log.error(_oEx);
            throw new JournalFailedException ("GL Journal Failed : " + _oEx.getMessage(), _oEx);
        }
	}

	/**
	 * create return journal 
	 * 
	 * @param _oTR
	 * @param _oTD
	 * @param _oItem
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createReturnGLTrans (PurchaseReturn _oTR, 
	                                  PurchaseReturnDetail _oTD, 
	                                  Item _oItem, 
	                                  List _vGLTrans) 
    	throws Exception
    {
		//if from trans then proces using cost value based returned trans
		//TODO: check * m_dRate is cost in base currency ?
		//double dItemCost = _oTD.getItemCost().doubleValue() * m_dRate;
		// log.debug("dItemCost " + dItemCost);
		BigDecimal bdItemCost = _oTD.getItemCost(); //item cost is already updated & calculated from inv costing tool
		
		//TODO: find out why and check again
		if (_oTR.getTransactionType() == i_RET_FROM_NONE || _oTD.getItemCost() == null || _oTD.getItemCost().equals(bd_ZERO))
		{				
			bdItemCost = InventoryLocationTool.getItemCost(_oTD.getItemId(), _oTR.getLocationId(), 
				_oTR.getReturnDate(), m_oConn).setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
			log.debug("dItemCost = Current COST " + bdItemCost);
		}

        double dQty = _oTD.getQty().doubleValue();
        double dReturnPrice = _oTD.getItemPrice().doubleValue();
        double dReturnAmount = dReturnPrice * dQty;
        
        double dReturnBase = dReturnAmount * m_dRate;
        double dInventoryBase = bdItemCost.multiply(_oTD.getQtyBase()).doubleValue();        
        double dReturnDiff = dInventoryBase - dReturnBase; 

        log.debug("dReturnBase " + dReturnBase);
        log.debug("dInventoryBase " + dInventoryBase);
        log.debug("dCOGSBase " + dReturnDiff);

        double dTax = _oTD.getSubTotalTax().doubleValue();
        double dTaxBase = dTax * m_dRate;
        
        Account oAcc = null;
        if (_oItem.getItemType() == i_INVENTORY_PART)
        {
	        //Inventory -> CREDIT 
	        //inventory always in base currency
	        String sInventory = _oItem.getInventoryAccount(); 
	        oAcc = AccountTool.getAccountByID (sInventory, m_oConn);        
	        validate (oAcc, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);
        }
		else if (_oItem.getItemType() == i_NON_INVENTORY_PART)
		{
			//Expense -> DEBIT
			String sExp = _oItem.getExpenseAccount(); 
			oAcc = AccountTool.getAccountByID (sExp, m_oConn);
			validate (oAcc, _oItem.getItemCode(), ItemPeer.EXPENSE_ACCOUNT);					
		}        	
        
        GlTransaction oInvTrans       = new GlTransaction();
        oInvTrans.setTransactionType  (GlAttributes.i_GL_TRANS_PURCHASE_RETURN);
        oInvTrans.setProjectId        (_oTD.getProjectId());
        oInvTrans.setDepartmentId     (_oTD.getDepartmentId());
        oInvTrans.setAccountId        (oAcc.getAccountId());
        oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
        oInvTrans.setCurrencyRate     (bd_ONE);
        oInvTrans.setAmount           (new BigDecimal(dInventoryBase));
        oInvTrans.setAmountBase       (new BigDecimal(dInventoryBase));
	    oInvTrans.setDebitCredit      (GlAttributes.i_CREDIT);

        addJournal (oInvTrans, oAcc, _vGLTrans);
        log.debug ("PR Journal Inventory : "  + oInvTrans );
        
        //AP -> DEBIT
        if (_oTR.getTransactionType() == i_RET_FROM_PR_DO)
        {
        	String sUnbilled = _oItem.getUnbilledGoodsAccount();
            Account oUnbilled = AccountTool.getAccountByID (sUnbilled, m_oConn);

	        GlTransaction oUnbilledTrans       = new GlTransaction();
	        oUnbilledTrans.setTransactionType  (GlAttributes.i_GL_TRANS_PURCHASE_RETURN);;
	        oUnbilledTrans.setProjectId        (_oTD.getProjectId());
	        oUnbilledTrans.setDepartmentId     (_oTD.getDepartmentId());
	        oUnbilledTrans.setAccountId        (oUnbilled.getAccountId());
	        oUnbilledTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
	        oUnbilledTrans.setCurrencyRate     (new BigDecimal(m_dRate));
	        oUnbilledTrans.setAmount           (new BigDecimal(dReturnAmount));
	        oUnbilledTrans.setAmountBase       (new BigDecimal(dReturnBase));
		    oUnbilledTrans.setDebitCredit      (GlAttributes.i_DEBIT);

	        addJournal (oUnbilledTrans, oUnbilled, _vGLTrans);	       
	        log.debug ("PR Journal Unbilled : "  + oUnbilledTrans );
        }
        else
        {
	        Account oAP = AccountTool.getAccountPayable(
	        	m_oCurrency.getCurrencyId(), m_oVendor.getVendorId(), m_oConn);
	
	        GlTransaction oAPTrans       = new GlTransaction();
	        oAPTrans.setTransactionType  (GlAttributes.i_GL_TRANS_PURCHASE_RETURN);
	        oAPTrans.setProjectId        (_oTD.getProjectId());
	        oAPTrans.setDepartmentId     (_oTD.getDepartmentId());
	        oAPTrans.setAccountId        (oAP.getAccountId());
	        oAPTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
	        oAPTrans.setCurrencyRate     (new BigDecimal(m_dRate));
	        oAPTrans.setAmount           (new BigDecimal(dReturnAmount));
	        oAPTrans.setAmountBase       (new BigDecimal(dReturnBase));
		    oAPTrans.setDebitCredit      (GlAttributes.i_DEBIT);
	        
	        addJournal (oAPTrans, oAP, _vGLTrans);	       
	        
	        //create tax 
	        createTaxTrans (_oTR, _oTD, _vGLTrans);
        }
        
        if (dReturnDiff != 0)
        {
        	String sPTAcc = _oItem.getPurchaseReturnAccount();
            Account oPTAcc = AccountTool.getAccountByID (sPTAcc, m_oConn);
	        validate (oPTAcc, _oItem.getItemCode(), ItemPeer.PURCHASE_RETURN_ACCOUNT);

            int iDebitCredit = GlAttributes.i_DEBIT;
            if (dReturnDiff < 0) 
            {
            	iDebitCredit = GlAttributes.i_CREDIT;
            	dReturnDiff = dReturnDiff * -1;
            }
            
	        GlTransaction oRetDiff       = new GlTransaction();
	        oRetDiff.setTransactionType  (GlAttributes.i_GL_TRANS_PURCHASE_RETURN);
	        oRetDiff.setProjectId        (_oTD.getProjectId());
	        oRetDiff.setDepartmentId     (_oTD.getDepartmentId());
	        oRetDiff.setAccountId        (oPTAcc.getAccountId());
	        oRetDiff.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oRetDiff.setCurrencyRate     (bd_ONE);
	        oRetDiff.setAmount           (new BigDecimal(dReturnDiff));
	        oRetDiff.setAmountBase       (new BigDecimal(dReturnDiff));
		    oRetDiff.setDebitCredit      (iDebitCredit);

		    addJournal (oRetDiff, oPTAcc, _vGLTrans);	       
        }
	}
	
	/**
	 * create tax GL trans (Purchase TAX & Base Currency AP Account) 
	 * 
	 * @param _oTR
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createTaxTrans (PurchaseReturn _oTR, 
								 PurchaseReturnDetail _oTD, 
								 List _vGLTrans)
		throws Exception
	{
		double dFiscalRate = 1;
		if (!m_oCurrency.getIsDefault()) 
		{
			dFiscalRate = CurrencyTool.getFiscalRate(m_oCurrency.getCurrencyId(), m_oConn);
			if (dFiscalRate == 1)
			{
				log.warn("Fiscal Rate is not set ! ");
				dFiscalRate = _oTR.getFiscalRate().doubleValue();
			}
		}
		double dTax  = _oTD.getSubTotalTax().doubleValue(); 
        double dTaxBase = dFiscalRate * dTax;
        
		//TAX VAT IN -> DEBIT
		if (dTax > 0)
		{                   
			//PURCHASE TAX (VAT IN) -> DEBIT
			Tax oTax = TaxTool.getTaxByID (_oTD.getTaxId(), m_oConn);
			String sTax  = oTax.getPurchaseTaxAccount();
			Account oTaxAcc = AccountTool.getAccountByID (sTax, m_oConn);
	        validate (oTaxAcc, oTax.getTaxCode(), TaxPeer.PURCHASE_TAX_ACCOUNT);

			GlTransaction oTaxTrans = new GlTransaction();
			oTaxTrans.setTransactionType  (i_GL_TRANS_PURCHASE_RETURN);
			oTaxTrans.setProjectId        (_oTD.getProjectId());
			oTaxTrans.setDepartmentId     (_oTD.getDepartmentId());
			oTaxTrans.setAccountId        (oTaxAcc.getAccountId());
			oTaxTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
			oTaxTrans.setCurrencyRate     (bd_ONE); //journal in base currency
			oTaxTrans.setAmount           (new BigDecimal(dTaxBase));
			oTaxTrans.setAmountBase       (new BigDecimal(dTaxBase));
			oTaxTrans.setDebitCredit      (i_CREDIT);
			
			addJournal (oTaxTrans, oTaxAcc, _vGLTrans);	
			
			//AP -> CREDIT
			Account oTaxAP = AccountTool.getAccountPayable(
				m_oCurrency.getCurrencyId(), m_oVendor.getVendorId(), m_oConn);			
			//if multi currency then get Tax AP account from base currency setting.
			if (!m_oCurrency.getCurrencyId().equals(m_oBaseCurrency.getCurrencyId()))
			{
				oTaxAP = AccountTool.getAccountByID(m_oBaseCurrency.getApAccount(), m_oConn);			
			}			
        	validate (oTaxAP, m_oBaseCurrency.getCurrencyCode(), CurrencyPeer.AP_ACCOUNT);
        	
        	GlTransaction oAPTrans 		 = new GlTransaction();
        	oAPTrans.setTransactionType  (i_GL_TRANS_PURCHASE_RETURN);
        	oAPTrans.setProjectId        (_oTD.getProjectId());
        	oAPTrans.setDepartmentId     (_oTD.getDepartmentId());
        	oAPTrans.setAccountId        (oTaxAP.getAccountId()); 
        	oAPTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
        	oAPTrans.setCurrencyRate     (bd_ONE); //journal in base currency
        	oAPTrans.setAmount           (new BigDecimal(dTaxBase));
        	oAPTrans.setAmountBase       (new BigDecimal(dTaxBase));
        	oAPTrans.setDebitCredit      (i_DEBIT);
        	
        	addJournal (oAPTrans, oTaxAP, _vGLTrans);				
		}		
	}	
	

	/**
	 * update old PT journal 
	 * 
	 * @param _sTransID
	 * @throws Exception
	 */
	public void updatePTJournal(PurchaseReturn _oTR) 
    	throws Exception
    {   		
		if(_oTR != null)
		{
			validate(m_oConn);
			List vExist = GlTransactionTool.getDataByTypeAndTransID(i_GL_TRANS_PURCHASE_RETURN, _oTR.getPurchaseReturnId(), m_oConn);			
			log.debug ("updatePTJournal vExistGL : " + vExist);
			if(vExist.size() > 0)
			{
				List _vTD = PurchaseReturnTool.getDetailsByID(_oTR.getPurchaseReturnId(), m_oConn);
				if (_vTD != null)
				{				
			        try
			        {
			        	deleteJournal(i_GL_TRANS_PURCHASE_RETURN, _oTR.getPurchaseReturnId(), m_oConn);
			        	
			            List vGLTrans = new ArrayList(_vTD.size());
			            for (int i = 0; i < _vTD.size(); i++)
			            {
			                PurchaseReturnDetail oTD = (PurchaseReturnDetail) _vTD.get(i);
			                Item oItem = ItemTool.getItemByID (oTD.getItemId(), m_oConn);
			                createReturnGLTrans(_oTR, oTD, oItem, vGLTrans);
			            }
			            setGLTrans (vGLTrans);
			            log.debug ("updatePTJournal vNewGLTrans : " + vGLTrans);
			            
			            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
			        }
			        catch (Exception _oEx)
			        {
			        	_oEx.printStackTrace();
			            throw new NestableException("Update PT GL Journal Failed : " + _oEx.getMessage(), _oEx);
			        }
				}
			}
		}
	}	
}