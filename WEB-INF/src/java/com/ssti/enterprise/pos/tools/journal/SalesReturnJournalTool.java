package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.GlConfigPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesReturnJournalTool.java,v 1.17 2008/08/20 16:03:43 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesReturnJournalTool.java,v $
 * 
 * 2017-01-07
 * - Change method createARJournal. if Medical then when payment_type is not Credit then return
 *   to PaymentType DirectSales. If not (normal retail) will return GL config Direct Sales
 * 
 * 2016-11-03
 * - Fix sales return rounding journal bug 
 * 
 * 2016-02-01
 * - Fix bug in SalesReturnTool method recalculateCost previously get cost from SI date.
 * 
 * </pre><br>
 */
public class SalesReturnJournalTool extends BaseJournalTool
{
	private static Log log = LogFactory.getLog ( SalesReturnJournalTool.class );
	
	private Currency m_oCurrency = null;
	private Currency m_oBaseCurrency = null;	
	private boolean m_bMultiCurrency = false;
	
	private double m_dRate = 1;
	private BigDecimal m_bdRate = bd_ONE;
	private Location m_oLoc = null;
	private Customer m_oCustomer = null;
	
	private boolean isConsignment = false;
	private boolean isStore = false;
	private boolean isStoreCashReturn = false;
	private boolean isCashReturn = false;
	
	public SalesReturnJournalTool(SalesReturn _oTR, Connection _oConn)
		throws Exception
	{
		validate (_oConn);
		m_oConn = _oConn;
		
        m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getReturnDate(), m_oConn);
        m_oCurrency = CurrencyTool.getCurrencyByID(_oTR.getCurrencyId(),m_oConn);
        m_oBaseCurrency = CurrencyTool.getDefaultCurrency(m_oConn);        
        m_dRate = _oTR.getCurrencyRate().doubleValue();		
        m_bdRate = new BigDecimal(m_dRate);
        if (!StringUtil.isEqual(m_oCurrency.getCurrencyId(), m_oBaseCurrency.getCurrencyId()))
        {
        	m_bMultiCurrency = true;
        }
        
        m_oLoc  = LocationTool.getLocationByID(_oTR.getLocationId());    	
    	
        //set consignment flag if, location is consignment type then do journal like DO
        if (m_oLoc != null )
        {
        	if (m_oLoc.getLocationType() == i_CONSIGNMENT) isConsignment = true;
        	if (m_oLoc.getLocationType() == i_STORE) 
        	{
        		isStore = true;
        		if (_oTR.getCashReturn() && StringUtil.isEmpty(_oTR.getBankId())) isStoreCashReturn = true;         		
        	}
        }

    	if (_oTR.getCashReturn())
    	{
    		isCashReturn = true;
    	}
        
        m_sTransID = _oTR.getSalesReturnId();
        m_sTransNo = _oTR.getReturnNo();
    	m_sUserName = _oTR.getUserName();
    	m_sLocationID = _oTR.getLocationId();    	
        
        StringBuilder oRemark = new StringBuilder (_oTR.getRemark());
    	if (StringUtil.isEmpty(_oTR.getRemark()))
    	{
        	oRemark.append (LocaleTool.getString("sales_return")).append(" ").append(_oTR.getReturnNo());
    	}        
    	m_sRemark = oRemark.toString();
    	
    	m_dTransDate = _oTR.getReturnDate();
    	m_dCreate = new Date();
    	
    	m_oCustomer = CustomerTool.getCustomerByID(_oTR.getCustomerId(), _oConn);
    	m_iSubLedger = i_SUB_LEDGER_AR;
    	m_sSubLedgerID = m_oCustomer.getCustomerId();
	}

	/**
	 * Sales ReturnTransaction journal, credit -> debit and inventory -> debit
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _vPayment
	 * @param m_oConn
	 * @throws Exception
	 */
	public void createSalesReturnJournal(SalesReturn _oTR, List _vTD, Map _mGroup) 
    	throws Exception
    {
		validate(m_oConn);
        try
        {
            List vGLTrans = new ArrayList(_vTD.size());
            for (int i = 0; i < _vTD.size(); i++)
            {
                SalesReturnDetail oTD = (SalesReturnDetail) _vTD.get(i);
                Item oItem = ItemTool.getItemByID (oTD.getItemId(), m_oConn);
                createSalesReturnGLTrans (_oTR, oTD, oItem, _mGroup, vGLTrans);
            }
	        if (!isConsignment && _oTR.getTransactionType() != i_RET_FROM_PR_DO)
	        {
	            createARJournal (_oTR, _vTD, vGLTrans);
	            createTotalDiscountJournal (_oTR, _vTD, vGLTrans);
	        }            
	        
	        setGLTrans(vGLTrans);
	        if (log.isDebugEnabled()) log.debug ("vGLTrans : " + vGLTrans );
            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);      
        }
        catch (Exception _oEx)
        {
			_oEx.printStackTrace();
			log.error(_oEx);
            throw new NestableException ("GL Journal Failed : " + _oEx.getMessage(), _oEx);
        }        
	}
	
	/**
	 * 
	 * @param _oTR
	 * @param _oTD
	 * @param _oItem
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createSalesReturnGLTrans (SalesReturn _oTR, 
	                                 	   SalesReturnDetail _oTD, 
										   Item _oItem, 
										   Map _mGroup,
										   List _vGLTrans) 
    	throws Exception
    {
        double dTax  = _oTD.getSubTotalTax().doubleValue(); 
        double dDisc = _oTD.getSubTotalDisc().doubleValue(); 
        double dReturn = _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();
        
        double dTaxBase = dTax * m_dRate;
        double dDiscBase = dDisc * m_dRate;
        double dReturnBase = dReturn * m_dRate;

        if(_oTR.getIsInclusiveTax())
        {
        	dReturnBase = dReturnBase - dTaxBase;
        	dReturn = dReturn - dTax;
        }
        
        //Inventory -> CREDIT
    	if (_oItem.getItemType() == i_INVENTORY_PART)
    	{
	        BigDecimal dCost = _oTD.getSubTotalCost();

	        //Inventory -> DEBIT returned item will be back to inventory 
	        String sInventory = _oItem.getInventoryAccount(); 
	        Account oInv = AccountTool.getAccountByID (sInventory, m_oConn);        
	        validate (oInv, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);
	        
	        GlTransaction oInvTrans       = new GlTransaction();
	        oInvTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
	        oInvTrans.setProjectId        (_oTD.getProjectId());
	        oInvTrans.setDepartmentId     (_oTD.getDepartmentId());
	        oInvTrans.setAccountId        (oInv.getAccountId());
	        oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oInvTrans.setCurrencyRate     (bd_ONE);
	        oInvTrans.setAmount           (dCost);
	        oInvTrans.setAmountBase       (dCost);
	        oInvTrans.setDebitCredit      (GlAttributes.i_DEBIT);
	        
	        //COGS -> CREDIT reduce COGS
	        String sCOGS  = _oItem.getCogsAccount();
	        Account oCOGS = AccountTool.getAccountByID (sCOGS, m_oConn);
	        if (_oTR.getTransactionType() == i_RET_FROM_PR_DO)
	        {
		        oCOGS = AccountTool.getDeliveryOrderAccount(sCOGS, m_oConn);	        
	        }
	        validate (oCOGS, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);
	        
	        GlTransaction oCOGSTrans = new GlTransaction();
	        oCOGSTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
	        oCOGSTrans.setProjectId        (_oTD.getProjectId());
	        oCOGSTrans.setDepartmentId     (_oTD.getDepartmentId());
	        oCOGSTrans.setAccountId        (oCOGS.getAccountId());
	        oCOGSTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oCOGSTrans.setCurrencyRate     (bd_ONE);
	        oCOGSTrans.setAmount           (dCost);
	        oCOGSTrans.setAmountBase       (dCost);
	        oCOGSTrans.setDebitCredit      (GlAttributes.i_CREDIT);
	        
	        addJournal (oInvTrans, oInv, _vGLTrans);
	        addJournal (oCOGSTrans, oCOGS, _vGLTrans);
	        if (log.isDebugEnabled())
	        {
	        	log.debug ("Sales Return Journal COGS : " + oCOGS.getAccountName() + oCOGSTrans);
	        	log.debug ("Sales Return Journal Inventory : " + oInv.getAccountName()  + oInvTrans);
	        }
    	}
    	else if (_oItem.getItemType() == i_GROUPING)
    	{
    		createGroupingInventoryJournal (_oTR.getLocationId(), _oTR.getTransactionDate(), 
    			_oTD.getProjectId(), _oTD.getDepartmentId(), _oTD.getQty().doubleValue(), 
					_oItem, _vGLTrans, _mGroup, m_oConn);
    	}    	
    	
    	//TODO: TAX ALWAYS IN BASE CURRENCY
        if (!isConsignment && _oTR.getTransactionType() != i_RET_FROM_PR_DO)
        {
        	if (dTax > 0)
        	{                   
        		//VAT IN TAX -> DEBIT 
        		Tax oTax = TaxTool.getTaxByID (_oTD.getTaxId(), m_oConn);
        		String sTax  = oTax.getSalesTaxAccount();
        		Account oTaxAcc = AccountTool.getAccountByID (sTax, m_oConn);
        		validate (oTaxAcc, oTax.getTaxCode(), TaxPeer.SALES_TAX_ACCOUNT);
        		
        		GlTransaction oTaxTrans = new GlTransaction();
        		oTaxTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
        		oTaxTrans.setProjectId        (_oTD.getProjectId());
        		oTaxTrans.setDepartmentId     (_oTD.getDepartmentId());
        		oTaxTrans.setAccountId        (oTaxAcc.getAccountId());
        		oTaxTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
        		oTaxTrans.setCurrencyRate     (m_bdRate);
        		oTaxTrans.setAmount           (new BigDecimal(dTaxBase));
        		oTaxTrans.setAmountBase       (new BigDecimal(dTaxBase));
        		oTaxTrans.setDebitCredit      (GlAttributes.i_DEBIT);
        		
        		addJournal (oTaxTrans, oTaxAcc, _vGLTrans);	
        	}
        	
        	if (dDisc != 0)
        	{ 
        		//DISCOUNT -> DEBIT            
        		String sDisc  = _oItem.getItemDiscountAccount();
        		Account oDisc = AccountTool.getAccountByID (sDisc, m_oConn);
        		validate (oDisc, _oItem.getItemCode(), ItemPeer.ITEM_DISCOUNT_ACCOUNT);
        		
        		GlTransaction oDiscTrans = new GlTransaction();
        		oDiscTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
        		oDiscTrans.setProjectId        (_oTD.getProjectId());
        		oDiscTrans.setDepartmentId     (_oTD.getDepartmentId());
        		oDiscTrans.setAccountId        (oDisc.getAccountId());
        		oDiscTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
        		oDiscTrans.setCurrencyRate     (m_bdRate);
        		oDiscTrans.setAmount           (new BigDecimal(dDisc));
        		oDiscTrans.setAmountBase       (new BigDecimal(dDiscBase));
        		oDiscTrans.setDebitCredit      (GlAttributes.i_CREDIT);
        		
        		addJournal (oDiscTrans, oDisc, _vGLTrans);	
        	}
        	
        	//REVENUE -> DEBIT reduce the revenue 
        	String sReturnRevenue  = _oItem.getSalesReturnAccount();
        	Account oReturnRevenue = AccountTool.getAccountByID (sReturnRevenue, m_oConn);
        	validate (oReturnRevenue, _oItem.getItemCode(), ItemPeer.SALES_ACCOUNT);
        	
        	GlTransaction oReturnTrans = new GlTransaction();
        	oReturnTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
        	oReturnTrans.setProjectId        (_oTD.getProjectId());
        	oReturnTrans.setDepartmentId     (_oTD.getDepartmentId());
        	oReturnTrans.setAccountId        (oReturnRevenue.getAccountId());
        	oReturnTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
        	oReturnTrans.setCurrencyRate     (m_bdRate);
        	oReturnTrans.setAmount           (new BigDecimal(dReturn));
        	oReturnTrans.setAmountBase       (new BigDecimal(dReturnBase));
        	oReturnTrans.setDebitCredit      (GlAttributes.i_DEBIT);
        	
        	addJournal (oReturnTrans, oReturnRevenue, _vGLTrans);	
        	
        	if (log.isDebugEnabled())
        	{
        		log.debug ("Sales Return Journal Revenue : " + 
        			AccountTool.getAccountNameByID (oReturnTrans.getAccountId(), m_oConn)  + oReturnTrans);
        	}
        }
    }

	/**
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _vGLTrans
	 * @throws Exception
	 */
	private void createARJournal(SalesReturn _oTR, List _vTD, List _vGLTrans) 
    	throws Exception
    {           
		double dTotalItemDisc = SalesReturnTool.countTotalDisc(_vTD).doubleValue();
		double dInvoiceDisc = _oTR.getTotalDiscount().doubleValue() - dTotalItemDisc;
		double dAmount = _oTR.getTotalAmount().doubleValue();
        if(!_oTR.getIsInclusiveTax())
        {
            dAmount = dAmount + _oTR.getTotalTax().doubleValue();
        }
        dAmount = dAmount - dInvoiceDisc;        
        double dRounding = _oTR.getRoundingAmount().doubleValue();
        dAmount = dAmount + dRounding;
        
		double dAmountBase = dAmount * m_dRate;
        //PaymentType oType = PaymentTypeTool.getPaymentTypeByID(_oTR.getPaymentTypeId());
        GlTransaction oPaymentTrans = new GlTransaction();
       
        Account oPMT = AccountTool.getAccountReceivable(
        	m_oCurrency.getCurrencyId(), m_oCustomer.getCustomerId(), m_oConn);
        validate (oPMT, m_oCurrency.getCurrencyCode(), CurrencyPeer.AR_ACCOUNT);
        
        //if store cash return then Journal to Direct Sales (Location == STORE, BANK == '')
        if (isCashReturn)
        {        
            PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(_oTR.getPaymentTypeId(),m_oConn);
            if(oPT == null || !oPT.getIsCredit())
            {            	
            	if(oPT != null && PreferenceTool.medicalInstalled())
            	{
            		//in med payment go to each payment type Direct Sales Account 
            		oPMT = AccountTool.getDirectSalesAccount(null, m_oConn);
            	}
            	else
            	{
            		//do not pass oPT so other than credit return will go to GL direct sales account
            		oPMT = AccountTool.getDirectSalesAccount(null, m_oConn);
            	}
            }
        }
        
        log.debug ("Sales Return  AR : " + oPMT  + " " + oPaymentTrans );
            
        oPaymentTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
        oPaymentTrans.setProjectId        ("");
        oPaymentTrans.setDepartmentId     ("");
        oPaymentTrans.setAccountId 		  (oPMT.getAccountId());
        oPaymentTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
        oPaymentTrans.setCurrencyRate     (m_bdRate);
        oPaymentTrans.setAmount           (new BigDecimal(dAmount));
        oPaymentTrans.setAmountBase       (new BigDecimal(dAmountBase));
        oPaymentTrans.setDebitCredit      (GlAttributes.i_CREDIT);
        addJournal (oPaymentTrans, oPMT, _vGLTrans);	

        double dRoundingBase = dRounding * m_dRate;
        if (dRounding != 0)
        {
        	Account oRound = AccountTool.getAccountByID(GLConfigTool.getGLConfig().getPosRoundingAccount(), m_oConn);
            validate (oRound, _oTR.getReturnNo(), GlConfigPeer.POS_ROUNDING_ACCOUNT);

        	GlTransaction oRoundTrans = new GlTransaction();
            oRoundTrans.setTransactionType  (i_GL_TRANS_SALES_RETURN);
            oRoundTrans.setProjectId        ("");
            oRoundTrans.setDepartmentId     ("");
            oRoundTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
            oRoundTrans.setAccountId        (oRound.getAccountId());
            oRoundTrans.setCurrencyRate     (m_bdRate);
            oRoundTrans.setAmount           (new BigDecimal(dRounding));
            oRoundTrans.setAmountBase       (new BigDecimal(dRoundingBase));            
            oRoundTrans.setDebitCredit      (i_DEBIT);
            
            addJournal (oRoundTrans, oRound, _vGLTrans);	
        }                       

    }

	/**
	 * @param _oTR Sales Return Object
	 * @param _vGLTrans GLTrans List
	 */
	private void createTotalDiscountJournal(SalesReturn _oTR, List _vTD, List _vGLTrans) 
		throws Exception
	{
		double dTotalItemDisc = SalesReturnTool.countTotalDisc(_vTD).doubleValue();
		double dAmount = _oTR.getTotalDiscount().doubleValue() - dTotalItemDisc;
		double dAmountBase = dAmount * m_dRate;
		
		if (log.isDebugEnabled())
		{
			log.debug("createTotalDiscountJournal : TD "    + _oTR.getTotalDiscount().doubleValue());
			log.debug("createTotalDiscountJournal : TDPCT " + _oTR.getTotalDiscountPct());
			log.debug("createTotalDiscountJournal : INVAMT " + _oTR.getTotalAmount().doubleValue());		
			log.debug("createTotalDiscountJournal : DISAMT " + dAmount);
		}

		//if invoice total discount exist then create invoice sales discount journal
        if (dAmount != 0)
        {
        	String sInvDisc = _oTR.getReturnDiscAccId(); //use invoice discount account ID set in trans
        	if (StringUtil.isEmpty(sInvDisc))
        	{
        		sInvDisc = m_oCurrency.getSdAccount();
        	}        	
        	Account oInvDisc = AccountTool.getAccountByID(sInvDisc, m_oConn);
            validate (oInvDisc, m_oCurrency.getCurrencyCode(), CurrencyPeer.SD_ACCOUNT);
        	
        	GlTransaction oDiscTrans = new GlTransaction();
            oDiscTrans.setTransactionType  (GlAttributes.i_GL_TRANS_SALES_RETURN);
            oDiscTrans.setProjectId        ("");
            oDiscTrans.setDepartmentId     ("");
            oDiscTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
            oDiscTrans.setAccountId        (oInvDisc.getAccountId());
            oDiscTrans.setCurrencyRate     (m_bdRate);
            oDiscTrans.setAmount           (new BigDecimal(dAmount));
            oDiscTrans.setAmountBase       (new BigDecimal(dAmountBase));
            oDiscTrans.setDebitCredit      (GlAttributes.i_CREDIT);
            addJournal (oDiscTrans, oInvDisc, _vGLTrans);	
        }		
	}	
	
	/**
	 * 
	 * @param _sLocationID
	 * @param _dTransDate
	 * @param _sProjectID
	 * @param _sDepartmentID
	 * @param _dQty
	 * @param _oItem
	 * @param _vGLTrans
	 * @param _oConn
	 * @throws Exception
	 */
	private void createGroupingInventoryJournal(String  _sLocationID,
												Date _dTransDate,
												String _sProjectID,
												String _sDepartmentID,
												double _dQty,
												Item _oItem, 
												List _vGLTrans,
												Map _mGroup,
												Connection _oConn) 
		throws Exception
	{
		List vGroup = (List) _mGroup.get(_oItem.getItemId());
		for (int i = 0; i < vGroup.size(); i++) 
		{	
			ItemGroup oItemGroup = (ItemGroup) vGroup.get (i);	 
			Item oItemPart = ItemTool.getItemByID(oItemGroup.getItemId(), _oConn);
			
			double dBaseValue = UnitTool.getBaseValue(oItemGroup.getItemId(), oItemGroup.getUnitId(), _oConn);
			double dGroupQty = oItemGroup.getQty().doubleValue();
			double dQtyBase = _dQty * dGroupQty * dBaseValue;			
			double dCost = oItemGroup.getCost().doubleValue();
			
			BigDecimal  dAmount = new BigDecimal (dCost * dQtyBase);
			
			String sInventory = oItemPart.getInventoryAccount(); 
	        Account oInv = AccountTool.getAccountByID ( sInventory, _oConn );        
	        validate (oInv, oItemPart.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);
			
	        GlTransaction oInvTrans       = new GlTransaction();
	        oInvTrans.setTransactionType  (i_GL_TRANS_SALES_RETURN);
	        oInvTrans.setProjectId        (_sProjectID);
	        oInvTrans.setDepartmentId     (_sDepartmentID);
	        oInvTrans.setAccountId        (oInv.getAccountId());
	        oInvTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oInvTrans.setCurrencyRate     (bd_ONE);
	        oInvTrans.setAmount           (dAmount);
	        oInvTrans.setAmountBase       (dAmount);
	        oInvTrans.setDebitCredit      (i_DEBIT);
		
	        //COGS -> DEBIT -- COGS will go to group account
	        String sCOGS  = _oItem.getCogsAccount();	        	        
			String sCode  = _oItem.getItemCode();
	        if (i_GL_GROUPING_COST == 2) //cogs journaled to item part
			{
				sCOGS = oItemPart.getCogsAccount();
				sCode = oItemPart.getItemCode();
			}
	        Account oCOGS = AccountTool.getAccountByID ( sCOGS, _oConn );
	        validate (oCOGS, sCode, ItemPeer.COGS_ACCOUNT);

	        GlTransaction oCOGSTrans = new GlTransaction();
	        oCOGSTrans.setTransactionType  (i_GL_TRANS_SALES_RETURN);
	        oCOGSTrans.setProjectId        (_sProjectID);
	        oCOGSTrans.setDepartmentId     (_sDepartmentID);
	        oCOGSTrans.setAccountId        (oCOGS.getAccountId());
	        oCOGSTrans.setCurrencyId       (m_oBaseCurrency.getCurrencyId());
	        oCOGSTrans.setCurrencyRate     (bd_ONE);
	        oCOGSTrans.setAmount           (dAmount);
	        oCOGSTrans.setAmountBase       (dAmount);
	        oCOGSTrans.setDebitCredit      (i_CREDIT);
	        
	        addJournal (oInvTrans, oInv, _vGLTrans);
	        addJournal (oCOGSTrans, oCOGS, _vGLTrans);
	        
            if (log.isDebugEnabled())
            {
            	log.debug ("Grouping Journal COGS : " + oCOGS.getAccountName() + oCOGSTrans);
            	log.debug ("Grouping Journal Inventory : " +  oInv.getAccountId() + oInvTrans);
            }	        
		}		
	}
}