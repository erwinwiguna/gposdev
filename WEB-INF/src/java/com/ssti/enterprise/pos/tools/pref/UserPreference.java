package com.ssti.enterprise.pos.tools.pref;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ssti.enterprise.pos.om.Preference;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/pref/UserPreference.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: UserPreference.java,v 1.3 2007/03/05 16:10:10 albert Exp $
 *
 * $Log: UserPreference.java,v $
 * Revision 1.3  2007/03/05 16:10:10  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/08/21 14:40:38  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/06/07 03:17:31  Albert
 * *** empty log message ***
 *
 */

public class UserPreference extends Preference
{
	private static final long serialVersionUID = 2L;
	
	//USERNAME
	String userName;
	
	//l10n PREFERENCE -> derived from Preference
	
	//SKIN PREFERENCE
	String skin;
	
	//POST LOGIN PREFERENCE
	
	//MENU PREFERENCE
	String menu;
	
	//MREPORT
	Map reportMap;
	
	//ITEM VIEW PREFERENCE
	boolean f1;
	boolean f2;
	boolean f3;
	boolean f4;
	boolean f5;
	boolean f6;
	boolean f7;
	boolean f8;
	boolean f9;
	boolean f10;
	boolean f11;
	boolean f12;
	boolean f13;
	boolean f14;
	boolean f15;
	boolean f16;
	boolean f17;
	boolean f18;
	boolean f19;
	boolean f20;
	boolean f21;
	boolean f22;
	boolean f23;
	boolean f24;
	boolean f25;
	boolean f26;
	boolean f27;
	boolean f28;
	boolean f29;
	boolean f30;
	boolean f31;
	boolean f32;
	boolean f33;
	boolean f34;
	boolean f35;
	
	String[] customField;
	
	public String[] getCustomField() {
		return customField;
	}
	
	public void setCustomField(String[] customField) {
		this.customField = customField;
	}

	public List getCustomFieldAsList() 
	{
		if (customField != null) 
		{
			List vField = new ArrayList(customField.length);
			for (int i = 0; i < customField.length; i++)
			{
				if (customField[i] != null && !customField[i].equals(""))
				{
					vField.add(customField[i]);
				}
			}
			return vField;
		}
		return null;
	}

	public boolean isFieldSelected (String _sCustomField)
	{
		if (_sCustomField != null && customField != null)
		{
			for (int i = 0; i < customField.length; i++)
			{
				if (_sCustomField.equals(customField[i])) return true;
			}
		}
		return false;
	}
	
	public String getSkin() {
		return skin;
	}
	public void setSkin(String skin) {
		this.skin = skin;
	}
	public boolean isF1() {
		return f1;
	}
	public void setF1(boolean f1) {
		this.f1 = f1;
	}
	public boolean isF2() {
		return f2;
	}
	public void setF2(boolean f2) {
		this.f2 = f2;
	}
	public boolean isF3() {
		return f3;
	}
	public void setF3(boolean f3) {
		this.f3 = f3;
	}
	public boolean isF4() {
		return f4;
	}
	public void setF4(boolean f4) {
		this.f4 = f4;
	}
	public boolean isF5() {
		return f5;
	}
	public void setF5(boolean f5) {
		this.f5 = f5;
	}
	public boolean isF6() {
		return f6;
	}
	public void setF6(boolean f6) {
		this.f6 = f6;
	}
	public boolean isF7() {
		return f7;
	}
	public void setF7(boolean f7) {
		this.f7 = f7;
	}
	public boolean isF8() {
		return f8;
	}
	public void setF8(boolean f8) {
		this.f8 = f8;
	}
	public boolean isF9() {
		return f9;
	}
	public void setF9(boolean f9) {
		this.f9 = f9;
	}
	public boolean isF10() {
		return f10;
	}
	public void setF10(boolean f10) {
		this.f10 = f10;
	}
	public boolean isF11() {
		return f11;
	}
	public void setF11(boolean f11) {
		this.f11 = f11;
	}
	public boolean isF12() {
		return f12;
	}
	public void setF12(boolean f12) {
		this.f12 = f12;
	}
	public boolean isF13() {
		return f13;
	}
	public void setF13(boolean f13) {
		this.f13 = f13;
	}
	public boolean isF14() {
		return f14;
	}
	public void setF14(boolean f14) {
		this.f14 = f14;
	}
	public boolean isF15() {
		return f15;
	}
	public void setF15(boolean f15) {
		this.f15 = f15;
	}
	public boolean isF16() {
		return f16;
	}
	public void setF16(boolean f16) {
		this.f16 = f16;
	}
	public boolean isF17() {
		return f17;
	}
	public void setF17(boolean f17) {
		this.f17 = f17;
	}
	public boolean isF18() {
		return f18;
	}
	public void setF18(boolean f18) {
		this.f18 = f18;
	}
	public boolean isF19() {
		return f19;
	}
	public void setF19(boolean f19) {
		this.f19 = f19;
	}	
	public boolean isF20() {
		return f20;
	}
	public void setF20(boolean f20) {
		this.f20 = f20;
	}
	public boolean isF21() {
		return f21;
	}
	public void setF21(boolean f21) {
		this.f21 = f21;
	}
	public boolean isF22() {
		return f22;
	}
	public void setF22(boolean f22) {
		this.f22 = f22;
	}
	public boolean isF23() {
		return f23;
	}
	public void setF23(boolean f23) {
		this.f23 = f23;
	}
	public boolean isF24() {
		return f24;
	}
	public void setF24(boolean f24) {
		this.f24 = f24;
	}
	public boolean isF25() {
		return f25;
	}
	public void setF25(boolean f25) {
		this.f25 = f25;
	}
	public boolean isF26() {
		return f26;
	}
	public void setF26(boolean f26) {
		this.f26 = f26;
	}
	public boolean isF27() {
		return f27;
	}
	public void setF27(boolean f27) {
		this.f27 = f27;
	}
	public boolean isF28() {
		return f28;
	}
	public void setF28(boolean f28) {
		this.f28 = f28;
	}
	public boolean isF29() {
		return f29;
	}
	public void setF29(boolean f29) {
		this.f29 = f29;
	}
	public boolean isF30() {
		return f30;
	}
	public void setF30(boolean f30) {
		this.f30 = f30;
	}
	public boolean isF31() {
		return f31;
	}
	public void setF31(boolean f31) {
		this.f31 = f31;
	}
	public boolean isF32() {
		return f32;
	}
	public void setF32(boolean f32) {
		this.f32 = f32;
	}
	public boolean isF33() {
		return f33;
	}
	public void setF33(boolean f33) {
		this.f33 = f33;
	}
	public boolean isF34() {
		return f34;
	}
	public void setF34(boolean f34) {
		this.f34 = f34;
	}
	public boolean isF35() {
		return f35;
	}
	public void setF35(boolean f35) {
		this.f35 = f35;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
		
	public Map getReportMap() {
		return reportMap;
	}

	public void setReportMap(Map reportMap) {
		this.reportMap = reportMap;
	}

	public String toString() 
	{
		return super.toString();
	}
	
}
