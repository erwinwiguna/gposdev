package com.ssti.enterprise.pos.tools.purchase;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;

import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.om.PurchaseReturnDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseReturnPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.ReturnReasonTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryCostingTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.journal.PurchaseReturnJournalTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for PurchaseReturn and PurchaseReturnDetail OM
 * @see PurchaseInvoice, PurchaseReceipt, PurchaseReturn, PurchaseReturnDetail 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseReturnTool.java,v 1.25 2009/05/04 02:04:54 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseReturnTool.java,v $
 * 
 * 2015-12-10
 * - change method saveData, use new InventoryCostingTool class for creating inventory transaction journal
 * - change method cancelTrans, use new InventoryCostingTool class for deleting inventory transaction journal  
 *
 * </pre><br>
 */
public class PurchaseReturnTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( PurchaseReturnTool.class );

	public static final String s_ITEM = new StringBuilder(PurchaseReturnPeer.PURCHASE_RETURN_ID).append(",")
	.append(PurchaseReturnDetailPeer.PURCHASE_RETURN_ID).append(",")
	.append(PurchaseReturnDetailPeer.ITEM_ID).toString();
	
	public static final String s_TAX = new StringBuilder(PurchaseReturnPeer.PURCHASE_RETURN_ID).append(",")
	.append(PurchaseReturnDetailPeer.PURCHASE_RETURN_ID).append(",")
	.append(PurchaseReturnDetailPeer.TAX_ID).toString();	
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), PurchaseReturnPeer.RETURN_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), PurchaseReturnPeer.REMARK);      
		m_FIND_PEER.put (Integer.valueOf(i_VENDOR     ), PurchaseReturnPeer.VENDOR_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), PurchaseReturnPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), PurchaseReturnPeer.USER_NAME);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), PurchaseReturnPeer.TRANSACTION_NO);             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), PurchaseReturnPeer.LOCATION_ID);             
		m_FIND_PEER.put (Integer.valueOf(i_ISSUER     ), PurchaseReturnPeer.VENDOR_TRANSACTION_NO); 
		m_FIND_PEER.put (Integer.valueOf(i_ITEM_CODE  ), s_ITEM);
		m_FIND_PEER.put (Integer.valueOf(i_ITEM_NAME  ), s_ITEM);
		m_FIND_PEER.put (Integer.valueOf(i_ITEM_DESC  ), s_ITEM);
		m_FIND_PEER.put (Integer.valueOf(i_KATEGORI   ), s_ITEM); 
        m_FIND_PEER.put (Integer.valueOf(i_PREF_VEND  ), s_ITEM);		
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), PurchaseReturnPeer.CURRENCY_ID);
	}   

	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), PurchaseReturnPeer.RETURN_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), PurchaseReturnPeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), PurchaseReturnPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), PurchaseReturnPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), PurchaseReturnPeer.USER_NAME);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), PurchaseReturnPeer.USER_NAME);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), "");
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), PurchaseReturnPeer.CURRENCY_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), PurchaseReturnPeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), PurchaseReturnPeer.LOCATION_ID); 
	}
    
    public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, true, _iSelected);}
	
	public static PurchaseReturn getHeaderByID(String _sID) 
    	throws Exception
    {
		return getHeaderByID(_sID, null);
	}

	/**
	 * @param _sID
	 * @param _oConn
	 * @return PurchaseReturn object
	 * @throws Exception
	 */
	public static PurchaseReturn getHeaderByID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseReturnPeer.PURCHASE_RETURN_ID, _sID);
	    List vData = PurchaseReturnPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) {
			return (PurchaseReturn) vData.get(0);
		}
	    return null;
	}	

	public static List getDetailsByID(String _sID) 
    	throws Exception
    {
		return getDetailsByID(_sID, null);
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of PurchaseReturnDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseReturnDetailPeer.PURCHASE_RETURN_ID, _sID);
		return PurchaseReturnDetailPeer.doSelect(oCrit, _oConn);
	}

	/**
	 * delete details by ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void deleteDetailsByID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReturnDetailPeer.PURCHASE_RETURN_ID, _sID);
        PurchaseReturnDetailPeer.doDelete(oCrit, _oConn);
	}		
	
	public static List getPurchaseReturnByTransID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReturnPeer.TRANSACTION_ID, _sID);
        oCrit.add(PurchaseReturnPeer.STATUS, i_TRANS_PROCESSED);
        return PurchaseReturnPeer.doSelect(oCrit, _oConn);
	}

	public static boolean isReturned (String _sID, Connection _oConn)
		throws Exception
	{
		if (PurchaseReturnTool.getPurchaseReturnByTransID(_sID, _oConn).size() > 0)	
		{
			return true;
		}	
		return false;
	}		
	    
	///////////////////////////////////////////////////////////////////////////
	// IMPORT METHODS
	///////////////////////////////////////////////////////////////////////////
        
    /**
     * Method to mapping purchase return from purchase receipt
     * @param _oPR	Purchase Receipt Object
     * @param _oPRet	Purchase Return Object
     * @throws Exception
     */
    public static void mapPRToPRet (PurchaseReceipt _oPR, PurchaseReturn _oPRet)
		throws Exception
	{
		_oPRet.setTransactionType		(i_RET_FROM_PR_DO);	
		_oPRet.setTransactionId			(_oPR.getPurchaseReceiptId	());
	    _oPRet.setLocationId			(_oPR.getLocationId   		());
	    _oPRet.setVendorTransactionNo   (_oPR.getVendorDeliveryNo	());

	    _oPRet.setTransactionNo			(_oPR.getReceiptNo			());
	    _oPRet.setTransactionDate		(_oPR.getReceiptDate		());
	    _oPRet.setVendorId				(_oPR.getVendorId			());
	    _oPRet.setVendorName			(_oPR.getVendorName			());
	    _oPRet.setPurchaseDiscount		(_oPR.getTotalDiscountPct	());
	    _oPRet.setCurrencyId			(_oPR.getCurrencyId			());
	    _oPRet.setCurrencyRate			(_oPR.getCurrencyRate		());
	}
    
    /**
     * Method to mapping purchase return detail from purchase receipt detail
     * @param _oPRDet Purchase Receipt Detail Object
     * @param _oPRetDet Purchase Return Detail Object
     * @throws Exception
     */
    public static void mapPRDetailToPRetDetail ( PurchaseReceiptDetail _oPRDet, 
    											 PurchaseReturn _oPR,
    											 PurchaseReturnDetail _oPRetDet )
		throws Exception
	{
    	double dRate = _oPR.getCurrencyRate().doubleValue();
    	double dCostPerUnit = _oPRDet.getCostPerUnit().doubleValue();
    	dCostPerUnit = dCostPerUnit / dRate; //to support multi currency purchase return process
    	
    	_oPRetDet.setTransactionDetailId	(_oPRDet.getPurchaseReceiptDetailId());
    	_oPRetDet.setItemId					(_oPRDet.getItemId());
    	_oPRetDet.setItemCode				(_oPRDet.getItemCode());
    	_oPRetDet.setItemName				(_oPRDet.getItemName());
    	_oPRetDet.setUnitId					(_oPRDet.getUnitId());
    	_oPRetDet.setUnitCode				(_oPRDet.getUnitCode());
    	_oPRetDet.setQty					(bd_ZERO);
    	_oPRetDet.setQtyBase				(bd_ZERO);
    	
    	//use cost per unit as price
    	log.debug("Cost Per Unit : " + dCostPerUnit);
    	
    	_oPRetDet.setItemPrice				(_oPRDet.getItemPriceInPo());
    	_oPRetDet.setItemCost				(new BigDecimal (dCostPerUnit));
    	_oPRetDet.setTaxId					(_oPRDet.getTaxId());
    	_oPRetDet.setTaxAmount			    (_oPRDet.getTaxAmount());
    	_oPRetDet.setDepartmentId			(_oPRDet.getDepartmentId());
    	_oPRetDet.setProjectId				(_oPRDet.getProjectId());	
	}
    
    /**Method to mapping purchase return from purchase invoice
     * @param _oPI Purchase Invoice Object
     * @param _oPRet Purchase Return Object
     * @throws Exception
     */
    public static void mapPIToPRet (PurchaseInvoice _oPI, PurchaseReturn _oPRet)
		throws Exception
	{
		_oPRet.setTransactionType		(i_RET_FROM_PR_DO);
		_oPRet.setTransactionId			(_oPI.getPurchaseInvoiceId	());
	    _oPRet.setLocationId			(_oPI.getLocationId   		());
	    _oPRet.setVendorTransactionNo   (_oPI.getVendorInvoiceNo	());
	    _oPRet.setTransactionNo			(_oPI.getPurchaseInvoiceNo	());
	    _oPRet.setTransactionDate		(_oPI.getPurchaseInvoiceDate());
	    _oPRet.setReturnDate			(new Date());

	    _oPRet.setVendorId				(_oPI.getVendorId			());
	    _oPRet.setVendorName			(_oPI.getVendorName			());
	    _oPRet.setPurchaseDiscount		(_oPI.getTotalDiscountPct	());
	    _oPRet.setCurrencyId			(_oPI.getCurrencyId			());
	    _oPRet.setCurrencyRate			(_oPI.getCurrencyRate		());
	}
    
    /**
     * Method to mapping purchase return detail from purchase invoice detail
     * @param _oPIDet Purchase Invoice Detail Object
     * @param _oPRetDet Purchase Receipt Detail Object
     * @throws Exception
     */
    public static void mapPIDetailToPRetDetail ( PurchaseInvoiceDetail _oPIDet, 
    											 PurchaseReturn _oPR, 
												 PurchaseReturnDetail _oPRetDet )
	throws Exception
	{
    	double dRate = _oPR.getCurrencyRate().doubleValue();
    	double dCostPerUnit = _oPIDet.getCostPerUnit().doubleValue();
    	dCostPerUnit = dCostPerUnit / dRate; //to support multi currency purchase return process
    	
    	_oPRetDet.setTransactionDetailId	(_oPIDet.getPurchaseInvoiceDetailId());
    	_oPRetDet.setItemId					(_oPIDet.getItemId());
    	_oPRetDet.setItemCode				(_oPIDet.getItemCode());
    	_oPRetDet.setItemName				(_oPIDet.getItemName());
    	_oPRetDet.setUnitId					(_oPIDet.getUnitId());
    	_oPRetDet.setUnitCode				(_oPIDet.getUnitCode());
    	_oPRetDet.setQty					(bd_ZERO);
    	_oPRetDet.setQtyBase				(bd_ZERO);

    	//TODO: check again use cost per unit as price    	
    	_oPRetDet.setItemPrice				(_oPIDet.getItemPrice());
    	_oPRetDet.setItemCost				(new BigDecimal (dCostPerUnit));
    	_oPRetDet.setTaxId					(_oPIDet.getTaxId());
    	_oPRetDet.setTaxAmount			    (_oPIDet.getTaxAmount());
    	_oPRetDet.setDepartmentId			(_oPIDet.getDepartmentId());
    	_oPRetDet.setProjectId				(_oPIDet.getProjectId());    	
	}	

	///////////////////////////////////////////////////////////////////////////
	// TRANS PROCESSING METHODS
	///////////////////////////////////////////////////////////////////////////
    
	public static double countQty (List _vDetails)
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseReturnDetail oDet = (PurchaseReturnDetail) _vDetails.get(i);
			dQty += oDet.getQty().doubleValue();
		}
		return dQty;
	}
	
   	public static double countTotalAmount (List _vDetails)
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseReturnDetail oDet = (PurchaseReturnDetail) _vDetails.get(i);
			dAmt += oDet.getReturnAmount().doubleValue();
		}
		return dAmt;
	}

   	public static BigDecimal countTotalCost (List _vDetails)
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseReturnDetail oDet = (PurchaseReturnDetail) _vDetails.get(i);
			dAmt += oDet.getSubTotalCost().doubleValue();
		}
		return new BigDecimal(dAmt);
	}   	
    
    /**
     * 
     * @param _sDiscountPct
     * @param _vDetails
     * @param _oTR
     * @return total tax
     */
    public static BigDecimal countTax (String _sDiscountPct, List _vDetails, PurchaseReturn _oTR)
    {
        double dTotalAmount = _oTR.getTotalAmount().doubleValue();
        double dAmt = 0;
        for (int i = 0; i < _vDetails.size(); i++)
        {
            PurchaseReturnDetail oDet = (PurchaseReturnDetail) _vDetails.get(i);
            double dSubTotal = oDet.getReturnAmount().doubleValue();
            double dDisc = Calculator.calculateDiscount(_sDiscountPct, dSubTotal);
            
            // if total amount
            if(!_sDiscountPct.contains(Calculator.s_PCT))
            {
                if (dTotalAmount > 0) //if dTotalAmount > 0, prevent divide by zero
                {
                    dSubTotal = dSubTotal - (dDisc * (dSubTotal / dTotalAmount));
                }
            }
            else
            {
                dSubTotal = dSubTotal - dDisc;
            }
            
            
            double dSubTotalTax = 0;
            if(!_oTR.getIsInclusiveTax())
            {
                dSubTotalTax = dSubTotal * oDet.getTaxAmount().doubleValue() / 100;
            }
            else
            {
                //calculate inclusive Tax
                dSubTotalTax = (dSubTotal * 100) / (oDet.getTaxAmount().doubleValue() + 100);
                dSubTotalTax = dSubTotal - dSubTotalTax;  
            }

            oDet.setSubTotalTax(new BigDecimal(dSubTotalTax));
            dAmt += oDet.getSubTotalTax().doubleValue();
        }
        return new BigDecimal(dAmt).setScale(2, RoundingMode.HALF_UP);
    }

   	public static double countTotalTax (List _vDetails)
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseReturnDetail oDet = (PurchaseReturnDetail) _vDetails.get(i);
			dAmt += oDet.getSubTotalTax().doubleValue();
		}
		return dAmt;
	}   	   	
   	
   	/**
   	 * set return with properties from RunData
   	 * @param _oPR
   	 * @param _vPRD
   	 * @param data
   	 * @throws Exception
   	 */
    public static void setHeaderProperties (PurchaseReturn _oPR, List _vPRD, RunData data)
    	throws Exception
    {
		try
		{
			if (data != null)
			{
				data.getParameters().setProperties (_oPR);			
				_oPR.setReturnDate(CustomParser.parseDate(data.getParameters().getString("ReturnDate")));
				if(data.getParameters().getInt("ReturnFrom") == i_RET_FROM_NONE) 
				{
					_oPR.setTransactionDate(_oPR.getReturnDate());
				}
			}
			
			if (StringUtil.isEmpty(_oPR.getVendorName()))
			{
				_oPR.setVendorName(VendorTool.getVendorNameByID(_oPR.getVendorId()));
			}

			//total return amount including tax
			_oPR.setTotalQty   	  (new BigDecimal(countQty(_vPRD)));
            
            double dTotalAmount = countTotalAmount(_vPRD);            
            double dInvoiceDiscount =  Calculator.calculateDiscount(_oPR.getPurchaseDiscount(), dTotalAmount);                              
            double dTotalDiscount =  dInvoiceDiscount;     
            double dTotalTax = countTax(_oPR.getPurchaseDiscount(), _vPRD, _oPR).doubleValue(); 
            double dReturnedAmount = dTotalAmount - dTotalDiscount;
            //count returned amount with tax
            if(!_oPR.getIsInclusiveTax())
            {
                dReturnedAmount = dReturnedAmount + dTotalTax;
            }
            _oPR.setTotalDiscount(new BigDecimal(dInvoiceDiscount));
            _oPR.setTotalTax     (new BigDecimal(dTotalTax));                    
            _oPR.setTotalAmount  (new BigDecimal(dReturnedAmount));                   
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			_oEx.printStackTrace();
			throw new NestableException ("Set Return Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
    /**
     * update return detail list 
     * @param _vPRD
     * @param data
     * @throws Exception
     */
    public static void updateDetail (List _vPRD, RunData data)
    	throws Exception
    {
    	ParameterParser oParam = data.getParameters();
		for ( int i = 0; i < _vPRD.size(); i++ ) 
		{
			PurchaseReturnDetail oPRD = (PurchaseReturnDetail) _vPRD.get(i);				

			int j = i + 1; //no index in template
			double dQty = oParam.getDouble("Qty" + j);
			double dPrice = oParam.getDouble("ItemPrice" + j);
			double dTax = oParam.getDouble("TaxAmount" + j);
			
			/*
			oPRD.setTransactionDetailId (oParam.getString ("TransactionDetailId" + j));
			oPRD.setItemId   ( oParam.getString ("ItemId"   + j) );		 
			oPRD.setItemCode ( oParam.getString ("ItemCode" + j) );		 
			oPRD.setItemName ( oParam.getString ("ItemName" + j) );		 
			oPRD.setUnitId   ( oParam.getString ("UnitId"   + j) );		 
			oPRD.setUnitCode ( oParam.getString ("UnitCode" + j) );
			*/
			
			oPRD.setQty		 ( new BigDecimal (dQty)  ); 
			oPRD.setTaxId	 ( data.getParameters().getString("TaxId" + j));

			double dUnitConv = UnitTool.getBaseValue(oPRD.getItemId(),oPRD.getUnitId());
	        double dQtyBase = dQty * dUnitConv;
	        double dCost = dPrice / dUnitConv;
       		double dSubTotal = dPrice * dQty;
       		double dSubTotalCost = dCost * dQty;
			double dSubTotalTax = dTax / 100 * dSubTotal;
				
       		oPRD.setQtyBase			(new BigDecimal(dQtyBase));
       		oPRD.setItemPrice     	( new BigDecimal (dPrice));
			oPRD.setItemCost      	( new BigDecimal (dCost));
			oPRD.setReturnAmount  	( new BigDecimal (dSubTotal));
			oPRD.setTaxAmount     	( new BigDecimal (dTax));
			oPRD.setSubTotalTax		( new BigDecimal (dSubTotalTax));
			oPRD.setSubTotalCost	( new BigDecimal (dSubTotalCost));		
			
			log.debug(oPRD);
		}
	}
	
    /**
     * count total item which already returned 
     * 
     * @param _vReturn
     * @param _sItemID
     * @return sum of total returned 
     * @throws Exception
     */
    protected static double countTotalReturn (List _vReturn, String _sItemID) 
    	throws Exception
    {
		List vID = new ArrayList (_vReturn.size());
		for (int i = 0; i < _vReturn.size(); i++)
		{
			PurchaseReturn oReturn = (PurchaseReturn) _vReturn.get(i);
			vID.add (oReturn.getPurchaseReturnId());
		}
		Criteria oCrit = new Criteria();
		oCrit.addIn (PurchaseReturnDetailPeer.PURCHASE_RETURN_ID, vID);
		oCrit.add (PurchaseReturnDetailPeer.ITEM_ID, _sItemID);
		List vDetail = PurchaseReturnDetailPeer.doSelect (oCrit);
		double dTotalQty = 0;
		for (int i = 0; i < vDetail.size(); i++)
		{
			PurchaseReturnDetail oDet = (PurchaseReturnDetail) vDetail.get(i);
			dTotalQty += oDet.getQty().doubleValue();
		}
        return dTotalQty;
	}
    
	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_TRANS_PENDING)   return LocaleTool.getString("pending");
		if (_iStatusID == i_TRANS_PROCESSED) return LocaleTool.getString("processed");
		if (_iStatusID == i_TRANS_CANCELLED) return LocaleTool.getString("cancelled");
		return LocaleTool.getString("new");
	}		


	/**
	 * 
	 * @param _oPR
	 * @param _oConn
	 * @throws Exception
	 */
	private static void validateReturnedTrans (PurchaseReturn _oPR, List _oPRF, Connection _oConn)
		throws Exception
	{
		//check whether PR / SI status is processed
		//this is needed so user cannot pending a sales return, cancel DO/SI then back to processed this
		if(_oPR.getTransactionType() == i_RET_FROM_PR_DO)
		{
			PurchaseReceipt oTR = PurchaseReceiptTool.getHeaderByID(_oPR.getTransactionId(), _oConn);
			if (oTR == null || oTR.getStatus() != i_TRANS_PROCESSED)
			{
				throw new NestableException (
					"Receipt status is not valid for return (" + 
					PurchaseReceiptTool.getStatusString(oTR.getStatus()) + ")"
				);
			}		
		}
		
		if(_oPR.getTransactionType() == i_RET_FROM_PI_SI)
		{	
			PurchaseInvoice oTR = PurchaseInvoiceTool.getHeaderByID(_oPR.getTransactionId(), _oConn);
			if (oTR == null || oTR.getStatus() != i_TRANS_PROCESSED)
			{
				throw new NestableException (
					"PI status is not valid for return (" + 
					PurchaseInvoiceTool.getStatusString(oTR.getStatus()) + ")"
				);
			}		
		}			
		validateReturnedQty (_oPR, _oPRF, _oConn);
	}
	
	/**
	 * validate returned qty so will not exceed qty in base trans
	 * 
	 * @param _oPR
	 * @param _vPRD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void validateReturnedQty (PurchaseReturn _oPR, List _vPRD, Connection _oConn)
		throws Exception
	{
		int iType = _oPR.getTransactionType();
		if (iType == i_RET_FROM_PR_DO || iType == i_RET_FROM_PI_SI)
		{
			for (int i = 0; i < _vPRD.size(); i++)
			{
				PurchaseReturnDetail oDet = (PurchaseReturnDetail) _vPRD.get(i);
				double dTransQty = 0;
				double dCurrentReturn = oDet.getQty().doubleValue();
				double dReturnedQty = 0;
				if(iType == i_RET_FROM_PR_DO)
				{
					PurchaseReceiptDetail oTD = 
						PurchaseReceiptTool.getDetailByDetailID(oDet.getTransactionDetailId(), _oConn);
					dTransQty = oTD.getQtyBase().doubleValue();
					dReturnedQty = oTD.getReturnedQty().doubleValue();
				}
				if(iType == i_RET_FROM_PI_SI)
				{
					PurchaseInvoiceDetail oTD = 
						PurchaseInvoiceTool.getDetailByDetailID(oDet.getTransactionDetailId(), _oConn);
					dTransQty = oTD.getQtyBase().doubleValue();
					dReturnedQty = oTD.getReturnedQty().doubleValue();
				}
				if (iType != i_RET_FROM_NONE)
				{
					if ((dCurrentReturn + dReturnedQty) > dTransQty)
					{
						throw new NestableException (
							" Item " + oDet.getItemCode() + " already returned " + 
							CustomFormatter.formatNumber(Double.valueOf(dReturnedQty)) + ", trans qty is " + 
							CustomFormatter.formatNumber(Double.valueOf(dTransQty))
						);
					}
				}	
			}
		}			
	}	
	
	/**
	 * save PurchaseReturn and Detail
	 * 
	 * @param _oPR
	 * @param _vTD
	 * @param data
	 * @throws Exception
	 */
	public static void saveData (PurchaseReturn _oPR, List _vTD) 
		throws Exception 
	{	
		Connection oConn = beginTrans ();
		validate(oConn);
		try 
		{
			//check for back date
			//_oPR.setTransactionDate(checkBackDate(_oPR.getTransactionDate(), i_INV_OUT));
			
			//validate date
			validateDate(_oPR.getReturnDate(), oConn);

			//check for back date
			_oPR.setReturnDate(checkBackDate(_oPR.getReturnDate(), i_INV_OUT));
			
			//validate purchase return, trans status & qty
			validateReturnedTrans (_oPR, _vTD, oConn);

			//process save purchase return Data
			processSavePurchaseReturnData (_oPR, _vTD, oConn);
			
			if (_oPR.getStatus() == i_TRANS_PROCESSED)
			{
				//create inventory transaction and update inventory location
				InventoryCostingTool oCT = new InventoryCostingTool(oConn);
				oCT.createFromPT(_oPR, _vTD);
				
				//if from receipt then update returned qty value in purchase receipt detail
				if(_oPR.getTransactionType() == i_RET_FROM_PR_DO) 
				{
					PurchaseReceiptTool.updateReturnQty(_oPR.getTransactionId(), _vTD, false, oConn);
				}
				
				//if from invoice then update returned qty value in purchase invoice detail
				if(_oPR.getTransactionType() == i_RET_FROM_PI_SI)
				{	
					PurchaseInvoiceTool.updateReturnQty(_oPR.getTransactionId(),_vTD, false, oConn);
				}
				
				//create debit memo so we can cross this return in any payable payment
				if ((_oPR.getTransactionType() == i_RET_FROM_PI_SI || 
					 _oPR.getTransactionType()== i_RET_FROM_NONE) && 
					 _oPR.getCreateDebitMemo()) 
				{
					DebitMemoTool.createFromPurchaseReturn (_oPR, oConn);
				}
				
				//create GL journal
				if (b_USE_GL)
				{
					PurchaseReturnJournalTool oJournal = new PurchaseReturnJournalTool (_oPR, oConn);
					oJournal.createPRJournal(_oPR, _vTD);
				}
			}
			commit (oConn);
		}
		catch (Exception _oEx) 
		{                 
			//restore object state to unsaved
			_oPR.setReturnNo("");
			_oPR.setStatus(i_TRANS_PENDING);		
			
			rollback (oConn);
			
			log.error ( _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}		
	
	/**
	 * process save purchase return
	 * 
	 * @param _oPR
	 * @param _vPRD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSavePurchaseReturnData (PurchaseReturn _oPR, 
													   List _vPRD, 
													   Connection _oConn) 
		throws Exception
	{
		try 
		{
			if (StringUtil.isEmpty(_oPR.getPurchaseReturnId()))
			{
				_oPR.setPurchaseReturnId (IDGenerator.generateSysID());
				_oPR.setNew(true);
			}
			
			validateID(getHeaderByID(_oPR.getPurchaseReturnId(), _oConn), "returnNo");
			
			if (_oPR.getReturnNo() == null) _oPR.setReturnNo("");
			if (StringUtil.isEmpty(_oPR.getReturnNo()) && _oPR.getStatus() == i_TRANS_PROCESSED) 
			{
				String sLocCode = LocationTool.getLocationCodeByID(_oPR.getLocationId(), _oConn);
				_oPR.setReturnNo (
		           LastNumberTool.generateByVendor(_oPR.getVendorId(),_oPR.getLocationId(),sLocCode,s_PT_FORMAT,LastNumberTool.i_PURCHASE_RETURN, _oConn)	
				);
				validateNo(PurchaseReturnPeer.RETURN_NO, _oPR.getReturnNo(), PurchaseReturnPeer.class, _oConn);
			}
			_oPR.save (_oConn);
			deleteDetailsByID(_oPR.getPurchaseReturnId(), _oConn);
			Iterator oIter = _vPRD.iterator();
			while (oIter.hasNext())
			{
				PurchaseReturnDetail oPRD = (PurchaseReturnDetail) oIter.next();
				oPRD.setNew (true);
				oPRD.setModified (true);
				
				if (oPRD.getQty().doubleValue() > 0)
				{
					if (_oPR.getStatus() == i_PROCESSED)
					{
						//validate serial no
						ItemSerialTool.validateSerial(oPRD.getItemId(), oPRD.getQty().doubleValue(), oPRD.getSerialTrans(), _oConn);
					}
					
					//Generate Sys ID
					oPRD.setPurchaseReturnDetailId ( IDGenerator.generateSysID() );	
					oPRD.setPurchaseReturnId (_oPR.getPurchaseReturnId());	
					oPRD.save(_oConn);
				}				
				else
				{
					if (_oPR.getStatus() == i_PROCESSED) oIter.remove();			
				}
			}
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			String sError = "Process Save Purchase Return Data : " + _oEx.getMessage(); 
			log.error (sError, _oEx);
			throw new NestableException (sError, _oEx);
		}
	}

	/**
	 * cancel PurchaseReturn 	 
	 *  
	 * @param _oPT
	 * @param _vTD
	 * @param data
	 * @throws Exception
	 */
	public static void cancelReturn (PurchaseReturn _oPT, List _vTD, String _sCancelBy) 
		throws Exception 
	{	
		Connection oConn = beginTrans ();
		try 
		{			
			//validate date
			validateDate(_oPT.getReturnDate(), oConn);
			
			if (_oPT.getStatus() == i_TRANS_PROCESSED)
			{
				boolean bMemoUsed = DebitMemoTool.isMemoUsed(_oPT.getPurchaseReturnId(), oConn);
				if (!bMemoUsed || (bMemoUsed && _oPT.getCashReturn()) )
				{
					//create inventory transaction
					InventoryCostingTool oCT = new InventoryCostingTool(oConn);
					oCT.delete(_oPT, _oPT.getPurchaseReturnId(), i_INV_TRANS_PURCHASE_RETURN); 						
					
					//if from receipt then update returned qty value in purchase receipt detail
					if(_oPT.getTransactionType() == i_RET_FROM_PR_DO) 
					{
						PurchaseReceiptTool.updateReturnQty(_oPT.getTransactionId(),  _vTD, true, oConn);
					}
					
					//if from invoice then update returned qty value in purchase invoice detail
					if(_oPT.getTransactionType() == i_RET_FROM_PI_SI)
					{	
						PurchaseInvoiceTool.updateReturnQty(_oPT.getTransactionId(),_vTD, true, oConn);
					}
					
					//cancel DM
					if (_oPT.getCreateDebitMemo()) 
					{
						DebitMemoTool.cancelFromPurchaseReturn(_oPT, _sCancelBy, oConn);
					}
					
					//create GL journal
					if (b_USE_GL)
					{
						PurchaseReturnJournalTool.deleteJournal
							(GlAttributes.i_GL_TRANS_PURCHASE_RETURN, _oPT.getPurchaseReturnId(), oConn);
					}
				}
				else
				{
					throw new NestableException (LocaleTool.getString("return_memo_used"));
				}
			}
		    _oPT.setRemark(cancelledBy(_oPT.getRemark(), _sCancelBy));
			_oPT.setStatus(i_TRANS_CANCELLED);
			_oPT.save(oConn);
			commit (oConn);
		}
		catch (Exception _oEx) 
		{                 
			rollback (oConn);
			log.error ( _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}	

	public static LargeSelect findData (int _iCond,
										String _sKeywords,
										Date _dStartReturn,
										Date _dEndReturn, 
										String _sVendorID,
										String _sLocationID,
										int _iStatus,
										int _iLimit,
										String _sCurrencyID)
		throws Exception
	{
		return findData(_iCond, _sKeywords, _dStartReturn, _dEndReturn, _sVendorID, _sLocationID, _iStatus, _iLimit, _sCurrencyID,"");
	}

	/**
	 * 
	 * @param _sTransNo
	 * @param _dTransactionDate
	 * @param _sReturnNo
	 * @param _dReturnDate
	 * @param _sVendorID
	 * @param _iLimit
	 * @return query result in LargeSelect
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond,
										String _sKeywords,
										Date _dStartReturn,
								  		Date _dEndReturn, 
										String _sVendorID,
										String _sLocationID,
										int _iStatus,
										int _iLimit,
										String _sCurrencyID,
										String _sReasonID) 
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords,
			PurchaseReturnPeer.RETURN_DATE, _dStartReturn, _dEndReturn, _sCurrencyID
		);

		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseReturnPeer.VENDOR_ID, _sVendorID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(PurchaseReturnPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sReasonID)) 
		{
			oCrit.add(PurchaseReturnPeer.RETURN_REASON_ID, _sReasonID);	
		}		
		if (_iStatus > 0) oCrit.add(PurchaseReturnPeer.STATUS, _iStatus);
		
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.PurchaseReturnPeer");
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods                                                                     
	/////////////////////////////////////////////////////////////////////////////////////

	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sVendorID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sCreateBy,
								  String _sCurrencyID) 
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				PurchaseReturnPeer.RETURN_DATE, _dStart, _dEnd, _sCurrencyID);   
		
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseReturnPeer.VENDOR_ID, _sVendorID);	
		}
		if(StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(PurchaseReturnPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sCreateBy))
		{
			oCrit.add(PurchaseReturnPeer.USER_NAME, _sCreateBy);
		}        
		if(_iStatus > 0)
		{
			oCrit.add(PurchaseReturnPeer.STATUS, _iStatus);
		} 
		log.debug(oCrit);
		return PurchaseReturnPeer.doSelect(oCrit);
	}
	
	//report method to group data by item
	public static PurchaseReturn filterTransByTransID (List _vTrans, String _sTransID) 
    	throws Exception
    { 

		PurchaseReturn oTrans = null;
		for (int i = 0; i < _vTrans.size(); i++) {
			oTrans = (PurchaseReturn) _vTrans.get(i);			
			if (oTrans.getPurchaseReturnId().equals(_sTransID)) {
				return oTrans;
			}
		}	
		return oTrans;
	}		
	
	public static List getTransDetails (List _vTrans) 
    	throws Exception
    { 
        return getTransDetails(_vTrans,"");
    }
	
	public static List getTransDetails (List _vTrans, String _sItemName)
    	throws Exception
    { 
		List vTransID = new ArrayList (_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++)
		 {
			PurchaseReturn oTrans = (PurchaseReturn) _vTrans.get(i);			
			vTransID.add (oTrans.getPurchaseReturnId());
		}		
		Criteria oCrit = new Criteria();
		if(_sItemName != null && !_sItemName.equals("") )
		{
		    oCrit.add(PurchaseReturnDetailPeer.ITEM_NAME,
		        (Object) SqlUtil.like (PurchaseReturnDetailPeer.ITEM_NAME, _sItemName,false,true), 
					Criteria.CUSTOM);
		}
        if(vTransID.size() > 0)
        {
		    oCrit.addIn (PurchaseReturnDetailPeer.PURCHASE_RETURN_ID, vTransID);
		}
		return PurchaseReturnDetailPeer.doSelect(oCrit);
	}		
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods                                                                     
	/////////////////////////////////////////////////////////////////////////////////////

	//-------------------------------------------------------------------------
	//return reason methods
	//------------------------------------------------------------------------- 
	public static ReturnReasonTool getReturnReasonTool()
	{
		return ReturnReasonTool.getInstance();
	}

	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
		
	/**
	 * get List of PurchaseReturn created since last store 2 ho
	 * 
	 * @return List of PurchaseReturn created since last store 2 ho
	 * @throws Exception
	 */
	public static List getStorePurchaseReturn ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(PurchaseReturnPeer.RETURN_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
        oCrit.add(PurchaseReturnPeer.LOCATION_ID, PreferenceTool.getLocationID());
    	oCrit.add(PurchaseReturnPeer.STATUS, i_PROCESSED);
		return PurchaseReturnPeer.doSelect(oCrit);
	}
	
	/**
	 * get all PT confirmed since last ho 2 store
	 * Store will not sent PT back so PT goes one way from ho 2 store	 
	 * 
	 * @return List of PurchaseReturn
	 * @throws Exception
	 */
	public static List getForStore ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(PurchaseReturnPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
        	oCrit.add(PurchaseReturnPeer.STATUS, i_PROCESSED);
		}
		log.debug("get PT FOR STORE : " + oCrit);
		return PurchaseReturnPeer.doSelect(oCrit);
	}
    	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PurchaseReturnPeer.class, PurchaseReturnPeer.PURCHASE_RETURN_ID, _sID, _bIsNext);
	}	
	//end next prev

}