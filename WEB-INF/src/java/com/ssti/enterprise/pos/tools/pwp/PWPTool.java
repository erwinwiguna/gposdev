package com.ssti.enterprise.pos.tools.pwp;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

import com.ssti.enterprise.pos.manager.DiscountManager;
import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.Pwp;
import com.ssti.enterprise.pos.om.PwpBuy;
import com.ssti.enterprise.pos.om.PwpBuyPeer;
import com.ssti.enterprise.pos.om.PwpGet;
import com.ssti.enterprise.pos.om.PwpGetPeer;
import com.ssti.enterprise.pos.om.PwpPeer;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for Pwp OM
 * @see Pwp, PwpPeer, PwpDetail, PwpDetailPeer
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PWPTool.java,v 1.1 2009/05/04 02:05:00 albert Exp $ <br>
 *
 * <pre>
 * $Log: PWPTool.java,v $
 * 
 * 2017-02-16
 * - fix method updateGetDiscAmount add all existing details to temporary list to check
 *   valid buy for multiple items term
 * - fix method updateGetDiscAmount, fix bug in get Qty discount amount. Previously if discount qty 
 *   is always getQty * validQty even though transDetail Qty less than that   
 * 
 * 2016-09-21
 * - @see {@link DefaultSalesTransaction#}
 * - total change of PWP logic
 * - deprecate method getPWPPrice, updatePWPGet, getTotalSetQty 
 * - add method updateGetDiscAmount for new PWP logic
 * 
 * 2016-08-01
 * - Add update function to update PWP Get items discount for case that Get items scanned before Buy Items 
 * 
 * </pre><br>
 */
public class PWPTool extends BaseTool
{
	public static final int i_PWP_BUY = 1;
	public static final int i_PWP_GET = 2;
	
	private static Log log = LogFactory.getLog(PWPTool.class);

	private static PWPTool instance = null;
	
	public static synchronized PWPTool getInstance()
	{
		if (instance == null) instance = new PWPTool();
		return instance;
	}
	
	public static Pwp getByID(String _sID)
		throws Exception
	{
		return getByID (_sID, null);
	}
	
	/**
	 * get data by id
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return Pwp object
	 * @throws Exception
	 */
	public static Pwp getByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PwpPeer.PWP_ID, _sID);
	    List vData = PwpPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (Pwp) vData.get(0);
		}
		return null;
	}
	
	public static List getBuyByID(String _sID, Connection _oConn)
		throws Exception
	{
	    return DiscountManager.getInstance().getBuyByID(_sID,_oConn);		
	}

	public static List getGetByID(String _sID, Connection _oConn)
		throws Exception
	{
        return DiscountManager.getInstance().getGetByID(_sID,_oConn);       
    }
		
	//deletes
	private static void deleteBuysByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PwpBuyPeer.PWP_ID, _sID);
	    PwpBuyPeer.doDelete(oCrit, _oConn);		
	}
	
    private static void deleteGetsByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PwpGetPeer.PWP_ID, _sID);
	    PwpGetPeer.doDelete(oCrit, _oConn);		
	}
	
	private static void clean(String _sID, Connection _oConn)
		throws Exception
	{
		deleteBuysByID(_sID, _oConn);
		deleteGetsByID(_sID, _oConn);
        DiscountManager.getInstance().refreshCache(_sID);
	}
		
	public static void saveData(Pwp _oPWP, List _vPWPBuy, List _vPWPGet)
		throws Exception
	{
		Connection oConn = null;		
		try 
		{
			oConn = Transaction.begin(s_DB_NAME);			
						
			//save pwp
			if (StringUtil.isEmpty(_oPWP.getPwpId()))
			{
				_oPWP.setPwpId(IDGenerator.generateSysID());
			}
			_oPWP.save(oConn);	
			
			//cleans
			clean(_oPWP.getPwpId(), oConn);			
            StringBuilder sItems = new StringBuilder();
            
			//save buys
			for (int i = 0; i < _vPWPBuy.size(); i++)
			{
				PwpBuy oPWPBuy = (PwpBuy) _vPWPBuy.get(i);
				oPWPBuy.setNew(true);
				oPWPBuy.setPwpBuyId(IDGenerator.generateSysID());
				oPWPBuy.setPwpId(_oPWP.getPwpId());
				oPWPBuy.save(oConn);
                
                if (sItems.length() > 0) sItems.append(",");
                sItems.append(oPWPBuy.getBuyItemCode());
			}

			//save gets
			for (int i = 0; i < _vPWPGet.size(); i++)
			{
				PwpGet oPWPGet = (PwpGet) _vPWPGet.get(i);
				oPWPGet.setNew(true);
				oPWPGet.setPwpGetId(IDGenerator.generateSysID());
				oPWPGet.setPwpId(_oPWP.getPwpId());
				oPWPGet.save(oConn);
                
                if (sItems.length() > 0) sItems.append(",");
                sItems.append(oPWPGet.getGetItemCode());
			}

            DiscountManager.getInstance().refreshCache(_oPWP.getPwpId());
            
            //save discount
            Discount oDisc = DiscountTool.getDiscountByID(_oPWP.getPwpId(),oConn);
            oDisc.setItems(sItems.toString());
            oDisc.save(oConn);

            Transaction.commit(oConn);
		} 
		catch (Exception _oEx) 
		{
			log.error (_oEx);
			_oEx.printStackTrace();
			Transaction.safeRollback(oConn);
			throw new Exception ("Saving PWP failed : " + _oEx.getMessage());
		}
	}
	
	public static void deleteData(Pwp _oPWP)
		throws Exception
	{
		Connection oConn = null;		
		try 
		{
			oConn = Transaction.begin(s_DB_NAME);			

			//delete all details
			clean(_oPWP.getPwpId(), oConn);
	
			Criteria oCrit = new Criteria();
		    oCrit.add(PwpPeer.PWP_ID, _oPWP.getPwpId());
		    PwpPeer.doDelete(oCrit, oConn);	

            DiscountManager.getInstance().refreshCache(_oPWP.getPwpId());

			Transaction.commit(oConn);
		} 
		catch (Exception _oEx) 
		{
			log.error (_oEx);
			_oEx.printStackTrace();
			Transaction.safeRollback(oConn);
			throw new Exception ("Saving PWP failed : " + _oEx.getMessage());
		}
	}
	
	public static List preparePWPData ()
		throws Exception
	{
		//default to ho2store
		return preparePWPData (false, "", AppAttributes.i_HO_TO_STORE);
	}
	
	public static List preparePWPData (boolean _bIsSetup, String _sLocationID, int _iSyncType)
		throws Exception
	{
		List vDisc = null;
		
		//if get the price list for remote store then get all active
		if (_bIsSetup) {
			vDisc = DiscountTool.getLocationActiveDisc ( _sLocationID );
		}
		//if get the price list for ho2store then get active and updated price list only
		else {
			vDisc = DiscountTool.getUpdatedDiscount();
		}	
		
		List vPWP = new ArrayList(); 
		for (int i = 0; i < vDisc.size(); i++)
		{
			Discount oDisc = (Discount) vDisc.get(i);			
			if (oDisc.getDiscountType() == DiscountTool.i_BY_PWP)
			{
				Pwp oPwp = getByID(oDisc.getDiscountId());
				List vBuy = getBuyByID(oDisc.getDiscountId(), null);
				List vGet = getGetByID(oDisc.getDiscountId(), null);
				if(oPwp != null && vBuy != null && vGet != null && vBuy.size() > 0 && vGet.size() > 0)
				{
					oPwp.setPwpBuy(vBuy);
					oPwp.setPwpGet(vGet);
				}
				else
				{
					throw new Exception("PWP Discount No :" + oDisc.getDiscountCode() + " Buy Items/Get Items Invalid ");
				}
				vPWP.add(oPwp);
			}
		}				
		return vPWP;
	}
	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PwpPeer.class, PwpPeer.PWP_ID, _sID, _bIsNext);
	}	   	
    
    //-------------------------------------------------------------------------
	// USAGE
    //-------------------------------------------------------------------------
	public static int validBuy(String _sID, List _vTD)
        throws Exception
    {
	    List vBuy = getBuyByID(_sID,null);
        boolean bAllValid = true;
        int iValidQty = 0;
        for (int i = 0; i < vBuy.size(); i++)
        {
            PwpBuy oBuy = (PwpBuy)vBuy.get(i);
            String sItemID = oBuy.getBuyItemId();
            double dBuyQty = oBuy.getBuyQty().doubleValue();
            boolean bFound = false;
            int iItemValidQty = 0;
            
            //logic to check matching between pwp buy requirements set here
            double dTotalBuy = getTotalQty(sItemID,_vTD, i_PWP_BUY);            
            if (dTotalBuy > 0)
            {
                iItemValidQty = (int)(dTotalBuy / dBuyQty);
                double dLeft = dTotalBuy % dBuyQty;
                if (dTotalBuy == dBuyQty || (dTotalBuy > dBuyQty && dLeft == 0))
                {
                    bFound = true;
                }                
                log.info("Item " + oBuy.getBuyItemCode() + " Valid Qty " + iItemValidQty);
            }
            log.info("Total Valid Qty " + iValidQty + " Item Valid Qty " + iItemValidQty);
            if (iItemValidQty <= iValidQty || i == 0) iValidQty = iItemValidQty;
            if (!bFound) bAllValid = false;
        }
        return iValidQty;        
    }	   
    
	/**
	 * getTotalQty of buy / get items, set pwpType property here
	 * 
	 * @param _sItemID
	 * @param _vTD
	 * @param _iType
	 * @return
	 */
    private static double getTotalQty(String _sItemID,List _vTD, int _iType)
    {
        double dTotalQty = 0;
        for (int j = 0; j < _vTD.size(); j++)
        {
            InventoryDetailOM oTD = (InventoryDetailOM) _vTD.get(j);
            if (StringUtil.isEqual(oTD.getItemId(),_sItemID))
            {
                dTotalQty += oTD.getQty().doubleValue();
                BeanUtil.setProperty(oTD, "pwpType", _iType);
            }                        	
        }
        return dTotalQty;        
    }
        
    /**
     * update Current TD or Existing TD DiscountAmount based on ValidBuy 
     * 
     * @param _oPWP PWP Discount     (Current Active PWP Discount)
     * @param _vTD  List of TransDet (Existing List of TransDet)
     * @param _oTD  Current TransDet (New/Merged Buy Item OR New/Merged Get Item)
     * @throws Exception
     */
    public static void updateGetDiscAmount(Discount _oPWP, List _vTD, InventoryDetailOM _oTD)
        throws Exception
    {

    	boolean bTDIsBuy = false;
    	//check valid buy from existing Item List _vTD if _oTD is GET item
        int iValidQty = PWPTool.validBuy(_oPWP.getDiscountId(), _vTD);
        log.info("ValidQty From Existing Item List vTD: " + iValidQty);
        
        //check valid buy from current _oTD if _oTD is BUY item
    	if (iValidQty == 0) //check valid buy qty from current TD
    	{
    		List vTMP = new ArrayList(1);
    		vTMP.addAll(_vTD); //include existing data in _vTD, needed if pwp buy terms more than 1 item
    		vTMP.add(_oTD);
    		iValidQty = PWPTool.validBuy(_oPWP.getDiscountId(), vTMP);    
    		bTDIsBuy = true;
    		log.info("ValidQty From Current New/Merged TD: " + iValidQty);
    	}
    	//if valid qty Exists, scans _vTD to calculate GET Discount Amount  
        if (iValidQty > 0)
        {
        	List vGet = getGetByID(_oPWP.getDiscountId(), null);
            for (int i = 0; i < vGet.size(); i++)
            {
                PwpGet oGet = (PwpGet)vGet.get(i);
                String sItemID = oGet.getGetItemId();
                double dGetQty = oGet.getGetQty().doubleValue();
                
                if (!bTDIsBuy && StringUtil.isEqual(_oTD.getItemId(),sItemID)) //update Current TD GET 
                {
                    double dTDQty = _oTD.getQty().doubleValue();
                	double dDiscQty = 0;
                    BeanUtil.setProperty(_oTD, "pwpType", i_PWP_GET);
                	
                	log.info("Current TD " + _oTD.getItemCode() + " Qty: " + dTDQty + " Valid Get: " + (dGetQty * iValidQty));                	

                    if ((dTDQty) >= (dGetQty * iValidQty))
                    {
                    	dDiscQty = (dGetQty * iValidQty);
                    }
                    else
                    {
                    	dDiscQty = dTDQty;
                    }

                    double dDiscAmt = Calculator.sub(_oTD.getItemPrice(), oGet.getGetPrice());
                    dDiscAmt = dDiscAmt * dDiscQty;                	           
                    log.info("Current TD Discount is: " + dDiscAmt);
                    
                    BeanUtil.setProperty(_oTD,"discount", new BigDecimal(dDiscAmt).toString());
                }
                else //update get in existing _vTD
                {            	
                    for (int j = 0; j < _vTD.size(); j++)
                    {
                        InventoryDetailOM oTD = (InventoryDetailOM) _vTD.get(j);
                        if (StringUtil.isEqual(oTD.getItemId(), sItemID))
                        {
                            double dTDQty = oTD.getQty().doubleValue();
                            double dDiscQty = 0;
                            log.info("Existing TD " + oTD.getItemCode() + " Qty: " + dTDQty + " Valid Get: " + (int)(dGetQty * iValidQty));  
                            if ((dTDQty) >= (dGetQty * iValidQty))
                            {
                            	dDiscQty = (dGetQty * iValidQty);
                            }
                            else
                            {
                            	dDiscQty = dTDQty;
                            }
                            
                            double dDiscAmt = Calculator.sub(oTD.getItemPrice(), oGet.getGetPrice());
                            dDiscAmt = dDiscAmt * dDiscQty;                	           
                            log.info("Existing TD Discount is: " + dDiscAmt);
                            
                            BeanUtil.setProperty(oTD,"discount", new BigDecimal(dDiscAmt).toString());
                            
            	        }
                    }            	
                }
            }
        }                
    }

    //-------------------------------------------------------------------------
    // OLD Deprecated Methods
    //-------------------------------------------------------------------------
      
    /**
     * return PWPGET Price for available item after validate with valid Qty 
     *   
     * @param _sID
     * @param _vTD
     * @param _oTD
     * @param _iValidQty
     * @return
     * @throws Exception
     * @deprecated
     */
    public static BigDecimal getPrice(String _sID,List _vTD,InventoryDetailOM _oTD,int _iValidQty)
        throws Exception
    {
        List vGet = getGetByID(_sID,null);
        for (int i = 0; i < vGet.size(); i++)
        {
            PwpGet oGet = (PwpGet)vGet.get(i);
            String sItemID = oGet.getGetItemId();
            double dGetQty = oGet.getGetQty().doubleValue();
            double dTDQty = _oTD.getQty().doubleValue();
            double dExistGet = getTotalQty(sItemID,_vTD,i_PWP_GET);
            
            if (StringUtil.isEqual(_oTD.getItemId(),sItemID))
            {
            	BeanUtil.setProperty(_oTD, "pwpType", i_PWP_GET);
                if ((dTDQty + dExistGet) <= (dGetQty * _iValidQty))
                {
                	//BeanUtil.setProperty(_oTD, "isPwpSet", true);
                    return oGet.getGetPrice();                
                }                
            }
            else
            {
            	//try to update existing PWP GET Items scanned before buy
            	updatePWPGet(oGet, _vTD, _iValidQty);
            }
        }
        return null;
    }
    
    /**
     * iterate through existing, 
     * 
     * @param _oGet
     * @param _vTD
     * @param _iValidQty
     * @deprecated
     */
    private static void updatePWPGet(PwpGet _oGet, List _vTD, int _iValidQty)
    {
    	int iCount = (int)getTotalSetQty(_oGet.getGetItemId(), _vTD);
        for (int j = 0; j < _vTD.size(); j++)
        {
            InventoryDetailOM oTD = (InventoryDetailOM) _vTD.get(j);
            if (StringUtil.isEqual(oTD.getItemId(), _oGet.getGetItemId()))
            {
            	System.out.println("iValidQty: " + _iValidQty);
            	System.out.println("iCount: " + iCount);
            	
	            if(oTD instanceof SalesTransactionDetail)
	            {
	            	SalesTransactionDetail oSTD = (SalesTransactionDetail) oTD;
	            	//boolean bSet = oSTD.getIsPwpSet();	            		            	
	            	if((iCount + oSTD.getQty().intValue()) <= _iValidQty)
	            	{
	            		//oSTD.setIsPwpSet(true);
                        if(_oGet.getGetPrice().intValue() == 0)
                        {
                        	oSTD.setDiscount("100%");
                        }
                        else
                        {
                        	oSTD.setItemPrice(_oGet.getGetPrice());
                        }
                        iCount++;
	            		oSTD.recalulate();	            		
	            	}	            			            	
	            }
	        }
        }
    }
    

    /**
     * only calculate not get qty where discount not set
     * 
     * @param _sItemID
     * @param _vTD
     * @return
     * @deprecated
     */
    private static double getTotalSetQty(String _sItemID, List _vTD)
    {
        double dTotalQty = 0;
        for (int j = 0; j < _vTD.size(); j++)
        {
            InventoryDetailOM oTD = (InventoryDetailOM) _vTD.get(j);
            boolean bSet = (Boolean) BeanUtil.invokeGetter(oTD, "isPwpSet");
            if (StringUtil.isEqual(oTD.getItemId(),_sItemID) && bSet)
            {
                dTotalQty += oTD.getQty().doubleValue();
            }                        	
        }
        return dTotalQty;        
    }

}

