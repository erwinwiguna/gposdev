package com.ssti.enterprise.pos.tools.report;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ReportTool.java,v 1.5 2008/03/16 07:04:35 albert Exp $ <br>
 *
 * <pre>
 * $Log: ReportTool.java,v $
 * 2015-11-18
 * -Change displayTD prevent 2x invoke getter
 * 
 * </pre><br>
 */
public class ReportTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( ReportTool.class );
	
	static ReportTool instance = null;
		
	public static synchronized ReportTool getInstance() 
	{
		if (instance == null) instance = new ReportTool();
		return instance;
	}	
	
	public static String displayGroupHeader(String _sTitle, String _sFieldList, Object o)
	{
		return displayGroupHeader(_sTitle, _sFieldList, o, null);
	}
	
	public static String displayGroupHeader(String _sTitle, String _sFieldList, Object o, List _vLink)
	{
		if (StringUtil.isNotEmpty(_sFieldList))
		{
			String[] aFld = StringUtil.split(_sFieldList, ",");
			StringBuilder oSB = new StringBuilder();
			if (StringUtil.isNotEmpty(_sTitle))
			{
				oSB.append(_sTitle).append(" : ");
			}		
			int iLast = aFld.length - 1;
			
			for (int i = 0; i < aFld.length; i++)
			{
				Object oValue = BeanUtil.invokeGetter(o, aFld[i]);
				Object[] aValue = {oValue};
				if (oValue != null)
				{
					if (_vLink != null && _vLink.size() >= 2) 
					{
						oSB.append(invokeLinked(_vLink, aValue, true));
					}
					else
					{
						oSB.append(processAsString(oValue));
					}
				}
				else
				{
					oSB.append(aFld[i]);	
				}
				if (i != iLast) oSB.append(" - ");				
			}	
			return oSB.toString();
		}
		return "";
	}	

	public static String displayGroupTotal(String _sTitle, String _sFieldList, Object o)
	{
		return displayGroupTotal(_sTitle, _sFieldList, o, null);
	}
	
	public static String displayGroupTotal(String _sTitle, String _sFieldList, Object o, List _vLink)
	{
		if (StringUtil.isNotEmpty(_sFieldList))
		{
			String[] aFld = StringUtil.split(_sFieldList, ",");
			StringBuilder oSB = new StringBuilder();
			if (StringUtil.isNotEmpty(_sTitle))
			{
				oSB.append(_sTitle);
			}		
			else
			{
				oSB.append("Total ");			
			}
			int iLast = aFld.length - 1;
			
			for (int i = 0; i < aFld.length; i++)
			{
				Object oValue = BeanUtil.invokeGetter(o, aFld[i]);
				Object[] aValue = {oValue};
				if (oValue != null)
				{
					if (_vLink != null && _vLink.size() >= 2) 
					{
						oSB.append(invokeLinked(_vLink, aValue, true));
					}
					else
					{
						oSB.append(processAsString(oValue));
					}					
				}
				else
				{
					oSB.append(aFld[i]);	
				}
				if (i != iLast) oSB.append(" - ");				
			}	
			return oSB.toString();
		}
		return "";
	}	
	
	public static String displayValue(Object o, String _sField)
	{
		if (o != null && StringUtil.isNotEmpty(_sField))
		{
			Object oValue = BeanUtil.invokeGetter(o, _sField);
			return processAsString (oValue);
		}
		return "";
	}	

	public static Object getLinkedValue(Object o, String _sField, List _vLink)
	{
		if (o != null && StringUtil.isNotEmpty(_sField))
		{
			Object oParam = BeanUtil.invokeGetter(o, _sField);			
			Object[] aParam = {oParam};
			return invokeLinked (_vLink, aParam, false);
		}
		return null;
	}	
	
	public static Object getLinkedValue(Object o, List _vField, List _vLink)
	{
		if (o != null && _vField != null && _vField.size() > 0)
		{
			Object[] oParams = new Object[_vField.size()]; 
			for (int i = 0; i < _vField.size(); i++)
			{
				oParams[i] = BeanUtil.invokeGetter(o, (String)_vField.get(i));
			}
			return invokeLinked (_vLink, oParams, false);
		}
		return null;
	}	
	
	public static String displayLinkedValue(Object o, String _sField, List _vLink)
	{
		Object oValue = getLinkedValue(o, _sField, _vLink);
		if (oValue != null)
		{
			return processAsString(oValue);
		}
		return "";
	}

	public static String displayLinkedValue(Object o, List _vField, List _vLink)
	{
		Object oValue = getLinkedValue(o, _vField, _vLink);
		if (oValue != null)
		{
			return processAsString(oValue);
		}
		return "";
	}	
	
	static Object invokeLinked(List _vLink, Object[] _aParams, boolean _bToString)
	{
		String sResult = "";
		Object oResult = null;
		if (_vLink != null && _vLink.size() >= 2)
		{
			Object tool = _vLink.get(0);
			String sMethod = (String) _vLink.get(1);
			if (_vLink.size() == 2)
			{
				oResult = invokeLinked(tool, sMethod, _aParams);
			}
			if (_vLink.size() == 3)
			{
				String sField = (String) _vLink.get(2);
				Object oData = invokeLinked(tool, sMethod, _aParams);
				oResult = BeanUtil.invokeGetter(oData, sField);	
			}
		}		
		if (_bToString)
		{
			return processAsString(oResult);
		}
		return oResult;
	}
		
	static Object invokeLinked(Object tool, String _sMethod, Object[] _aParams)
	{
		Class[] oClassParams = new Class[_aParams.length];
		for(int i = 0; i < _aParams.length; i++)
		{
			oClassParams[i] = _aParams[i].getClass();
		}
		try 
		{
			Object oResult = 
				tool.getClass().getMethod(_sMethod, oClassParams).invoke(tool, _aParams);
			
			return oResult;
		} 
		catch (Exception e) 
		{
			Method[] aMethods = tool.getClass().getMethods();
			
			for (int i = 0; i < aMethods.length; i++)
			{				
				if (aMethods[i].getName().equals(_sMethod))
				{
					try
					{
						return aMethods[i].invoke(tool, _aParams);
					}
					catch (Exception ex)
					{
						log.error(ex);
						ex.printStackTrace();
						return ("ERROR : " + ex.getMessage());
					}
				}
			}			
			log.error(e);
			e.printStackTrace();
			return ("ERROR : " + e.getMessage());
		} 	
	}
		
	static String processAsString(Object oResult)
	{
		try 
		{
			if (oResult != null)
			{
				if (oResult instanceof Date) { return CustomFormatter.formatDate(oResult); }
				if (oResult instanceof Number) { return CustomFormatter.formatAligned(oResult); }
				return oResult.toString();
			}
		} 
		catch (Exception e) 
		{
			log.error(e);
			e.printStackTrace();
		} 	
		return "";
	}	
	
	public static int getSumColspan(List _vSum)
	{
		int iResult = 0; 
		for (int i = 0; i < _vSum.size(); i++)
		{
			if ( ((Integer)_vSum.get(i)).intValue() < 0) iResult++; else break;
		}
		return iResult;
	}
	
	public static Map toMap(List _vData, String _sField) 
	{
		Map mData = new HashMap(_vData.size());
		if (_vData != null)
		{
			try 
			{
				List vFiltered = BeanUtil.getDistinctListByField(_vData, _sField);
				for (int i = 0; i < vFiltered.size(); i++)
				{
					Object oData = vFiltered.get(i);
					String sFilter = (String)BeanUtil.invokeGetter(oData, _sField);
					mData.put(sFilter, BeanUtil.filterListByFieldValue(_vData, _sField, sFilter));
				}
			} 
			catch (Exception _oEx) 
			{
				_oEx.printStackTrace();
			}			
		}
		return mData;
	}   
	
	public static String display(Object o, String _sField, int _iLinkType)
	{
		if (o != null && _sField != null)
		{
			try 
			{
				_sField = StringUtil.trim(_sField);
				if (_iLinkType == 0)
				{
					return processAsString(BeanUtil.invokeGetter(o,_sField));
				}
				if (_iLinkType == 1) //Code
				{
					String sValue = (String)BeanUtil.invokeGetter(o,_sField);
					if (StringUtil.isEqual(_sField,"employeeId")) return EmployeeTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"salesId")) return EmployeeTool.getCodeByID(sValue);					
					if (StringUtil.isEqual(_sField,"locationId")) return LocationTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"paymentTypeId")) return PaymentTypeTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"paymentTermId")) return PaymentTermTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"unitId")) return UnitTool.getCodeByID(sValue);					
					if (StringUtil.isEqual(_sField,"taxId")) return TaxTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"adjustmentTypeId")) return AdjustmentTypeTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"customerTypeId")) return CustomerTypeTool.getCodeByID(sValue);					
					if (StringUtil.isEqual(_sField,"customerId")) return CustomerTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"vendorId")) return VendorTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"currencyId")) return CurrencyTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"accountId")) return AccountTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"bankId")) return BankTool.getCodeByID(sValue);
					if (StringUtil.isEqual(_sField,"itemId")) return ItemTool.getItemCodeByID(sValue);
				}
				if (_iLinkType == 2) //Name / Desc
				{
					String sValue = (String)BeanUtil.invokeGetter(o,_sField);
					if (StringUtil.isEqual(_sField,"employeeId")) return EmployeeTool.getEmployeeNameByID(sValue);
					if (StringUtil.isEqual(_sField,"salesId")) return EmployeeTool.getEmployeeNameByID(sValue);					
					if (StringUtil.isEqual(_sField,"locationId")) return LocationTool.getLocationNameByID(sValue);
					if (StringUtil.isEqual(_sField,"paymentTypeId")) return PaymentTypeTool.getDescriptionByID(sValue);
					if (StringUtil.isEqual(_sField,"paymentTermId")) return PaymentTermTool.getDescriptionByID(sValue);
					if (StringUtil.isEqual(_sField,"unitId")) return UnitTool.getDescriptionByID(sValue);					
					if (StringUtil.isEqual(_sField,"taxId")) return TaxTool.getDescriptionByID(sValue);
					if (StringUtil.isEqual(_sField,"adjustmentTypeId")) return AdjustmentTypeTool.getDescriptionByID(sValue);
					if (StringUtil.isEqual(_sField,"customerTypeId")) return CustomerTypeTool.getDescriptionByID(sValue);										
					if (StringUtil.isEqual(_sField,"customerId")) return CustomerTool.getCustomerNameByID(sValue);
					if (StringUtil.isEqual(_sField,"vendorId")) return VendorTool.getVendorNameByID(sValue);
					if (StringUtil.isEqual(_sField,"currencyId")) return CurrencyTool.getDescriptionByID(sValue);
					if (StringUtil.isEqual(_sField,"accountId")) return AccountTool.getAccountNameByID(sValue,null);
					if (StringUtil.isEqual(_sField,"bankId")) return BankTool.getBankNameByID(sValue);
					if (StringUtil.isEqual(_sField,"itemId")) return ItemTool.getItemNameByID(sValue);
				}				
			} 
			catch (Exception e) 
			{
				log.error(e);
				e.printStackTrace();
			}
		}
		return "";		
	}
	
	public static String displayTD(Object o, String _sField, int _iLinkType, String _sCss)
	{
		StringBuilder oSB = new StringBuilder();
		
		oSB.append("<td ");
		if(StringUtil.equalsIgnoreCase(_sField, "itemCode") || 
		   StringUtil.equalsIgnoreCase(_sField, "barcode")  || 
		   StringUtil.equalsIgnoreCase(_sField, "itemSku")  ||
		   StringUtil.equalsIgnoreCase(_sField, "vendorItemCode"))
		{
			oSB.append(" class=\"xls-txt\" ");
		}
		oSB.append(">");
		try 
		{
			Object oRes = null;						
			String sVal = display(o,_sField,_iLinkType);
			if (StringUtil.isEmpty(sVal)) sVal = "&nbsp;"; 
			oSB.append(sVal);
		} 
		catch (Exception e) 
		{
			oSB.append(">").append("&nbsp;");
			log.error(e);
			e.printStackTrace();
		} 	
		oSB.append("</td>").append(s_LINE_SEPARATOR);
		return oSB.toString();
	}
}