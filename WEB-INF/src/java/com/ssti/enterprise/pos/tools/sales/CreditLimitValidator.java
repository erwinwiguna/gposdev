package com.ssti.enterprise.pos.tools.sales;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.financial.CustomerBalanceTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-09-09
 * -Change validateTransAmount activate validate overdue again
 * 
 * 2015-10-20
 * -Change validateTransAmount to not validate if overdue invoice exists if dist installed, 
 *  because dist will use EmpRoleConfig 
 * 
 * 
 * 2015-07-31
 * -Remove method validate MedSales to remove link with medical modules
 * </pre><br>
 */
public class CreditLimitValidator 
{
	static CreditLimitValidator instance = null;
	
	public static synchronized CreditLimitValidator getInstance() 
	{
		if (instance == null) instance = new CreditLimitValidator();
		return instance;
	}	
	
	public static final int OK = 0;
	public static final int OVER_BALANCE = 1;
	public static final int OVER_DUE = 2;
	public static final int OVER_LIMIT = 3;
	
	static Map m_Status = null;
	static
	{
		m_Status= new HashMap();
		m_Status.put(OK, "Valid Credit Limit");
		m_Status.put(OVER_BALANCE, LocaleTool.getString("current_balance") + " > " + LocaleTool.getString("credit_limit"));
		m_Status.put(OVER_DUE, LocaleTool.getString("overdue_inv_exist"));
		m_Status.put(OVER_LIMIT, LocaleTool.getString("overlimit"));
	}
	
	//-------------------------------------------------------------------------
	// CREDIT LIMIT
	//-------------------------------------------------------------------------
	
	public static int validateCreditLimit(String _sCustomerID)
		throws Exception
	{
		Customer oCust = CustomerTool.getCustomerByID(_sCustomerID);
		if (oCust != null)
		{
			double dLimit = 0;
			if (oCust.getCreditLimit() != null) 
			{
				dLimit = oCust.getCreditLimit().doubleValue();
			}
			if (dLimit > 0)
			{
				double dBalance = CustomerBalanceTool.getBalance(_sCustomerID).doubleValue();
				if (dBalance > dLimit)
				{
					return OVER_BALANCE;
				}
				else
				{
					//check overdue invoice
					if(true)
					{
						List vSI = getOverdueInvoices(_sCustomerID);
						if (vSI.size() > 0)
						{
							return OVER_DUE;
						}
					}
				}
			}
		}
		return OK;
	}
	
	public static int validateTransAmount(String _sCustomerID, Number _dAmount)
		throws Exception
	{
		Customer oCust = CustomerTool.getCustomerByID(_sCustomerID);
		if (oCust != null)
		{
			double dLimit = 0;
			if (oCust.getCreditLimit() != null) 
			{
				dLimit = oCust.getCreditLimit().doubleValue();
			}
			if(true)			
			{
				if (dLimit > 0)
				{					
					double dBalance = CustomerBalanceTool.getBalance(_sCustomerID).doubleValue();
					double dAmount = 0;
					if (_dAmount != null) dAmount = _dAmount.doubleValue();				
					if (dBalance > dLimit)
					{
						return OVER_BALANCE;
					}
					else if (dBalance <= dLimit && ((dBalance + dAmount) > dLimit))
					{				
						return OVER_LIMIT;
					}
					//enough limit but check overdue
					else
					{
						List vSI = getOverdueInvoices(_sCustomerID);
						if (vSI.size() > 0)
						{
							return OVER_DUE;
						}						
					}
					
				}
				else if (dLimit < 0) //if limit < 0 validate overdue
				{
					List vSI = getOverdueInvoices(_sCustomerID);
					if (vSI.size() > 0)
					{
						return OVER_DUE;
					}
				}
			}
		}
		return OK;
	}
	
	public static double getLimitLeft(String _sCustomerID)
		throws Exception
	{
		Customer oCust = CustomerTool.getCustomerByID(_sCustomerID);
		if (oCust != null)
		{
			double dLimit = 0;
			if (oCust.getCreditLimit() != null) 
			{
				dLimit = oCust.getCreditLimit().doubleValue();
			}
			if (dLimit > 0)
			{
				double dBalance = CustomerBalanceTool.getBalance(_sCustomerID).doubleValue();
				return dLimit - dBalance;
			}
		}
		return 0;
	}
	
	public static String getMessage(int i)
	{
		return (String) m_Status.get(i);
	}
	
	public static void validateSO(SalesOrder _oSO)
		throws Exception
	{
		if (_oSO != null)
		{
			//validate Credit Limit first
			int iResult = validateTransAmount(_oSO.getCustomerId(), _oSO.getTotalAmount());
			if (iResult != OK)
			{
				_oSO.setTransactionStatus(AppAttributes.i_SO_NEW);		
				throw new Exception(getMessage(iResult));
			}
		}	
	}

	public static void validateSI(SalesTransaction _oSI)
		throws Exception
	{
		if (_oSI != null)
		{
			//validate Credit Limit first
			boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oSI.getPaymentTypeId());
			int iResult = validateTransAmount(_oSI.getCustomerId(), _oSI.getTotalAmount());
			if (iResult != OK && bIsCredit)
			{
				_oSI.setStatus(AppAttributes.i_PENDING);		
				throw new Exception(getMessage(iResult));
			}
		}	
	}
		
	public static List getOverdueInvoices(String _sCustomerID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(SalesTransactionPeer.CUSTOMER_ID, _sCustomerID); 				
		oCrit.add(SalesTransactionPeer.STATUS, AppAttributes.i_PROCESSED); 		
		oCrit.and(SalesTransactionPeer.DUE_DATE,  
			DateUtil.getEndOfDayDate(DateUtil.getTodayDate()), Criteria.LESS_EQUAL);
		oCrit.add(SalesTransactionPeer.PAID_AMOUNT,
		    (Object) SqlUtil.buildCompareQuery 
				(SalesTransactionPeer.PAID_AMOUNT, SalesTransactionPeer.TOTAL_AMOUNT, "<"), Criteria.CUSTOM);
		return SalesTransactionPeer.doSelect(oCrit);
	}
	
	public static int getMaxOverdueDays (String _sCustomerID)
		throws Exception
	{
		int iMax = 0;
		List vOD = getOverdueInvoices(_sCustomerID);
		if(vOD.size() > 0)
		{
			Date dToday = new Date();			
			for(int i = 0; i < vOD.size(); i++)
			{
				SalesTransaction oTR = (SalesTransaction) vOD.get(i);
				int iDays = DateUtil.countDays(oTR.getDueDate(), dToday);
				//log.debug(oTR.getInvoiceNo() + "  " + iDays);
				if(iDays > iMax)
				{
					iMax = iDays;
				}
			}
		}
		return iMax;			
	}	
}
