package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.InvoicePaymentPeer;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.presentation.screens.transaction.DefaultSalesTransaction;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PaymentVoucherTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.enterprise.pos.tools.financial.AccountReceivableTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Business object controller for Invoice Payment OM
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InvoicePaymentTool.java,v 1.13 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: InvoicePaymentTool.java,v $
 * 
 * 2017-06-23
 * - add method deletePaymentsByID
 * - update method processPayment call delete by ID before save to DB to make sure no duplicate invoice payment
 * - update method processPayment, generate InvoicePaymentId using SalesTransactionId + 2 digit index 00, 01..
 * - @see {@link DefaultSalesTransaction}
 * 
 * 2017-03-13
 * - add overriding method, for validate Payment so it can be used not only by SalesTransaction Object, 
 *   but also from MedicalSales Object.
 * 
 * 2016-09-10
 * - change method validatePayment add EDC validation 
 * </pre><br>
 */
public class InvoicePaymentTool extends BaseTool
{
	private static Log log = LogFactory.getLog(InvoicePaymentTool.class);
	private static InvoicePaymentTool instance;
	   
    public static synchronized InvoicePaymentTool getInstance() 
    {
    	if (instance == null) 
    	{
    		instance = new InvoicePaymentTool();
    	}        
        return instance;
    }
	

	/**
	 * delete payment details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	private static void deletePaymentsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(InvoicePaymentPeer.TRANSACTION_ID, _sID);
        InvoicePaymentPeer.doDelete (oCrit, _oConn);
	}    
	/**
	 * set due date for transaction and invoice payment
	 * if transaction due date before inv payment due 
	 * then trans due date will be updated
	 * 
	 * @param _oTR
	 * @param _oPmt
	 * @throws Exception
	 */
	private static void setDueDate (SalesTransaction _oTR, InvoicePayment _oPmt, Connection _oConn) 
		throws Exception 
	{		    
		
		int iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oPmt.getPaymentTermId(), _oConn);

        Calendar cPaymentDue = new GregorianCalendar ();

        if (_oPmt.getDueDate() == null)
        {
            cPaymentDue.setTime (_oTR.getTransactionDate());
        	cPaymentDue.add (Calendar.DATE, iDueDays);
        }
        else
        {
        	cPaymentDue.setTime (_oPmt.getDueDate());        
        }
        
        Calendar cTransDue = new GregorianCalendar ();
        cTransDue.setTime (_oTR.getDueDate());

        //if trans due is before payment due then set the trans due to the current due date
        if (cTransDue.before(cPaymentDue))
        {
            _oTR.setDueDate (cPaymentDue.getTime());	
        }
        _oPmt.setDueDate (cPaymentDue.getTime());		
	}
	
	/**
	 * set SalesTransaction InvoicePayment object
	 * This functions set InvoicePayment Object from transaction if 
	 * no InvPayment exists from POSMultiPaymentSelector	 
	 * 
	 * @param _oTR
	 * @param data
	 * @throws Exception
	 */
    public static void setPayment (SalesTransaction _oTR, RunData data)
        throws Exception
    {
    	
        HttpSession oSes = data.getSession();
        List vPmt = (List) oSes.getAttribute(s_PMT);
        //if no payment info 
        if (vPmt == null || vPmt.size() == 0)
        {
            //create new
        	vPmt = new ArrayList (1);
            InvoicePayment oInv 	 = new InvoicePayment();
            oInv.setPaymentTypeId    (_oTR.getPaymentTypeId());
            oInv.setPaymentTermId    (_oTR.getPaymentTermId());
            oInv.setPaymentAmount    (_oTR.getTotalAmount());
            oInv.setTransTotalAmount (_oTR.getTotalAmount());
            oInv.setVoucherId        ("");
            oInv.setReferenceNo      ("");           
            vPmt.add (oInv);
        }
        else //if there's already invoice payment exist then try to update with the latest value
        {
        	if (vPmt.size() == 1)
        	{
                //single payment
        		InvoicePayment oInv 	 = (InvoicePayment) vPmt.get(0);
                oInv.setPaymentTypeId    (_oTR.getPaymentTypeId());
                oInv.setPaymentTermId    (_oTR.getPaymentTermId());
                oInv.setCashAmount		 (_oTR.getTotalAmount());
                oInv.setPaymentAmount    (_oTR.getTotalAmount());
                oInv.setTransTotalAmount (_oTR.getTotalAmount());
                oInv.setVoucherId        ("");
                if(!_oTR.getIsPaymentSet())
                {
	                oInv.setReferenceNo      (""); 
	                oInv.setBankIssuer       ("");
	                oInv.setApprovalNo		 ("");
                }
                vPmt.set(0, oInv);
        	}
        	else
        	{
        		//TODO: consider to reset to single payment 
        		data.setMessage(LocaleTool.getString("multi_exist_check"));
                log.warn("MULTIPLE PAYMENT EXIST, Must be checked again");
        	}
        }
        //_oTR.setIsPaymentSet();
        _oTR.setInvPayments(vPmt);
        
        double dPmtAmt = _oTR.getPaymentAmount().doubleValue();
        double dTtlAmt = _oTR.getTotalAmount().doubleValue();
        if (dPmtAmt < dTtlAmt || (!_oTR.getPaymentType().getIsCash() && !_oTR.getPaymentType().getIsMultiplePayment()))
        {        
        	_oTR.setPaymentAmount(_oTR.getTotalAmount());
        	_oTR.setChangeAmount(bd_ZERO);
        }
        oSes.setAttribute(s_PMT, vPmt);        
    }	
    
	/**
	 * set SalesTransaction InvoicePayment object
	 * 
	 * @param _oTR
	 * @param data
	 * @throws Exception
	 */
    public static void setPayment (String _sTypeID, 
    							   String _sTermID, 
    							   BigDecimal _bdTotalAmount, 
    							   RunData data)
        throws Exception
    {
        HttpSession oSes = data.getSession();
        List vPmt = (List) oSes.getAttribute(s_PMT);
        
        //if no payment info 
        if (vPmt == null || vPmt.size() == 0)
        {
            //create new
        	vPmt = new ArrayList (1);
            InvoicePayment oInv 	 = new InvoicePayment();
            oInv.setPaymentTypeId    (_sTypeID);
            oInv.setPaymentTermId    (_sTermID);
            oInv.setPaymentAmount    (_bdTotalAmount);
            oInv.setTransTotalAmount (_bdTotalAmount);
            oInv.setVoucherId        ("");
            oInv.setReferenceNo      ("");           
            vPmt.add (oInv);
        }
        else //if there's already invoice payment exist then try to update with the latest value
        {
        	if (vPmt.size() == 1)
        	{
                //single payment
        		InvoicePayment oInv 	 = (InvoicePayment) vPmt.get(0);
                oInv.setPaymentTypeId    (_sTypeID);
                oInv.setPaymentTermId    (_sTermID);
                oInv.setPaymentAmount    (_bdTotalAmount);
                oInv.setTransTotalAmount (_bdTotalAmount);
                oInv.setVoucherId        ("");
                vPmt.set(0, oInv);
        	}
        	else
        	{
        		//TODO: consider to reset to single payment 
        		data.setMessage(LocaleTool.getString("multi_exist_check"));
                log.warn("MULTIPLE PAYMENT EXIST, Must be checked again");
        	}
        }
        oSes.setAttribute(s_PMT, vPmt);
    }	
    
    /**
     * validate invoice payment
     * inv total amount must = total payment amount
     * 
     * @param _oTR
     * @param _vPayment
     * @throws Exception
     */
    public static void validatePayment (SalesTransaction _oTR, List _vPayment)
	    throws Exception
	{
		validatePayment(_oTR.getTotalAmount(), _oTR.getChangeAmount(), _vPayment);
	}	    

    /**
     * 
     * validate invoice payment
     * inv total amount must = total payment amount
     * 
     * @param _bdTotalAmount
     * @param _bdChangeAmount
     * @param _vPayment
     * @throws Exception
     */
    public static void validatePayment (BigDecimal _bdTotalAmount, BigDecimal _bdChangeAmount, List _vPayment)
	    throws Exception
	{
    	double dTotalSales = _bdTotalAmount.doubleValue();
		dTotalSales = new BigDecimal (dTotalSales).setScale(2,4).doubleValue();
		
	    double dTotalPaid = 0;
	    if (_vPayment != null && _vPayment.size() > 0)
	    {
	        for (int i = 0; i < _vPayment.size(); i++)
	        {
	            InvoicePayment oPmt = (InvoicePayment) _vPayment.get(i);
	            if (oPmt != null)
	            {
	            	dTotalPaid = dTotalPaid + oPmt.getPaymentAmount().doubleValue();                
	            }
	        }
			dTotalPaid = new BigDecimal (dTotalPaid).setScale(2,4).doubleValue();
			if (dTotalSales > dTotalPaid)
			{
				throw new Exception ("Total Sales > Invoice Payment Set");
			}
			if (dTotalSales < dTotalPaid)
			{
				throw new Exception ("Total Sales < Invoice Payment Set");
			}
			boolean bCardPayment = isCard(_vPayment);
			boolean bEDCSet = isEDCSet(_vPayment);
			if(PaymentTermTool.getEDC().size() > 0)
			{
				if(bCardPayment && !bEDCSet)
				{
					throw new Exception ("EDC Card Payment Not Set ");
				}
			}
			if(_bdChangeAmount != null && _bdChangeAmount.doubleValue() != 0)
			{
				boolean bValid = false;
				for (int i = 0; i < _vPayment.size(); i++)
		        {
		            InvoicePayment oPmt = (InvoicePayment) _vPayment.get(i);
		            if(oPmt.getIsCash()) 
		            {
//		            	if(oPmt.getChangeAmount() != _bdChangeAmount.doubleValue())
//		            	{
//		            		throw new Exception ("Change amount in Payment must be equals with Transaction ");
//		            	}
		            	bValid = true;		            
		            }
		        }
				if(!bValid)
				{
					throw new Exception ("Change amount exists, but No Cash Payment Set");
				}
			}
	    }
	    else
	    {
	    	throw new Exception ("Invoice Payment not set");
	    }
	}
    
    /**
     * process trans payment, create AR accordingly
     * 
     * @param _oTR
     * @param _vPayment
     * @param _oConn
     * @throws Exception
     */
    public static void processPayment (SalesTransaction _oTR, List _vPayment, Connection _oConn)
        throws Exception
    {
    	validate(_oConn);
    	String sTransID = _oTR.getSalesTransactionId();
    	double dTotalSales = _oTR.getTotalAmount().doubleValue();
        double dTotalPaid = 0;
        if (_vPayment != null)
        {   
        	//delete existing payment by ID
        	deletePaymentsByID (sTransID, _oConn);
        	
	        for (int i = 0; i < _vPayment.size(); i++)
	        {
	            InvoicePayment oPmt = (InvoicePayment) _vPayment.get(i);
	            if (oPmt != null && oPmt.getPaymentAmount().doubleValue() != 0)
	            {
		            oPmt.setInvoicePaymentId  (sTransID + StringUtil.formatNumberString(Integer.toString(i), 2));
		            oPmt.setTransactionDate   (_oTR.getTransactionDate());
		            oPmt.setTransactionId     (_oTR.getSalesTransactionId());
		            oPmt.setTransactionType   (1); //sales
					oPmt.setModified 		  (true);
					oPmt.setNew 			  (true);
			
					if (oPmt.getTransTotalAmount() == null) oPmt.setTransTotalAmount(_oTR.getTotalAmount());
					
		            PaymentType oType = PaymentTypeTool.getPaymentTypeByID(oPmt.getPaymentTypeId(), _oConn);

		            //set due date for both invoicepayment and salestransaction
		            setDueDate (_oTR, oPmt, _oConn);
		            
		            if (oType != null)
		            {
			            if (oType.getIsCredit())
			            {    
			                AccountReceivableTool.createAREntryFromSales (_oTR, oPmt, _oConn);
			            }         
			            else //if cash
			            {
			                if (oType.getIsCoupon()) //process coupon
			                {
			                	String sNo = oPmt.getReferenceNo();
			                	if (StringUtil.isNotEmpty(sNo))
			                	{
			                		DiscountTool.useCoupon(_oTR, oPmt, sNo, _oConn);			                		
			                	}
			                }   
			                if (oType.getIsVoucher()) //process coupon
			                {
			                	String sNo = oPmt.getReferenceNo();
			                	if (StringUtil.isNotEmpty(sNo))
			                	{
			                		PaymentVoucherTool.useVoucher(_oTR, oPmt, sNo, _oConn);				                	
			                	}
			                }
			                if (oType.getIsPointReward()) //process point reward
			                {
			                	PointRewardTool.createPointTransFromPayment(_oTR, oPmt, _oConn);
			                }
			                if (oType.getIsCash())
			                {
			                	if(oPmt.getCashAmount() == null || oPmt.getCashAmount().doubleValue() == 0)
			                	{
			                		oPmt.setCashAmount(_oTR.getPaymentAmount());
			                	}
			                }
			                if(oType.getIsCreditCard())
			                {
			                	String sPf = "";
			                	String sNo = oPmt.getReferenceNo();
			                	if (StringUtil.isNotEmpty(sNo) && sNo.length() > 4)
			                	{			             
			                		sPf = sNo.substring(0, 4); 
			                		sNo = sNo.substring(sNo.length() - 4, sNo.length());           		
			                	}
			                	if(StringUtil.isEmpty(sPf))
			                	{
			                		sNo = "xxxx-xxxx-xxxx-" + sNo;
			                	}
			                	else
			                	{
			                		sNo = sPf + "-xxxx-xxxx-" + sNo;
			                	}
			                	oPmt.setReferenceNo(sNo);
			                	
			                	//calculate mdr 
			                	if(StringUtil.isNotEmpty(oPmt.getBankIssuer()))
			                	{
			                		PaymentTermTool.setMDR(oPmt, _oConn);
			                	}
			                }
			                dTotalPaid = dTotalPaid + oPmt.getPaymentAmount().doubleValue();                
			            }            
		            }
		            else
		            {
		            	//should not happened
		            	throw new NestableException ("Invalid Payment Type in Invoice Payment ");
		            }
		            oPmt.save(_oConn);
		            _oTR.setPaidAmount (new BigDecimal (dTotalPaid));
		            _oTR.save(_oConn);

		            log.debug ("Payment " + i + " : " + oPmt);     
	            }
	        }
	        if (dTotalPaid >= dTotalSales) 
	        {
	        	_oTR.setPaymentStatus(i_PAYMENT_PAID);
	        	_oTR.setPaymentDate(_oTR.getTransactionDate());
	        	_oTR.save(_oConn);
	        }
        }
    }	
    
    /**
     * cancel invoice payment 
     * 
     * @param _oTR
     * @param _oConn
     * @throws Exception
     */
    public static void cancelPayment (SalesTransaction _oTR, Connection _oConn)
	    throws Exception
	{
    	validate (_oConn);
    	List vPayment = getTransPayment(_oTR.getSalesTransactionId(), _oConn);
	   
	    log.debug("******* cancel Payment : " + vPayment);
	    for (int i = 0; i < vPayment.size(); i++)
	    {
	    	InvoicePayment oPmt = (InvoicePayment) vPayment.get(i);
	    	if (oPmt != null)
	        {
	    		PaymentType oType = PaymentTypeTool.getPaymentTypeByID(oPmt.getPaymentTypeId(), _oConn);       
		        if (oType.getIsCredit())
		        {    
		        	//roll back AR
		        	AccountReceivableTool.rollbackAR(_oTR.getSalesTransactionId(), _oConn);
                }         
		        
		        //TODO:check point reward && voucher use
		        
		        Criteria oCrit = new Criteria();
		        oCrit.add(InvoicePaymentPeer.INVOICE_PAYMENT_ID, oPmt.getInvoicePaymentId());
		        InvoicePaymentPeer.doDelete(oCrit, _oConn);
	        }
	    }
	}	
    
    public static List getTransPayment (String _sID)
		throws Exception 
    {
    	return getTransPayment (_sID, null);
    }

    /**
     * get all invoice payment from sales transaction id
     * 
     * @param _sID
     * @param _oConn
     * @return
     * @throws Exception
     */
	public static List getTransPayment (String _sID, Connection _oConn) 
		throws Exception 
	{		    
        Criteria oCrit = new Criteria();
        oCrit.add (InvoicePaymentPeer.TRANSACTION_ID, _sID);
        if (_oConn != null)
        {
        	return InvoicePaymentPeer.doSelect(oCrit, _oConn);
        }
    	return InvoicePaymentPeer.doSelect(oCrit);
	}
	
	public static double calculatePayment (List _vPMT) 
		throws Exception 
	{		    
		double dTotalPayment = 0;
		if (_vPMT != null)
		{
			for (int i = 0; i < _vPMT.size(); i++)
			{
				InvoicePayment oPMT = (InvoicePayment) _vPMT.get(i);
				dTotalPayment += oPMT.getPaymentAmount().doubleValue();
			}
		}
		return dTotalPayment;
	}

	
	/**
	 * print payment type code, if multiple then print multiple code
	 * 
	 * @param _oTR
	 * @return
	 */
	public static String printPaymentType(SalesTransaction _oTR)
		throws Exception
	{
		try
		{
			PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(_oTR.getPaymentTypeId());
			if (oPT.getIsMultiplePayment())
			{
				List vInvPmt = InvoicePaymentTool.getTransPayment(_oTR.getSalesTransactionId());
				StringBuilder oSB = new StringBuilder();
				oSB.append(oPT.getPaymentTypeCode()).append("(");
				for(int i = 0; i < vInvPmt.size(); i++)
				{
					InvoicePayment oInvPmt = (InvoicePayment) vInvPmt.get(i);
					PaymentType oPTPmt = PaymentTypeTool.getPaymentTypeByID(oInvPmt.getPaymentTypeId());
					oSB.append(oPTPmt.getPaymentTypeCode()).append(" ").append(CustomFormatter.formatNumber(oInvPmt.getPaymentAmount()));
					if (i != (vInvPmt.size() - 1)) oSB.append(";");
				}
				oSB.append(")");
				return oSB.toString();
			}
			return oPT.getPaymentTypeCode();
		}
		catch (Exception _oEx) 
		{
			
		}
		return "";
	}

	/**
	 * filter by payment_type_id
	 * @param _vPmt
	 * @param _sPTypeID
	 * @return
	 */
	public static InvoicePayment filterByPTypeID(List _vPmt, String _sPTypeID)
	{
		if (_vPmt != null && _vPmt.size() > 0 && StringUtil.isNotEmpty(_sPTypeID))
		{
			for (int i = 0; i < _vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment) _vPmt.get(i);
				if(StringUtil.isEqual(oPmt.getPaymentTypeId(), _sPTypeID))
				{
					return oPmt;
				}
			}
		}
		return null;
	}
	
	/**
	 * get InvoicePayment by trans_id and payment_type_id
	 * 
	 * @param _sTransID
	 * @param _sPTypeID
	 * @return
	 * @throws Exception
	 */
	public InvoicePayment getByTransIDAndPTypeID(String _sTransID, String _sPTypeID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(InvoicePaymentPeer.TRANSACTION_ID,_sTransID);
		oCrit.add(InvoicePaymentPeer.PAYMENT_TYPE_ID, _sPTypeID);
		List v = InvoicePaymentPeer.doSelect(oCrit);
		if (v.size() > 0)
		{
			return (InvoicePayment)v.get(0);
		}
		return null;
	}
	
	/**
	 * get Payment By Cashier & PaymentTypeID From Opening To Closing
	 * 
	 * @param _sPTypeID
	 * @param _sCashier
	 * @param _dFrom
	 * @param _dTo
	 * @return
	 * @throws Exception
	 */
	public List findInvoicePayment(String _sLocID, String _sPTypeID, String _sCashier, Date _dFrom, Date _dTo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addJoin(InvoicePaymentPeer.TRANSACTION_ID,SalesTransactionPeer.SALES_TRANSACTION_ID);
		oCrit.add(SalesTransactionPeer.STATUS, i_PROCESSED);
		if(StringUtil.isNotEmpty(_sPTypeID))
		{
			oCrit.add(InvoicePaymentPeer.PAYMENT_TYPE_ID, _sPTypeID);
		}
		if(StringUtil.isNotEmpty(_sLocID))
		{
			oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocID);
		}				
		if(StringUtil.isNotEmpty(_sCashier))
		{
			oCrit.add(SalesTransactionPeer.CASHIER_NAME, _sCashier);			
		}				
		if(_dFrom != null)
		{
			oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, _dFrom, Criteria.GREATER_EQUAL);			
		}
		if(_dTo != null)
		{
			oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, _dTo, Criteria.LESS_EQUAL);			
		}
		log.debug("findInvoicePayment: " + oCrit);
		return InvoicePaymentPeer.doSelect(oCrit);
	}

	/**
	 * 
	 * @param _vPmt List of invoice payment
	 * @param _sPTypeID payment type_id
	 * @param _iType 1 Cash, 2 Card, 3 Credit, 4 Voucher, 5 Coupon, 6 Point Reward
	 * @param _sBank payment type bank
	 * @return
	 * @throws Exception
	 */
	public double sumByType(List _vPmt, String _sPTypeID, int _iType, String _sBank)
		throws Exception
	{
		double dTotal = 0;
		if (_vPmt != null)
		{
			
			for(int i = 0; i < _vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment)_vPmt.get(i);
				if(StringUtil.isNotEmpty(_sPTypeID))
				{
					if(StringUtil.isEqual(_sPTypeID, oPmt.getPaymentTypeId()))
					{
						if(StringUtil.isEmpty(_sBank) || 
							(StringUtil.isNotEmpty(_sBank) && StringUtil.isEqual(_sBank, oPmt.getBankIssuer())))
						{
							dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();
						}
					}
				}
				else
				{
					if(_iType == 1 && oPmt.getIsCash()) //cash
					{
						dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();						
					}
					else if(_iType == 2 && oPmt.getIsCard()) //card
					{
						if(StringUtil.isEmpty(_sBank) || 
							(StringUtil.isNotEmpty(_sBank) && StringUtil.isEqual(_sBank, oPmt.getBankIssuer())))
						{
							dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();
						}
					}
					else if(_iType == 3 && oPmt.getIsCredit()) //card
					{
						dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();						
					}
					else if(_iType == 4 && oPmt.getIsVoucher()) //card
					{
						dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();						
					}
					else if(_iType == 5 && oPmt.getIsCoupon()) //card
					{
						dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();						
					}
					else if(_iType == 6 && oPmt.getIsPointReward()) //card
					{
						dTotal = dTotal + oPmt.getPaymentAmount().doubleValue();						
					}					
				}
			}			
		}
		return dTotal;
	}
	
	//-------------------------------------------------------------------------
	// EDC
	//-------------------------------------------------------------------------
	public static boolean isCard(List _vPmt)
	{
		if(_vPmt != null)
		{
			for (int i = 0; i < _vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment) _vPmt.get(i);
				if(oPmt.getIsCard())
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isEDCSet(List _vPmt)
	{
		if(_vPmt != null)
		{
			for (int i = 0; i < _vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment) _vPmt.get(i);
				if(oPmt.getIsEDCSet())
				{
					return true;
				}
			}
		}
		return false;
	}
}