package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Sales Analysis Helper Tool
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesAnalysisTool.java,v 1.12 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesAnalysisTool.java,v $
 * 
 * 2016-12-08
 * - new getSalesByTime, getReturnByTime, getNetSalesByTime from getSalesPerLocationByMonthAndPaymentType     
 *   query sales amount group by time unit (month, week, day) per salesman / cust /loc
 *   
 * 2016-12-08
 * - change getDailySalesSummary add kategori and salesman filter 
 * 
 * 2015-02-15
 * - change build Std Query add if status -2 do not add status filter 
 * - Change sumTotalAmount for Tax Calculation if tax is not inclusive
 * 
 * </pre><br>
 */
public class SalesAnalysisTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( SalesAnalysisTool.class );
	
	private static SalesAnalysisTool instance = null;
	
	public static synchronized SalesAnalysisTool getinstance()
	{
		if (instance == null) instance = new SalesAnalysisTool();
		return instance;
	}
	
	public static StringBuilder buildStdQuery (StringBuilder oSQL, 
											   Date _dStart, 
											   Date _dEnd, 
											   int _iStatus,
											   String _sLocationID)
	{
		return buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, _sLocationID, "", "");
	}	
	
	public static StringBuilder buildStdQuery (StringBuilder oSQL, 
											   Date _dStart, 
											   Date _dEnd, 
											   int _iStatus,

											   String _sLocationID,
											   String _sDepartmentID,
											   String _sProjectID)
	{
		return buildStdQuery (oSQL, _dStart, _dEnd, _iStatus,"s.transaction_date", _sLocationID, _sDepartmentID, _sProjectID);
	}
	
	public static StringBuilder buildStdQuery (StringBuilder oSQL, 
											   Date _dStart, 
											   Date _dEnd, 
											   int _iStatus,
											   String _sDateCol,
											   String _sLocationID,
											   String _sDepartmentID,
											   String _sProjectID)
	{
		buildDateQuery(oSQL, _sDateCol, _dStart, _dEnd);
		
		if(_iStatus != -2)
		{
			if(_iStatus <= 0) _iStatus = i_TRANS_PROCESSED;
			oSQL.append (" AND s.status = ").append (_iStatus).append(" ");		
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oSQL.append (" AND s.location_id = '").append (_sLocationID).append ("'");
		}
		if (StringUtil.isNotEmpty(_sDepartmentID))
		{
			oSQL.append (" AND sd.department_id = '").append (_sDepartmentID).append ("'");
		}
		if (StringUtil.isNotEmpty(_sProjectID))
		{
			oSQL.append (" AND sd.project_id = '").append (_sProjectID).append ("'");
		}
		return oSQL;
	}

	public static StringBuilder buildAdvQuery (StringBuilder oSQL,
											   String _sKategoriID, 
											   String _sCustID, 
											   String _sCustTypeID, 
											   String _sCashier,
											   String _sSalesID)
		throws Exception
	{
		
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			vKategori.add(_sKategoriID);
		    oSQL.append (" AND i.kategori_id IN ")
	    		.append (SqlUtil.convertToINMode(vKategori));		
		}
		if (StringUtil.isNotEmpty(_sCustID))
		{
		    oSQL.append (" AND s.customer_id = '").append (_sCustID).append("'");		
		}
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
		    oSQL.append (" AND s.customer_id = c.customer_id ");					
		    oSQL.append (" AND c.customer_type_id = '").append (_sCustTypeID).append("'");		
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
		    oSQL.append (" AND s.cashier_name = '").append (_sCashier).append("'");		
		}
		if (StringUtil.isNotEmpty(_sSalesID))
		{
		    oSQL.append (" AND s.sales_id = '").append (_sSalesID).append("'");		
		}		
		return oSQL;
	}	
	
	public static StringBuilder buildRankOrder (StringBuilder oSQL, int _iRankBy, int _iRankType)
		throws Exception
	{	
		oSQL.append (" ORDER BY ");
		
		String sType = "DESC";
		if (_iRankType == i_ASC) sType = "ASC";
		
		if (b_IS_MYSQL)
		{
		if (_iRankBy == 1) oSQL.append (" total_amt ").append(sType); 	 //Amount 
		if (_iRankBy == 2) oSQL.append (" total_qty ").append(sType); 	 //Amount 
		if (_iRankBy == 3) oSQL.append (" total_profit ").append(sType); //Amount 			
		}
		else
		{
		if (_iRankBy == 1) oSQL.append (" SUM(sd.sub_total) ").append(sType); 	 //Amount 
		if (_iRankBy == 2) oSQL.append (" SUM(sd.qty_base) ").append(sType); 	 //Amount 
		if (_iRankBy == 3) oSQL.append (" SUM(sd.sub_total - sd.sub_total_cost) ").append(sType); //Amount 
		}			
		return oSQL;
	}	
	//-------------------------------------------------------------------------
	// SALES SUMMARY & ANALYSIS
	//-------------------------------------------------------------------------	

	public static Hashtable getDailySalesSummary (Date _dDate, String _sLocationID)
		throws Exception
	{		
		return getDailySalesSummary (_dDate, _sLocationID, false);
	}

	public static Hashtable getDailySalesSummary (Date _dDate, String _sLocationID, boolean _bIncTax)
		throws Exception
	{
		return getDailySalesSummary(_dDate, _sLocationID, "", "", _bIncTax);
	}
	
	public static Hashtable getDailySalesSummary(Date _dDate, 
												 String _sLocationID, 
												 String _sKategoriID,
												 String _sSalesmanID,
												 boolean _bIncTax)
		throws Exception
	{		
		List vData = TransactionTool.findTrans(_dDate, DateUtil.getEndOfDayDate(_dDate), 
											   i_TRANS_PROCESSED, _sLocationID, "", "");
			
		List vFData = vData;
		List vKat = null;
		if(StringUtil.isNotEmpty(_sKategoriID))
		{
			vKat = KategoriTool.getChildIDList(_sKategoriID);
			vKat.add(_sKategoriID);
		}		
		if(StringUtil.isNotEmpty(_sSalesmanID))
		{
			vFData = BeanUtil.filterListByFieldValue(vData, "salesId", _sSalesmanID);
		}
		
		double dTotalQty = 0;
		double dTotalCost = 0;
		double dTotalSales = 0;
		double dTotalDisc = 0;
		double dTotalGrossProfit = 0;
		double dTotalProfitPct = 0;
		
		for (int i = 0; i < vFData.size(); i++)
		{ 
			SalesTransaction oSales = (SalesTransaction) vFData.get(i);
			double dRate = oSales.getCurrencyRate().doubleValue();
			double dFiscal = oSales.getFiscalRate().doubleValue();
			
			if(StringUtil.isNotEmpty(_sKategoriID))
			{
				List vDet = TransactionTool.getDetailsByID(oSales.getSalesTransactionId());
				for (int j = 0; j < vDet.size(); j++)
				{
					SalesTransactionDetail oDet = (SalesTransactionDetail) vDet.get(j);
					if(vKat != null && vKat.contains(oDet.getItem().getKategoriId()))
					{
						double dAmt = oDet.getSubTotal().doubleValue();
						double dTax = oDet.getSubTotalTax().doubleValue();
						if (!_bIncTax && oSales.getIsInclusiveTax())
						{
							dAmt = ((dAmt - dTax) * dRate);
						}
						else if (!_bIncTax && !oSales.getIsInclusiveTax())
						{
							dAmt = ((dAmt - dTax) * dRate);
						}
						else if (_bIncTax)
						{
							dAmt = ((dAmt - dTax) * dRate) + (dTax * dFiscal);
						}	
						dTotalQty  += oDet.getQty().doubleValue();
						dTotalDisc += oDet.getSubTotalDisc().doubleValue();
						dTotalCost += oDet.getSubTotalCost().doubleValue();
						dTotalSales += dAmt;			
						dTotalGrossProfit += (dAmt - oDet.getSubTotalCost().doubleValue());
					}
				}
			}
			else
			{
				double dAmt = oSales.getTotalAmount().doubleValue();					
				double dTax = oSales.getTotalTax().doubleValue();
				if (!_bIncTax && oSales.getIsInclusiveTax())
				{
					dAmt = ((dAmt - dTax) * dRate);
				}
				else if (!_bIncTax && !oSales.getIsInclusiveTax())
				{
					dAmt = ((dAmt - dTax) * dRate);
				}
				else if (_bIncTax)
				{
					dAmt = ((dAmt - dTax) * dRate) + (dTax * dFiscal);
				}	
				dTotalQty  += oSales.getTotalQty().doubleValue();
				dTotalDisc += oSales.getTotalDiscount().doubleValue();
				dTotalCost += oSales.getTotalCost().doubleValue();
				dTotalSales += dAmt;			
				dTotalGrossProfit += (dAmt - oSales.getTotalCost().doubleValue());
			}
		}			
		if ((dTotalSales > 0) && (dTotalGrossProfit > 0))
		{
			dTotalProfitPct = (dTotalGrossProfit/dTotalSales) * 100;
		}
		
		Hashtable hSummary = new Hashtable(5);
		hSummary.put ("TotalQty", new BigDecimal (dTotalQty));
		hSummary.put ("TotalCost", new BigDecimal (dTotalCost));
		hSummary.put ("TotalDisc", new BigDecimal (dTotalDisc));
		hSummary.put ("TotalSales", new BigDecimal (dTotalSales));
		hSummary.put ("TotalGrossProfit", new BigDecimal (dTotalGrossProfit));
		hSummary.put ("TotalProfitPct", new BigDecimal ( dTotalProfitPct ));
		
		return hSummary;
	}	
	
	public static Hashtable getDailySalesReturnSummary (Date _dDate, String _sLocationID)
		throws Exception
	{	
		return getDailySalesReturnSummary (_dDate, _sLocationID, false);
	}
	
	public static Hashtable getDailySalesReturnSummary (Date _dDate, String _sLocationID, boolean _bIncTax)
		throws Exception
	{		
		List vData = TransactionTool.findTrans(_dDate, DateUtil.getEndOfDayDate(_dDate), 
				   							   i_TRANS_PROCESSED, _sLocationID, "", "");
		
		List vDataRet = 
			SalesReturnTool.findTrans(_dDate, _dDate, _sLocationID, "", i_TRANS_PROCESSED);
		
		double dTotalTrans = vData.size();
		double dTotalTransRet = vDataRet.size();
		
		double dTotalQty = 0;
		double dTotalSales = 0;
		double dTotalReturnQty = 0;
		double dTotalReturn = 0;
		
		for (int i = 0; i < vData.size(); i++)
		{
			SalesTransaction oSales = (SalesTransaction) vData.get(i);
			double dInvAmount = oSales.getTotalAmount().doubleValue();
			double dTax = oSales.getTotalTax().doubleValue();
			double dRate = oSales.getCurrencyRate().doubleValue();
			double dFiscal = oSales.getFiscalRate().doubleValue();
			if (!_bIncTax && oSales.getIsInclusiveTax())
			{
				dInvAmount = ((dInvAmount - dTax) * dRate);
			}
			else if (!_bIncTax && !oSales.getIsInclusiveTax())
			{
				dInvAmount = ((dInvAmount - dTax) * dRate);
			}
			else if (_bIncTax)
			{
				dInvAmount = ((dInvAmount - dTax) * dRate) + (dTax * dFiscal);
			}
			dTotalQty  += oSales.getTotalQty().doubleValue();
			dTotalSales += dInvAmount; //net sales			
		}
		
		for (int i = 0; i < vDataRet.size(); i++)
		{
			SalesReturn oSalesRet = (SalesReturn) vDataRet.get(i);
			double dNetRet = oSalesRet.getTotalAmount().doubleValue() * oSalesRet.getCurrencyRate().doubleValue();		
			if (!_bIncTax && oSalesRet.getIsInclusiveTax())
			{
				dNetRet = dNetRet - (oSalesRet.getTotalTax().doubleValue() * oSalesRet.getFiscalRate().doubleValue());
			}
			else if (_bIncTax && !oSalesRet.getIsInclusiveTax())
			{
				dNetRet = dNetRet + (oSalesRet.getTotalTax().doubleValue() * oSalesRet.getFiscalRate().doubleValue());
			}			
			dTotalReturnQty  += oSalesRet.getTotalQty().doubleValue();
			dTotalReturn += dNetRet;			
		}

		Hashtable hSummary = new Hashtable(5);
		hSummary.put ("TotalQty", new BigDecimal (dTotalQty));		
		hSummary.put ("TotalSales", new BigDecimal (dTotalSales));
		hSummary.put ("TotalReturnQty", new BigDecimal (dTotalReturnQty));
		hSummary.put ("TotalReturn", new BigDecimal (dTotalReturn));
		hSummary.put ("TotalTrans", new BigDecimal (dTotalTrans));		
		hSummary.put ("TotalTransRet", new BigDecimal (dTotalTransRet));
		return hSummary;
	}
	
	public static void sumTotalAmount(StringBuilder oSQL, boolean _bIncTax)
	{
		if (oSQL != null)
		{
			if (!_bIncTax)
			{
				oSQL.append (" SUM((s.total_amount * s.currency_rate) - (s.total_tax * s.fiscal_rate)) AS total_amount ");
//TODO: TEST AGAIN
//				oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((s.total_amount * s.currency_rate) - (s.total_tax * s.fiscal_rate)) ");
//				oSQL.append (" ELSE SUM(s.total_amount * s.currency_rate) END ");
			}
			else
			{
				oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((s.total_amount * s.currency_rate) + (s.total_tax * s.fiscal_rate)) AS total_amount ");
				oSQL.append (" ELSE SUM(s.total_amount * s.currency_rate) AS total_amount END ");
			}
		}
	}
	
	public static void sumDetailAmount(StringBuilder oSQL, boolean _bIncTax)
	{
		if (oSQL != null)
		{
			if (!_bIncTax)
			{
				oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((sd.sub_total * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
				oSQL.append (" ELSE SUM(sd.sub_total * s.currency_rate) END ");
			}
			else
			{
				oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((sd.sub_total * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
				oSQL.append (" ELSE SUM(sd.sub_total * s.currency_rate) END ");
			}
		}
	}
	
	public static double getTotalSales (Date _dStart, Date _dEnd)
		throws Exception
	{		
	    return getTotalSales (_dStart, _dEnd,i_TRANS_PROCESSED, false);
    }
   	
	public static double getTotalSales (Date _dStart, Date _dEnd, int _iStatus, boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT ");
		sumTotalAmount(oSQL, _bIncTax);
		oSQL.append (" FROM sales_transaction s WHERE ");
		buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, "");
		oSQL.append(" GROUP BY s.is_inclusive_tax ");
		log.debug(oSQL);
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			Record oData = (Record) vData.get(0);
			return oData.getValue(1).asDouble();	 
		}
		return 0;                                                                                                                                                                                 
	}

	public static List getTotalSalesPerSite (Date _dStart, Date _dEnd)
		throws Exception
	{		
	    return getTotalSalesPerSite (_dStart, _dEnd, i_TRANS_PROCESSED, false);
    }
    
	public static List getTotalSalesPerSite (Date _dStart, Date _dEnd, int _iStatus, boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT t.site_id, "); //SUM((total_amount * currency_rate) - (total_tax * fiscal_rate)) ");
		sumTotalAmount(oSQL, _bIncTax);
		oSQL.append (" FROM sales_transaction s, site t, location l WHERE ");		
		buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, "");
		oSQL.append (" AND s.location_id = l.location_id ");		
		oSQL.append (" AND l.site_id = t.site_id GROUP BY t.site_id, s.is_inclusive_tax  ");
		
		log.debug (oSQL.toString());
		
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(SiteTool.getSiteNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			vResult.add (vRecordData);	 
		}
		return vResult;                                                                                                                                                                                 
	}

	public static List getTotalSalesPerLocation (Date _dStart, Date _dEnd, String _sSiteID)
		throws Exception
	{		
	    return getTotalSalesPerLocation (_dStart, _dEnd, _sSiteID, i_TRANS_PROCESSED, false);
    }
    
	public static List getTotalSalesPerLocation (Date _dStart, Date _dEnd, String _sSiteID, int _iStatus, boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.location_id, ");
		sumTotalAmount(oSQL, _bIncTax);
		oSQL.append (" FROM sales_transaction s, site t, location l WHERE ");
		
		buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, "");		
		oSQL.append (" AND s.location_id = l.location_id ");
		oSQL.append (" AND l.site_id = t.site_id ");
		oSQL.append (" AND t.site_id = '").append(_sSiteID).append ("'");		
		oSQL.append (" GROUP BY s.location_id, s.is_inclusive_tax  ");
		
		log.debug (oSQL.toString());
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(LocationTool.getLocationNameByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			vResult.add (vRecordData); 
		}
		return vResult;	
	}

	public static List getTotalSalesPerKategori (Date _dStart, Date _dEnd, String _sLocationID)
		throws Exception
	{		
	    return getTotalSalesPerKategori (_dStart, _dEnd, _sLocationID, i_TRANS_PROCESSED, false);
    }
	
	public static List getTotalSalesPerKategori (Date _dStart, Date _dEnd, String _sLocationID, int _iStatus, boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, ");
		sumDetailAmount(oSQL, _bIncTax);
		oSQL.append (" ,SUM(sd.qty_base) ");
		oSQL.append (" FROM sales_transaction s, ");
		oSQL.append (" sales_transaction_detail sd, kategori k, item i WHERE ");

		buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, _sLocationID);
		
		oSQL.append (" AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append (" AND sd.item_id = i.item_id ");
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
		oSQL.append (" GROUP BY k.kategori_id, s.is_inclusive_tax ORDER BY k.internal_code ");
		
		log.debug (oSQL.toString());

		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (3);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(KategoriTool.getDescriptionByID(oData.getValue(1).asString()));
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			vResult.add (vRecordData);	 
		}
		return vResult;	
	}	
	
	public static List getTotalSalesPerItem (Date _dStart, Date _dEnd, String _sLocationID, String _sKategoriID)
		throws Exception
	{		
	    return getTotalSalesPerItem (_dStart, _dEnd, _sLocationID, _sKategoriID,i_TRANS_PROCESSED, false);
    }
	
	//TODO: there's similar method in SalesReportTool consider merge and refactor
	public static List getTotalSalesPerItem (Date _dStart, 
											 Date _dEnd, 
											 String _sLocationID, 
											 String _sKategoriID, 
											 int _iStatus,
											 boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT sd.item_id, sd.item_code, sd.item_name, ");
		sumDetailAmount(oSQL, _bIncTax);
		oSQL.append (" ,SUM(sd.qty_base)");
		oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd, kategori k, item i WHERE ");

		buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, _sLocationID);

		oSQL.append (" AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append (" AND sd.item_id = i.item_id ");
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
		oSQL.append (" AND k.kategori_id = '").append (_sKategoriID).append("'");
		oSQL.append (" GROUP BY sd.item_id, sd.item_code, sd.item_name, s.is_inclusive_tax ORDER BY sd.item_code ");
		
		log.debug (oSQL.toString());

		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (5);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oData.getValue(2).asString());
			vRecordData.add(oData.getValue(3).asString());
			vRecordData.add(oData.getValue(4).asBigDecimal());			
			vRecordData.add(oData.getValue(5).asBigDecimal());
			vResult.add (vRecordData); 
		}
		return vResult;	
	}	
	
	//-------------------------------------------------------------------------
	// TOP 10 METHODS
	//-------------------------------------------------------------------------
	public static List getRanking (boolean _bKat,
								   String _sPK,
								   Date _dStart, 
								   Date _dEnd, 
								   int _iLimit, 
								   int _iRankBy, 
								   int _iRankType,
								   String _sLocationID,
								   String _sKategoriID,
								   String _sCustomerID,
								   String _sCustTypeID,
								   String _sCashier,
								   String _sSalesID) 
		throws Exception
	{
		return getRanking(_bKat,_sPK,_dStart,_dEnd,_iLimit,_iRankBy,_iRankType,_sLocationID,
						  _sKategoriID,_sCustomerID,_sCustTypeID,_sCashier,_sSalesID,false);
	}
	
	public static List getRanking (boolean _bKat,
								   String _sPK,
								   Date _dStart, 
								   Date _dEnd, 
								   int _iLimit, 
								   int _iRankBy, 
								   int _iRankType,
								   String _sLocationID,
								   String _sKategoriID,
								   String _sCustomerID,
								   String _sCustTypeID,
								   String _sCashier,
								   String _sSalesID,
								   boolean _bIncTax)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		
		oSQL.append ("SELECT ").append(_sPK).append(",");
		sumDetailAmount(oSQL,_bIncTax);
		oSQL.append (" AS total_amt, ")
			.append (" SUM(sd.qty_base) AS total_qty, SUM(sd.sub_total - sd.sub_total_cost) AS total_profit, ")
			.append (" SUM(sd.sub_total_tax) AS total_tax, SUM(sd.sub_total_cost) AS total_cost, ")	
			.append (" COUNT(s.sales_transaction_id) AS total_tx ")
			.append	(" FROM sales_transaction s, sales_transaction_detail sd, item i");
		
		if (_bKat) oSQL.append (", kategori k");
		
		if (StringUtil.isNotEmpty(_sCustTypeID)) oSQL.append(", customer c ");
		oSQL.append (" WHERE ");
		
		buildStdQuery (oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, _sLocationID);
		
		oSQL.append (" AND s.sales_transaction_id = sd.sales_transaction_id ")
		.append (" AND sd.item_id = i.item_id ");
		
		if (_bKat) oSQL.append (" AND i.kategori_id = k.kategori_id ");
		
		buildAdvQuery (oSQL, _sKategoriID, _sCustomerID, _sCustTypeID, _sCashier, _sSalesID);
		
		oSQL.append ("  GROUP BY  " ).append(_sPK).append(", s.is_inclusive_tax ");
		
		buildRankOrder (oSQL, _iRankBy, _iRankType);
		
		SqlUtil.buildLimitQuery(oSQL, _iLimit);
		
		log.info (oSQL.toString());	
		
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (5);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oData.getValue("total_amt").asBigDecimal());	
			vRecordData.add(oData.getValue("total_qty").asBigDecimal());	
			vRecordData.add(oData.getValue("total_profit").asBigDecimal());	
			vRecordData.add(oData.getValue("total_tax").asBigDecimal());
			vRecordData.add(oData.getValue("total_cost").asBigDecimal());
			vRecordData.add(oData.getValue("total_tx").asBigDecimal());			
			vResult.add (vRecordData);	 
		}
		return vResult;	
	}

	//-------------------------------------------------------------------------
	// TIME SERIES METHODS
	//-------------------------------------------------------------------------
	
	//TODO: revised & test all query so it worked in MYSQL, PGSQL & ORACLE and delivers correct result
	//oracle use TO_DATE 
	public static List getSalesPerLocationByDateAndPaymentType (Date _dStart, Date _dEnd, boolean _bPaymentType)
		throws Exception
	{		
	    return getSalesPerLocationByDateAndPaymentType (_dStart, _dEnd, _bPaymentType,i_TRANS_PROCESSED);
    }
    
	public static List getSalesPerLocationByDateAndPaymentType (Date _dStart, 
																Date _dEnd, 
																boolean _bPaymentType,
																int _iStatus)
		throws Exception
	{		
		List vPTID = PaymentTypeTool.getPaymentTypeIDByStatus(_bPaymentType);
		String sIDs = SqlUtil.convertToINMode(vPTID);
		StringBuilder oSQL = new StringBuilder ();
		String sDay  = SqlUtil.dayCol("s.transaction_date");
		
		oSQL.append ("SELECT ").append(sDay)
			.append (", s.location_id, SUM((s.total_amount * s.currency_rate) - (s.total_tax * s.fiscal_rate)) ")
			.append ("FROM sales_transaction s WHERE ");

		buildStdQuery (oSQL, _dStart, _dEnd, _iStatus, "");

		oSQL.append (" AND s.payment_type_id IN ");
		oSQL.append (sIDs);
		
		oSQL.append (" GROUP BY s.location_id, ").append(sDay);
		oSQL.append (" ORDER BY ").append(sDay);
		
		log.debug ("getSalesPerLocationByDateAndPaymentType: " + oSQL.toString());
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		return vData;	
	}	
	
	public static List getReturnPerLocationByDate(Date _dStart, Date _dEnd)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		String sDay  = SqlUtil.dayCol("s.return_date");

		oSQL.append ("SELECT ").append(sDay)
			.append(", s.location_id, SUM(s.total_amount * s.currency_rate) ");
		oSQL.append ("FROM sales_return s WHERE ");

		buildStdQuery (oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, "s.return_date", "", "", "");
		
		oSQL.append (" GROUP BY s.location_id, ").append(sDay);
		oSQL.append (" ORDER BY ").append(sDay);
		
		log.debug ("getReturnPerLocationByDate:" + oSQL.toString());
		List vData = SalesReturnPeer.executeQuery(oSQL.toString());	
		return vData;	
	}	

	/**
	 * filter by date
	 * 
	 * @param _sDate
	 * @param _sLocationID
	 * @param _vData
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getLocationTotalSales (String _sDate, String _sLocationID, List _vData)
		throws Exception
	{					
		for (int i = 0; i < _vData.size(); i++)
		{
			Record oData = (Record) _vData.get(i);
			String sLocationID = oData.getValue(2).asString();			
			if (_sLocationID.equals(sLocationID) && 
					Integer.parseInt(_sDate) == Integer.parseInt(oData.getValue(1).asString())) 
			{
				return oData.getValue(3).asBigDecimal();
			}
		}
		return bd_ZERO;
	}
	
	// Location Yearly Sales Query
	public static List getSalesPerLocationByMonthAndPaymentType (Date _dStart, Date _dEnd, boolean _bPaymentType)
		throws Exception
	{
	  return getSalesPerLocationByMonthAndPaymentType (_dStart,_dEnd,_bPaymentType,i_TRANS_PROCESSED);
	}

	public static List getSalesPerLocationByMonthAndPaymentType (Date _dStart, 
																 Date _dEnd, 
																 boolean _bPaymentType, 
																 int _iStatus)
	throws Exception
	{
		return getSalesByTime(_dStart, _dEnd, true, "", false, "", false, "", true, _bPaymentType, 1);
	}
		
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _bByLoc
	 * @param _sLocID
	 * @param _bBySls
	 * @param _sSlsID
	 * @param _bByCus
	 * @param _sCusID
	 * @param _bByPmt
	 * @param _bCashCredit
	 * @param _iTimeUnit  //1 by month, 2 week, 3 day
	 * @return
	 * @throws Exception
	 */
	public static List getSalesByTime (Date _dStart, 
									   Date _dEnd, 																 
									   boolean _bByLoc,
									   String  _sLocID,
									   boolean _bBySls,
									   String  _sSlsID,
									   boolean _bByCus,
									   String  _sCusID,																 
									   boolean _bByPmt,
									   boolean _bCashCredit,
									   int _iTimeUnit)
		throws Exception
	{		
		List vPTID = PaymentTypeTool.getPaymentTypeIDByStatus (_bCashCredit);
		String sIDs = SqlUtil.convertToINMode(vPTID);
		StringBuilder oSQL = new StringBuilder ();
		String sTimeUnit = SqlUtil.monthCol("s.transaction_date");
		if(_iTimeUnit == 2) sTimeUnit = SqlUtil.weekCol("s.transaction_date"); 
		if(_iTimeUnit == 3) sTimeUnit = SqlUtil.dayCol("s.transaction_date");
		
		oSQL.append ("SELECT ").append(sTimeUnit);
		oSQL.append(", SUM((s.total_amount * s.currency_rate) - (s.total_tax * s.fiscal_rate)) AS amt ");
		if(_bByLoc) oSQL.append(", s.location_id AS locid ");
		if(_bByCus) oSQL.append(", s.customer_id AS cusid ");
		if(_bBySls) oSQL.append(", s.sales_id    AS slsid ");				
		oSQL.append ("FROM sales_transaction s WHERE ");

		buildStdQuery (oSQL, _dStart, _dEnd, i_PROCESSED, "");
		if(StringUtil.isNotEmpty(_sLocID)) oSQL.append (" AND s.location_id = '").append(_sLocID).append("' ");
		if(StringUtil.isNotEmpty(_sSlsID)) oSQL.append (" AND s.sales_id = '").append(_sSlsID).append("' ");
		if(StringUtil.isNotEmpty(_sCusID)) oSQL.append (" AND s.customer_id = '").append(_sCusID).append("' ");
		
		if(_bByPmt)
		{
			oSQL.append (" AND s.payment_type_id IN ");
			oSQL.append (sIDs);
		}
		
		oSQL.append (" GROUP BY ").append(sTimeUnit);
		if(_bByLoc) oSQL.append(", s.location_id ");
		if(_bByCus) oSQL.append(", s.customer_id ");
		if(_bBySls) oSQL.append(", s.sales_id ");
		
		oSQL.append (" ORDER BY ").append(sTimeUnit);
		
		System.out.println ("getSalesPerMonth:" + oSQL.toString());
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		return vData;	
	}	
	
	// Location Yearly Sales Return Query
	public static List getReturnPerLocationByMonth(Date _dStart, Date _dEnd)
		throws Exception
	{		
		return getReturnByTime(_dStart, _dEnd, true, "", false, "", false, "", 1);	
	}	
	
	public static List getReturnByTime (Date _dStart, 
										Date _dEnd, 																 
										boolean _bByLoc,
										String  _sLocID,
									    boolean _bBySls,
										String  _sSlsID,
										boolean _bByCus,
										String  _sCusID,
										int _iTimeUnit)
		throws Exception
	{				
		StringBuilder oSQL = new StringBuilder ();
		String sTimeUnit = SqlUtil.monthCol("s.return_date");
		if(_iTimeUnit == 2) sTimeUnit = SqlUtil.weekCol("s.return_date"); 
		if(_iTimeUnit == 3) sTimeUnit = SqlUtil.dayCol("s.return_date");		

		oSQL.append ("SELECT ").append(sTimeUnit);
		oSQL.append(", SUM((s.total_amount * s.currency_rate) - (s.total_tax * s.fiscal_rate)) AS amt ");
		if(_bByLoc) oSQL.append(", s.location_id AS locid ");
		if(_bByCus) oSQL.append(", s.customer_id AS cusid ");
		if(_bBySls) oSQL.append(", s.sales_id    AS slsid ");
		oSQL.append ("FROM sales_return s WHERE ");

		buildStdQuery (oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, "s.return_date","", "", "");

		if(StringUtil.isNotEmpty(_sLocID)) oSQL.append (" AND s.location_id = '").append(_sLocID).append("' ");
		if(StringUtil.isNotEmpty(_sSlsID)) oSQL.append (" AND s.sales_id = '").append(_sSlsID).append("' ");
		if(StringUtil.isNotEmpty(_sCusID)) oSQL.append (" AND s.customer_id = '").append(_sCusID).append("' ");

		oSQL.append (" GROUP BY ").append(sTimeUnit);
		if(_bByLoc) oSQL.append(", s.location_id ");
		if(_bByCus) oSQL.append(", s.customer_id ");
		if(_bBySls) oSQL.append(", s.sales_id ");

		oSQL.append (" ORDER BY ").append(sTimeUnit);

		log.debug ("getReturnPerMonth:" + oSQL.toString());
		List vData = SalesReturnPeer.executeQuery(oSQL.toString());	
		return vData;	
	}	
	
	public static List getNetSalesByTime (Date _dStart, 
			   							  Date _dEnd, 																 
			   							  boolean _bByLoc,
			   							  String  _sLocID,
			   							  boolean _bBySls,
			   							  String  _sSlsID,
			   							  boolean _bByCus,
			   							  String  _sCusID,																 
			   							  boolean _bByPmt,
			   							  boolean _bCashCredit,
			   							  int _iTimeUnit)
		throws Exception
	{		
		List vSI = getSalesByTime(_dStart,_dEnd,_bByLoc,_sLocID,_bBySls,_sSlsID,_bByCus,_sCusID,_bByPmt,_bCashCredit,_iTimeUnit);				
		List vSR = getReturnByTime(_dStart,_dEnd,_bByLoc,_sLocID,_bBySls,_sSlsID,_bByCus,_sCusID,_iTimeUnit);		
		for (int i = 0; i < vSI.size(); i++)
		{
			Record r = (Record) vSI.get(i);			
			String sUnt = r.getValue(1).asString(); //time unit
			BigDecimal bdSls = r.getValue("amt").asBigDecimal();
			String sLoc = ""; if(_bByLoc) sLoc = r.getValue("locid").asString();
			String sSls = ""; if(_bBySls) sSls = r.getValue("slsid").asString();
			String sCus = ""; if(_bByCus) sCus = r.getValue("cusid").asString();
			for (int j = 0; j < vSR.size(); j++)
			{
				Record r2 = (Record) vSR.get(j);
				String sRUnt = r2.getValue(1).asString();
				String sRLoc = ""; if(_bByLoc) sRLoc = r2.getValue("locid").asString();
				String sRSls = ""; if(_bBySls) sRSls = r2.getValue("slsid").asString();
				String sRCus = ""; if(_bByCus) sRCus = r2.getValue("cusid").asString();
				if(StringUtil.equals(sUnt, sRUnt) && StringUtil.equals(sLoc, sRLoc) && 
				   StringUtil.equals(sSls, sRSls) && StringUtil.equals(sCus, sRCus))
				{
					BigDecimal bdRet = r2.getValue("amt").asBigDecimal();
					r.setValue(2, bdSls.subtract(bdRet));					
				}					
			}
		}
		return vSI;
	}
	
	/**
	 * filter by month
	 * 
	 * @param _iMonth
	 * @param _sLocationID
	 * @param _vData
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getLocationTotalSales (int _iMonth, String _sLocationID, List _vData)
		throws Exception
	{					
		for (int i = 0; i < _vData.size(); i++)
		{
			Record oData = (Record) _vData.get(i);
			String sLocationID = oData.getValue(2).asString();	
				
			if (_sLocationID.equals(sLocationID) && (_iMonth == oData.getValue(1).asInt()) )
			{
				return oData.getValue(3).asBigDecimal();
			}
		}
		return bd_ZERO;
	}	

	public static BigDecimal sumLocationTotalSales (String _sLocationID, List _vData)
		throws Exception
	{			
	    double dTotal = 0;		
		for (int i = 0; i < _vData.size(); i++)
		{
			Record oData = (Record) _vData.get(i);
			String sLocationID = oData.getValue(2).asString();			
			if (_sLocationID.equals(sLocationID)) 
			{
				dTotal += oData.getValue(3).asDouble();
			}
		}
		return new BigDecimal (dTotal);
	}			

    public static BigDecimal sumLocationTotalSales (String _sLocationID, List _vCash, List _vCredit)
		throws Exception
	{
	    return sumLocationTotalSales (_sLocationID, _vCash, _vCredit, new ArrayList());
    }
    
	public static BigDecimal sumLocationTotalSales (String _sLocationID, 
													List _vCash, 
													List _vCredit, 
													List _vReturn)
		throws Exception
	{			
	    double dTotal = 0;		
		for (int i = 0; i < _vCash.size(); i++)
		{
			Record oData = (Record) _vCash.get(i);
			String sLocationID = oData.getValue(2).asString();			
			if (_sLocationID.equals(sLocationID)) 
			{
				dTotal += oData.getValue(3).asDouble();
			}
		}
		for (int i = 0; i < _vCredit.size(); i++)
		{
			Record oData = (Record) _vCredit.get(i);
			String sLocationID = oData.getValue(2).asString();			
			if (_sLocationID.equals(sLocationID)) 
			{
				dTotal += oData.getValue(3).asDouble();
			}
		}		
		for (int i = 0; i < _vReturn.size(); i++)
		{
			Record oData = (Record) _vReturn.get(i);
			String sLocationID = oData.getValue(2).asString();			
			if (_sLocationID.equals(sLocationID)) 
			{
				dTotal -= oData.getValue(3).asDouble();
			}
		}		
		return new BigDecimal(dTotal);
	}			
	
	
	/**
     * Method to get Item that most salesd based on quantity filter or total amount filter
     * 
     *
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _iLimit limit ranking
     * @param _iType order by
     * @return List of List
     */
	public static List getSalesPriceHistory(int _iCond, 
											String _sKey, 
											Date _dStart, 
											Date _dEnd,
											String _sKatID, 
											String _sCustomerID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.customer_id, s.customer_name, s.transaction_date, ");
		oSQL.append (" sd.item_id, sd.item_name, sd.item_code, s.invoice_no, sd.qty, sd.unit_code, ");
		oSQL.append (" sd.item_price, sd.discount, sd.sub_total, sd.sub_total_tax, s.location_id ");
		oSQL.append ("FROM sales_transaction s, sales_transaction_detail sd, item i");

		oSQL.append ("  WHERE s.sales_transaction_id = sd.sales_transaction_id AND i.item_id = sd.item_id ");
		if (_dStart != null) oSQL.append(" AND ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, "");
			        
	    if(StringUtil.isNotEmpty(_sKey))
	    {
	        oSQL.append (" AND "); 	        
	        if (_iCond == 1) oSQL.append(SqlUtil.like("i.item_code", _sKey));
	        if (_iCond == 2) oSQL.append(SqlUtil.like("i.item_name", _sKey));
	        if (_iCond == 3) oSQL.append(SqlUtil.like("i.description", _sKey));
	        if (_iCond == 4) oSQL.append(SqlUtil.like("i.item_sku", _sKey));
	        if (_iCond == 5) oSQL.append(SqlUtil.like("i.item_sku_name", _sKey));   	        
	    }	    
	    if (StringUtil.isNotEmpty(_sCustomerID))
	    {
	    	oSQL.append(" AND s.customer_id = '").append(_sCustomerID).append("'");
	    }	    
		if (StringUtil.isNotEmpty(_sKatID))
		{
			List vKategori = KategoriTool.getChildIDList(_sKatID);
			vKategori.add(_sKatID);
		    oSQL.append (" AND i.kategori_id IN ")
	    		.append (SqlUtil.convertToINMode(vKategori));		
		}
	    
		oSQL.append (" ORDER BY sd.sales_transaction_detail_id, s.transaction_date, sd.item_code ASC");
		log.debug (oSQL.toString());

		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			Map mRecord = new HashMap(12);
			
			mRecord.put("CustomerId", oData.getValue(1).asString());
			mRecord.put("CustomerName", oData.getValue(2).asString());
			mRecord.put("SalesInvoiceDate", oData.getValue(3).asDate());
			mRecord.put("ItemId", oData.getValue(4).asString());
			mRecord.put("ItemName", oData.getValue(5).asString());
			mRecord.put("ItemCode", oData.getValue(6).asString());
			mRecord.put("SalesInvoiceNo", oData.getValue(7).asString());
			mRecord.put("Qty", oData.getValue(8).asBigDecimal());
			mRecord.put("UnitCode", oData.getValue(9).asString());
			mRecord.put("ItemPrice", oData.getValue(10).asBigDecimal());
			mRecord.put("Discount", oData.getValue(11).asString());			
			mRecord.put("SubTotal", oData.getValue(12).asBigDecimal());
			mRecord.put("SubTotalTax", oData.getValue(13).asBigDecimal());
			mRecord.put("LocationId", oData.getValue(14).asString());
			
			vResult.add(mRecord); 
		}
		return vResult;	
	}
	
	public static List filterTrans (List _vSource, String _sValue, String _sKey)
		throws Exception
	{
	
		List vResult = (List)((ArrayList)_vSource).clone();
		Iterator oIter = vResult.iterator();
		while (oIter.hasNext()) 
		{
			Map mData = (Map) oIter.next();
			if (!mData.get(_sKey).equals(_sValue))
			{
				oIter.remove();
			}
		}
		return vResult;
	}
	
	private static boolean isInList (List _vResult, Map _mRecord, String _sKey)
		throws Exception
	{
		for (int i = 0; i < _vResult.size(); i++)
		{
			Map mData = (Map) _vResult.get(i);
			if (mData.get(_sKey).equals(_mRecord.get(_sKey)))
			{
				return true;
			}
		}
		return false;
	}
	
	public static List getDistinctList (List _vData, String _sKey)
		throws Exception
	{
		List vResult = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++)
		{
			Map mRecord = (Map) _vData.get(i); 
			if (!isInList (vResult, mRecord, _sKey))
			{
				vResult.add (mRecord);
			}
		}
		return vResult;
	}
	
	//-------------------------------------------------------------------------
	// ABC Z Analysis
	//-------------------------------------------------------------------------
	
	public static List getNoSalesItem(String _sKategoriID, String _sLocationID, Date _dStart, Date _dEnd, int _iExcludeDays)
		throws Exception
	{
		String sStart = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart));
		String sEnd = CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd));
		
		StringBuilder oSQL = new StringBuilder();
		oSQL.append("SELECT * FROM item i WHERE i.item_id NOT IN ")
		    .append(" (SELECT sd.item_id FROM sales_transaction_detail sd, sales_transaction s ")
		    .append("   WHERE sd.sales_transaction_id = s.sales_transaction_id AND s.status = 2 ")
		    .append("   AND s.transaction_date >= '").append(sStart).append("' ")
		    .append("   AND s.transaction_date <= '").append(sEnd).append("' ");    
		if(StringUtil.isNotEmpty(_sLocationID))
		{
			oSQL.append(" AND s.location_id = '").append(_sLocationID).append("' ");
		}
		oSQL.append(" )");
		List vKategori = KategoriTool.getChildIDList(_sKategoriID);
		vKategori.add(_sKategoriID);
		oSQL.append (" AND i.kategori_id IN ").append (SqlUtil.convertToINMode(vKategori));				
		if(_iExcludeDays > 0)
		{
			Date dBefore = DateUtil.addDays(new Date(), (_iExcludeDays * -1));
			String sBefore = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(dBefore));
			oSQL.append (" AND i.add_date <= '").append(sBefore).append("' ");					
		}		
		log.debug("getNoSalesItem: " + oSQL);
		return SqlUtil.executeQuery(oSQL.toString());
	}
}