package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesReturnDetailPeer;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.ReturnReasonTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryCostingTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.journal.SalesReturnJournalTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for SalesReturn and SalesReturnDetail OM
 * @see SalesInvoice, DeliveryOrder, SalesReturn, SalesReturnDetail 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesReturnTool.java,v 1.27 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesReturnTool.java,v $
 * 
 *  * 2016-02-01
 * - Fix bug in recalculate cost previously get cost from SI date.
 * 
 * </pre><br>
 */
public class SalesReturnTool extends BaseTool 
{
	private static Log log = LogFactory.getLog ( SalesReturnTool.class );

	public static final String s_ITEM = new StringBuilder(SalesReturnPeer.SALES_RETURN_ID).append(",")
	.append(SalesReturnDetailPeer.SALES_RETURN_ID).append(",")
	.append(SalesReturnDetailPeer.ITEM_ID).toString();

	public static final String s_TAX = new StringBuilder(SalesReturnPeer.SALES_RETURN_ID).append(",")
	.append(SalesReturnDetailPeer.SALES_RETURN_ID).append(",")
	.append(SalesReturnDetailPeer.TAX_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), SalesReturnPeer.RETURN_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), SalesReturnPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER	  ), SalesReturnPeer.CUSTOMER_ID);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUST_TYPE  ), SalesReturnPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), SalesReturnPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), SalesReturnPeer.USER_NAME);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), SalesReturnPeer.TRANSACTION_NO);             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), SalesReturnPeer.LOCATION_ID);             
		                        
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), SalesReturnPeer.PAYMENT_TYPE_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), "");
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), "");
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), SalesReturnPeer.CURRENCY_ID);
		m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), "");
		m_FIND_PEER.put (Integer.valueOf(i_SALESMAN   ), SalesReturnPeer.SALES_ID); 
		
		addInv(m_FIND_PEER, s_ITEM);
	}

	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), SalesReturnPeer.TRANSACTION_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), SalesReturnPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), SalesReturnPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), SalesReturnPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), SalesReturnPeer.USER_NAME);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), SalesReturnPeer.USER_NAME);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), SalesReturnPeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), SalesReturnPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), SalesReturnPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), SalesReturnPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), SalesReturnPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), SalesReturnPeer.SALES_ID); 
	}
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
	
	/**
	 * Create criteria for transaction date
	 * 
	 * @param _oCrit
	 * @param _dStart
	 * @param _dEnd
	 * @return modified criteria
	 */
	protected static Criteria transDateCriteria (Criteria _oCrit, Date _dStart, Date _dEnd)
	{        
        return transDateCriteria(_oCrit, SalesReturnPeer.RETURN_DATE, _dStart, _dEnd);
	}
	
	/**
	 * 
	 * @param _sID
	 * @return SalesReturn object
	 * @throws Exception
	 */
	public static SalesReturn getHeaderByID(String _sID) 
		throws Exception
	{
		return getHeaderByID(_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return SalesReturn object
	 * @throws Exception
	 */
	public static SalesReturn getHeaderByID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesReturnPeer.SALES_RETURN_ID, _sID);
        List vData = SalesReturnPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (SalesReturn) vData.get(0);
		}
		return null;
	}

	public static List getDetailsByID(String _sID) 
		throws Exception
	{
		return getDetailsByID(_sID, null);
	}	
	
	/**
	 * get details by ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of SalesReturnDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesReturnDetailPeer.SALES_RETURN_ID, _sID);
        oCrit.addAscendingOrderByColumn(SalesReturnDetailPeer.INDEX_NO);
		return SalesReturnDetailPeer.doSelect(oCrit, _oConn);
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return SalesReturn object
	 * @throws Exception
	 */
	public static SalesReturn getByTransID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesReturnPeer.TRANSACTION_ID, _sID);
        List vData = SalesReturnPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (SalesReturn) vData.get(0);
		}
		return null;
	}
	
	/**
	 * delete details by ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void deleteDetailsByID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesReturnDetailPeer.SALES_RETURN_ID, _sID);
		SalesReturnDetailPeer.doDelete(oCrit, _oConn);
	}	
	
	public static List getSalesReturnByTransID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesReturnPeer.TRANSACTION_ID, _sID);
        oCrit.add(SalesReturnPeer.STATUS, i_TRANS_PROCESSED);
        return SalesReturnPeer.doSelect(oCrit, _oConn);
	}

	public static boolean isReturned (String _sID, Connection _oConn)
		throws Exception
	{
		if (SalesReturnTool.getSalesReturnByTransID(_sID, _oConn).size() > 0)	
		{
			return true;
		}	
		return false;
	}

	public static double getTransReturnAmount(String _sID, Connection _oConn)
		throws Exception
	{
		double dTotal = 0;
		List vSR = SalesReturnTool.getSalesReturnByTransID(_sID, _oConn);
		for (int i = 0; i < vSR.size(); i++)
		{
			SalesReturn oSR = (SalesReturn) vSR.get(0);
			dTotal += oSR.getReturnedAmount().doubleValue();	
		}
		return dTotal;
	}
	
	public static SalesReturnDetail getDetailByDetailID(String _sDetailID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(SalesReturnDetailPeer.SALES_RETURN_DETAIL_ID, _sDetailID);
        List vData = SalesReturnDetailPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (SalesReturnDetail) vData.get(0);
		}
		return null;
	}		
	
	///////////////////////////////////////////////////////////////////////////
	// IMPORT METHODS
	///////////////////////////////////////////////////////////////////////////

	/**
     * Method to mapping sales return from DO
     * @param _oSO	DO Object
     * @param _oSR	Sales Return Object
     * @throws Exception
     */
    public static void mapDOToSR (DeliveryOrder _oTR, SalesReturn _oSR)
		throws Exception
	{	
		_oSR.setTransactionType		(i_RET_FROM_PR_DO);	
		_oSR.setTransactionId		(_oTR.getDeliveryOrderId	());
	    _oSR.setLocationId			(_oTR.getLocationId   		());
	    _oSR.setTransactionNo		(_oTR.getDeliveryOrderNo	());
	    _oSR.setTransactionDate		(_oTR.getDeliveryOrderDate  ());
	    _oSR.setCustomerId			(_oTR.getCustomerId			());
	    _oSR.setCustomerName		(_oTR.getCustomerName		());
	    _oSR.setTotalDiscountPct	("0");
	    _oSR.setCurrencyId			(_oTR.getCurrencyId			());
	    _oSR.setCurrencyRate		(_oTR.getCurrencyRate		());
	    _oSR.setPaymentTypeId		(_oTR.getPaymentTypeId		());
	    _oSR.setSalesId				(_oTR.getSalesId			());

	    _oSR.setTotalAmount			(bd_ZERO);
	    _oSR.setTotalDiscount		(bd_ZERO);
	    _oSR.setTotalQty			(bd_ZERO);
	    _oSR.setTotalTax			(bd_ZERO);
	    _oSR.setCreateCreditMemo	(true);
	    _oSR.setIsTaxable			(_oTR.getIsTaxable());
	    _oSR.setIsInclusiveTax		(_oTR.getIsInclusiveTax());
	}	

    /**
     * 
     * @param _oTD
     * @param _oSRD
     * @throws Exception
     */
    public static void mapDODetailToSRDetail (DeliveryOrderDetail _oTD, SalesReturnDetail _oSRD)
		throws Exception
	{
    	_oSRD.setTransactionDetailId	(_oTD.getDeliveryOrderDetailId());
    	_oSRD.setItemId					(_oTD.getItemId());
    	_oSRD.setItemCode				(_oTD.getItemCode());
    	_oSRD.setItemName				(_oTD.getItemName());
    	_oSRD.setUnitId					(_oTD.getUnitId());
    	_oSRD.setUnitCode				(_oTD.getUnitCode());
    	_oSRD.setQty					(bd_ZERO);
    	_oSRD.setQtyBase				(bd_ZERO);    	
    	_oSRD.setItemPrice				(bd_ZERO);
    	_oSRD.setItemCost				(_oTD.getCostPerUnit());
    	_oSRD.setTaxId					(_oTD.getTaxId());
    	_oSRD.setTaxAmount			    (bd_ZERO);
    	_oSRD.setDiscount				("0");
    	_oSRD.setDepartmentId			(_oTD.getDepartmentId());
    	_oSRD.setProjectId			    (_oTD.getProjectId());
	}    
    
	/**
     * Method to mapping sales return from SI
     * @param _oSI	SI Object
     * @param _oSR	Sales Return Object
     * @throws Exception
     */
    public static void mapSIToSR (SalesTransaction _oTR, SalesReturn _oSR)
		throws Exception
	{
		_oSR.setTransactionType		(i_RET_FROM_PI_SI);	
		_oSR.setTransactionId		(_oTR.getSalesTransactionId	());
	    _oSR.setLocationId			(_oTR.getLocationId   		());
	    _oSR.setTransactionNo		(_oTR.getInvoiceNo			());
	    _oSR.setTransactionDate		(_oTR.getTransactionDate    ());
	    _oSR.setCustomerId			(_oTR.getCustomerId			());
	    _oSR.setCustomerName		(_oTR.getCustomerName		());
	    _oSR.setTotalDiscountPct	(_oTR.getTotalDiscountPct	());
	    _oSR.setCurrencyId			(_oTR.getCurrencyId			());
	    _oSR.setCurrencyRate		(_oTR.getCurrencyRate		());
	    _oSR.setPaymentTypeId		(_oTR.getPaymentTypeId		());
	    _oSR.setSalesId				(_oTR.getSalesId			());

	    _oSR.setTotalAmount			(bd_ZERO);
	    _oSR.setTotalDiscount		(bd_ZERO);
	    _oSR.setTotalQty			(bd_ZERO);
	    _oSR.setTotalTax			(bd_ZERO);
	    
	    _oSR.setCreateCreditMemo	(true);	    
	    _oSR.setIsTaxable			(_oTR.getIsTaxable());
	    _oSR.setIsInclusiveTax		(_oTR.getIsInclusiveTax());
	    
	    PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(_oTR.getPaymentTypeId());
	    if (!oPT.getIsCredit())
	    {
	    	_oSR.setCashReturn(true);
	    }
	}    

    /**
     * 
     * @param _oTD
     * @param _oSRD
     * @throws Exception
     */
    public static void mapSIDetailToSRDetail ( SalesTransactionDetail _oTD, SalesReturnDetail _oSRD )
		throws Exception
	{
    	_oSRD.setTransactionDetailId	(_oTD.getSalesTransactionDetailId());
    	_oSRD.setItemId					(_oTD.getItemId());
    	_oSRD.setItemCode				(_oTD.getItemCode());
    	_oSRD.setItemName				(_oTD.getItemName());
    	_oSRD.setUnitId					(_oTD.getUnitId());
    	_oSRD.setUnitCode				(_oTD.getUnitCode());
    	_oSRD.setQty					(bd_ZERO);
    	_oSRD.setQtyBase				(bd_ZERO);    	
    	_oSRD.setItemPrice				(_oTD.getItemPrice());
    	_oSRD.setItemCost				(_oTD.getItemCost());
    	_oSRD.setTaxId					(_oTD.getTaxId());
    	_oSRD.setTaxAmount			    (_oTD.getTaxAmount());
    	_oSRD.setDiscount				(_oTD.getDiscount());
    	_oSRD.setDepartmentId			(_oTD.getDepartmentId());
    	_oSRD.setProjectId			    (_oTD.getProjectId());
	}       

	///////////////////////////////////////////////////////////////////////////
	// TRANS PROCESSING METHODS
	///////////////////////////////////////////////////////////////////////////

	public static double countQty (List _vDetails)
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesReturnDetail oDet = (SalesReturnDetail) _vDetails.get(i);
			dQty += oDet.getQty().doubleValue();
		}
		return dQty;
	}
	
	public static BigDecimal countTotalDisc(List _vDetail) 
	{
		double dTotalDisc = 0;
		for (int i = 0; i < _vDetail.size(); i++)
		{
			SalesReturnDetail oDetail = (SalesReturnDetail) _vDetail.get(i);
			if (oDetail.getQty().doubleValue() > 0)
			{
				dTotalDisc += oDetail.getSubTotalDisc().doubleValue();
			}
		}
		return new BigDecimal(dTotalDisc);
	}
	
	/**
	 * 
	 * @param _sDiscountPct
	 * @param _vDetails
	 * @param _oTR
	 * @return total tax
	 */
	public static BigDecimal countTax (String _sDiscountPct, List _vDetails, SalesReturn _oTR)
	{
	    double dTotalAmount = _oTR.getTotalAmount().doubleValue();
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesReturnDetail oDet = (SalesReturnDetail) _vDetails.get(i);
			double dSubTotal = oDet.getReturnAmount().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDiscountPct, dSubTotal);
			
			// if total amount
		    if(!_sDiscountPct.contains(Calculator.s_PCT))
		    {
		    	if (dTotalAmount > 0) //if dTotalAmount > 0, prevent divide by zero
		    	{
		    		dSubTotal = dSubTotal - (dDisc * (dSubTotal / dTotalAmount));
		    	}
		    }
		    else
		    {
		    	dSubTotal = dSubTotal - dDisc;
		    }
			
		    
		    double dSubTotalTax = 0;
			if(!_oTR.getIsInclusiveTax())
			{
			    dSubTotalTax = dSubTotal * oDet.getTaxAmount().doubleValue() / 100;
			}
			else
			{
			    //calculate inclusive Tax
			    dSubTotalTax = (dSubTotal * 100) / (oDet.getTaxAmount().doubleValue() + 100);
			    dSubTotalTax = dSubTotal - dSubTotalTax;  
			}

			oDet.setSubTotalTax(new BigDecimal(dSubTotalTax));
			dAmt += oDet.getSubTotalTax().doubleValue();
		}
		return new BigDecimal(dAmt).setScale(2, RoundingMode.HALF_UP);
	}
	
	public static BigDecimal countTotalAmount(List _vDetail) 
	{
		double dTotalAmount = 0;
		for (int i = 0; i < _vDetail.size(); i++)
		{
			SalesReturnDetail oDetail = (SalesReturnDetail) _vDetail.get(i);
			dTotalAmount += oDetail.getReturnAmount().doubleValue();
		}
		return new BigDecimal(dTotalAmount);
	}
		    
   	/**
   	 * set return with properties from RunData
   	 * @param _oSR
   	 * @param _vSRD
   	 * @param data
   	 * @throws Exception
   	 */
    public static void setHeaderProperties (SalesReturn _oSR, List _vSRD, RunData data)
    	throws Exception
    {
    	ValueParser formData = null;
		try
		{
			if (data != null)
			{				
				formData = data.getParameters();
				formData.setProperties (_oSR);
				
				String sEmpName = formData.getString("EmployeeName");
				String sSalesID = formData.getString("SalesId");
				if (StringUtil.isNotEmpty(sEmpName) && StringUtil.isEmpty(sSalesID))
				{
					_oSR.setSalesId(EmployeeTool.getIDByName(sEmpName));
				}
				
				//transaction date only set once from mapSItoSR
				_oSR.setReturnDate (CustomParser.parseDate(data.getParameters().getString("ReturnDate")));			
				_oSR.setDueDate    (CustomParser.parseDate(data.getParameters().getString("DueDate")));	
				_oSR.setIsInclusiveTax (data.getParameters().getBoolean("IsInclusiveTax"));
			}
			
			if (StringUtil.isEmpty(_oSR.getCustomerName()))
			{
				_oSR.setCustomerName(CustomerTool.getCustomerNameByID(_oSR.getCustomerId()));
			}
			if (_oSR.getTransactionDate() == null) _oSR.setTransactionDate (_oSR.getReturnDate());
			
			double dTotalItemDiscount = countTotalDisc(_vSRD).doubleValue();
			double dTotalAmount = countTotalAmount(_vSRD).doubleValue();

			double dInvoiceDiscount =  Calculator.calculateDiscount(_oSR.getTotalDiscountPct(), dTotalAmount);								
			double dTotalDiscount =  dInvoiceDiscount + dTotalItemDiscount;		
			
			_oSR.setTotalQty(new BigDecimal(countQty(_vSRD)));
			_oSR.setTotalAmount(new BigDecimal(dTotalAmount));
			_oSR.setTotalDiscount(new BigDecimal(dTotalDiscount));
			_oSR.setTotalTax(countTax(_oSR.getTotalDiscountPct(), _vSRD, _oSR));		    		

			
			double dReturnedAmount = dTotalAmount - dInvoiceDiscount;
			//count returned amount with tax
            if(!_oSR.getIsInclusiveTax())
            {
            	dReturnedAmount = dReturnedAmount + _oSR.getTotalTax().doubleValue();
            }
			_oSR.setReturnedAmount (new BigDecimal(dReturnedAmount));		    		

			if(PreferenceTool.posRoundingTo() > 0)
            {            	
				double dRounded = Calculator.roundNumber(dReturnedAmount, PreferenceTool.posRoundingTo(), PreferenceTool.posRoundingMode());						
				double dDelta = dRounded - dReturnedAmount;
				if(dDelta != 0)
				{
					log.debug("Rounding From : " + _oSR.getTotalAmount() + " TO: " + dRounded);	
				}
				_oSR.setRoundingAmount(new BigDecimal(dDelta));							
				_oSR.setReturnedAmount (new BigDecimal(dRounded));		    		
            }            
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new NestableException ("Set Return Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
    /**
     * update return detail list 
     * @param _vSRD
     * @param data
     * @throws Exception
     */
    public static void updateDetail (List _vSRD, RunData data)
    	throws Exception
    {
		//int iNo = data.getParameters().getInt("No", 0);
    	ParameterParser formData = data.getParameters();
		for (int i = 0; i < _vSRD.size(); i++)
		{
			SalesReturnDetail oSRD = (SalesReturnDetail) _vSRD.get(i);
			
			int j = i + 1;
			
			String sUnitID = data.getParameters().getString ("UnitId"+ j, "");
			String sUnitCode = data.getParameters().getString ("UnitCode"+ j, "");
			
			double dQty = data.getParameters().getDouble ("Qty" + j);
			double dQtyBase = UnitTool.getBaseQty(oSRD.getItemId(), sUnitID, dQty);

			oSRD.setTransactionDetailId (data.getParameters().getString ("TransactionDetailId"+ j,""));		 				
				
			oSRD.setItemId  	  ( formData.getString ("ItemId"      + j) );		 
			oSRD.setItemCode  	  ( formData.getString ("ItemCode"    + j) );		 
			oSRD.setItemName  	  ( formData.getString ("ItemName"    + j) );		 
			oSRD.setDiscountId    ( formData.getString ("DiscountId"  + j) );		 
			oSRD.setDiscount      ( formData.getString ("Discount"    + j) );
			oSRD.setTaxId    	  ( formData.getString ("TaxId"       + j) );		 
			
			oSRD.setItemCost	  ( formData.getBigDecimal ("ItemCost"     + j));		
			oSRD.setTaxAmount     ( formData.getBigDecimal ("TaxAmount"    + j));		
			
			oSRD.setSubTotalTax   ( formData.getBigDecimal ("SubTotalTax"  + j));
			oSRD.setSubTotalDisc  ( formData.getBigDecimal ("SubTotalDisc" + j));
			oSRD.setSubTotalCost  ( formData.getBigDecimal ("SubTotalCost" + j));

			oSRD.setUnitId  	  ( sUnitID);		 
			oSRD.setUnitCode      ( sUnitCode);

			oSRD.setQty			  ( new BigDecimal (dQty)  ); 				
			oSRD.setQtyBase		  ( new BigDecimal (dQtyBase)  ); 				
			oSRD.setItemPrice     ( new BigDecimal (formData.getDouble ("ItemPrice"    + j)));
			oSRD.setReturnAmount  ( new BigDecimal (formData.getDouble ("ReturnAmount" + j)));
		}
	}
    
    /**
     * count total item already returned
     * 
     * @param _vReturn
     * @param _sItemID
     * @return total returned qty
     * @throws Exception
     */
	private static double countTotalReturn (List _vReturn, String _sItemID) 
		throws Exception
	{
		List vID = new ArrayList (_vReturn.size());
		for (int i = 0; i < _vReturn.size(); i++)
		{
			SalesReturn oReturn = (SalesReturn) _vReturn.get(i);
			vID.add (oReturn.getSalesReturnId());
		}
		Criteria oCrit = new Criteria();
		oCrit.addIn (SalesReturnDetailPeer.SALES_RETURN_ID, vID);
		oCrit.add (SalesReturnDetailPeer.ITEM_ID, _sItemID);
		List vDetail = SalesReturnDetailPeer.doSelect (oCrit);
		double dTotalQty = 0;
		for (int i = 0; i < vDetail.size(); i++)
		{
			SalesReturnDetail oDet = (SalesReturnDetail) vDetail.get(i);
			dTotalQty += oDet.getQty().doubleValue();
		}
	    return dTotalQty;
	} 
	
	/**
	 * 
	 * @param _oSR
	 * @param _oConn
	 * @throws Exception
	 */
	private static void validateReturnedTrans (SalesReturn _oSR, List _vSRD, Connection _oConn)
		throws Exception
	{
		//check whether sales invoice / DO status is processed
		//this is needed so user cannot pending a sales return, cancel DO/SI then back to processed this
		if(_oSR.getTransactionType() == i_RET_FROM_PR_DO)
		{
			DeliveryOrder oTR = DeliveryOrderTool.getHeaderByID(_oSR.getTransactionId(), _oConn);
			if (oTR == null || oTR.getStatus() != i_TRANS_PROCESSED)
			{
				String sStatus = "";
				if (oTR != null)
				{
					sStatus = DeliveryOrderTool.getStatusString(oTR.getStatus());
					throw new NestableException ("DO status is not valid for return (" + sStatus + ")");
				}
				else
				{
					throw new NestableException ("DO " + _oSR.getTransactionNo() + " is not found !");					
				}	
			}		
		}
		if(_oSR.getTransactionType() == i_RET_FROM_PI_SI)
		{	
			SalesTransaction oTR = TransactionTool.getHeaderByID(_oSR.getTransactionId(), _oConn);
			if (oTR == null || oTR.getStatus() != i_TRANS_PROCESSED)
			{
				String sStatus = "";
				if (oTR != null)
				{
					sStatus = TransactionTool.getStatusString(oTR.getStatus());
					throw new NestableException ("SI status is not valid for return (" + sStatus + ")");
				}
				else
				{
					throw new NestableException ("SI " + _oSR.getTransactionNo() + " is not found !");					
				}
			}
			
			//validate cash Return & payment Type
			if(_oSR.getCashReturn())
			{
				PaymentType oPTP = PaymentTypeTool.getPaymentTypeByID(_oSR.getPaymentTypeId());
				if(oPTP.getIsCredit()) throw new Exception ("ERROR: Cash Return Selected while Payment Type is CREDIT!");				
			}
		}			
        if(_oSR.getStatus() == i_PENDING || _oSR.getStatus() == i_PROCESSED) //only validate return qty if processed
        {
            validateReturnedQty (_oSR, _vSRD, _oConn);
        }
	}
	
	/**
	 * 
	 * @param _oSR
	 * @param _vSRD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void validateReturnedQty (SalesReturn _oSR, List _vSRD, Connection _oConn)
		throws Exception
	{
		int iType = _oSR.getTransactionType();
		if (iType == i_RET_FROM_PR_DO || iType == i_RET_FROM_PI_SI)
		{
			//validate return qty again
			for (int i = 0; i < _vSRD.size(); i++)
			{
				SalesReturnDetail oDet = (SalesReturnDetail) _vSRD.get(i);
				double dTransQty = 0;
				double dCurrentReturn = oDet.getQty().doubleValue();
				double dReturnedQty = 0;
				if(_oSR.getTransactionType() == i_RET_FROM_PR_DO)
				{
					DeliveryOrderDetail oTD = 
						DeliveryOrderTool.getDetailByDetailID(oDet.getTransactionDetailId(), _oConn);
					dTransQty = oTD.getQty().doubleValue();
					dReturnedQty = oTD.getReturnedQty().doubleValue();
				}
				if(_oSR.getTransactionType() == i_RET_FROM_PI_SI)
				{
					SalesTransactionDetail oTD = 
						TransactionTool.getDetailByDetailID(oDet.getTransactionDetailId(), _oConn);
					dTransQty = oTD.getQty().doubleValue();
					dReturnedQty = oTD.getReturnedQty().doubleValue();
				}
				if (_oSR.getTransactionType() != i_RET_FROM_NONE)
				{
					if ((dCurrentReturn + dReturnedQty) > dTransQty)
					{
						throw new NestableException (
							" Item " + oDet.getItemCode() + " returned qty " + 
							CustomFormatter.formatNumber(Double.valueOf(dReturnedQty)) + " trans qty is " + 
							CustomFormatter.formatNumber(Double.valueOf(dTransQty))
						);
					}
				}
			}
		}			
	}	
	
	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_TRANS_PENDING)   return LocaleTool.getString("pending");
		if (_iStatusID == i_TRANS_PROCESSED) return LocaleTool.getString("processed");
		if (_iStatusID == i_TRANS_CANCELLED) return LocaleTool.getString("cancelled");
		return LocaleTool.getString("new");
	}		
	
	/**
	 * get the latest item cost for this invoice
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void recalculateCost (SalesReturn _oTR, List _vTD, Map _mGroup, Connection _oConn) 
		throws Exception
	{
		for ( int i = 0; i < _vTD.size(); i++ ) 
		{
			SalesReturnDetail oTD = (SalesReturnDetail) _vTD.get (i);
			Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
			if (oItem.getItemType() == i_INVENTORY_PART)
			{
				oTD.setItemCost(InventoryLocationTool.getItemCost(oTD.getItemId(), _oTR.getLocationId(), 
													  			  _oTR.getReturnDate(), _oConn));
			}
			else if (oItem.getItemType() == i_GROUPING)
			{
				List vGroup = ItemGroupTool.getItemGroupByGroupID(oTD.getItemId(), _oConn);
				double dGroupCost = 0;
				for (int j = 0; j < vGroup.size(); j++)
				{
					ItemGroup oGroupPart = (ItemGroup) vGroup.get(j);
					BigDecimal dCost = InventoryLocationTool.getItemCost(oGroupPart.getItemId(), 
							  											 _oTR.getLocationId(), 
							  											 _oTR.getReturnDate(), _oConn);
					dGroupCost += dCost.doubleValue();
					oGroupPart.setCost(dCost);
				}
				_mGroup.put(oTD.getItemId(), vGroup);
				oTD.setItemCost(new BigDecimal(dGroupCost));
			}
			oTD.setSubTotalCost(new BigDecimal(oTD.getItemCost().doubleValue() * oTD.getQtyBase().doubleValue()));
		}
	}	
	
	/**
	 * save return transaction
	 * 
	 * @param _oSR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (SalesReturn _oSR, List _vTD, Connection _oConn) 
		throws Exception 
	{	
		boolean bNew  = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans ();
			bStartTrans = true;
		}
		validate(_oConn);
		try 
		{
			if(StringUtil.isEmpty(_oSR.getSalesReturnId())) bNew = true;		
			
			//validate date
			validateDate(_oSR.getReturnDate(), _oConn);

			validateReturnedTrans (_oSR, _vTD, _oConn);
			
			Map mGroup = new HashMap();
			
			//recalculate cost
			recalculateCost(_oSR, _vTD, mGroup, _oConn);			
			
			//process save sales return Data
			processSaveSalesReturnData (_oSR, _vTD, _oConn);

			if (StringUtil.isNotEmpty(_oSR.getReturnNo()) && _oSR.getStatus() == i_TRANS_PROCESSED)
			{
				//update do
				if(_oSR.getTransactionType() == i_RET_FROM_PR_DO) 
				{
					DeliveryOrderTool.updateReturnQty(_vTD, false, _oConn);
				}
				
				//update si
				if(_oSR.getTransactionType() == i_RET_FROM_PI_SI)
				{	
					TransactionTool.updateReturnQty(_vTD, false, _oConn);
				}
				
				PointRewardTool.createPointRewardFromReturn(_oSR, _vTD, _oConn);
				
				List vInvTrans = 
					InventoryTransactionTool.getByTransID(_oSR.getSalesReturnId(),_oSR.getReturnNo(),i_INV_TRANS_SALES_RETURN, _oConn);
				
				if (vInvTrans.size() == 0)
				{
					//create inventory transaction and update
					InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
					oCT.createFromSR(_oSR, _vTD, mGroup);
				}
								
				if (PreferenceTool.getLocationFunction() != i_STORE)
				{
					//create Return Memo & GL
					if ((_oSR.getTransactionType() == i_RET_FROM_PI_SI || 
						 _oSR.getTransactionType() == i_RET_FROM_NONE) && 
						!_oSR.getCashReturn()) 
					{
						CreditMemoTool.createFromSalesReturn (_oSR, _oConn);
					}
					if (b_USE_GL)
					{
						SalesReturnJournalTool oReturnJournal = new SalesReturnJournalTool(_oSR, _oConn);
						oReturnJournal.createSalesReturnJournal(_oSR, _vTD, mGroup);                
					}
				}
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			_oSR.setStatus(i_TRANS_PENDING);
			_oSR.setReturnNo("");
			
			if (bNew)
			{
				_oSR.setNew(true);
				_oSR.setSalesReturnId("");
			}

			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			String sError = _oEx.getMessage(); 
			log.error (sError, _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}
	
	/**
	 * 
	 * @param _oSR
	 * @param _vSRD
	 * @param _oConn
	 * @param _bIsSync
	 * @throws Exception
	 */
	private static void processSaveSalesReturnData (SalesReturn _oSR, 
													List _vSRD, 
													Connection _oConn) 
		throws Exception
	{
		try 
		{
			if (StringUtil.isEmpty(_oSR.getSalesReturnId()))
			{
				_oSR.setSalesReturnId (IDGenerator.generateSysID());
				_oSR.setNew(true);
				validateID(getHeaderByID(_oSR.getSalesReturnId(),_oConn), "returnNo");				
			}
			
			//generate Return No
			if (_oSR.getReturnNo() == null) _oSR.setReturnNo("");			
			if (_oSR.getStatus() == i_TRANS_PROCESSED && StringUtil.isEmpty(_oSR.getReturnNo())) 
			{
				String sLocCode = "";
				if (!PreferenceTool.syncInstalled())
				{
					sLocCode = LocationTool.getLocationCodeByID(_oSR.getLocationId(), _oConn);
				}				
				_oSR.setReturnNo (
					LastNumberTool.generateByCustomer(_oSR.getCustomerId(),_oSR.getLocationId(),
						sLocCode,s_SR_FORMAT,LastNumberTool.i_SALES_RETURN, _oConn)	
				);

//				if different no needed between return from SI dan return from SO
//				if (_oSR.getTransactionType() == i_RET_FROM_NONE || _oSR.getTransactionType() == i_RET_FROM_PI_SI)
//				{
//				}			
//				else if(_oSR.getTransactionType() == i_RET_FROM_PR_DO)
//				{						
//				}
				validateNo(SalesReturnPeer.RETURN_NO,_oSR.getReturnNo(),SalesReturnPeer.class, _oConn);
			}
			
			_oSR.save (_oConn);
			
			renumber(_vSRD);
			deleteDetailsByID(_oSR.getSalesReturnId(), _oConn);
			Iterator oIter =  _vSRD.iterator();
			while (oIter.hasNext())
			{
				SalesReturnDetail oSRD = (SalesReturnDetail) oIter.next();
				oSRD.setNew (true);
				oSRD.setModified (true);
								
				//TODO -> if status == PROCESSED and qty > 0
				if (oSRD.getQty().doubleValue() > 0)
				{
					if (_oSR.getStatus() == i_PROCESSED)
					{
						//validate serial no
						ItemSerialTool.validateSerial(oSRD.getItemId(), oSRD.getQty().doubleValue(), oSRD.getSerialTrans(), _oConn);
					}
					
					//check & set cost if the item cost = 0, happens if this return is the first
					//inventory in transaction
					if (oSRD.getItem().getItemType() == i_INVENTORY_PART)
					{
						if(oSRD.getItemCost().doubleValue() <= 0)
						{
							Item oItem = ItemTool.getItemByID(oSRD.getItemId(), _oConn);
							BigDecimal dCost = InventoryLocationTool.getItemCost(oSRD.getItemId(), _oSR.getLocationId(), _oConn);
							if (dCost != null && dCost.doubleValue() <= 0)
							{
								dCost = oItem.getLastPurchasePrice();
							}
							double dSubTotal = dCost.doubleValue() * oSRD.getQtyBase().doubleValue();
							oSRD.setItemCost(dCost);
							oSRD.setSubTotalCost(new BigDecimal(dSubTotal));
						}
					}
					else
					{
						oSRD.setItemCost(bd_ZERO);
						oSRD.setSubTotalCost(bd_ZERO);
					}
					
					if (StringUtil.isEmpty(oSRD.getSalesReturnDetailId()))
					{
						oSRD.setSalesReturnDetailId (IDGenerator.generateSysID());	
					}
					oSRD.setSalesReturnId (_oSR.getSalesReturnId());	
					oSRD.save(_oConn);
				}
				else
				{
					oIter.remove();
				}
			}
		}
		catch (Exception _oEx) 
		{
			log.error (_oEx.getMessage(), _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * cancel sales return
	 * 
	 * @param _oSR
	 * @param _vTD
	 * @throws Exception
	 */
	public static void cancelReturn(SalesReturn _oSR, List _vTD, String _sCancelBy) 
		throws Exception 
	{	
		Connection oConn = beginTrans ();
		try 
		{	
			//validate date
			validateDate(_oSR.getReturnDate(), oConn);
			
			//update do
			if(_oSR.getTransactionType() == i_RET_FROM_PR_DO) 
			{
				DeliveryOrderTool.updateReturnQty(_vTD, true, oConn);
			}
			
			//update si
			if(_oSR.getTransactionType() == i_RET_FROM_PI_SI)
			{	
				TransactionTool.updateReturnQty(_vTD, true, oConn);
			}

			//update Point Reward
			PointRewardTool.deleteTrans(_oSR.getSalesReturnId(), oConn);
			
			//cancel credit memo
			CreditMemoTool.cancelFromSalesReturn (_oSR, _sCancelBy, oConn);

			//update inventory
			InventoryCostingTool oCT = new InventoryCostingTool(oConn);
			oCT.delete (_oSR, _oSR.getSalesReturnId(), i_INV_TRANS_SALES_RETURN);			
			
			if (b_USE_GL)
			{
				//update GL journal
				SalesReturnJournalTool.deleteJournal 
					(GlAttributes.i_GL_TRANS_SALES_RETURN, _oSR.getSalesReturnId(), oConn);                
			}
			_oSR.setRemark(cancelledBy(_oSR.getRemark(), _sCancelBy));			
			_oSR.setStatus(i_TRANS_CANCELLED);
			_oSR.save(oConn);
			commit (oConn);
		}
		catch (Exception _oEx)
		{
			_oSR.setStatus(i_TRANS_PROCESSED);
			
			rollback ( oConn );
			_oEx.printStackTrace();
			throw new NestableException(_oEx.getMessage(), _oEx);
		}
	}	

    public static void deleteTrans(String _sID)
        throws Exception
    {
        Connection oConn = null;
        try 
        {
            oConn = BaseTool.beginTrans();
            Criteria oCrit = new Criteria();
            oCrit.add(SalesReturnDetailPeer.SALES_RETURN_ID,_sID);
            SalesReturnDetailPeer.doDelete(oCrit,oConn);
            
            oCrit = new Criteria();
            oCrit.add(SalesReturnPeer.SALES_RETURN_ID,_sID);
            SalesReturnPeer.doDelete(oCrit,oConn);
            BaseTool.commit(oConn);
        }
        catch (Exception _oEx) 
        {
            BaseTool.rollback(oConn);            
            throw new NestableException("Delete Return Failed: " + _oEx.getMessage(), _oEx);
        }
    }
    
    /**
     * 
     * @param _iCond
     * @param _sKeywords
     * @param _dStartReturnDate
     * @param _dEndReturnDate
     * @param _sCustomerID
     * @param _sLocationID
     * @param _iStatus
     * @param _iLimit
     * @param _iGroupBy
     * @param _sCurrencyID
     * @param _sReasonID
     * @return
     * @throws Exception
     */
	public static LargeSelect findData ( int _iCond,
										 String _sKeywords, 
										 Date _dStartReturnDate, 
										 Date _dEndReturnDate, 
										 String _sCustomerID,
										 String _sLocationID,
										 int _iStatus,
										 int _iLimit,
										 int _iGroupBy,
										 String _sCurrencyID,
										 String _sReasonID) 
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
		   SalesReturnPeer.RETURN_DATE, _dStartReturnDate, _dEndReturnDate, _sCurrencyID);

		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesReturnPeer.CUSTOMER_ID, _sCustomerID);	
		}		
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(SalesReturnPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sReasonID)) 
		{
			oCrit.add(SalesReturnPeer.RETURN_REASON_ID, _sReasonID);	
		}
		if (_iStatus > 0) oCrit.add(SalesReturnPeer.STATUS, _iStatus);	
		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		else
		{
			oCrit.addAscendingOrderByColumn(SalesReturnPeer.RETURN_DATE);
		}
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.SalesReturnPeer");
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// HISTORY METHODS
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static List getCustomerReturnHistory (String _sCustomerID, 
												 String _sStartDate, 
												 String _sEndDate, 
												 String _sLocationID) 
    	throws Exception
	{		
		Date dStart = CustomParser.parseDate (_sStartDate);
		Date dEnd = CustomParser.parseDate (_sEndDate);
		
		if (dStart == null) dStart = new Date();
		if (dEnd == null) dEnd = new Date();		
		
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(SalesReturnPeer.LOCATION_ID, _sLocationID );		
    	}
        oCrit.add(SalesReturnPeer.CUSTOMER_ID, _sCustomerID );		         
        transDateCriteria (oCrit, dStart, dEnd);
        return SalesReturnPeer.doSelect(oCrit);
	}                          
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods                                                                     
	/////////////////////////////////////////////////////////////////////////////////////                               
	/**
	 * Simple Finder
	 *  
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sUserName
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List findTrans (Date _dStart, 
								  Date _dEnd, 
								  String _sLocationID,
								  String _sUserName, 
								  int _iStatus)
    	throws Exception
	{
		return findTrans (-1, "", _dStart, _dEnd, "", _sLocationID, _iStatus, "", _sUserName, "");
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _sVendorID
	 * @param _sUserName
	 * @param _sCurrencyID
	 * @return
	 * @throws Exception
	 */
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sUserName,
								  String _sCurrencyID) 
		throws Exception
	{
		return findTrans (_iCond,_sKeywords,_dStart,_dEnd,_sCustomerID,_sLocationID,_iStatus,_sVendorID,_sUserName,_sCurrencyID,"");
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _sVendorID
	 * @param _sUserName
	 * @param _sCurrencyID
	 * @return
	 * @throws Exception
	 */
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sUserName,
								  String _sCurrencyID,
								  String _sSalesID) 
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			SalesReturnPeer.RETURN_DATE, _dStart, _dEnd, _sCurrencyID);   
		
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesReturnPeer.CUSTOMER_ID, _sCustomerID);	
		}
		if(StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(SalesReturnPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sUserName))
		{
			oCrit.add(SalesReturnPeer.USER_NAME, _sUserName);
		}        
		if(_iStatus > 0)
		{
			oCrit.add(SalesReturnPeer.STATUS, _iStatus);
		}
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.addJoin(SalesReturnPeer.SALES_RETURN_ID,SalesReturnDetailPeer.SALES_RETURN_ID);
			oCrit.addJoin(SalesReturnDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.PREFERED_VENDOR_ID,_sVendorID);
		}	   
		if (StringUtil.isNotEmpty(_sSalesID))
		{
			oCrit.add(SalesReturnPeer.SALES_ID, _sSalesID);			
		}	   		
		log.debug(oCrit);
		return SalesReturnPeer.doSelect(oCrit);
	}
	
	public static List findCashierTrans (Date _dStart, Date _dEnd, String _sCashierName) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(SalesReturnPeer.STATUS, i_PROCESSED);
		oCrit.add(SalesReturnPeer.RETURN_DATE, _dStart, Criteria.GREATER_EQUAL);
		oCrit.and(SalesReturnPeer.RETURN_DATE, _dEnd, Criteria.LESS_EQUAL);
		if (StringUtil.isNotEmpty(_sCashierName))
		{
			oCrit.add(SalesReturnPeer.USER_NAME, _sCashierName);
		}
		log.debug(oCrit);
		return SalesReturnPeer.doSelect(oCrit);
	}
	
	//report method to group data by item
	public static SalesReturn filterTransByTransID (List _vTrans, String _sTransID) 
    	throws Exception
    { 

		SalesReturn oTrans = null;
		for (int i = 0; i < _vTrans.size(); i++) {
			oTrans = (SalesReturn) _vTrans.get(i);			
			if (oTrans.getSalesReturnId().equals(_sTransID)) {
				return oTrans;
			}
		}	
		return oTrans;
	}		
	
	public static List getTransDetails (List _vTrans) 
    	throws Exception
    { 
		List vTransID = new ArrayList (_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++) {
			SalesReturn oTrans = (SalesReturn) _vTrans.get(i);			
			vTransID.add (oTrans.getSalesReturnId());
		}		
		Criteria oCrit = new Criteria();
		if(vTransID.size()>0)
		{
		    oCrit.addIn (SalesReturnDetailPeer.SALES_RETURN_ID, vTransID);
		}
		return SalesReturnDetailPeer.doSelect(oCrit);
	}		

	public static List filterByCashReturn (List _vTrans, boolean _bCashReturn) 
		throws Exception
	{ 
	
		List vReturns = new ArrayList(_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++) 
		{
			SalesReturn oTrans = (SalesReturn) _vTrans.get(i);			
			if ((oTrans.getCashReturn() && _bCashReturn) || 
			    (!oTrans.getCashReturn() && !_bCashReturn)) 
			{
				vReturns.add(oTrans);
			}
		}	
		return vReturns;
	}	
	
	//-------------------------------------------------------------------------
	//return reason methods
	//------------------------------------------------------------------------- 
	public static ReturnReasonTool getReturnReasonTool()
	{
		return ReturnReasonTool.getInstance();
	}
	
	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
	
	/**
	 * get all sales return created in store since last store 2 ho
	 * 
	 * @return sales return created in store since last store 2 ho
	 * @throws Exception
	 */
	public static List getStoreSalesReturn ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate != null) 
		{
        	oCrit.add(SalesReturnPeer.RETURN_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);  
		}
		oCrit.add(SalesReturnPeer.STATUS, i_TRANS_PROCESSED);
		return SalesReturnPeer.doSelect(oCrit);
	}
		
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(SalesReturnPeer.class, SalesReturnPeer.SALES_RETURN_ID, _sID, _bIsNext);
	}	  	
}