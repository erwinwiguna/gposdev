package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.InvoicePaymentPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionDetailPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemMinPriceTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryCostingTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.journal.SalesJournalTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for SalesTransaction and SalesTransactionDetail OM
 * @see SalesOrderTool, DeliveryOrderTool, SalesTransaction, SalesTransactionDetail
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: TransactionTool.java,v 1.33 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * 2017-10-18
 * - Change method recalculateCost set ItemCost for Non Inventory Item with IsConsignment = true and 
 *   PreferedVendorId not empty
 * 
 * 2017-03-12
 * - Add method validateOverwrite, to prevent transaction overwriting another processed transaction, caused by 
 *   multiple window session / back button / or anything else
 * 
 * 2016-12-08
 * - Change if medical use normal SalesJournalTool,previously use MedSalesJournalTool 
 * - add overriding findCreditTrans method add salesman parameter
 * 
 * 2015-11-10
 * - change setHeaderProperties, add variable to detect request from POS Screen. If not from POS Screen allow input total discount 
 *   manually.
 * 
 * 2015-10-10
 * - change saveData, remove MedSalesJournalTool link. Medical SI Journal now invoked from MedicalSalesTool method saveData
 * 
 * 2015-09-17
 * - change findData (called by SalesTransactionAction and SalesReturnTransLookup) add parameter SalesID to filter by SalesID
 * 
 * 2015-04-10
 * - add findCreditTrans function with new parameter bIncPaid to query payment processed but paid 
 * after end date
 *  
 * </pre><br>
 */
public class TransactionTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( TransactionTool.class );
	
	public static final String s_MD = new StringBuilder(SalesTransactionPeer.SALES_TRANSACTION_ID).append(",")
	.append(SalesTransactionDetailPeer.SALES_TRANSACTION_ID).toString();
	
	public static final String s_ITEM = new StringBuilder(s_MD).append(",")
	.append(SalesTransactionDetailPeer.ITEM_ID).toString();

	public static final String s_TAX = new StringBuilder(s_MD).append(",")
	.append(SalesTransactionDetailPeer.TAX_ID).toString();
	
	public static final String s_DEPT = new StringBuilder(s_MD).append(",")
	.append(SalesTransactionDetailPeer.DEPARTMENT_ID).toString();

	public static final String s_PRJ = new StringBuilder(s_MD).append(",")
	.append(SalesTransactionDetailPeer.PROJECT_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), SalesTransactionPeer.INVOICE_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), SalesTransactionPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER), 	 SalesTransactionPeer.CUSTOMER_ID);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUST_TYPE  ), SalesTransactionPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), SalesTransactionPeer.CASHIER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), SalesTransactionPeer.CASHIER_NAME);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), SalesTransactionPeer.LOCATION_ID);             
		                        
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), SalesTransactionPeer.PAYMENT_TYPE_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), SalesTransactionPeer.PAYMENT_TERM_ID);
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), SalesTransactionPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), SalesTransactionPeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), SalesTransactionPeer.CURRENCY_ID);
		m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), "");
		m_FIND_PEER.put (Integer.valueOf(i_SALESMAN   ), SalesTransactionPeer.SALES_ID);             
		m_FIND_PEER.put (Integer.valueOf(i_SO_NO   	  ), SalesTransactionDetailPeer.SALES_ORDER_ID);             
		m_FIND_PEER.put (Integer.valueOf(i_DO_NO   	  ), SalesTransactionDetailPeer.DELIVERY_ORDER_ID);             
		
		addInv(m_FIND_PEER, s_ITEM);
		
		m_FIND_PEER.put (Integer.valueOf(i_DEPARTMENT ), s_DEPT);
		m_FIND_PEER.put (Integer.valueOf(i_PROJECT    ), s_PRJ);
	}   

	protected static Map m_GROUP_PEER = new HashMap();
	
	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), SalesTransactionPeer.TRANSACTION_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), SalesTransactionPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), SalesTransactionPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), SalesTransactionPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), SalesTransactionPeer.CASHIER_NAME);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), "");               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), SalesTransactionPeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), SalesTransactionPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), SalesTransactionPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), SalesTransactionPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), SalesTransactionPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), SalesTransactionPeer.SALES_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_TYPE  ), SalesTransactionPeer.CUSTOMER_ID); 
		
	}
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
	
	public static SalesTransaction getHeaderByID(String _sID) 
    	throws Exception
		
    {
		return getHeaderByID (_sID, null);
    }
	
	/**
	 * get header by ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return SalesTransaction object
	 * @throws Exception
	 */
	public static SalesTransaction getHeaderByID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(SalesTransactionPeer.SALES_TRANSACTION_ID, _sID);
	    List vData = SalesTransactionPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (SalesTransaction) vData.get(0);
		}
		return null;	
	}

	public static List getDetailsByID(String _sID) 
		throws Exception
	{
		return getDetailsByID(_sID, null);
	}
	
	/**
	 * get details by id
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of SalesTransactionDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, _sID);
        oCrit.addAscendingOrderByColumn(SalesTransactionDetailPeer.INDEX_NO);
		return SalesTransactionDetailPeer.doSelect(oCrit, _oConn);
	}
	

	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @return SalesTransactionDetail
	 * @throws Exception
	 */
	public static SalesTransactionDetail getDetailByDetailID(String _sID) 
		throws Exception
	{
	    return getDetailByDetailID(_sID, null) ;
	}	

	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @return SalesTransactionDetail
	 * @throws Exception
	 */
	public static SalesTransactionDetail getDetailByDetailID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_DETAIL_ID, _sID);
	    List vData = SalesTransactionDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) return (SalesTransactionDetail) vData.get(0);
	    return null;
	}		
	
	/**
	 * get SalesTransactionDetail by DeliveryOrderDetail ID
	 * 
	 * @param _sDODetID DeliveryOrderDetailId
	 * @return SalesTransactionDetail 
	 * @throws Exception
	 */
	public static SalesTransactionDetail getDetailByDODetailID(String _sDODetID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(SalesTransactionDetailPeer.DELIVERY_ORDER_DETAIL_ID, _sDODetID);
	    oCrit.add(SalesTransactionPeer.STATUS, i_TRANS_PROCESSED);	    
	    oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID, SalesTransactionDetailPeer.SALES_TRANSACTION_ID);	    
	    List vData = SalesTransactionDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) return (SalesTransactionDetail) vData.get(0);
	    return null;
	}
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, _sID);
		SalesTransactionDetailPeer.doDelete (oCrit, _oConn);
	}
	
	/**
	 * 
	 * @param _sDOID
	 * @param _oConn
	 * @return List of SalesTransactionDetail
	 * @throws Exception
	 */
	public static List getDetailsByDOID(String _sDOID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesTransactionDetailPeer.DELIVERY_ORDER_ID, _sDOID);
        oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID, SalesTransactionDetailPeer.SALES_TRANSACTION_ID);
        oCrit.add(SalesTransactionPeer.STATUS, i_PROCESSED);
		return SalesTransactionDetailPeer.doSelect(oCrit,_oConn);
	}

	/**
	 * 
	 * @param _sInvoiceNo
	 * @return SalesTransactionId
	 * @throws Exception
	 */
	public static String getIDByInvoiceNo(String _sInvoiceNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(SalesTransactionPeer.INVOICE_NO, 
				(Object) SqlUtil.like (SalesTransactionPeer.INVOICE_NO, _sInvoiceNo), Criteria.CUSTOM);
		oCrit.setLimit(1);
		List vData = SalesTransactionPeer.doSelect (oCrit);
		if (vData.size() > 0) 
		{
			return ((SalesTransaction) vData.get(0)).getSalesTransactionId();
		}
		return "";
	}
	
	/**
	 * update print times indicate how many times this invoice printed
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
    public static void updatePrintTimes (SalesTransaction _oTR) 
		throws Exception
	{
		_oTR.setPrintTimes (_oTR.getPrintTimes() + 1);
		_oTR.save();
	}

	/**
	 * check whether a DO is already invoiced
	 * 
	 * @param _sDOID
	 * @return is DO invoiced
	 * @throws Exception
	 */
	public static boolean isInvoiced(String _sDOID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(SalesTransactionPeer.STATUS, i_TRANS_CANCELLED, Criteria.NOT_EQUAL);
		oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID,SalesTransactionDetailPeer.SALES_TRANSACTION_ID);
	    oCrit.add(SalesTransactionDetailPeer.DELIVERY_ORDER_ID, _sDOID);
		List vData = SalesTransactionDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}
    
    
	///////////////////////////////////////////////////////////////////////////
	// import data from DO
	///////////////////////////////////////////////////////////////////////////
	
    /**
     * map DO Detail to SI Detail
     * 
     * @param _vDOD
     * @param _vSID
     * @param _sSOID
     * @param _sCustomerID
     * @throws Exception
     */
	public static void mapDODetailToSIDetail (List _vDOD, List _vSID, String _sSOID, String _sCustomerID)
		throws Exception
	{

		for (int i = 0; i < _vDOD.size(); i++)
    	{
    		DeliveryOrderDetail oDOD  	 = (DeliveryOrderDetail) _vDOD.get(i);
    		SalesTransactionDetail oSID  = new SalesTransactionDetail();
    		oSID.setSalesOrderId		  (_sSOID);
        	oSID.setDeliveryOrderId		  (oDOD.getDeliveryOrderId());
        	oSID.setDeliveryOrderDetailId (oDOD.getDeliveryOrderDetailId());
        	oSID.setItemId       		  (oDOD.getItemId());
        	oSID.setItemCode       		  (oDOD.getItemCode());
        	oSID.setItemName       		  (oDOD.getItemName());
        	oSID.setDescription       	  (oDOD.getDescription());
        	
        	Item oItem = ItemTool.getItemByID(oDOD.getItemId());
        	oSID.setDescription  (oItem.getDescription());
        	oSID.setUnitId		 (oDOD.getUnitId());
        	oSID.setUnitCode	 (oDOD.getUnitCode());
	       	oSID.setTaxId		 (oItem.getTaxId());
        	oSID.setTaxAmount	 (TaxTool.getAmountByID(oSID.getTaxId()));
	       	
        	double dQty = oDOD.getQty().doubleValue();
        	double dReturned = oDOD.getReturnedQty().doubleValue();
        	
        	//only invoiced non-returned qty
        	double dSIQty = dQty - dReturned;
        	oSID.setQty	(new BigDecimal (dSIQty));
	       	
	       	if(StringUtil.isEmpty(oDOD.getSalesOrderDetailId()))
	       	{
	       	    oItem = ItemTool.findItemByCode(oSID.getItemCode(),_sCustomerID,"");
        	    oSID.setItemPrice  (oItem.getItemPrice());
        	    oSID.setDiscount   (oItem.getDiscountAmount());
	        }
	        else
	        {
        	    oSID.setItemPrice  (oDOD.getItemPriceInSo());
        	    oSID.setDiscount   (oDOD.getDiscount());
        	}
        	oSID.setDiscountId     ("");
        	oSID.setTaxId		   (oDOD.getTaxId());
        	oSID.setTaxAmount	   (oDOD.getTaxAmount());
        	
        	double dTotalAmount = (oSID.getQty().doubleValue() * oSID.getItemPrice().doubleValue());
        	double dTotalDisc = Calculator.calculateDiscount(oSID.getDiscount(),dTotalAmount);
        	dTotalAmount = dTotalAmount - dTotalDisc;
        	double dTotalTax = (oSID.getTaxAmount().doubleValue()/100 * dTotalAmount);
            
        	oSID.setSubTotalTax		(new BigDecimal(dTotalTax));
        	oSID.setSubTotalDisc	(new BigDecimal(dTotalDisc));
        	oSID.setSubTotal		(new BigDecimal(dTotalAmount));
        	oSID.setItemCost		(oDOD.getCostPerUnit());
		    
        	double dItemCostTotal    = oSID.getItemCost().doubleValue() * oSID.getQty().doubleValue();  
		    
        	//get Temporary Cost then update when invoice saved
		    oSID.setSubTotalCost	(new BigDecimal(dItemCostTotal));
        	oSID.setDepartmentId	(oDOD.getDepartmentId());
        	oSID.setProjectId		(oDOD.getProjectId());
		    
        	_vSID.add(oSID);
    	}
	}
	//END from DO  methods    

	public static void mapSODetailToSIDetail (List _vSOD, List _vSID, String _sSOID, String _sCustomerID)
		throws Exception
	{
	
		for (int i = 0; i < _vSOD.size(); i++)
		{
			SalesOrderDetail oDOD = (SalesOrderDetail) _vSOD.get(i);
			SalesTransactionDetail oSID  = new SalesTransactionDetail();
			oSID.setSalesOrderId		  (_sSOID);
	    	oSID.setDeliveryOrderId		  (oDOD.getSalesOrderId());
	    	oSID.setDeliveryOrderDetailId (oDOD.getSalesOrderDetailId());
	    	oSID.setItemId       		  (oDOD.getItemId());
	    	oSID.setItemCode       		  (oDOD.getItemCode());
	    	oSID.setItemName       		  (oDOD.getItemName());
	    	oSID.setDescription       	  (oDOD.getDescription());
	    	
	    	Item oItem = ItemTool.getItemByID(oDOD.getItemId());
	    	oSID.setDescription  (oItem.getDescription());
	    	oSID.setUnitId		 (oDOD.getUnitId());
	    	oSID.setUnitCode	 (oDOD.getUnitCode());
	       	oSID.setTaxId		 (oItem.getTaxId());
	    	oSID.setTaxAmount	 (TaxTool.getAmountByID(oSID.getTaxId()));
	       	
	    	double dQty = oDOD.getQty().doubleValue();
	    	//double dReturned = oDOD.getReturnedQty().doubleValue();
	    	
	    	//only invoiced non-returned qty
	    	double dSIQty = dQty;
	    	oSID.setQty	(new BigDecimal (dSIQty));
	       	
	       	if(StringUtil.isEmpty(oDOD.getSalesOrderDetailId()))
	       	{
	       	    oItem = ItemTool.findItemByCode(oSID.getItemCode(),_sCustomerID,"");
	    	    oSID.setItemPrice  (oItem.getItemPrice());
	    	    oSID.setDiscount   (oItem.getDiscountAmount());
	        }
	        else
	        {
	    	    oSID.setItemPrice  (oDOD.getItemPrice());
	    	    oSID.setDiscount   (oDOD.getDiscount());
	    	}
	    	oSID.setDiscountId     ("");
	    	oSID.setTaxId		   (oDOD.getTaxId());
	    	oSID.setTaxAmount	   (oDOD.getTaxAmount());
	    	
	    	double dTotalAmount = (oSID.getQty().doubleValue() * oSID.getItemPrice().doubleValue());
	    	double dTotalDisc = Calculator.calculateDiscount(oSID.getDiscount(),dTotalAmount);
	    	dTotalAmount = dTotalAmount - dTotalDisc;
	    	double dTotalTax = (oSID.getTaxAmount().doubleValue()/100 * dTotalAmount);
	        
	    	oSID.setSubTotalTax		(new BigDecimal(dTotalTax));
	    	oSID.setSubTotalDisc	(new BigDecimal(dTotalDisc));
	    	oSID.setSubTotal		(new BigDecimal(dTotalAmount));
	    	oSID.setItemCost		(oDOD.getCostPerUnit());
		    
	    	double dItemCostTotal    = oSID.getItemCost().doubleValue() * oSID.getQty().doubleValue();  
		    
	    	//get Temporary Cost then update when invoice saved
		    oSID.setSubTotalCost	(new BigDecimal(dItemCostTotal));
	    	oSID.setDepartmentId	(oDOD.getDepartmentId());
	    	oSID.setProjectId		(oDOD.getProjectId());
		    
	    	_vSID.add(oSID);
		}
	}
		
	///////////////////////////////////////////////////////////////////////////
	////transaction processing method
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * validate whether sales price < min selling price
	 * 
	 * @param _oTR
	 * @param _oTD
	 * @throws Exception
	 */
	public static void validatePrice(SalesTransaction _oTR, SalesTransactionDetail _oTD)
		throws Exception
    {
    	if(b_MIN_SALES_PRICE_USE && _oTR != null && _oTD != null)
    	{
    		validatePrice(_oTD.getItemId(), _oTD.getItemPrice().doubleValue(), _oTD.getDiscount(), _oTR.getLocationId(), _oTR.getCustomerId());    		
    	}
    }
	
    /**
     * validate whether sales price < min selling price
     * 
     * @param _sItemID
     * @param _dPrice
     * @throws Exception
     */
    public static void validatePrice(String _sItemID, double _dPrice, String _sDisc, String _sLocID, String _sCustID)
		throws Exception
    {
    	if (b_MIN_SALES_PRICE_USE)
    	{
    		double dMinPrice = ItemMinPriceTool.getMinPrice(_sItemID, _sLocID, _sCustID);    
    		if (dMinPrice == 0)
    		{
    			Item oItem = ItemTool.getItemByID(_sItemID);
    			if (oItem != null && oItem.getMinPrice() != null) 
    			{
    				dMinPrice = oItem.getMinPrice().doubleValue();
    			}
    		}
    		if (StringUtil.isNotEmpty(_sDisc))
    		{
    			double dDisc = Calculator.calculateDiscount(_sDisc, _dPrice);
    			_dPrice = _dPrice - dDisc;
    		}
    		if (_dPrice < dMinPrice) 
    		{
    			throw new Exception (LocaleTool.getString("sales_price") + " < Minimum (" + CustomFormatter.formatNumber(dMinPrice) + ")");
    		}
    	}
    }	
	
	/**
	 * set header properties from screen entry
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param data
	 * @param _iIsImportDO
	 * @throws Exception
	 */
	public static void setHeaderProperties (SalesTransaction _oTR, List _vTD, RunData data) 
    	throws Exception
    {
		ValueParser formData = null;
		try 
		{
			boolean bPOSScreen = false;
			if (data != null)
			{
				if(StringUtil.containsIgnoreCase(data.getScreenTemplate(),"pos"))
				{
					bPOSScreen = true;
				}
				formData = data.getParameters();
				formData.setProperties (_oTR);
				
				String sEmpName = formData.getString("EmployeeName");
				String sSalesID = formData.getString("SalesId");
				if (StringUtil.isNotEmpty(sEmpName) && StringUtil.isEmpty(sSalesID))
				{
					_oTR.setSalesId(EmployeeTool.getIDByName(sEmpName));
				}
	    		_oTR.setTransactionDate (CustomParser.parseDate(formData.getString("TransactionDate")));    		
	    		_oTR.setIsTaxable  		(formData.getBoolean("IsTaxable", false));
	    		_oTR.setIsInclusiveTax  (formData.getBoolean("IsInclusiveTax", false));
			}
			
    		//TODO: totalQty is not currently having valid value if multi unit used
    		_oTR.setTotalQty 	  (new BigDecimal(countQty(_vTD)));
    		_oTR.setTotalCost	  (countCost 	(_vTD));
			_oTR.setTotalAmount   (countSubTotal (_vTD));
    		_oTR.setTotalDiscount (countDiscount (_vTD));
    		
    		setDueDate (_oTR);
			//count total amount after total discount percentage

    		String sTotalDiscPct = _oTR.getTotalDiscountPct();
            String sTotalDiscID  = _oTR.getTotalDiscountId();            
    		
            double dTotalDiscAmt = 0;
            double dTotalAmount = _oTR.getTotalAmount().doubleValue();

    		//Check for promo discount by total
    		Discount oDisc = DiscountTool.findTotalDiscount(_oTR);   
    		if(oDisc != null)
    		{
    			sTotalDiscPct = DiscountTool.getTotalDiscAmount(oDisc, dTotalAmount);
    			sTotalDiscID = oDisc.getDiscountId();
    			_oTR.setDiscountData(oDisc);
    		}
    		else 
    		{   
    			if(bPOSScreen) sTotalDiscPct = "0";
    			sTotalDiscID = "";
    			_oTR.setDiscountData(null);
    		}
			_oTR.setTotalDiscountPct(sTotalDiscPct);
			_oTR.setTotalDiscountId(sTotalDiscID);    		            
			_oTR.setTotalTax(countTax(sTotalDiscPct,_vTD,_oTR));
			
			if (StringUtil.isNotEmpty(sTotalDiscPct)) 
			{
				//get discount for all 
				//count total discount from total amount
				dTotalDiscAmt = Calculator.calculateDiscount(sTotalDiscPct,dTotalAmount);
				double dSubTotalDiscount = _oTR.getTotalDiscount().doubleValue();
				_oTR.setTotalDiscount (new BigDecimal(dTotalDiscAmt + dSubTotalDiscount));
				_oTR.setTotalAmount (new BigDecimal(dTotalAmount - dTotalDiscAmt));							
			}			

            //count Total Amount with tax
            if(!_oTR.getIsInclusiveTax())
            {
            	dTotalAmount = _oTR.getTotalAmount().doubleValue() + _oTR.getTotalTax().doubleValue();
            	_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
            }
            
            //if freight cost > 0 and INCLUDE TO Amount add freight cost to total amount
            if(_oTR.getTotalExpense().doubleValue() > 0 && _oTR.getIsInclusiveFreight())
            {
            	dTotalAmount = _oTR.getTotalAmount().doubleValue() + _oTR.getTotalExpense().doubleValue();
            	_oTR.setTotalAmount (new BigDecimal(dTotalAmount));
            }
            
            if(PreferenceTool.posRoundingTo() > 0)
            {            	
				double dRounded = Calculator.roundNumber(_oTR.getTotalAmount(), 
					PreferenceTool.posRoundingTo(), PreferenceTool.posRoundingMode());						
				double dDelta = dRounded - _oTR.getTotalAmount().doubleValue();
				if(dDelta != 0)
				{
					log.debug("Rounding From : " + _oTR.getTotalAmount() + " TO: " + dRounded);	
				}
				_oTR.setRoundingAmount(new BigDecimal(dDelta));
				_oTR.setTotalAmount(new BigDecimal(dRounded));			
            }
            if (data != null)
            {
               //init / setPayment
			    InvoicePaymentTool.setPayment(_oTR, data); 
            }
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }

	/**
	 * update detail properties
	 * 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 */
    public static void updateDetail (List _vDet, RunData data)
    	throws Exception
    {
    	ValueParser formData = data.getParameters();
    	boolean bIsIncTax = data.getParameters().getBoolean("IsInclusiveTax");
		try
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				SalesTransactionDetail oTD = (SalesTransactionDetail) _vDet.get(i);
				
				//log.debug ("Before update : "  + oDetail );
				//check null to prevent refresh from user that didn't submit param from data
				if (formData.getString("Discount" + (i + 1)) != null)
				{				
					oTD.setQty          ( formData.getBigDecimal("Qty"          + (i + 1)) );
					oTD.setQtyBase 		( UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
					oTD.setItemPrice    ( formData.getBigDecimal("ItemPrice"    + (i + 1)) );
					oTD.setTaxId        ( formData.getString    ("TaxId"        + (i + 1)) );
					oTD.setTaxAmount    ( formData.getBigDecimal("TaxAmount"    + (i + 1)) );
					oTD.setDiscount     ( formData.getString	("Discount"     + (i + 1)) );
				}
				//oTD.setSubTotalTax  ( formData.getBigDecimal("SubTotalTax"  + (i + 1)) );
				//oTD.setSubTotalDisc ( formData.getBigDecimal("SubTotalDisc" + (i + 1)) );
				//oTD.setSubTotal     ( formData.getBigDecimal("SubTotal"     + (i + 1)) );				
                
				BigDecimal bdSubTotal = oTD.getItemPrice().multiply(oTD.getQty());
				BigDecimal bdSubDisc = new BigDecimal(Calculator.calculateDiscount(oTD.getDiscount(), bdSubTotal));
				bdSubTotal = bdSubTotal.subtract(bdSubDisc);
				BigDecimal bdSubTax = bd_ZERO;
				if(bIsIncTax)
				{							
					bdSubTax = bdSubTotal.subtract(new BigDecimal(bdSubTotal.doubleValue() / ((100 + oTD.getTaxAmount().doubleValue()) / 100)));
				}
				else
				{
					bdSubTax = new BigDecimal(bdSubTotal.doubleValue() * (oTD.getTaxAmount().doubleValue() / 100));
				}	
				
				oTD.setSubTotalTax(bdSubTax);
				oTD.setSubTotalDisc(bdSubDisc);
				oTD.setSubTotal(bdSubTotal);
				
				//log.debug ("After update : "  + oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Update Detail Failed : " + _oEx.getMessage (), _oEx); 
		}
    }	
    
    /**
     * count total qty 
     * @param _vDetails
     * @return total qty
     */
	public static double countQty (List _vDetails) 
		throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			SalesTransactionDetail oDet = (SalesTransactionDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty()));
			}
			//the best we can do is count the base qty
			dQty += oDet.getQtyBase().doubleValue();
		}
		return dQty;
	}
	
	/**
	 * count qty per item
	 * 
	 * @param _vDetails
	 * @param _sItemID
	 * @return total qty
	 */
	public static double countQty (List _vDetails, String _sItemID)
	{
		return countQtyPerItem (_vDetails, _sItemID, "qty");
	}
	
	/**
	 * 
	 * @param _sDiscountPct
	 * @param _vDetails
	 * @param _oTR
	 * @return total tax
	 */
	public static BigDecimal countTax (String _sDiscountPct, List _vDetails, SalesTransaction _oTR)
	{
	    double dTotalAmount = _oTR.getTotalAmount().doubleValue();
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesTransactionDetail oDet = (SalesTransactionDetail) _vDetails.get(i);
			double dSubTotal = oDet.getSubTotal().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDiscountPct, dSubTotal);
			
			// if total amount
		    if(!_sDiscountPct.contains(Calculator.s_PCT))
		    {
		    	if (dTotalAmount > 0) //if dTotalAmount > 0, prevent divide by zero
		    	{
		    		dSubTotal = dSubTotal - (dDisc * (dSubTotal / dTotalAmount));
		    	}
		    }
		    else
		    {
		    	dSubTotal = dSubTotal - dDisc;
		    }
		    
		    double dSubTotalTax = 0;
			if(!_oTR.getIsInclusiveTax())
			{
			    dSubTotalTax = dSubTotal * oDet.getTaxAmount().doubleValue() / 100;
			}
			else
			{
			    //calculate inclusive Tax
			    dSubTotalTax = (dSubTotal * 100) / (oDet.getTaxAmount().doubleValue() + 100);
			    dSubTotalTax = dSubTotal - dSubTotalTax;  
			}

			oDet.setSubTotalTax(new BigDecimal(dSubTotalTax));
			dAmt += oDet.getSubTotalTax().doubleValue();
		}
		return new BigDecimal(dAmt);
	}
	
	/**
	 * 
	 * @param _vDetails
	 * @return sum of SubTotalDisc
	 */
	public static BigDecimal countDiscount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			SalesTransactionDetail oDet = (SalesTransactionDetail) _vDetails.get(i);
			dAmt += oDet.getSubTotalDisc().doubleValue();
		}
		return new BigDecimal(dAmt);
	}
	
	/**
	 * 
	 * @param _vDetails
	 * @return sum of SubTotalCost
	 * @throws Exception
	 */
	public static BigDecimal countCost (List _vDetails) 
		throws Exception
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			SalesTransactionDetail oDet = (SalesTransactionDetail) _vDetails.get(i);
			double dCost = oDet.getItemCost().doubleValue();
			double dQtyBase = oDet.getQty().doubleValue() * UnitTool.getBaseValue(oDet.getItemId(), oDet.getUnitId());
			double dSubTotalCost = dCost * dQtyBase;
			oDet.setSubTotalCost(new BigDecimal(dSubTotalCost));

			dAmt += dSubTotalCost;
		}
		return new BigDecimal(dAmt);
	}
	
	/**
	 * 
	 * @param _vDetails
	 * @return sum of SubTotal
	 */
	public static BigDecimal countSubTotal (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			SalesTransactionDetail oDet = (SalesTransactionDetail) _vDetails.get(i);
			
			double dPrice = oDet.getItemPrice().doubleValue();
			String sDisc = oDet.getDiscount();
			double dSubTotal = oDet.getQty().doubleValue() * dPrice;
			double dSubTotalDisc = Calculator.calculateDiscount(sDisc, dSubTotal, oDet.getQty());
			dSubTotal = dSubTotal - dSubTotalDisc;
			oDet.setSubTotalDisc(new BigDecimal(dSubTotalDisc));			
			oDet.setSubTotal(new BigDecimal(dSubTotal));
				
			dAmt += dSubTotal;
		}
		return new BigDecimal(dAmt);
	}

	
	/**
	 * set Payment Due Date
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
	private static void setDueDate (SalesTransaction _oTR) 
		throws Exception 
	{
		boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oTR.getPaymentTypeId());
		if (bIsCredit) 
		{ 
			if (_oTR.getTransactionDate() != null)
			{
				//check default payment term for this customer
				String sDefaultTerm = CustomerTool.getDefaultTermByID(_oTR.getCustomerId(), null);
				int iDueDays = 0;
				if (StringUtil.isNotEmpty(sDefaultTerm) && 
						PaymentTermTool.isCashPaymentTerm(_oTR.getPaymentTermId())) 
				{
					iDueDays = PaymentTermTool.getNetPaymentDaysByID (sDefaultTerm);
					_oTR.setPaymentTermId(sDefaultTerm);
				}
				else 
				{
				    iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oTR.getPaymentTermId());
				}
				Calendar oCal = new GregorianCalendar ();
				oCal.setTime (_oTR.getTransactionDate());
				oCal.add (Calendar.DATE, iDueDays);
				_oTR.setDueDate (oCal.getTime());		
			}
		}
		else
		{
			if (_oTR.getTransactionDate() != null)
			{
				_oTR.setDueDate(_oTR.getTransactionDate());
			}
			else
			{				
				_oTR.setDueDate(new Date());
			}
		}
	}
	
	/**
	 * generate transaction no
	 * 
	 * @param _oTR
	 * @param _oConn
	 * @return transaction no
	 * @throws Exception
	 */
	private static String generateTransactionNo (SalesTransaction _oTR, Connection _oConn) 
		throws Exception
	{
		String sTransNo = "";
		String sLocCode = "";
		if (!PreferenceTool.syncInstalled())
		{
			sLocCode = LocationTool.getLocationCodeByID(_oTR.getLocationId(), _oConn);
		}

		int iType = LastNumberTool.i_SALES_INVOICE;
		String sFormat = s_SI_FORMAT;		
		if (TransactionAttributes.b_SALES_SEPARATE_TAX_INV && _oTR.getTotalTax().intValue() > 0)
		{
			iType = LastNumberTool.i_TAX_INVOICE;
			sFormat = s_TX_FORMAT;
		}
		sTransNo = LastNumberTool.generateByCustomer(_oTR.getCustomerId(),_oTR.getLocationId(),sLocCode,sFormat,iType,_oConn);
		return sTransNo;
	}
	
	/**
	 * get the latest item cost for this invoice
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void recalculateCost (SalesTransaction _oTR, List _vTD, Map _mGroup, Connection _oConn) 
		throws Exception
	{
		for ( int i = 0; i < _vTD.size(); i++ ) 
		{
			SalesTransactionDetail oTD = (SalesTransactionDetail) _vTD.get (i);
			Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
			if (oItem != null)
			{
				if((StringUtil.empty(oTD.getDeliveryOrderId()) &&         //!FROM DO
					StringUtil.empty(oTD.getDeliveryOrderDetailId())) ||                
				   (!StringUtil.empty(oTD.getDeliveryOrderDetailId()) &&  // FROM SO
					 StringUtil.equals(oTD.getDeliveryOrderId(),oTD.getSalesOrderId())) ) //IF FROM DO DO NOT RECALCULATE COST BASED ON INVOICE
				{
					if(oItem.getItemType() == i_INVENTORY_PART)
					{
			        	oTD.setItemCost(InventoryLocationTool
			        		.getItemCost(oTD.getItemId(), _oTR.getLocationId(), _oTR.getTransactionDate(), _oConn));				
					}
					else if (oItem.getItemType() == i_GROUPING)
					{				
						List vGroup = ItemGroupTool.getItemGroupByGroupID(oTD.getItemId(), _oConn);
						double dGroupCost = 0;
						for (int j = 0; j < vGroup.size(); j++)
						{
							ItemGroup oGroupPart = (ItemGroup) vGroup.get(j);
							BigDecimal dCost = InventoryLocationTool.getItemCost(oGroupPart.getItemId(), 
									  											 _oTR.getLocationId(), 
									  											 _oTR.getTransactionDate(), _oConn);
							
							dGroupCost += (dCost.doubleValue() * oGroupPart.getQty().doubleValue());
							oGroupPart.setCost(dCost);
						}
						_mGroup.put(oTD.getItemId(), vGroup);
						oTD.setItemCost(new BigDecimal(dGroupCost));
					}
					else if (oItem.getItemType() == i_NON_INVENTORY_PART)
					{				
						if(oItem.getIsConsignment() && StringUtil.isNotEmpty(oItem.getPreferedVendorId()))
						{

						}
					}
				}
				else //SI FROM DO, TODO: consider update cost from DO Detail
				{
					
				}				
			}
			else
			{
				StringBuilder sMsg = new StringBuilder();
				sMsg.append("Recalculate Cost, Item with ID : ").append(oTD.getItemId()) 
				  	.append(" CODE : ").append(oTD.getItemCode()).append(" NOT FOUND !");
				throw new NestableException(sMsg.toString());
			}
			oTD.setSubTotalCost(new BigDecimal(oTD.getItemCost().doubleValue() * oTD.getQtyBase().doubleValue()));	
		}
		_oTR.setTotalCost(countCost(_vTD));		
	}

	///////////////////////////////////////////////////////////////////////
	// Save Transaction
	///////////////////////////////////////////////////////////////////////

	/**
	 * validate whether it's overlapping another processed transaction 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void validateOverwrite(String _sTransID, Connection _oConn)
		throws Exception
	{
		SalesTransaction oExist = getHeaderByID(_sTransID, _oConn);
		if(oExist != null && oExist.getStatus() == i_TRANS_PROCESSED) 
		{
			throw new Exception("Invalid Data, Try to Overwrite Transaction No: " + oExist.getInvoiceNo() + ", Please Create a New Transaction!");
		}
	}
	
	public static void saveData (SalesTransaction _oTR, List _vTD, List _vPayment)
		throws Exception 
	{
		saveData (_oTR, _vTD, _vPayment, null);
	} 

	/**
	 * Save Sales Invoice Data
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _vPayment
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (SalesTransaction _oTR, 
								 List _vTD, 
								 List _vPayment, 
								 Connection _oConn) 
		throws Exception 
	{	
		//set database connection
		boolean bNew = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		validate(_oConn);

		try 
		{
			if(StringUtil.isEmpty(_oTR.getSalesTransactionId())) 
			{
				bNew = true;		
			}
			else
			{
				validateOverwrite(_oTR.getSalesTransactionId(), _oConn);
			}
			if (_oTR.getStatus () == i_TRANS_PROCESSED && StringUtil.isEmpty(_oTR.getInvoiceNo())) 
			{
				//validate credit limit
				CreditLimitValidator.validateSI(_oTR);

				_oTR.setInvoiceNo (generateTransactionNo(_oTR, _oConn));
				validateNo(SalesTransactionPeer.INVOICE_NO,_oTR.getInvoiceNo(),SalesTransactionPeer.class,_oConn);				
			
				//check for back date
				_oTR.setTransactionDate(checkBackDate(_oTR.getTransactionDate(), i_INV_OUT));
			}
						
			//validate date towards period
			validateDate (_oTR.getTransactionDate(), _oConn);
			
			Map mGroup = new HashMap();						
			if (StringUtil.isNotEmpty(_oTR.getInvoiceNo()) && _oTR.getStatus () == i_TRANS_PROCESSED) 
			{
				//recalculate cost
				recalculateCost(_oTR, _vTD, mGroup, _oConn);
			}
			
			//process save sales Data
			processSaveSalesData (_oTR, _vTD, _oConn);
			
			//if transaction has been saved, status is processed and invoice number has been generated
			if (StringUtil.isNotEmpty(_oTR.getInvoiceNo()) && _oTR.getStatus () == i_TRANS_PROCESSED) 
			{
				//update inventory
				//InventoryTransactionTool.createFromSalesInvoice (_oTR, _vTD, mGroup, _oConn);
				InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
				oCT.createFromSI(_oTR, _vTD, mGroup);
				
				//process payment & AR
				InvoicePaymentTool.processPayment (_oTR, _vPayment, _oConn);
				
				//update DO if exist
				DeliveryOrderTool.updateDOFromInvoice(_vTD, false, _oConn);

				//update SO if exist
				SalesOrderTool.updateSOFromInvoice(_vTD, false, false, _oConn);
				
				//apply coupon 
				DiscountTool.createCoupon(_oTR, _oConn);
				
				//apply point reward
				PointRewardTool.createPointRewardFromSales(_oTR, _vTD, _vPayment, _oConn);
				
				if (b_USE_GL && PreferenceTool.getLocationFunction() != i_STORE)
				{
					///create GL Journal only if medical not installed
					//if(!PreferenceTool.medicalInstalled())
					//{
						SalesJournalTool oSalesJournal = new SalesJournalTool(_oTR, _oConn);
						oSalesJournal.createSalesJournal (_oTR, _vTD, _vPayment, mGroup);						
					//}
				}
			}
			if (bStartTrans) 
			{
			    commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			_oTR.setStatus(i_TRANS_PENDING);
			_oTR.setInvoiceNo("");
			
			if (bNew)
			{
				_oTR.setNew(true);
				_oTR.setSalesTransactionId("");
			}
			
			if (bStartTrans) 
			{
				rollback (_oConn);
			}			
			log.error (_oEx.getMessage(), _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}
	
	/**
	 * process save
	 * <li>generate trans no
	 * <li>delete all pending details
	 * <li>iterate and save details
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveSalesData (SalesTransaction _oTR, List _vTD, Connection _oConn) 
		throws Exception
	{
		validate(_oConn);
		//check whether transaction is paid and invoice no is not generated yet
		//invoice no must be checked for synchronization process
		try 
		{
			//generate Sys ID if this is not a previous pending trans or sync trans
			if (StringUtil.isEmpty(_oTR.getSalesTransactionId())) 
			{
				_oTR.setSalesTransactionId (IDGenerator.generateSysID());
				_oTR.setNew(true);
			}
			
			validateID (getHeaderByID(_oTR.getSalesTransactionId(), _oConn), "invoiceNo");
			
			if (_oTR.getInvoiceNo() == null) _oTR.setInvoiceNo ("");

			//delete all existing detail from pending transaction
			if (StringUtil.isNotEmpty(_oTR.getSalesTransactionId()))
			{
				deleteDetailsByID (_oTR.getSalesTransactionId(), _oConn);
			}

	        _oTR.setPaymentStatus(i_PAYMENT_OPEN);
			_oTR.save (_oConn);
			
			SalesTransactionDetail oTD = null;
			
			renumber(_vTD);
			for ( int i = 0; i < _vTD.size(); i++ ) 
			{
				oTD = (SalesTransactionDetail) _vTD.get (i);
				oTD.setModified (true);
				oTD.setNew (true);				
				if (_oTR.getStatus() == i_PROCESSED && StringUtil.isEmpty(oTD.getDeliveryOrderDetailId()))
				{
					//validate serial no
					ItemSerialTool.validateSerial(oTD.getItemId(), oTD.getQty().doubleValue(), oTD.getSerialTrans(), _oConn);
				}
				//TODO: test validate price necessary or not
				//validatePrice(_oTR, oTD);
				
				//Generate SYS Data
				if (StringUtil.isEmpty(oTD.getSalesTransactionDetailId()))
				{
					oTD.setSalesTransactionDetailId (IDGenerator.generateSysID());
				}
				oTD.setSalesTransactionId (_oTR.getSalesTransactionId());
				oTD.save (_oConn);				
			}
		}
		catch (Exception _oEx)
		{
			String sError = "Process Save Failed : " + _oEx.getMessage();
			throw new NestableException (sError, _oEx);
		}
	}
	
    ///////////////////////////////////////////////////////////////////////
    // Save Transaction
    ///////////////////////////////////////////////////////////////////////
    
    public static void saveEdit (SalesTransaction _oTR, List _vTD, List _vPayment, String _sEditBy)
        throws Exception 
    {
        //set database connection

        boolean bNew = false;
        boolean bStartTrans = true;
        Connection oConn = beginTrans();
        validate(oConn);
        try 
        {
            if(_oTR != null && _vTD != null)
            {
                String sRemark = _oTR.getRemark();
                cancelSales(_oTR, _vTD, "", oConn);
                deleteTrans(_oTR.getSalesTransactionId(),oConn);
                
                StringBuilder oSB = new StringBuilder();
                oSB.append(sRemark)
                   .append("\nEdited By: ").append(_sEditBy)
                   .append("\nEdit Date: ").append(CustomFormatter.formatDateTime(new Date()));
                
                _oTR.setStatus(i_PROCESSED);
                _oTR.setRemark(oSB.toString());
                _oTR.setNew(true);
                saveData(_oTR,_vTD,_vPayment,oConn);
            }            
            commit (oConn);
        }
        catch (Exception _oEx) 
        {            
            rollback (oConn);
            log.error (_oEx.getMessage(), _oEx);
            throw new NestableException (_oEx.getMessage(), _oEx);
        }
    } 
    
	/**
	 * update invoice paid amount
	 * 
	 * @param _sID
	 * @param _dPaymentDate
	 * @param _dPaidAmount
	 * @param _dDPAmount
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updatePaidAmount(String _sID, 
										Date _dPaymentDate, 
										double _dPaidAmount, 
										double _dDPAmount, 
										Connection _oConn)
		throws Exception
	{
		SalesTransaction oSI= getHeaderByID(_sID, _oConn);
		if (oSI != null) 
		{			
			Currency oCurr = CurrencyTool.getCurrencyByID(oSI.getCurrencyId(), _oConn);
			double dTotalAmount = oSI.getTotalAmount().doubleValue();
			if (!oCurr.getIsDefault()) {				
				//TODO: warning check this, validate when do Receive Payment in non Default
				//dTotalAmount = dTotalAmount - oSI.getTotalTax().doubleValue();
			}

			double dPaidAmount = oSI.getPaidAmount().doubleValue() + _dPaidAmount + _dDPAmount;
			oSI.setPaidAmount (new BigDecimal(dPaidAmount));
			if(Calculator.precise(dPaidAmount) > Calculator.precise(dTotalAmount))
			{
				String sMsg = " SI " + oSI.getInvoiceNo() + " Total Paid Amount " + CustomFormatter.fmt(dPaidAmount) + 
							  " Greater Than Total Amount " + CustomFormatter.fmt(dTotalAmount);					  
				throw new Exception (sMsg);					
			}			
			if (Calculator.precise(dPaidAmount) >= Calculator.precise(dTotalAmount))
			{
				oSI.setPaymentDate(_dPaymentDate);
				oSI.setPaymentStatus(i_PAYMENT_PAID);
			}
			else if (Calculator.precise(dPaidAmount) < Calculator.precise(dTotalAmount)) //for cancel
			{ 
				oSI.setPaymentDate(null);
				oSI.setPaymentStatus(i_PAYMENT_OPEN);				
			}
			oSI.save(_oConn);
		}
	}

	/**
	 * update SI returned qty 
	 * 
	 * @param vSRD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateReturnQty(List vSRD, boolean _bCancel, Connection _oConn)
		throws Exception
	{
		 try
		 {
			for(int i = 0; i < vSRD.size(); i++)
			{
				SalesReturnDetail oSRD = (SalesReturnDetail) vSRD.get(i);
				SalesTransactionDetail oSID = getDetailByDetailID(oSRD.getTransactionDetailId(), _oConn);
				if (oSID != null)
				{
					double dReturnQty = oSRD.getQty().doubleValue();
					if (_bCancel) dReturnQty = dReturnQty * -1;
					double dLastReturnedQty = oSID.getReturnedQty().doubleValue();
					oSID.setReturnedQty(new BigDecimal(dLastReturnedQty + dReturnQty));
					oSID.save(_oConn);
				}
			}
		 }
		 catch(Exception _oEx)
		 {
		 	_oEx.printStackTrace();
		 	throw new NestableException ("Update Returned Qty Failed, ERROR : ", _oEx);
		 }
	}	
	
	/**
	 * Finder used by sales transaction view
	 * 
	 * @param _sInvNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _iLimit
	 * @param _sCashierName
	 * @return query result in LargeSelect
	 * @throws Exception
	 */
	public static LargeSelect findData ( int _iCond,
									     String _sKeywords,
										 Date _dStart, 
										 Date _dEnd, 
								         String _sCustomerID, 
								         String _sLocationID, 
								         int _iStatus, 
								         int _iPaymentStatus,
								         int _iLimit,
										 int _iGroupBy,			
										 String _sSalesID,
								         String _sCashierName,								         
										 String _sCurrencyID) 
    	throws Exception
    {
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	SalesTransactionPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);    	
		
        if (_iCond == i_SO_NO && StringUtil.isNotEmpty(_sKeywords))
		{
			oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID, SalesTransactionDetailPeer.SALES_TRANSACTION_ID);				
			oCrit.addJoin(SalesTransactionDetailPeer.SALES_ORDER_ID, SalesOrderPeer.SALES_ORDER_ID);
			oCrit.add(SalesOrderPeer.SALES_ORDER_NO, (Object)_sKeywords ,Criteria.ILIKE);		
		}
		if (_iCond == i_DO_NO && StringUtil.isNotEmpty(_sKeywords))
		{
			oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID, SalesTransactionDetailPeer.SALES_TRANSACTION_ID);				
			oCrit.addJoin(SalesTransactionDetailPeer.DELIVERY_ORDER_ID, DeliveryOrderPeer.DELIVERY_ORDER_ID);
			oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_NO, (Object)_sKeywords ,Criteria.ILIKE);		
		}
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesTransactionPeer.CUSTOMER_ID, _sCustomerID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sCashierName)) 
		{
			oCrit.add(SalesTransactionPeer.CASHIER_NAME, _sCashierName);	
		}
		if (StringUtil.isNotEmpty(_sSalesID)) 
		{
			oCrit.add(SalesTransactionPeer.SALES_ID, _sSalesID);	
		}
		if (_iStatus > 0) 
		{
			oCrit.add(SalesTransactionPeer.STATUS, _iStatus);	
		}		
		if (_iPaymentStatus > 0) 
		{
			oCrit.add(SalesTransactionPeer.PAYMENT_STATUS, _iPaymentStatus);	
		}		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}		
		else
		{
			oCrit.addAscendingOrderByColumn(SalesTransactionPeer.TRANSACTION_DATE);
		}
		log.debug(oCrit);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.SalesTransactionPeer");
	}
	
	/**
	 * find invoice to pay in receivable payment
	 * 
	 * @param _sInvNo
	 * @param _dStart
	 * @param _dDueDate
	 * @param _sCustomerID
	 * @param _sPaymentTypeID
	 * @param _sCurrencyID
	 * @return List of credit trans
	 * @throws Exception
	 */
	public static List findCreditTrans (String _sInvNo, 
									    Date _dStart,
									    Date _dEnd,
										Date _dDueStart, 
										Date _dDueEnd, 										
								        String _sCustomerID, 
								        String _sLocationID,
										String _sPaymentTypeID, 
										String _sCurrencyID,
										boolean _bTax) 
    	throws Exception
    {
		return findCreditTrans(_sInvNo, _dStart, _dEnd, _dDueStart, _dDueEnd, 
							   _sCustomerID, "", _sLocationID, _sPaymentTypeID, _sCurrencyID, _bTax);
    }
	
	/**
	 * find invoice to pay in receivable payment
	 * 
	 * @param _sInvNo
	 * @param _dStart
	 * @param _dDueDate
	 * @param _sCustomerID
	 * @param _sPaymentTypeID
	 * @param _sCurrencyID
	 * @return List of credit trans
	 * @throws Exception
	 */
	public static List findCreditTrans (String _sInvNo, 
									    Date _dStart,
									    Date _dEnd,
										Date _dDueStart, 
										Date _dDueEnd, 										
								        String _sCustomerID, 
								        String _sCustomerTypeID,
								        String _sLocationID,
										String _sPaymentTypeID, 
										String _sCurrencyID,
										boolean _bTax) 
    	throws Exception
    {
		return findCreditTrans(_sInvNo, _dStart, _dEnd, _dDueStart, _dDueEnd, 
							   _sCustomerID, _sCustomerTypeID, _sLocationID, _sPaymentTypeID, _sCurrencyID, _bTax, false);
    }

	public static List findCreditTrans (String _sInvNo, 
									    Date _dStart,
									    Date _dEnd,
										Date _dDueStart, 
										Date _dDueEnd, 										
								        String _sCustomerID, 
								        String _sCustomerTypeID,
								        String _sLocationID,
										String _sPaymentTypeID, 
										String _sCurrencyID,
										boolean _bTax,
										boolean _bIncPaid) 
		throws Exception
	{
		return findCreditTrans(_sInvNo, _dStart, _dEnd, _dDueStart, _dDueEnd, 
					_sCustomerID, _sCustomerTypeID, _sLocationID, _sPaymentTypeID, _sCurrencyID, "", _bTax, _bIncPaid);	
	}
	
	/**
	 * find invoice to pay in receivable payment also used in open invoice report
	 * 
	 * @param _sInvNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _dDueStart
	 * @param _dDueEnd
	 * @param _sCustomerID
	 * @param _sCustomerTypeID
	 * @param _sLocationID
	 * @param _sPaymentTypeID
	 * @param _sCurrencyID
	 * @param _bTax
	 * @param _bIncPaid - include paid invoice but payment date > _dEnd
	 * @return
	 * @throws Exception
	 */
	public static List findCreditTrans (String _sInvNo, 
									    Date _dStart,
									    Date _dEnd,
										Date _dDueStart, 
										Date _dDueEnd, 										
								        String _sCustomerID, 
								        String _sCustomerTypeID,
								        String _sLocationID,
										String _sPaymentTypeID, 
										String _sCurrencyID,
										String _sSalesmanID,
										boolean _bTax,
										boolean _bIncPaid) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		
		if (StringUtil.isNotEmpty(_sInvNo)) 
		{
			oCrit.add(SalesTransactionPeer.INVOICE_NO, 
				(Object) SqlUtil.like (SalesTransactionPeer.INVOICE_NO, _sInvNo), Criteria.CUSTOM);
		}
		
		transDateCriteria (oCrit, _dStart, _dEnd);

		oCrit.add(SalesTransactionPeer.STATUS, i_PROCESSED);
		
		if (_dDueStart != null) 
		{
			oCrit.add(SalesTransactionPeer.DUE_DATE, DateUtil.getStartOfDayDate(_dDueStart), Criteria.GREATER_EQUAL);
		}		
		if (_dDueEnd != null) 
		{
			oCrit.and(SalesTransactionPeer.DUE_DATE, DateUtil.getEndOfDayDate(_dDueEnd), Criteria.LESS_EQUAL);			
		}
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesTransactionPeer.CUSTOMER_ID, _sCustomerID);	
		}		
		if (StringUtil.isNotEmpty(_sCustomerTypeID)) 
		{
			oCrit.addJoin(SalesTransactionPeer.CUSTOMER_ID, CustomerPeer.CUSTOMER_ID);	
			oCrit.add(CustomerPeer.CUSTOMER_TYPE_ID, _sCustomerTypeID);
		}
		if (StringUtil.isNotEmpty(_sSalesmanID)) 
		{
			oCrit.add(SalesTransactionPeer.SALES_ID, _sSalesmanID);	
		}		
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocationID);	
		}		
		if (StringUtil.isNotEmpty(_sPaymentTypeID)) 
		{
			oCrit.add(SalesTransactionPeer.PAYMENT_TYPE_ID, _sPaymentTypeID);	
		}
		if (StringUtil.isNotEmpty(_sCurrencyID)) 
		{
			oCrit.add(SalesTransactionPeer.CURRENCY_ID, _sCurrencyID);	
		}
		if (!_bTax)
		{
			if(!_bIncPaid)
			{
				oCrit.add(SalesTransactionPeer.PAYMENT_STATUS, i_PAYMENT_OPEN);
			}
			else
			{
				Criteria.Criterion oPmtOpen = oCrit.getNewCriterion(SalesTransactionPeer.PAYMENT_STATUS, i_PAYMENT_OPEN, Criteria.EQUAL);
				Criteria.Criterion oPmtProc = oCrit.getNewCriterion(SalesTransactionPeer.PAYMENT_STATUS, i_PAYMENT_PROCESSED, Criteria.EQUAL);
				Criteria.Criterion oPmtDate = oCrit.getNewCriterion(SalesTransactionPeer.PAYMENT_DATE, _dEnd, Criteria.GREATER_EQUAL); 						
				oCrit.add(oPmtProc.and(oPmtDate));
				oCrit.or(oPmtOpen);
			}
		}
		else
		{
			Criteria.Criterion oPmtProc = oCrit.getNewCriterion(SalesTransactionPeer.PAYMENT_STATUS, i_PAYMENT_PROCESSED, Criteria.EQUAL);
			Criteria.Criterion oAmtLE  = oCrit.getNewCriterion(SalesTransactionPeer.TOTAL_AMOUNT,(Object) SqlUtil.buildCompareQuery 
					(SalesTransactionPeer.PAID_AMOUNT, SalesTransactionPeer.TOTAL_AMOUNT, "<") , Criteria.CUSTOM);

			Criteria.Criterion oPmtOpen = oCrit.getNewCriterion(SalesTransactionPeer.PAYMENT_STATUS, i_PAYMENT_OPEN, Criteria.EQUAL);
			Criteria.Criterion oAmtZero = oCrit.getNewCriterion(SalesTransactionPeer.PAID_AMOUNT, bd_ZERO, Criteria.EQUAL);
			
			oCrit.add(oPmtProc.and(oAmtLE));
			oCrit.or(oPmtOpen.and(oAmtZero));	
		}
		
		oCrit.addAscendingOrderByColumn(SalesTransactionPeer.TRANSACTION_DATE);
		return SalesTransactionPeer.doSelect(oCrit);
	}
	
	/**
	 * cancel sales transaction
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @throws Exception
	 */
	public static void cancelSales (SalesTransaction _oTR, 
								    List _vTD, 
								    String _sCancelBy, 
								    Connection _oConn) 
		throws Exception 
	{	
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		
		int iOldStatus = _oTR.getStatus();
		
		try 
		{	
			//validate date towards period
			validateDate (_oTR.getTransactionDate(), _oConn);
			
			if (!ReceivablePaymentTool.isInvoicePaid(_oTR.getSalesTransactionId(), _oConn))
			{
				if (!SalesReturnTool.isReturned(_oTR.getSalesTransactionId(), _oConn))
				{				
					if (_oTR.getStatus() == i_TRANS_PROCESSED)
					{
						//apply coupon 
						DiscountTool.deleteCoupon(_oTR, _oConn);

						//update DO if exist
						DeliveryOrderTool.updateDOFromInvoice(_vTD, true, _oConn);

						//update SO if exist
						SalesOrderTool.updateSOFromInvoice(_vTD, false, true, _oConn);
						
						//update inventory
						//InventoryTransactionTool.delete (_oTR.getSalesTransactionId(), i_INV_TRANS_SALES_INVOICE, _oConn);
						InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
						oCT.delete(_oTR, _oTR.getSalesTransactionId(), i_INV_TRANS_SALES_INVOICE);						
						
						//update Point Reward
						PointRewardTool.deleteTrans(_oTR.getTransactionId(), _oConn);
						
						//update AR journal
						InvoicePaymentTool.cancelPayment(_oTR, _oConn);

						if (b_USE_GL && PreferenceTool.getLocationFunction() != i_STORE)
						{
							//update GL journal
							SalesJournalTool.deleteJournal (
								GlAttributes.i_GL_TRANS_SALES_INVOICE, _oTR.getSalesTransactionId(), _oConn
							);                
						}
					}
					_oTR.setRemark(cancelledBy(_oTR.getRemark(), _sCancelBy));
					_oTR.setStatus(i_TRANS_CANCELLED);
					_oTR.save(_oConn);
				}
				else
				{
					throw new NestableException (LocaleTool.getString("inv_returned"));
				}
			}
			else
			{
				throw new NestableException (LocaleTool.getString("inv_paid"));
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			_oTR.setStatus(iOldStatus);			
			if (bStartTrans) 
			{
				rollback(_oConn);
			}	
			_oEx.printStackTrace();
			throw new NestableException(_oEx.getMessage(), _oEx);
		}
	}
	
    public static void deleteTrans(String _sID)
        throws Exception
    {
        Connection oConn = null;
        try 
        {
            oConn = BaseTool.beginTrans();
            Criteria oCrit = new Criteria();
            oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID,_sID);
            SalesTransactionDetailPeer.doDelete(oCrit,oConn);
            
            oCrit = new Criteria();
            oCrit.add(SalesTransactionPeer.SALES_TRANSACTION_ID,_sID);
            SalesTransactionPeer.doDelete(oCrit,oConn);

            oCrit = new Criteria();
            oCrit.add(InvoicePaymentPeer.TRANSACTION_ID,_sID);
            InvoicePaymentPeer.doDelete(oCrit,oConn);

            BaseTool.commit(oConn);
        }
        catch (Exception _oEx) 
        {
            BaseTool.rollback(oConn);            
            throw new NestableException("Delete Transaction Failed: " + _oEx.getMessage(), _oEx);
        }
    }
    
    /**
     * delete only called from SAVE EDIT
     * 
     * @param _sID
     * @param _oConn
     * @throws Exception
     */
    private static void deleteTrans(String _sID, Connection _oConn)
        throws Exception
    {        
        try 
        {
            Criteria oCrit = new Criteria();
            oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID,_sID);
            SalesTransactionDetailPeer.doDelete(oCrit,_oConn);
            
            oCrit = new Criteria();
            oCrit.add(SalesTransactionPeer.SALES_TRANSACTION_ID,_sID);
            SalesTransactionPeer.doDelete(oCrit,_oConn);
            
            oCrit = new Criteria();
            oCrit.add(InvoicePaymentPeer.TRANSACTION_ID,_sID);
            InvoicePaymentPeer.doDelete(oCrit,_oConn);
        }
        catch (Exception _oEx) 
        {
            throw new NestableException("Delete Transaction Failed: " + _oEx.getMessage(), _oEx);
        }
    }
        
	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_TRANS_PENDING) return LocaleTool.getString("pending");
		if (_iStatusID == i_TRANS_PROCESSED) return LocaleTool.getString("processed");
		if (_iStatusID == i_TRANS_CANCELLED) return LocaleTool.getString("cancelled");
		return LocaleTool.getString("new");
	}	
		
	/////////////////////////////////////////////////////////////////////////////////////
	//reporting related methods
	/////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Create criteria for transaction date
	 * 
	 * @param _oCrit
	 * @param _dStart
	 * @param _dEnd
	 * @return modified criteria 
	 */
	protected static Criteria transDateCriteria (Criteria _oCrit, Date _dStart, Date _dEnd)
	{        
        return transDateCriteria(_oCrit, SalesTransactionPeer.TRANSACTION_DATE, _dStart, _dEnd);
	}
	
	/**
	 * Simple Find Trans
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _iStatus
	 * @param _sLocationID
	 * @param _sVendorID
	 * @param _sCashierName
	 * @return
	 * @throws Exception
	 */
	public static List findTrans (Date _dStart, 
								  Date _dEnd,  
								  int _iStatus, 
								  String _sLocationID,
								  String _sVendorID, 
								  String _sCashierName) 
		throws Exception
	{
		return findTrans (-1, "", _dStart, _dEnd, "", _sLocationID, _iStatus, _sVendorID, _sCashierName, "");
	}	
    
	/**
	 * use by SalesReportByAmount.vm & SalesRevenueReport.vm
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _sVendorID
	 * @param _sCashierName
	 * @param _sCurrencyID
	 * @return
	 * @throws Exception
	 */
    public static List findTrans (int _iCond,
                                  String _sKeywords,
                                  Date _dStart, 
                                  Date _dEnd,  
                                  String _sCustomerID,
                                  String _sLocationID,
                                  int _iStatus, 
                                  String _sVendorID,
                                  String _sCashierName,
                                  String _sCurrencyID) 
        throws Exception
    {
        return findTrans (_iCond, _sKeywords, _dStart, _dEnd, _sCustomerID, _sLocationID, _iStatus, _sVendorID, _sCashierName, "", _sCurrencyID, -1);            
    }
	
	/**
	 * find Trans for reporting purpose, return list instead of large select
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _sVendorID
	 * @param _sCashierName
	 * @param _sCurrencyID
     * @param _iGroupBy
	 * @return List of Sales Transaction
	 * @throws Exception
	 */
	
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sCashierName,
								  String _sSalesmanID,
								  String _sCurrencyID,
                                  int _iGroupBy) 
    	throws Exception
	{
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	SalesTransactionPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);   
        
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesTransactionPeer.CUSTOMER_ID, _sCustomerID);	
		}
        if(StringUtil.isNotEmpty(_sLocationID))
        {
           oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocationID);
        }
        if (StringUtil.isNotEmpty(_sCashierName))
	    {
	    	oCrit.add(SalesTransactionPeer.CASHIER_NAME, _sCashierName);
	    }        
        if (StringUtil.isNotEmpty(_sSalesmanID))
	    {
	    	oCrit.add(SalesTransactionPeer.SALES_ID, _sSalesmanID);
	    }        
        if(_iStatus > 0)
        {
           oCrit.add(SalesTransactionPeer.STATUS, _iStatus);
        }
	    if (StringUtil.isNotEmpty(_sVendorID))
	    {
		    oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID,SalesTransactionDetailPeer.SALES_TRANSACTION_ID);
		    oCrit.addJoin(SalesTransactionDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
		    oCrit.add(ItemPeer.PREFERED_VENDOR_ID,_sVendorID);
	    }	   
        if (_iGroupBy > 0)
        {
            buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
        }
		else
		{
			oCrit.addAscendingOrderByColumn(SalesTransactionPeer.TRANSACTION_DATE);
		}        
        
        log.debug(oCrit);
		return SalesTransactionPeer.doSelect(oCrit);
	}

	public static List findCashierTrans (Date _dStart, Date _dEnd, String _sCashierName) 
		throws Exception
	{
		return findCashierTrans(_dStart, _dEnd, _sCashierName, i_PROCESSED);
	}

	public static List findCashierTrans (Date _dStart, Date _dEnd, String _sCashierName, int _iStatus) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if(_iStatus <= 0) _iStatus = i_PROCESSED;
		oCrit.add(SalesTransactionPeer.STATUS, _iStatus);
		oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, _dStart, Criteria.GREATER_EQUAL);
		oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, _dEnd, Criteria.LESS_EQUAL);
		if (StringUtil.isNotEmpty(_sCashierName))
		{
			oCrit.add(SalesTransactionPeer.CASHIER_NAME, _sCashierName);
		}
		log.debug(oCrit);
		return SalesTransactionPeer.doSelect(oCrit);
	}
	
	/**
	 * get this customer all due invoices
	 * 
	 * @param _sCustomerID
	 * @return List of due invoices
	 * @throws Exception
	 */
	public static List getDueToInvoices(String _sCustomerID)
		throws Exception
	{
        return getDueToInvoices (null, null, _sCustomerID, "", "");
    }

	/**
	 * get customer invoices which due in certain range
	 * 
	 * @param _dDueStart
	 * @param _dDueEnd
	 * @param _sCustomerID
	 * @return List of due invoices
	 * @throws Exception
	 */
	public static List getDueToInvoices(Date _dDueStart, Date _dDueEnd, String _sCustomerID)
		throws Exception
	{
	    return getDueToInvoices (_dDueStart, _dDueEnd, _sCustomerID, "", "");
	}
	
	/**
	 * get customer invoices which due in certain range
	 * 
	 * @param _dDueStart
	 * @param _dDueEnd
	 * @param _sCustomerID	 
	 * @param _sCustomerTypeID	 	 
	 * @return List of due invoices
	 * @throws Exception
	 */
	public static List getDueToInvoices (Date _dDueStart, 
										 Date _dDueEnd, 
										 String _sCustomerID, 
										 String _sCustomerTypeID,
										 String _sLocationID) 
    	throws Exception
	{
		Criteria oCrit = new Criteria();
		if (_dDueStart != null)
		{
            oCrit.add(SalesTransactionPeer.DUE_DATE, 
            	DateUtil.getStartOfDayDate(_dDueStart), Criteria.GREATER_EQUAL);
        }
		if (_dDueEnd != null)
		{
            oCrit.and(SalesTransactionPeer.DUE_DATE, 
            	DateUtil.getEndOfDayDate(_dDueEnd), Criteria.LESS_EQUAL);
        }
		oCrit.add(SalesTransactionPeer.PAID_AMOUNT,
		    (Object) SqlUtil.buildCompareQuery 
				(SalesTransactionPeer.PAID_AMOUNT, SalesTransactionPeer.TOTAL_AMOUNT, "<"), Criteria.CUSTOM);
		
		oCrit.add(SalesTransactionPeer.STATUS, i_PROCESSED); 		
        if (StringUtil.isNotEmpty(_sCustomerID))
        {
            oCrit.add(SalesTransactionPeer.CUSTOMER_ID, _sCustomerID); 
        }
        else 
        {
        	if (StringUtil.isNotEmpty(_sCustomerTypeID))
        	{
                oCrit.addJoin(SalesTransactionPeer.CUSTOMER_ID, CustomerPeer.CUSTOMER_ID);
                oCrit.add(CustomerPeer.CUSTOMER_TYPE_ID, _sCustomerTypeID);
        	}
        }
        if (StringUtil.isNotEmpty(_sLocationID))
        {
            oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocationID); 
        }        
		return SalesTransactionPeer.doSelect(oCrit);
	}

	/**
	 * get trans details of this list of master
	 * 
	 * @param _vTrans
	 * @return List of trans details
	 * @throws Exception
	 */
	public static List getTransDetails (List _vTrans)
    	throws Exception
    { 
		List vTransID = new ArrayList (_vTrans.size());
		for (int i = 0; i < _vTrans.size(); i++) 
		{
			SalesTransaction oTrans = (SalesTransaction) _vTrans.get(i);			
			vTransID.add (oTrans.getSalesTransactionId());
		}		
		Criteria oCrit = new Criteria();
        if(vTransID.size() > 0)
        {
		    oCrit.addIn (SalesTransactionDetailPeer.SALES_TRANSACTION_ID, vTransID);
		}
		return SalesTransactionDetailPeer.doSelect(oCrit);
	}

	/**
	 * 
	 * @param _sCustomerID
	 * @param _sStartDate
	 * @param _sEndDate
	 * @param _sLocationID
	 * @return customer sales history
	 * @throws Exception
	 */
	public static List getCustomerSalesHistory (String _sCustomerID, 
											    String _sStartDate, 
												String _sEndDate, 
												String _sLocationID) 
		throws Exception
	{		
		Date dStart = CustomParser.parseDate (_sStartDate);
		Date dEnd = CustomParser.parseDate (_sEndDate);
		
		if (dStart == null) dStart = new Date();
		if (dEnd == null) dEnd = new Date();		
		
		Criteria oCrit = new Criteria();

		transDateCriteria(oCrit, dStart, dEnd);

		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocationID );		
		}
	    oCrit.add(SalesTransactionPeer.CUSTOMER_ID, _sCustomerID );		         
		return SalesTransactionPeer.doSelect(oCrit);
	}
	
	/**
	 * create map contains payment type id and amount from sales invoices
	 * if multiple payment then break down to each payment type
	 * 
	 * @param _vSales
	 * @return
	 * @throws Exception
	 */
	public static Map groupAmountByPaymentType(List _vSales)
		throws Exception
	{
		Map mResult = new HashMap();
		String sChangeKey = "Changes";
		List vPT = PaymentTypeTool.getAllPaymentType();
		for (int i = 0; i < vPT.size(); i++)
		{
			PaymentType oPT = (PaymentType)vPT.get(i);
			String sTypeID = oPT.getPaymentTypeId();
			List vSalesPT = BeanUtil.filterListByFieldValue(_vSales,"paymentTypeId",sTypeID);			
			if (!oPT.getIsMultiplePayment())
			{
				double dSalesPT = 0;
				double dChanges = 0;
				for(int j = 0; j < vSalesPT.size(); j++)
				{
					SalesTransaction oTR = (SalesTransaction) vSalesPT.get(j);
					if (oPT.getIsCash())
					{
						dSalesPT = dSalesPT + oTR.getPaymentAmount().doubleValue();
						dChanges = dChanges + oTR.getChangeAmount().doubleValue();
					}
					else
					{
						dSalesPT = dSalesPT + oTR.getTotalAmount().doubleValue();						
					}
				}
				//get existing
				if (mResult.containsKey(sTypeID))
				{
					BigDecimal dExistPT = (BigDecimal) mResult.get(sTypeID);
					dSalesPT = dExistPT.doubleValue() + dSalesPT;
				}
				mResult.put(sTypeID, new BigDecimal(dSalesPT));
				
				//if cash calc changes
				if(oPT.getIsCash())
				{				
					if (mResult.containsKey(sChangeKey))
					{
						BigDecimal dExistCH = (BigDecimal) mResult.get(sChangeKey);
						dChanges = dExistCH.doubleValue() + dChanges;
					}
					mResult.put(sChangeKey, new BigDecimal(dChanges));
				}
			}
			else
			{
				for(int j = 0; j < vSalesPT.size(); j++)
				{
					SalesTransaction oTR = (SalesTransaction) vSalesPT.get(j);					
					List vPMT = InvoicePaymentTool.getTransPayment(oTR.getSalesTransactionId());
					log.debug("MULTI : "  + oTR.getInvoiceNo() + " PMT: " + vPMT.size());
					for (int k = 0; k < vPMT.size(); k++)
					{
						InvoicePayment oPMT = (InvoicePayment) vPMT.get(k);
						String sPMTTypeID = oPMT.getPaymentTypeId();
						double dPMTAmt = oPMT.getAmount();
						
						//get existing
						if (mResult.containsKey(sPMTTypeID))
						{
							BigDecimal dExistPT = (BigDecimal) mResult.get(sPMTTypeID);							
							dPMTAmt = dPMTAmt + dExistPT.doubleValue();
						}
						mResult.put(sPMTTypeID, new BigDecimal(dPMTAmt));
						
						//if cash calculate changes
						double dChanges = 0;
						if(oPMT.getIsCash())
						{
							dChanges = oPMT.getChangeAmount();
							if (mResult.containsKey(sChangeKey))
							{
								BigDecimal dExistCH = (BigDecimal) mResult.get(sChangeKey);							
								dChanges = dChanges + dExistCH.doubleValue();
							}
							mResult.put(sChangeKey, new BigDecimal(dChanges));							
						}
					}
				}
			}
		}
		return mResult;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////
		
	/**
	 * get all sales invoice created since last store 2 ho
	 * 
	 * @return List of SalesTransaction created since last store 2 ho
	 * @throws Exception
	 */
	public static List getStoreSalesInvoice ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate != null) 
		{
        	oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		oCrit.addAscendingOrderByColumn(SalesTransactionPeer.TRANSACTION_DATE);
		oCrit.add(SalesTransactionPeer.STATUS, i_TRANS_PROCESSED);
		return SalesTransactionPeer.doSelect(oCrit);
	}

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(SalesTransactionPeer.class, SalesTransactionPeer.SALES_TRANSACTION_ID, _sID, _bIsNext);
	}	  		
}