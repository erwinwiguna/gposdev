package com.ssti.framework.chart.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ChartHelper.java,v 1.3 2007/02/23 14:11:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: ChartHelper.java,v $
 * Revision 1.3  2007/02/23 14:11:15  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public interface ChartHelper
{    
	/**
	 * generate chart
	 * @param _mParams
	 * @param _oSession
	 * @return chart file name
	 * @throws Exception
	 */
	public String getChartFileName (Map _mParams, HttpSession _oSession) throws Exception;
}