package com.ssti.framework.db;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;


public class AbstractDSFactory implements Attributes
{
    private static final Log log = LogFactory.getLog(AbstractDSFactory.class);

    protected String sDBDRV   = "";
    protected String sDBURL   = "";
    protected String sDBUSR   = "";
    protected String sDBPWD   = "";
    
    protected int iMaxActive = 100;
    protected int iMaxIdle   = 50;
    protected int iMinIdle   = 10;
    protected int iMaxWait   = 1000;

    protected void init(Configuration conf)
    {
        if (conf != null)
        {                        
            String sSub  = "connection";
            sDBDRV = conf.getString(sSub + ".driver");
            sDBURL = conf.getString(sSub + ".url");
            sDBUSR = conf.getString(sSub + ".user");
            sDBPWD = conf.getString(sSub + ".password");
            
            sSub  = "pool";            
            if(StringUtil.isNotEmpty(conf.getString(sSub + ".maxActive")))iMaxActive = conf.getInt(sSub + ".maxActive");
            if(StringUtil.isNotEmpty(conf.getString(sSub + ".maxIdle")))  iMaxIdle   = conf.getInt(sSub + ".maxIdle");
            if(StringUtil.isNotEmpty(conf.getString(sSub + ".minIdle")))  iMinIdle   = conf.getInt(sSub + ".minIdle");
            if(StringUtil.isNotEmpty(conf.getString(sSub + ".maxWait")))  iMaxWait   = conf.getInt(sSub + ".maxWait"); 
            
            log.debug(sDBDRV + " URL:" + sDBURL + " USR:" + sDBUSR + " PWD:" + sDBPWD + " ");
        }
    }
}
