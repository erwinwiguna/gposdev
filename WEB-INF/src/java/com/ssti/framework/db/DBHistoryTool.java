package com.ssti.framework.db;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.torque.Torque;
import org.apache.torque.util.Criteria;

import com.ssti.framework.om.DbHistory;
import com.ssti.framework.om.DbHistoryPeer;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

public class DBHistoryTool 
{
	public static String getLatestVersion() 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addDescendingOrderByColumn(DbHistoryPeer.DB_VERSION);
		oCrit.setLimit(1);
		
		List vData = DbHistoryPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			DbHistory oDB = (DbHistory) vData.get(0);
			return oDB.getDbVersion();
		}
		return "1";
	}
	
	public static List getHistory() 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addDescendingOrderByColumn(DbHistoryPeer.DB_VERSION);
		return DbHistoryPeer.doSelect(oCrit);
	}
	
	public static String executeBatch(String _sSQL) 
		throws Exception
	{
		StringBuilder oResult = new StringBuilder();
		StringTokenizer oTokenizer = new StringTokenizer(_sSQL,";");
		while (oTokenizer.hasMoreTokens())
		{
			String sSQL = oTokenizer.nextToken();
			if (validate(sSQL))
			{
				String sResult = executeScript(sSQL);
				oResult
				.append("---------------EXECUTING SQL:------------------------")
				.append(Attributes.s_LINE_SEPARATOR)
				.append(sSQL)
				.append(Attributes.s_LINE_SEPARATOR)
				.append("---------------EXECUTION RESULT:---------------------")
				.append(Attributes.s_LINE_SEPARATOR)
				.append(sResult)
				.append(Attributes.s_LINE_SEPARATOR)
				.append("*****************************************************")
				.append(Attributes.s_LINE_SEPARATOR)
				.append(Attributes.s_LINE_SEPARATOR);			
			}
			else
			{
				oResult
				.append("-------------PROHIBITED SQL COMMAND !!!--------------")
				.append(Attributes.s_LINE_SEPARATOR)
				.append(sSQL)
				.append(Attributes.s_LINE_SEPARATOR)
				.append("#####################################################")
				.append(Attributes.s_LINE_SEPARATOR)
				.append(Attributes.s_LINE_SEPARATOR);			
			}
		}
		return oResult.toString();
	}
	
	private static boolean validate(String _sSQL) 
	{
		if (StringUtil.isNotEmpty(_sSQL))
		{
			if (_sSQL.contains("DROP DATABASE") || 
				_sSQL.contains("TRUNCATE TABLE") || 
				_sSQL.contains("DELETE FROM"))
			{
				return false;
			}
		}
		return true;
	}

	public static String executeScript(String _sSQL) 
		throws Exception
	{
		Connection oConn = null;
		String sResult = "";
		try 
		{
			oConn = Torque.getConnection();
			Statement oStmt = oConn.createStatement();
			boolean bSuccess = oStmt.execute(_sSQL);
			sResult = "SQL Script Executed Successfully";
			oStmt.close();
		} 
		catch (Exception e) 
		{
			sResult = "ERROR Executing Update Script, Message: " + e.getMessage();
			e.printStackTrace();
		}
		finally
		{
			if (oConn != null) Torque.closeConnection(oConn);
		}
		return sResult;
	}
}
