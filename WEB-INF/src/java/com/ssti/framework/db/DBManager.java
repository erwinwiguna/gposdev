package com.ssti.framework.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Encapsulate multiple DB operation 
 * (Specific for Retailsoft Operation)
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DBManager.java,v 1.4 2007/02/23 14:11:39 albert Exp $ <br>
 *
 * <pre>
 * $Log: DBManager.java,v $
 * Revision 1.4  2007/02/23 14:11:39  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public class DBManager 
{	
	public static final int i_BACKUP = 1;
	public static final int i_RESTORE = 2;
	public static final int i_NEWDB = 3;
	
	private static Log log = LogFactory.getLog(DBManager.class); 
	
	private DBTool oDBTool = null;
	private String result;
	
	public String getResult() {
		return result;
	}
	
	/**
	 * @param _iOp
	 * @param _sDBName Database Name
	 * @param _sFile File to invoke against the Database
	 * @param _sPath path needed by newDB
	 */
	public DBManager (int _iOp, String _sDBType, String _sDBName, String _sFile, String _sPath)
	{
		try
		{
			if (_sDBType.equals("mysql"))
			{
				oDBTool = new MySQLTool(_sDBName, _sFile, _sPath);
			}
			if (_sDBType.equals("postgresql"))
			{
				oDBTool = new PostgreSQLTool(_sDBName, _sFile, _sPath);
			}
			
			if (oDBTool != null) 
			{
				if (_iOp == i_BACKUP) 
				{
					oDBTool.backup();
				}
				else if (_iOp == i_RESTORE)
				{
					oDBTool.restore();
				}
				else if (_iOp == i_NEWDB)
				{
					oDBTool.newDB();
				}
				result = oDBTool.getResult();
			}
		}
		catch (Exception ex)
		{
			result += "\nDBManager Error : " + ex.getMessage();
			ex.printStackTrace();
		}
	}
}
