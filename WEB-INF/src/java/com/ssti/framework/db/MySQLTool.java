package com.ssti.framework.db;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.Turbine;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.DataStreamer;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MySQLTool.java,v 1.4 2007/02/23 14:11:39 albert Exp $ <br>
 *
 * <pre>
 * $Log: MySQLTool.java,v $
 * Revision 1.4  2007/02/23 14:11:39  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public class MySQLTool implements DBTool 
{
	static final Log log = LogFactory.getLog(MySQLTool.class); 
	
	//default values
	private static String s_DB_USER = "root";
	private static String s_DB_PWD =  "crossfire";
	
	private static String s_USER = "-u" + s_DB_USER;
	private static String s_PWD = "-p" + s_DB_PWD;
	
	private static String m_sDBName = "pos";
	private String m_sFile = "data_backup.sql";
	private String m_sPath = "/DBServer/bin/";
	private String m_sInit = "/AppServer/scripts/init.vm";
	
	//sb to save operation result
	private StringBuilder m_oResult = new StringBuilder();

	public MySQLTool  (String _sDBName, String _sFile)
	{
		init (_sDBName, "", "", _sFile, "");
	}
	
	public MySQLTool  (String _sDBName, String _sFile, String _sPath)
	{
		init (_sDBName, "", "", _sFile, _sPath);
	}
	
	public MySQLTool  (String _sDBName, String _sUser, String _sPwd ,String _sFile, String _sPath)
	{
		init (_sDBName, _sUser, _sPwd, _sFile, _sPath);	
	}
	
	/**
	 * 
	 * @param _sDBName the DB name targeted for operations
	 * @param _sUser DB username 
	 * @param _sPwd DB password
	 * @param _sFile the SQL file that will be invoked against the DB (empty used for new DB)
	 * @param _sPath the executable path to the DBServer Client (mysql client)
	 *        in retailsoft mostly located in retailsoft.home/DBServer/bin
	 */
	private void init (String _sDBName, String _sUser, String _sPwd ,String _sFile, String _sPath)
	{
		if (StringUtil.isNotEmpty(_sUser)) {s_DB_USER = _sUser; s_USER = "-u" + s_DB_USER;}
		if (StringUtil.isNotEmpty(_sPwd))  {s_DB_PWD = _sPwd; s_PWD = "-p" + s_DB_PWD;}
			
		if (StringUtil.isNotEmpty(_sDBName)) m_sDBName = _sDBName;
		if (StringUtil.isNotEmpty(_sFile)) m_sFile = _sFile;					
		
		if (StringUtil.isNotEmpty(_sPath))
		{
			m_sPath = _sPath;
			m_sInit = _sPath + m_sInit;			
		}
		else
		{
			String sPath = System.getProperty(Attributes.s_HOME_ENV);
			//if no retailsoft_home
			if (sPath == null)
			{
				sPath = Turbine.getApplicationRoot();
				log.info ("getting path value from Turbine.getApplicationRoot() : " + sPath);
			}
			m_sInit = sPath + m_sInit;			
			m_sPath = sPath + m_sPath;
		}
		
		//correct path according to OS specific separator
		m_sPath = m_sPath.replace("/", s_FILE_SEPARATOR);
		m_sInit = m_sInit.replace("/", s_FILE_SEPARATOR);

		m_sPath = m_sPath.replace("\\", s_FILE_SEPARATOR);
		m_sInit = m_sInit.replace("\\", s_FILE_SEPARATOR);
		
		log.info ("db:" + m_sDBName + " file: " +  m_sFile + " exec: " + m_sPath + " init: " + m_sInit );		
	}
	
	public String getResult() 
	{
		return m_oResult.toString();
	}	
	
	/**
	 * run mysqldump 
	 */
  	public void backup() 
  	{
		try 
    	{ 			
			List oCmd = new ArrayList();
			
			oCmd.add(m_sPath + "mysqldump ");
			oCmd.add(s_USER);
			oCmd.add(s_PWD);
			oCmd.add("--skip-opt");
			oCmd.add("--single-transaction");
			oCmd.add("--extended-insert=FALSE");
			oCmd.add("--add-drop-table");
			oCmd.add("--add-locks");							
			oCmd.add("-r" + m_sFile );							
			oCmd.add(m_sDBName);
			
			log.info (oCmd);
			
			ProcessBuilder oPB = new ProcessBuilder(oCmd);
			
			Process oProc = oPB.start(); 
			
			DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oResult);            
			DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
				
			oErrorStreamer.start();
			oOutStreamer.start();
			
			int iExitCode = oProc.waitFor();
			
			if (iExitCode == 0) 
			{
				m_oResult.append("\nDatabase '" + m_sDBName + "' Back Up Successful ");
				m_oResult.append("\n\nBackup File: " + m_sFile);				
				log.info(m_oResult.toString());
			}
			else 
			{
				m_oResult.insert(0, 
				    "\nData Back Up Failed Error Code : " + iExitCode + System.getProperty("line.separator"));
				
				log.error(m_oResult);
			}
		}
  		catch (Exception _oEx)
  		{
			log.error ("MysqlDump Process Failed : " + _oEx.getMessage());
			m_oResult.append("Backup Process Failed : " + _oEx.getMessage());
			_oEx.printStackTrace();
  		}
  	} 

  	private String m_sTempBatch = "";
  	
  	/**
  	 * restore a db using a temp batch file
  	 */
  	public void restore() 
  	{
		try 
    	{ 	
			createRestoreBatch();
			
			String[] aCmd = {m_sTempBatch};
			ProcessBuilder oPB = new ProcessBuilder(aCmd);
			Process oProc = oPB.start(); 
			
			DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oResult);            
			DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
				
			oErrorStreamer.start();
			oOutStreamer.start();
			
			int iExitCode = oProc.waitFor();
			
			if (iExitCode == 0) 
			{
				m_oResult.append("\nData File : ").append(m_sFile)
						.append(" Restored Successfully To Database '" + m_sDBName + "'");
				System.out.println(m_oResult.toString());
			}
			else 
			{
				m_oResult.insert(0, 
				    "\nData Restore Failed Error Code : " + iExitCode + s_LINE_SEPARATOR);
				
				log.error(m_oResult);
			}
			
			delTempBatch(); 
		}
  		catch (Exception _oEx)
  		{
			log.error ("MySQL Restore Failed : " + _oEx.getMessage());
			m_oResult.append("Restore Process Failed : " + _oEx.getMessage());
			_oEx.printStackTrace();
  		}
  	}

  	/**
  	 * create a newDB
  	 */
  	public void newDB() 
  	{
		try 
    	{ 	
			createNewDBBatch(m_sInit);
			String[] aCmd = {m_sTempBatch};

			ProcessBuilder oPB = new ProcessBuilder(aCmd);
			
			Process oProc = oPB.start(); 
			
			DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oResult);            
			DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
				
			oErrorStreamer.start();
			oOutStreamer.start();
			
			int iExitCode = oProc.waitFor();
			
			if (iExitCode == 0) 
			{
				m_oResult.append("\nDatabase '").append(m_sDBName).append("' Created Successfully ");
				log.info(m_oResult.toString());
			}
			else 
			{
				m_oResult.insert(0, 
				    "\nCreate New DB Failed Error Code : " + iExitCode + s_LINE_SEPARATOR);

				log.error(m_oResult);
			}
			
			delTempBatch(); 
		}
  		catch (Exception _oEx)
  		{
			log.error ("MySQL Create DB Failed : " + _oEx.getMessage());
			m_oResult.append("MySQL Create DB Failed : " + _oEx.getMessage());
			_oEx.printStackTrace();
  		}
  	}  	
  	
	public void createRestoreBatch() 
		throws Exception
	{
		StringBuilder sFileName = new StringBuilder(m_sPath).append("restoreDB_")
			.append(m_sDBName).append("_").append(System.currentTimeMillis()).append(".bat");

		StringBuilder sContent = new StringBuilder();

		System.out.println (sFileName);
		
		if (s_FILE_SEPARATOR.equals("/"))
		{
			sContent.append("#!bin/sh").append(s_LINE_SEPARATOR);
		}	
		else
		{
			sContent.append("@echo off").append(s_LINE_SEPARATOR);
		}
		sContent.append(m_sPath)
			.append("mysql ").append(s_USER).append(" ").append(s_PWD).append(" -e ")
			.append("\"source ").append(m_sFile).append("\" ").append(m_sDBName).append(s_LINE_SEPARATOR);
		
		log.debug (sFileName + " content : " + sContent);
		
		FileWriter wr = new FileWriter(sFileName.toString());
		BufferedWriter bwr = new BufferedWriter(wr);
		bwr.write(sContent.toString());
		bwr.flush();
		bwr.close();
				
		m_sTempBatch = sFileName.toString();		
		IOTool.makeExecutable(m_sTempBatch);
	} 	

	public void createNewDBBatch(String _sInitFile) 
		throws Exception
	{
		StringBuilder sFileName = new StringBuilder(m_sPath).append(s_FILE_SEPARATOR).append("newDB_")
			.append(m_sDBName).append("_").append(System.currentTimeMillis()).append(".bat");
		
		StringBuilder sContent = new StringBuilder();
	
		System.out.println (sFileName);
		
		if (s_FILE_SEPARATOR.equals("/"))
		{
			sContent.append("#!bin/sh").append(s_LINE_SEPARATOR);
		}	
				
		FileReader reader = new FileReader (_sInitFile);
		BufferedReader br = new BufferedReader (reader);
		while (true)
		{
			String s = br.readLine();
			if (s == null) {break;}
			s = s.replace("$INSTALL_PATH", m_sPath);
			s = s.replace("$db", m_sDBName);			
			s = s.replace("$user", s_DB_USER);			
			s = s.replace("$pwd", s_DB_PWD);			
			
			sContent.append(s).append(s_LINE_SEPARATOR);
		}
		br.close();
		log.debug (sFileName + " content : " + sContent);
		
		FileWriter wr = new FileWriter(sFileName.toString());
		BufferedWriter bwr = new BufferedWriter(wr);
		bwr.write(sContent.toString());
		bwr.flush();
		bwr.close();
	
		m_sTempBatch = sFileName.toString();	
		IOTool.makeExecutable(m_sTempBatch);		
	} 	
	
	private void delTempBatch() 
		throws Exception
	{
		File oFile = new File(m_sTempBatch);
		oFile.delete();
	} 				
}
