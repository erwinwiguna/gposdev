package com.ssti.framework.excel.helper;

import java.io.InputStream;

public interface ExcelLoader
{
	public void loadData (InputStream _oInput)	throws Exception;
	public String getSummary();
	public String getResult();
}
