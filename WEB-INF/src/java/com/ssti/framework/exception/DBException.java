package com.ssti.framework.exception;

import org.apache.commons.lang.exception.NestableException;

public class DBException extends NestableException
{
	static final String s_PREFIX = "";
	
	public DBException (String _sMessage)
	{
		super(s_PREFIX + _sMessage);
	}
	
	public DBException (String _sMessage, Throwable t)
	{
		super(s_PREFIX + _sMessage, t);
	}
}
