package com.ssti.framework.io;

import java.io.File;
import java.util.Comparator;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Comparator for File Date
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: FileDateComparator.java,v 1.2 2007/02/23 14:12:09 albert Exp $ <br>
 *
 * <pre>
 * $Log: FileDateComparator.java,v $
 * Revision 1.2  2007/02/23 14:12:09  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class FileDateComparator implements Comparator 
{

	public int compare (Object o1, Object o2)
	{
		File f1 = (File) o1;
		File f2 = (File) o2;
		int iResult = -1;
		if (f1.lastModified() > f2.lastModified())
		{
			iResult = 1;
		}
		else if (f1.lastModified() == f2.lastModified())
		{
			iResult = 0;
		}
		return iResult;
	}

}


