package com.ssti.framework.jasper;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.1  2007/04/10 10:20:29  seph
 * *** empty log message ***
 * 
 * </pre><br>
 */
public interface JasperAttributes 
{
    public static final String FORMAT_PDF  = "PDF";
    public static final String FORMAT_XML  = "XML";
    public static final String FORMAT_HTML = "HTML";
    public static final String FORMAT_XLS  = "XLS";
    public static final String FORMAT_CSV  = "CSV";
    public static final String FORMAT_RTF  = "RTF";
    public static final String FORMAT_TXT  = "TXT";
    public static final String FORMAT_VIEW = "VIEWER";
    
    public static final String s_BEANDS = "1";
    public static final String s_JDBCDS = "2";
    public static final String s_RSDS   = "3";
   
}
