package com.ssti.framework.license;
import java.io.Serializable;

public class License implements Serializable
{
	static final long serialVersionUID = 1L;
	
	private String ProductName;
    private String CompanyName;
    private String InstallLocation;
    private int LicenseType;
    private String ExpiredDate;
    private boolean Valid;
    private String Description;
    private int MaxUser;

	public String getProductName() { return (this.ProductName); }
	public String getCompanyName() { return (this.CompanyName); }
	public String getInstallLocation() { return (this.InstallLocation); }
	public int getLicenseType() { return (this.LicenseType); }
	public String getExpiredDate() { return (this.ExpiredDate); }
	public boolean getValid() { return (this.Valid); }
	public String getDescription() { return (this.Description); }	
	public int getMaxUser() { return MaxUser;}	
	
    public void setExpiredDate(String _sExpiredDate) { this.ExpiredDate = _sExpiredDate;}
    public void setValid(boolean _bValid) { this.Valid = _bValid; }
	public void setLicenseType(int licenseType) {LicenseType = licenseType;}
    public void setDescription(String _sDescription) { this.Description = _sDescription;}
    public void setMaxUser(int maxUser) { MaxUser = maxUser; }
	    
	public License (String _sProductName, String _sCompanyName,String _sInstallLocation)
	{
            this.ProductName = _sProductName;
            this.CompanyName = _sCompanyName;
            this.InstallLocation = _sInstallLocation;
	}
}