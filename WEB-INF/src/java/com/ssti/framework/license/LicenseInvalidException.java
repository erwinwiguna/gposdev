package com.ssti.framework.license;

public class LicenseInvalidException extends Exception 
{
	public LicenseInvalidException (String _sMessage)
	{
		super (_sMessage);
	}
}
