package com.ssti.framework.mvc.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Receive file upload from store
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: HOUploadServlet.java,v 1.4 2008/02/26 05:15:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: HOUploadServlet.java,v $
 * Revision 1.4  2008/02/26 05:15:17  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/02/23 14:12:55  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class DownloadServlet extends HttpServlet 
{
	private static final Log log = LogFactory.getLog (DownloadServlet.class);
	
	public static final String s_ENC_KEY = "123456";		
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException 
	{
		String sURI = request.getRequestURI();
		System.out.println("URI: " + sURI);

		String sFile = sURI.substring(sURI.lastIndexOf("/"), sURI.length());
		System.out.println("FILE: " + sFile);
		
		String sPath = sURI.substring(0, sURI.lastIndexOf("/"));
		sPath = sURI.substring(sPath.indexOf("/download/") + "/download/".length(), sPath.length());
		System.out.println("PATH: " + sPath);

		if (StringUtils.isNotEmpty(sPath))
		{
			String sFilePath = sPath + sFile;
			System.out.println(sFilePath);
			
			File oFile = new File(sFilePath);
			int length = (int)oFile.length();
			FileInputStream input = new FileInputStream(oFile);
			byte content[] = new byte[length];
			input.read(content);
			
			response.setContentLength(length);
			response.setContentType("application/octet-stream");
			ServletOutputStream out = response.getOutputStream();
			out.write(content);
			out.flush();
			
			input.close();
		}
	}
}
