package com.ssti.framework.mvc.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.oreilly.servlet.MultipartRequest;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Receive file upload POST request
 * Parameters:
 * - targetpath - directories to store data 
 * - relpath - is path relative to webapps
 * - jsonresult - return result in JSON
 * - restpl - result template
 * 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: HOUploadServlet.java,v 1.4 2008/02/26 05:15:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: HOUploadServlet.java,v $
 * 2018-08-18
 * - add parameters
 * 
 * </pre><br>
 *
 */
public class HOUploadServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;

	private static final Log log = LogFactory.getLog (HOUploadServlet.class);
		
	protected String resultTemplate = "/screens/periodic/StoreUploadResult.vm";	
	
	public static final String s_TARGET_STOREDATA = "storedata";	
	public static final String s_TARGET_HODATA = "hodata";	
	public static final String s_TARGET_TRFIN = "trfin";	
	
	protected ServletContext servContext = null;
	protected VelocityContext context = null;
	protected StringWriter writer = null;
	protected boolean relPath = false;
	protected boolean jsonResult = false;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException 
	{
		if(StringUtil.equalsIgnoreCase(request.getParameter("relpath"), "true")) { relPath = true; }
		response.getWriter().write(setPath(request.getParameter("targetpath")) + " relative: " + request.getParameter("relpath"));
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException 
	{		
		MultipartRequest wrapper = null;
		StringBuilder sbResult = new StringBuilder();
		try 
		{
			servContext = request.getServletContext();			
			context = new VelocityContext();									
			wrapper = new MultipartRequest(request, 
				System.getProperty("java.io.tmpdir"), (1024 * 1024 * 100));
						
			if(StringUtil.equalsIgnoreCase(wrapper.getParameter("relpath"), "true")) { relPath = true; }
			if(StringUtil.equalsIgnoreCase(wrapper.getParameter("jsonresult"), "true")) { jsonResult = true; }			
			context.put("isJSON",jsonResult);
			context.put("sutil", StringUtil.getInstance());
			
			Enumeration e = wrapper.getFileNames();
			while (e.hasMoreElements())
			{
				String sName = (String)e.nextElement();
				File oFile = wrapper.getFile(sName);		
				
				String sFileName = oFile.getName();							
				String sPath = setPath(wrapper.getParameter("targetpath"));
				
				System.out.println("UploadServlet Save Path:" + sPath + " File:" + sFileName);
				
				sbResult.append ("\nFile Name : " + sFileName);					
				sbResult.append ("\nFile Saved In : " + sPath + sFileName);			
								
				//retrieve the file data
				InputStream in = new FileInputStream(oFile);
				
				//write the file to the file specified
				OutputStream out = new FileOutputStream(sPath + sFileName);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				
				while ((bytesRead = in.read(buffer, 0, 8192)) != -1) {
					out.write(buffer, 0, bytesRead);
				}
				out.close();        
				in.close();	
			}

			context.put("isSuccess",Boolean.valueOf(true));
			context.put("message", "Data Successfully Uploaded ");
			context.put("result", sbResult.toString());
		}
		catch (Exception _oEx) 
		{			
			_oEx.printStackTrace();
			String sError = "ERROR : Upload Failed : " + _oEx.getMessage();
			context.put("isSuccess",Boolean.valueOf(false));
			context.put("message", sError);
			log.error (_oEx);
		}
		
		try 
		{
			String resTpl = request.getParameter("restpl");
			if(StringUtil.isNotEmpty(resTpl)) resultTemplate = resTpl;
			
			writer = new StringWriter ();
			Velocity.mergeTemplate( resultTemplate, Attributes.s_DEFAULT_ENCODING, context, writer );
			response.getWriter().write(writer.toString());
			System.out.println(writer.toString());
			response.getWriter().flush();
		} 		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String setPath(String _sTarget)
	{
		String sPath = "";
		System.out.println("UploadServlet Target Path: " + _sTarget);
		if (StringUtil.isEmpty(_sTarget) || StringUtil.isEqual(_sTarget, s_TARGET_STOREDATA))
		{
			sPath = SynchronizationTool.getStoreDataPath(); //default path
		}
		else if (StringUtil.isEqual(_sTarget, s_TARGET_HODATA))
		{
			sPath = SynchronizationTool.getHODataPath();
		}
		else if (StringUtil.isEqual(_sTarget, s_TARGET_TRFIN))
		{
			sPath = PreferenceTool.getSysConfig().getSyncStoretrfPath();
            sPath = IOTool.checkPath(sPath);
		}
		else
		{
			sPath = _sTarget;
			if(relPath)
			{
				sPath = servContext.getRealPath(_sTarget);
			}		
			if(!sPath.endsWith("/")) sPath += "/";
		}
		return sPath;
	}
}
