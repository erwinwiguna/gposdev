package com.ssti.framework.mvc.session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.framework.om.ActiveUserPeer;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * HTTP Session Listener, a listener class to maintain user login session 
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class SessionHandler implements HttpSessionListener
{ 
	private static final Log log = LogFactory.getLog (SessionHandler.class);

	public static HashMap<String,HttpSession> SessionsMap = new HashMap<String,HttpSession>();
	 
	public void sessionCreated(HttpSessionEvent se) 
	{ 
		log.debug("***** Session Created: " + se.getSession().getId());
		SessionsMap.put(se.getSession().getId(),se.getSession()); 
	}

	public void sessionDestroyed(HttpSessionEvent se) 
	{	 
		deleteActiveUser("", se.getSession().getId());
		log.debug("***** Session Destroyed: " + se.getSession().getId());
		SessionsMap.remove(se.getSession().getId());	 
	}  
	
	public static void invalidateSession(String SessionId)
	{	 
		try 
		{
			HttpSession session =  SessionsMap.get(SessionId); 
			SessionsMap.remove(SessionId);
			if (session != null) session.invalidate();
			log.debug("***** Remove Existing Session: " + SessionId);			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void deleteActiveUser(String _sUser, String _sSesID)
	{
		try 
		{
			Criteria oCrit = new Criteria();
			if (StringUtil.isNotEmpty(_sUser)) oCrit.add(ActiveUserPeer.USER_NAME, _sUser);
			if (StringUtil.isNotEmpty(_sSesID)) oCrit.add(ActiveUserPeer.SESSION_ID, _sSesID);
			ActiveUserPeer.doDelete(oCrit);
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
		}
	}
	
	public static List getActiveUser()
	{
		try 
		{
			Criteria oCrit = new Criteria();
			return ActiveUserPeer.doSelect(oCrit);			
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
		}
		return new ArrayList(1);
	}
	
	/**
	 * count total user currently login
	 * 
	 * @return total login user
	 */
	public static int countActiveUser()
	{
		return getActiveUser().size();
	}
	
	/**
	 * count active user exclude current user
	 * 
	 * @param _sUser
	 * @return total other login user
	 */
	public static int countOtherUser(String _sUser)
	{
		try 
		{
			Criteria oCrit = new Criteria();
			oCrit.add(ActiveUserPeer.USER_NAME, (Object)_sUser, Criteria.NOT_EQUAL);
			List vUser = ActiveUserPeer.doSelect(oCrit);
			return vUser.size();
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
		}
		return getActiveUser().size();
	}	
}
