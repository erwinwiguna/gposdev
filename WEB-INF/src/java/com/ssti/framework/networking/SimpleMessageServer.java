package com.ssti.framework.networking;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Message Server in Client Side / POS Client
 * Used for receiving Notification from Server
 * <br>
 *
 * $@author  Author: LionZ $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class SimpleMessageServer extends Thread
{
    static final int i_PORT = 8088;
    
    JFrame frmMessage;
    JPanel pnlMessage;
    JTextArea txtMessage;
    JPanel pnlButton;
    JButton btnOK;
    
    public SimpleMessageServer()
    {
        initGUI();
    }
    
    private void initGUI()
    {
        // status frame        
        frmMessage = new JFrame("Message From Server:");
        frmMessage.getContentPane().setLayout(new BorderLayout());
        frmMessage.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        pnlMessage = new JPanel();
        pnlMessage.setLayout(new BorderLayout());
        frmMessage.getContentPane().add(pnlMessage, BorderLayout.CENTER);

        txtMessage = new JTextArea(6, 50);
        txtMessage.setText("");
        txtMessage.setBorder(BorderFactory.createEtchedBorder());
        txtMessage.setLineWrap(true);
        txtMessage.setWrapStyleWord(true);
        txtMessage.setEditable(false);

        pnlMessage.add(txtMessage);

        pnlButton = new JPanel();
        btnOK = new JButton(" OK ");
        btnOK.addActionListener(
            new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    if (e.getSource().equals(btnOK))
                    {
                        frmMessage.setVisible(false);
                    }
                }
            });
        pnlButton.add(btnOK);
        frmMessage.getContentPane().add(pnlButton, BorderLayout.SOUTH);
        frmMessage.pack();
        frmMessage.setResizable(false);
        frmMessage.setLocationRelativeTo(null);
        frmMessage.setVisible(false);
    }

    private void playSound()
    {
        try
        {
            File soundFile = new File(System.getenv("windir") + "/media/chimes.wav");
            AudioInputStream audioInput = AudioSystem.getAudioInputStream(new FileInputStream(soundFile));
            AudioFormat audioFormat = audioInput.getFormat();
            DataLine.Info dataLineInfo = new DataLine.Info(Clip.class, audioFormat);
            Clip clip = (Clip) AudioSystem.getLine(dataLineInfo);
            clip.open(audioInput);
            clip.start();
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();
        }
    }

    public void run()
    {
        try
        {
            ServerSocket server = new ServerSocket(i_PORT);
            System.out.println(server);
            while (true)
            {
                Socket conn = null;
                try
                {
                    conn = server.accept();
                    System.out.println(conn);
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    StringBuffer info = new StringBuffer(80);
                    while (true)
                    {
                        int i = in.read();
                        if (i == -1)
                        {
                            break;
                        }
                        else
                        {
                            info.append((char) i);
                        }
                    }
                    System.out.println("Receive Message From Server: " + info);
                    txtMessage.setText(info.toString());
                    frmMessage.setVisible(true);
                    playSound();
                }
                catch (IOException _oIOEx)
                {
                    _oIOEx.printStackTrace();
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.close();
                    }
                }
            }
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();
        }
    }
}
