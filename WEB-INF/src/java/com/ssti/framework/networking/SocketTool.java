package com.ssti.framework.networking;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.perf.StopWatch;

/**
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * TCP/IP Socket Utilities
 * <br>
 *
 * $@author  Author: LionZ $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class SocketTool 
{
	private static Log log = LogFactory.getLog(SocketTool.class);

	static SocketTool instance = null;
	
	public static synchronized SocketTool getInstance() 
	{
		if (instance == null) instance = new SocketTool();
		return instance;
	}	
	
    public static void writeToSocket(List vIP, int port, String msg)
    {
    	for (int i = 0; i < vIP.size(); i++)
    	{
    		writeToSocket((String)vIP.get(i), port, msg);
    	}
    }
	
    public static void writeToSocket(String[] ip, int port, String msg)
    {
    	for (int i = 0; i < ip.length; i++)
    	{
    		writeToSocket(ip[i], port, msg);
    	}
    }
	
    public static void writeToSocket(String ip, int port, String msg)
    {
        Socket socket = null;
        PrintWriter out = null;
        try
        {
            socket = new Socket(ip, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            out.write(msg);
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
            log.error("Unknown Host " + ip + " : " + e.getMessage(), e);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            log.error("IO Exception " + e.getMessage(), e);
        }
        finally
        {
        	if (socket != null)
        	{
        		try 
        		{
                    out.close();
        			socket.close();
				} 
        		catch (IOException e) 
        		{
					e.printStackTrace();
				}
        	}
        }
    }
    
    public static boolean ping()
    {
    	return ping("google.com", 80);
    }
    
    public static boolean ping(String host, int port)
    {
    	Socket socket = null;
        StopWatch sw = new StopWatch();
        sw.start("ping");
    	try
        {        	
        	socket = new Socket(host, port);
        	sw.stop("ping");
        	System.out.println("Connect successful in " + sw.result("ping"));
        	return true;
        }
        catch (ConnectException e)
        {
        	sw.stop("ping");
        	System.out.println("Failed connecting to " + host + ": " + e.getMessage() + sw.result("ping"));
            return false;
        }
        catch (UnknownHostException e)
        {
        	sw.stop("ping");
        	log.info("Unknown Host/No Connection " + host + " " + sw.result("ping"));
            return false;
        }
        catch (IOException e)
        {
        	sw.stop("ping");
        	//e.printStackTrace();
            log.error("IO Exception " + e.getMessage(), e);
            return false;
        }
        finally
        {
        	if (socket != null)
        	{
        		try 
        		{
        			socket.close();
				} 
        		catch (IOException e) 
        		{
					e.printStackTrace();
				}
        	}
        }
    }
}

