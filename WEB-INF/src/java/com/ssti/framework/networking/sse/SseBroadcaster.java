package com.ssti.framework.networking.sse;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class implements a one-to-many connection for broadcasting messages across multiple subscribers.
 *
 * @author <a href="http://github.com/mariomac">Mario Mac�as</a>
 */
public class SseBroadcaster 
{
	private static final Log log = LogFactory.getLog(SseBroadcaster.class);

	private Map<String,SseDispatcher> dispatchers = Collections.synchronizedMap(new HashMap<String,SseDispatcher>());

	/**
	 * Adds a subscriber to the broadcaster from a  {@link HttpServletRequest} reference.
	 * @param req The {@link HttpServletRequest} reference, as sent by the subscribers.
	 * @return the {@link SseDispatcher} object that will be used to communicate with the recently created subscriber
	 * @throws IOException if there was an error during the acknowledge process between broadcaster and subscriber
     */
	public SseDispatcher addSubscriber(String user, HttpServletRequest req) throws IOException {
		SseDispatcher dispatcher = null;
		synchronized (dispatchers) 
		{
			SseDispatcher prevdisp = (SseDispatcher) dispatchers.get(user);
			if(prevdisp != null) prevdisp.close();
			
			dispatcher = new SseDispatcher(req).ok().open();
			dispatchers.put(user,dispatcher);
		}
		System.out.println("Total Subscriber:" + dispatchers.size());
		return dispatcher;
	}

	/**
	 * Adds a subscriber to the broadcaster from a  {@link HttpServletRequest} reference. After the connection has been
	 * successfully established, the broadcaster sends a welcome message exclusively to this subscriber.
	 * @param req The {@link HttpServletRequest} reference, as sent by the subscribers.
	 * @param welcomeMessage The welcome message
	 * @return the {@link SseDispatcher} object that will be used to communicate with the recently created subscriber
	 * @throws IOException if there was an error during the acknowledge process between broadcaster and subscriber, or
	 *         if the subscriber immediately closed the connection before receiving the welcome message
	 */
	public SseDispatcher addSubscriber(String user, HttpServletRequest req, MessageEvent welcomeMessage) throws IOException {
		SseDispatcher dispatcher = null;
		synchronized (dispatchers) {
			dispatcher = new SseDispatcher(req).ok().open().send(welcomeMessage);
			dispatchers.put(user,dispatcher);
		}
		return dispatcher;
	}

	/**
	 * <p>Broadcasts a {@link MessageEvent} to all the subscribers, containing only 'event' and 'data' fields.</p>
	 *
	 * <p>This method relies on the {@link SseDispatcher#send(MessageEvent)} method. If this method throws an
	 * {@link IOException}, the broadcaster assumes the subscriber went offline and silently detaches it
	 * from the collection of subscribers.</p>
	 *
	 * @param event The descriptor of the 'event' field.
	 * @param data The content of the 'data' field.
	 */
	public void broadcast(String id, String event, String data) {
		broadcast(new MessageEvent.Builder()
				.setEvent(event)
				.setData(data)
				.setRetry(10000)
				.build());
	}

	/**
	 * <p>Broadcasts a {@link MessageEvent} to the subscribers.</p>
	 *
	 * <p>This method relies on the {@link SseDispatcher#send(MessageEvent)} method. If this method throws an
	 * {@link IOException}, the broadcaster assumes the subscriber went offline and silently detaches it
	 * from the collection of subscribers.</p>
	 *
	 * @param messageEvent The instance that encapsulates all the desired fields for the {@link MessageEvent}
	 */
	public void broadcast(MessageEvent messageEvent) 
	{
		Map<String,SseDispatcher> dispMap = new HashMap<String,SseDispatcher>(dispatchers.size());
		synchronized (dispatchers) 
		{
			for (String key : dispatchers.keySet())
			{
				dispMap.put(key, (SseDispatcher)dispatchers.get(key));
			}
		}
		for(String key : dispMap.keySet()) 
		{
			try 
			{
				SseDispatcher dispatcher = dispMap.get(key);
				dispatcher.send(messageEvent);				
			} 
			catch (IOException e) 
			{
				System.out.println("Non Active: " + key + " :" + e.getMessage());
				// Client disconnected. Removing from dispatchers
				dispatchers.remove(key);
			}
		}
		System.out.println("Active Subscribers:" + dispatchers.size());
	}

	/**
	 * Closes all the connections between the broadcaster and the subscribers, and detaches all of them from the
	 * collection of subscribers.
	 */
	public void close() {
		SseDispatcher[] disp;
		synchronized (dispatchers) 
		{
			for (String key : dispatchers.keySet())
			{
				SseDispatcher d = (SseDispatcher)dispatchers.get(key);		
				try 
				{
					d.close();
				} 
				catch (Exception e) {
					// Uncontrolled exception when closing a dispatcher. Removing anyway and ignoring.
				}
			}
			dispatchers.clear();
		}
	}
}
