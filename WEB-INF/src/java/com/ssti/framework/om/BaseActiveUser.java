package com.ssti.framework.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ActiveUser
 */
public abstract class BaseActiveUser extends BaseObject
{
    /** The Peer class */
    private static final ActiveUserPeer peer =
        new ActiveUserPeer();

        
    /** The value for the userName field */
    private String userName;
      
    /** The value for the sessionId field */
    private String sessionId;
      
    /** The value for the ipAddress field */
    private String ipAddress;
  
    
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SessionId
     *
     * @return String
     */
    public String getSessionId()
    {
        return sessionId;
    }

                        
    /**
     * Set the value of SessionId
     *
     * @param v new value
     */
    public void setSessionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sessionId, v))
              {
            this.sessionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IpAddress
     *
     * @return String
     */
    public String getIpAddress()
    {
        return ipAddress;
    }

                        
    /**
     * Set the value of IpAddress
     *
     * @param v new value
     */
    public void setIpAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.ipAddress, v))
              {
            this.ipAddress = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("UserName");
              fieldNames.add("SessionId");
              fieldNames.add("IpAddress");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("SessionId"))
        {
                return getSessionId();
            }
          if (name.equals("IpAddress"))
        {
                return getIpAddress();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ActiveUserPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(ActiveUserPeer.SESSION_ID))
        {
                return getSessionId();
            }
          if (name.equals(ActiveUserPeer.IP_ADDRESS))
        {
                return getIpAddress();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getUserName();
            }
              if (pos == 1)
        {
                return getSessionId();
            }
              if (pos == 2)
        {
                return getIpAddress();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ActiveUserPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ActiveUserPeer.doInsert((ActiveUser) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ActiveUserPeer.doUpdate((ActiveUser) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key userName ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setUserName(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setUserName(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getUserName());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ActiveUser copy() throws TorqueException
    {
        return copyInto(new ActiveUser());
    }
  
    protected ActiveUser copyInto(ActiveUser copyObj) throws TorqueException
    {
          copyObj.setUserName(userName);
          copyObj.setSessionId(sessionId);
          copyObj.setIpAddress(ipAddress);
  
                    copyObj.setUserName((String)null);
                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ActiveUserPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ActiveUser\n");
        str.append("----------\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("SessionId            : ")
           .append(getSessionId())
           .append("\n")
           .append("IpAddress            : ")
           .append(getIpAddress())
           .append("\n")
        ;
        return(str.toString());
    }
}
