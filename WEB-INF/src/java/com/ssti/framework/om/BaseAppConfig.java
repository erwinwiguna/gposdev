package com.ssti.framework.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AppConfig
 */
public abstract class BaseAppConfig extends BaseObject
{
    /** The Peer class */
    private static final AppConfigPeer peer =
        new AppConfigPeer();

        
    /** The value for the appConfigId field */
    private String appConfigId;
      
    /** The value for the part field */
    private String part;
      
    /** The value for the name field */
    private String name;
      
    /** The value for the value field */
    private String value;
  
    
    /**
     * Get the AppConfigId
     *
     * @return String
     */
    public String getAppConfigId()
    {
        return appConfigId;
    }

                        
    /**
     * Set the value of AppConfigId
     *
     * @param v new value
     */
    public void setAppConfigId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.appConfigId, v))
              {
            this.appConfigId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Part
     *
     * @return String
     */
    public String getPart()
    {
        return part;
    }

                        
    /**
     * Set the value of Part
     *
     * @param v new value
     */
    public void setPart(String v) 
    {
    
                  if (!ObjectUtils.equals(this.part, v))
              {
            this.part = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Name
     *
     * @return String
     */
    public String getName()
    {
        return name;
    }

                        
    /**
     * Set the value of Name
     *
     * @param v new value
     */
    public void setName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.name, v))
              {
            this.name = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value
     *
     * @return String
     */
    public String getValue()
    {
        return value;
    }

                        
    /**
     * Set the value of Value
     *
     * @param v new value
     */
    public void setValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value, v))
              {
            this.value = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AppConfigId");
              fieldNames.add("Part");
              fieldNames.add("Name");
              fieldNames.add("Value");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AppConfigId"))
        {
                return getAppConfigId();
            }
          if (name.equals("Part"))
        {
                return getPart();
            }
          if (name.equals("Name"))
        {
                return getName();
            }
          if (name.equals("Value"))
        {
                return getValue();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AppConfigPeer.APP_CONFIG_ID))
        {
                return getAppConfigId();
            }
          if (name.equals(AppConfigPeer.PART))
        {
                return getPart();
            }
          if (name.equals(AppConfigPeer.NAME))
        {
                return getName();
            }
          if (name.equals(AppConfigPeer.VALUE))
        {
                return getValue();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAppConfigId();
            }
              if (pos == 1)
        {
                return getPart();
            }
              if (pos == 2)
        {
                return getName();
            }
              if (pos == 3)
        {
                return getValue();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AppConfigPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AppConfigPeer.doInsert((AppConfig) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AppConfigPeer.doUpdate((AppConfig) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key appConfigId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAppConfigId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAppConfigId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAppConfigId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AppConfig copy() throws TorqueException
    {
        return copyInto(new AppConfig());
    }
  
    protected AppConfig copyInto(AppConfig copyObj) throws TorqueException
    {
          copyObj.setAppConfigId(appConfigId);
          copyObj.setPart(part);
          copyObj.setName(name);
          copyObj.setValue(value);
  
                    copyObj.setAppConfigId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AppConfigPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AppConfig\n");
        str.append("---------\n")
           .append("AppConfigId          : ")
           .append(getAppConfigId())
           .append("\n")
           .append("Part                 : ")
           .append(getPart())
           .append("\n")
           .append("Name                 : ")
           .append(getName())
           .append("\n")
           .append("Value                : ")
           .append(getValue())
           .append("\n")
        ;
        return(str.toString());
    }
}
