package com.ssti.framework.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.framework.om.map.CardioConfigMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseCardioConfigPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "cardio_config";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CardioConfigMapBuilder.CLASS_NAME);
    }

      /** the column name for the CARDIO_CONFIG_ID field */
    public static final String CARDIO_CONFIG_ID;
      /** the column name for the PRINTER_HOST field */
    public static final String PRINTER_HOST;
      /** the column name for the PRINTER_PORT field */
    public static final String PRINTER_PORT;
      /** the column name for the PRINTER_PAGE_LENGTH field */
    public static final String PRINTER_PAGE_LENGTH;
      /** the column name for the PRINTER_PRINT_MODE field */
    public static final String PRINTER_PRINT_MODE;
      /** the column name for the PRINTER_FORM_FEED field */
    public static final String PRINTER_FORM_FEED;
      /** the column name for the PRINTER_CUTPAPER field */
    public static final String PRINTER_CUTPAPER;
      /** the column name for the PRINTER_CUTPAPER_CMD field */
    public static final String PRINTER_CUTPAPER_CMD;
      /** the column name for the PRINTER_DETAIL_LINE field */
    public static final String PRINTER_DETAIL_LINE;
      /** the column name for the DRAWER_USE field */
    public static final String DRAWER_USE;
      /** the column name for the DRAWER_TYPE field */
    public static final String DRAWER_TYPE;
      /** the column name for the DRAWER_PORT field */
    public static final String DRAWER_PORT;
      /** the column name for the DRAWER_OPEN_COMMAND field */
    public static final String DRAWER_OPEN_COMMAND;
      /** the column name for the DISPLAYER_USE field */
    public static final String DISPLAYER_USE;
      /** the column name for the DISPLAYER_HOST field */
    public static final String DISPLAYER_HOST;
      /** the column name for the DISPLAYER_PORT field */
    public static final String DISPLAYER_PORT;
      /** the column name for the DISPLAYER_DEF_TEXT field */
    public static final String DISPLAYER_DEF_TEXT;
      /** the column name for the DISPLAYER_DEF_SCROLL field */
    public static final String DISPLAYER_DEF_SCROLL;
      /** the column name for the DISPLAYER_CLEAR_CMD field */
    public static final String DISPLAYER_CLEAR_CMD;
      /** the column name for the DISPLAYER_FL_PREF field */
    public static final String DISPLAYER_FL_PREF;
      /** the column name for the DISPLAYER_SL_PREF field */
    public static final String DISPLAYER_SL_PREF;
      /** the column name for the DISPLAYER_LINE1_TEXT field */
    public static final String DISPLAYER_LINE1_TEXT;
      /** the column name for the DISPLAYER_LINE2_TEXT field */
    public static final String DISPLAYER_LINE2_TEXT;
      /** the column name for the BARCODE_PRINTER_USE field */
    public static final String BARCODE_PRINTER_USE;
      /** the column name for the DEFAULT_NUMBER_LOCALE field */
    public static final String DEFAULT_NUMBER_LOCALE;
      /** the column name for the DEFAULT_NUMBER_FORMAT field */
    public static final String DEFAULT_NUMBER_FORMAT;
      /** the column name for the DEFAULT_SIMPLE_FORMAT field */
    public static final String DEFAULT_SIMPLE_FORMAT;
      /** the column name for the DEFAULT_ACC_FORMAT field */
    public static final String DEFAULT_ACC_FORMAT;
      /** the column name for the DEFAULT_ALIGNED_FORMAT field */
    public static final String DEFAULT_ALIGNED_FORMAT;
      /** the column name for the DEFAULT_DATE_FORMAT field */
    public static final String DEFAULT_DATE_FORMAT;
      /** the column name for the DEFAULT_DATETIME_FORMAT field */
    public static final String DEFAULT_DATETIME_FORMAT;
      /** the column name for the DEFAULT_TIME_FORMAT field */
    public static final String DEFAULT_TIME_FORMAT;
      /** the column name for the LONG_DATE_FORMAT field */
    public static final String LONG_DATE_FORMAT;
      /** the column name for the LONG_DATETIME_FORMAT field */
    public static final String LONG_DATETIME_FORMAT;
  
    static
    {
          CARDIO_CONFIG_ID = "cardio_config.CARDIO_CONFIG_ID";
          PRINTER_HOST = "cardio_config.PRINTER_HOST";
          PRINTER_PORT = "cardio_config.PRINTER_PORT";
          PRINTER_PAGE_LENGTH = "cardio_config.PRINTER_PAGE_LENGTH";
          PRINTER_PRINT_MODE = "cardio_config.PRINTER_PRINT_MODE";
          PRINTER_FORM_FEED = "cardio_config.PRINTER_FORM_FEED";
          PRINTER_CUTPAPER = "cardio_config.PRINTER_CUTPAPER";
          PRINTER_CUTPAPER_CMD = "cardio_config.PRINTER_CUTPAPER_CMD";
          PRINTER_DETAIL_LINE = "cardio_config.PRINTER_DETAIL_LINE";
          DRAWER_USE = "cardio_config.DRAWER_USE";
          DRAWER_TYPE = "cardio_config.DRAWER_TYPE";
          DRAWER_PORT = "cardio_config.DRAWER_PORT";
          DRAWER_OPEN_COMMAND = "cardio_config.DRAWER_OPEN_COMMAND";
          DISPLAYER_USE = "cardio_config.DISPLAYER_USE";
          DISPLAYER_HOST = "cardio_config.DISPLAYER_HOST";
          DISPLAYER_PORT = "cardio_config.DISPLAYER_PORT";
          DISPLAYER_DEF_TEXT = "cardio_config.DISPLAYER_DEF_TEXT";
          DISPLAYER_DEF_SCROLL = "cardio_config.DISPLAYER_DEF_SCROLL";
          DISPLAYER_CLEAR_CMD = "cardio_config.DISPLAYER_CLEAR_CMD";
          DISPLAYER_FL_PREF = "cardio_config.DISPLAYER_FL_PREF";
          DISPLAYER_SL_PREF = "cardio_config.DISPLAYER_SL_PREF";
          DISPLAYER_LINE1_TEXT = "cardio_config.DISPLAYER_LINE1_TEXT";
          DISPLAYER_LINE2_TEXT = "cardio_config.DISPLAYER_LINE2_TEXT";
          BARCODE_PRINTER_USE = "cardio_config.BARCODE_PRINTER_USE";
          DEFAULT_NUMBER_LOCALE = "cardio_config.DEFAULT_NUMBER_LOCALE";
          DEFAULT_NUMBER_FORMAT = "cardio_config.DEFAULT_NUMBER_FORMAT";
          DEFAULT_SIMPLE_FORMAT = "cardio_config.DEFAULT_SIMPLE_FORMAT";
          DEFAULT_ACC_FORMAT = "cardio_config.DEFAULT_ACC_FORMAT";
          DEFAULT_ALIGNED_FORMAT = "cardio_config.DEFAULT_ALIGNED_FORMAT";
          DEFAULT_DATE_FORMAT = "cardio_config.DEFAULT_DATE_FORMAT";
          DEFAULT_DATETIME_FORMAT = "cardio_config.DEFAULT_DATETIME_FORMAT";
          DEFAULT_TIME_FORMAT = "cardio_config.DEFAULT_TIME_FORMAT";
          LONG_DATE_FORMAT = "cardio_config.LONG_DATE_FORMAT";
          LONG_DATETIME_FORMAT = "cardio_config.LONG_DATETIME_FORMAT";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CardioConfigMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CardioConfigMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  34;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.framework.om.CardioConfig";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCardioConfigPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                              
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CARDIO_CONFIG_ID);
          criteria.addSelectColumn(PRINTER_HOST);
          criteria.addSelectColumn(PRINTER_PORT);
          criteria.addSelectColumn(PRINTER_PAGE_LENGTH);
          criteria.addSelectColumn(PRINTER_PRINT_MODE);
          criteria.addSelectColumn(PRINTER_FORM_FEED);
          criteria.addSelectColumn(PRINTER_CUTPAPER);
          criteria.addSelectColumn(PRINTER_CUTPAPER_CMD);
          criteria.addSelectColumn(PRINTER_DETAIL_LINE);
          criteria.addSelectColumn(DRAWER_USE);
          criteria.addSelectColumn(DRAWER_TYPE);
          criteria.addSelectColumn(DRAWER_PORT);
          criteria.addSelectColumn(DRAWER_OPEN_COMMAND);
          criteria.addSelectColumn(DISPLAYER_USE);
          criteria.addSelectColumn(DISPLAYER_HOST);
          criteria.addSelectColumn(DISPLAYER_PORT);
          criteria.addSelectColumn(DISPLAYER_DEF_TEXT);
          criteria.addSelectColumn(DISPLAYER_DEF_SCROLL);
          criteria.addSelectColumn(DISPLAYER_CLEAR_CMD);
          criteria.addSelectColumn(DISPLAYER_FL_PREF);
          criteria.addSelectColumn(DISPLAYER_SL_PREF);
          criteria.addSelectColumn(DISPLAYER_LINE1_TEXT);
          criteria.addSelectColumn(DISPLAYER_LINE2_TEXT);
          criteria.addSelectColumn(BARCODE_PRINTER_USE);
          criteria.addSelectColumn(DEFAULT_NUMBER_LOCALE);
          criteria.addSelectColumn(DEFAULT_NUMBER_FORMAT);
          criteria.addSelectColumn(DEFAULT_SIMPLE_FORMAT);
          criteria.addSelectColumn(DEFAULT_ACC_FORMAT);
          criteria.addSelectColumn(DEFAULT_ALIGNED_FORMAT);
          criteria.addSelectColumn(DEFAULT_DATE_FORMAT);
          criteria.addSelectColumn(DEFAULT_DATETIME_FORMAT);
          criteria.addSelectColumn(DEFAULT_TIME_FORMAT);
          criteria.addSelectColumn(LONG_DATE_FORMAT);
          criteria.addSelectColumn(LONG_DATETIME_FORMAT);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CardioConfig row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CardioConfig obj = (CardioConfig) cls.newInstance();
            CardioConfigPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CardioConfig obj)
        throws TorqueException
    {
        try
        {
                obj.setCardioConfigId(row.getValue(offset + 0).asString());
                  obj.setPrinterHost(row.getValue(offset + 1).asString());
                  obj.setPrinterPort(row.getValue(offset + 2).asString());
                  obj.setPrinterPageLength(row.getValue(offset + 3).asString());
                  obj.setPrinterPrintMode(row.getValue(offset + 4).asString());
                  obj.setPrinterFormFeed(row.getValue(offset + 5).asString());
                  obj.setPrinterCutpaper(row.getValue(offset + 6).asString());
                  obj.setPrinterCutpaperCmd(row.getValue(offset + 7).asString());
                  obj.setPrinterDetailLine(row.getValue(offset + 8).asString());
                  obj.setDrawerUse(row.getValue(offset + 9).asString());
                  obj.setDrawerType(row.getValue(offset + 10).asString());
                  obj.setDrawerPort(row.getValue(offset + 11).asString());
                  obj.setDrawerOpenCommand(row.getValue(offset + 12).asString());
                  obj.setDisplayerUse(row.getValue(offset + 13).asString());
                  obj.setDisplayerHost(row.getValue(offset + 14).asString());
                  obj.setDisplayerPort(row.getValue(offset + 15).asString());
                  obj.setDisplayerDefText(row.getValue(offset + 16).asString());
                  obj.setDisplayerDefScroll(row.getValue(offset + 17).asString());
                  obj.setDisplayerClearCmd(row.getValue(offset + 18).asString());
                  obj.setDisplayerFlPref(row.getValue(offset + 19).asString());
                  obj.setDisplayerSlPref(row.getValue(offset + 20).asString());
                  obj.setDisplayerLine1Text(row.getValue(offset + 21).asString());
                  obj.setDisplayerLine2Text(row.getValue(offset + 22).asString());
                  obj.setBarcodePrinterUse(row.getValue(offset + 23).asString());
                  obj.setDefaultNumberLocale(row.getValue(offset + 24).asString());
                  obj.setDefaultNumberFormat(row.getValue(offset + 25).asString());
                  obj.setDefaultSimpleFormat(row.getValue(offset + 26).asString());
                  obj.setDefaultAccFormat(row.getValue(offset + 27).asString());
                  obj.setDefaultAlignedFormat(row.getValue(offset + 28).asString());
                  obj.setDefaultDateFormat(row.getValue(offset + 29).asString());
                  obj.setDefaultDatetimeFormat(row.getValue(offset + 30).asString());
                  obj.setDefaultTimeFormat(row.getValue(offset + 31).asString());
                  obj.setLongDateFormat(row.getValue(offset + 32).asString());
                  obj.setLongDatetimeFormat(row.getValue(offset + 33).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCardioConfigPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                              
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CardioConfigPeer.row2Object(row, 1,
                CardioConfigPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCardioConfigPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CARDIO_CONFIG_ID, criteria.remove(CARDIO_CONFIG_ID));
                                                                                                                                                                                                                                                                                                                                                
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CardioConfigPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                              
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CardioConfig obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CardioConfig obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CardioConfig obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CardioConfig obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CardioConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CardioConfig obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CardioConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CardioConfig obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CardioConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CardioConfig obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCardioConfigPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CARDIO_CONFIG_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CardioConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CARDIO_CONFIG_ID, obj.getCardioConfigId());
              criteria.add(PRINTER_HOST, obj.getPrinterHost());
              criteria.add(PRINTER_PORT, obj.getPrinterPort());
              criteria.add(PRINTER_PAGE_LENGTH, obj.getPrinterPageLength());
              criteria.add(PRINTER_PRINT_MODE, obj.getPrinterPrintMode());
              criteria.add(PRINTER_FORM_FEED, obj.getPrinterFormFeed());
              criteria.add(PRINTER_CUTPAPER, obj.getPrinterCutpaper());
              criteria.add(PRINTER_CUTPAPER_CMD, obj.getPrinterCutpaperCmd());
              criteria.add(PRINTER_DETAIL_LINE, obj.getPrinterDetailLine());
              criteria.add(DRAWER_USE, obj.getDrawerUse());
              criteria.add(DRAWER_TYPE, obj.getDrawerType());
              criteria.add(DRAWER_PORT, obj.getDrawerPort());
              criteria.add(DRAWER_OPEN_COMMAND, obj.getDrawerOpenCommand());
              criteria.add(DISPLAYER_USE, obj.getDisplayerUse());
              criteria.add(DISPLAYER_HOST, obj.getDisplayerHost());
              criteria.add(DISPLAYER_PORT, obj.getDisplayerPort());
              criteria.add(DISPLAYER_DEF_TEXT, obj.getDisplayerDefText());
              criteria.add(DISPLAYER_DEF_SCROLL, obj.getDisplayerDefScroll());
              criteria.add(DISPLAYER_CLEAR_CMD, obj.getDisplayerClearCmd());
              criteria.add(DISPLAYER_FL_PREF, obj.getDisplayerFlPref());
              criteria.add(DISPLAYER_SL_PREF, obj.getDisplayerSlPref());
              criteria.add(DISPLAYER_LINE1_TEXT, obj.getDisplayerLine1Text());
              criteria.add(DISPLAYER_LINE2_TEXT, obj.getDisplayerLine2Text());
              criteria.add(BARCODE_PRINTER_USE, obj.getBarcodePrinterUse());
              criteria.add(DEFAULT_NUMBER_LOCALE, obj.getDefaultNumberLocale());
              criteria.add(DEFAULT_NUMBER_FORMAT, obj.getDefaultNumberFormat());
              criteria.add(DEFAULT_SIMPLE_FORMAT, obj.getDefaultSimpleFormat());
              criteria.add(DEFAULT_ACC_FORMAT, obj.getDefaultAccFormat());
              criteria.add(DEFAULT_ALIGNED_FORMAT, obj.getDefaultAlignedFormat());
              criteria.add(DEFAULT_DATE_FORMAT, obj.getDefaultDateFormat());
              criteria.add(DEFAULT_DATETIME_FORMAT, obj.getDefaultDatetimeFormat());
              criteria.add(DEFAULT_TIME_FORMAT, obj.getDefaultTimeFormat());
              criteria.add(LONG_DATE_FORMAT, obj.getLongDateFormat());
              criteria.add(LONG_DATETIME_FORMAT, obj.getLongDatetimeFormat());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CardioConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CARDIO_CONFIG_ID, obj.getCardioConfigId());
                          criteria.add(PRINTER_HOST, obj.getPrinterHost());
                          criteria.add(PRINTER_PORT, obj.getPrinterPort());
                          criteria.add(PRINTER_PAGE_LENGTH, obj.getPrinterPageLength());
                          criteria.add(PRINTER_PRINT_MODE, obj.getPrinterPrintMode());
                          criteria.add(PRINTER_FORM_FEED, obj.getPrinterFormFeed());
                          criteria.add(PRINTER_CUTPAPER, obj.getPrinterCutpaper());
                          criteria.add(PRINTER_CUTPAPER_CMD, obj.getPrinterCutpaperCmd());
                          criteria.add(PRINTER_DETAIL_LINE, obj.getPrinterDetailLine());
                          criteria.add(DRAWER_USE, obj.getDrawerUse());
                          criteria.add(DRAWER_TYPE, obj.getDrawerType());
                          criteria.add(DRAWER_PORT, obj.getDrawerPort());
                          criteria.add(DRAWER_OPEN_COMMAND, obj.getDrawerOpenCommand());
                          criteria.add(DISPLAYER_USE, obj.getDisplayerUse());
                          criteria.add(DISPLAYER_HOST, obj.getDisplayerHost());
                          criteria.add(DISPLAYER_PORT, obj.getDisplayerPort());
                          criteria.add(DISPLAYER_DEF_TEXT, obj.getDisplayerDefText());
                          criteria.add(DISPLAYER_DEF_SCROLL, obj.getDisplayerDefScroll());
                          criteria.add(DISPLAYER_CLEAR_CMD, obj.getDisplayerClearCmd());
                          criteria.add(DISPLAYER_FL_PREF, obj.getDisplayerFlPref());
                          criteria.add(DISPLAYER_SL_PREF, obj.getDisplayerSlPref());
                          criteria.add(DISPLAYER_LINE1_TEXT, obj.getDisplayerLine1Text());
                          criteria.add(DISPLAYER_LINE2_TEXT, obj.getDisplayerLine2Text());
                          criteria.add(BARCODE_PRINTER_USE, obj.getBarcodePrinterUse());
                          criteria.add(DEFAULT_NUMBER_LOCALE, obj.getDefaultNumberLocale());
                          criteria.add(DEFAULT_NUMBER_FORMAT, obj.getDefaultNumberFormat());
                          criteria.add(DEFAULT_SIMPLE_FORMAT, obj.getDefaultSimpleFormat());
                          criteria.add(DEFAULT_ACC_FORMAT, obj.getDefaultAccFormat());
                          criteria.add(DEFAULT_ALIGNED_FORMAT, obj.getDefaultAlignedFormat());
                          criteria.add(DEFAULT_DATE_FORMAT, obj.getDefaultDateFormat());
                          criteria.add(DEFAULT_DATETIME_FORMAT, obj.getDefaultDatetimeFormat());
                          criteria.add(DEFAULT_TIME_FORMAT, obj.getDefaultTimeFormat());
                          criteria.add(LONG_DATE_FORMAT, obj.getLongDateFormat());
                          criteria.add(LONG_DATETIME_FORMAT, obj.getLongDatetimeFormat());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CardioConfig retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CardioConfig retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CardioConfig retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CardioConfig retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CardioConfig retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CardioConfig)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CARDIO_CONFIG_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}
