package com.ssti.framework.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to DbHistory
 */
public abstract class BaseDbHistory extends BaseObject
{
    /** The Peer class */
    private static final DbHistoryPeer peer =
        new DbHistoryPeer();

        
    /** The value for the dbHistoryId field */
    private String dbHistoryId;
      
    /** The value for the dbVersion field */
    private String dbVersion;
      
    /** The value for the versionDate field */
    private String versionDate;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the updateScript field */
    private String updateScript;
  
    
    /**
     * Get the DbHistoryId
     *
     * @return String
     */
    public String getDbHistoryId()
    {
        return dbHistoryId;
    }

                        
    /**
     * Set the value of DbHistoryId
     *
     * @param v new value
     */
    public void setDbHistoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.dbHistoryId, v))
              {
            this.dbHistoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DbVersion
     *
     * @return String
     */
    public String getDbVersion()
    {
        return dbVersion;
    }

                        
    /**
     * Set the value of DbVersion
     *
     * @param v new value
     */
    public void setDbVersion(String v) 
    {
    
                  if (!ObjectUtils.equals(this.dbVersion, v))
              {
            this.dbVersion = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VersionDate
     *
     * @return String
     */
    public String getVersionDate()
    {
        return versionDate;
    }

                        
    /**
     * Set the value of VersionDate
     *
     * @param v new value
     */
    public void setVersionDate(String v) 
    {
    
                  if (!ObjectUtils.equals(this.versionDate, v))
              {
            this.versionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateScript
     *
     * @return String
     */
    public String getUpdateScript()
    {
        return updateScript;
    }

                        
    /**
     * Set the value of UpdateScript
     *
     * @param v new value
     */
    public void setUpdateScript(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updateScript, v))
              {
            this.updateScript = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DbHistoryId");
              fieldNames.add("DbVersion");
              fieldNames.add("VersionDate");
              fieldNames.add("UpdateDate");
              fieldNames.add("UpdateScript");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DbHistoryId"))
        {
                return getDbHistoryId();
            }
          if (name.equals("DbVersion"))
        {
                return getDbVersion();
            }
          if (name.equals("VersionDate"))
        {
                return getVersionDate();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("UpdateScript"))
        {
                return getUpdateScript();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DbHistoryPeer.DB_HISTORY_ID))
        {
                return getDbHistoryId();
            }
          if (name.equals(DbHistoryPeer.DB_VERSION))
        {
                return getDbVersion();
            }
          if (name.equals(DbHistoryPeer.VERSION_DATE))
        {
                return getVersionDate();
            }
          if (name.equals(DbHistoryPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(DbHistoryPeer.UPDATE_SCRIPT))
        {
                return getUpdateScript();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDbHistoryId();
            }
              if (pos == 1)
        {
                return getDbVersion();
            }
              if (pos == 2)
        {
                return getVersionDate();
            }
              if (pos == 3)
        {
                return getUpdateDate();
            }
              if (pos == 4)
        {
                return getUpdateScript();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DbHistoryPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DbHistoryPeer.doInsert((DbHistory) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DbHistoryPeer.doUpdate((DbHistory) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key dbHistoryId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDbHistoryId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDbHistoryId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDbHistoryId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public DbHistory copy() throws TorqueException
    {
        return copyInto(new DbHistory());
    }
  
    protected DbHistory copyInto(DbHistory copyObj) throws TorqueException
    {
          copyObj.setDbHistoryId(dbHistoryId);
          copyObj.setDbVersion(dbVersion);
          copyObj.setVersionDate(versionDate);
          copyObj.setUpdateDate(updateDate);
          copyObj.setUpdateScript(updateScript);
  
                    copyObj.setDbHistoryId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DbHistoryPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("DbHistory\n");
        str.append("---------\n")
           .append("DbHistoryId          : ")
           .append(getDbHistoryId())
           .append("\n")
           .append("DbVersion            : ")
           .append(getDbVersion())
           .append("\n")
           .append("VersionDate          : ")
           .append(getVersionDate())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("UpdateScript         : ")
           .append(getUpdateScript())
           .append("\n")
        ;
        return(str.toString());
    }
}
