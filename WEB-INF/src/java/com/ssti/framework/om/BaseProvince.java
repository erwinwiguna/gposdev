package com.ssti.framework.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Province
 */
public abstract class BaseProvince extends BaseObject
{
    /** The Peer class */
    private static final ProvincePeer peer =
        new ProvincePeer();

        
    /** The value for the provinceId field */
    private String provinceId;
      
    /** The value for the province field */
    private String province;
  
    
    /**
     * Get the ProvinceId
     *
     * @return String
     */
    public String getProvinceId()
    {
        return provinceId;
    }

                                              
    /**
     * Set the value of ProvinceId
     *
     * @param v new value
     */
    public void setProvinceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.provinceId, v))
              {
            this.provinceId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated Regency
        if (collRegencys != null)
        {
            for (int i = 0; i < collRegencys.size(); i++)
            {
                ((Regency) collRegencys.get(i))
                    .setProvinceId(v);
            }
        }
                                }
  
    /**
     * Get the Province
     *
     * @return String
     */
    public String getProvince()
    {
        return province;
    }

                        
    /**
     * Set the value of Province
     *
     * @param v new value
     */
    public void setProvince(String v) 
    {
    
                  if (!ObjectUtils.equals(this.province, v))
              {
            this.province = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collRegencys
     */
    protected List collRegencys;

    /**
     * Temporary storage of collRegencys to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initRegencys()
    {
        if (collRegencys == null)
        {
            collRegencys = new ArrayList();
        }
    }

    /**
     * Method called to associate a Regency object to this object
     * through the Regency foreign key attribute
     *
     * @param l Regency
     * @throws TorqueException
     */
    public void addRegency(Regency l) throws TorqueException
    {
        getRegencys().add(l);
        l.setProvince((Province) this);
    }

    /**
     * The criteria used to select the current contents of collRegencys
     */
    private Criteria lastRegencysCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getRegencys(new Criteria())
     *
     * @throws TorqueException
     */
    public List getRegencys() throws TorqueException
    {
              if (collRegencys == null)
        {
            collRegencys = getRegencys(new Criteria(10));
        }
        return collRegencys;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Province has previously
     * been saved, it will retrieve related Regencys from storage.
     * If this Province is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getRegencys(Criteria criteria) throws TorqueException
    {
              if (collRegencys == null)
        {
            if (isNew())
            {
               collRegencys = new ArrayList();
            }
            else
            {
                        criteria.add(RegencyPeer.PROVINCE_ID, getProvinceId() );
                        collRegencys = RegencyPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(RegencyPeer.PROVINCE_ID, getProvinceId());
                            if (!lastRegencysCriteria.equals(criteria))
                {
                    collRegencys = RegencyPeer.doSelect(criteria);
                }
            }
        }
        lastRegencysCriteria = criteria;

        return collRegencys;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getRegencys(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getRegencys(Connection con) throws TorqueException
    {
              if (collRegencys == null)
        {
            collRegencys = getRegencys(new Criteria(10), con);
        }
        return collRegencys;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Province has previously
     * been saved, it will retrieve related Regencys from storage.
     * If this Province is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getRegencys(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collRegencys == null)
        {
            if (isNew())
            {
               collRegencys = new ArrayList();
            }
            else
            {
                         criteria.add(RegencyPeer.PROVINCE_ID, getProvinceId());
                         collRegencys = RegencyPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(RegencyPeer.PROVINCE_ID, getProvinceId());
                             if (!lastRegencysCriteria.equals(criteria))
                 {
                     collRegencys = RegencyPeer.doSelect(criteria, con);
                 }
             }
         }
         lastRegencysCriteria = criteria;

         return collRegencys;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Province is new, it will return
     * an empty collection; or if this Province has previously
     * been saved, it will retrieve related Regencys from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Province.
     */
    protected List getRegencysJoinProvince(Criteria criteria)
        throws TorqueException
    {
                    if (collRegencys == null)
        {
            if (isNew())
            {
               collRegencys = new ArrayList();
            }
            else
            {
                              criteria.add(RegencyPeer.PROVINCE_ID, getProvinceId());
                              collRegencys = RegencyPeer.doSelectJoinProvince(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(RegencyPeer.PROVINCE_ID, getProvinceId());
                                    if (!lastRegencysCriteria.equals(criteria))
            {
                collRegencys = RegencyPeer.doSelectJoinProvince(criteria);
            }
        }
        lastRegencysCriteria = criteria;

        return collRegencys;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ProvinceId");
              fieldNames.add("Province");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ProvinceId"))
        {
                return getProvinceId();
            }
          if (name.equals("Province"))
        {
                return getProvince();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ProvincePeer.PROVINCE_ID))
        {
                return getProvinceId();
            }
          if (name.equals(ProvincePeer.PROVINCE))
        {
                return getProvince();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getProvinceId();
            }
              if (pos == 1)
        {
                return getProvince();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ProvincePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ProvincePeer.doInsert((Province) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ProvincePeer.doUpdate((Province) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collRegencys != null)
            {
                for (int i = 0; i < collRegencys.size(); i++)
                {
                    ((Regency) collRegencys.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key provinceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setProvinceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setProvinceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getProvinceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Province copy() throws TorqueException
    {
        return copyInto(new Province());
    }
  
    protected Province copyInto(Province copyObj) throws TorqueException
    {
          copyObj.setProvinceId(provinceId);
          copyObj.setProvince(province);
  
                    copyObj.setProvinceId((String)null);
                  
                                      
                            
        List v = getRegencys();
        for (int i = 0; i < v.size(); i++)
        {
            Regency obj = (Regency) v.get(i);
            copyObj.addRegency(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ProvincePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Province\n");
        str.append("--------\n")
           .append("ProvinceId           : ")
           .append(getProvinceId())
           .append("\n")
           .append("Province             : ")
           .append(getProvince())
           .append("\n")
        ;
        return(str.toString());
    }
}
