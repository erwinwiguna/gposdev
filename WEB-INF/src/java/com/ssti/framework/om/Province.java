
package com.ssti.framework.om;


import java.util.ArrayList;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class Province
    extends com.ssti.framework.om.BaseProvince
    implements Persistent
{
	static List empty = new ArrayList(1);
	
	public String getId() {
		return getProvinceId();
	}

	public String getCode() {
		return getProvince();
	}

	public String getName() {
		return getProvince();
	}
	
	public List getRegencies()
	{
		if(StringUtil.isNotEmpty(getProvinceId()))
		{
			try {
				return AddressTool.getInstance().getRegency(getProvinceId());	
			} catch (Exception e) {
			}
		}
		return empty;
	}	
}
