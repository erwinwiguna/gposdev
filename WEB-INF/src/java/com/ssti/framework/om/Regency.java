
package com.ssti.framework.om;


import java.util.ArrayList;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class Regency
    extends com.ssti.framework.om.BaseRegency
    implements Persistent
{
	static List empty = new ArrayList(1);
	
	public String getId() {
		return getRegencyId();
	}

	public String getCode() {
		return getRegency();
	}

	public String getName() {
		return getRegency();
	}
	
	public List getDistricts()
	{
		if(StringUtil.isNotEmpty(getRegencyId()))
		{
			try {
				return AddressTool.getInstance().getDistrict(getRegencyId());	
			} catch (Exception e) {
			}
		}
		return empty;
	}	
}
