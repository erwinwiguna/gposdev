package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ActiveUserMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.ActiveUserMapBuilder";

    /**
     * Item
     * @deprecated use ActiveUserPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "active_user";
    }

  
    /**
     * active_user.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use ActiveUserPeer.active_user.USER_NAME constant
     */
    public static String getActiveUser_UserName()
    {
        return "active_user.USER_NAME";
    }
  
    /**
     * active_user.SESSION_ID
     * @return the column name for the SESSION_ID field
     * @deprecated use ActiveUserPeer.active_user.SESSION_ID constant
     */
    public static String getActiveUser_SessionId()
    {
        return "active_user.SESSION_ID";
    }
  
    /**
     * active_user.IP_ADDRESS
     * @return the column name for the IP_ADDRESS field
     * @deprecated use ActiveUserPeer.active_user.IP_ADDRESS constant
     */
    public static String getActiveUser_IpAddress()
    {
        return "active_user.IP_ADDRESS";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("active_user");
        TableMap tMap = dbMap.getTable("active_user");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("active_user.USER_NAME", "");
                          tMap.addColumn("active_user.SESSION_ID", "");
                          tMap.addColumn("active_user.IP_ADDRESS", "");
          }
}
