package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ProvinceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.ProvinceMapBuilder";

    /**
     * Item
     * @deprecated use ProvincePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "province";
    }

  
    /**
     * province.PROVINCE_ID
     * @return the column name for the PROVINCE_ID field
     * @deprecated use ProvincePeer.province.PROVINCE_ID constant
     */
    public static String getProvince_ProvinceId()
    {
        return "province.PROVINCE_ID";
    }
  
    /**
     * province.PROVINCE
     * @return the column name for the PROVINCE field
     * @deprecated use ProvincePeer.province.PROVINCE constant
     */
    public static String getProvince_Province()
    {
        return "province.PROVINCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("province");
        TableMap tMap = dbMap.getTable("province");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("province.PROVINCE_ID", "");
                          tMap.addColumn("province.PROVINCE", "");
          }
}
