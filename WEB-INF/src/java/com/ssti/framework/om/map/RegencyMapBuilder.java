package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class RegencyMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.RegencyMapBuilder";

    /**
     * Item
     * @deprecated use RegencyPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "regency";
    }

  
    /**
     * regency.REGENCY_ID
     * @return the column name for the REGENCY_ID field
     * @deprecated use RegencyPeer.regency.REGENCY_ID constant
     */
    public static String getRegency_RegencyId()
    {
        return "regency.REGENCY_ID";
    }
  
    /**
     * regency.PROVINCE_ID
     * @return the column name for the PROVINCE_ID field
     * @deprecated use RegencyPeer.regency.PROVINCE_ID constant
     */
    public static String getRegency_ProvinceId()
    {
        return "regency.PROVINCE_ID";
    }
  
    /**
     * regency.REGENCY
     * @return the column name for the REGENCY field
     * @deprecated use RegencyPeer.regency.REGENCY constant
     */
    public static String getRegency_Regency()
    {
        return "regency.REGENCY";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("regency");
        TableMap tMap = dbMap.getTable("regency");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("regency.REGENCY_ID", "");
                          tMap.addForeignKey(
                "regency.PROVINCE_ID", "" , "province" ,
                "province_id");
                          tMap.addColumn("regency.REGENCY", "");
          }
}
