package com.ssti.framework.parser;

import java.util.Calendar;
import java.util.Date;

import com.ssti.framework.tools.DateUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Min, Max, Reorder Formula Parser
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: FormulaParser.java,v 1.6 2007/02/23 14:13:02 albert Exp $ <br>
 *
 * <pre>
 * $Log: FormulaParser.java,v $
 * Revision 1.6  2007/02/23 14:13:02  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class FormulaParser
{   
	public static final int i_START = 0;
	public static final int i_END   = 1;
	
	private Date m_dStartMonth;
	private Date m_dEndMonth;
	
	private int m_iStartYear;
	private int m_iEndYear;
	
	private String m_sFormula;
	private Date m_dToday;
	
	/**
	 * 
	 * @param _sFormula
	 */
	public FormulaParser(String _sFormula)
	{
		this.m_sFormula = _sFormula;     
		this.m_dToday = new Date();   
		initParser();
	}
	
	public Date[] getResult()
	{
		Date[] aResult = {m_dStartMonth, m_dEndMonth};
		return aResult;
	}
	
	private void initParser()
	{
		if(m_sFormula.indexOf("M") != -1 && m_sFormula.indexOf("Y") == -1)
		{
			setMonthFormula();
		}
		else if(m_sFormula.indexOf("Y") != -1 && m_sFormula.indexOf("M") == -1)
		{
			setYearFormula();
		}
		else if(m_sFormula.indexOf("Y") != -1 &&  
				m_sFormula.indexOf("M") != -1 &&  
				m_sFormula.indexOf("Y") < m_sFormula.indexOf("M"))
		{
			String sYear = m_sFormula.substring(0,m_sFormula.indexOf("M"));			
			
			if(sYear.indexOf("(") != -1 && sYear.indexOf(")") != -1)
			{
			}
			else if(m_sFormula.indexOf("AVG") != -1)
			{
			}
			else
			{
				setYearMonthFormula();
			}
		}
	}
	
	private void setMonthFormula()
	{
		int iMonthLimitStart;
		int iMonthLimitEnd;
		int iMonthLast;
		int iMonth;
		
		String sTemp;
		Calendar oCal = Calendar.getInstance();
		
		//month
		if(m_sFormula.indexOf("M") != -1)
		{
			sTemp = m_sFormula.substring(m_sFormula.indexOf("M"),m_sFormula.length());
			iMonthLast = sTemp.indexOf("L");
			iMonthLimitStart = sTemp.indexOf("(");
			iMonthLimitEnd = sTemp.indexOf(")");
			
			if(iMonthLast != -1)
			{				
				if(sTemp.indexOf("td") != -1)
				{
					oCal.set(oCal.get(Calendar.YEAR),oCal.get(Calendar.MONTH),oCal.get(Calendar.DATE));
					m_dEndMonth= oCal.getTime();
				}
				else
				{
					
					oCal.add(Calendar.MONTH,-1);
					oCal.set(oCal.get(Calendar.YEAR),oCal.get(Calendar.MONTH),
							DateUtil.aLastDateOfMonth[oCal.get(Calendar.MONTH)]);
					
					m_dEndMonth= oCal.getTime();
					oCal.add(Calendar.MONTH,1);
				} 
				
				
				sTemp = sTemp.substring(sTemp.indexOf("M")+1,sTemp.indexOf("L"));
				iMonth = 0 - Integer.parseInt(sTemp);
				
				oCal.add(Calendar.MONTH,iMonth);
				oCal.set(oCal.get(Calendar.YEAR),oCal.get(Calendar.MONTH),1);
				m_dStartMonth = oCal.getTime();
			}
			else
			{
				if(iMonthLimitStart != -1 && iMonthLimitEnd != -1)
				{
					sTemp = sTemp.substring(sTemp.indexOf("(") + 1, sTemp.indexOf(")"));
					if(sTemp.indexOf("-") == -1)
					{
						int iMonthSelected = Integer.parseInt(sTemp);
						oCal.set(oCal.get(Calendar.YEAR),iMonthSelected-1,1);
						
						m_dStartMonth = oCal.getTime();
						
						oCal.set(oCal.get(Calendar.YEAR),
							iMonthSelected - 1, DateUtil.aLastDateOfMonth[iMonthSelected-1]);
						
						m_dEndMonth= oCal.getTime();						
					}
					else
					{
						String sMonthStart = sTemp.substring(0,sTemp.indexOf("-"));
						String sMonthEnd = sTemp.substring(sTemp.indexOf("-") + 1, sTemp.length());
						int iMonthStart = Integer.parseInt(sMonthStart);
						int iMonthEnd = Integer.parseInt(sMonthEnd);
						
						oCal.set(oCal.get(Calendar.YEAR),iMonthStart-1,1);
						
						m_dStartMonth = oCal.getTime();
						
						oCal.add(Calendar.MONTH,iMonthEnd);
						oCal.set(oCal.get(Calendar.YEAR),iMonthEnd-1,DateUtil.aLastDateOfMonth[iMonthEnd-1]);
						
						m_dEndMonth= oCal.getTime();						
					}
				}
			}
		}
	}
	
	private void setYearFormula()
	{
		int iYearLast;
		int iYear;
		
		String sTemp;
		Calendar oCal = Calendar.getInstance();
		
		//Year
		iYearLast = m_sFormula.indexOf("L");
		
		if(m_sFormula.indexOf("Y") != -1)
		{
			if(iYearLast != -1)
			{
				sTemp = m_sFormula.substring(m_sFormula.indexOf("Y") + 1, m_sFormula.indexOf("L"));
				iYear = 0 - Integer.parseInt(sTemp);
				
				if(m_sFormula.indexOf("M") == -1)
				{
					
					if(m_sFormula.indexOf("tm") != -1)
					{
						oCal.add(Calendar.MONTH,-1);
						oCal.set(oCal.get(Calendar.YEAR),
							oCal.get(Calendar.MONTH) - 1,
								DateUtil.aLastDateOfMonth[oCal.get(Calendar.MONTH ) - 1]);
						
						m_dEndMonth= oCal.getTime();
					}
					else if(m_sFormula.indexOf("td") != -1)
					{
						oCal.set(Calendar.getInstance().get(Calendar.YEAR),
							Calendar.getInstance().get(Calendar.MONTH),
								Calendar.getInstance().get(Calendar.DATE));
						
						m_dEndMonth= oCal.getTime();
					}
					else
					{
						oCal.set(Calendar.getInstance().get(Calendar.YEAR) - 1 ,11,
							DateUtil.aLastDateOfMonth[11]);
						m_dEndMonth= oCal.getTime();
						
						oCal.set(Calendar.getInstance().get(Calendar.YEAR),
							Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DATE));
					} 
					
					oCal.add(Calendar.YEAR,iYear);
					oCal.set(oCal.get(Calendar.YEAR),0,1);
					
					m_dStartMonth = oCal.getTime();
				}
							
				m_iStartYear= oCal.get(Calendar.YEAR);
				m_iEndYear= oCal.get(Calendar.YEAR);
				
			}
			else
			{
				oCal.set(oCal.get(Calendar.YEAR),oCal.get(Calendar.MONTH),1);
				
			}
		}
	}
	
	private void setYearMonthFormula()
	{ 	    
		Date dStartTemp;
		Date dEndTemp;
		
		if(m_sFormula.indexOf("M") != -1 && m_sFormula.indexOf("Y") != -1 && 
				m_sFormula.indexOf("Y") < m_sFormula.indexOf("M"))
		{
			if(m_sFormula.indexOf("AVG") != -1)
			{
				
			}
			else
			{ 	        
				setMonthFormula();
				dStartTemp = m_dStartMonth;
				dEndTemp = m_dEndMonth;
				setYearFormula();
				
				Calendar oCalTemp = Calendar.getInstance();
				oCalTemp.setTime(dStartTemp);
				oCalTemp.set(Calendar.YEAR,m_iStartYear);
				m_dStartMonth= oCalTemp.getTime();
				
				oCalTemp.setTime(dEndTemp);
				oCalTemp.set(Calendar.YEAR,m_iEndYear);
				m_dEndMonth= oCalTemp.getTime();
			}
		}
	}
}