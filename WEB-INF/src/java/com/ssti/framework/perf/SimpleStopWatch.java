package com.ssti.framework.perf;

import java.util.Hashtable;

import org.apache.commons.logging.Log;

import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Measured code execution time
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.1  2007/10/30 04:05:38  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/07/06 02:50:12  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/02/23 14:13:08  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class SimpleStopWatch 
{	
	static long start = 0;
	static Hashtable m = null;
	static Hashtable res = new Hashtable();

	public static void start (String s)
	{
		if (m == null) m = new Hashtable();
		start = System.currentTimeMillis();	
		
		if (StringUtil.isNotEmpty(s))
		{
			m.put(s, Long.valueOf(start));
		}
	}

	public static long stop (String s)
	{
		return stop (s, null);
	}
	
	public static long stop (String s, Log log)
	{
		long elapsed = System.currentTimeMillis() - start;	
		if (m != null && StringUtil.isNotEmpty(s)) 
		{
			Long end = (Long) m.get(s);
			if (end != null)
			{
				elapsed = System.currentTimeMillis() - end.longValue();
			}
		}
		res.put(s, s + " elapsed time : " + elapsed + " ms");	
		
		if (log != null)
		{
			if (log.isInfoEnabled())log.info(res.get(s));
			if (log.isDebugEnabled())log.debug(res.get(s));			
		}
		else
		{
			System.out.println(res.get(s));
		}		
		return elapsed;
	}

	public static void refresh ()
	{
		m = new Hashtable();
		res = new Hashtable();
	}
}