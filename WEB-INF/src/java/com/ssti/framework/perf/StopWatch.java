package com.ssti.framework.perf;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Measured code execution time
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.6  2007/11/12 00:49:28  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/10/30 04:05:38  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/07/06 02:50:12  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/02/23 14:13:08  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class StopWatch 
{
	Log logger = LogFactory.getLog(StopWatch.class);
	
	boolean toLogger = true;
	long start = 0;
	Map m = null;
	Map res = new HashMap();
	
	public boolean isToLogger() 
	{
		return toLogger;
	}

	public void setToLogger(boolean toLogger) 
	{
		this.toLogger = toLogger;
	}

	public void setLogger(Log log) 
	{
		this.logger = log;
	}
	
	public void start (String s)
	{
		if (m == null) m = new HashMap();
		start = System.currentTimeMillis();	
		
		if (StringUtil.isNotEmpty(s))
		{
			m.put(s, Long.valueOf(start));
		}
	}

	public long stop (String s)
	{
		long elapsed = System.currentTimeMillis() - start;	
		if (m != null && StringUtil.isNotEmpty(s)) 
		{
			Long end = (Long) m.get(s);
			if (end != null)
			{
				elapsed = System.currentTimeMillis() - end.longValue();
			}
		}
		res.put(s, s + " elapsed time : " + elapsed + " ms");
		if (toLogger && logger != null) 
		{
			if (logger.isDebugEnabled()) logger.debug (res.get(s));
			else if (logger.isInfoEnabled()) logger.info (res.get(s));			
		}
		else 
		{
			System.out.println (res.get(s));
		}
		return elapsed;
	}
	
	public String result (String s)
	{
		return (String)res.get(s);
	}
}