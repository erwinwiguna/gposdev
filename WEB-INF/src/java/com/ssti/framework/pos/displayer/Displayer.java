package com.ssti.framework.pos.displayer;

import com.ssti.framework.print.helper.PrintAttributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Displayer Interface
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: Displayer.java,v 1.3 2007/02/23 14:13:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: Displayer.java,v $
 * Revision 1.3  2007/02/23 14:13:25  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public interface Displayer extends PrintAttributes
{
    /**
     * clear displayer screen
     *
     */
	public void clearScreen ();

	/**
	 * display string in certain displayer line
	 * 
	 * @param _iLine
	 * @param _sData
	 */
    public void display (int _iLine, String _sData);    

    /**
     * display 2 line
     * @param _sFirstLine
     * @param _sSecondLine
     */
    public void display (String _sFirstLine, String _sSecondLine);
    
    /**
     * reset displayer screen
     *
     */
    public void resetScreen ();  
    
    /**
     * create welcome string
     * @param _sFirstLine
     * @param _sSecondLine
     */
    public void welcome (String _sFirstLine, String _sSecondLine);


    /**
     * @return configuration 
     */
    public String displayConfig();
    
}
