package com.ssti.framework.pos.displayer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.tools.DeviceConfigTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Displayer Tool used to display text into displayer
 * @see Displayer
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DisplayerTool.java,v 1.6 2007/02/23 14:13:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: DisplayerTool.java,v $
 * Revision 1.6  2007/02/23 14:13:25  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class DisplayerTool implements Displayer
{   
	private static Log log = LogFactory.getLog ( DisplayerTool.class );
    
//    private static final byte[] b_CLEAR = {0x0C};
//    private static final byte[] b_SAVE = {0x1B, 0x53, 0x31};
//
//    private static final byte[] b_FL  = {0x1B, 0x51, 0x41};
//    private static final byte[] b_SL  = {0x1B, 0x51, 0x42};
//    private static final byte[] b_END = {0x0D};
        
//	byte[] b_CLEAR = { 12 };
//	byte[] b_SAVE  = { 27, 83, 49 };
//	byte[] b_FL    = { 27, 81, 65 };
//	byte[] b_SL    = { 27, 81, 66 };
//	byte[] b_END   = { 13 };    
    
    private static final String s_DISP_USE = "displayer.use";
    private static final String s_DISP_PORT = "displayer.port";
    //private static final String s_DISP_DEF_TXT = "displayer.default.text";
    //private static final String s_DISP_DEF_SCROLL = "displayer.default.text.scroll";

    private static final String s_DISP_CLEAR  = "displayer.clear.cmd";
    private static final String s_DISP_FLPREF = "displayer.firstline.pref";
    private static final String s_DISP_SLPREF = "displayer.secondline.pref"; 
    private static final String s_DEL_LINE = "displayer.delline";        
    
    private static final String s_DISP_TRANS_LINE1 = "displayer.trans.line1.text";
    private static final String s_DISP_TRANS_LINE2 = "displayer.trans.line2.text";
    
    private static String m_sClearCMD = "12";
    private static String m_sFLPref = "";
    private static String m_sSLPref = "";
    private static String m_sDelLine = "";    
    
    private static String m_sDefaultLine1 = "";
    private static String m_sDefaultLine2 = "";

    private static boolean m_bUse = true;
    private static String m_sPort = "\\COM2";
    private static FileOutputStream m_oOut = null;
    
    private static DisplayerTool m_INSTANCE;

    //prevent new DisplayerTool
    private DisplayerTool() {}    

    //-------------------------------------------------------------------------
    // POSClient Displayer instance initialzation Method by screen, 
    // can return instance of DisplayerTool for Local and RemoteDisplayerTool 
    // for remote Displayer based on device config
    //-------------------------------------------------------------------------

    /**
     * local displayer getInstance via DefaultSalesTransaction Screen Class
     * 
     * @param _oConfig
     * @return
     */
    public static synchronized Displayer getInstance (String _sClientIP)
    {
    	//if Configuration set to remote then return RemoteDisplayerTool instance to send 
    	//HTTP POST request to POSClient
    	if (DeviceConfigTool.isDisplayerRemote() && !StringUtil.isEqual(_sClientIP, s_LOCALHOST))
    	{
    		log.debug("Remote Displayer");
    		return new RemoteDisplayerTool(_sClientIP);
    	}
    	else //return DisplayerTool for local / attached displayer
    	{
	    	if (m_INSTANCE == null) 
	    	{
	    		m_INSTANCE = new DisplayerTool ();
	    		m_INSTANCE.init();
	    	}
	    	return m_INSTANCE;
    	}
    }    
    
    /**
     * local displayer initialization called by local getInstance from Screen
     *
     */
    private void init()
    {
		m_bUse  = DeviceConfigTool.displayerUse();
        if (m_bUse) 
        {
            m_sPort = DeviceConfigTool.getDisplayerPort(); 
            m_sDefaultLine1 = DeviceConfigTool.getDisplayerLine1(); 
            m_sDefaultLine2 = DeviceConfigTool.getDisplayerLine2(); 
            
            m_sClearCMD = DeviceConfigTool.getDisplayerClearCmd();
            m_sFLPref = DeviceConfigTool.getDisplayerFlPref();
            m_sSLPref = DeviceConfigTool.getDisplayerSlPref();            
        }
    }
    
    //-------------------------------------------------------------------------
    // POSClient Displayer instance initialzation Method by RemotePrintServlet
    //-------------------------------------------------------------------------
    
    /**
     * local displayer getInstance via POSClient RemotePrintServlet 
     * 
     * @param _oConfig
     * @return
     */
    public static synchronized Displayer getInstance (Properties _oConfig)
    {
    	if (m_INSTANCE == null) 
    	{
    		m_INSTANCE = new DisplayerTool ();
    		m_INSTANCE.init(_oConfig);
    	}
    	return m_INSTANCE;
    }

    /**
     * local displayer initialization via POSClient RemotePrintServlet
     * 
     * @param _oConfig
     */
    private void init(Properties _oConfig)
    {
        m_bUse  = Boolean.parseBoolean(_oConfig.getProperty(s_DISP_USE));
        if (m_bUse) 
        {
            m_sPort = _oConfig.getProperty(s_DISP_PORT); 
            m_sDefaultLine1 = _oConfig.getProperty(s_DISP_TRANS_LINE1); 
            m_sDefaultLine2 = _oConfig.getProperty(s_DISP_TRANS_LINE2);
                       
            m_sClearCMD = _oConfig.getProperty(s_DISP_CLEAR);
            m_sFLPref   = _oConfig.getProperty(s_DISP_FLPREF);
            m_sSLPref   = _oConfig.getProperty(s_DISP_SLPREF);
            m_sDelLine  = _oConfig.getProperty(s_DEL_LINE);
        }
    }
    //-------------------------------------------------------------------------
    // END POSClient Method
    //-------------------------------------------------------------------------
    
    //utils
    public void clearScreen () 
    {
        try 
        {
            if (m_bUse) 
            {
            	System.out.println("Displayer Tool: Clear Screen: Command:" + m_sClearCMD);
            	
                m_oOut = new FileOutputStream (m_sPort);
                m_oOut.write (StringUtil.toByteArray(m_sClearCMD));
                m_oOut.flush();
                m_oOut.close();
            }
        }
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }        
    }
    
    /**
     * display per line
     * @param _iLine lineNo
     * @param _sData Data  to display
     */
    public void display (int _iLine, String _sData) 
    {
        try 
        {
            if (m_bUse) 
            {        
            	System.out.println("DisplayerTool.display, Line:" + _iLine + " Data: " + _sData);
            	
                m_oOut = new FileOutputStream (m_sPort);
                if (_iLine == 1) 
                {
                	writePref(m_sFLPref);
                	writePref(m_sDelLine);
                }
                else 
                {                	
                	writePref(m_sSLPref);
                	writePref(m_sDelLine);
                	if(StringUtil.isEmpty(m_sSLPref)) m_oOut.write(s_LINE_SEPARATOR.getBytes());
                }
                m_oOut.write (_sData.getBytes());        
                m_oOut.flush();
                m_oOut.close();
            }
        }
        catch (FileNotFoundException _oFileEx)
		{
        	log.warn("Displayer not found or not attached properly at " + m_sPort);
		}
        catch (IOException _oIOEx)
		{
        	log.error("I/O Error : " + _oIOEx.getMessage() + m_sPort);
		}
        catch (Exception _oEx) 
        {
            log.error(_oEx);
        }        
    }    
    
    /**
     * display 2 lines 
     */
    public void display (String _sFirstLine, String _sSecondLine) 
    {
        if (m_bUse) 
        {
        	System.out.println("Displayer Tool.display (method2), Line1:" + _sFirstLine + " Line2:" + _sSecondLine);

        	display(1, StringUtil.left(_sFirstLine, 20));
        	display(2, StringUtil.left(_sSecondLine, 20));
        }
    }    
    
    /**
     * reset Screen
     */
    public void resetScreen () 
    {
        if (m_bUse) 
        {
        	System.out.println("DisplayerTool.resetScreen, DefLine1:" + m_sDefaultLine1 + " DefLine2:" + m_sDefaultLine2);
        	
        	display(1, StringUtil.center(m_sDefaultLine1, 20));
        	display(2, StringUtil.center(m_sDefaultLine2, 20));
        }
    }  

    public void welcome (String _sFirstLine, String _sSecondLine) 
    {
        if (m_bUse) 
        {
        	System.out.println("DisplayerTool.welcome, Line1:" + _sFirstLine + " Line2:" + _sSecondLine);
        	
        	display(1, StringUtil.center(_sFirstLine, 20));
        	display(2, StringUtil.center(_sSecondLine, 20));
        }
    }    
    
    private void writePref(String _sPref)
    	 throws Exception
    {
    	if(StringUtil.isNotEmpty(_sPref))
    	{
    		m_oOut.write(StringUtil.toByteArray(_sPref));
    	}
    }
    
	public String displayConfig() 
	{	    
		StringBuilder oConf = new StringBuilder();
		oConf.append(s_DISP_USE)		.append(" : ").append(m_bUse).append (s_LINE_SEPARATOR)
			 .append(s_DISP_PORT)		.append(" : ").append(m_sPort).append(s_LINE_SEPARATOR)
			 .append(s_DISP_TRANS_LINE1).append(" : ").append(m_sDefaultLine1).append(s_LINE_SEPARATOR)
		 	 .append(s_DISP_TRANS_LINE1).append(" : ").append(m_sDefaultLine2).append(s_LINE_SEPARATOR)
		 	 .append("CLEAR ")			.append(" : ").append(m_sClearCMD).append(s_LINE_SEPARATOR)
		 	 .append("SAVE ")			.append(" : ").append(m_sClearCMD).append(s_LINE_SEPARATOR)
		 	 .append("1ST LINE PREF ")	.append(" : ").append(m_sFLPref).append(s_LINE_SEPARATOR)
		 	 .append("2ND LINE PREF ")	.append(" : ").append(m_sSLPref).append(s_LINE_SEPARATOR)
		 	 .append("DEL LINE ")		.append(" : ").append(m_sDelLine).append(s_LINE_SEPARATOR)

		 	 //.append("LINE END ")		.append(" : ").append("").append(s_LINE_SEPARATOR)
		 	 ;
		return oConf.toString();
	}	
}