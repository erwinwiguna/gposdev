package com.ssti.framework.pos.displayer;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.networking.HttpClientPoster;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Remote Displayer Tool used to display text into remote displayer 
 * by sending displayer HTTP POST Request from local application to POSClient
 * 
 * @see Displayer
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: RemoteDisplayerTool.java,v 1.4 2007/02/23 14:13:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: RemoteDisplayerTool.java,v $
 * Revision 1.4  2007/02/23 14:13:25  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */

public class RemoteDisplayerTool implements Displayer
{   
	private static Log log = LogFactory.getLog (RemoteDisplayerTool.class);

    private String m_sClientIP = "";
    private String m_sURL = "";
    
    private HttpClient oHTTPClient;
    private PostMethod oPost;
    
    public RemoteDisplayerTool (String _sClientIP)
    {
    	m_sClientIP = _sClientIP;
    	m_sURL = s_CLIENT_HTTP + m_sClientIP + s_CLIENT_PORT;
    	
    	log.debug("Remote Displayer: " + displayConfig());
    }
    
    //utils
    public void clearScreen () 
    {
        try 
		{
        	//cleanParam();
        	oPost =  new PostMethod (m_sURL);
        	oPost.addParameter(s_DEST, Integer.valueOf(i_DISPLAYER).toString());    	
        	oPost.addParameter(s_CMD, Integer.valueOf(i_CLEAR).toString());
        	
        	execute();
        	
        	log.debug("Remote Displayer [Clear]");
        }
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }        
    }

    public void display (int _iLine, String _sData) 
    {
        try 
        {
        	//cleanParam();
        	oPost =  new PostMethod (m_sURL);
        	oPost.addParameter(s_DEST, Integer.valueOf(i_DISPLAYER).toString());    
        	oPost.addParameter(s_CMD, Integer.valueOf(i_DISPLAY).toString());
        	oPost.addParameter("line", Integer.valueOf(_iLine).toString());
        	oPost.addParameter(s_DOCS, _sData);

        	log.debug("Remote Displayer [Display] " + " Line" + _iLine + ": "+ _sData + " Q: " + oPost.getQueryString());

        	execute();                	
        }
        catch (Exception _oEx) 
        {
            log.error(_oEx);
        }        
    }    

    public void display (String _sLine1, String _sLine2) 
    {
        try 
        {
        	//cleanParam();
        	oPost =  new PostMethod (m_sURL);
        	oPost.addParameter(s_DEST, Integer.valueOf(i_DISPLAYER).toString());    
        	oPost.addParameter(s_CMD, Integer.valueOf(i_DISPLAY).toString());
        	oPost.addParameter("line1", Integer.valueOf(_sLine1).toString());
        	oPost.addParameter("line2", Integer.valueOf(_sLine2).toString());

        	log.debug("Remote Displayer [Display] " + " Line1:" + _sLine1 + " Line2:"+ _sLine2);
        	execute();                	
        }
        catch (Exception _oEx) 
        {
            log.error(_oEx);
        }        
    }    

    
    public void resetScreen () 
    {
        try 
        {   
        	//cleanParam();
        	oPost =  new PostMethod (m_sURL);
        	oPost.addParameter(s_DEST, Integer.valueOf(i_DISPLAYER).toString());    
        	oPost.addParameter(s_CMD, Integer.valueOf(i_RESET).toString());

        	execute();
        	
        	log.debug("Remote Displayer [Reset]");
	    }
	    catch (Exception _oEx) 
	    {
	        log.error(_oEx);
	    }  
    }  

    public void welcome (String _sFirstLine, String _sSecondLine) 
    {
        try 
        {   
        	//cleanParam();
        	oPost =  new PostMethod (m_sURL);
        	oPost.addParameter(s_DEST, Integer.valueOf(i_DISPLAYER).toString());    
        	oPost.addParameter(s_CMD, Integer.valueOf(i_WELCOME).toString());
        	oPost.addParameter("line1", _sFirstLine);
        	oPost.addParameter("line2", _sSecondLine);

        	execute();           	

        	log.debug("Remote Displayer [Welcome]");
        }
	    catch (Exception _oEx) 
	    {
	        log.error(_oEx);
	    }  
    }    
    
    private void cleanParam()
    {
    	if (oPost != null)
    	{
	    	oPost.removeParameter(s_CMD);
	    	oPost.removeParameter("line1");
	    	oPost.removeParameter("line2");
	    	oPost.removeParameter("line");    	
	    	oPost.removeParameter(s_DOCS);
    	}
    }
    
	public String displayConfig() 
	{	    
		StringBuilder oConf = new StringBuilder();
		oConf.append("CLIENT IP ").append(" : ").append(m_sClientIP).append (s_LINE_SEPARATOR)
			 .append("POSCLIENT URL ").append(" : ").append(m_sURL).append(s_LINE_SEPARATOR)
		 	 ;
		return oConf.toString();
	}	
	
	public void execute()
		throws Exception
	{
//    	oHTTPClient = new HttpClient();
//    	oHTTPClient.executeMethod(oPost);                	
		
    	HttpClientPoster poster = new HttpClientPoster(oPost);
    	Thread posterThread = new Thread(poster);
    	posterThread.start();
	}
}