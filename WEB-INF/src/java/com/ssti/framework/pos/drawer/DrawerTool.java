package com.ssti.framework.pos.drawer;

import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.networking.HttpClientPoster;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.servlet.RemotePrintServlet;
import com.ssti.framework.tools.DeviceConfigTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Cash Drawer related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DrawerTool.java,v 1.6 2007/02/23 14:13:32 albert Exp $ <br>
 *
 * <pre>
 * $Log: DrawerTool.java,v $
 * Revision 1.6  2007/02/23 14:13:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class DrawerTool implements PrintAttributes
{   
	private static Log log = LogFactory.getLog ( DrawerTool.class );
    
	private boolean m_bUse = true;
    private boolean m_bRemote = false;
    private String m_sClientURL = "";
    
    private static byte[] b_OPEN  = {27,112,0,25,(byte)250};
    
    private static final String s_DRAWER_USE     = "drawer.use"             ;     
    private static final String s_DRAWER_PORT    = "drawer.port"            ;
    private static final String s_DRAWER_TYPE    = "drawer.type"            ;
    private static final String s_DRAWER_OPENCMD = "drawer.open.command"    ;

    private static String m_sPort = "\\COM1";
    private static String[] m_sOpen = {"27","112","0","25","250"};
    private static String m_sType = "1";
    
    private static FileOutputStream m_oOut = null;

    /**
     * Constructor from ScreenTransaction
     * 
     * @param _oConfig
     * @param _sClientIP
     */
    public DrawerTool (String _sClientIP)
    {
    	//if Configuration set to remote then return RemoteDisplayerTool instance to send 
    	//HTTP POST request to POSClient
		if (DeviceConfigTool.isPrinterRemote() && !StringUtil.isEqual(_sClientIP, s_LOCALHOST))
		{
			m_bRemote = true;
			m_sClientURL = s_CLIENT_HTTP + _sClientIP + s_CLIENT_PORT;
			log.info ("Remote Drawer URL : " + m_sClientURL);			
		}
		else //for local / attached drawer
		{
	        m_bUse  = DeviceConfigTool.drawerUse();
	        if (m_bUse) 
	        {
	            m_sPort = DeviceConfigTool.drawerPort(); 
	            m_sType = DeviceConfigTool.drawerType();
	            if (StringUtil.isNotEmpty(DeviceConfigTool.drawerOpenCommand()))
	            {
	            	b_OPEN = StringUtil.toByteArray (DeviceConfigTool.drawerOpenCommand());
	            }            
	        }
		}
    }

    //-------------------------------------------------------------------------
    // POSClient Drawer instance Method by RemotePrintServlet
    //-------------------------------------------------------------------------

    /**
     * Constructor from RemotePrintServlet
     * 
     * @param _oConfig
     * @param _sClientIP
     */
    public DrawerTool (Properties _oConfig)
    {
        m_bUse  = Boolean.parseBoolean(_oConfig.getProperty(s_DRAWER_USE));
        if (m_bUse) 
        {
            m_sPort = _oConfig.getProperty(s_DRAWER_PORT); 
            m_sType = _oConfig.getProperty(s_DRAWER_TYPE); 
            if (StringUtil.isNotEmpty(_oConfig.getProperty(s_DRAWER_OPENCMD)))
            {
            	String sOpen = _oConfig.getProperty(s_DRAWER_OPENCMD);
            	b_OPEN = StringUtil.toByteArray (sOpen);
            }
        }
    }
    //-------------------------------------------------------------------------
    // END POSClient Method
    //-------------------------------------------------------------------------
        
    //utils
    public void openDrawer () 
    {
        try 
        {	
        	if (m_bRemote)
        	{
        		PostMethod oPost =  new PostMethod (m_sClientURL);
        		oPost.addParameter(RemotePrintServlet.s_DEST, 
        				Integer.valueOf(RemotePrintServlet.i_DRAWER).toString());
        		
        		HttpClientPoster poster = new HttpClientPoster(oPost);
        		Thread posterThread = new Thread(poster);
        		posterThread.start();
        	}
        	else
        	{
        		if (m_bUse) 
        		{
        			System.out.println("open drawer, command : " + StringUtil.byteToStr(b_OPEN));
        			
        			m_oOut = new FileOutputStream (m_sPort);
        			m_oOut.write (b_OPEN);
        			m_oOut.flush();
        			m_oOut.close();
        		}
        	}
        }
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }        
    }
    
	public boolean useDrawer() 
	{
		return m_bUse;
	}
	
	public String displayConfig() 
	{
		StringBuilder oConf = new StringBuilder();
		oConf.append(s_DRAWER_USE).append(" : ").append(m_bUse).append (s_LINE_SEPARATOR)
			 .append(s_DRAWER_PORT).append(" : ").append(m_sPort).append(s_LINE_SEPARATOR)
			 .append(s_DRAWER_TYPE).append(" : ").append(m_sType).append(s_LINE_SEPARATOR)
			 .append(s_DRAWER_OPENCMD).append(" : ").append(m_sOpen).append(s_LINE_SEPARATOR)
		 	 .append("OPEN CMD BYTE").append(" : ").append(StringUtil.byteToStr(b_OPEN)).append(s_LINE_SEPARATOR)
		 	 ;
		return oConf.toString();
	}	
}