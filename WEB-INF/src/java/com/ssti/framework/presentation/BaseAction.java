package com.ssti.framework.presentation;

import org.apache.turbine.modules.Action;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2017-05-27
 * - Change validateLicense method, only skip validation if 
 *   companyName is empty and no location data
 * - add locations variable to capture list of locations
 *       
 * </pre><br>
 */
public abstract class BaseAction extends Action
{	
	
}
