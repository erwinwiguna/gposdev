package com.ssti.framework.presentation;

import org.apache.turbine.modules.actions.VelocitySecureAction;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.turbine.util.security.PermissionSet;
import org.apache.velocity.context.Context;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base Secure action
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SecureAction.java,v 1.10 2007/07/10 04:52:08 albert Exp $ <br>
 *
 * <pre>
 * $Log: SecureAction.java,v $
 * Revision 1.10  2007/07/10 04:52:08  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/02/23 14:13:38  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class SecureAction extends VelocitySecureAction implements SecureAttributes
{	
    public void doPerform( RunData data,Context context )
        throws Exception
    {
    }

	/**
     * This will provide basic authentication for any 
     * Secure Screen. Must be overridden in each class
     * that extends this
     * @param data
     * @return isAutohrized
     */
    protected boolean isAuthorized (RunData data)  throws Exception 
    {
        AccessControlList acl = data.getACL();
        if (acl == null) 
        {
            data.setScreenTemplate(s_LOGIN_TPL);
            return false;
        }
        return true;
    }
    
    /**
     * validate permission 
     * 
     * @param data
     * @param _aPerms array of permission to validate
     * @return isAutohrized
     * @throws Exception
     */
    protected boolean isAuthorized(RunData data, String[] _aPerms)
		throws Exception
	{
		AccessControlList acl = data.getACL();
	    if(acl == null) 
	    {
	        data.setScreenTemplate(s_LOGIN_TPL);
	        return false;
	    }
	    else
	    {
			PermissionSet oPS = acl.getPermissions();
	    	if (oPS != null)
	    	{
			    for (int i = 0; i < _aPerms.length; i++)
			    {
				    if(!oPS.containsName(_aPerms[i])) 
				    {
				        data.setScreenTemplate(s_NO_PERM_TPL);
				    	return false;
				    }
			    }
			    return true;
	    	}
	    	return false;
	    }
	}    
}
