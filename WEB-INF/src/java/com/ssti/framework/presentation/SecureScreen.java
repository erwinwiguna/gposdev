package com.ssti.framework.presentation;

import org.apache.turbine.modules.screens.VelocitySecureScreen;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.turbine.util.security.PermissionSet;
import org.apache.velocity.context.Context;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base class for all secure screen
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SecureScreen.java,v 1.12 2007/07/21 13:50:32 albert Exp $ <br>
 *
 * <pre>
 * $Log: SecureScreen.java,v $
 * Revision 1.12  2007/07/21 13:50:32  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/07/10 04:52:08  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/02/23 14:13:38  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class SecureScreen extends VelocitySecureScreen implements SecureAttributes
{	
	protected boolean b_REFRESH = false;
	/**
	 * check whether this submit is a refresh page
	 * 
	 * @param data
	 * @param context
	 * @return whether current request is user refresh 
	 */
	protected boolean isRefresh(RunData data, Context context)
	{
		int iCurrent = data.getParameters().getInt("_session_access_counter");
		Integer iTemp = (Integer) data.getUser().getTemp("_session_access_counter");
		if (iTemp != null)
		{
			//log.debug(data.getScreenTemplate() + " SAS: " + iCurrent + " TMP: " + iTemp );
			int iPrev = iTemp.intValue() - 1; 
			if ( iCurrent > 0 && iCurrent < iPrev)
			{
	    		log.info(data.getScreenTemplate() + " page refresh called current : " + iCurrent + " prev : " + iPrev);
				b_REFRESH = true;
			}
			else
			{
				b_REFRESH = false;
			}
			if (context != null) 
			{
				context.put(s_REFRESH, Boolean.valueOf(b_REFRESH));
			}
		}
		else
		{
			log.warn("page session already ended " );			
		}
		return b_REFRESH;
	}
	
    public void doBuildTemplate (RunData data, Context context)
    {
    }
	
	/**
     * This will provide basic authentication for any 
     * Secure Screen. Must be overridden in each class
     * that extends this
     * @param data
     * @return is user authorized
     */
    protected boolean isAuthorized (RunData data)  
    	throws Exception 
    {
        AccessControlList acl = data.getACL();
        if (acl == null) 
        {
            data.setScreenTemplate(s_LOGIN_TPL);
            return false;
        }
        return true;
    }

    /**
     * validate permission 
     * 
     * @param data
     * @param _aPerms array of permission to validate
     * @return is user authorized 
     * @throws Exception
     */
    protected boolean isAuthorized(RunData data, String[] _aPerms)
		throws Exception
	{
		AccessControlList acl = data.getACL();
	    if(acl == null) 
	    {
	        data.setScreenTemplate(s_LOGIN_TPL);
	        return false;
	    }
	    else
	    {
			PermissionSet oPS = acl.getPermissions();
	    	if (oPS != null)
	    	{
			    for (int i = 0; i < _aPerms.length; i++)
			    {
				    if(!oPS.containsName(_aPerms[i])) 
				    {
				        log.debug("No Permission " + _aPerms[i]);
				    	data.setScreenTemplate(s_NO_PERM_TPL);
				    	return false;
				    }
			    }
			    return true;
	    	}
	    	return false;
	    }
	}
    
    protected boolean hasPermission(RunData data, String _sPerm)
	{
		AccessControlList acl = data.getACL();
	    if(acl == null) 
	    {
	        data.setScreenTemplate(s_LOGIN_TPL);
	        return false;
	    }
	    else
	    {
			PermissionSet oPS = acl.getPermissions();
	    	if (oPS != null)
	    	{
	    		if(!oPS.containsName(_sPerm)) 
	    		{
	    			return false;
	    		}
	    		else
	    		{
	    			return true;
	    		}
	    	}
	    	return false;
	    }
	}    
}
