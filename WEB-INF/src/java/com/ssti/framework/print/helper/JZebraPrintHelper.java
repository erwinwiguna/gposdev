package com.ssti.framework.print.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GenericPrintHelper.java,v 1.2 2008/03/04 02:16:35 albert Exp $ <br>
 *
 * <pre>
 * $Log: GenericPrintHelper.java,v $
 * Revision 1.2  2008/03/04 02:16:35  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class JZebraPrintHelper 
	extends BasePrintHelper 
{   
	static final Log log = LogFactory.getLog(JZebraPrintHelper.class);
	
	String s_JZ_TPL_NAME = "/print/JZebra.vm";
	
	public String getJZebraPage(String _sDocument, Map oParam)	
		throws Exception
	{   
		if (oParam != null)
		{
			String sJZebraTPL = getString(oParam, "jzebra_tpl");
			if (StringUtil.isNotEmpty(sJZebraTPL))
			{
				s_JZ_TPL_NAME = sJZebraTPL;    	
			}
		}
		oCtx.put("document", _sDocument);
		Velocity.mergeTemplate(s_JZ_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter);    	
		return oWriter.toString();
	}
	
	public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {   
		return getJZebraPage("",oParam);
    }
   		 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
    	return getJZebraPage("",oParam);
    }
}
