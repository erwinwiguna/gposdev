package com.ssti.framework.print.helper;


/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PrintAttributes.java,v 1.16 2008/06/29 07:11:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: PrintAttributes.java,v $
 * Revision 1.16  2008/06/29 07:11:18  albert
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/26 05:15:51  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/12/20 13:45:04  albert
 * *** empty log message ***
 *
 * Revision 1.13  2007/02/23 14:13:46  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public interface PrintAttributes 
{
    public static final int i_DOC_TYPE_TEXT  = 1;
    public static final int i_DOC_TYPE_PDF   = 2;
    public static final int i_DOC_TYPE_HTML  = 3;
    public static final int i_DOC_TYPE_JZEBRA = 4;    
    
    //ESC/P Configuration
    public static final char ESC = 27; //escape                                                                                                                                                                
    public static final char AT = 64; //@                                                                                                                                                                      
    public static final char LINE_FEED = 10; //line feed/new line                                                                                                                                              
    public static final char PARENTHESIS_LEFT = 40;                                                                                                                                                            
    public static final char BACKSLASH = 92;                                                                                                                                                                   
    public static final char CR = 13; //carriage return                                                                                                                                                        
    public static final char TAB = 9; //horizontal tab                                                                                                                                                         
    public static final char FF = 12; //form feed                                                                                                                                                              
    public static final char g = 103; //15cpi pitch                                                                                                                                                            
    public static final char p = 112; //used for choosing proportional mode or fixed-pitch                                                                                                                     
    public static final char t = 116; //used for character set assignment/selection                                                                                                                            
    public static final char l = 108; //used for setting left margin                                                                                                                                           
    public static final char x = 120; //used for setting draft or letter quality (LQ) printing                                                                                                                 
    public static final char E = 69; //bold font on                                                                                                                                                            
    public static final char F = 70; //bold font off                                                                                                                                                           
    public static final char J = 74; //used for advancing paper vertically                                                                                                                                     
    public static final char P = 80; //10cpi pitch                                                                                                                                                             
    public static final char Q = 81; //used for setting right margin                                                                                                                                           
    public static final char $ = 36; //used for absolute horizontal positioning      
    
    public static final byte[] b_PRINTER_FONT_MODE_CHAR = {15};   //condensed
    public static final byte[] b_PRINTER_ESC_P_CHAR  	= {ESC, AT};
    public static final byte[] b_PRINTER_PAGE_MODE_CHAR = {ESC, 67};    
    public static final byte[] b_PRINTER_FORM_FEED_CHAR = {CR, FF};
    public static final byte[] b_PRINTER_BOLD_ON = {ESC, E};
    public static final byte[] b_PRINTER_BOLD_OFF = {ESC, F};
    public static final byte[] b_PRINTER_ITALIC_ON = {ESC, 52};
    public static final byte[] b_PRINTER_ITALIC_OFF = {ESC, 53};
    	
    public static final String s_PRINTER_PORT  = "\\LPT1";    
    public static final String s_LOCALHOST 	 = "127.0.0.1";
    public static final String s_LOCALV6 	 = "0:0:0:0:0:0:0:1%0";
    
        
    //used for remote printing and displayer
    public static final String s_CLIENT_HTTP = "http://";
    public static final String s_CLIENT_PORT = ":8080";

    //remote call destination
    public static final int i_PRINTER   = 1;
    public static final int i_DISPLAYER = 2;
    public static final int i_DRAWER 	= 3;    
    
    //remote displayer command code
    public static final int i_DISPLAY = 1;
    public static final int i_RESET = 2;
    public static final int i_CLEAR = 3;
    public static final int i_WELCOME = 4;
    
    //remoteprintservlet param
    public static final String s_DOCS  = "docs";
    public static final String s_DEST  = "dest";
    public static final String s_CMD   = "cmd";

    public static final String s_DETAIL_PER_PAGE = "detail_per_page";
	
    //standard attributes
    public static final String s_LINE_SEPARATOR = System.getProperty("line.separator");
	public static final String s_ASCII_CHARSET = "US-ASCII";
	public static final String s_DEFAULT_ENCODING = "ISO-8859-1";

}
