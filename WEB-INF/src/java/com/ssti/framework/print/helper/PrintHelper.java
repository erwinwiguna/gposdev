package com.ssti.framework.print.helper;

import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * print helper interface
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PrintHelper.java,v 1.5 2007/09/17 07:37:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: PrintHelper.java,v $
 * Revision 1.5  2007/09/17 07:37:15  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/02/23 14:13:46  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public interface PrintHelper
{    
	public void init (HttpServletRequest request,HttpServletResponse response, ServletConfig config);
	
	/**
	 * parse print templates then return the string for printing
	 * 
	 * @param _mParams
	 * @param _iPrintType
	 * @return prepared text
	 * @throws Exception
	 */
	public String getPrintedText (Map _mParams, int _iPrintType, HttpSession _oSession) throws Exception;	
	
	/**
	 * parse print templates then return the string for printing
	 * 
	 * @param _mParams
	 * @param _iPrintType
	 * @param _iDetailPerPage
	 * @param _oSession
	 * @return prepared text 
	 * @throws Exception
	 */
	public String getPrintedText (Map _mParams, int _iPrintType, int _iDetailPerPage, HttpSession _oSession) throws Exception;		
	
	public String appendInitEject(String _sDocument);
	
	public String cleanInitEject(String _sDocument);
	
}