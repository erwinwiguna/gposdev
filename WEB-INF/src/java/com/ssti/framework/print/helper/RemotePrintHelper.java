package com.ssti.framework.print.helper;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.framework.print.servlet.RemotePrintServlet;

/**
* 
* <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
*
* <b>Purpose: </b><br>
* Submit print text content to POSClient 
* <br>
*/
public class RemotePrintHelper implements Runnable
{
	private static final Log log = 
		LogFactory.getLog (RemotePrintHelper.class);

	String sURL;
	String sDocument;

	public RemotePrintHelper(String surl, String document) 
	{
		sURL = surl;
		sDocument = document;
	}

	public void postToRemotePrintServlet()
	{
		PostMethod oPost =  new PostMethod (sURL);
    	oPost.addParameter(RemotePrintServlet.s_DEST, Integer.valueOf(RemotePrintServlet.i_PRINTER).toString());
    	oPost.addParameter(RemotePrintServlet.s_DOCS, sDocument);
    	HttpClient oHTTPClient = new HttpClient();
    	
    	try 
    	{
			int iResult = oHTTPClient.executeMethod(oPost);
			if (iResult != -1)
			{
				oPost.releaseConnection();
				log.info("HTTP Client POST Result : " + iResult);
			}
		} 
    	catch (HttpException e) 
    	{
    		log.error(e);
			e.printStackTrace();
		} 
    	catch (IOException e) 
    	{
    		log.error(e);
    		e.printStackTrace();
		}
	}
	
	public void run() 
	{		
		postToRemotePrintServlet();
	}
}
