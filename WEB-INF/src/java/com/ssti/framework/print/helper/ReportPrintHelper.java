package com.ssti.framework.print.helper;

import java.io.StringWriter;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Multi Page Report Print Helper
 * @see GenericPrintHelper 
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.2  2008/06/29 07:11:18  albert
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/04 02:16:07  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/12/20 13:45:04  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class ReportPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper
{   
	static final Log log = LogFactory.getLog(ReportPrintHelper.class);
	
	int m_iPageWidth = 135;
	int m_iPageLength = 60;
	String m_sTitle = "";
	
	boolean m_bShowHeader = false;
    boolean m_bShowHeading = false;    
	String m_sHeader = "";
    String m_sHeading = "";
    int iHeaderLine = 1;
    int iHeadingLine = 1;
	
	static final String s_REPORT_HEADER = "/print/report/ReportHeaderTXT.vm";
	static final String s_REPORT_LINE = "--REPORT LINE--";
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {   
    	checkTemplate (oParam);
    	
    	if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
    	{		
    		int iLength = StringUtil.getInt(oParam, "page_length");    		
    		if (iLength > 0) m_iPageLength = iLength;
    		
    		int iWidth = StringUtil.getInt(oParam, "page_width");
    		if (iWidth > 0) m_iPageWidth = iLength;

            Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
            StringBuilder sbDoc = new StringBuilder();
            sbDoc.append(oWriter.toString());

    		m_sTitle = getString(oParam, "title");
    		m_bShowHeader = StringUtil.getBoolean(oParam, "use_header");
            m_bShowHeading = StringUtil.getBoolean(oParam, "use_heading");
    		if (m_bShowHeader)
    		{
                StringWriter oHeader = new StringWriter();
                Velocity.mergeTemplate(s_REPORT_HEADER, Attributes.s_DEFAULT_ENCODING, oCtx, oHeader);
    			m_sHeader = oHeader.toString();
                if (StringUtil.isNotEmpty(m_sHeader))
                {
                    iHeaderLine = StringUtil.split(m_sHeader,Attributes.s_WIN_LINE_SEPARATOR).length;
                }
            }
            if (m_bShowHeading)
            {
                StringWriter oHeading = new StringWriter();
                String sHeadingTpl = StringUtil.getString(oParam,"hdg_tpl");
                Velocity.mergeTemplate(sHeadingTpl, Attributes.s_DEFAULT_ENCODING, oCtx, oHeading);
                m_sHeading = oHeading.toString();
                if (StringUtil.isNotEmpty(m_sHeading))
                {
                    iHeadingLine = StringUtil.split(m_sHeading,Attributes.s_WIN_LINE_SEPARATOR).length;
                }
            }    		
        	return parseTextForPaging(sbDoc.toString());
    	}
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF)
        {
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML)
        {
            Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }        
    	return oWriter.toString();
    }
   	
   	/**
	 * get Printed Text (use detail per page)
	 */
	 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
    	return getPrintedText(oParam, _iPrintType, _oSession);
    }
    
    public String parseTextForPaging(String _sDoc)
    {
		if (_sDoc.contains(Attributes.s_UNIX_LINE_SEPARATOR))
		{
			_sDoc = _sDoc.replace(Attributes.s_UNIX_LINE_SEPARATOR, Attributes.s_WIN_LINE_SEPARATOR);
		}		
    	StringTokenizer oToken = new StringTokenizer(_sDoc, Attributes.s_WIN_LINE_SEPARATOR, false);
        StringBuilder sbParsed = new StringBuilder();
    	int i = 1;
    	int iPage = 1;
    	while (oToken.hasMoreTokens())
    	{
    		String sLine = oToken.nextToken();    		
    		if (StringUtil.isNotEmpty(sLine) && sLine.equals(s_REPORT_LINE)) 
    		{
    			sLine = Attributes.s_LINE_SEPARATOR;
    			sbParsed.append(sLine);
    		}
    		else
    		{
    			sbParsed.append(sLine).append(Attributes.s_LINE_SEPARATOR);
    		}
    		i++;
    		if (i > m_iPageLength && m_iPageLength > 0)
    		{
                i = 2;
    			iPage++;
    			sbParsed.append(s_EJECT_STRING);
    			sbParsed.append(s_INIT_STRING);
    			if (m_bShowHeader && StringUtil.isNotEmpty(m_sHeader)) 
    			{
                    i = i + iHeaderLine;
                    sbParsed.append(m_sHeader);
                }
    			sbParsed.append(Attributes.s_LINE_SEPARATOR);
                if (StringUtil.isNotEmpty(m_sTitle))
                {
        			int iLength = m_iPageWidth - (m_sTitle.length() + 1);
        			sbParsed.append(s_BOLD_ON).append(m_sTitle).append(s_BOLD_OFF)
        					.append(StringUtil.right("page " + iPage,iLength))
        					.append(Attributes.s_LINE_SEPARATOR);
                }
                if (m_bShowHeading && StringUtil.isNotEmpty(m_sHeading))
                {
                    i = i + iHeadingLine;
                    sbParsed.append(m_sHeading);             
                }                    
    		}    		
    		log.debug("LINE " + i + " " + sLine);
    	}
    	sbParsed.append(s_EJECT_STRING);
    	return sbParsed.toString();
    }
}
