package com.ssti.framework.print.servlet;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.ConnectException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;

import com.ssti.framework.print.helper.JZebraPrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.print.helper.RemotePrintHelper;
import com.ssti.framework.tools.DeviceConfigTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Print Servlet 
 * 1. Direct TEXT printing to Printer & Display Text
 * 2. Generate PDF 
 * 3. Generate HTML Output
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PrintServlet.java,v 1.25 2009/05/04 01:29:03 albert Exp $ <br>
 *
 * <pre>
 * 2016-12-11
 * - Change method printText now if printer.host setting in cardio_config can have 3 values
 *   local, remote, remote-local if remote-local, printServlet will only return text, to be sent by browser to POSClient
 * - remove instance variables m_sClientIP for thread safety, that possibly result in sending to wrong cleint IP
 * - Must also change DeviceConfigTool
 * </pre><br>
 *
 */
public class PrintServlet extends HttpServlet implements PrintAttributes
{    
	private static final Log log = LogFactory.getLog (PrintServlet.class);
    	    
    private static final String s_HELPER_CLASS 	= "helper_class";
    private static final String s_PRINT_TYPE 	= "print_type";
    private static final String s_PRINTER_PORT 	= "port";
        
    private String m_sPrinterPort = "LPT1";
    
    private Map m_oParam = null;
    private HttpSession m_oSession = null;
    
	protected TransformerFactory oTransFactory;
    protected FopFactory oFopFactory;
    
    public void init()
	    throws ServletException
	{
    	//initialize XML TRANSFORMER & FOP FACTORY
	    oTransFactory = TransformerFactory.newInstance();
	    oFopFactory = FopFactory.newInstance();
	}
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException 
	{
        try 
        {
        	String sClientIP = request.getRemoteAddr(); 
        	
            //init session & config
            m_oSession = request.getSession();
            
            //configure printer port to use
            configurePort (request);

			//get HTTP request parameters as map then parse them
			m_oParam = request.getParameterMap ();                         

			String sClassName = StringUtil.getString(m_oParam, s_HELPER_CLASS);
			int iPrintType 	  = StringUtil.getInt(m_oParam, s_PRINT_TYPE);			
			String sDocument  = null;
			
			//check parameter completeness 					            
            if (StringUtil.isNotEmpty(sClassName)) 
            {    
				//create and initialize print helper
            	PrintHelper oHelper = (PrintHelper) Class.forName(sClassName).newInstance();
				oHelper.init (request, response, getServletConfig());
				
				if ( iPrintType == i_DOC_TYPE_PDF  ) //PDF
				{
					sDocument = oHelper.getPrintedText (m_oParam, iPrintType, m_oSession);
					if (log.isDebugEnabled()) log.debug(sDocument);
					renderFO (sDocument, response);
				}
				else if ( iPrintType == i_DOC_TYPE_TEXT ) //TXT 
				{ 
					int iDetailPerPage = StringUtil.getInt(m_oParam, s_DETAIL_PER_PAGE);
					if (iDetailPerPage > 0) 
					{
						sDocument = oHelper.getPrintedText (m_oParam, iPrintType, iDetailPerPage, m_oSession);
					}
					else 
					{
						sDocument = oHelper.getPrintedText (m_oParam, iPrintType, m_oSession);
						sDocument = oHelper.appendInitEject(sDocument);
					}
					displayText(sDocument, response);
					printText(sDocument,sClientIP);
            	}
				else if ( iPrintType == i_DOC_TYPE_HTML ) //HTML
				{
					sDocument = oHelper.getPrintedText (m_oParam, iPrintType, m_oSession);
					response.getWriter().println(sDocument);					
				}			
				else if ( iPrintType == i_DOC_TYPE_JZEBRA) //JZEBRA HTML
				{
					int iDetailPerPage = StringUtil.getInt(m_oParam, s_DETAIL_PER_PAGE);
					if (iDetailPerPage > 0) 
					{
						sDocument = oHelper.getPrintedText (m_oParam, i_DOC_TYPE_TEXT, iDetailPerPage, m_oSession);
					}
					else 
					{
						sDocument = oHelper.getPrintedText (m_oParam, i_DOC_TYPE_TEXT, m_oSession);
						sDocument = oHelper.appendInitEject(sDocument);
					}
					JZebraPrintHelper oZH = new JZebraPrintHelper();
					oZH.init (request, response, getServletConfig());
					response.getWriter().println(oZH.getJZebraPage(sDocument,m_oParam));
				}
            	else 
            	{
            		displayError (response, "Invalid Document Type or Document Type is not Defined", null);
            	}
            }                                                                                    
            else 
            {
                displayError (response, "Invalid Parameters (HELPER CLASS), check the caller templates / javascript ", null);
            }
        }
        catch (Exception _oEx) 
		{
          	_oEx.printStackTrace();
        	log.error (_oEx);
            displayError (response, _oEx.getMessage(), _oEx);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException 
	{
    	doGet(request,response);
	}
    
    private void displayError (HttpServletResponse response, String errMsg, Exception ex)
    {
    	PrintWriter out;
		try 
		{
			StringBuilder err = new StringBuilder ("<html><head><title>PrintServlet Error</title></head>\n")
				.append ("<body><font face=arial><h2>Print Servlet Error</h2><h3><font color=#990000>")
				.append(errMsg)
				.append("</font></h3><h5>Error Stack Trace : </h5><pre>");
			
			out = response.getWriter();
			out.println(err.toString());
			if (ex != null) 
			{
				ex.printStackTrace(out);
			}
			out.println("</pre></body></html>");
			out.flush();
		} 
		catch (IOException e) 
		{
			log.error (e);
			e.printStackTrace();
		}    
    }
    
    /**
	 * configure port to print
	 * 1. if config != null -> get from configuration
	 * 2. if parameter != null -> get from 'port' parameter passed to servlet
	 * else use default value	 
	 * 
	 * @param request
	 */
	private void configurePort(HttpServletRequest request) 
	{
		//get from device config
    	String sPrinterPort = DeviceConfigTool.getPrinterPort();
    	if (StringUtil.isNotEmpty(sPrinterPort))
    	{
    		m_sPrinterPort = sPrinterPort;
    	}
        
    	//check param get from request
    	sPrinterPort = request.getParameter(s_PRINTER_PORT);
        if (StringUtil.isNotEmpty(sPrinterPort))
        {
        	m_sPrinterPort = sPrinterPort;
        }		
	}

	public void renderFO(String _sDocument, HttpServletResponse response) 
    	throws Exception
    {
        try 
		{
        	Source oSrc = new StreamSource(new StringReader(_sDocument));        	
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            
            FOUserAgent oAgent = oFopFactory.newFOUserAgent();
            Fop oFOP = oFopFactory.newFop("application/pdf", oAgent, out);            
            Result oResult = new SAXResult(oFOP.getDefaultHandler());
            
            Transformer transformer = oTransFactory.newTransformer();
            transformer.transform(oSrc, oResult);
            
            byte[] content = out.toByteArray();
            response.setContentLength(content.length);
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        } 
        catch (Exception _oEx) 
		{
        	log.error(_oEx);
        	throw new NestableException("FO Rendering Failed : " + _oEx.getMessage(), _oEx);
        }
    }

	private void displayText(String _sDocument, HttpServletResponse response)
		throws Exception
	{
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        //out.println(_oHelper.cleanInitEject(_sDocument));		
        out.println(_sDocument);
	}	
    
    /**
     * print document to text printer 
     * if printer.host in POS.properties is remote, then try to print to POS client
     * 
     * @param _sDocument
     * @throws Exception
     */
    private void printText(String _sDocument, String _sClientIP) 
    	throws Exception
    {
    	try 
    	{
			//remote printing through HTTP
			if((DeviceConfigTool.isPrinterRemote() || DeviceConfigTool.isPrinterRemoteLocal()) &&
				!StringUtil.equals(_sClientIP, s_LOCALHOST) && !StringUtil.equals(_sClientIP, s_LOCALV6))
			{
				String sClientURL = s_CLIENT_HTTP + _sClientIP + s_CLIENT_PORT;
				log.info ("Remote Printing To : " + sClientURL);
				
				if(DeviceConfigTool.isPrinterRemote())
				{
					RemotePrintHelper oHelper = new RemotePrintHelper(sClientURL, _sDocument);
					Thread oPrintThread = new Thread(oHelper);
					oPrintThread.start();					
				}
				else
				{
					log.info ("Remote Printing to localhost via browser");
				}
				
				/*
				PostMethod oPost =  new PostMethod (sClientURL);
		    	oPost.addParameter(RemotePrintServlet.s_DEST, Integer.valueOf(RemotePrintServlet.i_PRINTER).toString());
		    	oPost.addParameter(RemotePrintServlet.s_DOCS, _sDocument);
		    	HttpClient oHTTPClient = new HttpClient();
		    	oHTTPClient.executeMethod(oPost);
		    	*/
			}
			//local printing through file
			else
			{
				log.info ("Printing To : " + m_sPrinterPort);
				FileOutputStream oPrintStream = new FileOutputStream (m_sPrinterPort);
				byte[] aContent = _sDocument.getBytes();
				oPrintStream.write (aContent);				
				
				if (DeviceConfigTool.printerCutPaper())
				{
					log.info("Cut Paper, command : " + DeviceConfigTool.printerCutPaperCommandStr());
					oPrintStream.write (DeviceConfigTool.printerCutPaperCommand());				
				}
				oPrintStream.flush();
				oPrintStream.close();
			}
    	}
    	catch (FileNotFoundException _oFEx)
    	{
    		log.error("Can not access port : "  + _oFEx.getMessage(), _oFEx);
    	}
    	catch (ConnectException _oCNEx)
    	{
    		log.error("Connection Failed : "  + _oCNEx.getMessage(), _oCNEx);
    	}
    	catch (Exception _oEx) 
		{
    		log.error("Text Printing Failed : "  + _oEx.getMessage(), _oEx);
		}
    }
}
