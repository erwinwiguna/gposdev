package com.ssti.framework.tools;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base Peer derived from Torque
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BasePeer.java,v 1.3 2007/02/23 14:14:04 albert Exp $ <br>
 * 
 * <pre>
 * $Log: BasePeer.java,v $
 * Revision 1.3  2007/02/23 14:14:04  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class BasePeer extends org.apache.torque.util.BasePeer
{    		
}