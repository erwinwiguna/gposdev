package com.ssti.framework.tools;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.framework.om.CardioConfig;
import com.ssti.framework.om.CardioConfigPeer;
import com.ssti.framework.om.manager.CardioConfigManager;

public class CardioConfigTool extends AppConfigTool
{
	static final Log log = LogFactory.getLog(CardioConfigTool.class);
	static CardioConfigTool instance = null;
	
	public static synchronized CardioConfigTool getInstance() 
	{
		if (instance == null) instance = new CardioConfigTool();
		return instance;
	}	
	
	public static CardioConfig getCardioConfig() 
	{
		try 
		{
			CardioConfig oSC = CardioConfigManager.getInstance().getCardioConfig();
			if (oSC != null)
			{
				return oSC;
			}
			else
			{
				return createDefaultConfig();
			}			
		} 
		catch (Exception e) 
		{
			log.error(e);
		}
		return new CardioConfig();

	}

	private static CardioConfig createDefaultConfig() 
		throws Exception
	{
		//test first
		List v = CardioConfigPeer.doSelect(new Criteria());
		if (v.size() == 0)
		{		
			CardioConfig oConf = new CardioConfig();
			oConf.setCardioConfigId("1");
			oConf.setNew(true);
			oConf.save();
			
			CardioConfigManager.getInstance().refreshCache(oConf);
			return oConf;
		}		
		return getCardioConfig();		
	}

	static final List vFields = CardioConfig.getFieldNames();

	public static Object getValue(String _sField) 
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sField))
		{
			String sFirst = _sField.substring(0,1).toLowerCase();
			String sField = sFirst + _sField.substring(1);
			try 
			{
				return BeanUtil.invokeGetter(getCardioConfig(), sField);							
			} 
			catch (Exception _oEx) 
			{
				log.error("Error Getting Cardio Config "+ sField + ": " + _oEx.getMessage(), _oEx);
			}
		}
		return "";
	}
}
