package com.ssti.framework.tools;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * General purpose formatter
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomFormatter.java,v 1.16 2007/05/09 07:19:36 albert Exp $ <br>
 * 
 * <pre>
 * $Log: CustomFormatter.java,v $
 * Revision 1.16  2007/05/09 07:19:36  albert
 * *** empty log message ***
 *
 * Revision 1.15  2007/04/17 13:10:32  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CustomFormatter implements Attributes
{    		
	static CustomFormatter instance = null;
	
	static final Log log = LogFactory.getLog(CustomFormatter.class);
	
	public static synchronized CustomFormatter getInstance() 
	{
		if (instance == null) instance = new CustomFormatter();
		return instance;
	}
	
	public static DecimalFormatSymbols SYMBOL = new DecimalFormatSymbols();	
	static
	{
		if (StringUtil.equals(CardioConfigTool.getCardioConfig().getDefaultNumberLocale(), "id"))
		{
			SYMBOL.setDecimalSeparator(',');
			SYMBOL.setGroupingSeparator('.');
		}
	}
	
	/**
	 * Format Object based on object type 
	 * if Date then will be formatted as Date
	 * if Date then will be formatted as Number
	 * 
	 * @param o
	 * @return formatted String
	 */
	public static String fmt(Object o)
	{
		if (o != null)
		{
			if (o instanceof Date) return formatDate(o);
            if (o instanceof Number) return formatNumber(o);
			return o.toString();
		}
		return "";
	}
	
	/**
	 * try parse String to Decimal
	 * 
	 * @param _oValue String 
	 * @return BigDciemal
	 */
	public static Object toDecimal (Object _oValue)
	{
		try 
		{
			if (_oValue != null && _oValue instanceof String)
			{
				_oValue = new BigDecimal((String)_oValue);
			}	
		} 
		catch (Exception _oEx) 
		{
			log.error("ERROR converting object " + _oValue + " to decimal, Message: "  + _oEx.getMessage(), _oEx);
			_oEx.printStackTrace();
		}
		return _oValue;
	}
	
	/**
	 * format Object to Rupiah Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */
	public static String formatRupiah (Object _oValue)
    {
		return formatNumber(_oValue, s_RUPIAH_FORMAT);    
    }
	/**
	 * format Object to USD Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */	
	public static String formatDollar (Object _oValue)
    {
		return formatNumber(_oValue, s_DOLLAR_FORMAT);
    }
	
	/**
	 * format Object to Default Number Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */	
	public static String formatNumber (Object _oValue)
    {
		return formatNumber(_oValue, s_NUMBER_FORMAT);    
	}
	
	/**
	 * format Object to Number Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */
	public static String formatSimple (Object _oValue)
    {
    	return formatNumber(_oValue, s_SIMPLE_FORMAT);
    }	
	
	/**
	 * format Object to Aligned Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */
	public static String formatAligned (Object _oValue)
    {
    	return formatNumber(_oValue, s_ALIGNED_FORMAT);
    }
    
	/**
	 * @see formatAligned
	 * @param _oValue
	 * @return
	 */
    public static String fmtAlg (Object _oValue)
    {
        return formatAligned(_oValue);
    }
    
    /**
	 * format Object to Accounting Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */
	public static String formatAccounting (Object _oValue)
    {
		return formatNumber(_oValue, s_ACCOUNTING_FORMAT);
    }
	
	/**
	 * @see formatAccounting
	 * @param _oValue
	 * @return
	 */
    public static String fmtAcc (Object _oValue)
    {
        return formatAccounting(_oValue);
    }
    
    /**
	 * format Object to Rounded Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue
	 * @return formatted String
	 */
	public static String formatRounded (Object _oValue)
    {
		return formatNumber(_oValue, "#,##0");
    }

	/**
	 * format Object to Number Formatted String 
	 * @see Application Config Number Format 
	 * 
	 * @param _oValue oject to format
	 * @param _sFormat number format
	 * @return formatted String
	 */
	public static String formatNumber(Object _oValue, String _sFormat)
	{
		if (_oValue != null && StringUtil.isNotEmpty(_sFormat)) 
		{
			try 
			{
				
				_oValue = toDecimal (_oValue);
				NumberFormat oFormatter = new DecimalFormat(_sFormat, SYMBOL);
				return oFormatter.format(_oValue);
			} 
			catch (Exception _oEx) 
			{
				log.error("Error Parsing Number " + _oValue + " Class " + _oValue.getClass() + " Format " + _sFormat, _oEx);
			}
    	}
    	return null; 
	}
    
	/**
	 * format Accounting with options
	 * 
	 * @param _oValue  
	 * @param _bThousand display number in thousands
	 * @param _bToPlus format negative to positive
	 * @param _bDecimal display decimal
	 * @return formatted String
	 */
    public static String fmtAcc2(Object _oValue, 
    							 boolean _bThousand, 
    							 boolean _bToPlus, 
    							 boolean _bDecimal)
    {
        if (_oValue != null) 
        {
            StringBuilder sFMT = new StringBuilder("#,##0");
            try 
            {                
                _oValue = toDecimal(_oValue);
                BigDecimal bdValue = new BigDecimal(_oValue.toString());
                double dValue = 0;
                if(_bThousand)
                {
                    dValue = bdValue.doubleValue() / 1000;            
                }
                if(_bToPlus && dValue < 0)
                {
                    dValue = dValue * -1;
                }
                if (_bDecimal) sFMT.append(".00");
                sFMT.append(";(#,##0");
                if (_bDecimal) sFMT.append(".00");
                sFMT.append(")");
                
                NumberFormat oFormatter = new DecimalFormat(sFMT.toString(), SYMBOL);
                return oFormatter.format(dValue);
            } 
            catch (Exception _oEx) 
            {
                log.error("Error Parsing Number " + _oValue + " Class " + _oValue.getClass() + 
                          " Format " + sFMT.toString(), _oEx);
            }
        }
        return null;
    }
	
    /**
     * convert object to date
     * 
     * @param _oValue string
     * @return return date object
     */
	public static Object toDate (Object _oValue)
	{
		try 
		{
			if (_oValue != null && _oValue instanceof String)
			{
				_oValue = CustomParser.parseDate((String)_oValue);
			}	
		} 
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
		}
		return _oValue;
	}
    
	/**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
    public static String formatDate (Object _oValue)
    {
    	return formatCustomDate (_oValue, s_DEFAULT_DATE_FORMAT);	
	}
	
    /**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatLongDate (Object _oValue)
    {
		return formatCustomDate (_oValue, s_LONG_DATE_FORMAT);	
	}

	/**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatLongDateTime (Object _oValue)
    {
		return formatCustomDate (_oValue, s_LONG_DATE_TIME_FORMAT);	
	}

	/**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatLongDate (Object _oValue, Locale _oLocale)
    {
		return formatCustomDate (_oValue, s_LONG_DATE_FORMAT, _oLocale);	
	}

	/**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatLongDateTime (Object _oValue, Locale _oLocale)
    {
		return formatCustomDate (_oValue, s_LONG_DATE_TIME_FORMAT, _oLocale);	
	}
	
	/**
	 * format Date Object to SQL Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatSqlDate (Object _oValue)
    {
		 return formatCustomDate (_oValue, s_SQL_DATE_FORMAT);	
	}
	
	/**
	 * format Date Object to Formatted Date Time String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatDateTime (Object _oValue)
    {
		 return formatCustomDate (_oValue, s_DATE_TIME_FORMAT);	
    }
		
	/**
	 * format Date Object to File Formatted String
	 * @param _oValue Date to format
	 * @return formatted String
	 */
	public static String formatFileDateTime (Object _oValue)
    {		 
		 return formatCustomDate (_oValue, s_FILE_DATE_TIME_FORMAT);
	}
	
	/**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @param _sFormat String Format
	 * @return formatted String
	 */
	public static String formatCustomDateTime (Object _oValue,String _sFormat)
    {
		 return formatCustomDate (_oValue, _sFormat);
	}
	
	/**
	 * format Date Object to Formatted String
	 * @param _oValue Date to format
	 * @param _sFormat String Format
	 * @return formatted String
	 */
	public static String formatCustomDate (Object _oValue, String _sFormat)
    {
		 return formatCustomDate(_oValue, _sFormat, null);
	}
	
	/**
	 * format Date Object to Formatted String based on Locale
	 * @param _oValue Date to format
	 * @param _sFormat String Format
	 * @param _oLocale Locale 
	 * @return formatted String
	 */	
	public static String formatCustomDate (Object _oValue, String _sFormat, Locale _oLocale)
    {
		 if (_oValue != null && StringUtil.isNotEmpty(_sFormat)) 
		 {
			try 
			{
				_oValue = toDate (_oValue);
				DateFormat oFormatter = null;
				if (_oLocale == null)
				{
					oFormatter = new SimpleDateFormat(_sFormat);
				}
				else
				{
					oFormatter = new SimpleDateFormat(_sFormat, _oLocale);					
				}
	    	 	return oFormatter.format(_oValue);
			} 
			catch (Exception _oEx) 
			{
				log.error("Error Formatting Date " + _oValue + " Format " + _sFormat, _oEx);
			}			
		 }
		 return null;
	}

	/**
	 * Format Number to Spelled String
	 * i.e 1000 -> one thousand
	 * @param _oLocale Locale To Format
	 * @param _dNumber Number To Display
	 * @return formatted String
	 */
	public static String numberToStr (Locale _oLocale, Number _dNumber)
	{
		try
		{
			if (_oLocale != null)
			{
				if (_oLocale.getLanguage().equals("en"))
				{
					return EnglishDecimalFormat.convert(_dNumber);
				}
				else if (_oLocale.getLanguage().equals("in"))
				{
					return IndonesianDecimalFormat.convert(_dNumber);
				}
				return EnglishDecimalFormat.convert(_dNumber);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
		return "";
	}
}
