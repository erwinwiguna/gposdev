package com.ssti.framework.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * general purpose parser between types
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomParser.java,v 1.8 2007/06/26 01:51:10 albert Exp $ <br>
 * 
 * <pre>
 * $Log: CustomParser.java,v $
 * Revision 1.8  2007/06/26 01:51:10  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CustomParser
{    		
	static final Log log = LogFactory.getLog(CustomParser.class);
	
	/**
	 * Parse String to Date  
	 * "22/12/2012" -> date
	 * 
	 * @param _sInput
	 * @return Date
	 */
	public static Date parseDate (String _sInput) 
	{
		if (StringUtil.isNotEmpty(_sInput)) 
		{
			SimpleDateFormat oFormat = new SimpleDateFormat (Attributes.s_DEFAULT_DATE_FORMAT);		
			if (_sInput.contains(":")) //probably contains date & time
			{
				oFormat = new SimpleDateFormat (Attributes.s_DATE_TIME_FORMAT);
			}
			try 
			{
				Date oDate = oFormat.parse (_sInput);
				if (oDate != null && DateUtil.isToday(oDate)) 
				{
					return new Date();
				}
				return oDate;
			}
			catch (Exception _oEx) 
			{
				log.error("Error parsing date : " + _sInput, _oEx);
			}
		}
		return null;
	}

	/**
	 * Parse String with date time to Date
	 * "22/12/2012 23:45" -> date
	 * 
	 * @param _sInput
	 * @return
	 */
	public static Date parseDateTime (String _sInput) 
	{
		SimpleDateFormat oFormat = new SimpleDateFormat (Attributes.s_DATE_TIME_FORMAT);
		if (StringUtil.isNotEmpty(_sInput)) 
		{
			try 
			{
				return oFormat.parse (_sInput);
			}
			catch (Exception _oEx) 
			{
				log.error("Error parsing date time : " + _sInput, _oEx);
			}
		}
		return null;
	}	
	
	/**
	 * Parse String with date time to Date
	 * "22 September 2012" -> date
	 * 
	 * @param _sInput
	 * @return
	 */
	public static Date parseLongDate (String _sInput) 
	{
		SimpleDateFormat oFormat = new SimpleDateFormat (Attributes.s_LONG_DATE_FORMAT);
		if (StringUtil.isNotEmpty(_sInput)) 
		{
			try 
			{
				return oFormat.parse (_sInput);
			}
			catch (Exception _oEx) 
			{
				log.error("Error parsing long date : " + _sInput, _oEx);
			}
		}
		return null;
	}
	
	/**
	 * parse String as Int
	 * 
	 * @param _sInput
	 * @return
	 */
	public static int parseInt (String _sInput) 
	{
		if (StringUtil.isNotEmpty(_sInput)) 
		{
			try 
			{
				return Integer.valueOf(_sInput);
			}
			catch (Exception _oEx) 
			{
				log.error("Error parsing integer : " + _sInput, _oEx);
			}
		}
		return 0;
	}
	
	/**
	 * parse String As Number
	 * 
	 * @param _sInput
	 * @return
	 */
	public static Number parseNumber (String _sInput) 
	{
		if (StringUtil.isNotEmpty(_sInput)) 
		{
			try 
			{
				return Double.valueOf(_sInput);
			}
			catch (Exception _oEx) 
			{
				log.error("Error parsing number : " + _sInput, _oEx);
			}
		}
		return 0;
	}	
	
	/**
	 * Parse String as Date with defined Format
	 * 
	 * @param _sInput
	 * @param _sFormat
	 * @return
	 */
	public static Date parseCustom (String _sInput, String _sFormat) 
	{
		if (StringUtil.isNotEmpty(_sInput) && StringUtil.isNotEmpty(_sFormat)) 
		{
			try 			
			{
				SimpleDateFormat oFormat = new SimpleDateFormat(_sFormat);		
				Date oDate = oFormat.parse (_sInput);
				if (oDate != null && DateUtil.isToday(oDate)) 
				{
					return new Date();
				}
				return oDate;
			}
			catch (Exception _oEx) 
			{
				log.error("Error parsing date : " + _sInput, _oEx);
			}
		}
		return null;
	}
}