package com.ssti.framework.tools;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Various date related utilites
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DateUtil.java,v 1.29 2008/08/17 02:16:53 albert Exp $ <br>
 * 
 * <pre>
 * 2018-08-22
 * - add method getInterval to get jodatime Interval object
 * 
 * 2016-12-05
 * - add method getEndOfDayIncSec to get date in dd/mm/yyyy 23:59:59.9999
 * 
 * 2016-08-29
 * -add new method toSQLDate, to convert java.util.Date to java.sql.date
 * 
 * </pre><br>
 */
public class DateUtil
{    		
	public static int[] aLastDateOfMonth = new int[12];
	
	static 
	{	
		aLastDateOfMonth [0]  = 31;
		aLastDateOfMonth [1]  = 28;
		aLastDateOfMonth [2]  = 31;
		aLastDateOfMonth [3]  = 30;
		aLastDateOfMonth [4]  = 31;
		aLastDateOfMonth [5]  = 30;
		aLastDateOfMonth [6]  = 31;
		aLastDateOfMonth [7]  = 31;
		aLastDateOfMonth [8]  = 30;
		aLastDateOfMonth [9]  = 31;
		aLastDateOfMonth [10] = 30;
		aLastDateOfMonth [11] = 31;		
	}

	public static Date toDate(long l)
	{
		return new Date(l);
	}
		
	public static Date getStartOfDayDate(Date _dDate)
	{
		if (_dDate == null) _dDate = new Date();
		String sDate = CustomFormatter.formatDateTime(_dDate);
		//if (!isNow(_dDate) || sDate.contains("00:00") || sDate.contains("23:59"))
		//if (!isNow(_dDate) || sDate.contains("00:00") || sDate.contains("23:59"))
		{
			sDate = CustomFormatter.formatDate(_dDate) + " 00:00";
		}
		return CustomParser.parseDateTime(sDate);
	}

	public static Date getEndOfDayDate(Date _dDate)
	{		
		if (_dDate == null) _dDate = new Date();
		String sDate = CustomFormatter.formatDateTime(_dDate);
		if (!isNow(_dDate) || sDate.contains("00:00") || sDate.contains("23:59"))
		{
			Calendar oCal = Calendar.getInstance();
			oCal.setTime(_dDate);
			oCal.set(Calendar.HOUR_OF_DAY,23);
			oCal.set(Calendar.MINUTE,59);
			oCal.set(Calendar.SECOND,0);
			oCal.set(Calendar.MILLISECOND,0);
			return oCal.getTime();
		}
		return CustomParser.parseDateTime(sDate);
	}

	public static Date getEndOfDayIncSec(Date _dDate)
	{		
		if (_dDate == null) _dDate = new Date();		
		Calendar oCal = Calendar.getInstance();
		oCal.setTime(_dDate);
		oCal.set(Calendar.HOUR_OF_DAY,23);
		oCal.set(Calendar.MINUTE,59);
		oCal.set(Calendar.SECOND,59);
		oCal.set(Calendar.MILLISECOND,999);
		return oCal.getTime();
	}
	
	public static Date parseStartDate(String _sDate)
	{
		if (StringUtil.isNotEmpty(_sDate) && !StringUtil.contains(_sDate,":"))
		{
			_sDate = _sDate + " 00:00";
		}
		return CustomParser.parseDateTime(_sDate);	
	}

	public static Date parseEndDate(String _sDate)
	{
		if (StringUtil.isNotEmpty(_sDate) && !StringUtil.contains(_sDate,":"))
		{
			Date sDate = CustomParser.parseDate(_sDate);
			if (sDate != null)
			{
				Calendar oCal = Calendar.getInstance();
				oCal.setTime(sDate);
				oCal.set(Calendar.HOUR_OF_DAY,23);
				oCal.set(Calendar.MINUTE,59);
				oCal.set(Calendar.SECOND,59);				
				return oCal.getTime();
			}
			_sDate = _sDate + " 23:59";
		}
		return CustomParser.parseDateTime(_sDate);
	}
	
	public static Date setTimeToDate(Date _dDate, String _sHour)
	{
		if (StringUtil.isNotEmpty(_sHour))
		{
			if (_dDate == null) _dDate = new Date();
			String sDate = CustomFormatter.formatDate (_dDate);
			_dDate = CustomParser.parseDateTime (sDate + " " + _sHour);
		}
		return _dDate;		
	}
		
	public static Date getDateWithHourMinute(Date _dDate, String _sHour, String _sMinute)
	{
		String sDate = CustomFormatter.formatDate (_dDate);
		StringBuilder oSBDate = new StringBuilder(sDate);
		oSBDate.append(" ");
		oSBDate.append(_sHour);
		oSBDate.append(":");
		oSBDate.append(_sMinute);
		Date dEndDate = CustomParser.parseDateTime (oSBDate.toString());
		return dEndDate;
	}

	public static Date getStartOfMonthDate(Date _dDate)
	{
		Calendar oCal = Calendar.getInstance();
		oCal.setTime(_dDate);
		int iYear  = oCal.get(Calendar.YEAR);
        int iMonth = oCal.get(Calendar.MONTH);
		Calendar cStart = new GregorianCalendar (iYear, iMonth, 1);
		return cStart.getTime();
	}

	public static Date getStartOfMonthDate(int iMonth, int iYear)
	{
		if (iYear < 1)
		{
			iYear  = Calendar.getInstance().get(Calendar.YEAR);
		}
		Calendar cStart = new GregorianCalendar (iYear, iMonth, 1);
		return cStart.getTime();
	}
	
    public static Date getEndOfMonthDate(Date _dDate)
    {
        Calendar oCal = Calendar.getInstance();
        oCal.setTime(_dDate);
        int iYear  = oCal.get(Calendar.YEAR);
        int iMonth = oCal.get(Calendar.MONTH);
        return getEndOfMonthDate(iMonth, iYear);
    }
    
	public static Date getEndOfMonthDate(int iMonth, int iYear)
	{
		if (iYear < 1)
		{
			iYear  = Calendar.getInstance().get(Calendar.YEAR);
		}
		int iLastDateOfMonth = aLastDateOfMonth [iMonth];
		GregorianCalendar oCal = new GregorianCalendar (iYear, iMonth, 1);
		if (iMonth == 1 && oCal.isLeapYear (iYear) ) iLastDateOfMonth = 29;
		
		Calendar cStart = new GregorianCalendar (iYear, iMonth, iLastDateOfMonth, 23, 59);
		return cStart.getTime();
	}
	
	public static List getDateInRange (Date _dStart, Date  _dEnd)
	{
	    List vDates = new ArrayList ();

	    if (_dStart != null && _dEnd != null) {

			Calendar cStart = Calendar.getInstance();
	    	Calendar cEnd = Calendar.getInstance();
	    	cStart.setTime (_dStart);
	    	cEnd.setTime (_dEnd);
	    	
	    	while (cStart.before(cEnd)) {
	    		vDates.add (cStart.getTime());
	    		cStart.add (Calendar.DAY_OF_MONTH, 1);
	    	}
	    	if (!cStart.after(cEnd)) vDates.add(cStart.getTime());
	    }
	    return vDates;
	}
	
	public static Date addDays(int _iDays)
	{			
		Calendar cDate = Calendar.getInstance();
	    cDate.setTime (getStartOfDayDate(new Date()));
	    cDate.add (Calendar.DAY_OF_MONTH, _iDays);
	    return cDate.getTime();
	}	
	
	public static Date addDays(Date _dStart, int _iDays)
	{			
		Calendar cDate = Calendar.getInstance();
	    if (_dStart != null)
	    {
	    	cDate.setTime(_dStart);
	    }
	    else
	    {
	    	cDate.setTime(getStartOfDayDate(new Date()));
	    }
	    cDate.add (Calendar.DAY_OF_MONTH, _iDays);
	    return cDate.getTime();
	}
	
	public static Date addMonths(Date _dStart, int _iMonths)
	{			

		Calendar cDate = Calendar.getInstance();
	    if (_dStart != null)
	    {
	    	cDate.setTime(_dStart);
	    }
	    else
	    {
	    	cDate.setTime(getStartOfDayDate(new Date()));
	    }
	    cDate.add (Calendar.MONTH, _iMonths);
	    return cDate.getTime();
	}	
	
	public static int countAge(Date _dBirthDate)
	{
		int iAge=0;
		if(_dBirthDate!=null)
		{
			Calendar cDate = Calendar.getInstance();
	    	cDate.setTime (new Date());
	    	int iCurrentYear = cDate.get(Calendar.YEAR);
			Calendar cBDate = Calendar.getInstance();
	    	cBDate.setTime (_dBirthDate);
	    	int iBirthYear = cBDate.get(Calendar.YEAR);
	    	iAge = iCurrentYear - iBirthYear;
        	
	    	Calendar cThisYearBDate = Calendar.getInstance();	    
	    	cThisYearBDate.set (cDate.get(Calendar.YEAR), cBDate.get(Calendar.MONTH), cBDate.get(Calendar.DATE));
			
			if (cThisYearBDate.before(cDate))
			{
				return iAge - 1;
			}
		}
		return iAge;
	}

	public static int getNextAge(Date _dBirthDate)
	{
		Calendar cDate = Calendar.getInstance();
	    cDate.setTime (new Date());
	    int iCurrentYear = cDate.get(Calendar.YEAR);
		Calendar cBDate = Calendar.getInstance();
	    cBDate.setTime (_dBirthDate);
	    int iBirthYear = cBDate.get(Calendar.YEAR);
	    int iAge = iCurrentYear - iBirthYear;

	    Calendar cThisYearBDate = Calendar.getInstance();	    
	    cThisYearBDate.set (cDate.get(Calendar.YEAR), cBDate.get(Calendar.MONTH), cBDate.get(Calendar.DATE));
		
		if (cThisYearBDate.before(cDate))
		{
			return iAge + 1;
		}			    
		return iAge;
	}
	
	public static boolean isToday (Date _dDate)
	{
		Calendar cCDate = Calendar.getInstance();
	    cCDate.setTime (new Date());
	    int iCurrentDay   = cCDate.get(Calendar.DATE);
	    int iCurrentMonth = cCDate.get(Calendar.MONTH);
	    int iCurrentYear = cCDate.get(Calendar.YEAR);
	    
		Calendar cDate = Calendar.getInstance();
	    cDate.setTime (_dDate);
	    int iDay   = cDate.get(Calendar.DATE);
	    int iMonth = cDate.get(Calendar.MONTH);
	    int iYear = cDate.get(Calendar.YEAR);
	    
	    if ((iCurrentDay == iDay) && (iCurrentMonth == iMonth) && (iCurrentYear == iYear)) {
			return true;
		} 
	    return false;
	}
	
	public static boolean isNow(Date _dDate)
	{
		if (isToday(_dDate))
		{
			Calendar cCDate = Calendar.getInstance();
			cCDate.setTime (new Date());
			int iCurrentHour = cCDate.get(Calendar.HOUR);
			int iCurrentMinute = cCDate.get(Calendar.MINUTE);
			
			Calendar cDate = Calendar.getInstance();
			cDate.setTime (_dDate);
			int iHour  = cDate.get(Calendar.HOUR);
			int iMinute = cDate.get(Calendar.MINUTE);

			if ((iHour == iCurrentHour) && (iMinute <= iCurrentMinute + 1) && (iMinute >= iCurrentMinute - 1)) 
			{
				return true;
			}
	    } 
	    return false;
	}
	
	public static int countDaysinRange(Date _dStart,Date _dEnd)
	{
		Calendar cStartDate = Calendar.getInstance();
	    cStartDate.setTime (_dStart);
	    
		Calendar cEndDate = Calendar.getInstance();
	    cEndDate.setTime (_dEnd);

		int iDays = 0;
		while (cEndDate.after(cStartDate)) {
			cStartDate.add (Calendar.DATE, 1);
			iDays++;
		}
	    return iDays;
	}
	
	public static int getDaysToBirthDay (Date _dBirthDate)
	{
		Calendar cDate = Calendar.getInstance();
	    cDate.setTime (new Date());
	    
		Calendar cThisYearBirthDate = Calendar.getInstance();
	    cThisYearBirthDate.setTime (_dBirthDate);
	    cThisYearBirthDate.set (Calendar.YEAR, cDate.get(Calendar.YEAR));

		if (cDate.after(cThisYearBirthDate)) {
			//set bday to next year		
		    cThisYearBirthDate.set (Calendar.YEAR, cDate.get(Calendar.YEAR) + 1);
		}
		int iDays = 0;
		while (cThisYearBirthDate.after(cDate)) {
			cDate.add (Calendar.DATE, 1);
			iDays++;
		}
	    return iDays;
	}

    public static int getCurrentMonth()
    {
        return Calendar.getInstance().get(Calendar.MONTH);
    }

	public static int getCurrentYear()
	{
		return Calendar.getInstance().get(Calendar.YEAR);
	}
	
	public static String buildDaySB (String _sName, int _iSelected)
	{
		if (_iSelected < 1) _iSelected = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		StringBuilder oSB = new StringBuilder();
		oSB.append ("<select name=\"");
		oSB.append (_sName);
		oSB.append ("\">\n");
		oSB.append ("<option value=\"-1\">-- DATE --</option>\n");
		for (int i = 1; i <= 31; i++)
		{
			oSB.append ("<option value=\"");
			oSB.append (i);
			oSB.append ("\">"); 
			oSB.append (i);
			oSB.append ("</option>\n");
		}
		oSB.append ("</select>");
		return oSB.toString();
	}

	public static String buildYearSB (String _sName, int iFrom, int iTo, int _iSelected)
	{		
		if (iFrom < 1) iFrom = 1900;
		if (iTo < 1) iTo = Calendar.getInstance().get(Calendar.YEAR);

		
		StringBuilder oSB = new StringBuilder();
		oSB.append ("<select name=\"");
		oSB.append (_sName);
		oSB.append ("\">\n");
		oSB.append ("<option value=\"-1\">-- YEAR --</option>\n");
		for (int i = iFrom; i <= iTo; i++)
		{
			oSB.append ("<option value=\"");
			oSB.append (i);
			oSB.append ("\"  "); if (_iSelected == i ) oSB.append (" SELECTED "); oSB.append(">");
			oSB.append (i);
			oSB.append ("</option>\n");
		}
		oSB.append ("</select>");
		return oSB.toString();
	}

	//simple method to get today's date in velocity template
	public static Date getTodayDate()
	{
		return new Date();
	}

	public static boolean isDateEqual (Date _dDate1, Date _dDate2)
	{
		Calendar oCal1 = Calendar.getInstance();
		Calendar oCal2 = Calendar.getInstance();
        
		if (_dDate1 != null) oCal1.setTime (_dDate1);
		if (_dDate2 != null) oCal2.setTime (_dDate2);
        
		if (oCal1.get(Calendar.DAY_OF_MONTH) == oCal2.get(Calendar.DAY_OF_MONTH) && 
			oCal1.get(Calendar.MONTH) == oCal2.get(Calendar.MONTH) &&
			oCal1.get(Calendar.YEAR) == oCal2.get(Calendar.YEAR))
		{
			return true;
		}
        return false;		
	}

	public static boolean isBefore (Date _dDate1, Date _dDate2)
	{		
		Calendar oCal1 = Calendar.getInstance();
		Calendar oCal2 = Calendar.getInstance();
        
		if (_dDate1 != null) oCal1.setTime (_dDate1);
		if (_dDate2 != null) oCal2.setTime (_dDate2);
        
        return oCal1.before(oCal2);		
	}

	public static boolean isAfter (Date _dDate1, Date _dDate2)
	{
		Calendar oCal1 = Calendar.getInstance();
		Calendar oCal2 = Calendar.getInstance();
        
		if (_dDate1 != null) oCal1.setTime (_dDate1);
		if (_dDate2 != null) oCal2.setTime (_dDate2);
        
		//prevent second slips causing RP/PP/CF Pending
		oCal1.set(Calendar.SECOND,0);
		oCal2.set(Calendar.SECOND,0);
		oCal1.set(Calendar.MILLISECOND,0);
		oCal2.set(Calendar.MILLISECOND,0);

        return oCal1.after(oCal2);		
	}
	
	public static boolean isBackDate(Date _dDate)
	{	
		if (!isToday(_dDate) && isBefore(_dDate, new Date()))
		{				
			return true;
		}
		return false;
	}
	
	public static boolean isTimeSet(Date _dDate)
	{	
		if (_dDate != null)
		{
			String sDate = CustomFormatter.formatDateTime(_dDate);
			if (!sDate.contains("00:00"))
			{
				return true;
			}
		}
		return false;
	}
	
	public static int countAging(Date _dDueDate)
		throws Exception
	{
		return countAging (new Date(), _dDueDate);
	}	

	public static int countAging(Date _dAsOf, Date _dDueDate)
		throws Exception
	{
	    if (_dDueDate != null) 
	    {
		    if (_dAsOf == null) _dAsOf = new Date(); 
		    if (_dAsOf.after (_dDueDate)) 
		    {
		    	Calendar oDueCal = new GregorianCalendar ();
		    	oDueCal.setTime (_dDueDate);
		    	
		    	Calendar oCal = new GregorianCalendar ();
		    	oCal.setTime (_dAsOf);
		    	int iDays = 0;
		    	while (oCal.after(oDueCal)) 
		    	{
		    		oDueCal.add (Calendar.DATE, 1);
		    		iDays++;
		    	}
		    	return iDays - 1;
		    }
        }
		return 0;
	}	

	public static int countDays(Date _dFrom, Date _dTo)
		throws Exception
	{
		int iDays = 0;

	    if (_dTo != null)
		{
		    if (_dFrom == null) _dFrom = new Date();
		    
		    Calendar cFrom = new GregorianCalendar ();
		    cFrom.setTime (_dFrom);
            
            Calendar cTo = new GregorianCalendar ();
		    cTo.setTime (_dTo);
		        	
		    if (cTo.after(cFrom))
            {
		        while (cTo.after(cFrom)) 
		        {
		            cFrom.add (Calendar.DATE, 1);
		            iDays++;
                }
            }
            else if (cTo.before(cFrom))
            {
		        while (cTo.before(cFrom)) 
		        {
		            cTo.add (Calendar.DATE, 1);
		            iDays--;
                }
            }        
        }
		return iDays;		
	}		
	
	/**
	 * get Last Month End Date 
	 * i.e if supplied date is 01/06/2007 then last month is 31/05/2007
	 * 
	 * @param _dDate current date
	 * @return
	 */
	public static Date getLastMonthEndDate(Date _dDate)
	{
		if (_dDate != null)
		{
			Calendar c = new GregorianCalendar();
			c.setTime(_dDate);
			int iMonth = c.get(Calendar.MONTH);
			int iYear = c.get(Calendar.YEAR);
			if (iMonth != 0) //calendar month is [0..11]
			{
				iMonth = iMonth - 1;
			}
			else
			{
				iMonth = 11;
				iYear = iYear - 1;
			}
			return getEndOfMonthDate(iMonth, iYear);
		}
		return null;
	}

	/**
	 * get Last Month End Date 
	 * i.e if supplied date is 01/06/2007 then last month is 31/05/2007
	 * 
	 * @param _dDate current date
	 * @return
	 */
	public static Date getLastYearEndDate(Date _dDate)
	{
		if (_dDate != null)
		{
			Calendar c = new GregorianCalendar();
			c.setTime(_dDate);
			int iMonth = c.get(Calendar.MONTH);
			int iYear = c.get(Calendar.YEAR) - 1;		
			return getEndOfMonthDate(iMonth, iYear);
		}
		return null;
	}

	
	public static String getDefaultDateFormat()
	{
		return Attributes.s_DEFAULT_DATE_FORMAT;
	}

	public static String getDefaultDateFormatJS()
	{
		return Attributes.s_DEFAULT_DATE_FORMAT.replace("MM", "mm");
	}
    
    public static String getDayCode (Date _dDate)
    {
        DateTime oDt = null; 
        if (_dDate != null) 
        {
            oDt = new DateTime(_dDate);
        }
        else
        {
            oDt = new DateTime();
        }
        return oDt.dayOfWeek().getAsShortText();
    }
    
    /**
     * 
     * @return
     */
    public static TimeZone getTimeZone()
    {
    	return Calendar.getInstance().getTimeZone(); 	
    }
    
    public static Timestamp getTimestamp()
    {
    	return new Timestamp(System.currentTimeMillis()); 	
    }

    /**
     * create list of invterval
     * ["8:00 - 8.29","8:30 - 8.59" etc]
     * 
     * @param _sDate
     * @param _sSHour
     * @param _sEHour
     * @param _iInterval
     * @return
     */
    public static List createTimeSlot(String _sDate, String _sSHour, String _sEHour, int _iInterval)
    {
    	
    	Date dStart = CustomParser.parseDateTime(_sDate + " "  + _sSHour);
    	Date dEnd = CustomParser.parseDateTime(_sDate + " "  + _sEHour);
    	List vInt = new ArrayList();	    	
    	if (dStart != null && dEnd != null)	
    	{
        	DateTime oStart = new DateTime(dStart);
        	DateTime oEnd = new DateTime(dEnd);
        	        	        	
    		Interval ival = new Interval(oStart, oEnd);
    		int iTotalMin = (ival.toPeriod().getHours() * 60) + ival.toPeriod().getMinutes();
    		int iNum = (int)iTotalMin / _iInterval;
    		int iLeft = (int)iTotalMin % _iInterval;
    		if (iLeft > 0) iNum += 1;
    		
    		for(int i = 0; i < iNum; i++)
    		{
    			int iPlusMin = i * _iInterval;
    			Interval ival2 = new Interval(oStart.plusMinutes(iPlusMin), oStart.plusMinutes(iPlusMin + _iInterval - 1).plusMillis(59999));    			
        		vInt.add(ival2);
    		}    		
    	}
    	return vInt;
    }
    
    /**
     * convert string to java SQL Date
     * 
     * @param s
     * @return
     */
    public static java.sql.Date toSQLDate(String s, String fmt)
    {
    	if(StringUtil.isNotEmpty(s))
    	{
    		
    		Date d = null;
    		if(StringUtil.isNotEmpty(fmt))
    		{
    			d = CustomParser.parseCustom(s, fmt);
    		}
    		else
    		{
    			d = CustomParser.parseDate(s);
    		}
    		if(d != null)
    		{
    			return new java.sql.Date(d.getTime());
    		}
    	}
    	return null;
    }

    public static String getDOW(Date _dDate, String _sLocale)
	{
    	return LocaleTool.getDOW(_dDate, PreferenceTool.getLocale(_sLocale));
	} 
    
    public static Interval getInterval(Date _dStart, Date _dEnd)
    {
    	if(_dStart != null)
    	{
    		DateTime dEnd = new DateTime();    		
    		if(_dEnd != null)
    		{
    			dEnd = new DateTime(_dEnd);
    		}
    		return new Interval(new DateTime(_dStart), dEnd);
    	}
    	return null;
    }
}

