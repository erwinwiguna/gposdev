package com.ssti.framework.tools;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Dynamic Comparator used for Sorting, holds field name of an object <br>
 * @see SortingTool
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DynamicComparator.java,v 1.6 2007/02/23 14:14:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: DynamicComparator.java,v $
 * Revision 1.6  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class DynamicComparator implements Comparator, Attributes
{
	private static final Log log = LogFactory.getLog(DynamicComparator.class);
	
	private String m_sFieldName;
	
	private int m_iSort = 1; //DEFAULT ASC 
	
	public DynamicComparator (String _sFieldName)
	{
		m_sFieldName = _sFieldName;
	}

	/**
	 * Constructor sFieldName, Sort
	 * 
	 * @param _sFieldName
	 * @param _iSort
	 */
	public DynamicComparator (String _sFieldName, int _iSort)
	{
		m_sFieldName = _sFieldName;
		if (_iSort == i_ASC || _iSort == i_DESC) m_iSort = _iSort;
	}
	
	/**
	 * implementation of method compare in @see Comparator 
	 * 
	 * @param o1
	 * @param o2
	 * @return compare result between 2 object
	 */
	public int compare (Object o1, Object o2)
	{
		try
		{			
			Class c1 = o1.getClass();
			Class c2 = o2.getClass();
			
			PropertyDescriptor[] aP1 = Introspector.getBeanInfo(c1).getPropertyDescriptors();
			PropertyDescriptor[] aP2 = Introspector.getBeanInfo(c2).getPropertyDescriptors();
			
			PropertyDescriptor oP1 = BeanUtil.getPropertyDescriptorByName(aP1, m_sFieldName);
			PropertyDescriptor oP2 = BeanUtil.getPropertyDescriptorByName(aP2, m_sFieldName);
			
			if (oP1 != null && oP2 != null)
			{
	    		Method m1 = oP1.getReadMethod();
	    		Method m2 = oP2.getReadMethod();
	    		
	    		if (m1 != null && m2 != null)
	    		{
	    	   		Object[] aRes1 = {null};
	    	   		Object[] aRes2 = {null};
	    	   		
		   			aRes1[0] = m1.invoke ( o1, (Object[])null);
		   			aRes2[0] = m2.invoke ( o2, (Object[])null);
		   			
		   			if (aRes1[0] instanceof String) 
		   			{	
		   				String sRes1 = (String) aRes1[0];
		   				String sRes2 = (String) aRes2[0];
		   				if (m_iSort == i_DESC) return sRes2.compareTo(sRes1);
		   				return sRes1.compareTo(sRes2);
		   			}
		   			else if (aRes1[0] instanceof Number)
		   			{
		   				double dRes1 = ((Number) aRes1[0]).doubleValue();
		   				double dRes2 = ((Number) aRes2[0]).doubleValue();
		   				if (m_iSort == i_DESC) return (new Double(dRes2)).compareTo((new Double(dRes1)));
		   				return (new Double(dRes1)).compareTo((new Double(dRes2)));
		   			}
		   			else if (aRes1[0] instanceof Date)
		   			{
		   				if (m_iSort == i_DESC) return ((Date)aRes2[0]).compareTo((Date)(aRes1[0]));
		   				return ((Date)aRes1[0]).compareTo((Date)(aRes2[0]));
		   			}	   			
		   			else if (aRes1[0] instanceof Calendar)
		   			{
		   				if (m_iSort == i_DESC) return ((Calendar)aRes2[0]).compareTo((Calendar)(aRes1[0]));
		   				return ((Calendar)aRes1[0]).compareTo((Calendar)(aRes2[0]));
		   			}
		   			else if (aRes1[0] instanceof Boolean)
		   			{
		   				if (m_iSort == i_DESC) return ((Boolean)aRes2[0]).compareTo((Boolean)(aRes1[0]));
		   				return ((Boolean)aRes1[0]).compareTo((Boolean)(aRes2[0]));
		   			}
		   			
	    		}
			}	
		}
		catch(Exception _oEx)
		{
			log.error("Dynamic Comparator ERROR : " + _oEx, _oEx);
			_oEx.printStackTrace();
		}
		return 0;
	}
}
