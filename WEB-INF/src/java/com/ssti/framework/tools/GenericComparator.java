package com.ssti.framework.tools;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Comparator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Similar to <code>DynamicComparator</code>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GenericComparator.java,v 1.4 2007/02/23 14:14:04 albert Exp $ <br>
 * 
 * <pre>
 * $Log: GenericComparator.java,v $
 * Revision 1.4  2007/02/23 14:14:04  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class GenericComparator implements Comparator 
{
	Log log = LogFactory.getLog(GenericComparator.class);

	public static final int i_ASC  = 1; 
	public static final int i_DESC = 2; 
		
	private String m_sFieldName = null;
	private String m_sCompareField = null;
	private String m_sCompareMethod = null;
	private int m_iSortOrder = 1;

	/**
	 * 
	 * @param _sField
	 */
	public GenericComparator(String _sField)
	{
		this(_sField,i_ASC, null, null);	
	}	

	/**
	 * 
	 * @param _sField
	 * @param _iSortBy
	 */
	public GenericComparator(String _sField, int _iSortBy)
	{
		this(_sField,_iSortBy, null, null);	
	}	
	
	/**
	 * Constructor
	 * 
	 * @param _sField FieldName to Sort
	 * @param _iOrder Order By 1 = ASCENDING, 2 = DESCENDING
	 * @param _sCompareField Field to Compare with 
	 * @param _sCompareMethod Compile Method
	 */
	public GenericComparator(String _sField, int _iOrder, String _sCompareField, String _sCompareMethod) 
	{
		super();
		this.m_sFieldName=_sField;
		this.m_sCompareField = _sCompareField;
		this.m_sCompareMethod = _sCompareMethod;
		if (_iOrder == 2) m_iSortOrder = _iOrder;
	}
	
	/**
	 * @see java.util.Comparator#compare(Object, Object)
	 */
	public int compare(Object _oObject1, Object _oObject2) 
	{
		Object oValue1;
		Object oValue2;
		try
		{
			Field _oField = getAccessibleField(_oObject1);
					
			if (_oField == null) return 0;
			
			if (this.m_iSortOrder == i_ASC)
			{
				oValue1 = _oField.get(_oObject1);
				oValue2 = _oField.get(_oObject2);
			} 
			else 
			{
				oValue2 = _oField.get(_oObject1);
				oValue1 = _oField.get(_oObject2);	
			}	
			
			if (this.m_sCompareField != null)
			{ //someone told us which field to sort on...
				_oField = getAccessibleField(this.m_sCompareField);

				oValue1 = _oField.get(oValue1);
				oValue2=  _oField.get(oValue2);
				return this.compareField( oValue1, oValue2);
				
			} 
			else if (this.m_sCompareMethod != null)
			{
				Method m = oValue1.getClass().getMethod(this.m_sCompareMethod, (Class[])null); 
				oValue1 = m.invoke(oValue1, (Object[])null);
				oValue2 = m.invoke(oValue2, (Object[])null);	
				return this.compareField( oValue1, oValue2);
			}	
			return this.compareField( oValue1, oValue2);		
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			return 0;	
		}	
		
	}
	
	private int compareField(Object _oValue1, Object _oValue2) 
		throws IllegalAccessException
    {		
		if (_oValue1 instanceof Comparable)
		{
			return ((Comparable) _oValue1).compareTo(_oValue2);
					
		} 
		else 
		{		
			return _oValue1.toString().compareTo(_oValue2.toString()); //try String sort.
		}	
	
	
	}	
	
	private Field getAccessibleField(Object _oBean)
	{
		Field[] aFields = _oBean.getClass().getDeclaredFields();
		for (int i = 0; i < aFields.length; i++)
		{
			if (aFields[i].getName().equals(this.m_sFieldName))
			{
				aFields[i].setAccessible(true); 
				return aFields[i];	
			}
					
		}
		log.error("Field " + this.m_sFieldName + " is not accessible ");
		return null;
	}
}
