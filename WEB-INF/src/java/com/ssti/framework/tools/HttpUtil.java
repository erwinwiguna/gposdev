package com.ssti.framework.tools;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpState;

public class HttpUtil 
{
	/**
	 * copy state from current servlet request to http client state
	 * 
	 * @param request
	 * @return
	 */
	public static HttpState copyState(HttpServletRequest request)
	{
        Cookie cookies[] = request.getCookies();
        HttpState state = new HttpState();
        for (int i = 0; i < cookies.length; i++) 
        {
        	javax.servlet.http.Cookie c = cookies[i];
        	if (c.getName().equals("JSESSIONID")) 
        	{
        		org.apache.commons.httpclient.Cookie newCookie = new org.apache.commons.httpclient.Cookie();
        		newCookie.setDomain(c.getDomain());
        		newCookie.setPath(c.getPath());
        		newCookie.setName(c.getName());
        		newCookie.setValue(c.getValue());
        		state.addCookie(newCookie);
        	}
        }
		return state;
	}
	
	public static void setHeader (HttpMethod method, HttpState state)
	{
		org.apache.commons.httpclient.Cookie[] cookies = state.getCookies();
        int numberOfCookies = cookies.length;              
        for(int i = 0; i<numberOfCookies; i++)
        {
            method.addRequestHeader("Cookie",cookies[i].toString());
        }                
	}
	
	public static String getCookiesValue(HttpServletRequest request, String key)
	{
		Cookie[] cookies = request.getCookies();
		if(cookies != null)
		{
			for(int i = 0; i < cookies.length; i++)
			{
				Cookie c  = cookies[i];
				if(StringUtil.isEqual(c.getName(),key))
				{
					return c.getValue();
				}
			}
		}
		return "";
	}

	public static void saveCookies(HttpServletResponse response, 
								   String key, String value, 
								   int maxAge)
	{
		if(response != null && StringUtil.isNotEmpty(key))
		{
			Cookie c = new Cookie(key, value);
			c.setMaxAge(maxAge);
			response.addCookie(c);
		}
	}	
}
