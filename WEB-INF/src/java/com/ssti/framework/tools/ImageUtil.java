package com.ssti.framework.tools;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.google.gson.Gson;

public class ImageUtil 
{
	private static Log log = LogFactory.getLog ( ImageUtil.class );

	static ImageUtil instance = null;
	
	public static synchronized ImageUtil getInstance()
	{
		if (instance == null) instance = new ImageUtil();
		return instance;
	}
	
    /**
     * Decode string to image
     * @param imageString The string to decode
     * @return decoded image
     */
    public static BufferedImage decodeToImage(String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Encode image to string
     * @param image The image to encode
     * @param type jpeg, bmp, ...
     * @return encoded string
     */
    public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

    public static BufferedImage convertJsonToImage(String jsonString)
    {
    	System.out.println(jsonString);
        Gson gson = new Gson();
        SignatureLine[] signatureLines = gson.fromJson(jsonString, SignatureLine[].class);
        BufferedImage offscreenImage = new BufferedImage(400, 200, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = offscreenImage.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0,0,400,200);
        g2.setPaint(Color.black);
        for (SignatureLine line : signatureLines ) {
            g2.drawLine(line.lx, line.ly, line.mx, line.my);
        }
        return offscreenImage;
    }

    public static void saveJsonToImgFile(String jsonString, String _sPath) 
    	throws Exception
    {
    	ImageIO.write(convertJsonToImage(jsonString), "png", new File(_sPath));
    }
    
    public static void main (String args[]) throws IOException 
    {
        /* Test image to string and string to image start */
        BufferedImage img = ImageIO.read(new File(args[0]));
        BufferedImage newImg;
        String imgstr;
        imgstr = encodeToString(img, "png");
        System.out.println(imgstr);
        newImg = decodeToImage(imgstr);
        ImageIO.write(newImg, "png", new File(args[1]));
        /* Test image to string and string to image finish */
    }
    
    public class SignatureLine 
    {
        int lx, ly, mx, my;

        public SignatureLine() {
        }

        public int getLx() {
            return lx;
        }

        public void setLx(int lx) {
            this.lx = lx;
        }

        public int getLy() {
            return ly;
        }

        public void setLy(int ly) {
            this.ly = ly;
        }

        public int getMx() {
            return mx;
        }

        public void setMx(int mx) {
            this.mx = mx;
        }

        public int getMy() {
            return my;
        }

        public void setMy(int my) {
            this.my = my;
        }
    }
}