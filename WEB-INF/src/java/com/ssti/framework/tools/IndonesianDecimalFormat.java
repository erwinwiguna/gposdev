package com.ssti.framework.tools;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Display number in Indonesian language
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: IndonesianDecimalFormat.java,v 1.3 2007/02/23 14:14:04 albert Exp $ <br>
 * 
 * <pre>
 * $Log: IndonesianDecimalFormat.java,v $
 * Revision 1.3  2007/02/23 14:14:04  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class IndonesianDecimalFormat 
{
	private static final Log log = LogFactory.getLog(IndonesianDecimalFormat.class);
	
	static final String s_ZERO = " nol";
	static final String s_COMMA = " koma";
	static final String s_MINUS = "minus";
	
	private static final String[] majorNames = 
	{
	    "",
	    " ribu",
	    " juta",
	    " miliar",
		" triliun",
		" biliun",
		" "
	};

	private static final String[] tensNames = 
	{
	    "",
	    " sepuluh",
	    " dua puluh",
	    " tiga puluh",
	    " empat puluh",
	    " lima puluh",
	    " enam puluh",
	    " tujuh puluh",
	    " delapan puluh",
	    " sembilan puluh"
	};

	private static final String[] numNames = 
	{
	    "",
	    " satu",
	    " dua",
	    " tiga",
	    " empat",
	    " lima",
	    " enam",
	    " tujuh",
	    " delapan",
	    " sembilan",
	    " sepuluh",
	    " sebelas",
	    " duabelas",
	    " tigabelas",
	    " empatbelas",
	    " limabelas",
	    " enambelas",
	    " tujuhbelas",
	    " delapanbelas",
	    " sembilanbelas"
	};

	private static String convertLessThanOneThousand(int number) 
	{
	  	String soFar;

	    if (number % 100 < 20)
	    {
	        soFar = numNames[number % 100];
	        number /= 100;
	    }
	    else 
	    {
	        soFar = numNames[number % 10];
	        number /= 10;

	        soFar = tensNames[number % 10] + soFar;
	        number /= 10;
	    }
	    if (number == 0) return soFar;
	    return numNames[number] + " ratus" + soFar;
	}
	
	private static String formatIndonesianNumber (String _sNumber)
    {
	 	_sNumber = _sNumber.replaceAll("satu ratus", "seratus");
		if (_sNumber.startsWith("satu ribu"))
		{
			_sNumber = _sNumber.replaceFirst("satu ribu", "seribu");
		}
		return _sNumber;	
    }

	public static String simpleConvert(Object o)
	{
		String s = o.toString();
		StringBuilder sResult = new StringBuilder();
		for (int i = 0; i < s.length(); i++)
		{
			int iIdx = Integer.parseInt(Character.toString(s.charAt(i)));
			String sN = numNames[iIdx];
			if (StringUtil.isEmpty(sN)) sN = s_ZERO;
			sResult.append(sN);	
		}
		return sResult.toString();
	}
	
	public static String convert(Number _number) 
	{
		log.debug("num double val : " + Double.toString(_number.doubleValue()));
		BigDecimal bdNumber = new BigDecimal(Double.toString(_number.doubleValue()));
		log.debug("num BD val : " + bdNumber);
		
		double number = bdNumber.setScale(0,BigDecimal.ROUND_DOWN).doubleValue();
		log.debug("rounded val : " + number);
				
		double comma = new BigDecimal(bdNumber.doubleValue() - number)
			.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		
		int iComma = 0;
		
		String sComma = null;
		if (comma > 0)
		{
			log.debug("comma val : " + comma);
			sComma = Double.toString(comma);
			if (sComma.length() > 2)
			{
				sComma = sComma.substring(2);
			}
			log.debug("comma num : " + sComma);	
			sComma = simpleConvert (sComma);
			log.debug("comma fmtd : " + sComma);
		}
		
	    /* special case */
	    if (number == 0) { return s_ZERO; }

	    String prefix = "";

	    if (number < 0) 
	    {
	        number = -number;
	        prefix = s_MINUS;
	    }

	    String soFar = "";
	    int place = 0;
	    do 
	    {
	    	int n = (int)(number % 1000);
	    	if (n != 0)
	    	{
	    		String s = convertLessThanOneThousand(n);
	    		soFar = s + majorNames[place] + soFar;
	    	}
	    	place++;
	    	number /= 1000;
	    } 
	    while (number > 0);
	    String sResult = (prefix + soFar).trim();
	    sResult = formatIndonesianNumber(sResult);
	    if (StringUtil.isNotEmpty(sComma))
	    {
	    	sResult = sResult.concat(s_COMMA).concat(sComma);
	    }
	    return sResult;
	}
}