package com.ssti.framework.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.torque.om.Persistent;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <code>Persistent</code> Interface related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PersistentTool.java,v 1.8 2007/02/23 14:14:05 albert Exp $ <br>
 * 
 * <pre>
 * $Log: PersistentTool.java,v $
 * Revision 1.8  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PersistentTool
{    		
	public static void loadPersistentData(List _vData, Connection _oConn) 
		throws Exception 
	{
		for (int i = 0; i < _vData.size(); i++)
		{
			Persistent oData = (Persistent) _vData.get(i);
			savePersistentObject (oData, _oConn);			
		}
	}
	
	 public static void savePersistentObject (Persistent _oData, Connection _oConn) 
    	throws Exception
    {
	 	_oData.setModified(true);
		_oData.setNew(true);
		if (_oConn != null)
		{
			_oData.save(_oConn);	
		}
		else
		{
			_oData.save();
		}
    }

	 public static void saveOrUpdatePersistentObject (Persistent _oData, Persistent _oExist, Connection _oConn) 
    	throws Exception
    {
		if (_oExist != null) 
		{
			//update
			_oData.setModified (true);
			_oData.setNew (false);
			_oData.save (_oConn);
		} 
		else 
		{
			//insert new
			_oData.setModified (true);
			_oData.setNew (true);
			_oData.save (_oConn);
		}
    }    
    
	 public static void saveOrUpdatePersistentObject (Persistent _oData, Persistent _oExist) 
	 	throws Exception
	 {
	 	if (_oExist != null) 
	 	{
	 		//update
	 		_oData.setModified (true);
	 		_oData.setNew (false);
	 		_oData.save ();
	 	} 
	 	else 
	 	{
	 		//insert new
	 		_oData.setModified (true);
	 		_oData.setNew (true);
	 		_oData.save ();
	 	}
	 }  	 
	 
    public static List getListOfID(List _vObject)
    {
        List vData = new ArrayList(_vObject.size());
        for(int i=0;i<_vObject.size();i++)
        {
            Persistent oData = (Persistent) _vObject.get(i);
            vData.add(oData.getPrimaryKey().toString());
        }
        return vData;
    }
}
