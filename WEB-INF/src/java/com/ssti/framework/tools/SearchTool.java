package com.ssti.framework.tools;

import org.apache.torque.util.Criteria;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * General purpose Search Tool
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SearchTool.java,v 1.4 2007/02/23 14:14:05 albert Exp $ <br>
 * 
 * <pre>
 * $Log: SearchTool.java,v $
 * Revision 1.4  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SearchTool
{    		
	public static final String s_EQUAL 		    = "=" ;
	public static final String s_LESS_EQUAL 	= "<=";
	public static final String s_GREATER_EQUAL  = ">=";
	public static final String s_LESS 			= "<" ;
	public static final String s_GREATER 		= ">" ;
	public static final String s_NOT_EQUAL 		= "!=" ;
	
	public static final int i_EQUAL 		= 1;
	public static final int i_LESS_EQUAL 	= 2;
	public static final int i_GREATER_EQUAL = 3;
	public static final int i_LESS 		 	= 4;
	public static final int i_GREATER 		= 5;
	
	public static final String s_AND = "AND" ;
	public static final String s_OR  = "OR"  ;
	public static final String s_NOT = "NOT" ;
	
	public static final int i_AND 	= 6;
	public static final int i_OR 	= 7;
	public static final int i_NOT 	= 8;

	public static String toString (int _iSymbol)
	{
		if (_iSymbol == i_EQUAL			) return s_EQUAL		 ;
		if (_iSymbol == i_LESS_EQUAL 	) return s_LESS_EQUAL 	 ;
		if (_iSymbol == i_GREATER_EQUAL ) return s_GREATER_EQUAL ;
		if (_iSymbol == i_LESS 		 	) return s_LESS 		 ;
		if (_iSymbol == i_GREATER 		) return s_GREATER 		 ;
		if (_iSymbol == i_AND 			) return s_AND 			 ;
		if (_iSymbol == i_OR 			) return s_OR 			 ;
		if (_iSymbol == i_NOT 			) return s_NOT 			 ;
		
		return "";
	}

	public static Object getSqlEnum (int _iSymbol)
	{
		if (_iSymbol == i_EQUAL			) return Criteria.EQUAL		     ;
		if (_iSymbol == i_LESS_EQUAL 	) return Criteria.LESS_EQUAL 	 ;
		if (_iSymbol == i_GREATER_EQUAL ) return Criteria.GREATER_EQUAL  ;
		if (_iSymbol == i_LESS 		 	) return Criteria.LESS_THAN 	 ;
		if (_iSymbol == i_GREATER 		) return Criteria.GREATER_THAN 	 ;
		
		return null;
	}

	public static int getInt (String _sSymbol)
	{
		if (_sSymbol.equals ( s_EQUAL			)) return i_EQUAL		  ;
		if (_sSymbol.equals ( s_LESS_EQUAL 		)) return i_LESS_EQUAL 	  ;
		if (_sSymbol.equals ( s_GREATER_EQUAL   )) return i_GREATER_EQUAL ;
		if (_sSymbol.equals ( s_LESS 		 	)) return i_LESS 		  ;
		if (_sSymbol.equals ( s_GREATER 		)) return i_GREATER 	  ;
		if (_sSymbol.equals ( s_AND 			)) return i_AND 		  ;
		if (_sSymbol.equals ( s_OR 				)) return i_OR 			  ;
		if (_sSymbol.equals ( s_NOT 			)) return i_NOT 		  ;
		
		return -1;
	}
}