package com.ssti.framework.tools;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.thoughtworks.xstream.XStream;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Serialization related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SerializationUtil.java,v 1.11 2009/05/04 01:33:51 albert Exp $ <br>
 * 
 * <pre>
 * $Log: SerializationUtil.java,v $
 * Revision 1.11  2009/05/04 01:33:51  albert
 * *** empty log message ***
 *
 * Revision 1.10  2008/06/29 07:11:30  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SerializationUtil
{
	static final XStream xstream = new XStream();
	
	static final Log log = LogFactory.getLog(SerializationUtil.class);
	
	static
	{
	}
	
    public static Object clone(Serializable object)
    	throws Exception
    {
        return deserialize(serialize(object));
    }

    public static void toXML(Serializable obj, Writer fileWriter)
		throws Exception
	{
		xstream.toXML(obj, fileWriter);
	}
    
    public static void serialize(Serializable obj, OutputStream outputStream)
    	throws Exception
    {
        ObjectOutputStream out = null;
        try
        {
            out = new ObjectOutputStream(outputStream);
            out.writeObject(obj);
        }
        catch(IOException ex)
        {
            throw new Exception(ex);
        }
        finally
        {
            try
            {
                if(out != null)
                    out.close();
            }
            catch(IOException ex) { }
        }
    }

    public static byte[] serialize(Serializable obj)
    	throws Exception
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serialize(obj, baos);
        return baos.toByteArray();
    }

    public static Object deserialize(InputStream inputStream)
    	throws Exception
    {
        ObjectInputStream in = null;
        try
        {
            in = new ObjectInputStream(inputStream);
            Object obj = in.readObject();
            return obj;
        }
        catch(ClassNotFoundException ex)
        {
            throw new Exception(ex);
        }
        catch(IOException ex)
        {
            throw new Exception(ex);
        }
        finally
        {
            try
            {
                if(in != null)
                    in.close();
            }
            catch(IOException ex) { }
        }
    }

    public static Object deserializeXML(InputStream inputStream)
		throws Exception
	{
    	long lstart = System.currentTimeMillis();
    	Reader oReader = new BufferedReader(new InputStreamReader(inputStream));
    	Object oData = xstream.fromXML(oReader);
    	oReader.close();
    	log.debug("Deserialize from XML : " + (System.currentTimeMillis() - lstart) + " ms");
    	return oData;

	}	    
    
    public static Object deserialize(byte objectData[])
    	throws Exception
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(objectData);
        return deserialize(bais);
    }	

    public static Object deserialize(String _sFilePath)
    	throws StreamCorruptedException, FileNotFoundException, Exception
    {
	    File oFile = new File (_sFilePath);
        if (oFile.exists()) 
        {        		
            if(IOTool.getFileExtension(oFile.getName()).equals(".xml"))
            {
            	long lstart = System.currentTimeMillis();
            	Reader oReader = new BufferedReader(new FileReader(oFile));
            	Object oData = xstream.fromXML(oReader);
            	oReader.close();
            	log.debug("Deserialize from XML : " + (System.currentTimeMillis() - lstart) + " ms");
            	return oData;
            }
            else
            {
            	long lstart = System.currentTimeMillis();
            	InputStream oBufferStream = new BufferedInputStream(new FileInputStream (oFile));
	        	ObjectInputStream oStream = new ObjectInputStream(oBufferStream);
		    	Object oData = oStream.readObject();
	        	oStream.close();        		
            	log.debug("Deserialize from Binary : " + (System.currentTimeMillis() - lstart) + " ms");
	        	return oData;
            }
        }
        return null;
    }	    
}