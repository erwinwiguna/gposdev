package com.ssti.framework.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Sorting Tool can be used to sort list of object. Possibilities are : <br>
 * <ul>
 * <li>list and custom comparator (asc/desc depends on comparator) 
 * <li>list and object to sort field name (ascending) 
 * <li>list and object to sort field name (ascending or <br>
 *     descending depends on Attributes.i_ASC / Attributes.i_DESC) 
 * </ul>
 * 
 * @see DynamicComparator   
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SortingTool.java,v 1.7 2008/08/17 02:16:53 albert Exp $ <br>
 * 
 * <pre>
 * $Log: SortingTool.java,v $
 * Revision 1.7  2008/08/17 02:16:53  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */

public class SortingTool 
{
	static final Log log = LogFactory.getLog(SortingTool.class);
	
	/**
	 * Sort using custom comparator. 
	 * 
	 * @param vData
	 * @param sComparatorName comparator class name
	 * @return sorted list
	 * @throws Exception
	 */
	public static List sortList (List vData, String sComparatorName) 
	    throws Exception
	{    
	    if(sComparatorName != null)
	    {
	        Object[] aData = vData.toArray();
	        Comparator cComparator = (Comparator) Class.forName(sComparatorName).newInstance();
	        Arrays.sort(aData, cComparator);
	        //Arrays.asList return AbstractList, cannot do any operation on the resulting List
	        //so we need to create an array List as real object
	        vData = new ArrayList(Arrays.asList(aData));
	    }
	    return vData;
	}
	
	/**
	 * Sort using field name as parameter
	 * @param _vData List to sort
	 * @param _sFieldName field name
	 * @return sorted list
	 * @throws Exception
	 */
	public static List sort (List _vData, String _sFieldName) 
	    throws Exception
	{    		
		//default to ascending
		return sort (_vData, _sFieldName, Attributes.i_ASC);
	}	

	/**
	 * Sort using field name as parameter and asc/desc
	 * @param _vData List to sort
	 * @param _sFieldName field name
	 * @param _iSort ASC/DESC
	 * @return sorted list
	 * @throws Exception
	 */
	public static List sort (List _vData, String _sFieldName, int _iSort) 
	    throws Exception
	{
		long lStart = System.currentTimeMillis();
		
		if (_vData != null)
		{
			Object[] aData = _vData.toArray();
		    Arrays.sort(aData, new DynamicComparator(_sFieldName, _iSort));
		    
		    //Arrays.asList return AbstractList, cannot do any operation on the resulting List
	        //so we need to create an array List as real object
		    _vData = new ArrayList(Arrays.asList(aData));
		    		    
		    if (log.isDebugEnabled())
			{
			    //long lElapsed = System.currentTimeMillis() - lStart;
		    	//log.debug ("sorting list with size : " + _vData.size() + " took : " + lElapsed + " ms");
			}
		}
	    return _vData;
	}	
	
	/**
	 * Sort using field name as parameter and asc/desc
	 * @param _vData List to sort
	 * @param _sFieldName field name
	 * @param _iSort ASC/DESC
	 * @return sorted list
	 * @throws Exception
	 */
	public static List sort (Collection _vData) 
	    throws Exception
	{
		long lStart = System.currentTimeMillis();
		
		List vResult = null;
		if (_vData != null)
		{
			Object[] aData = _vData.toArray();
		    Arrays.sort(aData);
		    
		    //Arrays.asList return AbstractList, cannot do any operation on the resulting List
	        //so we need to create an array List as real object
		    vResult = new ArrayList(Arrays.asList(aData));
		    		    
		    if (log.isDebugEnabled())
			{
			    //long lElapsed = System.currentTimeMillis() - lStart;
		    	//log.debug ("sorting list with size : " + _vData.size() + " took : " + lElapsed + " ms");
			}
		}
	    return vResult;
	}	
}