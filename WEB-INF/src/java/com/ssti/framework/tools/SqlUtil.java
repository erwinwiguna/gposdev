package com.ssti.framework.tools;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LimitHelper;
import org.apache.torque.util.Query;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.workingdogs.village.Record;
import com.workingdogs.village.Schema;
import com.workingdogs.village.Value;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Various SQL Utilities 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SqlUtil.java,v 1.14 2009/05/04 01:34:15 albert Exp $ <br>
 * 
 * <pre>
 * $Log: SqlUtil.java,v $
 * 
 * 2018-04-15
 * - add method weekCol, format week column
 * 
 * 2018-01-27
 * - add method getConn and closeConn, to connect to another postgresql
 * 
 * 2016-10-19
 * - add method createJSON (List _vData)
 *   convert List of Record to flat JSON Array 
 * 
 * 2016-08-17
 * - add method toListOfKeys(List _vData, String _sKeyFieldName)
 * 	 create list of user defined field keys of passed object 
 *   i.e List<Item> / "itemId"
 *   i.e List<Record> / "item_id"
 * 
 * 2016-03-28
 * - add method createDataMap(List, keyCol, ValCol, op) to convert List to map for easy data access in template
 * 
 * 2015-10-17
 * - add method in SqlUtil.setBirthDayCriteria
 * - birthday format supported: dd-dd/mm 10-20/01 
 * 
 * 2015-05-12
 * - add method getDistinctInRecord, filterRecord
 * 
 * </pre><br>
 */
public class SqlUtil
{    		
	private static final Log log = LogFactory.getLog(SqlUtil.class);
	
	private static SqlUtil instance = null;
	
	public static synchronized SqlUtil getInstance()
	{
		if (instance == null) instance = new SqlUtil();
		return instance;
	}
	
	private static final String s_DB_TYPE = 
		Attributes.CONFIG.getString("retailsoft.db.server");

	/**
	 * execute SQL query through Base Peer
	 * 
	 * @param _sSQL
	 * @return list of village Records
	 * @throws Exception
	 */
	public static List executeQuery ( String _sSQL)
		throws Exception
	{
		return BasePeer.executeQuery(_sSQL);
	}  
	
	/**
	 * execute SQL statement through Base Peer
	 * 
	 * @param _sSQL
	 * @throws Exception
	 */
	public static int executeStatement(String _sSQL)
		throws Exception
	{
		return BasePeer.executeStatement(_sSQL);
	}  

	/**
	 * create like sql string
	 * 
	 * @param _sColumn
	 * @param _sKeyword
	 * @return like with default true prefix & suffix
	 */
	public static String like (String _sColumn, String _sKeyword)
	{
		return like (_sColumn, _sKeyword, true, true);
	}  
	
	/**
	 * create like sql string
	 * 
	 * @param _sColumn
	 * @param _sKeyword
	 * @param _bPrefix add % to first char of keyword
	 * @param _bSuffix add % to last char of keyword
	 * @return sql string
	 */
	public static String like ( String _sColumn, 
							    String _sKeyword, 
								boolean _bPrefix, 
								boolean _bSuffix)
	{
		StringBuilder oSB = new StringBuilder (_sColumn);
		if (s_DB_TYPE.equals("postgresql"))
		{
			oSB.append (" ILIKE ");		
		}
		else
		{
			oSB.append (" LIKE ");					
		}
		oSB.append("'");
		if (_bPrefix) oSB.append ("%");
		oSB.append (escape(_sKeyword));		
		if (_bSuffix) oSB.append ("%");
		oSB.append ("'");
		return oSB.toString();
	}  

	/**
	 * simple query helper to build compare query
	 * 
	 * @param _sColumn
	 * @param _sKeyword
	 * @param _sSign
	 * @return compare query
	 */
	public static String buildCompareQuery (String _sColumn, String _sKeyword, String _sSign)
	{
		StringBuilder oSB = new StringBuilder (_sColumn);
		oSB.append (_sSign);
		oSB.append (escape(_sKeyword));
		return oSB.toString();
	}  
	
	/**
	 * convert arraylist data to IN sql string
	 * 
	 * @param _vData
	 * @return string of IN SQL
	 */
	public static String convertToINMode (List _vData)
	{
		StringBuilder oSB = new StringBuilder ("(");
		for (int i = 0; i < _vData.size(); i++)
		{
			oSB.append("'");			
			oSB.append(_vData.get(i));
			oSB.append("'");						
			if (i != (_vData.size() - 1)) {
				oSB.append(",");
			}
		}
		oSB.append(")");
		String sResult = oSB.toString();
		if (sResult.equals("()")) //if empty
		{
			sResult = "('')";
		}
		return sResult;
	}  

	/**
	 * execute SQL from given templates
	 * 
	 * @param _sTPLName
	 * @param _oTplObject
	 * @param _oConn
	 * @return statement execute result
	 * @throws Exception
	 */
	public static boolean executeSqlFromTemplate (String _sTPLName, 
												  Object _oTplObject,
												  Connection _oConn) 
		throws Exception
	{
		String s_TPL_PATH = "/sql/";
    	String s_VEL_PROP = "file.resource.loader.path";
    	
		Properties oProp = new Properties();
	    oProp.setProperty(s_VEL_PROP, s_TPL_PATH); 	
		Velocity.init(oProp);		
		
		StringWriter oWriter = new StringWriter ();
		VelocityContext oCtx = new VelocityContext();
        oCtx.put ("data", _oTplObject);
        Velocity.mergeTemplate(s_TPL_PATH + _sTPLName, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );

		Statement oStmt = _oConn.createStatement();
		boolean bResult = oStmt.execute (oWriter.toString());
		oStmt.close();
		return bResult;
	}
	
	/**
	 * validate whether a data exist in other table
	 * 
	 * @param _sTable
	 * @param _sColumn
	 * @param _sValue
	 * @return whether data exist in other table
	 * @throws Exception
	 */
	public static boolean validateFKRef(String _sTable, String _sColumn, String _sValue)
		throws Exception
	{
		StringBuilder oSQL = new  StringBuilder ();
		oSQL.append ("SELECT * FROM ")
			.append (_sTable)
			.append (" WHERE ")
			.append (_sColumn)
			.append (" = '")
			.append (_sValue)
			.append ("' LIMIT 1");
		
		List vData = BasePeer.executeQuery(oSQL.toString());
		if(vData.size() > 0) 
		{			
			throw new Exception("Data " + _sValue + " has Reference to: " + _sTable + " Column: " + _sColumn);
			//return false;
		}
		return true;
	}

	/**
	 * validate whether a data exist in other table
	 * 
	 * @param _sTable
	 * @param _sColumn
	 * @param _sValue
	 * @return whether data exist in other table
	 * @throws Exception
	 */
	public static boolean validateFKRef(String _sTable, String _sColumn, String _sValue, StringBuilder _oMsg)
		throws Exception
	{
		StringBuilder oSQL = new  StringBuilder ();
		oSQL.append ("SELECT * FROM ")
			.append (_sTable)
			.append (" WHERE ")
			.append (_sColumn)
			.append (" = '")
			.append (_sValue)
			.append ("' LIMIT 1");
		
		List vData = BasePeer.executeQuery(oSQL.toString());
		if(vData.size() > 0) 
		{
			String sError = "FK Ref: " + _sTable + " Column: " + _sColumn;
			if (_oMsg != null) _oMsg.append(sError).append("\n");			
			log.warn(sError);
			return false;
		}
		return true;
	}

	
	/**
	 * count data rows in tables 
	 * 
	 * @param _sTable
	 * @return number of records
	 * @throws Exception
	 */
	public static int countRecords(String _sTable)
		throws Exception
	{
		return countRecords(_sTable, "", "");
	}
	
	/**
	 * count data rows in tables 
	 * 
	 * @param _sTable
	 * @param _sColumn
	 * @param _sClause
	 * @return number of records
	 * @throws Exception
	 */
	public static int countRecords(String _sTable, String _sColumn, String _sClause)
		throws Exception
	{
		StringBuilder oSQL = new  StringBuilder();
		if (StringUtil.isEmpty(_sColumn))
		{
			_sColumn = "*";
		}
		oSQL.append ("SELECT COUNT(").append(_sColumn).append(") FROM ").append (_sTable);
		
		if (StringUtil.isNotEmpty(_sClause))
		{
			oSQL.append(" ").append(_sClause);
		}
		List vData = BasePeer.executeQuery(oSQL.toString());
		if(vData.size() > 0) 
		{	
			Record oRecord = (Record) vData.get(0);
			return oRecord.getValue(1).asInt();			
		}
		return 0;
	}	
	
	/**
	 * add limit query to current sql string
	 * 
	 * @param _oSQL
	 * @param _iLimit
	 * @return string with added limit query
	 * @throws Exception
	 */
	public static StringBuilder buildLimitQuery (StringBuilder _oSQL, int _iLimit)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.setLimit(_iLimit);
		Query q = new Query();
		LimitHelper.buildLimit(oCrit, q);
		
		if (_iLimit > 0)
		{
			if (q.getPreLimit() != null) _oSQL.insert(0, q.getPreLimit());
			if (q.getLimit() != null) _oSQL.append (" LIMIT ").append (_iLimit);
			if (q.getPostLimit() != null) _oSQL.append(q.getPostLimit());		
		}
		return _oSQL;
	}	
	
	/**
	 * get day specific select function for particular database
	 * 
	 * @param _sColumn
	 * @return string of day select function (01-31)
	 * @throws Exception
	 */
	public static String dayCol (String _sColumn)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		if (s_DB_TYPE.equals("oracle") || s_DB_TYPE.equals("postgresql"))
		{
			oSB.append(" TO_CHAR(").append(_sColumn).append(",'DD')");						
		}
		else
		{
			oSB.append(" DAYOFMONTH(").append(_sColumn).append(")");
		}
		return oSB.toString();
	}	
	
	/**
	 * get day specific select function for particular database
	 * 
	 * @param _sColumn
	 * @return string of week (1-53) 
	 * @throws Exception
	 */
	public static String weekCol (String _sColumn)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		if (s_DB_TYPE.equals("oracle") || s_DB_TYPE.equals("postgresql"))
		{
			oSB.append(" TO_CHAR(").append(_sColumn).append(",'WW')");						
		}
		else
		{
			oSB.append(" WEEKOFMONTH(").append(_sColumn).append(")");
		}
		return oSB.toString();
	}

	/**
	 * get month specific select function for particular database
	 * 
	 * @param _sColumn
	 * @return string of month select function 
	 * @throws Exception
	 */
	public static String monthCol (String _sColumn)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		if (s_DB_TYPE.equals("oracle") || s_DB_TYPE.equals("postgresql"))
		{
			oSB.append(" TO_CHAR(").append(_sColumn).append(",'MM')");						
		}
		else
		{
			oSB.append(" MONTH(").append(_sColumn).append(")");
		}
		return oSB.toString();
	}	
	
	/**
	 * check if in keywords contains ' then escape into \'
	 * 
	 * @param _sStr
	 * @return escaped string
	 */
    public static String escape(String _sStr)
    {
    	if (_sStr != null)
    	{
    		StringBuilder oSB = new StringBuilder (_sStr);
    		int iAddedChar = 0;
    		for (int i = 0; i < _sStr.length(); i++)
    		{
    			if (_sStr.charAt(i) == '\'' || _sStr.charAt(i) == '\\')
    			{
    	    		oSB.insert(i + iAddedChar, '\\');    				
    	    		iAddedChar++;
    			}
    		}
			_sStr = oSB.toString();
    	}
    	return _sStr;
    }
    
    /**
     * function similar to BeanUtil.getDistinctListByField
     * filter distinct list of record by column value
     * 
     * @param _vData
     * @param _iCol
     * @return
     * @throws Exception
     */
    public static List getDistinctInRecord (List _vData, int _iCol) 
    	throws Exception
    {
    	List vResult = new ArrayList(_vData.size());
    	for (int i = 0; i < _vData.size(); i++)
    	{
    		Record oRec = (Record) _vData.get(i);
    		Value oVal = oRec.getValue(_iCol);
    		if (oVal != null && !isExists(vResult, oVal))
    		{
    			vResult.add(oVal);
    		}
    	}
    	return vResult;
    }

    /**
     * function similar to BeanUtil.getDistinctListByField
     * filter distinct list of record by column value
     * 
     * @param _vData
     * @param _sCol - Column Name
     * @return
     * @throws Exception
     */
    public static List getDistinctInRecord (List _vData, String _sCol) 
    	throws Exception
    {
    	List vResult = new ArrayList(_vData.size());
    	for (int i = 0; i < _vData.size(); i++)
    	{
    		Record oRec = (Record) _vData.get(i);
    		Value oVal = oRec.getValue(_sCol);
    		if (oVal != null && !isExists(vResult, oVal))
    		{
    			vResult.add(oVal);
    		}
    	}
    	return vResult;
    }

    
    /**
     * check wheter certain value exist in result list
     * 
     * @param _vResult
     * @param _oVal
     * @return
     */
    public static boolean isExists(List _vResult, Value _oVal)
    {
    	for (int i = 0; i < _vResult.size(); i++)
    	{    
    		Value oVal = (Value) _vResult.get(i);
    		if (oVal.asString().equals(_oVal.asString()))
    		{
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * 
     * @param _vData
     * @param _iCol
     * @param _sValue
     * @return
     * @throws Exception
     */
    public static List filterRecord (List _vData, int _iCol, String _sValue) 
    	throws Exception
    {
    	List vResult = new ArrayList(_vData.size());
    	for (int i = 0; i < _vData.size(); i++)
    	{
    		Record oRec = (Record) _vData.get(i);
    		Value oVal = oRec.getValue(_iCol);
    		if (StringUtil.isEqual(oVal.asString(), _sValue))
    		{
    			vResult.add(oRec);
    		}
    	}
    	return vResult;
    }

    /**
     * 
     * @param _vData
     * @param _sCol 
     * @param _sValue
     * @return
     * @throws Exception
     */
    public static List filterRecord (List _vData, String _sCol, String _sValue) 
    	throws Exception
    {
    	if(_vData != null)
    	{
	    	List vResult = new ArrayList(_vData.size());
	    	for (int i = 0; i < _vData.size(); i++)
	    	{
	    		Record oRec = (Record) _vData.get(i);
	    		Value oVal = oRec.getValue(_sCol);
	    		if (StringUtil.isEqual(oVal.asString(), _sValue))
	    		{
	    			vResult.add(oRec);
	    		}
	    	}
	    	return vResult;
    	}
    	return null;
    }

    
    /**
     * 
     * @param _vData
     * @param _sSumCol
     * @param _sSearchCol
     * @param _sValue
     * @return
     * @throws Exception
     */
    public static double sumRecord (List _vData, String _sSumCol, String _sSearchCol, String _sValue) 
    	throws Exception
    {
    	double dAmount = 0;
    	if(_vData != null)
    	{
	    	for (int i = 0; i < _vData.size(); i++)
	    	{
	    		Record oRec = (Record) _vData.get(i);
	    		if(StringUtil.isNotEmpty(_sSearchCol))
	    		{	
	    			Value oVal = oRec.getValue(_sSearchCol);
	    			if (StringUtil.isEqual(oVal.asString(), _sValue))
	        		{
	            		dAmount = dAmount + oRec.getValue(_sSumCol).asDouble();    			
	        		}
	    		}  
	    		else
	    		{
	        		dAmount = dAmount + oRec.getValue(_sSumCol).asDouble();  
	    		}    		
	    	}
    	}
    	return dAmount;
    }
    
    /**
     * create list of primary keys of passed object
     * 
     * @param _vData
     * @return
     */
    public static List toListOfID(List _vData)
    {
    	List vID = new ArrayList();
    	if (_vData != null)
    	{
    		try
    		{
		    	for (int i = 0; i < _vData.size(); i++)
		    	{    
		    		Object oVal = (Object) _vData.get(i);
		    		if (oVal != null && oVal instanceof Persistent)
		    		{
		    			Persistent oVal2 = (Persistent) oVal;
		    			vID.add(oVal2.getPrimaryKey().toString());
		    		}
		    	}
    		}
    		catch(Exception _oEx)
    		{
    			log.error(_oEx);
    		}
    	}
    	return vID;
    }

    /**
     * create list of user defined field keys of passed object
     * List<Item> / "itemId"
     * List<Record> / "item_id"
     * 
     * @param _vData List of Persistent / List Of Record
     * @param _sKeyFieldName Field Name / Column Name
     * @return
     */
    public static List toListOfKeys(List _vData, String _sKeyFieldName)
    {
    	List vID = new ArrayList();
    	if (_vData != null)
    	{
    		try
    		{
		    	for (int i = 0; i < _vData.size(); i++)
		    	{    
		    		Object oVal = (Object) _vData.get(i);
		    		if (oVal != null && oVal instanceof Persistent)
		    		{
		    			Persistent oVal2 = (Persistent) oVal;
		    			String sVal = (String) BeanUtil.invokeGetter(oVal2, _sKeyFieldName);
		    			vID.add(sVal);
		    		}
		    		if (oVal != null && oVal instanceof Record)
		    		{
		    			Record oVal2 = (Record) oVal;
		    			String sVal = (String) oVal2.getValue(_sKeyFieldName).asString();
		    			vID.add(sVal);
		    		}
		    	}
    		}
    		catch(Exception _oEx)
    		{
    			log.error(_oEx);
    		}
    	}
    	return vID;
    }

    /**
     * create birthdate (month and day) criteria from keywords to specific column
     * 
     * @param _oCrit
     * @param _sPeerColumn
     * @param _sKeywords
     * @throws Exception
     */
    public static void setBirthDayCriteria(Criteria _oCrit, String _sPeerColumn, String _sKeywords)
    	throws Exception
    {
    	if(_oCrit != null && StringUtil.isNotEmpty(_sPeerColumn) && StringUtil.isNotEmpty(_sKeywords))
    	{
    		log.debug("*** Column: " + _sPeerColumn + " Keywords: " + _sKeywords);
    		String sDaySep = "-";
    		String sMonSep = "/";
    		String sMon = _sKeywords;
			if(_sKeywords.indexOf(sMonSep) > 0)
			{
	    		String sDay = StringUtil.substring(_sKeywords, 0, _sKeywords.indexOf(sMonSep));
				log.debug("*** sDay: " + sDay);
				if(StringUtil.containsIgnoreCase(sDay, sDaySep)) //multi day 01-31
				{
					String sDayFr = StringUtil.substring(sDay, 0, sDay.indexOf(sDaySep));
					String sDayTo = StringUtil.substring(sDay, sDay.indexOf(sDaySep) + 1);
					log.debug("*** sDayFr: " + sDayFr);
					log.debug("*** sDayTo: " + sDayTo);
					
					_oCrit.and(_sPeerColumn, (Object)(dayCol(_sPeerColumn) + " >= '" + sDayFr + "'"), Criteria.CUSTOM);
					_oCrit.and(_sPeerColumn, (Object)(dayCol(_sPeerColumn) + " <= '" + sDayTo + "'"), Criteria.CUSTOM);            			
				}
				else
				{
					_oCrit.and(_sPeerColumn, (Object)(dayCol(_sPeerColumn) + " = '" + sDay + "'"), Criteria.CUSTOM);
				}
				sMon = StringUtil.substring(_sKeywords, _sKeywords.indexOf(sMonSep) + 1);
				if(StringUtil.containsIgnoreCase(sMon, sMonSep)) //remove year
				{
					log.debug("*** sMon: " + sMon);
					sMon = StringUtil.substring(sMon, 0, sMon.indexOf(sMonSep));
				}
			}
			log.debug("*** sMon: " + sMon);
			_oCrit.and(_sPeerColumn, (Object)(SqlUtil.monthCol(_sPeerColumn) + " = '" + sMon + "'"), Criteria.CUSTOM);
    	}
    }
    
    /**
     * 
     * @param _vData List of data (Record)
     * @param _sKeyCol Key Column
     * @param _sValueCol Value Column
     * @param _iOp 1 -> Overwrite, 2 -> CSV, 3 -> SUM ADD, 4 -> SUM SUB
     * @return
     */
    public static Map createDataMap(List _vData, String _sKeyCol, String _sValueCol, int _iOp)
    {
    	Map mResult = new HashMap(_vData.size());
    	for (int i = 0; i < _vData.size(); i++)
    	{
    		try 
    		{
        		Record oRec = (Record) _vData.get(i);
        		String sKey = oRec.getValue(_sKeyCol).asString();
        		if(_iOp == 3 || _iOp == 4)
        		{
        			Number nVal = oRec.getValue(_sValueCol).asBigDecimal();
        			if(_iOp == 3) TemplateUtil.addToMap(mResult, sKey, nVal);
        			if(_iOp == 4) TemplateUtil.subToMap(mResult, sKey, nVal);        		
        		}
        		else
        		{
        			String sVal = oRec.getValue(_sValueCol).asString();
        			if(_iOp == 1)
        			{
        			}
        			if(_iOp == 2)
        			{
        		    	String sExist = (String) mResult.get(sKey);
        		    	if (StringUtil.isNotEmpty(sExist))
        		    	{
        		    		sVal = sExist + "," + sVal;        		    		
        		    	}        				
        			}
        			mResult.put(sKey, sVal);
        		}
			} 
    		catch (Exception e) 
    		{
    			e.printStackTrace();
    			log.error(e);    			
			}
    	}
    	return mResult;
    } 
    
    /**
     * create JSON from List of Record
     * 
     * @param _vData
     * @return
     */
    public static String createJSON(List _vData)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("[\n");
    	for (int i = 0; i < _vData.size(); i++)
    	{
    		try 
    		{
        		Record oRec = (Record) _vData.get(i);
        		Schema oSch = oRec.schema();
        		
        		sb.append("{");        		        	        		
        		int iSize = oRec.size();
        		for (int j = 1; j <= iSize; j++)
        		{        			
        			String sColName = oSch.column(j).name();
        			sb.append("\"").append(sColName).append("\":");
        			String sVal = "";
        			if(oRec.getValue(j) != null) sVal = oRec.getValue(j).asString();
        			//column name contains *json*
        			if(StringUtil.containsIgnoreCase(sColName, "json") &&
        			   StringUtil.isNotEmpty(sVal) && sVal.trim().startsWith("{") && sVal.trim().endsWith("}"))
        			{        		
        				sb.append(sVal).append("\n");
        			}
        			else 
        			{
	        			sVal = StringUtil.replace(StringUtil.cleanForJS(sVal),"\n","<br>");	        			
	        			sb.append("\"").append(sVal).append("\"");
        			}        			
        			if(j != iSize) sb.append(",");
        			sb.append("\n");
        		}
        		sb.append("}");
        		if(i != _vData.size() - 1) sb.append(",");
        		sb.append("\n");
			} 
    		catch (Exception e) 
    		{
    			e.printStackTrace();
    			log.error(e);    			
			}
    	}
    	sb.append("]");
    	return sb.toString();
    } 
    
    /**
     * get JDBC Connection to another Database Server 
     * assumed the JDBC driver already loaded
     * 
     * @param _sURL
     * @param _sUser
     * @param _sPwd
     * @return
     * @throws Exception
     */
  	public static Connection getConn(String _sURL, String _sUser, String _sPwd)                                                                                 
		throws Exception                                                                                                
	{                                                                                                                   
		if(StringUtil.isNotEmpty(_sURL))
		{    		
			if(StringUtil.isEmpty(_sUser)) _sUser = "root";
			if(StringUtil.isEmpty(_sPwd)) _sPwd = "crossfire";
			Properties oProp = new Properties();
			oProp.put("user", _sUser);
			oProp.put("password", _sPwd);
			Connection conn = DriverManager.getConnection(_sURL, oProp);
			return conn;						
		}
		return null;
	}    

  	/**
  	 * close a connection object
  	 * 
  	 * @param conn
  	 * @throws Exception
  	 */
	public static void closeConn(Connection conn)                                                                                 
		throws Exception                                                                                                
	{                                                                                                                   
		if(conn != null)
		{
			try {
				conn.close();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
		}
	}
}
