package com.ssti.framework.tools;

import java.util.List;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base OM Tool interface
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: Tool.java,v 1.3 2007/02/23 14:14:04 albert Exp $ <br>
 * 
 * <pre>
 * $Log: Tool.java,v $
 * Revision 1.3  2007/02/23 14:14:04  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public interface Tool 
{    
    public Object getDataByID (String _sID) throws Exception;
	
	public List getAllData() throws Exception;
}
