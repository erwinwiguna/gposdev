package com.ssti.framework.word.servlet;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import com.ssti.framework.tools.HttpUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Create excel 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: WordServlet.java,v 1.1 2009/05/04 01:28:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: WordServlet.java,v $
 * Revision 1.1  2009/05/04 01:28:18  albert
 * *** empty log message ***
 *
 * Revision 1.6  2008/06/29 07:11:01  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/02/23 14:12:03  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class WordServlet extends HttpServlet 
{
    public static final String s_HELPER_CLASS  = "helper_class";
    public static final String s_URL  = "url";
    
    
    private Map m_oParam = null;

    public void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException 
	{
        try 
        {
            HttpSession oSession = request.getSession();
            m_oParam = request.getParameterMap();
			String sClassName = StringUtil.getString(m_oParam, s_HELPER_CLASS);
			String sURL = StringUtil.getString(m_oParam, s_URL);
			//check parameter completeness 					            
            if (StringUtil.isNotEmpty(sURL))
            {
            	HttpClient oClient = new HttpClient();
            	oClient.setState(HttpUtil.copyState(request));
            	
            	GetMethod oGet = new GetMethod(sURL);
            	HttpUtil.setHeader(oGet, oClient.getState());
            	
            	String sContent = "";
            	int iStatus = oClient.executeMethod(oGet);
            	if (iStatus != -1) 
            	{
            		sContent = oGet.getResponseBodyAsString();
            		oGet.releaseConnection();
            		
            		response.setContentType("application/ms-word");
            		response.getOutputStream().write(sContent.getBytes());
            		response.getOutputStream().flush();
            	}
            }
        }
        catch (Exception _oEx) 
        {
          	_oEx.printStackTrace();
            throw new ServletException(_oEx);
        }
    }
}
