package com.ssti.framework.xml;


/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.2  2007/07/02 15:37:32  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/06/30 13:32:08  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public interface XMLLoader 
{   
	public void setXML(String xml);
	public void setUsername(String user);
	public String getResult();
	public String getSummary();
	public void load();
}                                                                                           