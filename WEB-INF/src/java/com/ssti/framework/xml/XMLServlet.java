package com.ssti.framework.xml;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssti.framework.tools.StringUtil;


/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Receive file upload from store
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: HOUploadServlet.java,v 1.4 2008/02/26 05:15:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: XMLServlet,v $
 * 
 * </pre><br>
 *
 */
public class XMLServlet extends HttpServlet 
{
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException 
	{
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException 
	{
		try 
		{
			Map m = request.getParameterMap();
			
			System.out.println("doPOST");
			String sUser = StringUtil.getString(m,"user");			
			String sDoc = StringUtil.getString(m,"doc");
			String sHelper = StringUtil.getString(m,"helper");
			
			if (sDoc != null && sHelper != null && sHelper != "")
			{
				System.out.println("helper: " + sHelper);
				System.out.println("doc: " + sDoc);
				XMLLoader oXML = (XMLLoader) Class.forName(sHelper).newInstance();
				oXML.setXML(sDoc);
				oXML.setUsername(sUser);
				oXML.load();				
				response.getWriter().write("XML Sending Result: \n" + oXML.getResult());
			}
			else
			{
				response.getWriter().write("Invalid Parameter");
			}
			
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			String sError = "ERROR : Processing XML Failed : " + _oEx.getMessage();
			System.out.println(sError);
		}

	}
}
