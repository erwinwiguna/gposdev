package com.ssti.framework.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: XMLTransformer.java,v 1.3 2007/02/23 14:14:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: XMLTransformer.java,v $
 * Revision 1.3  2007/02/23 14:14:17  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class XMLTransformer 
{	
	/**
	 * Transform XML using XSL
	 * 
	 * @param _sInputFile
	 * @param _sXSLFile
	 * @param _sOutputFile
	 * @throws TransformerException
	 * @throws TransformerConfigurationException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void transform (String _sInputFile, String _sXSLFile, String _sOutputFile)
    	throws TransformerException, TransformerConfigurationException, FileNotFoundException, IOException
	{
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer(new StreamSource(_sXSLFile));
        transformer.transform(
        	new StreamSource(_sInputFile), 
        		new StreamResult(new FileOutputStream(_sOutputFile)));
	
	}
	
	/**
	 * "Usage : XMLTransformer.transform  input_file  xsl_file  output_file"
	 * 
	 * @param args 
	 */
	public static void main(String[] args)
  	{  
  		try 
  		{
  			if (args.length < 3) 
  			{
  				System.out.println ("Usage : XMLTransformer.transform  input_file  xsl_file  output_file");
  			}
  			else 
  			{
  				transform (args[0],args[1],args[2]);
  				System.out.println ("XML successfily transformed into  : " + args[2]);
  			}
  		}
  		catch (Exception _oEx)
  		{
  			System.out.println ("ERROR Transforming XML : " + _oEx.getMessage());  			
  		}
  	}
}
