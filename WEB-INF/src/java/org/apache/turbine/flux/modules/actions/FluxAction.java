package org.apache.turbine.flux.modules.actions;

import org.apache.turbine.flux.tools.FluxTool;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.presentation.SecureAction;

public class FluxAction extends SecureAction
{
    public FluxAction()
    {
    }

    public void doPerform(RunData rundata, Context context1)
        throws Exception
    {
    }

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if (data.getACL().hasRole(FluxTool.s_ADMIN))
        {
        	return true;
        }
        data.setScreenTemplate(s_NO_PERM_TPL);
        return false;
    }
}