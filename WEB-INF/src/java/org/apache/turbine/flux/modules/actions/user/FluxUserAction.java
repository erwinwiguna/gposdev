package org.apache.turbine.flux.modules.actions.user;

import java.util.Date;

import org.apache.turbine.flux.modules.actions.FluxAction;
import org.apache.turbine.om.security.Group;
import org.apache.turbine.om.security.User;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.services.security.UserManager;
import org.apache.turbine.services.security.db.DBUserManager;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.turbine.util.security.PasswordMismatchException;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

public class FluxUserAction extends FluxAction
{
    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
	    if (StringUtil.isNotEmpty(data.getParameters().getString("eventSubmit_doChangepassword")))
	    {
	    	return true;
	    }
    	return super.isAuthorized(data);
	}
	
    public void doPerform(RunData data, Context context)
    {
        data.setMessage(LocaleTool.getString("action_notfound"));
    }
    
    public void doInsert(RunData data, Context context)
    {    	
    	try
		{
	        User user = TurbineSecurity.getUserInstance();
	        data.getParameters().setProperties(user);
	        String username = data.getParameters().getString("username");
	        String password = data.getParameters().getString("password");
	        //boolean createEmp = data.getParameters().getBoolean("CreateEmployee");
	        
	        if(password == null) password = "";
	        if(TurbineSecurity.accountExists(username))
	        {
	            context.put("username", username);
	            context.put("user", user);
	            data.getParameters().add("mode", "insert");
	            data.setMessage(LocaleTool.getString("user_exist"));
	        	return;
	        } 
	        else
	        {
	            Date now = new Date();
	            user.setCreateDate(now);
	            user.setLastLogin(new Date(0L));
	            TurbineSecurity.addUser(user, password);
	        }
	    	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
		}
    	catch (Exception _oEx)
		{
    		_oEx.printStackTrace();
	    	data.setMessage(LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
		}
    }

    public void doRoles(RunData data, Context context)
    {
        try
		{
	    	String username = data.getParameters().getString("username");
	        User user = TurbineSecurity.getUser(username);
	        AccessControlList acl = TurbineSecurity.getACL(user);
	        Group groups[] = TurbineSecurity.getAllGroups().getGroupsArray();
	        org.apache.turbine.om.security.Role roles[] = TurbineSecurity.getAllRoles().getRolesArray();
	        for(int i = 0; i < groups.length; i++)
	        {
	            String groupName = groups[i].getName();
	            for(int j = 0; j < roles.length; j++)
	            {
	                String roleName = roles[j].getName();
	                String groupRole = groupName + roleName;
	                String formGroupRole = data.getParameters().getString(groupRole);
	                if(formGroupRole != null && !acl.hasRole(roles[j], groups[i]))
	                {
	                    TurbineSecurity.grant(user, groups[i], roles[j]);
	                }
	                else if(formGroupRole == null && acl.hasRole(roles[j], groups[i]))
	                {
	                	TurbineSecurity.revoke(user, groups[i], roles[j]);
	                }
	            }
	        }
	        data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
		}
        catch (Exception _oEx)
		{
        	_oEx.printStackTrace();
	        data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());        
		}
	}

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
    	try
		{
    		User user = TurbineSecurity.getUser(data.getParameters().getString("username"));
	        data.getParameters().setProperties(user);
	        Date now = new Date();
	        user.setCreateDate(now);
	        user.setLastLogin(new Date(0L));
	        TurbineSecurity.saveUser(user);
	        data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
		}
        catch (Exception _oEx)
		{
        	_oEx.printStackTrace();
	        data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());        
		}        
    }

    public void doDelete(RunData data, Context context)
	    throws Exception
	{
		try
		{    	
			User user = TurbineSecurity.getUser(data.getParameters().getString("username"));
			TurbineSecurity.removeUser(user);
			//EmployeeTool.deleteEmployeeData (data.getParameters().getString("username"));
			data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
		}
	}

	public void doChangepassword (RunData data, Context context)
        throws Exception
    {
        UserManager manager = new DBUserManager();
        try 
		{
	        manager.changePassword (data.getUser(), 
	        						data.getParameters().getString("OldPassword"), 
	        						data.getParameters().getString("NewPassword"));
			data.setMessage (LocaleTool.getString("pass_changed"));
    	}
    	catch (PasswordMismatchException pmex) 
		{
			data.setMessage (LocaleTool.getString("old_pass_incorrect"));
    	}    
    }
    
    public void insertEmployeeData (RunData data)
        throws Exception
    {
		try
		{
			Employee oEmployee = new Employee();
			oEmployee.setEmployeeName (data.getParameters().getString("FirstName") + " " + data.getParameters().getString ("LastName"));
			oEmployee.setUserName (data.getParameters().getString("UserName"));
			oEmployee.setEmail (data.getParameters().getString("Email"));
			oEmployee.setJobTitle (data.getParameters().getString("JobTitle"));						
			oEmployee.setUpdateDate(new Date());
			oEmployee.save();
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }
}