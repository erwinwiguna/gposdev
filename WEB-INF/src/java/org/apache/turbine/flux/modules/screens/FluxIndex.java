package org.apache.turbine.flux.modules.screens;

import org.apache.turbine.flux.tools.FluxTool;
import org.apache.turbine.modules.screens.VelocityScreen;
import org.apache.turbine.services.velocity.TurbineVelocity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

public class FluxIndex extends VelocityScreen
{
    public FluxIndex()
    {
    }

    protected void doBuildTemplate(RunData data)
        throws Exception
    {
        if(isAuthorized(data))
            doBuildTemplate(data, TurbineVelocity.getContext(data));
    }

    public void doBuildTemplate(RunData rundata, Context context1)
    {
    }

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        String fluxAdminRole = FluxTool.CONFIG.getString("flux.admin.role");
        boolean isAuthorized = false;
        AccessControlList acl = data.getACL();
        if(acl == null || !acl.hasRole(fluxAdminRole))
        {
            data.getTemplateInfo().setScreenTemplate(FluxTool.CONFIG.getString("template.login"));
            data.setScreen(FluxTool.CONFIG.getString("screen.login"));
            isAuthorized = false;
        } 
        else if(acl.hasRole(fluxAdminRole))
        {
        	isAuthorized = true;	
        }
        return isAuthorized;
    }
}