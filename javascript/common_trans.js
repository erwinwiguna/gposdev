/**
 * Common JS function used in transactions
 * 
 * 2016-07-13
 * - move setRate, validateRate & validate Date from TransCommonJS.vm to here
 * - new Dependency currencyList JSON & dateConfig
 
 * 2016-06-29
 * - initial commit, move most of TransCommonJS function here
 * - dependency: JS Global variables in Layouts (sCTX, sTPL, bEDIT)  
 */
var JSFMTNO = ",**0.*0";
function setCurrencyRate()
{
    if(typeof curr == 'undefined')
    {
        jQuery.post(sTPL + 'setup,MultiCurrencyDetails.vm', 'op=1', 
            function(resp){ curr=resp; setRate(curr);},'json');    
    }
    else {setRate(curr);}       
}
function setRate(curr)
{
    var f = document.frm;
    var i = f.CurrencyId.selectedIndex;

    f.CurrencyRate.value = curr[i].rate;
    f.FiscalRate.value   = curr[i].fiscal;
    
    if(curr[i].default == 'true')
    {
        f.CurrencyRate.readOnly = true;
        f.FiscalRate.readOnly = true;
    }
    else
    {
        f.CurrencyRate.readOnly = false;
        f.FiscalRate.readOnly = false;
    }
}
function validateRate()
{
    var f = document.frm;
    var i = f.CurrencyId.selectedIndex;
    var c = curr[i];
    if(c && c.default && c.default == 'false' && f.CurrencyRate && f.FiscalRate)
    {
        var dRate = f.CurrencyRate.value;
        var dFisc = f.FiscalRate.value;
        if (dRate == "0" || parseFloat(dRate) <= 0)
        {
            alert ('$!l10n.fill_in Currency Rate'); f.CurrencyRate.focus(); return false;
        }
        if(dRate == "1" && !confirm('$!l10n.rate ($!oCurr.CurrencyCode) = 1 ?'))
        {
            return false;
        }
        if(dFisc == "1" && !confirm('$!PaymentDueDatel10n.fiscal_rate ($!oCurr.CurrencyCode) = 1 ?'))
        {
            return false;
        }
    }
    return true;
}
function validateDate(sDate)
{
    var dLastEnd = '';
    if(dateConfig.lastClosed != '')
    {
        dLastEnd = new Date(dateConfig.lastClosed);
    }; 
    
	var dStart = new Date(dateConfig.startDate);                      
    if (sDate.length < dateConfig.defaultFormat.length)
    {
        alert ('Invalid Date Format should be ' + dateConfig.defaultFormat);
        return false;
    }

    var dTrans = moment(sDate,'DD/MM/YYYY');
    if(dTrans < dStart)
    {
        alert ('Invalid Date ' + sDate + ',Start Date is ' + dateConfig.StartDate + ' !');
        return false;
    }
    if(dLastEnd != '' && dTrans <= dLastEnd)
    {
        alert ('Invalid Date ' + sDate + ',Period is already closed !');
        return false;
    }
    var dNow = new Date();
    if (dTrans > dNow)
    {
        alert ('Invalid Date ' + sDate + ', Date is in the future !');
        return false;
    }
    return true;
}
function setDate (dateForm)
{
	if (dateForm && dateForm.value == "")
	{
	    dateForm.value = moment().format('DD/MM/YYYY');
	    try{ if(sTDT && sTDT != '') dateForm.value = sTDT; }
	    catch(e){ console.log('ERROR' + e);}		
	}
}
function handleSearchByNameKeypress(e)
{
    var key = getKey(e);
    if(key == 13) {getItemDataByName();}
}
function handleSearchByDescriptionKeypress(e)
{
    var key = getKey(e);
    if(key == 13) {getItemDataByDescription();}
}
function handleCodeKeypress(e)
{
    var f = document.frm;
    if (f.ItemCode && f.ItemCode.value != '')
    {
        var key = getKey(e);
        if (key == 13) {getItemData ();}
    }
}
function getItemDataByName()
{
    var f = document.frm
    var sName = f.ItemName.value;
    if(sName != '' && sName.indexOf("'") < 0 && sName.length >= 3) 
    {                 
        f.ItemName.style.backgroundColor = '#fff';
        var url = sTPL + 'transaction,ajax,ItemNameLookup.vm';
        var param = 'ItemName=' + sName;
        jQuery.post(url,param,function(resp){eval(resp);} );                
    }
    else { f.ItemName.style.backgroundColor = '#fdd'; }
}
function getItemDataByDescription()
{
    var sDesc = document.frm.Description.value;
    if (sDesc != '' && sDesc.indexOf("'") < 0 && sDesc.length >= 3)
    {
        var url = sTPL + 'transaction,ajax,ItemNameLookup.vm';
        var param = 'Description=' + sDesc;
        jQuery.post(url,param,function(resp){eval(resp);} );
    }
}
function viewNameSelector()
{
    var f = document.frm;
    var param = '';                              
    if(f.LocationId) param += '&LocationId=' + f.LocationId.value; 
    if(f.IsADJ) param += '&isADJ=' + f.IsADJ.value;
    if(f.IsPUR) param += '&isPUR=' + f.IsPUR.value;
    if(f.IsPOS) param += '&IsPOS=' + f.IsPOS.value;    
    if(sSCR != 'undefined') param += '&from=' + sSCR;
    var url = sTPL + 'transaction,ItemNameSelector.vm?mode=view' + param;                    
    jQuery.modal('<iframe src="' + url + '" class="modal-iframe">');
}
function displayAll()
{
    document.location.href = sSCR;
}
function setDirectAdd()
{
    var f = document.frm;
    var da = f.DirectAdd;
    var ta = f.txtAutoFlag;
    if(da)
    {
        if(da.value == 'false')
        {
            da.value = true;
            if(ta) ta.value = 'AUTO';
            if(jQuery('#ItemCode')) {
                jQuery('#ItemCode').attr('placeholder', 'Code [A]');
                jQuery('#ItemCode').css('background', '#fff');
            }
        }
        else
        {
            da.value = false;
            if(ta) ta.value = 'MANUAL';
            if(jQuery('#ItemCode')) {
                jQuery('#ItemCode').attr('placeholder', 'Code [M]');
                jQuery('#ItemCode').css('background', '#eee');
            }            
        }
    }
}
function calculator()
{
    var url = sTPL + 'Calculator.vm';
    jQuery.modal('<iframe src="' + url + '" class="modal-iframe">');
}
function setFocus(name, select)
{
    var f = document.frm;
    try
    {
        if(select) eval('f.' + name + '.select();');
        else eval('f.'+ name + '.focus();')
    }
    catch(e) {}
}
function setMemo (key, no)
{
    if (key != '' && no != '')
    {
        var url = sTPL + 'transaction,DetailMemoSelector.vm?key=' + key +
                   '&no=' + no + '&edit=' + bEDIT;
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});
    }
}
function setHeaderRemark (key, bEdit, bSave)
{
    if (key != '')
    {
        var url = sTPL + 'transaction,HeaderRemarkSetter.vm?key=' + key +
                   '&no=' + no + '&edit=' + bEdit + '&save=' + bSave;
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});
    }
}
function setRemark (key, no)
{
    if (key != '' && no != '')
    {
        var url = sTPL + 'transaction,DetailRemarkSetter.vm?key=' + key +
                   '&no=' + no + '&edit='  + bEDIT;
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});
    }
}

function setShowTab(t)
{
    currShow = document.getElementById(t);
}
function setCancelRemark(key, desc, date, id, js)
{
    if (validateDate(date) && key != '')
    {
        var url = sTPL + 'transaction,SetCancelRemark.vm?key=' + key +
                  '&desc=' + desc + '&id=' + id + '&edit=' + bEDIT;
        if(js && js !== 'undefined') {url += '&js=true';}
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe" style="height:320px;">',{
           containerCss:{width:480,height:360},
           overlayClose:true
         });
    }
}
function setDept(key, no)
{
    if (key != '' && no != '')
    {
        var url = sTPL + 'transaction,DepartmentProjectSelector.vm?key=' + key +
                   '&no=' + no + '&edit=' + bEDIT;
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});
    }
}
function setBatchNo(key, no)
{
    if (key != '' && no != '')
    {
        var url = sTPL + 'transaction,DetailBatchNoSetter.vm?key=' + key +
                   '&no=' + no + '&edit=' + bEDIT; 
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});                   
    }
}
function setSerialNo(key, no, qty)
{
    if (key != '' && no != '')
    {
        var url = sTPL + 'transaction,serial,DetailSerialNoSetter.vm?key=' + key +
                   '&no=' + no + '&qty=' + qty + '&edit=' + bEDIT;
        jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});                   
    }
}
function printBarcodeLabelJS(sesKey, bRemoteLocal, locid, tplFile) 
{    
    if(typeof(locid) == 'undefined')  { locid = ''; }
    if(typeof(tplFile) == 'undefined' || tplFile == '')  { tplFile = 'BarcodeLabel.vm'; }    
    var url = sCTX + '/print?helper_class=com.ssti.framework.print.helper.GenericPrintHelper' +
              '&txt_tpl=/print/report/' + tplFile + '&print_type=1&detail_per_page=999' + 
              '&ses_key=' + sesKey + '&loc_id=' + locid;               
    jQuery.get(url,'', function(resp){
        console.log(resp);
        if(bRemoteLocal == 'true') printToLocal(resp);
    });
}
function printToLocal(docs)
{
    var url = 'http://localhost:8080/';
    var param = {"dest":"1", "docs":docs};
                       
    jQuery.ajax({
        url: 'http://localhost:8080/',
        type: 'POST',
        data: param,
        cache: false,
    }).done(function(resp){            
        console.log('response From POSClient:' + resp);
        if(resp.indexOf('ERROR:') != -1) alert(resp);
    })
    .fail( function(xhr, stat, err){
        alert('ERROR:' + err + ' STATUS:' + stat + ' MSG:' + xhr.responseText);
    });    
}

