// Original JavaScript code by Duncan Crombie: dcrombie@chirp.com.au
// Please acknowledge use of this code by including this header.

// CONSTANTS
var separator = ",";  // use comma as 000's separator
var decpoint = ".";   // use period as decimal point
var percent = "%";
var currency = "$";  // use dollar sign for currency

function formatNumber(number, format, print) 
{  
    // use: formatNumber(number, "format")
    if (print) document.write("formatNumber(" + number + ", \"" + format + "\")<br>");

    if (number - 0 != number) return null;              // if number is NaN return null
    var useSeparator = format.indexOf(separator) != -1; // use separators in number
    var usePercent = format.indexOf(percent) != -1;     // convert output to percentage
    var useCurrency = format.indexOf(currency) != -1;   // use currency format
    var isNegative = (number < 0);
    
    number = Math.abs (number);
    
    if (usePercent) number *= 100;
    
    format = strip(format, separator + percent + currency);  // remove key characters
    
    number = "" + number;  // convert number input to string

     // split input value into LHS and RHS using decpoint as divider
    var dec = number.indexOf(decpoint) != -1;
    var nleftEnd = (dec) ? number.substring(0, number.indexOf(".")) : number;
    var nrightEnd = (dec) ? number.substring(number.indexOf(".") + 1) : "";

     // split format string into LHS and RHS using decpoint as divider
    dec = format.indexOf(decpoint) != -1;
    var sleftEnd = (dec) ? format.substring(0, format.indexOf(".")) : format;
    var srightEnd = (dec) ? format.substring(format.indexOf(".") + 1) : "";

     // adjust decimal places by cropping or adding zeros to LHS of number
    if (srightEnd.length < nrightEnd.length) 
    {
        var nextChar = nrightEnd.charAt(srightEnd.length) - 0;
        nrightEnd = nrightEnd.substring(0, srightEnd.length);
        
        if (nextChar >= 5) nrightEnd = "" + ((nrightEnd - 0) + 1);  // round up

        // patch provided by Patti Marcoux 1999/08/06
        while (srightEnd.length > nrightEnd.length) {
            nrightEnd = "0" + nrightEnd;
        }

        if (srightEnd.length < nrightEnd.length) 
        {
            nrightEnd = nrightEnd.substring(1);
            nleftEnd = (nleftEnd - 0) + 1;
        }
    } 
    else 
    {
        for (var i=nrightEnd.length; srightEnd.length > nrightEnd.length; i++) 
        {
            if (srightEnd.charAt(i) == "0") nrightEnd += "0";  // append zero to RHS of number
            else break;
        }
    }

    // adjust leading zeros
    sleftEnd = strip(sleftEnd, "*");  // remove hashes from LHS of format
    
    while (sleftEnd.length > nleftEnd.length) 
    {
      nleftEnd = "0" + nleftEnd;  // prepend zero to LHS of number
    }

    if (useSeparator) nleftEnd = separate(nleftEnd, separator);  // add separator
    var output = nleftEnd + ((nrightEnd != "") ? "." + nrightEnd : "");  // combine parts
    output = ((useCurrency) ? currency : "") + output + ((usePercent) ? percent : "");
    if (isNegative) 
    {
        // patch suggested by Tom Denn 25/4/2001
        output = (useCurrency) ? "(" + output + ")" : "-" + output;
    }
    return output;
}

function strip(input, chars) 
{  
    // strip all characters in 'chars' from input
    var output = "";  // initialise output string
    for (var i=0; i < input.length; i++)
      if (chars.indexOf(input.charAt(i)) == -1)
        output += input.charAt(i);
    return output;
}

function separate(input, separator) 
{  
    // format input using 'separator' to mark 000's
    input = "" + input;
    var output = "";  // initialise output string
    for (var i=0; i < input.length; i++) {
      if (i != 0 && (input.length - i) % 3 == 0) output += separator;
      output += input.charAt(i);
    }
    return output;
}

function parse(sInput) 
{  
    var sResult = sInput.replace(/\b,\b/ig, '');
    var fResult = parseFloat(sResult);
    if (isNaN (fResult)) return 0;
    return fResult;
}

//Say Amount Related Procedures
function makeArray0() {
  for (i = 0; i<makeArray0.arguments.length; i++)
   this[i] = makeArray0.arguments[i];
}

var numbers = new makeArray0('','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifthteen','sixteen','seventeen','eighteen','nineteen');

var numbers10 = new makeArray0('','ten','twenty','thirty','fourty','fifty','sixty','seventy','eighty','ninety');

function sayAmount(input, sCurrency) 
{
  var dollars = Math.floor(input);
  var cents = Math.round((input*100 - dollars*100));

  var thousands = (dollars - dollars % 1000) / 1000;
  dollars -= thousands * 1000;
  
  var hundreds = (dollars - dollars % 100) / 100;
  dollars -= hundreds * 100;

  var output = '';

  output += (thousands > 0 ? fN(thousands) + ' thousand ' : '') +
            (hundreds > 0 ? fN(hundreds) + ' hundred ' : '') +
            (dollars > 0 ? fN(dollars) + ' ' : '') +
            ((thousands > 0 || hundreds > 0 || dollars > 0) ? sCurrency + ' ' : '') +
            ((Math.floor(input) > 0 && cents > 0) ? 'and ' : '') +
            (cents > 0 ? fN(cents) + ' cents' : '');

  return output.substring(0,1).toUpperCase() + output.substring(1);
}

function fN(i) 
{
  if (i<20) return numbers[i];
  var tens = (i - i % 10) / 10, units = i - (i - i % 10);
  return numbers10[tens] + ((tens > 0 && units > 0) ? '-' : '') + numbers[units];
}
