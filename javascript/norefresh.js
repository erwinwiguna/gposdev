window.history.forward(1);
function noRefresh(evt) 
{
    evt = (evt) ? evt : ((event) ? event : null);
    var key = getKey(evt);
    if (evt) 
    {                
        if (key == 8 ) //backspace
        { 
            var src = '';
            document.all ? src = evt.srcElement.type : src = evt.target.type;
            if (src != "text" && src != "textarea" && src != "password" && src != "select" && src != "input" && src != "search")
            {
                cancelKey(evt);
            }
        }
        //disable F1, F3, F5, F6, F7
        if (key == 112 || key == 114 || key == 116 || key == 117 || key == 118) //refresh 
        {
            cancelKey(evt);
        }
        else if (evt.altKey && (key == 37 || key == 38)) //alt + left & right
        {
            //not working in ff 1.0
            cancelKey(evt);
        }
        else if (evt.ctrlKey && (key == 78 || key == 82)) //ctrl + n & r
        {
            //not working in ff 1.0
            cancelKey(evt);
        }
    }
}
function cancelKey(evt) 
{
    if (evt.preventDefault)  //mozilla
    {
        evt.preventDefault();
        return false;
    }
    else //ie
    {
        evt.keyCode = 0;
        evt.returnValue = false;
    }
}
document.onkeydown = noRefresh;
document.oncontextmenu = new Function("return false;")
