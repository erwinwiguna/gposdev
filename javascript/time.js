var timerID = null;      
var timerRunning = false;             
var today = null;

months = new Array(12);
days = new Array(7);

months[0] = "January";
months[1] = "February";
months[2] = "March";
months[3] = "April";
months[4] = "May";
months[5] = "June";
months[6] = "July";
months[7] = "August";
months[8] = "September";
months[9] = "October";
months[10]= "November  ";
months[11]= "December  ";

days[0] = "Sunday";
days[1] = "Monday";
days[2] = "Tuesday";
days[3] = "Wednesday";
days[4] = "Thursday";
days[5] = "Friday ";
days[6] = "Saturday";

function stopclock()                 
{	
    if(timerRunning) clearTimeout(timerID);
    timerRunning = false;
}
function startclock(dt)
{
    today = new Date(dt); 
    stopclock();
    showtime();
}
function showtime()
{
    today.setSeconds(today.getSeconds() + 1);
    var dy=days[today.getDay()];	
    var MM=months[today.getMonth()];
    var dd=((today.getDate() < 10) ? "0" : "") + today.getDate();
    var yy=((today.getYear() < 1900) ? (today.getYear() + 1900) : today.getYear());
    var hh=today.getHours(); 
    var mm=((today.getMinutes() < 10) ? "0" : "") + today.getMinutes();
    var ss=((today.getSeconds() < 10) ? "0" : "") + today.getSeconds();
    var dtm = document.getElementById("DateTime")
    dtm.innerHTML = dy + ', ' + dd + ' ' + MM + ' ' + yy + ' - ' + hh + ':' + mm + ':' + ss + '&nbsp;';
    timerID = setTimeout("showtime()", 1000);
    timerRunning = true;
}
