function closeTrans()
{
    var f = document.frm;
    if (confirm ('PO will be closed, are you sure ?'))
    {
        f.save.value=1;
        f.TransactionStatus.value=3;
        f.action = sACT;
        f.submit();
    }
}
function unconfirmTrans()
{
    var f = document.frm;
    if (confirm ('PO will be unconfirmed, are you sure ?'))
    {
        f.save.value = 2;
        f.TransactionStatus.value=6;
        //f.action = sACT;
        f.submit();
    }
}
function generatePO()
{
    var f = document.frm;
    f.op.value = 8;
    f.gentarget.value = 'binary';
    f.action = sSCR;
    f.submit();
}
function openRequestLookUpWindow()
{
    var f = document.frm;
    if (f.VendorId.value == "")
    {
        alert ('Please select Vendor');
        f.VendorId.focus(); return false;
    }
    var param = 'mode=2&VendorId=' + f.VendorId.value + '&LocationId='+ f.LocationId.value;
    var url = sTPL + 'transaction,PurchaseRequestView.vm?' + param;
    var wndTx=window.open(url, 'wndItem', 'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=800,height=600,left=20,top=20');
    wndTx.focus();
}
function openSOLookUpWindow()
{
    var f = document.frm;
    if (f.VendorId.value == "")
    {
        alert ('Please select Vendor');
        f.VendorId.focus(); return false;
    }
    var param = 'fromTrans=true&transType=po&status=1&VendorId=' + f.VendorId.value + '&LocationId='+ f.LocationId.value;
    var url = sTPL + 'transaction,SalesOrderView.vm?' + param;
    var wndTx=window.open(url, 'wndItem', 'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=800,height=600,left=20,top=20');
    wndTx.focus();
}
function changeQtyManual(i,e)
{
    eval('document.frm.ChangedManual' + i + '.value=1');
    recalculate(i);
}
function handleQtyKeypress(i,e)
{
    if (getKey(e) == 13)
    {
        var n = parseInt(i) + 1;
        var f = document.frm;
        eval('if(f.TxtQty' + n +') f.TxtQty' + n + '.select(); else f.Qty' + n + '.select();');
    }
}
function recalculate (i)
{
    var f = document.frm;
    if (f.CurrencyRate.value == '') f.CurrencyRate.value = 1;

    if(i != '') eval('f.Qty' + i + '.value = parse(f.TxtQty' + i + '.value)');
    eval('f.ItemPrice' + i + '.value = parse(f.TxtItemPrice' + i + '.value)');

    var Qty = parseFloat(eval('f.Qty' + i + '.value'));
    if(eval('f.ItemPrice' + i + '.value') == '') eval('f.ItemPrice' + i + '.value=0');
    if(eval('f.Discount'  + i + '.value') == '') eval('f.Discount'  + i + '.value=0');
    if(eval('f.TaxAmount' + i + '.value') == '') eval('f.TaxAmount' + i + '.value=0');

    if(eval ('f.ItemPrice' + i + '.value') != '' &&
       eval ('f.Discount'  + i + '.value') != '' &&
       eval ('f.TaxAmount' + i + '.value') != '' )
    {
        var OldPrice  = parseFloat(eval ('f.OldPrice'+ i + '.value'));
        var ItemPrice = parseFloat(eval ('f.ItemPrice'+ i + '.value'));

        var Diff = (ItemPrice - OldPrice) * Qty
        if(i == '')
        {
            Diff = 0;
        }

        var TaxAmount     = parseFloat(eval ('f.TaxAmount'+ i + '.value')) / 100;

        var TotalPurchase = (Qty * ItemPrice);
        var TotalDiscount = calculateDiscount(i, TotalPurchase, 1);

        TotalPurchase     = TotalPurchase - TotalDiscount;
        var TotalTax      = TaxAmount * TotalPurchase;
        var CostPerUnit   = 0;

        eval ('f.SubTotalTax'  + i + '.value=' + TotalTax);
        eval ('f.SubTotalDisc' + i + '.value=' + TotalDiscount);
        eval ('f.SubTotal'     + i + '.value=' + TotalPurchase);
        eval ('f.TxtSubTotal'  + i + '.value=' + TotalPurchase);
        eval ('f.CostPerUnit'  + i + '.value=' + CostPerUnit);

        if (i != '')
        {
            f.TotalAmount.value = parseFloat(f.TotalAmount.value) + Diff;
        }
        eval('f.TxtSubTotal'  + i + '.value=formatNumber(TotalPurchase,JSFMTNO)');
        eval('f.TxtItemPrice' + i + '.value=formatNumber(ItemPrice,JSFMTNO)');
        if (i != '') eval('f.TxtQty' + i + '.value=formatNumber(Qty,JSFMTNO)');
    }
}
function openBankPaymentSelector(stitle)
{
    var f = document.frm;
    var url = sTPL + 'transaction,BankPaymentSelector.vm?title=PO Down Payment' +
               '&from=PO&Editable=' + f.Editable.value;
    var wndBank = window.open(url, 'wndBank',
        'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=480,height=280,left=20,top=20');
    wndBank.focus();
}
function validateDP()
{
    var f = document.frm;
    if(parseFloat(f.DownPayment.value) > 0)
    {
        if(f.BankId.value == "")
        {
            openBankPaymentSelector();
            return false;
        }
    }
    return true;
}
function cekDPBeforeSave()
{
    var f = document.frm;
    if(validateDP())
    {
        saveTransaction();
    }
}
function viewMemo(memoid)
{
    var url = sTPL + 'transaction,DebitMemoTransaction.vm?op=4&id=' + memoid;
    var wndMemo = window.open(url, 'wndMemo',
        'toolbar=no,scrollbars=yes,menubar=yes,resizable=yes,width=800,height=400,left=20,top=20');
    wndMemo.focus();
}
function createMemo(memoid)
{
    var f = document.frm;
    var url = sTPL + 'transaction,DebitMemoTransaction.vm?op=6&orderid=' + f.PurchaseOrderId.value;
    var wndMemo = window.open(url, 'wndMemo',
        'toolbar=no,scrollbars=yes,menubar=yes,resizable=yes,width=800,height=400,left=20,top=20');
    wndMemo.focus();
}
function setShipTo()
{
    var f = document.frm;
    var url = sTPL + 'transaction,LocationLookup.vm';
    jQuery.modal('<iframe src="' + url + '" class="modal-iframe">',{overlayClose:true});
}
function openImportItemWindow()
{
    var f = document.frm;
    var param = 'LocationId=' + f.LocationId.value +'&VendorId=' + f.VendorId.value +'&TransactionDate=' + f.TransactionDate.value + '&Remark=' + f.Remark.value;
    var url = sTPL + 'transaction,ImportPOItemExcel.vm?' +  param
    var wndExcel  = window.open (url,'wndExcel',
        'toolbar=no,scrollbars=yes,menubar=no,statusbar=yes,resizable=no,width=560,height=200,left=20,top=200');
    wndExcel.focus();
}
function confirmPO()
{
    var f = document.frm;
    f.TransactionStatus.value=1;
    cekDPBeforeSave();
}
function openApprovalForm()
{
    var f = document.frm;
    var url = sTPL + 'transaction,POApproval.vm?PoId=' + f.PurchaseOrderId.value ;
    var wndApp  = window.open (url,'wndApp',
        'toolbar=no,scrollbars=yes,menubar=no,statusbar=yes,resizable=no,width=680,height=360,left=40,top=40');
    wndApp.focus();
}
jQuery(document).ready(function(){
    var f = document.frm;
    jQuery('select[name=PaymentTermId]').on('change', function(){
        f.DueDate.value =
            moment(f.TransactionDate.value,'DD/MM/YYYY')
                .add(jQuery('option:selected',this).data('due'),'d').format('DD/MM/YYYY');
    });
});

