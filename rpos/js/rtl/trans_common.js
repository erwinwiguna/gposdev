/** 
   trans_common.js
   common javascript function for transaction screens, previously in TransCommonJS.vm
   move to .js file for caching & ease of maintenance purpose
   screens variables:
   -is adj 
   -is purchase  
   2016-06-10  
   - initial commit
 */

function getCustomerDataByID() 
{    
    var url = sTPL + 'transaction,CustomerDataLookup.vm';    
    var param = '&CustomerId=' + document.frm.CustomerId.value;    
    jQuery.post(url, param, function(resp){eval(resp);});
}
function handleCustomerNameKeypress (e) 
{
    var f = document.frm;
    if (f.CustomerName && f.CustomerName.value != '') 
    {
        if (getKey(e) == 13) {getCustomerDataByName();}
    }    
}
function handleCustomerCodeKeypress (e) 
{
    var f = document.frm;
    if (f.CustomerCode && f.CustomerCode.value != '') 
    {
        if (getKey(e) == 13) {getCustomerDataByCode();}
    }    
}
function getCustomerDataByName () 
{
    var url = sTPL + 'transaction,CustomerNameLookup.vm';
    var param = '&CustomerName=' + document.frm.CustomerName.value; 
    jQuery.post(url, param, function(resp){eval(resp);});
}
function getCustomerDataByCode () 
{
    var url = sTPL + 'transaction,CustomerNameLookup.vm';
    var param = '&CustomerCode=' + document.frm.CustomerCode.value; 
    jQuery.post(url, param, function(resp){eval(resp);});
}
function viewCustomerNameSelector () 
{
    var url = sTPL = 'transaction,CustomerNameSelector.vm';     
    var wndCust = window.open(url, 'wndCust', 
        'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=720,height=600,left=20,top=20');
    wndCust.focus();            
} 
 
var JSFMTNO = ",**0.*0";
function handleSearchByNameKeypress (e)
{
    var key = getKey(e);
    if(key == 13) {getItemDataByName();}
}
function handleSearchByDescriptionKeypress (e)
{
    var key = getKey(e);
    if(key == 13) {getItemDataByDescription();}
}
function handleCodeKeypress (e)
{
    var f = document.frm;
    if (f.ItemCode && f.ItemCode.value != '')
    {
        var key = getKey(e);
        if (key == 13) {getItemData ();}
    }
}
function getItemDataByName()
{
    var sName = document.frm.ItemName.value;
    if(sName != '')
    {
        var url = sTPL + 'transaction,ajax,ItemNameLookup.vm?ItemName=' + sName;
        var param = '';
        jQuery.post(url,param,function(resp){eval(resp);} );
    }
}
function getItemDataByDescription()
{
    var sDesc = document.frm.Description.value;
    if (sDesc != '')
    {
        var url =  sTPL + 'transaction,ajax,ItemNameLookup.vm?Description=' + sDesc;
        var param = '';
        jQuery.post(url,param,function(resp){eval(resp);} );
    }
}
function viewNameSelector()
{
    var f = document.frm;
    var sLocID = '';
    if (f.LocationId) sLocID = f.LocationId.value;
    var url = sTPL + 'transaction,ItemNameSelector.vm' +
               '?LocationId=' + sLocID + '&IsADJ=$!bIsADJ&IsPurchase=$!bIsPURCH';
    var wndItem = window.open(url, 'wndItem',
        'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=720,height=520,left=20,top=20');
    wndItem.focus();
}
function calculator ()
{
    var url = sTPL + 'Calculator.vm';
    window.open(url, 'calcWnd',
        'toolbar=no,scrollbars=no,statusbar=no,menubar=no,resizable=no,width=180,height=245,left=20,top=20');
}
function setFocus(name, select)
{
    var f = document.frm;
    if (eval('f.'+name))
    {
        try
        {
            if (select) eval('f.' + name + '.select();');
            else eval('f.'+ name + '.focus();')
        }
        catch(e) {}
    }
}
